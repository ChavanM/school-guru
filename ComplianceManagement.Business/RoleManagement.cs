﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class RoleManagement
    {
        public static List<Role> GetAllRoles()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where (row.ID == 2 || row.ID == 7 || row.ID == 8)
                             select row);

                return roles.ToList();
            }
        }

        public static string GetCustNameByID(int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string CName = (from row in entities.CustomerViews
                                where row.ID == CID && row.IsDeleted == false
                                select row.Name).FirstOrDefault();

                return CName;
            }
        }
        public static List<Role> GetLimitedRole(bool? onlyForCompliance = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.ID == 2 || row.ID == 7 || row.ID == 8 || row.ID==13 || row.ID == 9
                              || row.ID == 19
                             select row);

                if (onlyForCompliance.HasValue)
                {
                    roles = roles.Where(entry => entry.IsForCompliance == onlyForCompliance.Value && entry.Code != "APPR");
                }
                return roles.ToList();
            }
        }
        public static List<Role> GetAll(bool? onlyForCompliance = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.ID <= 19 || row.Code.Equals("SPADM")
                             || row.Code.Equals("HVADM") || row.Code.Equals("HVAUD")
                             //where row.ID < 14 || row.ID == 19 || row.Code.Equals("SPADM")
                             select row);

                if (onlyForCompliance.HasValue)
                {
                    roles = roles.Where(entry => entry.IsForCompliance == onlyForCompliance.Value && entry.Code != "APPR");
                }
                return roles.ToList();
            }
        }
              

        public static List<Role> GetAll_HRCompliance_Roles(int customerID = 0)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //List<string> hrRoleCodes = new List<string> { "CADMN", "HEXCT", "HMGR" };
                //List<string> hrRoleCodes = new List<string> { "CADMN", "HEXCT", "HMGMT", "HMGR" };
                //List<string> hrRoleCodes = new List<string> { "CADMN", "HEXCT", "HMGMT", "HMGR", "SPADM", "DADMN" };
                //List<string> hrRoleCodes = new List<string> { "CADMN", "HEXCT", "HMGMT", "HMGR", "DADMN" };
                List<string> hrRoleCodes = new List<string> { "CADMN", "HEXCT", "HMGR" , "HVAUD", "HVEND "};
                if (customerID != 0)
                {
                    var customerRecord = (from row in entities.Customers
                                          where row.ID == customerID
                                          select row).FirstOrDefault();

                    if (customerRecord != null)
                    {
                        if (customerRecord.IsServiceProvider != null)
                        {
                            if (Convert.ToBoolean(customerRecord.IsServiceProvider))
                                hrRoleCodes.Add("SPADM");
                        }

                        if (customerRecord.IsDistributor != null)
                        {
                            if (Convert.ToBoolean(customerRecord.IsDistributor))
                                hrRoleCodes.Add("DADMN");
                        }

                        if (customerRecord.ComplianceProductType != null)
                        {
                            if (Convert.ToInt32(customerRecord.ComplianceProductType) == 3)
                            {
                                hrRoleCodes.Add("HMGMT");
                                hrRoleCodes.Add("LSPOC");
                                hrRoleCodes.Add("HAPPR");
                            }
                        }
                    }
                }

                var roles = (from row in entities.Roles
                             where (hrRoleCodes.Contains(row.Code))
                             select row);

                return roles.ToList();
            }
        }

        public static List<Role> GetAll_Secretarial_Roles()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<string> secRoleCodes = new List<string> { "HDCS", "CS", "DRCTR" };

                var roles = (from row in entities.Roles
                             where row.IsForSecretarial == true
                             && secRoleCodes.Contains(row.Code)
                             select row);

                return roles.ToList();
            }
        }

        public static List<Role> GetLitigationRoles(bool? onlyForLitigation = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.IsForLitigation==true && row.ID<14
                             select row);

                if (onlyForLitigation.HasValue)
                {
                    roles = roles.Where(entry => entry.IsForLitigation == onlyForLitigation.Value);
                }
                return roles.ToList();
            }
        }

        public static Role GetByID(int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var role = (from row in entities.Roles
                            where row.ID == roleID
                            select row).SingleOrDefault();

                return role;
            }
        }

        public static Role GetByCode(string roleCode)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var role = (from row in entities.Roles
                            where row.Code == roleCode
                            select row).FirstOrDefault();

                return role;
            }
        }

        public static void Delete(int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //Role roleToDelete = new Role() { ID = roleID };
                //entities.Roles.Attach(roleToDelete);
                Role roleToDelete = (from row in entities.Roles
                                     where row.ID == roleID
                                     select row).FirstOrDefault();

                entities.Roles.Remove(roleToDelete);

                entities.SaveChanges();
            }
        }

        public static bool Exists(Role role)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Roles
                             where row.Name.Equals(role.Name)
                             select row);

                if (role.ID > 0)
                {
                    query = query.Where(entry => entry.ID != role.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void Update(Role role)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //Role roleToUpdate = new Role() { ID = role.ID };
                //entities.Roles.Attach(roleToUpdate);
                Role roleToUpdate = (from row in entities.Roles
                                     where row.ID == role.ID
                                     select row).FirstOrDefault();

                roleToUpdate.Name = role.Name;

                entities.SaveChanges();
            }
        }

        public static void Create(Role role)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Roles.Add(role);

                entities.SaveChanges();
            }
        }
    }
}
