﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Collections;
using System.Data;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
   
    public class TaskManagment
    {
        public static bool IsTaskAfterInternal(long ComplianceScheduleOnID, int roleID, string period, int CustomerBranchid, int customerID)
        {
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var documentData = (from row in entities.SP_TaskLinkingInstanceTransactionInternalView(customerID)
                                        where row.ParentID == null
                                        && row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                    if (documentData.Count != 0)
                    {
                        var isafter = documentData.Where(entry => (entry.IsAfter == true)).Count();

                        if (isafter > 0)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static List<Sp_StatuoryTaskActInternal_Result> GetInternalActsByUserID(int Customerid, int UserID, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Sp_StatuoryTaskActInternal(Customerid, UserID, flag)
                            select row);
                return acts.ToList();
            }
        }
        public static TaskForm GetTaskFormByTaskID(long taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var taskForm = (from row in entities.TaskForms
                            where row.TaskID == taskID
                            select row).FirstOrDefault();

                return taskForm;
            }
        }
        public static bool CreateUpdateTaskForm(TaskForm uploadedFormRecord)
        {
            bool saveSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    uploadedFormRecord.CreatedOn = DateTime.Now;
                    uploadedFormRecord.UpdatedOn = DateTime.Now;

                    var prevFormRecord = (from row in entities.TaskForms
                                          where row.TaskID == uploadedFormRecord.TaskID
                                          select row).FirstOrDefault();

                    if (prevFormRecord != null)
                    {
                        prevFormRecord.FileName = uploadedFormRecord.FileName;
                        prevFormRecord.FileData = uploadedFormRecord.FileData;
                        prevFormRecord.FilePath = uploadedFormRecord.FilePath;

                        prevFormRecord.UpdatedBy = uploadedFormRecord.UpdatedBy;
                        prevFormRecord.UpdatedOn = uploadedFormRecord.UpdatedOn;
                    }
                    else
                    {
                        entities.TaskForms.Add(uploadedFormRecord);                       
                    }

                    entities.SaveChanges();
                    saveSuccess = true;
                }
                return saveSuccess;
            }
            catch(Exception ex)
            {
                return saveSuccess;
            }
        }
        
        public static int? GetActualDays(int ParentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var taskType = (from row in entities.Tasks
                                where row.ID == ParentID
                                select row.ActualDueDays).SingleOrDefault();

                return taskType;
            }
        }
        public static bool IsTaskAfter(long ComplianceScheduleOnID, int roleID, string period, int CustomerBranchid, int customerID)
        {
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
                                        where row.ParentID == null
                                        && row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                    if (documentData.Count != 0)
                    {
                        var isafter = documentData.Where(entry => (entry.IsAfter == true)).Count();

                        if (isafter > 0)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool? GetTaskBeforeAfter(int TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var task = (from row in entities.Tasks
                            where row.ID == TaskID
                            select row.IsAfter).Distinct().FirstOrDefault();

                if(task == null)
                {
                    task =false;
                }
                return task;
            }
        }
        public static bool CheckTaskTransacion(long TaskInstanceID, long TaskScheduleOnID, long ComplianceScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CheckTaskTransaction = (from row in entities.TaskTransactions
                                where row.TaskInstanceId == TaskInstanceID && row.TaskScheduleOnID == TaskScheduleOnID
                                 && row.ComplianceScheduleOnID == ComplianceScheduleOnID && row.StatusId ==4
                                 select row).FirstOrDefault();

                if (CheckTaskTransaction != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static object GetAllSubTaskType(int TaskType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.TaskSubTypes
                               where row.TaskTypeID == TaskType
                               select row).ToList();

                return ObjTask;
            }
        }

        public static object GetTaskTypeData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.TaskTypes
                               select row).ToList();

                return ObjTask;
            }
        }

        public static bool IsExistTaskType(TaskType objtask)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var check = (from row in entities.TaskTypes
                             where row.Name == objtask.Name
                             select row).ToList();
                if (check.Count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static void CreateTaskType(TaskType objtask)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.TaskTypes.Add(objtask);
                entities.SaveChanges();
            }
        }

        public static void CreateSubTaskType(TaskSubType objtask)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.TaskSubTypes.Add(objtask);
                entities.SaveChanges();
            }
        }

        public static bool IsExistSubTaskType(TaskSubType objtask)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var check = (from row in entities.TaskSubTypes
                             where row.Name == objtask.Name
                             && row.TaskTypeID == objtask.TaskTypeID
                             select row).ToList();
                if (check.Count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static bool IsTaskAssignmentAvailable(long taskInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.TaskAssignments
                                      where row.TaskInstanceID == taskInstanceId
                                      select row).ToList();

                if (ComplianceData.Count > 0)
                    return true;
                else
                    return false;
            }
        }
        public static void RemoveUpcomingTaskSubTaskSchudule(int taskID, DateTime deactiveDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var task = (from row in entities.Tasks
                            where row.MainTaskID == taskID
                            select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

                task.ForEach(taskentry =>
                {
                    var taskInstances = (from row in entities.TaskInstances
                                         where row.IsDeleted == false && row.TaskId == taskentry.ID
                                         select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                    //taskInstances
                    taskInstances.ForEach(taskInstancesentry =>
                    {
                        var taskScheduleOns = (from row in entities.TaskScheduleOns
                                               where row.TaskInstanceID == taskInstancesentry.ID && row.ScheduleOn >= deactiveDate
                                               select row).ToList();
                        //taskScheduleOns
                        taskScheduleOns.ForEach(taskScheduleOnssentry =>
                        {
                            var taskAssignments = (from row in entities.TaskAssignments
                                                   where row.TaskInstanceID == taskScheduleOnssentry.TaskInstanceID
                                                   select row).ToList();

                            var taskTransactions = (from row in entities.TaskTransactions
                                                    where row.TaskInstanceId == taskScheduleOnssentry.TaskInstanceID
                                                    && row.TaskScheduleOnID == taskScheduleOnssentry.ID //&& row.StatusId == 1
                                                    select row).ToList();

                            if (taskTransactions.Count < 2)
                            {
                                //Remove taskTransactions
                                taskTransactions.ForEach(entry =>
                                entities.TaskTransactions.Remove(entry));
                                entities.SaveChanges();

                                taskAssignments.ForEach(taskAssignmentsentry =>
                                {
                                    var taskReminders = (from row in entities.TaskReminders
                                                         where row.TaskAssignmentID == taskAssignmentsentry.ID
                                                         && row.TaskDueDate >= deactiveDate
                                                         select row).ToList();
                                    //Remove taskReminders
                                    taskReminders.ForEach(entry =>
                                        entities.TaskReminders.Remove(entry));
                                    entities.SaveChanges();
                                });
                                //Remove taskScheduleOns
                                entities.TaskScheduleOns.Remove(taskScheduleOnssentry);
                                entities.SaveChanges();
                            }
                        });
                    });

                });

            }
        }

        public static void RemoveUpcomingSubTaskkSchudule(int taskID, DateTime deactiveDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var task = (from row in entities.Tasks
                            where row.ID == taskID 
                            select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

                task.ForEach(taskentry =>
                {
                    var taskInstances = (from row in entities.TaskInstances
                                           where row.IsDeleted == false && row.TaskId == taskentry.ID
                                         select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                //taskInstances
                taskInstances.ForEach(taskInstancesentry =>
                {
                    var taskScheduleOns = (from row in entities.TaskScheduleOns
                                                 where row.TaskInstanceID == taskInstancesentry.ID && row.ScheduleOn >= deactiveDate
                                                 select row).ToList();
                    //taskScheduleOns
                    taskScheduleOns.ForEach(taskScheduleOnssentry =>
                    {
                        var taskAssignments = (from row in entities.TaskAssignments
                                                     where row.TaskInstanceID == taskScheduleOnssentry.TaskInstanceID
                                                     select row).ToList();

                        var taskTransactions = (from row in entities.TaskTransactions
                                                      where row.TaskInstanceId == taskScheduleOnssentry.TaskInstanceID
                                                      && row.TaskScheduleOnID == taskScheduleOnssentry.ID //&& row.StatusId == 1
                                                      select row).ToList();

                        if (taskTransactions.Count < 2)
                        {
                            //Remove taskTransactions
                            taskTransactions.ForEach(entry =>
                            entities.TaskTransactions.Remove(entry));
                            entities.SaveChanges();

                            taskAssignments.ForEach(taskAssignmentsentry =>
                            {
                                var taskReminders = (from row in entities.TaskReminders
                                                           where row.TaskAssignmentID == taskAssignmentsentry.ID
                                                           && row.TaskDueDate >= deactiveDate
                                                           select row).ToList();
                                //Remove taskReminders
                                taskReminders.ForEach(entry =>
                                    entities.TaskReminders.Remove(entry));
                                entities.SaveChanges();
                            });
                            //Remove taskScheduleOns
                            entities.TaskScheduleOns.Remove(taskScheduleOnssentry);
                            entities.SaveChanges();
                        }
                    });
                });

                });

            }
        }


        public static void ChangeTaskSubTaskStatus(int taskID, DateTime? DeactiveDate, string Status, string Desc)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var TaskIDList = (from row in entities.Tasks
                                   where row.MainTaskID == taskID
                                   select row.ID).ToList();
                if (TaskIDList.Count > 0)
                {
                    TaskIDList.ForEach(entrytask =>
                    {
                        Task TaskStatus = (from row in entities.Tasks
                                           where row.ID == entrytask
                                           select row).FirstOrDefault();

                        TaskStatus.Status = Status;
                        TaskStatus.DeactivateOn = DeactiveDate;
                        TaskStatus.DeactivateDesc = Desc;
                        entities.SaveChanges();
                    });
                }
            }
        }
        public static void ChangeTaskStatus(int taskID, DateTime? DeactiveDate, string Status, string Desc)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Task TaskStatus = (from row in entities.Tasks
                                               where row.ID == taskID
                                   select row).FirstOrDefault();

                TaskStatus.Status = Status;
                TaskStatus.DeactivateOn = DeactiveDate;
                TaskStatus.DeactivateDesc = Desc;
                entities.SaveChanges();
            }
        }
        public static List<Sp_GetTaskAssignedCompliance_Result> GetComplianceForTask(int UserID, int actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var events = entities.Sp_GetTaskAssignedCompliance(UserID, actid);
                if (events != null)
                {
                    return events.ToList();
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<SP_GetMgmtMappedCompliance_Result> GetMgmtComplianceForTask(long CustomerID, int UserID,string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var events = entities.SP_GetMgmtMappedCompliance((int?)CustomerID,UserID, flag);
                if (events != null)
                {
                    return events.ToList();
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<Sp_GetTaskAssignedACT_Result> BindAct(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actlst = (from row in entities.Sp_GetTaskAssignedACT(Userid)
                              select row).Distinct();
                return actlst.ToList();
            }
        }

        public static List<object> BindActmGMT(long customerid,int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var acts = (from row in entities.SP_GetMgmtMappedCompliance((int?)customerid, Userid, "S")
                            select new { ID = row.ActID, Name = row.ActName }).OrderBy(entry => entry.Name).ToList<object>();
                acts = acts.Distinct().ToList(); 
                return acts;
            }
        }
        public static void DeleteTaskAssignmentFromCompliance(long ComplianceID, int CustomerBranchID, int RoleID,int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var GetSubTaskCount = (from row in entities.SP_TaskInstanceTransactionStatutoryView(Customerid)
                                       where row.ComplianceID == ComplianceID
                                       && row.CustomerBranchID == CustomerBranchID && row.RoleID == RoleID
                                       select row.TaskInstanceID).Distinct().ToList();

                if (GetSubTaskCount.Count > 0)
                {
                    GetSubTaskCount.ForEach(TaskInstancesentry =>
                    {
                        var assignmentData = (from row in entities.TaskAssignments
                                              where row.TaskInstanceID == TaskInstancesentry && row.RoleID == RoleID 
                                              select row).FirstOrDefault();

                        List<TaskReminder> reminderData = (from row in entities.TaskReminders
                                                           where row.TaskAssignmentID == assignmentData.ID
                                                           select row).ToList();

                        reminderData.ForEach(entry =>
                               entities.TaskReminders.Remove(entry)
                            );


                        entities.TaskAssignments.Remove(assignmentData);
                        entities.SaveChanges();
                    });
                }
            }
        }

        public static void DeleteTaskAssignmentFromInternalCompliance(long ComplianceID, int CustomerBranchID, int RoleID, int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var GetSubTaskCount = (from row in entities.SP_TaskInstanceTransactionInternalView(Customerid)
                                       where row.ComplianceID == ComplianceID
                                       && row.CustomerBranchID == CustomerBranchID && row.RoleID == RoleID
                                       select row.TaskInstanceID).ToList();

                if (GetSubTaskCount.Count > 0)
                {
                    GetSubTaskCount.ForEach(TaskInstancesentry =>
                    {
                        var assignmentData = (from row in entities.TaskAssignments
                                              where row.TaskInstanceID == TaskInstancesentry && row.RoleID == RoleID
                                              select row).FirstOrDefault();

                        List<TaskReminder> reminderData = (from row in entities.TaskReminders
                                                           where row.TaskAssignmentID == assignmentData.ID
                                                           select row).ToList();

                        reminderData.ForEach(entry =>
                               entities.TaskReminders.Remove(entry)
                            );


                        entities.TaskAssignments.Remove(assignmentData);
                        entities.SaveChanges();
                    });
                }
            }
        }

        public static ComplianceInstance  GetComplianceIDFromInstance(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compilance = (from row in entities.ComplianceInstances
                            where row.ID == ComplianceInstanceID
                            select row).SingleOrDefault();

                return compilance;
            }
        }


        public static InternalComplianceInstance GetInternalComplianceIDFromInstance(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var internalCompilance = (from row in entities.InternalComplianceInstances 
                            where row.ID == ComplianceInstanceID
                            select row).SingleOrDefault();

                return internalCompilance;
            }
        }
        public static long GetTaskIDFromInstance(long taskInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var task = (from row in entities.TaskInstances
                            where row.ID == taskInstanceID
                            select row.TaskId).SingleOrDefault();

                return task;
            }
        }

        public static List<TaskAssignedInstancesView> GetAllTaskAssignedInstances(int branchID = -1, int userID = -1, int customerId = -1, string filter = "")
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.TaskAssignedInstancesViews
                                                select row);

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }

                return complianceInstancesQuery.OrderByDescending(entry => entry.TaskInstanceID).ToList();

            }
        }

        public static void ReassignTask(int oldUserID, int newUserID, List<Tuple<int, int>> chkList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var cil in chkList)
                {
                    var assignmentData = (from row in entities.TaskAssignments
                                          where row.TaskInstanceID == cil.Item1 && row.RoleID == cil.Item2 && row.UserID == oldUserID
                                          select row).FirstOrDefault();

                    if (assignmentData != null)
                    {
                        assignmentData.UserID = newUserID;
                    }
                }

                entities.SaveChanges();
            }
        }
        public static bool DeleteTaskAssignmentTask(int UserID, List<Tuple<int, int>> chkList,int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var cil in chkList)
                {
                    long taskid = GetTaskIDFromInstance(cil.Item1);
                    var GetSubTaskCount = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerid)
                                           where row.ParentID ==  taskid && row.RoleID == cil.Item2
                                           select row.MainTaskID).ToList();

                    if (GetSubTaskCount.Count > 1)
                    {
                        return false;
                    }
                    else
                    {
                        var assignmentData = (from row in entities.TaskAssignments
                                              where row.TaskInstanceID == cil.Item1 && row.RoleID == cil.Item2 && row.UserID == UserID
                                              select row).FirstOrDefault();

                        List<TaskReminder> reminderData = (from row in entities.TaskReminders
                                                                         where row.TaskAssignmentID == assignmentData.ID
                                                                         select row).ToList();

                        reminderData.ForEach(entry =>
                               entities.TaskReminders.Remove(entry)
                            );


                        entities.TaskAssignments.Remove(assignmentData);
                   }
                }
                entities.SaveChanges();
                return true;
            }
        }

        public static List<SP_TaskInstanceTransactionStatutoryView_Result> GetAllTaskAssignedInstancesReassginExcludeStatutory(int userID, string filter, int customerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerId)
                                                where row.UserID == userID
                                                select row);

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                //if (branchID != -1)
                //{
                //    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                //}

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.TaskTitle.Contains(filter) || entry.Role.Contains(filter));
                }
                return complianceInstancesQuery.OrderByDescending(entry => entry.TaskInstanceID).ToList();

            }
        }

        public static List<SP_TaskInstanceTransactionInternalView_Result> GetAllTaskAssignedInstancesReassginExcludeInternal(int userID, string filter, int customerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var TaskInstancesQuery = (from row in entities.SP_TaskInstanceTransactionInternalView(customerId)
                                          where row.UserID == userID
                                          select row);

                if (customerId != -1)
                {
                    TaskInstancesQuery = TaskInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                //if (branchID != -1)
                //{
                //    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                //}

                if (!string.IsNullOrEmpty(filter))
                {
                    TaskInstancesQuery = TaskInstancesQuery.Where(entry => entry.TaskTitle.Contains(filter) || entry.Role.Contains(filter));
                }
                return TaskInstancesQuery.OrderByDescending(entry => entry.TaskInstanceID).ToList();
            }
        }

        public static void ReplaceUserForTaskAssignmentNew(int oldUserID, int newUserID, List<Tuple<int, string, int, string>> TaskInstanceIdList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                foreach (var cil in TaskInstanceIdList)
                {
                    int role = -1;
                    if (cil.Item2.Equals("Performer"))
                        role = 3;
                    if (cil.Item2.Equals("Reviewer1"))
                        role = 4;
                    if (cil.Item2.Equals("Reviewer2"))
                        role = 5;
                    if (cil.Item2.Equals("Approver"))
                        role = 6;

                    var assignmentData = (from row in entities.TaskAssignments
                                          where row.TaskInstanceID == cil.Item1 && row.RoleID == role && row.UserID == cil.Item3
                                          select row).FirstOrDefault();

                    if (assignmentData != null)
                    {
                        assignmentData.UserID = newUserID;
                    }
                }
                entities.SaveChanges();
            }
        }

        public static List<SP_TaskInstanceTransactionStatutoryView_Result> GetAllAssignedPerformerListOfStatutoty(int userID, long customerID, int branchID, string filter)//, int branchID, string filter
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ListOfTaskinstanceID = (from row in entities.TaskAssignments
                                            where row.RoleID == 4 && row.UserID == userID
                                            select row.TaskInstanceID).ToList();

                var userid = (from row in entities.TaskAssignments
                              where row.RoleID == 3 && ListOfTaskinstanceID.Contains(row.TaskInstanceID)
                              select row.UserID
                                ).Distinct().ToList();

                var TaskInstancesQuery = (from row in entities.SP_TaskInstanceTransactionStatutoryView(Convert.ToInt32(customerID))
                                          where ListOfTaskinstanceID.Contains(row.TaskInstanceID) && row.RoleID == 3
                                          && userid.Contains((long) row.UserID)
                                          select row).ToList();

                if (branchID != -1)
                {
                    TaskInstancesQuery = TaskInstancesQuery.Where(entry => entry.CustomerBranchID == branchID).ToList();
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    TaskInstancesQuery = TaskInstancesQuery.Where(entry => entry.ShortDescription.Contains(filter) || entry.Branch.Contains(filter) || entry.Role.Contains(filter) || entry.User.Contains(filter)).ToList();
                }

                return TaskInstancesQuery.OrderByDescending(entry => entry.TaskInstanceID).ToList();
            }
        }

        public static List<SP_TaskInstanceTransactionInternalView_Result> GetAllAssignedPerformerListOfInternal(int userID, long customerID, int branchID, string filter) //, int branchID, string filter
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ListOfTaskinstanceID = (from row in entities.TaskAssignments
                                            where row.RoleID == 4 && row.UserID == userID
                                            select row.TaskInstanceID).ToList();

                var userid = (from row in entities.TaskAssignments
                              where row.RoleID == 3 && ListOfTaskinstanceID.Contains(row.TaskInstanceID)
                              select row.UserID
                                ).Distinct().ToList();



                var TaskInstancesQuery = (from row in entities.SP_TaskInstanceTransactionInternalView(Convert.ToInt32(customerID))
                                          where ListOfTaskinstanceID.Contains(row.TaskInstanceID) && row.RoleID == 3
                                          && userid.Contains((long) row.UserID)
                                          select row).ToList();

                if (branchID != -1)
                {
                    TaskInstancesQuery = TaskInstancesQuery.Where(entry => entry.CustomerBranchID == branchID).ToList();
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    TaskInstancesQuery = TaskInstancesQuery.Where(entry => entry.ShortDescription.Contains(filter) || entry.Branch.Contains(filter) || entry.Role.Contains(filter) || entry.User.Contains(filter)).ToList();
                }

                return TaskInstancesQuery.OrderByDescending(entry => entry.TaskInstanceID).ToList();
            }
        }

        public static Compliance GetComplianceDetails(long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance compliance = (from row in entities.Compliances
                                         where row.ID == ComplianceID
                                         select row).SingleOrDefault();

                return compliance;
            }
        }

        public static InternalCompliance GetInternalComplianceDetails(long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalCompliance compliance = (from row in entities.InternalCompliances
                                                 where row.ID == ComplianceID
                                                 select row).SingleOrDefault();

                return compliance;
            }
        }
        public static bool CheckTaskScheduleOnPresentFromTask(long TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.TaskAssignments
                             join row3 in entities.TaskInstances
                             on row.TaskInstanceID equals row3.ID
                             join row1 in entities.TaskScheduleOns
                             on row.TaskInstanceID equals row1.TaskInstanceID
                             where row3.TaskId == TaskID
                             select row).ToList().Distinct().FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
     
        public static List<int> GetAssignedLocationSUBTASKList(int UserID, int custID, int TaskId)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.TaskAssignedInstancesViews
                             join row1 in entities.CustomerBranches
                             on row.CustomerBranchID equals row1.ID
                             where row1.CustomerID == custID &&
                             row.TaskID == TaskId
                             select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                if (query != null)
                    LocationList = query.Select(a => a.ID).Distinct().ToList();

                return LocationList;
            }
        }
        public static List<NameValueHierarchy> GetAllHierarchyForSubTask(long customerID, int taskId)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntitiesForSubTask(item, true, entities, taskId);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntitiesForSubTask(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, int taskId)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                join row1 in entities.TaskInstances
                                                on row.ID equals row1.CustomerBranchID
                                                where row1.TaskId == taskId
                                                where row.IsDeleted == false
                                                select row);
            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntitiesForSubTask(item, false, entities, taskId);
            }
        }
        public static bool CheckTaskTypeApplicable(int CustomerID, int userID, int TaskType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CheckType = (from row in entities.Tasks
                                 join row1 in entities.TaskInstances
                                 on row.ID equals row1.TaskId
                                 join row2 in entities.TaskAssignments
                                 on row1.ID equals row2.TaskInstanceID
                                 where row.CustomerID == CustomerID && row.TaskType == TaskType
                                 && row2.UserID == userID
                                 select row).FirstOrDefault();

                if (CheckType != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        //public static bool CheckTaskTypeApplicable(int CustomerID, int userID, int TaskType)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var CheckType = (from row in entities.Tasks
        //                         join row1 in entities.TaskInstances
        //                         on row.MainTaskID equals row1.TaskId
        //                         join row2 in entities.TaskAssignments
        //                         on row1.ID equals row2.TaskInstanceID
        //                         where row.CustomerID == CustomerID && row.TaskType == TaskType
        //                         && row2.UserID == userID
        //                         select row).FirstOrDefault();

        //        if (CheckType != null)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //}
        public static List<Sp_StatuoryTaskSubType_Result> GetSubTypeByUserID(int CustomerId, int UserID, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Sp_StatuoryTaskSubType(CustomerId, UserID, flag)
                            select row);
                return acts.ToList();
            }
        }
        public static List<object> GetAllTaskAssignedInternalCategory(int CustomerId, int UserID, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Sp_InternalTaskCategory(CustomerId, UserID, flag)
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                acts = acts.Distinct().ToList();
                return acts;
            }
        }
        public static List<object> GetAllTaskAssignedCategory(int CustomerId, int UserID, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Sp_StatutoryTaskCategory(CustomerId, UserID, flag)
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                acts = acts.Distinct().ToList();
                return acts;
            }
        }
        public static List<object> GetAllAssignedType(int Customerid, int UserID, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Sp_StatutoryTaskType(Customerid, UserID, flag)
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                acts = acts.Distinct().ToList();
                return acts;
            }
        }
        public static List<object> GetAllAssignedInternalType(int Customerid, int UserID, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Sp_InternalTaskType(Customerid, UserID, flag)
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                acts = acts.Distinct().ToList();
                return acts;
            }
        }
        public static List<Sp_StatuoryTaskAct_Result> GetActsByUserID(int Customerid, int UserID, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Sp_StatuoryTaskAct(Customerid, UserID, flag)
                            select row);
                return acts.ToList();
            }
        }
        public static bool SubTaskNameExistOrnot(Task task)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.Tasks
                               where row.Title.ToUpper().Trim() == task.Title.ToUpper().Trim()
                               && row.CustomerID == task.CustomerID
                               && row.ID != task.ID && row.TaskType == task.TaskType
                               && row.MainTaskID == task.MainTaskID && row.Isdeleted == false
                               select row.Title).FirstOrDefault();

                if (ObjTask != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool GetTaskAssignedData(long TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.Tasks
                               join row1 in entities.TaskInstances
                               on row.ID equals row1.TaskId
                               join row2 in entities.TaskAssignments
                               on row1.ID equals row2.TaskInstanceID
                               where row.ID == TaskID
                               select row2).FirstOrDefault();

                if (ObjTask != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool CheckSubtaskDataDeleted(int taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.Tasks
                               where row.ParentID == taskID && row.Isdeleted == false
                               select row).FirstOrDefault();

                if (ObjTask != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static void DeleteTaskRecord(int taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Task taskToUpdate = (from row in entities.Tasks
                                     where row.ID == taskID
                                     select row).FirstOrDefault();

                if (taskToUpdate != null)
                {
                    taskToUpdate.Isdeleted = true;
                    entities.SaveChanges();
                }
            }
        }

        public static byte? SubTaskComplianceFrequency(int MainTaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.Tasks
                               join row1 in entities.TaskComplianceMappings
                               on row.MainTaskID equals row1.TaskID
                               join row2 in entities.Compliances
                               on row1.ComplianceID equals row2.ID
                               where row.MainTaskID == MainTaskID
                               select row2.Frequency).FirstOrDefault();

                return ObjTask;
            }
        }

        public static byte? SubTaskComplianceFrequencyInternal(int MainTaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.Tasks
                               join row1 in entities.TaskComplianceMappings
                               on row.MainTaskID equals row1.TaskID
                               join row2 in entities.InternalCompliances
                               on row1.ComplianceID equals row2.ID
                               where row.MainTaskID == MainTaskID
                               select row2.IFrequency).FirstOrDefault();

                return ObjTask;
            }
        }

        public static List<SP_TaskInstanceTransactionInternalView_Result> GetCannedReportTaskInternal(int Customerid, int userID, string userRole, CannedReportFilterNewStatus status, int location, int type, int category, int ActID, DateTime? FromDate, DateTime? ToDate, int typeID, int subTypeID, string StringType, int RoleID, int selectedUserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<SP_TaskInstanceTransactionInternalView_Result> transactionsQuery = new List<SP_TaskInstanceTransactionInternalView_Result>();

                transactionsQuery = (from row in entities.SP_TaskInstanceTransactionInternalView(Customerid)
                                     select row).ToList();

                if (userRole != "" && userRole != "MGMT")
                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == RoleID
                        && (entry.IsUpcomingNotDeleted != false)).ToList();
                else
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == RoleID && (entry.IsUpcomingNotDeleted != false)).ToList();

                    transactionsQuery = (from row in transactionsQuery
                                         join row1 in entities.EntitiesAssignmentInternals
                                         on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid
                                         && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID
                                         select row).Distinct().ToList();
                }

                //Status
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.TaskStatusID == 1 || entry.TaskStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 6).ToList();
                        break;
                }

                //taskType Filter -- Statutory--Internal
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.TaskType == Convert.ToInt32(type))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                //Type -- Central - State
                if (typeID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceTypeId == typeID).ToList();

                //Performer-Reviewer User Filter
                if (selectedUserID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.UserID == selectedUserID)).ToList();
                }

                // category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToInt32(category))).ToList();
                }

                // Act Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(ActID))).ToList();
                }

                //Due Date Filter
                if (FromDate != null && ToDate != null)
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();

                // Find data through String contained in Description
                if (StringType != "" && transactionsQuery.Count > 0)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.TaskDescription.Contains(StringType))).ToList();
                }

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }

        //public static List<SP_TaskInstanceTransactionInternalView_Result> GetCannedReportTaskInternal(int Customerid, int userID, string userRole, CannedReportFilterNewStatus status, int location, int type, int category, DateTime? FromDate, DateTime? ToDate, int typeID, int subTypeID, string StringType, int RoleID, int selectedUserID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        List<SP_TaskInstanceTransactionInternalView_Result> transactionsQuery = new List<SP_TaskInstanceTransactionInternalView_Result>();

        //        transactionsQuery = (from row in entities.SP_TaskInstanceTransactionInternalView(Customerid)
        //                             select row).ToList();

        //        if (userRole != "" && userRole != "MGMT")
        //            transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == RoleID
        //                && (entry.IsUpcomingNotDeleted != false)).ToList();
        //        else
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == RoleID && (entry.IsUpcomingNotDeleted != false)).ToList();

        //            transactionsQuery = (from row in transactionsQuery
        //                                 join row1 in entities.EntitiesAssignmentInternals
        //                                 on (long) row.ComplianceCategoryId equals row1.ComplianceCatagoryID
        //                                 where row.CustomerID == Customerid
        //                                 && row.CustomerBranchID == row1.BranchID
        //                                 && row1.UserID == userID
        //                                 select row).Distinct().ToList();
        //        }

        //        //Status
        //        switch (status)
        //        {
        //            case CannedReportFilterNewStatus.Upcoming:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.TaskStatusID == 1 || entry.TaskStatusID == 10)).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.Overdue:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10) && entry.ScheduledOn < now).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.ClosedTimely:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 4).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.ClosedDelayed:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 5).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.PendingForReview:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 11)).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.Rejected:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 6).ToList();
        //                break;
        //        }

        //        //taskType Filter -- Statutory--Internal
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.TaskType == Convert.ToByte(type))).ToList();
        //        }

        //        //Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
        //        }

        //        //Type -- Central - State
        //        if (typeID != -1)
        //            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceTypeId == typeID).ToList();

        //        //Performer-Reviewer User Filter
        //        if (selectedUserID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.UserID == selectedUserID)).ToList();
        //        }

        //        // category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToInt32(category))).ToList();
        //        }

        //        //Due Date Filter
        //        if (FromDate != null && ToDate != null)
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();

        //        // Find data through String contained in Description
        //        if (StringType != "" && transactionsQuery.Count > 0)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.TaskDescription.Contains(StringType))).ToList();
        //        }

        //        return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
        //    }
        //}

        public static List<SP_TaskInstanceTransactionStatutoryView_Result> GetCannedReportTask(int Customerid, int userID, string userRole, CannedReportFilterNewStatus status, int location, int taskType, int category, int ActID, DateTime? FromDate, DateTime? ToDate, int typeID, int subTypeID, string StringType, int RoleID, int selectedUserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<SP_TaskInstanceTransactionStatutoryView_Result> transactionsQuery = new List<SP_TaskInstanceTransactionStatutoryView_Result>();

                transactionsQuery = (from row in entities.SP_TaskInstanceTransactionStatutoryView(Customerid)
                                     select row).ToList();

                if (userRole != "" && userRole != "MGMT")
                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == RoleID
                        && (entry.IsUpcomingNotDeleted != false)).ToList();
                else
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == RoleID && (entry.IsUpcomingNotDeleted != false)).ToList();

                    transactionsQuery = (from row in transactionsQuery
                                         join row1 in entities.EntitiesAssignments
                                         on (long) row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid
                                         && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID
                                         select row).Distinct().ToList();
                }

                //Status
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.TaskStatusID == 1 || entry.TaskStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 6).ToList();
                        break;
                }

                //taskType Filter -- Statutory--Internal
                if (taskType != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.TaskType == Convert.ToByte(taskType))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                //Type -- Central - State
                if (typeID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceTypeId == typeID).ToList();

                //SubType
                if (subTypeID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceSubTypeID == subTypeID).ToList();

                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToInt32(category))).ToList();
                }

                //Performer-Reviewer User Filter
                if (selectedUserID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.UserID == selectedUserID)).ToList();
                }

                // Act Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(ActID))).ToList();
                }

                //Date Filter
                if (FromDate != null && ToDate != null)
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();

                // Find data through String contained in Description
                if (StringType != "" && transactionsQuery.Count > 0)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.TaskDescription.Contains(StringType))).ToList();
                }

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }

        public static Task GetTaskByID(int TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var task = (from row in entities.Tasks
                            where row.ID == TaskID
                            select row).SingleOrDefault();

                return task;
            }
        }
        public static long GetComplianceID(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceid = (from row in entities.ComplianceInstances
                                        where row.ID == complianceInstanceID
                                        select row.ComplianceId).FirstOrDefault();

                return complianceid;
            }
        }


        public static long GetInternalComplianceID(long InternalcomplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceid = (from row in entities.InternalComplianceInstances
                                    where row.ID == InternalcomplianceInstanceID
                                    select row.InternalComplianceID).FirstOrDefault();

                return complianceid;
            }
        }

        public static Task GetTask(long taskInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Task = (from row in entities.Tasks
                                    join row1 in entities.TaskInstances
                                    on row.ID equals row1.TaskId
                                    where row1.ID == taskInstanceID
                                    select row).FirstOrDefault();

                return Task;
            }
        }

        public static TaskInstance GetTaskStartDate(long taskInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var taskInstance = (from row in entities.TaskInstances
                            where row.ID == taskInstanceID
                            select row).FirstOrDefault();

                return taskInstance;
            }
        }
        public static void GenerateTaskSchuduleFromStatutory(long TaskInstanceID, long complianceID, long complianceInstanceID, DateTime TaskStartDate, int? ActualDueDays,Boolean IsAfter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                    var complianceScheduleOns = (from row in entities.ComplianceScheduleOns
                                                 join row1 in entities .RecentComplianceTransactionViews
                                                 on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                                 where row.ComplianceInstanceID == complianceInstanceID 
                                                 && row.ID == row1.ComplianceScheduleOnID
                                                 && row.ScheduleOn >= TaskStartDate && row1.ComplianceStatusID == 1
                                                 select row).ToList();

                complianceScheduleOns.ForEach(complianceScheduleOnssentry =>
                {
                    DateTime scheduelon;
                    if (IsAfter == true)
                    {
                        scheduelon = complianceScheduleOnssentry.ScheduleOn.AddDays(Convert.ToInt64(ActualDueDays));
                    }
                    else
                    {
                        scheduelon = complianceScheduleOnssentry.ScheduleOn.AddDays(-Convert.ToInt64(ActualDueDays));
                    }

                    long TaskScheduleOnID = (from row in entities.TaskScheduleOns
                                             where row.TaskInstanceID == TaskInstanceID 
                                             && row.ScheduleOn == scheduelon  // complianceScheduleOnssentry.ScheduleOn.AddDays(-Convert.ToInt64(ActualDueDays)) 
                                             && row.ForPeriod == complianceScheduleOnssentry.ForPeriod
                                             // && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row.ID).SingleOrDefault();

                    if (TaskScheduleOnID <= 0)
                    {
                        TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
                        taskScheduleOn.TaskInstanceID = TaskInstanceID;
                        taskScheduleOn.ComplianceScheduleOnID = complianceScheduleOnssentry.ID;
                        taskScheduleOn.ForMonth = complianceScheduleOnssentry.ForMonth;
                        taskScheduleOn.ForPeriod = complianceScheduleOnssentry.ForPeriod;
                        taskScheduleOn.ScheduleOn = scheduelon; //complianceScheduleOnssentry.ScheduleOn.AddDays(-Convert.ToInt64(ActualDueDays));     //nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                        taskScheduleOn.IsActive = true;
                        taskScheduleOn.IsUpcomingNotDeleted = true;
                        entities.TaskScheduleOns.Add(taskScheduleOn);
                        entities.SaveChanges();

                        var AssignedTaskRole = GetAssignedTaskUsers((int) TaskInstanceID);
                        var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
                        if (performerTaskRole != null)
                        {
                            var user = UserManagement.GetByID((int) performerTaskRole.UserID);
                            TaskTransaction tasktransaction = new TaskTransaction()
                            {
                                TaskInstanceId = TaskInstanceID,
                                TaskScheduleOnID = taskScheduleOn.ID,
                                ComplianceScheduleOnID = complianceScheduleOnssentry.ID,
                                CreatedBy = performerTaskRole.UserID,
                                CreatedByText = user.FirstName + " " + user.LastName,
                                StatusId = 1,
                                Remarks = "New task assigned."
                            };

                            CreateTaskTransaction(tasktransaction);

                            foreach (var roles in AssignedTaskRole)
                            {
                                if (roles.RoleID != 6)
                                {
                                    CreateTaskReminders(TaskInstanceID, complianceID, roles.ID, complianceScheduleOnssentry.ScheduleOn.AddDays(-Convert.ToInt64(ActualDueDays)), complianceScheduleOnssentry.ID,IsAfter);
                                }
                            }
                        }
                    }
                });

            }
        }

        public static void GenerateTaskSchuduleFromInternal(long TaskInstanceID, long complianceID, long complianceInstanceID, DateTime TaskStartDate, int? ActualDueDays,Boolean IsAfter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceScheduleOns = (from row in entities.InternalComplianceScheduledOns
                                             join row1 in entities.InternalRecentComplianceTransactionViews
                                             on row.InternalComplianceInstanceID equals row1.InternalComplianceInstanceID
                                             where row.InternalComplianceInstanceID == complianceInstanceID
                                             && row.ID == row1.InternalComplianceScheduledOnID
                                             && row.ScheduledOn >= TaskStartDate && row1.InternalComplianceStatusID == 1
                                             select row).ToList();

                complianceScheduleOns.ForEach(complianceScheduleOnssentry =>
                {
                    DateTime scheduelon;
                    if (IsAfter == true)
                    {
                        scheduelon = complianceScheduleOnssentry.ScheduledOn.AddDays(Convert.ToInt64(ActualDueDays));
                    }
                    else
                    {
                        scheduelon = complianceScheduleOnssentry.ScheduledOn.AddDays(-Convert.ToInt64(ActualDueDays));
                    }
                    long TaskScheduleOnID = (from row in entities.TaskScheduleOns
                                             where row.TaskInstanceID == TaskInstanceID
                                             && row.ScheduleOn == scheduelon  // complianceScheduleOnssentry.ScheduleOn.AddDays(-Convert.ToInt64(ActualDueDays)) 
                                             && row.ForPeriod == complianceScheduleOnssentry.ForPeriod
                                             // && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row.ID).SingleOrDefault();

                    if (TaskScheduleOnID <= 0)
                    {
                        TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
                        taskScheduleOn.TaskInstanceID = TaskInstanceID;
                        taskScheduleOn.ComplianceScheduleOnID = complianceScheduleOnssentry.ID;
                        taskScheduleOn.ForMonth = complianceScheduleOnssentry.ForMonth;
                        taskScheduleOn.ForPeriod = complianceScheduleOnssentry.ForPeriod;
                        taskScheduleOn.ScheduleOn = scheduelon; // complianceScheduleOnssentry.ScheduledOn.AddDays(-Convert.ToInt64(ActualDueDays));
                        taskScheduleOn.IsActive = true;
                        taskScheduleOn.IsUpcomingNotDeleted = true;
                        entities.TaskScheduleOns.Add(taskScheduleOn);
                        entities.SaveChanges();

                        var AssignedTaskRole = GetAssignedTaskUsers((int) TaskInstanceID);
                        var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
                        if (performerTaskRole != null)
                        {
                            var user = UserManagement.GetByID((int) performerTaskRole.UserID);
                            TaskTransaction tasktransaction = new TaskTransaction()
                            {
                                TaskInstanceId = TaskInstanceID,
                                TaskScheduleOnID = taskScheduleOn.ID,
                                ComplianceScheduleOnID = complianceScheduleOnssentry.ID,
                                CreatedBy = performerTaskRole.UserID,
                                CreatedByText = user.FirstName + " " + user.LastName,
                                StatusId = 1,
                                Remarks = "New task assigned."
                            };

                            CreateTaskTransaction(tasktransaction);

                            foreach (var roles in AssignedTaskRole)
                            {
                                if (roles.RoleID != 6)
                                {
                                    DateTime schdulon;
                                    if (IsAfter == true)
                                    {
                                        schdulon = complianceScheduleOnssentry.ScheduledOn.AddDays(Convert.ToInt64(ActualDueDays));
                                    }
                                    else
                                    {
                                        schdulon = complianceScheduleOnssentry.ScheduledOn.AddDays(-Convert.ToInt64(ActualDueDays));
                                    }
                                    CreateTaskReminders(TaskInstanceID, complianceID, roles.ID, schdulon, complianceScheduleOnssentry.ID,IsAfter);
                                }
                            }
                        }
                    }
                });

            }
        }

        public static void CreateTaskReminders(long taskID, long complianceID, int TaskAssignmentId, DateTime scheduledOn, long complianceScheduleOnID,Boolean? IsAfter)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    Compliance compliance = (from row in entities.Compliances
                                             where row.ID == complianceID
                                             select row).FirstOrDefault();

                    if (compliance.SubComplianceType == 0)
                    {
                        if (compliance.ReminderType == 0)
                        {
                            if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                            {
                                TaskReminder reminder = new TaskReminder()
                                {
                                    TaskAssignmentID = TaskAssignmentId,
                                    ComplianceScheduleOnID = complianceScheduleOnID,
                                    ReminderTemplateID = 1,
                                    TaskDueDate = scheduledOn,
                                    RemindOn = scheduledOn.Date,
                                };
                                reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                entities.TaskReminders.Add(reminder);
                            }
                            else
                            {
                                TaskReminder reminder = new TaskReminder()
                                {
                                    TaskAssignmentID = TaskAssignmentId,
                                    ComplianceScheduleOnID = complianceScheduleOnID,
                                    ReminderTemplateID = 1,
                                    TaskDueDate = scheduledOn,
                                    RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                                };
                                reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                entities.TaskReminders.Add(reminder);


                                TaskReminder reminder1 = new TaskReminder()
                                {
                                    TaskAssignmentID = TaskAssignmentId,
                                    ComplianceScheduleOnID = complianceScheduleOnID,
                                    ReminderTemplateID = 1,
                                    TaskDueDate = scheduledOn,
                                    RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                                };
                                reminder1.Status = (byte) (reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                entities.TaskReminders.Add(reminder1);
                            }
                        }
                        else
                        {
                            if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                            {
                                TaskReminder reminder = new TaskReminder()
                                {
                                    TaskAssignmentID = TaskAssignmentId,
                                    ComplianceScheduleOnID = complianceScheduleOnID,
                                    ReminderTemplateID = 1,
                                    TaskDueDate = scheduledOn,
                                    RemindOn = scheduledOn.Date,
                                };
                                reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                entities.TaskReminders.Add(reminder);
                            }
                            else
                            {
                                DateTime TempScheduled;
                                if (IsAfter == true)
                                {
                                    TempScheduled = scheduledOn.AddDays((Convert.ToDouble(compliance.ReminderBefore)));
                                }
                                else
                                {
                                    TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.ReminderBefore)));
                                }

                                while (TempScheduled.Date < scheduledOn)
                                {
                                    TaskReminder reminder = new TaskReminder()
                                    {
                                        TaskAssignmentID = TaskAssignmentId,
                                        ComplianceScheduleOnID = complianceScheduleOnID,
                                        ReminderTemplateID = 1,
                                        TaskDueDate = scheduledOn,
                                        RemindOn = TempScheduled.Date,
                                    };

                                    reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.TaskReminders.Add(reminder);
                                    TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.ReminderGap));
                                }
                            }
                        }
                    }
                    else
                    {

                        if (compliance.ReminderType == 0)
                        {
                            if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 0)
                            {

                                TaskReminder reminder = new TaskReminder()
                                {
                                    TaskAssignmentID = TaskAssignmentId,
                                    ComplianceScheduleOnID = complianceScheduleOnID,
                                    ReminderTemplateID = 1,
                                    TaskDueDate = scheduledOn,
                                    RemindOn = scheduledOn.Date,
                                };
                                reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                entities.TaskReminders.Add(reminder);
                            }
                            else if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 1)
                            {

                                TaskReminder reminder = new TaskReminder()
                                {
                                    TaskAssignmentID = TaskAssignmentId,
                                    ComplianceScheduleOnID = complianceScheduleOnID,
                                    ReminderTemplateID = 1,
                                    TaskDueDate = scheduledOn,
                                    RemindOn = scheduledOn.Date,
                                };
                                reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                entities.TaskReminders.Add(reminder);

                            }
                            else if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                            {

                                TaskReminder reminder = new TaskReminder()
                                {
                                    TaskAssignmentID = TaskAssignmentId,
                                    ComplianceScheduleOnID = complianceScheduleOnID,
                                    ReminderTemplateID = 1,
                                    TaskDueDate = scheduledOn,
                                    RemindOn = scheduledOn.Date,
                                };
                                reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                entities.TaskReminders.Add(reminder);

                            }
                            else
                            {
                                if (compliance.Frequency.HasValue)
                                {
                                    var reminders = (from row in entities.ReminderTemplates
                                                     where row.Frequency == compliance.Frequency.Value && row.IsSubscribed == true
                                                     select row).ToList();

                                    reminders.ForEach(day =>
                                    {
                                        TaskReminder reminder = new TaskReminder()
                                        {
                                            TaskAssignmentID = TaskAssignmentId,
                                            ComplianceScheduleOnID = complianceScheduleOnID,
                                            ReminderTemplateID = 1,
                                            TaskDueDate = scheduledOn,
                                            RemindOn = scheduledOn.Date.AddDays(-1 * day.TimeInDays),
                                        };

                                        reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                        entities.TaskReminders.Add(reminder);

                                    });
                                }
                                else
                                {
                                    if (compliance.ComplianceType == 0 && compliance.EventID != null)
                                    {
                                        TaskReminder reminder = new TaskReminder()
                                        {
                                            TaskAssignmentID = TaskAssignmentId,
                                            ComplianceScheduleOnID = complianceScheduleOnID,
                                            ReminderTemplateID = 1,
                                            TaskDueDate = scheduledOn,
                                            RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                                        };
                                        reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.TaskReminders.Add(reminder);


                                        TaskReminder reminder1 = new TaskReminder()
                                        {
                                            TaskAssignmentID = TaskAssignmentId,
                                            ComplianceScheduleOnID = complianceScheduleOnID,
                                            ReminderTemplateID = 1,
                                            TaskDueDate = scheduledOn,
                                            RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                                        };
                                        reminder1.Status = (byte) (reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.TaskReminders.Add(reminder1);
                                    }
                                }
                            }
                        }
                        else
                        {
                            DateTime TempScheduled;
                            if (IsAfter == true)
                            {
                                TempScheduled = scheduledOn.AddDays((Convert.ToDouble(compliance.ReminderBefore)));
                            }
                            else
                            {
                                TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.ReminderBefore)));
                            }

                            while (TempScheduled.Date < scheduledOn)
                            {
                                TaskReminder reminder = new TaskReminder()
                                {
                                    TaskAssignmentID = TaskAssignmentId,
                                    ComplianceScheduleOnID = complianceScheduleOnID,
                                    ReminderTemplateID = 1,
                                    TaskDueDate = scheduledOn,
                                    RemindOn = TempScheduled.Date,
                                };

                                reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                entities.TaskReminders.Add(reminder);

                                TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.ReminderGap));
                            }
                        }
                    }

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();

                LogMessage msg = new LogMessage();
                msg.ClassName = "TaskManagement";
                //msg.FunctionName = "CreateReminders";
                msg.FunctionName = "CreateReminders" + "TaskAssignmentId" + TaskAssignmentId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as CreateTaskReminders Function", "CreateTaskReminders" + "TaskAssignmentId" + TaskAssignmentId);

            }
        }

        public static List<TaskAssignment> GetAssignedTaskUsers(int TAskInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedUsers = (from row in entities.TaskAssignments
                                     where row.TaskInstanceID == TAskInstanceID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

                return AssignedUsers;
            }
        }
        public static long GetStatutoryComplianceInstanceIDFromTask(long TaskInstanceId,int LocationID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long ComplianceInstanceID = (from row in entities.TaskInstances
                                         join row1 in entities.Tasks 
                                         on row.TaskId equals row1.ID
                                         join row2 in entities.TaskComplianceMappings
                                         on row1.MainTaskID equals row2.TaskID 
                                         join row3 in entities.ComplianceInstances
                                         on new { complianceID = row2.ComplianceID, locationID = row.CustomerBranchID } equals
                                         new { complianceID = row3.ComplianceId, locationID = row3.CustomerBranchID }
                                         join row4 in entities.ComplianceAssignments 
                                         on row3.ID equals row4.ComplianceInstanceID 
                                         where row.ID == TaskInstanceId && row.CustomerBranchID == LocationID 
                                         && row.IsDeleted == false && row3.IsDeleted == false && row4.RoleID == 3
                                         select row3.ID).SingleOrDefault();

                return ComplianceInstanceID;
            }
        }


        public static long GetInternalComplianceInstanceIDFromTask(long TaskInstanceId, int LocationID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long ComplianceInstanceID = (from row in entities.TaskInstances
                                             join row1 in entities.Tasks
                                             on row.TaskId equals row1.ID
                                             join row2 in entities.TaskComplianceMappings
                                             on row1.MainTaskID equals row2.TaskID
                                             join row3 in entities.InternalComplianceInstances
                                             on new { complianceID = row2.ComplianceID, locationID = row.CustomerBranchID } equals
                                             new { complianceID = row3.InternalComplianceID, locationID = row3.CustomerBranchID }
                                             join row4 in entities.InternalComplianceAssignments
                                             on row3.ID equals row4.InternalComplianceInstanceID
                                             where row.ID == TaskInstanceId && row.CustomerBranchID == LocationID
                                             && row.IsDeleted == false && row3.IsDeleted == false && row4.RoleID == 3
                                             select row3.ID).SingleOrDefault();

                return ComplianceInstanceID;
            }
        }
        public static TaskTransactionView GetClosedTransaction(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.TaskTransactionViews
                                  where row.TaskScheduleOnID == ScheduledOnID
                                  && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 7 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 9)
                                  orderby row.TaskTransactionID descending
                                  select row).ToList();

                return statusList.FirstOrDefault();
            }
        }
        public static string GetTaskComplianceDescription(long TaskID, int IsStatutoryOrInternal)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var userRecord = "";
                if (IsStatutoryOrInternal == 1)
                {
                    //tasktype 1 means statutory 
                    userRecord = (from TCM in entities.TaskComplianceMappings
                                  join C in entities.Compliances
                                  on TCM.ComplianceID equals C.ID
                                  where TCM.IsActive == true && C.IsDeleted == false
                                  && TCM.TaskID == TaskID
                                  select C.ShortDescription).FirstOrDefault();
                }
                else if (IsStatutoryOrInternal == 2)
                {
                    //tasktype 2 means internal
                    userRecord = (from TCM in entities.TaskComplianceMappings
                                  join IC in entities.InternalCompliances
                                  on TCM.ComplianceID equals IC.ID
                                  where TCM.IsActive == true && IC.IsDeleted == false
                                  && TCM.TaskID == TaskID
                                  select IC.IShortDescription).FirstOrDefault();
                }

                return userRecord;
            }
        }

        public static int GetCustomerBranchID(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int CustomerBranchID = (from row in entities.ComplianceInstances 
                                 where row.ID == ComplianceInstanceID
                                 select row.CustomerBranchID).SingleOrDefault();

                return CustomerBranchID;
            }
        }
     
        public static List<Act> BindAct()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actlst = (from row in entities.Acts
                              join row1 in entities.Compliances
                              on row.ID equals row1.ActID
                              where row.IsDeleted == false 
                              && row1.Frequency != 7 && row1.Frequency !=8
                              && row1.EventFlag == null
                              select row).Distinct();
                return actlst.ToList();
            }
        }
        public static List<GetTaskDocumentView> GetFileDataView(int TaskScheduleOnID, int FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetTaskDocumentViews
                                where row.TaskScheduleOnID == TaskScheduleOnID && row.FileID == FileID
                                select row).ToList();

                return fileData;
            }
        }
        public static void UpdateAssignedUserTAsk(int AssignmentID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TaskAssignment Assignment = (from row in entities.TaskAssignments
                                             where row.ID == AssignmentID
                                             select row).FirstOrDefault();
                Assignment.UserID = UserID;
                entities.SaveChanges();
            }
        }
        public static bool CheckTaskScheduleOnPresent(int taskassignmentid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.TaskAssignments
                             join row1 in entities.TaskScheduleOns
                             on row.TaskInstanceID equals row1.TaskInstanceID
                             where row.ID == taskassignmentid
                             select row).ToList().Distinct().FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static List<GetTaskDocumentView> GetDocumnets(long TaskScheduleOnID, long transactionID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime curruntDate = DateTime.Now.AddDays(-1);

                var DocumentList = (from row in entities.GetTaskDocumentViews
                                    where row.TaskScheduleOnID == TaskScheduleOnID
                                    select row).GroupBy(g => g.FileID).Select(g => g.FirstOrDefault()).ToList();//&& row.TransactionID == transactionID


                return DocumentList;
            }
        }


        public static object GetTaskStatutoryPerformerReviewerList(int Customerid, int UserID, int roleID, int tasktype, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime nextOneMonth = DateTime.Now.Date.AddMonths(1);

                var lstPR = (from row in entities.Sp_GetTaskAssignedPerformerOrReviewer(Customerid, UserID, roleID, tasktype, flag)
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).Distinct().ToList<object>();
                return lstPR;
            }
        }
        public static string GetTaskAssignedUser(int customerID, byte taskType, long taskInstanceID, long taskScheduledOnID, int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var userRecord = (from row in entities.TaskInstanceTransactionViews
                                  join users in entities.Users on row.UserID equals users.ID
                                  where row.TaskInstanceID == taskInstanceID
                                  && row.TaskScheduledOnID == taskScheduledOnID
                                  && row.RoleID == roleID
                                  && row.TaskType == taskType
                                  && (row.IsActive == true || row.IsUpcomingNotDeleted == true)
                                  select users.FirstName + " " + users.LastName).FirstOrDefault();

                return userRecord;
            }
        }

        public static string GetTaskDocumentVersion(int taskScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.TaskDocumentsViews
                                where row.TaskScheduleOnID == taskScheduledOnID
                                select row).GroupBy(entry => entry.FileID).Select(en => en.FirstOrDefault()).ToList();

                if (fileData.Count == 0 || fileData == null)
                {
                    return "1.0";
                }
                else
                {
                    var version = fileData.OrderByDescending(entry => entry.FileID).Take(1).FirstOrDefault();
                    int lastVersion = 0;
                    if (version.Version == null)
                    {
                        return "2.0";
                    }
                    else
                    {
                        lastVersion = Convert.ToInt32(version.Version.Split('.')[0]);
                        lastVersion += 1;
                        return lastVersion + ".0";
                    }
                }
            }
        }

        public static List<GetTaskDocumentView> GetTaskRelatedFileData(int taskScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetTaskDocumentViews
                                where row.TaskScheduleOnID == taskScheduledOnID
                                select row).ToList();
                return fileData;
            }
        }

        
        public static TaskInstance GetTaskInstanceByID(int taskInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.TaskInstances
                                    where row.ID == taskInstanceID 
                                    && row.IsDeleted == false
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }

        public static RecentTaskTransactionView GetCurrentTaskStatus(int taskInstanceID, int taskScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var currentStatusRecord = (from row in entities.RecentTaskTransactionViews
                                           where row.TaskScheduleOnID == taskScheduledOnID
                                           && row.TaskInstanceId == taskInstanceID
                                           select row).FirstOrDefault();

                return currentStatusRecord;
            }
        }


        public static bool CheckTaskAssigned(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.TaskAssignments
                               where row.UserID == userID
                               select row.UserID).FirstOrDefault();
                if (ObjTask == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static List<int> GetAssignedTaskUserRole(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.TaskAssignments
                               where row.UserID == userID
                               select row.RoleID).Distinct().ToList();

                return ObjTask;
            }
        }

        public static List<int> GetAssignedLocationList(int UserID, int custID, String Role)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (Role == "MGMT")
                {
                    var query = (from row in entities.EntitiesAssignments
                                 where row.UserID == UserID
                                 select row).Distinct().ToList();

                    if (query != null)
                        LocationList = query.Select(a => (int) a.BranchID).Distinct().ToList();

                }
                else
                {
                    var query = (from row in entities.TaskAssignedInstancesViews
                                 join row1 in entities.CustomerBranches
                                 on row.CustomerBranchID equals row1.ID
                                 where row.UserID == UserID //&& row1.ID==990
                                 select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                    if (query != null)
                        LocationList = query.Select(a => a.ID).Distinct().ToList();
                }
                return LocationList;
            }
        }

        public static byte? GetComplianceFrequency(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceDetail = (from row in entities.Compliances
                                        where row.ID == complianceID
                                        select row.Frequency).FirstOrDefault();

                return complianceDetail;
            }
        }

        public static byte? GetInternalComplianceFrequency(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceDetail = (from row in entities.InternalCompliances
                                        where row.ID == complianceID
                                        select row.IFrequency).FirstOrDefault();

                return complianceDetail;
            }
        }

        public static int? GetDueDays(long TaskId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int? days = (from row in entities.Tasks
                                      where row.ID == TaskId
                                      select row.DueDays).SingleOrDefault();

                return days;
            }
        }
        public static List<int> GetSubTaskComplianceMappedLocations(List<long> ComplianceList, long customerID, long TaskId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> BranchList = (from CI in entities.ComplianceInstances
                                        join CA in entities.ComplianceAssignments
                                        on CI.ID equals CA.ComplianceInstanceID
                                        join CB in entities.CustomerBranches
                                        on CI.CustomerBranchID equals CB.ID
                                        join Cust in entities.Customers
                                        on CB.CustomerID equals Cust.ID
                                        join TAIV in entities.TaskAssignedInstancesViews
                                        on CI.CustomerBranchID equals TAIV.CustomerBranchID
                                        where
                                            ComplianceList.Contains(CI.ComplianceId)
                                            && Cust.IsDeleted == false
                                            && Cust.ID == customerID
                                            && TAIV.TaskID == TaskId
                                            && TAIV.TaskType == 1
                                        select CI.CustomerBranchID).Distinct().ToList();

                return BranchList;
            }
        }

        public static List<int> GetSubTaskComplianceMappedLocationMgmt(List<long> ComplianceList, long customerID, long TaskId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> BranchList = (from CI in entities.ComplianceCategoryMgmtUserInstances
                                        join CA in entities.ComplianceAssignments
                                        on CI.ComplianceInstanceId equals CA.ComplianceInstanceID
                                        join CB in entities.CustomerBranches
                                        on CI.CustomerBranchID equals CB.ID
                                        join Cust in entities.Customers
                                        on CB.CustomerID equals Cust.ID
                                        join TAIV in entities.TaskAssignedInstancesViews
                                        on CI.CustomerBranchID equals TAIV.CustomerBranchID
                                        where
                                            ComplianceList.Contains(CI.ComplianceId)
                                            && Cust.IsDeleted == false
                                            && Cust.ID == customerID
                                            && TAIV.TaskID == TaskId
                                            && TAIV.TaskType == 1
                                        select CI.CustomerBranchID).Distinct().ToList();

                return BranchList;
            }
        }

        public static List<int> GetSubTaskInternalComplianceMappedLocations(List<long> ComplianceList, long customerID, long TaskId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                List<int> BranchList = (from ICI in entities.InternalComplianceInstances
                                        join ICA in entities.InternalComplianceAssignments
                                        on ICI.ID equals ICA.InternalComplianceInstanceID
                                        join CB in entities.CustomerBranches
                                        on ICI.CustomerBranchID equals CB.ID
                                        join Cust in entities.Customers
                                        on CB.CustomerID equals Cust.ID
                                        join TAIV in entities.TaskAssignedInstancesViews
                                        on ICI.CustomerBranchID equals TAIV.CustomerBranchID
                                        where
                                            ComplianceList.Contains(ICI.InternalComplianceID)
                                            && Cust.IsDeleted == false
                                            && Cust.ID == customerID
                                            && TAIV.TaskID == TaskId
                                            && TAIV.TaskType == 2
                                        select ICI.CustomerBranchID).Distinct().ToList();



                return BranchList;
            }
        }

        public static List<int> GetSubTaskInternalComplianceMappedLocationMgmt(List<long> ComplianceList, long customerID, long TaskId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                List<int> BranchList = (from ICI in entities.InternalComplianceCategoryMgmtUserInstances
                                        join ICA in entities.InternalComplianceAssignments
                                        on ICI.ComplianceInstanceId equals ICA.InternalComplianceInstanceID
                                        join CB in entities.CustomerBranches
                                        on ICI.CustomerBranchID equals CB.ID
                                        join Cust in entities.Customers
                                        on CB.CustomerID equals Cust.ID
                                        join TAIV in entities.TaskAssignedInstancesViews
                                        on ICI.CustomerBranchID equals TAIV.CustomerBranchID
                                        where
                                            ComplianceList.Contains(ICI.ComplianceId)
                                            && Cust.IsDeleted == false
                                            && Cust.ID == customerID
                                            && TAIV.TaskID == TaskId
                                            && TAIV.TaskType == 2
                                        select ICI.CustomerBranchID).Distinct().ToList();



                return BranchList;
            }
        }
        public static List<int> GetTaskComplianceMappedLocations(List<long> ComplianceList,long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> BranchList = (from row in entities.ComplianceInstances
                                             join row1 in entities.ComplianceAssignments
                                             on row.ID equals row1.ComplianceInstanceID
                                             join row2 in entities.CustomerBranches
                                             on row.CustomerBranchID equals row2.ID
                                             join row3 in entities.Customers
                                             on row2.CustomerID equals row3.ID
                                             where ComplianceList.Contains(row.ComplianceId)
                                             && row3.ID == customerID
                                        select row.CustomerBranchID).Distinct().ToList();

                return BranchList;
            }
        }

        public static List<int> GetTaskComplianceMappedLocationMgMT(List<long> ComplianceList, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> BranchList = (from row in entities.ComplianceCategoryMgmtUserInstances
                                        join row1 in entities.ComplianceAssignments
                                        on row.ComplianceInstanceId equals row1.ComplianceInstanceID
                                        join row2 in entities.CustomerBranches
                                        on row.CustomerBranchID equals row2.ID
                                        join row3 in entities.Customers
                                        on row2.CustomerID equals row3.ID
                                        where ComplianceList.Contains(row.ComplianceId)
                                        && row3.ID == customerID
                                        select row.CustomerBranchID).Distinct().ToList();

                return BranchList;
            }
        }
        public static List<int> GetTaskInternalComplianceMappedLocations(List<long> ComplianceList,long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<int> BranchList = (from row in entities.InternalComplianceInstances
                                        join row1 in entities.InternalComplianceAssignments
                                        on row.ID equals row1.InternalComplianceInstanceID
                                        join row2 in entities.CustomerBranches
                                        on row.CustomerBranchID equals row2.ID
                                        join row3 in entities.Customers
                                        on row2.CustomerID equals row3.ID
                                        where ComplianceList.Contains(row.InternalComplianceID)
                                        && row3.ID == customerID
                                        select row.CustomerBranchID).Distinct().ToList();

                return BranchList;
            }
        }

        public static List<int> GetTaskInternalComplianceMappedLocationMgmt(List<long> ComplianceList, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<int> BranchList = (from row in entities.InternalComplianceCategoryMgmtUserInstances
                                        join row1 in entities.InternalComplianceAssignments
                                        on row.ComplianceInstanceId equals row1.InternalComplianceInstanceID
                                        join row2 in entities.CustomerBranches
                                        on row.CustomerBranchID equals row2.ID
                                        join row3 in entities.Customers
                                        on row2.CustomerID equals row3.ID
                                        where ComplianceList.Contains(row.ComplianceId)
                                        && row3.ID == customerID
                                        select row.CustomerBranchID).Distinct().ToList();

                return BranchList;
            }
        }
        public static List<long> GetTaskMappingComplianceList(int TaskId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> complianceList = (from row in entities.TaskComplianceMappings
                                 where row.TaskID == TaskId
                                 select row.ComplianceID).ToList();

                return complianceList;
            }
        }
        public static byte GetTaskType(long TaskId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                byte taskType = (from row in entities.Tasks
                             where row.ID == TaskId
                             select row.TaskType).SingleOrDefault();

                return taskType;
            }
        }

        public static bool? GetTaskBeforeAfter(long TaskId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool? taskType = (from row in entities.Tasks
                                 where row.ID == TaskId
                                 select row.IsAfter).SingleOrDefault();

                return taskType;
            }
        }

        public static int? TaskApplicable(long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int? taskapplicable = (from row in entities.Tasks
                             join row1 in entities.TaskInstances 
                             on row.ID equals row1.TaskId 
                             join row3 in entities.Customers 
                             on row.CustomerID equals row3.ID
                             where row1.CustomerBranchID  == customerid
                             select row3.TaskApplicable).Distinct().SingleOrDefault();

                return taskapplicable;
            }
        }

        public static void CreateTaskInstances(List<Tuple<TaskInstance, TaskAssignment>> assignments, long createdByID, string creatdByName,int TaskType, Boolean IsAfter)
        {
            long CreateInstancesErrorcomplianceInstanceID = -1;
            long CreateInstancesErrorComplianceId = -1;
            try
            {
                if (assignments.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        assignments.ForEach(entry =>
                        {
                            long taskInstanceID = (from row in entities.TaskInstances
                                                  where row.TaskId == entry.Item1.TaskId 
                                                  && row.CustomerBranchID == entry.Item1.CustomerBranchID
                                                  && row.IsDeleted == false
                                                  select row.ID).SingleOrDefault();

                            if (taskInstanceID <= 0)
                            {
                                entry.Item1.CreatedOn = DateTime.UtcNow;
                                entry.Item1.IsDeleted = false;
                                entities.TaskInstances.Add(entry.Item1);
                                entities.SaveChanges();

                                taskInstanceID = entry.Item1.ID;

                                CreateInstancesErrorcomplianceInstanceID = taskInstanceID;
                                CreateInstancesErrorComplianceId = entry.Item1.TaskId;
                            }
                            else
                            {
                                entry.Item1.ID = taskInstanceID;
                            }

                            long taskAssignment = (from row in entities.TaskAssignments
                                                   where row.TaskInstanceID == entry.Item1.ID && row.RoleID == entry.Item2.RoleID
                                                   select row.UserID).FirstOrDefault();

                            if (taskAssignment <= 0)
                            {
                                entry.Item2.TaskInstanceID = taskInstanceID;
                                entities.TaskAssignments.Add(entry.Item2);
                                entities.SaveChanges();

                                if(TaskType == 1) //Statutory
                                {
                                    long complianceInstanceID = GetStatutoryComplianceInstanceIDFromTask(taskInstanceID, entry.Item1.CustomerBranchID);

                                    if (complianceInstanceID != 0)
                                    {
                                        long ComplianceID = GetComplianceID(complianceInstanceID);
                                        Task task = GetTask(taskInstanceID);
                                        TaskInstance taskinstance = GetTaskStartDate(taskInstanceID);
                                        GenerateTaskSchuduleFromStatutory(taskInstanceID, ComplianceID, complianceInstanceID, taskinstance.ScheduledOn, task.DueDays,IsAfter);
                                    }
                                }
                                else  //Internal
                                {
                                    long InternalcomplianceInstanceID = GetInternalComplianceInstanceIDFromTask(taskInstanceID, entry.Item1.CustomerBranchID);

                                    if (InternalcomplianceInstanceID != 0)
                                    {
                                        long InternalComplianceID = GetInternalComplianceID(InternalcomplianceInstanceID);
                                        Task task = GetTask(taskInstanceID);
                                        TaskInstance taskinstance = GetTaskStartDate(taskInstanceID);
                                        GenerateTaskSchuduleFromInternal(taskInstanceID, InternalComplianceID, InternalcomplianceInstanceID, taskinstance.ScheduledOn, task.DueDays,IsAfter);
                                    }
                                }
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "TaskManagement";
                msg.FunctionName = "CreateTaskInstances" + "TaskId" + CreateInstancesErrorComplianceId + "TaskInstanceID" + CreateInstancesErrorcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as CreateTaskInstances Function", "CreateTaskInstances" + "TaskId" + CreateInstancesErrorComplianceId + "TaskInstanceID" + CreateInstancesErrorcomplianceInstanceID);
            }
        }
        public static bool CreateTaskTransaction(TaskTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.TaskTransactions.Add(transaction);
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as CreateTransaction Function", "CreateTransaction");
                return false;
            }
        }
        public static bool CreateTaskTransaction(TaskTransaction transaction, List<TaskFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Task_SaveDocFiles(filesList);
                            transaction.Dated = DateTime.Now;
                            entities.TaskTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (TaskFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.TaskFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    TaskFileDataMapping fileMapping = new TaskFileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.TaskScheduledOnID = transaction.TaskScheduleOnID;
                                    fileMapping.ComplianceScheduledOnID = transaction.ComplianceScheduleOnID;

                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }

                                    entities.TaskFileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "TaskManagment";
                msg.FunctionName = "CreateTaskTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;

                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("rahul@avantis.info");
                TO.Add("narendra@avantis.info");

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }
        public static bool CreateTaskTransactionAWS(TaskTransaction transaction, List<TaskFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, string DirectoryPath, long ScheduleOnID, int CustId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                AmazonS3.SaveDocFilesAWSStorageTask(filesList, DirectoryPath, ScheduleOnID, AWSData.BucketName, AWSData.AccessKeyID, AWSData.SecretKeyID);
                            }
                            else
                            {
                                DocumentManagement.Task_SaveDocFiles(filesList);
                            }
                            transaction.Dated = DateTime.Now;
                            entities.TaskTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (TaskFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.TaskFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    TaskFileDataMapping fileMapping = new TaskFileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.TaskScheduledOnID = transaction.TaskScheduleOnID;
                                    fileMapping.ComplianceScheduledOnID = transaction.ComplianceScheduleOnID;

                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }

                                    entities.TaskFileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "TaskManagment";
                msg.FunctionName = "CreateTaskTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;

                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("rahul@avantis.info");
                TO.Add("narendra@avantis.info");

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }

        public static List<TaskTransactionView> GetAllTaskTransactionLog(int taskScheduledOnID, int complianceScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.TaskTransactionViews
                                  where row.TaskScheduleOnID == taskScheduledOnID
                                  && row.ComplianceScheduleOnID == complianceScheduleOnID
                                  orderby row.TaskTransactionID descending
                                  select row).ToList();

                return statusList;
            }
        }

        public static object GetHierarchyTask(int TaskID, long parentTaskID)
        {
            List<NameValue> hierarchy = new List<NameValue>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (parentTaskID != -1 && parentTaskID != 0)
                {
                    long? parentID = parentTaskID;
                    while (parentID.HasValue)
                    {
                        var subtask = (from row in entities.Tasks
                                       where row.ID == parentID.Value
                                       select new { row.ID, row.Title, row.ParentID }).SingleOrDefault();

                        hierarchy.Add(new NameValue() { ID = (int) subtask.ID, Name = subtask.Title });
                        parentID = subtask.ParentID;
                    }
                }
                //var task = (from row in entities.Tasks
                //            where row.ID == TaskID
                //            select row.Title).SingleOrDefault();

                //hierarchy.Add(new NameValue() { ID = TaskID, Name = task });
            }

            hierarchy.Reverse();

            return hierarchy;
        }

        public static List<Compliance> GetComplianceForTask(int actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var events = (from row in entities.Compliances
                              where row.IsDeleted == false
                              && row.Status == null
                             select row).ToList();
                if (actid == 0)
                {
                    return events.Where(entry => entry.IsDeleted == false && entry.Event == null && entry.Frequency != 7 && entry.Frequency != 8).OrderBy(entry => entry.ShortDescription).ToList();
                }
                else
                {
                    return events.Where(entry => entry.IsDeleted == false && entry.ActID == actid && entry.Event == null && entry.Frequency != 7 && entry.Frequency != 8).OrderBy(entry => entry.ShortDescription).ToList();
                }
            }
        }

        public static void LoadSubEntitiesRestrict(List<int> CustomerBranchList, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where CustomerBranchList.Contains(row.ID)
                                                && row.IsDeleted == false
                                                select row);
            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntitiesRestrict(CustomerBranchList,item, false, entities);
            }
        }


        public static void LoadSubEntities( NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                select row);
            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntities(item, false, entities);
            }
        }
        
        public static List<NameValueHierarchy> GetAllHierarchyRestrictLocationForTask(int TaskType,List<long> Compliancelist, long customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (TaskType == 1)
                {
                    var BranchMappedList = TaskManagment.GetTaskComplianceMappedLocations(Compliancelist, customerID);
                    var query = (from row in entities.CustomerBranches
                                 join Cust in entities.Customers
                                 on row.CustomerID equals Cust.ID
                                 where BranchMappedList.Contains(row.ID)
                                 select Cust).Distinct();

                    if (customerID != -1)
                    {
                        query = query.Where(entry => entry.ID == customerID);
                    }

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                    foreach (var item in hierarchy)
                    {
                        LoadSubEntitiesRestrict(BranchMappedList,item, true, entities);
                    }
                }
                else
                {
                    var BranchMappedList = TaskManagment.GetTaskInternalComplianceMappedLocations(Compliancelist, customerID);
                    var query = (from row in entities.CustomerBranches
                                 join Cust in entities.Customers
                                 on row.CustomerID equals Cust.ID
                                 where BranchMappedList.Contains(row.ID)
                                 select Cust).Distinct();

                    if (customerID != -1)
                    {
                        query = query.Where(entry => entry.ID == customerID);
                    }

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                    foreach (var item in hierarchy)
                    {
                        LoadSubEntitiesRestrict(BranchMappedList,item, true, entities);
                    }
                }
            }

            return hierarchy;
        }

        public static List<NameValueHierarchy> GetAllHierarchySatutory(long customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }

        public static List<long> GetTaskCompliaceMappingbyTaskID(long TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.Tasks
                                      join row1 in entities.TaskComplianceMappings
                                      on row.ID equals row1.TaskID
                                      where row1.TaskID == TaskID && row.Isdeleted == false
                                      select row1.ComplianceID).ToList();

                return eventMappedIDs;
            }
        }
        public static List<InternalCompliance> GetInternalComplianceForPerformer(int CustomerID,long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.InternalCompliances
                                  join row1 in entities.InternalComplianceInstances
                                  on row.ID equals row1.InternalComplianceID
                                  join row2 in entities.InternalComplianceAssignments
                                  on row1.ID equals row2.InternalComplianceInstanceID
                                  where row.CustomerID == CustomerID
                                   && row.IsDeleted == false && row2.UserID == UserID
                                  select row).ToList();

                return compliance;
            }
        }
        public static List<InternalCompliance> GetInternalCompliance(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.InternalCompliances
                                  where row.CustomerID == CustomerID
                                   && row.IsDeleted == false
                                  select row).ToList();

                return compliance;
            }
        }

        public static void DeleteTask(int TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Task TaskToDelete = (from row in entities.Tasks
                                     where row.ID == TaskID
                                     select row).FirstOrDefault();
                TaskToDelete.Isdeleted = true;
                entities.SaveChanges();
            }
        }

        public static void DeleteTaskAssignment(int AssignID,int TaskInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //TaskInstance TaskInstanceToDelete = (from row in entities.TaskInstances
                //                               where row.ID == TaskInstanceID
                //                               select row).FirstOrDefault();
                //entities.TaskInstances.Remove(TaskInstanceToDelete);
                //entities.SaveChanges();

                TaskAssignment TaskToDelete = (from row in entities.TaskAssignments
                                     where row.TaskInstanceID == TaskInstanceID && row.ID == AssignID
                                               select row).FirstOrDefault();

                entities.TaskAssignments.Remove(TaskToDelete);
                entities.SaveChanges();
            }
        }

        public static Task GetByTaskDetails(long taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var task = (from row in entities.Tasks
                            where row.ID == taskID
                            select row).SingleOrDefault();

                return task;
            }
        }

        public static TaskInstanceTransactionView GetTaskDetail(int taskScheduledOnID, int taskInstanceID, int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.TaskInstanceTransactionViews
                             where row.TaskScheduledOnID == taskScheduledOnID
                             && row.TaskInstanceID == taskInstanceID
                             && row.RoleID == roleID
                             select row).FirstOrDefault();

                return query;
            }
        }

        public static void CreateTaskComplianceMapping(TaskComplianceMapping Mapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.TaskComplianceMappings.Add(Mapping);
                entities.SaveChanges();
            }
        }

        public static void UpdateTaskComplianceMappedID(long TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.TaskComplianceMappings
                           where row.TaskID == TaskID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    TaskComplianceMapping prevmappedids = (from row in entities.TaskComplianceMappings
                                                           where row.ID == entry
                                                           select row).FirstOrDefault();
                    entities.TaskComplianceMappings.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }
        }

        public static bool CreateTask(Task task)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Tasks.Add(task);
                    entities.SaveChanges();


                    Task taskToUpdate = (from row in entities.Tasks
                                         where row.ID == task.ID
                                         select row).FirstOrDefault();

                    if (taskToUpdate != null)
                    {
                        taskToUpdate.MainTaskID = task.ID;
                        entities.SaveChanges();
                        return true;
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool CreateSubTask(Task task)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Tasks.Add(task);
                    entities.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateTask(Task task)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Task taskToUpdate = (from row in entities.Tasks
                                         where row.ID == task.ID
                                         select row).FirstOrDefault();

                    if (taskToUpdate != null)
                    {
                        taskToUpdate.Title = task.Title;
                        taskToUpdate.Description = task.Description;
                        taskToUpdate.TaskType = task.TaskType;
                        taskToUpdate.DueDays = task.DueDays;
                        taskToUpdate.ActualDueDays = task.ActualDueDays;
                        taskToUpdate.ActFilter = task.ActFilter;
                        taskToUpdate.UpdatedBy = task.UpdatedBy;
                        taskToUpdate.UpdatedDate = task.UpdatedDate;
                        taskToUpdate.TaskTypeID = task.TaskTypeID;
                        taskToUpdate.SubTaskID = task.SubTaskID;
                        taskToUpdate.IsAfter = task.IsAfter;
                        taskToUpdate.IsBothYesNo = task.IsBothYesNo;
                        taskToUpdate.Yesmessage = task.Yesmessage;
                        taskToUpdate.Nomessage = task.Nomessage;
                        taskToUpdate.Message = task.Message;
                        taskToUpdate.IsYesNo = task.IsYesNo;
                        taskToUpdate.IsYes = task.IsYes;
                        taskToUpdate.IsNo = task.IsNo;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }               
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateMappingTask(Task task)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Task taskToUpdate = (from row in entities.Tasks
                                         where row.ID == task.ID
                                         select row).FirstOrDefault();

                    if (taskToUpdate != null)
                    {
                        taskToUpdate.Title = task.Title;
                        taskToUpdate.Description = task.Description;
                        //taskToUpdate.TaskType = task.TaskType;
                        //taskToUpdate.DueDays = task.DueDays;
                        //taskToUpdate.ActualDueDays = task.ActualDueDays;
                        //taskToUpdate.ActFilter = task.ActFilter;
                        taskToUpdate.UpdatedBy = task.UpdatedBy;
                        taskToUpdate.UpdatedDate = task.UpdatedDate;
                        taskToUpdate.TaskTypeID = task.TaskTypeID;
                        taskToUpdate.SubTaskID = task.SubTaskID;
                        //taskToUpdate.IsAfter = task.IsAfter;
                        taskToUpdate.IsBothYesNo = task.IsBothYesNo;
                        taskToUpdate.Yesmessage = task.Yesmessage;
                        taskToUpdate.Nomessage = task.Nomessage;
                        taskToUpdate.Message = task.Message;
                        taskToUpdate.IsYesNo = task.IsYesNo;
                        taskToUpdate.IsYes = task.IsYes;
                        taskToUpdate.IsNo = task.IsNo;
                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static List<Task> GetAllTask(int UserID,int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.Tasks
                               where row.Isdeleted == false
                               && row.ParentID == null
                               && row.CreatedBy == UserID
                               && row.CustomerID == CustomerID
                               select row).ToList();

                return ObjTask;
            }
        }
        public static List<Task> GetAll_Task(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.Tasks
                               where row.Isdeleted == false 
                               && row.ParentID == null
                               && row.CustomerID == CustomerID
                               select row).ToList();

                return ObjTask;
            }
        }

        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();

                });
            }

        }

        public static bool TaskNameExistOrnot(Task task)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjTask = (from row in entities.Tasks
                               where row.Title.ToUpper().Trim() == task.Title.ToUpper().Trim()
                               && row.CustomerID == task.CustomerID
                               && row.ID != task.ID && row.TaskType == task.TaskType
                               && row.ParentID == null && row.Isdeleted == false
                               select row.Title).FirstOrDefault();

                if (ObjTask != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        //Statutory Count 
        public static List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> InternalTaskDashboardDataForPerformerDisplayCount(int CustomerID, int userID, int taskType, CannedReportFilterForPerformer filter,List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> MasterQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> transactionsQuery = new List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
               // int performerRoleID = 3;
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                transactionsQuery =MasterQuery.ToList();
                if (taskType != 0)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.TaskType == taskType).ToList();
                }

                transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == performerRoleID && entry.UserID == userID).ToList();

                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth)
                                           && (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)).ToList();
                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)
                                                        && entry.ScheduledOn < now).ToList();

                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 12)).ToList();
                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;

                    case CannedReportFilterForPerformer.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 6 || entry.TaskStatusID == 14)).ToList();
                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        public static List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> StatutoryTaskDashboardDataForReviewerDisplayCount(int CustomerID, int userID, CannedReportFilterForPerformer filter, int taskType,List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> MasterQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> transactionsQuery = new List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                //var reviewerRoleIDs = 4;
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                transactionsQuery = MasterQuery.ToList();

                if (taskType != 0)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.TaskType == taskType).ToList();
                }                
                transactionsQuery = transactionsQuery.Where(entry => reviewerRoleIDs.Contains(entry.RoleID) && entry.UserID == userID).ToList();

                switch (filter)
                {
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10)
                                                        && entry.ScheduledOn < now).ToList();

                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 11 || entry.TaskStatusID == 12)).ToList();

                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;

                    case CannedReportFilterForPerformer.Rejected:

                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 6 || entry.TaskStatusID == 14)).ToList();

                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        //Statutory Display Details
        public static List<SP_TaskInstanceTransactionStatutoryView_Result> GetFilteredTaskStatutoryList(int customerID, int userID, int roleID, int taskType, int branchID, int Risk, string statusCode, int comTypeID, int SubTypeID, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.Now.AddMonths(1);

                var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
                                    select row).ToList();

                documentData = documentData.Where(entry => entry.RoleID == roleID && entry.UserID == userID).ToList();

                if (taskType != 0)
                {
                    documentData = documentData.Where(entry => entry.TaskType == taskType).ToList();
                }
                if (branchID != -1)
                    documentData = documentData.Where(entry => entry.CustomerBranchID == branchID).ToList();

                switch (statusCode)
                {
                    case "Upcoming":

                        documentData = documentData.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth)
                        && (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)).ToList();
                        break;

                    case "Overdue":
                        documentData = documentData.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)
                                                         && entry.ScheduledOn < now).ToList();
                        break;

                    case "DueButNotSubmitted":
                        documentData = documentData.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12)
                                                          && entry.ScheduledOn < now).ToList();
                        break;

                    case "PendingForReview":
                        documentData = documentData.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 12)).ToList();
                        break;

                    case "Rejected":
                        documentData = documentData.Where(entry => entry.TaskStatusID == 6 || entry.TaskStatusID == 14).ToList();
                        break;
                }

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.RiskType == Convert.ToByte(Risk)).ToList();

                if (comTypeID != -1)
                    documentData = documentData.Where(entry => entry.ComplianceTypeId == comTypeID).ToList();

                if (SubTypeID != -1)
                    documentData = documentData.Where(entry => entry.ComplianceSubTypeID == SubTypeID).ToList();

                if (ComCategory != -1)
                    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                if (Act != -1)
                    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                // Find data through String contained in Description
                if (StringType != "")
                {
                    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                if (documentData.Count > 0)
                {
                    documentData = documentData.OrderBy(entry => entry.TaskInstanceID).ThenBy(entry => entry.ScheduledOn).ToList();
                }

                return documentData.GroupBy(entry => entry.TaskScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }




        //Internal Count 
        public static List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> StatutoryTaskDashboardDataForPerformerDisplayCount(int CustomerID, int userID, int taskType, CannedReportFilterForPerformer filter, List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> MasterQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var transactionsQuery;
                List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> transactionsQuery = new List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
              // int performerRoleID = 3;
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                transactionsQuery = MasterQuery.ToList();

                if (taskType != 0)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.TaskType == taskType).ToList();
                }
                transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == performerRoleID && entry.UserID == userID).ToList();

                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth)
                                           && (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)).ToList();

                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)
                                                        && entry.ScheduledOn < now).ToList();

                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 12)).ToList();
                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;

                    case CannedReportFilterForPerformer.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 6 || entry.TaskStatusID == 14)).ToList();
                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        public static List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> InternalTaskDashboardDataForReviewerDisplayCount(int CustomerID, int userID, CannedReportFilterForPerformer filter, int taskType, List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> MasterQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result> transactionsQuery = new List<SP_TaskInstanceTransactionStatutoryView_Dashboard_Result>();
                var reviewerRoleIDs = (from row in entities.Roles
                                        where row.Code.StartsWith("RVW")
                                        select row.ID).ToList();
                //var reviewerRoleIDs = 4;
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                transactionsQuery = MasterQuery.ToList();

                if (taskType != 0)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.TaskType == taskType).ToList();
                }                
                transactionsQuery = transactionsQuery.Where(entry => reviewerRoleIDs.Contains(entry.RoleID) && entry.UserID == userID).ToList();


                switch (filter)
                {
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10)
                                                        && entry.ScheduledOn < now).ToList();

                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 11)).ToList();

                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;

                    case CannedReportFilterForPerformer.Rejected:

                        transactionsQuery = transactionsQuery.Where(entry => (entry.TaskStatusID == 6 || entry.TaskStatusID == 14)).ToList();

                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.TaskScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        //Internal Display Details

        public static List<SP_TaskInstanceTransactionInternalView_Result> GetFilteredTaskInternalList(int customerID, int userID, int roleID, int taskType, int branchID, int Risk, string statusCode, int comTypeID, int SubTypeID, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.Now.AddMonths(1);

                var documentData = (from row in entities.SP_TaskInstanceTransactionInternalView(customerID)
                                    select row).ToList();

                documentData = documentData.Where(entry => entry.RoleID == roleID && entry.UserID == userID).ToList();
                if (taskType != 0)
                {
                    documentData = documentData.Where(entry => entry.TaskType == taskType).ToList();
                }

                if (branchID != -1)
                    documentData = documentData.Where(entry => entry.CustomerBranchID == branchID).ToList();

                switch (statusCode)
                {
                    case "Upcoming":

                        if (customerID == 63)
                        {
                            documentData = documentData.Where(entry => (entry.ScheduledOn >= now) //&& entry.ScheduledOn <= nextOneMonth
                             && (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)).ToList();
                        }
                        else
                        {
                            documentData = documentData.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth)
                             && (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)).ToList();
                        }
                        break;

                    case "Overdue":
                        documentData = documentData.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)
                                                         && entry.ScheduledOn < now).ToList();
                        break;

                    case "DueButNotSubmitted":
                        documentData = documentData.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12)
                                                          && entry.ScheduledOn < now).ToList();
                        break;

                    case "PendingForReview":
                        documentData = documentData.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 12)).ToList();
                        break;

                    case "Rejected":
                        documentData = documentData.Where(entry => entry.TaskStatusID == 6 || entry.TaskStatusID == 14).ToList();
                        break;
                }

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.RiskType == Risk).ToList();

                if (comTypeID != -1)
                    documentData = documentData.Where(entry => entry.ComplianceTypeId == comTypeID).ToList();

                if (SubTypeID != -1)
                    documentData = documentData.Where(entry => entry.ISubComplianceType == SubTypeID).ToList();

                if (ComCategory != -1)
                    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                if (Act != -1)
                    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                // Find data through String contained in Description
                if (StringType != "")
                {
                    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                if (documentData.Count > 0)
                {
                    documentData = documentData.OrderBy(entry => entry.TaskInstanceID).ThenBy(entry => entry.ScheduledOn).ToList();
                }

                return documentData.GroupBy(entry => entry.TaskScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }

        //public static List<SP_TaskInstanceTransactionInternalView_Result> GetFilteredTaskInternalList(int customerID, int userID, int roleID, int taskType, int branchID, int Risk, string statusCode, int comTypeID, int SubTypeID, int ComCategory, DateTime? StartDate, DateTime? EndDate, string StringType)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.Now.AddMonths(1);

        //        var documentData = (from row in entities.SP_TaskInstanceTransactionInternalView(customerID)
        //                            select row).ToList();

        //        documentData = documentData.Where(entry => entry.RoleID == roleID && entry.UserID == userID).ToList();
        //        if (taskType != 0)
        //        {
        //            documentData = documentData.Where(entry => entry.TaskType == taskType).ToList();
        //        }

        //        if (branchID != -1)
        //            documentData = documentData.Where(entry => entry.CustomerBranchID == branchID).ToList();

        //        switch (statusCode)
        //        {
        //            case "Upcoming":
        //                documentData = documentData.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth)
        //                 && (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)).ToList();
        //                break;

        //            case "Overdue":
        //                documentData = documentData.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12 || entry.TaskStatusID == 13)
        //                                                 && entry.ScheduledOn < now).ToList();
        //                break;

        //            case "DueButNotSubmitted":
        //                documentData = documentData.Where(entry => (entry.TaskStatusID == 1 || entry.TaskStatusID == 10 || entry.TaskStatusID == 12)
        //                                                  && entry.ScheduledOn < now).ToList();
        //                break;

        //            case "PendingForReview":
        //                documentData = documentData.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 12)).ToList();
        //                break;

        //            case "Rejected":
        //                documentData = documentData.Where(entry => entry.TaskStatusID == 6 || entry.TaskStatusID == 14).ToList();
        //                break;
        //        }

        //        if (StartDate != null && EndDate != null)
        //            documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

        //        if (Risk != -1)
        //            documentData = documentData.Where(entry => entry.RiskType == Risk).ToList();

        //        if (comTypeID != -1)
        //            documentData = documentData.Where(entry => entry.ComplianceTypeId == comTypeID).ToList();

        //        if (SubTypeID != -1)
        //            documentData = documentData.Where(entry => entry.ISubComplianceType == SubTypeID).ToList();

        //        if (ComCategory != -1)
        //            documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

        //        // Find data through String contained in Description
        //        if (StringType != "")
        //        {
        //            documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }

        //        if (documentData.Count > 0)
        //        {
        //            documentData = documentData.OrderBy(entry => entry.TaskInstanceID).ThenBy(entry => entry.ScheduledOn).ToList();
        //        }

        //        return documentData.GroupBy(entry => entry.TaskScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
        //    }
        //}
        public static List<Sp_TaskInstanceTransaction_Result> TaskDashboardDataForReviewerDisplayCount(int CustomerID, int userID, CannedReportFilterForPerformer filter, string Statutory)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                //var transactionsQuery;
                List<Sp_TaskInstanceTransaction_Result> transactionsQuery = new List<Sp_TaskInstanceTransaction_Result>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);


                switch (filter)
                {
                    case CannedReportFilterForPerformer.Overdue:
                        //var idList = new[] { 3,11,12 };
                        int?[] idList = new int?[] { 1, 10 };
                        transactionsQuery = entities.Sp_TaskInstanceTransaction(CustomerID, userID).ToList();
                        if (!string.IsNullOrEmpty(Statutory))
                        {
                            if (Statutory == "Statutory")
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.TaskType == 1).ToList();
                            }
                            if (Statutory == "Internal")
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.TaskType == 2).ToList();
                            }
                        }
                        transactionsQuery = transactionsQuery.Where(entry => reviewerRoleIDs.Contains(entry.RoleID)).ToList();
                        transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn < now).ToList();
                        transactionsQuery = transactionsQuery.Where(entry => idList.Contains(entry.TaskStatusID)).ToList();
                        //transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 1 || entry <= entry.TaskStatusID == 10 || entry <= entry.TaskStatusID == 12).ToList();
                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        int?[] idListReview = new int?[] { 2, 3, 11, 12 };
                        transactionsQuery = entities.Sp_TaskInstanceTransaction(CustomerID, userID).ToList();
                        if (!string.IsNullOrEmpty(Statutory))
                        {
                            if (Statutory == "Statutory")
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.TaskType == 1).ToList();
                            }
                            if (Statutory == "Internal")
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.TaskType == 2).ToList();
                            }
                        }
                        transactionsQuery = transactionsQuery.Where(entry => reviewerRoleIDs.Contains(entry.RoleID)).ToList();
                        transactionsQuery = transactionsQuery.Where(entry => idListReview.Contains(entry.TaskStatusID)).ToList();
                        // transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 2 || entry <= entry.TaskStatusID == 3).ToList();
                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;

                    case CannedReportFilterForPerformer.Rejected:
                        int?[] idListReject = new int?[] { 6, 14 };
                        transactionsQuery = entities.Sp_TaskInstanceTransaction(CustomerID, userID).ToList();
                        if (!string.IsNullOrEmpty(Statutory))
                        {
                            if (Statutory == "Statutory")
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.TaskType == 1).ToList();
                            }
                            if (Statutory == "Internal")
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.TaskType == 2).ToList();
                            }
                        }
                        transactionsQuery = transactionsQuery.Where(entry => reviewerRoleIDs.Contains(entry.RoleID)).ToList();
                        transactionsQuery = transactionsQuery.Where(entry => idListReject.Contains(entry.TaskStatusID)).ToList();
                        // transactionsQuery = transactionsQuery.Where(entry => entry.TaskStatusID == 6 || entry <= entry.TaskStatusID == 14).ToList();
                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        public static List<SP_GetTaskComplianceDocument_Result> GetFilteredTaskDocuments(int customerID, int userID, string Role, int Roleid, DocumentFilterNewStatus status, string location, DateTime? StartDate, DateTime? EndDate, string stringType, string TaskType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetTaskComplianceDocument(customerID)
                                    select row).ToList();

                if (RoleID != 8)
                {
                    var GetApprover = (from row in entities.TaskAssignments
                                       where row.UserID == userID
                                       select row)
                                       .GroupBy(a => a.RoleID)
                                       .Select(a => a.FirstOrDefault())
                                       .Select(entry => entry.RoleID).ToList();

                    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                        Roleid = 6;

                    if (!Role.Equals("CADMN"))
                    {
                        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                    }

                    if (Roleid != 0)
                        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                }
                else
                {
                    documentData = (from row in documentData
                                    join row1 in entities.EntitiesAssignments
                                    on (long) row.CustomerBranchID equals row1.BranchID
                                    where row.CustomerID == customerID
                                    && row1.UserID == userID
                                    select row).Distinct().ToList();
                }


                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();


                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.TaskStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.TaskStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.TaskStatusID == 2 || entry.TaskStatusID == 3 || entry.TaskStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.TaskStatusID == 6).ToList();
                        break;
                }

                if (TaskType != "-1")
                    documentData = documentData.Where(entry => entry.TaskType == Convert.ToInt32(TaskType)).ToList();

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();


                // Find data through String contained in Description
                if (stringType != "")
                {
                    documentData = documentData.Where(entry => (entry.TaskDescription.Contains(stringType))).ToList();
                }

                return documentData.GroupBy(entry => entry.TaskScheduleOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }
        public static string GetTaskAssignedUser(int customerID, int taskType, long taskInstanceID, long taskScheduledOnID, int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var userRecord = "";
                userRecord = (from row in entities.TaskInstanceTransactionViews
                                  join users in entities.Users on row.UserID equals users.ID
                                  where row.TaskInstanceID == taskInstanceID
                                  && row.TaskScheduledOnID == taskScheduledOnID
                                  && row.RoleID == roleID
                                  && row.TaskType == taskType
                                  && (row.IsActive == true || row.IsUpcomingNotDeleted == true)
                                  select users.FirstName + " " + users.LastName).FirstOrDefault();

                return userRecord;
            }
        }
  
    }
}
