﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Text;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class CustomerBranchManagement
    {
        public static int CreateBranchType(string typename)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevTypeRecord = entities.NodeTypes.Where(row => row.Name.ToUpper().Equals(typename.Trim().ToUpper())).FirstOrDefault();

                    if (prevTypeRecord == null)
                    {
                        NodeType newType = new NodeType()
                        {
                            Name = typename.Trim()
                        };

                        entities.NodeTypes.Add(newType);
                        entities.SaveChanges();
                        return newType.ID;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static List<NameValueHierarchy> GetAllHierarchyManagementSatutoryNew(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from Cust in entities.Customers
                             where Cust.ID == customerID
                             select Cust);

                hierarchy = (from row in query
                             select new NameValueHierarchy() { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                IQueryable<CustomerBranch> query1 = (from row in entities.CustomerBranches
                                                     where row.IsDeleted == false
                                                     && row.CustomerID == customerID
                                                     select row);
                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, query1);
                }
            }

            return hierarchy;
        }

        public static List<string> GetAllAssignedContractListbyuser(int custID = -1, int UserID = -1, string Role = "")
        {
            List<string> contractList = new List<string>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var LocationList = (from row in entities.ContractEntitiesAssignments
                                    where row.UserID == UserID
                                    select (int)row.BranchID).ToList();

                //if (Role == "MGMT")
                //{
                    var query1 = (from row in entities.Cont_tbl_ContractInstance
                                  join row1 in entities.Cont_tbl_UserAssignment
                                  on row.ID equals row1.ContractID
                                  where
                                  row.CustomerID == custID &&
                                  (row.CreatedBy == UserID || row1.UserID == UserID || row1.CreatedBy == UserID || LocationList.Contains(row.CustomerBranchID))
                                  select row).Distinct().ToList();

                    if (query1 != null)
                        contractList = query1.Select(a => a.ContractNo).ToList();


                //}
            }
            return contractList;
        }
        public static List<int> GetAllAssignedLocationListbyuser(int custID = -1, int UserID = -1)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var getAssignedEntitiyByUSer = (from row in entities.ContractEntitiesAssignments
                                                where row.UserID == UserID
                                                select row).ToList();
                if (getAssignedEntitiyByUSer.Count > 0)
                {
                    var query1 = (from row in entities.Cont_tbl_ContractInstance
                                  join row1 in entities.Cont_tbl_UserAssignment
                                  on row.ID equals row1.ContractID
                                  join row2 in entities.CustomerBranches
                                  on row.CustomerBranchID equals row2.ID
                                  join row3 in entities.ContractEntitiesAssignments
                                  on row2.ID equals row3.BranchID
                                  where row.IsDeleted == false
                                  && row2.CustomerID == custID
                                  && (row.CreatedBy == UserID || row1.UserID == UserID || row.UpdatedBy == UserID)
                                  select row2).Distinct();

                    if (query1 != null)
                        LocationList = query1.Select(a => a.ID).ToList();


                }
            }
            return LocationList;
        }
        public static List<NameValueHierarchy> GetAllAssignedEntitiesHierarchySatutory(int customerID = -1, int userid = -1, string role = "")
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from Cust in entities.Customers
                             where Cust.ID == customerID
                             select Cust);

                hierarchy = (from row in query
                             select new NameValueHierarchy() { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();
                //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                //if (IsEntityassignmentCustomer == Convert.ToString(customerID))
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    var getAssignedEntitiyByUSer = (from row in entities.LitigationEntitiesAssignments
                                                    where row.UserID == userid
                                                    select row.BranchID).ToList();
                   

                        List<int> data = (from row in entities.tbl_LegalCaseAssignment
                                                             join row1 in entities.tbl_LegalCaseInstance
                                                             on row.CaseInstanceID equals row1.ID
                                                             where row1.CustomerID == (int)customerID
                                                             && (row.UserID == userid || row1.OwnerID == userid || row1.CreatedBy == userid )
                                                             select row1.CustomerBranchID).ToList();

                    var query1 = (from row in entities.CustomerBranches
                                  where row.IsDeleted == false
                                 && row.CustomerID == customerID
                                 && (getAssignedEntitiyByUSer.Contains(row.ID)
                                 || data.Contains(row.ID))
                                  select row);


                    foreach (var item in hierarchy)
                        {
                            LoadSubEntities(item, true, entities, query1);
                        }
                   
                  
                }
                else
                {
                    IQueryable<CustomerBranch> query1 = (from row in entities.CustomerBranches
                                                         where row.IsDeleted == false
                                                         && row.CustomerID == customerID
                                                         select row);
                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, true, entities, query1);
                    }
                }




            }

            return hierarchy;
        }
        #region added by sagar on 18-05-2020
        public static bool ExistsBranch(string customerBranch, int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                            && row.Name == customerBranch && row.CustomerID == CustomerId
                             select row);

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool ExistsBranchInAuditDb(string customerBranch, int CustomerId)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false
                             && row.Name == customerBranch && row.CustomerID == CustomerId
                             select row);

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch GetByNameFromAuditDb(string customerBranchName, int customerid)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                var customerBranch = (from row in entities.mst_CustomerBranch
                                      where row.Name == customerBranchName && row.IsDeleted == false
                                      && row.CustomerID == customerid
                                      select row).FirstOrDefault();

                return customerBranch;
            }
        }

        #endregion
        public static List<int> GetAll_Branches(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranches = (from row in entities.CustomerBranches
                                        where row.IsDeleted == false
                                        && row.CustomerID == customerID
                                        select row.ID);

                return customerBranches.ToList();
            }
        }

        public static List<Act> BindActEvent(List<Act> actlst, int StateID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                if (StateID != -1 && StateID != 0)
                {
                    actlst = actlst.Where(entry => entry.StateID == StateID).ToList();
                }
                else
                {
                    //For Central
                    if (StateID == 0)
                    {
                        actlst = actlst.Where(entry => entry.ComplianceTypeId == 2).ToList();
                    }
                }

                return actlst.ToList();
            }
        }

        public static List<Act> BindActEvent(int StateID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actlst = (from row in entities.Acts
                              join row1 in entities.Compliances
                              on row.ID equals row1.ActID
                              where row.IsDeleted == false
                              && row1.EventFlag == true || row1.ComplianceType == 1
                              select row).Distinct().ToList();

                if (StateID != -1 && StateID != 0)
                {
                    actlst = actlst.Where(entry => entry.StateID == StateID).ToList();
                }
                else
                {
                    //For Central
                    if (StateID == 0)
                    {
                        actlst = actlst.Where(entry => entry.ComplianceTypeId == 2).ToList();
                    }
                }

                return actlst.ToList();
            }
        }
        public static void TaskBindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<int> LocationList)
        {
            foreach (var item in nvp.Children)
            {
                TreeNode node = new TreeNode(item.Name, item.ID.ToString());

                if (FindNodeExists(item, LocationList))
                {
                    if (!LocationList.Contains(item.ID))
                    {
                        node.ShowCheckBox = false;
                    }
                    TaskBindBranchesHierarchy(node, item, LocationList);
                    parent.ChildNodes.Add(node);
                }
            }
        }

        public static bool AuditNameExist(int custId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditDetails
                             where row.CustomerID == custId
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static List<int> GetAuditorAssignedLocationList(int UserID, int custID)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditDetails
                             where row.AuditorID == UserID
                             && row.CustomerID == custID
                             select row).Distinct().ToList();
                
                if (query != null)
                    LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                
                return LocationList;
            }
        }
        public static List<NameValueHierarchy> GetAllHierarchyForNonSecretrialEvent(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntitiesForNonSecretrialEvent(item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntitiesForNonSecretrialEvent(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                //&& row.Type == 1
                                                select row);

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntitiesForNonSecretrialEvent(item, false, entities);
            }
        }

        public static List<int> GetAssignedEventLocationList(int UserID, int custID)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Event_Assigned_View
                             join row1 in entities.CustomerBranches
                             on row.CustomerBranchID equals row1.ID
                             where row.UserID == UserID
                             select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                if (query != null)
                    LocationList = query.Select(a => a.ID).Distinct().ToList();

                return LocationList;
            }
        }

        public static List<int> GetAssignedCalenderRoleId(int Userid, DateTime dt)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> roles = new List<int>();
                entities.Database.CommandTimeout = 180;
                var rolesfromsp = (entities.Sp_GetAllAssignedCalenderRoles(Userid, dt)).ToList();
                roles = rolesfromsp.Where(x => x != null).Cast<int>().ToList();
                return roles;
            }
        }
        public static int UpdateCustBranch(CustomerBranch customerBranch)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //CustomerBranch customerBranchToUpdate = new CustomerBranch() { ID = customerBranch.ID };
                    //entities.CustomerBranches.Attach(customerBranchToUpdate);

                    CustomerBranch customerBranchToUpdate = (from row in entities.CustomerBranches
                                                             where row.ID == customerBranch.ID && row.IsDeleted == false
                                                             select row).FirstOrDefault();

                    customerBranchToUpdate.Name = customerBranch.Name;
                    customerBranchToUpdate.Type = customerBranch.Type;
                    customerBranchToUpdate.LegalRelationShipOrStatus = customerBranch.LegalRelationShipOrStatus;
                    customerBranchToUpdate.AddressLine1 = customerBranch.AddressLine1;
                    customerBranchToUpdate.AddressLine2 = customerBranch.AddressLine2;
                    customerBranchToUpdate.StateID = customerBranch.StateID;
                    customerBranchToUpdate.CityID = customerBranch.CityID;
                    customerBranchToUpdate.Others = customerBranch.Others;
                    customerBranchToUpdate.Industry = customerBranch.Industry;
                    customerBranchToUpdate.ContactPerson = customerBranch.ContactPerson;
                    customerBranchToUpdate.Landline = customerBranch.Landline;
                    customerBranchToUpdate.Mobile = customerBranch.Mobile;
                    customerBranchToUpdate.EmailID = customerBranch.EmailID;
                    customerBranchToUpdate.PinCode = customerBranch.PinCode;
                    customerBranchToUpdate.Status = customerBranch.Status;
                    customerBranchToUpdate.LegalEntityTypeID = customerBranch.LegalEntityTypeID;//added by manisha
                    customerBranchToUpdate.ComType = customerBranch.ComType;
                    customerBranchToUpdate.AuditPR = customerBranch.AuditPR;

                    entities.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static List<CompanyType> GetallType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objTypeList = (from row in entities.CompanyTypes
                                   where row.Name != null
                                   select row);
                return objTypeList.ToList();
            }
        }

        public static bool TypeNameExist(string typeName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CompanyTypes
                             where row.Name.Equals(typeName)
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateType(CompanyType objType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.CompanyTypes.Add(objType);
                entities.SaveChanges();
            }
        }

        public static void UpdateType(CompanyType objType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CompanyType objTypeList = (from row in entities.CompanyTypes
                                           where row.ID == objType.ID
                                           select row).FirstOrDefault();

                objTypeList.Name = objType.Name;
                entities.SaveChanges();
            }
        }

        public static CompanyType GetTypeByID(int iD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objTypes = (from row in entities.CompanyTypes
                                where row.ID == iD
                                select row).SingleOrDefault();
                return objTypes;
            }
        }

        public static void DeleteType(int iD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CompanyType objTypes = (from row in entities.CompanyTypes
                                        where row.ID == iD
                                        select row).FirstOrDefault();

                entities.CompanyTypes.Remove(objTypes);
                entities.SaveChanges();
            }
        }
        public static List<CompanyType> GetAllComanyType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComanyType = (from row in entities.CompanyTypes
                                  where row.IsVisible == true
                                  select row).ToList();
                                   
                return ComanyType.OrderBy(entry => entry.Name).ToList();
            }
        }


        public static List<CompanyType> GetAllComanyTypeCustomerBranch()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> ids = new List<int>();
                ids.Add(1);
                ids.Add(2);
                ids.Add(3);
                ids.Add(5);
                ids.Add(12);
                ids.Add(13);

                var ComanyType = (from row in entities.CompanyTypes
                                  where ids.Contains(row.ID)
                                  select row).ToList();

                return ComanyType.OrderBy(entry => entry.Name).ToList();
            }
        }


        public static List<int> GetAssignedLocationList(int UserID, int custID, String Role,string statutoryInternal)
        {
            List<int> LocationList = new List<int>();

            var UserDetails = UserManagement.GetByID(UserID);
            if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
            {
                if ((bool)UserDetails.IsHead)
                {
                    Role = "DEPT";
                }
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (statutoryInternal == "S")
                {
                    if (Role == "MGMT" || Role == "AUDT" || Role == "HRMGR")
                    {
                        var query = (from row in entities.EntitiesAssignments                                    
                                     where row.UserID == UserID
                                     select row).Distinct().ToList();                      
                        if (query != null)
                            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                    }
                    else if (Role == "DEPT")
                    {
                        var query = (from row in entities.EntitiesAssignment_IsDept
                                     where row.UserID == UserID && row.IsStatutoryInternal=="S"
                                     select row).Distinct().ToList();
                        if (query != null)
                            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                    }
                    else
                    {
                        var query = (from row in entities.ComplianceAssignedInstancesViews
                                     join row1 in entities.CustomerBranches
                                     on row.CustomerBranchID equals row1.ID
                                     where row.UserID == UserID
                                     select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                        if (query != null)
                            LocationList = query.Select(a => a.ID).Distinct().ToList();
                    }
                }
                else if(statutoryInternal == "I")
                {
                    if (Role == "MGMT" || Role == "AUDT" || Role == "HRMGR")
                    {
                        var query = (from row in entities.EntitiesAssignmentInternals
                                     where row.UserID == UserID
                                     select row).Distinct().ToList();

                    
                        if (query != null)
                            LocationList = query.Select(a => (int)a.BranchID).ToList();

                    }
                    else if (Role == "DEPT")
                    {
                        var query = (from row in entities.EntitiesAssignment_IsDept
                                     where row.UserID == UserID && row.IsStatutoryInternal == "I"
                                     select row).Distinct().ToList();
                        if (query != null)
                            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                    }
                    else
                    {
                        var query = (from row in entities.InternalComplianceAssignedInstancesViews
                                     join row1 in entities.CustomerBranches
                                     on row.CustomerBranchID equals row1.ID
                                     where row.UserID == UserID
                                     select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                        if (query != null)
                            LocationList = query.Select(a => a.ID).ToList();
                    }
                }                               
                return LocationList;
            }
        }
        public static List<int> RLCS_GetAssignedLocationList(int UserID, int custID, String Role, string statutoryInternal)
        {
            List<int> LocationList = new List<int>();

            var UserDetails = UserManagement.GetByID(UserID);
            if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
            {
                if ((bool)UserDetails.IsHead)
                {
                    Role = "DEPT";
                }
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (statutoryInternal == "S")
                {
                    if (Role == "MGMT" || Role == "AUDT" || Role == "HRMGR" || Role == "HMGR")
                    {
                        var query = (from row in entities.EntitiesAssignments
                                     where row.UserID == UserID
                                     select row).Distinct().ToList();
                        if (query != null)
                            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                    }
                    else if (Role == "DEPT")
                    {
                        var query = (from row in entities.EntitiesAssignment_IsDept
                                     where row.UserID == UserID && row.IsStatutoryInternal == "S"
                                     select row).Distinct().ToList();
                        if (query != null)
                            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                    }
                    else
                    {
                        var query = (from row in entities.ComplianceAssignedInstancesViews
                                     join row1 in entities.CustomerBranches
                                     on row.CustomerBranchID equals row1.ID
                                     where row.UserID == UserID
                                     select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                        if (query != null)
                            LocationList = query.Select(a => a.ID).Distinct().ToList();
                    }
                }
                else if (statutoryInternal == "I")
                {
                    if (Role == "MGMT" || Role == "AUDT" || Role == "HRMGR")
                    {
                        var query = (from row in entities.EntitiesAssignmentInternals
                                     where row.UserID == UserID
                                     select row).Distinct().ToList();


                        if (query != null)
                            LocationList = query.Select(a => (int)a.BranchID).ToList();

                    }
                    else if (Role == "DEPT")
                    {
                        var query = (from row in entities.EntitiesAssignment_IsDept
                                     where row.UserID == UserID && row.IsStatutoryInternal == "I"
                                     select row).Distinct().ToList();
                        if (query != null)
                            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                    }
                    else
                    {
                        var query = (from row in entities.InternalComplianceAssignedInstancesViews
                                     join row1 in entities.CustomerBranches
                                     on row.CustomerBranchID equals row1.ID
                                     where row.UserID == UserID
                                     select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                        if (query != null)
                            LocationList = query.Select(a => a.ID).ToList();
                    }
                }
                return LocationList;
            }
        }
        public static void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            foreach (var item in nvp.Children)
            {
                TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                BindBranchesHierarchy(node, item);
                parent.ChildNodes.Add(node);
            }
        }
        public static void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<int> LocationList)
        {
            foreach (var item in nvp.Children)
            {
                TreeNode node = new TreeNode(item.Name, item.ID.ToString());

                if (FindNodeExists(item, LocationList))
                {
                    BindBranchesHierarchy(node, item, LocationList);
                    parent.ChildNodes.Add(node);
                }
            }
        }

        public static bool FindNodeExists(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {
                            result = FindNodeExists(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public static List<SP_GetManagerAssignedBranch_Result> GetMGMTAssignedBranch(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                var rolesfromsp = (entities.SP_GetManagerAssignedBranch(Userid)).ToList();               
                return rolesfromsp;
            }
        }
        public static List<int> GetAssignedroleid(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                        
                List<int> roles = new List<int>();
                var rolesfromsp = (entities.Sp_GetAllAssignedRoles(Userid)).ToList();                
                roles= rolesfromsp.Where(x => x != null).Cast<int>().ToList();
                #region Previous Code
                //var Compliance = (from row in entities.ComplianceAssignments                                  
                //                  where row.UserID == Userid 
                //                  select row.RoleID).ToList().Distinct();

                //if (Compliance !=null)
                //{
                //    ComplianceAssignedRoles = Compliance.ToList();
                //}
                //var InternalCompliance = (from row in entities.InternalComplianceAssignments
                //                  where row.UserID == Userid
                //                  select row.RoleID).ToList().Distinct();
                //if (InternalCompliance != null)
                //{
                //    InternalComplianceAssignedRoles = InternalCompliance.ToList();
                //}               
                //var EventOwner = (from row in entities.EventAssignments
                //                  where row.UserID == Userid
                //                  select row.Role).ToList().Distinct();

                //if (EventOwner != null)
                //{
                //    EventOwnerAssignedRoles = EventOwner.ToList();
                //}                
                //roles = ComplianceAssignedRoles.Union(EventOwnerAssignedRoles).ToList();
                //roles= roles.Union(InternalComplianceAssignedRoles).ToList();
                #endregion
                return roles;
            }
        }

        public static List<CustomerBranch> GetAllBranchesid(int ParentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
               
                var query = (from row in entities.CustomerBranches
                                        where row.ParentID == ParentID
                             select row).ToList();

                return query;
            }
        }

        public static List<CustomerBranch> GetAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranches = (from row in entities.CustomerBranches
                                        where row.IsDeleted == false
                                        select row);

                return customerBranches.ToList();
            }
        }

        public static List<NameValue> GetAllNVP(int? customerID = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranches = (from row in entities.CustomerBranches
                                        where row.IsDeleted == false
                                        select row);

                if (customerID.HasValue)
                {
                    customerBranches = customerBranches.Where(row => row.CustomerID == customerID.Value);
                }

                return customerBranches.Select(row => new NameValue() { ID = row.ID, Name = row.Name }).ToList();
            }
        }

        public static List<NodeType> GetAllNodeTypes()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var nodeTypes = entities.NodeTypes;

                return nodeTypes.OrderBy(entry=>entry.Name).ToList();
            }
        }

        public static List<SubIndustry> GetAllSubIndustry(List<long?> Industrylist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var subindustries =(from row in entities.SubIndustries
                                    where Industrylist.Contains(row.IndustryId)
                                    select row).ToList() ;

                return subindustries.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<Industry> GetAllIndustry()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.Industries;

                return industries.OrderBy(entry=>entry.Name).ToList();
            }
        }

        public static List<LegalStatu> GetAllLegalStatus(int IndustryId, int NodeTypeId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var legalStatus = (from row in entities.LegalStatus
                                   where row.IndustryId == IndustryId && row.NodeTypeId == NodeTypeId
                                   select row).ToList();

                return legalStatus.OrderBy(entry=>entry.Name).ToList();
            }
        }

        public static List<CustomerBranchView> GetAll(int customerID, long parentID, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranches = (from row in entities.CustomerBranchViews
                                        where row.IsDeleted == false && row.CustomerID == customerID
                                        select row);

                if (parentID != -1)
                {
                    customerBranches = customerBranches.Where(entry => entry.ParentID == parentID);
                }
                else
                {
                    customerBranches = customerBranches.Where(entry => entry.ParentID == null);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    customerBranches = customerBranches.Where(entry => entry.Name.Contains(filter) || entry.TypeName.Contains(filter) || entry.ContactPerson.Contains(filter) || entry.EmailID.Contains(filter) || entry.Landline.Contains(filter));
                }

                return customerBranches.ToList();
            }
        }
      
        public static CustomerBranch GetByID(long customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranch = (from row in entities.CustomerBranches
                                      where row.ID == customerBranchID && row.IsDeleted==false
                                      select row).SingleOrDefault();

                return customerBranch;
            }
        }
        public static Int32 GetByGRadingReportName(string customerBranchName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranch = (from row in entities.CustomerBranches
                                      where row.Name == customerBranchName && row.IsDeleted == false
                                      select row.ID).FirstOrDefault();

                return customerBranch;
            }
        }
        public static CustomerBranch GetByName(string customerBranchName, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranch = (from row in entities.CustomerBranches
                                      where row.Name == customerBranchName && row.IsDeleted == false
                                      && row.CustomerID == customerid
                                      select row).FirstOrDefault();

                return customerBranch;
            }
        }
        public static int GetBranchIDByName(string customerBranchName, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranch = (from row in entities.CustomerBranches
                                      where row.Name == customerBranchName && row.IsDeleted == false
                                      && row.CustomerID == customerid
                                      select row.ID).SingleOrDefault();

                return customerBranch;
            }
        }

        public static int GetIndustryIDByCustomerId(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranch = (from row in entities.CustomerBranches
                                      where row.ID == customerID && row.IsDeleted == false
                                      select row).SingleOrDefault();

                return Convert.ToInt32(customerBranch.Industry);
            }
        }

        public static void Delete(int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {               
                CustomerBranch customerBranchToDelete = (from row in entities.CustomerBranches
                                                         where row.ID == customerBranchID 
                                                         select row).FirstOrDefault();

                customerBranchToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }
        public static void Delete_RLCSMappingBranch(int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchToDelete = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                                                      where row.AVACOM_BranchID == customerBranchID
                                                                                      select row).FirstOrDefault();

                customerBranchToDelete.CM_Status ="I";

                entities.SaveChanges();
            }
        }

        public static bool Exists(CustomerBranch customerBranch,int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.Name.Equals(customerBranch.Name) && row.CustomerID == CustomerId
                             select row);

                if (customerBranch.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customerBranch.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static bool Exists1(com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch mst_CustomerBranch, int CustomerId)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false
                             && row.Name.Equals(mst_CustomerBranch.Name) && row.CustomerID == CustomerId
                             select row);

                if (mst_CustomerBranch.ID > 0)
                {
                    query = query.Where(entry => entry.ID != mst_CustomerBranch.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static bool Update(CustomerBranch customerBranch)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //CustomerBranch customerBranchToUpdate = new CustomerBranch() { ID = customerBranch.ID };
                    //entities.CustomerBranches.Attach(customerBranchToUpdate);

                    CustomerBranch customerBranchToUpdate = (from row in entities.CustomerBranches
                                                             where row.ID == customerBranch.ID && row.IsDeleted == false
                                                             select row).FirstOrDefault();

                    customerBranchToUpdate.Name = customerBranch.Name;
                    customerBranchToUpdate.Type = customerBranch.Type;
                    customerBranchToUpdate.LegalRelationShipOrStatus = customerBranch.LegalRelationShipOrStatus;
                    customerBranchToUpdate.AddressLine1 = customerBranch.AddressLine1;
                    customerBranchToUpdate.AddressLine2 = customerBranch.AddressLine2;
                    customerBranchToUpdate.StateID = customerBranch.StateID;
                    customerBranchToUpdate.CityID = customerBranch.CityID;
                    customerBranchToUpdate.Others = customerBranch.Others;
                    customerBranchToUpdate.Industry = customerBranch.Industry;
                    customerBranchToUpdate.ContactPerson = customerBranch.ContactPerson;
                    customerBranchToUpdate.Landline = customerBranch.Landline;
                    customerBranchToUpdate.Mobile = customerBranch.Mobile;
                    customerBranchToUpdate.EmailID = customerBranch.EmailID;
                    customerBranchToUpdate.PinCode = customerBranch.PinCode;
                    customerBranchToUpdate.Status = customerBranch.Status;
                    customerBranchToUpdate.LegalEntityTypeID = customerBranch.LegalEntityTypeID;//added by manisha
                    customerBranchToUpdate.ComType = customerBranch.ComType;
                    customerBranchToUpdate.AuditPR = customerBranch.AuditPR;

                    customerBranchToUpdate.LegalRelationShipID = customerBranch.LegalRelationShipID;
                    customerBranchToUpdate.CreatedBy = customerBranch.CreatedBy;

                    var customizedid = CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(customerBranchToUpdate.CustomerID), "GSTFields");
                    if (customizedid == customerBranch.CustomerID)
                    {
                        customerBranchToUpdate.GSTNumber = customerBranch.GSTNumber;
                    }

                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool Update1(com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch)
        {
            try
            {
                using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
                {
                    //CustomerBranch customerBranchToUpdate = new CustomerBranch() { ID = customerBranch.ID };
                    //entities.CustomerBranches.Attach(customerBranchToUpdate);

                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranchToUpdate = (from row in entities.mst_CustomerBranch
                                                                                                                          where row.ID == customerBranch.ID
                                                                                                                          select row).FirstOrDefault();
                    customerBranchToUpdate.Name = customerBranch.Name;
                    customerBranchToUpdate.Type = customerBranch.Type;
                    customerBranchToUpdate.LegalRelationShipOrStatus = customerBranch.LegalRelationShipOrStatus;
                    customerBranchToUpdate.AddressLine1 = customerBranch.AddressLine1;
                    customerBranchToUpdate.AddressLine2 = customerBranch.AddressLine2;
                    customerBranchToUpdate.StateID = customerBranch.StateID;
                    customerBranchToUpdate.CityID = customerBranch.CityID;
                    customerBranchToUpdate.Others = customerBranch.Others;
                    customerBranchToUpdate.Industry = customerBranch.Industry;
                    customerBranchToUpdate.ContactPerson = customerBranch.ContactPerson;
                    customerBranchToUpdate.Landline = customerBranch.Landline;
                    customerBranchToUpdate.Mobile = customerBranch.Mobile;
                    customerBranchToUpdate.EmailID = customerBranch.EmailID;
                    customerBranchToUpdate.PinCode = customerBranch.PinCode;
                    customerBranchToUpdate.Status = customerBranch.Status;
                    customerBranchToUpdate.LegalEntityTypeID = customerBranch.LegalEntityTypeID;//added by manisha
                    customerBranchToUpdate.AuditPR = customerBranch.AuditPR;
                    customerBranchToUpdate.ComType = customerBranch.ComType;

                    customerBranchToUpdate.LegalRelationShipID = customerBranch.LegalRelationShipID;

                    entities.SaveChanges();

                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        public static void deleteCustomerBranch(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var tet = entities.CustomerBranches.Where(x => x.ID == Userid).FirstOrDefault();
                    entities.CustomerBranches.Remove(tet);
                    entities.SaveChanges();
                    Userid--;
                    entities.SP_ResetIDCustomerBranch(Userid);
                }
                catch (Exception ex)
                {
                }
            }
        }
        public static int Create(CustomerBranch customerBranch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int addid = 0;
                try
                {
                    customerBranch.IsDeleted = false;
                    customerBranch.CreatedOn = DateTime.UtcNow;
                    entities.CustomerBranches.Add(customerBranch);
                    entities.SaveChanges();
                    addid = Convert.ToInt32(customerBranch.ID);
                    return addid;
                }
                catch (Exception ex)
                {
                    return addid = 0;
                }
            }
        }

        //public static void Create1(com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch mst_CustomerBranch)
        //{
        //    using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
        //    {
        //        mst_CustomerBranch.IsDeleted = false;
        //        mst_CustomerBranch.CreatedOn = DateTime.UtcNow;

        //        entities.mst_CustomerBranch.Add(mst_CustomerBranch);

        //        entities.SaveChanges();
        //    }
        //}
        public static bool Create1(com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch mst_CustomerBranch)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                try
                {
                    mst_CustomerBranch.IsDeleted = false;
                    mst_CustomerBranch.CreatedOn = DateTime.UtcNow;
                    entities.mst_CustomerBranch.Add(mst_CustomerBranch);
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static object GetHierarchy(int customerID, long parentCustomerBranchID)
        {
            List<NameValue> hierarchy = new List<NameValue>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (parentCustomerBranchID != -1)
                {
                    long? parentID = parentCustomerBranchID;
                    while (parentID.HasValue)
                    {
                        var customerBranch = (from row in entities.CustomerBranches
                                              where row.ID == parentID.Value
                                              select new { row.ID, row.Name, row.ParentID }).SingleOrDefault();

                        hierarchy.Add(new NameValue() { ID = (int)customerBranch.ID, Name = customerBranch.Name });
                        parentID = customerBranch.ParentID;
                    }
                }

                var customerName = (from row in entities.Customers
                                    where row.ID == customerID
                                    select row.Name).SingleOrDefault();

                hierarchy.Add(new NameValue() { ID = customerID, Name = customerName });
            }

            hierarchy.Reverse();

            return hierarchy;
        }
        public static List<NameValueHierarchy> GetAllHierarchySatutory(long customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }
        public static List<NameValueHierarchy> GetAllHierarchy(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {  
                var query = (from row in entities.Customers
                             where row.IsDeleted == false 
                             && row.ID==customerID
                             && row.IsDeleted == false
                             select row);  

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).Distinct().ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }
        public static List<NameValueHierarchy> GetAllHierarchyLocation(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {   
                var query = (from row in entities.Customers
                             where row.IsDeleted == false

                             && row.ID == customerID
                             && row.IsDeleted == false
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).Distinct().ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }

        public static List<NameValueHierarchy> GetAllHierarchyEventBased(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                             select row);



                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntitiesEventBased(item, true, entities);
                }
            }

            return hierarchy;
        }
        public static List<NameValueHierarchy> GetAllHierarchyManagementSatutory(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                var query = (from Cust in entities.Customers
                             where Cust.ID == customerID
                             select Cust);
                
                hierarchy = (from row in query
                             select new NameValueHierarchy() { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                IQueryable<CustomerBranch> query1 = (from row in entities.CustomerBranches
                                                     where row.IsDeleted == false
                                                     && row.CustomerID == customerID
                                                     select row);
                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, query1);
                }
            }

            return hierarchy;
        }

        public static List<NameValueHierarchy> GetAllContractEntitiesAssignedHierarchyManagementSatutory(int customerID = -1, int userID = -1, string role = "")
        {
            List<NameValueHierarchy> hierarchy = null;
            //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from Cust in entities.Customers
                             where Cust.ID == customerID
                             select Cust);

                hierarchy = (from row in query
                             select new NameValueHierarchy() { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                //if ( IsEntityassignmentCustomer == Convert.ToString(customerID))
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {


                    List<long> getAssignedEntitiyByUSer = (from row in entities.ContractEntitiesAssignments
                                                           where row.UserID == userID
                                                           select row.BranchID).ToList();

                    List<long> LegalCasebranchlist = (from row in entities.Cont_tbl_ContractInstance
                                                      join row1 in entities.Cont_tbl_UserAssignment
                                                      on row.ID equals row1.ContractID
                                                      where row.IsDeleted == false
                                                      && row.CustomerID == customerID
                                                      && (row.CreatedBy == userID || row1.UserID == userID || row.UpdatedBy == userID)
                                                      select (long)row.CustomerBranchID).ToList();


                    var query1 = (from row in entities.CustomerBranches
                                  where row.IsDeleted == false
                                 && row.CustomerID == customerID
                                 && (getAssignedEntitiyByUSer.Contains(row.ID)
                                 || LegalCasebranchlist.Contains(row.ID))
                                  select row);


                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, true, entities, query1);
                    }

                }
                else
                {
                    var query1 = (from row in entities.CustomerBranches
                                  where row.IsDeleted == false
                                 && row.CustomerID == customerID
                                  select row);



                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, true, entities, query1);
                    }
                }




            }

            return hierarchy;
        }


        public static List<NameValueHierarchy> GetAllEntitiesAssignedHierarchyManagementSatutory(int customerID = -1, int userID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from Cust in entities.Customers
                             where Cust.ID == customerID
                             select Cust);

                hierarchy = (from row in query
                             select new NameValueHierarchy() { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                IQueryable<CustomerBranch> query1 = (from row in entities.CustomerBranches
                                                     join row1 in entities.LitigationEntitiesAssignments
                                                     on row.ID equals row1.BranchID
                                                     where row.IsDeleted == false
                                                     && row.CustomerID == customerID
                                                     && row1.UserID==userID
                                                     select row);
                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, query1);
                }
            }

            return hierarchy;
        }

        public static List<NameValueHierarchy> GetAllHierarchyInternal(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var query = (from row in entities.Customers
                //             where row.IsDeleted == false
                //             select row);

                var query = (from row in entities.EntitiesAssignmentInternals
                             join Cbranch in entities.CustomerBranches
                             on row.BranchID equals Cbranch.ID
                             join Cust in entities.Customers
                             on Cbranch.CustomerID equals Cust.ID                            
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }                
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }      
        public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, IQueryable<CustomerBranch> query1)
        {
            var query = query1;
            if (isClient)
            {
                query = query1.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query1.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntities(item, false, entities, query1);
            }
        }
        public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.Status == 1
                                                select row);
           
            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
               LoadSubEntities(item, false, entities);
            }
        }

        public static void LoadSubEntitiesEventBased(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {

            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                join row1 in entities.EventInstances
                                                on row.ID equals row1.CustomerBranchID
                                                join row2 in entities.EventScheduleOns
                                                on row1.ID equals row2.EventInstanceID
                                                where row.IsDeleted == false && row.Status == 1
                                                && row2.IsDeleted == false && row1.IsDeleted == false
                                                select row).Distinct();

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).Distinct().ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntitiesEventBased(item, false, entities);
            }
        }


        public static CustomerBranch GetlegalEntity(int BranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranch = (from row in entities.CustomerBranches
                                      where row.ID == BranchID
                                      select row).SingleOrDefault();
                if(customerBranch.ParentID !=null)
                {
                         return entities.CustomerBranches.Where(entry=>entry.ID == customerBranch.ParentID).FirstOrDefault();
                }else
                {
                    return customerBranch;
                }
            }
        }
        //Bind secretrial tag
        public static List<Compliance_SecretarialTagMaster> GetAllIsForSecretarial()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var SecretrialType = entities.Compliance_SecretarialTagMaster;
                return SecretrialType.OrderBy(entry => entry.Tag).ToList();

            }
        }
        //added By Manisha
        public static List<M_LegalEntityType> GetAllLegalEntityType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var LegalEntityType = entities.M_LegalEntityType;
                return LegalEntityType.OrderBy(entry => entry.EntityTypeName).ToList();

            }
        }

        public static List<Act> BindAct()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actlst = (from row in entities.Acts
                              join row1 in entities.Compliances
                              on row.ID equals row1.ActID
                              where row.IsDeleted == false 
                              && row1.EventFlag == true || row1.ComplianceType  == 1 
                              select row).Distinct();
                return actlst.ToList();
            }
        }

        public static List<NameValueHierarchy> GetAllHierarchyForEvent(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntitiesForEvent(item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntitiesForEvent(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.Type == 1
                                                select row);

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntitiesForEvent(item, false, entities);
            }
        }

        public static List<ComplianceList> GetCompliance(int actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Compliancelist = (from row in entities.Compliances
                                      select new ComplianceList
                                      {
                                          ID = row.ID,
                                          ShortDescription = row.ShortDescription,
                                          IsDeleted = row.IsDeleted,
                                          EventFlag = row.EventFlag,
                                          ActID = row.ActID,
                                      }).Distinct().ToList();
                if (actid == 0)
                {
                    return Compliancelist.Where(entry => entry.IsDeleted == false && entry.EventFlag == true).OrderBy(entry => entry.ShortDescription).ToList();
                }
                else
                {
                    return Compliancelist.Where(entry => entry.IsDeleted == false && entry.ActID == actid && entry.EventFlag == true).OrderBy(entry => entry.ShortDescription).ToList();

                }
            }
        }

        public static List<ComplianceList> GetCompliances()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Compliancelist = (from row in entities.Compliances
                                      select new ComplianceList
                                      {
                                          ID = row.ID,
                                          ShortDescription = row.ShortDescription,
                                          IsDeleted = row.IsDeleted,
                                          EventFlag = row.EventFlag,
                                          ActID = row.ActID,
                                      }).Distinct().ToList();

                return Compliancelist.Where(entry => entry.IsDeleted == false).OrderBy(entry => entry.ShortDescription).ToList();
            }
        }

        public static List<Event> GetEvent(int type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var events = entities.Events;

                return events.Where(entry => entry.EventType == type && entry.IsDeleted == false).OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<Event> GetEventExceptOne(int eventid, int companyType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var events = entities.Events;

                if (eventid != -1 && eventid != null)
                {
                    return events.Where(entry => entry.Type == companyType && entry.IsDeleted == false && entry.ID != eventid).OrderBy(entry => entry.Name).ToList();
                }
                else
                {
                    return events.Where(entry => entry.Type == companyType && entry.IsDeleted == false).OrderBy(entry => entry.Name).ToList();
                }
            }
        }
        public static List<NameValueHierarchy> GetAllHierarchyARS(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from Cbranch in entities.CustomerBranches
                             join Cust in entities.Customers
                             on Cbranch.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }
        public static bool CheckPersonResponsibleFlowApplicable(int CustomerId, List<long> CustomerbranchIds)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == CustomerId &&  CustomerbranchIds.Contains(row.ID) // && row.ParentID == null
                             && row.AuditPR == true
                             select row).ToList().Distinct().FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckPersonResponsibleFlowApplicable(int CustomerId,int CustomerbranchId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == CustomerId && row.ID== CustomerbranchId // && row.ParentID == null
                             && row.AuditPR == true
                             select row).ToList().Distinct().FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
      
        public static int GetByNameforUpload(string customerBranchName)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.CustomerBranches
                                 where row.Name == customerBranchName
                                 select row);
                    var CustomerBranchid = query.FirstOrDefault<CustomerBranch>();
                    return Convert.ToInt32(CustomerBranchid.ID);
                }
            }
            catch (Exception)
            {
                return -1;
            }

        }

        //Added by Amita as on 4 JAN 2019
        public static List<NameValueHierarchy> GetAllHierarchyForMappedLocation(int customerID)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {                   
                    LoadSubEntitiesForMappedLocation(item, true, entities);
                }
            }

            return hierarchy;
        }
        public static void LoadSubEntitiesForMappedLocation(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            var query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.Status == 1                                                
                                                select row).ToList();           

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null).ToList();               
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID).ToList();
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();


            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);                
                LoadSubEntitiesForMappedLocation(item, false, entities);
            }
        }

        public static List<NameValueHierarchy> GetAllHierarchyForMappingLocation(int customerID = -1,int mappedbranchID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntitiesForMappingLocation(item, true, entities, customerID, mappedbranchID);
                }
            }

            return hierarchy;
        }
        public static void LoadSubEntitiesForMappingLocation(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, int customerID, int mappedbranchID =-1)
        {                 
            IQueryable<CustomerBranch> Finalquery = (from row in entities.CustomerBranches
                                                     where row.IsDeleted == false && row.Status == 1                                                                                                          
                                                     select row).Distinct();                                               

            if (isClient)
            {
                Finalquery = Finalquery.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                Finalquery = Finalquery.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = Finalquery.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntitiesForMappingLocation(item, false, entities, customerID,mappedbranchID);
            }
        }       

        public static bool ExistsMappedBranch(int mappedbranchID =-1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> statCustomerBranchlst = new List<int>();
                List<int> checklistCustomerbranchlst = new List<int>();

                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.Status == 1
                             && row.ID == mappedbranchID
                             select row).ToList();

                statCustomerBranchlst = (from row in entities.TempAssignmentTables
                                         where row.CustomerBranchID == mappedbranchID
                                         && row.RoleID == 3                                       
                                         select row.CustomerBranchID).Distinct().ToList();

                checklistCustomerbranchlst = (from row in entities.TempAssignmentTableCheckLists                                              
                                              where row.RoleID == 3
                                              && row.CustomerBranchID == mappedbranchID
                                              select row.CustomerBranchID).Distinct().ToList();
              
                if (statCustomerBranchlst == null && checklistCustomerbranchlst == null)
                {
                    return false;
                }
                else
                {
                    query = query.Where(entry => statCustomerBranchlst.Contains(entry.ID) || checklistCustomerbranchlst.Contains(entry.ID)).ToList();
                    return query.Select(entry => true).FirstOrDefault();                    
                }
            }
        }

        public static bool IsParentForOtherBranch(int mappedbranchID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {               
                List<int> isParentBranchFrOtherbrancheslst = new List<int>();                
               
                isParentBranchFrOtherbrancheslst = (from row in entities.CustomerBranches
                                                    where row.ParentID == mappedbranchID
                                                    select row.ID).Distinct().ToList();

                return isParentBranchFrOtherbrancheslst.Select(entry => true).FirstOrDefault();
               
            }
        }

        public static bool ExistsMappedStateIDToBranch(int mappingbranchID =-1, int mappedbranchID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                int StateId = -1;

                if (mappedbranchID != -1)
                    StateId = CustomerBranchManagement.GetByID(mappedbranchID).StateID;

                List<int> isStateIDMappedFrMappingBranch = new List<int>();

                isStateIDMappedFrMappingBranch = (from row in entities.CustomerBranches
                                                  where row.IsDeleted == false && row.Status == 1                                                 
                                                  && row.ID == mappingbranchID
                                                  && row.StateID == StateId
                                                  select row.ID).Distinct().ToList();

                return isStateIDMappedFrMappingBranch.Select(entry => true).FirstOrDefault();

            }
        }

        public static void UpdateFromSecretarial(CustomerBranch customerBranch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //CustomerBranch customerBranchToUpdate = new CustomerBranch() { ID = customerBranch.ID };
                //entities.CustomerBranches.Attach(customerBranchToUpdate);

                CustomerBranch customerBranchToUpdate = (from row in entities.CustomerBranches
                                                         where row.ID == customerBranch.ID && row.IsDeleted == false
                                                         select row).FirstOrDefault();

                customerBranchToUpdate.Name = customerBranch.Name;
                //customerBranchToUpdate.Type = customerBranch.Type;
                //customerBranchToUpdate.LegalRelationShipOrStatus = customerBranch.LegalRelationShipOrStatus;
                customerBranchToUpdate.AddressLine1 = customerBranch.AddressLine1;
                customerBranchToUpdate.AddressLine2 = customerBranch.AddressLine2;
                customerBranchToUpdate.StateID = customerBranch.StateID;
                customerBranchToUpdate.CityID = customerBranch.CityID;
                //customerBranchToUpdate.Others = customerBranch.Others;
                //customerBranchToUpdate.Industry = customerBranch.Industry;
                //customerBranchToUpdate.ContactPerson = customerBranch.ContactPerson;
                //customerBranchToUpdate.Landline = customerBranch.Landline;
                //customerBranchToUpdate.Mobile = customerBranch.Mobile;
                customerBranchToUpdate.EmailID = customerBranch.EmailID;
                customerBranchToUpdate.PinCode = customerBranch.PinCode;
                //customerBranchToUpdate.Status = customerBranch.Status;
                //customerBranchToUpdate.LegalEntityTypeID = customerBranch.LegalEntityTypeID;//added by manisha
                //customerBranchToUpdate.ComType = customerBranch.ComType;
                //customerBranchToUpdate.AuditPR = customerBranch.AuditPR;

                entities.SaveChanges();
            }
        }
        
        public static bool AddCustomerBranchClientsLocationMapping(int customerID, int branchID)
        {
            try
            {
                string clientid = string.Empty;
                CustomerBranch customerBranch = CustomerBranchManagement.GetByID(branchID);
                if (string.IsNullOrEmpty(Convert.ToString(customerBranch.ParentID)))
                {
                    string branchname = RLCSManagement.GetRLCSBranchNameByID(customerBranch.ID, "E");
                    if (string.IsNullOrEmpty(branchname))
                    {
                        clientid = GetClientID(customerBranch.Name.Trim());
                        clientid += branchID;

                        if (!CheckClientID(clientid))
                        {
                            for (int i = 1; i <= 10; i++)
                            {
                                if (CheckClientID(clientid + i.ToString()))
                                {
                                    clientid = clientid + i.ToString();
                                    break;
                                }
                            }
                        }
                    }
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.AddCustomerBranchClientsLocationMapping(customerID, branchID, clientid);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string GetClientID(string clientName)
        {
            clientName = clientName.Replace(" ", "");
            StringBuilder result = new StringBuilder();
            try
            {
                foreach (char c in clientName)
                {
                    if (result.Length < 3)
                    {
                        if (Char.IsLetterOrDigit(c))
                            result.Append(c);
                    }
                }

                return "" + result.ToString().ToUpper();
            }
            catch (Exception ex)
            {
                return result.ToString();
            }
        }

        protected static bool CheckClientID(string clientid)
        {
            if (!String.IsNullOrEmpty(clientid))
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool clientExists = false;
                    bool entityExists = false;

                    string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                    rlcsAPIURL += "AventisIntegration/CheckClientIdExists?ClientId=" + clientid.Trim();

                    string responseData = RLCSAPIClasses.Invoke("GET", rlcsAPIURL, "");

                    if (responseData != null)
                    {
                        string data = responseData;
                        if (!string.IsNullOrWhiteSpace(data))
                            clientExists = Convert.ToBoolean(data);
                        else
                            clientExists = false;
                        if (clientExists)
                        {
                            return false;
                        }
                        else
                        {
                            entityExists = RLCS_Master_Management.Exists_CorporateID(clientid.Trim());
                            if (entityExists)
                            {
                                return false;
                            }
                            else
                            {
                                clientExists = RLCS_Master_Management.Exists_ClientID(clientid.Trim());
                                if (clientExists)
                                {
                                    return false;
                                }
                                else
                                {
                                    return true;
                                }

                            }
                        }
                    }
                    else
                        return false;
                }
            }

            return false;
        }


        public static List<int> GetAssignedLocationListLitigationRahulbyuser(int custID = -1, int UserID = -1, string role = "", int statusflag = -1)
        {
            List<int> LocationListnew = new List<int>();

            //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var LocationList = (from row in entities.LitigationEntitiesAssignments
                                    where row.UserID == UserID
                                    select (int)row.BranchID).ToList();


                List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                 join row1 in entities.tbl_LegalCaseInstance
                                                 on row.CaseInstanceID equals row1.ID
                                                 where row1.CustomerID == (int)custID
                                                 && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID || LocationList.Contains(row1.CustomerBranchID))
                                                 select (int)row1.ID).ToList();

                List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                   join row1 in entities.tbl_LegalNoticeInstance
                                                   on row.NoticeInstanceID equals row1.ID
                                                   where row1.CustomerID == (int)custID
                                                   && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID || LocationList.Contains(row1.CustomerBranchID))
                                                   select (int)row1.ID).ToList();

                //List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                //LocationListnew = LegalCasbranchlist.Select(a => a).ToList();


                if (statusflag == 1)
                {
                    LocationListnew = LegalNoticebranchlist.Select(a => a).ToList();
                }
                else if (statusflag == 2)
                {
                    LocationListnew = LegalCasebranchlist.Select(a => a).ToList();
                }
                else
                {
                    List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                    LocationListnew = LegalCasbranchlist.Select(a => a).ToList();
                }


            }


            return LocationListnew;
        }
    }
}
