﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class RoleManagementRisk
    {

        public static mst_Role GetByID(int roleID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var role = (from row in entities.mst_Role
                            where row.ID == roleID
                            select row).SingleOrDefault();

                return role;
            }
        }

        public static List<mst_Role> GetLimitedRoleAudit()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var roles = (from row in entities.mst_Role
                             where row.ID == 2 || row.ID == 7 || row.ID == 8                               
                             select row);

                return roles.ToList();
            }
        }
    }
}
