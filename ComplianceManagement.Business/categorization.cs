﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class categorization
    {
        public static object GetAll(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.mst_categorization
                             where row.IsActive == false                           
                             select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter));
                }

                return users.ToList();
            }
        }
        public static bool categoryExists(mst_categorization cat)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.mst_categorization
                             where row.Name.ToUpper().Equals(cat.Name.ToUpper()) && row.IsActive == false                         
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void CreatecategorizationMaster(mst_categorization cat)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.mst_categorization.Add(cat);
                entities.SaveChanges();
            }
        }
        public static void UpdateDepartmentMaster(mst_categorization cat)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                mst_categorization deptMasterToUpdate = (from row in entities.mst_categorization
                                                         where row.ID == cat.ID
                                                    && row.IsActive == false
                                                  
                                                 select row).FirstOrDefault();

                deptMasterToUpdate.Name = cat.Name;
                entities.SaveChanges();
            }
        }
        public static mst_categorization categorizationMasterGetByIDAudit(int categoryid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MsttMaster = (from row in entities.mst_categorization
                                           where row.ID == categoryid && row.IsActive == false

                                           select row).SingleOrDefault();

                return MsttMaster;
            }
        }
        public static void DeletecategorizationMaster(int categoryid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var categorizationMastertoDelete = (from row in entities.mst_categorization
                                                       where row.ID == categoryid
                                                    select row).FirstOrDefault();

                categorizationMastertoDelete.IsActive = true;
                entities.SaveChanges();
            }
        }
        public static List<mst_categorization> GetAllcategorizationList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DepartmentMasterList = (from row in entities.mst_categorization
                                            where row.IsActive == false                                            
                                            select row);

                
                return DepartmentMasterList.OrderBy(entry => entry.Name).ToList();
            }
        }
    }
}
