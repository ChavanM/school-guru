﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class LitigationTaskManagement
    {
        #region Advocate Bill
  

        public static bool CreateAdvocateBill(tbl_CaseAdvocateBill objRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                  
                    entities.tbl_CaseAdvocateBill.Add(objRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        #endregion

        #region Notice-Case-Task

        public static bool UpdateTask(tbl_TaskScheduleOn newRecord)
        {
            using (ComplianceDBEntities Entities = new ComplianceDBEntities())
            {
                var QueryResult = (from row in Entities.tbl_TaskScheduleOn
                                   where row.ID == newRecord.ID
                                   select row).FirstOrDefault();

                if (QueryResult != null)
                {
                    QueryResult.IsActive = newRecord.IsActive;
                    QueryResult.TaskType = newRecord.TaskType;
                    QueryResult.NoticeCaseInstanceID = newRecord.NoticeCaseInstanceID;
                    QueryResult.ScheduleOnDate = newRecord.ScheduleOnDate;
                    QueryResult.TaskTitle = newRecord.TaskTitle;
                    QueryResult.TaskDesc = newRecord.TaskDesc;
                    QueryResult.StatusID = newRecord.StatusID;
                    QueryResult.CustomerID = newRecord.CustomerID;
                    QueryResult.UpdatedBy = newRecord.CreatedBy;
                    QueryResult.CreatedByText = newRecord.CreatedByText;
                    QueryResult.UpdatedOn = DateTime.Now;
                    QueryResult.LinkCreatedOn = newRecord.LinkCreatedOn;
                    QueryResult.Remark = newRecord.Remark;
                    QueryResult.URLExpired = newRecord.URLExpired;
                    QueryResult.ExpOutcome = newRecord.ExpOutcome;
                    QueryResult.AssignTo = newRecord.AssignTo;
                    QueryResult.PriorityID = newRecord.PriorityID;

                    Entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static List<View_NoticeCaseTaskDetail> GetDashboardTaskList(List<View_NoticeCaseTaskDetail> masterRecords, int loggedInUserID, string loggedInUserRole, int roleID) /*, int taskStatus*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in masterRecords
                             where row.IsActive == true
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID)).ToList();
                //else
                //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                if (query.Count > 0)
                    query = query.OrderBy(entry => entry.StatusID)
                        .ThenBy(entry => entry.ScheduleOnDate)
                        .ThenBy(entry => entry.PriorityID).Take(5).ToList();

                return query;
            }
        }


        public static int GetAssignedTaskCount(List<View_NoticeCaseTaskDetail> masterRecords, int loggedInUserID, string loggedInUserRole, int roleID, int taskStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in masterRecords
                             where row.IsActive == true
                             select row).ToList();

                if (taskStatus != 0)
                {
                    if (taskStatus != 3)//3--Closed otherwise Open
                        query = query.Where(entry => entry.StatusID < 3).ToList();
                    else
                        query = query.Where(entry => entry.StatusID == taskStatus).ToList();
                }

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();
                //else
                //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                return query.Count();
            }
        }

        public static List<View_NoticeCaseTaskDetail> GetAssignedTaskList(int loggedInUserID, string loggedInUserRole, int roleID, int priorityID, int partyID, int deptID, int taskStatus, string taskType, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_NoticeCaseTaskDetail
                             where row.IsActive == true
                             && row.CustomerID == CustomerID
                             select row).ToList();

                if (taskStatus != -1)
                {
                    query = query.Where(entry => entry.StatusID == taskStatus).ToList();

                    //if (taskStatus != 3)//3--Closed otherwise Open
                    //    query = query.Where(entry => entry.StatusID < 3).ToList();
                    //else
                    //    query = query.Where(entry => entry.StatusID == taskStatus).ToList();
                }

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    //query = query.Where(entry => entry.AssignTo == loggedInUserID || entry.TaskCreatedBy == loggedInUserID).ToList();
                    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();
                //else
                //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                if (priorityID != -1)
                    query = query.Where(entry => entry.PriorityID == priorityID).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID == partyID).ToList();

                //if (deptID != -1)
                //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                if (taskType != "" && taskType != "B") //B--Both --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TaskType == taskType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TaskType)
                        .ThenBy(entry => entry.ScheduleOnDate)
                        .ThenBy(entry => entry.PriorityID).ToList();
                }
                //if (lstSelectedFileTags.Count > 0)
                //    //query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();


                return query.ToList();
            }
        }
        public static List<SP_Litigation_NoticeCaseTaskDetail_Result> GetAssignedTaskListforTag(int loggedInUserID, string loggedInUserRole, int roleID, int priorityID, int partyID, int deptID, int taskStatus, string taskType, long CustomerID, List<string> lstSelectedFileTags, string FileName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_Litigation_NoticeCaseTaskDetail(CustomerID)
                             select row).ToList();

                if (taskStatus != -1)
                {
                    query = query.Where(entry => entry.StatusID == taskStatus).ToList();

                    //if (taskStatus != 3)//3--Closed otherwise Open
                    //    query = query.Where(entry => entry.StatusID < 3).ToList();
                    //else
                    //    query = query.Where(entry => entry.StatusID == taskStatus).ToList();
                }

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    //query = query.Where(entry => entry.AssignTo == loggedInUserID || entry.TaskCreatedBy == loggedInUserID).ToList();
                    query = query.Where(entry => (entry.AssignTo == loggedInUserID)  || (entry.TaskCreatedBy == loggedInUserID)).ToList();
                //else
                //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                if (priorityID != -1)
                    query = query.Where(entry => entry.PriorityID == priorityID).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID == partyID).ToList();

                if (query !=null)
                    //query = query.Where(entry => entry.DepartmentID == deptID).ToList();
                    query = query.Where(entry => entry.NoticeCaseInstanceID == 0).ToList();

                if (taskType != "" && taskType != "B") //B--Both --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TaskType == taskType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TaskType)
                        .ThenBy(entry => entry.ScheduleOnDate)
                        .ThenBy(entry => entry.PriorityID).ToList();
                }
              
                if (!string.IsNullOrEmpty(FileName))
                {
                    query = (from row in query where row.DocumentName != null select row).ToList();
                    query = query.Where(Entry => Entry.DocumentName.ToLower().Contains(FileName.ToLower())).ToList();

                }
                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TaskType)
                        .ThenBy(entry => entry.ScheduleOnDate)
                        .ThenBy(entry => entry.PriorityID).ToList();
                }
              
                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.RefID,
                                 g.Priority,
                                 g.Status,
                                 g.AssignTo,
                                 //g.DocumentName,
                                 g.TaskType,
                                 g.TaskTitle,
                                 g.TaskDesc,
                                 g.ScheduleOnDate,
                                 g.PriorityID,
                                 g.AssignToName,
                                 g.TaskID
                                 //g.Filetag
                             } into GCS
                             select new SP_Litigation_NoticeCaseTaskDetail_Result()
                             {
                                 //FileName=GCS.Key.FileName,
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 RefID = GCS.Key.RefID,
                                 Priority=GCS.Key.Priority,
                                 Status=GCS.Key.Status,
                                 AssignTo=GCS.Key.AssignTo,
                                 //DocumentName=GCS.Key.DocumentName,
                                 TaskType = GCS.Key.TaskType,
                                 TaskTitle = GCS.Key.TaskTitle,
                                 TaskDesc = GCS.Key.TaskDesc,
                                 ScheduleOnDate=GCS.Key.ScheduleOnDate,
                                 PriorityID=GCS.Key.PriorityID,
                                 AssignToName=GCS.Key.AssignToName,
                                 TaskID=GCS.Key.TaskID
                                                             //Filetag = GCS.Key.Filetag
                             }).ToList();
                }
                ////return query;
               

                return query;
            }
        }
        public static List<View_NoticeCaseTaskDetail> GetTaskDetails(int noticeCaseInstanceID, string tasktype)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.View_NoticeCaseTaskDetail
                                   where row.NoticeCaseInstanceID == noticeCaseInstanceID
                                   && row.TaskType == tasktype
                                   && row.IsActive == true
                                   select row).ToList();

                if (queryResult.Count > 0)
                    queryResult = queryResult.OrderByDescending(entry => entry.Status)
                        .OrderBy(entry => entry.RefID)
                        .ThenByDescending(entry => entry.TaskUpdatedOn)
                        .ThenByDescending(entry => entry.ScheduleOnDate).ToList();


                return queryResult;
            }
        }

        public static List<View_NoticeCaseTaskDetail> GetResponseTaskDetails(long noticeCaseInstanceID, long refID, string tasktype)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.View_NoticeCaseTaskDetail
                                   where row.NoticeCaseInstanceID == noticeCaseInstanceID
                                   && row.RefID == refID
                                   && row.TaskType == tasktype
                                   && row.IsActive == true
                                   select row).ToList();

                if (queryResult.Count > 0)
                    queryResult = queryResult.OrderByDescending(entry => entry.Status)
                        .ThenByDescending(entry => entry.TaskUpdatedOn)
                        .ThenByDescending(entry => entry.TaskCreatedOn).ToList();


                return queryResult;
            }
        }

        public static tbl_TaskScheduleOn GetTaskDetailByTaskID(int noticeCaseInstanceID, int taskID, string taskType, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.tbl_TaskScheduleOn
                                   where row.NoticeCaseInstanceID == noticeCaseInstanceID
                                   && row.CustomerID == CustomerID
                                   && row.ID == taskID
                                   && row.TaskType == taskType
                                   && row.IsActive == true
                                   select row).FirstOrDefault();
                return queryResult;
            }
        }

        public static View_NoticeCaseTaskDetail GetNoticeCaseTaskViewDetails(int noticeCaseInstanceID, int taskID) /*, string tasktType*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.View_NoticeCaseTaskDetail
                                   where row.NoticeCaseInstanceID == noticeCaseInstanceID
                                   && row.TaskID == taskID
                                   //&& row.TaskType == tasktType
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }

        public static bool CreateTask(tbl_TaskScheduleOn objTaskRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objTaskRecord.CreatedOn = DateTime.Now;

                    entities.tbl_TaskScheduleOn.Add(objTaskRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistNoticeCaseTaskTitle(string taskTitle, string tasktType, long noticeCaseInstanceID, int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_TaskScheduleOn taskDetail = entities.tbl_TaskScheduleOn
                        .Where(x => x.TaskType == tasktType
                        && x.NoticeCaseInstanceID == noticeCaseInstanceID
                        && x.IsActive == true && x.CustomerID == CustomerID
                        && x.TaskTitle.Trim().ToUpper() == taskTitle.Trim().ToUpper()).FirstOrDefault();

                    if (taskDetail != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistCaseAdvocateBill(string taskTitle, string tasktType, long noticeCaseInstanceID, int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_TaskScheduleOn taskDetail = entities.tbl_TaskScheduleOn
                        .Where(x => x.TaskType == tasktType
                        && x.NoticeCaseInstanceID == noticeCaseInstanceID
                        && x.IsActive == true && x.CustomerID == CustomerID
                        && x.TaskTitle.Trim().ToUpper() == taskTitle.Trim().ToUpper()).FirstOrDefault();

                    if (taskDetail != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static string NoticeCaseTaskTitle(string tasktType, long noticeCaseInstanceID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (tasktType == "case")
                    {
                        var datatbl = entities.tbl_LegalCaseInstance
                             .Where(x => x.ID == noticeCaseInstanceID
                                ).FirstOrDefault();
                        if (datatbl != null)
                        {
                            return datatbl.CaseTitle;
                        }
                        else
                        {
                            return null;
                        }

                    }
                    else if (tasktType == "notice")
                    {

                        var datatbl = entities.tbl_LegalNoticeInstance
                             .Where(x => x.ID == noticeCaseInstanceID
                                ).FirstOrDefault();

                        if (datatbl != null)
                        {
                            return datatbl.NoticeTitle;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else { return ""; }
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public static bool UpdateTaskAccessURL(long taskID, tbl_TaskScheduleOn taskRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_TaskScheduleOn taskToUpdate = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID).FirstOrDefault();

                    if (taskToUpdate != null)
                    {
                        taskToUpdate.AccessURL = taskRecord.AccessURL;
                        taskToUpdate.UpdatedBy = taskRecord.UpdatedBy;
                        taskToUpdate.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateTaskStatus(long taskID, int statusID, int loggedInUserID, long CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_TaskScheduleOn taskToUpdate = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID && x.CustomerID == CustomerID).FirstOrDefault();

                    if (taskToUpdate != null)
                    {
                        taskToUpdate.StatusID = statusID;
                        taskToUpdate.UpdatedBy = loggedInUserID;
                        taskToUpdate.UpdatedOn = DateTime.Now;

                        if (statusID == 3)
                            taskToUpdate.URLExpired = true;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExpireTaskURL(long taskID, int loggedInUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_TaskScheduleOn taskToUpdate = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID).FirstOrDefault();

                    if (taskToUpdate != null)
                    {
                        taskToUpdate.URLExpired = true;
                        taskToUpdate.UpdatedBy = loggedInUserID;
                        taskToUpdate.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteTask(int taskID, int deletedByUserID, long CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_TaskScheduleOn ActionLogToDelete = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID && x.CustomerID == CustomerID).FirstOrDefault();

                    if (ActionLogToDelete != null)
                    {
                        ActionLogToDelete.IsActive = false;
                        ActionLogToDelete.DeletedBy = deletedByUserID;
                        ActionLogToDelete.DeletedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Task-Response
        public static List<tbl_TaskResponse> GetTaskResponseDetails(int taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.tbl_TaskResponse
                                   where row.TaskID == taskID
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;
            }
        }

        public static long CreateTaskResponseLog(tbl_TaskResponse objRespRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objRespRecord.CreatedOn = DateTime.Now;

                    entities.tbl_TaskResponse.Add(objRespRecord);
                    entities.SaveChanges();

                    return objRespRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool DeleteTaskResponseLog(int taskResponseID, int deletedByUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<tbl_LitigationFileData> ResponseRelatedDocToDelete = entities.tbl_LitigationFileData
                        .Where(x => x.DocTypeInstanceID == taskResponseID && x.DocType.Trim() == "T").ToList();

                    if (ResponseRelatedDocToDelete.Count > 0)
                    {
                        ResponseRelatedDocToDelete.ForEach(entry => entry.IsDeleted = true);
                        ResponseRelatedDocToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        ResponseRelatedDocToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    tbl_TaskResponse taskResponseLogToDelete = entities.tbl_TaskResponse.Where(x => x.ID == taskResponseID).FirstOrDefault();

                    if (taskResponseLogToDelete != null)
                    {
                        taskResponseLogToDelete.IsActive = false;
                        taskResponseLogToDelete.DeletedBy = deletedByUserID;
                        taskResponseLogToDelete.DeletedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<tbl_LitigationFileData> GetTaskDocuments(long taskID, long instanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == instanceID
                                   && row.DocTypeInstanceID == taskID
                                   && (row.DocType.Trim() == "CT" || row.DocType.Trim() == "NT" || row.DocType.Trim() == "T")
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }

        public static int GetTaskDocumentsCount(long taskID, long instanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == instanceID
                                   && row.DocTypeInstanceID == taskID
                                   && (row.DocType.Trim() == "CT" || row.DocType.Trim() == "NT" || row.DocType.Trim() == "T")
                                   && row.IsDeleted == false
                                   select row).ToList().Count;

                return queryResult;
            }
        }

        public static List<tbl_LitigationFileData> GetTaskResponseDocuments(long taskID, long responseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == taskID
                                   && row.DocTypeInstanceID == responseID
                                   && row.DocType.Trim() == "TR"
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }

        #endregion

        public static string GetTaskTypebyID(int TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var QueryResult = (from row in entities.tbl_TaskScheduleOn
                                   where row.ID == TaskID
                                   select row.TaskType).FirstOrDefault();
                return QueryResult;
            }
        }
    }
}
