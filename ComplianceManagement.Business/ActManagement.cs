﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Data;
using System.Data.Entity.Core.Objects;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ActManagement
    {
        public static List<ActView> GetAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.ActViews
                            select row);
                return acts.ToList();
            }
        }

    
        public static Lic_tbl_LicenseInstance GetByLICID(long LICID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Lic_tbl_LicenseInstance
                            where row.ID == LICID && row.IsDeleted == false
                            select row).FirstOrDefault();

                return data;
            }
        }
        public static List<object> GetActCategoryWiseCL(long? ActGroupID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ActGroupID != -1 )
                {
                    var acts = (from row in entities.Acts
                                where row.IsDeleted == false
                                && row.ActGroupId == ActGroupID
                                orderby row.Name ascending
                                select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return acts;
                }
                else
                {
                    var acts = (from row in entities.Acts
                                where row.IsDeleted == false
                                orderby row.Name ascending
                                select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return acts;
                }
            }
        }

        #region Rahul Changes 19 MAy 2020 on Act selection
        public static void CreateOrUpdateCompanyTypeMapping(List<Act_CompanyTypeMapping> mappingCompanyTypeMappingList, long actID, bool updateFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (updateFlag)
                {
                    List<Act_CompanyTypeMapping> mappedCompanyType = (from row in entities.Act_CompanyTypeMapping
                                                                      where row.ActID == actID
                                                                      select row).ToList();

                    foreach (Act_CompanyTypeMapping industry in mappedCompanyType)
                    {
                        entities.Act_CompanyTypeMapping.Remove(industry);
                        entities.SaveChanges();
                    }

                    //int lastID = (from row in entities.Act_CompanyTypeMapping
                    //              orderby row.ID descending
                    //              select row.ID).FirstOrDefault();
                }

                foreach (Act_CompanyTypeMapping mappingIndustry in mappingCompanyTypeMappingList)
                {
                    entities.Act_CompanyTypeMapping.Add(mappingIndustry);
                    entities.SaveChanges();
                }
            }
        }

        public static void CreateOrUpdateBusinessActivityMapping(List<Act_BusinessActivityMapping> mappingBusinessActivityMappingList, long actID, bool updateFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (updateFlag)
                {
                    List<Act_BusinessActivityMapping> mappedBusinessActivity = (from row in entities.Act_BusinessActivityMapping
                                                                                where row.ActID == actID
                                                                                select row).ToList();

                    foreach (Act_BusinessActivityMapping industry in mappedBusinessActivity)
                    {
                        entities.Act_BusinessActivityMapping.Remove(industry);
                        entities.SaveChanges();
                    }

                    //int lastID = (from row in entities.Act_BusinessActivityMapping
                    //              orderby row.ID descending
                    //              select row.ID).FirstOrDefault();
                }

                foreach (Act_BusinessActivityMapping mappingIndustry in mappingBusinessActivityMappingList)
                {
                    entities.Act_BusinessActivityMapping.Add(mappingIndustry);
                    entities.SaveChanges();
                }
            }
        }

        public static void CreateOrUpdateActApplicabilityMapping(List<Act_ActApplicabilityMapping> mappingActApplicabilityMappingList, long actID, bool updateFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (updateFlag)
                {
                    List<Act_ActApplicabilityMapping> mappedActApplicability = (from row in entities.Act_ActApplicabilityMapping
                                                                                where row.ActID == actID
                                                                                select row).ToList();

                    foreach (Act_ActApplicabilityMapping industry in mappedActApplicability)
                    {
                        entities.Act_ActApplicabilityMapping.Remove(industry);
                        entities.SaveChanges();
                    }

                    //int lastID = (from row in entities.Act_ActApplicabilityMapping
                    //              orderby row.ID descending
                    //              select row.ID).FirstOrDefault();
                }

                foreach (Act_ActApplicabilityMapping mappingIndustry in mappingActApplicabilityMappingList)
                {
                    entities.Act_ActApplicabilityMapping.Add(mappingIndustry);
                    entities.SaveChanges();
                }
            }
        }

        public static List<int> GetActCompanyTypeMappedID(long actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.Act_CompanyTypeMapping
                                         where row.ActID == actID
                                         select row.TypeID).ToList();

                return IndustryMappedIDs;
            }

        }

        public static List<int> GetActBusinessActivityMappedID(long actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.Act_BusinessActivityMapping
                                         where row.ActID == actID
                                         select row.BATypeID).ToList();

                return IndustryMappedIDs;
            }

        }

        public static List<int> GetActApplicabilityMappedID(long actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.Act_ActApplicabilityMapping
                                         where row.ActID == actID
                                         select row.Act_ApplicabilityID).ToList();

                return IndustryMappedIDs;
            }

        }


        //compliance

        public static void CreateOrUpdateCompliance_CompanyTypeMapping(List<Compliance_CompanyTypeMapping> mappingCompanyTypeMappingList, long complianceID, bool updateFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (updateFlag)
                {
                    List<Compliance_CompanyTypeMapping> mappedCompanyType = (from row in entities.Compliance_CompanyTypeMapping
                                                                             where row.ComplianceID == complianceID
                                                                             select row).ToList();

                    foreach (Compliance_CompanyTypeMapping industry in mappedCompanyType)
                    {
                        entities.Compliance_CompanyTypeMapping.Remove(industry);
                        entities.SaveChanges();
                    }
                }

                foreach (Compliance_CompanyTypeMapping mappingIndustry in mappingCompanyTypeMappingList)
                {
                    entities.Compliance_CompanyTypeMapping.Add(mappingIndustry);
                    entities.SaveChanges();
                }
            }
        }

        public static void CreateOrUpdateCompliance_BusinessActivityMappingMapping(List<Compliance_BusinessActivityMapping> mappingBusinessActivityMappingList, long complianceID, bool updateFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (updateFlag)
                {
                    List<Compliance_BusinessActivityMapping> mappedBusinessActivity = (from row in entities.Compliance_BusinessActivityMapping
                                                                                       where row.ComplianceID == complianceID
                                                                                       select row).ToList();

                    foreach (Compliance_BusinessActivityMapping industry in mappedBusinessActivity)
                    {
                        entities.Compliance_BusinessActivityMapping.Remove(industry);
                        entities.SaveChanges();
                    }

                }

                foreach (Compliance_BusinessActivityMapping mappingIndustry in mappingBusinessActivityMappingList)
                {
                    entities.Compliance_BusinessActivityMapping.Add(mappingIndustry);
                    entities.SaveChanges();
                }
            }
        }

        public static void CreateOrUpdateCompliance_ActApplicabilityMappingMapping(List<Compliance_ActApplicabilityMapping> mappingActApplicabilityMappingList, long complianceID, bool updateFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (updateFlag)
                {
                    List<Compliance_ActApplicabilityMapping> mappedActApplicability = (from row in entities.Compliance_ActApplicabilityMapping
                                                                                       where row.ComplianceID == complianceID
                                                                                       select row).ToList();

                    foreach (Compliance_ActApplicabilityMapping industry in mappedActApplicability)
                    {
                        entities.Compliance_ActApplicabilityMapping.Remove(industry);
                        entities.SaveChanges();
                    }

                }

                foreach (Compliance_ActApplicabilityMapping mappingIndustry in mappingActApplicabilityMappingList)
                {
                    entities.Compliance_ActApplicabilityMapping.Add(mappingIndustry);
                    entities.SaveChanges();
                }
            }
        }



        public static List<int> GetComplianceCompanyTypeMappedID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.Compliance_CompanyTypeMapping
                                         where row.ComplianceID == complianceID
                                         select row.TypeID).ToList();

                return IndustryMappedIDs;
            }

        }

        public static List<int> GetComplianceBusinessActivityMappedID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.Compliance_BusinessActivityMapping
                                         where row.ComplianceID == complianceID
                                         select row.BATypeID).ToList();

                return IndustryMappedIDs;
            }

        }
        public static List<int> GetSecretrialTagMappedID(long ComplianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var SecretrialtagMappedIDs = (from row in entities.Compliance_SecretarialTagMapping
                                              where row.ComplianceID == ComplianceId && row.IsActive == true
                                              select row.TagID).ToList();

                return SecretrialtagMappedIDs;
            }
        }
        public static List<int> GetComplianceApplicabilityMappedID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.Compliance_ActApplicabilityMapping
                                         where row.ComplianceID == complianceID
                                         select row.Act_ApplicabilityID).ToList();

                return IndustryMappedIDs;
            }

        }
        #endregion
        public static List<Sp_Act_Document_Result> getFileNameID(int actID, long ID)
        {
            try
            {
                using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                {
                    var getfileName = (from row in dbcontext.Sp_Act_Document(actID)
                                       where row.ID == ID
                                       select row).ToList();

                    return getfileName;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static object GetAllActDocumentType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_Type
                             select new { row.ID, row.Name }).ToList();

                return query;
            }
        }
        public static object GetAllActDepartment()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_Department
                             where row.IsDeleted == false
                             select new { row.ID, row.Name }).ToList();

                return query;
            }
        }

        public static object GetAllMinistry()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Ministries
                             where row.IsDeleted == false
                             select new { row.ID, row.Name }).ToList();

                return query;
            }
        }

        public static object GetAllRegulator()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Regulators
                             where row.IsDeleted == false
                             select new { row.ID, row.Name }).ToList();

                return query;
            }
        }
        public static bool UpdateActFiledata(Act_Document act_Document)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var findActDoc = (from row in entities.Act_Document
                                      where row.IsActive == true && row.Act_ID == act_Document.Act_ID
                                       && row.FileName == act_Document.FileName
                                      select row).FirstOrDefault();
                    if (findActDoc != null)
                    {
                        findActDoc.FileName = act_Document.FileName;
                        findActDoc.FilePath = act_Document.FilePath;
                        findActDoc.Updatedby = act_Document.Updatedby;
                        findActDoc.UpdatedOn = act_Document.UpdatedOn;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        //findActDoc.FileName = act_Document.FileName;
                        //findActDoc.FilePath = act_Document.FilePath;
                        //findActDoc.Updatedby = act_Document.Updatedby;
                        //findActDoc.UpdatedOn = act_Document.UpdatedOn;
                        entities.Act_Document.Add(act_Document);
                        entities.SaveChanges();
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SaveActFiledata(Act_Document act_Document)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (act_Document != null)
                    {
                        entities.Act_Document.Add(act_Document);
                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static List<Act_Type> getActType()
        {
            try
            {
                using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                {
                    var getfileName = (from row in dbcontext.Act_Type
                                       select row).ToList();

                    return getfileName;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<Sp_Act_Document_Result> getActDocumentTypeWise(int actID,int DocTypeID)
        {
            try
            {
                using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                {
                    var getfileName = (from row in dbcontext.Sp_Act_Document(actID)
                                       where row.DocumentTypeID == DocTypeID
                                       select row).ToList();

                    return getfileName;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<Sp_Act_Document_Result> getActDocumentIDWise(int actID, int ID)
        {
            try
            {
                using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                {
                    var getfileName = (from row in dbcontext.Sp_Act_Document(actID)
                                       where row.ID == ID
                                       select row).ToList();

                    return getfileName;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<Sp_Act_Document_Result> getFileNamebyID(int actID)
        {
            try
            {
                using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                {
                    var getfileName = (from row in dbcontext.Sp_Act_Document(actID)
                                       orderby row.ID ascending
                                       select row).ToList();

                    return getfileName;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static Act_Document getFiledtls(int fileid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var getfilename = (from row in entities.Act_Document
                                       where row.Id == fileid
                                       && row.IsActive == true
                                       select row).FirstOrDefault();
                    return getfilename;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static long deleteFiledtls(int fileid,int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var delfiledata = (from row in entities.Act_Document
                                   where row.Id == fileid
                                  && row.IsActive == true
                                   select row).FirstOrDefault();
                if (delfiledata != null)
                {
                    delfiledata.IsActive = false;
                    delfiledata.Updatedby = userID;
                    delfiledata.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();

                    ReAssignVersion(delfiledata.Act_ID, fileid, userID, entities);

                    return delfiledata.Act_ID;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static List<object> GetAllNVPInternalActs(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.InternalActs
                            where row.IsDeleted == false
                            && row.CustomerID == CustomerID
                            orderby row.Name ascending
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return acts;
            }
        }
        public static void UpdateinternalAct(InternalAct act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                InternalAct actToUpdate = (from row in entities.InternalActs
                                           where row.ID == act.ID
                                           select row).FirstOrDefault();

                actToUpdate.Name = act.Name;
                actToUpdate.UpdatedBy = act.UpdatedBy;
                actToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }

        public static bool UpdateAllActMapping(string RLCS_ActID, string HActID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_Act_Mapping RLCS_Act_Mapping = (from row in entities.RLCS_Act_Mapping
                                                     where row.AM_ActID == RLCS_ActID
                                                     select row).FirstOrDefault();
                if (RLCS_Act_Mapping != null && RLCS_Act_Mapping.AVACOM_ActID == null)
                {
                    RLCS_Act_Mapping.AVACOM_ActID = HActID;
                    entities.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        public static bool UpdateAllSectionMapping(string ComplianceID, string HActID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_Section_Compliance_Mapping RLCS_Section_Compliance_Mapping = (from row in entities.RLCS_Section_Compliance_Mapping
                                                     where row.SM_SectionID == HActID
                                                     select row).FirstOrDefault();
                if (RLCS_Section_Compliance_Mapping != null && RLCS_Section_Compliance_Mapping.AVACOM_ComplianceID == null)
                {
                    RLCS_Section_Compliance_Mapping.AVACOM_ComplianceID = Convert.ToInt32(ComplianceID);
                    entities.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }

        }
        public static void CreateInternalAct(InternalAct act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                act.CreatedOn = DateTime.Now;
                act.UpdatedOn = DateTime.Now;
                act.IsDeleted = false;

                entities.InternalActs.Add(act);

                entities.SaveChanges();
            }
        }
        public static bool ExistsInternalAct(InternalAct act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalActs
                             where row.IsDeleted == false && row.Name.Equals(act.Name)
                             select row);

                if (act.ID > 0)
                {
                    query = query.Where(entry => entry.ID != act.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void DeleteInternalAct(int actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalAct actToDelete = (from row in entities.InternalActs
                                           where row.ID == actID
                                           select row).FirstOrDefault();

                entities.InternalActs.Remove(actToDelete);

                entities.SaveChanges();
            }
        }
        public static bool CheckInternalActMapped(long ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.InternalCompliances
                             join row1 in entities.InternalActs
                             on row.ActID equals row1.ID
                             where row.ActID == ActID && row.IsDeleted == false
                             select row1.ID).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        
        public static InternalAct GetByIDInternalActs(int actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var act = (from row in entities.InternalActs
                           where row.ID == actID && row.IsDeleted == false
                           select row).SingleOrDefault();

                return act;
            }
        }
        public static List<InternalAct> GetAllInternalActs()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.InternalActs
                            where row.IsDeleted == false
                            select row);
                return acts.ToList();
            }
        }
        public static List<object> GetAllAssignedInternalActs(int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<object> acts = new List<object>();
                acts = (from row in entities.InternalComplianceAssignedInstancesViews
                        where row.CustomerID == Customerid
                        select new { ID = row.ActID, Name = row.ActName }).OrderBy(entry => entry.Name).ToList<object>();

                acts = acts.Distinct().ToList();
                return acts;
            }
        }
        public static List<SP_GetInternalActsByUserID_Result> GetInternalActsByUserID(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.SP_GetInternalActsByUserID(UserID)
                            select row);
                return acts.ToList();
            }
        }

        public static List<RLCS_Act_Mapping> GetAllActsMapping(string Type)
        {
            using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
            {
                var acts = (from a in dbcontext.RLCS_Act_Mapping
                            select a).ToList();

                if (!string.IsNullOrEmpty(Type) && Type == "NotMapped")
                {
                     acts = (from a in dbcontext.RLCS_Act_Mapping.Where(a => a.AVACOM_ActID == null) select a).ToList();
                 
                }
                else if (!string.IsNullOrEmpty(Type) && Type == "Mapped")
                {
                      acts = (from a in dbcontext.RLCS_Act_Mapping.Where(a =>a.AVACOM_ActID != null) select a).ToList();
                   
                }
                return acts;

            }

        }
        public static List<RLCS_Section_Compliance_Mapping> GetALLSectionMapping()
        {
            using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
            {
                var acts = (from a in dbcontext.RLCS_Section_Compliance_Mapping
                            select a).ToList();
                
                return acts;

            }

        }
        public static List<ActView> GetAllActEventBased(int catagoryid, int typeId, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.ActViews
                            join row1 in entities.Compliances
                            on row.ID equals row1.ActID
                            where row1.IsDeleted == false && row1.EventFlag != null
                            select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    acts = acts.Where(entry => entry.Name.Contains(filter) || entry.ComplianceCategoryName.Contains(filter) || entry.ComplianceTypeName.Contains(filter) || entry.State.Contains(filter) || entry.City.Contains(filter));
                }

                if (catagoryid != -1)
                {
                    acts = acts.Where(entry => entry.ComplianceCategoryId == catagoryid);
                }

                if (typeId != -1)
                {
                    acts = acts.Where(entry => entry.ComplianceTypeId == typeId);
                }

                var ActList = acts.Distinct().ToList();

                return ActList.ToList();
            }
        }

        public static List<object> GetAllAssignedPeriods(long ActID,long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts
                            join row1 in entities.Compliances
                            on row.ID equals row1.ActID
                            join row2 in entities.ComplianceInstances
                            on row1.ID equals row2.ComplianceId
                            join row3 in entities.ComplianceAssignments
                            on row2.ID equals row3.ComplianceInstanceID
                            join row4 in entities.ComplianceScheduleOns
                            on row3.ComplianceInstanceID equals row4.ComplianceInstanceID
                            where row1.IsDeleted == false && row1.ActID == ActID
                            && row1.ID == ComplianceID 
                            && row1.EventFlag == null
                            && row4.IsUpcomingNotDeleted == true  && row4.IsActive == true
                            orderby row.Name ascending
                            select new { ID = row4.ForMonth, Name = row4.ForMonth }).Distinct().OrderBy(entry => entry.Name).ToList<object>();
                return acts;
            }
        }

        public static List<object> GetAllAssignedCompliances(long ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts
                            join row1 in entities.Compliances
                            on row.ID equals row1.ActID
                            join row2 in entities.ComplianceInstances
                            on row1.ID equals row2.ComplianceId
                            join row3 in entities.ComplianceAssignments
                            on row2.ID equals row3.ComplianceInstanceID
                            where row.IsDeleted == false && row1.IsDeleted == false && row1.ActID == ActID && row1.EventFlag == null
                            orderby row.Name ascending
                            select new { ID = row1.ID, Name = row1.ShortDescription }).Distinct().OrderBy(entry => entry.Name).ToList<object>();

                return acts;
            }
        }
        public static List<object> GetAllAssignedActs()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts
                            join row1 in entities.Compliances
                            on row.ID equals row1.ActID
                            join row2 in entities.ComplianceInstances
                            on row1.ID equals row2.ComplianceId
                            join row3 in entities.ComplianceAssignments
                            on row2.ID equals row3.ComplianceInstanceID
                            where row.IsDeleted == false && row1.EventFlag == null
                            && row1.IsDeleted==false && row1.Status == null
                            orderby row.Name ascending
                            select new { ID = row.ID, Name = row.Name }).Distinct().OrderBy(entry => entry.Name).ToList<object>();

                return acts;
            }
        }
        public static int GetInternalComplianceTypeIDByName(string ComplianceType, int? CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actID = (from row in entities.InternalComplianceTypes
                             where row.Name.ToUpper().Trim().Equals(ComplianceType.ToUpper().Trim()) && row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             select row.ID).FirstOrDefault();
                return actID;
            }
        }
        public static int GetInternalComplianceCategoryIDByName(string ComplianceCategory, int? CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actID = (from row in entities.InternalCompliancesCategories
                             where row.Name.ToUpper().Trim().Equals(ComplianceCategory.ToUpper().Trim()) 
                             && row.IsDeleted ==false
                             && row.CustomerID == CustomerID
                           
                             select row.ID).FirstOrDefault();
                return actID;
            }
        }
        public static object GetPerformerList(int userID, int userRoleID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == customerID && row.RoleID == 3
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false) && row.ScheduledOn <= nextOneMonth
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select new { ID = row.UserID, Name = row.User }).OrderBy(entry => entry.Name).Distinct().ToList<object>();
                return transactionsQuery;
            }
        }
        public static List<SP_GetActiveEventDropDown_Result> GetActiveEvents(int eventRoleID, int Customerid, int UserID, int userRoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Event = (from row in entities.SP_GetActiveEventDropDown(UserID, Customerid, userRoleID)
                             select row).ToList();
                return Event;
            }
        }

        public static List<SP_GetActiveEventNatureDropDown_Result> GetEventNature(int eventRoleID, int Customerid, int UserID, int EventID, int userRoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Event = (from row in entities.SP_GetActiveEventNatureDropDown(UserID, Customerid, EventID, userRoleID)
                             select row).ToList();
                return Event;
            }
        }

        public static int GetUserRoleID(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int RoleID = (from row in entities.Users
                              where row.ID == UserID
                              select row.RoleID).Single();

                return RoleID;
            }
        }


        public static List<SP_GetActsByUserID_Result> GetActsByUserID(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.SP_GetActsByUserID(UserID)
                            select row);
                return acts.ToList();
            }
        }

        public static List<SP_GetComplianceSubTypeID_Result> GetSubTypeByUserID(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.SP_GetComplianceSubTypeID(UserID)
                            select row);
                return acts.ToList();
            }
        }

        public static List<SP_GetComplianceSubTypeIDAll_Result> GetSubTypeByUserIDAll(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.SP_GetComplianceSubTypeIDAll(UserID)
                            select row);
                return acts.ToList();
            }
        }

        public static List<object> GetAllAssignedActs(int Customerid, int CategoryId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<object> acts = new List<object>();              

                if (CategoryId != -1)
                {
                    acts = (from row in entities.SP_GetAssignedActs(Customerid)
                            where row.ComplianceCategoryId == CategoryId
                            select new { ID = row.ActID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();                    
                }
                else
                {
                    acts = (from row in entities.SP_GetAssignedActs(Customerid)                           
                            select new { ID = row.ActID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                }
                acts = acts.ToList();
                return acts;
            }
        }

        public static List<object> GetAllAssignedActs_RLCS(int Customerid, int userID, int CategoryId, string ProfileID, List<long> blist,bool IsAvantis)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var details = (from row in entities.SP_RLCS_GetApplicableActs(Customerid, userID, ProfileID, IsAvantis)
                               select row).ToList();

                if (blist.Count > 0)
                {
                    details = details.Where(entry => blist.Contains(entry.BranchID)).ToList();
                }

                details = details.GroupBy(entity => entity.ACTID).Select(entity => entity.FirstOrDefault()).ToList();
                List<object> acts = new List<object>();
                if (CategoryId != -1)
                {
                    acts = (from row in details
                            where row.ComplianceCategoryId == CategoryId
                            select new { ID = row.ACTID, Name = row.ACTNAME }).OrderBy(entry => entry.Name).ToList<object>();

                    //acts = (from row in entities.SP_RLCS_GetApplicableActs(Customerid, userID, ProfileID)
                    //        where row.ComplianceCategoryId == CategoryId
                    //        select new { ID = row.ACTID, Name = row.ACTNAME }).OrderBy(entry => entry.Name).ToList<object>();                
                }
                else
                {
                    acts = (from row in details
                            select new { ID = row.ACTID, Name = row.ACTNAME }).OrderBy(entry => entry.Name).ToList<object>();

                    //acts = (from row in entities.SP_RLCS_GetApplicableActs(Customerid, userID, ProfileID)
                    //        select new { ID = row.ACTID, Name = row.ACTNAME }).OrderBy(entry => entry.Name).ToList<object>();                   
                }

                acts = acts.ToList();
                return acts;
            }
        }

        public static List<object> GetAllAssignedCategoryAll(int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.SP_GetAssignedCategory(Customerid,-1)                           
                            select new { ID = row.CategoryId, Name = row.CategoryName }).OrderBy(entry => entry.Name).ToList<object>();

                //var acts = (from row in entities.ComplianceAssignedInstancesViews
                //            join row1 in entities.ComplianceCategories
                //            on row.ComplianceCategoryId equals row1.ID
                //            where row.CustomerID == Customerid
                //            select new { ID = row1.ID, Name = row1.Name }).OrderBy(entry => entry.Name).ToList<object>();


                acts = acts.ToList();
                return acts;
            }
        }

        public static List<object> GetAllAssignedCategory(int Customerid, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var acts = (from row in entities.ComplianceAssignedInstancesViews
                //            join row1 in entities.ComplianceCategories
                //            on row.ComplianceCategoryId equals row1.ID
                //            where row.CustomerID == Customerid && row.UserID == UserID
                //            select new { ID = row1.ID, Name = row1.Name }).OrderBy(entry => entry.Name).ToList<object>();

                var acts = (from row in entities.SP_GetAssignedCategory(Customerid, UserID)
                            select new { ID = row.CategoryId, Name = row.CategoryName }).OrderBy(entry => entry.Name).ToList<object>();


                acts = acts.ToList();
                return acts;
            }
        }

        public static List<object> GetAllAssignedTypeAll(int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var acts = (from row in entities.ComplianceAssignedInstancesViews
                //            where row.CustomerID == Customerid
                //            select new { ID = row.ComplianceTypeID, Name = row.ComplianceTypeName }).OrderBy(entry => entry.Name).ToList<object>();
                //acts = acts.Distinct().ToList();

                var acts = (from row in entities.SP_GetAssignedTypes(Customerid,-1)
                            select new { ID = row.TypId, Name = row.TypeName }).OrderBy(entry => entry.Name).ToList<object>();


                acts = acts.ToList();

                return acts;
            }
        }

        public static List<object> GetAllAssignedType(int Customerid, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var acts = (from row in entities.ComplianceAssignedInstancesViews
                //            where row.CustomerID == Customerid && row.UserID == UserID
                //            select new { ID = row.ComplianceTypeID, Name = row.ComplianceTypeName }).OrderBy(entry => entry.Name).ToList<object>();
                //acts = acts.Distinct().ToList();

                var acts = (from row in entities.SP_GetAssignedTypes(Customerid, UserID)
                            select new { ID = row.TypId, Name = row.TypeName }).OrderBy(entry => entry.Name).ToList<object>();

                acts = acts.ToList();
                return acts;
            }
        }


        //Internal 

        public static List<object> GetAllAssignedInternalCategoryAll(int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.InternalComplianceAssignedInstancesViews
                            join row1 in entities.InternalCompliancesCategories
                            on row.InternalComplianceCategoryID equals row1.ID
                            where row.CustomerID == Customerid
                            select new { ID = row1.ID, Name = row1.Name }).OrderBy(entry => entry.Name).ToList<object>();
                acts = acts.Distinct().ToList();
                return acts;
            }
        }

        public static List<object> GetAllAssignedInternalCategory(int Customerid, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.InternalComplianceAssignedInstancesViews
                            join row1 in entities.InternalCompliancesCategories
                            on row.InternalComplianceCategoryID equals row1.ID
                            where row.CustomerID == Customerid && row.UserID == UserID
                            select new { ID = row1.ID, Name = row1.Name }).OrderBy(entry => entry.Name).ToList<object>();
                acts = acts.Distinct().ToList();
                return acts;
            }
        }

        public static List<object> GetAllAssignedInternalTypeAll(int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.InternalComplianceAssignedInstancesViews
                            join row1 in entities.InternalComplianceTypes
                            on row.InternalComplianceTypeID equals row1.ID
                            where row.CustomerID == Customerid
                            && row1.IsDeleted == false
                            select new { ID = row1.ID, Name = row1.Name }).OrderBy(entry => entry.Name).ToList<object>();
                acts = acts.Distinct().ToList();
                return acts;
            }
        }


        public static List<object> GetAllAssignedInternalType(int Customerid, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.InternalComplianceAssignedInstancesViews
                            join row1 in entities.InternalComplianceTypes
                            on row.InternalComplianceTypeID equals row1.ID
                            where row.CustomerID == Customerid && row.UserID == UserID
                            && row1.IsDeleted == false
                            select new { ID = row1.ID, Name = row1.Name }).OrderBy(entry => entry.Name).ToList<object>();
                acts = acts.Distinct().ToList();
                return acts;
            }
        }
    
        public static List<ActView> GetAll(int catagoryid, int typeId, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.ActViews
                            select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    acts = acts.Where(entry => entry.Name.Contains(filter) || entry.ComplianceCategoryName.Contains(filter) || entry.ComplianceTypeName.Contains(filter) || entry.State.Contains(filter) || entry.City.Contains(filter));
                }

                if (catagoryid != -1)
                {
                    acts = acts.Where(entry => entry.ComplianceCategoryId == catagoryid);
                }

                if (typeId != -1)
                {
                    acts = acts.Where(entry => entry.ComplianceTypeId == typeId);
                }
                return acts.ToList();
            }
        }
        public static List<ActView> GetAllActs(int catagoryid, int typeId, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.ActViews
                            select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    acts = acts.Where(entry => entry.Name.Contains(filter) || entry.ComplianceCategoryName.Contains(filter) || entry.ComplianceTypeName.Contains(filter) || entry.State.Contains(filter) || entry.City.Contains(filter));
                }

                if (catagoryid != -1 && catagoryid != 0)
                {
                    acts = acts.Where(entry => entry.ComplianceCategoryId == catagoryid);
                }

                if (typeId != -1 && typeId != 0)
                {
                    acts = acts.Where(entry => entry.ComplianceTypeId == typeId);
                }
                return acts.ToList();
            }
        }
        public static List<object> GetAllNVP()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts
                            where row.IsDeleted == false
                            orderby row.Name ascending
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return acts;
            }
        }

        public static List<object> GetActCategoryWise(int CategoryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts
                            where row.IsDeleted == false
                            && row.ComplianceCategoryId == CategoryID
                            orderby row.Name ascending
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return acts;
            }
        }
        public static List<object> GetScheme()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Schemes
                            where row.IsActive == true
                            orderby row.Title ascending
                            select new { ID = row.ID, Name = row.Title }).OrderBy(entry => entry.Name).ToList<object>();

                return acts;
            }
        }
        public static List<long> GetResearchMappedDetails(List<long> ActIDs)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var raas = (from row in entities.Research_ActAssignment                          
                            where ActIDs.Contains(row.ActId)  && row.IsActive == false
                            select (long)row.UserId).ToList();
                return raas;
            }
        }
        public static List<long> GetSchemeMappedActIDs(int SchemeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Schemes
                            join row1 in entities.SchemeMappings on row.ID equals row1.SchemeID
                            where row.ID == SchemeID && row1.IsActive == true
                            select row1.ActID ).ToList();

                return acts;
            }
        }
        public DataTable GetAllActForDailyUpdate()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
               
                var acts = (from row in entities.Acts
                            where row.IsDeleted == false
                            orderby row.Name ascending
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Name", typeof(string));

                if (acts.Count > 0)
                {
                    foreach (var item in acts)
                    {
                        DataRow dr = table.NewRow();
                        dr["ID "] = item.ID;
                        dr["Name"] = item.Name;
                        table.Rows.Add(dr);
                    }
                }

                    DataRow dr1 = table.NewRow();
                    dr1["ID "] = -1;
                    dr1["Name"] = "< Other >";
                    table.Rows.Add(dr1);

                return table;
            }
        }
        public static Act GetByID(int actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var act = (from row in entities.Acts
                           where row.ID == actID && row.IsDeleted == false 
                           select row).SingleOrDefault();

                return act;
            }
        }

        public static ComplianceSubType GetByComplianceSubType(int ComplianceSubTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var act = (from row in entities.ComplianceSubTypes
                           where row.ID == ComplianceSubTypeID
                           select row).SingleOrDefault();

                return act;
            }
        }

        public static void Delete(int actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //Act actToDelete = new Act() { ID = actID };
                //entities.Acts.Attach(actToDelete);

                Act actToDelete = (from row in entities.Acts
                                   where row.ID == actID
                                   select row).FirstOrDefault();

                entities.Acts.Remove(actToDelete);

                entities.SaveChanges();
            }
        }

        public static bool Exists(Act act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Acts
                             where   row.IsDeleted == false && row.Name.Trim().ToUpper().Equals(act.Name.Trim().ToUpper())
                             select row);

                if (act.ID > 0)
                {
                    query = query.Where(entry => entry.ID != act.ID);
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static int Update(Act act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                Act actToUpdate = (from row in entities.Acts
                                   where row.ID == act.ID
                                   select row).FirstOrDefault();

                actToUpdate.Name = act.Name;
                actToUpdate.ShortForm = act.ShortForm;
                actToUpdate.Description = act.Description;
                actToUpdate.StateID = act.StateID;
                actToUpdate.CityID = act.CityID;
                actToUpdate.State = act.State;
                actToUpdate.City = act.City;
                actToUpdate.ComplianceCategoryId = act.ComplianceCategoryId;
                actToUpdate.ComplianceTypeId = act.ComplianceTypeId;
                actToUpdate.UpdatedOn = DateTime.Now;
                actToUpdate.StartDate = act.StartDate;
                actToUpdate.Act_DeptID = act.Act_DeptID;
                actToUpdate.RegulatorID = act.RegulatorID;
                actToUpdate.MinistryID = act.MinistryID;
                actToUpdate.duedatetype = act.duedatetype;
                actToUpdate.CountryID = act.CountryID;
                actToUpdate.ActGroupId = act.ActGroupId;
                actToUpdate.Actapplicabiltyrules = act.Actapplicabiltyrules;
                entities.SaveChanges();
                return act.ID;
            }
        }


        //public static void Update(Act act)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {

        //        Act actToUpdate = (from row in entities.Acts
        //                           where row.ID == act.ID
        //                           select row).FirstOrDefault();

        //        actToUpdate.Name = act.Name;
        //        actToUpdate.Description = act.Description;
        //        actToUpdate.StateID = act.StateID;
        //        actToUpdate.CityID = act.CityID;
        //        actToUpdate.State = act.State;
        //        actToUpdate.City = act.City;
        //        actToUpdate.ComplianceCategoryId = act.ComplianceCategoryId;
        //        actToUpdate.ComplianceTypeId = act.ComplianceTypeId;
        //        actToUpdate.UpdatedOn = DateTime.Now;
        //        entities.SaveChanges();
        //    }
        //}
        public static int Create(Act act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                act.CreatedOn = DateTime.Now;
                act.UpdatedOn = DateTime.Now;
                act.IsDeleted = false;

                entities.Acts.Add(act);

                entities.SaveChanges();
                if (act.ID > 0)
                {
                    return act.ID;
                }
                else
                {
                    return 0;
                }
            }
        }

        //public static void Create(Act act)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        act.CreatedOn = DateTime.Now;
        //        act.UpdatedOn = DateTime.Now;
        //        act.IsDeleted = false;

        //        entities.Acts.Add(act);

        //        entities.SaveChanges();
        //    }
        //}



        public static int CreateAct(Act_Litigation act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                act.CreatedOn = DateTime.Now;
                act.UpdatedOn = DateTime.Now;
                act.IsDeleted = false;

                entities.Act_Litigation.Add(act);

                entities.SaveChanges();

                return act.ID;
            }
        }

        // added by sudarshan for saving list of Act object
        public static void Create(List<Act> act)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                act.ForEach(entry =>
                {
                    entities.Acts.Add(entry);

                });
                entities.SaveChanges();
            }

        }

        public static bool Exists(string actName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Acts
                             where row.Name.Contains(actName)
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                //return query.Select(entry => true).First();
            }
        }

        //added by sudarshan for id Excel Utility
        public static int GetIdByName(string actName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actID = (from row in entities.Acts
                             where row.Name.ToUpper().Trim().Equals(actName.ToUpper().Trim())
                             select row.ID).FirstOrDefault();
                return actID;
            }
        }

        public static List<ComplianceAssignedInstancesView> GetActDetailsByText(String TextToSearch, int CustID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ActDetails = (from row in entities.ComplianceAssignedInstancesViews
                                  where row.CustomerID == CustID
                                  && row.Name.ToLower().Contains(TextToSearch)
                                  select row)
                                  .GroupBy(g => g.ActID)
                                  .Select(g => g.FirstOrDefault()).ToList();

                return ActDetails;
            }
        }

        //Added By Amita as on 17 JAN 2019
        public static string GetFileVersion(int actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string maxVersion = "1.0";

                maxVersion = (from row in entities.Act_Document
                              where row.Act_ID == actID
                              && row.IsActive == true
                              orderby row.Id descending
                              select row.Version).FirstOrDefault();

                return maxVersion;
            }
        }

        //Added By Amita as on 17 JAN 2019
        public static void ReAssignVersion(long actID, int fileID,int userID, ComplianceDBEntities entities)
        {
            List<Act_Document> actDocumentList = new List<Act_Document>();
            string version = string.Empty;

            actDocumentList = (from row in entities.Act_Document
                               where row.Act_ID == actID
                               && row.IsActive == true
                               orderby row.Id ascending
                               select row).ToList();

            if(fileID > 0 && actDocumentList.Count > 0)
            {
                int count = 1;

                foreach (Act_Document doc in actDocumentList)
                {
                    doc.Version = Convert.ToString(count) + ".0";
                    doc.Updatedby = userID;
                    doc.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();

                    count++;
                }
            }

        }
        
        public static bool ExistsCompliancesForAct(int actID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Compliances
                             where row.ActID == actID
                             select row.ID).Distinct().ToList();

                if (query != null && query.Count() > 0)
                    return true;
                else
                    return false;
            }
        }

        //Added by Amita as on 24JAN2019
        public static void CreateOrUpdateActIndustryMapping(List<Act_IndustryMapping> mappingInustries,long actID,bool updateFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (updateFlag)
                {
                    List<Act_IndustryMapping> mappedIndustrys = (from row in entities.Act_IndustryMapping
                                                                 where row.ActID == actID
                                                                 select row).ToList();

                    foreach (Act_IndustryMapping industry in mappedIndustrys)
                    {
                        entities.Act_IndustryMapping.Remove(industry);
                        entities.SaveChanges();
                    }

                    int lastID = (from row in entities.Act_IndustryMapping
                                  orderby row.ID descending
                                  select row.ID).FirstOrDefault();                    
                }

                foreach(Act_IndustryMapping mappingIndustry in mappingInustries)
                {
                    entities.Act_IndustryMapping.Add(mappingIndustry);
                    entities.SaveChanges();
                }
            }
        }

        public static void CreateOrUpdateActSubIndustryMapping(List<Act_SubIndustryMapping> mappingSubInustries, long actID, bool updateFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (updateFlag)
                {
                    List<Act_SubIndustryMapping> mappedSubIndustrys = (from row in entities.Act_SubIndustryMapping
                                                                 where row.ActID == actID                                                                
                                                                 select row).ToList();

                    foreach (Act_SubIndustryMapping Subindustry in mappedSubIndustrys)
                    {
                        entities.Act_SubIndustryMapping.Remove(Subindustry);
                        entities.SaveChanges();
                    }

                    int lastID = (from row in entities.Act_SubIndustryMapping
                                  orderby row.ID descending
                                  select row.ID).FirstOrDefault();
                }

                foreach (Act_SubIndustryMapping mappingSubIndustry in mappingSubInustries)
                {
                    entities.Act_SubIndustryMapping.Add(mappingSubIndustry);
                    entities.SaveChanges();
                }
            }
        }

        public static List<long> GetSubIndustryMappedID(long industryID, long subIndustryid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var SubIndustryMappedIDs = (from row in entities.SubIndustries
                                            where row.IndustryId == industryID && row.ID==subIndustryid
                                            select row.ID).ToList();

                return SubIndustryMappedIDs;
            }

        }
        //Added by Amita as on 24JAN2019
        public static List<int> GetActIndustryMappedID(long actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.Act_IndustryMapping
                                         where row.ActID == actID 
                                         select row.IndustryID).ToList();

                return IndustryMappedIDs;
            }

        }

        public static List<int> GetActSubIndustryMappedID(long actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.Act_SubIndustryMapping
                                         where row.ActID == actID
                                         select row.SubIndustryID).ToList();

                return IndustryMappedIDs;
            }

        }

        public static List<RLCS_ActGroup_Mapping> GetRLCSActGroupAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actgroups = (from row in entities.RLCS_ActGroup_Mapping
                                 where row.RS_Status == "A"
                                 select row).ToList();

                return actgroups;
            }
        }
    }
}
