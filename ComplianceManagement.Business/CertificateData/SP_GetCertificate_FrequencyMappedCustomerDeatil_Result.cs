//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.CertificateData
{
    using System;
    
    public partial class SP_GetCertificate_FrequencyMappedCustomerDeatil_Result
    {
        public int ID { get; set; }
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CertificateFrequency { get; set; }
        public System.DateTime StartDate { get; set; }
        public int DueDate { get; set; }
        public Nullable<int> IsDeleted { get; set; }
    }
}
