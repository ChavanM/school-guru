﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{

    public class ComplianceExclude_Reassign
    {
        #region Internal
        public static List<Sp_InternalComplianceAssignedInstance_Result> Internal_GetAllAssignedInstances(string IsChecklist, long customerId, long userID, long branchID = -1, string filter = "")
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.Sp_InternalComplianceAssignedInstance(customerId, userID)
                                                select row).ToList();


              
                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID).ToList();
                }
                
                if (IsChecklist == "Y")
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.IComplianceType == 1).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    if (ComplianceManagement.CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.InternalComplianceID == a).ToList();
                    }
                    else
                    {
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.IShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }
                return complianceInstancesQuery.OrderByDescending(entry => entry.InternalComplianceInstanceID).ToList();

            }
        }    
        public static void Internal_DeleteComplianceAssignment(long UserID, List<Tuple<long, string>> ComplianceInstanceIdList, long Customerid,long CreatedBy)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ExcludeUserEmailSend> MasterEUESList = new List<ExcludeUserEmailSend>();
                foreach (var cil in ComplianceInstanceIdList)
                {
                    #region Task Assignmnet Internal Exclude                
                    var compliance = Internal_GetComplianceIDFromInstance(Convert.ToInt32(cil.Item1));
                    Internal_DeleteTaskAssignmentFromCompliance(compliance.InternalComplianceID, compliance.CustomerBranchID, Customerid, UserID);
                    #endregion

                    var assignmentData = (from row in entities.InternalComplianceAssignments
                                          where row.InternalComplianceInstanceID == cil.Item1
                                          select row).ToList();

                    if (assignmentData != null)
                    {
                        var perofrmerdata = assignmentData.Where(entry => entry.RoleID == 3).FirstOrDefault();
                        var reviewerrdata = assignmentData.Where(entry => entry.RoleID == 4).FirstOrDefault();

                        long PID = 0;
                        long RID = 0;
                        long CompID = 0;
                        long BranchID = 0;
                        if (perofrmerdata != null)
                        {
                            PID = perofrmerdata.UserID;
                            //this assign here in case of only performer or reviewer case
                            CompID = perofrmerdata.InternalComplianceInstance.InternalComplianceID;
                            BranchID = perofrmerdata.InternalComplianceInstance.CustomerBranchID;
                        }
                        if (reviewerrdata != null)
                        {
                            RID = reviewerrdata.UserID;
                            //this assign here in case of only performer or reviewer case
                            CompID = reviewerrdata.InternalComplianceInstance.InternalComplianceID;
                            BranchID = reviewerrdata.InternalComplianceInstance.CustomerBranchID;
                        }

                        ExcludeUserEmailSend RUES = new ExcludeUserEmailSend();
                        RUES.Prev_PID = PID;
                        RUES.Perv_RID = RID;
                        RUES.Createdby = CreatedBy;
                        RUES.Createdon = DateTime.Now;
                        RUES.IsSend = false;
                        RUES.ComplianceID = CompID;
                        RUES.BranchID = BranchID;
                        RUES.IsStatutory = "I";
                        MasterEUESList.Add(RUES);

                        var assignid = assignmentData.Select(entry => entry.ID).ToList();
                        var reminderData = (from row in entities.InternalComplianceReminders
                                            where assignid.Contains(row.ComplianceAssignmentID)
                                            select row).ToList();

                        reminderData.ForEach(entry1 => entities.InternalComplianceReminders.Remove(entry1));
                        assignmentData.ForEach(entry2 => entities.InternalComplianceAssignments.Remove(entry2));
                        entities.SaveChanges();
                    }
                }//foreach end
                if (MasterEUESList.Count > 0)
                {
                    InsertExcludeUserEmailSend(MasterEUESList);
                }
            }
        }
        public static void Internal_DeleteTaskAssignmentFromCompliance(long ComplianceID, long CustomerBranchID, long Customerid, long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var GetSubTaskCount = (from row in entities.Sp_TASK_Statutory_InternalComplianceAssignedInstance(Customerid, UserID)
                                       where row.ComplianceID == ComplianceID
                                       && row.CustomerBranchID == CustomerBranchID
                                       && row.TaskType==2
                                       select row.TaskInstanceID).Distinct().ToList();

                if (GetSubTaskCount.Count > 0)
                {
                    GetSubTaskCount.ForEach(TaskInstancesentry =>
                    {
                        var taskassignmentData = (from row in entities.TaskAssignments
                                                  where row.TaskInstanceID == TaskInstancesentry
                                                  select row).ToList();

                        var assigned = taskassignmentData.Select(entry => entry.ID).ToList();

                        var reminderData = (from row in entities.TaskReminders
                                            where assigned.Contains(row.TaskAssignmentID)
                                            select row).ToList();

                        reminderData.ForEach(entry => entities.TaskReminders.Remove(entry));

                        taskassignmentData.ForEach(entry => entities.TaskAssignments.Remove(entry));

                        entities.SaveChanges();
                    });
                }
            }
        }        
        public static void Internal_ReplaceUserForComplianceAssignment(long PerformerUserID, long ReviewerUserID,long CreatedBy, List<Tuple<long, string>> ComplianceInstanceIdList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceAssignment> ICAList = new List<InternalComplianceAssignment>();
                List<ReassignmentUserEmailSend> MasterRUESList = new List<ReassignmentUserEmailSend>();
                foreach (var cil in ComplianceInstanceIdList)
                {
                    var assignmentData = (from row in entities.InternalComplianceAssignments
                                          where row.InternalComplianceInstanceID == cil.Item1
                                          select row).ToList();

                    if (assignmentData != null)
                    {
                        InternalComplianceAssignment ICA = new InternalComplianceAssignment();
                        var perofrmerdata = assignmentData.Where(entry => entry.RoleID == 3).FirstOrDefault();
                        var reviewerrdata = assignmentData.Where(entry => entry.RoleID == 4).FirstOrDefault();
                        long newpid = 0;
                        long newrid = 0;
                        long PrvPID = 0;
                        long PrvRID = 0;
                        long CompID = 0;
                        long BranchID = 0;
                        if (perofrmerdata != null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = perofrmerdata.InternalComplianceInstance.InternalComplianceID;
                            BranchID = perofrmerdata.InternalComplianceInstance.CustomerBranchID;
                            PrvPID = perofrmerdata.UserID;
                            if (perofrmerdata.UserID == PerformerUserID || PerformerUserID == -1)
                            {
                                newpid = 0;
                            }
                            else
                            {
                                ICA.UserID = PerformerUserID;
                                ICA.RoleID = 3;
                                ICA.InternalComplianceInstanceID = cil.Item1;
                                newpid = PerformerUserID;
                            }
                        }
                        else if (PerformerUserID == -1)
                        {
                            newpid = 0;
                        }
                        else
                        {
                            ICA.UserID = PerformerUserID;
                            ICA.RoleID = 3;
                            ICA.InternalComplianceInstanceID = cil.Item1;
                            newpid = PerformerUserID;
                        }

                        if (reviewerrdata != null)
                        {                        
                            //this assign here in case of only performer or reviewer case
                            CompID = reviewerrdata.InternalComplianceInstance.InternalComplianceID;
                            BranchID = reviewerrdata.InternalComplianceInstance.CustomerBranchID;
                            PrvRID = reviewerrdata.UserID;
                            if (reviewerrdata.UserID == ReviewerUserID || ReviewerUserID == -1)
                            {
                                newrid = 0;
                            }
                            else
                            {
                                newrid = ReviewerUserID;
                                ICA.UserID = ReviewerUserID;
                                ICA.RoleID = 4;
                                ICA.InternalComplianceInstanceID = cil.Item1;
                            }
                        }
                        else if (ReviewerUserID == -1)
                        {
                            newrid = 0;
                        }
                        else
                        {
                            ICA.UserID = ReviewerUserID;
                            ICA.RoleID = 4;
                            ICA.InternalComplianceInstanceID = cil.Item1;
                            newrid = ReviewerUserID;
                        }
                        ReassignmentUserEmailSend RUES = new ReassignmentUserEmailSend();
                        RUES.Prev_PID = PrvPID;
                        RUES.Perv_RID = PrvRID;
                        RUES.New_PID = newpid;
                        RUES.New_RID = newrid;
                        RUES.Createdby = CreatedBy;
                        RUES.Createdon = DateTime.Now;
                        RUES.IsSend = false;
                        RUES.ComplianceID = CompID;
                        RUES.BranchID = BranchID;
                        RUES.IsStatutory = "I";
                        MasterRUESList.Add(RUES);
                        if (ICA != null  && ICA.UserID != 0)
                        {
                            ICAList.Add(ICA);
                        }
                        if (perofrmerdata != null)
                        {
                            if (PerformerUserID != -1 && PerformerUserID != 0)
                            {
                                perofrmerdata.UserID = PerformerUserID;
                            }
                        }
                        if (reviewerrdata != null)
                        {
                            if (ReviewerUserID != -1 && ReviewerUserID != 0)
                            {
                                reviewerrdata.UserID = ReviewerUserID;
                            }
                        }
                        entities.SaveChanges();
                    }
                }
                if (MasterRUESList.Count > 0)
                {
                    InsertReassignmentUserEmailSend(MasterRUESList);
                    if (ICAList.Count>0)
                    {
                        InsertInternalComplianceAssignment(ICAList);
                    }
                }
            }
        }
        public static InternalComplianceInstance Internal_GetComplianceIDFromInstance(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var internalCompilance = (from row in entities.InternalComplianceInstances
                                          where row.ID == ComplianceInstanceID
                                          select row).SingleOrDefault();

                return internalCompilance;
            }
        }
        public static bool InsertInternalComplianceAssignment(List<InternalComplianceAssignment> ICAlist)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ICAlist.ForEach(entry =>
                    {
                        var ca = (from row in entities.InternalComplianceAssignments
                                  where row.InternalComplianceInstanceID == entry.InternalComplianceInstanceID
                                  && row.RoleID == entry.RoleID
                                  select row).FirstOrDefault();
                        if (ca != null)
                        {

                        }
                        else
                        {
                            entities.InternalComplianceAssignments.Add(entry);
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static List<Sp_InternalComplianceAssignedInstanceNew_Result> Internal_GetAllAssignedInstancesNew(string IsChecklist, long customerId, long userID, long branchID = -1, string filter = "")
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.Sp_InternalComplianceAssignedInstanceNew(customerId, userID)
                                                select row).ToList();
                if (complianceInstancesQuery.Count > 0)
                {
                    if (branchID != -1)
                    {
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID).ToList();
                    }
                    if (IsChecklist == "Y")
                    {
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.IComplianceType == 1).ToList();
                    }

                    if (!string.IsNullOrEmpty(filter))
                    {
                        if (ComplianceManagement.CheckInt(filter))
                        {
                            int a = Convert.ToInt32(filter.ToUpper());
                            complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.InternalComplianceID == a).ToList();
                        }
                        else
                        {
                            complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.IShortDescription.ToUpper().Contains(filter.ToUpper()) || (entry.LicenseNo != null && entry.LicenseNo.ToUpper().Contains(filter.ToUpper()))).ToList();
                        }
                    }
                }
                return complianceInstancesQuery.OrderByDescending(entry => entry.InternalComplianceInstanceID).ToList();

            }
        }

        public static void Internal_ReplaceUserForComplianceAssignmentNew(long PerformerUserID, long ReviewerUserID, List<int>approverid, long CreatedBy, List<Tuple<long, string>> ComplianceInstanceIdList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceAssignment> ICAList = new List<InternalComplianceAssignment>();
                List<ReassignmentUserEmailSend> MasterRUESList = new List<ReassignmentUserEmailSend>();
                foreach (var cil in ComplianceInstanceIdList)
                {
                    var assignmentData = (from row in entities.InternalComplianceAssignments
                                          where row.InternalComplianceInstanceID == cil.Item1
                                          select row).ToList();

                    var approverdatalist = assignmentData.Where(entry => entry.RoleID == 6).ToList();
                    if(approverdatalist.Count>0 && approverid.Count > 0)
                    {
                        foreach (var item1 in approverdatalist)
                        {
                            DeleteInternalApproverdata(cil.Item1);
                        }
                    }

                    if (assignmentData != null)
                    {
                        InternalComplianceAssignment ICA = new InternalComplianceAssignment();
                        var perofrmerdata = assignmentData.Where(entry => entry.RoleID == 3).FirstOrDefault();
                        var reviewerrdata = assignmentData.Where(entry => entry.RoleID == 4).FirstOrDefault();
                        var approverdata = assignmentData.Where(entry => entry.RoleID == 6).FirstOrDefault();
                        long newpid = 0;
                        long newrid = 0;
                        //long newaid = 0;
                        long PrvPID = 0;
                        long PrvRID = 0;
                        //long PrvAID = 0;
                        long CompID = 0;
                        long BranchID = 0;
                       

                        foreach (var item in approverid)
                        {
                            
                                if (item != -1 && item != 0)
                                {
                                InternalComplianceAssignment CAApprover = new InternalComplianceAssignment();
                                CAApprover.UserID = item;
                                CAApprover.RoleID = 6;
                                CAApprover.InternalComplianceInstanceID = cil.Item1;
                                entities.InternalComplianceAssignments.Add(CAApprover);
                                entities.SaveChanges();
                            }
                            
                        }

                        if (perofrmerdata != null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = perofrmerdata.InternalComplianceInstance.InternalComplianceID;
                            BranchID = perofrmerdata.InternalComplianceInstance.CustomerBranchID;
                            PrvPID = perofrmerdata.UserID;
                            if (perofrmerdata.UserID == PerformerUserID || PerformerUserID == -1)
                            {
                                newpid = 0;
                            }
                            else
                            {
                                ICA.UserID = PerformerUserID;
                                ICA.RoleID = 3;
                                ICA.InternalComplianceInstanceID = cil.Item1;
                                newpid = PerformerUserID;
                            }
                        }
                        else if (PerformerUserID == -1)
                        {
                            newpid = 0;
                        }
                        else
                        {
                            ICA.UserID = PerformerUserID;
                            ICA.RoleID = 3;
                            ICA.InternalComplianceInstanceID = cil.Item1;
                            newpid = PerformerUserID;
                        }

                        if (reviewerrdata != null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = reviewerrdata.InternalComplianceInstance.InternalComplianceID;
                            BranchID = reviewerrdata.InternalComplianceInstance.CustomerBranchID;
                            PrvRID = reviewerrdata.UserID;
                            if (reviewerrdata.UserID == ReviewerUserID || ReviewerUserID == -1)
                            {
                                newrid = 0;
                            }
                            else
                            {
                                newrid = ReviewerUserID;
                                ICA.UserID = ReviewerUserID;
                                ICA.RoleID = 4;
                                ICA.InternalComplianceInstanceID = cil.Item1;
                            }
                        }
                        else if (ReviewerUserID == -1)
                        {
                            newrid = 0;
                        }
                        else
                        {
                            ICA.UserID = ReviewerUserID;
                            ICA.RoleID = 4;
                            ICA.InternalComplianceInstanceID = cil.Item1;
                            newrid = ReviewerUserID;
                        }
                       
                        ReassignmentUserEmailSend RUES = new ReassignmentUserEmailSend();
                        RUES.Prev_PID = PrvPID;
                        RUES.Perv_RID = PrvRID;
                        RUES.New_PID = newpid;
                        RUES.New_RID = newrid;
                        RUES.Createdby = CreatedBy;
                        RUES.Createdon = DateTime.Now;
                        RUES.IsSend = false;
                        RUES.ComplianceID = CompID;
                        RUES.BranchID = BranchID;
                        RUES.IsStatutory = "I";
                        MasterRUESList.Add(RUES);
                        if (ICA != null && ICA.UserID != 0)
                        {
                            ICAList.Add(ICA);
                        }
                        if (perofrmerdata != null)
                        {
                            if (PerformerUserID != -1 && PerformerUserID != 0)
                            {
                                perofrmerdata.UserID = PerformerUserID;
                            }
                        }
                        if (reviewerrdata != null)
                        {
                            if (ReviewerUserID != -1 && ReviewerUserID != 0)
                            {
                                reviewerrdata.UserID = ReviewerUserID;
                            }
                        }
                        entities.SaveChanges();
                        
                }
                if (MasterRUESList.Count > 0)
                {
                    InsertReassignmentUserEmailSend(MasterRUESList);
                    if (ICAList.Count > 0)
                    {
                        InsertInternalComplianceAssignment(ICAList);
                    }
                }
              }
            }
        }

        public static void InternalApprover_DeleteComplianceAssignment(List<Tuple<long, string>> InternalComplianceInstanceIdList, long Customerid, long CreatedBy)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ExcludeUserEmailSend> MasterEUESList = new List<ExcludeUserEmailSend>();
                foreach (var cil in InternalComplianceInstanceIdList)
                {
                    var assignmentData = (from row in entities.InternalComplianceAssignments
                                          where row.InternalComplianceInstanceID == cil.Item1
                                          && row.RoleID == 6
                                          select row).ToList();
                    if (assignmentData != null)
                    {
                        foreach (var item in assignmentData)
                        {
                            entities.InternalComplianceAssignments.Remove(item);
                            entities.SaveChanges();
                        }
                      
                    }
                }//foreach end
            }
        }

        #endregion

        #region Statutory
        public static List<Sp_ComplianceAssignedInstanceNew_Result> Statutory_GetAllAssignedInstancesReassginExcludeNew(string EventFlag, long customerId, long userID, string IsCheckList, long branchID = -1, string filter = "")
        {

            List<Sp_ComplianceAssignedInstanceNew_Result> complianceInstancesQuery = new List<Sp_ComplianceAssignedInstanceNew_Result>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (IsCheckList == "Yes")
                {
                    complianceInstancesQuery = (from row in entities.Sp_ComplianceAssignedInstanceNew(customerId, userID, "NA")
                                                where row.ComplianceType == 1
                                                select row).ToList();
                }
                else
                {
                    if (EventFlag == "Y")
                    {
                        complianceInstancesQuery = (from row in entities.Sp_ComplianceAssignedInstanceNew(customerId, userID, "EV")
                                                        //where row.ComplianceType != 1 && row.EventFlag == true
                                                    select row).ToList();
                    }
                    else
                    {
                        entities.Database.CommandTimeout = 360;
                        complianceInstancesQuery = (from row in entities.Sp_ComplianceAssignedInstanceNew(customerId, userID, "NA")
                                                    where row.ComplianceType != 1 && row.EventFlag == null
                                                    select row).ToList();
                    }
                }
                if (complianceInstancesQuery.Count > 0)
                {
                    if (branchID != -1)
                    {
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID).ToList();
                    }
                    if (!string.IsNullOrEmpty(filter))
                    {
                        if (ComplianceManagement.CheckInt(filter))
                        {
                            int a = Convert.ToInt32(filter.ToUpper());
                            complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ComplianceID == a).ToList();
                        }
                        else
                        {
                            //complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                            complianceInstancesQuery = complianceInstancesQuery.Where(entry =>
                              entry.ShortDescription.ToUpper().Trim().Contains(filter.ToUpper().Trim())
                              || entry.Branch.ToUpper().Trim().Contains(filter.ToUpper().Trim())
                              || (entry.Perofrmer != null && entry.Perofrmer.ToUpper().Trim().Contains(filter.ToUpper().Trim()))
                              || (entry.Reviewer != null && entry.Reviewer.ToUpper().Trim().Contains(filter.ToUpper().Trim()))
                              || (entry.Approver != null && entry.Approver.ToUpper().Trim().Contains(filter.ToUpper().Trim()))
                              || (entry.LicenseNo != null && entry.LicenseNo.ToUpper().Trim().Contains(filter.ToUpper().Trim()))
                                ).ToList();
                        }
                    }
                }
                return complianceInstancesQuery.OrderBy(entry => entry.ComplianceID).ToList();

            }
        }
        public static List<Sp_ComplianceAssignedInstance_Result> Statutory_GetAllAssignedInstancesReassginExclude(string EventFlag, long customerId, long userID, string IsCheckList, long branchID = -1, string filter = "")
        {

            List<Sp_ComplianceAssignedInstance_Result> complianceInstancesQuery = new List<Sp_ComplianceAssignedInstance_Result>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (IsCheckList == "Yes")
                {
                    complianceInstancesQuery = (from row in entities.Sp_ComplianceAssignedInstance(customerId, userID,"NA")
                                                where row.ComplianceType == 1
                                                select row).ToList();
                }
                else
                {
                    if (EventFlag == "Y")
                    {
                        complianceInstancesQuery = (from row in entities.Sp_ComplianceAssignedInstance(customerId, userID,"EV")
                                                    //where row.ComplianceType != 1 && row.EventFlag == true
                                                    select row).ToList();
                    }
                    else
                    {
                        complianceInstancesQuery = (from row in entities.Sp_ComplianceAssignedInstance(customerId, userID,"NA")
                                                    where row.ComplianceType != 1 && row.EventFlag == null
                                                    select row).ToList();
                    }
                }
                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    if (ComplianceManagement.CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ComplianceID == a).ToList();
                    }
                    else
                    {
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }

                return complianceInstancesQuery.OrderBy(entry => entry.ComplianceID).ToList();

            }
        }
        public static void Statutory_DeleteComplianceAssignment(long UserID, List<Tuple<long, string>> ComplianceInstanceIdList, long Customerid,long CreatedBy)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ExcludeUserEmailSend> MasterEUESList = new List<ExcludeUserEmailSend>();
                foreach (var cil in ComplianceInstanceIdList)
                {
                    #region Task Assignmnet Statutory Exclude
                    var compliance = Statutory_GetComplianceIDFromInstance(Convert.ToInt32(cil.Item1));
                    Statutory_DeleteTaskAssignmentFromCompliance(compliance.ComplianceId, compliance.CustomerBranchID, Customerid, UserID);
                    #endregion

                    var assignmentData = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == cil.Item1
                                          select row).ToList();
                    if (assignmentData != null)
                    {
                        var perofrmerdata = assignmentData.Where(entry => entry.RoleID == 3).FirstOrDefault();
                        var reviewerrdata = assignmentData.Where(entry => entry.RoleID == 4).FirstOrDefault();
                        long PID = 0;
                        long RID = 0;
                        long CompID = 0;
                        long BranchID = 0;
                        if (perofrmerdata !=null)
                        {
                            PID = perofrmerdata.UserID;
                            //this assign here in case of only performer or reviewer case
                            CompID = perofrmerdata.ComplianceInstance.ComplianceId;
                            BranchID = perofrmerdata.ComplianceInstance.CustomerBranchID;

                        }
                        if (reviewerrdata != null)
                        {
                            RID = reviewerrdata.UserID;
                            //this assign here in case of only performer or reviewer case
                            CompID = reviewerrdata.ComplianceInstance.ComplianceId;
                            BranchID = reviewerrdata.ComplianceInstance.CustomerBranchID;

                        }
                        ExcludeUserEmailSend RUES = new ExcludeUserEmailSend();
                        RUES.Prev_PID = PID;
                        RUES.Perv_RID = RID;                        
                        RUES.Createdby = CreatedBy;
                        RUES.Createdon = DateTime.Now;
                        RUES.IsSend = false;
                        RUES.ComplianceID = CompID;
                        RUES.BranchID = BranchID;
                        RUES.IsStatutory = "S";
                        MasterEUESList.Add(RUES);


                        var assignid = assignmentData.Select(A => A.ID).ToList();

                        var reminderData = (from row in entities.ComplianceReminders
                                            where assignid.Contains(row.ComplianceAssignmentID)
                                            select row).ToList();

                        reminderData.ForEach(entry => entities.ComplianceReminders.Remove(entry));

                        assignmentData.ForEach(entry1 => entities.ComplianceAssignments.Remove(entry1));

                        entities.SaveChanges();
                    }
                }//foreach end

                if (MasterEUESList.Count > 0)
                {
                    InsertExcludeUserEmailSend(MasterEUESList);
                }
            }
        }
        public static ComplianceInstance Statutory_GetComplianceIDFromInstance(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compilance = (from row in entities.ComplianceInstances
                                  where row.ID == ComplianceInstanceID
                                  select row).SingleOrDefault();

                return compilance;
            }
        }
        public static void Statutory_DeleteTaskAssignmentFromCompliance(long ComplianceID, long CustomerBranchID, long Customerid, long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                                    
                var GetSubTaskCount = (from row in entities.Sp_TASK_Statutory_InternalComplianceAssignedInstance(Customerid, UserID)
                                       where row.ComplianceID == ComplianceID
                                       && row.CustomerBranchID == CustomerBranchID
                                       && row.TaskType==1
                                       select row.TaskInstanceID).Distinct().ToList();

                if (GetSubTaskCount.Count > 0)
                {
                    GetSubTaskCount.ForEach(TaskInstancesentry =>
                    {
                        var taskassignmentData = (from row in entities.TaskAssignments
                                                  where row.TaskInstanceID == TaskInstancesentry
                                                  select row).ToList();

                        var assigned = taskassignmentData.Select(entry => entry.ID).ToList();

                        var reminderData = (from row in entities.TaskReminders
                                            where assigned.Contains(row.TaskAssignmentID)
                                            select row).ToList();

                        reminderData.ForEach(entry => entities.TaskReminders.Remove(entry));

                        taskassignmentData.ForEach(entry => entities.TaskAssignments.Remove(entry));

                        entities.SaveChanges();
                    });
                }
            }
        }
        public static void Statutory_ReplaceUserForComplianceAssignment(long PerformerUserID, long ReviewerUserID,long CreatedBy, List<Tuple<long, string>> ComplianceInstanceIdList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                List<ReassignmentUserEmailSend> MasterRUESList = new List<ReassignmentUserEmailSend>();
                List<ComplianceAssignment> CAList = new List<ComplianceAssignment>();
                foreach (var cil in ComplianceInstanceIdList)
                {
                    var assignmentData = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == cil.Item1
                                          select row).ToList();
                    
                    if (assignmentData != null)
                    {
                        ComplianceAssignment CA = new ComplianceAssignment();
                        var perofrmerdata = assignmentData.Where(entry => entry.RoleID == 3).FirstOrDefault();
                        var reviewerrdata = assignmentData.Where(entry => entry.RoleID == 4).FirstOrDefault();                        
                        long newpid = 0;
                        long newrid = 0;
                        long PrvPID = 0;
                        long PrvRID = 0;
                        long CompID = 0;
                        long BranchID = 0;
                        if (perofrmerdata != null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = perofrmerdata.ComplianceInstance.ComplianceId;
                            BranchID = perofrmerdata.ComplianceInstance.CustomerBranchID;
                            PrvPID = perofrmerdata.UserID;

                            if (PerformerUserID == -1)
                            {
                                newpid = 0;
                            }
                            else if (perofrmerdata.UserID == PerformerUserID)
                            {
                                newpid = 0;
                            }
                            else
                            {
                                newpid = PerformerUserID;
                                CA.UserID = PerformerUserID;
                                CA.RoleID = 3;
                                CA.ComplianceInstanceID = cil.Item1;
                            }
                        }
                        else if (PerformerUserID == -1)
                        {
                            newpid = 0;
                        }
                        else
                        {
                            CA.UserID = PerformerUserID;
                            CA.RoleID = 3;
                            CA.ComplianceInstanceID = cil.Item1;
                            newpid = PerformerUserID;
                        }

                        if (reviewerrdata != null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = reviewerrdata.ComplianceInstance.ComplianceId;
                            BranchID = reviewerrdata.ComplianceInstance.CustomerBranchID;

                            PrvRID = reviewerrdata.UserID;
                            if (ReviewerUserID == -1)
                            {
                                newrid = 0;
                            }
                            else if (reviewerrdata.UserID == ReviewerUserID)
                            {
                                newrid = 0;
                            }
                            else
                            {
                                newrid = ReviewerUserID;
                                CA.UserID = ReviewerUserID;
                                CA.RoleID = 4;
                                CA.ComplianceInstanceID = cil.Item1;
                            }
                        }
                        else if (ReviewerUserID == -1)
                        {
                            newrid = 0;
                        }
                        else
                        {
                            CA.UserID = ReviewerUserID;
                            CA.RoleID = 4;
                            CA.ComplianceInstanceID = cil.Item1;
                            newrid = ReviewerUserID;
                        }
                        
                        ReassignmentUserEmailSend RUES = new ReassignmentUserEmailSend();
                        RUES.Prev_PID = PrvPID;
                        RUES.Perv_RID = PrvRID;
                        RUES.New_PID = newpid;
                        RUES.New_RID = newrid;                       
                        RUES.Createdby = CreatedBy;
                        RUES.Createdon = DateTime.Now;
                        RUES.IsSend = false;
                        RUES.ComplianceID = CompID;
                        RUES.BranchID = BranchID;
                        RUES.IsStatutory = "S";
                        MasterRUESList.Add(RUES);
                        if (CA!=null  && CA.UserID != 0) 
                        {                            
                            CAList.Add(CA);
                        }
                        
                        if (perofrmerdata !=null)
                        {
                            if (PerformerUserID != -1 && PerformerUserID != 0)
                            {
                                perofrmerdata.UserID = PerformerUserID;
                            }
                        }
                        if (reviewerrdata !=null)
                        {
                            if (ReviewerUserID != -1 && ReviewerUserID != 0)
                            {
                                reviewerrdata.UserID = ReviewerUserID;
                            }
                        }                       
                        entities.SaveChanges();
                    }
                }
                if (MasterRUESList.Count > 0)
                {
                    InsertReassignmentUserEmailSend(MasterRUESList);
                    if (CAList.Count>0)
                    {
                        InsertComplianceAssignment(CAList);
                    }                    
                }
            }
        }
        public static bool InsertReassignmentUserEmailSend(List<ReassignmentUserEmailSend> RUESlist)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    RUESlist.ForEach(entry =>
                    {
                        entities.ReassignmentUserEmailSends.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool InsertComplianceAssignment(List<ComplianceAssignment> CAlist)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CAlist.ForEach(entry =>
                    {
                        var ca = (from row in entities.ComplianceAssignments
                                  where row.ComplianceInstanceID == entry.ComplianceInstanceID
                                  && row.RoleID == entry.RoleID
                                  select row).FirstOrDefault();
                        if (ca != null)
                        {

                        }
                        else
                        {
                            entities.ComplianceAssignments.Add(entry);
                        }                        
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool InsertExcludeUserEmailSend(List<ExcludeUserEmailSend> EUESlist)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    EUESlist.ForEach(entry =>
                    {
                        entities.ExcludeUserEmailSends.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static void Statutory_ReplaceUserForComplianceAssignmentNew(long PerformerUserID, long ReviewerUserID, List<int> ApproverUserID, long CreatedBy, List<Tuple<long, string>> ComplianceInstanceIdList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<ReassignmentUserEmailSend> MasterRUESList = new List<ReassignmentUserEmailSend>();
                List<ComplianceAssignment> CAList = new List<ComplianceAssignment>();
                foreach (var cil in ComplianceInstanceIdList)
                {
                    var assignmentData = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == cil.Item1
                                          select row).ToList();

                    var approverdatalist = assignmentData.Where(entry => entry.RoleID == 6).ToList();
                    if (approverdatalist.Count > 0 && ApproverUserID.Count>0)
                    {
                        foreach (var item1 in approverdatalist)
                        {
                            DeleteApproverdata(cil.Item1);
                        }
                    }

                    ComplianceAssignment CA = new ComplianceAssignment();
                    var perofrmerdata = assignmentData.Where(entry => entry.RoleID == 3).FirstOrDefault();
                    var reviewerrdata = assignmentData.Where(entry => entry.RoleID == 4).FirstOrDefault();
                    var approverdata = assignmentData.Where(entry => entry.RoleID == 6).FirstOrDefault();
                    long newpid = 0;
                    long newrid = 0;
                    //long newaid = 0;
                    long PrvPID = 0;
                    long PrvRID = 0;
                    //long PrvAID = 0;
                    long CompID = 0;
                    long BranchID = 0;
                   

                    foreach (var item in ApproverUserID)
                    {
                        if (item != -1 && item != 0)
                        {
                            ComplianceAssignment CAApprover = new ComplianceAssignment();
                            CAApprover.UserID = item;
                            CAApprover.RoleID = 6;
                            CAApprover.ComplianceInstanceID = cil.Item1;
                            entities.ComplianceAssignments.Add(CAApprover);
                            entities.SaveChanges();
                        }
                       

                    }


                    if (perofrmerdata != null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = perofrmerdata.ComplianceInstance.ComplianceId;
                            BranchID = perofrmerdata.ComplianceInstance.CustomerBranchID;
                            PrvPID = perofrmerdata.UserID;

                            if (PerformerUserID == -1)
                            {
                                newpid = 0;
                            }
                            else if (perofrmerdata.UserID == PerformerUserID)
                            {
                                newpid = 0;
                            }
                            else
                            {
                                newpid = PerformerUserID;
                                CA.UserID = PerformerUserID;
                                CA.RoleID = 3;
                                CA.ComplianceInstanceID = cil.Item1;
                            }
                        }
                        else if (PerformerUserID == -1)
                        {
                            newpid = 0;
                        }
                        else
                        {
                            CA.UserID = PerformerUserID;
                            CA.RoleID = 3;
                            CA.ComplianceInstanceID = cil.Item1;
                            newpid = PerformerUserID;
                        }

                        if (reviewerrdata != null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = reviewerrdata.ComplianceInstance.ComplianceId;
                            BranchID = reviewerrdata.ComplianceInstance.CustomerBranchID;

                            PrvRID = reviewerrdata.UserID;
                            if (ReviewerUserID == -1)
                            {
                                newrid = 0;
                            }
                            else if (reviewerrdata.UserID == ReviewerUserID)
                            {
                                newrid = 0;
                            }
                            else
                            {
                                newrid = ReviewerUserID;
                                CA.UserID = ReviewerUserID;
                                CA.RoleID = 4;
                                CA.ComplianceInstanceID = cil.Item1;
                            }
                        }
                        else if (ReviewerUserID == -1)
                        {
                            newrid = 0;
                        }
                        else
                        {
                            CA.UserID = ReviewerUserID;
                            CA.RoleID = 4;
                            CA.ComplianceInstanceID = cil.Item1;
                            newrid = ReviewerUserID;
                        }

                        ReassignmentUserEmailSend RUES = new ReassignmentUserEmailSend();
                        RUES.Prev_PID = PrvPID;
                        RUES.Perv_RID = PrvRID;
                        RUES.New_PID = newpid;
                        RUES.New_RID = newrid;
                        RUES.Createdby = CreatedBy;
                        RUES.Createdon = DateTime.Now;
                        RUES.IsSend = false;
                        RUES.ComplianceID = CompID;
                        RUES.BranchID = BranchID;
                        RUES.IsStatutory = "S";
                        MasterRUESList.Add(RUES);
                        if (CA != null && CA.UserID != 0)
                        {
                            CAList.Add(CA);
                        }

                        if (perofrmerdata != null)
                        {
                            if (PerformerUserID != -1 && PerformerUserID != 0)
                            {
                                perofrmerdata.UserID = PerformerUserID;
                              
                            }
                        }
                        if (reviewerrdata != null)
                        {
                            if (ReviewerUserID != -1 && ReviewerUserID != 0)
                            {
                                reviewerrdata.UserID = ReviewerUserID;
                               
                            }
                        }
                    

                   

                    entities.SaveChanges();
                   
                }
                if (MasterRUESList.Count > 0)
                {
                    InsertReassignmentUserEmailSend(MasterRUESList);
                    if (CAList.Count > 0)
                    {
                        InsertComplianceAssignment(CAList);
                    }
                }
            }
        }
        public static void DeleteTaskApproverdata(long Taskinstanceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var assignmentapproverData = (from row in entities.TaskAssignments
                                              where row.TaskInstanceID == Taskinstanceid
                                              && row.RoleID == 6
                                              select row).FirstOrDefault();
                if (assignmentapproverData != null)
                {
                    entities.TaskAssignments.Remove(assignmentapproverData);
                }
                entities.SaveChanges();
            }
        }
        public static void DeleteInternalApproverdata(long cominstanceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var assignmentapproverData = (from row in entities.InternalComplianceAssignments
                                              where row.InternalComplianceInstanceID == cominstanceid
                                              && row.RoleID == 6
                                              select row).FirstOrDefault();
                if (assignmentapproverData != null)
                {
                    entities.InternalComplianceAssignments.Remove(assignmentapproverData);
                }
                entities.SaveChanges();
            }
        }
        public static void DeleteApproverdata(long cominstanceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var assignmentapproverData = (from row in entities.ComplianceAssignments
                                              where row.ComplianceInstanceID == cominstanceid
                                              && row.RoleID == 6
                                              select row).ToList();
                if (assignmentapproverData!=null)
                {
                    foreach (var item in assignmentapproverData)
                    {
                        entities.ComplianceAssignments.Remove(item);
                        entities.SaveChanges();
                    }
                   
                }
                
            }
        }

        public static void StatutoryApprover_DeleteComplianceAssignment(List<Tuple<long, string>> ComplianceInstanceIdList, long Customerid, long CreatedBy)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ExcludeUserEmailSend> MasterEUESList = new List<ExcludeUserEmailSend>();
                foreach (var cil in ComplianceInstanceIdList)
                {

                    var assignmentData = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == cil.Item1
                                          && row.RoleID == 6
                                          select row).ToList();
                    if (assignmentData != null)
                    {

                       foreach (var item in assignmentData)
                       {
                            entities.ComplianceAssignments.Remove(item);
                            entities.SaveChanges();
                       
                       }
                    }
                }//foreach end
            }
        }

        #endregion

        #region Task
        public static List<Sp_TASK_Statutory_InternalComplianceAssignedInstance_Result> GetAllTaskAssignedInstancesReassginExcludeStatutory_Internal(long customerId, long userID, string IsStatutoryOrInternal, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.Sp_TASK_Statutory_InternalComplianceAssignedInstance(customerId, userID)
                                                select row).ToList();


                if (IsStatutoryOrInternal == "S")
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.TaskType == 1).ToList();
                }
                else
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.TaskType == 2).ToList();
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    if (ComplianceManagement.CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.TaskID == a).ToList();
                    }
                    else
                    {
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.TaskTitle.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }
                
                return complianceInstancesQuery.OrderByDescending(entry => entry.TaskInstanceID).ToList();

            }
        }
        public static long GetTaskIDFromInstance(long taskInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var task = (from row in entities.TaskInstances
                            where row.ID == taskInstanceID
                            select row.TaskId).SingleOrDefault();

                return task;
            }
        }        
        public static bool DeleteTaskAssignmentTask(long UserID, List<Tuple<long, int>> chkList, long customerid,long CreatedBy)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ExcludeUserEmailSend> MasterEUESList = new List<ExcludeUserEmailSend>();
                foreach (var cil in chkList)
                {
                    long taskid = GetTaskIDFromInstance(cil.Item1);
                    var GetSubTaskCount = (from row in entities.Sp_TASK_Statutory_InternalComplianceAssignedInstance(customerid, UserID)
                                           where row.ParentID == taskid                                            
                                           select row.MainTaskID).ToList();

                    if (GetSubTaskCount.Count > 1)
                    {
                        return false;
                    }
                    else
                    {
                        var assignmentData = (from row in entities.TaskAssignments
                                              where row.TaskInstanceID == cil.Item1
                                              select row).ToList();
                        if (assignmentData != null)
                        {
                            var perofrmerdata = assignmentData.Where(entry => entry.RoleID == 3).FirstOrDefault();
                            var reviewerrdata = assignmentData.Where(entry => entry.RoleID == 4).FirstOrDefault();
                            long PID = 0;
                            long RID = 0;
                            long CompID = 0;
                            long BranchID = 0;
                            if (perofrmerdata != null)
                            {
                                PID = perofrmerdata.UserID;
                                //this assign here in case of only performer or reviewer case
                                CompID = perofrmerdata.TaskInstance.TaskId;
                                BranchID = perofrmerdata.TaskInstance.CustomerBranchID;
                            }
                            if (reviewerrdata != null)
                            {
                                RID = reviewerrdata.UserID;
                                //this assign here in case of only performer or reviewer case
                                CompID = reviewerrdata.TaskInstance.TaskId;
                                BranchID = reviewerrdata.TaskInstance.CustomerBranchID;
                            }

                            ExcludeUserEmailSend RUES = new ExcludeUserEmailSend();
                            RUES.Prev_PID = PID;
                            RUES.Perv_RID = RID;
                            RUES.Createdby = CreatedBy;
                            RUES.Createdon = DateTime.Now;
                            RUES.IsSend = false;
                            RUES.ComplianceID = CompID;
                            RUES.BranchID = BranchID;
                            RUES.IsStatutory = "T";
                            MasterEUESList.Add(RUES);

                            var assignedID = assignmentData.Select(entry => entry.ID).ToList();
                            List<TaskReminder> reminderData = (from row in entities.TaskReminders
                                                               where assignedID.Contains(row.TaskAssignmentID)
                                                               select row).ToList();

                            reminderData.ForEach(entry => entities.TaskReminders.Remove(entry));
                            assignmentData.ForEach(entry => entities.TaskAssignments.Remove(entry));
                            entities.SaveChanges();
                        }
                    }
                }// foreach end
                
                if (MasterEUESList.Count > 0)
                {
                    InsertExcludeUserEmailSend(MasterEUESList);
                }
                return true;
            }
        }
        public static void ReassignTask(long PerformerUserID, long ReviewerUserID,long CreatedBy, List<Tuple<long, int>> chkList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<TaskAssignment> TAList = new List<TaskAssignment>();
                List<ReassignmentUserEmailSend> MasterRUESList = new List<ReassignmentUserEmailSend>();
                foreach (var cil in chkList)
                {
                    var assignmentData = (from row in entities.TaskAssignments
                                          where row.TaskInstanceID == cil.Item1                                          
                                          select row).ToList();

                    if (assignmentData != null)
                    {
                        TaskAssignment TA = new TaskAssignment();
                        var perofrmerdata = assignmentData.Where(entry => entry.RoleID == 3).FirstOrDefault();
                        var reviewerrdata = assignmentData.Where(entry => entry.RoleID == 4).FirstOrDefault();
                        long newpid = 0;
                        long newrid = 0;
                        long PrvPID = 0;
                        long PrvRID = 0;
                        long CompID = 0;
                        long BranchID = 0;

                        if (perofrmerdata !=null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = perofrmerdata.TaskInstance.TaskId;
                            BranchID = perofrmerdata.TaskInstance.CustomerBranchID;
                            PrvPID = perofrmerdata.UserID;
                            if (PerformerUserID == -1)
                            {
                                newpid = 0;
                            }
                            else if (perofrmerdata.UserID == PerformerUserID)
                            {
                                newpid = 0;
                            }
                            else
                            {
                                TA.UserID = PerformerUserID;
                                TA.RoleID = 3;
                                TA.TaskInstanceID = cil.Item1;
                                newpid = PerformerUserID;
                            }
                        }
                        else if (PerformerUserID == -1)
                        {
                            newpid = 0;
                        }
                        else
                        {
                            newpid = PerformerUserID;
                            TA.UserID = PerformerUserID;
                            TA.RoleID = 3;
                            TA.TaskInstanceID = cil.Item1;
                        }

                    
                        if (reviewerrdata != null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = reviewerrdata.TaskInstance.TaskId;
                            BranchID = reviewerrdata.TaskInstance.CustomerBranchID;
                            PrvRID = reviewerrdata.UserID;
                            if (ReviewerUserID == -1)
                            {
                                newrid = 0;
                            }
                            else if (reviewerrdata.UserID == ReviewerUserID)
                            {
                                newrid = 0;
                            }
                            else
                            {
                                newrid = ReviewerUserID;
                                TA.UserID = ReviewerUserID;
                                TA.RoleID = 4;
                                TA.TaskInstanceID = cil.Item1;
                            }
                        }
                        else if (ReviewerUserID == -1)
                        {
                            newrid = 0;
                        }
                        else
                        {
                            newrid = ReviewerUserID;
                            TA.UserID = ReviewerUserID;
                            TA.RoleID = 4;
                            TA.TaskInstanceID = cil.Item1;
                        }
                        ReassignmentUserEmailSend RUES = new ReassignmentUserEmailSend();
                        RUES.Prev_PID = PrvPID;
                        RUES.Perv_RID = PrvRID;
                        RUES.New_PID = newpid;
                        RUES.New_RID = newrid;
                        RUES.Createdby = CreatedBy;
                        RUES.Createdon = DateTime.Now;
                        RUES.IsSend = false;
                        RUES.ComplianceID = CompID;
                        RUES.BranchID = BranchID;
                        RUES.IsStatutory = "T";
                        MasterRUESList.Add(RUES);
                        if (TA != null && TA.UserID != 0)
                        {
                            TAList.Add(TA);
                        }
                        if (perofrmerdata != null)
                        {
                            if (PerformerUserID != -1 && PerformerUserID != 0)
                            {
                                perofrmerdata.UserID = PerformerUserID;
                            }
                        }
                        if (reviewerrdata != null)
                        {
                            if (ReviewerUserID != -1 && ReviewerUserID != 0)
                            {
                                reviewerrdata.UserID = ReviewerUserID;
                            }
                        }                       
                        entities.SaveChanges();                        
                    }
                }
                if (MasterRUESList.Count > 0)
                {
                    InsertReassignmentUserEmailSend(MasterRUESList);

                    if (TAList.Count > 0)
                    {
                        InsertTaskAssignment(TAList);
                    }
                }
                
            }
        }
        public static bool InsertTaskAssignment(List<TaskAssignment> CAlist)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CAlist.ForEach(entry =>
                    {
                        var ca = (from row in entities.TaskAssignments
                                  where row.TaskInstanceID == entry.TaskInstanceID
                                  && row.RoleID == entry.RoleID
                                  select row).FirstOrDefault();
                        if (ca != null)
                        {

                        }
                        else
                        {
                            entities.TaskAssignments.Add(entry);
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result> GetAllTaskAssignedInstancesReassginExcludeStatutory_InternalNew(long customerId, long userID, string IsStatutoryOrInternal, string filter, long branchID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew(customerId, userID, IsStatutoryOrInternal)
                                                select row).ToList();

                if (complianceInstancesQuery.Count > 0)
                {
                    if (branchID != -1)
                    {
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID).ToList();
                    }
                    if (IsStatutoryOrInternal == "S")
                    {
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.TaskType == 1).ToList();
                    }
                    else
                    {
                        complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.TaskType == 2).ToList();
                    }
                    if (!string.IsNullOrEmpty(filter))
                    {
                        if (ComplianceManagement.CheckInt(filter))
                        {
                            int a = Convert.ToInt32(filter.ToUpper());
                            complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.TaskID == a).ToList();
                        }
                        else
                        {
                             complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.TaskTitle.ToUpper().Contains(filter.ToUpper())).ToList();

                        }
                    }
                }
                return complianceInstancesQuery.OrderByDescending(entry => entry.TaskInstanceID).ToList();

            }
        }
        public static void TaskApprover_DeleteComplianceAssignment(List<Tuple<long, int>> TaskComplianceInstanceIdList, long Customerid, long CreatedBy)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ExcludeUserEmailSend> MasterEUESList = new List<ExcludeUserEmailSend>();
                foreach (var cil in TaskComplianceInstanceIdList)
                {
                    var assignmentData = (from row in entities.TaskAssignments
                                          where row.TaskInstanceID == cil.Item1
                                          && row.RoleID == 6
                                          select row).ToList();
                    if (assignmentData != null)
                    {
                        foreach (var item in assignmentData)
                        {
                            entities.TaskAssignments.Remove(item);
                            entities.SaveChanges();
                        }
                       
                    }
                }//foreach end
            }
        }

        public static void ReassignTaskNew(long PerformerUserID, long ReviewerUserID,List<int>approverid, long CreatedBy, List<Tuple<long, int>> chkList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<TaskAssignment> TAList = new List<TaskAssignment>();
                List<ReassignmentUserEmailSend> MasterRUESList = new List<ReassignmentUserEmailSend>();
                foreach (var cil in chkList)
                {
                    var assignmentData = (from row in entities.TaskAssignments
                                          where row.TaskInstanceID == cil.Item1
                                          select row).ToList();

                    var approverdatalist = assignmentData.Where(entry => entry.RoleID == 6).ToList();
                    if (approverdatalist.Count > 0 && approverid.Count > 0)
                    {
                        foreach (var item1 in approverdatalist)
                        {
                            DeleteTaskApproverdata(cil.Item1);
                        }
                    }
                

                    if (assignmentData != null)
                    {
                        TaskAssignment TA = new TaskAssignment();
                        var perofrmerdata = assignmentData.Where(entry => entry.RoleID == 3).FirstOrDefault();
                        var reviewerrdata = assignmentData.Where(entry => entry.RoleID == 4).FirstOrDefault();
                        var approverdata = assignmentData.Where(entry => entry.RoleID == 6).FirstOrDefault();
                        long newpid = 0;
                        long newrid = 0;
                        //long newaid = 0;
                        long PrvPID = 0;
                        long PrvRID = 0;
                        //long PrvAID = 0;
                        long CompID = 0;
                        long BranchID = 0;

                       

                        foreach (var item in approverid)
                        {
                            TaskAssignment TAApprover = new TaskAssignment();
                            TAApprover.UserID = item;
                            TAApprover.RoleID = 6;
                            TAApprover.TaskInstanceID = cil.Item1;
                            entities.TaskAssignments.Add(TAApprover);
                            entities.SaveChanges();
                        }

                        if (perofrmerdata != null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = perofrmerdata.TaskInstance.TaskId;
                            BranchID = perofrmerdata.TaskInstance.CustomerBranchID;
                            PrvPID = perofrmerdata.UserID;
                            if (PerformerUserID == -1)
                            {
                                newpid = 0;
                            }
                            else if (perofrmerdata.UserID == PerformerUserID)
                            {
                                newpid = 0;
                            }
                            else
                            {
                                TA.UserID = PerformerUserID;
                                TA.RoleID = 3;
                                TA.TaskInstanceID = cil.Item1;
                                newpid = PerformerUserID;
                            }
                        }
                        else if (PerformerUserID == -1)
                        {
                            newpid = 0;
                        }
                        else
                        {
                            newpid = PerformerUserID;
                            TA.UserID = PerformerUserID;
                            TA.RoleID = 3;
                            TA.TaskInstanceID = cil.Item1;
                        }


                        if (reviewerrdata != null)
                        {
                            //this assign here in case of only performer or reviewer case
                            CompID = reviewerrdata.TaskInstance.TaskId;
                            BranchID = reviewerrdata.TaskInstance.CustomerBranchID;
                            PrvRID = reviewerrdata.UserID;
                            if (ReviewerUserID == -1)
                            {
                                newrid = 0;
                            }
                            else if (reviewerrdata.UserID == ReviewerUserID)
                            {
                                newrid = 0;
                            }
                            else
                            {
                                newrid = ReviewerUserID;
                                TA.UserID = ReviewerUserID;
                                TA.RoleID = 4;
                                TA.TaskInstanceID = cil.Item1;
                            }
                        }
                        else if (ReviewerUserID == -1)
                        {
                            newrid = 0;
                        }
                        else
                        {
                            newrid = ReviewerUserID;
                            TA.UserID = ReviewerUserID;
                            TA.RoleID = 4;
                            TA.TaskInstanceID = cil.Item1;
                        }
                        ReassignmentUserEmailSend RUES = new ReassignmentUserEmailSend();
                        RUES.Prev_PID = PrvPID;
                        RUES.Perv_RID = PrvRID;
                        RUES.New_PID = newpid;
                        RUES.New_RID = newrid;
                        RUES.Createdby = CreatedBy;
                        RUES.Createdon = DateTime.Now;
                        RUES.IsSend = false;
                        RUES.ComplianceID = CompID;
                        RUES.BranchID = BranchID;
                        RUES.IsStatutory = "T";
                        MasterRUESList.Add(RUES);
                        if (TA != null && TA.UserID != 0)
                        {
                            TAList.Add(TA);
                        }
                        if (perofrmerdata != null)
                        {
                            if (PerformerUserID != -1 && PerformerUserID != 0)
                            {
                                perofrmerdata.UserID = PerformerUserID;
                            }
                        }
                        if (reviewerrdata != null)
                        {
                            if (ReviewerUserID != -1 && ReviewerUserID != 0)
                            {
                                reviewerrdata.UserID = ReviewerUserID;
                            }
                        }
                      
                        entities.SaveChanges();
                   
                }
                if (MasterRUESList.Count > 0)
                {
                    InsertReassignmentUserEmailSend(MasterRUESList);

                    if (TAList.Count > 0)
                    {
                        InsertTaskAssignment(TAList);
                    }
                }
              }
            }
        }

        #endregion


       

       
    }
}
