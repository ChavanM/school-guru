﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ProcessManagement
    {

        public static Mst_LocationMaster LocationMasterGetByID(int ID, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_LocationMaster
                                        where row.ID == ID
                                        && row.CustomerID == CustomerID
                                        select row).SingleOrDefault();

                return MstAuditorMaster;
            }
        }
        public static void DeleteLocationMaster(int AuditerID, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_LocationMaster AuditorMastertoDelete = (from row in entities.Mst_LocationMaster
                                                            where row.ID == AuditerID
                                                            && row.CustomerID == CustomerID
                                                            select row).FirstOrDefault();

                AuditorMastertoDelete.IsActive = true;
                entities.SaveChanges();
            }
        }
        public static List<Mst_LocationMaster> GetAllLocationMasterList(long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMasterList = (from row in entities.Mst_LocationMaster
                                             where row.IsActive == false
                                             && row.CustomerID == CustomerID
                                             select row);

                AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.ID);

                return AuditorUserMasterList.OrderBy(entry => entry.ID).ToList();
            }
        }
        public static List<Mst_LocationMaster> GetAllLocationMaster(long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMaster = (from row in entities.Mst_LocationMaster
                                         where row.IsActive == false
                                         && row.CustomerID == CustomerID
                                         select row);

                return AuditorUserMaster.ToList();
            }
        }
        public static bool LocationExists(string LocationName, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_LocationMaster
                             where row.Name.Equals(LocationName) && row.IsActive == false
                             && row.CustomerID == CustomerID
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static string GetLocationTypeName(int LocationTypeid, long customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.Mst_LocationMaster
                                         where row.ID == LocationTypeid && row.IsActive == false
                                         && row.CustomerID == customerID
                                         select row.Name).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static int GetLocationTypeID(string LocationType, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.Mst_LocationMaster
                                         where row.Name == LocationType && row.IsActive == false
                                         && row.CustomerID == CustomerID
                                         select row.ID).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static object FillProcessDropdownCompanyAdmin(int customerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.Mst_Process
                         where row.CustomerID == customerId
                         && row.IsDeleted == false
                         select row).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static List<Mst_Process> GetAssingedProcessManagement(long customerID, long userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.EntitiesAssignmentManagementRisks
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row.ISACTIVE == true
                         && row.CustomerID == customerID
                         && row.UserID == userID
                         select row1).Distinct().ToList();

                return query;
            }
        }
        public static List<Mst_Process> GetAssingedProcessManagement(long customerID, List<long> listofBranch, long userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();

                if (listofBranch.Count > 0)
                {
                    query = (from row in entities.EntitiesAssignmentManagementRisks
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             where row.ISACTIVE == true
                             && row.CustomerID == customerID
                             && listofBranch.Contains(row.BranchID)
                             && row.UserID == userID
                             select row1).Distinct().ToList();

                }
                else
                {
                    query = (from row in entities.EntitiesAssignmentManagementRisks
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             where row.ISACTIVE == true
                             && row.CustomerID == customerID
                             && row.UserID == userID
                             select row1).Distinct().ToList();
                }
                return query;
            }
        }

        public static List<Mst_Process> GetAssingedProcess(long customerID, List<long> listofBranch, long userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();

                if (listofBranch.Count > 0)
                {
                    query = (from row in entities.EntitiesAssignmentAuditManagerRisks
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             where row.ISACTIVE == true
                             && row.CustomerID == customerID
                             && listofBranch.Contains(row.BranchID)
                             && row.UserID == userID
                             select row1).Distinct().ToList();

                }
                else
                {
                    query = (from row in entities.EntitiesAssignmentAuditManagerRisks
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             where row.ISACTIVE == true
                             && row.CustomerID == customerID
                             && row.UserID == userID
                             select row1).Distinct().ToList();
                }

                return query;
            }
        }

        public static List<Mst_Process> GetAssingedProcess(long customerID, long userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();

                query = (from row in entities.EntitiesAssignmentAuditManagerRisks
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row.ISACTIVE == true
                         && row.CustomerID == customerID
                         && row.UserID == userID
                         select row1).Distinct().ToList();

                return query;
            }
        }
        #region AuditKickOffLog

        public static bool AuditKickOffLogExists(int CustID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditKickOffMailLogs
                             where row.AuditId == AuditID
                             && row.CustomerID == CustID
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateAuditKickOffLog(AuditKickOffMailLog AKOML)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.AuditKickOffMailLogs.Add(AKOML);
                entities.SaveChanges();

            }
        }


        #endregion
        public static InternalAuditScheduling GetScheduleonDataByID(int scheduleID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Obj = (from row in entities.InternalAuditSchedulings
                           where row.Id == scheduleID
                           select row).SingleOrDefault();

                return Obj;
            }
        }

        public static void SaveInternalAuditSchedulingReportLogData(InternalAuditSchedulingReportLog objlog)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.InternalAuditSchedulingReportLogs.Add(objlog);
                entities.SaveChanges();
            }
        }
        public static List<MSDPerformanceReportCardView> GetMSDPerformanceReportCard(int RoleID, int customerid, int userID, int VerticalID, int ProcessID, string FinancialYear, string Period, List<long> BranchList, DateTime fromDatePeroid, DateTime toDatePeroid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<MSDPerformanceReportCardView> auditdsplay = new List<MSDPerformanceReportCardView>();
                auditdsplay = (from row in entities.MSDPerformanceReportCardViews
                               join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                               on row.processId equals EAAR.ProcessId
                               where row.CustomerbranchId == EAAR.BranchID
                               && EAAR.ISACTIVE == true
                               && EAAR.UserID == userID
                               && (row.CreatedDate >= fromDatePeroid && row.CreatedDate <= toDatePeroid)
                               select row).OrderBy(entry => new { entry.CompanyName, entry.FindingsInitiatives, entry.Observation }).ToList();

                if (BranchList.Count > 0)
                {
                    List<long?> BranchAssigned = BranchList.Select(x => (long?)x).ToList();
                    auditdsplay = auditdsplay.Where(Entry => BranchAssigned.Contains(Entry.CustomerbranchId)).ToList();
                }

                if (FinancialYear != "")
                    auditdsplay = auditdsplay.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (Period != "")
                    auditdsplay = auditdsplay.Where(entry => entry.ForPeriod == Period).ToList();

                if (VerticalID != -1)
                    auditdsplay = auditdsplay.Where(entry => entry.VerticalID == VerticalID).ToList();

                return auditdsplay;
            }
        }


        public static List<InternalAuditReportView> GetInternalAuditReport(int RoleID, int customerid, int userID, int VerticalID, int ProcessID, string FinancialYear, string Period, List<long> BranchList, DateTime fromDatePeroid, DateTime toDatePeroid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<InternalAuditReportView> auditdsplay = new List<InternalAuditReportView>();
                auditdsplay = (from row in entities.InternalAuditReportViews
                               join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                               on row.processId equals EAAR.ProcessId
                               where row.CustomerbranchId == EAAR.BranchID
                               && EAAR.ISACTIVE == true
                               && EAAR.UserID == userID
                               && (row.CreatedDate >= fromDatePeroid && row.CreatedDate <= toDatePeroid)
                               select row).OrderBy(entry => new { entry.Department, entry.locationname, entry.Observation }).ToList();

                if (BranchList.Count > 0)
                {
                    List<long?> BranchAssigned = BranchList.Select(x => (long?)x).ToList();
                    auditdsplay = auditdsplay.Where(Entry => BranchAssigned.Contains(Entry.CustomerbranchId)).ToList();
                }

                if (FinancialYear != "")
                    auditdsplay = auditdsplay.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (Period != "")
                    auditdsplay = auditdsplay.Where(entry => entry.AuditPeriod == Period).ToList();

                if (VerticalID != -1)
                    auditdsplay = auditdsplay.Where(entry => entry.VerticalID == VerticalID).ToList();

                return auditdsplay;
            }
        }



        public static List<ProcessAuditReportView> GetProcessAuditView(int customerid, int userID, int VerticalID, string FinancialYear, List<long> BranchList, DateTime fromDatePeroid, DateTime toDatePeroid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ProcessAuditReportView> auditdsplay = new List<ProcessAuditReportView>();
                auditdsplay = (from row in entities.ProcessAuditReportViews
                               join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                               on row.ProcessId equals EAAR.ProcessId
                               where row.CustomerbranchId == EAAR.BranchID
                               && EAAR.ISACTIVE == true
                               && EAAR.UserID == userID
                               && (row.CreatedDate >= fromDatePeroid
                               && row.CreatedDate <= toDatePeroid)
                               select row).OrderBy(entry => new { entry.LocationName, entry.ProcessName, entry.ObservationTitle, entry.ATBDId }).ToList();

                if (BranchList.Count > 0)
                {
                    List<long?> BranchAssigned = BranchList.Select(x => (long?)x).ToList();
                    auditdsplay = auditdsplay.Where(Entry => BranchAssigned.Contains(Entry.CustomerbranchId)).ToList();
                }

                if (FinancialYear != "")
                    auditdsplay = auditdsplay.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                //if (Period != "")
                //    auditdsplay = auditdsplay.Where(entry => entry.ForPeriod == Period).ToList();

                if (VerticalID != -1)
                    auditdsplay = auditdsplay.Where(entry => entry.VerticalID == VerticalID).ToList();

                return auditdsplay;
            }
        }
        public static List<CoalReviewKPUnitView> GetAuditCommitteeView(int RoleID, int customerid, int userID, int VerticalID, int ProcessID, string FinancialYear, string Period, List<long> BranchList, DateTime fromDatePeroid, DateTime toDatePeroid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<CoalReviewKPUnitView> auditdsplay = new List<CoalReviewKPUnitView>();
                auditdsplay = (from row in entities.CoalReviewKPUnitViews
                               join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                               on row.ProcessId equals EAAR.ProcessId
                               where row.CustomerbranchId == EAAR.BranchID && EAAR.ISACTIVE == true
                                && EAAR.UserID == userID
                               && (row.ISACPORMIS == 1 || row.ISACPORMIS == 2)
                               && (row.CreatedDate >= fromDatePeroid && row.CreatedDate <= toDatePeroid)
                               select row).OrderBy(entry => new { entry.ObsevationCategoryName, entry.LocationName, entry.Name, entry.ObservationTitle, entry.ATBDId }).ToList();

                if (BranchList.Count > 0)
                {
                    List<long?> BranchAssigned = BranchList.Select(x => (long?)x).ToList();
                    auditdsplay = auditdsplay.Where(Entry => BranchAssigned.Contains(Entry.CustomerbranchId)).ToList();
                }

                if (FinancialYear != "")
                    auditdsplay = auditdsplay.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (Period != "")
                    auditdsplay = auditdsplay.Where(entry => entry.ForPeriod == Period).ToList();

                if (VerticalID != -1)
                    auditdsplay = auditdsplay.Where(entry => entry.VerticalID == VerticalID).ToList();

                return auditdsplay;
            }
        }
        public static List<MISReportForPending> GetMISPendingView(int RoleID, int customerid, int userID, int VerticalID, int ProcessID, string FinancialYear, string Period, List<long> BranchList, DateTime PendingDate)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<MISReportForPending> auditdsplay = new List<MISReportForPending>();
                auditdsplay = (from row in entities.MISReportForPendings
                               join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                               on row.ProcessId equals EAAR.ProcessId
                               where row.CustomerbranchId == EAAR.BranchID && EAAR.ISACTIVE == true
                               && EAAR.UserID == userID
                               where (row.ISACPORMIS == 0 || row.ISACPORMIS == 2) && row.CreatedDate <= PendingDate
                               select row).OrderBy(entry => new { entry.ObsevationCategoryName, entry.LocationName, entry.Name, entry.ObservationTitle, entry.ATBDId }).ToList();

                if (BranchList.Count > 0)
                {
                    List<long?> BranchAssigned = BranchList.Select(x => (long?)x).ToList();
                    auditdsplay = auditdsplay.Where(Entry => BranchAssigned.Contains(Entry.CustomerbranchId)).ToList();
                }

                if (FinancialYear != "")
                    auditdsplay = auditdsplay.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (Period != "")
                    auditdsplay = auditdsplay.Where(entry => entry.ForPeriod == Period).ToList();

                if (VerticalID != -1)
                    auditdsplay = auditdsplay.Where(entry => entry.VerticalID == VerticalID).ToList();

                return auditdsplay;
            }
        }
        public static List<CoalReviewKPUnitView> GetMISViewMajor(int RoleID, int customerid, int userID, int VerticalID, int ProcessID, string FinancialYear, string Period, List<long> BranchList, DateTime startDatePeroid, DateTime EndDatePeroid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<CoalReviewKPUnitView> auditdsplay = new List<CoalReviewKPUnitView>();

                auditdsplay = (from row in entities.CoalReviewKPUnitViews
                               join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                               on row.ProcessId equals EAAR.ProcessId
                               where row.CustomerbranchId == EAAR.BranchID && EAAR.ISACTIVE == true
                               && EAAR.UserID == userID
                               && (row.ISACPORMIS == 0 || row.ISACPORMIS == 2)
                               && (row.CreatedDate >= startDatePeroid && row.CreatedDate <= EndDatePeroid)
                               select row).OrderBy(entry =>
                               new
                               {
                                   entry.ObsevationCategoryName,
                                   entry.LocationName,
                                   entry.Name,
                                   entry.ObservationTitle,
                                   entry.ATBDId
                               }).Distinct().ToList();

                if (BranchList.Count > 0)
                {
                    List<long?> BranchAssigned = BranchList.Select(x => (long?)x).ToList();
                    auditdsplay = auditdsplay.Where(Entry => BranchAssigned.Contains(Entry.CustomerbranchId)).ToList();
                }

                if (FinancialYear != "")
                    auditdsplay = auditdsplay.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (Period != "")
                    auditdsplay = auditdsplay.Where(entry => entry.ForPeriod == Period).ToList();

                if (VerticalID != -1)
                    auditdsplay = auditdsplay.Where(entry => entry.VerticalID == VerticalID).ToList();

                return auditdsplay;
            }
        }
        public static List<CoalReviewKPUnitView> GetCoalReviewKPUnitView(int RoleID, int customerid, int userID, int VerticalID, int ProcessID, string FinancialYear, string Period, List<long> BranchList, DateTime fromDatePeroid, DateTime toDatePeroid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<CoalReviewKPUnitView> auditdsplay = new List<CoalReviewKPUnitView>();
                auditdsplay = (from row in entities.CoalReviewKPUnitViews
                               join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                              on row.ProcessId equals EAAR.ProcessId
                               where row.CustomerbranchId == EAAR.BranchID && EAAR.ISACTIVE == true
                               && EAAR.UserID == userID
                               && row.CreatedDate >= fromDatePeroid && row.CreatedDate <= toDatePeroid
                               select row).OrderBy(entry => entry.ObservationTitle).ToList();


                if (BranchList.Count > 0)
                {
                    List<long?> BranchAssigned = BranchList.Select(x => (long?)x).ToList();
                    auditdsplay = auditdsplay.Where(Entry => BranchAssigned.Contains(Entry.CustomerbranchId)).ToList();
                }

                if (FinancialYear != "")
                    auditdsplay = auditdsplay.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (Period != "")
                    auditdsplay = auditdsplay.Where(entry => entry.ForPeriod == Period).ToList();

                if (VerticalID != -1)
                    auditdsplay = auditdsplay.Where(entry => entry.VerticalID == VerticalID).ToList();

                return auditdsplay;
            }
        }

        public static object FillProcessFromRCM(int customerID, List<long> branchList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> ProcessList = new List<Mst_Process>();

                if (branchList.Count > 0)
                {
                    var query = (from row in entities.RiskActivityTransactions
                                 join row1 in entities.Mst_Process
                                 on row.ProcessId equals row1.Id
                                 where branchList.Contains(row.CustomerBranchId)
                                 && row1.CustomerID == customerID
                                 select row1).Distinct();

                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                }
                else
                {
                    var query = (from row in entities.RiskActivityTransactions
                                 join row1 in entities.Mst_Process
                                 on row.ProcessId equals row1.Id
                                 where row1.CustomerID == customerID
                                 select row1).Distinct();

                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                }
            }
        }
        public static List<Mst_ObservationCategory> GetAllObservationCategory(long CustID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMaster = (from row in entities.Mst_ObservationCategory
                                         where row.IsActive == false
                                         && row.CustomerID == CustID
                                         select row);

                return AuditorUserMaster.ToList();
            }
        }


        public static bool ObservationCategoryExists(long CustID, string CategoryName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_ObservationCategory
                             where row.Name.Equals(CategoryName)
                             && row.CustomerID == CustID
                             && row.IsActive == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static int GetCountObservationProcessWiseCount(String financialyear, String Period, int ProcessId, int Customerbranchid, int VerticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var a = (from RATBM in entities.RiskActivityToBeDoneMappings
                         join RAT in entities.RiskActivityTransactions
                         on RATBM.RiskActivityId equals RAT.Id
                         join ICAR in entities.InternalControlAuditResults
                         on RATBM.ID equals ICAR.ATBDId
                         where RATBM.ProcessId == ProcessId
                         && ICAR.CustomerBranchId == Customerbranchid
                         && ICAR.VerticalID == VerticalId
                         && ICAR.RoleID == 4 && ICAR.AStatusId == 3
                         && ICAR.FinancialYear == financialyear
                         && ICAR.ForPerid == Period
                         && !(ICAR.Observation == null || ICAR.Observation.Equals(""))
                         select RATBM).ToList().Count;

                return a;
            }
        }

        public static List<SP_OneTime_Result> GetSPOneTimeDisplay(int Branchid, string FinancialYear)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_OneTime(Branchid, FinancialYear).ToList();
                return auditdsplay;
            }
        }

        public static List<AuditassignmentDetails> GetAllInternalAuditAssignment(long customerid, List<long> branchid, int VerticalListID, long UserID, string Role)
        {
            List<AuditassignmentDetails> AuditorUserMaster = new List<AuditassignmentDetails>();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (Role == "CADMN")
                {
                    entities.Database.CommandTimeout = 300;
                    AuditorUserMaster = (from row in entities.Internal_AuditAssignment
                                         join row1 in entities.mst_Vertical
                                         on row.VerticalID equals row1.ID
                                         where row.IsActive == false
                                         && row.CustomerId == customerid
                                         select new AuditassignmentDetails()
                                         {
                                             Id = row.Id,
                                             CustomerBranchid = row.CustomerBranchid,
                                             AssignedTo = row.AssignedTo,
                                             ExternalAuditorId = row.ExternalAuditorId,
                                             CustomerId = row.CustomerId,
                                             VerticalID = row.VerticalID,
                                             VerticalName = row1.VerticalName
                                         }).Distinct().ToList();
                }
                else
                {

                    entities.Database.CommandTimeout = 300;
                    AuditorUserMaster = (from row in entities.Internal_AuditAssignment
                                         join row1 in entities.mst_Vertical
                                         on row.VerticalID equals row1.ID
                                         join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                         on row.CustomerBranchid equals EAAR.BranchID
                                         where row.IsActive == false && EAAR.UserID == UserID
                                         && row.CustomerId == customerid && EAAR.ISACTIVE == true
                                         select new AuditassignmentDetails()
                                         {
                                             Id = row.Id,
                                             CustomerBranchid = row.CustomerBranchid,
                                             AssignedTo = row.AssignedTo,
                                             ExternalAuditorId = row.ExternalAuditorId,
                                             CustomerId = row.CustomerId,
                                             VerticalID = row.VerticalID,
                                             VerticalName = row1.VerticalName
                                         }).Distinct().ToList();

                }
                if (branchid.Count > 0)
                {
                    AuditorUserMaster = AuditorUserMaster.Where(entry => branchid.Contains(entry.CustomerBranchid)).ToList();
                }
                if (VerticalListID != -1)
                {
                    AuditorUserMaster = AuditorUserMaster.Where(entry => entry.VerticalID == VerticalListID).ToList();
                }
                return AuditorUserMaster.ToList();
            }
        }

        public static List<SP_ImplementationAuditSummary_Result> GetAuditsForAuditManagerIMP(int userid, int branchid, string FinancialYear, int statusid, string Period, int customerID) /**/
        {
            List<SP_ImplementationAuditSummary_Result> transactionsQuery = new List<SP_ImplementationAuditSummary_Result>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                string isAMAH = CustomerManagementRisk.GetAuditHeadOrManagerid(userid);
                if (!String.IsNullOrEmpty(isAMAH) && (isAMAH == "AM" || isAMAH == "AH"))
                {
                    List<long> branchids = CheckAuditManagerLocation(userid);

                    transactionsQuery = (from row in entities.SP_ImplementationAuditSummary(customerID)
                                         where row.AuditStatusID == statusid
                                         && row.RoleID == 4
                                         && branchids.Contains(row.CustomerBranchID)
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).Distinct().ToList();

                    if (FinancialYear != "")
                        transactionsQuery = transactionsQuery.Where(Entry => Entry.FinancialYear == FinancialYear).ToList();

                    if (Period != "")
                        transactionsQuery = transactionsQuery.Where(Entry => Entry.ForMonth == Period).ToList();

                    if (statusid != -1)
                        transactionsQuery = transactionsQuery.Where(Entry => Entry.AuditStatusID == statusid).ToList();

                    if (branchid != -1)
                        transactionsQuery = transactionsQuery.Where(Entry => Entry.CustomerBranchID == branchid).ToList();

                }
                return transactionsQuery;
            }
        }

        public static List<AuditSummaryCountView> GetAuditsForAuditManager(int userid, int branchid, string FinancialYear, int statusid) /*string Period,*/
        {
            List<AuditSummaryCountView> transactionsQuery = new List<AuditSummaryCountView>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                string isAMAH = CustomerManagementRisk.GetAuditHeadOrManagerid(userid);
                if (!String.IsNullOrEmpty(isAMAH) && (isAMAH == "AM" || isAMAH == "AH"))
                {
                    List<long> branchids = CheckAuditManagerLocation(userid);

                    transactionsQuery = (from row in entities.AuditSummaryCountViews
                                         where row.AuditStatusID == statusid
                                         && row.RoleID == 4
                                         && branchids.Contains(row.CustomerBranchID)
                                         select row).GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).Distinct().ToList();

                    if (FinancialYear != "")
                        transactionsQuery = transactionsQuery.Where(Entry => Entry.FinancialYear == FinancialYear).ToList();

                    //if (Period != "")
                    //    transactionsQuery = transactionsQuery.Where(Entry => Entry.ForMonth == Period).ToList();

                    if (statusid != -1)
                        transactionsQuery = transactionsQuery.Where(Entry => Entry.AuditStatusID == statusid).ToList();

                }
                return transactionsQuery;
            }
        }

        public static List<long> GetAllMst_ProcessList(List<int> rolelist, int AuditID, int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<long> ProcessList = new List<long>();

                var Result = (from row in entities.InternalControlAuditAssignments
                              where row.AuditID == AuditID
                              && row.UserID == userID
                              && rolelist.Contains(row.RoleID)
                              select row).Distinct().ToList();

                if (Result.Count > 0)
                {
                    ProcessList = Result.Select(entry => entry.ProcessId).ToList();
                }
                return ProcessList;
            }
        }

        public static List<SP_FillInternalAuditAssignment_AuditKickOff_Result> GetSP_FillInternalAuditAssignment_AuditKickOff_ResultProcedure(int branchid, string FinancialYear, string Period)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.SP_FillInternalAuditAssignment_AuditKickOff(branchid, FinancialYear, Period).ToList();
                return complianceReminders;
            }

        }
        public static List<mst_User> GetAllAuditorUserMasterList(long Customerid, int AuditorID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMasterList = (from row in entities.mst_User
                                             where row.IsActive == true && row.IsDeleted == false && row.AuditorID == AuditorID
                                             && row.CustomerID == Customerid
                                             select row);

                AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.FirstName);

                return AuditorUserMasterList.OrderBy(entry => entry.FirstName).ToList();
            }
        }


        public static void DeleteAuditorTeamUser(long Customerid, int AuditerUserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_User AuditorUserMastertoDelete = (from row in entities.mst_User
                                                      where row.ID == AuditerUserID && row.CustomerID == Customerid
                                                      select row).FirstOrDefault();

                AuditorUserMastertoDelete.IsDeleted = true;
                entities.SaveChanges();
            }
        }


        public static mst_User AuditorTeamUserGetByID(long Customerid, int AuditorUserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var mstuser = (from row in entities.mst_User
                               where row.ID == AuditorUserID && row.CustomerID == Customerid
                               select row).SingleOrDefault();

                return mstuser;
            }
        }


        public static List<mst_User> GetAllAuditorTeamUser(long Customerid, int AuditorID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMaster = (from row in entities.mst_User
                                         where row.IsActive == true && row.IsDeleted == false && row.AuditorID == AuditorID
                                         && row.CustomerID == Customerid
                                         select row);

                return AuditorUserMaster.ToList();
            }
        }

        public static bool AuditorTeamUserExists(string Email, int AuditorUserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_User
                             where row.Email.Equals(Email) && row.ID == AuditorUserID && row.IsActive == true && row.IsDeleted == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static int RiskActivityToBeDoneMappingGetByName(String Name, long riskcreationId, long riskactivityid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RiskActivityToBeDoneMappingGetByName = (from row in entities.RiskActivityToBeDoneMappings
                                                            where row.ActivityTobeDone.ToUpper().Trim() == Name.ToUpper().Trim()
                                                            && row.RiskCategoryCreationId == riskcreationId
                                                            && row.RiskActivityId == riskactivityid
                                                            && row.SpecialAuditFlag == false
                                                            select row);
                return RiskActivityToBeDoneMappingGetByName.ToList().Count;
            }
        }

        public static List<KeyName_Result> GetKeyNameProcedure(int ID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.KeyName(ID).GroupBy(entry => entry).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }
        }
        public static List<PersonResponsibleName_Result> GetPersonResponsibleNameProcedure(int PersonID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.PersonResponsibleName(PersonID).GroupBy(entry => entry).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }
        }
        public static List<ControlName_Result> GetControlNameProcedure(int ID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.ControlName(ID).GroupBy(entry => entry).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }
        }

        public static List<FrequencyName_Result> GetFrequencyNameProcedure(int ID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.FrequencyName(ID).GroupBy(entry => entry).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }
        }
        public static int ProcessRatingGetByProcessAndRating(long PID, long CustBID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_ProcessRating
                                        where row.ProcessID == PID && row.CustomerBranchid == CustBID
                                        select row);
                return MstAuditorMaster.ToList().Count;
            }
        }
        public static int LocationMasterGetByName(String Name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_LocationMaster
                                        where row.Name == Name && row.IsActive == false
                                        select row);
                return MstAuditorMaster.ToList().Count;
            }
        }

        public static int RiskActivityToBeDoneMappingGetByName(String Name, long riskcreationId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RiskActivityToBeDoneMappingGetByName = (from row in entities.RiskActivityToBeDoneMappings
                                                            where row.ActivityTobeDone == Name && row.RiskCategoryCreationId == riskcreationId
                                                            select row);
                return RiskActivityToBeDoneMappingGetByName.ToList().Count;
            }
        }

        public static int RiskRiskActivityTransactionGetByName(String ControlDescription)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RiskActivityToBeDoneMappingGetByName = (from row in entities.RiskActivityTransactions
                                                            where row.ControlDescription == ControlDescription
                                                            && row.SpecialAuditFlag == false
                                                            // && row.RiskCreationId == riskcreationId
                                                            select row);
                return RiskActivityToBeDoneMappingGetByName.ToList().Count;
            }
        }
        public static object FilliNDUSTRY()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Industry
                                 //where row.IsDeleted == false
                             select row);
                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static List<ReviewerCommentReportView> GetReviewerCommentReportViewDisplay(long Userid, int? CustomerBranchID, string FinancialYear)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ReviewerCommentReportView> auditdsplay = new List<ReviewerCommentReportView>();

                //if (FinancialYear != "" && formonth != "")
                //{
                //    auditdsplay = (from row in entities.ReviewerCommentReportViews
                //                   where row.UserID == Userid && row.CustomerBranchID == CustomerBranchID
                //                   && row.FinancialYear == FinancialYear && row.ForMonth == formonth
                //                   select row).ToList();
                //}
                if (FinancialYear != "")
                {
                    auditdsplay = (from row in entities.ReviewerCommentReportViews
                                   where row.UserID == Userid && row.CustomerBranchID == CustomerBranchID
                                   && row.FinancialYear == FinancialYear
                                   select row).ToList();
                }
                else if (CustomerBranchID != -1)
                {
                    auditdsplay = (from row in entities.ReviewerCommentReportViews
                                   where row.UserID == Userid && row.CustomerBranchID == CustomerBranchID
                                   select row).ToList();
                }
                // var auditdsplay = entities.SP_PendingTestingReportTest (Convert.ToInt32(Userid), formonth, FinancialYear, CustomerBranchID).ToList();
                return auditdsplay;
            }

        }
        public static List<PendingTestingReportView> GetPendingTestingReportViewDisplay(long Userid, long Customerid, int? CustomerBranchID, string FinancialYear, string formonth)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<PendingTestingReportView> auditdsplay = new List<PendingTestingReportView>();
                auditdsplay = (from row in entities.PendingTestingReportViews
                               where row.UserID == Userid && row.CustomerID == Customerid
                               && row.FinancialYear == FinancialYear
                               select row).ToList();

                if (CustomerBranchID != -1)
                {
                    auditdsplay = auditdsplay.Where(entry => entry.CustomerBranchID == CustomerBranchID).ToList();
                }
                if (formonth != "")
                {
                    auditdsplay = auditdsplay.Where(entry => entry.ForMonth == formonth).ToList();
                }
                return auditdsplay;
            }
        }
        public static object FillLocationType(long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_LocationMaster
                             where row.IsActive == false
                             //&& row.CustomerID == CustomerID
                             select row);

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillLocationType(string flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_Customer
                             where row.IsDeleted == false
                             select row);

                //if (flag == "N")
                //{
                //    query = query.Where(entry => entry.IsProcessNonProcess == "N");
                //}
                //else
                //{
                //    query = query.Where(entry => entry.IsProcessNonProcess == "P");
                //}


                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }



        public static object FillLocationType()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_LocationMaster
                             where row.IsActive == false
                             select row);

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        #region Scheduling



        public static List<AuditExpectedStartEndDate_Result> GetAuditExpectedStartEndDate_ResultProcedure(string ISAHMQP, string FinancialYear, int CustomerBranchId, string Termname, int Verticatid, int AuditId)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.AuditExpectedStartEndDate(ISAHMQP, FinancialYear, CustomerBranchId, Termname, Verticatid, AuditId).GroupBy(entry => entry.Id).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }
        }


        public static List<long> CheckAuditManagerLocation(int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                         where row.UserID == userid
                                         select row.BranchID).Distinct().ToList();
                return transactionsQuery.ToList();
            }
        }
        public static List<InternalAuditInstanceTransactionView> InternalAuditInstanceTransactionPersonResponsible_View(int userid, int branchid, string FinancialYear, string Period, int statusid)
        {

            var PersonResp = CustomerManagementRisk.GetInternalPersonResponsibleid(userid);
            List<InternalAuditInstanceTransactionView> transactionsQuery = new List<InternalAuditInstanceTransactionView>();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (PersonResp == userid)
                {



                    transactionsQuery = (from row in entities.InternalAuditInstanceTransactionViews
                                         where row.AuditStatusID == statusid && row.RoleID == 4
                                            && row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear
                                            && row.ForMonth == Period
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).Distinct().ToList();

                }
                return transactionsQuery;
            }

        }

        public static List<InternalAuditInstanceTransactionView> InternalAuditInstanceTransactionAuditManager_View(int userid, int branchid, string FinancialYear, string Period, int statusid)
        {
            List<InternalAuditInstanceTransactionView> transactionsQuery = new List<InternalAuditInstanceTransactionView>();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                string isAMAH = CustomerManagementRisk.GetAuditHeadOrManagerid(userid);
                if (!String.IsNullOrEmpty(isAMAH) && (isAMAH == "AM" || isAMAH == "AH"))
                {
                    if (CheckAuditManagerLocation(userid).ToList().Contains(branchid))
                    {


                        transactionsQuery = (from row in entities.InternalAuditInstanceTransactionViews
                                             where row.AuditStatusID == statusid && row.RoleID == 4
                                                && row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear
                                                && row.ForMonth == Period
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).Distinct().ToList();
                    }
                }
                return transactionsQuery;
            }

        }

        public static List<InternalAuditInstanceTransactionView> InternalAuditInstanceTransactionReviewer_View(int userid, int branchid, string FinancialYear, string Period, int statusid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //var transactionsQuery = (from row in entities.InternalAuditInstanceTransactionViews
                //    where row.UserID == userid && row.RoleID == 4
                //    && row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear
                //    && row.ForMonth == Period
                //    select row).GroupBy(entity => entity.ProcessId).Select(entity => entity.FirstOrDefault()).Distinct().ToList(); 

                var transactionsQuery = (from row in entities.InternalAuditInstanceTransactionViews
                                         where row.UserID == userid && row.RoleID == 4
                                         && row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear
                                         && row.ForMonth == Period //&& row.AuditStatusID == statusid
                                         select row).GroupBy(entity => entity.ProcessId).Select(entity => entity.FirstOrDefault()).Distinct().ToList();

                return transactionsQuery;
            }

        }
        public static List<InternalAuditInstanceTransactionView> InternalAuditInstanceTransactionReviewComment_View(int userid, int branchid, string FinancialYear, string Period)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> PerformerstatusIds = new List<int>();
                PerformerstatusIds.Clear();
                PerformerstatusIds.Add(4);
                var transactionsQuery = (from row in entities.InternalAuditInstanceTransactionViews
                                         where row.UserID == userid && row.RoleID == 3
                                      && row.CustomerBranchID == branchid
                                      && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                         // && FrequencyIds.Contains((int)row.Frequency)
                                         select row).GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();

                //var complianceReminders = entities.SP_GETAssignedProcess_Performer(userid, FinancialYear, Period, branchid).ToList();
                return transactionsQuery;
            }

        }
        public static List<InternalAuditInstanceTransactionView> InternalAuditInstanceTransactionPerofrmer_View(int userid, int branchid, string FinancialYear, string Period, int statusid)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<InternalAuditInstanceTransactionView> transactionsQuery = new List<InternalAuditInstanceTransactionView>();

                //transactionsQuery = (from row in entities.InternalAuditInstanceTransactionViews
                //                     where row.UserID == userid && row.RoleID == 3
                //                  && row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear
                //                  && row.ForMonth == Period
                //                     select row).GroupBy(entity => entity.ProcessId).Select(entity => entity.FirstOrDefault()).Distinct().ToList();

                transactionsQuery = (from row in entities.InternalAuditInstanceTransactionViews
                                     where row.UserID == userid && row.RoleID == 3
                                  && row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear
                                  && row.ForMonth == Period //&& row.AuditStatusID==statusid
                                     select row).GroupBy(entity => entity.ProcessId).Select(entity => entity.FirstOrDefault()).Distinct().ToList();

                return transactionsQuery;
            }

        }
        public static List<ImplementationAuditInstanceTransactionView> ImplementationAuditInstanceTransactionAuditManager_View(int userid, int branchid, string FinancialYear, string Period, int statusid)
        {
            List<ImplementationAuditInstanceTransactionView> transactionsQuery = new List<ImplementationAuditInstanceTransactionView>();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                string isAMAH = CustomerManagementRisk.GetAuditHeadOrManagerid(userid);
                if (!String.IsNullOrEmpty(isAMAH) && (isAMAH == "AM" || isAMAH == "AH"))
                {
                    if (CheckAuditManagerLocation(userid).ToList().Contains(branchid))
                    {


                        transactionsQuery = (from row in entities.ImplementationAuditInstanceTransactionViews
                                             where row.AuditStatusID == statusid && row.RoleID == 4
                                                && row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear
                                                && row.ForMonth == Period
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).Distinct().ToList();
                    }
                }
                return transactionsQuery;
            }

        }
        public static List<ImplementationAuditInstanceTransactionView> ImplementationAuditInstanceTransaction_ViewReviewer(int userid, int branchid, string FinancialYear, string Period)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ImplementationAuditInstanceTransactionView> transactionsQuery = new List<ImplementationAuditInstanceTransactionView>();

                transactionsQuery = (from row in entities.ImplementationAuditInstanceTransactionViews
                                     where row.UserID == userid && row.RoleID == 4
                                  && row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear
                                  && row.ForMonth == Period
                                     select row).GroupBy(entity => entity.FinancialYear).Select(entity => entity.FirstOrDefault()).Distinct().ToList();

                return transactionsQuery;
            }

        }
        public static List<ImplementationAuditInstanceTransactionView> ImplementationAuditInstanceTransaction_View(int userid, int branchid, string FinancialYear, string Period)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ImplementationAuditInstanceTransactionView> transactionsQuery = new List<ImplementationAuditInstanceTransactionView>();

                transactionsQuery = (from row in entities.ImplementationAuditInstanceTransactionViews
                                     where row.UserID == userid && row.RoleID == 3
                                  && row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear
                                  && row.ForMonth == Period
                                     select row).GroupBy(entity => entity.FinancialYear).Select(entity => entity.FirstOrDefault()).Distinct().ToList();

                return transactionsQuery;
            }

        }


        public static List<SP_GETAssignedProcess_Performer_Result> SP_GETAssignedProcess_Performer_ResultProcedure(int userid, int branchid, string FinancialYear, string Period)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var complianceReminders = entities.SP_GETAssignedProcess_Performer(userid, FinancialYear, Period, branchid).ToList();
                return complianceReminders;
            }

        }
        public static List<SP_GETAssignedProcess_Reviewer_Result> SP_GETAssignedProcess_Reviewer_ResultProcedure(int userid, int branchid, string FinancialYear, string Period)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var complianceReminders = entities.SP_GETAssignedProcess_Reviewer(userid, FinancialYear, Period, branchid).ToList();
                return complianceReminders;
            }

        }
        public static List<Sp_ImplementationStatus_Result> GetSp_ImplementationStatus_ResultProcedure(int branchid, string FinancialYear, string ForPeriod)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.Sp_ImplementationStatus(3, branchid, FinancialYear, ForPeriod).ToList();
                return complianceReminders;
            }

        }
        public static List<SP_FillTMP_Assignment_Saved_Result> GetSP_FillTMP_Assignment_Saved_ResultProcedure(int branchid, string FinancialYear, string ForPeriod)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.SP_FillTMP_Assignment_Saved(branchid, FinancialYear, ForPeriod).ToList();
                return complianceReminders;
            }

        }
        public static List<SP_TMP_ImplementationAssignment_Saved_Result> GetSP_TMP_ImplementationAssignment_Saved_ResultProcedure(int branchid, string FinancialYear)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.SP_TMP_ImplementationAssignment_Saved(branchid, FinancialYear).ToList();
                return complianceReminders;
            }

        }

        public static List<SP_TMP_ImplementationAssignment_Result> GetSP_TMP_ImplementationAssignment_ResultProcedure(int branchid, string FinancialYear)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.SP_TMP_ImplementationAssignment(branchid, FinancialYear).ToList();
                return complianceReminders;
            }

        }

        public static List<SP_FillTMP_Assignment_Result> GetSP_FillTMP_Assignment_ResultProcedure(int branchid, string FinancialYear, string ForPeriod)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                // var complianceReminders = entities.SP_FillInternalAuditAssignment(processId, branchid).GroupBy(entry => new {entry.ProcessId,entry.CustomerBranchID }).Select(en => en.FirstOrDefault()).ToList();
                var complianceReminders = entities.SP_FillTMP_Assignment(branchid, FinancialYear, ForPeriod).ToList();
                return complianceReminders;
            }

        }
        public static List<SP_FillInternalAuditAssignmentNew_Result> GetSP_FillInternalAuditAssignmentNEW_ResultProcedure(int branchid, string FinancialYear, string Period)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                // var complianceReminders = entities.SP_FillInternalAuditAssignment(processId, branchid).GroupBy(entry => new {entry.ProcessId,entry.CustomerBranchID }).Select(en => en.FirstOrDefault()).ToList();
                var complianceReminders = entities.SP_FillInternalAuditAssignmentNew(branchid, FinancialYear, Period).ToList();
                return complianceReminders;
            }

        }
        public static List<SP_FillInternalAuditAssignment_Result> GetSP_FillInternalAuditAssignment_ResultProcedure(int processId, int branchid, string FinancialYear, string Period)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                // var complianceReminders = entities.SP_FillInternalAuditAssignment(processId, branchid).GroupBy(entry => new {entry.ProcessId,entry.CustomerBranchID }).Select(en => en.FirstOrDefault()).ToList();
                var complianceReminders = entities.SP_FillInternalAuditAssignment(processId, branchid, FinancialYear, Period).ToList();
                return complianceReminders;
            }

        }
        public static List<SP_GETPerformer_Result> GETSP_GETPerformerProcedure(string ISInternalExternal, int processId, int branchid, int Userid, string FinancialYear)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.SP_GETPerformer(ISInternalExternal, processId, branchid, Userid, FinancialYear).GroupBy(entry => entry.Name).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }

        }

        public static List<SP_GETReviewer_Result> GETSP_GETReviewer_ResultProcedure(string ISInternalExternal, int processId, int branchid, int Userid, string FinancialYear)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.SP_GETReviewer(ISInternalExternal, processId, branchid, Userid, FinancialYear).GroupBy(entry => entry.Name).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }

        }

        public static List<CustomerBranchName_Result> GetCustomerBranchNameProcedure(int riskcategorycreationID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.CustomerBranchName(riskcategorycreationID).GroupBy(entry => entry.ID).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }

        }
        public static List<AssertionsName_Result> GetAssertionsNameProcedure(int riskcategorycreationID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.AssertionsName(riskcategorycreationID).GroupBy(entry => entry.Id).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }

        }
        public static List<CustomerBranchNameLocationWise_Result> GetCustomerBranchNameProcedureLocationWise(int riskcategorycreationID, int Branchid)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.CustomerBranchNameLocationWise(riskcategorycreationID, Branchid).GroupBy(entry => entry.ID).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }

        }

        public static List<TesingStatusReport_Result> GetTestingStatusGridDisplay(int Branchid, string FinancialYear)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.TesingStatusReport(Branchid, FinancialYear).ToList();
                return auditdsplay;
            }

        }
        //public static List<FailedControlView> GetFailedControlsViewPerformer(int Branchid, string FinancialYear, int Roleid)
        //{
        //    //date = date.Date;
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<FailedControlView> transactionsQuery = new List<FailedControlView>();
        //        if (Branchid != -1 && !string.IsNullOrEmpty(FinancialYear))
        //        {
        //            transactionsQuery = (from row in entities.FailedControlViews
        //                                 where row.RoleID == Roleid && row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear
        //                                 select row).ToList();
        //        }
        //        else if (Branchid != -1)
        //        {
        //            transactionsQuery = (from row in entities.FailedControlViews
        //                                 where row.RoleID == Roleid && row.CustomerBranchID == Branchid
        //                                 select row).ToList();
        //        }
        //        return transactionsQuery;

        //    }

        //}
        //public static List<FailedControlView> GetFailedControlsViewReviewer(int Branchid, string FinancialYear, int Roleid, int Userid)
        //{
        //    //date = date.Date;
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<FailedControlView> transactionsQuery = new List<FailedControlView>();
        //        if (Branchid != -1 && !string.IsNullOrEmpty(FinancialYear))
        //        {
        //            transactionsQuery = (from row in entities.FailedControlViews
        //                                 where row.RoleID == Roleid && row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear
        //                                 && row.PersonResponsible == Userid && row.TimeLine == null
        //                                 select row).GroupBy(x => x.ScheduledOnID).Select(y => y.FirstOrDefault()).ToList();
        //        }
        //        else if (Branchid != -1)
        //        {
        //            transactionsQuery = (from row in entities.FailedControlViews
        //                                 where row.RoleID == Roleid && row.CustomerBranchID == Branchid && row.PersonResponsible == Userid && row.TimeLine == null
        //                                 select row).GroupBy(x => x.ScheduledOnID).Select(y => y.FirstOrDefault()).ToList();
        //        }
        //        return transactionsQuery;

        //    }

        //}
        public static List<AuditTestingstatusExportView> GetTestingStatusGridDisplayView(int Customerid, int Branchid, string FinancialYear)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditTestingstatusExportView> transactionsQuery = new List<AuditTestingstatusExportView>();

                transactionsQuery = (from row in entities.AuditTestingstatusExportViews
                                     where row.CustomerId == Customerid && row.FinancialYear == FinancialYear
                                     select row).ToList();

                if (Branchid != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchId == Branchid).ToList();
                }
                return transactionsQuery;

            }

        }
        public static List<AUDITREPORTINGSYSTEM_NOTDONE_Result> AUDITREPORTINGSYSTEM_NOTDONEProcedure()
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.AUDITREPORTINGSYSTEM_NOTDONE().ToList();
                return auditdsplay;
            }
        }
        public static List<AUDITREPORTINGSYSTEM_NOTDONE_SubProcess_Result> AUDITREPORTINGSYSTEM_NOTDONE_SubProcessProcedure(int Processid, int Subprocessid)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.AUDITREPORTINGSYSTEM_NOTDONE_SubProcess(Processid, Subprocessid).ToList();
                return auditdsplay;
            }
        }

        public static List<IFCRAuditUploadView> GetAuditGridDisplay(int Customerid, List<long> BranchID, int ProcessId, int SubprocessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<IFCRAuditUploadView> riskassignmentexport = new List<IFCRAuditUploadView>();
                riskassignmentexport = (from row in entities.IFCRAuditUploadViews
                                        where row.CustomerId == Customerid && row.IsInternalAudit == "N"
                                        select row).ToList();
                if (BranchID.Count > 0)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => BranchID.Contains((long)entry.CustomerBranchId)).ToList();
                }
                if (ProcessId != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.ProcessId == ProcessId).ToList();
                }
                if (SubprocessId != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                }
                return riskassignmentexport;
            }
        }

        public static List<Sp_ProcessRiskRatingDetail_Result> GetSPProcessRiskRatingDetail(int customerid, int Branchid, int processid)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.Sp_ProcessRiskRatingDetail(customerid, Branchid, processid).ToList();
                auditdsplay = auditdsplay.OrderByDescending(x => x.Printploted).ToList();
                return auditdsplay;
            }
        }


        public static List<SP_Annualy_Result> GetSPAnnualyDisplay(int Branchid, string FinancialYear, int verticalId)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Annualy(Branchid, FinancialYear, verticalId).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Quarterly_Result> GetSPQuarterwiseDisplay(int Branchid, string FinancialYear, int verticalId)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Quarterly(Branchid, FinancialYear, verticalId).ToList();
                return auditdsplay;
            }
        }

        public static List<SP_Monthly_Result> GetSPMonthlyDisplay(int Branchid, string FinancialYear, int verticalId)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Monthly(Branchid, FinancialYear, verticalId).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_HalfYearly_Result> GetSPHalfYearlyDisplay(int Branchid, string FinancialYear, int verticalId)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_HalfYearly(Branchid, FinancialYear, verticalId).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase1_Result> GetSP_Phase1Display(int Branchid, string FinancialYear, int verticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var auditdsplay = entities.SP_Phase1(Branchid, FinancialYear, verticalId).ToList();
                return auditdsplay;

            }
        }
        public static List<SP_Phase2_Result> GetSP_Phase2Display(int Branchid, string FinancialYear, int verticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var auditdsplay = entities.SP_Phase2(Branchid, FinancialYear, verticalId).ToList();
                return auditdsplay;

            }
        }
        public static List<SP_Phase3_Result> GetSP_Phase3Display(int Branchid, string FinancialYear, int verticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var auditdsplay = entities.SP_Phase3(Branchid, FinancialYear, verticalId).ToList();
                return auditdsplay;

            }
        }
        public static List<SP_Phase4_Result> GetSP_Phase4Display(int Branchid, string FinancialYear, int verticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var auditdsplay = entities.SP_Phase4(Branchid, FinancialYear, verticalId).ToList();
                return auditdsplay;

            }
        }
        public static List<SP_Phase5_Result> GetSP_Phase5Display(int Branchid, string FinancialYear, int verticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var auditdsplay = entities.SP_Phase5(Branchid, FinancialYear, verticalId).ToList();
                return auditdsplay;

            }
        }

        public static List<IndustryName_Result> GetIndustryNameProcedure(int riskcategorycreationID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.IndustryName(riskcategorycreationID).GroupBy(entry => entry.ID).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }

        }
        public static List<RiskCategoryName_Result> GetRiskCategoryNameProcedure(int riskcategorycreationID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.RiskCategoryName(riskcategorycreationID).GroupBy(entry => entry.Id).Select(en => en.FirstOrDefault()).ToList();
                return complianceReminders;
            }

        }

        public static int GetCountOfScheduledProcess(string FinancialYear, string Period, int Customerbranchid, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var a = (from row in entities.InternalAuditSchedulings
                         where row.FinancialYear == FinancialYear && row.TermName == Period
                         && row.CustomerBranchId == Customerbranchid && row.VerticalID == VerticalID
                         select row).ToList().Count;

                return a;
            }
        }
        #endregion
        #region Process       
        public static object FillProcess(long Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_Process
                             where row.IsDeleted == false && row.CustomerID == Customerid
                             select row);
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcess(string flag, long Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.Mst_Process
                             where row.IsDeleted == false && row.CustomerID == Customerid
                             select row);

                if (flag == "N")
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "N");
                }
                else
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "P");
                }


                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcess(int Customerid, int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();

                if (Branchid != -1)
                {
                    query = (from row in entities.RiskActivityTransactions
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             where row.IsDeleted == false && row1.CustomerID == Customerid && row.CustomerBranchId == Branchid
                             select row1).Distinct().ToList();

                    if (query.Count == 0)
                    {
                        query = (from row in entities.Mst_Process
                                 where row.CustomerID == Customerid
                                 select row).Distinct().ToList();
                    }
                }
                else
                {
                    query = (from row in entities.Mst_Process
                             where row.CustomerID == Customerid
                             select row).Distinct().ToList();
                }
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcessDropdownAuditManager(int Customerid, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.EntitiesAssignmentAuditManagerRisks
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row.ISACTIVE == true
                         && row.UserID == UserID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcessDropdownAuditManager(int Customerid, long UserID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.EntitiesAssignmentAuditManagerRisks
                         join row2 in entities.InternalControlAuditAssignments
                         on row.ProcessId equals row2.ProcessId
                         join row1 in entities.Mst_Process
                          on row.ProcessId equals row1.Id
                         where row2.RoleID == 3 && row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row.ISACTIVE == true
                         && row.UserID == UserID
                         && row2.AuditID == AuditID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcessDropdownDepartmentHead(int Customerid, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Sp_GetDepartmentHeadProcess_Result> query = new List<Sp_GetDepartmentHeadProcess_Result>();
                query = (from row in entities.Sp_GetDepartmentHeadProcess(Convert.ToInt32(UserID))
                         select row).Distinct().ToList();
                var users = (from row in query
                             select new { ID = row.ProcessId, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcessDropdownManagement(int Customerid, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.EntitiesAssignmentManagementRisks
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row.ISACTIVE == true
                         && row.UserID == UserID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static object FillProcessDropdownPerformerAndReviewer(int Customerid, long UserID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.InternalControlAuditAssignments
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row.UserID == UserID
                         && row.AuditID == AuditID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcessDropdownPerformerAndReviewer(int Customerid, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.InternalControlAuditAssignments
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row.UserID == UserID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcessDropdownPersonResponsible(int Customerid, long UserID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.InternalControlAuditResults
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row.PersonResponsible == UserID
                         && row.AuditID == AuditID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcess(string flag, int Customerid, int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();

                if (Branchid != -1)
                {
                    query = (from row in entities.RiskActivityTransactions
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             where row.IsDeleted == false && row1.CustomerID == Customerid && row.CustomerBranchId == Branchid
                             select row1).Distinct().ToList();

                    if (query.Count == 0)
                    {
                        query = (from row in entities.Mst_Process
                                 where row.CustomerID == Customerid
                                 select row).Distinct().ToList();
                    }
                }
                else
                {
                    query = (from row in entities.Mst_Process
                             where row.CustomerID == Customerid
                             select row).Distinct().ToList();
                }
                if (flag == "N")
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "N").ToList();
                }
                else
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "P").ToList();
                }
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcess(string flag, int Customerid, int Branchid, List<long> processlist)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();

                if (Branchid != -1)
                {
                    query = (from row in entities.RiskActivityTransactions
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             where row.IsDeleted == false && row1.CustomerID == Customerid && row.CustomerBranchId == Branchid
                             select row1).Distinct().ToList();

                    if (query.Count == 0)
                    {
                        query = (from row in entities.Mst_Process
                                 where row.CustomerID == Customerid
                                 select row).Distinct().ToList();
                    }
                }
                else
                {
                    query = (from row in entities.Mst_Process
                             where row.CustomerID == Customerid
                             select row).Distinct().ToList();
                }
                if (processlist.Count > 0)
                {
                    query = query.Where(entry => processlist.Contains(entry.Id)).ToList();
                }
                if (flag == "N")
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "N").ToList();
                }
                else
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "P").ToList();
                }
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static object FillProcessDropdownAuditManagerSummary(string flag, int Customerid, int AuditID, int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.InternalControlAuditAssignments
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         join row2 in entities.EntitiesAssignmentAuditManagerRisks
                         on row.ProcessId equals row2.ProcessId
                         where row.CustomerBranchID == row2.BranchID && row.AuditID == AuditID
                         && row2.UserID == userid
                         && row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row2.ISACTIVE == true
                         select row1).Distinct().ToList();

                if (flag == "N")
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "N").ToList();
                }
                else
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "P").ToList();
                }
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcessDropdown(string flag, int Customerid, int AuditID, int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.InternalControlAuditAssignments
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row.AuditID == AuditID
                         && row1.CustomerID == Customerid
                         && row.UserID == UserID
                         && row1.IsDeleted == false
                         select row1).Distinct().ToList();

                if (flag == "N")
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "N").ToList();
                }
                else
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "P").ToList();
                }
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillProcessDropdown(string flag, int Customerid, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.InternalControlAuditAssignments
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row.AuditID == AuditID && row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         select row1).Distinct().ToList();

                if (flag == "N")
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "N").ToList();
                }
                else
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "P").ToList();
                }
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static List<Mst_Process> GetAllProcess(long customerID, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> eventList = (from row in entities.Mst_Process
                                               where row.IsDeleted == false && row.CustomerID == customerID
                                               select row).ToList();

                if (!string.IsNullOrEmpty(filter))
                {
                    eventList = eventList.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter)).ToList();
                }
                return eventList;
            }
        }
        public static List<Mst_Process> GetProcessFillDropDown(long customerid, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processList = (from row in entities.SP_ImplementationAuditSummary((int)customerid)
                                   join row1 in entities.Mst_Process
                                   on row.ProcessID equals row1.Id
                                   where row.AuditID == AuditID
                                   //&& instanceids.Contains(row.InstanceID)
                                   select row1).Distinct().ToList();
                return processList;
            }
        }
        public static List<Mst_Process> GetProcessFillDropDown(int userid, int Roleid, long customerid, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processList = (from row in entities.SP_ImplementationAuditSummary((int)customerid)
                                   join row1 in entities.Mst_Process
                                   on row.ProcessID equals row1.Id
                                   where row.UserID == userid
                                   && row.RoleID == Roleid
                                   && row.AuditID == AuditID
                                   //&& instanceids.Contains(row.InstanceID)
                                   select row1).Distinct().ToList();
                return processList;
            }
        }
        public static List<Mst_Process> GetAllDepartmentProcess(int Departmentid, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceCategorys = (from row in entities.Mst_Process
                                           where row.IsDeleted == false && row.DepartmentID == Departmentid
                                           select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<Mst_Process> GetAllNew(long Customerid, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceCategorys = (from row in entities.Mst_Process
                                           where row.IsDeleted == false && row.CustomerID == Customerid
                                           select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<Mst_Process> GetAllProcess(string Flag, long customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> MstProcess = new List<Mst_Process>();
                if (Flag == "P")
                {
                    MstProcess = (from row in entities.Mst_Process
                                  where row.IsProcessNonProcess == "P" && row.CustomerID == customerID
                                  select row).ToList();
                }
                else
                {
                    MstProcess = (from row in entities.Mst_Process
                                  where row.IsProcessNonProcess == "N" && row.CustomerID == customerID
                                  select row).ToList();
                }

                return MstProcess.ToList();
            }
        }
        public static void Create(Mst_Process ProcessData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_Process.Add(ProcessData);
                entities.SaveChanges();

            }
        }
        public static void Delete(int PorcessID, long Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_Process eventToDelete = (from row in entities.Mst_Process
                                             where row.Id == PorcessID && row.CustomerID == Customerid
                                             select row).FirstOrDefault();

                eventToDelete.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        public static void Update(Mst_Process ProcessData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_Process eventToUpdate = (from row in entities.Mst_Process
                                             where row.Id == ProcessData.Id
                                             && row.CustomerID == ProcessData.CustomerID
                                             select row).FirstOrDefault();

                eventToUpdate.Name = ProcessData.Name;
                eventToUpdate.DepartmentID = ProcessData.DepartmentID;
                eventToUpdate.Description = ProcessData.Description;
                eventToUpdate.UpdatedBy = ProcessData.UpdatedBy;
                eventToUpdate.UpdatedOn = ProcessData.UpdatedOn;
                eventToUpdate.IsDeleted = false;
                eventToUpdate.IsProcessNonProcess = ProcessData.IsProcessNonProcess;
                entities.SaveChanges();


            }
        }
        public static string GetProcessName(long processId, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.Mst_Process
                                         where row.Id == processId && row.IsDeleted == false
                                         && row.CustomerID == CustomerID
                                         select row.Name).FirstOrDefault();
                return transactionsQuery;
            }
        }

        public static int GetProcessIDByName(string Processname, int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessId = (from row in entities.Mst_Process
                                 where row.Name.ToUpper() == Processname.ToUpper()
                                  && row.CustomerID == CustomerID
                                  && row.IsDeleted == false
                                 select row.Id).SingleOrDefault();

                return Convert.ToInt32(ProcessId);
            }
        }

        public static Mst_Process GetByID(long ProcessID, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.Mst_Process
                            where row.Id == ProcessID && row.IsDeleted == false
                             && row.CustomerID == CustomerID
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static bool Exists(int CustID, string ProcessName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_Process
                             where row.Name.Equals(ProcessName)
                             && row.CustomerID == CustID
                             && row.IsDeleted == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        #endregion Process
        #region Sub Process

        public static bool CreateExcelProcess(List<mst_Subprocess> mstSubprocess)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    mstSubprocess.ForEach(entry =>
                    {
                        entities.mst_Subprocess.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static int GetSubProcessIDByName(string subProcessname, long Processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var stateId = (from row in entities.mst_Subprocess
                               where row.Name.ToUpper().Trim() == subProcessname.ToUpper().Trim() && row.ProcessId == Processid
                               select row.Id).SingleOrDefault();

                return Convert.ToInt32(stateId);
            }
        }
        public static bool ExistsProcess(long Processid, string SubProcessName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Subprocess
                             where row.Name.ToUpper().Trim() == SubProcessName.ToUpper().Trim() &&
                             row.ProcessId == Processid
                             //&& row.IsProcessNonProcess == Flag
                             && row.IsDeleted == false
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                //return query.Select(entry => true).First();
            }
        }
        public static int GetSubProcessIDByName(string subProcessname)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var subprocessid = (from row in entities.mst_Subprocess
                                    where row.Name.ToUpper() == subProcessname.ToUpper()
                                    select row.Id).SingleOrDefault();

                return Convert.ToInt32(subprocessid);
            }
        }

        public static bool SubprocessExists(mst_Subprocess subProcess)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Subprocess
                             where row.IsDeleted == false
                             && row.Name.Equals(subProcess.Name)
                             select row);

                if (subProcess.Id > 0)
                {
                    query = query.Where(entry => entry.Id != subProcess.Id);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static string GetSubProcessName(long processId, long SubprocessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.mst_Subprocess
                                         where row.Id == SubprocessId && row.IsDeleted == false
                                         && row.ProcessId == processId
                                         select row.Name).FirstOrDefault();
                return transactionsQuery;
            }
        }

        public static string GetSubProcessName(int SubProcessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.mst_Subprocess
                                         where row.Id == SubProcessId && row.IsDeleted == false
                                         select row.Name).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static decimal? GetSubProcessScore(int ProcessID, int subProcessID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_Subprocess
                            where row.Id == subProcessID
                            && row.ProcessId == ProcessID
                            select row.Scores).FirstOrDefault();

                if (data != null)
                    return data;
                else
                    return 0;
            }
        }
        public static List<mst_Subprocess> GetSubProcessAll(int customerID, int ProcessId, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<mst_Subprocess> eventList = (from row in entities.mst_Subprocess
                                                  join row1 in entities.Mst_Process
                                                  on row.ProcessId equals row1.Id
                                                  where row.IsDeleted == false && row1.CustomerID == customerID
                                                  && row.ProcessId == ProcessId
                                                  select row).ToList();




                if (!string.IsNullOrEmpty(filter))
                {
                    eventList = eventList.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter)).ToList();
                }

                eventList = eventList.OrderBy(entry => entry.Name).ToList();

                return eventList;
            }
        }

        public static List<mst_Subprocess> GetSubProcessAll(int customerID, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<mst_Subprocess> eventList = (from row in entities.mst_Subprocess
                                                  join row1 in entities.Mst_Process
                                                  on row.ProcessId equals row1.Id
                                                  where row.IsDeleted == false && row1.CustomerID == customerID
                                                  select row).ToList();




                if (!string.IsNullOrEmpty(filter))
                {
                    eventList = eventList.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter)).ToList();
                }

                eventList = eventList.OrderBy(entry => entry.Name).ToList();

                return eventList;
            }
        }
        public static object FillSubProcess(long Processid, string flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Subprocess
                             where row.IsDeleted == false && row.ProcessId == Processid
                             select row);

                if (flag == "N")
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "N");
                }
                else
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "P");
                }
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static object FillSubProcessActivity(long Processid, long SubProcessid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Activity
                             where row.IsDeleted == false && row.ProcessId == Processid
                             && row.SubProcessId == SubProcessid
                             select row);

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillSubProcess(int dDLProcessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Subprocess
                             where row.IsDeleted == false && row.ProcessId == dDLProcessId
                             select row);

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static List<ProcessDetails> GetAll(int ProcessId, long Customerid, long parentID, string filter)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var Subprocess = (from row in entities.mst_Subprocess
                                  join row1 in entities.Mst_Process
                                  on row.ProcessId equals row1.Id
                                  into t
                                  from rt in t.DefaultIfEmpty()
                                  where row.IsDeleted == false && row.ProcessId == ProcessId
                                  && rt.CustomerID == Customerid
                                  select new ProcessDetails()
                                  {
                                      Id = row.Id
                                  ,
                                      SubProcessName = row.Name
                                  ,
                                      Scores = row.Scores
                                  ,
                                      ProcessId = rt.Id
                                  ,
                                      Name = rt.Name
                                  ,
                                      ParentID = row.ParentID
                                  ,
                                      IsDeleted = row.IsDeleted

                                  });


                if (parentID != -1)
                {
                    Subprocess = Subprocess.Where(entry => entry.ParentID == parentID);
                }
                else
                {
                    Subprocess = Subprocess.Where(entry => entry.ParentID == null);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    Subprocess = Subprocess.Where(entry => entry.Name.Contains(filter) || entry.SubProcessName.Contains(filter));
                }

                return Subprocess.ToList();
            }
        }


        public static object GetHierarchy(int ProcessId, long Customerid, long parentCustomerBranchID)
        {
            List<NameValue> hierarchy = new List<NameValue>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (parentCustomerBranchID != -1)
                {
                    long? parentID = parentCustomerBranchID;
                    while (parentID.HasValue)
                    {
                        var eventcatagory = (from row in entities.mst_Subprocess
                                             where row.Id == parentID.Value
                                             select new { row.Id, row.Name, row.ParentID }).SingleOrDefault();

                        hierarchy.Add(new NameValue() { ID = (int)eventcatagory.Id, Name = eventcatagory.Name });
                        parentID = eventcatagory.ParentID;
                    }
                }

                var EventName = (from row in entities.Mst_Process
                                 where row.Id == ProcessId
                                 && row.CustomerID == Customerid
                                 select row.Name).SingleOrDefault();

                hierarchy.Add(new NameValue() { ID = ProcessId, Name = EventName });
            }

            hierarchy.Reverse();

            return hierarchy;
        }
        public static void SubprocessUpdate(mst_Subprocess subProcess)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Subprocess subEventToUpdate = (from row in entities.mst_Subprocess
                                                   where row.ProcessId == subProcess.ProcessId
                                                   && row.Id == subProcess.Id
                                                   select row).FirstOrDefault();

                subEventToUpdate.Name = subProcess.Name;
                subEventToUpdate.Description = subProcess.Description;
                subEventToUpdate.UpdatedBy = subProcess.UpdatedBy;
                subEventToUpdate.Scores = subProcess.Scores;
                subEventToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static void SubprocessCreate(mst_Subprocess subProcess)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                subProcess.IsDeleted = false;
                subProcess.CreatedOn = DateTime.UtcNow;
                subProcess.UpdatedOn = DateTime.UtcNow;
                entities.mst_Subprocess.Add(subProcess);
                entities.SaveChanges();
            }
        }
        public static mst_Subprocess GetSubProcessByID(long subProcessID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_Subprocess
                            where row.Id == subProcessID
                            && row.IsDeleted == false
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static void SubEventDelete(int ProcessID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<mst_Subprocess> subEventList = new List<mst_Subprocess>();
                mst_Subprocess eventSubObject = new mst_Subprocess() { Id = ProcessID };
                entities.mst_Subprocess.Attach(eventSubObject);
                eventSubObject.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        public static bool SubprocessExists(mst_Subprocess subProcess, int PorcessID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Subprocess
                             where row.IsDeleted == false
                             && row.Name.Equals(subProcess.Name) && row.ProcessId == PorcessID
                             select row);

                if (subProcess.Id > 0)
                {
                    query = query.Where(entry => entry.Id != subProcess.Id);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }


        public static mst_Subprocess GetAllSubprocessName(string subprocessname)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_Subprocess
                            where row.Name.ToUpper().Trim() == subprocessname.ToUpper().Trim()
                            && row.IsDeleted == false
                            select row).FirstOrDefault();
                if (data != null)
                {
                    return data;

                }
                else
                {
                    return null;
                }
            }
        }

        public static int GetSubProcessBYName(long Processid, string subprocessname)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_Subprocess
                            where row.ProcessId == Processid
                             && row.Name.ToUpper() == subprocessname.ToUpper()
                             && row.IsDeleted == false
                            select row.Id).FirstOrDefault();
                if (data != 0)
                {
                    return Convert.ToInt32(data);
                }
                else
                {
                    return -1;
                }

            }
        }
        #endregion Sub Process


        #region Activity
        public static List<ActivityView> GetAll(int ProcessId, long SubprocessID, long Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //List<ActivityDetails> aa = new List<ActivityDetails>();
                //return aa;
                var ActivityDetails = (from row in entities.ActivityViews

                                       where row.ProcessId == ProcessId
                                       && row.SubProcessId == SubprocessID
                                       && row.CustomerID == Customerid
                                       select row).ToList();


                if (ActivityDetails != null)
                {
                    return ActivityDetails;
                }
                else
                {
                    return null;
                }

            }
        }
        public static object GetHierarchyActivity(int subProcessId)
        {
            List<NameValue> hierarchy = new List<NameValue>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var SubprocessName = (from row in entities.mst_Subprocess
                                      where row.Id == subProcessId
                                      select row.Name).SingleOrDefault();

                hierarchy.Add(new NameValue() { ID = subProcessId, Name = SubprocessName });
            }

            hierarchy.Reverse();

            return hierarchy;
        }
        public static bool ExistsActivity(long Processid, long SubProcessId, string ActivityName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Activity
                             where row.ProcessId == Processid
                             && row.SubProcessId == SubProcessId
                             && row.Name.ToUpper().Trim() == ActivityName.ToUpper().Trim()
                             && row.IsDeleted == false
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                //return query.Select(entry => true).First();
            }
        }
        public static bool ActivityExists(mst_Activity subactivity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Activity
                             where row.IsDeleted == false
                             && row.SubProcessId == subactivity.SubProcessId
                             && row.Name.Equals(subactivity.Name)
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static void SubActivityUpdate(mst_Activity subActivity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Activity subEventToUpdate = (from row in entities.mst_Activity
                                                 where row.ProcessId == subActivity.ProcessId
                                                 && row.SubProcessId == subActivity.SubProcessId
                                                   && row.Id == subActivity.Id
                                                 select row).FirstOrDefault();

                subEventToUpdate.Name = subActivity.Name;
                subEventToUpdate.UpdatedBy = subActivity.UpdatedBy;
                subEventToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static void SubActivityCreate(mst_Activity subActivity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                subActivity.IsDeleted = false;
                subActivity.CreatedOn = DateTime.Now;
                subActivity.UpdatedOn = DateTime.Now;
                entities.mst_Activity.Add(subActivity);
                entities.SaveChanges();
            }
        }
        public static mst_Activity GetSubActivityByID(long ActivityID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_Activity
                            where row.Id == ActivityID
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static void SubActivityDelete(int ActivityID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Activity eventSubObject = new mst_Activity() { Id = ActivityID };
                entities.mst_Activity.Attach(eventSubObject);
                eventSubObject.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        public static bool CreateExcelActivity(List<mst_Activity> mstActivity)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    mstActivity.ForEach(entry =>
                    {
                        entities.mst_Activity.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public static int GetActivityBYName(long Processid, long SubProcessID, string Activityname)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_Activity
                            where row.ProcessId == Processid
                            && row.SubProcessId == SubProcessID
                             && row.Name.ToUpper() == Activityname.ToUpper()
                             && row.IsDeleted == false
                            select row.Id).FirstOrDefault();
                if (data != 0)
                {
                    return Convert.ToInt32(data);
                }
                else
                {
                    return -1;
                }

            }
        }
        #endregion
        public static int GetCountOfProcessAndBranchwiseChecklistCount(int ProcessId, int Customerbranchid, int VerticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var a = (from row in entities.RiskActivityToBeDoneMappings
                         join row1 in entities.RiskActivityTransactions
                         on row.RiskActivityId equals row1.Id
                         where row.ProcessId == ProcessId
                         && row1.CustomerBranchId == Customerbranchid
                         && row1.VerticalsId == VerticalId
                         select row).ToList().Count;

                return a;
            }
        }

        public static string GetLocationTypeName(int LocationTypeid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.Mst_LocationMaster
                                         where row.ID == LocationTypeid && row.IsActive == false
                                         select row.Name).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static int GetLocationTypeID(string LocationType)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.Mst_LocationMaster
                                         where row.Name == LocationType && row.IsActive == false
                                         select row.ID).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static string GetClientnameName(long BranchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.mst_CustomerBranch
                                         where
                                         row.ID == BranchId && row.IsDeleted == false && row.Status == 1
                                         select row.Name).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static string GetRiskCategoryName(string flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = "";
                if (flag == "I")
                {
                    transactionsQuery = (from row in entities.mst_Riskcategory
                                         where
                                         row.IsICRRisk == "I" && row.IsDeleted == false
                                         select row.IsICRRisk).FirstOrDefault();
                }
                else
                {
                    transactionsQuery = (from row in entities.mst_Riskcategory
                                         where
                                         row.IsICRRisk == "R" && row.IsDeleted == false
                                         select row.IsICRRisk).FirstOrDefault();
                }
                return transactionsQuery;
            }
        }
        public static string GetRiskCategoryName(long categoryid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.mst_Riskcategory
                                         where
                                         row.Id == categoryid && row.IsDeleted == false
                                         select row.Name).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static bool TestingUIExists(long RiskCreationId, long CustomerBranchID, long ProcessId, long SubProcessId, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (ProcessId != -1 && SubProcessId != -1)
                {
                    var query = (from row in entities.Mst_RiskResult
                                 where row.RiskCreationId == RiskCreationId
                                     && row.CustomerBranchId == CustomerBranchID
                                     && row.FinancialYear == FinancialYear
                                     && //row.ProcessId == ProcessId && row.SubProcessId == SubProcessId &&
                                     row.IsDeleted == false
                                 select row);
                    return query.Select(entry => true).SingleOrDefault();
                }
                else
                {
                    var query = (from row in entities.Mst_RiskResult
                                 where row.RiskCreationId == RiskCreationId
                                     && row.CustomerBranchId == CustomerBranchID
                                      && row.FinancialYear == FinancialYear
                                     &&// row.ProcessId == ProcessId &&
                                     row.IsDeleted == false
                                 select row);
                    return query.Select(entry => true).SingleOrDefault();
                }

            }
        }

        public static bool Exists(List<ControlTestingAssignmentCheckExistView> CTACEV, long RiskCreationId, long CustomerBranchID, long ProcessId, long SubProcessId, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in CTACEV
                             where row.RiskCreationId == RiskCreationId
                                 && row.CustomerBranchID == CustomerBranchID
                                 && row.ProcessId == ProcessId && row.SubProcessId == SubProcessId
                                 && row.FinancialYear == FinancialYear &&
                                 row.IsDeleted == false
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static bool Exists(long RiskCreationId, long CustomerBranchID, long ProcessId, long SubProcessId, string FinancialYear, string ForPeriod)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.AuditInstances
                             join row1 in entities.AuditScheduleOns
                             on row.ID equals row1.AuditInstanceId
                             where row.RiskCreationId == row1.RiskCreationID && row.RiskCreationId == RiskCreationId
                                 && row.CustomerBranchID == CustomerBranchID
                                 && row.ProcessId == ProcessId
                                 && row.SubProcessId == SubProcessId
                                 && row1.FinancialYear == FinancialYear
                                 && row.IsDeleted == false
                                 && row1.ForMonth == ForPeriod
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }

        #region Auditor Master
        public static void CreateAuditorMaster(Mst_AuditorMaster mstauditormaster)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_AuditorMaster.Add(mstauditormaster);
                entities.SaveChanges();
            }
        }



        public static void UpdateAuditorMaster(Mst_AuditorMaster mstauditormaster)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Mst_AuditorMaster mstauditormasterToUpdate = (from row in entities.Mst_AuditorMaster
                                                              where row.ID == mstauditormaster.ID
                                                              select row).FirstOrDefault();


                mstauditormasterToUpdate.Name = mstauditormaster.Name;
                mstauditormasterToUpdate.Partnername = mstauditormaster.Partnername;
                mstauditormasterToUpdate.PartnerEmail = mstauditormaster.PartnerEmail;
                mstauditormasterToUpdate.PContactNo = mstauditormaster.PContactNo;
                mstauditormasterToUpdate.AuditMName = mstauditormaster.AuditMName;
                mstauditormasterToUpdate.AuditMEmail = mstauditormaster.AuditMEmail;
                //mstauditormasterToUpdate.AuditMContactNo = mstauditormaster.AuditMEmail;
                mstauditormasterToUpdate.AuditMContactNo = mstauditormaster.AuditMContactNo;  // Change by Hardik Sapara...
                mstauditormasterToUpdate.CreatedBy = null;
                mstauditormasterToUpdate.CreatedOn = DateTime.Now;
                entities.SaveChanges();
            }

        }
        public static Mst_AuditorMaster AuditorMasterGetByID(int AuditerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_AuditorMaster
                                        where row.ID == AuditerID
                                        select row).SingleOrDefault();

                return MstAuditorMaster;
            }
        }
        public static void DeleteAuditorMaster(int AuditerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_AuditorMaster AuditorMastertoDelete = (from row in entities.Mst_AuditorMaster
                                                           where row.ID == AuditerID
                                                           select row).FirstOrDefault();

                AuditorMastertoDelete.IsActive = true;
                entities.SaveChanges();
            }
        }
        public static List<Mst_AuditorMaster> GetAllAuditorMaster(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorMaster = (from row in entities.Mst_AuditorMaster
                                     where row.IsActive == false && row.CustomerID == customerID
                                     select row);

                return AuditorMaster.ToList();
            }
        }

        public static List<Mst_AuditorMaster> GetAllList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorMasterList = (from row in entities.Mst_AuditorMaster
                                         where row.IsActive == false
                                         select row);

                AuditorMasterList = AuditorMasterList.OrderBy(entry => entry.Name);

                return AuditorMasterList.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static bool riskCategorycreationExists(string activitydescription, int Customerbranchid, int processid, int subprocessid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskCategoryCreations
                             where row.ActivityDescription.Trim().ToUpper() == activitydescription
                             && row.CustomerBranchId == Customerbranchid
                             && row.ProcessId == processid
                             && row.SubProcessId == subprocessid
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool AuditorNameExists(string AuditorName, int Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_AuditorMaster
                             where row.CustomerID == Customerid
                             && row.Name.ToUpper().Trim().Equals(AuditorName.ToUpper().Trim())
                             && row.IsActive == false
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool AuditorEmailExists(string Email, int Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_AuditorMaster
                             where row.CustomerID == Customerid
                             && row.PartnerEmail.ToUpper().Trim().Equals(Email.ToUpper().Trim()) && row.IsActive == false
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion
        #region Auditor User Master
        public static void CreateAuditorUserMaster(Mst_AuditorUserMaster mstauditorusermaster)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_AuditorUserMaster.Add(mstauditorusermaster);
                entities.SaveChanges();
            }
        }
        public static void UpdateAuditorUserMaster(Mst_AuditorUserMaster mstauditorUsermaster)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Mst_AuditorUserMaster mstAuditorUserMasterToUpdate = (from row in entities.Mst_AuditorUserMaster
                                                                      where row.ID == mstauditorUsermaster.ID
                                                                      select row).FirstOrDefault();

                mstAuditorUserMasterToUpdate.AuditorId = mstauditorUsermaster.AuditorId;
                mstAuditorUserMasterToUpdate.FirstName = mstauditorUsermaster.FirstName;
                mstAuditorUserMasterToUpdate.LastName = mstauditorUsermaster.LastName;
                mstAuditorUserMasterToUpdate.Address = mstauditorUsermaster.Address;
                mstAuditorUserMasterToUpdate.EmailID = mstauditorUsermaster.EmailID;
                mstAuditorUserMasterToUpdate.ContactNo = mstauditorUsermaster.ContactNo;
                mstAuditorUserMasterToUpdate.CreatedBy = null;
                mstAuditorUserMasterToUpdate.CreatedOn = DateTime.Now;
                entities.SaveChanges();
            }

        }
        public static void DeleteAuditorUserMaster(int AuditerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_AuditorUserMaster AuditorMastertoDelete = (from row in entities.Mst_AuditorUserMaster
                                                               where row.ID == AuditerID
                                                               select row).FirstOrDefault();

                AuditorMastertoDelete.IsActive = true;
                entities.SaveChanges();
            }
        }
        public static Mst_AuditorUserMaster AuditorUserMasterGetByID(int AuditerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_AuditorUserMaster
                                        where row.ID == AuditerID
                                        select row).SingleOrDefault();

                return MstAuditorMaster;
            }
        }
        public static object GetAllAuditorUserMaster(int AuditorID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMaster = (from row in entities.Mst_AuditorUserMaster
                                         where row.IsActive == false && row.AuditorId == AuditorID
                                         select row);

                return AuditorUserMaster.ToList();
            }
        }
        public static List<Mst_AuditorUserMaster> GetAllAuditorUserMasterList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMasterList = (from row in entities.Mst_AuditorUserMaster
                                             where row.IsActive == false
                                             select row);

                AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.FirstName);

                return AuditorUserMasterList.OrderBy(entry => entry.FirstName).ToList();
            }
        }
        public static bool AuditorUserExists(string Email, int AuditorID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_AuditorUserMaster
                             where row.EmailID.Equals(Email) && row.AuditorId == AuditorID
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        #endregion
        #region Auditor Period Master
        public static void CreateAuditorPeriodMaster(Mst_AuditPeriodMaster MstAuditPeriodMaster)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_AuditPeriodMaster.Add(MstAuditPeriodMaster);
                entities.SaveChanges();
            }
        }
        public static void UpdateAuditorPeriodMaster(Mst_AuditPeriodMaster MstAuditPeriodMaster)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Mst_AuditPeriodMaster mstAuditorPeriodMasterToUpdate = (from row in entities.Mst_AuditPeriodMaster
                                                                        where row.ID == MstAuditPeriodMaster.ID
                                                                        select row).FirstOrDefault();
                mstAuditorPeriodMasterToUpdate.Name = MstAuditPeriodMaster.Name;
                entities.SaveChanges();
            }

        }
        public static void DeleteAuditorPeriodMaster(int AuditerPeriodID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_AuditPeriodMaster AuditorPeriodMastertoDelete = (from row in entities.Mst_AuditPeriodMaster
                                                                     where row.ID == AuditerPeriodID
                                                                     select row).FirstOrDefault();

                AuditorPeriodMastertoDelete.IsActive = true;
                entities.SaveChanges();
            }
        }
        public static Mst_AuditPeriodMaster AuditorPeriodMasterGetByID(int AuditerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_AuditPeriodMaster
                                        where row.ID == AuditerID
                                        select row).SingleOrDefault();

                return MstAuditorMaster;
            }
        }
        //public static object GetAllAuditorPeriodMaster()
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var AuditorMaster = (from row in entities.Mst_AuditPeriodMaster
        //                             where row.IsActive == false
        //                             select row);

        //        return AuditorMaster.ToList();
        //    }
        //}

        public static List<Mst_AuditPeriodMaster> GetAllAuditorPeriodMaster()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorMaster = (from row in entities.Mst_AuditPeriodMaster
                                     where row.IsActive == false
                                     select row);

                return AuditorMaster.ToList();
            }
        }
        public static List<Mst_AuditPeriodMaster> GetAllPeriodList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorMasterList = (from row in entities.Mst_AuditPeriodMaster
                                         where row.IsActive == false
                                         select row);

                AuditorMasterList = AuditorMasterList.OrderBy(entry => entry.Name);

                return AuditorMasterList.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static bool AuditPeriodExists(string PeriodName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_AuditPeriodMaster
                             where row.Name.Equals(PeriodName) && row.IsActive == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        #endregion

        #region Category
        public static bool CategoryExists(string CategoryName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Riskcategory
                             where row.Name.Equals(CategoryName) && row.IsDeleted == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void CategoryCreate(mst_Riskcategory CategoryData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_Riskcategory.Add(CategoryData);
                entities.SaveChanges();

            }
        }
        public static void CategoryUpdate(mst_Riskcategory CategoryData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Riskcategory eventToUpdate = (from row in entities.mst_Riskcategory
                                                  where row.Id == CategoryData.Id
                                                  select row).FirstOrDefault();

                eventToUpdate.Name = CategoryData.Name;
                eventToUpdate.Description = CategoryData.Description;
                eventToUpdate.IsDeleted = false;
                entities.SaveChanges();
            }
        }
        public static void CategoryDelete(int categoryID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_Riskcategory eventToDelete = (from row in entities.mst_Riskcategory
                                                  where row.Id == categoryID
                                                  select row).FirstOrDefault();

                eventToDelete.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        public static List<mst_Riskcategory> GetAllCategory()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<mst_Riskcategory> categoryList = (from row in entities.mst_Riskcategory
                                                       where row.IsDeleted == false
                                                       select row).ToList();


                return categoryList;
            }
        }

        public static mst_Riskcategory CategoryGetByID(long categoryID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_Riskcategory
                            where row.Id == categoryID
                            select row).FirstOrDefault();
                return data;
            }
        }
        #endregion Category
        #region Risk and Control Rating

        public static string Get_Risk_ControlRating(string flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = "";
                if (flag == "R")
                {
                    transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                         where
                                         row.IsRiskControl == "R" && row.IsActive == false
                                         select row.IsRiskControl).FirstOrDefault();
                }
                else
                {
                    transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                         where
                                         row.IsRiskControl == "C" && row.IsActive == false
                                         select row.IsRiskControl).FirstOrDefault();
                }
                return transactionsQuery;
            }
        }

        public static bool RiskControlRatingExists(string CategoryName, string isriskcontrol)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Risk_ControlRating
                             where row.Name.Equals(CategoryName) && row.IsActive == false
                             && row.IsRiskControl == isriskcontrol
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void RiskControlRatingCreate(mst_Risk_ControlRating CategoryData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_Risk_ControlRating.Add(CategoryData);
                entities.SaveChanges();

            }
        }
        public static void RiskControlRatingUpdate(mst_Risk_ControlRating CategoryData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Risk_ControlRating eventToUpdate = (from row in entities.mst_Risk_ControlRating
                                                        where row.Id == CategoryData.Id && row.IsActive == false
                                                        select row).FirstOrDefault();

                eventToUpdate.Name = CategoryData.Name;
                eventToUpdate.Value = CategoryData.Value;
                eventToUpdate.IsRiskControl = CategoryData.IsRiskControl;
                eventToUpdate.IsActive = false;
                entities.SaveChanges();
            }
        }
        public static void RiskControlRatingDelete(int categoryID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_Risk_ControlRating eventToDelete = (from row in entities.mst_Risk_ControlRating
                                                        where row.Id == categoryID
                                                        select row).FirstOrDefault();

                eventToDelete.IsActive = true;
                entities.SaveChanges();
            }
        }
        public static mst_Risk_ControlRating RiskControlRatingGetByIDFetchDetail(long categoryID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_Risk_ControlRating
                            where row.Id == categoryID && row.IsActive == true
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static List<mst_Risk_ControlRating> GetAllRiskControlRating(string flag, string Flagall)
        {
            List<mst_Risk_ControlRating> categoryList = new List<mst_Risk_ControlRating>();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (Flagall == "A")
                {
                    categoryList = (from row in entities.mst_Risk_ControlRating
                                    where row.IsActive == false
                                    select row).ToList();
                }
                else
                {
                    categoryList = (from row in entities.mst_Risk_ControlRating
                                    where row.IsActive == false && row.IsRiskControl == flag
                                    select row).ToList();
                }

                return categoryList.ToList();
            }
        }

        public static mst_Risk_ControlRating RiskControlRatingGetByID(long categoryID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_Risk_ControlRating
                            where row.Id == categoryID && row.IsActive == false
                            select row).FirstOrDefault();
                return data;
            }
        }
        #endregion Category

        public static void UpdateTMP_ImplementationAssignment(TMP_ImplementationAssignment TMPImplementationAssignment)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var mstAuditorUserMasterToUpdate = (from row in entities.TMP_ImplementationAssignment
                                                    where row.CustomerBranchID == TMPImplementationAssignment.CustomerBranchID
                                                              && row.FinancialYear == TMPImplementationAssignment.FinancialYear
                                                              //&& row.ForPeriod == TMPImplementationAssignment.ForPeriod
                                                              && row.Status == 0
                                                    select row).ToList();

                mstAuditorUserMasterToUpdate.ForEach(entry => entry.Status = TMPImplementationAssignment.Status);

                entities.SaveChanges();
            }

        }
        public static void UpdateTMP_Assignment(TMP_Assignment TMPAssignment)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var mstAuditorUserMasterToUpdate = (from row in entities.TMP_Assignment
                                                    where row.CustomerBranchID == TMPAssignment.CustomerBranchID
                                                              && row.FinancialYear == TMPAssignment.FinancialYear
                                                              && row.ProcessId == TMPAssignment.ProcessId
                                                              && row.ForPeriod == TMPAssignment.ForPeriod
                                                              && row.Status == 0
                                                    select row).ToList();

                mstAuditorUserMasterToUpdate.ForEach(entry => entry.Status = TMPAssignment.Status);

                entities.SaveChanges();
            }

        }
        #region Audit Assignment
        public static void CreateInternalAuditAssignment(Internal_AuditAssignment internalauditassignment)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Internal_AuditAssignment.Add(internalauditassignment);
                entities.SaveChanges();
            }
        }
        public static void UpdateInternalAuditAssignment(Internal_AuditAssignment internalauditassignment)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Internal_AuditAssignment mstAuditorUserMasterToUpdate = (from row in entities.Internal_AuditAssignment
                                                                         where row.CustomerBranchid == internalauditassignment.CustomerBranchid
                                                                         && row.VerticalID == internalauditassignment.VerticalID
                                                                         && row.IsActive == false
                                                                         select row).FirstOrDefault();

                mstAuditorUserMasterToUpdate.AssignedTo = internalauditassignment.AssignedTo;
                mstAuditorUserMasterToUpdate.ExternalAuditorId = internalauditassignment.ExternalAuditorId;
                mstAuditorUserMasterToUpdate.UpdatedBy = internalauditassignment.UpdatedBy;
                mstAuditorUserMasterToUpdate.UpdatedOn = DateTime.Now;
                //mstAuditorUserMasterToUpdate.CustomerId = internalauditassignment.CustomerId;
                entities.SaveChanges();
            }

        }


        public static Internal_AuditAssignment InternalAuditAssignmentGetByID(int branchid, int verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Internal_AuditAssignment
                                        where row.CustomerBranchid == branchid
                                        && row.VerticalID == verticalid
                                        && row.IsActive == false
                                        select row).FirstOrDefault();

                return MstAuditorMaster;
            }
        }
        //public static Internal_AuditAssignment InternalAuditAssignmentGetByID(int branchid)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var MstAuditorMaster = (from row in entities.Internal_AuditAssignment
        //                                where row.CustomerBranchid == branchid
        //                                select row).SingleOrDefault();

        //        return MstAuditorMaster;
        //    }
        //}
        public static List<Internal_AuditAssignment> GetAllInternalAuditAssignment(int customerid, int branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Internal_AuditAssignment> AuditorUserMaster = new List<Internal_AuditAssignment>();
                if (customerid != -1 && branchid != -1)
                {
                    AuditorUserMaster = (from row in entities.Internal_AuditAssignment
                                         where row.IsActive == false && row.CustomerBranchid == branchid
                                         && row.CustomerId == customerid
                                         select row).ToList();
                }
                else if (branchid != -1)
                {
                    AuditorUserMaster = (from row in entities.Internal_AuditAssignment
                                         where row.IsActive == false && row.CustomerBranchid == branchid
                                         && row.CustomerId == customerid
                                         select row).ToList();
                }
                else if (customerid != -1)
                {
                    AuditorUserMaster = (from row in entities.Internal_AuditAssignment
                                         where row.IsActive == false
                                         && row.CustomerId == customerid
                                         select row).ToList();
                    if (AuditorUserMaster != null)
                    {
                        //AuditorUserMaster = (from row in entities.mst_CustomerBranch
                        //                     where row.IsDeleted == false
                        //                     && row.CustomerID == customerid
                        //                     select row).ToList();
                    }
                }
                return AuditorUserMaster.ToList();
            }
        }
        public static List<Internal_AuditAssignment> GetAllInternalAuditAssignmentList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMasterList = (from row in entities.Internal_AuditAssignment
                                             where row.IsActive == false
                                             select row);

                AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.CustomerBranchid);

                return AuditorUserMasterList.OrderBy(entry => entry.CustomerBranchid).ToList();
            }
        }
        #endregion


        #region Process Rating
        public static void CreateMst_ProcessRating(Mst_ProcessRating mstprocessrating)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_ProcessRating.Add(mstprocessrating);
                entities.SaveChanges();
            }
        }
        public static void UpdateMst_ProcessRating(Mst_ProcessRating mstprocessrating)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Mst_ProcessRating mstAuditorUserMasterToUpdate = (from row in entities.Mst_ProcessRating
                                                                  where row.CustomerBranchid == mstprocessrating.CustomerBranchid
                                                                  && row.ProcessID == mstprocessrating.ProcessID && row.IsActive == false
                                                                  select row).FirstOrDefault();

                mstAuditorUserMasterToUpdate.ProcessID = mstprocessrating.ProcessID;
                mstAuditorUserMasterToUpdate.Rating = mstprocessrating.Rating;
                mstAuditorUserMasterToUpdate.UpdatedBy = mstprocessrating.UpdatedBy;
                mstAuditorUserMasterToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }

        }
        public static Mst_ProcessRating Mst_ProcessRatingGetByID(int branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_ProcessRating
                                        where row.CustomerBranchid == branchid
                                        select row).SingleOrDefault();

                return MstAuditorMaster;
            }
        }
        //public static object GetAllMst_ProcessRating(int branchid)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var AuditorUserMaster = (from row in entities.Mst_ProcessRating
        //                                 where row.IsActive == false && row.CustomerBranchid == branchid
        //                                 select row);

        //        return AuditorUserMaster.ToList();
        //    }
        //}

        public static List<Mst_ProcessRating> GetAllMst_ProcessRating(int branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMaster = (from row in entities.Mst_ProcessRating
                                         where row.IsActive == false && row.CustomerBranchid == branchid
                                         select row);

                return AuditorUserMaster.ToList();
            }
        }
        public static List<Mst_ProcessRating> GetAllMst_ProcessRatingList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMasterList = (from row in entities.Mst_ProcessRating
                                             where row.IsActive == false
                                             select row);

                AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.CustomerBranchid);

                return AuditorUserMasterList.OrderBy(entry => entry.CustomerBranchid).ToList();
            }
        }
        #endregion


        #region ObservationCategory
        public static void CreateObservationCategory(Mst_ObservationCategory mstprocessrating)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_ObservationCategory.Add(mstprocessrating);
                entities.SaveChanges();
            }
        }
        public static void UpdateObservationCategory(Mst_ObservationCategory mstprocessrating)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Mst_ObservationCategory mstAuditorUserMasterToUpdate = (from row in entities.Mst_ObservationCategory
                                                                        where row.ID == mstprocessrating.ID
                                                                        && row.IsActive == false
                                                                        select row).FirstOrDefault();

                mstAuditorUserMasterToUpdate.Name = mstprocessrating.Name;
                entities.SaveChanges();
            }

        }
        public static Mst_ObservationCategory ObservationCategoryGetByID(int ID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_ObservationCategory
                                        where row.ID == ID
                                        select row).SingleOrDefault();

                return MstAuditorMaster;
            }
        }
        public static List<Mst_ObservationCategory> GetAllObservationCategory()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMaster = (from row in entities.Mst_ObservationCategory
                                         where row.IsActive == false
                                         select row);

                return AuditorUserMaster.ToList();
            }
        }
        public static List<Mst_ObservationCategory> GetAllObservationCategoryList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMasterList = (from row in entities.Mst_ObservationCategory
                                             where row.IsActive == false
                                             select row);

                AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.ID);

                return AuditorUserMasterList.OrderBy(entry => entry.ID).ToList();
            }
        }

        public static void DeleteObservationCategory(int AuditerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_ObservationCategory AuditorMastertoDelete = (from row in entities.Mst_ObservationCategory
                                                                 where row.ID == AuditerID
                                                                 select row).FirstOrDefault();

                AuditorMastertoDelete.IsActive = true;
                entities.SaveChanges();
            }
        }
        public static bool ObservationCategoryExists(string CategoryName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_ObservationCategory
                             where row.Name.Equals(CategoryName) && row.IsActive == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        #endregion

        #region LocationMaster
        public static void CreateLocationMaster(Mst_LocationMaster mstprocessrating)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_LocationMaster.Add(mstprocessrating);
                entities.SaveChanges();
            }
        }
        public static void UpdateLocationMaster(Mst_LocationMaster mstprocessrating)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Mst_LocationMaster mstAuditorUserMasterToUpdate = (from row in entities.Mst_LocationMaster
                                                                   where row.ID == mstprocessrating.ID
                                                                   && row.IsActive == false
                                                                   select row).FirstOrDefault();

                mstAuditorUserMasterToUpdate.Name = mstprocessrating.Name;
                entities.SaveChanges();
            }

        }
        public static Mst_LocationMaster LocationMasterGetByID(int ID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_LocationMaster
                                        where row.ID == ID
                                        select row).SingleOrDefault();

                return MstAuditorMaster;
            }
        }
        public static List<Mst_LocationMaster> GetAllLocationMaster()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMaster = (from row in entities.Mst_LocationMaster
                                         where row.IsActive == false
                                         select row);

                return AuditorUserMaster.ToList();
            }
        }
        public static List<Mst_LocationMaster> GetAllLocationMasterList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMasterList = (from row in entities.Mst_LocationMaster
                                             where row.IsActive == false
                                             select row);

                AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.ID);

                return AuditorUserMasterList.OrderBy(entry => entry.ID).ToList();
            }
        }
        public static void DeleteLocationMaster(int AuditerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_LocationMaster AuditorMastertoDelete = (from row in entities.Mst_LocationMaster
                                                            where row.ID == AuditerID
                                                            select row).FirstOrDefault();

                AuditorMastertoDelete.IsActive = true;
                entities.SaveChanges();
            }
        }
        public static bool LocationExists(string LocationName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_LocationMaster
                             where row.Name.Equals(LocationName) && row.IsActive == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        #endregion
        #region InternalAuditorScheduling
        public static void UpdateInternalAuditorScheduling(InternalAuditScheduling IAS)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                InternalAuditScheduling IASToUpdate = (from row in entities.InternalAuditSchedulings
                                                       where row.Id == IAS.Id
                                                       select row).FirstOrDefault();

                IASToUpdate.StartDate = IAS.StartDate;
                IASToUpdate.EndDate = IAS.EndDate;

                entities.SaveChanges();
            }
        }

        #endregion

        #region Audit Frequency
        public static void CreateAuditFrequency(Mst_AuditFrequency Mstauditfrequency)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_AuditFrequency.Add(Mstauditfrequency);
                entities.SaveChanges();
            }
        }
        public static void UpdateAuditFrequency(Mst_AuditFrequency Mstauditfrequency)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Mst_AuditFrequency mstAuditorUserMasterToUpdate = (from row in entities.Mst_AuditFrequency
                                                                   where row.ID == Mstauditfrequency.ID
                                                                   && row.IsActive == false
                                                                   select row).FirstOrDefault();

                mstAuditorUserMasterToUpdate.Rating = Mstauditfrequency.Rating;
                mstAuditorUserMasterToUpdate.Frequency = Mstauditfrequency.Frequency;
                mstAuditorUserMasterToUpdate.Customerbranchid = Mstauditfrequency.Customerbranchid;
                entities.SaveChanges();
            }

        }
        public static Mst_AuditFrequency Mst_AuditFrequencyGetByID(int ID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_AuditFrequency
                                        where row.ID == ID
                                        select row).SingleOrDefault();

                return MstAuditorMaster;
            }
        }
        public static List<Mst_AuditFrequency> GetAllAuditFrequency(int customerid, int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_AuditFrequency> AuditorUserMaster = new List<Mst_AuditFrequency>();
                if (Branchid == -1)
                {
                    AuditorUserMaster = (from row in entities.Mst_AuditFrequency
                                         join row1 in entities.mst_CustomerBranch
                                         on row.Customerbranchid equals row1.ID
                                         where row.IsActive == false && row1.IsDeleted == false && row1.Status == 1 && row1.CustomerID == customerid
                                         select row).ToList();
                }
                else
                {
                    AuditorUserMaster = (from row in entities.Mst_AuditFrequency
                                         join row1 in entities.mst_CustomerBranch
                                         on row.Customerbranchid equals row1.ID
                                         where row.IsActive == false && row1.CustomerID == customerid
                                         && row1.IsDeleted == false && row1.Status == 1
                                         && row.Customerbranchid == Branchid
                                         select row).ToList();

                }
                return AuditorUserMaster.ToList();
            }
        }
        //public static object GetAllAuditFrequency(int customerid,int Branchid)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<Mst_AuditFrequency> AuditorUserMaster =new List<Mst_AuditFrequency>();
        //        if (Branchid == -1)
        //        {                   
        //            AuditorUserMaster = (from row in entities.Mst_AuditFrequency
        //                                 join row1 in entities.mst_CustomerBranch
        //                                 on row.Customerbranchid equals row1.ID
        //                                 where row.IsActive == false && row1.CustomerID == customerid
        //                                 select row).ToList();
        //        }
        //        else
        //        {
        //            AuditorUserMaster = (from row in entities.Mst_AuditFrequency
        //                                 join row1 in entities.mst_CustomerBranch
        //                                 on row.Customerbranchid equals row1.ID
        //                                 where row.IsActive == false && row1.CustomerID == customerid
        //                                 && row.Customerbranchid == Branchid
        //                                 select row).ToList();

        //        }
        //        return AuditorUserMaster.ToList();
        //    }
        //}
        public static List<Mst_AuditFrequency> GetAllAuditFrequencyList(int customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var AuditorUserMasterList = (from row in entities.Mst_AuditFrequency
                                             join row1 in entities.mst_CustomerBranch
                                             on row.Customerbranchid equals row1.ID
                                             where row.IsActive == false && row1.IsDeleted == false && row1.Status == 1 && row1.CustomerID == customerid
                                             select row);

                //var AuditorUserMasterList = (from row in entities.Mst_AuditFrequency
                //                             where row.IsActive == false
                //                             select row);

                AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.ID);

                return AuditorUserMasterList.OrderBy(entry => entry.ID).ToList();
            }
        }
        public static void DeleteAuditFrequency(int AuditerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_AuditFrequency AuditorMastertoDelete = (from row in entities.Mst_AuditFrequency
                                                            where row.ID == AuditerID
                                                            select row).FirstOrDefault();

                AuditorMastertoDelete.IsActive = true;
                entities.SaveChanges();
            }
        }
        public static bool AuditFrequencyExists(int Rating, long Customerbranchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_AuditFrequency
                             where row.IsActive == false
                             && row.Rating == Rating
                             && row.Customerbranchid == Customerbranchid
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        #endregion


        #region Audit Matrix
        public static void CreateAuditMatrix(Mst_AuditMatrix mstauditmatrix)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_AuditMatrix.Add(mstauditmatrix);
                entities.SaveChanges();
            }
        }
        public static void UpdateAuditMatrix(Mst_AuditMatrix mstauditmatrix)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Mst_AuditMatrix mstAuditorUserMasterToUpdate = (from row in entities.Mst_AuditMatrix
                                                                where row.ID == mstauditmatrix.ID
                                                                && row.IsActive == false
                                                                select row).FirstOrDefault();

                mstAuditorUserMasterToUpdate.Rating = mstauditmatrix.Rating;
                mstAuditorUserMasterToUpdate.Lessthan = mstauditmatrix.Lessthan;
                mstAuditorUserMasterToUpdate.Greaterthan = mstauditmatrix.Greaterthan;
                entities.SaveChanges();
            }

        }
        public static Mst_AuditMatrix Mst_AuditMatrixGetByID(int ID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.Mst_AuditMatrix
                                        where row.ID == ID
                                        select row).SingleOrDefault();

                return MstAuditorMaster;
            }
        }
        //public static object GetAllAuditMatrix(int Branchid)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<Mst_AuditMatrix> AuditorUserMaster = new List<Mst_AuditMatrix>();
        //        if (Branchid == -1)
        //        {
        //            AuditorUserMaster = (from row in entities.Mst_AuditMatrix
        //                                 where row.IsActive == false
        //                                 select row).ToList();
        //        }
        //        else
        //        {
        //            AuditorUserMaster = (from row in entities.Mst_AuditMatrix
        //                                 where row.IsActive == false && row.CustomerBranchID == Branchid
        //                                 select row).ToList();

        //        }
        //        return AuditorUserMaster.ToList();
        //    }
        //}
        public static List<Mst_AuditMatrix> GetAllAuditMatrixList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditorUserMasterList = (from row in entities.Mst_AuditMatrix
                                             where row.IsActive == false
                                             select row);

                AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.ID);

                return AuditorUserMasterList.OrderBy(entry => entry.ID).ToList();
            }
        }
        public static void DeleteAuditMatrix(int AuditerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_AuditMatrix AuditorMastertoDelete = (from row in entities.Mst_AuditMatrix
                                                         where row.ID == AuditerID
                                                         select row).FirstOrDefault();

                AuditorMastertoDelete.IsActive = true;
                entities.SaveChanges();
            }
        }

        public static object FillProcessFromRATBDM(int customerID, List<long> branchList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //List<Mst_Process> ProcessList = new List<Mst_Process>();

                //if (branchList.Count > 0)
                //{
                    //var query = (from row in entities.RiskActivityToBeDoneMappings
                    //             join row1 in entities.Mst_Process
                    //             on row.ProcessId equals row1.Id
                    //             where branchList.Contains((long)row.CustomerBranchID)
                    //             && row1.CustomerID == customerID
                    //             && row1.IsDeleted == false
                    //             select row1).Distinct().ToList();

                    //var assignedprocess = (from row in entities.EntitiesAssignmentAuditManagerRisks
                    //                       where branchList.Contains((long)row.BranchID)
                    //                       && row.CustomerID == customerID
                    //                       && row.ISACTIVE == true
                    //                       select row.ProcessId).Distinct().ToList();
                    //if (assignedprocess.Count > 0)
                    //{
                    //    query = query.Where(en => !assignedprocess.Contains((int)en.Id)).ToList();
                    //}
                    //var query = (from row in entities.Mst_Process
                    //         where row.IsDeleted == false
                    //         && row.CustomerID == customerID
                    //         select row).Distinct().ToList();
                    var query = (from row in entities.Mst_Process
                                 where row.IsDeleted == false
                                 && row.CustomerID == customerID
                                 select row).Distinct().ToList();
                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                //}
                //else
                //{
                //    var query = (from row in entities.RiskActivityToBeDoneMappings
                //                 join row1 in entities.Mst_Process
                //                 on row.ProcessId equals row1.Id
                //                 where row1.CustomerID == customerID
                //                 select row1).Distinct().ToList();

                //    //var assignedprocess = (from row in entities.EntitiesAssignmentAuditManagerRisks
                //    //                       where row.CustomerID == customerID && row.ISACTIVE == true
                //    //                       select row.ProcessId).Distinct().ToList();
                //    //if (assignedprocess.Count > 0)
                //    //{
                //    //    query = query.Where(en => !assignedprocess.Contains((int)en.Id)).ToList();
                //    //}
                //    var users = (from row in query
                //                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                //    return users;
                //}
            }
        }
        public static object FillProcessFromRATBDMEdit(int customerID, List<long> branchList)//,long UserID
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //List<Mst_Process> ProcessList = new List<Mst_Process>();

                //if (branchList.Count > 0)
                //{
                    //var query = (from row in entities.RiskActivityToBeDoneMappings
                    //             join row1 in entities.Mst_Process
                    //             on row.ProcessId equals row1.Id
                    //             where branchList.Contains((long)row.CustomerBranchID)
                    //             && row1.CustomerID == customerID
                    //             && row1.IsDeleted == false
                    //             select row1).Distinct().ToList();

                    //var assignedprocess = (from row in entities.EntitiesAssignmentAuditManagerRisks
                    //                       where branchList.Contains((long)row.BranchID)
                    //                       && row.CustomerID == customerID
                    //                       && row.ISACTIVE == true
                    //                       select row.ProcessId).Distinct().ToList();
                    //if (assignedprocess.Count > 0)
                    //{
                    //    query = query.Where(en => !assignedprocess.Contains((int)en.Id)).ToList();
                    //}
                    var query = (from row in entities.Mst_Process
                                 where row.IsDeleted == false
                                 && row.CustomerID == customerID
                                 select row).Distinct().ToList();
                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                //}
                //else
                //{
                //    var query = (from row in entities.RiskActivityToBeDoneMappings
                //                 join row1 in entities.Mst_Process
                //                 on row.ProcessId equals row1.Id
                //                 where row1.CustomerID == customerID
                //                 select row1).Distinct().ToList();

                    //var assignedprocess = (from row in entities.EntitiesAssignmentAuditManagerRisks
                    //                       where row.CustomerID == customerID && row.ISACTIVE == true
                    //                       select row.ProcessId).Distinct().ToList();
                    //if (assignedprocess.Count > 0)
                    //{
                    //    query = query.Where(en => !assignedprocess.Contains((int)en.Id)).ToList();
                    //}
                //    var users = (from row in query
                //                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                //    return users;
                //}
            }
        }
        public static object FillProcessFromManagementRATBDM(int customerID, List<long> branchList, int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> ProcessList = new List<Mst_Process>();
                if (branchList.Count > 0)
                {
                    var query = (from row in entities.RiskActivityToBeDoneMappings
                                 join row1 in entities.Mst_Process
                                 on row.ProcessId equals row1.Id
                                 where branchList.Contains((long)row.CustomerBranchID)
                                 && row1.CustomerID == customerID
                                 select row1).Distinct().ToList();

                    //var assignedprocess = (from row in entities.EntitiesAssignmentManagementRisks
                    //                       where branchList.Contains((long)row.BranchID)
                    //                       && row.CustomerID == customerID
                    //                       && row.UserID == UserID
                    //                       && row.ISACTIVE == true
                    //                       select row.ProcessId).Distinct().ToList();
                    //if (assignedprocess.Count > 0)
                    //{
                    //    query = query.Where(en => !assignedprocess.Contains((int)en.Id)).ToList();
                    //}

                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                }
                else
                {
                    var query = (from row in entities.RiskActivityToBeDoneMappings
                                 join row1 in entities.Mst_Process
                                 on row.ProcessId equals row1.Id
                                 where row1.CustomerID == customerID
                                 select row1).Distinct().ToList();

                    //var assignedprocess = (from row in entities.EntitiesAssignmentManagementRisks
                    //                       where row.CustomerID == customerID
                    //                       && row.UserID == UserID
                    //                       && row.ISACTIVE == true
                    //                       select row.ProcessId).Distinct().ToList();
                    //if (assignedprocess.Count > 0)
                    //{
                    //    query = query.Where(en => !assignedprocess.Contains((int)en.Id)).ToList();
                    //}

                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                }
            }
        }
        public static object FillProcessFromManagementRATBDMEdit(int customerID, List<long> branchList, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> ProcessList = new List<Mst_Process>();
                if (branchList.Count > 0)
                {
                    var query = (from row in entities.RiskActivityToBeDoneMappings
                                 join row1 in entities.Mst_Process
                                 on row.ProcessId equals row1.Id
                                 where branchList.Contains((long)row.CustomerBranchID)
                                 && row1.CustomerID == customerID
                                 select row1).Distinct().ToList();

                    //var assignedprocess = (from row in entities.EntitiesAssignmentManagementRisks
                    //                       where branchList.Contains((long)row.BranchID)
                    //                       && row.CustomerID == customerID
                    //                       && row.UserID == UserID
                    //                       && row.ISACTIVE == true
                    //                       select row.ProcessId).Distinct().ToList();
                    //if (assignedprocess.Count > 0)
                    //{
                    //    query = query.Where(en => !assignedprocess.Contains((int)en.Id)).ToList();
                    //}

                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                }
                else
                {
                    var query = (from row in entities.RiskActivityToBeDoneMappings
                                 join row1 in entities.Mst_Process
                                 on row.ProcessId equals row1.Id
                                 where row1.CustomerID == customerID
                                 select row1).Distinct().ToList();

                    //var assignedprocess = (from row in entities.EntitiesAssignmentManagementRisks
                    //                       where row.CustomerID == customerID
                    //                       && row.UserID == UserID
                    //                       && row.ISACTIVE == true
                    //                       select row.ProcessId).Distinct().ToList();
                    //if (assignedprocess.Count > 0)
                    //{
                    //    query = query.Where(en => !assignedprocess.Contains((int)en.Id)).ToList();
                    //}

                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                }
            }
        }
        public static bool AuditMatrixExists(int Rating, long Customerbranchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_AuditMatrix
                             where row.IsActive == false
                             && row.Rating == Rating
                             && row.CustomerBranchID == Customerbranchid
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        #endregion

        public static long GetProcessIDByResultID(int resultID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.ID == resultID
                             select row.ProcessId).FirstOrDefault();
                return query;
            }
        }

        public static object FillSubProcessUserWise(long Processid, string flag, int AuditID, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditAssignments
                             join row1 in entities.mst_Subprocess
                             on row.ProcessId equals row1.ProcessId
                             where
                             row.SubProcessId == row1.Id
                             && row.AuditID == AuditID
                             && row.UserID == UserID
                             && row1.IsDeleted == false
                             && row1.ProcessId == Processid
                             select row1);

                if (flag == "N")
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "N");
                }
                else
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "P");
                }
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static object FillSubProcessForAuditHead(long Processid, string flag, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditAssignments
                             join row1 in entities.mst_Subprocess
                             on row.ProcessId equals row1.ProcessId
                             where
                             row.SubProcessId == row1.Id
                             && row.AuditID == AuditID
                             && row1.IsDeleted == false
                             && row1.ProcessId == Processid
                             && row.RoleID == 3
                             select row1);

                if (flag == "N")
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "N");
                }
                else
                {
                    query = query.Where(entry => entry.IsProcessNonProcess == "P");
                }
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static List<CoalReviewKPUnitView> GetFinalAuditCommitteeView(int RoleID, int customerid, int userID, int VerticalID, int ProcessID, string FinancialYear, string Period, List<int?> BranchList, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<CoalReviewKPUnitView> auditdsplay = new List<CoalReviewKPUnitView>();
                auditdsplay = (from row in entities.CoalReviewKPUnitViews
                               join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                               on row.ProcessId equals EAAR.ProcessId
                               where row.CustomerbranchId == EAAR.BranchID && EAAR.ISACTIVE == true
                                && EAAR.UserID == userID
                               && (row.ISACPORMIS == 1 || row.ISACPORMIS == 2)
                               && row.AuditID == AuditID
                               select row).OrderBy(entry => new { entry.ObsevationCategoryName, entry.LocationName, entry.Name, entry.ObservationTitle, entry.ATBDId }).ToList();

                if (BranchList.Count > 0)
                {
                    List<long?> BranchAssigned = BranchList.Select(x => (long?)x).ToList();
                    auditdsplay = auditdsplay.Where(Entry => BranchAssigned.Contains(Entry.CustomerbranchId)).ToList();
                }

                if (FinancialYear != "")
                    auditdsplay = auditdsplay.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (Period != "")
                    auditdsplay = auditdsplay.Where(entry => entry.ForPeriod == Period).ToList();

                if (VerticalID != -1)
                    auditdsplay = auditdsplay.Where(entry => entry.VerticalID == VerticalID).ToList();

                return auditdsplay;
            }
        }

        //Added by Madhur
        public static object FillSubProcessDropdownManagementForAuditHeadDashBoard(List<long?> ProcessIdList, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<mst_Subprocess> query = new List<mst_Subprocess>();
                query = (from cs in entities.mst_Subprocess
                         where ProcessIdList.Contains(cs.ProcessId)
                         && cs.IsDeleted == false
                         select cs).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;

            }
        }

        public static object FillProcessDropdownAuditManagerForAuditHeadDashBoard(int CustomerId, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.EntitiesAssignmentAuditManagerRisks
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == CustomerId
                         && row1.IsDeleted == false
                         && row.ISACTIVE == true
                         && row.UserID == UserID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static object FillProcessDropdownDepartmentHeadForAuditHeadDashBoard(int CustomerId, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Sp_GetDepartmentHeadProcess_Result> query = new List<Sp_GetDepartmentHeadProcess_Result>();
                query = (from row in entities.Sp_GetDepartmentHeadProcess(Convert.ToInt32(UserID))
                         select row).Distinct().ToList();
                var users = (from row in query
                             select new { ID = row.ProcessId, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static object FillProcessDropdownManagementForAuditHeadDashBoard(int CustomerID, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.EntitiesAssignmentManagementRisks
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == CustomerID
                         && row1.IsDeleted == false
                         && row.ISACTIVE == true
                         && row.UserID == UserID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        //End

        /*Added by Madhur*/
        public static int SubprocessCreatewithID(mst_Subprocess subProcess)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                subProcess.IsDeleted = false;
                subProcess.CreatedOn = DateTime.UtcNow;
                subProcess.UpdatedOn = DateTime.UtcNow;
                entities.mst_Subprocess.Add(subProcess);
                entities.SaveChanges();
                return (int)subProcess.Id;
            }
        }

        public static mst_Department GetAllDepartmentByID(int DptID, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Department Dept = (from row in entities.mst_Department
                                       where row.ID == DptID
                                       && row.CustomerID == CustomerID
                                       select row).FirstOrDefault();

                return Dept;
            }
        }

        public static long GetProcessbyName(int CustID, string ProcessName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_Process
                             where row.Name.ToLower() == ProcessName.Trim().ToLower()
                             && row.CustomerID == CustID
                             && row.IsDeleted == false
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        /*End*/
    }
}
