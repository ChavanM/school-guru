﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCSVendorMastersManagement
    {
        public static List<LocationFilter> GetBranchClientWise(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                     join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
                                     join CB2 in entities.CustomerBranches on CB.ParentID equals CB2.ID
                                     where RVAS.AVACOM_CustomerID == CustomerID
                                       && CB2.IsDeleted == false && CB2.Status == 1
                                     select new LocationFilter
                                     {
                                         Name = CB.Name,
                                         ID = CB.ID
                                     }).Distinct().ToList();

                return objchechklist;
            }
        }
        public static string GetContractMasterStatus(long CustID)
        {
            string output = "False";
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_ContractMaster_CustomerMapping updateDetails = (from row in entities.RLCS_ContractMaster_CustomerMapping
                                                                     where row.CustID == CustID
                                                                     && row.IsActive == true
                                                                     select row).FirstOrDefault();
                if (updateDetails != null)
                {
                    output = "True";
                }
            }
            return output;
        }
        public static List<SP_RLCS_GetVendorAuditCustomersClientWise_Result> GetAllClientWise(long UserID, string Role, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.SP_RLCS_GetVendorAuditCustomersClientWise(UserID, CustomerID, Role)
                             select row);

                return users.ToList();
            }
        }
        public static string IsServiceProviderCheck(long CustID)
        {
            string output = "False";
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Customer updateDetails = (from row in entities.Customers
                                          where row.ID == CustID
                                          && row.IsDeleted == false
                                          && row.ServiceProviderID != 94
                                          select row).FirstOrDefault();
                if (updateDetails != null)
                {
                    output = "True";
                }
            }
            return output;
        }

        public static List<object> GetStateEntityWise(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string checkISTeamlease = IsServiceProviderCheck(CustomerID);
                if (checkISTeamlease == "True")
                {
                    var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                         join RSM in entities.RLCS_State_Mapping
                                         on RVAS.CC_StateID equals RSM.SM_Code
                                         where RVAS.AVACOM_CustomerID == CustomerID
                                         select new
                                         {
                                             Name = RSM.SM_Name,
                                             ID = RSM.SM_Code
                                         }).Distinct().ToList();
                    return objchechklist.ToList<object>();

                    //var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                    //                     join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
                    //                     where RVAS.AVACOM_CustomerID == CustomerID
                    //                     select new
                    //                     {
                    //                         Name = CB.Name,
                    //                         ID = CB.ID
                    //                     }).Distinct().ToList();
                    //return objchechklist.ToList<object>();
                }
                else
                {
                    var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                         join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
                                        where RVAS.AVACOM_CustomerID == CustomerID
                                        && CB.IsDeleted == false && CB.Status == 1
                                        select new
                                        {
                                            //Name = CB.Name,
                                            ID = CB.StateID
                                        }).Distinct().ToList();

                    var objchechklistNew = (from St in objchechklist
                                            join SM in  entities.RLCS_State_Mapping on  St.ID equals SM.AVACOM_StateID
                                            where SM.AVACOM_StateID == St.ID
                                            select new
                                            {
                                                Name = SM.SM_Name,
                                                ID = SM.SM_Code
                                            }).Distinct().ToList();

                    return objchechklistNew.ToList<object>();
                }
            }
        }

        //public static List<object> GetStateEntityWise(int CustomerID)
        //      {
        //          using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //          {
        //              string checkISTeamlease = IsServiceProviderCheck(CustomerID);
        //              if (checkISTeamlease == "True")
        //              {
        //                  var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
        //                                       join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
        //                                       where RVAS.AVACOM_CustomerID == CustomerID
        //                                       select new
        //                                       {
        //                                           Name = CB.Name,
        //                                           ID = CB.ID
        //                                       }).Distinct().ToList();
        //                  return objchechklist.ToList<object>();
        //              }
        //              else
        //              {
        //                  var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
        //                                       join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
        //                                       join CB2 in entities.CustomerBranches on CB.ParentID equals CB2.ID
        //                                       join CB3 in entities.CustomerBranches on CB2.ParentID equals CB3.ID
        //                                       where RVAS.AVACOM_CustomerID == CustomerID //&& CB3.ParentID == EntityID  
        //                                       select new
        //                                       {
        //                                           Name = CB3.Name,
        //                                           ID = CB3.ID
        //                                       }).Distinct().ToList();
        //                  return objchechklist.ToList<object>();
        //              }
        //          }
        //      }
        public static List<object> GetLocationStateWise(int EntityID, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string checkISTeamlease = IsServiceProviderCheck(CustomerID);
                if (checkISTeamlease == "True")
                {
                    var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                         join RLCM in entities.RLCS_Location_City_Mapping
                                         on RVAS.CC_Location equals RLCM.LM_Code
                                         where RVAS.AVACOM_CustomerID == CustomerID
                                         && RVAS.AVACOM_BranchID == EntityID
                                         && RVAS.AVACOM_CustomerID == CustomerID
                                         select new
                                         {
                                             Name = RLCM.LM_Name,
                                             ID = RLCM.LM_Code
                                         }).Distinct().ToList();
                    return objchechklist.ToList<object>();
                }
                else
                {
                    var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                         join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID                                                                                  
                                         join CB2 in entities.CustomerBranches on CB.ParentID equals CB2.ID
                                         where RVAS.AVACOM_CustomerID == CustomerID && CB2.ParentID == EntityID                                         
                                         && CB2.IsDeleted == false && CB2.Status == 1
                                         select new
                                         {
                                             Name = CB2.Name,
                                             ID = CB2.ID
                                         }).Distinct().ToList();
                    return objchechklist.ToList<object>();
                }
            }
        }
 

        public static List<object> GetLocationStateWise(string EntityID, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                     join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
                                     where RVAS.AVACOM_CustomerID == CustomerID
                                     && CB.IsDeleted == false && CB.Status == 1
                                     select new
                                     {
                                         //Name = CB.Name,
                                         ID = CB.CityID
                                     }).Distinct().ToList();

                var objchechklistNew = (from St in objchechklist
                                        join SM in entities.RLCS_Location_City_Mapping on St.ID equals SM.AVACOM_CityID
                                        where SM.SM_Code == EntityID
                                        select new
                                        {
                                            Name = SM.LM_Name,
                                            ID = SM.SM_Code
                                        }).Distinct().ToList();

                return objchechklistNew.ToList<object>();
            }
        }


        public static int GetDeptID(string dept)
        {
            int HRDeptID = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Vendor_Department Dept = (from row in entities.Vendor_Department
                                          where row.IsDeleted == false
                                          && row.Name == dept
                                          select row).FirstOrDefault();
                if (Dept != null)
                {
                    HRDeptID = (int)Dept.Vendor_DeptID;
                }

                return HRDeptID;
            }
        }

        #region amol
        public static bool DeleteAssignCheklistMappingDetail(long AuditID,string Period,long CheckListID)
        {
            bool flag = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_VendorActChecklistMapping obj = (from row in entities.RLCS_VendorActChecklistMapping
                                                      where row.AuditID == AuditID && row.SchedulePeriod== Period && row.CheckListMasterId== CheckListID
                                                      select row).FirstOrDefault();
                if (obj != null)
                {
                    obj.IsDeleted = true;
                    entities.SaveChanges();
                    flag = true;
                }
                else
                {
                    flag = false;
                }
                return flag;
            }
        }
        public static List<RLCS_VendorAuditResultStatus> getRLCSResultStatus(string role)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (role.Equals("HVEND"))
                {
                    var objStatusList = (from row in entities.RLCS_VendorAuditResultStatus
                                         where row.IsPerformer == true
                                         orderby row.Name
                                         select row).ToList();

                    return objStatusList;
                }
                else if (role.Equals("HVAUD"))
                {
                    var objStatusList = (from row in entities.RLCS_VendorAuditResultStatus
                                         where row.IsReviewer == true
                                         select row).ToList();

                    return objStatusList;
                }
                else
                {
                    return null;
                }
            }
        }


        public static bool DeleteRLCSVendorFile(long FileID, int userID)
        {
            bool result = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCSVendorFileData objDeletelist = (from row in entities.RLCSVendorFileDatas
                                                    where row.IsDeleted == false
                                                    && row.ID == FileID
                                                    select row).FirstOrDefault();
                if (objDeletelist != null)
                {
                    objDeletelist.IsDeleted = true;
                    objDeletelist.UpdatedOn = DateTime.Now;
                    objDeletelist.UpdatedBy = userID;
                    entities.SaveChanges();
                    result = true;
                }
                return result;
            }
        }

        public static RLCSVendorFileData GetRLCSVendorFile(long FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCSVendorFileData file = (from row in entities.RLCSVendorFileDatas
                                           where row.IsDeleted == false
                                           && row.ID == FileID
                                           select row).FirstOrDefault();

                return file;
            }
        }

      
        public static bool getAuditRescheduleAllow(long AuditId,long ScheduleOnId, string ForMonth)
        {
            bool Status = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var VendorAuditScheduleOnData = (from row in entities.RLCS_VendorAuditScheduleOn
                                                     where row.AuditID == AuditId
                                                     && row.ForMonth.Equals(ForMonth)
                                                     && row.ID == ScheduleOnId
                                                     && row.IsActive == true
                                                     select row).ToList();
                    if (VendorAuditScheduleOnData.Count > 0)
                    {
                        return Status = true;
                    }
                    else
                    {
                        return Status;
                    }
                }
                catch (Exception ex)
                {
                    List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                    return Status;
                }
            }
        }

        public static bool UpdateAuditReschedule(long ScheduleOnId, long AuditId, DateTime StartDate, DateTime EndDate,string Remark,long UserID)
        {
            bool Status = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var GetData = (from row in entities.RLCS_VendorAuditScheduleOn
                                    where row.ID == ScheduleOnId
                                    && row.AuditID == AuditId
                                    && row.IsActive == false
                                    select row).FirstOrDefault();
                    if (GetData != null)
                    {
                        GetData.IsActive = true;
                        GetData.Remark = Remark;
                        GetData.AuditStatusID = 1;
                        GetData.ScheduleOn = StartDate;
                        GetData.ScheduleOnEndDate = EndDate;
                        entities.SaveChanges();
                       

                        
                        RLCS_VendorScheduleLog transactionlog = new RLCS_VendorScheduleLog()
                        {
                            AuditID = AuditId,
                            ScheduleOnID = ScheduleOnId,                            
                            Remark = Remark,
                            CreatedBy= UserID,
                            CreatedOn=DateTime.Now,
                        };
                        entities.RLCS_VendorScheduleLog.Add(transactionlog);
                        entities.SaveChanges();

                        Status = true;
                    }
                    return Status;             
                }
                catch (Exception ex)
                {
                    List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                    return Status;
                }
            }
        }


        public static bool UpdateAuditScheduleOnStatus(long ScheduleOnId, long AuditId, string Remark)
        {
            bool Status = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    RLCS_VendorAuditScheduleOn ExistData = (from row in entities.RLCS_VendorAuditScheduleOn
                                                            where row.ID == ScheduleOnId
                                                               && row.AuditID == AuditId
                                                               && row.IsActive == true
                                                            select row).FirstOrDefault();
                    if (ExistData != null)
                    {
                        ExistData.IsActive = false;
                        ExistData.Remark = Remark;
                        ExistData.AuditStatusID = 5;
                        entities.SaveChanges();
                        return Status = true;
                    }
                    else
                    {
                        return Status;
                    }
                }
                catch (Exception ex)
                {
                    List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                    return Status;
                }
            }
        }


        public static int getAuditStatus(long ScheduleOnId, long AuditId)
        {
            int Status = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    RLCS_VendorAuditScheduleOn ExistData = (from row in entities.RLCS_VendorAuditScheduleOn
                                                            where row.ID == ScheduleOnId
                                                               && row.AuditID == AuditId
                                                            select row).FirstOrDefault();
                    if (ExistData != null)
                    {
                        Status = ExistData.AuditStatusID;
                        return Status;
                    }
                    else
                    {
                        return Status;
                    }
                }
                catch (Exception ex)
                {
                    List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                    return Status;
                }
            }
        }
        #endregion
        public static List<SP_RLCS_VendorGetAuditLogTransaction_Result> GetAllTransactionLog(int ScheduledOnID, long checklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.SP_RLCS_VendorGetAuditLogTransaction(ScheduledOnID, checklistID)
                                  orderby row.Dated descending
                                  select row).ToList();

                return statusList;
            }
        }
        public static List<RLCS_VendorCheckListStatus> getRLCSChecklistStatus()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist = (from row in entities.RLCS_VendorCheckListStatus
                                    select row).ToList();

                return objChecklist;
            }
        }
        public static bool FileUpload(long checklistID, long ChecklistScheduleOnID, List<RLCSVendorFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Vendor_SaveDocFiles(filesList);                            
                            if (files != null)
                            {
                                foreach (RLCSVendorFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.RLCSVendorFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    RLCSVendorFileDataMapping fileMapping = new RLCSVendorFileDataMapping();
                                    fileMapping.ChecklistID = checklistID;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = ChecklistScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.RLCSVendorFileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();                               
                return false;
            }
        }

        public static bool FileUploadMaster(long checklistID, long ContractorID, List<RLCSVendorFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, string State, string location, string Branch, string StateOrCentral, string StateFilter)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Vendor_SaveDocFiles(filesList);
                            if (files != null)
                            {
                                foreach (RLCSVendorFileData fl in files)
                                {
                                    if (fl.ID > 0)
                                    {
                                        RLCSVendorFileData obj = (from row in entities.RLCSVendorFileDatas
                                                                  where row.ID == fl.ID
                                                                  select row).FirstOrDefault();
                                        if (obj != null)
                                        {
                                            obj.Name = fl.Name;
                                            obj.FilePath = fl.FilePath;
                                            obj.FileKey = fl.FileKey;
                                            obj.Version = fl.Version;
                                            obj.CreatedOn = fl.CreatedOn;
                                            obj.FileSize = fl.FileSize;
                                            obj.IsDeleted = false;
                                            obj.EnType = "A";
                                            obj.ValidTill = fl.ValidTill;
                                            obj.CreatedBy = fl.CreatedBy;
                                            entities.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        fl.IsDeleted = false;
                                        fl.EnType = "A";
                                        entities.RLCSVendorFileDatas.Add(fl);

                                        entities.SaveChanges();

                                        RLCSMasterFileData fileMapping = new RLCSMasterFileData();

                                        fileMapping.ChecklistID = Convert.ToInt32(checklistID);
                                        fileMapping.ScheduleOnID = 0;
                                        fileMapping.ContractorID = Convert.ToInt32(ContractorID);
                                        fileMapping.FileID = fl.ID;
                                        fileMapping.State = State;
                                        fileMapping.Location = location;
                                        fileMapping.Branch = Branch;
                                        fileMapping.StateOrCentral = StateOrCentral;
                                        fileMapping.Year = null;
                                        fileMapping.Period = null;
                                        //fileMapping.ScheduledOnID = ChecklistScheduleOnID;
                                        //if (list != null)
                                        //{
                                        //    fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                        //}
                                        entities.RLCSMasterFileDatas.Add(fileMapping);
                                    }
                                   
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                return false;
            }
        }

        public static bool MusterRollFileUpload(long AuditID, long ChecklistScheduleOnID, List<RLCSVendorFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Vendor_SaveDocFiles(filesList);
                            if (files != null)
                            {
                                foreach (RLCSVendorFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.RLCSVendorFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    RLCSVendorMusterRollFileDataMapping fileMapping = new RLCSVendorMusterRollFileDataMapping();
                                    fileMapping.Audit_ID = AuditID;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = ChecklistScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.RLCSVendorMusterRollFileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                return false;
            }
        }

        public static List<RLCSVendorFileData> getDocumentData(long CheckListID, long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist = (from row in entities.RLCSVendorFileDatas
                                    join row1 in entities.RLCSVendorFileDataMappings
                                    on row.ID equals row1.FileID
                                    where row.IsDeleted == false
                                          && row1.ChecklistID == CheckListID
                                          && row1.ScheduledOnID == ScheduledOnID
                                    select row  ).ToList();

                return objChecklist;
            }
        }

        public static List<RLCSVendorFileData> getDocumentData(long CheckListID, long ScheduledOnID, long VendorID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist = (from row in entities.RLCSVendorFileDatas
                                    join row1 in entities.RLCSVendorFileDataMappings
                                    on row.ID equals row1.FileID
                                    where row.IsDeleted == false
                                          && row1.ChecklistID == CheckListID
                                          && row1.ScheduledOnID == ScheduledOnID
                                    select row).ToList();

                var RLCSVendorFileData12fromMasterData = (from row in entities.RLCSVendorFileDatas
                                                          join row2 in entities.RLCSMasterFileDatas
                                                          on row.ID equals row2.FileID
                                                          where row.IsDeleted == false
                                                            && row2.ContractorID == VendorID
                                                            && row2.ChecklistID == CheckListID
                                                          select row).ToList();

                

                objChecklist = objChecklist.Union(RLCSVendorFileData12fromMasterData).ToList() as List<RLCSVendorFileData>;
                objChecklist = objChecklist.GroupBy(p => p.ID)
                  .Select(g => g.First())
                  .ToList();

                return objChecklist;
            }
        }

        public static List<RLCSVendorFileData> getDocumentDataByUserID(long ActID, long ContrID)//, long ScheduleOnId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist = (from row in entities.RLCSVendorFileDatas
                                    join row1 in entities.RLCSMasterFileDatas
                                    on row.ID equals row1.FileID
                                    where row1.ChecklistID == ActID && row1.ContractorID== ContrID && row.IsDeleted == false //&& row1.ScheduleOnID == ScheduleOnId
                                    select row).ToList();

                return objChecklist;
            }
        }

        public static List<RLCSVendorFileData> getDocumentDataByFileID(long FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist = (from row in entities.RLCSVendorFileDatas
                                    where row.ID == FileID
                                    select row).ToList();

                return objChecklist;
            }
        }

        public static List<RLCSVendorFileData> GetMustorDocumentData(long AuditID, long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist1 = (from row in entities.RLCSVendorFileDatas
                                     join row2 in entities.RLCSVendorMusterRollFileDataMappings
                                     on row.ID equals row2.FileID
                                     where row.IsDeleted == false
                                           && row2.ScheduledOnID == ScheduledOnID
                                           && row2.Audit_ID == AuditID
                                     select row).ToList();

                return objChecklist1;
            }
        }
       

        public static List<RLCS_Act_Mapping> getRLCSActGrouplistData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist = (from row in entities.RLCS_Act_Mapping
                                    where row.IsDeleted == false
                                    select row).ToList();

                return objChecklist;
            }
        }
        public static List<RLCS_Act_Mapping> getRLCSActlistData(string ISCentral)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist = (from row in entities.RLCS_Act_Mapping
                                    where row.IsDeleted == false
                                    select row).ToList();

                if (ISCentral.ToUpper().Trim() == "Central".ToUpper().Trim())
                {
                    objChecklist = objChecklist.Where(a => a.AM_StateID.ToUpper().Trim() == ISCentral.ToUpper().Trim()).ToList();
                }
                return objChecklist;
            }
        }
        
        public static List<SP_RLCS_GetVendorAuditCustomers_Result> GetAll(long UserID,string Role,string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.SP_RLCS_GetVendorAuditCustomers(UserID, Role)                           
                             select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter));
                }

                return users.ToList();
            }
        }

        public static List<SP_RLCS_GetBranchWiseVendorList_Result> GetAllVendor(long BranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var list = (from row in entities.SP_RLCS_GetBranchWiseVendorList(BranchID)
                            select row).ToList();

                return list;
            }
        }
        public static List<SP_RLCS_GetVendorList_Result> getRLCSVendorUserlistData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var list = (from row in entities.SP_RLCS_GetVendorList()
                           select row).ToList();

                return list;
            }
        }
        public static bool saveActMappingDetail(List<RLCS_VendorActChecklistMapping> objVAM)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int Count = 0;
                    objVAM.ForEach(EachStep =>
                    {
                        Count++;
                        entities.RLCS_VendorActChecklistMapping.Add(EachStep);
                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }                       
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static void saveActMappingDetail(RLCS_VendorActChecklistMapping objact)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_VendorActChecklistMapping obj = (from row in entities.RLCS_VendorActChecklistMapping
                                                    where row.ActID == objact.ActID
                                                    && row.VendorID == objact.VendorID
                                                    && row.CheckListMasterId==objact.CheckListMasterId && row.SchedulePeriod == objact.SchedulePeriod
                                                      select row).FirstOrDefault();
                if (obj != null)
                {
                    obj.IsDeleted = false;
                    entities.SaveChanges();
                }
                else
                {
                    entities.RLCS_VendorActChecklistMapping.Add(objact);
                    entities.SaveChanges();
                }
            }
        }
        public static bool ExistActMappingDetail(string ActId, int ChecklistId, List<int> VendorList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_VendorActChecklistMapping obj = (from row in entities.RLCS_VendorActChecklistMapping
                                                      where row.ActID == ActId && row.IsDeleted == false
                                                   && row.CheckListMasterId == ChecklistId
                                                   && VendorList.Contains(row.VendorID)
                                             select row).FirstOrDefault();
                if (obj != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
       
        public static void UpdateActMappingDetail(string Actid, int ChecklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_VendorActChecklistMapping obj = (from row in entities.RLCS_VendorActChecklistMapping
                                                      where row.ActID == Actid && row.CheckListMasterId == ChecklistID
                                             select row).FirstOrDefault();
                if (obj != null)
                {
                    obj.IsDeleted = true;
                    entities.SaveChanges();
                }
            }
        }

       
        public static List<RLCS_State_Mapping> getRLCSddlStateData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist = (from row in entities.RLCS_State_Mapping
                                    where row.SM_Status == "A"
                                    select row).ToList();

                return objChecklist;
            }
        }
      
        public static bool ExistsActData(String ActId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objVendor = (from row in entities.RLCS_Act_Mapping
                                 where row.AM_ActID.Equals(ActId)
                                 && row.IsDeleted == false
                                 select row);

                return objVendor.Select(entry => true).FirstOrDefault();
            }
        }
        public static bool ExistsStateData(String stateId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objVendor = (from row in entities.RLCS_State_Mapping
                                 where row.SM_Code.Equals(stateId) || row.SM_Name.Equals(stateId)
                                 select row);

                return objVendor.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool ExistsChecklistData(String actId, String stateId, String NatureOfCompliance)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objVendor = (from row in entities.RLCS_tbl_CheckListMaster
                                 where row.StateID.Equals(stateId)
                                       && row.ActID.Equals(actId)
                                       && row.NatureOfCompliance.Equals(NatureOfCompliance)
                                       && row.IsDeleted == false
                                 select row);

                return objVendor.Select(entry => true).FirstOrDefault();
            }
        }
        public static bool ExistsChecklistDataforUpdate(long checklistId,string ActID, String NatureOfCompliance)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objVendor = (from row in entities.RLCS_tbl_CheckListMaster
                                 where  row.NatureOfCompliance.ToUpper().Trim().Equals(NatureOfCompliance.ToUpper().Trim())
                                       && row.IsDeleted == false
                                       && row.ActID.ToUpper().Trim() == ActID.ToUpper().Trim()
                                       && row.Id != checklistId
                                 select row);

                return objVendor.Select(entry => true).FirstOrDefault();
            }
        }

        public static string GetStateData(string StateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from row in entities.RLCS_State_Mapping
                                     where row.SM_Code == StateID
                                     select row.SM_Name).FirstOrDefault();

                return objchechklist;
            }
        }
        public static string GetActData(string ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from row in entities.RLCS_Act_Mapping
                                     where row.AM_ActID == ActID
                                     select row.AM_ActName).FirstOrDefault();

                return objchechklist;
            }
        }
        public static RLCS_tbl_CheckListMaster GetChecklistData(long CheckListId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from row in entities.RLCS_tbl_CheckListMaster
                                     where row.Id == CheckListId
                                     select row).FirstOrDefault();

                return objchechklist;
            }
        }

        public static void saveCheckListDetail(RLCS_tbl_CheckListMaster objchklst)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.RLCS_tbl_CheckListMaster.Add(objchklst);
                entities.SaveChanges();
            }
        }
        public static List<object> GetAllStates(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                     join C in entities.RLCS_State_Mapping on RVAS.CC_StateID equals C.SM_Code
                                     where RVAS.AVACOM_CustomerID == CustomerID
                                     select new
                                     {
                                         Name = C.SM_Name,
                                         ID = C.SM_Code
                                     }).Distinct().ToList();

                return objchechklist.ToList<object>();
            }
        }
        public static List<LocationFilter> GetBranchesbyStates(int CustomerID,String State)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                     where RVAS.AVACOM_CustomerID == CustomerID && RVAS.CC_StateID.Trim().ToUpper() == State.Trim().ToUpper()
                                     select new LocationFilter
                                     {
                                         ID = RVAS.AVACOM_BranchID,
                                         Name = RVAS.CC_BranchID
                                     }).Distinct().ToList();

                return objchechklist;
            }
        }

        public static List<object> GetEntitiesCustomerWise(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                     join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
                                     join CB2 in entities.CustomerBranches on CB.ParentID equals CB2.ID
                                     join CB3 in entities.CustomerBranches on CB2.ParentID equals CB3.ID
                                     join CB4 in entities.CustomerBranches on CB3.ParentID equals CB4.ID
                                     where RVAS.AVACOM_CustomerID == CustomerID
                                     && CB4.IsDeleted == false && CB4.Status == 1
                                     select new
                                     {
                                         Name = CB4.Name,
                                         ID = CB4.ID
                                     }).Distinct().ToList();

                return objchechklist.ToList<object>(); ;
            }
        }

        //public static List<object> GetStateEntityWise(int CustomerID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
        //                             join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
        //                             join CB2 in entities.CustomerBranches on CB.ParentID equals CB2.ID
        //                             join CB3 in entities.CustomerBranches on CB2.ParentID equals CB3.ID
        //                             where RVAS.AVACOM_CustomerID == CustomerID //&& CB3.ParentID == EntityID  
        //                             select   new
        //                             {
        //                                 Name = CB3.Name,
        //                                 ID = CB3.ID
        //                             }).Distinct().ToList();

        //        return objchechklist.ToList<object>(); ;
        //    }
        //}

        public static List<LocationFilter> GetBranchLocationWise(int CustomerID,int LocationID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                     join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
                                     join CB2 in entities.CustomerBranches on CB.ParentID equals CB2.ID 
                                     where RVAS.AVACOM_CustomerID == CustomerID && CB2.ID == LocationID
                                     && CB.IsDeleted == false && CB.Status == 1
                                     select new LocationFilter
                                     {
                                         Name = CB.Name,
                                         ID = CB.ID
                                     }).Distinct().ToList();

                return objchechklist;
            }
        }

        public static List<LocationFilter> GetBranchLocationWise(int CustomerID,string LocationID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                     join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
                                     join CB2 in entities.CustomerBranches on CB.ParentID equals CB2.ID 
                                     where RVAS.AVACOM_CustomerID == CustomerID && RVAS.CC_StateID == LocationID
                                     && CB.IsDeleted == false && CB.Status == 1
                                     select new LocationFilter
                                     {
                                         Name = CB.Name,
                                         ID = CB.ID
                                     }).Distinct().ToList();

                return objchechklist;
            }
        }

        //public static List<object> GetLocationStateWise(int EntityID, int CustomerID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
        //                             join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
        //                             join CB2 in entities.CustomerBranches on CB.ParentID equals CB2.ID
        //                             where RVAS.AVACOM_CustomerID == CustomerID && CB2.ParentID == EntityID
        //                             select new
        //                             {
        //                                 Name = CB2.Name,
        //                                 ID = CB2.ID
        //                             }).Distinct().ToList();

        //        return objchechklist.ToList<object>(); ;
        //    }
        //}
    }
}
