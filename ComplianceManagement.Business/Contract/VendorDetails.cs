﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;

namespace com.VirtuosoITech.ComplianceManagement.Business.Contract
{
    public class VendorDetails
    {
        public static Cont_tbl_SharingDoc GetDocSharing_AllByID(long CustomerID, int ID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                Cont_tbl_SharingDoc Query = (from row in entities.Cont_tbl_SharingDoc
                                             where row.CustomerID == CustomerID
                                             && row.IsDeleted == false
                                             && row.Id == ID
                                             select row).FirstOrDefault();
                return Query;
            }
        }
        public static bool CrontactDocUnSharingMappingByID(int RecordId, int CustID, int UID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                bool saveSuccess = false;
                try
                {
                    Cont_tbl_SharingDoc vendorMappingExists = (from row in entities.Cont_tbl_SharingDoc
                                                               where row.CustomerID == CustID
                                                               && row.Id == RecordId
                                                               select row).FirstOrDefault();

                    if (vendorMappingExists != null)
                    {
                        vendorMappingExists.IsDeleted = true;
                        vendorMappingExists.UpdatedBy = UID;
                        vendorMappingExists.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        public static bool CrontactDocUnSharingMapping(int RecordId, int CustID, int ContractId, int FileID, int UID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                bool saveSuccess = false;
                try
                {
                    Cont_tbl_SharingDoc vendorMappingExists = (from row in entities.Cont_tbl_SharingDoc
                                                               where row.ContractID == ContractId
                                                               && row.CustomerID == CustID
                                                               //&& row.FileID == FileID
                                                               && row.Id == RecordId
                                                               select row).FirstOrDefault();

                    if (vendorMappingExists != null)
                    {
                        vendorMappingExists.IsDeleted = true;
                        vendorMappingExists.UpdatedBy = UID;
                        vendorMappingExists.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        public static List<Cont_SP_GetSharingDocDetail_Result> GetDocSharing_All(long CustomerID, string Filter)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var Query = (from row in entities.Cont_SP_GetSharingDocDetail(CustomerID, -1, -1, Filter)
                             where row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             select row).OrderBy(entry => entry.VendorName);
                return Query.ToList();
            }
        }
        public static bool CrontactDocSharingMapping(Cont_tbl_SharingDoc record)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                bool saveSuccess = false;
                try
                {
                    Cont_tbl_SharingDoc vendorMappingExists = (from row in entities.Cont_tbl_SharingDoc
                                                               where row.ContractID == record.ContractID
                                                               && row.VendorID == record.VendorID
                                                               && row.FileID == record.FileID
                                                               select row).FirstOrDefault();

                    if (vendorMappingExists != null)
                    {
                        vendorMappingExists.IsDeleted = record.IsDeleted;
                        vendorMappingExists.UpdatedBy = record.CreatedBy;
                        vendorMappingExists.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        entities.Cont_tbl_SharingDoc.Add(record);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        //Nullable<long> customerid, Nullable<long> customerbranchid, Nullable<long> contracttypeid, Nullable<long> contractsubtypeid
        public static string GetContractNumber(long custid,long customerbranchid,long contracttypeid,long contractsubtypeid)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var masterlstVendors = (from row in entities.Cont_SP_GetContractNumber(custid, customerbranchid, contracttypeid, contractsubtypeid)
                                        select row.ContractNumber).FirstOrDefault();
                return masterlstVendors;
            }
        }


        #region Contract-Vendor Mapping

        public static bool CreateUpdate_VendorMapping(List<Cont_tbl_VendorMapping> lstVendorMapping)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                bool saveSuccess = false;
                try
                {
                    lstVendorMapping.ForEach(eachRecord =>
                    {
                        var vendorMappingExists = (from row in entities.Cont_tbl_VendorMapping
                                                   where row.ContractID == eachRecord.ContractID
                                                   && row.VendorID == eachRecord.VendorID
                                                   select row).FirstOrDefault();

                        if (vendorMappingExists != null)
                        {
                            vendorMappingExists.IsActive = true;
                            vendorMappingExists.UpdatedBy = eachRecord.CreatedBy;
                            vendorMappingExists.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            entities.Cont_tbl_VendorMapping.Add(eachRecord);
                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static Cont_tbl_VendorMaster GetVendorMasterDetails(long userID)
        {

           
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                Cont_tbl_VendorMaster masterlstVendors = (from row in entities.Cont_tbl_VendorMaster
                                              where row.ID == userID
                                                && row.IsDeleted == false
                                              select row).FirstOrDefault();
                return masterlstVendors;
            }
        }
      
        public static List<Cont_tbl_VendorMaster> GetVendorDetails(long CustomerID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var masterlstVendors = entities.Cont_tbl_VendorMaster.Where(row => row.CustomerID == CustomerID).ToList();
                return masterlstVendors;
            }
        }

        public static Cont_tbl_VendorMaster GetVendorMasterDetails(long UserID, long CustomerID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                Cont_tbl_VendorMaster vendorObj = (from row in entities.Cont_tbl_VendorMaster
                                                   where row.ID == UserID
                                                        && row.CustomerID == CustomerID
                                                        && row.IsDeleted == false
                                                   select row).FirstOrDefault();
                return vendorObj;
            }
        }

       
        public static bool DeActiveExistingVendorMapping(long contractID)
        {
            try
            {
                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                {
                    var queryResult = (from row in entities.Cont_tbl_VendorMapping
                                       where row.ContractID == contractID
                                       && row.IsActive == true
                                       select row).ToList();

                    if (queryResult.Count > 0)
                    {
                        queryResult.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static void BindVendors(ListBox lstBoxtoBindData, int customerID)
        {
            var lstVendors = GetVendors_All(customerID);

            //Drop-Down at Modal Pop-up
            lstBoxtoBindData.DataTextField = "VendorName";
            lstBoxtoBindData.DataValueField = "ID";

            lstBoxtoBindData.DataSource = lstVendors;
            lstBoxtoBindData.DataBind();

            lstBoxtoBindData.Items.Add(new ListItem("Add New", "0"));
        }
        public static List<long> GetVendorMapping(long contractID)
        {
            List<long> lstVendorMappingIDs = new List<long>();
            try
            {

                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                {
                    lstVendorMappingIDs = (from row in entities.Cont_tbl_VendorMapping
                                           where row.ContractID == contractID
                                           && row.IsActive == true
                                           select row.VendorID).ToList();
                    return lstVendorMappingIDs;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstVendorMappingIDs;
            }
        }

        #endregion
        #region Vendor Master


        public static List<Cont_SP_GetVendors_Paging_Result> GetVendorDetails_Paging(int CustomerID, string filter)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var Query = entities.Cont_SP_GetVendors_Paging(CustomerID, filter).ToList();
                return Query.ToList();
            }
        }

        public static bool ExistsVendorData(String VendorName, int CustomerID, int ContractVendorID, int VendorTypeID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var objVendor = (from row in entities.Cont_tbl_VendorMaster
                                 where row.VendorName.Equals(VendorName)
                                 && row.Type == VendorTypeID
                                 && row.CustomerID == CustomerID
                                 && row.IsDeleted == false
                                 select row);

                if (ContractVendorID != 0)
                {
                    objVendor = objVendor.Where(entry => entry.ID != ContractVendorID);
                }

                return objVendor.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool ExistsVendorName(Cont_tbl_VendorMaster objVendor, long VendorID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var query = (from row in entities.Cont_tbl_VendorMaster
                             where row.VendorName.Equals(objVendor.VendorName)
                             && row.CustomerID == objVendor.CustomerID
                             && row.IsDeleted == false
                             select row);

                if (query != null)
                {
                    if (VendorID > 0)
                    {
                        query = query.Where(entry => entry.ID != VendorID);
                    }
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool CreateVendorData(Cont_tbl_VendorMaster objVendor)
        {
            try
            {
                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                {
                    entities.Cont_tbl_VendorMaster.Add(objVendor);
                    entities.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static Cont_tbl_VendorMaster GetVendorDetailsByID(long ID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var objVendoretails = (from row in entities.Cont_tbl_VendorMaster
                                       where row.ID == ID
                                       select row).FirstOrDefault();
                return objVendoretails;
            }
        }

        public static bool UpdateVendorData(Cont_tbl_VendorMaster objVendor)
        {
            bool updateSuccess = false;
            try
            {
                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                {
                    Cont_tbl_VendorMaster ObjQuery = (from row in entities.Cont_tbl_VendorMaster
                                                      where row.ID == objVendor.ID
                                                      && row.CustomerID == objVendor.CustomerID
                                                      select row).FirstOrDefault();
                    if (ObjQuery != null)
                    {
                        ObjQuery.Type = objVendor.Type;
                        ObjQuery.VendorName = objVendor.VendorName;
                        ObjQuery.ContactPerson = objVendor.ContactPerson;
                        ObjQuery.Email = objVendor.Email;
                        ObjQuery.ContactNumber = objVendor.ContactNumber;
                        ObjQuery.Address = objVendor.Address;
                        ObjQuery.CountryID = objVendor.CountryID;
                        ObjQuery.StateID = objVendor.StateID;
                        ObjQuery.CityID = objVendor.CityID;
                        ObjQuery.VAT = objVendor.VAT;
                        ObjQuery.TIN = objVendor.TIN;
                        ObjQuery.GSTIN = objVendor.GSTIN;
                        ObjQuery.TOC = objVendor.TOC;
                        ObjQuery.PAN = objVendor.PAN;
                        ObjQuery.UpdatedBy = objVendor.UpdatedBy;
                        ObjQuery.UpdatedOn = DateTime.Now;
                        ObjQuery.isMSME = objVendor.isMSME;
                        entities.SaveChanges();

                        updateSuccess = true;
                    }
                }

                return updateSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return false;
            }
        }
        public static bool CheckVendorMapping(long vendorID)
        {
            bool CheckSuccess = true;
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var objRecord = (from row in entities.Cont_tbl_VendorMapping
                                 where row.VendorID == vendorID
                                 && row.IsActive == true
                                 select row).FirstOrDefault();
                if (objRecord != null)
                {
                    CheckSuccess = false;
                }
            }
            return CheckSuccess;
        }
        public static bool DeleteVendor(long vendorID, int customerID)
        {
            bool deleteSuccess = false;
            try
            {

                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                {
                    var objRecord = (from row in entities.Cont_tbl_VendorMaster
                                     where row.ID == vendorID
                                     && row.CustomerID == customerID
                                     select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.IsDeleted = true;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                }

                return deleteSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }

        #endregion
        public static long GetVendorIDByName(string vendorName, int customerID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var objVendorID = (from row in entities.Cont_tbl_VendorMaster
                                   where row.VendorName.Trim().ToUpper().Equals(vendorName.Trim().ToUpper())
                                   && row.CustomerID == customerID
                                   && row.IsDeleted == false
                                   select row.ID).FirstOrDefault();

                return Convert.ToInt64(objVendorID);
            }
        }

        public static long GetVendorIDByName(List<Cont_tbl_VendorMaster> lstVendors, string vendorName, int customerID)
        {
            var objVendorID = (from row in lstVendors
                               where row.VendorName.Trim().ToUpper().Equals(vendorName.Trim().ToUpper())
                               && row.CustomerID == customerID
                               && row.IsDeleted == false
                               select row.ID).FirstOrDefault();

            return Convert.ToInt64(objVendorID);
        }

        public static List<Cont_tbl_VendorMaster> GetVendors_All(long CustomerID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var Query = (from row in entities.Cont_tbl_VendorMaster
                             where row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             select row).OrderBy(entry => entry.VendorName);
                return Query.ToList();
            }
        }

    }
}
