﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.Contract
{
    public class Cont_GetMyDocuments
    {
        public long ContractID { get; set; }
        public int CustomerID { get; set; }
        public string ContractNo { get; set; }
        public string ContractTitle { get; set; }
        public string ContractDetailDesc { get; set; }
        public int CustomerBranchID { get; set; }
        public int DepartmentID { get; set; }
        public long ContractTypeID { get; set; }
        public Nullable<long> ContractSubTypeID { get; set; }
        public Nullable<System.DateTime> ProposalDate { get; set; }
        public Nullable<System.DateTime> AgreementDate { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<System.DateTime> ReviewDate { get; set; }
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public string VendorIDs { get; set; }
        public string VendorNames { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<int> RoleID { get; set; }
        public string FileTag { get; set; }
        public long ContractStatusID { get; set; }
        public string StatusName { get; set; }
    }

    public class ContractDocumentManagement
    {
        public static List<Cont_SP_MyDocumentsNew_All_Result> GetAssigned_MyDocumentsAssigned(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int vendorID, int deptID, long contractStatusID, long contractTypeID, List<string> lstSelectedFileTags)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Cont_SP_MyDocumentsNew_All_Result> query = new List<Cont_SP_MyDocumentsNew_All_Result>();
                var ContractList = (from row in entities.ContractAssignments
                                    where row.UserID == loggedInUserID
                                    && row.IsActive == true
                                    select row.ContractInstanceID).ToList();
                if (ContractList.Count > 0)
                {
                    query = (from row in entities.Cont_SP_MyDocumentsNew_All(customerID)
                             select row).ToList();

                    if (query.Count > 0)
                    {
                        if (branchList.Count > 0)
                            query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                        if (vendorID != -1)
                            query = query.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                        if (deptID != -1)
                            query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                        if (contractStatusID != -1 && contractStatusID != 0)
                            query = query.Where(entry => entry.ContractStatusID == contractStatusID).ToList();

                        if (contractTypeID != -1 && contractTypeID != 0)
                            query = query.Where(entry => entry.ContractTypeID == contractTypeID).ToList();

                        if (lstSelectedFileTags.Count > 0)
                            query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();

                        //var ContractList = (from row in entities.ContractAssignments
                        //                    where row.UserID == loggedInUserID
                        //                    && row.IsActive == true
                        //                    select row.ContractInstanceID).ToList();

                        if (ContractList.Count > 0)
                        {
                            query = query.Where(Entry => ContractList.Contains(Entry.ContractID)).ToList();
                        }
                    }

                    if (query.Count > 0)
                    {
                        query = (from g in query
                                 group g by new
                                 {
                                     g.ContractID, //
                                     g.CustomerID,
                                     g.ContractNo,
                                     g.ContractTitle,
                                     g.ContractDetailDesc,
                                     g.VendorIDs,
                                     g.VendorNames,
                                     g.CustomerBranchID,
                                     g.DepartmentID,
                                     g.ContractTypeID,
                                     g.ContractSubTypeID,
                                     g.EffectiveDate,
                                     g.ExpirationDate,
                                     g.StatusName
                                 } into GCS
                                 select new Cont_SP_MyDocumentsNew_All_Result()
                                 {
                                     ContractID = GCS.Key.ContractID,
                                     CustomerID = GCS.Key.CustomerID,
                                     ContractNo = GCS.Key.ContractNo,
                                     ContractTitle = GCS.Key.ContractTitle,
                                     ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                     VendorIDs = GCS.Key.VendorIDs,
                                     VendorNames = GCS.Key.VendorNames,
                                     CustomerBranchID = GCS.Key.CustomerBranchID,
                                     DepartmentID = GCS.Key.DepartmentID,
                                     ContractTypeID = GCS.Key.ContractTypeID,
                                     ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                     EffectiveDate = GCS.Key.EffectiveDate,
                                     ExpirationDate = GCS.Key.ExpirationDate,
                                     StatusName = GCS.Key.StatusName
                                 }).ToList();
                    }
                }
                return query.ToList();
            }
        }

        //public static List<Cont_SP_MyDocumentsNew_All_Result> GetAssigned_MyDocumentsAssigned(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int vendorID, int deptID, long contractStatusID, long contractTypeID, List<string> lstSelectedFileTags)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<Cont_SP_MyDocumentsNew_All_Result> query = new List<Cont_SP_MyDocumentsNew_All_Result>();
        //        var ContractList = (from row in entities.ContractAssignments
        //                            where row.UserID == loggedInUserID
        //                            && row.IsActive == true
        //                            select row.ContractInstanceID).ToList();
        //        if (ContractList.Count > 0)
        //        {
        //            query = (from row in entities.Cont_SP_MyDocumentsNew_All(customerID)
        //                     select row).ToList();

        //            if (query.Count > 0)
        //            {
        //                if (branchList.Count > 0)
        //                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //                if (vendorID != -1)
        //                    query = query.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

        //                if (deptID != -1)
        //                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

        //                if (contractStatusID != -1 && contractStatusID != 0)
        //                    query = query.Where(entry => entry.ContractStatusID == contractStatusID).ToList();

        //                if (contractTypeID != -1 && contractTypeID != 0)
        //                    query = query.Where(entry => entry.ContractTypeID == contractTypeID).ToList();

        //                if (lstSelectedFileTags.Count > 0)
        //                    query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();

        //                //var ContractList = (from row in entities.ContractAssignments
        //                //                    where row.UserID == loggedInUserID
        //                //                    && row.IsActive == true
        //                //                    select row.ContractInstanceID).ToList();
        //                //if (ContractList.Count > 0)
        //                //{
        //                //    query = query.Where(Entry => ContractList.Contains(Entry.ContractID)).ToList();
        //                //}
        //            }

        //            if (query.Count > 0)
        //            {
        //                query = (from g in query
        //                         group g by new
        //                         {
        //                             g.ContractID, //
        //                             g.CustomerID,
        //                             g.ContractNo,
        //                             g.ContractTitle,
        //                             g.ContractDetailDesc,
        //                             g.VendorIDs,
        //                             g.VendorNames,
        //                             g.CustomerBranchID,
        //                             g.DepartmentID,
        //                             g.ContractTypeID,
        //                             g.ContractSubTypeID,
        //                             g.EffectiveDate,
        //                             g.ExpirationDate,
        //                             g.StatusName
        //                         } into GCS
        //                         select new Cont_SP_MyDocumentsNew_All_Result()
        //                         {
        //                             ContractID = GCS.Key.ContractID,
        //                             CustomerID = GCS.Key.CustomerID,
        //                             ContractNo = GCS.Key.ContractNo,
        //                             ContractTitle = GCS.Key.ContractTitle,
        //                             ContractDetailDesc = GCS.Key.ContractDetailDesc,
        //                             VendorIDs = GCS.Key.VendorIDs,
        //                             VendorNames = GCS.Key.VendorNames,
        //                             CustomerBranchID = GCS.Key.CustomerBranchID,
        //                             DepartmentID = GCS.Key.DepartmentID,
        //                             ContractTypeID = GCS.Key.ContractTypeID,
        //                             ContractSubTypeID = GCS.Key.ContractSubTypeID,
        //                             EffectiveDate = GCS.Key.EffectiveDate,
        //                             ExpirationDate = GCS.Key.ExpirationDate,
        //                             StatusName = GCS.Key.StatusName
        //                         }).ToList();
        //            }
        //        }
        //        return query.ToList();
        //    }
        //}

        public static List<ContractDocument> GetContractDocumentDetail(long contractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AllDocument = (from row in entities.ContractDocuments
                                   where row.ContractID == contractID
                                   select row).ToList();
                return AllDocument;
            }
        }
        public static List<Cont_SP_MyDocumentsNew_All_Result> GetAssigned_MyDocumentsNew(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int vendorID, int deptID, long contractStatusID, long contractTypeID, List<string> lstSelectedFileTags)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_SP_MyDocumentsNew_All(customerID)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (vendorID != -1)
                        query = query.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (contractStatusID != -1 && contractStatusID != 0)
                        query = query.Where(entry => entry.ContractStatusID == contractStatusID).ToList();

                    if (contractTypeID != -1 && contractTypeID != 0)
                        query = query.Where(entry => entry.ContractTypeID == contractTypeID).ToList();

                    if (lstSelectedFileTags.Count > 0)
                        query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();

                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    {
                        //query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID)).ToList();
                        query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.CreatedBy == loggedInUserID)).ToList();
                    }
                    else // In case of MGMT or CADMN
                    {
                        query = query.Where(entry => entry.RoleID == roleID).Distinct().ToList();
                    }
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.ContractID, //
                                 g.CustomerID,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.ContractDetailDesc,
                                 g.VendorIDs,
                                 g.VendorNames,
                                 g.CustomerBranchID,
                                 g.DepartmentID,
                                 g.ContractTypeID,
                                 g.ContractSubTypeID,
                                 g.EffectiveDate,
                                 g.ExpirationDate,
                                 g.StatusName
                             } into GCS
                             select new Cont_SP_MyDocumentsNew_All_Result()
                             {
                                 ContractID = GCS.Key.ContractID,
                                 CustomerID = GCS.Key.CustomerID,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                 VendorIDs = GCS.Key.VendorIDs,
                                 VendorNames = GCS.Key.VendorNames,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 ContractTypeID = GCS.Key.ContractTypeID,
                                 ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                 EffectiveDate = GCS.Key.EffectiveDate,
                                 ExpirationDate = GCS.Key.ExpirationDate,
                                 StatusName = GCS.Key.StatusName
                             }).ToList();
                }

                return query.ToList();
            }
        }

        public static List<Cont_SP_GetContractDocuments_Paging_Result> GetContractDocuments_Paging(int customerID, long contractInstanceID, int pageSize, int pageNumber)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = entities.Cont_SP_GetContractDocuments_Paging(pageSize, pageNumber, contractInstanceID, customerID).ToList();

                //if (queryResult.Count > 0)
                //    queryResult = queryResult.OrderBy(entry => entry.FileName).ThenByDescending(entry => entry.CreatedOn).ToList();

                return queryResult;
            }
        }

        public static List<Cont_SP_GetContractDocuments_All_Result> GetContractDocuments_All(int customerID, long contractInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = entities.Cont_SP_GetContractDocuments_All(contractInstanceID, customerID).ToList();

                if (queryResult.Count > 0)
                    queryResult = queryResult.OrderBy(row => row.FileName).ToList();

                return queryResult;
            }
        }

        //public static List<Cont_SP_MyDocuments_All_Result> GetAssigned_MyDocuments(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int vendorID, int deptID, long contractStatusID, long contractTypeID, List<string> lstSelectedFileTags)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var query = (from row in entities.Cont_SP_MyDocuments_All(customerID)
        //                     select row).ToList();

        //        if (query.Count > 0)
        //        {
        //            if (branchList.Count > 0)
        //                query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //            if (vendorID != -1)
        //                query = query.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

        //            if (deptID != -1)
        //                query = query.Where(entry => entry.DepartmentID == deptID).ToList();

        //            if (contractStatusID != -1 && contractStatusID != 0)
        //                query = query.Where(entry => entry.ContractStatusID == contractStatusID).ToList();

        //            if (contractTypeID != -1 && contractTypeID != 0)
        //                query = query.Where(entry => entry.ContractTypeID == contractTypeID).ToList();

        //            if (lstSelectedFileTags.Count > 0)
        //                query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();

        //            if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
        //                query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID)).ToList();
        //            else // In case of MGMT or CADMN 
        //            {
        //                query = query.Where(entry => entry.RoleID == roleID).Distinct().ToList();
        //            }
        //        }

        //        if (query.Count > 0)
        //        {
        //            query = (from g in query
        //                     group g by new
        //                     {
        //                         g.ContractID, //
        //                         g.CustomerID,
        //                         g.ContractNo,
        //                         g.ContractTitle,
        //                         g.ContractDetailDesc,
        //                         g.VendorIDs,
        //                         g.VendorNames,
        //                         g.CustomerBranchID,                                
        //                         g.DepartmentID,                                 
        //                         g.ContractTypeID,                                
        //                         g.ContractSubTypeID,                               
        //                         g.EffectiveDate,
        //                         g.ExpirationDate,                                
        //                         g.StatusName
        //                     } into GCS
        //                     select new Cont_SP_MyDocuments_All_Result()
        //                     {
        //                         ContractID = GCS.Key.ContractID, 
        //                         CustomerID = GCS.Key.CustomerID,
        //                         ContractNo = GCS.Key.ContractNo,
        //                         ContractTitle = GCS.Key.ContractTitle,
        //                         ContractDetailDesc = GCS.Key.ContractDetailDesc,
        //                         VendorIDs = GCS.Key.VendorIDs,
        //                         VendorNames = GCS.Key.VendorNames,
        //                         CustomerBranchID = GCS.Key.CustomerBranchID,                                
        //                         DepartmentID = GCS.Key.DepartmentID,                                 
        //                         ContractTypeID = GCS.Key.ContractTypeID,                                
        //                         ContractSubTypeID = GCS.Key.ContractSubTypeID,                                 
        //                         EffectiveDate = GCS.Key.EffectiveDate,
        //                         ExpirationDate = GCS.Key.ExpirationDate,                                
        //                         StatusName = GCS.Key.StatusName
        //                     }).ToList();
        //        }               

        //        return query.ToList();
        //    }
        //}

        public static List<Cont_SP_MyDocuments_All_Result> GetAssigned_MyDocuments(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int vendorID, int deptID, long contractStatusID, long contractTypeID, List<string> lstSelectedFileTags)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                var query = (from row in entities.Cont_SP_MyDocuments_All(customerID)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (vendorID != -1)
                        query = query.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (contractStatusID != -1 && contractStatusID != 0)
                        query = query.Where(entry => entry.ContractStatusID == contractStatusID).ToList();

                    if (contractTypeID != -1 && contractTypeID != 0)
                        query = query.Where(entry => entry.ContractTypeID == contractTypeID).ToList();

                    if (lstSelectedFileTags.Count > 0)
                        query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();

                    if (isexist != null)
                    {
                        var assignedLocationList = CustomerBranchManagement.GetAllAssignedContractListbyuser(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole);
                        query = query.Where(entry => assignedLocationList.Contains(entry.ContractNo)).ToList();

                    }
                   else if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    {
                        //query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID)).ToList();
                        query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.CreatedBy == loggedInUserID)).ToList();
                    }
                    else // In case of MGMT or CADMN 
                    {
                        query = query.Where(entry => entry.RoleID == roleID).Distinct().ToList();
                    }
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.ContractID, //
                                 g.CustomerID,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.ContractDetailDesc,
                                 g.VendorIDs,
                                 g.VendorNames,
                                 g.CustomerBranchID,
                                 g.DepartmentID,
                                 g.ContractTypeID,
                                 g.ContractSubTypeID,
                                 g.EffectiveDate,
                                 g.ExpirationDate,
                                 g.StatusName
                             } into GCS
                             select new Cont_SP_MyDocuments_All_Result()
                             {
                                 ContractID = GCS.Key.ContractID,
                                 CustomerID = GCS.Key.CustomerID,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                 VendorIDs = GCS.Key.VendorIDs,
                                 VendorNames = GCS.Key.VendorNames,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 ContractTypeID = GCS.Key.ContractTypeID,
                                 ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                 EffectiveDate = GCS.Key.EffectiveDate,
                                 ExpirationDate = GCS.Key.ExpirationDate,
                                 StatusName = GCS.Key.StatusName
                             }).ToList();
                }

                return query.ToList();
            }
        }

        public static List<Cont_SP_GetContractDocuments_FileTags_All_Result>Cont_SP_GetContractDocuments_FileTags_All(int customerID, long contractInstanceID, List<string> lstSelectedFileTags)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = entities.Cont_SP_GetContractDocuments_FileTags_All(contractInstanceID, customerID).ToList();

                if (lstSelectedFileTags.Count > 0)
                    queryResult = queryResult.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();

                if (queryResult.Count > 0)
                {                   
                    queryResult = (from g in queryResult
                             group g by new
                             {
                                 g.FileID, //ContractID
                                 g.CustomerID,
                                 g.DocTypeID,
                                 g.DocTypeName,
                                 g.FileName,
                                 g.FilePath,
                                 g.FileKey,
                                 g.Version,
                                 g.VersionDate,
                                 g.VersionComment,
                                 g.UploadedByName,
                                 g.CreatedBy,
                                 g.CreatedOn,                                
                             } into GCS
                             select new Cont_SP_GetContractDocuments_FileTags_All_Result()
                             {
                                 FileID = GCS.Key.FileID, //ContractID
                                 CustomerID = GCS.Key.CustomerID,
                                 DocTypeID = GCS.Key.DocTypeID,
                                 DocTypeName = GCS.Key.DocTypeName,
                                 FileName = GCS.Key.FileName,
                                 FilePath = GCS.Key.FilePath,
                                 FileKey = GCS.Key.FileKey,
                                 Version = GCS.Key.Version,
                                 VersionDate = GCS.Key.VersionDate,
                                 VersionComment = GCS.Key.VersionComment,
                                 UploadedByName = GCS.Key.UploadedByName,
                                 CreatedBy = GCS.Key.CreatedBy,
                                 CreatedOn = GCS.Key.CreatedOn,                                
                             }).ToList();
                }

                return queryResult;
            }
        }

        public static int ExistsContractDocumentReturnVersion(Cont_tbl_FileData newDocumentRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Cont_tbl_FileData
                                 where row.ContractID == newDocumentRecord.ContractID
                                 && row.DocTypeID == newDocumentRecord.DocTypeID
                                 && row.FileName == newDocumentRecord.FileName
                                 && row.IsDeleted == false
                                 select row).ToList();

                    if (query.Count > 0)
                        return query.Count;
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static List<string> GetDistinctFileTags(int customerID, long contractID = 0)
        {
            try
            {
                List<string> lstFileTags = new List<string>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (contractID != 0)
                    {
                         lstFileTags = (from row in entities.Cont_tbl_FileDataTagsMapping
                                           join row1 in entities.Cont_tbl_FileData
                                           on row.FileID equals row1.ID
                                           where row1.ContractID == contractID
                                           && row1.CustomerID == customerID
                                           && row1.IsDeleted == false
                                           && row.IsActive == true
                                          select row.FileTag.Trim()).Distinct().ToList();
                    }
                    else
                    {
                         lstFileTags = (from row in entities.Cont_tbl_FileDataTagsMapping
                                           join row1 in entities.Cont_tbl_FileData
                                           on row.FileID equals row1.ID
                                           where row1.CustomerID == customerID
                                           && row1.IsDeleted == false
                                           && row.IsActive == true
                                        select row.FileTag.Trim()).Distinct().ToList();
                    }

                    return lstFileTags;
                }
            }
            catch(Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new List<string>();
            }
        }

        public static List<Cont_tbl_FileData> GetDocumentsByIDs(List<long> fileIDs)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_tbl_FileData
                             where fileIDs.Contains(row.ID)
                             select row).ToList();

                return query;
            }
        }

        public static Cont_tbl_FileData GetContractDocumentByID(long contFileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Cont_tbl_FileData file = entities.Cont_tbl_FileData.Where(x => x.ID == contFileID).FirstOrDefault();
                return file;
            }
        }

        public static Cont_SP_GetFileDetails_Result GetDocumentDetailByFileID(long contFileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Cont_SP_GetFileDetails_Result file = entities.Cont_SP_GetFileDetails(contFileID).FirstOrDefault();
                return file;
            }
        }

        public static long CreateContractDocumentMapping(Cont_tbl_FileData newContDoc)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    newContDoc.EnType = "A";
                    entities.Cont_tbl_FileData.Add(newContDoc);
                    entities.SaveChanges();
                    return newContDoc.ID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool UpdateContractDocumentMapping(Cont_tbl_FileData newContDoc)
        {
            bool updateSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.Cont_tbl_FileData
                                      where row.ID == newContDoc.ID                                     
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.DocTypeID = newContDoc.DocTypeID;
                        //prevRecord.UpdatedBy = newContDoc.UpdatedBy;
                        //prevRecord.UpdatedOn = newContDoc.UpdatedOn;
                        entities.SaveChanges();

                        updateSuccess = true;
                    }
                }

                return updateSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return updateSuccess;
            }
        }

        public static bool DeleteContDocument(long contFileID, int deletedByUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_FileData fileToDelete = entities.Cont_tbl_FileData.Where(x => x.ID == contFileID).FirstOrDefault();

                    if (fileToDelete != null)
                    {
                        fileToDelete.IsDeleted = true;
                        fileToDelete.DeletedBy = deletedByUserID;
                        fileToDelete.DeletedOn = DateTime.Now;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<Cont_tbl_FileData> GetContractDocumentsByDocTypeID(long contractID, long docTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AllDocument = (from row in entities.Cont_tbl_FileData
                                   where row.ContractID == contractID 
                                   && row.IsDeleted==false 
                                   select row).ToList();

                if (docTypeID != 0)
                {
                    AllDocument = AllDocument.Where(row => row.DocTypeID == docTypeID).ToList();
                }

                return AllDocument;
            }
        }

        public static bool ExistsContractDocumentType(String DocumentType, int CustomerID, int ContractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objDocType = (from row in entities.Cont_tbl_DocumentTypeMaster
                                  where row.TypeName.Equals(DocumentType)
                               && row.CustomerID == CustomerID && row.IsDeleted == false
                                  select row);

                if (ContractID != 0)
                {
                    objDocType = objDocType.Where(entry => entry.ID != ContractID);
                }

                return objDocType.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool CreateUpdate_FileTagsMapping(List<Cont_tbl_FileDataTagsMapping> lstRecords)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstRecords.ForEach(_objRecord =>
                    {
                        var prevRecord = (from row in entities.Cont_tbl_FileDataTagsMapping
                                                    where row.FileID == _objRecord.FileID
                                                    && row.FileTag.Trim().ToUpper().Equals(_objRecord.FileTag.Trim().ToUpper())
                                                    select row).FirstOrDefault();

                        if (prevRecord != null)
                        {
                            prevRecord.IsActive = true;

                            prevRecord.UpdatedBy = _objRecord.UpdatedBy;
                            prevRecord.UpdatedOn = DateTime.Now;

                            saveSuccess = true;
                        }
                        else
                        {
                            _objRecord.CreatedOn = DateTime.Now;
                            _objRecord.UpdatedOn = DateTime.Now;

                            entities.Cont_tbl_FileDataTagsMapping.Add(_objRecord);
                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool Delete_FileTagsMapping(long contFileID, int deletedByUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var filesToDelete = entities.Cont_tbl_FileDataTagsMapping.Where(x => x.FileID == contFileID).ToList();

                    if (filesToDelete != null && filesToDelete.Count > 0)
                    {
                        filesToDelete.ForEach(entry => entry.IsActive = false);
                        filesToDelete.ForEach(entry => entry.UpdatedBy = deletedByUserID);
                        filesToDelete.ForEach(entry => entry.UpdatedOn = DateTime.Now);                        
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
    }
}
