﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Business.Contract
{
    public class ContractTemplateManagement
    {
        public static void CreateContractTemplateSection(Cont_tbl_ContractTemplateSectionMapping _objTemplate)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Cont_tbl_ContractTemplateSectionMapping.Add(_objTemplate);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void CreateContractTemplateAssignment1(ContractTemplateAssignment _objTemplate)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ContractTemplateAssignment objRecord = (from row in entities.ContractTemplateAssignments
                                                            where row.ContractTemplateInstanceID == _objTemplate.ContractTemplateInstanceID
                                                            && row.SectionID == _objTemplate.SectionID
                                                            && row.RoleID == _objTemplate.RoleID
                                                            select row).FirstOrDefault();

                    if (objRecord != null)
                    {

                        if (objRecord.UserID == _objTemplate.UserID)
                        {
                            objRecord.Visibility = _objTemplate.Visibility;
                            objRecord.IsActive = true;
                            entities.SaveChanges();
                        }
                        else
                        {
                            objRecord.IsActive = false;
                            entities.SaveChanges();

                            entities.ContractTemplateAssignments.Add(_objTemplate);
                            entities.SaveChanges();
                        }
                    }
                    else
                    {
                        entities.ContractTemplateAssignments.Add(_objTemplate);
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void DeleteContractTemplateAssignmentApprover(int ContractTemplateInstanceID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var objRecord = (from row in entities.ContractTemplateAssignments
                                     where row.ContractTemplateInstanceID == ContractTemplateInstanceID
                                     && row.RoleID == 6
                                     select row).ToList();
                    if (objRecord != null)
                    {
                        objRecord.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();

                        objRecord.ForEach(entry => entry.ApproveOrder = 0);
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void CreateContractTemplateAssignmentApprover(ContractTemplateAssignment _objTemplate)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ContractTemplateAssignment objRecord = (from row in entities.ContractTemplateAssignments
                                                            where row.ContractTemplateInstanceID == _objTemplate.ContractTemplateInstanceID
                                                            && row.SectionID == _objTemplate.SectionID
                                                            && row.RoleID == _objTemplate.RoleID
                                                            && row.UserID == _objTemplate.UserID
                                                            select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        if (objRecord.UserID == _objTemplate.UserID)
                        {
                            objRecord.Visibility = _objTemplate.Visibility;
                            objRecord.ApproveOrder = _objTemplate.ApproveOrder;
                            objRecord.IsAction = _objTemplate.IsAction;
                            objRecord.IsActive = _objTemplate.IsActive;
                            entities.SaveChanges();
                        }
                        else
                        {
                            objRecord.IsActive = false;
                            entities.SaveChanges();

                            entities.ContractTemplateAssignments.Add(_objTemplate);
                            entities.SaveChanges();
                        }
                    }
                    else
                    {
                        entities.ContractTemplateAssignments.Add(_objTemplate);
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void CreateContractTemplateAssignment(ContractTemplateAssignment _objTemplate)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ContractTemplateAssignment objRecord = (from row in entities.ContractTemplateAssignments
                                                            where row.ContractTemplateInstanceID == _objTemplate.ContractTemplateInstanceID
                                                            && row.SectionID == _objTemplate.SectionID
                                                            && row.RoleID == _objTemplate.RoleID
                                                            && row.IsActive == true
                                                            select row).FirstOrDefault();

                    if (objRecord != null)
                    {

                        if (objRecord.UserID == _objTemplate.UserID)
                        {
                            objRecord.Visibility = _objTemplate.Visibility;
                            objRecord.IsActive = true;
                            entities.SaveChanges();
                        }
                        else
                        {
                            objRecord.IsActive = false;
                            entities.SaveChanges();

                            entities.ContractTemplateAssignments.Add(_objTemplate);
                            entities.SaveChanges();
                        }
                    }
                    else
                    {
                        entities.ContractTemplateAssignments.Add(_objTemplate);
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<Cont_SP_GetContractSections_All_Result> GetContractSections_All(int customerID, int ContractTemplateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstSections = entities.Cont_SP_GetContractSections_All(customerID, ContractTemplateID).ToList();
                return lstSections;
            }
        }
        public static string getVersion(int ContractTemplateID, int CustomerID)
        {
            string updateSuccess = string.Empty;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_ContractInstance objRecord = (from row in entities.Cont_tbl_ContractInstance
                                                           where row.ID == ContractTemplateID
                                                           && row.CustomerID == CustomerID
                                                           && row.IsDeleted == false
                                                           select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        updateSuccess = objRecord.Version;
                    }
                }

                return updateSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return updateSuccess;
            }
        }

        public static void CreateTemplateAssignment(TemplateAssignment _objTemplate)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TemplateAssignment objRecord = (from row in entities.TemplateAssignments
                                                    where row.TemplateInstanceID == _objTemplate.TemplateInstanceID
                                                    && row.SectionID == _objTemplate.SectionID
                                                    && row.RoleID == _objTemplate.RoleID
                                                    && row.IsActive == true
                                                    select row).FirstOrDefault();

                    if (objRecord != null)
                    {
                        if (objRecord.UserID == _objTemplate.UserID)
                        {
                            objRecord.IsActive = true;
                            entities.SaveChanges();
                        }
                        else
                        {
                            objRecord.IsActive = false;
                            entities.SaveChanges();

                            entities.TemplateAssignments.Add(_objTemplate);
                            entities.SaveChanges();
                        }
                    }
                    else
                    {
                        entities.TemplateAssignments.Add(_objTemplate);
                        entities.SaveChanges();
                    }
                }
                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{
                //    entities.TemplateAssignments.Add(_objTemplate);
                //    entities.SaveChanges();
                //}
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void DeleteTemplateAssignment(int TemplateInstanceID, int RoleID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var objRecord = (from row in entities.TemplateAssignments
                                     where row.TemplateInstanceID == TemplateInstanceID
                                     && row.RoleID == RoleID
                                     select row).ToList();
                    if (objRecord != null)
                    {
                        objRecord.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static Cont_tbl_SectionMaster GetSectionDetailsByID(long sectionID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var record = (from row in entities.Cont_tbl_SectionMaster
                               where row.ID == sectionID
                               && row.CustomerID == customerID
                               select row).FirstOrDefault();
                return record;
            }
        }

        public static bool ExistsSection(Cont_tbl_SectionMaster _objSection, int CustomerID, long sectionID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objRecord = (from row in entities.Cont_tbl_SectionMaster
                                  where row.Header.Trim().ToUpper().Equals(_objSection.Header.Trim().ToUpper())
                                  && row.Version.Trim().ToUpper().Equals(_objSection.Version.Trim().ToUpper())
                                  && row.CustomerID == CustomerID 
                                  && row.IsActive == true
                                  select row);

                if (sectionID != 0)
                {
                    objRecord = objRecord.Where(entry => entry.ID != sectionID);
                }

                return objRecord.Select(entry => true).SingleOrDefault();
            }
        }

        public static long CreateSection(Cont_tbl_SectionMaster _objSection)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Cont_tbl_SectionMaster.Add(_objSection);
                    entities.SaveChanges();

                    return _objSection.ID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool UpdateSection(Cont_tbl_SectionMaster _objSection)
        {
            bool updateSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_SectionMaster objRecord = (from row in entities.Cont_tbl_SectionMaster
                                                     where row.ID == _objSection.ID
                                                     && row.CustomerID == _objSection.CustomerID
                                                     select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.Header = _objSection.Header;
                        objRecord.Version = _objSection.Version;
                        objRecord.BodyContent = _objSection.BodyContent;
                        objRecord.DeptID = _objSection.DeptID;
                        objRecord.UpdatedBy = _objSection.UpdatedBy;
                        objRecord.UpdatedOn = _objSection.UpdatedOn;
                        objRecord.Headervisibilty = _objSection.Headervisibilty;                        
                        entities.SaveChanges();
                        updateSuccess = true;
                    }
                }

                return updateSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return updateSuccess;
            }
        }

        public static bool DeleteSection(long sectionID, int customerID, int loggedInUserID)
        {
            bool deleteSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_SectionMaster objRecord = (from row in entities.Cont_tbl_SectionMaster
                                                      where row.ID == sectionID
                                                      && row.CustomerID == customerID
                                                      select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.IsActive = false;
                        objRecord.UpdatedBy = loggedInUserID;
                        objRecord.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                        deleteSuccess = true;
                    }
                }
                return deleteSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }

        public static List<Cont_SP_GetSections_Paging_Result> GetSections_Paging(int CutomerID, string Filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstSections = entities.Cont_SP_GetSections_Paging(CutomerID, Filter).ToList();

                return lstSections;
            }
        }

        public static List<Cont_SP_GetSections_All_Result> GetSections_All(int customerID, string Filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstSections = entities.Cont_SP_GetSections_All(customerID, Filter).ToList();
                return lstSections;
            }
        }

        public static Cont_tbl_TemplateMaster GetTemplateDetailsByID(long templateID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var record = (from row in entities.Cont_tbl_TemplateMaster
                              where row.ID == templateID
                              && row.CustomerID == customerID
                              select row).FirstOrDefault();
                return record;
            }
        }

        public static bool ExistsTemplate(Cont_tbl_TemplateMaster _objTemplate, int customerID, long sectionID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objRecord = (from row in entities.Cont_tbl_TemplateMaster
                                 where row.TemplateName.Trim().ToUpper().Equals(_objTemplate.TemplateName.Trim().ToUpper())
                                 && row.Version.Trim().ToUpper().Equals(_objTemplate.Version.Trim().ToUpper())
                                 && row.CustomerID == customerID
                                 && row.IsDeleted == false
                                 select row);

                if (sectionID != 0)
                {
                    objRecord = objRecord.Where(entry => entry.ID != sectionID);
                }

                return objRecord.Select(entry => true).SingleOrDefault();
            }
        }

        public static long CreateTemplate(Cont_tbl_TemplateMaster _objTemplate)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Cont_tbl_TemplateMaster.Add(_objTemplate);
                    entities.SaveChanges();

                    return _objTemplate.ID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool UpdateTemplate(Cont_tbl_TemplateMaster _objTemplate)
        {
            bool updateSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_TemplateMaster objRecord = (from row in entities.Cont_tbl_TemplateMaster
                                                        where row.ID == _objTemplate.ID
                                                        && row.CustomerID == _objTemplate.CustomerID
                                                        select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.TemplateName = _objTemplate.TemplateName;
                        objRecord.Version = _objTemplate.Version;
                        
                        objRecord.UpdatedBy = _objTemplate.UpdatedBy;
                        objRecord.UpdatedOn = _objTemplate.UpdatedOn;

                        entities.SaveChanges();
                        updateSuccess = true;
                    }
                }

                return updateSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return updateSuccess;
            }
        }

        public static bool DeleteTemplate(long templateID, int customerID, int loggedInUserID)
        {
            bool deleteSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_TemplateMaster objRecord = (from row in entities.Cont_tbl_TemplateMaster
                                                        where row.ID == templateID
                                                        && row.CustomerID == customerID
                                                        select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.IsDeleted = true;

                        objRecord.UpdatedBy = loggedInUserID;
                        objRecord.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                        deleteSuccess = true;
                    }
                }
                return deleteSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }

    
        public static List<Cont_SP_GetTemplates_Paging_Result> GetTemplates_Paging(int UserID, int CutomerID, string Filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstTemplates = entities.Cont_SP_GetTemplates_Paging(CutomerID, UserID, Filter).ToList();

                return lstTemplates;
            }
        }

        public static bool CreateUpdate_TemplateSectionMapping(List<Cont_tbl_TemplateSectionMapping> lstObjMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool saveSuccess = false;
                try
                {
                    lstObjMapping.ForEach(eachRecord =>
                    {
                        var _objMappingExists = (from row in entities.Cont_tbl_TemplateSectionMapping
                                                   where row.TemplateID == eachRecord.TemplateID
                                                   && row.SectionID == eachRecord.SectionID
                                                   select row).FirstOrDefault();

                        if (_objMappingExists != null)
                        {
                            _objMappingExists.SectionOrder = eachRecord.SectionOrder;
                            _objMappingExists.IsActive = true;

                            _objMappingExists.UpdatedBy = eachRecord.CreatedBy;
                            _objMappingExists.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            entities.Cont_tbl_TemplateSectionMapping.Add(eachRecord);
                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool DeActiveExistingTemplateSectionMapping(long templateID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var queryResult = (from row in entities.Cont_tbl_TemplateSectionMapping
                                       where row.TemplateID == templateID
                                       && row.IsActive == true
                                       select row).ToList();

                    if (queryResult.Count > 0)
                    {
                        queryResult.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool GetTemplateSectionMapping(long templateID, long UserID)
        {
            bool CheckUpdate = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstObjMappingIDs = (from row in entities.Cont_tbl_TemplateSectionMapping
                                           where row.TemplateID == templateID
                                           && row.IsActive == true
                                           select row).ToList();

                    foreach (var item in lstObjMappingIDs)
                    {
                        item.IsActive = false;
                        item.UpdatedBy = Convert.ToInt32(UserID);
                        item.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                    }
                    return CheckUpdate=true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return CheckUpdate;
            }
        }
        public static List<long> GetTemplateSectionMapping(long templateID)
        {
            List<long> lstObjMappingIDs = new List<long>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstObjMappingIDs = (from row in entities.Cont_tbl_TemplateSectionMapping
                                           where row.TemplateID == templateID
                                           && row.IsActive == true
                                           select row.SectionID).ToList();
                    return lstObjMappingIDs;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstObjMappingIDs;
            }
        }

        public static List<Cont_tbl_TemplateSectionMapping> GetExistingTemplateSectionMapping(long templateID)
        {
            List<Cont_tbl_TemplateSectionMapping> lstObjMappings = new List<Cont_tbl_TemplateSectionMapping>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstObjMappings = (from row in entities.Cont_tbl_TemplateSectionMapping
                                        where row.TemplateID == templateID
                                        && row.IsActive == true
                                        select row).ToList();
                    return lstObjMappings;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstObjMappings;
            }
        }
    }
}
