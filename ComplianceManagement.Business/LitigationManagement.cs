﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Data;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class LitigationManagement
    {
        public static List<tbl_Advcatebillapprovers> GetAllAdvBillApprovers(string Type, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstAppType = (from row in entities.tbl_Advcatebillapprovers
                                  where row.ApproverLevel == Type
                                  && row.IsDeleted == false
                                  && row.CustomerID == CustomerID
                                  select row);
                return lstAppType.ToList();
            }
        }
        public static List<int> GetAllAssigneConEntityHierarchy(int customerID, int customerBranchID, int userID)
        {
            List<NameValueHierarchy> hierarchy = null;

            List<int> branchHierarchyList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var masterListCustomerBranches = (from row in entities.Cont_tbl_ContractInstance
                                                  join row1 in entities.Cont_tbl_UserAssignment
                                                  on row.ID equals row1.ContractID
                                                  join row2 in entities.CustomerBranches
                                                  on row.CustomerBranchID equals row2.ID
                                                  join row3 in entities.ContractEntitiesAssignments
                                                  on row2.ID equals row3.BranchID
                                                  where row.IsDeleted == false
                                                  && row2.CustomerID == customerID
                                                  && (row.CreatedBy == userID || row1.UserID == userID || row.UpdatedBy == userID)
                                                  select row2).Distinct();


                var query = masterListCustomerBranches.Where(row => row.ID == customerBranchID);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    branchHierarchyList.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities, branchHierarchyList, masterListCustomerBranches);
                }
            }

            return branchHierarchyList;
        }

        public static bool SavePageAutorizedval(tbl_PageAuthorizationMaster _objpageAuthorized)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var _objPageAuth = (from row in entities.tbl_PageAuthorizationMaster
                                        where row.UserID == _objpageAuthorized.UserID
                                        && row.isActive == true
                                        && row.PageID == _objpageAuthorized.PageID
                                        && row.UserID == _objpageAuthorized.UserID
                                        select row).FirstOrDefault();
                    if (_objPageAuth != null)
                    {
                        _objPageAuth.Addval = _objpageAuthorized.Addval;
                        _objPageAuth.Deleteval = _objpageAuthorized.Deleteval;
                        _objPageAuth.Modify = _objpageAuthorized.Modify;
                        _objPageAuth.Viewval = _objpageAuthorized.Viewval;
                    }
                    else
                    {
                        entities.tbl_PageAuthorizationMaster.Add(_objpageAuthorized);

                    }
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static List<Temp_MisReort> GetMISFielsDetails()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Temp_MisReort
                                   select row).ToList();

                return queryResult;
            }
        }

        public static tbl_CaseSummary getalldtlsofCaseSummary(int Id, int CustomerId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var getdtls = (from row in entities.tbl_CaseSummary
                                   where row.CustomerId == CustomerId
                                   && row.Id == Id
                                   select row).FirstOrDefault();
                    return getdtls;
                }

            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }




        //public static bool saveMisReportFieldValue(List<Temp_MisReort> objMainList)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        try
        //        {
        //            objMainList.ForEach(eachMISRecord =>
        //            {
        //                var checkUser = (from row in entities.Temp_MisReort
        //                                 where row.Name == eachMISRecord.Name
        //                                 && row.Ischeck == eachMISRecord.Ischeck
        //                                 && row.CutomerId == eachMISRecord.CutomerId
        //                                 select row).FirstOrDefault();
        //                if (checkUser != null)
        //                {
                         
        //                    checkUser.Ischeck = eachMISRecord.Ischeck;
        //                    entities.SaveChanges();
        //                }
        //                else
        //                {
        //                    entities.Temp_MisReort.Add(eachMISRecord);
        //                }
        //            });

        //            entities.SaveChanges();

        //            return true;
        //        }
        //        catch (Exception ex)
        //        {
        //            LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //            return false;
        //        }
        //    }
        //}

        public static List<tbl_CaseType> GetAllPerticularsbyCustomerId(int customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstofPerticulars = (from row in entities.tbl_CaseType
                                            where row.CustomerID == customerID
                                            select row).ToList();
                    return lstofPerticulars;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static bool DeleteCaseSummaryDetailByID(int Id, long customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var deletecaseSummarybyId = (from row in entities.tbl_CaseSummary
                                                 where row.Id == Id && row.isActive == true
                                                 && row.CustomerId == customerID
                                                 select row).FirstOrDefault();
                    if (deletecaseSummarybyId != null)
                    {
                        deletecaseSummarybyId.isActive = false;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CheckCaseSummarydtls(int Id, long customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var checkcaseSummarybyId = (from row in entities.tbl_CaseSummary
                                                where row.Id == Id && row.isActive == true
                                                && row.CustomerId == customerID
                                                select row).FirstOrDefault();
                    if (checkcaseSummarybyId != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<State> getAllSatatevalue()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var state = (from row in entities.States
                                 select row).ToList();
                    return state;

                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<City> getAllJurisdictionvalue()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var city = (from row in entities.Cities
                                 select row).ToList();
                    return city ;

                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static void CreatePerticularsDetails(tbl_Perticulars objper)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.tbl_Perticulars.Add(objper);
                    entities.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static bool CheckPerticularsExist(tbl_Perticulars objper)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var checkPerticularExist = (from row in entities.tbl_Perticulars
                                                where row.isDelete == false
                                                && row.CustomerId == objper.CustomerId
                                                && row.Perticulars == objper.Perticulars
                                                select row).FirstOrDefault();
                    if (checkPerticularExist != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CheckCaseSummaryExist(tbl_CaseSummary _objcasesummary)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var checkExistanceofCS = (from row in entities.tbl_CaseSummary
                                              where row.PerticularsId == _objcasesummary.PerticularsId
                                              && row.CustomerId == _objcasesummary.CustomerId
                                              && row.StateId == _objcasesummary.StateId
                                              && row.isActive == true
                                              select row).FirstOrDefault();
                    if (checkExistanceofCS != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<State> getAllState()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lisofStates = (from row in entities.States
                                       orderby row.Name
                                       select row).ToList();
                    return lisofStates;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static bool UpdateCaseSummuryDetails(tbl_CaseSummary _objcasesummary)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (_objcasesummary != null)
                    {
                        var updateCaseSummary = (from row in entities.tbl_CaseSummary
                                                 where row.Id == _objcasesummary.Id
                                                 && row.CustomerId == _objcasesummary.CustomerId
                                                 && row.isActive == true
                                                 select row).FirstOrDefault();
                        if (updateCaseSummary != null)
                        {
                            updateCaseSummary.StateId = _objcasesummary.StateId;
                            updateCaseSummary.PerticularsId = _objcasesummary.PerticularsId;
                            updateCaseSummary.DisputedAmt = _objcasesummary.DisputedAmt;
                            updateCaseSummary.Interest = _objcasesummary.Interest;
                            updateCaseSummary.Penalty = _objcasesummary.Penalty;
                            updateCaseSummary.Total = _objcasesummary.Total;
                            updateCaseSummary.AmountPaid = _objcasesummary.AmountPaid;
                            updateCaseSummary.Favourable = _objcasesummary.Favourable;
                            updateCaseSummary.Unfavourable = _objcasesummary.Unfavourable;
                            updateCaseSummary.Expense = _objcasesummary.Expense;
                            updateCaseSummary.Updatedby = _objcasesummary.Updatedby;
                            updateCaseSummary.UpdatedOn = _objcasesummary.UpdatedOn;
                            updateCaseSummary.isActive = true;
                            entities.SaveChanges();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static void UpdatePerticularsDetails(tbl_Perticulars objper)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var updatePerticular = (from row in entities.tbl_Perticulars
                                            where row.Perticulars == objper.Perticulars
                                            && row.isDelete == false
                                            select row).FirstOrDefault();
                    if (updatePerticular != null)
                    {
                        updatePerticular.Perticulars = objper.Perticulars;
                        updatePerticular.isDelete = false;
                        updatePerticular.Updatedby = objper.Updatedby;
                        updatePerticular.UpdatedOn = objper.UpdatedOn;
                        entities.SaveChanges();

                    }
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<SP_CaseSummary_Result> GetCaseSpecimanDetails(int CustomerId,string Filter)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var queryResult = (from row in entities.SP_CaseSummary(CustomerId)
                                       select row).ToList();
                    if(Filter!="" && Filter!=null)
                    {
                        queryResult = (from row in queryResult where row.Perticulars != null select row).ToList();
                        queryResult = queryResult.Where(Entry => Entry.Perticulars.ToLower().Contains(Filter.ToLower())).ToList();
                    }

                    return queryResult;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool saveCaseSummary(tbl_CaseSummary _objcasesummary)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (_objcasesummary != null)
                    {
                        entities.tbl_CaseSummary.Add(_objcasesummary);
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeActiveExistingeCustomsFieldByNoticeOrCaseID(int noticeOrCase, long caseNoticeInstanceID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var QueryResult = (from row in entities.tbl_NoticeCaseCustomParameter
                                       where row.NoticeCaseInstanceID == caseNoticeInstanceID
                                       && row.NoticeCaseType == noticeOrCase
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                       select row).ToList();

                    if (QueryResult.Count > 0)
                    {
                        QueryResult.ForEach(entry => entry.IsActive = false);
                        QueryResult.ForEach(entry => entry.IsDeleted = true);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static SP_Litigation_GetCustomParameters_Result GetColumnTotal(List<SP_Litigation_GetCustomParameters_Result> lstCustomParameters)
        {
            SP_Litigation_GetCustomParameters_Result totalRecord = new SP_Litigation_GetCustomParameters_Result()
            {
                LableID = 0,
                Label = "Total",
                labelValue = "0",
                SettlementValue = "0"
            };

            try
            {
                decimal totalValueColSum = 0;
                decimal totalInterestValueColSum = 0;
                decimal totalPenaltyValueColSum = 0;
                decimal totalTotalValueColSum = 0;
                decimal totalSettlementValueColSum = 0;

                if (lstCustomParameters.Count > 0)
                {
                    lstCustomParameters.ForEach(eachCustomParameterRecord =>
                    {
                        if (eachCustomParameterRecord.labelValue != "" && eachCustomParameterRecord.labelValue != null)
                        {
                            if (!eachCustomParameterRecord.labelValue.Contains("-"))
                                totalValueColSum += LitigationManagement.csvToNumber(eachCustomParameterRecord.labelValue);
                            else
                                totalValueColSum += 0;
                        }

                        if (eachCustomParameterRecord.Interest != "" && eachCustomParameterRecord.Interest != null)
                        {
                            if (!eachCustomParameterRecord.Interest.Contains("-"))
                                totalInterestValueColSum += LitigationManagement.csvToNumber(eachCustomParameterRecord.Interest);
                            else
                                totalInterestValueColSum += 0;
                        }

                        if (eachCustomParameterRecord.Penalty != "" && eachCustomParameterRecord.Penalty != null)
                        {
                            if (!eachCustomParameterRecord.Penalty.Contains("-"))
                                totalPenaltyValueColSum += LitigationManagement.csvToNumber(eachCustomParameterRecord.Penalty);
                            else
                                totalPenaltyValueColSum += 0;
                        }

                        if (eachCustomParameterRecord.Total != "" && eachCustomParameterRecord.Total != null)
                        {
                            if (!eachCustomParameterRecord.Total.Contains("-"))
                                totalTotalValueColSum += LitigationManagement.csvToNumber(eachCustomParameterRecord.Total);
                            else
                                totalTotalValueColSum += 0;
                        }

                        if (eachCustomParameterRecord.SettlementValue != "" && eachCustomParameterRecord.SettlementValue != null)
                        {
                            if (!eachCustomParameterRecord.SettlementValue.Contains("-"))
                                totalSettlementValueColSum += LitigationManagement.csvToNumber(eachCustomParameterRecord.SettlementValue);
                            else
                                totalSettlementValueColSum += 0;
                        }
                    });

                    totalRecord.labelValue = totalValueColSum.ToString("N2");
                    totalRecord.Interest = totalInterestValueColSum.ToString("N2");
                    totalRecord.Penalty = totalPenaltyValueColSum.ToString("N2");
                    totalRecord.Total = totalTotalValueColSum.ToString("N2");
                    totalRecord.SettlementValue = totalSettlementValueColSum.ToString("N2");
                }
                return totalRecord;
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return totalRecord;
            }
        }

        public static DataTable GetColumnTotal(DataTable dtCustomFields, bool IsTaxLitigation)
        {
            try
            {
                if (dtCustomFields.Rows.Count > 0)
                {
                    int rowIndex = 0;
                    string lblValue = string.Empty;
                    string lblfY = string.Empty;
                    decimal totalValueColSum = 0;
                    decimal totalInterestValueColSum = 0;
                    decimal totalPenaltyValueColSum = 0;
                    decimal totalTotalValueColSum = 0;
                    decimal totalSettlementValueColSum = 0;

                    foreach (DataRow eachCustomFieldRow in dtCustomFields.Rows)
                    {
                        lblValue = string.Empty;
                        lblfY = string.Empty;
                        if (eachCustomFieldRow["LableID"] != null && eachCustomFieldRow["labelValue"] != null)
                        {
                            if (eachCustomFieldRow["labelValue"].ToString() != "")
                            {
                                if (!eachCustomFieldRow["labelValue"].ToString().Contains("-"))
                                    totalValueColSum += LitigationManagement.csvToNumber(eachCustomFieldRow["labelValue"].ToString());
                                else
                                    totalValueColSum += 0;
                            }
                        }

                        if (IsTaxLitigation)
                        {
                            if (eachCustomFieldRow["Interest"] != null)
                            {
                                if (eachCustomFieldRow["Interest"].ToString() != "")
                                {
                                    if (!eachCustomFieldRow["Interest"].ToString().Contains("-"))
                                        totalInterestValueColSum += LitigationManagement.csvToNumber(eachCustomFieldRow["Interest"].ToString());
                                    else
                                        totalInterestValueColSum += 0;
                                }
                            }

                            if (eachCustomFieldRow["Penalty"] != null)
                            {
                                if (eachCustomFieldRow["Penalty"].ToString() != "")
                                {
                                    if (!eachCustomFieldRow["Penalty"].ToString().Contains("-"))
                                        totalPenaltyValueColSum += LitigationManagement.csvToNumber(eachCustomFieldRow["Penalty"].ToString());
                                    else
                                        totalPenaltyValueColSum += 0;
                                }
                            }

                            if (eachCustomFieldRow["Total"] != null)
                            {
                                if (eachCustomFieldRow["Total"].ToString() != "")
                                {
                                    if (!eachCustomFieldRow["Total"].ToString().Contains("-"))
                                        totalTotalValueColSum += LitigationManagement.csvToNumber(eachCustomFieldRow["Total"].ToString());
                                    else
                                        totalTotalValueColSum += 0;
                                }
                            }

                            if (eachCustomFieldRow["SettlementValue"] != null)
                            {
                                if (eachCustomFieldRow["SettlementValue"].ToString() != "")
                                {
                                    if (!eachCustomFieldRow["SettlementValue"].ToString().Contains("-"))
                                        totalSettlementValueColSum += LitigationManagement.csvToNumber(eachCustomFieldRow["SettlementValue"].ToString());
                                    else
                                        totalSettlementValueColSum += 0;
                                }
                            }
                        }

                        rowIndex++;
                    }

                    DataRow drowCustomField = null;

                    drowCustomField = dtCustomFields.NewRow();

                    drowCustomField["LableID"] = "0";
                    drowCustomField["Label"] = "Total";
                    drowCustomField["labelValue"] = totalValueColSum.ToString("N2");

                    if (IsTaxLitigation)
                    {
                        drowCustomField["Interest"] = totalInterestValueColSum.ToString("N2");
                        drowCustomField["Penalty"] = totalPenaltyValueColSum.ToString("N2");
                        drowCustomField["Total"] = totalTotalValueColSum.ToString("N2");
                        drowCustomField["SettlementValue"] = totalSettlementValueColSum.ToString("N2");
                        drowCustomField["ProvisionInBook"] = string.Empty;
                    }

                    dtCustomFields.Rows.Add(drowCustomField);
                }

                return dtCustomFields;
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return dtCustomFields;
            }
        }

        public static DataTable LoopAndDeleteBlankRows(DataTable dtCustomFields)
        {
            DataTable dtCustomFieldsEdited = new DataTable();
            //dtCustomFieldsEdited = dtCustomFields;

            if (dtCustomFields.Rows.Count > 0)
            {
                string lblValue = string.Empty;

                dtCustomFieldsEdited = dtCustomFields.AsEnumerable().Where(r => r.Field<string>("LableID") != "").CopyToDataTable();
            }

            return dtCustomFieldsEdited;
        }

        public static decimal csvToNumber(string csvNumber)
        {
            try
            {
                string result = string.Empty;
                decimal finalDecimalNumber = 0;
                if (!string.IsNullOrEmpty(csvNumber))
                {
                    var array = csvNumber.Split(',');

                    for (var i = 0; i < array.Length; i++)
                    {
                        result = result + array[i];
                    }

                    if (!string.IsNullOrEmpty(result))
                    {
                        finalDecimalNumber = Convert.ToDecimal(result);
                    }
                }
                return finalDecimalNumber;
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }

        }

        public static bool DeletePreviousCustomParameter(int noticeOrCase, long caseInstanceID, int caseTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var recordToDelete = (from NCCP in entities.tbl_NoticeCaseCustomParameter
                                          join CF in entities.tbl_CustomField
                                          on NCCP.LabelID equals CF.ID
                                          where NCCP.NoticeCaseInstanceID == caseInstanceID
                                          && CF.CaseNoticeCategory != caseTypeID
                                          && NCCP.NoticeCaseType == noticeOrCase
                                          select NCCP).ToList();

                    if (recordToDelete.Count > 0)
                    {
                        recordToDelete.ForEach(row => row.IsActive = false);
                        recordToDelete.ForEach(row => row.IsDeleted = true);
                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static List<SP_Litigation_NoticeCaseLinking_Result> GetLinkedNoticeCaseList(int customerID, long NoticeCaseInstanceID, int NoticeOrCaseType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_Litigation_NoticeCaseLinking(NoticeOrCaseType, NoticeCaseInstanceID, customerID)
                             select row).ToList();

                return query.ToList();
            }
        }

        public static bool CreateUpdateNoticeCaseLinking(tbl_NoticeCaseLinking objNewRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.tbl_NoticeCaseLinking
                                      where row.Type == objNewRecord.Type
                                      && row.NoticeOrCaseInstanceID == objNewRecord.NoticeOrCaseInstanceID
                                      && row.LinkedNoticeOrCaseInstanceID == objNewRecord.LinkedNoticeOrCaseInstanceID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.IsActive = true;
                        prevRecord.UpdatedBy = objNewRecord.UpdatedBy;
                        prevRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        objNewRecord.CreatedOn = DateTime.Now;
                        objNewRecord.UpdatedOn = DateTime.Now;
                        entities.tbl_NoticeCaseLinking.Add(objNewRecord);
                        saveSuccess = true;
                    }
                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteNoticeCaseLinking(long noticeCaseInstanceID, long linkedNoticeOrCaseInstanceID, int noticeCaseType, int loggedInUserID)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.tbl_NoticeCaseLinking
                                      where row.Type == noticeCaseType
                                      && row.NoticeOrCaseInstanceID == noticeCaseInstanceID
                                      && row.LinkedNoticeOrCaseInstanceID == linkedNoticeOrCaseInstanceID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.IsActive = false;
                        prevRecord.UpdatedBy = loggedInUserID;
                        prevRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static void InsertLog(Exception ex, string ClassName, string FunctionName)
        {
            try
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> lst_LogMessages = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = ClassName;
                msg.FunctionName = FunctionName;
                msg.CreatedOn = DateTime.Now;
                if (ex != null)
                {
                    msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                    msg.StackTrace = ex.StackTrace;
                }
                else
                {
                    msg.Message = ClassName;
                    msg.StackTrace = ClassName;
                }
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;

                if (ex != null)
                {
                    lst_LogMessages.Add(new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage() { LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ex.Message + "----\r\n" + ex.InnerException, StackTrace = ex.StackTrace, CreatedOn = DateTime.Now });
                }
                else
                {
                    lst_LogMessages.Add(new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage() { LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ClassName, StackTrace = ClassName, CreatedOn = DateTime.Now });
                }
                Business.ComplianceManagement.InsertLogToDatabase(lst_LogMessages);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static long CreateAuditLog(string noticeOrCase, long noticeCaseInstanceID, string TableName, string Action, int customerID, int Createdby, string Remark, bool IsVisibleToUser)
        {
            //noticeOrCase 1-Case 2-Notice
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    tbl_LitigationAuditLog TBL_LAL = new tbl_LitigationAuditLog()
                    {
                        Type = noticeOrCase,
                        NoticeCaseInstanceID = noticeCaseInstanceID,
                        TableName = TableName,
                        Action = Action,
                        CustomerId = customerID,
                        CreatedBy = Createdby,
                        CreatedOn = DateTime.Now,
                        IsActive = true,
                        Remark = Remark,
                        SentMail = false,
                        IsVisibleToUser = IsVisibleToUser,
                    };

                    entities.tbl_LitigationAuditLog.Add(TBL_LAL);
                    entities.SaveChanges();

                    return TBL_LAL.Id;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }

        public static void CreateSampleFile(LitigationPaymentForm form,long paymentid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                form.NoticeCasePaymentId = Convert.ToInt32(paymentid);
                form.CreatedOn = DateTime.Now;
                form.UpdatedOn = DateTime.Now;
                entities.LitigationPaymentForms.Add(form);
                entities.SaveChanges();
            }

        }

        public static List<SP_LitigationCaseNoticeAuditLog_Result> GetCaseNoticeAuditLog(int customerID, long noticeCaseInstanceID, string CaseOrNotice)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstAuditLogs = (from row in entities.SP_LitigationCaseNoticeAuditLog(customerID, noticeCaseInstanceID, CaseOrNotice)
                                    where row.IsVisibleToUser == true
                                    select row).OrderByDescending(entry => entry.CreatedOn).ToList();
                return lstAuditLogs.ToList();
            }
        }

        public static List<SP_Litigation_NoticeCasePayment_Result> GetCasePaymentDetails(int noticeInstanceID, string noticeOrCase)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.SP_Litigation_NoticeCasePayment(noticeInstanceID, noticeOrCase)
                                   select row).Distinct().ToList();

                return queryResult;
            }
        }

        public static List<tbl_TypeMaster> GetAllType(string Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstOrderType = (from row in entities.tbl_TypeMaster
                                    where row.TypeCode == Type
                                    && row.IsActive == true
                                    select row);
                return lstOrderType.ToList();
            }
        }


        public static List<LitigationAssignment> GetLitigationAssignmentData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.LitigationAssignments
                             where row.IsDeleted == false
                             select row).ToList();

                return query.ToList();
            }
        }

        public static void CreateLitigationAssignment(LitigationAssignment objlawyer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.LitigationAssignments.Add(objlawyer);
                entities.SaveChanges();
            }
        }

        public static LitigationAssignment GetLitiAssignmentID(LitigationAssignment objlawyer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.LitigationAssignments
                             where row.LegalEntity == objlawyer.LegalEntity && row.DepartmentID == objlawyer.DepartmentID
                             && row.Title == objlawyer.Title && row.Description == objlawyer.Description
                             select row).FirstOrDefault();
                return Query;
            }
        }

        public static void CreateLitiAssignmentMapping(LitiAssignment_Mapping objAssignment)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.LitiAssignment_Mapping.Add(objAssignment);
                entities.SaveChanges();
            }
        }

        public static void UpdateLitigationAssignment(LitigationAssignment objlawyer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                LitigationAssignment ObjQuery = (from row in entities.LitigationAssignments
                                                 where row.ID == objlawyer.ID
                                                 select row).FirstOrDefault();
                if (ObjQuery != null)
                {
                    ObjQuery.LegalEntity = objlawyer.LegalEntity;
                    ObjQuery.DepartmentID = objlawyer.DepartmentID;
                    ObjQuery.CityID = objlawyer.CityID;
                    ObjQuery.ActID = objlawyer.ActID;
                    ObjQuery.Title = objlawyer.Title;
                    ObjQuery.Description = objlawyer.Description;
                    ObjQuery.LiabilityAmount = objlawyer.LiabilityAmount;
                    ObjQuery.IsDeleted = false;
                    entities.SaveChanges();
                }
            }
        }

        public static void UpdateLitAssignmentMappingAllFalse(LitiAssignment_Mapping objMap)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjQuery = (from row in entities.LitiAssignment_Mapping
                                where row.Liti_AssignmentID == objMap.Liti_AssignmentID
                                select row).ToList();
                if (ObjQuery != null)
                {
                    foreach (var item in ObjQuery)
                    {
                        item.IsActive = false;
                        entities.SaveChanges();
                    }
                }
            }
        }

        public static void UpdateLitigationMappingData(LitiAssignment_Mapping objMap)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                LitiAssignment_Mapping ObjQuery = (from row in entities.LitiAssignment_Mapping
                                                   where row.Liti_AssignmentID == objMap.Liti_AssignmentID
                                                   && row.LawyerID == objMap.LawyerID
                                                   select row).FirstOrDefault();
                if (ObjQuery != null)
                {
                    ObjQuery.IsActive = true;
                    entities.SaveChanges();
                }
            }
        }

        public static LitigationAssignment GetLitigation_AssignmentByID(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.LitigationAssignments
                             where row.ID == ID
                             select row).FirstOrDefault();
                return Query;
            }
        }

        public static void DeteleLitigatinoAssignmentData(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                LitigationAssignment ObjQuery = (from row in entities.LitigationAssignments
                                                 where row.ID == ID
                                                 select row).FirstOrDefault();
                if (ObjQuery != null)
                {
                    ObjQuery.IsDeleted = true;
                    entities.SaveChanges();
                }
            }
        }

        public static List<LitiAssignment_Mapping> GetAssignMentMappingData(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objAssig = (from row in entities.LitiAssignment_Mapping
                                where row.Liti_AssignmentID == ID && row.IsActive == true
                                select row);
                return objAssig.ToList();
            }
        }

        public static bool CheckLitiAssignmentExist(LitiAssignment_Mapping objMap)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objAssig = (from row in entities.LitiAssignment_Mapping
                                where row.Liti_AssignmentID == objMap.Liti_AssignmentID && row.LawyerID == objMap.LawyerID
                                select row).FirstOrDefault();
                if (objAssig != null)
                {
                    return true;
                }
                else
                { return false; }
            }
        }
        public static List<int> GetAllAssigneEntityHierarchy(int customerID, int customerBranchID)
        {
            List<NameValueHierarchy> hierarchy = null;

            List<int> branchHierarchyList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var query = (from row in entities.CustomerBranches
                //             where row.IsDeleted == false
                //             && row.CustomerID == customerID
                //             && row.ID == customerBranchID
                //             select row);

                var masterListCustomerBranches = (from row in entities.CustomerBranches
                                                  join row1 in entities.LitigationEntitiesAssignments
                                                  on row.ID equals row1.BranchID
                                                  where row.IsDeleted == false
                                                  && row.CustomerID == customerID
                                                  //&& row.ID == customerBranchID
                                                  select row);

                var query = masterListCustomerBranches.Where(row => row.ID == customerBranchID);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    branchHierarchyList.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities, branchHierarchyList, masterListCustomerBranches);
                }
            }

            return branchHierarchyList;
        }
        public static List<int> GetAllHierarchy(int customerID, int customerBranchID)
        {
            List<NameValueHierarchy> hierarchy = null;

            List<int> branchHierarchyList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var query = (from row in entities.CustomerBranches
                //             where row.IsDeleted == false
                //             && row.CustomerID == customerID
                //             && row.ID == customerBranchID
                //             select row);

                var masterListCustomerBranches = (from row in entities.CustomerBranches
                                                  where row.IsDeleted == false
                                                  && row.CustomerID == customerID
                                                  //&& row.ID == customerBranchID
                                                  select row);

                var query = masterListCustomerBranches.Where(row => row.ID == customerBranchID);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    branchHierarchyList.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities, branchHierarchyList, masterListCustomerBranches);
                }
            }

            return branchHierarchyList;
        }     
        public static void LoadSubEntities(int customerID, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, List<int> branchHierarchyList, IQueryable<CustomerBranch> masterListCustomerBranches)
        {

            //IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
            //                                    where row.IsDeleted == false &&
            //                                    row.CustomerID == customerID
            //                                    && row.ParentID == nvp.ID
            //                                    select row);

            IQueryable<CustomerBranch> query = masterListCustomerBranches.Where(row => row.ParentID == nvp.ID);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                branchHierarchyList.Add(item.ID);
                LoadSubEntities(customerID, item, false, entities, branchHierarchyList, masterListCustomerBranches);
            }
        }

        public static bool CreateLitigationReminder(tbl_LitigationCustomReminder newRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tbl_LitigationCustomReminder.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistsLitigationReminder(tbl_LitigationCustomReminder objReminder, long reminderID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.tbl_LitigationCustomReminder
                             where row.ReminderTitle.ToLower().Equals(objReminder.ReminderTitle.Trim().ToLower())
                             && row.RemindOn == objReminder.RemindOn
                             && row.UserID == objReminder.UserID
                             && row.InstanceID == objReminder.InstanceID
                             select row);

                if (query != null)
                {
                    if (reminderID > 0)
                    {
                        query = query.Where(entry => entry.ID != reminderID);
                    }
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool DeleteLitigationReminderByID(int reminderID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var queryResult = (from row in entities.tbl_LitigationCustomReminder
                                       where row.ID == reminderID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateLitigationReminder(tbl_LitigationCustomReminder recordDetail)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var recordtoEdit = (from row in entities.tbl_LitigationCustomReminder
                                        where row.ID == recordDetail.ID
                                        select row).FirstOrDefault();

                    if (recordtoEdit != null)
                    {
                        recordtoEdit.Type = recordDetail.Type;
                        recordtoEdit.InstanceID = recordDetail.InstanceID;
                        recordtoEdit.ReminderTitle = recordDetail.ReminderTitle;
                        recordtoEdit.Description = recordDetail.Description;
                        recordtoEdit.Remark = recordDetail.Remark;
                        recordtoEdit.RemindOn = recordDetail.RemindOn;
                        recordtoEdit.Status = recordDetail.Status;

                        recordtoEdit.IsDeleted = false;
                        recordtoEdit.UpdatedBy = recordDetail.UpdatedBy;
                        recordtoEdit.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static tbl_LitigationCustomReminder GetLitigationReminderDetailByID(long reminderID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.tbl_LitigationCustomReminder
                             where row.ID == reminderID
                             select row).FirstOrDefault();
                return Query;
            }
        }

        public static List<View_LitigationReminderView> GetAssignedReminderList(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, int reminderType, long instanceID) /*, int taskStatus*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_LitigationReminderView
                             where row.CustomerID == customerID
                             //&& row.ReminderStatus == 0
                             select row).ToList(); //0-Pending 1-Sent

                if (instanceID != -1)
                    query = query.Where(entry => entry.InstanceID == instanceID).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => entry.UserID == loggedInUserID).ToList();
                //else
                //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                if (reminderType != 0)
                {
                    if (reminderType == 1) //Notice
                        query = query.Where(entry => entry.Type == "N").ToList();
                    else if (reminderType == 2) //Case
                        query = query.Where(entry => entry.Type == "C").ToList();
                    else if (reminderType == 3) //Task
                        query = query.Where(entry => entry.Type == "T").ToList();
                }

                if (query.Count > 0)
                    query = query.OrderBy(entry => entry.RemindOn)
                        .ThenBy(entry => entry.Type).ToList();

                return query;
            }
        }

        public static List<ListItem> ReArrange_FileTags(CheckBoxList lstBoxFileTags)
        {
            List<ListItem> lstSelectedTags = new List<ListItem>();
            List<ListItem> lstNonSelectedTags = new List<ListItem>();
            foreach (ListItem eachListItem in lstBoxFileTags.Items)
            {
                if (eachListItem.Selected)
                    lstSelectedTags.Add(eachListItem);
                else
                    lstNonSelectedTags.Add(eachListItem);
            }

            return lstSelectedTags.Union(lstNonSelectedTags).ToList();
        }

        public static List<ListItem> GetSelectedItems(CheckBoxList lst)
        {
            return lst.Items.OfType<ListItem>().Where(i => i.Selected).ToList();
        }

  public static List<TempDocument> getUploadedDocList(int ProductID, string productype)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AllDocument = (from row in entities.TempDocuments
                                            where row.IsActive == true
                                            && row.ProductID == ProductID
                                            && row.FileType == productype
                                   select row).ToList();

                
                
                return AllDocument;
            }           
        }

        public static List<TempDocument> getAllUploadedDocList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AllDocument = (from row in entities.TempDocuments
                                   where row.IsActive == true
                                   select row).ToList();

                return AllDocument;
            }
        }
        public static List<string> getfileList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var aa = (from row in entities.TempDocuments
                                   where row.IsActive == true
                          select row.FileNo).ToList();

                return aa;
            }
        }

        public static List<string> FileNoList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var FileNosList = ((from tom in entities.tbl_LegalCaseInstance
                                    select tom.CaseRefNo)
                                        .Union(from tom in entities.tbl_LegalNoticeInstance
                                               select tom.RefNo)
                                        .Union(from tom in entities.Cont_tbl_ContractInstance
                                               select tom.ContractNo)).ToList();

                return FileNosList;

            }
        }

        public static bool ExistsCaseHeringForAdvocateBill(string HearingRef,int caseinstanceid)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool savesuccess = false;
                string[] values = HearingRef.Split(',');
                foreach (var item in values)
                {
                    var query = (from row in entities.tbl_CaseAdvocateBill
                                 where row.HearingRef.ToUpper().Trim().Contains(item.ToUpper().Trim())
                                 && row.NoticeCaseInstanceID == caseinstanceid
                                 && row.IsActive == true
                                 select row.ID).Distinct().ToList();

                   
               
                if (query != null && query.Count() > 0)
                        savesuccess = true;
                    else
                        savesuccess =false;
                }
                return savesuccess;
            }
        }

        public static string GetHearingRef(int responceid,int caseinstanceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

              var Data= (from row in entities.tbl_LegalCaseResponse
                         where
                         row.IsActive == true
                         && row.ID == responceid
                         && row.CaseInstanceID == caseinstanceid

                         select row.RefID).FirstOrDefault();

                var lstRefNo = (from row in entities.tbl_CaseHearingRef
                            
                                where row.HearingRefNo != null
                                && row.ID== Data
                                && row.CaseNoticeInstanceID== caseinstanceid

                                select row.HearingRefNo).FirstOrDefault();

                return lstRefNo;


                
            }
        }


    }
}
