﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class RiskCategoryManagement
    {

        public static List<Sp_AuditeeTimeLineHisotry_Result> GetIMPTimeLineHistory(int ResultID, int AuditID, int PersonResponsible)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Sp_AuditeeTimeLineHisotry(ResultID, AuditID, PersonResponsible)
                             select row).Distinct().ToList();

                return query;
            }
        }
        public static string GetIMPLatestAuditteeStatus(AuditImplementationTransaction transaction)
        {
            string StatusID = string.Empty;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditImplementationTransactions
                             where row.ProcessId == transaction.ProcessId && row.FinancialYear == transaction.FinancialYear
                             && row.ForPeriod == transaction.ForPeriod && row.CustomerBranchId == transaction.CustomerBranchId
                             && row.ImplementationScheduleOnID == transaction.ImplementationScheduleOnID && row.VerticalID == transaction.VerticalID
                             && row.AuditID == transaction.AuditID && row.ResultID == transaction.ResultID
                             && row.AuditeeResponse != null
                             select row).OrderByDescending(x => x.ID).FirstOrDefault();

                if (query != null)
                {
                    StatusID = query.AuditeeResponse;
                }
                return StatusID;
            }
        }
        public static AuditImplementationTransaction GetLatestImplementationAuditteeResponse(int resultID, int verticalID, int auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditImplementationTransactions
                             where row.ResultID == resultID
                             && row.VerticalID == verticalID
                             && row.AuditID == auditID
                             && row.AuditeeResponse != null
                             select row).OrderByDescending(x => x.ID).FirstOrDefault();

                return query;
            }
        }
        public static object Reviewer2Users(long customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<int> a = new List<int>();
                a.Clear();
                a.Add(12);
                a.Add(10);
                a.Add(11);
                a.Add(2);
                var query = (from row in entities.mst_User
                             where row.CustomerID == customerid &&
                             row.IsDeleted == false && row.IsActive == true
                             && !a.Contains(row.RoleID) /*&& row.AuditorID==null*/
                             select row);

                query = query.Where(entry => entry.IsHead == false);

                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static bool CheckAllStepClosed(long? auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                AuditClosureDetail query = (from row in entities.AuditClosureDetails
                                            where row.ID == auditID && row.IsDeleted == false
                                            select row).FirstOrDefault();
                if (query != null)
                {
                    if (query.Total == query.Closed)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public static void CreateAuditClosureClosed(AuditClosureClose auditclosureclose)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.AuditClosureCloses.Add(auditclosureclose);
                entities.SaveChanges();
            }
        }
        public static InternalControlAuditResult GetInternalControlAuditResultbyID(int resultID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.ID == resultID
                             select row).FirstOrDefault();

                return query;
            }
        }
        public static int GetLatestStatusOfAudittee(InternalControlAuditResult ICR)
        {
            int StatusID = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.ProcessId == ICR.ProcessId && row.FinancialYear == ICR.FinancialYear
                             && row.ForPerid == ICR.ForPerid && row.CustomerBranchId == ICR.CustomerBranchId
                             && row.ATBDId == ICR.ATBDId && row.VerticalID == ICR.VerticalID
                             && row.AuditID == ICR.AuditID
                             select row).OrderByDescending(x => x.ID).FirstOrDefault();

                if (query != null)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(query.AStatusId)))
                    {
                        StatusID = (int)query.AStatusId;
                    }
                }
                return StatusID;
            }
        }
        public static string GetLatestStatusOfAuditteeResponse(InternalControlAuditResult ICR)
        {
            string StatusID = string.Empty;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.ProcessId == ICR.ProcessId && row.FinancialYear == ICR.FinancialYear
                             && row.ForPerid == ICR.ForPerid && row.CustomerBranchId == ICR.CustomerBranchId
                             && row.ATBDId == ICR.ATBDId && row.VerticalID == ICR.VerticalID
                             && row.AuditID == ICR.AuditID
                             select row).OrderByDescending(x => x.ID).FirstOrDefault();

                if (query != null)
                {
                    StatusID = query.AuditeeResponse;
                }
                return StatusID;
            }
        }
        public static bool CreateRiskActivityTBDMapping(RiskActivityToBeDoneMapping RATBDM)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;
                    entities.RiskActivityToBeDoneMappings.Add(RATBDM);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool CreateStepChecklistMapping(List<StepChecklistMapping> SCM)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;
                    SCM.ForEach(entry =>
                    {
                        if (entry.ID == 0)
                        {
                            if (!(RiskCategoryManagement.StepChecklistMappingExists(entry.ATBDID, entry.ChecklistDocument.Trim())))
                            {
                                entities.StepChecklistMappings.Add(entry);
                            }
                        }
                        else
                        {
                            if (!(RiskCategoryManagement.StepChecklistMappingExists(entry.ATBDID, entry.ChecklistDocument.Trim(), entry.ID)))
                            {
                                entities.StepChecklistMappings.Add(entry);
                            }
                            else
                            {
                                RiskCategoryManagement.StepChecklistMappingUpdate(entry.ATBDID, entry.ChecklistDocument.Trim());
                            }
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool StepChecklistMappingExists(long atbdid, string checklistdocument, int CDID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.StepChecklistMappings
                             where row.ChecklistDocument.Trim().ToUpper() == checklistdocument.Trim().ToUpper()
                             && row.ATBDID == atbdid
                             && row.ID == CDID
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }
        public static bool StepChecklistMappingExists(long atbdid, string checklistdocument)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.StepChecklistMappings
                             where row.ChecklistDocument.Trim().ToUpper() == checklistdocument.Trim().ToUpper()
                             && row.ATBDID == atbdid
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }


        public static bool StepChecklistMappingUpdate(long atbdid, string checklistdocument)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.StepChecklistMappings
                             where row.ChecklistDocument.Trim().ToUpper() == checklistdocument.Trim().ToUpper()
                             && row.ATBDID == atbdid
                             select row).FirstOrDefault();


                if (query != null)
                {
                    query.ChecklistDocument = checklistdocument;
                    query.IsActive = true;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool StepChecklistMappingUpdate(long atbdid, string checklistdocument, long COID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.StepChecklistMappings
                             where row.ChecklistDocument.Trim().ToUpper() == checklistdocument.Trim().ToUpper()
                             && row.ATBDID == atbdid
                             && row.ID == COID
                             select row).FirstOrDefault();


                if (query != null)
                {
                    query.ChecklistDocument = checklistdocument;
                    query.IsActive = true;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckAllAuditStepsAreClosed(long AuditID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var AccDetails = (from row in entities.AuditClosureDetails
                                      where row.ID == AuditID
                                      select row).FirstOrDefault();

                    if (AccDetails != null)
                    {
                        if (AccDetails.Total == AccDetails.Closed)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        //public static List<Sp_GetAllAssginmentControlForDelete_Result> GetAllDataofAssignment(int customerId, List<long> BranchID, int ProcessId, int SubprocessId, string fnancialYear, string forPeriod, List<string> controlList)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var Query = (from row in entities.Sp_GetAllAssginmentControlForDelete(fnancialYear, forPeriod, customerId)
        //                     select row).ToList();

        //        if (BranchID.Count > 0)
        //        {
        //            Query = Query.Where(entry => BranchID.Contains((long)entry.CustomerBranchId)).ToList();
        //        }
        //        if (ProcessId != -1)
        //        {
        //            Query = Query.Where(entry => entry.ProcessId == ProcessId).ToList();
        //        }
        //        if (SubprocessId != -1)
        //        {
        //            Query = Query.Where(entry => entry.SubProcessId == SubprocessId).ToList();
        //        }
        //        if (controlList.Count > 0)
        //        {
        //            Query = Query.Where(entry => controlList.Contains(entry.ControlNo)).ToList();
        //        }
        //        return Query;
        //    }
        //}
        public static List<Sp_GetAllAssginmentControlForDelete_Result> GetAllDataofAssignment(int customerId, long BranchID, int ProcessId, int SubprocessId, string fnancialYear, string forPeriod, List<string> controlList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Query = (from row in entities.Sp_GetAllAssginmentControlForDelete(fnancialYear, BranchID, forPeriod, customerId)
                             select row).ToList();

                if (ProcessId != -1)
                {
                    Query = Query.Where(entry => entry.ProcessId == ProcessId).ToList();
                }
                if (SubprocessId != -1)
                {
                    Query = Query.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                }
                if (controlList.Count > 0)
                {
                    Query = Query.Where(entry => controlList.Contains(entry.ControlNo)).ToList();
                }
                return Query;
            }
        }
        public static List<Sp_GetAllKickOfDeleteData_Result> GetAllDataofKickoff(int BranchID, string fnancialYear, string forPeriod, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Query = (from row in entities.Sp_GetAllKickOfDeleteData(fnancialYear, forPeriod, BranchID, VerticalID)
                             select row).ToList();

                //if (BranchID.Count > 0)
                //{
                //    Query = Query.Where(entry => BranchID.Contains((long)entry.BranchId)).ToList();
                //}

                //if (Verticals != -1)
                //{
                //    Query = Query.Where(entry => entry.VerticalId == Verticals).ToList();
                //}
                return Query;
            }
        }
        public static bool CheckICRAssignmentAssignmentExist(int AuditInstanceid, int riskcreationID, int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Result = (from row in entities.AuditTransactions
                              where row.AuditInstanceId == AuditInstanceid
                              && row.RiskCreationID == riskcreationID
                              && row.CustomerBranchId == Branchid
                              && row.StatusId != 1
                              select row).ToList();

                if (Result.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckKickOffAssignmentExist(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Result = (from row in entities.InternalAuditTransactions
                              where row.AuditID == AuditID
                              && row.StatusId != 1
                              select row).ToList();

                if (Result.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #region AuditChecklist Add after all step closed
        //RiskCategoryCreations
        public static bool riskCategorycreationExists(string activitydescription, int Customerbranchid, int processid, int subprocessid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskCategoryCreations
                             where row.ActivityDescription.Trim().ToUpper() == activitydescription
                             && row.CustomerBranchId == Customerbranchid
                             && row.ProcessId == processid
                             && row.SubProcessId == subprocessid
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static int GetRiskCategpryCreationID(string activitydescription, int Customerbranchid, int processid, int subprocessid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskCategoryCreations
                             where row.ActivityDescription.Trim().ToUpper() == activitydescription
                             && row.CustomerBranchId == Customerbranchid
                             && row.ProcessId == processid
                             && row.SubProcessId == subprocessid
                             select row.Id).FirstOrDefault();
                if (query != null)
                {
                    return (int)query;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static bool CreateriskCategorycreation(RiskCategoryCreation riskcategorycreation)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.RiskCategoryCreations.Add(riskcategorycreation);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        //RiskActivityTransactions
        public static bool riskactivitytransactionExists(string controldescription, int Customerbranchid, int Verticalid, int processid, int subprocessid, int riskcreationId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             where row.ControlDescription.Trim().ToUpper() == controldescription
                             && row.CustomerBranchId == Customerbranchid
                             && row.VerticalsId == Verticalid
                             && row.ProcessId == processid
                             && row.SubProcessId == subprocessid
                             && row.RiskCreationId == riskcreationId
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static int GetRiskActivityID(string controldescription, int Customerbranchid, int Verticalid, int processid, int subprocessid, int riskcreationId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             where row.ControlDescription.Trim().ToUpper() == controldescription
                             && row.CustomerBranchId == Customerbranchid
                             && row.VerticalsId == Verticalid
                             && row.ProcessId == processid
                             && row.SubProcessId == subprocessid
                             && row.RiskCreationId == riskcreationId
                             select row.Id).FirstOrDefault();
                if (query != null)
                {
                    return query;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static bool CreateRiskActivityTransactionRahul(RiskActivityTransaction riskactivitytransactions)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.RiskActivityTransactions.Add(riskactivitytransactions);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        //RiskActivityToBeDoneMapping
        public static bool CreateRiskActivityToBeDoneMapping(RiskActivityToBeDoneMapping riskactivitytobedonemapping)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.RiskActivityToBeDoneMappings.Add(riskactivitytobedonemapping);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static int GetATBDID(long RCCID, long RATID, long custBranchID, int verticalID, long Processid, long SubProcessID, string AuditStep)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var stateId = (from row in entities.RiskActivityToBeDoneMappings
                               where row.ActivityTobeDone.Trim().ToUpper() == AuditStep.Trim().ToUpper()
                              && row.RiskCategoryCreationId == RCCID
                              && row.RiskActivityId == RATID
                              && row.ProcessId == Processid
                              && row.SubProcessId == SubProcessID
                              && row.CustomerBranchID == custBranchID
                              && row.VerticalID == verticalID
                               select row.ID).FirstOrDefault();
                return Convert.ToInt32(stateId);
            }
        }
        public static bool RiskActivityToBeDoneMappingExists(long RCCID, long RATID, long custBranchID, int verticalID, long Processid, long SubProcessID, string AuditStep)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityToBeDoneMappings
                             where row.ActivityTobeDone.Trim().ToUpper() == AuditStep.Trim().ToUpper()
                             && row.RiskCategoryCreationId == RCCID
                             && row.RiskActivityId == RATID
                             && row.ProcessId == Processid
                             && row.SubProcessId == SubProcessID
                             && row.CustomerBranchID == custBranchID
                             && row.VerticalID == verticalID
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }

        public static bool RiskActivityToBeDoneMappingUpdate(long RCCID, long RATID, long custBranchID, int verticalID, long Processid, long SubProcessID, string AuditStep, string auditObj)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityToBeDoneMappings
                             where row.ActivityTobeDone.Trim().ToUpper() == AuditStep.Trim().ToUpper()
                             && row.RiskCategoryCreationId == RCCID
                             && row.RiskActivityId == RATID
                             && row.ProcessId == Processid
                             && row.SubProcessId == SubProcessID
                             && row.CustomerBranchID == custBranchID
                             && row.VerticalID == verticalID
                             select row).FirstOrDefault();


                if (query != null)
                {
                    query.AuditObjective = auditObj;
                    query.IsActive = true;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        //AuditStepMappings
        public static bool CreateAuditStepMapping(AuditStepMapping auditstepmapping)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.AuditStepMappings.Add(auditstepmapping);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool AuditStepMappingExists(AuditStepMapping ASM)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditStepMappings
                             where row.AuditID == ASM.AuditID
                             && row.ATBDId == ASM.ATBDId
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }

        //AuditClosureDetails
        public static bool UpdateAuditClosureDetailsCount(long AuditId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.AuditStepMappings
                             where row.AuditID == AuditId
                             select row).ToList().Count;

                if (query != 0)
                {
                    var AuditDetails = (from ACD in entities.AuditClosureDetails
                                        where ACD.ID == AuditId
                                        select ACD).FirstOrDefault();

                    if (AuditDetails != null)
                    {
                        AuditDetails.Total = query;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        //mst_Activity
        public static int GetActivityID(int processid, int subprocessid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Activity
                             where row.ProcessId == processid
                             && row.SubProcessId == subprocessid
                             select row.Id).FirstOrDefault();
                if (query != null)
                {
                    return (int)query;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static bool ActivityExists(int processid, int subprocessid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Activity
                             where row.ProcessId == processid
                             && row.SubProcessId == subprocessid
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool Createmst_Activity(mst_Activity mstActivity)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.mst_Activity.Add(mstActivity);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        //AuditStepMaster
        public static bool AuditStepMasterExists(string newAuditStep)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditStepMasters
                             where row.AuditStep.Trim().ToUpper() == newAuditStep.Trim().ToUpper()
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static long GetAuditStepMasterID(string newAuditStep)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditStepMasters
                             where row.AuditStep.Trim().ToUpper() == newAuditStep.Trim().ToUpper()
                             select row).FirstOrDefault();

                if (query != null)
                    return query.ID;
                else
                    return 0;
            }
        }
        public static bool CreateAuditStepMaster(AuditStepMaster newASM)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.AuditStepMasters.Add(newASM);

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        public static void CreateRiskActivityTransaction(RiskActivityTransaction riskactivitytransaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.RiskActivityTransactions.Add(riskactivitytransaction);
                entities.SaveChanges();
            }
        }
        #region Draft Closure Upload
        public static List<Tran_DraftClosureUpload> GetFileDataDraftClosureDocument(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.Tran_DraftClosureUpload
                                where row.AuditID == AuditID
                                select row).ToList();
                return fileData;
            }
        }
        public static bool CreateTran_DraftClosureUploadResult(Tran_DraftClosureUpload DraftClosureResult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    DraftClosureResult.EnType = "A";
                    entities.Tran_DraftClosureUpload.Add(DraftClosureResult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static int Tran_DraftClosureUploadResultCount(Tran_DraftClosureUpload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Tran_DraftClosureUpload
                             where row.AuditID == ICR.AuditID
                             select row).ToList();
                if (query != null)
                {
                    return query.Count;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static bool Tran_DraftClosureUploadResultExists(Tran_DraftClosureUpload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Tran_DraftClosureUpload
                             where row.AuditID == ICR.AuditID
                             && row.FileName == ICR.FileName
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region FeedbackFormUpload   
        public static int FeedBackFormFileCount(FeedbackFormUpload objFeedbackFormUpload)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.FeedbackFormUploads
                             where row.Name == objFeedbackFormUpload.Name &&
                             row.AuditId == objFeedbackFormUpload.AuditId
                             select row).ToList();

                if (query != null)
                {
                    return query.Count;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static bool CheckFeedbackFormExists(FeedbackFormUpload objFeedbackFormUpload)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.FeedbackFormUploads
                             where row.Name == objFeedbackFormUpload.Name &&
                             row.AuditId == objFeedbackFormUpload.AuditId
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool SaveFeedbackFormData(FeedbackFormUpload objFeedbackFormUpload)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    objFeedbackFormUpload.EnType = "A";
                    entities.FeedbackFormUploads.Add(objFeedbackFormUpload);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        public static List<string> GetAuditClosurePeriodZEE(long CustomerBranchID, long Verticalid, string FinancialYear, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from AC in entities.AuditClosures
                                     join MCB in entities.mst_CustomerBranch
                                     on AC.CustomerbranchId equals MCB.ID
                                     where AC.ACStatus == 1
                                     && MCB.ID == CustomerBranchID
                                     && AC.FinancialYear == FinancialYear
                                     && AC.VerticalID == Verticalid
                                     && AC.AuditID == AuditID
                                     && MCB.IsDeleted == false
                                     && MCB.Status == 1
                                     select AC.ForPeriod).Distinct().ToList();

                return MstRiskResult;
            }
        }

        public static bool ExistsAuditClosureCloseRecordZEE(int CustomerBranchId, int VerticalID, string FinancialYear, string ForPeriod, long AuditID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var AccDetails = (from row in entities.AuditClosureCloses
                                      join MCB in entities.mst_CustomerBranch
                                      on row.CustomerBranchId equals MCB.ID
                                      join row1 in entities.Tran_FinalDeliverableUpload
                                      on row.AuditId equals row1.AuditID
                                      where MCB.ID == CustomerBranchId
                                            //&& row.VerticalID == VerticalID
                                            //&& row.FinancialYear == FinancialYear
                                            //&& row.ForPeriod == ForPeriod
                                            && row.AuditId == AuditID
                                            && MCB.IsDeleted == false
                                            && MCB.Status == 1
                                      select row).FirstOrDefault();

                    if (AccDetails != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }




        public static Tran_AnnxetureUpload GetFileInternalFileData_Annxeture(int fileID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var file = (from row in entities.Tran_AnnxetureUpload
                            where row.Id == fileID
                            select row).FirstOrDefault();

                return file;
            }
        }

        public static bool BulkInternalReviewHistoryInsert(List<InternalReviewHistory> ListIRH)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int Count = 0;
                    ListIRH.ForEach(EachStep =>
                    {
                        Count++;
                        entities.InternalReviewHistories.Add(EachStep);
                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool BulkInternalAuditTransactionInsert(List<InternalAuditTransaction> ListIAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int Count = 0;
                    ListIAT.ForEach(EachStep =>
                    {
                        InternalAuditTransaction IATtoUpdate = (from row in entities.InternalAuditTransactions
                                                                where row.ATBDId == EachStep.ATBDId
                                                                  && row.CustomerBranchId == EachStep.CustomerBranchId
                                                                  && row.VerticalID == EachStep.VerticalID
                                                                  && row.UserID == EachStep.UserID
                                                                  && row.RoleID == EachStep.RoleID
                                                                  && row.ForPeriod == EachStep.ForPeriod
                                                                  && row.FinancialYear == EachStep.FinancialYear
                                                                  && row.AuditScheduleOnID == EachStep.AuditScheduleOnID
                                                                  && row.InternalAuditInstance == EachStep.InternalAuditInstance
                                                                  && row.StatusId == 2
                                                                  && row.AuditID == EachStep.AuditID
                                                                select row).FirstOrDefault();

                        if (IATtoUpdate == null)
                        {
                            Count++;
                            entities.InternalAuditTransactions.Add(EachStep);
                            if (Count >= 100)
                            {
                                entities.SaveChanges();
                                Count = 0;
                            }
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool BulkRATBDMUpdate(List<RiskActivityToBeDoneMapping> ListRATBD)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int Count = 0;
                    ListRATBD.ForEach(EachStep =>
                    {
                        RiskActivityToBeDoneMapping RATBDToUpdate = (from row in entities.RiskActivityToBeDoneMappings
                                                                     where row.ID == EachStep.ID
                                                                     select row).FirstOrDefault();

                        if (RATBDToUpdate != null)
                        {
                            RATBDToUpdate.AuditStepMasterID = EachStep.AuditStepMasterID;
                            RATBDToUpdate.AuditObjective = EachStep.AuditObjective;
                            RATBDToUpdate.ActivityTobeDone = EachStep.ActivityTobeDone;
                            RATBDToUpdate.Rating = EachStep.Rating;
                            Count++;
                        }

                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }



        public static bool ExistsRATBDMByID(int ATBDID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityToBeDoneMappings
                             where row.ID == ATBDID
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }

        public static void BulkRCCUpdate(List<RiskCategoryCreation> ListRCCData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int Count = 0;
                ListRCCData.ForEach(EachRCC =>
                {
                    RiskCategoryCreation RCCtoUpdate = (from row in entities.RiskCategoryCreations
                                                        where row.Id == EachRCC.Id
                                                        select row).FirstOrDefault();
                    if (RCCtoUpdate != null)
                    {
                        RCCtoUpdate.ControlNo = EachRCC.ControlNo;
                        RCCtoUpdate.ActivityDescription = EachRCC.ActivityDescription;
                        RCCtoUpdate.ControlObjective = EachRCC.ControlObjective;
                        Count++;
                    }

                    if (Count >= 100)
                    {
                        entities.SaveChanges();
                        Count = 0;
                    }
                });

                entities.SaveChanges();
            }
        }
        public static void BulkRATUpdate(List<RiskActivityTransaction> ListRATData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int Count = 0;
                ListRATData.ForEach(EachRAT =>
                {
                    RiskActivityTransaction RATtoUpdate = (from row in entities.RiskActivityTransactions
                                                           where row.Id == EachRAT.Id
                                                           && row.RiskCreationId == EachRAT.RiskCreationId
                                                           select row).FirstOrDefault();

                    if (RATtoUpdate != null)
                    {
                        RATtoUpdate.ControlDescription = EachRAT.ControlDescription;
                        RATtoUpdate.MControlDescription = EachRAT.MControlDescription;
                        RATtoUpdate.ControlOwner = EachRAT.ControlOwner;
                        RATtoUpdate.PersonResponsible = EachRAT.PersonResponsible;
                        RATtoUpdate.Key_Value = EachRAT.Key_Value;
                        RATtoUpdate.KC2 = EachRAT.KC2;
                        RATtoUpdate.TestStrategy = EachRAT.TestStrategy;
                        //RATtoUpdate.Frequency = EachRAT.Frequency;
                        RATtoUpdate.RiskRating = EachRAT.RiskRating;
                        RATtoUpdate.ControlRating = EachRAT.ControlRating;
                        RATtoUpdate.ProcessScore = EachRAT.ProcessScore;
                        Count++;
                    }

                    if (Count > 100)
                    {
                        entities.SaveChanges();
                        Count = 0;
                    }
                });

                entities.SaveChanges();
            }
        }

        public static bool ExistsRATByID(int RiskActivityID, long RCCID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             where row.Id == RiskActivityID
                             && row.RiskCreationId == RCCID
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }


        public static bool ExistsRCCByID(long RiskID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskCategoryCreations
                             where row.Id == RiskID
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }
        public static bool CheckAuditKickedOfforNot(int CustBranchID, int verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditAssignments
                             where row.CustomerBranchID == CustBranchID
                             && row.IsActive == true
                             && row.VerticalID == verticalid
                             select row).ToList();

                if (query.Count > 0)
                    return false;
                else
                    return true;
            }
        }
        public static void BulkInsert(DataTable dt, String ConnectionString, String TableName)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // make sure to enable triggers
                // more on triggers in next post
                SqlBulkCopy bulkCopy =
                    new SqlBulkCopy
                    (
                    connection,
                    SqlBulkCopyOptions.TableLock |
                    SqlBulkCopyOptions.FireTriggers |
                    SqlBulkCopyOptions.UseInternalTransaction,
                    null
                    );

                foreach (DataColumn col in dt.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                // set the destination table name
                bulkCopy.DestinationTableName = TableName;
                connection.Open();

                // write the data in the "dataTable"
                bulkCopy.WriteToServer(dt);
                connection.Close();
            }
            // reset
            //this.dataTable.Clear();
        }
        public static List<int> getAssignedVerticalBranchWise(long BranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var BranchVerticalList = (from row in entities.BranchVerticals
                                          where row.Branch == BranchID
                                          && row.IsActive == true
                                          select row.VerticalID).ToList();
                return BranchVerticalList;
            }
        }



        public static bool UpdateProcessOrderAuditClosure(AuditClosure ICR)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var query = (from row in entities.AuditClosures
                                 where row.ProcessId == ICR.ProcessId
                                 && row.FinancialYear == ICR.FinancialYear
                                 && row.ForPeriod == ICR.ForPeriod
                                 && row.CustomerbranchId == ICR.CustomerbranchId
                                 && row.VerticalID == ICR.VerticalID
                                 select row).ToList();

                    if (query.Count > 0)
                    {
                        query.ForEach(EachRecord =>
                        {
                            EachRecord.ProcessOrder = ICR.ProcessOrder;
                        });

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateCategeriAuditClosure(AuditClosure ICR)
        {

            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var query = (from row in entities.AuditClosures
                                 where row.ProcessId == ICR.ProcessId
                                 && row.FinancialYear == ICR.FinancialYear
                                 && row.ForPeriod == ICR.ForPeriod
                                 && row.CustomerbranchId == ICR.CustomerbranchId
                                 && row.VerticalID == ICR.VerticalID
                                 && row.ATBDId == ICR.ATBDId
                                 && row.AuditID == ICR.AuditID
                                 select row).ToList();

                    if (query.Count > 0)
                    {
                        query.ForEach(EachRecord =>
                        {
                            EachRecord.ObservationCategory = ICR.ObservationCategory;
                            EachRecord.ObservationSubCategory = ICR.ObservationSubCategory;
                            EachRecord.ISACPORMIS = ICR.ISACPORMIS;
                            EachRecord.UpdatedBy = ICR.UpdatedBy;
                            EachRecord.UpdatedOn = ICR.UpdatedOn;
                            EachRecord.UHPersonResponsible = ICR.UHPersonResponsible;
                            EachRecord.PRESIDENTPersonResponsible = ICR.PRESIDENTPersonResponsible;
                            EachRecord.UHComment = ICR.UHComment;
                            EachRecord.PRESIDENTComment = ICR.PRESIDENTComment;
                            EachRecord.BodyContent = ICR.BodyContent;
                        });

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool UpdateCategeriAuditClosure_ObservationList(AuditClosure ICR)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var query = (from row in entities.AuditClosures
                                 where row.ProcessId == ICR.ProcessId
                                 && row.FinancialYear == ICR.FinancialYear
                                 && row.ForPeriod == ICR.ForPeriod
                                 && row.CustomerbranchId == ICR.CustomerbranchId
                                 && row.VerticalID == ICR.VerticalID
                                 && row.ATBDId == ICR.ATBDId
                                 && row.AuditID == ICR.AuditID
                                 select row).ToList();

                    if (query.Count > 0)
                    {
                        query.ForEach(EachRecord =>
                        {
                            EachRecord.ObservationTitle = ICR.ObservationTitle;
                            EachRecord.Observation = ICR.Observation;
                            EachRecord.BriefObservation = ICR.BriefObservation;
                            EachRecord.ObjBackground = ICR.ObjBackground;
                            EachRecord.ObservationRating = ICR.ObservationRating;
                            EachRecord.BodyContent = ICR.BodyContent;
                            EachRecord.ISACPORMIS = ICR.ISACPORMIS;
                            EachRecord.ObservationCategory = ICR.ObservationCategory;
                            EachRecord.ObservationSubCategory = ICR.ObservationSubCategory;
                            EachRecord.FinancialImpact = ICR.FinancialImpact;
                            EachRecord.RootCost = ICR.RootCost;
                            EachRecord.Recomendation = ICR.Recomendation;
                            EachRecord.TimeLine = ICR.TimeLine;
                            EachRecord.PersonResponsible = EachRecord.PersonResponsible;
                            EachRecord.UpdatedBy = ICR.UpdatedBy;
                            EachRecord.UpdatedOn = ICR.UpdatedOn;
                            EachRecord.UHPersonResponsible = ICR.UHPersonResponsible;
                            EachRecord.PRESIDENTPersonResponsible = ICR.PRESIDENTPersonResponsible;
                            EachRecord.UHComment = ICR.UHComment;
                            EachRecord.PRESIDENTComment = ICR.PRESIDENTComment;
                            EachRecord.AnnexueTitle = ICR.AnnexueTitle;// added by sagar more on 10-01-2020
                        });

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool updateInternalAuditTxnObjCatergeriAM(InternalAuditTransaction ICR)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var query = (from row in entities.InternalAuditTransactions
                                 where row.ProcessId == ICR.ProcessId
                                 && row.SubProcessId == ICR.SubProcessId
                                 && row.FinancialYear == ICR.FinancialYear
                                 && row.ForPeriod == ICR.ForPeriod
                                 && row.CustomerBranchId == ICR.CustomerBranchId
                                 && row.VerticalID == ICR.VerticalID
                                 && row.ATBDId == ICR.ATBDId
                                 && row.StatusId == ICR.StatusId
                                 && row.AuditID == ICR.AuditID
                                 select row).ToList();

                    if (query.Count > 0)
                    {
                        query.ForEach(EachRecord =>
                        {
                            EachRecord.ObservationCategory = ICR.ObservationCategory;
                            EachRecord.ObservationSubCategory = ICR.ObservationSubCategory;
                            EachRecord.UpdatedOn = ICR.UpdatedOn;
                            EachRecord.UpdatedBy = ICR.UpdatedBy;
                        });

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool AuditClosureRecordExists(AuditClosure ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditClosures
                             where row.ProcessId == ICR.ProcessId
                             && row.FinancialYear == ICR.FinancialYear
                             && row.ForPeriod == ICR.ForPeriod
                             && row.CustomerbranchId == ICR.CustomerbranchId
                             && row.VerticalID == ICR.VerticalID
                             && row.ATBDId == ICR.ATBDId
                             && row.AuditID == ICR.AuditID
                             select row).ToList();

                if (query.Count > 0)
                    return true;
                else
                    return false;
            }
        }
        #region  Audit Manager
        public static bool InternalAuditTxnExistsAuditManager_ObaservationList(InternalAuditTransaction IAT)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditTransactions
                             where row.ProcessId == IAT.ProcessId && row.FinancialYear == IAT.FinancialYear
                             && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId
                              && row.RoleID == IAT.RoleID && row.ATBDId == IAT.ATBDId && row.VerticalID == IAT.VerticalID
                              && row.AuditID == IAT.AuditID && row.StatusId == 3 && row.SubProcessId == IAT.SubProcessId
                             select row).OrderByDescending(x => x.ID).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool InternalAuditTxnExistsAuditManager(InternalAuditTransaction IAT)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditTransactions
                             where row.ProcessId == IAT.ProcessId && row.FinancialYear == IAT.FinancialYear
                             && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId
                              && row.RoleID == IAT.RoleID && row.ATBDId == IAT.ATBDId && row.VerticalID == IAT.VerticalID
                              && row.StatusId == 5 && row.SubProcessId == IAT.SubProcessId
                              && row.AuditID == IAT.AuditID
                             select row).OrderByDescending(x => x.ID).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool UpdateInternalAuditTxnStatusReviewerAuditmanager(InternalAuditTransaction IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    InternalAuditTransaction RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                               where row.ProcessId == IAT.ProcessId && row.FinancialYear == IAT.FinancialYear
                                                                   && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId
                                                                   && row.RoleID == IAT.RoleID && row.ATBDId == IAT.ATBDId
                                                               && row.StatusId == 5 && row.VerticalID == IAT.VerticalID
                                                               && row.AuditID == IAT.AuditID
                                                               select row).OrderByDescending(x => x.ID).FirstOrDefault();

                    RecordtoUpdate.PersonResponsible = IAT.PersonResponsible;
                    RecordtoUpdate.ObservatioRating = IAT.ObservatioRating;
                    RecordtoUpdate.ObservationCategory = IAT.ObservationCategory;
                    RecordtoUpdate.ForPeriod = IAT.ForPeriod;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.Remarks = IAT.Remarks;
                    RecordtoUpdate.StatusId = IAT.StatusId;
                    RecordtoUpdate.StatusChangedOn = IAT.StatusChangedOn;
                    RecordtoUpdate.ReviewerRiskRating = IAT.ReviewerRiskRating;
                    RecordtoUpdate.ObservationSubCategory = IAT.ObservationSubCategory;// Added By Hardik Sapara For Update Observation SubCategory. 
                    RecordtoUpdate.VerticalID = IAT.VerticalID;
                    RecordtoUpdate.AuditID = IAT.AuditID;
                    RecordtoUpdate.UpdatedBy = IAT.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = IAT.UpdatedOn;
                    RecordtoUpdate.UHComment = IAT.UHComment;
                    RecordtoUpdate.UHPersonResponsible = IAT.UHPersonResponsible;
                    RecordtoUpdate.PRESIDENTComment = IAT.PRESIDENTComment;
                    RecordtoUpdate.PRESIDENTPersonResponsible = IAT.PRESIDENTPersonResponsible;
                    RecordtoUpdate.BodyContent = IAT.BodyContent;

                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }
        #endregion      
        public static bool UpdateInternalAuditTxnStatusReviewer(InternalAuditTransaction IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    InternalAuditTransaction RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                               where row.ProcessId == IAT.ProcessId && row.FinancialYear == IAT.FinancialYear
                                                               && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId
                                                               && row.UserID == IAT.UserID && row.RoleID == IAT.RoleID && row.ATBDId == IAT.ATBDId
                                                               && row.VerticalID == IAT.VerticalID && row.AuditID == IAT.AuditID && row.SubProcessId == IAT.SubProcessId
                                                               select row).FirstOrDefault();

                    RecordtoUpdate.PersonResponsible = IAT.PersonResponsible;
                    RecordtoUpdate.ObservatioRating = IAT.ObservatioRating;
                    RecordtoUpdate.ObservationCategory = IAT.ObservationCategory;
                    RecordtoUpdate.ForPeriod = IAT.ForPeriod;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.Remarks = IAT.Remarks;
                    RecordtoUpdate.StatusId = IAT.StatusId;
                    RecordtoUpdate.StatusChangedOn = IAT.StatusChangedOn;
                    RecordtoUpdate.ReviewerRiskRating = IAT.ReviewerRiskRating;
                    RecordtoUpdate.ObservationSubCategory = IAT.ObservationSubCategory;// Added By Hardik Sapara For Update Observation SubCategory. 
                    RecordtoUpdate.VerticalID = IAT.VerticalID;
                    RecordtoUpdate.AuditID = IAT.AuditID;
                    RecordtoUpdate.UpdatedBy = IAT.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = IAT.UpdatedOn;
                    RecordtoUpdate.UHComment = IAT.UHComment;
                    RecordtoUpdate.UHPersonResponsible = IAT.UHPersonResponsible;
                    RecordtoUpdate.PRESIDENTComment = IAT.PRESIDENTComment;
                    RecordtoUpdate.PRESIDENTPersonResponsible = IAT.PRESIDENTPersonResponsible;
                    RecordtoUpdate.BodyContent = IAT.BodyContent;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        public static bool UpdateInternalAuditTxnStatusReviewer_ObaservationList(InternalAuditTransaction IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    InternalAuditTransaction RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                               where row.ProcessId == IAT.ProcessId && row.FinancialYear == IAT.FinancialYear
                                                               && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId
                                                               && row.StatusId == 3 && row.ATBDId == IAT.ATBDId
                                                               && row.SubProcessId == IAT.SubProcessId
                                                               && row.VerticalID == IAT.VerticalID && row.AuditID == IAT.AuditID
                                                               select row).FirstOrDefault();

                    RecordtoUpdate.PersonResponsible = IAT.PersonResponsible;
                    RecordtoUpdate.ObservatioRating = IAT.ObservatioRating;
                    RecordtoUpdate.ObservationCategory = IAT.ObservationCategory;
                    RecordtoUpdate.ForPeriod = IAT.ForPeriod;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.Remarks = IAT.Remarks;
                    RecordtoUpdate.StatusId = IAT.StatusId;
                    RecordtoUpdate.StatusChangedOn = IAT.StatusChangedOn;
                    RecordtoUpdate.ReviewerRiskRating = IAT.ReviewerRiskRating;
                    RecordtoUpdate.ObservationSubCategory = IAT.ObservationSubCategory;// Added By Hardik Sapara For Update Observation SubCategory. 
                    RecordtoUpdate.VerticalID = IAT.VerticalID;
                    RecordtoUpdate.AuditID = IAT.AuditID;
                    RecordtoUpdate.UpdatedBy = IAT.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = IAT.UpdatedOn;
                    RecordtoUpdate.UHComment = IAT.UHComment;
                    RecordtoUpdate.UHPersonResponsible = IAT.UHPersonResponsible;
                    RecordtoUpdate.PRESIDENTComment = IAT.PRESIDENTComment;
                    RecordtoUpdate.PRESIDENTPersonResponsible = IAT.PRESIDENTPersonResponsible;
                    RecordtoUpdate.BodyContent = IAT.BodyContent;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        #region Audit Commite Presantion Upload
        public static int Tran_AuditCommitteeUploadResultCount(Tran_AuditCommitePresantionUpload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Tran_AuditCommitePresantionUpload
                             where row.AuditID == ICR.AuditID
                             select row).ToList();

                if (query != null)
                {
                    return query.Count;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static bool CreateTran_AuditCommitePresantionUploadResult(Tran_AuditCommitePresantionUpload mstriskresult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Tran_AuditCommitePresantionUpload.Add(mstriskresult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool Tran_AuditCommitePresantionUploadResultExists(Tran_AuditCommitePresantionUpload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Tran_AuditCommitePresantionUpload
                             where row.AuditID == ICR.AuditID
                             && row.FileName == ICR.FileName
                             select row).FirstOrDefault();


                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion
        #region Final Deleverable Upload
        public static bool CreateTran_FinalDeliverableUploadResult(Tran_FinalDeliverableUpload mstriskresult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    mstriskresult.EnType = "A";
                    entities.Tran_FinalDeliverableUpload.Add(mstriskresult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static int Tran_FinalDeliverableUploadResultCount(Tran_FinalDeliverableUpload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Tran_FinalDeliverableUpload
                             where row.AuditID == ICR.AuditID

                             select row).ToList();

                if (query != null)
                {
                    return query.Count;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static bool Tran_FinalDeliverableUploadResultExists(Tran_FinalDeliverableUpload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Tran_FinalDeliverableUpload
                             where row.AuditID == ICR.AuditID
                             && row.FileName == ICR.FileName
                             select row).FirstOrDefault();


                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion
        #region Annxture Upload
        public static bool CreateTran_AnnxetureUploadResult(Tran_AnnxetureUpload mstriskresult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    mstriskresult.EnType = "A";
                    entities.Tran_AnnxetureUpload.Add(mstriskresult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool Tran_AnnxetureUploadResultExists(Tran_AnnxetureUpload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Tran_AnnxetureUpload
                             where row.AuditID == ICR.AuditID
                             && row.FileName == ICR.FileName
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region Internal_AuditAssignment
        public static bool CreateInternal_AuditAssignmentlist(List<Internal_AuditAssignment> internalauditassignment)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    internalauditassignment.ForEach(entry =>
                    {
                        entities.Internal_AuditAssignment.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool EditAssignmentVisibleorNot(int CustBranchID, int VerticalID)
        {
            try
            {
                return RiskCategoryManagement.CheckAuditKickedOfforNot(CustBranchID, VerticalID);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static List<Internal_AuditAssignment> GetAllBInternal_AuditAssignmentList(int custID, int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.Internal_AuditAssignment
                               where (row.CustomerId == custID) && row.CustomerBranchid == Branchid && row.IsActive == false
                               select row);
                return objcust.ToList();

            }
        }
        public static bool UpdateInternal_AuditAssignment(int customerid, List<long> Branchid, string flag, int AuditorId)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ids = (from row in entities.Internal_AuditAssignment
                               where row.CustomerId == customerid && Branchid.Contains(row.CustomerBranchid)
                               select row).ToList();

                    ids.ForEach(entry =>
                    {
                        if (EditAssignmentVisibleorNot((int)entry.CustomerBranchid, (int)entry.VerticalID))
                        {
                            Internal_AuditAssignment InternalAuditAssignmnet = (from row in entities.Internal_AuditAssignment
                                                                                where row.Id == entry.Id
                                                                                select row).FirstOrDefault();
                            InternalAuditAssignmnet.IsActive = true;
                            //InternalAuditAssignmnet.AssignedTo = flag;
                            //InternalAuditAssignmnet.ExternalAuditorId = AuditorId;
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static void EditInternal_AuditAssignmentExist(int customerid, long Branchid, int verticalid, long Userid, string flag, int AuditorId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //var RiskToUpdate = (from row in InternalAuditAssignmentList
                //                    where row.CustomerBranchid == Branchid && row.VerticalID == verticalid
                //                    select row).FirstOrDefault();

                var RiskToUpdate = (from row in entities.Internal_AuditAssignment
                                    where row.CustomerId == customerid && row.CustomerBranchid == Branchid && row.VerticalID == verticalid
                                    select row).FirstOrDefault();
                if (RiskToUpdate != null)
                {
                    RiskToUpdate.IsActive = false;
                    RiskToUpdate.UpdatedBy = Userid;
                    RiskToUpdate.UpdatedOn = DateTime.Now;
                    RiskToUpdate.AssignedTo = flag;
                    RiskToUpdate.ExternalAuditorId = AuditorId;
                    entities.SaveChanges();
                }
            }
        }

        //public static bool Internal_AuditAssignmentExists(List<Internal_AuditAssignment> InternalAuditAssignmentList, long Branchid, int verticalid)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var query = (from row in InternalAuditAssignmentList
        //                     where row.CustomerBranchid == Branchid && row.VerticalID == verticalid
        //                     select row).FirstOrDefault();
        //        if (query != null)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //}
        public static bool Internal_AuditAssignmentExists(List<Internal_AuditAssignment> InternalAuditAssignmentList, long Branchid, int verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (InternalAuditAssignmentList.Count > 0)
                {
                    var query = (from row in InternalAuditAssignmentList
                                 where row.CustomerBranchid == Branchid && row.VerticalID == verticalid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
        }
        public static bool Internal_AuditAssignmentExists(Internal_AuditAssignment IAA)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Internal_AuditAssignment
                             where row.CustomerId == IAA.CustomerId
                             && row.CustomerBranchid == IAA.CustomerBranchid && row.IsActive == false
                             && row.VerticalID == IAA.VerticalID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region AuditClosureResult
        public static bool CreateAuditClosureResult(AuditClosure mstriskresult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.AuditClosures.Add(mstriskresult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateAuditClosure(AuditClosure AC)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var RecordtoUpdate = (from row in entities.AuditClosures
                                          where row.AuditID == AC.AuditID
                                          select row).ToList();
                    if (RecordtoUpdate.Count > 0)
                    {
                        RecordtoUpdate.ForEach(EachRecord =>
                        {
                            EachRecord.DraftClosureDate = AC.DraftClosureDate;
                        });

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Audit Closure Closed       
        public static bool ExistsAuditClosureCloseRecord(AuditClosureClose ACCRecord)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var AccDetails = (from row in entities.AuditClosureCloses
                                      where row.CustomerBranchId == ACCRecord.CustomerBranchId
                                      && row.VerticalID == ACCRecord.VerticalID
                                      && row.FinancialYear == ACCRecord.FinancialYear
                                      && row.ForPeriod == ACCRecord.ForPeriod
                                      && row.CustomerID == ACCRecord.CustomerID
                                      && row.AuditId == ACCRecord.AuditId
                                      select row).FirstOrDefault();

                    if (AccDetails != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool CreateAuditClosureClosedResult(List<AuditClosureClose> ACC)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;
                    ACC.ForEach(entry =>
                    {
                        if (!ExistsAuditClosureCloseRecord(entry))
                        {
                            entities.AuditClosureCloses.Add(entry);
                            entities.SaveChanges();
                        }
                    });

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool AuditClosureCloseExists(int Customerbranchid, int VerticalId, string FinancialYear, string ForPeriod, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditClosureCloses
                             where row.CustomerBranchId == Customerbranchid
                             && row.VerticalID == VerticalId
                             && row.FinancialYear == FinancialYear
                             && row.ForPeriod == ForPeriod
                             && row.AuditId == AuditID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region ImplementationAuditResult


        public static bool CreateImplementationAuditResult(ImplementationAuditResult mstriskresult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.ImplementationAuditResults.Add(mstriskresult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateAuditImplementationTransactionISACTIVE(int ResultID, long AuditID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var RecordtoUpdate = (from row in entities.AuditImplementationTransactions
                                          where row.ResultID == ResultID && row.AuditID == AuditID
                                          select row).ToList();
                    if (RecordtoUpdate.Count > 0)
                    {
                        RecordtoUpdate.ForEach(EachRecord =>
                        {
                            EachRecord.ISACTIVE = false;
                        });

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateAuditImplementationTransactionISACTIVE(int ResultID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var RecordtoUpdate = (from row in entities.AuditImplementationTransactions
                                          where row.ResultID == ResultID
                                          select row).ToList();
                    if (RecordtoUpdate.Count > 0)
                    {
                        RecordtoUpdate.ForEach(EachRecord =>
                        {
                            EachRecord.ISACTIVE = false;
                        });

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateImplementationAuditResultISACTIVE(int ResultID, long AuditID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var RecordtoUpdate = (from row in entities.ImplementationAuditResults
                                          where row.ResultID == ResultID &&
                                          row.AuditID == AuditID
                                          select row).ToList();
                    if (RecordtoUpdate.Count > 0)
                    {
                        RecordtoUpdate.ForEach(EachRecord =>
                        {
                            EachRecord.ISACTIVE = false;
                        });

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateImplementationAuditResultISACTIVE(int ResultID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var RecordtoUpdate = (from row in entities.ImplementationAuditResults
                                          where row.ResultID == ResultID
                                          select row).ToList();
                    if (RecordtoUpdate.Count > 0)
                    {
                        RecordtoUpdate.ForEach(EachRecord =>
                        {
                            EachRecord.ISACTIVE = false;
                        });

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Implementation Transaction

        public static bool ImplementationTransactionsExists(long ProcessId, string FinancialYear, string ForPeriod, long CustomerBranchId, int UserID, long RoleID, long ResultID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditImplementationTransactions
                             where row.ProcessId == ProcessId && row.FinancialYear == FinancialYear
                             && row.ForPeriod == ForPeriod && row.CustomerBranchId == CustomerBranchId && row.UserID == UserID
                             && row.RoleID == RoleID && row.ResultID == ResultID && row.StatusId == 6
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CreateAuditImplementationTransaction(AuditImplementationTransaction IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    IAT.Dated = DateTime.Now;
                    entities.AuditImplementationTransactions.Add(IAT);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region InternalAuditControlResult

        public static InternalControlAuditResult GetInternalControlAuditResultByInstanceID(int Atbtid, int CustomerBranchID, string FinancialYear, int AuditStatusId, int VerticalID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var AuditRiskcreationID = (from row in entities.InternalControlAuditResults
                                           where row.ATBDId == Atbtid && row.CustomerBranchId == CustomerBranchID
                                           && row.FinancialYear == FinancialYear && row.AStatusId == AuditStatusId
                                           && row.VerticalID == VerticalID
                                           && row.AuditID == AuditID
                                           select row).OrderByDescending(entry => entry.ID).FirstOrDefault();
                return AuditRiskcreationID;
            }
        }
        public static InternalControlAuditResult GetInternalControlAuditResultByInstanceID(int Atbtid, int CustomerBranchID, string FinancialYear, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var AuditRiskcreationID = (from row in entities.InternalControlAuditResults
                                           where row.ATBDId == Atbtid && row.CustomerBranchId == CustomerBranchID
                                           && row.FinancialYear == FinancialYear && row.AStatusId == 3
                                           && row.AuditID == AuditID
                                           select row).Distinct().FirstOrDefault();
                return AuditRiskcreationID;
            }
        }
        public static InternalControlAuditResult GetInternalControlAuditResultbyIMPlementationID(int ResultID, int VerticalID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from row in entities.InternalControlAuditResults
                                     where row.ID == ResultID && row.VerticalID == VerticalID
                                     && row.AuditID == AuditID
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        public static InternalControlAuditResult GetInternalControlAuditResultbyID(long AuditScheduleOnID, int ATBDId, int AuditStatusId, int Verticalid, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from row in entities.InternalControlAuditResults
                                     where row.AuditScheduleOnID == AuditScheduleOnID && row.ATBDId == ATBDId &&
                                     row.AStatusId == AuditStatusId && row.VerticalID == Verticalid
                                     && row.AuditID == AuditID
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        public static InternalControlAuditResult GetInternalControlAuditResultbyID(long AuditScheduleOnID, int ATBDId, int Verticalid, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.InternalControlAuditResults
                                     join row1 in entities.RiskActivityToBeDoneMappings
                                     on row.ATBDId equals row1.ID
                                     where row.AuditScheduleOnID == AuditScheduleOnID
                                     && row.ATBDId == ATBDId && row.VerticalID == Verticalid
                                     && row.AuditID == AuditID
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }

        public static Sp_GetInternalAuditResultByID_Result GetInternalControlAuditResultData(long AuditScheduleOnID, int ATBDId, int Verticalid, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.Sp_GetInternalAuditResultByID(Convert.ToInt32(AuditScheduleOnID), ATBDId, Verticalid, AuditID)
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        public static List<InternalControlAuditResult> GetObservationRatingList(int customerBranchId, int verticalID, string fnancialYear, string forPeriod, int processID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RatingList = (from row in entities.InternalControlAuditResults
                                  where row.CustomerBranchId == customerBranchId
                                  && row.ProcessId == processID && row.FinancialYear == fnancialYear
                                  && row.VerticalID == verticalID && row.ForPerid == forPeriod
                                  && row.AStatusId == 3
                                  && row.Observation != null
                                  && row.Observation != ""
                                  select row).ToList();
                return RatingList;
            };
        }
        public static bool InternalControlResultExistsCheckLastestNullWithStatus(InternalControlAuditResult ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.ProcessId == ICR.ProcessId && row.FinancialYear == ICR.FinancialYear
                             && row.ForPerid == ICR.ForPerid && row.CustomerBranchId == ICR.CustomerBranchId && row.UserID == ICR.UserID
                             && row.RoleID == ICR.RoleID && row.ATBDId == ICR.ATBDId && row.VerticalID == ICR.VerticalID
                             && row.AStatusId == null && row.AuditID == ICR.AuditID
                             select row).OrderByDescending(x => x.ID).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool InternalControlResultExistsWithStatus(InternalControlAuditResult ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.ProcessId == ICR.ProcessId && row.FinancialYear == ICR.FinancialYear
                             && row.ForPerid == ICR.ForPerid && row.CustomerBranchId == ICR.CustomerBranchId && row.UserID == ICR.UserID
                             && row.RoleID == ICR.RoleID && row.ATBDId == ICR.ATBDId && row.VerticalID == ICR.VerticalID
                             && row.AStatusId == ICR.AStatusId && row.AuditID == ICR.AuditID
                             select row).OrderByDescending(x => x.ID).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int InternalControlResultClosedCount(InternalControlAuditResult ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.ProcessId == ICR.ProcessId && row.FinancialYear == ICR.FinancialYear
                             && row.ForPerid == ICR.ForPerid && row.CustomerBranchId == ICR.CustomerBranchId && row.UserID == ICR.UserID
                             && row.RoleID == ICR.RoleID && row.ATBDId == ICR.ATBDId && row.VerticalID == ICR.VerticalID
                             && row.AStatusId == ICR.AStatusId && row.AuditID == ICR.AuditID
                             select row).OrderByDescending(x => x.ID).ToList();
                return query.Count;
            }
        }
        public static bool InternalControlResultExists(InternalControlAuditResult ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.ProcessId == ICR.ProcessId && row.FinancialYear == ICR.FinancialYear
                             && row.ForPerid == ICR.ForPerid && row.CustomerBranchId == ICR.CustomerBranchId && row.UserID == ICR.UserID
                             && row.RoleID == ICR.RoleID && row.ATBDId == ICR.ATBDId && row.VerticalID == ICR.VerticalID
                             && row.AuditID == ICR.AuditID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool InternalControlResultExists_ObservationList(InternalControlAuditResult ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.ProcessId == ICR.ProcessId && row.FinancialYear == ICR.FinancialYear
                             && row.ForPerid == ICR.ForPerid && row.CustomerBranchId == ICR.CustomerBranchId
                             && row.ATBDId == ICR.ATBDId
                             && row.VerticalID == ICR.VerticalID
                             && row.AuditID == ICR.AuditID
                             && row.AStatusId == 3
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool BulkInternalControlAuditResultInsert(List<InternalControlAuditResult> ListICAR)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int Count = 0;
                    ListICAR.ForEach(EachStep =>
                    {
                        InternalControlAuditResult ICARoUpdate = (from row in entities.InternalControlAuditResults
                                                                  where row.ATBDId == EachStep.ATBDId
                                                                  && row.CustomerBranchId == EachStep.CustomerBranchId
                                                                  && row.VerticalID == EachStep.VerticalID
                                                                  && row.UserID == EachStep.UserID
                                                                  && row.RoleID == EachStep.RoleID
                                                                  && row.ForPerid == EachStep.ForPerid
                                                                  && row.FinancialYear == EachStep.FinancialYear
                                                                  && row.AuditScheduleOnID == EachStep.AuditScheduleOnID
                                                                  && row.InternalAuditInstance == EachStep.InternalAuditInstance
                                                                  && row.AStatusId == 2
                                                                  && row.AuditID == EachStep.AuditID
                                                                  select row).FirstOrDefault();
                        if (ICARoUpdate == null)
                        {
                            Count++;
                            entities.InternalControlAuditResults.Add(EachStep);
                            if (Count >= 100)
                            {
                                entities.SaveChanges();
                                Count = 0;
                            }
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateInternalControlResult(InternalControlAuditResult ICR)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    InternalControlAuditResult RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                 where row.ProcessId == ICR.ProcessId && row.FinancialYear == ICR.FinancialYear
                                                                 && row.ForPerid == ICR.ForPerid && row.CustomerBranchId == ICR.CustomerBranchId
                                                                 && row.UserID == ICR.UserID && row.RoleID == ICR.RoleID && row.ATBDId == ICR.ATBDId
                                                                 && row.VerticalID == ICR.VerticalID && row.AuditID == ICR.AuditID
                                                                 select row).OrderByDescending(x => x.ID).FirstOrDefault();

                    RecordtoUpdate.AuditObjective = ICR.AuditObjective;
                    RecordtoUpdate.AuditSteps = ICR.AuditSteps;
                    RecordtoUpdate.AnalysisToBePerofrmed = ICR.AnalysisToBePerofrmed;
                    RecordtoUpdate.ProcessWalkthrough = ICR.ProcessWalkthrough;
                    RecordtoUpdate.ActivityToBeDone = ICR.ActivityToBeDone;
                    RecordtoUpdate.Population = ICR.Population;
                    RecordtoUpdate.Sample = ICR.Sample;
                    RecordtoUpdate.ObservationNumber = ICR.ObservationNumber;
                    RecordtoUpdate.ObservationTitle = ICR.ObservationTitle;
                    RecordtoUpdate.Observation = ICR.Observation;
                    RecordtoUpdate.Risk = ICR.Risk;
                    RecordtoUpdate.RootCost = ICR.RootCost;
                    RecordtoUpdate.FinancialImpact = ICR.FinancialImpact;
                    RecordtoUpdate.Recomendation = ICR.Recomendation;
                    RecordtoUpdate.ManagementResponse = ICR.ManagementResponse;
                    RecordtoUpdate.TimeLine = ICR.TimeLine;
                    RecordtoUpdate.PersonResponsible = ICR.PersonResponsible;
                    RecordtoUpdate.Owner = ICR.Owner;
                    RecordtoUpdate.ObservationRating = ICR.ObservationRating;
                    RecordtoUpdate.ObservationCategory = ICR.ObservationCategory;
                    RecordtoUpdate.FixRemark = ICR.FixRemark;
                    RecordtoUpdate.AStatusId = ICR.AStatusId;
                    RecordtoUpdate.ObservationSubCategory = ICR.ObservationSubCategory; // Added By Hardik Sapara For Update Observation SubCategory.
                    RecordtoUpdate.VerticalID = ICR.VerticalID;
                    RecordtoUpdate.AuditScores = ICR.AuditScores;
                    RecordtoUpdate.ISACPORMIS = ICR.ISACPORMIS;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.AuditID = ICR.AuditID;
                    RecordtoUpdate.UpdatedBy = ICR.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = ICR.UpdatedOn;
                    RecordtoUpdate.UHComment = ICR.UHComment;
                    RecordtoUpdate.UHPersonResponsible = ICR.UHPersonResponsible;
                    RecordtoUpdate.PRESIDENTComment = ICR.PRESIDENTComment;
                    RecordtoUpdate.PRESIDENTPersonResponsible = ICR.PRESIDENTPersonResponsible;
                    RecordtoUpdate.BodyContent = ICR.BodyContent;
                    RecordtoUpdate.BriefObservation = ICR.BriefObservation;
                    RecordtoUpdate.DefeciencyType = ICR.DefeciencyType;
                    RecordtoUpdate.ObjBackground = ICR.ObjBackground;
                    RecordtoUpdate.AnnexueTitle = ICR.AnnexueTitle;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        public static bool UpdateInternalControlResult_ObaservationList(InternalControlAuditResult ICR)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    InternalControlAuditResult RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                 where row.ProcessId == ICR.ProcessId && row.FinancialYear == ICR.FinancialYear
                                                                 && row.ForPerid == ICR.ForPerid && row.CustomerBranchId == ICR.CustomerBranchId
                                                                 && row.ATBDId == ICR.ATBDId && row.AStatusId == 3
                                                                 && row.VerticalID == ICR.VerticalID && row.AuditID == ICR.AuditID
                                                                 select row).OrderByDescending(x => x.ID).FirstOrDefault();

                    RecordtoUpdate.AuditObjective = ICR.AuditObjective;
                    RecordtoUpdate.AuditSteps = ICR.AuditSteps;
                    RecordtoUpdate.AnalysisToBePerofrmed = ICR.AnalysisToBePerofrmed;
                    RecordtoUpdate.ProcessWalkthrough = ICR.ProcessWalkthrough;
                    RecordtoUpdate.ActivityToBeDone = ICR.ActivityToBeDone;
                    RecordtoUpdate.Population = ICR.Population;
                    RecordtoUpdate.Sample = ICR.Sample;
                    RecordtoUpdate.ObservationNumber = ICR.ObservationNumber;
                    RecordtoUpdate.ObservationTitle = ICR.ObservationTitle;
                    RecordtoUpdate.Observation = ICR.Observation;
                    RecordtoUpdate.Risk = ICR.Risk;
                    RecordtoUpdate.RootCost = ICR.RootCost;
                    RecordtoUpdate.FinancialImpact = ICR.FinancialImpact;
                    RecordtoUpdate.Recomendation = ICR.Recomendation;
                    RecordtoUpdate.ManagementResponse = ICR.ManagementResponse;
                    RecordtoUpdate.TimeLine = ICR.TimeLine;
                    RecordtoUpdate.PersonResponsible = ICR.PersonResponsible;
                    RecordtoUpdate.Owner = ICR.Owner;
                    RecordtoUpdate.ObservationRating = ICR.ObservationRating;
                    RecordtoUpdate.ObservationCategory = ICR.ObservationCategory;
                    RecordtoUpdate.FixRemark = ICR.FixRemark;
                    RecordtoUpdate.AStatusId = ICR.AStatusId;
                    RecordtoUpdate.ObservationSubCategory = ICR.ObservationSubCategory; // Added By Hardik Sapara For Update Observation SubCategory.
                    RecordtoUpdate.VerticalID = ICR.VerticalID;
                    RecordtoUpdate.AuditScores = ICR.AuditScores;
                    RecordtoUpdate.ISACPORMIS = ICR.ISACPORMIS;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.AuditID = ICR.AuditID;
                    RecordtoUpdate.UpdatedBy = ICR.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = ICR.UpdatedOn;
                    RecordtoUpdate.UHComment = ICR.UHComment;
                    RecordtoUpdate.UHPersonResponsible = ICR.UHPersonResponsible;
                    RecordtoUpdate.PRESIDENTComment = ICR.PRESIDENTComment;
                    RecordtoUpdate.PRESIDENTPersonResponsible = ICR.PRESIDENTPersonResponsible;
                    RecordtoUpdate.BodyContent = ICR.BodyContent;
                    RecordtoUpdate.BriefObservation = ICR.BriefObservation;
                    RecordtoUpdate.DefeciencyType = ICR.DefeciencyType;
                    RecordtoUpdate.ObjBackground = ICR.ObjBackground;
                    RecordtoUpdate.AnnexueTitle = ICR.AnnexueTitle;// added by sagar more on 10-01-2020
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateInternalAuditTransactionPersonresponsible(InternalControlAuditResult IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<InternalAuditTransaction> RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                                     where row.FinancialYear == IAT.FinancialYear
                                                                     && row.ForPeriod == IAT.ForPerid
                                                                     && row.CustomerBranchId == IAT.CustomerBranchId
                                                                     && row.ATBDId == IAT.ATBDId
                                                                     && row.VerticalID == IAT.VerticalID
                                                                     && row.AuditID == IAT.AuditID
                                                                     select row).ToList();

                    if (RecordtoUpdate != null)
                    {
                        RecordtoUpdate.ForEach(entry =>
                        {
                            entry.PersonResponsible = null;
                        });
                    }
                    entities.SaveChanges();
                };
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static void UpdateObservationDataDelete(InternalControlAuditResult objaudit, string OptDelete)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<InternalControlAuditResult> objcust = (from row in entities.InternalControlAuditResults
                                                            where row.FinancialYear == objaudit.FinancialYear
                                                            && row.ForPerid == objaudit.ForPerid
                                                            && row.CustomerBranchId == objaudit.CustomerBranchId
                                                            && row.ATBDId == objaudit.ATBDId
                                                            && row.VerticalID == objaudit.VerticalID
                                                            && row.AuditID == objaudit.AuditID
                                                            select row).ToList();


                if (OptDelete == null)
                {
                    objcust.ForEach(entry =>
                    {
                        entry.ATBDId = objaudit.ATBDId;
                        entry.Observation = objaudit.Observation;
                        entry.ManagementResponse = objaudit.ManagementResponse;
                        entry.TimeLine = objaudit.TimeLine;
                        entry.PersonResponsible = objaudit.PersonResponsible;
                        entry.Dated = DateTime.Now;
                    });
                }
                else
                {
                    objcust.ForEach(entry =>
                    {
                        entry.ATBDId = objaudit.ATBDId;
                        entry.Observation = null;
                        entry.ManagementResponse = objaudit.ManagementResponse;
                        entry.TimeLine = objaudit.TimeLine;
                        entry.PersonResponsible = null;
                        entry.Dated = DateTime.Now;
                    });
                }

                entities.SaveChanges();
            }
        }
        public static void UpdateObservationData(InternalControlAuditResult objaudit)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<InternalControlAuditResult> objcust = (from row in entities.InternalControlAuditResults
                                                            where row.FinancialYear == objaudit.FinancialYear
                                                            && row.ForPerid == objaudit.ForPerid
                                                            && row.CustomerBranchId == objaudit.CustomerBranchId
                                                            && row.ATBDId == objaudit.ATBDId
                                                            && row.VerticalID == objaudit.VerticalID
                                                            && row.AuditID == objaudit.AuditID
                                                            select row).OrderByDescending(x => x.ID).ToList();

                if (objcust != null)
                {
                    objcust.ForEach(entry =>
                    {
                        entry.ATBDId = objaudit.ATBDId;
                        entry.Observation = objaudit.Observation;
                        entry.ManagementResponse = objaudit.ManagementResponse;
                        entry.TimeLine = objaudit.TimeLine;
                        entry.PersonResponsible = objaudit.PersonResponsible;
                        entry.Dated = DateTime.Now;
                        entry.UpdatedBy = objaudit.UpdatedBy;
                        entry.UpdatedOn = objaudit.UpdatedOn;
                    });
                }
                entities.SaveChanges();
            }
        }
        public static bool CreateInternalControlResult(InternalControlAuditResult mstriskresult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    mstriskresult.Dated = DateTime.Now;
                    entities.InternalControlAuditResults.Add(mstriskresult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool UpdateInternalControlResult(InternalControlAuditResult ICR, int AuditStatusId)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    InternalControlAuditResult RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                 where row.ProcessId == ICR.ProcessId && row.FinancialYear == ICR.FinancialYear
                                                                 && row.ForPerid == ICR.ForPerid && row.CustomerBranchId == ICR.CustomerBranchId
                                                                 && row.AStatusId == AuditStatusId && row.RoleID == ICR.RoleID && row.ATBDId == ICR.ATBDId
                                                                 && row.VerticalID == ICR.VerticalID
                                                                 && row.AuditID == ICR.AuditID
                                                                 select row).OrderByDescending(x => x.ID).FirstOrDefault();

                    RecordtoUpdate.AuditObjective = ICR.AuditObjective;
                    RecordtoUpdate.AuditSteps = ICR.AuditSteps;
                    RecordtoUpdate.AnalysisToBePerofrmed = ICR.AnalysisToBePerofrmed;
                    RecordtoUpdate.ProcessWalkthrough = ICR.ProcessWalkthrough;
                    RecordtoUpdate.ActivityToBeDone = ICR.ActivityToBeDone;
                    RecordtoUpdate.Population = ICR.Population;
                    RecordtoUpdate.Sample = ICR.Sample;
                    RecordtoUpdate.ObservationNumber = ICR.ObservationNumber;
                    RecordtoUpdate.ObservationTitle = ICR.ObservationTitle;
                    RecordtoUpdate.Observation = ICR.Observation;
                    RecordtoUpdate.Risk = ICR.Risk;
                    RecordtoUpdate.RootCost = ICR.RootCost;
                    RecordtoUpdate.FinancialImpact = ICR.FinancialImpact;
                    RecordtoUpdate.Recomendation = ICR.Recomendation;
                    RecordtoUpdate.ManagementResponse = ICR.ManagementResponse;
                    RecordtoUpdate.TimeLine = ICR.TimeLine;
                    RecordtoUpdate.PersonResponsible = ICR.PersonResponsible;
                    RecordtoUpdate.ObservationRating = ICR.ObservationRating;
                    RecordtoUpdate.ObservationCategory = ICR.ObservationCategory;
                    RecordtoUpdate.FixRemark = ICR.FixRemark;
                    RecordtoUpdate.AStatusId = ICR.AStatusId;
                    RecordtoUpdate.ObservationSubCategory = ICR.ObservationSubCategory; // Added By Hardik Sapara For Update Observation SubCategory.
                    RecordtoUpdate.VerticalID = ICR.VerticalID; // Added By Hardik Sapara For Update Observation SubCategory.
                    RecordtoUpdate.AuditScores = ICR.AuditScores;
                    RecordtoUpdate.ISACPORMIS = ICR.ISACPORMIS;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.UpdatedOn = ICR.UpdatedOn;
                    RecordtoUpdate.UpdatedBy = ICR.UpdatedBy;

                    RecordtoUpdate.UHComment = ICR.UHComment;
                    RecordtoUpdate.UHPersonResponsible = ICR.UHPersonResponsible;
                    RecordtoUpdate.PRESIDENTComment = ICR.PRESIDENTComment;
                    RecordtoUpdate.PRESIDENTPersonResponsible = ICR.PRESIDENTPersonResponsible;
                    RecordtoUpdate.BodyContent = ICR.BodyContent;
                    RecordtoUpdate.DefeciencyType = ICR.DefeciencyType;
                    RecordtoUpdate.BriefObservation = ICR.BriefObservation;
                    RecordtoUpdate.ObjBackground = ICR.ObjBackground;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }


        #endregion
        public static bool AuditClosureDetailsExists(long CustomerID, long BranchId, long VerticalId, string ForMonth, string FinancialYear, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditClosureDetails
                             where row.CustomerID == CustomerID && row.BranchId == BranchId
                             && row.VerticalId == VerticalId && row.ForMonth == ForMonth && row.FinancialYear == FinancialYear
                             && row.ID == AuditID
                             select row).FirstOrDefault();
                if (query != null)
                {
                    long closedcount = query.Closed ?? 0;
                    if ((query.Total >= (closedcount + 1)))
                    {
                        query.Closed = closedcount + 1;
                        entities.SaveChanges();
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #region InternalAuditTxn
        public static bool CreateInternalAuditTxn(InternalAuditTransaction IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    IAT.Dated = DateTime.Now;
                    entities.InternalAuditTransactions.Add(IAT);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool InternalAuditTxnExistsNew(InternalAuditTransaction IAT)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditTransactions
                             where row.ProcessId == IAT.ProcessId
                                    && row.FinancialYear == IAT.FinancialYear
                                    && row.ForPeriod == IAT.ForPeriod
                                    && row.CustomerBranchId == IAT.CustomerBranchId
                                    && row.UserID == IAT.UserID
                                    && row.RoleID == IAT.RoleID
                                    && row.ATBDId == IAT.ATBDId
                                    && row.VerticalID == IAT.VerticalID
                                    && row.AuditID == IAT.AuditID
                                    && row.StatusId == IAT.StatusId
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool InternalAuditTxnExists(InternalAuditTransaction IAT)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditTransactions
                             where row.ProcessId == IAT.ProcessId
                             && row.SubProcessId == IAT.SubProcessId
                                    && row.FinancialYear == IAT.FinancialYear
                                    && row.ForPeriod == IAT.ForPeriod
                                    && row.CustomerBranchId == IAT.CustomerBranchId
                                    && row.UserID == IAT.UserID
                                    && row.RoleID == IAT.RoleID
                                    && row.ATBDId == IAT.ATBDId
                                    && row.VerticalID == IAT.VerticalID
                                    && row.AuditID == IAT.AuditID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool InternalAuditTransactionsExists(long ProcessId, string FinancialYear, string ForPeriod, long CustomerBranchId, int UserID, long RoleID, long ATBDId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditTransactions
                             where row.ProcessId == ProcessId && row.FinancialYear == FinancialYear
                             && row.ForPeriod == ForPeriod && row.CustomerBranchId == CustomerBranchId && row.UserID == UserID
                             && row.RoleID == RoleID && row.ATBDId == ATBDId && row.StatusId == 6
                             && row.AuditID == AuditID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool UpdateInternalAuditTxn(InternalAuditTransaction IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    InternalAuditTransaction RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                               where row.ProcessId == IAT.ProcessId && row.FinancialYear == IAT.FinancialYear
                                                               && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId
                                                               && row.UserID == IAT.UserID && row.RoleID == IAT.RoleID && row.ATBDId == IAT.ATBDId
                                                               && row.VerticalID == IAT.VerticalID && row.AuditID == IAT.AuditID
                                                               select row).FirstOrDefault();

                    RecordtoUpdate.PersonResponsible = IAT.PersonResponsible;
                    RecordtoUpdate.ObservatioRating = IAT.ObservatioRating;
                    RecordtoUpdate.ObservationCategory = IAT.ObservationCategory;
                    RecordtoUpdate.ForPeriod = IAT.ForPeriod;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.ObservationSubCategory = IAT.ObservationSubCategory;// Added By Hardik Sapara For Update Observation SubCategory.
                    RecordtoUpdate.UpdatedOn = DateTime.Now;
                    RecordtoUpdate.UpdatedBy = IAT.UpdatedBy;
                    RecordtoUpdate.AuditID = IAT.AuditID;
                    RecordtoUpdate.UHComment = IAT.UHComment;
                    RecordtoUpdate.UHPersonResponsible = IAT.UHPersonResponsible;
                    RecordtoUpdate.PRESIDENTComment = IAT.PRESIDENTComment;
                    RecordtoUpdate.PRESIDENTPersonResponsible = IAT.PRESIDENTPersonResponsible;
                    RecordtoUpdate.BodyContent = IAT.BodyContent;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateInternalAuditTxnStatus(InternalAuditTransaction IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    InternalAuditTransaction RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                               where row.ProcessId == IAT.ProcessId && row.FinancialYear == IAT.FinancialYear
                                                               && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId
                                                               && row.UserID == IAT.UserID && row.RoleID == IAT.RoleID && row.ATBDId == IAT.ATBDId
                                                               && row.VerticalID == IAT.VerticalID && row.AuditID == IAT.AuditID
                                                               select row).FirstOrDefault();

                    RecordtoUpdate.PersonResponsible = IAT.PersonResponsible;
                    RecordtoUpdate.ObservatioRating = IAT.ObservatioRating;
                    RecordtoUpdate.ObservationCategory = IAT.ObservationCategory;
                    RecordtoUpdate.ForPeriod = IAT.ForPeriod;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.Remarks = IAT.Remarks;
                    RecordtoUpdate.StatusId = IAT.StatusId;
                    RecordtoUpdate.StatusChangedOn = IAT.StatusChangedOn;
                    RecordtoUpdate.ObservationSubCategory = IAT.ObservationSubCategory;// Added By Hardik Sapara For Update Observation SubCategory.                 
                    RecordtoUpdate.VerticalID = IAT.VerticalID;
                    RecordtoUpdate.AuditID = IAT.AuditID;
                    RecordtoUpdate.UpdatedBy = IAT.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = IAT.UpdatedOn;
                    RecordtoUpdate.UHComment = IAT.UHComment;
                    RecordtoUpdate.UHPersonResponsible = IAT.UHPersonResponsible;
                    RecordtoUpdate.PRESIDENTComment = IAT.PRESIDENTComment;
                    RecordtoUpdate.PRESIDENTPersonResponsible = IAT.PRESIDENTPersonResponsible;
                    RecordtoUpdate.BodyContent = IAT.BodyContent;
                    RecordtoUpdate.BriefObservation = IAT.BriefObservation;
                    RecordtoUpdate.ObjBackground = IAT.ObjBackground;
                    RecordtoUpdate.DefeciencyType = IAT.DefeciencyType;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }


        #endregion

        public static bool CreateRiskActivityTBDMapping(List<RiskActivityToBeDoneMapping> RATBDM)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;
                    RATBDM.ForEach(entry =>
                    {
                        entities.RiskActivityToBeDoneMappings.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateRiskActivityTBDMappings(RiskActivityToBeDoneMapping RATBD, int RATBDMID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    RiskActivityToBeDoneMapping RATBDToUpdate = (from row in entities.RiskActivityToBeDoneMappings
                                                                 where row.ID == RATBDMID && row.RiskActivityId == RATBD.RiskActivityId
                                                                 select row).FirstOrDefault();
                    RATBDToUpdate.ActivityTobeDone = RATBD.ActivityTobeDone;
                    RATBDToUpdate.Rating = RATBD.Rating;
                    RATBDToUpdate.IsActive = true;
                    RATBDToUpdate.AuditStepMasterID = RATBD.AuditStepMasterID;
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static void RiskActivityTxnUpdate(RiskActivityTransaction RiskData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                RiskActivityTransaction eventToUpdate = (from row in entities.RiskActivityTransactions
                                                         where row.Id == RiskData.Id
                                                         && row.RiskCreationId == RiskData.RiskCreationId
                                                         && row.ProcessId == RiskData.ProcessId && row.SubProcessId == RiskData.SubProcessId
                                                         && row.IsDeleted == false
                                                         select row).FirstOrDefault();

                eventToUpdate.ControlDescription = RiskData.ControlDescription;
                eventToUpdate.MControlDescription = RiskData.MControlDescription;
                eventToUpdate.PersonResponsible = RiskData.PersonResponsible;
                eventToUpdate.EffectiveDate = RiskData.EffectiveDate;
                eventToUpdate.CustomerBranchId = RiskData.CustomerBranchId;
                eventToUpdate.Key_Value = RiskData.Key_Value;
                eventToUpdate.PrevationControl = RiskData.PrevationControl;
                eventToUpdate.AutomatedControl = RiskData.AutomatedControl;
                eventToUpdate.Frequency = RiskData.Frequency;
                eventToUpdate.GapDescription = RiskData.GapDescription;
                eventToUpdate.Recommendations = RiskData.Recommendations;
                eventToUpdate.ActionRemediationplan = RiskData.ActionRemediationplan;
                eventToUpdate.IPE = RiskData.IPE;
                eventToUpdate.ERPsystem = RiskData.ERPsystem;
                eventToUpdate.FRC = RiskData.FRC;
                eventToUpdate.UniqueReferred = RiskData.UniqueReferred;
                eventToUpdate.RiskRating = RiskData.RiskRating;
                eventToUpdate.ControlRating = RiskData.ControlRating;
                eventToUpdate.VerticalsId = RiskData.VerticalsId;
                eventToUpdate.ProcessScore = RiskData.ProcessScore;
                entities.SaveChanges();
            }
        }

        public static void EditRiskCategoryMapping(long Categoryid, long Riskid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RiskToUpdate = (from row in entities.RiskCategoryMappings
                                    where row.RiskCategoryCreationId == Riskid && row.RiskCategoryId == Categoryid
                                    select row).FirstOrDefault();
                RiskToUpdate.IsActive = true;
                entities.SaveChanges();
            };
        }

        public static void EditAssertionsMapping(long Assertionid, long Riskid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RiskToUpdate = (from row in entities.AssertionsMappings
                                    where row.RiskCategoryCreationId == Riskid && row.AssertionId == Assertionid
                                    select row).FirstOrDefault();
                RiskToUpdate.IsActive = true;
                entities.SaveChanges();
            };
        }

        public static bool RiskCategoryMappingExists(long RiskCategoryid, long Riskid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskCategoryMappings
                             where row.RiskCategoryCreationId == Riskid && row.RiskCategoryId == RiskCategoryid
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool AssertionMappingExists(long Assertionid, long Riskid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AssertionsMappings
                             where row.RiskCategoryCreationId == Riskid && row.AssertionId == Assertionid
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static void RiskUpdateRCM(RiskCategoryCreation RiskData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                RiskCategoryCreation eventToUpdate = (from row in entities.RiskCategoryCreations
                                                      where row.Id == RiskData.Id
                                                      select row).FirstOrDefault();
                eventToUpdate.ActivityDescription = RiskData.ActivityDescription;
                eventToUpdate.ControlObjective = RiskData.ControlObjective;
                eventToUpdate.LocationType = RiskData.LocationType;

                entities.SaveChanges();
            }
        }
        public static void RiskUpdate(RiskCategoryCreation RiskData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                RiskCategoryCreation eventToUpdate = (from row in entities.RiskCategoryCreations
                                                      where row.Id == RiskData.Id
                                                      select row).FirstOrDefault();
                eventToUpdate.ControlNo = RiskData.ControlNo;
                eventToUpdate.ActivityDescription = RiskData.ActivityDescription;
                eventToUpdate.ControlObjective = RiskData.ControlObjective;
                eventToUpdate.LocationType = RiskData.LocationType;
                eventToUpdate.CustomerBranchId = RiskData.CustomerBranchId;
                entities.SaveChanges();
            }
        }
        public static void UpdateIndustryMappedID(long categoryid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {


                var ids = (from row in entities.IndustryMappings
                           where row.RiskCategoryCreationId == categoryid
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    IndustryMapping prevmappedids = (from row in entities.IndustryMappings
                                                     where row.ID == entry
                                                     select row).FirstOrDefault();
                    entities.IndustryMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }
        }

        public static void UpdateRiskIndustryMapping(long Riskid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ids = (from row in entities.IndustryMappings
                           where row.RiskCategoryCreationId == Riskid
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    IndustryMapping RiskToUpdate = (from row in entities.IndustryMappings
                                                    where row.ID == entry
                                                    select row).FirstOrDefault();
                    RiskToUpdate.IsActive = false;
                });
                entities.SaveChanges();
            }
        }

        public static void UpdateRiskCategoryMapping(long Riskid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ids = (from row in entities.RiskCategoryMappings
                           where row.RiskCategoryCreationId == Riskid
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    RiskCategoryMapping RiskToUpdate = (from row in entities.RiskCategoryMappings
                                                        where row.ID == entry
                                                        select row).FirstOrDefault();
                    RiskToUpdate.IsActive = false;
                });
                entities.SaveChanges();
            }
        }



        public static void UpdateAssertionsMapping(long Riskid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ids = (from row in entities.AssertionsMappings
                           where row.RiskCategoryCreationId == Riskid
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    AssertionsMapping AssertionToUpdate = (from row in entities.AssertionsMappings
                                                           where row.ID == entry
                                                           select row).FirstOrDefault();
                    AssertionToUpdate.IsActive = false;
                });
                entities.SaveChanges();
            }
        }


        public static bool UpdateRecord(long Processid, long SubProcessid, long riskcreationId, long Frequency, long BranchId, long verticalID, long Activityid, string ControlDescription, long? processOwnerID, long? controlOwnerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             where row.CustomerBranchId == BranchId
                             && row.VerticalsId == verticalID
                             && row.ProcessId == Processid
                             && row.SubProcessId == SubProcessid
                             && row.RiskCreationId == riskcreationId
                             && row.Frequency == Frequency
                             && row.ActivityID == Activityid
                             && row.ControlDescription.ToUpper().Trim() == ControlDescription.ToUpper().Trim()
                             select row).FirstOrDefault();

                if (query != null)
                {
                    query.PersonResponsible = (long)processOwnerID;
                    query.ControlOwner = (long)controlOwnerID;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static RiskCategoryCreation RiskGetByID(int ID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.RiskCategoryCreations
                                        where row.Id == ID && row.SpecialAuditFlag == false
                                        select row).FirstOrDefault();

                return MstAuditorMaster;
            }
        }
        public static RiskActivityTransaction RiskActivityTransactionGetByID(int RiskActivityId, int RiskCreationId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstAuditorMaster = (from row in entities.RiskActivityTransactions
                                        where row.Id == RiskActivityId && row.RiskCreationId == RiskCreationId
                                        && row.SpecialAuditFlag == false
                                        select row).FirstOrDefault();

                return MstAuditorMaster;
            }
        }
        public static FileData_Risk GetFile(int fileID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var file = (from row in entities.FileData_Risk
                            where row.ID == fileID
                            select row).FirstOrDefault();

                return file;
            }
        }
        public static Tran_AuditCommitePresantionUpload GetFileTran_AuditCommitePresantionUpload(int fileID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var file = (from row in entities.Tran_AuditCommitePresantionUpload
                            where row.Id == fileID
                            select row).FirstOrDefault();

                return file;
            }
        }
        public static Tran_FinalDeliverableUpload GetFileTran_FinalDeliverableUpload(int fileID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var file = (from row in entities.Tran_FinalDeliverableUpload
                            where row.Id == fileID
                            select row).FirstOrDefault();

                return file;
            }
        }

        public static Tran_AnnxetureUpload GetFileTran_AnnxetureUpload(int fileID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var file = (from row in entities.Tran_AnnxetureUpload
                            where row.Id == fileID
                            select row).FirstOrDefault();

                return file;
            }
        }

        public static InternalFileData_Risk GetFileInternalFileData_Risk(int fileID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var file = (from row in entities.InternalFileData_Risk
                            where row.ID == fileID
                            select row).FirstOrDefault();

                return file;
            }
        }
        public static AuditSampleForm GetComplianceFormByID(long RiskCreationID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var form = (from row in entities.AuditSampleForms
                            where row.RiskCreationID == RiskCreationID
                            select row).FirstOrDefault();

                return form;
            }
        }
        public static InternalAuditInstance GetInternalAuditInstanceData(int InternalAuditInstance)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var instanceData = (from row in entities.InternalAuditInstances
                                    where row.ID == InternalAuditInstance && row.IsDeleted == false
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }



        public static AuditInstance GetAuditInstanceData(int RiskCreationID, long AuditInstanceID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var instanceData = (from row in entities.AuditInstances
                                    where row.ID == AuditInstanceID && row.RiskCreationId == RiskCreationID && row.IsDeleted == false
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }
        public static AuditImplementationInstance GetAuditImplementationInstanceDataOLD(string forperiod, int Implementationauditinstance, int Customerbranchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var instanceData = (from row in entities.AuditImplementationInstances
                                    where row.ForPeriod == forperiod && row.IsDeleted == false
                                    && row.Id == Implementationauditinstance
                                    && row.CustomerBranchID == Customerbranchid
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }

        public static InternalAuditInstance GetInternalAuditInstanceData(int ProcessId, int Internalauditinstance)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var instanceData = (from row in entities.InternalAuditInstances
                                    where row.ProcessId == ProcessId && row.IsDeleted == false
                                    && row.ID == Internalauditinstance
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }
        public static RecentAuditTransactionView GetCurrentStatusByComplianceID(int ScheduledOnID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var currentStatusID = (from row in entities.RecentAuditTransactionViews
                                       where row.AuditScheduleOnID == ScheduledOnID
                                       select row).FirstOrDefault();

                return currentStatusID;
            }
        }
        public static RecentInternalAuditTransactionView GetCurrentStatusByInternalAuditComplianceID(int ScheduledOnID, int ATBDID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var currentStatusID = (from row in entities.RecentInternalAuditTransactionViews
                                       where row.ATBDId == ATBDID
                                       select row).FirstOrDefault();

                return currentStatusID;
            }
        }
        public static RecentImplementationAuditTransactionView GetCurrentStatusByInternalAuditComplianceIDIMP(int ScheduledOnID, int ResultID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var currentStatusID = (from row in entities.RecentImplementationAuditTransactionViews
                                       where row.ResultID == ResultID
                                       select row).FirstOrDefault();

                return currentStatusID;
            }
        }
        //public static RecentInternalAuditTransactionView GetCurrentStatusByInternalAuditComplianceID(int ScheduledOnID,int ATBDID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var currentStatusID = (from row in entities.RecentInternalAuditTransactionViews
        //                               where row.AuditScheduleOnID == ScheduledOnID && row.ATBDId == ATBDID
        //                               select row).First();

        //        return currentStatusID;
        //    }
        //}
        public static AuditSampleForm GetAuditSampleFormByID(long RiskCreationID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var form = (from row in entities.AuditSampleForms
                            where row.RiskCreationID == RiskCreationID
                            select row).FirstOrDefault();

                return form;
            }
        }
        public static Mst_RiskResult GetMst_RiskResultyID(long RiskCreationID, long AuditScheduleOnId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //var MstRiskResult = (from row in entities.Mst_RiskResult
                //                     where row.RiskCreationId == RiskCreationID && row.AuditScheduleOnID == AuditScheduleOnId
                //            select row).FirstOrDefault();
                var MstRiskResult = (from row in entities.Mst_RiskResult
                                     where row.RiskCreationId == RiskCreationID && row.AuditScheduleOnID == AuditScheduleOnId
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }



        public static long GetRoleIDbyCode(String role)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long RoleID = (from row in entities.mst_Role
                               where row.Code == role
                               select row.ID).Single();

                return RoleID;
            }

        }
        public static ImplementationAuditResult GetImplementationAuditResultIMPlementationIDOLD(int ResultID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from row in entities.ImplementationAuditResults
                                     where row.ResultID == ResultID
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        //public static ImplementationAuditResult GetImplementationAuditResultIMPlementationID(int ResultID,int VerticalID,int AuditScheduleOnID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {

        //        var MstRiskResult = (from row in entities.ImplementationAuditResults
        //                             where row.ResultID == ResultID && row.VerticalID== VerticalID
        //                             && row.AuditScheduleOnID== AuditScheduleOnID
        //                             select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

        //        return MstRiskResult;
        //    }
        //}
        public static ImplementationAuditResult GetImplementationAuditResultIMPlementationID(int ResultID, int VerticalID, int AuditScheduleOnID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from row in entities.ImplementationAuditResults
                                     where row.ResultID == ResultID && row.VerticalID == VerticalID
                                     && row.AuditScheduleOnID == AuditScheduleOnID
                                     && row.AuditID == AuditID
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        public static List<string> GetAuditClosurePeriod(long CustomerBranchID, long Verticalid, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from AC in entities.AuditClosures
                                     join MCB in entities.mst_CustomerBranch
                                     on AC.CustomerbranchId equals MCB.ID
                                     where AC.ACStatus == 1
                                     && MCB.ParentID == CustomerBranchID
                                     && AC.FinancialYear == FinancialYear
                                     && AC.VerticalID == Verticalid
                                     && MCB.IsDeleted == false
                                     && MCB.Status == 1
                                     select AC.ForPeriod).Distinct().ToList();

                return MstRiskResult;
            }
        }
        public static List<string> GetAuditClosureLocation(long CustomerBranchID, long ProcessId, int ObservationCategory, long ObservationSubCategory, string FinancialYear, string Observation, string ManagementResponse, long Personresponsible)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from AC in entities.AuditClosures
                                     join MCB in entities.mst_CustomerBranch
                                     on AC.CustomerbranchId equals MCB.ID
                                     where AC.ACStatus == 1
                                     && MCB.ParentID == CustomerBranchID
                                     && AC.FinancialYear == FinancialYear
                                     && AC.ObservationCategory == ObservationCategory
                                     && AC.ObservationSubCategory == ObservationSubCategory
                                     && AC.ProcessId == ProcessId
                                     && MCB.IsDeleted == false
                                     && MCB.Status == 1
                                     && AC.Observation.ToUpper().Trim() == Observation.ToUpper().Trim()
                                     && AC.ManagementResponse.ToUpper().Trim() == ManagementResponse.ToUpper().Trim()
                                     && AC.PersonResponsible == Personresponsible
                                     select MCB.Name).Distinct().ToList();

                return MstRiskResult;
            }
        }


        public static AuditImplementationTransaction GetInternalAuditTransactionbyIDIMPOLD(long AuditScheduleOnID, int ResultId, string FinancialYear, string Period, int RoleId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from row in entities.AuditImplementationTransactions
                                     where row.ImplementationScheduleOnID == AuditScheduleOnID && row.ResultID == ResultId
                                     && row.FinancialYear == FinancialYear && row.ForPeriod == Period && row.RoleID == RoleId
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        //public static AuditImplementationTransaction GetInternalAuditTransactionbyIDIMP(long AuditScheduleOnID, int ResultId, string FinancialYear, string Period, int RoleId,int VerticalID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {

        //        var MstRiskResult = (from row in entities.AuditImplementationTransactions
        //                             where row.ImplementationScheduleOnID == AuditScheduleOnID && row.ResultID == ResultId
        //                             && row.FinancialYear == FinancialYear && row.ForPeriod == Period && row.RoleID == RoleId
        //                             && row.VerticalID== VerticalID
        //                             select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

        //        return MstRiskResult;
        //    }
        //}
        public static AuditImplementationTransaction GetInternalAuditTransactionbyIDIMP(long AuditScheduleOnID, int ResultId, string FinancialYear, string Period, int RoleId, int VerticalID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from row in entities.AuditImplementationTransactions
                                     where row.ImplementationScheduleOnID == AuditScheduleOnID && row.ResultID == ResultId
                                     && row.FinancialYear == FinancialYear && row.ForPeriod == Period && row.RoleID == RoleId
                                     && row.VerticalID == VerticalID && row.AuditID == AuditID
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }

        public static InternalAuditTransaction GetInternalAuditTransactionbyIDNew(long AuditScheduleOnID, int ATBDId, string FinancialYear, string Period, int RoleId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from row in entities.InternalAuditTransactions
                                     where row.AuditScheduleOnID == AuditScheduleOnID && row.ATBDId == ATBDId
                                     && row.FinancialYear == FinancialYear && row.ForPeriod == Period && row.RoleID == RoleId
                                     && row.AuditID == AuditID
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        public static InternalAuditTransaction GetInternalAuditTransactionbyID(long AuditScheduleOnID, int ATBDId, string FinancialYear, string Period, int RoleId, int VerticalId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from row in entities.InternalAuditTransactions
                                     where row.AuditScheduleOnID == AuditScheduleOnID && row.ATBDId == ATBDId
                                     && row.FinancialYear == FinancialYear && row.ForPeriod == Period && row.RoleID == RoleId
                                     && row.VerticalID == VerticalId
                                     && row.AuditID == AuditID
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        public static RiskActivityTransaction GetAuditByInstanceID(int ScheduledOnID, int CustomerBranchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                long AuditRiskcreationID = (from row in entities.AuditTransactions
                                            where row.AuditScheduleOnID == ScheduledOnID && row.CustomerBranchId == CustomerBranchid
                                            select row.RiskCreationID).FirstOrDefault();

                long complianceID = (from row in entities.AuditInstances
                                     where row.RiskCreationId == AuditRiskcreationID && row.CustomerBranchID == CustomerBranchid && row.IsDeleted == false
                                     select row.RiskCreationId).FirstOrDefault();

                var riskactivitytransaction = (from row in entities.RiskCategoryCreations
                                               join row1 in entities.RiskActivityTransactions
                                               on row.Id equals row1.RiskCreationId
                                               where row.Id == complianceID && row1.CustomerBranchId == CustomerBranchid
                                               select row1).FirstOrDefault();

                return riskactivitytransaction;
            }
        }

        public static RiskActivityToBeDoneMapping GetRiskActivityToBeDoneMappingByInstanceID(int ATBNID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var AuditRiskcreationID = (from row in entities.RiskActivityToBeDoneMappings
                                           where row.ID == ATBNID //&& row.CustomerBranchId == CustomerBranchID
                                           //&& row.FinancialYear == FinancialYear
                                           select row).Distinct().FirstOrDefault();
                return AuditRiskcreationID;
            }
        }
        public static InternalAuditScheduleOn GetInternalAuditScheduleOnByDetails(int UserID, int RoleId, string FinancialYear, string ForMonth, int CustomerBranchID, int ATBDId, int VerticalId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (
                                    from row in entities.InternalControlAuditAssignments
                                    join row1 in entities.InternalAuditScheduleOns
                                    on row.InternalAuditInstance equals row1.InternalAuditInstance
                                    join row2 in entities.RiskActivityToBeDoneMappings
                                    on row.ProcessId equals row2.ProcessId
                                    where row.ProcessId == row1.ProcessId
                                    && row1.FinancialYear == FinancialYear
                                    && row1.ForMonth == ForMonth
                                    && row.CustomerBranchID == CustomerBranchID
                                    && row.VerticalID == VerticalId
                                    && row2.ID == ATBDId
                                    && row.AuditID == AuditID
                                    select row1
                             ).FirstOrDefault();
                return MstRiskResult;
            }
        }
        //public static InternalAuditScheduleOn GetInternalAuditScheduleOnByDetailsNew(int UserID, int RoleId, string FinancialYear, string ForMonth, int CustomerBranchID, int ATBDId)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var MstRiskResult = (
        //                            from row in entities.InternalControlAuditAssignments
        //                            join row1 in entities.InternalAuditScheduleOns
        //                            on row.InternalAuditInstance equals row1.InternalAuditInstance
        //                            join row2 in entities.RiskActivityToBeDoneMappings
        //                            on row.ProcessId equals row2.ProcessId
        //                            where row.ProcessId == row1.ProcessId
        //                            && row1.FinancialYear == FinancialYear
        //                            && row1.ForMonth == ForMonth
        //                            && row.CustomerBranchID == CustomerBranchID
        //                            && row2.ID == ATBDId
        //                            select row1
        //                     ).FirstOrDefault();
        //        return MstRiskResult;
        //    }
        //}
        //public static InternalAuditScheduleOn GetInternalAuditScheduleOnByDetails1OLD(int UserID, int RoleId, string FinancialYear, string ForMonth, int CustomerBranchID, int ATBDId)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var MstRiskResult = (
        //                            from row in entities.InternalControlAuditAssignments
        //                            join row1 in entities.InternalAuditScheduleOns
        //                            on row.InternalAuditInstance equals row1.InternalAuditInstance
        //                            join row2 in entities.RiskActivityToBeDoneMappings
        //                            on row.ProcessId equals row2.ProcessId
        //                            join row3 in entities.RecentInternalAuditTransactionViews
        //                            on row1.ID equals row3.AuditScheduleOnID
        //                            where row.ProcessId == row1.ProcessId
        //                            && row1.FinancialYear == FinancialYear
        //                            && row1.ForMonth == ForMonth
        //                            && row.CustomerBranchID == CustomerBranchID
        //                            && row2.ID == ATBDId 
        //                            select row1
        //                     ).FirstOrDefault();
        //        return MstRiskResult;
        //    }
        //}
        //public static InternalAuditScheduleOn GetInternalAuditScheduleOnByDetails(int UserID, int RoleId, string FinancialYear, string ForMonth, int CustomerBranchID, int ATBDId,int VerticalID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var MstRiskResult = (
        //                            from row in entities.InternalControlAuditAssignments
        //                            join row1 in entities.InternalAuditScheduleOns
        //                            on row.InternalAuditInstance equals row1.InternalAuditInstance
        //                            join row2 in entities.RiskActivityToBeDoneMappings
        //                            on row.ProcessId equals row2.ProcessId
        //                            join row3 in entities.RecentInternalAuditTransactionViews
        //                            on row1.ID equals row3.AuditScheduleOnID
        //                            where row.ProcessId == row1.ProcessId                                  
        //                            && row1.FinancialYear == FinancialYear
        //                            && row1.ForMonth == ForMonth
        //                            && row.CustomerBranchID == CustomerBranchID
        //                            && row2.ID == ATBDId && row.VerticalID== VerticalID
        //                            select row1
        //                     ).FirstOrDefault();
        //        return MstRiskResult;
        //    }
        //}

        public static ImplementationAuditTransactionView GetClosedTransaction(int ScheduledOnID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.ImplementationAuditTransactionViews
                                  where row.ImplementationScheduleOnID == ScheduledOnID
                                  // && (row.AuditStatusID == 3 || row.AuditStatusID == 3 || row.AuditStatusID == 7 || row.AuditStatusID == 8 || row.AuditStatusID == 9)
                                  orderby row.AuditTransactionID descending
                                  select row).ToList();



                return statusList.FirstOrDefault();
            }
        }
        public static long GetImpementationIsNotImplemented(string FinancialYear, string ForMonth, int CustomerBranchid, int verticalId, int Scheduledonid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (
                from row in entities.ImplementationAuditResults
                where row.FinancialYear == FinancialYear && row.ForPeriod == ForMonth
                && row.CustomerBranchId == CustomerBranchid
                && row.VerticalID == verticalId && row.AuditScheduleOnID == Scheduledonid
                && row.Status == 3
                select row.ImplementationStatus).FirstOrDefault();
                if (MstRiskResult != null)
                {
                    return (long)MstRiskResult;
                }
                else
                {
                    return 0;
                }

            }
        }
        public static AuditImpementationScheduleOn GetAuditImpementationScheduleOnDetails(string FinancialYear, string ForMonth, int CustomerBranchid, int verticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (
                                    from row in entities.AuditImpementationScheduleOns
                                    join row1 in entities.AuditImplementationInstances
                                    on row.ImplementationInstance equals row1.Id
                                    where row.FinancialYear == FinancialYear && row.ForMonth == ForMonth
                                    && row1.CustomerBranchID == CustomerBranchid
                                          && row1.VerticalID == verticalId
                                    select row).FirstOrDefault();
                return MstRiskResult;
            }
        }




        public static Mst_RiskResult GetMst_RiskResultByScheduledOnID(int ScheduledOnID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from row in entities.Mst_RiskResult
                                     where row.AuditScheduleOnID == ScheduledOnID
                                     select row).FirstOrDefault();

                return MstRiskResult;
            }
        }
        public static List<NameValue> GetUserRoles(int userID = -1)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<NameValue> AssignedRoles = new List<NameValue>();
                List<NameValue> EventAssignedRoles = new List<NameValue>();
                List<NameValue> roles = new List<NameValue>();
                if (userID != -1)
                {
                    AssignedRoles = (from row in entities.AuditAssignments
                                     join ro in entities.mst_Role
                                     on row.RoleID equals ro.ID
                                     where row.UserID == userID
                                     select new NameValue()
                                     {
                                         ID = row.RoleID,
                                         Name = ro.Name
                                     }).GroupBy(entry => entry.ID).Select(entry => entry.FirstOrDefault()).ToList();
                }
                else
                {
                    AssignedRoles = (from row in entities.AuditAssignments
                                     join ro in entities.mst_Role
                                     on row.RoleID equals ro.ID
                                     select new NameValue()
                                     {
                                         ID = row.RoleID,
                                         Name = ro.Name
                                     }).ToList();
                }

                //roles = AssignedRoles.Union(EventAssignedRoles).ToList();
                roles = AssignedRoles.ToList();
                return roles;
            }
        }

        //added by rahul for id Excel Utility

        public static int GetBranchVerticalIDByName(int Customerid, string Verticalname)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var BranchVerticalID = (from row in entities.mst_Vertical
                                        where row.VerticalName.ToUpper() == Verticalname.ToUpper()
                                        && row.CustomerID == Customerid
                                        select row.ID).FirstOrDefault();

                return Convert.ToInt32(BranchVerticalID);
            }
        }
        public static int GetriskcategoryIDByName(string riskcategoryname)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var riskcategoryId = (from row in entities.mst_Riskcategory
                                      where row.Name.ToUpper() == riskcategoryname.ToUpper()
                                      && row.IsDeleted == false
                                      select row.Id).FirstOrDefault();

                return Convert.ToInt32(riskcategoryId);
            }
        }
        public static int GetAssertionIDByName(string AssertionName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var branchId = (from row in entities.mst_Assertions
                                where row.Name.ToUpper() == AssertionName.ToUpper()
                                select row.Id).FirstOrDefault();

                return Convert.ToInt32(branchId);
            }
        }

        public static int GetUserIDByName(string UserName, long customerID, string EmailID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long userid = -1;
                string[] split = UserName.Split(' ');
                //if (split.Length == 2)
                //{
                //    string firstname = split[0].ToString().Trim();
                //    string lastname = split[1].ToString().Trim();
                //    userid = (from row in entities.mst_User
                //              where
                //              //row.FirstName.ToUpper() == firstname.ToUpper() &&
                //              //row.LastName.ToUpper() == lastname.ToUpper() &&
                //              row.IsDeleted == false && row.CustomerID == customerID 
                //              && row.Email.ToUpper() == EmailID.ToUpper()
                //              select row.ID).FirstOrDefault();
                //}
                //else
                //{
                userid = (from row in entities.mst_User
                          where row.IsDeleted == false
                          && row.CustomerID == customerID
                          && row.Email.ToUpper() == EmailID.ToUpper()
                          select row.ID).FirstOrDefault();

                //}
                return Convert.ToInt32(userid);
            }
        }
        public static int GetPrimarySecondaryIdByName(string PrimarySecondaryName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var branchId = (from row in entities.mst_Control
                                where row.Name.ToUpper().Trim() == PrimarySecondaryName.ToUpper().Trim()
                                && row.Flag == "S"
                                select row.Id).FirstOrDefault();

                return Convert.ToInt32(branchId);
            }
        }
        public static int GetPrevationControlIdByName(string PrevationControlName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var branchId = (from row in entities.mst_Control
                                where row.Name.ToUpper() == PrevationControlName.ToUpper()
                                && row.Flag == "P"
                                select row.Id).FirstOrDefault();

                return Convert.ToInt32(branchId);
            }
        }
        public static int GetAutomatedControlIDByName(string AutomatedControlName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var branchId = (from row in entities.mst_Control
                                where row.Name.ToUpper() == AutomatedControlName.ToUpper()
                                && row.Flag == "A"
                                select row.Id).FirstOrDefault();

                return Convert.ToInt32(branchId);
            }
        }
        public static int GetFrequencyIDByName(string FrequencyName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var branchId = (from row in entities.mst_Frequency
                                where row.Name.ToUpper() == FrequencyName.ToUpper()
                                select row.Id).FirstOrDefault();

                return Convert.ToInt32(branchId);
            }
        }
        public static int GetKeyIdByName(string KeyName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var branchId = (from row in entities.mst_Key
                                where row.Name.ToUpper() == KeyName.ToUpper()
                                select row.Id).FirstOrDefault();

                return Convert.ToInt32(branchId);
            }
        }
        public static int GetCustomerBranchIDByName(string CustomerBranchName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var branchId = (from row in entities.mst_CustomerBranch
                                where row.Name.ToUpper() == CustomerBranchName.ToUpper()
                                && row.IsDeleted == false && row.Status == 1
                                select row.ID).FirstOrDefault();

                return Convert.ToInt32(branchId);
            }
        }
        public static int GetIndustryIdByName(string Industryname)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var IndustryId = (from row in entities.mst_Industry
                                  where row.Name.ToUpper() == Industryname.ToUpper()
                                  select row.ID).FirstOrDefault();

                return Convert.ToInt32(IndustryId);
            }
        }
        public static int GetBranchIdByName(string BranchName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var branchId = (from row in entities.mst_CustomerBranch
                                where row.Name.ToUpper() == BranchName.ToUpper()
                                && row.IsDeleted == false && row.Status == 1
                                select row.ID).FirstOrDefault();

                return Convert.ToInt32(branchId);
            }
        }
        public static int GetAssertionsIdByName(string AssertionsName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var branchId = (from row in entities.mst_Assertions
                                where row.Name.ToUpper() == AssertionsName.ToUpper()
                                select row.Id).FirstOrDefault();

                return Convert.ToInt32(branchId);
            }
        }

        public static bool CreateRiskResult(Mst_RiskResult mstriskresult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Mst_RiskResult.Add(mstriskresult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool CreateRiskResult(List<Mst_RiskResult> mstriskresult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    mstriskresult.ForEach(entry =>
                    {
                        entities.Mst_RiskResult.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CreateRiskActivityTransaction(List<RiskActivityTransaction> riskactivitytransaction)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    riskactivitytransaction.ForEach(entry =>
                    {
                        entities.RiskActivityTransactions.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateRiskCategoryCreationList(List<RiskCategoryCreation> riskcategorycreation)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    riskcategorycreation.ForEach(entry =>
                    {
                        RiskCategoryCreation mstRiskCategoryCreationToUpdate = (from row in entities.RiskCategoryCreations
                                                                                where row.Id == entry.Id
                                                                                select row).FirstOrDefault();
                        mstRiskCategoryCreationToUpdate.ActivityToBeDone = entry.ActivityToBeDone;
                        mstRiskCategoryCreationToUpdate.Rating = entry.Rating;
                        entities.SaveChanges();
                    });

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool UpdateRiskActivityTransaction(RiskActivityTransaction riskactivitytransaction)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    RiskActivityTransaction mstRiskActivityTransactionToUpdate = (from row in entities.RiskActivityTransactions
                                                                                  where row.Id == riskactivitytransaction.Id
                                                                                  && row.RiskCreationId == riskactivitytransaction.RiskCreationId
                                                                                  && row.VerticalsId == riskactivitytransaction.VerticalsId
                                                                                  select row).FirstOrDefault();
                    mstRiskActivityTransactionToUpdate.ControlDescription = riskactivitytransaction.ControlDescription;
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool UpdateRiskCategoryCreation(RiskCategoryCreation riskcategorycreation)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    RiskCategoryCreation mstRiskCategoryCreationToUpdate = (from row in entities.RiskCategoryCreations
                                                                            where row.Id == riskcategorycreation.Id
                                                                            select row).FirstOrDefault();
                    mstRiskCategoryCreationToUpdate.ActivityToBeDone = riskcategorycreation.ActivityToBeDone;
                    mstRiskCategoryCreationToUpdate.Rating = riskcategorycreation.Rating;
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }



        public static bool CreateExcel(List<RiskCategoryCreation> riskcategorycreation)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    riskcategorycreation.ForEach(entry =>
                    {
                        if (!(RiskCategoryManagement.ExistsNew(Convert.ToString(entry.ActivityDescription), Convert.ToString(entry.ControlObjective), Convert.ToInt32(entry.ProcessId), Convert.ToInt32(entry.LocationType), Convert.ToInt32(entry.CustomerId), Convert.ToInt32(entry.CustomerBranchId), Convert.ToInt32(entry.SubProcessId), entry.ControlNo)))
                        {
                            entities.RiskCategoryCreations.Add(entry);
                        }

                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool CreateExcel(List<RiskCategoryCreation> riskcategorycreation, List<RiskActivityTransaction> riskactivitytransaction)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    riskcategorycreation.ForEach(entry =>
                    {
                        riskactivitytransaction.ForEach(a =>
                            {
                                entities.RiskCategoryCreations.Add(entry);
                                a.RiskCreationId = entry.Id;
                                entities.RiskActivityTransactions.Add(a);
                            });
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CreateBulkExcelRCMMaster(List<mst_RCMMaster> ListOH)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int Count = 0;
                    ListOH.ForEach(EachStep =>
                    {
                        Count++;
                        entities.mst_RCMMaster.Add(EachStep);
                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool CreateExcelTransaction(List<RiskActivityTransaction> riskactivitytransaction)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    riskactivitytransaction.ForEach(entry =>
                    {
                        entities.RiskActivityTransactions.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool CreateAuditAssignment(List<Internal_AuditAssignment> riskactivitytransaction)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    riskactivitytransaction.ForEach(entry =>
                    {
                        entities.Internal_AuditAssignment.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool CreateExcel1(List<RiskCategoryCreation> riskcategorycreation, List<RiskCategoryMapping> riskcategorymapping)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    riskcategorycreation.ForEach(entry =>
                    {
                        entities.RiskCategoryCreations.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool CreateExcelMappling(List<RiskCategoryMapping> riskcategorymapping, long categoryceationid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    riskcategorymapping.ForEach(entry =>
                    {
                        entry.RiskCategoryCreationId = categoryceationid;
                        entities.RiskCategoryMappings.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public static int GetControlCreationIdNew(long processid, long subprocessid, long riskcreationId,
            string controldescription, long frequency, int customerbranchid, long verticalid, long activityid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var stateId = (from row in entities.RiskActivityTransactions
                               where row.ProcessId == processid && row.SubProcessId == subprocessid
                               && row.RiskCreationId == riskcreationId
                               && row.ControlDescription.ToUpper().Trim() == controldescription.ToUpper().Trim()
                               && row.Frequency == frequency && row.CustomerBranchId == customerbranchid
                               && row.VerticalsId == verticalid && row.ActivityID == activityid
                               select row.Id).FirstOrDefault();
                return Convert.ToInt32(stateId);
            }
        }
        public static int GetRiskCreationIdNew(string ActivityDescription, string ControlObjective, long Processid, int CustomerId, int Customerbranchid, long subprocessid, string ControlNumber)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var stateId = (from row in entities.RiskCategoryCreations
                               where row.ActivityDescription.ToUpper().Trim() == ActivityDescription.ToUpper().Trim() &&
                               row.ControlObjective.ToUpper().Trim() == ControlObjective.ToUpper().Trim()
                               && row.ProcessId == Processid && row.SubProcessId == subprocessid
                               && row.CustomerId == CustomerId && row.CustomerBranchId == Customerbranchid
                               && row.ControlNo.ToUpper().Trim() == ControlNumber.ToUpper().Trim()
                               select row.Id).FirstOrDefault();

                return Convert.ToInt32(stateId);
            }
        }
        public static int GetRiskCreationIdNew(string ActivityDescription, string ControlObjective, long Processid, long locationtype, int CustomerId, int Customerbranchid, long subprocessid, string ControlNumber)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var stateId = (from row in entities.RiskCategoryCreations
                               where row.ActivityDescription.ToUpper().Trim() == ActivityDescription.ToUpper().Trim() &&
                               row.ControlObjective.ToUpper().Trim() == ControlObjective.ToUpper().Trim()
                               && row.ProcessId == Processid && row.SubProcessId == subprocessid
                               && row.LocationType == locationtype //&& row.IsInternalAudit == "N"
                               && row.CustomerId == CustomerId && row.CustomerBranchId == Customerbranchid
                               && row.ControlNo.ToUpper().Trim() == ControlNumber.ToUpper().Trim()
                               select row.Id).FirstOrDefault();

                return Convert.ToInt32(stateId);
            }
        }
        public static int GetRiskCreationId(string ActivityDescription, string ControlObjective, long Processid, long locationtype, int CustomerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //var stateId = (from row in entities.mst_Subprocess
                //               where row.Name == subProcessname && row.ProcessId == Processid
                //               select row.Id).FirstOrDefault();

                var stateId = (from row in entities.RiskCategoryCreations
                               where row.ActivityDescription == ActivityDescription &&
                               row.ControlObjective == ControlObjective
                               && row.ProcessId == Processid && row.LocationType == locationtype && row.IsInternalAudit == "N"
                               && row.CustomerId == CustomerId
                               select row.Id).FirstOrDefault();

                return Convert.ToInt32(stateId);
            }
        }
        //public static bool CustomerBranchMappingExistsNEW(int riskcreationId, int branchid, int Processid, int SubProcessid)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var query = (from row in entities.CustomerBranchMappings
        //                     where row.RiskCategoryCreationId == riskcreationId 
        //                     && row.BranchId == branchid
        //                     && row.ProcessId == Processid 
        //                     && row.SubProcessId == SubProcessid
        //                     && row.IsActive == true
        //                     select row).FirstOrDefault();

        //        if (query != null)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }

        //        //return query.Select(entry => true).First();
        //    }
        //}

        public static bool ExistsNew(string ControlNos, long Processid, long SubProcessID, int CustomerId, int customerbranchid, string ActivityDescription, string ControlObjective)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskCategoryCreations
                             where row.ActivityDescription.ToUpper().Trim().Contains(ActivityDescription.ToUpper().Trim()) &&
                             row.ControlObjective.ToUpper().Trim().Contains(ControlObjective.ToUpper().Trim())
                             && row.ProcessId == Processid && row.SubProcessId == SubProcessID && row.IsInternalAudit == "N"
                             && row.CustomerId == CustomerId && row.CustomerBranchId == customerbranchid
                             && row.ControlNo.ToUpper().Trim() == ControlNos.ToUpper().Trim()
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                //return query.Select(entry => true).First();
            }
        }
        public static bool ExistsNew(string ActivityDescription, string ControlObjective, long Processid, long locationtype, int CustomerId, int customerbranchid, long SubProcessID, string ControlNos)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskCategoryCreations
                             where row.ActivityDescription.ToUpper().Trim().Contains(ActivityDescription.ToUpper().Trim()) &&
                             row.ControlObjective.ToUpper().Trim().Contains(ControlObjective.ToUpper().Trim())
                             && row.ProcessId == Processid && row.SubProcessId == SubProcessID && row.LocationType == locationtype && row.IsInternalAudit == "N"
                             && row.CustomerId == CustomerId && row.CustomerBranchId == customerbranchid
                             && row.ControlNo.ToUpper().Trim() == ControlNos.ToUpper().Trim()
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                //return query.Select(entry => true).First();
            }
        }
        public static bool Exists(string ActivityDescription, string ControlObjective, long Processid, long locationtype, int CustomerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskCategoryCreations
                             where row.ActivityDescription.Contains(ActivityDescription) &&
                             row.ControlObjective.Contains(ControlObjective)
                             && row.ProcessId == Processid && row.LocationType == locationtype && row.IsInternalAudit == "N"
                             && row.CustomerId == CustomerId
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                //return query.Select(entry => true).First();
            }
        }

        public static bool Exists(long Processid, long SubProcessid, long riskcreationId, long Frequency, long BranchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             where row.ProcessId == Processid &&
                             row.SubProcessId == SubProcessid
                             && row.RiskCreationId == riskcreationId && row.Frequency == Frequency
                             && row.CustomerBranchId == BranchId
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                //return query.Select(entry => true).First();
            }
        }

        public static bool Exists(long Processid, long SubProcessid, long riskcreationId, long Frequency, long BranchId, long verticalID, long Activityid, string ControlDescription)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             where row.CustomerBranchId == BranchId
                             && row.VerticalsId == verticalID
                             && row.ProcessId == Processid
                             && row.SubProcessId == SubProcessid
                             && row.RiskCreationId == riskcreationId
                             && row.Frequency == Frequency
                             && row.ActivityID == Activityid
                             && row.ControlDescription.ToUpper().Trim() == ControlDescription.ToUpper().Trim()
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;

                //return query.Select(entry => true).First();
            }
        }

        public static bool Exists(long Processid, long SubProcessid, long riskcreationId, long Frequency, long BranchId, long verticalID, long Activityid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             where row.CustomerBranchId == BranchId
                             && row.VerticalsId == verticalID
                             && row.ProcessId == Processid
                             && row.SubProcessId == SubProcessid
                             && row.RiskCreationId == riskcreationId
                             && row.Frequency == Frequency
                             && row.ActivityID == Activityid
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;

                //return query.Select(entry => true).First();
            }
        }


        public static bool Exists_Internal_AuditAssignment(long customerbranchId, long verticalID, long CustomerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Internal_AuditAssignment
                             where row.CustomerBranchid == customerbranchId
                             && row.VerticalID == verticalID
                             && row.AssignedTo == "I"
                             && row.CustomerId == CustomerId

                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;

                //return query.Select(entry => true).First();
            }
        }

        public static bool Exists(Mst_AuditPlanning MstAuditPlanning)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_AuditPlanning
                             where row.IsActive == false
                                  && row.ProcessId.Equals(MstAuditPlanning.ProcessId)
                                  && row.Personresponsible.Equals(MstAuditPlanning.Personresponsible)
                                  && row.Period.Equals(MstAuditPlanning.Period)
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static void CreateIndustryMapping(IndustryMapping IndustryMapping)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.IndustryMappings.Add(IndustryMapping);
                entities.SaveChanges();
            }
        }
        public static void CreateAuditSampleForm(AuditSampleForm auditsampleform)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                AuditSampleForm AuditSampleFormToUpdate = (from row in entities.AuditSampleForms
                                                           where row.RiskCreationID == auditsampleform.RiskCreationID
                                                      && row.CustomerBranchId == auditsampleform.CustomerBranchId
                                                           select row).FirstOrDefault();

                if (AuditSampleFormToUpdate == null)
                {
                    entities.AuditSampleForms.Add(auditsampleform);
                    entities.SaveChanges();
                }
                else
                {
                    AuditSampleFormToUpdate.Name = auditsampleform.Name;
                    AuditSampleFormToUpdate.FilePath = auditsampleform.FilePath;
                    AuditSampleFormToUpdate.FileKey = auditsampleform.FileKey;
                    entities.SaveChanges();
                }


            }
        }
        public static void CreateFileDataRisk(FileData_Risk filedatarisk)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                FileData_Risk filedatariskToUpdate = (from row in entities.FileData_Risk
                                                      where row.RiskCreationId == filedatarisk.RiskCreationId
                                                      && row.CustomerBranchId == filedatarisk.CustomerBranchId
                                                      select row).FirstOrDefault();

                if (filedatariskToUpdate == null)
                {
                    entities.FileData_Risk.Add(filedatarisk);
                    entities.SaveChanges();
                }
                else
                {
                    filedatariskToUpdate.Name = filedatarisk.Name;
                    filedatariskToUpdate.FilePath = filedatarisk.FilePath;
                    filedatariskToUpdate.FileKey = filedatarisk.FileKey;
                    entities.SaveChanges();
                }


            }
        }
        public static void UpdateFileDataRisk(FileData_Risk filedatarisk, long ProcessId, long SubProcessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                FileData_Risk filedatariskToUpdate = (from row in entities.FileData_Risk
                                                      where row.RiskCreationId == filedatarisk.RiskCreationId
                                                       && row.CustomerBranchId == filedatarisk.CustomerBranchId
                                                      select row).FirstOrDefault();

                if (filedatariskToUpdate == null)
                {
                    filedatarisk.ProcessID = ProcessId;
                    filedatarisk.SubProcessId = SubProcessId;
                    entities.FileData_Risk.Add(filedatarisk);
                    entities.SaveChanges();
                }
                else
                {
                    filedatariskToUpdate.TestUploadName = filedatarisk.TestUploadName;
                    filedatariskToUpdate.TestFilePath = filedatarisk.TestFilePath;
                    filedatariskToUpdate.TestFileKey = filedatarisk.TestFileKey;
                    entities.SaveChanges();
                }
            }
        }
        //public static void CreateCustomerBranchMappingg(CustomerBranchMapping customerbranchmapping)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        entities.CustomerBranchMappings.Add(customerbranchmapping);
        //        entities.SaveChanges();
        //    }
        //}
        public static void CreateRiskCategoryMapping(RiskCategoryMapping RiskCategoryMapping)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.RiskCategoryMappings.Add(RiskCategoryMapping);
                entities.SaveChanges();
            }
        }
        //public static void CreateCustomerBranchMapping(CustomerBranchMapping CustomerBranchMapping)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        entities.CustomerBranchMappings.Add(CustomerBranchMapping);
        //        entities.SaveChanges();
        //    }
        //}
        public static void RiskActivityTransactionMapping(RiskActivityTransaction riskactivitytransaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.RiskActivityTransactions.Add(riskactivitytransaction);
                entities.SaveChanges();
            }
        }
        public static void RiskCategoryCreationMapping(RiskCategoryCreation riskcategorycreation)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.RiskCategoryCreations.Add(riskcategorycreation);
                entities.SaveChanges();
            }
        }


        public static void CreateAssertionsMapping(AssertionsMapping AssertionsMapping)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.AssertionsMappings.Add(AssertionsMapping);
                entities.SaveChanges();
            }
        }

        public static List<int?> GetIndustryMappedID(long processId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var IndustryMappedIDs = (from row in entities.IndustryMappings
                                         where row.ProcessId == processId && row.IsActive == true
                                         select row.IndustryID).ToList();

                return IndustryMappedIDs;
            }

        }


        public static bool Exists(RiskCategoryCreation riskcategorycreation)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskCategoryCreations
                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                                  && row.ActivityDescription.Equals(riskcategorycreation.ActivityDescription)
                                  && row.ControlObjective.Equals(riskcategorycreation.ControlObjective)
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool RiskActivityTransactionExists(RiskActivityTransaction riskactivitytransaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             where row.IsDeleted == false
                                  && row.ControlDescription.Equals(riskactivitytransaction.ControlDescription)
                                  && row.MControlDescription.Equals(riskactivitytransaction.MControlDescription)
                                  && row.Frequency.Equals(riskactivitytransaction.Frequency)
                                 && row.ProcessId.Equals(riskactivitytransaction.ProcessId)
                                 && row.VerticalsId == riskactivitytransaction.VerticalsId
                             // && row.SubProcessId.Equals(riskactivitytransaction.SubProcessId)
                             select row).FirstOrDefault();


                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static object FillRiskCategoryProcess(string flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (flag == "P")
                {
                    var query = (from row in entities.mst_Riskcategory
                                 where row.IsDeleted == false && row.IsICRRisk == "I"
                                 select row);
                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                }
                else
                {
                    var query = (from row in entities.mst_Riskcategory
                                 where row.IsDeleted == false && row.IsICRRisk == "R"
                                 select row);
                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                }

            }
        }

        public static object FillCustomerBranch(long customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.Status == 1 && row.CustomerID == customerid
                             select row);
                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static object FillAssertions()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_Assertions
                             select row);
                var Assertions = (from row in query
                                  select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Assertions;
            }
        }
        public static object FillPeriod()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.Mst_AuditPeriodMaster
                             select row);
                var Assertions = (from row in query
                                  select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Assertions;
            }
        }

        public static object FillPrimarySecondary()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_Control
                             where row.Flag == "S"
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillResponsiblePerson()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.Mst_AuditorMaster
                             select row);
                var Assertions = (from row in query
                                  select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Assertions;
            }
        }
        public static object FillFrequency()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_Frequency
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillAutomatedControl()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_Control
                             where row.Flag == "A"
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillPreventiveControl()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_Control
                             where row.Flag == "P"
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillControlRating()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_Risk_ControlRating
                             where row.IsRiskControl == "C" && row.IsActive == false
                             orderby row.Value
                             select row);
                var Frequency = (from row in query
                                 select new { ID = (int)row.Value, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillRiskRating()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_Risk_ControlRating
                             where row.IsRiskControl == "R" && row.IsActive == false
                             orderby row.Value
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.Value, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillKey()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_Key
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillManagementUsers(long customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_User
                             where row.CustomerID == customerid
                             && row.IsDeleted == false && row.IsActive == true
                             && row.RoleID == 8
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }

        public static object FillAuditKickOffUsers(long customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<int> a = new List<int>();
                a.Clear();
                a.Add(8);
                a.Add(12);
                a.Add(10);
                a.Add(11);
                a.Add(2);
                var query = (from row in entities.mst_User
                             where row.CustomerID == customerid &&
                             row.IsDeleted == false && row.IsActive == true
                             && !a.Contains(row.RoleID) /*&& row.AuditorID==null*/
                             select row);

                query = query.Where(entry => entry.IsHead == false);

                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillUsers(long customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_User
                             where row.CustomerID == customerid
                             && row.IsDeleted == false && row.IsActive == true
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillAuditusers(long customerid, long Auditorid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<int> a = new List<int>();
                a.Clear();
                a.Add(8);
                a.Add(12);

                var query = (from row in entities.mst_User
                             where row.CustomerID == customerid && row.IsDeleted == false && row.IsActive == true && !a.Contains(row.RoleID)
                             && row.AuditorID == Auditorid //||row.AuditorID !=null) 
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillIndustry()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Industry
                             select row);
                var industry = (from row in query
                                select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return industry;
            }
        }

        public static List<RiskExportinfoReportNEW> GetAllRiskCategoryCreationExportBranchWise(int BranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<RiskExportinfoReportNEW> riskcategorycreations = new List<RiskExportinfoReportNEW>();
                if (BranchID != -1)
                {
                    riskcategorycreations = (from row in entities.RiskCategoryCreations
                                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                                             && row.CustomerBranchId == BranchID
                                             select new RiskExportinfoReportNEW()
                                             {
                                                 ControlNo = row.ControlNo,
                                                 RiskCreationId = row.Id,
                                                 ActivityDescription = row.ActivityDescription,
                                                 ControlObjective = row.ControlObjective,
                                                 RiskCategory = row.Id,
                                                 Industry = row.Id,
                                                 BranchId = (int)row.CustomerBranchId,
                                                 LocationType = row.LocationType,
                                                 EffectiveDate = null,
                                                 ProcessId = row.ProcessId,
                                                 SubProcessId = row.SubProcessId,
                                                 Assertions = null,
                                                 ControlDescription = null,
                                                 MControlDescription = null,
                                                 PersonResponsible = null,
                                                 Email = null,
                                                 Key = null,
                                                 PreventiveControl = null,
                                                 AutomatedControl = null,
                                                 Frequency = null,
                                                 GapDescription = null,
                                                 Recommendations = null,
                                                 ActionRemediationplan = null,
                                                 IPE = null,
                                                 ERPsystem = null,
                                                 FRC = null,
                                                 UniqueReferred = null,
                                                 TestStrategy = null,
                                                 DocumentsExamined = null,
                                                 RiskRating = null,
                                                 ControlRating = null
                                             }).ToList();

                    //riskcategorycreations = (from row in entities.RiskCategoryCreations
                    //                         join row1 in entities.CustomerBranchMappings
                    //                         on row.Id equals row1.RiskCategoryCreationId
                    //                         where row.IsDeleted == false && row.IsInternalAudit == "N"
                    //                         && row1.BranchId == BranchID
                    //                         select new RiskExportinfoReportNEW()
                    //                         {
                    //                             ControlNo = row.ControlNo,
                    //                             RiskCreationId = row.Id,
                    //                             ActivityDescription = row.ActivityDescription,
                    //                             ControlObjective = row.ControlObjective,
                    //                             RiskCategory = row.Id,
                    //                             Industry = row.Id,
                    //                             BranchId = row1.BranchId,
                    //                             LocationType = row.LocationType,
                    //                             EffectiveDate = null,
                    //                             ProcessId = row.ProcessId,
                    //                             SubProcessId = row.SubProcessId,
                    //                             Assertions = null,
                    //                             ControlDescription = null,
                    //                             MControlDescription = null,
                    //                             PersonResponsible = null,
                    //                             Email = null,
                    //                             Key = null,
                    //                             PreventiveControl = null,
                    //                             AutomatedControl = null,
                    //                             Frequency = null,
                    //                             GapDescription = null,
                    //                             Recommendations = null,
                    //                             ActionRemediationplan = null,
                    //                             IPE = null,
                    //                             ERPsystem = null,
                    //                             FRC = null,
                    //                             UniqueReferred = null,
                    //                             TestStrategy = null,
                    //                             DocumentsExamined = null,
                    //                             RiskRating = null,
                    //                             ControlRating=null
                    //                         }).ToList();

                }
                return riskcategorycreations;
            }
        }

        public static List<RiskExportinfoReportNEW> GetAllRiskCategoryCreationExportAssersationWise(int Branchid, int asserationid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<RiskExportinfoReportNEW> riskcategorycreations = new List<RiskExportinfoReportNEW>();
                if (asserationid != -1)
                {
                    riskcategorycreations = (from row in entities.RiskCategoryCreations
                                             join row1 in entities.AssertionsMappings
                                             on row.Id equals row1.RiskCategoryCreationId
                                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                                             && row1.AssertionId == asserationid
                                             select new RiskExportinfoReportNEW()
                                             {
                                                 RiskCreationId = row.Id,
                                                 ActivityDescription = row.ActivityDescription,
                                                 ControlObjective = row.ControlObjective,
                                                 RiskCategory = row.Id,
                                                 Industry = row.Id,
                                                 BranchId = Branchid,
                                                 LocationType = row.LocationType,
                                                 EffectiveDate = null,
                                                 ProcessId = row.ProcessId,
                                                 SubProcessId = row.SubProcessId,
                                                 Assertions = null,
                                                 ControlDescription = null,
                                                 MControlDescription = null,
                                                 PersonResponsible = null,
                                                 Key = null,
                                                 PreventiveControl = null,
                                                 AutomatedControl = null,
                                                 Frequency = null,
                                                 GapDescription = null,
                                                 Recommendations = null,
                                                 ActionRemediationplan = null,
                                                 IPE = null,
                                                 ERPsystem = null,
                                                 FRC = null,
                                                 UniqueReferred = null,
                                                 TestStrategy = null,
                                                 DocumentsExamined = null,
                                             }).ToList();

                }
                return riskcategorycreations;
            }
        }

        public static List<RiskExportinfoReportNEW> GetAllRiskCategoryCreationExportIndustryWise(int Branchid, int industryid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<RiskExportinfoReportNEW> riskcategorycreations = new List<RiskExportinfoReportNEW>();
                if (industryid != -1)
                {
                    riskcategorycreations = (from row in entities.RiskCategoryCreations
                                             join row1 in entities.IndustryMappings
                                             on row.Id equals row1.RiskCategoryCreationId
                                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                                             && row1.IndustryID == industryid
                                             select new RiskExportinfoReportNEW()
                                             {
                                                 RiskCreationId = row.Id,
                                                 ActivityDescription = row.ActivityDescription,
                                                 ControlObjective = row.ControlObjective,
                                                 RiskCategory = row.Id,
                                                 Industry = row.Id,
                                                 BranchId = Branchid,
                                                 LocationType = row.LocationType,
                                                 EffectiveDate = null,
                                                 ProcessId = row.ProcessId,
                                                 SubProcessId = row.SubProcessId,
                                                 Assertions = null,
                                                 ControlDescription = null,
                                                 MControlDescription = null,
                                                 PersonResponsible = null,
                                                 Key = null,
                                                 PreventiveControl = null,
                                                 AutomatedControl = null,
                                                 Frequency = null,
                                                 GapDescription = null,
                                                 Recommendations = null,
                                                 ActionRemediationplan = null,
                                                 IPE = null,
                                                 ERPsystem = null,
                                                 FRC = null,
                                                 UniqueReferred = null,
                                                 TestStrategy = null,
                                                 DocumentsExamined = null,
                                             }).ToList();

                }
                return riskcategorycreations;
            }
        }
        public static List<RiskExportinfoReportNEW> GetAllRiskCategoryCreationExportRiskCategoryWise(int Branchid, int riskcategoryid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<RiskExportinfoReportNEW> riskcategorycreations = new List<RiskExportinfoReportNEW>();
                if (riskcategoryid != -1)
                {
                    riskcategorycreations = (from row in entities.RiskCategoryCreations
                                             join row1 in entities.RiskCategoryMappings
                                             on row.Id equals row1.RiskCategoryCreationId
                                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                                             && row1.RiskCategoryId == riskcategoryid
                                             select new RiskExportinfoReportNEW()
                                             {
                                                 RiskCreationId = row.Id,
                                                 ActivityDescription = row.ActivityDescription,
                                                 ControlObjective = row.ControlObjective,
                                                 RiskCategory = row.Id,
                                                 Industry = row.Id,
                                                 BranchId = Branchid,
                                                 LocationType = row.LocationType,
                                                 EffectiveDate = null,
                                                 ProcessId = row.ProcessId,
                                                 SubProcessId = row.SubProcessId,
                                                 Assertions = null,
                                                 ControlDescription = null,
                                                 MControlDescription = null,
                                                 PersonResponsible = null,
                                                 Key = null,
                                                 PreventiveControl = null,
                                                 AutomatedControl = null,
                                                 Frequency = null,
                                                 GapDescription = null,
                                                 Recommendations = null,
                                                 ActionRemediationplan = null,
                                                 IPE = null,
                                                 ERPsystem = null,
                                                 FRC = null,
                                                 UniqueReferred = null,
                                                 TestStrategy = null,
                                                 DocumentsExamined = null,
                                             }).ToList();

                }
                return riskcategorycreations;
            }
        }

        public static List<RiskExportinfoReportNEW> GetAllRiskCategoryCreationExportProcessWise(int branchid, int processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<RiskExportinfoReportNEW> riskcategorycreations = new List<RiskExportinfoReportNEW>();
                if (processid != -1)
                {
                    riskcategorycreations = (from row in entities.RiskCategoryCreations
                                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                                             && row.ProcessId == processid
                                             select new RiskExportinfoReportNEW()
                                             {
                                                 ControlNo = row.ControlNo,
                                                 RiskCreationId = row.Id,
                                                 ActivityDescription = row.ActivityDescription,
                                                 ControlObjective = row.ControlObjective,
                                                 RiskCategory = row.Id,
                                                 Industry = row.Id,
                                                 BranchId = branchid,
                                                 LocationType = row.LocationType,
                                                 EffectiveDate = null,
                                                 ProcessId = row.ProcessId,
                                                 SubProcessId = row.SubProcessId,
                                                 Assertions = null,
                                                 ControlDescription = null,
                                                 MControlDescription = null,
                                                 PersonResponsible = null,
                                                 Email = null,
                                                 Key = null,
                                                 PreventiveControl = null,
                                                 AutomatedControl = null,
                                                 Frequency = null,
                                                 GapDescription = null,
                                                 Recommendations = null,
                                                 ActionRemediationplan = null,
                                                 IPE = null,
                                                 ERPsystem = null,
                                                 FRC = null,
                                                 UniqueReferred = null,
                                                 TestStrategy = null,
                                                 DocumentsExamined = null,
                                                 RiskRating = null,
                                                 ControlRating = null
                                             }).ToList();

                }
                return riskcategorycreations;
            }
        }
        public static List<ControlTestingAssignmentCheckExistView> GetAllControlTestingAssignmentExists(List<long> BranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ControlTestingAssignmentCheckExistView> riskassignmentexport = new List<ControlTestingAssignmentCheckExistView>();
                riskassignmentexport = (from row in entities.ControlTestingAssignmentCheckExistViews
                                        select row).GroupBy(entry => entry.RiskCreationId).Select(entry => entry.FirstOrDefault()).ToList();
                if (BranchID.Count > 0)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => BranchID.Contains((long)entry.CustomerBranchID)).ToList();
                }
                return riskassignmentexport;
            }
        }
        public static List<ControlTestingAssignmentView> GetAllControlTestingAssignment(int Customerid, List<long> BranchID, int ProcessId, int SubprocessId, string Flag, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ControlTestingAssignmentView> riskassignmentexport = new List<ControlTestingAssignmentView>();
                riskassignmentexport = (from row in entities.ControlTestingAssignmentViews
                                        where row.CustomerId == Customerid
                                        select row).ToList();

                riskassignmentexport = riskassignmentexport.GroupBy(entry => entry.RiskCategoryCreationId).Select(entry => entry.FirstOrDefault()).ToList();
                if (BranchID.Count > 0)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => BranchID.Contains((long)entry.CustomerBranchId)).ToList();
                }
                if (ProcessId != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.ProcessId == ProcessId).ToList();
                }
                if (SubprocessId != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                }

                //var riskassignment = (from AI in entities.AuditInstances
                //                      join ASO in entities.AuditScheduleOns
                //                      on AI.ID equals ASO.AuditInstanceId
                //                      where AI.RiskCreationId == ASO.RiskCreationID &&
                //                      BranchID.Contains((long) AI.CustomerBranchID)
                //                      && ASO.FinancialYear == FinancialYear
                //                      select AI.RiskCreationId).Distinct().ToList();
                //if (Flag == "E")
                //{
                //    riskassignmentexport = riskassignmentexport.Where(entry => riskassignment.Contains(entry.RiskCategoryCreationId)).ToList();
                //}
                //else
                //{
                //    //riskassignmentexport = riskassignmentexport.Where(entry => !riskassignment.Contains(entry.RiskCategoryCreationId)).ToList();
                //    riskassignmentexport = riskassignmentexport.Where(entry => riskassignment.Contains(entry.RiskCategoryCreationId)).ToList();
                //}
                return riskassignmentexport;
            }
        }

        public static List<SP_ControlTestingAssignmentAssignedView_Result> GetAllControlTestingAssignmentAssigned(int UserID, int Customerid, List<long> BranchID, int ProcessId, int SubprocessId, string Flag, string FinancialYear, string ForMonth)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_ControlTestingAssignmentAssignedView_Result> riskassignmentexport = new List<SP_ControlTestingAssignmentAssignedView_Result>();
                riskassignmentexport = (from row in entities.SP_ControlTestingAssignmentAssignedView(UserID, ForMonth, FinancialYear)
                                        select row).ToList();

                riskassignmentexport = riskassignmentexport.GroupBy(entry => entry.RiskCategoryCreationId).Select(entry => entry.FirstOrDefault()).ToList();
                if (BranchID.Count > 0)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => BranchID.Contains((long)entry.CustomerBranchId)).ToList();
                }
                if (ProcessId != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.ProcessId == ProcessId).ToList();
                }
                if (SubprocessId != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                }

                return riskassignmentexport;
            }
        }
        public static List<SP_ControlTestingAssignmentView_Result> GetAllControlTestingAssignment(int UserID, int Customerid, List<long> BranchID, int ProcessId, int SubprocessId, string Flag, string FinancialYear, string ForMonth)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_ControlTestingAssignmentView_Result> riskassignmentexport = new List<SP_ControlTestingAssignmentView_Result>();
                riskassignmentexport = (from row in entities.SP_ControlTestingAssignmentView(UserID, ForMonth, FinancialYear)
                                        select row).ToList();

                riskassignmentexport = riskassignmentexport.GroupBy(entry => entry.RiskCategoryCreationId).Select(entry => entry.FirstOrDefault()).ToList();
                if (BranchID.Count > 0)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => BranchID.Contains((long)entry.CustomerBranchId)).ToList();
                }
                if (ProcessId != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.ProcessId == ProcessId).ToList();
                }
                if (SubprocessId != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                }

                return riskassignmentexport;
            }
        }
        public static List<RiskAssignmentExport> GetAllRiskActivityTransactionAssignment(int ProcessId, int SubprocessId, int BranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<RiskAssignmentExport> riskassignmentexport = new List<RiskAssignmentExport>();
                if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1)
                {
                    riskassignmentexport = (from row in entities.RiskCategoryCreations
                                            join row1 in entities.RiskActivityTransactions
                                            on row.Id equals row1.RiskCreationId
                                            where row.ProcessId == ProcessId && row.SubProcessId == SubprocessId
                                            && row1.CustomerBranchId == BranchID && row.IsInternalAudit == "N"
                                            select new RiskAssignmentExport()
                                            {
                                                ID = row.Id,
                                                RiskCategoryCreationId = row.Id,
                                                ActivityDescription = row.ActivityDescription,
                                                ControlObjective = row.ControlObjective,
                                                ControlDescription = row1.ControlDescription,
                                                MitigatingControl = row1.MControlDescription,
                                            }).Distinct().ToList();
                }
                else if (ProcessId != -1 && BranchID != -1)
                {
                    riskassignmentexport = (from row in entities.RiskCategoryCreations
                                            join row1 in entities.RiskActivityTransactions
                                            on row.Id equals row1.RiskCreationId
                                            where row.ProcessId == ProcessId && row1.CustomerBranchId == BranchID && row.IsInternalAudit == "N"
                                            select new RiskAssignmentExport()
                                            {
                                                ID = row.Id,
                                                RiskCategoryCreationId = row.Id,
                                                ActivityDescription = row.ActivityDescription,
                                                ControlObjective = row.ControlObjective,
                                                ControlDescription = row1.ControlDescription,
                                                MitigatingControl = row1.MControlDescription,


                                            }).Distinct().ToList();


                }
                else if (SubprocessId != -1 && BranchID != -1)
                {
                    riskassignmentexport = (from row in entities.RiskCategoryCreations
                                            join row1 in entities.RiskActivityTransactions
                                            on row.Id equals row1.RiskCreationId
                                            where row.SubProcessId == SubprocessId && row1.CustomerBranchId == BranchID && row.IsInternalAudit == "N"
                                            select new RiskAssignmentExport()
                                            {
                                                ID = row.Id,
                                                RiskCategoryCreationId = row.Id,
                                                ActivityDescription = row.ActivityDescription,
                                                ControlObjective = row.ControlObjective,
                                                ControlDescription = row1.ControlDescription,
                                                MitigatingControl = row1.MControlDescription,


                                            }).Distinct().ToList();



                }

                return riskassignmentexport;
            }
        }
        public static List<RiskAuditExportDownload> GetAllRiskActivityTransactionTestingUI(int ProcessId, int SubprocessId, int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<RiskAuditExportDownload> riskassignmentexport = new List<RiskAuditExportDownload>();
                if (ProcessId != -1 && SubprocessId != -1 && Branchid != -1)
                {
                    riskassignmentexport = (from RCC in entities.RiskCategoryCreations
                                            join RAT in entities.RiskActivityTransactions
                                            on RCC.Id equals RAT.RiskCreationId
                                            join mk in entities.mst_Key on RAT.Key_Value equals mk.Id
                                            into MSK
                                            from MSKS in MSK.DefaultIfEmpty()
                                            join FDRS in entities.FileData_Risk
                                            on RCC.Id equals FDRS.RiskCreationId into FDR
                                            from FDRI in FDR.DefaultIfEmpty()
                                            where RCC.ProcessId == ProcessId && RCC.SubProcessId == SubprocessId && RCC.IsInternalAudit == "N"
                                            && RAT.CustomerBranchId == Branchid
                                            select new RiskAuditExportDownload()
                                            {
                                                ID = RCC.Id,
                                                RiskCategoryCreationId = RCC.Id,
                                                ActivityDescription = RCC.ActivityDescription,
                                                ControlObjective = RCC.ControlObjective,
                                                SampleDownloadFileName = FDRI.Name,
                                                TestingDownloadFileName = FDRI.TestUploadName,
                                                KeyName = MSKS.Name,
                                                ProcessID = RCC.ProcessId,
                                                SubProcessID = RCC.SubProcessId,
                                            }).ToList();
                }
                else if (ProcessId != -1 && Branchid != -1)
                {
                    riskassignmentexport = (from RCC in entities.RiskCategoryCreations
                                            join RAT in entities.RiskActivityTransactions
                                            on RCC.Id equals RAT.RiskCreationId
                                            join mk in entities.mst_Key on RAT.Key_Value equals mk.Id
                                            into MSK
                                            from MSKS in MSK.DefaultIfEmpty()
                                            join FDRS in entities.FileData_Risk
                                            on RCC.Id equals FDRS.RiskCreationId into FDR
                                            from FDRI in FDR.DefaultIfEmpty()
                                            where RCC.ProcessId == ProcessId && RAT.CustomerBranchId == Branchid && RCC.IsInternalAudit == "N"
                                            select new RiskAuditExportDownload()
                                            {
                                                ID = RCC.Id,
                                                RiskCategoryCreationId = RCC.Id,
                                                ActivityDescription = RCC.ActivityDescription,
                                                ControlObjective = RCC.ControlObjective,
                                                SampleDownloadFileName = FDRI.Name,
                                                TestingDownloadFileName = FDRI.TestUploadName,
                                                KeyName = MSKS.Name,
                                                ProcessID = RCC.ProcessId,
                                                SubProcessID = RCC.SubProcessId,
                                            }).ToList();
                }
                else if (SubprocessId != -1 && Branchid != -1)
                {
                    riskassignmentexport = (from RCC in entities.RiskCategoryCreations
                                            join RAT in entities.RiskActivityTransactions
                                            on RCC.Id equals RAT.RiskCreationId
                                            join mk in entities.mst_Key on RAT.Key_Value equals mk.Id
                                            into MSK
                                            from MSKS in MSK.DefaultIfEmpty()
                                            join FDRS in entities.FileData_Risk
                                            on RCC.Id equals FDRS.RiskCreationId into FDR
                                            from FDRI in FDR.DefaultIfEmpty()
                                            where RCC.SubProcessId == SubprocessId && RAT.CustomerBranchId == Branchid && RCC.IsInternalAudit == "N"
                                            select new RiskAuditExportDownload()
                                            {
                                                ID = RCC.Id,
                                                RiskCategoryCreationId = RCC.Id,
                                                ActivityDescription = RCC.ActivityDescription,
                                                ControlObjective = RCC.ControlObjective,
                                                SampleDownloadFileName = FDRI.Name,
                                                TestingDownloadFileName = FDRI.TestUploadName,
                                                KeyName = MSKS.Name,
                                                ProcessID = RCC.ProcessId,
                                                SubProcessID = RCC.SubProcessId,
                                            }).ToList();

                }
                return riskassignmentexport;
            }
        }
        public static List<RiskAuditExportDownload> GetAllRiskActivityTransactionDownload(int ProcessId, int SubprocessId, int BranchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<RiskAuditExportDownload> riskassignmentexport = new List<RiskAuditExportDownload>();
                if (ProcessId != -1 && SubprocessId != -1 && BranchId != -1)
                {
                    riskassignmentexport = (from row in entities.RiskCategoryCreations
                                            join row1 in entities.FileData_Risk
                                            on row.Id equals row1.RiskCreationId
                                            into FDR
                                            from FDRI in FDR.DefaultIfEmpty()
                                            where row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.IsInternalAudit == "N"
                                            && FDRI.CustomerBranchId == BranchId
                                            select new RiskAuditExportDownload()
                                            {
                                                ID = row.Id,
                                                RiskCategoryCreationId = row.Id,
                                                ActivityDescription = row.ActivityDescription,
                                                ControlObjective = row.ControlObjective,
                                                SampleDownloadFileName = FDRI.Name,
                                                ProcessID = row.ProcessId,
                                                SubProcessID = row.SubProcessId,
                                            }).ToList();
                }
                else if (ProcessId != -1 && BranchId != -1)
                {
                    riskassignmentexport = (from row in entities.RiskCategoryCreations
                                            join row1 in entities.FileData_Risk
                                            on row.Id equals row1.RiskCreationId
                                            into FDR
                                            from FDRI in FDR.DefaultIfEmpty()
                                            where row.ProcessId == ProcessId && FDRI.CustomerBranchId == BranchId && row.IsInternalAudit == "N"
                                            select new RiskAuditExportDownload()
                                            {
                                                ID = row.Id,
                                                RiskCategoryCreationId = row.Id,
                                                ActivityDescription = row.ActivityDescription,
                                                ControlObjective = row.ControlObjective,
                                                SampleDownloadFileName = FDRI.Name,
                                                ProcessID = row.ProcessId,
                                                SubProcessID = row.SubProcessId,
                                            }).ToList();
                }
                else if (SubprocessId != -1 && BranchId != -1)
                {
                    riskassignmentexport = (from row in entities.RiskCategoryCreations
                                            join row1 in entities.FileData_Risk
                                            on row.Id equals row1.RiskCreationId
                                            into FDR
                                            from FDRI in FDR.DefaultIfEmpty()
                                            where row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && FDRI.CustomerBranchId == BranchId && row.IsInternalAudit == "N"
                                            select new RiskAuditExportDownload()
                                            {
                                                ID = row.Id,
                                                RiskCategoryCreationId = row.Id,
                                                ActivityDescription = row.ActivityDescription,
                                                ControlObjective = row.ControlObjective,
                                                SampleDownloadFileName = FDRI.Name,
                                                ProcessID = row.ProcessId,
                                                SubProcessID = row.SubProcessId,
                                            }).ToList();

                }
                return riskassignmentexport;
            }
        }
        public static List<RiskCategoryCreation> GetAllRiskCategoryCreation(int ProcessId, int SubprocessId, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {


                var riskcategorycreations = (from row in entities.RiskCategoryCreations
                                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                                             select row);

                if (ProcessId != -1 && SubprocessId != -1)
                {
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId && entry.SubProcessId == SubprocessId);
                }
                else if (ProcessId != -1)
                {
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId);
                }
                else if (SubprocessId != -1)
                {
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.ActivityDescription.Contains(filter) || entry.ControlObjective.Contains(filter));
                }

                return riskcategorycreations.OrderBy(entry => entry.ActivityDescription).ToList();
                //return users.ToList();
            }
        }

        //int ProcessId, int SubprocessId, int BranchID, int LocationType, int RiskCategory, int Industry,int customerID,string FlagProcessNonProcess, com.VirtuosoITech.ComplianceManagement.Business.Data.ReiskRegisterFilterForInternalControl filter

        public static List<FilterRiskControlMatrix> GetAllRiskControlMatrix(int ProcessId, int SubprocessId, int BranchID, int LocationType, int RiskCategory, int Industry, int customerID, string FlagProcessNonProcess, string filter)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<FilterRiskControlMatrix> riskcategorycreations = new List<FilterRiskControlMatrix>();
                if (FlagProcessNonProcess == "P")
                {
                    if (filter == "All")
                    {
                        #region All
                        riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                 where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                 && row.CustomerId == customerID && row.IsProcessNonProcess == "P"
                                                 select row).ToList();
                        #endregion
                    }
                    else if (filter == "Location")
                    {
                        #region Customer Location Wise
                        if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1 && SubprocessId != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        else if (SubprocessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                      && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        else if (BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                      && row.ProcessId == ProcessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        #endregion
                    }
                    else if (filter == "Process")
                    {
                        #region Process

                        if (ProcessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                      && row.ProcessId == ProcessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        #endregion
                    }
                    else if (filter == "SubProcess")
                    {
                        #region SubProcess
                        if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1 && SubprocessId != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();


                        }
                        else if (ProcessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        else if (SubprocessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();


                        }
                        else if (BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        else if (SubprocessId != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "P"
                                                     select row).ToList();
                        }
                        #endregion
                    }
                    else if (filter == "RiskCategory")
                    {
                        #region RiskCategory
                        //if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && BranchID != -1 && RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (SubprocessId != -1 && BranchID != -1 && RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P"
                        //                             select row).ToList();

                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && RiskCategory != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();


                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "P"
                        //                             select row).ToList();
                        //}
                        //else if (BranchID != -1 && ProcessId != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P"
                        //                             select row).ToList();
                        //}
                        //else if (BranchID != -1 && SubprocessId != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P"
                        //                             select row).ToList();
                        //}
                        //else if (BranchID != -1 && RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        #endregion
                    }
                    else if (filter == "Industry")
                    {
                        #region Industry
                        //if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && RiskCategory != -1 && Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && RiskCategory != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();


                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P" && row.IndustryID == Industry
                        //                             select row).ToList();


                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && RiskCategory != -1 && Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && BranchID != -1 && RiskCategory != -1 && Industry != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (SubprocessId != -1 && BranchID != -1 && RiskCategory != -1 && Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P"
                        //                             select row).ToList();

                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "P" && row.IndustryID == Industry
                        //                             select row).ToList();

                        //}
                        //else if (ProcessId != -1 && BranchID != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P"
                        //                             select row).ToList();
                        //}
                        //else if (SubprocessId != -1 && BranchID != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "P"
                        //                             select row).ToList();

                        //}
                        //else if (ProcessId != -1 && RiskCategory != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "P" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && Industry != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "P" && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (SubprocessId != -1 && Industry != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "P" && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "P" && row.IndustryID == Industry
                        //                             select row).ToList();

                        //}
                        #endregion
                    }
                    else if (filter == "LocationType")
                    {
                        #region LocationType
                        if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P" && row.LocationType == LocationType
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1 && BranchID != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P" && row.LocationType == LocationType
                                                     select row).ToList();

                        }
                        else if (ProcessId != -1 && SubprocessId != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "P" && row.LocationType == LocationType
                                                     select row).ToList();


                        }
                        else if (BranchID != -1 && SubprocessId != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P" && row.LocationType == LocationType
                                                     select row).ToList();
                        }
                        else if (BranchID != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                      && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "P" && row.LocationType == LocationType
                                                     select row).ToList();

                        }
                        else if (SubprocessId != -1 && LocationType != -1)
                        {

                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "P" && row.LocationType == LocationType
                                                     select row).ToList();


                        }
                        else if (ProcessId != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "P" && row.LocationType == LocationType
                                                     select row).ToList();
                        }
                        else if (LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "P" && row.LocationType == LocationType
                                                     select row).ToList();
                        }
                        #endregion
                    }

                }
                else//Non Process
                {
                    if (filter == "All")
                    {
                        #region All
                        riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                 where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                 && row.CustomerId == customerID && row.IsProcessNonProcess == "N"
                                                 select row).ToList();
                        #endregion
                    }
                    else if (filter == "Location")
                    {
                        #region Customer Location Wise
                        if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1 && SubprocessId != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        else if (SubprocessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                      && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        else if (BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                      && row.ProcessId == ProcessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        #endregion
                    }
                    else if (filter == "Process")
                    {
                        #region Process

                        if (ProcessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                      && row.ProcessId == ProcessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        #endregion
                    }
                    else if (filter == "SubProcess")
                    {
                        #region SubProcess
                        if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1 && SubprocessId != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();


                        }
                        else if (ProcessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        else if (SubprocessId != -1 && BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();


                        }
                        else if (BranchID != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        else if (SubprocessId != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "N"
                                                     select row).ToList();
                        }
                        #endregion
                    }
                    else if (filter == "RiskCategory")
                    {
                        #region RiskCategory
                        //if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && BranchID != -1 && RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (SubprocessId != -1 && BranchID != -1 && RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N"
                        //                             select row).ToList();

                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && RiskCategory != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();


                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "N"
                        //                             select row).ToList();
                        //}
                        //else if (BranchID != -1 && ProcessId != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N"
                        //                             select row).ToList();
                        //}
                        //else if (BranchID != -1 && SubprocessId != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N"
                        //                             select row).ToList();
                        //}
                        //else if (BranchID != -1 && RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        #endregion
                    }
                    else if (filter == "Industry")
                    {
                        #region Industry
                        //if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && RiskCategory != -1 && Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && RiskCategory != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();


                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N" && row.IndustryID == Industry
                        //                             select row).ToList();


                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && RiskCategory != -1 && Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && BranchID != -1 && RiskCategory != -1 && Industry != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (SubprocessId != -1 && BranchID != -1 && RiskCategory != -1 && Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N"
                        //                             select row).ToList();

                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && RiskCategory != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && SubprocessId != -1 && Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "N" && row.IndustryID == Industry
                        //                             select row).ToList();

                        //}
                        //else if (ProcessId != -1 && BranchID != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N"
                        //                             select row).ToList();
                        //}
                        //else if (SubprocessId != -1 && BranchID != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                        //                             && row.IsProcessNonProcess == "N"
                        //                             select row).ToList();

                        //}
                        //else if (ProcessId != -1 && RiskCategory != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.ProcessId == ProcessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "N" && row.RiskCategoryId == RiskCategory
                        //                             select row).ToList();
                        //}
                        //else if (ProcessId != -1 && Industry != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "N" && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (SubprocessId != -1 && Industry != -1)
                        //{

                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "N" && row.IndustryID == Industry
                        //                             select row).ToList();
                        //}
                        //else if (Industry != -1)
                        //{
                        //    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                        //                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                        //                             && row.CustomerId == customerID
                        //                             && row.IsProcessNonProcess == "N" && row.IndustryID == Industry
                        //                             select row).ToList();

                        //}
                        #endregion
                    }
                    else if (filter == "LocationType")
                    {
                        #region LocationType
                        if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N" && row.LocationType == LocationType
                                                     select row).ToList();
                        }
                        else if (ProcessId != -1 && BranchID != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N" && row.LocationType == LocationType
                                                     select row).ToList();

                        }
                        else if (ProcessId != -1 && SubprocessId != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "N" && row.LocationType == LocationType
                                                     select row).ToList();


                        }
                        else if (BranchID != -1 && SubprocessId != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.SubProcessId == SubprocessId && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N" && row.LocationType == LocationType
                                                     select row).ToList();
                        }
                        else if (BranchID != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                      && row.BranchId == BranchID
                                                     && row.IsProcessNonProcess == "N" && row.LocationType == LocationType
                                                     select row).ToList();

                        }
                        else if (SubprocessId != -1 && LocationType != -1)
                        {

                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.SubProcessId == SubprocessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "N" && row.LocationType == LocationType
                                                     select row).ToList();


                        }
                        else if (ProcessId != -1 && LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.ProcessId == ProcessId && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "N" && row.LocationType == LocationType
                                                     select row).ToList();
                        }
                        else if (LocationType != -1)
                        {
                            riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                                     where row.IsDeleted == false && row.IsInternalAudit == "N"
                                                     && row.CustomerId == customerID
                                                     && row.IsProcessNonProcess == "N" && row.LocationType == LocationType
                                                     select row).ToList();
                        }
                        #endregion
                    }
                }
                #region Previous Code
                //                if (FlagProcessNonProcess == "P")
                //                {
                //                    #region Process
                //                    if (BranchID != -1)
                //                    {
                //                        if (ProcessId != -1 && SubprocessId != -1)
                //                        {
                //                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                //                                                     join row1 in entities.CustomerBranchMappings
                //                                                     on row.Id equals row1.RiskCategoryCreationId
                //                                                     join row2 in entities.RiskActivityTransactions
                //                                                     on row.Id equals row2.RiskCreationId
                //                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                //                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "P" && row.IsInternalAudit == "N"
                //                                                     && row1.BranchId == BranchID && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId
                //                                                     select new RiskExportinfoReport()
                //                                                     {
                //                                                         ID = row2.Id,
                //                                                         RiskCreationId = row.Id,
                //                                                         ActivityDescription = row.ActivityDescription,
                //                                                         ControlObjective = row.ControlObjective,
                //                                                         RiskCategory = row.Id,
                //                                                         ActivityTobeDone = row.ActivityToBeDone,
                //                                                         RiskRating = (row.Rating).ToString(),
                //                                                         Industry = row.Id,
                //                                                         BranchId = row1.BranchId,
                //                                                         LocationType = row.LocationType,
                //                                                         EffectiveDate = row2.EffectiveDate,
                //                                                         ProcessId = row.ProcessId,
                //                                                         SubProcessId = row.SubProcessId,
                //                                                         Assertions = null,
                //                                                         ControlDescription = row2.ControlDescription,
                //                                                         MControlDescription = row2.MControlDescription,
                //                                                         PersonResponsible = row2.PersonResponsible,
                //                                                         Key = row2.Key_Value,
                //                                                         PreventiveControl = row2.PrevationControl,
                //                                                         AutomatedControl = row2.AutomatedControl,
                //                                                         Frequency = row2.Frequency,
                //                                                         GapDescription = row2.GapDescription,
                //                                                         Recommendations = row2.Recommendations,
                //                                                         ActionRemediationplan = row2.ActionRemediationplan,
                //                                                         IPE = row2.IPE,
                //                                                         ERPsystem = row2.ERPsystem,
                //                                                         FRC = row2.FRC,
                //                                                         UniqueReferred = row2.UniqueReferred,
                //                                                         TestStrategy = row2.TestStrategy,
                //                                                         DocumentsExamined = row2.DocumentsExamined,
                //                                                     }).ToList();
                //                        }
                //                        else if (ProcessId != -1)
                //                        {
                //                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                //                                                     join row1 in entities.CustomerBranchMappings
                //                                                     on row.Id equals row1.RiskCategoryCreationId
                //                                                     join row2 in entities.RiskActivityTransactions
                //                                                     on row.Id equals row2.RiskCreationId
                //                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                //                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "P" && row.IsInternalAudit == "N"
                //                                                     && row1.BranchId == BranchID && row.ProcessId == ProcessId
                //                                                     select new RiskExportinfoReport()
                //                                                     {
                //                                                         ID = row2.Id,
                //                                                         RiskCreationId = row.Id,
                //                                                         ActivityDescription = row.ActivityDescription,
                //                                                         ControlObjective = row.ControlObjective,
                //                                                         RiskCategory = row.Id,
                //                                                         ActivityTobeDone = row.ActivityToBeDone,
                //                                                         RiskRating = (row.Rating).ToString(),
                //                                                         Industry = row.Id,
                //                                                         BranchId = row1.BranchId,
                //                                                         LocationType = row.LocationType,
                //                                                         EffectiveDate = row2.EffectiveDate,
                //                                                         ProcessId = row.ProcessId,
                //                                                         SubProcessId = row.SubProcessId,
                //                                                         Assertions = null,
                //                                                         ControlDescription = row2.ControlDescription,
                //                                                         MControlDescription = row2.MControlDescription,
                //                                                         PersonResponsible = row2.PersonResponsible,
                //                                                         Key = row2.Key_Value,
                //                                                         PreventiveControl = row2.PrevationControl,
                //                                                         AutomatedControl = row2.AutomatedControl,
                //                                                         Frequency = row2.Frequency,
                //                                                         GapDescription = row2.GapDescription,
                //                                                         Recommendations = row2.Recommendations,
                //                                                         ActionRemediationplan = row2.ActionRemediationplan,
                //                                                         IPE = row2.IPE,
                //                                                         ERPsystem = row2.ERPsystem,
                //                                                         FRC = row2.FRC,
                //                                                         UniqueReferred = row2.UniqueReferred,
                //                                                         TestStrategy = row2.TestStrategy,
                //                                                         DocumentsExamined = row2.DocumentsExamined,
                //                                                     }).ToList();
                //                        }
                //                        else if (SubprocessId != -1)
                //                        {
                //                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                //                                                     join row1 in entities.CustomerBranchMappings
                //                                                     on row.Id equals row1.RiskCategoryCreationId
                //                                                     join row2 in entities.RiskActivityTransactions
                //                                                     on row.Id equals row2.RiskCreationId
                //                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                //                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "P" && row.IsInternalAudit == "N"
                //                                                     && row1.BranchId == BranchID && row.SubProcessId == SubprocessId
                //                                                     select new RiskExportinfoReport()
                //                                                     {
                //                                                         ID = row2.Id,
                //                                                         RiskCreationId = row.Id,
                //                                                         ActivityDescription = row.ActivityDescription,
                //                                                         ControlObjective = row.ControlObjective,
                //                                                         RiskCategory = row.Id,
                //                                                         ActivityTobeDone = row.ActivityToBeDone,
                //                                                         RiskRating = (row.Rating).ToString(),
                //                                                         Industry = row.Id,
                //                                                         BranchId = row1.BranchId,
                //                                                         LocationType = row.LocationType,
                //                                                         EffectiveDate = row2.EffectiveDate,
                //                                                         ProcessId = row.ProcessId,
                //                                                         SubProcessId = row.SubProcessId,
                //                                                         Assertions = null,
                //                                                         ControlDescription = row2.ControlDescription,
                //                                                         MControlDescription = row2.MControlDescription,
                //                                                         PersonResponsible = row2.PersonResponsible,
                //                                                         Key = row2.Key_Value,
                //                                                         PreventiveControl = row2.PrevationControl,
                //                                                         AutomatedControl = row2.AutomatedControl,
                //                                                         Frequency = row2.Frequency,
                //                                                         GapDescription = row2.GapDescription,
                //                                                         Recommendations = row2.Recommendations,
                //                                                         ActionRemediationplan = row2.ActionRemediationplan,
                //                                                         IPE = row2.IPE,
                //                                                         ERPsystem = row2.ERPsystem,
                //                                                         FRC = row2.FRC,
                //                                                         UniqueReferred = row2.UniqueReferred,
                //                                                         TestStrategy = row2.TestStrategy,
                //                                                         DocumentsExamined = row2.DocumentsExamined,
                //                                                     }).ToList();
                //                        }
                //                        else if (ProcessId == -1 && SubprocessId == -1)
                //                        {
                //                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                //                                                     join row1 in entities.CustomerBranchMappings
                //                                                     on row.Id equals row1.RiskCategoryCreationId
                //                                                     join row2 in entities.RiskActivityTransactions
                //                                                     on row.Id equals row2.RiskCreationId
                //                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                //                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "P" && row.IsInternalAudit == "N"
                //                                                     && row1.BranchId == BranchID
                //                                                     select new RiskExportinfoReport()
                //                                                     {
                //                                                         ID = row2.Id,
                //                                                         RiskCreationId = row.Id,
                //                                                         ActivityDescription = row.ActivityDescription,
                //                                                         ControlObjective = row.ControlObjective,
                //                                                         RiskCategory = row.Id,
                //                                                         ActivityTobeDone = row.ActivityToBeDone,
                //                                                         RiskRating = (row.Rating).ToString(),
                //                                                         Industry = row.Id,
                //                                                         BranchId = row1.BranchId,
                //                                                         LocationType = row.LocationType,
                //                                                         EffectiveDate = row2.EffectiveDate,
                //                                                         ProcessId = row.ProcessId,
                //                                                         SubProcessId = row.SubProcessId,
                //                                                         Assertions = null,
                //                                                         ControlDescription = row2.ControlDescription,
                //                                                         MControlDescription = row2.MControlDescription,
                //                                                         PersonResponsible = row2.PersonResponsible,
                //                                                         Key = row2.Key_Value,
                //                                                         PreventiveControl = row2.PrevationControl,
                //                                                         AutomatedControl = row2.AutomatedControl,
                //                                                         Frequency = row2.Frequency,
                //                                                         GapDescription = row2.GapDescription,
                //                                                         Recommendations = row2.Recommendations,
                //                                                         ActionRemediationplan = row2.ActionRemediationplan,
                //                                                         IPE = row2.IPE,
                //                                                         ERPsystem = row2.ERPsystem,
                //                                                         FRC = row2.FRC,
                //                                                         UniqueReferred = row2.UniqueReferred,
                //                                                         TestStrategy = row2.TestStrategy,
                //                                                         DocumentsExamined = row2.DocumentsExamined,
                //                                                     }).ToList();
                //                        }
                //                    }
                //                    #endregion
                //                }
                //                else
                //                {
                //                    #region NonProcess
                //                    if (BranchID != -1)
                //                    {
                //                        if (ProcessId != -1 && SubprocessId != -1)
                //                        {
                //                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                //                                                     join row1 in entities.CustomerBranchMappings
                //                                                     on row.Id equals row1.RiskCategoryCreationId
                //                                                     join row2 in entities.RiskActivityTransactions
                //                                                     on row.Id equals row2.RiskCreationId
                //                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                //                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "N" && row.IsInternalAudit == "N"
                //                                                     && row1.BranchId == BranchID && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId
                //                                                     select new RiskExportinfoReport()
                //                                                     {
                //                                                         ControlNo = row.ControlNo,
                //                                                         ID = row2.Id,
                //                                                         RiskCreationId = row.Id,
                //                                                         ActivityDescription = row.ActivityDescription,
                //                                                         ControlObjective = row.ControlObjective,
                //                                                         RiskCategory = row.Id,
                //                                                         ActivityTobeDone = row.ActivityToBeDone,
                //                                                         RiskRating = (row.Rating).ToString(),
                //                                                         Industry = row.Id,
                //                                                         BranchId = row1.BranchId,
                //                                                         LocationType = row.LocationType,
                //                                                         EffectiveDate = row2.EffectiveDate,
                //                                                         ProcessId = row.ProcessId,
                //                                                         SubProcessId = row.SubProcessId,
                //                                                         Assertions = null,
                //                                                         ControlDescription = row2.ControlDescription,
                //                                                         MControlDescription = row2.MControlDescription,
                //                                                         PersonResponsible = row2.PersonResponsible,
                //                                                         Key = row2.Key_Value,
                //                                                         PreventiveControl = row2.PrevationControl,
                //                                                         AutomatedControl = row2.AutomatedControl,
                //                                                         Frequency = row2.Frequency,
                //                                                         GapDescription = row2.GapDescription,
                //                                                         Recommendations = row2.Recommendations,
                //                                                         ActionRemediationplan = row2.ActionRemediationplan,
                //                                                         IPE = row2.IPE,
                //                                                         ERPsystem = row2.ERPsystem,
                //                                                         FRC = row2.FRC,
                //                                                         UniqueReferred = row2.UniqueReferred,
                //                                                         TestStrategy = row2.TestStrategy,
                //                                                         DocumentsExamined = row2.DocumentsExamined,
                //                                                     }).ToList();
                //                        }
                //                        else if (ProcessId != -1)
                //                        {
                //                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                //                                                     join row1 in entities.CustomerBranchMappings
                //                                                     on row.Id equals row1.RiskCategoryCreationId
                //                                                     join row2 in entities.RiskActivityTransactions
                //                                                     on row.Id equals row2.RiskCreationId
                //                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                //                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "N" && row.IsInternalAudit == "N"
                //                                                     && row1.BranchId == BranchID && row.ProcessId == ProcessId
                //                                                     select new RiskExportinfoReport()
                //                                                     {
                //                                                         ControlNo = row.ControlNo,
                //                                                         ID = row2.Id,
                //                                                         RiskCreationId = row.Id,
                //                                                         ActivityDescription = row.ActivityDescription,
                //                                                         ControlObjective = row.ControlObjective,
                //                                                         RiskCategory = row.Id,
                //                                                         ActivityTobeDone = row.ActivityToBeDone,
                //                                                         RiskRating = (row.Rating).ToString(),
                //                                                         Industry = row.Id,
                //                                                         BranchId = row1.BranchId,
                //                                                         LocationType = row.LocationType,
                //                                                         EffectiveDate = row2.EffectiveDate,
                //                                                         ProcessId = row.ProcessId,
                //                                                         SubProcessId = row.SubProcessId,
                //                                                         Assertions = null,
                //                                                         ControlDescription = row2.ControlDescription,
                //                                                         MControlDescription = row2.MControlDescription,
                //                                                         PersonResponsible = row2.PersonResponsible,
                //                                                         Key = row2.Key_Value,
                //                                                         PreventiveControl = row2.PrevationControl,
                //                                                         AutomatedControl = row2.AutomatedControl,
                //                                                         Frequency = row2.Frequency,
                //                                                         GapDescription = row2.GapDescription,
                //                                                         Recommendations = row2.Recommendations,
                //                                                         ActionRemediationplan = row2.ActionRemediationplan,
                //                                                         IPE = row2.IPE,
                //                                                         ERPsystem = row2.ERPsystem,
                //                                                         FRC = row2.FRC,
                //                                                         UniqueReferred = row2.UniqueReferred,
                //                                                         TestStrategy = row2.TestStrategy,
                //                                                         DocumentsExamined = row2.DocumentsExamined,
                //                                                     }).ToList();
                //                        }
                //                        else if (SubprocessId != -1)
                //                        {
                //                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                //                                                     join row1 in entities.CustomerBranchMappings
                //                                                     on row.Id equals row1.RiskCategoryCreationId
                //                                                     join row2 in entities.RiskActivityTransactions
                //                                                     on row.Id equals row2.RiskCreationId
                //                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                //                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "N" && row.IsInternalAudit == "N"
                //                                                     && row1.BranchId == BranchID && row.SubProcessId == SubprocessId
                //                                                     select new RiskExportinfoReport()
                //                                                     {
                //                                                         ControlNo = row.ControlNo,
                //                                                         ID = row2.Id,
                //                                                         RiskCreationId = row.Id,
                //                                                         ActivityDescription = row.ActivityDescription,
                //                                                         ControlObjective = row.ControlObjective,
                //                                                         RiskCategory = row.Id,
                //                                                         ActivityTobeDone = row.ActivityToBeDone,
                //                                                         RiskRating = (row.Rating).ToString(),
                //                                                         Industry = row.Id,
                //                                                         BranchId = row1.BranchId,
                //                                                         LocationType = row.LocationType,
                //                                                         EffectiveDate = row2.EffectiveDate,
                //                                                         ProcessId = row.ProcessId,
                //                                                         SubProcessId = row.SubProcessId,
                //                                                         Assertions = null,
                //                                                         ControlDescription = row2.ControlDescription,
                //                                                         MControlDescription = row2.MControlDescription,
                //                                                         PersonResponsible = row2.PersonResponsible,
                //                                                         Key = row2.Key_Value,
                //                                                         PreventiveControl = row2.PrevationControl,
                //                                                         AutomatedControl = row2.AutomatedControl,
                //                                                         Frequency = row2.Frequency,
                //                                                         GapDescription = row2.GapDescription,
                //                                                         Recommendations = row2.Recommendations,
                //                                                         ActionRemediationplan = row2.ActionRemediationplan,
                //                                                         IPE = row2.IPE,
                //                                                         ERPsystem = row2.ERPsystem,
                //                                                         FRC = row2.FRC,
                //                                                         UniqueReferred = row2.UniqueReferred,
                //                                                         TestStrategy = row2.TestStrategy,
                //                                                         DocumentsExamined = row2.DocumentsExamined,
                //                                                     }).ToList();
                //                        }
                //                        else if (ProcessId == -1 && SubprocessId == -1)
                //                        {
                //                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                //                                                     join row1 in entities.CustomerBranchMappings
                //                                                     on row.Id equals row1.RiskCategoryCreationId
                //                                                     join row2 in entities.RiskActivityTransactions
                //                                                     on row.Id equals row2.RiskCreationId
                //                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                //                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "N" && row.IsInternalAudit == "N"
                //                                                     && row1.BranchId == BranchID
                //                                                     select new RiskExportinfoReport()
                //                                                     {
                //                                                         ControlNo = row.ControlNo,
                //                                                         ID = row2.Id,
                //                                                         RiskCreationId = row.Id,
                //                                                         ActivityDescription = row.ActivityDescription,
                //                                                         ControlObjective = row.ControlObjective,
                //                                                         RiskCategory = row.Id,
                //                                                         ActivityTobeDone = row.ActivityToBeDone,
                //                                                         RiskRating = (row.Rating).ToString(),
                //                                                         Industry = row.Id,
                //                                                         BranchId = row1.BranchId,
                //                                                         LocationType = row.LocationType,
                //                                                         EffectiveDate = row2.EffectiveDate,
                //                                                         ProcessId = row.ProcessId,
                //                                                         SubProcessId = row.SubProcessId,
                //                                                         Assertions = null,
                //                                                         ControlDescription = row2.ControlDescription,
                //                                                         MControlDescription = row2.MControlDescription,
                //                                                         PersonResponsible = row2.PersonResponsible,
                //                                                         Key = row2.Key_Value,
                //                                                         PreventiveControl = row2.PrevationControl,
                //                                                         AutomatedControl = row2.AutomatedControl,
                //                                                         Frequency = row2.Frequency,
                //                                                         GapDescription = row2.GapDescription,
                //                                                         Recommendations = row2.Recommendations,
                //                                                         ActionRemediationplan = row2.ActionRemediationplan,
                //                                                         IPE = row2.IPE,
                //                                                         ERPsystem = row2.ERPsystem,
                //                                                         FRC = row2.FRC,
                //                                                         UniqueReferred = row2.UniqueReferred,
                //                                                         TestStrategy = row2.TestStrategy,
                //                                                         DocumentsExamined = row2.DocumentsExamined,
                //                                                     }).ToList();
                //                        }
                //                    }
                //#endregion
                //                }
                #endregion
                return riskcategorycreations;
            }
        }

        public static List<RiskCategoryCreation> GetAllRiskCategoryControl(int ProcessId, int SubprocessId, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var riskcategorycreations = (from row in entities.RiskCategoryCreations
                                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                                             select row);

                if (ProcessId != -1 && SubprocessId != -1)
                {
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId && entry.SubProcessId == SubprocessId);
                }
                else if (ProcessId != -1)
                {
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId);
                }
                else if (SubprocessId != -1)
                {
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.ActivityDescription.Contains(filter) || entry.ControlObjective.Contains(filter));
                }

                return riskcategorycreations.OrderBy(entry => entry.ActivityDescription).ToList();
                //return users.ToList();
            }
        }
        public static void Delete(int riskcreationid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                RiskCategoryCreation userToDelete = (from row in entities.RiskCategoryCreations
                                                     where row.Id == riskcreationid
                                                     select row).FirstOrDefault();

                userToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }
        public static bool Create(RiskCategoryCreation riskcategorycreation)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        entities.RiskCategoryCreations.Add(riskcategorycreation);
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        //entities.Connection.Close();
                        return false;
                    }
                }
            }
        }

        public static bool CreateReviewRemark(ReviewHistory RevHis)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    RevHis.EnType = "A";
                    entities.ReviewHistories.Add(RevHis);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool CreateInternalReviewRemark(InternalReviewHistory RevHis)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    RevHis.EnType = "A";
                    entities.InternalReviewHistories.Add(RevHis);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CreateImplementationReviewHistoryRemark(ImplementationReviewHistory RevHis)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    RevHis.EnType = "A";
                    entities.ImplementationReviewHistories.Add(RevHis);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<RiskExportinfoReport> GetAllRiskControlMatrixChange(int ProcessId, int SubprocessId, int BranchID, string Flag, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<RiskExportinfoReport> riskcategorycreations = new List<RiskExportinfoReport>();
                if (Flag == "P")
                {
                    #region Process and sub Process
                    if (BranchID != -1)
                    {
                        if (ProcessId != -1 && SubprocessId != -1)
                        {

                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                                                         //join row1 in entities.CustomerBranchMappings
                                                         //on row.Id equals row1.RiskCategoryCreationId
                                                     join row2 in entities.RiskActivityTransactions
                                                     on row.Id equals row2.RiskCreationId
                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                                                     join row3 in entities.RiskActivityToBeDoneMappings
                                                     on row2.Id equals row3.RiskActivityId
                                                     into a
                                                     from MSKS in a.DefaultIfEmpty()
                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "P"
                                                     && row.IsInternalAudit == "N"
                                                     && row.CustomerBranchId == BranchID
                                                     && row.ProcessId == ProcessId
                                                     && row.SubProcessId == SubprocessId
                                                     select new RiskExportinfoReport()
                                                     {
                                                         ID = row2.Id,
                                                         RiskCreationId = row.Id,
                                                         ControlNo = row.ControlNo, // Added By Hardik Sapara for  take gridview control number.
                                                         ActivityDescription = row.ActivityDescription,
                                                         ControlObjective = row.ControlObjective,
                                                         RiskCategory = row.Id,
                                                         ActivityTobeDone = MSKS.ActivityTobeDone,
                                                         RiskRating = (MSKS.Rating).ToString(),
                                                         Industry = row.Id,
                                                         BranchId = (int)row.CustomerBranchId,
                                                         LocationType = row.LocationType,
                                                         EffectiveDate = row2.EffectiveDate,
                                                         ProcessId = row.ProcessId,
                                                         SubProcessId = row.SubProcessId,
                                                         Assertions = null,
                                                         ControlDescription = row2.ControlDescription,
                                                         MControlDescription = row2.MControlDescription,
                                                         PersonResponsible = row2.PersonResponsible,
                                                         Key = row2.Key_Value,
                                                         PreventiveControl = row2.PrevationControl,
                                                         AutomatedControl = row2.AutomatedControl,
                                                         Frequency = row2.Frequency,
                                                         GapDescription = row2.GapDescription,
                                                         Recommendations = row2.Recommendations,
                                                         ActionRemediationplan = row2.ActionRemediationplan,
                                                         IPE = row2.IPE,
                                                         ERPsystem = row2.ERPsystem,
                                                         FRC = row2.FRC,
                                                         UniqueReferred = row2.UniqueReferred,
                                                         TestStrategy = row2.TestStrategy,
                                                         DocumentsExamined = row2.DocumentsExamined,
                                                     }).ToList();


                        }
                        else if (ProcessId != -1)
                        {


                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                                                         //join row1 in entities.CustomerBranchMappings
                                                         //on row.Id equals row1.RiskCategoryCreationId
                                                     join row2 in entities.RiskActivityTransactions
                                                     on row.Id equals row2.RiskCreationId
                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                                                     join row3 in entities.RiskActivityToBeDoneMappings
                                                     on row2.Id equals row3.RiskActivityId
                                                     into a
                                                     from MSKS in a.DefaultIfEmpty()
                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "P" && row.IsInternalAudit == "N"
                                                     && row.CustomerBranchId == BranchID && row.ProcessId == ProcessId
                                                     select new RiskExportinfoReport()
                                                     {
                                                         ID = row2.Id,
                                                         RiskCreationId = row.Id,
                                                         ControlNo = row.ControlNo, // Added By Hardik Sapara for  take gridview control number.
                                                         ActivityDescription = row.ActivityDescription,
                                                         ControlObjective = row.ControlObjective,
                                                         RiskCategory = row.Id,
                                                         ActivityTobeDone = MSKS.ActivityTobeDone,
                                                         RiskRating = (MSKS.Rating).ToString(),
                                                         Industry = row.Id,
                                                         BranchId = (int)row.CustomerBranchId,
                                                         LocationType = row.LocationType,
                                                         EffectiveDate = row2.EffectiveDate,
                                                         ProcessId = row.ProcessId,
                                                         SubProcessId = row.SubProcessId,
                                                         Assertions = null,
                                                         ControlDescription = row2.ControlDescription,
                                                         MControlDescription = row2.MControlDescription,
                                                         PersonResponsible = row2.PersonResponsible,
                                                         Key = row2.Key_Value,
                                                         PreventiveControl = row2.PrevationControl,
                                                         AutomatedControl = row2.AutomatedControl,
                                                         Frequency = row2.Frequency,
                                                         GapDescription = row2.GapDescription,
                                                         Recommendations = row2.Recommendations,
                                                         ActionRemediationplan = row2.ActionRemediationplan,
                                                         IPE = row2.IPE,
                                                         ERPsystem = row2.ERPsystem,
                                                         FRC = row2.FRC,
                                                         UniqueReferred = row2.UniqueReferred,
                                                         TestStrategy = row2.TestStrategy,
                                                         DocumentsExamined = row2.DocumentsExamined,
                                                     }).ToList();
                        }
                        else if (SubprocessId != -1)
                        {


                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                                                         //join row1 in entities.CustomerBranchMappings
                                                         //on row.Id equals row1.RiskCategoryCreationId
                                                     join row2 in entities.RiskActivityTransactions
                                                     on row.Id equals row2.RiskCreationId
                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                                                     join row3 in entities.RiskActivityToBeDoneMappings
                                                     on row2.Id equals row3.RiskActivityId
                                                     into a
                                                     from MSKS in a.DefaultIfEmpty()
                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "P" && row.IsInternalAudit == "N"
                                                     && row.CustomerBranchId == BranchID && row.SubProcessId == SubprocessId
                                                     select new RiskExportinfoReport()
                                                     {
                                                         ID = row2.Id,
                                                         RiskCreationId = row.Id,
                                                         ControlNo = row.ControlNo, // Added By Hardik Sapara for  take gridview control number.
                                                         ActivityDescription = row.ActivityDescription,
                                                         ControlObjective = row.ControlObjective,
                                                         RiskCategory = row.Id,
                                                         ActivityTobeDone = MSKS.ActivityTobeDone,
                                                         RiskRating = (MSKS.Rating).ToString(),
                                                         Industry = row.Id,
                                                         BranchId = (int)row.CustomerBranchId,
                                                         LocationType = row.LocationType,
                                                         EffectiveDate = row2.EffectiveDate,
                                                         ProcessId = row.ProcessId,
                                                         SubProcessId = row.SubProcessId,
                                                         Assertions = null,
                                                         ControlDescription = row2.ControlDescription,
                                                         MControlDescription = row2.MControlDescription,
                                                         PersonResponsible = row2.PersonResponsible,
                                                         Key = row2.Key_Value,
                                                         PreventiveControl = row2.PrevationControl,
                                                         AutomatedControl = row2.AutomatedControl,
                                                         Frequency = row2.Frequency,
                                                         GapDescription = row2.GapDescription,
                                                         Recommendations = row2.Recommendations,
                                                         ActionRemediationplan = row2.ActionRemediationplan,
                                                         IPE = row2.IPE,
                                                         ERPsystem = row2.ERPsystem,
                                                         FRC = row2.FRC,
                                                         UniqueReferred = row2.UniqueReferred,
                                                         TestStrategy = row2.TestStrategy,
                                                         DocumentsExamined = row2.DocumentsExamined,
                                                     }).ToList();
                        }

                    }
                    #endregion
                }
                else
                {
                    if (BranchID != -1)
                    {
                        if (ProcessId != -1 && SubprocessId != -1)
                        {

                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                                                         //join row1 in entities.CustomerBranchMappings
                                                         //on row.Id equals row1.RiskCategoryCreationId
                                                     join row2 in entities.RiskActivityTransactions
                                                     on row.Id equals row2.RiskCreationId
                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                                                     join row3 in entities.RiskActivityToBeDoneMappings
                                                     on row2.Id equals row3.RiskActivityId
                                                     into a
                                                     from MSKS in a.DefaultIfEmpty()
                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "N"
                                                     && row.IsInternalAudit == "N"
                                                     //&& row.Id == row3.RiskCategoryCreationId
                                                     && row.CustomerBranchId == BranchID
                                                     && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId
                                                     select new RiskExportinfoReport()
                                                     {
                                                         ID = row2.Id,
                                                         RiskCreationId = row.Id,
                                                         ControlNo = row.ControlNo, // Added By Hardik Sapara for  take gridview control number.
                                                         ActivityDescription = row.ActivityDescription,
                                                         ControlObjective = row.ControlObjective,
                                                         RiskCategory = row.Id,
                                                         ActivityTobeDone = MSKS.ActivityTobeDone,
                                                         RiskRating = (MSKS.Rating).ToString(),
                                                         Industry = row.Id,
                                                         BranchId = (int)row.CustomerBranchId,
                                                         LocationType = row.LocationType,
                                                         EffectiveDate = row2.EffectiveDate,
                                                         ProcessId = row.ProcessId,
                                                         SubProcessId = row.SubProcessId,
                                                         Assertions = null,
                                                         ControlDescription = row2.ControlDescription,
                                                         MControlDescription = row2.MControlDescription,
                                                         PersonResponsible = row2.PersonResponsible,
                                                         Key = row2.Key_Value,
                                                         PreventiveControl = row2.PrevationControl,
                                                         AutomatedControl = row2.AutomatedControl,
                                                         Frequency = row2.Frequency,
                                                         GapDescription = row2.GapDescription,
                                                         Recommendations = row2.Recommendations,
                                                         ActionRemediationplan = row2.ActionRemediationplan,
                                                         IPE = row2.IPE,
                                                         ERPsystem = row2.ERPsystem,
                                                         FRC = row2.FRC,
                                                         UniqueReferred = row2.UniqueReferred,
                                                         TestStrategy = row2.TestStrategy,
                                                         DocumentsExamined = row2.DocumentsExamined,
                                                     }).ToList();
                        }
                        else if (ProcessId != -1)
                        {


                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                                                         //join row1 in entities.CustomerBranchMappings
                                                         //on row.Id equals row1.RiskCategoryCreationId
                                                     join row2 in entities.RiskActivityTransactions
                                                     on row.Id equals row2.RiskCreationId
                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                                                     join row3 in entities.RiskActivityToBeDoneMappings
                                                     on row2.Id equals row3.RiskActivityId
                                                     into a
                                                     from MSKS in a.DefaultIfEmpty()
                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "N" && row.IsInternalAudit == "N"
                                                     && row.CustomerBranchId == BranchID && row.ProcessId == ProcessId
                                                     select new RiskExportinfoReport()
                                                     {
                                                         ID = row2.Id,
                                                         RiskCreationId = row.Id,
                                                         ControlNo = row.ControlNo, // Added By Hardik Sapara for  take gridview control number.
                                                         ActivityDescription = row.ActivityDescription,
                                                         ControlObjective = row.ControlObjective,
                                                         RiskCategory = row.Id,
                                                         ActivityTobeDone = MSKS.ActivityTobeDone,
                                                         RiskRating = (MSKS.Rating).ToString(),
                                                         Industry = row.Id,
                                                         BranchId = (int)row.CustomerBranchId,
                                                         LocationType = row.LocationType,
                                                         EffectiveDate = row2.EffectiveDate,
                                                         ProcessId = row.ProcessId,
                                                         SubProcessId = row.SubProcessId,
                                                         Assertions = null,
                                                         ControlDescription = row2.ControlDescription,
                                                         MControlDescription = row2.MControlDescription,
                                                         PersonResponsible = row2.PersonResponsible,
                                                         Key = row2.Key_Value,
                                                         PreventiveControl = row2.PrevationControl,
                                                         AutomatedControl = row2.AutomatedControl,
                                                         Frequency = row2.Frequency,
                                                         GapDescription = row2.GapDescription,
                                                         Recommendations = row2.Recommendations,
                                                         ActionRemediationplan = row2.ActionRemediationplan,
                                                         IPE = row2.IPE,
                                                         ERPsystem = row2.ERPsystem,
                                                         FRC = row2.FRC,
                                                         UniqueReferred = row2.UniqueReferred,
                                                         TestStrategy = row2.TestStrategy,
                                                         DocumentsExamined = row2.DocumentsExamined,
                                                     }).ToList();
                        }
                        else if (SubprocessId != -1)
                        {


                            riskcategorycreations = (from row in entities.RiskCategoryCreations
                                                         //join row1 in entities.CustomerBranchMappings
                                                         //on row.Id equals row1.RiskCategoryCreationId
                                                     join row2 in entities.RiskActivityTransactions
                                                     on row.Id equals row2.RiskCreationId
                                                     join MP in entities.Mst_Process on row.ProcessId equals MP.Id
                                                     join row3 in entities.RiskActivityToBeDoneMappings
                                                     on row2.Id equals row3.RiskActivityId
                                                     into a
                                                     from MSKS in a.DefaultIfEmpty()
                                                     where row.IsDeleted == false && MP.IsProcessNonProcess == "N" && row.IsInternalAudit == "N"
                                                     && row.CustomerBranchId == BranchID && row.SubProcessId == SubprocessId
                                                     select new RiskExportinfoReport()
                                                     {
                                                         ID = row2.Id,
                                                         RiskCreationId = row.Id,
                                                         ControlNo = row.ControlNo, // Added By Hardik Sapara for  take gridview control number.
                                                         ActivityDescription = row.ActivityDescription,
                                                         ControlObjective = row.ControlObjective,
                                                         RiskCategory = row.Id,
                                                         ActivityTobeDone = MSKS.ActivityTobeDone,
                                                         RiskRating = (MSKS.Rating).ToString(),
                                                         Industry = row.Id,
                                                         BranchId = (int)row.CustomerBranchId,
                                                         LocationType = row.LocationType,
                                                         EffectiveDate = row2.EffectiveDate,
                                                         ProcessId = row.ProcessId,
                                                         SubProcessId = row.SubProcessId,
                                                         Assertions = null,
                                                         ControlDescription = row2.ControlDescription,
                                                         MControlDescription = row2.MControlDescription,
                                                         PersonResponsible = row2.PersonResponsible,
                                                         Key = row2.Key_Value,
                                                         PreventiveControl = row2.PrevationControl,
                                                         AutomatedControl = row2.AutomatedControl,
                                                         Frequency = row2.Frequency,
                                                         GapDescription = row2.GapDescription,
                                                         Recommendations = row2.Recommendations,
                                                         ActionRemediationplan = row2.ActionRemediationplan,
                                                         IPE = row2.IPE,
                                                         ERPsystem = row2.ERPsystem,
                                                         FRC = row2.FRC,
                                                         UniqueReferred = row2.UniqueReferred,
                                                         TestStrategy = row2.TestStrategy,
                                                         DocumentsExamined = row2.DocumentsExamined,
                                                     }).ToList();
                        }
                    }
                }


                return riskcategorycreations;
            }
        }


        #region ObservationHistory
        public static bool CheckExistObservationHistory(ObservationHistory objHistory)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.ObservationHistories
                             where row.AuditId == objHistory.AuditId &&
                             row.Observation == objHistory.Observation
                             && row.ATBTID == objHistory.ATBTID
                             && row.AuditId == objHistory.AuditId
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                //if (query != null)
                //    return false;
                //else
                //    return true;
            }
        }

        public static bool CheckExistObservationReport(ObservationHistory objHistory)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.ObservationHistories
                             where row.AuditId == objHistory.AuditId &&
                             row.Observation == objHistory.Observation &&
                             row.ISACPORMIS == objHistory.ISACPORMIS //&& row.RoleID==objHistory.RoleID
                             select row).FirstOrDefault();

                if (query != null)
                    return false;
                else
                    return true;
            }
        }
        public static bool BulkObservationHistoryInsert(List<ObservationHistory> ListOH)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int Count = 0;
                    ListOH.ForEach(EachStep =>
                    {
                        Count++;
                        entities.ObservationHistories.Add(EachStep);
                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static void AddObservationHistory(ObservationHistory objHistory)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.ObservationHistories.Add(objHistory);
                    entities.SaveChanges();
                }
            }
            catch (Exception)
            {

            }
        }

        #endregion

        //30 Jan

        public static AuditImpementationScheduleOn GetAuditImpementationScheduleOnDetailsNew(string FinancialYear, string ForMonth, int CustomerBranchid, int verticalId, int Scheduledonid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult =
                                (
                                    from row in entities.AuditImpementationScheduleOns
                                    where row.FinancialYear == FinancialYear && row.ForMonth == ForMonth
                                    && row.Id == Scheduledonid
                                    && row.AuditID == AuditID
                                    select row
                                ).FirstOrDefault();

                //var MstRiskResult = (
                //                    from row in entities.AuditImpementationScheduleOns
                //                    join row1 in entities.AuditImplementationInstances
                //                    on row.ImplementationInstance equals row1.Id
                //                    where row.FinancialYear == FinancialYear && row.ForMonth == ForMonth
                //                    && row1.CustomerBranchID == CustomerBranchid
                //                          && row1.VerticalID == verticalId && row.Id == Scheduledonid
                //                          && row.AuditID == AuditID
                //                    select row).FirstOrDefault();
                return MstRiskResult;
            }
        }

        public static bool ImplementationAuditResultExists(ImplementationAuditResult ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.ImplementationAuditResults
                             where row.FinancialYear == ICR.FinancialYear
                             && row.ForPeriod == ICR.ForPeriod
                             && row.CustomerBranchId == ICR.CustomerBranchId
                             && row.UserID == ICR.UserID
                             && row.RoleID == ICR.RoleID
                             && row.ResultID == ICR.ResultID
                             && row.VerticalID == ICR.VerticalID
                             && row.AuditID == ICR.AuditID
                             && row.AuditScheduleOnID == ICR.AuditScheduleOnID
                             && row.ISACTIVE == true
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool AuditImplementationTransactionExists(AuditImplementationTransaction IAT)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditImplementationTransactions
                             where row.FinancialYear == IAT.FinancialYear
                             && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId && row.UserID == IAT.UserID
                              && row.RoleID == IAT.RoleID && row.ResultID == IAT.ResultID
                              && row.VerticalID == IAT.VerticalID && row.AuditID == IAT.AuditID && row.ISACTIVE == true
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool UpdateImplementationAuditResult(ImplementationAuditResult ICR)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    ImplementationAuditResult RecordtoUpdate = (from row in entities.ImplementationAuditResults
                                                                where row.FinancialYear == ICR.FinancialYear
                                                                && row.ForPeriod == ICR.ForPeriod && row.CustomerBranchId == ICR.CustomerBranchId
                                                                && row.UserID == ICR.UserID && row.RoleID == ICR.RoleID && row.ResultID == ICR.ResultID
                                                                && row.VerticalID == ICR.VerticalID && row.AuditID == ICR.AuditID && row.ISACTIVE == true
                                                                select row).OrderByDescending(x => x.ID).FirstOrDefault();

                    RecordtoUpdate.ProcessWalkthrough = ICR.ProcessWalkthrough;
                    RecordtoUpdate.ActualWorkDone = ICR.ActualWorkDone;
                    RecordtoUpdate.Population = ICR.Population;
                    RecordtoUpdate.Sample = ICR.Sample;
                    RecordtoUpdate.PersonResponsible = ICR.PersonResponsible;
                    RecordtoUpdate.ManagementResponse = ICR.ManagementResponse;
                    RecordtoUpdate.TimeLine = ICR.TimeLine;
                    RecordtoUpdate.Status = ICR.Status;
                    RecordtoUpdate.ImplementationStatus = ICR.ImplementationStatus;
                    RecordtoUpdate.FixRemark = ICR.FixRemark;
                    RecordtoUpdate.VerticalID = ICR.VerticalID;
                    RecordtoUpdate.UpdatedBy = ICR.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = ICR.UpdatedOn;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        public static bool UpdateAuditImplementationTransactionStatus(AuditImplementationTransaction IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    AuditImplementationTransaction RecordtoUpdate = (from row in entities.AuditImplementationTransactions
                                                                     where row.FinancialYear == IAT.FinancialYear
                                                                     && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId
                                                                     && row.UserID == IAT.UserID && row.RoleID == IAT.RoleID && row.ResultID == IAT.ResultID
                                                                     && row.VerticalID == IAT.VerticalID && row.AuditID == IAT.AuditID && row.ISACTIVE == true
                                                                     select row).FirstOrDefault();

                    RecordtoUpdate.PersonResponsible = IAT.PersonResponsible;
                    RecordtoUpdate.ForPeriod = IAT.ForPeriod;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.Remarks = IAT.Remarks;
                    RecordtoUpdate.StatusId = IAT.StatusId;
                    RecordtoUpdate.ImplementationStatusId = IAT.ImplementationStatusId;
                    RecordtoUpdate.StatusChangedOn = IAT.StatusChangedOn;
                    RecordtoUpdate.VerticalID = IAT.VerticalID;
                    RecordtoUpdate.UpdatedBy = IAT.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = IAT.UpdatedOn;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        public static bool UpdateAuditImplementationTransactionTxnStatusReviewer(AuditImplementationTransaction IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    AuditImplementationTransaction RecordtoUpdate = (from row in entities.AuditImplementationTransactions
                                                                     where row.FinancialYear == IAT.FinancialYear
                                                                     && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId
                                                                     && row.UserID == IAT.UserID && row.RoleID == IAT.RoleID && row.ResultID == IAT.ResultID
                                                                     && row.VerticalID == IAT.VerticalID && row.AuditID == IAT.AuditID && row.ISACTIVE == true
                                                                     select row).FirstOrDefault();


                    RecordtoUpdate.ForPeriod = IAT.ForPeriod;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.Remarks = IAT.Remarks;
                    RecordtoUpdate.StatusId = IAT.StatusId;
                    RecordtoUpdate.ImplementationStatusId = IAT.ImplementationStatusId;
                    RecordtoUpdate.StatusChangedOn = IAT.StatusChangedOn;
                    RecordtoUpdate.VerticalID = IAT.VerticalID;
                    RecordtoUpdate.UpdatedBy = IAT.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = IAT.UpdatedOn;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        public static bool ImplementationAuditResultExistsWithStatus(ImplementationAuditResult ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.ImplementationAuditResults
                             where row.FinancialYear == ICR.FinancialYear
                             && row.ForPeriod == ICR.ForPeriod
                             && row.CustomerBranchId == ICR.CustomerBranchId
                             && row.UserID == ICR.UserID
                             && row.RoleID == ICR.RoleID
                             && row.ResultID == ICR.ResultID
                             && row.VerticalID == ICR.VerticalID
                             && row.AuditScheduleOnID == ICR.AuditScheduleOnID
                             && row.Status == ICR.Status
                             && row.AuditID == ICR.AuditID
                             && row.ImplementationStatus == ICR.ImplementationStatus
                             && row.ISACTIVE == true
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool UpdateImplementationAuditResultResult(ImplementationAuditResult ICR, int AuditStatusId)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    ImplementationAuditResult RecordtoUpdate = (from row in entities.ImplementationAuditResults
                                                                where row.FinancialYear == ICR.FinancialYear
                                                                && row.ForPeriod == ICR.ForPeriod && row.CustomerBranchId == ICR.CustomerBranchId
                                                                && row.Status == AuditStatusId && row.RoleID == ICR.RoleID && row.ResultID == ICR.ResultID
                                                                && row.VerticalID == ICR.VerticalID && row.AuditID == ICR.AuditID && row.ISACTIVE == true
                                                                select row).OrderByDescending(x => x.ID).FirstOrDefault();

                    RecordtoUpdate.ProcessWalkthrough = ICR.ProcessWalkthrough;
                    RecordtoUpdate.ActualWorkDone = ICR.ActualWorkDone;
                    RecordtoUpdate.Population = ICR.Population;
                    RecordtoUpdate.Sample = ICR.Sample;
                    RecordtoUpdate.PersonResponsible = ICR.PersonResponsible;
                    RecordtoUpdate.ManagementResponse = ICR.ManagementResponse;
                    RecordtoUpdate.TimeLine = ICR.TimeLine;
                    RecordtoUpdate.Status = ICR.Status;
                    RecordtoUpdate.ImplementationStatus = ICR.ImplementationStatus;
                    RecordtoUpdate.FixRemark = ICR.FixRemark;
                    RecordtoUpdate.VerticalID = ICR.VerticalID;
                    RecordtoUpdate.UpdatedBy = ICR.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = ICR.UpdatedOn;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        public static bool UpdateAuditImplementationTransaction(AuditImplementationTransaction IAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    AuditImplementationTransaction RecordtoUpdate = (from row in entities.AuditImplementationTransactions
                                                                     where row.FinancialYear == IAT.FinancialYear
                                                                     && row.ForPeriod == IAT.ForPeriod && row.CustomerBranchId == IAT.CustomerBranchId
                                                                     && row.UserID == IAT.UserID && row.RoleID == IAT.RoleID && row.ResultID == IAT.ResultID
                                                                     && row.VerticalID == IAT.VerticalID && row.AuditID == IAT.AuditID && row.ISACTIVE == true
                                                                     select row).FirstOrDefault();

                    RecordtoUpdate.PersonResponsible = IAT.PersonResponsible;
                    RecordtoUpdate.StatusId = IAT.StatusId;
                    RecordtoUpdate.ForPeriod = IAT.ForPeriod;
                    RecordtoUpdate.Dated = DateTime.Now;
                    RecordtoUpdate.VerticalID = IAT.VerticalID;
                    RecordtoUpdate.UpdatedBy = IAT.UpdatedBy;
                    RecordtoUpdate.UpdatedOn = IAT.UpdatedOn;
                    entities.SaveChanges();
                };

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        public static AuditImplementationInstance GetAuditImplementationInstanceData(string forperiod, int Implementationauditinstance, int Customerbranchid, int VerticalID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var instanceData = (from row in entities.AuditImplementationInstances
                                    where row.ForPeriod == forperiod && row.IsDeleted == false
                                    && row.Id == Implementationauditinstance
                                    && row.CustomerBranchID == Customerbranchid
                                    && row.VerticalID == VerticalID
                                    && row.AuditID == AuditID
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }

        public static ImplementationFileData_Risk GetFileImplementationFileData_Risk(int fileID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var file = (from row in entities.ImplementationFileData_Risk
                            where row.ID == fileID && row.AuditID == AuditID
                            select row).FirstOrDefault();

                return file;
            }
        }

        public static AuditClosure GetAuditClosureResult(string ForPeriod, int CustomerBranchId, string FinancialYear, int VerticalID, int ProcessID, long ATBTID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var MstRiskResult = (from row in entities.AuditClosures
                                     where row.CustomerbranchId == CustomerBranchId
                                     && row.FinancialYear == FinancialYear && row.ForPeriod == ForPeriod
                                     && row.VerticalID == VerticalID && row.ProcessId == ProcessID
                                     && row.ATBDId == ATBTID && row.AuditID == AuditID
                                     select row).FirstOrDefault();

                return MstRiskResult;
            }
        }

        //Added By Sushant 10 Sept 2018

        public static bool CheckObservationImageExist(ObservationImage objImg)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.ObservationImages
                             where row.ATBTID == objImg.ATBTID && row.AuditID == objImg.AuditID
                             && row.ImagePath == objImg.ImagePath && row.ImageName == objImg.ImageName
                             && row.IsActive == true
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool UpdateObservationImg(ObservationImage objImg)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.ObservationImages
                             where row.ATBTID == objImg.ATBTID && row.AuditID == objImg.AuditID
                             && row.ImagePath == objImg.ImagePath && row.ImageName == objImg.ImageName
                             && row.IsActive == true
                             select row).FirstOrDefault();

                if (query != null)
                {
                    query.IsActive = false;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CreateObservationImg(ObservationImage objImg)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.ObservationImages.Add(objImg);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static ObservationImage GetObservationImageFile(int ID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.ObservationImages
                             where row.ID == ID
                             && row.IsActive == true
                             select row).FirstOrDefault();

                return query;
            }
        }

        public static List<ObservationImage> GetObservationImageFileList(int auditID, long ATBTID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.ObservationImages
                             where row.AuditID == auditID && row.ATBTID == ATBTID
                             && row.IsActive == true
                             select row).ToList();

                return query;
            }
        }

        // added by sagar more on 08-01-2020
        public static void SaveObservationAudioVideo(ObservationAudioVideo observationAudioVideo)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.ObservationAudioVideos.Add(observationAudioVideo);
                entities.SaveChanges();
            }
        }

        // added by sagar more on 08-01-2020
        public static bool CheckObservationAudioVideoExist(ObservationAudioVideo observationAudioVideo)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.ObservationAudioVideos
                             where cs.ATBTID == observationAudioVideo.ATBTID &&
                             cs.AuditId == observationAudioVideo.AuditId &&
                             cs.AudioVideoLink == observationAudioVideo.AudioVideoLink
                             select cs).FirstOrDefault();
                if (query != null)
                    return true;
                else
                    return false;
            }
        }

        // added by sagar more on 08-01-2020
        public static void UpdateObservationAudioVideo(ObservationAudioVideo observationAudioVideo)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                ObservationAudioVideo objObservationAudioVideo = (from cs in entities.ObservationAudioVideos
                                                                  where cs.ATBTID == observationAudioVideo.ATBTID &&
                                                                  cs.AuditId == observationAudioVideo.AuditId
                                                                  && cs.AudioVideoLink == observationAudioVideo.AudioVideoLink
                                                                  select cs).FirstOrDefault();

                if (objObservationAudioVideo != null)
                {
                    objObservationAudioVideo.AuditId = observationAudioVideo.AuditId;
                    objObservationAudioVideo.ATBTID = observationAudioVideo.ATBTID;
                    objObservationAudioVideo.AudioVideoLink = observationAudioVideo.AudioVideoLink;
                    objObservationAudioVideo.UpdatedBy = observationAudioVideo.UpdatedBy;
                    objObservationAudioVideo.UpdatedOn = observationAudioVideo.UpdatedOn;
                    objObservationAudioVideo.IsActive = true;
                    entities.SaveChanges();
                }
            }
        }

        // added by sagar more on 08-01-2020
        public static List<ObservationAudioVideo> GetAllObservationAudioVideo(long AuditId, long ATBTID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ObservationAudioVideo> observationAudioVideoList = (from cs in entities.ObservationAudioVideos
                                                                         where cs.ATBTID == ATBTID && cs.AuditId == AuditId
                                                                         && cs.IsActive == true
                                                                         select cs).ToList();
                return observationAudioVideoList;
            }
        }

        // added by sagar more on 08-01-2020
        public static void DeleteObservationAudioVideo(string AudioVideoLink, long AuditId, long ATBTID, int userId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                ObservationAudioVideo query = (from cs in entities.ObservationAudioVideos
                                               where cs.ATBTID == ATBTID && cs.AuditId == AuditId
                                               && cs.AudioVideoLink == AudioVideoLink
                                               && cs.IsActive == true
                                               select cs).FirstOrDefault();
                if (query != null)
                {
                    query.IsActive = false;
                    query.UpdatedBy = userId;
                    query.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
            }
        }

        // added by sagar more on 09-01-2020
        public static List<ObservationAudioVideo> GetObservationAudioVideoList(long AuditId, long ATBTID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ObservationAudioVideo> observationAudioVideoList = (from cs in entities.ObservationAudioVideos
                                                                         where cs.ATBTID == ATBTID && cs.AuditId == AuditId
                                                                         && cs.IsActive == true
                                                                         select cs).ToList();
                return observationAudioVideoList;
            }
        }

        public static int GetAuditReportNumber(long auditID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var result = (from row in entities.AuditClosureDetails
                              where row.ID == auditID
                              select row.ReportNumber).FirstOrDefault();

                if (result != null)
                {
                    return Convert.ToInt32(result);
                }
                else
                {
                    var result1 = (from row in entities.AuditClosureDetails
                                   where row.FinancialYear == FinancialYear
                                   select row.ReportNumber).Max();
                    if (result1 == null)
                    {
                        return 1;
                    }
                    else
                    {
                        return Convert.ToInt32(result1) + 1;
                    }
                }
            }
        }

        public static string GetReportNameAndNumber(long auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //var result = (from row in entities.AuditClosureDetails
                //              where row.ID == auditID
                //              select row.ReportName + row.ReportNumber).FirstOrDefault();


                // changed by sagar more on 23-01-2020
                string reportNameAndNumber = string.Empty;

                var result = (from row in entities.AuditClosureDetails
                              where row.ID == auditID
                              select row).FirstOrDefault();
                if (result.ReportNumber < 10)
                {
                    string rptNumber = "0" + result.ReportNumber.ToString();
                    string.Format("{0:D2}", rptNumber);
                    reportNameAndNumber = result.ReportName + rptNumber;
                }
                else
                {
                    reportNameAndNumber = result.ReportName + result.ReportNumber;
                }
                return reportNameAndNumber;
            }
        }

        public static bool CheckGeneareReportExistOrNot(int auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var result = (from row in entities.Tran_FinalDeliverableUpload
                              where row.AuditID == auditID
                              select row).FirstOrDefault();
                if (result != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region added by sagar more on 21-05-2020
        public static List<RiskActivityToBeDoneMapping> GetRiskActivityToBeDoneMappingsList(string ActivityToBeDone, int UserID, int RoleId, string FinancialYear, string ForMonth, int CustomerBranchID, int VerticalId, int AuditID, long ProcessId, long SubprocessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (
                                    from row in entities.InternalControlAuditAssignments
                                    join row1 in entities.InternalAuditScheduleOns
                                    on row.InternalAuditInstance equals row1.InternalAuditInstance
                                    join row2 in entities.RiskActivityToBeDoneMappings
                                    on row.ProcessId equals row2.ProcessId
                                    where row.ProcessId == row1.ProcessId
                                    && row1.FinancialYear == FinancialYear
                                    && row1.ForMonth == ForMonth
                                    && row.CustomerBranchID == CustomerBranchID
                                    && row.VerticalID == VerticalId
                                    && row.AuditID == AuditID
                                    && row.RoleID == RoleId
                                    && row2.ActivityTobeDone == ActivityToBeDone
                                    && row2.ProcessId == ProcessId
                                    && row2.SubProcessId == SubprocessId
                                    select row2
                             ).Distinct().ToList();
                return MstRiskResult;

                //var MstRiskResult = (from row in entities.sp_GetRiskActivityToBeDoneMappingsListForHistoricalObservation(FinancialYear,ForMonth,CustomerBranchID,VerticalId,AuditID)
                //                     select row).Distinct().ToList();

                //return MstRiskResult;
            }
        }

        public static bool RiskActivityToBeDoneMappingExists1(long RCCID, long custBranchID, int verticalID, long Processid, long SubProcessID, string AuditStep)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityToBeDoneMappings
                             where row.ActivityTobeDone.Trim().ToUpper() == AuditStep.Trim().ToUpper()
                             && row.RiskCategoryCreationId == RCCID
                             && row.ProcessId == Processid
                             && row.SubProcessId == SubProcessID
                             && row.CustomerBranchID == custBranchID
                             && row.VerticalID == verticalID
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }

        public static int GetResultIdForHistroricalObservation(long CustomerbranchId, string ForPeriod, string FinancialYear, long ATBDId, int VerticalID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int resultId = 0;
                var query = (from cs in entities.AuditClosures
                             where cs.CustomerbranchId == CustomerbranchId && cs.ForPeriod == ForPeriod && cs.FinancialYear == FinancialYear
                             && cs.ATBDId == ATBDId && cs.VerticalID == VerticalID && cs.AuditID == AuditID
                             select cs).FirstOrDefault();
                if (query != null)
                {
                    resultId = Convert.ToInt32(query.ResultID);
                    return resultId;
                }
                else
                {
                    return resultId;
                }
            }
        }

        public static int GetScheduledOnIDForHistroricalObservation(long CustomerbranchId, string ForPeriod, string FinancialYear, long ATBDId, int VerticalID, long AuditID, int ProcessId)
        {
            int scheduledOnID = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from AISO in entities.AuditImpementationScheduleOns
                             join ICAR in entities.InternalControlAuditResults
                             on AISO.ProcessId equals ICAR.ProcessId
                             where AISO.ForMonth == ForPeriod && AISO.FinancialYear == FinancialYear && AISO.AuditID == AuditID &&
                             ICAR.CustomerBranchId == CustomerbranchId && ICAR.ForPerid == ForPeriod && ICAR.FinancialYear == FinancialYear
                             && ICAR.ATBDId == ATBDId && ICAR.VerticalID == VerticalID && ICAR.ProcessId == ProcessId
                             select AISO).FirstOrDefault();

                if (query != null)
                {
                    scheduledOnID = Convert.ToInt32(query.Id);
                    return scheduledOnID;
                }
                else
                {
                    return scheduledOnID;
                }

            }
        }
        #endregion

        //Added by Madhur
        public static long GetRiskCategoryCreationID(string ControlNos, int processId, int subProcessId, int CustomerId, long customerBranchId, string activityDescription, string controlObjective)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Query = (from row in entities.RiskCategoryCreations
                             where row.ActivityDescription.ToUpper().Trim().Contains(activityDescription.ToUpper().Trim()) &&
                             row.ControlObjective.ToUpper().Trim().Contains(controlObjective.ToUpper().Trim())
                             && row.ProcessId == processId && row.SubProcessId == subProcessId && row.IsInternalAudit == "N"
                             && row.CustomerId == CustomerId && row.CustomerBranchId == customerBranchId
                             && row.ControlNo.ToUpper().Trim() == ControlNos.ToUpper().Trim()
                             select row.Id).FirstOrDefault();

                return Query;
            }
        }

        public static int GetRiskActivityTransaction(int processId, int subProcessId, long riskCategoryCreationID, int Frequency, long customerBranchId, long verticalId, long activityid, string controlDescription)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             where row.CustomerBranchId == customerBranchId
                             && row.VerticalsId == verticalId
                             && row.ProcessId == processId
                             && row.SubProcessId == subProcessId
                             && row.RiskCreationId == riskCategoryCreationID
                             && row.Frequency == Frequency
                             && row.ActivityID == activityid
                             && row.ControlDescription.ToUpper().Trim() == controlDescription.ToUpper().Trim()
                             select row.Id).FirstOrDefault();
                return query;
            }
        }

        public static List<RiskActivityToBeDoneMapping> GetRiskActivityToBeDoneMappingsList(string ActivityToBeDone, int UserID, int RoleId, string FinancialYear, string ForMonth, int CustomerBranchID, int VerticalId, int AuditID, long ProcessId, long SubprocessId, long RiskCategoryCreationID, int RiskActivityTransaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (
                                    from row in entities.InternalControlAuditAssignments
                                    join row1 in entities.InternalAuditScheduleOns
                                    on row.InternalAuditInstance equals row1.InternalAuditInstance
                                    join row2 in entities.RiskActivityToBeDoneMappings
                                    on row.ProcessId equals row2.ProcessId
                                    where row.ProcessId == row1.ProcessId
                                    && row1.FinancialYear == FinancialYear
                                    && row1.ForMonth == ForMonth
                                    && row.CustomerBranchID == CustomerBranchID
                                    && row.VerticalID == VerticalId
                                    && row.AuditID == AuditID
                                    && row.RoleID == RoleId
                                    && row2.ActivityTobeDone == ActivityToBeDone
                                    && row2.ProcessId == ProcessId
                                    && row2.SubProcessId == SubprocessId
                                    && row2.RiskCategoryCreationId == RiskCategoryCreationID
                                    && row2.RiskActivityId == RiskActivityTransaction
                                    select row2
                             ).Distinct().ToList();
                return MstRiskResult;

                //var MstRiskResult = (from row in entities.sp_GetRiskActivityToBeDoneMappingsListForHistoricalObservation(FinancialYear,ForMonth,CustomerBranchID,VerticalId,AuditID)
                //                     select row).Distinct().ToList();

                //return MstRiskResult;
            }
        }
        //End
        public static int GetRiskActivityTransactionID(long Processid, long SubProcessid, long riskcreationId, long BranchId, long verticalID, string ControlDescription)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             where row.CustomerBranchId == BranchId
                             && row.VerticalsId == verticalID
                             && row.ProcessId == Processid
                             && row.SubProcessId == SubProcessid
                             && row.RiskCreationId == riskcreationId
                             && row.ControlDescription.ToUpper().Trim() == ControlDescription.ToUpper().Trim()
                             select row.Id).FirstOrDefault();

                if (query != null)
                    return query;
                else
                    return 0;
            }
        }

        public static long GetRiskActivityToBeDoneMappingID(long RCCID, long RATID, long custBranchID, int verticalID, long Processid, long SubProcessID, long auditStepMasterID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityToBeDoneMappings
                             where row.RiskCategoryCreationId == RCCID
                             && row.RiskActivityId == RATID
                             && row.ProcessId == Processid
                             && row.SubProcessId == SubProcessID
                             && row.CustomerBranchID == custBranchID
                             && row.VerticalID == verticalID
                             && row.AuditStepMasterID == auditStepMasterID
                             select row.ID).FirstOrDefault();

                if (query != null)
                    return query;
                else
                    return 0;
            }
        }

        public static List<RiskActivityToBeDoneMapping> GetRiskActivityToBeDoneMappingsDetails(long ATBDID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.RiskActivityToBeDoneMappings
                                     where row.ID == ATBDID
                                     select row).Distinct().ToList();

                return MstRiskResult;
            }
        }

        public static void InternalAuditTxnUpdate(InternalAuditTransaction IAT)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditTransactions
                             where row.ProcessId == IAT.ProcessId
                             && row.SubProcessId == IAT.SubProcessId
                                    && row.FinancialYear == IAT.FinancialYear
                                    && row.ForPeriod == IAT.ForPeriod
                                    && row.CustomerBranchId == IAT.CustomerBranchId
                                    && row.UserID == IAT.UserID
                                    && row.RoleID == IAT.RoleID
                                    && row.ATBDId == IAT.ATBDId
                                    && row.VerticalID == IAT.VerticalID
                                    && row.AuditID == IAT.AuditID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    query.PersonResponsible = IAT.PersonResponsible;
                    query.Owner = IAT.Owner;
                    entities.SaveChanges();
                }
            }
        }

        public static void UpdateAuditClosureResult(AuditClosure IAT)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditClosures
                             where row.ProcessId == IAT.ProcessId
                                    && row.FinancialYear == IAT.FinancialYear
                                    && row.ForPeriod == IAT.ForPeriod
                                    && row.CustomerbranchId == IAT.CustomerbranchId
                                    && row.ATBDId == IAT.ATBDId
                                    && row.VerticalID == IAT.VerticalID
                                    && row.AuditID == IAT.AuditID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    query.PersonResponsible = IAT.PersonResponsible;
                    query.Owner = IAT.Owner;
                    entities.SaveChanges();
                }
            }
        }
    }
}
