﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class RiskControlMatrixClass
    {

        public static List<FilterRiskControlMatrixForUpdate> GetRiskControlMatrixDataForUpdate(int customerID, List<long> BranchID, int ProcessId, int SubprocessId, int LocationType, string filter)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<FilterRiskControlMatrixForUpdate> riskcategorycreations = new List<FilterRiskControlMatrixForUpdate>();

                riskcategorycreations = (from row in entities.FilterRiskControlMatrixForUpdates
                                         where row.IsDeleted == false
                                         && row.IsInternalAudit == "N"
                                         && row.CustomerId == customerID
                                         && row.SpecialAuditFlag == false
                                         select row).ToList();

               
                if (BranchID.Count > 0)
                    riskcategorycreations = riskcategorycreations.Where(entry => BranchID.Contains((long) entry.CustomerBranchId)).ToList();

                if (ProcessId != -1)
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId).ToList();

                if (SubprocessId != -1)
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();


                riskcategorycreations = riskcategorycreations.Distinct().ToList();

                if (filter != "")
                    riskcategorycreations = riskcategorycreations
                                             .Where(entry => entry.ControlNo.Contains(filter)
                                             || entry.ControlDescription.Contains(filter)
                                             || entry.ControlObjective.Contains(filter)
                                             || entry.ActivityDescription.Contains(filter)).ToList();

                if (riskcategorycreations.Count > 0)
                {
                    riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ControlNo)
                                                                .ThenBy(entry => entry.ActivityDescription)
                                                                .ThenBy(entry => entry.ControlObjective)
                                                                .ThenBy(entry => entry.ControlDescription).ToList();
                }

                return riskcategorycreations;
            }
        }

        public static List<GetRiskControlMatrixView> GetAllRiskControlMatrix(int customerID, List<long> BranchID, int ProcessId, int SubprocessId, int LocationType)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<GetRiskControlMatrixView> RCMMasterData = new List<GetRiskControlMatrixView>();

                RCMMasterData = (from row in entities.GetRiskControlMatrixViews.AsNoTracking()
                                 where row.IsDeleted == false
                                 && row.CustomerId == customerID
                                 select row).ToList();

                if (BranchID.Count > 0)
                    RCMMasterData = RCMMasterData.Where(entry => BranchID.Contains((long) entry.BranchId)).ToList();

                if (ProcessId != -1)
                    RCMMasterData = RCMMasterData.Where(entry => entry.ProcessId == ProcessId).ToList();

                if (SubprocessId != -1)
                    RCMMasterData = RCMMasterData.Where(entry => entry.SubProcessId == SubprocessId).ToList();

                if (LocationType != -1)
                    RCMMasterData = RCMMasterData.Where(entry => entry.LocationType == LocationType).ToList();

                return RCMMasterData;
            }
        }

        public static List<FilterViewRiskRegister> GetAllRiskCategoryCreation1(int ProcessId, int SubprocessId,List<long> BranchID, int LocationType, int RiskCategory, int Industry, int customerID, string FlagProcessNonProcess, string AFlag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<FilterViewRiskRegister> riskcategorycreations = new List<FilterViewRiskRegister>();
                if (FlagProcessNonProcess == "P")
                {
                    riskcategorycreations = (from row in entities.FilterViewRiskRegisters
                                             where row.CustomerId == customerID && row.IsDeleted == false && row.IsInternalAudit == "N"
                                                 && row.IsProcessNonProcess == "P" && row.SpecialAuditFlag == false
                                             select row).ToList();


                    if (BranchID.Count > 0)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => BranchID.Contains((long)entry.BranchId)).ToList();
                    }
                    if (ProcessId != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId).ToList();
                    }
                    if (SubprocessId != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                    }
                    if (LocationType != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.LocationType == LocationType).ToList();
                    }
                }
                else // Non Process
                {

                    riskcategorycreations = (from row in entities.FilterViewRiskRegisters
                                             where row.CustomerId == customerID && row.IsDeleted == false && row.IsInternalAudit == "N"
                                                 && row.IsProcessNonProcess == "N"
                                             select row).ToList();


                    if (BranchID.Count > 0)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => BranchID.Contains((long)entry.BranchId)).ToList();
                    }
                    if (ProcessId != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId).ToList();
                    }
                    if (SubprocessId != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                    }
                    if (LocationType != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.LocationType == LocationType).ToList();
                    }
                }         
                return riskcategorycreations;
            }
        }

        public static List<FilterRiskControlMatrix> GetAllRiskControlMatrix(int ProcessId, int SubprocessId, List<long> BranchID, int LocationType, int RiskCategory, int Industry, int customerID, string FlagProcessNonProcess, string filter)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<FilterRiskControlMatrix> riskcategorycreations = new List<FilterRiskControlMatrix>();
                if (FlagProcessNonProcess == "P")
                {

                    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                                             && row.CustomerId == customerID && row.IsProcessNonProcess == "P"
                                             && row.SpecialAuditFlag == false
                                             select row).ToList();

                    if (BranchID.Count > 0)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => BranchID.Contains((long)entry.BranchId)).ToList();
                    }
                    if (ProcessId != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId).ToList();
                    }
                    if (SubprocessId != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                    }
                    if (LocationType != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.LocationType == LocationType).ToList();
                    }                   
                }
                else//Non Process
                {
                    riskcategorycreations = (from row in entities.FilterRiskControlMatrices
                                             where row.IsDeleted == false && row.IsInternalAudit == "N"
                                             && row.CustomerId == customerID && row.IsProcessNonProcess == "N"
                                             select row).ToList();


                    if (BranchID.Count > 0)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => BranchID.Contains((long) entry.BranchId)).ToList();
                    }
                    if (ProcessId != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId).ToList();
                    }
                    if (SubprocessId != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                    }
                    if (LocationType != -1)
                    {
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.LocationType == LocationType).ToList();
                    }                 
                }                
                return riskcategorycreations;
            }
        }
    }
}
