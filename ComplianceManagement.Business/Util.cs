﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Utility;
using System.Net.NetworkInformation;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class Util
    {
        #region AES
        private static string secretIV = "3BD28F6C-BAF7-4E";
        private static string secretKey = "3BD28F6C-BAF7-4E97-91A9-0577F728";
        private static int AESkeysize = 256;                
        public static RijndaelManaged GetRijndaelManaged()
        {
            var secretIVBytes = Encoding.UTF8.GetBytes(secretIV);
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            RijndaelManaged aesEncryption = new RijndaelManaged();
            aesEncryption.KeySize = AESkeysize;
            aesEncryption.BlockSize = 128;
            aesEncryption.Mode = CipherMode.CBC;
            aesEncryption.Padding = PaddingMode.PKCS7;
            aesEncryption.IV = secretIVBytes;
            aesEncryption.Key = secretKeyBytes;
            return aesEncryption;
        }
        public static string CalculateAESHash(string input)
        {
            RijndaelManaged aesEncryption = GetRijndaelManaged();
            byte[] plainText = ASCIIEncoding.UTF8.GetBytes(input);
            ICryptoTransform crypto = aesEncryption.CreateEncryptor();
            byte[] cipherText = crypto.TransformFinalBlock(plainText, 0, plainText.Length);
            return Convert.ToBase64String(cipherText);
        }    
        public static string CalculateAESHashDecrypt(string input)
        {
            RijndaelManaged aesEncryption = GetRijndaelManaged();
            byte[] encryptedData = Convert.FromBase64String(input);
            ICryptoTransform transform = aesEncryption.CreateDecryptor();
            byte[] plainText = transform.TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            return Encoding.UTF8.GetString(plainText);
        }
        public static string DCalculateAESHash(string input)
        {
            RijndaelManaged aesEncryption = GetRijndaelManaged();
            byte[] encryptedData = Convert.FromBase64String(input);
            ICryptoTransform transform = aesEncryption.CreateDecryptor();
            byte[] plainText = transform.TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            return Encoding.UTF8.GetString(plainText);           
        }
        public static string Decrypt(string cipherData, string keyString, string ivString)
        {
            byte[] key = Encoding.UTF8.GetBytes(keyString);
            byte[] iv = Encoding.UTF8.GetBytes(ivString);

            try
            {
                using (var rijndaelManaged =
                       new RijndaelManaged { Key = key, IV = iv, Mode = CipherMode.CBC })
                using (var memoryStream =
                       new MemoryStream(Convert.FromBase64String(cipherData)))
                using (var cryptoStream =
                       new CryptoStream(memoryStream,
                           rijndaelManaged.CreateDecryptor(key, iv),
                           CryptoStreamMode.Read))
                {
                    return new StreamReader(cryptoStream).ReadToEnd();
                }
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
            // You may want to catch more exceptions here...
        }
        #endregion        
        #region MD5
        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            using (MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hash = md5.ComputeHash(inputBytes);

                // step 2, convert byte array to hex string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        #endregion

        public static string CreateRandomPassword(int length)
        {
            string _allowedChars = "abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789!@$?";
            char[] chars = new char[length];
            Random rnd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = _allowedChars[rnd.Next(_allowedChars.Length)];
            }

            return new string(chars);
        }
        public static int FinancialMonth()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string monthName = (from row in entities.FinancialYears
                                    select row.Name).FirstOrDefault();

                return DateTime.ParseExact(monthName, "MMMM", CultureInfo.CurrentCulture).Month;
            }
            
        }        
        public static string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            } return sMacAddress;
        }
    }
}