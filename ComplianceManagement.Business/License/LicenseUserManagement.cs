﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.License
{
    public class LicenseUserManagement
    {
        public static object GetAllAuthorizeUsers_License(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.RoleID != 2
                             select row).ToList();

                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return lstUsers.ToList();
            }
        }
     
        public static int CreateNewUserCompliance(User user)
        {
            int newUserID = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        entities.Users.Add(user);
                        entities.SaveChanges();

                        newUserID = Convert.ToInt32(user.ID);

                        dbtransaction.Commit();

                        return newUserID;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        newUserID--;
                        entities.SP_ResetIDUser(newUserID);

                        return newUserID = 0;
                    }
                }
            }
        }

        public static bool CreateNewUserAudit(mst_User user)
        {
            int newUserID = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {                        
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        entities.mst_User.Add(user);
                        entities.SaveChanges();

                        newUserID = Convert.ToInt32(user.ID);
                        dbtransaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        newUserID--;
                        entities.SP_ResetIDMstUser(newUserID);
                        return false;
                    }
                }
            }
        }        
    }
}
