﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class DataEncryption
    {
        
        public static string key = "avantis";

        public static RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        public static RijndaelManaged GetRijndaelManagedIOS(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        public static byte[] Encrypt(byte[] plainBytes, RijndaelManaged rijndaelManaged)
        {
            byte[] decode = null;
            try
            {
                decode = rijndaelManaged.CreateEncryptor()
             .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
            }
            catch
            {
                rijndaelManaged = GetRijndaelManagedIOS(key);
                decode = rijndaelManaged.CreateEncryptor()
             .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
            }
            return decode;
        }

        public static byte[] Decrypt(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            byte[] decode = null;
            try
            {
                decode = rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            }
            catch
            {
                rijndaelManaged = GetRijndaelManagedIOS(key);
                decode = rijndaelManaged.CreateDecryptor()
               .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            }
            return decode;
        }

        public static byte[] EncryptIOS(byte[] plainBytes, RijndaelManaged rijndaelManaged)
        {
            byte[] decode = null;
            try
            {
                rijndaelManaged = GetRijndaelManagedIOS(key);
                decode = rijndaelManaged.CreateEncryptor()
             .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
            }
            catch { }
            return decode;
        }

        public static byte[] DecryptIOS(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            byte[] decode = null;
            try
            {
                rijndaelManaged = GetRijndaelManagedIOS(key);
                decode = rijndaelManaged.CreateDecryptor()
               .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            }
            catch { }
            return decode;
        }

        // Encrypts plaintext using AES 128bit key and a Chain Block Cipher and returns a base64 encoded string          
        public static String Encrypt(String plainText, String key)
        {
            try
            {
                var plainBytes = Encoding.UTF8.GetBytes(plainText);
                string ans = Convert.ToBase64String(Encrypt(plainBytes, GetRijndaelManaged(key)));
                return Convert.ToBase64String(Encrypt(plainBytes, GetRijndaelManaged(key)));
            }
            catch
            {
                var plainBytes = Encoding.UTF8.GetBytes(plainText);
                string ans = Convert.ToBase64String(Encrypt(plainBytes, GetRijndaelManagedIOS(key)));
                return Convert.ToBase64String(Encrypt(plainBytes, GetRijndaelManagedIOS(key)));
            }
        }

        public static String Decrypt(String encryptedText, String key)
        {
            try
            {
                var encryptedBytes = Convert.FromBase64String(encryptedText);
                return Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManaged(key)));
            }
            catch
            {
                var encryptedBytes = Convert.FromBase64String(encryptedText);
                return Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManagedIOS(key)));
            }
        }
    }
}
