﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.VenderAudits
{
    public class VenderAuditManagment
    {

        public static List<SP_GetAuditChecklistLicenceDetail_Result> AuditChecklistLicence(long? AuditDetailID, long? ChecklistID, DateTime fromdate, DateTime Todate, string ComplianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AuditDetail = (from row in entities.SP_GetAuditChecklistLicenceDetail(AuditDetailID, ChecklistID, fromdate, Todate, ComplianceType)
                                   select row).ToList();

                return AuditDetail;
            }
        }

        public static void DeleteCriticalDocumentChecklistMappedID(long FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.AuditCriticalDocumentMappings
                           where row.FileID == FileID 
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    AuditCriticalDocumentMapping prevmappedids = (from row in entities.AuditCriticalDocumentMappings
                                                          where row.ID == entry
                                                          select row).FirstOrDefault();
                    entities.AuditCriticalDocumentMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }
        }
        public static void CreateAuditCriticalDocMapping(AuditCriticalDocumentMapping auditCriticalDocumentMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.AuditCriticalDocumentMappings.Add(auditCriticalDocumentMapping);
                entities.SaveChanges();
            }
        }
        public static List<SP_GetAuditChecklistComplianceDocument_Result> AuditChecklistComplianceDocumentSelected(long FileID, long ScheduledOnID, long? AuditDetailID, long? CheckListMappingID, DateTime dtFrom, DateTime dtTo, string ComplianceType, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.SP_GetAuditChecklistComplianceDocument(AuditDetailID, CheckListMappingID, dtFrom, dtTo, ComplianceType, CustomerID,"")
                                where row.ID == FileID && row.ScheduledOnID == ScheduledOnID
                                select row).ToList();

                return fileData;
            }
        }
        #region AuditVendor
        public static bool AuditCategoryExists(int CategoryId)
        {
            bool result = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditChecklists
                             where row.Isactive == false && row.CategoryID == CategoryId
                             select row).ToList();

                if (query.Count > 0)
                {
                    result = true;
                }

                return result;
            }
        }
        public static bool AuditVendorExists(int vendorid)
        {
            bool result = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditChecklists
                             where row.Isactive == false && row.VendorID == vendorid
                             select row).ToList();

                if (query.Count > 0)
                {
                    result = true;
                }

                return result;
            }
        }
        public static void Delete(int vendorid, long updatedbyID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                AuditVendor ltlmToUpdate = (from row in entities.AuditVendors
                                            where row.ID == vendorid
                                            select row).FirstOrDefault();


                ltlmToUpdate.UpdatedBy = updatedbyID;
                ltlmToUpdate.Isactive = true;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static bool Exists(AuditVendor av)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditVendors
                             where row.Isactive == false && row.Name.ToUpper().Trim().Equals(av.Name.ToUpper().Trim())
                               && row.CustomerID == av.CustomerID
                             select row);

                if (av.ID > 0)
                {
                    query = query.Where(entry => entry.ID != av.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void UpdateVendor(AuditVendor newvendor)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var updatevendordtls = (from row in entities.AuditVendors
                                            where row.ID == newvendor.ID
                                            && row.Isactive == false
                                            select row).FirstOrDefault();
                    if (updatevendordtls != null)
                    {
                        updatevendordtls.Name = newvendor.Name;
                        updatevendordtls.Isactive = newvendor.Isactive;
                        updatevendordtls.UpdatedBy = newvendor.UpdatedBy;
                        updatevendordtls.UpdatedOn = newvendor.UpdatedOn;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static int CreateVendor(AuditVendor newvendor)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.AuditVendors.Add(newvendor);
                    entities.SaveChanges();
                    if (newvendor.ID != 0)
                    {
                        return Convert.ToInt32(newvendor.ID);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static AuditVendor GetVendor(long Id, long customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var getIdVendor = (from row in entities.AuditVendors
                                       where row.Isactive == false
                                       && row.CustomerID == customerID
                                       && row.ID == Id
                                       select row).FirstOrDefault();
                    return getIdVendor;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<AuditVendor> GetVendorListDtls(long customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var vandorList = (from row in entities.AuditVendors
                                      where row.CustomerID == customerID
                                      && row.Isactive == false
                                      select row).ToList();
                    return vandorList;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<AuditVendor> FillVendorDropdown(int CustomerId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var listofvendor = (from row in entities.AuditVendors
                                        where row.CustomerID == CustomerId
                                        && row.Isactive == false
                                        select row).ToList();
                    return listofvendor;
                }
            }
            catch (Exception ex)
            {
                return null;

            }
        }
        #endregion

        #region AuditCategory

        public static void UpdateAuditDetail(long auditDetailID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                AuditDetail eventToUpdate = (from row in entities.AuditDetails
                                             where row.ID == auditDetailID
                                             select row).FirstOrDefault();

                eventToUpdate.AuditCloseFlag = true;
                entities.SaveChanges();

            }
        }
        public static string GetComplianceType(long AuditDetailID, long ChecklistMappingID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditDetails
                             join row1 in entities.AuditCheckListMappings
                             on row.ID equals row1.AuditDetailID
                             join row2 in entities.AuditChecklistComplianceMappings
                             on row1.ChecklistID equals row2.ChecklistID
                             where row2.Isactive == false && row1.Isactive == false
                             && row.Isactive == false
                             && row.ID == AuditDetailID && row1.ID == ChecklistMappingID
                             select row2.ComplianceType).Distinct().FirstOrDefault();
                return query;
            }
        }
        

        public static string GetComplianceType1(long AuditDetailID, long ChecklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditDetails
                             join row1 in entities.AuditCheckListMappings
                             on row.ID equals row1.AuditDetailID
                             join row2 in entities.AuditChecklistComplianceMappings
                             on row1.ChecklistID equals row2.ChecklistID
                             join row3 in entities.AuditChecklists
                             on row2.ChecklistID equals row3.ID
                             where row2.Isactive == false && row1.Isactive == false
                             && row.Isactive == false
                             where row.ID == AuditDetailID && row3.ID == ChecklistID
                             select row2.ComplianceType).Distinct().FirstOrDefault();
                return query;
            }
        }
        public static List<long> GetComplianceInstanceIDList(long AuditDetailID,long ChecklistMappingID,long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditDetails
                             join row1 in entities.AuditCheckListMappings
                             on row.ID equals row1.AuditDetailID
                             join row2 in entities.AuditChecklistComplianceMappings
                             on row1.ChecklistID equals row2.ChecklistID
                             join row3 in entities.Compliances
                             on row2.ComplianceID equals row3.ID
                             join row4 in entities.ComplianceInstances
                             on row3.ID equals row4.ComplianceId
                             join row5 in entities.CustomerBranches  on row4.CustomerBranchID equals row5.ID
                             //on new { ComplianceId = row3.ID, BranchID = row.BranchID } equals
                             //new { ComplianceId = row4.ComplianceId, BranchID = row4.CustomerBranchID }
                             where row2.Isactive == false && row1.Isactive == false
                             && row.Isactive == false
                             where row.ID == AuditDetailID && row1.ID == ChecklistMappingID
                             && row5.CustomerID== CustomerID
                             select row4.ID).ToList();
                return query;
            }
        }

        public static List<long> GetInternalComplianceInstanceIDList(long AuditDetailID, long ChecklistMappingID,long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditDetails
                             join row1 in entities.AuditCheckListMappings
                             on row.ID equals row1.AuditDetailID
                             join row2 in entities.AuditChecklistComplianceMappings
                             on row1.ChecklistID equals row2.ChecklistID
                             join row3 in entities.InternalCompliances
                             on row2.ComplianceID equals row3.ID
                             join row4 in entities.InternalComplianceInstances
                              on row3.ID equals row4.InternalComplianceID
                             join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                             //on new { ComplianceId = row3.ID, BranchID = row.BranchID } equals
                             //new { ComplianceId = row4.InternalComplianceID, BranchID = row4.CustomerBranchID }
                             where row2.Isactive == false && row1.Isactive == false
                             && row.Isactive == false
                             where row.ID == AuditDetailID && row1.ID == ChecklistMappingID
                             && row5.CustomerID== CustomerID
                             select row4.ID).ToList();
                return query;
            }
        }
        public static AuditSampleDocumentData GetLatestAuditSampleDocumentData(long ChecklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditSampleDocumentDatas
                             where row.ChecklistID == ChecklistID && row.IsDeleted == false
                             orderby row.ID descending
                             select row).FirstOrDefault();
                return query;
            }
        }
        public static AuditSampleDocumentData GetSelectedAuditSampleDocumentData(long ID, long ChecklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditSampleDocumentDatas
                             where row.ChecklistID == ChecklistID && row.ID == ID && row.IsDeleted == false
                             select row).FirstOrDefault();
                return query;
            }
        }

        public static List<Auditdetaillist> GetAuditHideData(long ChecklistID,long documentid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditSampleDocumentDatas
                             join row1 in entities.Users
                             on row.CreatedBy equals row1.ID
                             where row.ChecklistID == ChecklistID && row.IsDeleted == false
                             && row.ID != documentid
                             select new Auditdetaillist
                             {
                                 ID = row.ID,
                                 ChecklistID = row.ChecklistID,
                                 Name = row.Name,
                                 UserName = row1.FirstName + "" + row1.LastName,
                                 CreatedOn = row.CreatedOn
                             }).ToList();
                return query;
            }
        }

        public static List<SP_AuditHideDocument_Result> AuditDocument(long checklistid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.SP_AuditHideDocument(checklistid)
                                where row.ChecklistID == checklistid
                                select row).ToList();

                return fileData;
            }
        }

        public static List<Auditdetaillist> GetAuditData(long ChecklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditSampleDocumentDatas
                             join row1 in entities.AuditDetailHideDocuments
                             on row.ID equals row1.FileDataID
                             join row2 in entities.Users
                             on row.CreatedBy equals row2.ID
                             where row.ChecklistID == ChecklistID && row.IsDeleted == false
                             && (row1.IsHide==true || row1.IsHide==false)
                             
                             select new Auditdetaillist
                             {
                                ID=row.ID,
                                ChecklistID =  row.ChecklistID,
                                Name= row.Name,
                               // IsHide=row1.IsHide,
                                UserName = row2.FirstName + "" + row2.LastName,
                                CreatedOn = row.CreatedOn,
                               // IsHide=row1.IsHide,

                             
                             }).ToList();

                
                return query;

              
            }
        }

        public static List<AuditSampleDocumentData> GetAuditSampleDocumentData(long ChecklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditSampleDocumentDatas
                             where row.ChecklistID == ChecklistID && row.IsDeleted == false
                             select row).ToList();
                return query;
            }
        }

        public static List<AuditCriticalDocumentMapping> GetAuditCriticalChecklistDcoumentMapping(long ChecklistID,long FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var checklist = (from row in entities.AuditCriticalDocumentMappings
                                 where row.ChecklistID == ChecklistID && row.FileID == FileID
                                 select row).ToList();

                return checklist;
            }
        }
        public static List<AuditDetailHideDocument> GetAuditDetailHideDocument(long AuditID,long ChecklistID,long FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditDetailHideDocuments
                             where row.AuditID == AuditID && row.ChecklistID == ChecklistID
                             && row.FileDataID == FileID
                             select row).ToList();
                return query;
            }
        }

     
        public static void UpdateObservation(AuditCheckListMapping auditCheckListMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                AuditCheckListMapping AuditCheckListMappingToUpdate = (from row in entities.AuditCheckListMappings
                                                    where row.AuditDetailID == auditCheckListMapping.AuditDetailID
                                                    && row.ID == auditCheckListMapping.ID
                                                    && row.Isactive == false
                                                    select row).FirstOrDefault();

                AuditCheckListMappingToUpdate.Observation = auditCheckListMapping.Observation;
                AuditCheckListMappingToUpdate.UpdatedBy = auditCheckListMapping.UpdatedBy;
                AuditCheckListMappingToUpdate.UpdatedOn = auditCheckListMapping.UpdatedOn;
                entities.SaveChanges();
            }
        }
        public static void AuditCategoryDelete(int categoryid, long updatedbyID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                AuditCategory ltlmToUpdate = (from row in entities.AuditCategories
                                              where row.ID == categoryid
                                              select row).FirstOrDefault();


                ltlmToUpdate.UpdatedBy = updatedbyID;
                ltlmToUpdate.Isactive = true;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static bool AuditCategoryExists(AuditCategory av)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditCategories
                             where row.Isactive == false && row.Name.ToUpper().Trim().Equals(av.Name.ToUpper().Trim())
                              && row.VendorID == av.VendorID
                             select row);

                if (av.ID > 0)
                {
                    query = query.Where(entry => entry.ID != av.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void UpdateAuditCategory(AuditCategory newAuditCategory)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var AuditUpdate = (from row in entities.AuditCategories
                                       where row.ID == newAuditCategory.ID
                                        && row.Isactive == false
                                       select row).FirstOrDefault();
                    if (AuditUpdate != null)

                    {
                        AuditUpdate.Name = newAuditCategory.Name;
                        AuditUpdate.VendorID = newAuditCategory.VendorID;
                        AuditUpdate.Isactive = newAuditCategory.Isactive;
                        AuditUpdate.UpdatedBy = newAuditCategory.UpdatedBy;
                        AuditUpdate.UpdatedOn = newAuditCategory.UpdatedOn;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static long CreateAuditCategory(AuditCategory newAuditCategory)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.AuditCategories.Add(newAuditCategory);
                    entities.SaveChanges();
                    if (newAuditCategory.ID != 0)
                    {
                        return newAuditCategory.ID;

                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static List<ListForAudit> GetAuditCategory(long customerID)
        {

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var AuditList = (from row in entities.AuditCategories
                                     join rows in entities.AuditVendors
                                     on row.VendorID equals rows.ID
                                     where row.CustomerID == customerID
                                     && row.Isactive == false
                                     && rows.Isactive == false
                                     select new ListForAudit
                                     {
                                         VendorName = rows.Name,
                                         AuditCategoryName = row.Name,
                                         Id = row.ID,
                                         IsActive = row.Isactive
                                     }).ToList();
                    return AuditList;
                }

            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public static AuditCategory getdataformACategory(int id, int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getdate = (from row in entities.AuditCategories
                               where row.ID == id
                               && row.Isactive == false
                               && row.CustomerID == CustomerId
                               select row).FirstOrDefault();
                return getdate;

            }
        }
        public class ListForAudit
        {
            public long Id { get; set; }
            public string VendorName { get; set; }
            public string AuditCategoryName { get; set; }
            public bool IsActive { get; set; }
        }

        public class Auditdetaillist
        {
           public long ID { get; set; }
           public long ChecklistID { get; set; }
           public string Name { get; set; }
           public string UserName { get; set; }
           public DateTime CreatedOn { get; set; }
         //  public Boolean ?IsHide { get; set; }
         }

        #endregion
        public static List<object> GetAllAuditor(int customerID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.IsActive == true
                             && row.RoleID == 19
                             && row.CustomerID == customerID
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();

                return users.ToList<object>();
            }
        }

        public static List<object> GetAllVender(long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditVendors
                             where row.CustomerID == CustomerID && row.Isactive == false
                             select row);


                var Vendor = (from row in query
                              select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                return Vendor.ToList<object>();
            }
        }


        public static List<object> GetAllLocation(long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.CustomerID == CustomerID 
                             select row);


                var Vendor = (from row in query
                              select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                return Vendor.ToList<object>();
            }
        }

        public static List<AuditChecklist> GetAuditChecklist(int venderid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Checklist = (from row in entities.AuditChecklists
                                 where row.VendorID == venderid && row.Isactive == false
                                 select row).ToList();
                return Checklist;
            }
        }

        public static bool CheckAuditExists(int CustomerID, string auditName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditDetails
                             where row.AuditName.Equals(auditName) && row.Isactive == false
                             && row.CustomerID == CustomerID
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static void DeleteAuditcomplianceBranchTable(long AuditDetailID, long ChecklistID, long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.AuditComplianceBranchMappings
                           where row.ChecklistID == ChecklistID && row.AuditDetailID == AuditDetailID
                           && row.ComplianceID == complianceID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    AuditComplianceBranchMapping prevmappedids = (from row in entities.AuditComplianceBranchMappings
                                                                  where row.ID == entry
                                                                  select row).FirstOrDefault();
                    entities.AuditComplianceBranchMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }
        }
        public static void CreateAuditComplianceBranchDetails(AuditComplianceBranchMapping auditcompliancebranch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                entities.AuditComplianceBranchMappings.Add(auditcompliancebranch);
                entities.SaveChanges();


                
            }
        }

        public static void CreateAuditDetails(AuditDetail auditDetail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.AuditDetails.Add(auditDetail);
                entities.SaveChanges();
            }
        }

        public static AuditDetail GetAuditDetailByID(long auditID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.AuditDetails
                            where row.ID == auditID
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static void CreateauditCheckListMapping(AuditCheckListMapping auditCheckListMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                entities.AuditCheckListMappings.Add(auditCheckListMapping);
                entities.SaveChanges();
            }
        }


        public static void UpdateauditCheckListMapping(AuditCheckListMapping auditCheckListMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                AuditCheckListMapping auditCheckListMappingToUpdate = (from row in entities.AuditCheckListMappings
                                                                       where row.AuditDetailID == auditCheckListMapping.AuditDetailID && row.Isactive == false
                                                                       && row.ChecklistID == auditCheckListMapping.ChecklistID
                                                                       select row).FirstOrDefault();

                auditCheckListMappingToUpdate.Isactive = auditCheckListMapping.Isactive;
                auditCheckListMappingToUpdate.UpdatedBy = auditCheckListMapping.UpdatedBy;
                auditCheckListMappingToUpdate.UpdatedOn = auditCheckListMapping.UpdatedOn;
                entities.SaveChanges();

            }
        }
        public static void UpdateAuditDetails(AuditDetail auditDetail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                AuditDetail eventToUpdate = (from row in entities.AuditDetails
                                             where row.ID == auditDetail.ID
                                             select row).FirstOrDefault();

                eventToUpdate.AuditName = auditDetail.AuditName;
                //eventToUpdate.AuditorID = auditDetail.AuditorID;
                eventToUpdate.AuditStartDate = auditDetail.AuditStartDate;
                eventToUpdate.AuditEndDate = auditDetail.AuditEndDate;
                //eventToUpdate.VendorID = auditDetail.VendorID;
                eventToUpdate.UpdatedBy = auditDetail.UpdatedBy;
                eventToUpdate.UpdatedOn = auditDetail.UpdatedOn;
                entities.SaveChanges();

            }
        }

        public static bool CheckAuditCheckListMappingExists(int AuditDetailID, int ChecklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditCheckListMappings
                             where row.AuditDetailID == AuditDetailID && row.Isactive == false
                             && row.ChecklistID == ChecklistID
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool CheckAuditCheckListMappingObservationExists(int AuditDetailID, int ChecklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditCheckListMappings
                             where row.AuditDetailID == AuditDetailID && row.Isactive == false
                             && row.ChecklistID == ChecklistID
                             select row.Observation);
                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static string GetAuditCheckListObservation(long AuditDetailID,long ChecklistID,long ChecklistMappingID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Observation = (from row in entities.AuditCheckListMappings
                                 where row.AuditDetailID == AuditDetailID && row.Isactive == false
                                 && row.ChecklistID == ChecklistID && row.ID == ChecklistMappingID
                                 select row.Observation).FirstOrDefault();
                return Observation;
            }
        }

        public static Compliance GetComplianceFromInstance(long InstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ComplianceInstances
                            join row1 in entities.Compliances
                            on row.ComplianceId  equals row1.ID
                            where row.ID == InstanceID
                            select row1).FirstOrDefault();
                return data;
            }
        }

        public static InternalCompliance GetInternalComplianceFromInstance(long InstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.InternalComplianceInstances
                            join row1 in entities.InternalCompliances
                            on row.InternalComplianceID equals row1.ID
                            where row.ID == InstanceID
                            select row1).FirstOrDefault();
                return data;
            }
        }

        public static List<SP_GetAuditChecklistComplianceDocument_Result> AuditChecklistComplianceDocument(long? AuditDetailID, long? CheckListMappingID, DateTime dtFrom, DateTime dtTo, string ComplianceType,long CustomerID,string User)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.SP_GetAuditChecklistComplianceDocument(AuditDetailID, CheckListMappingID, dtFrom, dtTo, ComplianceType, CustomerID, User)
                                select row).ToList();

                return fileData;
            }
        }

        public static List<SP_GetAuditChecklistComplianceDocument_Result> AuditChecklistDocumentPerticular(long? AuditDetailID, long? CheckListMappingID, DateTime dtFrom, DateTime dtTo, string ComplianceType, long CustomerID,int FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.SP_GetAuditChecklistComplianceDocument(AuditDetailID, CheckListMappingID, dtFrom, dtTo, ComplianceType, CustomerID,"MGMT")
                                where row.FileID == FileID
                                select row).ToList();

                return fileData;
            }
        }
    }
}
