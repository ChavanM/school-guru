﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public static class Extensions
    {
        //public static DataTable ToDataTable<T>(this IList<T> data)
        //{
        //    PropertyDescriptorCollection properties =
        //        TypeDescriptor.GetProperties(typeof(T));
        //    DataTable table = new DataTable();
        //    foreach (PropertyDescriptor prop in properties)
        //        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        //    foreach (T item in data)
        //    {

        //        DataRow row = table.NewRow();

        //        foreach (PropertyDescriptor prop in properties)
        //        {
        //            //  row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
        //            if ((prop.Name).Equals("CloseDate"))
        //            {
        //                DateTime tess = Convert.ToDateTime(prop.GetValue(item));
        //                DateTime timeToUse = new DateTime(1900, 1, 1, 00, 00, 00);
        //                if (Convert.ToDateTime(prop.GetValue(item)) == timeToUse)
        //                {
        //                    row[prop.Name] = DBNull.Value;
        //                }
        //                else
        //                {
        //                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
        //                }
        //            }
        //            else
        //            {
        //                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
        //            }
        //        }
        //        table.Rows.Add(row);
        //    }
        //    return table;
        //}
        //public static DateTime ToCleanDateTime(this DateTime dt)
        //{
        //    return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        //}

        //public static DateTime? ToCleanDateTime(this DateTime? dt)
        //{
        //    if (dt.HasValue)
        //    {
        //        return dt.Value.ToCleanDateTime();
        //    }
        //    return dt;
        //}
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

    }
}
