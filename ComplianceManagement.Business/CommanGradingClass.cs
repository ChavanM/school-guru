﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class CommanGradingClass
    {
        public static List<SP_GetGradingReportUsers_Result> GetAllUserCustomerID(string Is_S_I,string Is_DEPT)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_GetGradingReportUsers(Is_S_I, Is_DEPT)
                             select row).ToList();

                return query;
            }
        }
       
        public static List<int> BindYear()
        {
            try
            {
                List<int> master = new List<int>();
                int CurrentYear = DateTime.Now.Year;
                for (int i = 1; i < 10; ++i)
                {
                    master.Add(CurrentYear);
                    CurrentYear = DateTime.Now.AddYears(-i).Year;
                }
                return master.ToList();
            }
            catch (Exception ex)
            {
                InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public static void InsertLog(Exception ex, string ClassName, string FunctionName)
        {
            try
            {
                List<LogMessage> ReminderUserList = new List<LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = ClassName;
                msg.FunctionName = FunctionName;
                msg.CreatedOn = DateTime.Now;
                if (ex != null)
                {
                    msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                    msg.StackTrace = ex.StackTrace;
                }
                else
                {
                    msg.Message = ClassName;
                    msg.StackTrace = ClassName;
                }
                msg.LogLevel = 0;

                if (ex != null)
                {
                    ReminderUserList.Add(new LogMessage() { LogLevel = 0, ClassName = ClassName, FunctionName = FunctionName, Message = ex.Message + "----\r\n" + ex.InnerException, StackTrace = ex.StackTrace, CreatedOn = DateTime.Now });
                }
                else
                {
                    ReminderUserList.Add(new LogMessage() { LogLevel = 0, ClassName = ClassName, FunctionName = FunctionName, Message = ClassName, StackTrace = ClassName, CreatedOn = DateTime.Now });
                }
                CommanGradingClass.InsertLogToDatabase(ReminderUserList);
            }
            catch (Exception)
            {
                throw;

            }
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();
                });
            }

        }
        public static List<int> GetAssignedLocationList(int UserID, int custID, String Role, string statutoryInternal)
        {
            List<int> LocationList = new List<int>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (statutoryInternal == "S")
                {
                    if (Role == "MGMT" || Role == "AUDT" || Role == "HRMGR")
                    {
                        var query = (from row in entities.EntitiesAssignments
                                     where row.UserID == UserID
                                     select row.BranchID).ToList();


                        if (query != null)
                            LocationList = query.Select(a => (int)a).Distinct().ToList();

                    }
                    else
                    {
                        var query = (from row in entities.ComplianceInstances
                                     join row1 in entities.ComplianceAssignments
                                     on row.ID equals row1.ComplianceInstanceID
                                     where row1.UserID == UserID && row1.RoleID == 6
                                     select row.CustomerBranchID).ToList();


                        if (query != null)
                            LocationList = query.Select(a => a).Distinct().ToList();
                    }
                }
                else if (statutoryInternal == "I")
                {
                    if (Role == "MGMT" || Role == "AUDT" || Role == "HRMGR")
                    {
                        var query = (from row in entities.EntitiesAssignmentInternals
                                     where row.UserID == UserID
                                     select row.BranchID).ToList();


                        if (query != null)
                            LocationList = query.Select(a => (int)a).Distinct().ToList();

                    }
                    else
                    {
                        var query = (from row in entities.InternalComplianceInstances
                                     join row1 in entities.InternalComplianceAssignments
                                     on row.ID equals row1.InternalComplianceInstanceID
                                     where row1.UserID == UserID && row1.RoleID == 6
                                     select row.CustomerBranchID).ToList();



                        if (query != null)
                            LocationList = query.Select(a => a).Distinct().ToList();
                    }
                }
                return LocationList;
            }
        }
        public static bool InsertGrading_PreValue(List<GradingPreFillData> GPFD, long cuID, string Is_S_I,long UserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    entities.GradingPreFillDatas.RemoveRange(entities.GradingPreFillDatas.Where(x => x.CustomerID == cuID
                    && x.IsStatutory.ToUpper().Trim() == Is_S_I.ToUpper().Trim()
                    && x.UserID== UserID));

                    entities.SaveChanges();

                    GPFD.ForEach(entry =>
                    {
                        entities.GradingPreFillDatas.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public static bool InsertGrading_PreValue(List<DEPT_GradingPreFillData> GPFD, long cuID, string Is_S_I, long UserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    entities.DEPT_GradingPreFillData.RemoveRange(entities.DEPT_GradingPreFillData.Where(x => x.CustomerID == cuID
                    && x.IsStatutory.ToUpper().Trim() == Is_S_I.ToUpper().Trim()
                    && x.UserID == UserID));

                    entities.SaveChanges();

                    GPFD.ForEach(entry =>
                    {
                        entities.DEPT_GradingPreFillData.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
