﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class Mst_StateCountry
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> CountryID { get; set; }
        public string CountryName { get; set; }
    }

    public class ShowReportCustomField
    {
        public string LabelValue { get; set; }

        public string LabelName { get; set; }

        public string SettlementValue { get; set; }
        public string Interest { get; set; }
        public string Penalty { get; set; }

        public string Total { get; set; }
        public string ProvisionInBook { get; set; }
        public Nullable<bool> IsAllowed { get; set; }
        

    }

    public class CustFieldAndType
    {
        public long ID { get; set; }
        public string Lable { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int TypeID { get; set; }
        public string CaseType { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class HearingDetailReport
    {
        public long CaseInstanceID { get; set; }
        public Nullable<System.DateTime> ResponseDate { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public string CreatedByText { get; set; }

        public Nullable<System.DateTime> NextHearingDate { get; set; }
    }

    public class ResponseDetailReport
    {
        public long CaseInstanceID { get; set; }
        public Nullable<System.DateTime> ResponseDate { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public string CreatedByText { get; set; }

        public Nullable<System.DateTime> NextHearingDate { get; set; }
    }
}
