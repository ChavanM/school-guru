﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Configuration;


namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class CustomerManagement
    {
        public static int GetClientName(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName == "LicenseType"
                            && row.ClientID == customerID
                            select row.ClientID).FirstOrDefault();
                return data;
            }
        }
        public static URL_Customization GetURLCustomization(int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    URL_Customization exists = (from row in entities.URL_Customization
                                                where row.CustID == CustomerID
                                                select row).FirstOrDefault();

                    return exists;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static object FrequencyDetails()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.FrequencyDetails
                             select row);

                return users.ToList();
            }
        }
        public static int? GetByCustID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customer = (from row in entities.Users
                                where row.ID == userID
                                select row.CustomerID).SingleOrDefault();

                return customer;
            }
        }
        
        public static Customer GetByID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customer = (from row in entities.Customers
                                where row.ID == customerID
                                select row).SingleOrDefault();

                return customer;
            }
        }

        public static int GetCustomizedCustomerid(long custid, string Param)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where
                            row.CustomizationName.ToLower() == Param.ToLower()
                            && row.ClientID == custid
                            select row.ClientID).FirstOrDefault();

                return data;
            }
        }

        public static ClientCustomization GetAssignedEntity(string customizationname, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ClientCustomization CustomerToUpdate = (from Row in entities.ClientCustomizations
                                                        where Row.ClientID == customerid
                                                        && Row.CustomizationName == customizationname
                                                        select Row).FirstOrDefault();

                return CustomerToUpdate;
            }
        }

        public static bool CheckForClient(int customerID, string Param)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName.ToLower() == Param.ToLower()
                            && row.ClientID == customerID
                            select row).Count();

                if (data != 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public static int GetCustomizedCustomerid(int custid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName == "TaxReportFields"
                            && row.ClientID == custid
                            select row.ClientID).FirstOrDefault();

                return data;
            }
        }

     
        public static void UpdateCustomerStatus(int custID, int newStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var custRecord = (from cust in entities.Customers
                                  where cust.ID == custID
                                  select cust).FirstOrDefault();

                if (custRecord != null)
                {
                    custRecord.Status = newStatus;
                    entities.SaveChanges();
                }
            }
        }

        public static object FillServiceProvider(int serviceProviderID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false 
                             && row.IsServiceProvider == true
                             select row);

                if (serviceProviderID != -1)
                    query = query.Where(row => row.ID == serviceProviderID);

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static object Fill_ServiceProviders_Distributors(int IsSPDist, int serviceProviderID, int distributorID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                IQueryable<Customer> query = null;

                if (IsSPDist == 0)
                {
                    query = (from row in entities.Customers
                             where row.IsDeleted == false
                             && row.IsServiceProvider == true
                             select row);
                }
                else if (IsSPDist == 1)
                {
                    query = (from row in entities.Customers
                             where row.IsDeleted == false
                             && row.IsDistributor == true
                             select row);
                }
                else if (IsSPDist == 2)
                {
                    query = (from row in entities.Customers
                             where row.IsDeleted == false
                             && (row.IsServiceProvider == true || row.IsDistributor == true)
                             select row);
                }

                if (serviceProviderID != -1)
                    query = query.Where(row => row.ID == serviceProviderID);

                if (distributorID != -1)
                    query = query.Where(row => row.ID == distributorID);

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static List<Customer> GetClient()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             join row1 in entities.KeyDetails
                             on row.ID equals row1.CustomerID
                             where row.IsDeleted == false
                             select row);
                return users.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static bool CustomerDelete(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var exists = (from row in entities.ComplianceInstances
                              join row1 in entities.CustomerBranches
                              on row.CustomerBranchID equals row1.ID
                              where row1.CustomerID == CustomerID
                              select true).FirstOrDefault();

                return !exists;
            }
        }

        public static bool CustomerBranchDelete(int CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var exists = (from row in entities.ComplianceInstances
                             where row.CustomerBranchID == CustomerBranchID
                              select true).FirstOrDefault();

                return !exists;
            }
        }
       
       
        public static List<Customer> GetCustomers()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             where row.IsDeleted == false
                             //&& row.ServiceProviderID == 95
                             && row.ComplianceProductType != 1
                             && row.Status == 1
                             select row);
                return users.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<int> GetAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             where row.IsDeleted == false
                             //&& row.ServiceProviderID == 95
                              && row.ComplianceProductType != 1
                              && row.Status == 1
                             select row.ID);
                return users.ToList();
            }
        }
        public static List<Customer> GetAll(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             where row.IsDeleted == false
                             //&& row.ServiceProviderID == 95
                              && row.ComplianceProductType != 1
                              && row.Status == 1
                             select row);

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.ID == customerID);
                }


                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter) || entry.BuyerContactNumber.Contains(filter));
                }

                return users.OrderBy(entry => entry.Name).ToList();
            }
        }
      
        public static object GetAll(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers 
                             where row.IsDeleted == false 
                             //&& row.ServiceProviderID==95
                             && row.ComplianceProductType !=1
                             && row.Status == 1
                             select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter));
                }

                return users.ToList();
            }
        }
        public static object GetAllCustomer(int userID,string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             join row1 in entities.CustomerAssignmentDetails
                             on row.ID equals row1.CustomerID
                             where row1.UserID==userID
                             && row.IsDeleted == false
                             && row1.IsDeleted == false  
                              //&& row.ServiceProviderID == 95
                              && row.ComplianceProductType != 1
                              && row.Status == 1
                             select row);
          
                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.ToUpper().Contains(filter) || entry.BuyerName.ToUpper().Contains(filter) || entry.BuyerEmail.ToUpper().Contains(filter));
                }   

                return users.Distinct().ToList();
            }
        }
       

        public static List<Customer> GetAll_HRComplianceCustomers(int customerID, int serviceProviderID, int prodType, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 join rccm in entities.RLCS_Customer_Corporate_Mapping
                                 on row.ID equals rccm.AVACOM_CustomerID
                                 where row.IsDeleted == false
                                 //&& row.ComplianceProductType > 0
                                 && row.ComplianceProductType >= prodType
                                 && row.ComplianceProductType != null
                                 select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    customers = customers.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter));
                }

                if (serviceProviderID != -1)
                    customers = customers.Where(entry => entry.ServiceProviderID == serviceProviderID);

                if (customerID != -1)
                    customers = customers.Where(entry => entry.ID == customerID);

                //changed by tejashri-for ascending order----------------

                //if (customers.Count() > 0)
                //    customers = customers.OrderBy(row => row.Name);

                return customers.OrderBy(row => row.Name).ToList();

                //End changed by tejashri-for ascending order----------------
            }
        }

        public static List<SP_RLCS_GetHRComplianceCustomers_Result> GetAll_HRComplianceCustomers(int userID, int customerID, int serviceProviderID, int distributorID, string roleCode, bool showSubDist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.SP_RLCS_GetHRComplianceCustomers(userID, roleCode, customerID, distributorID, serviceProviderID)
                                 select row).ToList();

                if (customers != null)
                    if (customers.Count > 0 && distributorID != -1)
                        if (!showSubDist)
                            customers = customers.Where(entry => (entry.ParentID == distributorID && entry.IsDistributor == false) || entry.ID == distributorID).ToList();

                return customers;
            }
        }

        public static List<Customer> GetAll_HRComplianceCustomersByServiceProviderOrDistributor(int customerID, int serviceProviderID, int distributorID, int prodType, bool showSubDist, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 join rccm in entities.RLCS_Customer_Corporate_Mapping
                                 on row.ID equals rccm.AVACOM_CustomerID
                                 where row.IsDeleted == false
                                 && row.ComplianceProductType >= prodType
                                 //&& row.ComplianceProductType == prodType
                                 && row.ComplianceProductType != null
                                 select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    customers = customers.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter));
                }

                if (serviceProviderID != -1)
                    customers = customers.Where(entry => entry.ServiceProviderID == serviceProviderID || entry.ID == serviceProviderID);

                if (distributorID != -1)
                {
                    if (!showSubDist)
                        customers = customers.Where(entry => (entry.ParentID == distributorID && entry.IsDistributor == false) || entry.ID == distributorID);
                    else
                        customers = customers.Where(entry => entry.ParentID == distributorID || entry.ID == distributorID);
                }

                if (customerID != -1)
                    customers = customers.Where(entry => entry.ID == customerID);                

                if (customers.Count() > 0)
                    customers = customers.OrderBy(row => row.Name);

                return customers.ToList();
            }
        }        
        
        public static List<int> GetCustomerBranchTypeList(long? customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CompTypeList = (from row in entities.CustomerBranches
                                    where row.CustomerID == customerID
                                    && row.Type == 1
                                    select (int)row.ComType).Distinct().ToList();

                return CompTypeList;
            }
        }


        public static object GetComplainceType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complainceType = (from row in entities.ComplianceTypes
                             //where row.IsDeleted == false
                             select row);

                return complainceType.ToList();
            }
        }

        public static object GetComplainceCategory()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complaincecat = (from row in entities.ComplianceCategories
                                      //where row.IsDeleted == false
                                      select row);

                return complaincecat.ToList();
            }
        }


        public static object GetComplainceCategoryInternal()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complaincecat = (from row in entities.InternalCompliancesCategories
                                         //where row.IsDeleted == false
                                     select row);

                return complaincecat.ToList();
            }
        }


        public static object GetComplainceTypeInternal()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complainceType = (from row in entities.InternalComplianceTypes
                                          //where row.IsDeleted == false
                                      select row);

                return complainceType.ToList();
            }
        }


        public static object GetAct()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var act = (from row in entities.Acts
                                      where row.IsDeleted == false
                                      select row);

                return act.ToList();
            }
        }

        public static List<CustomerBranch> GetCustomerBranchList(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CompTypeList = (from row in entities.CustomerBranches
                                    join row1 in entities.Customers 
                                    on row.CustomerID equals row1.ID
                                    join row2 in entities.Users
                                    on row1.ID equals row2.CustomerID
                                    where row2.ID == UserID
                                    && row.Type == 1
                                    select row).ToList();

                return CompTypeList;
            }
        }

        //added by sudarshan for manage custome role wise.
        public static string CustomerGetByIDName(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customer = (from row in entities.Customers
                                where row.ID == customerID
                                select row.Name).FirstOrDefault();

                return customer;
            }
        }
        public static int? GetExpirydays(int customerID,int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerdetails = (from row in entities.Mst_Group
                                       join row1 in entities.mst_UserList_Group
                                       on row.Id equals row1.GroupID
                                       where row.CustomerID == customerID
                                       && row1.UserID == UserID
                                       select row.RememberMe).FirstOrDefault();


                return customerdetails;
            }
        }
        public static int? GetExpirydays(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerdetails = (from row in entities.CustomerwiseCookies
                                       where row.CustomerID == customerID
                                       select row.Expiresdays).SingleOrDefault();


                return customerdetails;
            }
        }
        public static int? GetByTaskApplicableID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customer = (from row in entities.Customers
                                where row.ID == customerID
                                select row.TaskApplicable).FirstOrDefault();

                if (customer == null)
                {
                    return -1;
                }
                else
                {
                    return customer;


                }
            }
        }
        public static int? GetByIcompilanceapplicableID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customer = (from row in entities.Customers
                                where row.ID == customerID
                                select row.IComplianceApplicable).FirstOrDefault();

                if (customer ==null)
                {
                    return -1;
                }
                else
                {
                    return customer;
                    

                }                
            }
        }

        //public static Role GetByID(int roleID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var role = (from row in entities.Roles
        //                    where row.ID == roleID
        //                    select row).SingleOrDefault();

        //        return role;
        //    }
        //}
        public static List<Industry> GetIndustries()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customer = entities.Industries.ToList();

                return customer;
            }
        }

        public static void Delete(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //Customer customerToDelete = new Customer() { ID = customerID };
                //entities.Customers.Attach(customerToDelete);
                Customer customerToDelete = (from row in entities.Customers
                                             where row.ID == customerID
                                             select row).FirstOrDefault();

                customerToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }

        public static bool Exists(Customer customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                                  && row.Name.Equals(customer.Name)
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool IsCustomerActive(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false && row.Status==1 && row.ID == customerID
                             select row);

                if (customerID > 0)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void Update(Customer customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {           
               Customer customerToUpdate = (from row in entities.Customers
                                             where row.ID == customer.ID
                                             select row).FirstOrDefault();

                if (customerToUpdate != null)
                {
                    customerToUpdate.Name = customer.Name;
                    customerToUpdate.Address = customer.Address;
                    customerToUpdate.Industry = customer.Industry;
                    customerToUpdate.BuyerName = customer.BuyerName;
                    customerToUpdate.BuyerContactNumber = customer.BuyerContactNumber;
                    customerToUpdate.BuyerEmail = customer.BuyerEmail;
                    customerToUpdate.StartDate = customer.StartDate;
                    customerToUpdate.EndDate = customer.EndDate;
                    customerToUpdate.DiskSpace = customer.DiskSpace;
                    customerToUpdate.Status = customer.Status;
                    customerToUpdate.LocationType = customer.LocationType;
                    customerToUpdate.IComplianceApplicable = customer.IComplianceApplicable;
                    customerToUpdate.TaskApplicable = customer.TaskApplicable;
                    customerToUpdate.VerticalApplicable = customer.VerticalApplicable;
                    customerToUpdate.IsLabelApplicable = customer.IsLabelApplicable;
                    customerToUpdate.IsServiceProvider = customer.IsServiceProvider;
                    customerToUpdate.ServiceProviderID = customer.ServiceProviderID;
                    customerToUpdate.ComplianceProductType = customer.ComplianceProductType;
                    customerToUpdate.CreatedBy = customer.CreatedBy;

                    if (customer.IsDistributor != null)
                        customerToUpdate.IsDistributor = customer.IsDistributor;

                    if (customer.ParentID != null)
                        customerToUpdate.ParentID = customer.ParentID;

                    if (customer.CanCreateSubDist != null)
                        customerToUpdate.CanCreateSubDist = customer.CanCreateSubDist;

                    if (customer.IsPayment != null)
                        customerToUpdate.IsPayment = customer.IsPayment;

                    entities.SaveChanges();

                    bool userStatus = false;
                    if (customerToUpdate.Status == 1)
                        userStatus = true;

                    UserManagement.UpdateUserStatus(customerToUpdate.ID, userStatus);
                }           
            }
        }

        public static int Create(Customer customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int addid = 0;
                customer.IsDeleted = false;
                customer.CreatedOn = DateTime.UtcNow;
                entities.Customers.Add(customer);
                entities.SaveChanges();
                addid = Convert.ToInt32(customer.ID);
                return addid;
            }
        }
        public static void deleteCustReset(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var tet = entities.Customers.Where(x => x.ID == Userid).FirstOrDefault();
                    entities.Customers.Remove(tet);
                    entities.SaveChanges();
                    Userid--;
                    entities.SP_ResetIdCustomer(Userid);
                }
                catch (Exception ex)
                {
                }
            }
        }

        //public static void Create(Customer customer)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        customer.IsDeleted = false;
        //        customer.CreatedOn = DateTime.UtcNow;

        //        entities.Customers.Add(customer);

        //        entities.SaveChanges();
        //    }
        //}
        public static int GetByName(string customerName)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Customers
                                 where row.Name == customerName
                                 select row);
                    var customerid = query.FirstOrDefault<Customer>();
                    return Convert.ToInt32(customerid.ID);
                }
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public static void UpdateCustomerPhoto(int custID, String FilePath)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Customer CustomerToUpdate = (from Row in entities.Customers
                                             where Row.ID == custID
                                             select Row).FirstOrDefault();

                CustomerToUpdate.LogoPath = FilePath;
                entities.SaveChanges();
            }
        }


        public static List<UserView> GetAllUser(int customerID, int serviceProviderID, int distributorID, int prodType, string filter = null)
        {
            List<UserView> users = new List<UserView>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (serviceProviderID != -1)
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where (cust.ServiceProviderID == serviceProviderID || cust.ID == serviceProviderID)
                             && cust.ComplianceProductType >= prodType
                             select row).ToList();
                }
                else if (distributorID != -1)
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where (cust.ParentID == distributorID || cust.ID == distributorID)
                             && cust.ComplianceProductType >= prodType
                             select row).ToList();
                }
                else
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where cust.ComplianceProductType >= prodType
                              && row.HR_Role != null
                             select row).ToList();
                }

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter)).ToList();
                }

                return users.OrderBy(entry => entry.FirstName).ToList();
            }
        }
        public static List<object> GetAll_RLCSUsers_IncludingServiceProvider(int customerID, int serviceProviderID, int prodType, string filter = null)
        {
            List<UserView> users = new List<UserView>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (serviceProviderID != -1)
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where (cust.ServiceProviderID == serviceProviderID || cust.ID == serviceProviderID)
                             && cust.ComplianceProductType >= prodType
                             && row.HR_Role!=null
                             select row).ToList();
                }
                else
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where cust.ComplianceProductType >= prodType
                             && row.HR_Role != null
                             select row).ToList();
                }

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID || entry.CustomerID == serviceProviderID).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter)).ToList();
                }

                var userList = (from row in users
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).Distinct().ToList<object>();

                return userList;
            }
        }

        public static int? GetServiceProviderID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var serviceProviderID = (from row in entities.Customers
                                where row.ID == customerID
                                select row.ServiceProviderID).FirstOrDefault();

                return serviceProviderID;
            }
        }
        public static bool AddCustomerCorporateMapping(int customerID)
        {
            try
            {
                Customer customer = CustomerManagement.GetByID(customerID);
                if (customer != null)
                {
                    string carporateid = GetCorpID(customer.Name);
                    carporateid += customerID;

                    if (!CheckCorporateID(carporateid))
                    {
                        for (int i = 1; i <= 10; i++)
                        {
                            if (CheckCorporateID(carporateid + i.ToString()))
                            {
                                carporateid = carporateid + i.ToString();
                                break;
                            }
                        }
                    }

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        entities.AddCustomerCorporateMapping(carporateid, customerID);
                    }

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected static string GetCorpID(string corpName)
        {
            corpName = corpName.Replace(" ", "");
            StringBuilder result = new StringBuilder();
            try
            {
                foreach (char c in corpName)
                {
                    if (result.Length < 3)
                    {
                        if (Char.IsLetterOrDigit(c))
                            result.Append(c);
                    }
                }

                return "AVA" + result.ToString().ToUpper();
            }
            catch (Exception ex)
            {
                return result.ToString();
            }
        }

        protected static bool CheckCorporateID(string carporateid)
        {
            try
            {
                if (!string.IsNullOrEmpty(carporateid))
                {
                    string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];

                    rlcsAPIURL += "AventisIntegration/CheckCorporateIdExists?CorporateId=" + carporateid.Trim();

                    string responseData = WebAPIUtility.Invoke("GET", rlcsAPIURL, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        bool corpExists = Convert.ToBoolean(responseData.Trim());

                        if (corpExists)
                        {
                            return false;
                        }
                        else
                        {
                            corpExists = RLCS_Master_Management.Exists_CorporateID(carporateid.Trim());
                            bool clientExists = RLCS_Master_Management.Exists_ClientID(carporateid.Trim());
                            if (corpExists)
                            {
                                return false;
                            }
                            else if (clientExists)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
