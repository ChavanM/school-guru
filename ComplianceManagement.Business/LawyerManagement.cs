﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class LawyerManagement
    {
        public static List<SP_Lawyer_Result> GetLawyerListAllNew(long CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjCountry = (from row in entities.SP_Lawyer()
                                  where row.IsActive == true
                                  && row.CustomerID == CustomerId
                                  select row).ToList();
                return ObjCountry;
            }
        }
        public static object GetLawyerListForMapping(long CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.tbl_Lawyer
                             where row.IsActive == true
                             && row.CustomerID == CustomerId
                             select row).ToList();

                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();
                return Frequency;
            }
        }

        public static List<tbl_Lawyer> GetLawyerListAll(long CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjCountry = (from row in entities.tbl_Lawyer
                                  where row.IsActive == true 
                                  && row.CustomerID== CustomerId
                                  select row);
                return ObjCountry.ToList();
            }
        }

        public static List<tbl_CourtType> FillLawyerTypeList(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objCourtType = (from row in entities.tbl_CourtType
                                    where row.CustomerID == customerID
                                    select row);
                return objCourtType.ToList();
            }
        }

        public static bool LawyerDatacheckExist(string firstName, string lastName, long CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.tbl_Lawyer
                             where row.FirstName.Equals(firstName) && row.LastName.Equals(lastName)
                             && row.CustomerID==CustomerId
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateLawyerData(tbl_Lawyer objlawyer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.tbl_Lawyer.Add(objlawyer);
                entities.SaveChanges();
            }
        }

        public static tbl_Lawyer GetlawyerID(tbl_Lawyer objlawyer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.tbl_Lawyer
                             where row.FirstName == objlawyer.FirstName && row.LastName == objlawyer.LastName
                             && row.ContactNumber == objlawyer.ContactNumber && row.Email == objlawyer.Email
                             && row.CustomerID==objlawyer.CustomerID    
                             select row).FirstOrDefault();
                return Query;

            }
        }

        public static void CreateLawyerMapping(LawyerCourtMapping objCourt)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.LawyerCourtMappings.Add(objCourt);
                entities.SaveChanges();
            }
        }
        public static void UpdatelawyerData(tbl_Lawyer objlawyer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                tbl_Lawyer ObjQuery = (from row in entities.tbl_Lawyer
                                       where row.ID == objlawyer.ID
                                       && row.CustomerID == objlawyer.CustomerID
                                       select row).FirstOrDefault();

                ObjQuery.FirstName = objlawyer.FirstName;
                ObjQuery.LastName = objlawyer.LastName;
                ObjQuery.Email = objlawyer.Email;
                ObjQuery.ContactNumber = objlawyer.ContactNumber;
                ObjQuery.Address = objlawyer.Address;
                ObjQuery.Specilisation = objlawyer.Specilisation;
                ObjQuery.BankDetails = objlawyer.BankDetails;
                ObjQuery.PanCard = objlawyer.PanCard;
                ObjQuery.IsActive = true;
                entities.SaveChanges();
            }
        }
        //public static void UpdatelawyerData(tbl_Lawyer objlawyer)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        tbl_Lawyer ObjQuery = (from row in entities.tbl_Lawyer
        //                               where row.ID == objlawyer.ID
        //                               && row.CustomerID==objlawyer.CustomerID
        //                               select row).FirstOrDefault();

        //        ObjQuery.FirstName = objlawyer.FirstName;
        //        ObjQuery.LastName = objlawyer.LastName;
        //        ObjQuery.Email = objlawyer.Email;
        //        ObjQuery.ContactNumber = objlawyer.ContactNumber;
        //        ObjQuery.Address = objlawyer.Address;
        //        ObjQuery.Specilisation = objlawyer.Specilisation;
        //        ObjQuery.IsActive = true;
        //        entities.SaveChanges();
        //    }
        //}

        public static void UpdateLawMappingData(LawyerCourtMapping objCourt)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                LawyerCourtMapping ObjQuery = (from row in entities.LawyerCourtMappings
                                               where row.LawyerID == objCourt.LawyerID && row.CourtID == objCourt.CourtID
                                               select row).FirstOrDefault();
                if (ObjQuery != null)
                {
                    ObjQuery.IsActive = true;
                    entities.SaveChanges();
                }
            }
        }


        public static void UpdateLawMappingDataFalseAll(LawyerCourtMapping objCourt)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjQuery = (from row in entities.LawyerCourtMappings
                                where row.LawyerID == objCourt.LawyerID
                                select row).ToList();

                if (ObjQuery != null)
                {
                    foreach (var item in ObjQuery)
                    {
                        item.IsActive = false;
                        entities.SaveChanges();
                    }
                }
            }
        }
        
        public static tbl_Lawyer LawyerDataGetByID(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.tbl_Lawyer
                             where row.ID == ID
                             select row).FirstOrDefault();
                return Query;
            }
        }

        public static void DeleteLawyerData(int ID,long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                tbl_Lawyer Query = (from row in entities.tbl_Lawyer
                                    where row.ID == ID && row.CustomerID== customerID
                                    select row).FirstOrDefault();
                if (Query != null)
                {
                    Query.IsActive = false;
                    //entities.Mst_Laywer.Remove(Query);
                    entities.SaveChanges();
                }
            }
        }

        public static List<SP_GetCriteriaMaster_Paging_Result> GetCriteriaDetails(int CustomerID, string txtfilter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var criteriaList = entities.SP_GetCriteriaMaster_Paging(CustomerID, txtfilter).ToList();
                return criteriaList;

            }
        }

        public static bool CheckCriteriaExist(tbl_CriteriaRatingMaster _objcriteriarating)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.tbl_CriteriaRatingMaster
                             where row.Name.Equals(_objcriteriarating.Name)
                             && row.CustomerID == _objcriteriarating.CustomerID
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateCriteriaDetails(tbl_CriteriaRatingMaster _objcriteriarating)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.tbl_CriteriaRatingMaster.Add(_objcriteriarating);
                entities.SaveChanges();

            }
        }

        public static tbl_CriteriaRatingMaster GetCriteriaVal(int iD, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objcriteriadtl = (from row in entities.tbl_CriteriaRatingMaster
                                       where row.IsActive == true && row.ID == iD &&
                                       row.CustomerID == customerID
                                       select row).FirstOrDefault();
                return _objcriteriadtl;
            }
        }

        public static void UpdateCriteriaDetails(tbl_CriteriaRatingMaster _objcriteriarating)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                tbl_CriteriaRatingMaster _Objrating = (from row in entities.tbl_CriteriaRatingMaster
                                                       where row.ID == _objcriteriarating.ID
                                                       && row.CustomerID == _objcriteriarating.CustomerID
                                                       select row).FirstOrDefault();

                _Objrating.Name = _objcriteriarating.Name;
                entities.SaveChanges();
            }
        }

        public static bool DeleteCrtteriaByID(long customerID, int iD)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var _objdelcriiaID = (from row in entities.tbl_CriteriaRatingMaster
                                          where row.CustomerID == customerID
                                          && row.ID == iD
                                          && row.IsActive == true
                                          select row).FirstOrDefault();
                    if (_objdelcriiaID != null)
                    {
                        _objdelcriiaID.IsActive = false;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
