﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class DashboardManagement
    {
        //public static List<SP_GetCheckListInformativeCannedReportCompliancesSummary_Result> DashboardDataPerformerInformative_Checklist(int userID, long Customerid, int risk, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType, int ComplianceType, string queryStringFlag)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<SP_GetCheckListInformativeCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCheckListInformativeCannedReportCompliancesSummary_Result>();

        //        DateTime now = DateTime.UtcNow.Date;
        //        transactionsQuery = (from row in entities.SP_GetCheckListInformativeCannedReportCompliancesSummary(userID, (int)Customerid, "PERF")
        //                             where row.RoleID == 3
        //                             select row).ToList().GroupBy(entity => entity.ComplianceID).Select(entity => entity.FirstOrDefault()).ToList();

        //        //Type Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
        //        }
        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
        //        }
        //        //category Filter
        //        if (ActID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
        //        }
        //        ////Datr Filter
        //        //if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        //{
        //        //    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
        //        //}
        //        //Risk Filter
        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }
        //        // Find data through String contained in Description
        //        if (StringType != "")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }

        //        //if (queryStringFlag != "A")
        //        //{
        //        //    //Check Statutory Checklist
        //        if (ComplianceType == -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == null)).ToList();
        //        }
        //        //    //Check EventBased Checklist
        //        if (ComplianceType == 0)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
        //        }
        //        //}

        //        return transactionsQuery.ToList();
        //    }
        //}

        //public static List<SP_GetCheckListInformativeCannedReportCompliancesSummary_Result> DashboardDataReviewerInformative_Checklist(int userID, long Customerid, int risk, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType, int ComplianceType, string queryStringFlag)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<SP_GetCheckListInformativeCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCheckListInformativeCannedReportCompliancesSummary_Result>();

        //        DateTime now = DateTime.UtcNow.Date;
        //        transactionsQuery = (from row in entities.SP_GetCheckListInformativeCannedReportCompliancesSummary(userID, (int)Customerid, "RVW")
        //                             where row.RoleID == 4
        //                             select row).ToList().GroupBy(entity => entity.ComplianceID).Select(entity => entity.FirstOrDefault()).ToList();

        //        //Type Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
        //        }
        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
        //        }
        //        //category Filter
        //        if (ActID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
        //        }
        //        ////Datr Filter
        //        //if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        //{
        //        //    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
        //        //}
        //        //Risk Filter
        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }
        //        // Find data through String contained in Description
        //        if (StringType != "")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }

        //        //if (queryStringFlag != "A")
        //        //{
        //        //    //Check Statutory Checklist
        //        if (ComplianceType == -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == null)).ToList();
        //        }
        //        //    //Check EventBased Checklist
        //        if (ComplianceType == 0)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
        //        }
        //        //}

        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<Sp_GetCannedReportPerformerAndReviewer_Result> GetlstComplianceAssignments(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.Sp_GetCannedReportPerformerAndReviewer(customerID)
                                         select row).ToList();
                return transactionsQuery;
            }
        }
        public static string GetUserName(List<Sp_GetCannedReportPerformerAndReviewer_Result> masterListComAssign, long complianceinstanceid, int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in masterListComAssign
                                         where row.ComplianceInstanceID == complianceinstanceid
                                         && row.UserID == row.UserID
                                         && row.RoleID == roleID
                                         select row.Uname).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static string GetUserNameForEscalation(long ComplianceID, long CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.ComplianceAssignedInstancesViews
                                         join cm in entities.Users on row.UserID equals cm.ID
                                         where
                                         row.ComplianceID == ComplianceID
                                         && row.RoleID == 3  //
                                         && row.CustomerBranchID == CustomerBranchID
                                         select cm.FirstName + " " + cm.LastName).FirstOrDefault();

                return transactionsQuery;
            }
        }

        public static long GetUserIDPerformerForEscalation(long ComplianceID, int CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.ComplianceAssignedInstancesViews
                                         where row.ComplianceID == ComplianceID
                                         && row.RoleID == 3  //
                                         && row.CustomerBranchID == CustomerBranchID
                                         select (long)row.UserID).FirstOrDefault();

                return transactionsQuery;
            }
        }
        public static string GetUserName(long complianceinstanceid, int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.ComplianceAssignments
                                         where row.ComplianceInstanceID == complianceinstanceid
                                         && row.User.ID == row.UserID
                                         && row.RoleID == roleID
                                         select row.User.FirstName + " " + row.User.LastName).FirstOrDefault();
                return transactionsQuery;
            }
        }      
     
        public static string GetUserNameInternal(long complianceinstanceid, int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.InternalComplianceAssignments
                                         where   row.User.ID == row.UserID &&
                                         row.InternalComplianceInstanceID == complianceinstanceid
                                         && row.RoleID == roleID                                       
                                         select row.User.FirstName + " " + row.User.LastName).FirstOrDefault();                
                return transactionsQuery;
            }
        }
     
        public static List<InternalComplianceInstanceTransactionView> BindGridInternalScheduleOnWise(DateTime scheduleOnIDTemp, int customerID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == customerID && row.UserID == UserID &&
                                         row.InternalScheduledOn == scheduleOnIDTemp
                                         select row).ToList();
                return transactionsQuery;
            }
        }

        public static List<ComplianceInstanceTransactionView> BindGridScheduleOnDateWise(DateTime scheduleOnIDTemp, int customerID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == customerID && row.UserID == UserID &&
                                         row.PerformerScheduledOn == scheduleOnIDTemp
                                         select row).ToList();
                return transactionsQuery;
            }
        }


      

        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerUpcoming(int userID, string flag, CannedReportFilterForPerformer filter)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);


                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case CannedReportFilterForPerformer.Rejected:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 6)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                if (flag == "E")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (flag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }

                return transactionsQuery.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerUpcomingNew(int userID, string queryStringFlag, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int EventID, int EventSchudeOnID, int SubTypeID, string StringType, int UserIDPerformer, DateTime CalenderDate)//15
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                              && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14)
                                              && row.PerformerScheduledOn < nextOneMonth
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                              && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14)
                                              && row.PerformerScheduledOn == CalenderDate
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Overdue")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.PerformerScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {

                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {

                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                if (queryStringFlag == "E")
                {
                    if (EventID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                    if (EventSchudeOnID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (queryStringFlag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                //PerformerID
                if (UserIDPerformer != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == UserIDPerformer)).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<sp_ComplianceInstanceTransaction_Result> DashboardDataForPerformerUpcomingNewSp(int userID, string queryStringFlag, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int EventID, int EventSchudeOnID, int SubTypeID, string StringType, int UserIDPerformer, DateTime CalenderDate)//15
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceInstanceTransaction_Result> transactionsQuery = new List<sp_ComplianceInstanceTransaction_Result>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                              && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14)
                                              && row.PerformerScheduledOn < nextOneMonth
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                              && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14)
                                              && row.PerformerScheduledOn == CalenderDate
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Overdue")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.PerformerScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {

                    transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {

                    transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                if (queryStringFlag == "E")
                {
                    if (EventID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                    if (EventSchudeOnID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (queryStringFlag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                //PerformerID
                if (UserIDPerformer != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == UserIDPerformer)).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }



        //public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerUpcomingNew(int userID, string queryStringFlag, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int EventID, int EventSchudeOnID, int SubTypeID, string StringType, int UserIDPerformer)//15
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        if (status == "Status")
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                  && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14)
        //                                  && row.PerformerScheduledOn < nextOneMonth
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Upcoming")
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Overdue")
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.PerformerScheduledOn < now
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Rejected")
        //        {

        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "PendingForReview")
        //        {

        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }

        //        if (queryStringFlag == "E")
        //        {
        //            if (EventID != -1)
        //                transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

        //            if (EventSchudeOnID != -1)
        //                transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
        //        }
        //        else if (queryStringFlag == "C")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

        //        }

        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
        //        }

        //        //category Filter
        //        if (ActID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
        //        }

        //        //Datr Filter
        //        if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
        //        }

        //        //Risk Filter
        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }

        //        //Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

        //        }

        //        //Type SubType
        //        if (SubTypeID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
        //        }
        //        //PerformerID
        //        if (UserIDPerformer != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == UserIDPerformer)).ToList();
        //        }
        //        // Find data through String contained in Description
        //        if (StringType != "")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}
        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerUpcomingNew(int userID, string queryStringFlag, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int EventID, int EventSchudeOnID, int SubTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Status")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14)
                                          && row.PerformerScheduledOn < nextOneMonth
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 ||row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Overdue")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.PerformerScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {

                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {

                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                if (queryStringFlag == "E")
                {
                    if (EventID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                    if (EventSchudeOnID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (queryStringFlag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                return transactionsQuery.ToList();
            }
        }


        


        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerUpcomingDisplayCountOldDesign(int userID, CannedReportFilterForPerformer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                List<ComplianceInstanceTransactionView> display = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:

                        //transactionsQuery = display.Where(entry =>  (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID && row.EventFlag == null
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID && row.EventFlag == null
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID && row.EventFlag == null
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case CannedReportFilterForPerformer.Rejected:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 6)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardDataForPerformerUpcomingDisplayCount(int userID,
          List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery, CannedReportFilterForPerformer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:

                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
                                             && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 19)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID// && row.EventFlag == null                                              
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 19) && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                              
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case CannedReportFilterForPerformer.Rejected:

                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID                                             
                                             && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerUpcomingDisplayCount(int userID, CannedReportFilterForPerformer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                List<ComplianceInstanceTransactionView> display = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:

                        //transactionsQuery = display.Where(entry =>  (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null 
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID// && row.EventFlag == null 
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null 
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case CannedReportFilterForPerformer.Rejected:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }


        public static List<SP_GetAllActivatedEvent_Result> DashboardDataActiveEventCount(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                int eventRoleID = 10;              
                var transactionsQuery = entities.SP_GetAllActivatedEvent(eventRoleID, userID, Convert.ToInt32(UserManagement.GetByID(userID).CustomerID));

                return transactionsQuery.ToList();
            }
        }

        public static List<Event_Assigned_View> DashboardDataAssignedEventCount(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {               
                int eventRoleID = 10;
                var transactionsQuery = EventManagement.GetAllAssignedInstancesCount(eventRoleID, -1, userID, Convert.ToInt32(UserManagement.GetByID(userID).CustomerID));

                return transactionsQuery.ToList();
            }
        }


        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerOverDue(int userID, string flag, CannedReportFilterForPerformer filter)
        {
            #region Previous Code
            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{

            //    int performerRoleID = (from row in entities.Roles
            //                           where row.Code == "PERF"
            //                           select row.ID).Single();

            //    DateTime now = DateTime.UtcNow.Date;
            //    DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
            //    var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
            //                             where row.UserID == userID && row.RoleID == performerRoleID
            //                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)//chnage by rahul on 10 March 2016
            //                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());


            //    switch (filter)
            //    {
            //        case CannedReportFilterForPerformer.Upcoming:
            //            transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 10));
            //            break;
            //        case CannedReportFilterForPerformer.Overdue:
            //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now);
            //            break;
            //        case CannedReportFilterForPerformer.PendingForApproval:
            //            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3);
            //            break;
            //    }
            //    //var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).Select(entry => entry.FirstOrDefault()).ToList();//Change by rahul on 12 May becoz timeout error
            //    var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();        
            //    List<ComplianceInstanceTransactionView> newList = new List<ComplianceInstanceTransactionView>();
            //    foreach (var loc in locations)
            //    {
            //        ComplianceInstanceTransactionView BranchRow = new ComplianceInstanceTransactionView();
            //        BranchRow.ShortDescription = loc.Branch;
            //        BranchRow.Description = loc.Branch;
            //        BranchRow.ComplianceInstanceID = -1;
            //        BranchRow.UserID = -1;
            //        BranchRow.RoleID = -1;
            //        BranchRow.ComplianceStatusID = -1;
            //        newList.Add(BranchRow);

            //        List<ComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderByDescending(entry => entry.ScheduledOn).ToList();
            //        foreach (var item1 in temptransaction)
            //        {
            //            newList.Add(item1);
            //        }
            //    }
            //    return newList.ToList();
            //}
            #endregion

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 10)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 10) && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                if (flag == "E")
                {
                    //transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true).ToList();
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (flag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }
                var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();
                List<ComplianceInstanceTransactionView> newList = new List<ComplianceInstanceTransactionView>();
                foreach (var loc in locations)
                {
                    ComplianceInstanceTransactionView BranchRow = new ComplianceInstanceTransactionView();
                    BranchRow.ShortDescription = loc.Branch;
                    BranchRow.Description = loc.Branch;
                    BranchRow.ComplianceInstanceID = -1;
                    BranchRow.UserID = -1;
                    BranchRow.RoleID = -1;
                    BranchRow.ComplianceStatusID = -1;
                    newList.Add(BranchRow);
                    List<ComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderByDescending(entry => entry.ScheduledOn).ToList();
                    foreach (var item1 in temptransaction)
                    {
                        newList.Add(item1);
                    }
                }
                return newList.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerOverDueDisplayCountOldDesign(int userID, CannedReportFilterForPerformer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID && row.EventFlag == null
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID && row.EventFlag == null
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID && row.EventFlag == null
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardDataForPerformerOverDueDisplayCount(int userID,
             List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery, CannedReportFilterForPerformer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 19)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        //transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                        //                     where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
                        //                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn < now
                        //                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        transactionsQuery = (from row in MastertransactionsQuery
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 19) && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformer(int userID, string flag, CannedReportFilterForPerformer filter)
        {
            #region Previous Code
            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{

            //    int performerRoleID = (from row in entities.Roles
            //                           where row.Code == "PERF"
            //                           select row.ID).Single();

            //    DateTime now = DateTime.UtcNow.Date;
            //    DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

            //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
            //                         where row.UserID == userID && row.RoleID == performerRoleID
            //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
            //                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
            //switch (filter)
            //{
            //    case CannedReportFilterForPerformer.Upcoming:
            //        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 10));
            //        break;
            //    case CannedReportFilterForPerformer.Overdue:
            //        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now);
            //        break;
            //    case CannedReportFilterForPerformer.PendingForApproval:
            //        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3);
            //        break;
            //}
            //    var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();

            //    List<ComplianceInstanceTransactionView> newList = new List<ComplianceInstanceTransactionView>();
            //    foreach (var loc in locations)
            //    {
            //        ComplianceInstanceTransactionView BranchRow = new ComplianceInstanceTransactionView();

            //        BranchRow.ShortDescription = loc.Branch;
            //        BranchRow.Description = loc.Branch;
            //        BranchRow.ComplianceInstanceID = -1;
            //        BranchRow.UserID = -1;
            //        BranchRow.RoleID = -1;
            //        BranchRow.ComplianceStatusID = -1;
            //        newList.Add(BranchRow);

            //        List<ComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderByDescending(entry => entry.ScheduledOn).ToList();
            //        foreach (var item1 in temptransaction)
            //        {
            //            newList.Add(item1);
            //        }
            //    }

            //    return newList.ToList();
            //}
            #endregion

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 10)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 10)
                                             && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                if (flag == "E")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true).ToList();
                }
                else if (flag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }
                var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();
                List<ComplianceInstanceTransactionView> newList = new List<ComplianceInstanceTransactionView>();
                foreach (var loc in locations)
                {
                    ComplianceInstanceTransactionView BranchRow = new ComplianceInstanceTransactionView();
                    BranchRow.ShortDescription = loc.Branch;
                    BranchRow.Description = loc.Branch;
                    BranchRow.ComplianceInstanceID = -1;
                    BranchRow.UserID = -1;
                    BranchRow.RoleID = -1;
                    BranchRow.ComplianceStatusID = -1;
                    newList.Add(BranchRow);
                    List<ComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderByDescending(entry => entry.ScheduledOn).ToList();
                    foreach (var item1 in temptransaction)
                    {
                        newList.Add(item1);
                    }
                }
                return newList.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerDisplayCountOldDesign(int userID, CannedReportFilterForPerformer filter)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID && row.EventFlag == null
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID && row.EventFlag == null
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10)
                                             && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID && row.EventFlag == null
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                }
                return transactionsQuery.ToList();
            }
        }


        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardDataForPerformerDisplayCount(int userID,
           List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> mastertransactionsQuery, CannedReportFilterForPerformer filter)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in mastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                              
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in mastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                              
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 13)
                                             && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in mastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID // && row.EventFlag == null                                              
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12
                                             || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForReviewer(int userID, string flag, CannedReportFilterForPerformer filter, bool pending = false)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 1 && row.ScheduledOn <= now)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (filter == CannedReportFilterForPerformer.Rejected)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 6)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && row.ScheduledOn <= nextOneMonth
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (flag == "E")
                {
                    // transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true).ToList();
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (flag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }
                var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();
                List<ComplianceInstanceTransactionView> newList = new List<ComplianceInstanceTransactionView>();
                foreach (var loc in locations)
                {
                    ComplianceInstanceTransactionView BranchRow = new ComplianceInstanceTransactionView();
                    BranchRow.ShortDescription = loc.Branch;
                    BranchRow.Description = loc.Branch;
                    BranchRow.ComplianceInstanceID = -1;
                    BranchRow.UserID = -1;
                    BranchRow.RoleID = -1;
                    BranchRow.ComplianceStatusID = -1;
                    newList.Add(BranchRow);
                    List<ComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderBy(entry => entry.ScheduledOn).ToList();
                    foreach (var item1 in temptransaction)
                    {
                        newList.Add(item1);
                    }
                }
                return newList.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForReviewerNew(int userID, string queryStringFlag, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int EventID, int EventSchudeOnID, int SubTypeID, int PerformerID, string StringType, DateTime CalenderDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false) && row.ScheduledOn <= nextOneMonth
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false) && row.ScheduledOn == CalenderDate
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && row.ScheduledOn <= now
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (queryStringFlag == "E")
                {

                    if (EventID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                    if (EventSchudeOnID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (queryStringFlag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                if (PerformerID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == PerformerID)).ToList();
                }
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }


        public static List<sp_ComplianceInstanceTransaction_Result> DashboardDataForReviewerNewSp(int userID, string queryStringFlag, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int EventID, int EventSchudeOnID, int SubTypeID, int PerformerID, string StringType, DateTime CalenderDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceInstanceTransaction_Result> transactionsQuery = new List<sp_ComplianceInstanceTransaction_Result>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn <= nextOneMonth
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn == CalenderDate
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true 
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.sp_ComplianceInstanceTransaction(userID)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true 
                                         && row.ScheduledOn <= now
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (queryStringFlag == "E")
                {

                    if (EventID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                    if (EventSchudeOnID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (queryStringFlag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                if (PerformerID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == PerformerID)).ToList();
                }
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }


        //public static List<ComplianceInstanceTransactionView> DashboardDataForReviewerNew(int userID, string queryStringFlag, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int EventID, int EventSchudeOnID, int SubTypeID, int PerformerID, string StringType)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();

        //        var reviewerRoleIDs = (from row in entities.Roles
        //                               where row.Code.StartsWith("RVW")
        //                               select row.ID).ToList();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
        //        if (status == "Status")
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false) && row.ScheduledOn <= nextOneMonth
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "DueButNotSubmitted")
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.ScheduledOn <= now
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Rejected")
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                  && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "PendingForReview")
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                  && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Upcoming")
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                 && row.ScheduledOn <= now
        //                                  && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        if (queryStringFlag == "E")
        //        {

        //            if (EventID != -1)
        //                transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

        //            if (EventSchudeOnID != -1)
        //                transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
        //        }
        //        else if (queryStringFlag == "C")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

        //        }

        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
        //        }

        //        //category Filter
        //        if (ActID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
        //        }

        //        //Datr Filter
        //        if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
        //        }

        //        //Risk Filter
        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }

        //        //Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

        //        }

        //        //Type SubType
        //        if (SubTypeID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
        //        }
        //        if (PerformerID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == PerformerID)).ToList();
        //        }
        //        if (StringType != "")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}



        public static List<ComplianceInstanceTransactionView> DashboardDataForReviewerNew(int userID, string queryStringFlag, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int EventID, int EventSchudeOnID, int SubTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (status == "Status")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false) && row.ScheduledOn <= nextOneMonth
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && row.ScheduledOn <= now
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (queryStringFlag == "E")
                {

                    if (EventID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                    if (EventSchudeOnID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (queryStringFlag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                return transactionsQuery.ToList();
            }
        }


        //Change Sachin 28 Feb 2018
        public static List<ComplianceInstanceTransactionView> DashboardDataForReviewerDisplayCountOldDesign(int userID, CannedReportFilterForPerformer filter, bool pending = false)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) && row.EventFlag == null
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due but Not Submitted
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) && row.EventFlag == null 
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (filter == CannedReportFilterForPerformer.Rejected)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 6) && row.EventFlag == null
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) && row.EventFlag == null
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && row.ScheduledOn <= nextOneMonth
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                return transactionsQuery.ToList();
            }
        }


        //public static List<ComplianceInstanceTransactionView> DashboardDataForReviewerDisplayCountOldDesign(int userID, CannedReportFilterForPerformer filter, bool pending = false)
        //{

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
        //        var reviewerRoleIDs = (from row in entities.Roles
        //                               where row.Code.StartsWith("RVW")
        //                               select row.ID).ToList();

        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        if (filter == CannedReportFilterForPerformer.PendingForReview)
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) && row.EventFlag == null
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                  && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (pending)
        //        {
        //            //Due but Not Submitted
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null 
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn <= now
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (filter == CannedReportFilterForPerformer.Rejected)
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                 && (row.ComplianceStatusID == 6)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                 && row.ScheduledOn <= nextOneMonth
        //                                  && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardDataForReviewerDisplayCount(int userID,
         List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery, CannedReportFilterForPerformer filter, bool pending = false)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due but Not Submitted
                    //transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                    //                     where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null 
                    //                   //  && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                    //                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn <= now
                    //                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                    transactionsQuery = (from row in MastertransactionsQuery
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 19) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (filter == CannedReportFilterForPerformer.Rejected)
                {
                    transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && row.ScheduledOn <= nextOneMonth
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                return transactionsQuery.ToList();
            }
        }

        //public static List<ComplianceInstanceTransactionView> DashboardDataForReviewerDisplayCount(int userID, CannedReportFilterForPerformer filter, bool pending = false)
        //{

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
        //        var reviewerRoleIDs = (from row in entities.Roles
        //                               where row.Code.StartsWith("RVW")
        //                               select row.ID).ToList();

        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        if (filter == CannedReportFilterForPerformer.PendingForReview)
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                  && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (pending)
        //        {
        //            //Due but Not Submitted
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null 
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn <= now
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (filter == CannedReportFilterForPerformer.Rejected)
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                 && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null
        //                                 && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                 && row.ScheduledOn <= nextOneMonth
        //                                  && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<OverdueComplianceDashboard> DashboardDataForReviewer1(int userID, CannedReportFilterForPerformer filter, bool pending = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                List<OverdueComplianceDashboard> newList = new List<OverdueComplianceDashboard>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();


                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);


                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                    //where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                    //&& (row.IsActive != false || row.IsUpcomingNotDeleted != false)//chnage by rahul on 10 March 2016
                    //select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                    var transactionsQuery = entities.ComplianceInstanceTransactionViews.AsEnumerable()
                            .Where(entry => entry.UserID == userID
                            && reviewerRoleIDs.Contains((int) entry.RoleID)
                            && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                            && entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5

                            )
                            .Select(r => new
                            {
                                r.User,
                                r.RoleID,
                                r.Role,
                                r.ComplianceInstanceID,
                                r.CustomerBranchID,
                                r.CustomerID,
                                r.Branch,
                                r.ShortDescription,
                                r.ScheduledOn,
                                r.Description,
                                r.NonComplianceType,
                                r.Risk,
                                r.ComplianceID,
                                r.FileName,
                                r.FilePath,
                                r.ForMonth,
                                r.Status,
                                r.EventID,
                                r.EventName,
                                r.UserID,
                                r.ScheduledOnID,
                                r.ComplianceStatusID,
                                r.ReferenceMaterialText

                            })
                            .GroupBy(x => new { x.ScheduledOnID })
                            .Select(g => new OverdueComplianceDashboard()
                            {
                                ComplianceInstanceID = g.First().ComplianceInstanceID,
                                CustomerBranchID = g.First().CustomerBranchID,
                                Description = g.First().Description,
                                NonComplianceType = g.First().NonComplianceType,
                                Risk = g.First().Risk,
                                ComplianceID = g.First().ComplianceID,
                                CustomerID = g.First().CustomerID,
                                FileName = g.First().FileName,
                                FilePath = g.First().FilePath,
                                ForMonth = g.First().ForMonth,
                                ScheduledOn = g.First().ScheduledOn,
                                ShortDescription = g.First().ShortDescription,
                                Status = g.First().Status,
                                EventID = g.First().EventID,
                                EventName = g.First().EventName,
                                CustomerBranchName = g.First().Branch,
                                ReferenceMaterialText = g.First().ReferenceMaterialText,
                                ComplianceStatusID = g.First().ComplianceStatusID,
                                UserID = g.First().UserID,
                                Performer = g.Where(n => n.RoleID == 3).Select(x => x.User).FirstOrDefault(),
                                Reviewer = g.Where(n => n.RoleID == 4).Select(x => x.User).FirstOrDefault(),
                                Approver = g.Where(n => n.RoleID == 6).Select(x => x.User).FirstOrDefault(),
                                RoleID = g.First().RoleID,
                            }).ToList();


                    //Change by SACHIN 17 MAY 2016
                    //var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).Select(entry => entry.FirstOrDefault()).ToList();
                    var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();

                    foreach (var loc in locations)
                    {
                        OverdueComplianceDashboard BranchRow = new OverdueComplianceDashboard();

                        BranchRow.ShortDescription = loc.Branch;
                        BranchRow.Description = loc.Branch;
                        BranchRow.ComplianceInstanceID = -1;
                        BranchRow.UserID = -1;
                        BranchRow.RoleID = -1;
                        BranchRow.ComplianceStatusID = -1;
                        newList.Add(BranchRow);

                        List<OverdueComplianceDashboard> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderBy(entry => entry.ScheduledOn).ToList();
                        foreach (var item1 in temptransaction)
                        {
                            newList.Add(item1);
                        }
                    }
                }
                else if (pending)
                {
                    var transactionsQuery = entities.ComplianceInstanceTransactionViews.AsEnumerable()
                           .Where(entry => entry.UserID == userID
                           && reviewerRoleIDs.Contains((int) entry.RoleID)
                           && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                           && entry.ComplianceStatusID == 1 && entry.ScheduledOn <= now

                           )
                           .Select(r => new
                           {
                               r.User,
                               r.RoleID,
                               r.Role,
                               r.ComplianceInstanceID,
                               r.CustomerBranchID,
                               r.CustomerID,
                               r.Branch,
                               r.ShortDescription,
                               r.ScheduledOn,
                               r.Description,
                               r.NonComplianceType,
                               r.Risk,
                               r.ComplianceID,
                               r.FileName,
                               r.FilePath,
                               r.ForMonth,
                               r.Status,
                               r.EventID,
                               r.EventName,
                               r.UserID,
                               r.ScheduledOnID,
                               r.ComplianceStatusID,
                               r.ReferenceMaterialText

                           })
                           .GroupBy(x => new { x.ScheduledOnID })
                           .Select(g => new OverdueComplianceDashboard()
                           {
                               ComplianceInstanceID = g.First().ComplianceInstanceID,
                               CustomerBranchID = g.First().CustomerBranchID,
                               Description = g.First().Description,
                               NonComplianceType = g.First().NonComplianceType,
                               Risk = g.First().Risk,
                               ComplianceID = g.First().ComplianceID,
                               CustomerID = g.First().CustomerID,
                               FileName = g.First().FileName,
                               FilePath = g.First().FilePath,
                               ForMonth = g.First().ForMonth,
                               ScheduledOn = g.First().ScheduledOn,
                               ShortDescription = g.First().ShortDescription,
                               Status = g.First().Status,
                               EventID = g.First().EventID,
                               EventName = g.First().EventName,
                               CustomerBranchName = g.First().Branch,
                               ReferenceMaterialText = g.First().ReferenceMaterialText,
                               UserID = g.First().UserID,
                               ComplianceStatusID = g.First().ComplianceStatusID,
                               Performer = g.Where(n => n.RoleID == 3).Select(x => x.User).FirstOrDefault(),
                               Reviewer = g.Where(n => n.RoleID == 4).Select(x => x.User).FirstOrDefault(),
                               Approver = g.Where(n => n.RoleID == 6).Select(x => x.User).FirstOrDefault(),
                               RoleID = g.First().RoleID,
                           }).ToList();



                    var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).Select(entry => entry.FirstOrDefault()).ToList();
                    foreach (var loc in locations)
                    {
                        OverdueComplianceDashboard BranchRow = new OverdueComplianceDashboard();

                        BranchRow.ShortDescription = loc.Branch;
                        BranchRow.Description = loc.Branch;
                        BranchRow.ComplianceInstanceID = -1;
                        BranchRow.UserID = -1;
                        BranchRow.RoleID = -1;
                        BranchRow.ComplianceStatusID = -1;
                        newList.Add(BranchRow);

                        List<OverdueComplianceDashboard> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderBy(entry => entry.ScheduledOn).ToList();
                        foreach (var item1 in temptransaction)
                        {
                            newList.Add(item1);
                        }
                    }
                    //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn <= now);
                }
                else
                {
                    var transactionsQuery = entities.ComplianceInstanceTransactionViews.AsEnumerable()
                           .Where(entry => entry.UserID == userID
                           && reviewerRoleIDs.Contains((int) entry.RoleID)
                           && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                          && entry.ScheduledOn <= nextOneMonth && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)

                           )
                           .Select(r => new
                           {
                               r.User,
                               r.RoleID,
                               r.Role,
                               r.ComplianceInstanceID,
                               r.CustomerBranchID,
                               r.CustomerID,
                               r.Branch,
                               r.ShortDescription,
                               r.ScheduledOn,
                               r.Description,
                               r.NonComplianceType,
                               r.Risk,
                               r.ComplianceID,
                               r.FileName,
                               r.FilePath,
                               r.ForMonth,
                               r.Status,
                               r.EventID,
                               r.EventName,
                               r.UserID,
                               r.ScheduledOnID,
                               r.ComplianceStatusID,
                               r.ReferenceMaterialText

                           })
                           .GroupBy(x => new { x.ScheduledOnID })
                           .Select(g => new OverdueComplianceDashboard()
                           {
                               ComplianceInstanceID = g.First().ComplianceInstanceID,
                               CustomerBranchID = g.First().CustomerBranchID,
                               Description = g.First().Description,
                               NonComplianceType = g.First().NonComplianceType,
                               Risk = g.First().Risk,
                               ComplianceID = g.First().ComplianceID,
                               CustomerID = g.First().CustomerID,
                               FileName = g.First().FileName,
                               FilePath = g.First().FilePath,
                               ForMonth = g.First().ForMonth,
                               ScheduledOn = g.First().ScheduledOn,
                               ShortDescription = g.First().ShortDescription,
                               Status = g.First().Status,
                               EventID = g.First().EventID,
                               EventName = g.First().EventName,
                               CustomerBranchName = g.First().Branch,
                               ReferenceMaterialText = g.First().ReferenceMaterialText,
                               UserID = g.First().UserID,
                               ComplianceStatusID = g.First().ComplianceStatusID,
                               Performer = g.Where(n => n.RoleID == 3).Select(x => x.User).FirstOrDefault(),
                               Reviewer = g.Where(n => n.RoleID == 4).Select(x => x.User).FirstOrDefault(),
                               Approver = g.Where(n => n.RoleID == 6).Select(x => x.User).FirstOrDefault(),
                               RoleID = g.First().RoleID,
                           }).ToList();



                    var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).Select(entry => entry.FirstOrDefault()).ToList();
                    foreach (var loc in locations)
                    {
                        OverdueComplianceDashboard BranchRow = new OverdueComplianceDashboard();

                        BranchRow.ShortDescription = loc.Branch;
                        BranchRow.Description = loc.Branch;
                        BranchRow.ComplianceInstanceID = -1;
                        BranchRow.UserID = -1;
                        BranchRow.RoleID = -1;
                        BranchRow.ComplianceStatusID = -1;
                        newList.Add(BranchRow);

                        List<OverdueComplianceDashboard> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderBy(entry => entry.ScheduledOn).ToList();
                        foreach (var item1 in temptransaction)
                        {
                            newList.Add(item1);
                        }
                    }
                    // transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn <= nextOneMonth && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11));
                }
                return newList.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForApprover(int userID, string flag, CannedReportFilterForPerformer filter)
        {
            #region Previous Code
            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{
            //    int approverRoleID = (from row in entities.Roles
            //                          where row.Code == "APPR"
            //                          select row.ID).Single();

            //    DateTime now = DateTime.UtcNow.Date;
            //    DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
            //    var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
            //                             where row.UserID == userID && row.RoleID == approverRoleID
            //                            && (row.IsActive != false || row.IsUpcomingNotDeleted != false)//chnage by rahul on 10 March 2016
            //                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

            //    switch (filter)
            //    {
            //        case CannedReportFilterForPerformer.Upcoming:
            //            transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 6));
            //            break;
            //        case CannedReportFilterForPerformer.Overdue:
            //            transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn < now && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 6));
            //            break;
            //        case CannedReportFilterForPerformer.PendingForApproval:
            //            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID==4 || entry.ComplianceStatusID==5);
            //            break;
            //    }

            //    //Change by SACHIN 18 MAY 2016
            //    //var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).Select(entry => entry.FirstOrDefault()).ToList();
            //    var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();

            //    List<ComplianceInstanceTransactionView> newList = new List<ComplianceInstanceTransactionView>();
            //    foreach (var loc in locations)
            //    {
            //        ComplianceInstanceTransactionView BranchRow = new ComplianceInstanceTransactionView();
            //        BranchRow.ShortDescription = loc.Branch;
            //        BranchRow.Description = loc.Branch;
            //        BranchRow.ComplianceInstanceID = -1;
            //        BranchRow.UserID = -1;
            //        BranchRow.RoleID = -1;
            //        BranchRow.ComplianceStatusID = -1;
            //        newList.Add(BranchRow);

            //        List<ComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderByDescending(entry => entry.ScheduledOn).ToList();
            //        foreach (var item1 in temptransaction)
            //        {
            //            newList.Add(item1);
            //        }
            //    }
            //    return newList.ToList();
            //}
            #endregion

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                int approverRoleID = (from row in entities.Roles
                                      where row.Code == "APPR"
                                      select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == approverRoleID
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 6)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == approverRoleID
                                            && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                            && row.ScheduledOn < now && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 6)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == approverRoleID
                                            && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                            && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                if (flag == "E")
                {
                    //transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true).ToList();
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();

                }
                else if (flag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }
                var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();

                List<ComplianceInstanceTransactionView> newList = new List<ComplianceInstanceTransactionView>();
                foreach (var loc in locations)
                {
                    ComplianceInstanceTransactionView BranchRow = new ComplianceInstanceTransactionView();
                    BranchRow.ShortDescription = loc.Branch;
                    BranchRow.Description = loc.Branch;
                    BranchRow.ComplianceInstanceID = -1;
                    BranchRow.UserID = -1;
                    BranchRow.RoleID = -1;
                    BranchRow.ComplianceStatusID = -1;
                    newList.Add(BranchRow);

                    List<ComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderByDescending(entry => entry.ScheduledOn).ToList();
                    foreach (var item1 in temptransaction)
                    {
                        newList.Add(item1);
                    }
                }
                return newList.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForApproverDisplayCount(int userID, CannedReportFilterForPerformer filter)
        {
            #region Previous Code
            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{
            //    int approverRoleID = (from row in entities.Roles
            //                          where row.Code == "APPR"
            //                          select row.ID).Single();
            //    DateTime now = DateTime.UtcNow.Date;
            //    DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
            //    var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
            //                             where row.UserID == userID && row.RoleID == approverRoleID
            //                            && (row.IsActive != false || row.IsUpcomingNotDeleted != false)//chnage by rahul on 10 March 2016
            //                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
            //    switch (filter)
            //    {
            //        case CannedReportFilterForPerformer.Upcoming:
            //            transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 6));
            //            break;
            //        case CannedReportFilterForPerformer.Overdue:
            //            transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn < now && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 6));
            //            break;
            //        case CannedReportFilterForPerformer.PendingForApproval:
            //            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5);
            //            break;
            //    }
            //    return transactionsQuery.ToList();
            //}
            #endregion
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                int approverRoleID = (from row in entities.Roles
                                      where row.Code == "APPR"
                                      select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == approverRoleID
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 6)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == approverRoleID
                                            && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                            && row.ScheduledOn < now && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 6)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == approverRoleID
                                            && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                            && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }

        public static DataTable PerformanceSummaryforPerformer(int Customerid, int userID, PerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, int branchId = -1, PerformanceSummaryForPerformer subFilter = PerformanceSummaryForPerformer.Risk, bool isReportee = false, long ReviewerID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DataTable table = new DataTable();

                switch (filter)
                {
                    case PerformanceSummaryForPerformer.Risk:

                        IEnumerable<dynamic> transactionsQuery;

                        if (isReportee)
                        {

                            var instanceIds = ComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                            transactionsQuery = GetComplianceDashboardSummary(Customerid).ToList()
                                                .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate && instanceIds.Contains(entry.ComplianceInstanceID))
                                                .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                        }
                        else
                        {
                            transactionsQuery = GetComplianceDashboardSummary(Customerid).ToList()
                                               .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                               .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                        }

                        table = GetPerformanceSummaryRiskWise(transactionsQuery, "PERF");

                        break;

                    case PerformanceSummaryForPerformer.Category:

                        IEnumerable<dynamic> transactionsQueryforCatagory;
                        if (isReportee)
                        {

                            var instanceIds = ComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                            transactionsQueryforCatagory = GetComplianceDashboardSummary(Customerid).ToList()
                                                            .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate && instanceIds.Contains(entry.ComplianceInstanceID))
                                                            .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                        }
                        else
                        {
                            transactionsQueryforCatagory = GetComplianceDashboardSummary(Customerid).ToList()
                                                            .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                                            .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                        }

                        table = GetPerformanceSummaryCatagoryWise(transactionsQueryforCatagory, "PERF");

                        break;
                    case PerformanceSummaryForPerformer.Location:

                        if (subFilter == PerformanceSummaryForPerformer.Risk)
                        {
                            if (branchId != -1)
                            {
                                IEnumerable<dynamic> transactionsQueryforLocation;
                                if (isReportee)
                                {

                                    var instanceIds = ComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                                    transactionsQueryforLocation = GetComplianceDashboardSummary(Customerid).ToList()
                                                                   .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate && entry.CustomerBranchID == branchId
                                                                   && instanceIds.Contains(entry.ComplianceInstanceID))
                                                                   .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                                }
                                else
                                {
                                    transactionsQueryforLocation = GetComplianceDashboardSummary(Customerid).ToList()
                                                                   .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate && entry.CustomerBranchID == branchId)
                                                                   .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                                }

                                table = GetPerformanceSummaryRiskWise(transactionsQueryforLocation, "PERF");
                            }
                            else
                            {

                                IEnumerable<dynamic> transactionsQueryforLocation;
                                if (isReportee)
                                {

                                    var instanceIds = ComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                                    transactionsQueryforLocation = GetComplianceDashboardSummary(Customerid).ToList()
                                                                   .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate && instanceIds.Contains(entry.ComplianceInstanceID))
                                                                   .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                                }
                                else
                                {
                                    transactionsQueryforLocation = GetComplianceDashboardSummary(Customerid).ToList()
                                                                   .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                                                   .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                                }

                                table = GetPerformanceSummaryRiskWise(transactionsQueryforLocation, "PERF");
                            }

                        }
                        else
                        {
                            if (branchId != -1)
                            {
                                IEnumerable<dynamic> QueryLocationCatagory;
                                if (isReportee)
                                {
                                    var instanceIds = ComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                                    QueryLocationCatagory = GetComplianceDashboardSummary(Customerid).ToList()
                                                           .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate && entry.CustomerBranchID == branchId
                                                           && instanceIds.Contains(entry.ComplianceInstanceID))
                                                           .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                                }
                                else
                                {
                                    QueryLocationCatagory = GetComplianceDashboardSummary(Customerid).ToList()
                                                                  .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate && entry.CustomerBranchID == branchId)
                                                                  .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                                }
                                table = GetPerformanceSummaryCatagoryWise(QueryLocationCatagory, "PERF");
                            }
                            else
                            {
                                IEnumerable<dynamic> QueryLocationCatagory;
                                if (isReportee)
                                {

                                    var instanceIds = ComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                                    QueryLocationCatagory = GetComplianceDashboardSummary(Customerid).ToList()
                                                           .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate && instanceIds.Contains(entry.ComplianceInstanceID))
                                                           .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                                }
                                else
                                {
                                    QueryLocationCatagory = GetComplianceDashboardSummary(Customerid).ToList()
                                                           .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                                           .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                                }
                                table = GetPerformanceSummaryCatagoryWise(QueryLocationCatagory, "PERF");
                            }
                        }
                        break;
                }
                return table;
            }
        }

        public static List<object> PerformanceSummaryforPerformerPending(int customerID, int userID, PerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, out bool displayflag, int catagoryId = -1, int branchId = -1, PerformanceSummaryForPerformer subFilter = PerformanceSummaryForPerformer.Risk, bool isReportee = false, long ReviewerID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime today = DateTime.Now.Date;
                bool flag = false;
                List<object> summary = new List<object>();

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                IEnumerable<dynamic> transactionsQuery;
                if (isReportee)
                {
                    var instanceIds = ComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.CustomerID == customerID && row.UserID == userID && row.RoleID == performerRoleID && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6) && row.ScheduledOn > fromdate && row.ScheduledOn < todate
                                         && instanceIds.Contains(row.ComplianceInstanceID)
                                          && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select new
                                         {
                                             ComplianceInstanceID = row.ComplianceInstanceID,
                                             row.ComplianceID,
                                             acte.ComplianceCategoryId,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.ScheduledOnID
                                         }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.CustomerID == customerID && row.UserID == userID && row.RoleID == performerRoleID && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6)
                                         && row.ScheduledOn > fromdate && row.ScheduledOn < todate
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select new
                                         {
                                             ComplianceInstanceID = row.ComplianceInstanceID,
                                             row.ComplianceID,
                                             acte.ComplianceCategoryId,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.ScheduledOnID
                                         }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                }

                switch (filter)
                {
                    case PerformanceSummaryForPerformer.Risk:
                        var CatagoryList = ComplianceCategoryManagement.GetAll();

                        int cnt = 0;
                        foreach (ComplianceCategory cc in CatagoryList)
                        {
                            var Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0).Count();
                            if (Convert.ToInt32(Count) > 0)
                                summary.Add(new { Name = cc.Name, Quantity = Count });

                            cnt += Count;
                        }

                        flag = cnt != 0 ? true : false;

                        break;
                    case PerformanceSummaryForPerformer.Category:
                        int HighRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 0).Count();
                        int MediumRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 1).Count();
                        int LowRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 2).Count();

                        summary.Add(new { Name = "Low", Quantity = LowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = MediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = HighRiskCount });

                        flag = (LowRiskCount + MediumRiskCount + HighRiskCount) != 0 ? true : false;
                        break;
                    case PerformanceSummaryForPerformer.Location:

                        if (subFilter == PerformanceSummaryForPerformer.Risk)
                        {
                            var CatagoryListforLocation = ComplianceCategoryManagement.GetAll();

                            int LocCnt = 0;
                            foreach (ComplianceCategory cc in CatagoryListforLocation)
                            {
                                int Count = 0;
                                if (branchId != -1)
                                    Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0 && entry.CustomerBranchID == branchId).Count();
                                else
                                    Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0).Count();

                                if (Count > 0)
                                    summary.Add(new { Name = cc.Name, Quantity = Count });

                                LocCnt += Count;
                            }
                            flag = LocCnt != 0 ? true : false;
                        }
                        else
                        {
                            if (branchId != -1)
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == branchId);
                            }
                            int HighRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 0).Count();
                            int MediumRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 1).Count();
                            int LowRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 2).Count();
                            summary.Add(new { Name = "Low", Quantity = LowRiskCountForLoc });
                            summary.Add(new { Name = "Medium", Quantity = MediumRiskCountForLoc });
                            summary.Add(new { Name = "High", Quantity = HighRiskCountForLoc });

                            flag = (LowRiskCountForLoc + MediumRiskCountForLoc + HighRiskCountForLoc) != 0 ? true : false;
                        }

                        break;
                }

                displayflag = flag;

                return summary;
            }
        }

        public static List<object> PerformanceSummaryforPerformerDelayed(int customerID, int userID, PerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, out bool displayflag, int catagoryId = -1, int branchId = -1, PerformanceSummaryForPerformer subFilter = PerformanceSummaryForPerformer.Risk, bool isReportee = false, long ReviewerID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime today = DateTime.Now.Date;
                bool flag = false;
                List<object> summary = new List<object>();

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                IEnumerable<dynamic> transactionsQuery;
                if (isReportee)
                {
                    var instanceIds = ComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.CustomerID == customerID && row.UserID == userID && row.RoleID == performerRoleID && row.ComplianceStatusID == 5 && row.ScheduledOn > fromdate && row.ScheduledOn < todate
                                         && instanceIds.Contains(row.ComplianceInstanceID) && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select new
                                         {
                                             ComplianceInstanceID = row.ComplianceInstanceID,
                                             row.ComplianceID,
                                             acte.ComplianceCategoryId,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.ScheduledOnID
                                         }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.CustomerID == customerID && row.UserID == userID && row.RoleID == performerRoleID && row.ComplianceStatusID == 5 && row.ScheduledOn > fromdate && row.ScheduledOn < todate
                                          && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select new
                                         {
                                             ComplianceInstanceID = row.ComplianceInstanceID,
                                             row.ComplianceID,
                                             acte.ComplianceCategoryId,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.ScheduledOnID
                                         }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                }

                switch (filter)
                {
                    case PerformanceSummaryForPerformer.Risk:
                        var CatagoryList = ComplianceCategoryManagement.GetAll();

                        int cnt = 0;
                        foreach (ComplianceCategory cc in CatagoryList)
                        {
                            var Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0).Count();

                            if (Convert.ToInt32(Count) > 0)
                                summary.Add(new { Name = cc.Name, Quantity = Count });

                            cnt += Count;
                        }
                        flag = cnt != 0 ? true : false;
                        break;

                    case PerformanceSummaryForPerformer.Category:
                        int HighRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 0).Count();
                        int MediumRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 1).Count();
                        int LowRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 2).Count();
                        summary.Add(new { Name = "Low", Quantity = LowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = MediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = HighRiskCount });

                        flag = (LowRiskCount + MediumRiskCount + HighRiskCount) != 0 ? true : false;
                        break;

                    case PerformanceSummaryForPerformer.Location:

                        if (subFilter == PerformanceSummaryForPerformer.Risk)
                        {
                            var CatagoryListforLocation = ComplianceCategoryManagement.GetAll();

                            int LocCnt = 0;
                            foreach (ComplianceCategory cc in CatagoryListforLocation)
                            {
                                int Count = 0;
                                if (branchId != -1)
                                    Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0 && entry.CustomerBranchID == branchId).Count();
                                else
                                    Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0).Count();

                                if (Count > 0)
                                    summary.Add(new { Name = cc.Name, Quantity = Count });

                                LocCnt += Count;
                            }
                            flag = LocCnt != 0 ? true : false;
                        }
                        else
                        {
                            if (branchId != -1)
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == branchId);
                            }
                            int HighRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 0).Count();
                            int MediumRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 1).Count();
                            int LowRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 2).Count();
                            summary.Add(new { Name = "Low", Quantity = LowRiskCountForLoc });
                            summary.Add(new { Name = "Medium", Quantity = MediumRiskCountForLoc });
                            summary.Add(new { Name = "High", Quantity = HighRiskCountForLoc });

                            flag = (LowRiskCountForLoc + MediumRiskCountForLoc + HighRiskCountForLoc) != 0 ? true : false;
                        }

                        break;
                }

                displayflag = flag;
                return summary;
            }
        }

        public static string GetSumofPenaltyForPerformer(int customerID, int userID, DateTime fromdate, DateTime todate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;

                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join cm in entities.Compliances on row.ComplianceID equals cm.ID
                                         where row.CustomerID == customerID && row.UserID == userID && row.RoleID == performerRoleID && row.ScheduledOn > fromdate && row.ScheduledOn < todate
                                         && row.ComplianceStatusID == 6 && row.NonComplianceType == 0
                                          && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select new
                                         {
                                             row.ComplianceInstanceID,
                                             row.ComplianceStatusID,
                                             row.ComplianceID,
                                             cm.FixedMinimum,
                                             cm.FixedMaximum,
                                             row.ScheduledOnID
                                         }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                string penalty = "";
                if (transactionsQuery.Count() != 0)
                {
                    double? fixedMinimumValue = transactionsQuery.Sum(entity => entity.FixedMinimum);
                    double? fixedMaximumValue = transactionsQuery.Sum(entity => entity.FixedMaximum);

                    penalty = "*Sum of Penalty minimum " + fixedMinimumValue + " and maximum " + fixedMaximumValue + " Rupees is applicable for " + transactionsQuery.Count() + " Non-Compliances.";
                }

                return penalty;
            }
        }

        public static DataTable GetPerformanceSummaryRiskWise(IEnumerable<dynamic> transactionsQuery, string role)
        {
            long delayedCount;
            long Intime;
            long pendingcount;
            //DateTime now = DateTime.UtcNow.Date;

            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("RiskCatagory", typeof(string));
            table.Columns.Add("Delayed", typeof(long));
            table.Columns.Add("InTime", typeof(long));
            table.Columns.Add("Pending", typeof(long));

            // for Heigh risk Compliances
            delayedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.Risk == 0).Count();
            Intime = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.Risk == 0).Count();
            if (role.Equals("PERF"))
            {
                pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10 || entry.ComplianceStatusID == 6) && entry.Risk == 0).Count();
            }
            else
            {
                pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3) && entry.Risk == 0).Count();
            }
            table.Rows.Add(0, "High", delayedCount, Intime, pendingcount);

            // for Medium risk Compliances
            delayedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.Risk == 1).Count();
            Intime = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.Risk == 1).Count();
            if (role.Equals("PERF"))
            {
                pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10 || entry.ComplianceStatusID == 6) && entry.Risk == 1).Count();
            }
            else
            {
                pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3) && entry.Risk == 1).Count();
            }

            table.Rows.Add(1, "Medium", delayedCount, Intime, pendingcount);

            // for Low risk Compliances
            delayedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.Risk == 2).Count();
            Intime = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.Risk == 2).Count();
            if (role.Equals("PERF"))
            {
                pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10 || entry.ComplianceStatusID == 6) && entry.Risk == 2).Count();
            }
            else
            {
                pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3) && entry.Risk == 2).Count();
            }

            table.Rows.Add(2, "Low", delayedCount, Intime, pendingcount);

            return table;

        }

        public static DataTable GetPerformanceSummaryCatagoryWise(IEnumerable<dynamic> transactionsQueryforCatagory, string role)
        {
            long delayedCount;
            long Intime;
            long pendingcount;
            //DateTime now = DateTime.UtcNow.Date;

            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("RiskCatagory", typeof(string));
            table.Columns.Add("Delayed", typeof(long));
            table.Columns.Add("InTime", typeof(long));
            table.Columns.Add("Pending", typeof(long));

            var CatagoryList = ComplianceCategoryManagement.GetAll();

            foreach (ComplianceCategory cc in CatagoryList)
            {
                delayedCount = transactionsQueryforCatagory.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                Intime = transactionsQueryforCatagory.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                if (role.Equals("PERF"))
                {
                    pendingcount = transactionsQueryforCatagory.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10 || entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                }
                else
                {
                    pendingcount = transactionsQueryforCatagory.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3) && entry.ComplianceCategoryId == cc.ID).Count();
                }

                if (delayedCount != 0 || Intime != 0 || pendingcount != 0)
                    table.Rows.Add(cc.ID, cc.Name, delayedCount, Intime, pendingcount);

            }

            return table;
        }

        public static int GetBranchCountforPerformer(int userID, DateTime fromdate, DateTime todate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID && row.ScheduledOn > fromdate && row.ScheduledOn < todate
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row).GroupBy(entity => entity.CustomerBranchID).Select(entity => entity.FirstOrDefault());
                return transactionsQuery.Count();
            }
        }

        //Performence summary for reviewer.
        public static DataTable PerformanceSummaryforReviewer(int Customerid, int userID, PerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, int branchId = -1, PerformanceSummaryForPerformer subFilter = PerformanceSummaryForPerformer.Risk)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                DataTable table = new DataTable();

                switch (filter)
                {
                    case PerformanceSummaryForPerformer.Risk:

                        var transactionsQuery = GetComplianceDashboardSummary(Customerid).ToList()
                                                .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int) entry.RoleID) && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                                .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                        table = GetPerformanceSummaryRiskWise(transactionsQuery, "RVW");

                        break;

                    case PerformanceSummaryForPerformer.Category:

                        var transactionsQueryforCatagory = GetComplianceDashboardSummary(Customerid).ToList()
                                                           .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int) entry.RoleID) && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                                           .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                        table = GetPerformanceSummaryCatagoryWise(transactionsQueryforCatagory, "RVW");

                        break;
                    case PerformanceSummaryForPerformer.Location:

                        if (subFilter == PerformanceSummaryForPerformer.Risk)
                        {
                            if (branchId != -1)
                            {
                                var transactionsQueryforLocation = GetComplianceDashboardSummary(Customerid).ToList()
                                                                   .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int) entry.RoleID) && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                                                   .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                                table = GetPerformanceSummaryRiskWise(transactionsQueryforLocation, "RVW");
                            }
                            else
                            {

                                var transactionsQueryforLocation = GetComplianceDashboardSummary(Customerid).ToList()
                                                                  .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int) entry.RoleID) && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                                                  .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());
                                table = GetPerformanceSummaryRiskWise(transactionsQueryforLocation, "RVW");
                            }

                        }
                        else
                        {
                            if (branchId != -1)
                            {

                                var QueryLocationCatagory = GetComplianceDashboardSummary(Customerid).ToList()
                                                           .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int) entry.RoleID) && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                                           .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                                table = GetPerformanceSummaryCatagoryWise(QueryLocationCatagory, "RVW");
                            }
                            else
                            {

                                var QueryLocationCatagory = GetComplianceDashboardSummary(Customerid).ToList()
                                                         .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int) entry.RoleID) && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                                         .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                                table = GetPerformanceSummaryCatagoryWise(QueryLocationCatagory, "RVW");
                            }

                        }

                        break;

                    case PerformanceSummaryForPerformer.Reportee:

                        table = GetPerformanceSummaryofReporty(userID, fromdate, todate, branchId);

                        break;


                }

                return table;

            }
        }

        public static List<object> PerformanceSummaryforReviewerPending(int userID, PerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, out bool displayflag, int catagoryId = -1, int branchId = -1, PerformanceSummaryForPerformer subFilter = PerformanceSummaryForPerformer.Risk)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime today = DateTime.Now.Date;
                bool flag = false;
                List<object> summary = new List<object>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3) && row.ScheduledOn > fromdate && row.ScheduledOn < todate
                                          && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select new
                                         {
                                             ComplianceInstanceID = row.ComplianceInstanceID,
                                             row.ComplianceID,
                                             acte.ComplianceCategoryId,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.ScheduledOnID
                                         }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                switch (filter)
                {
                    case PerformanceSummaryForPerformer.Risk:
                        var CatagoryList = ComplianceCategoryManagement.GetAll();

                        int cnt = 0;
                        foreach (ComplianceCategory cc in CatagoryList)
                        {
                            var Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0).Count();
                            summary.Add(new { Name = cc.Name, Quantity = Count });
                            cnt += Count;
                        }

                        flag = cnt != 0 ? true : false;

                        break;
                    case PerformanceSummaryForPerformer.Category:
                        int HighRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 0).Count();
                        int MediumRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 1).Count();
                        int LowRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 2).Count();

                        summary.Add(new { Name = "Low", Quantity = LowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = MediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = HighRiskCount });

                        flag = (LowRiskCount + MediumRiskCount + HighRiskCount) != 0 ? true : false;
                        break;
                    case PerformanceSummaryForPerformer.Location:

                        if (subFilter == PerformanceSummaryForPerformer.Risk)
                        {
                            var CatagoryListforLocation = ComplianceCategoryManagement.GetAll();

                            int LocCnt = 0;
                            foreach (ComplianceCategory cc in CatagoryListforLocation)
                            {
                                int Count = 0;
                                if (branchId != -1)
                                    Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0 && entry.CustomerBranchID == branchId).Count();
                                else
                                    Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0).Count();

                                summary.Add(new { Name = cc.Name, Quantity = Count });
                                LocCnt += Count;
                            }
                            flag = LocCnt != 0 ? true : false;
                        }
                        else
                        {
                            if (branchId != -1)
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == branchId);
                            }
                            int HighRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 0).Count();
                            int MediumRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 1).Count();
                            int LowRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 2).Count();
                            summary.Add(new { Name = "Low", Quantity = LowRiskCountForLoc });
                            summary.Add(new { Name = "Medium", Quantity = MediumRiskCountForLoc });
                            summary.Add(new { Name = "High", Quantity = HighRiskCountForLoc });

                            flag = (LowRiskCountForLoc + MediumRiskCountForLoc + HighRiskCountForLoc) != 0 ? true : false;
                        }

                        break;
                    case PerformanceSummaryForPerformer.Reportee:

                        var ReportyQuery = (from row in entities.ComplianceInstanceTransactionViews
                                            where row.ScheduledOn > fromdate && row.ScheduledOn < todate && row.UserID == catagoryId && row.RoleID == 3
                                            && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6)
                                            && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                            select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                        int ReportyHighRiskCount = ReportyQuery.Where(entry => entry.Risk == 0).Count();
                        int ReportyMediumRiskCount = ReportyQuery.Where(entry => entry.Risk == 1).Count();
                        int ReportyLowRiskCount = ReportyQuery.Where(entry => entry.Risk == 2).Count();

                        summary.Add(new { Name = "Low", Quantity = ReportyLowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = ReportyMediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = ReportyHighRiskCount });

                        flag = (ReportyLowRiskCount + ReportyMediumRiskCount + ReportyHighRiskCount) != 0 ? true : false;
                        break;

                }

                displayflag = flag;

                return summary;
            }
        }

        public static List<object> PerformanceSummaryforReviewerDelayed(int userID, PerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, out bool displayflag, int catagoryId = -1, int branchId = -1, PerformanceSummaryForPerformer subFilter = PerformanceSummaryForPerformer.Risk)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime today = DateTime.Now.Date;
                bool flag = false;
                List<object> summary = new List<object>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();


                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.UserID == userID && (row.IsActive != false || row.IsUpcomingNotDeleted != false) && reviewerRoleIDs.Contains((int) row.RoleID) && row.ComplianceStatusID == 5 && row.ScheduledOn > fromdate && row.ScheduledOn < todate
                                         select new
                                         {
                                             ComplianceInstanceID = row.ComplianceInstanceID,
                                             row.ComplianceID,
                                             acte.ComplianceCategoryId,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.ScheduledOnID
                                         }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                switch (filter)
                {
                    case PerformanceSummaryForPerformer.Risk:
                        var CatagoryList = ComplianceCategoryManagement.GetAll();

                        int cnt = 0;
                        foreach (ComplianceCategory cc in CatagoryList)
                        {
                            var Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0).Count();
                            summary.Add(new { Name = cc.Name, Quantity = Count });
                            cnt += Count;
                        }
                        flag = cnt != 0 ? true : false;
                        break;

                    case PerformanceSummaryForPerformer.Category:
                        int HighRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 0).Count();
                        int MediumRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 1).Count();
                        int LowRiskCount = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 2).Count();
                        summary.Add(new { Name = "Low", Quantity = LowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = MediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = HighRiskCount });

                        flag = (LowRiskCount + MediumRiskCount + HighRiskCount) != 0 ? true : false;
                        break;

                    case PerformanceSummaryForPerformer.Location:

                        if (subFilter == PerformanceSummaryForPerformer.Risk)
                        {
                            var CatagoryListforLocation = ComplianceCategoryManagement.GetAll();

                            int LocCnt = 0;
                            foreach (ComplianceCategory cc in CatagoryListforLocation)
                            {
                                int Count = 0;
                                if (branchId != -1)
                                    Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0 && entry.CustomerBranchID == branchId).Count();
                                else
                                    Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc.ID && entry.Risk == 0).Count();

                                summary.Add(new { Name = cc.Name, Quantity = Count });
                                LocCnt += Count;
                            }
                            flag = LocCnt != 0 ? true : false;
                        }
                        else
                        {
                            if (branchId != -1)
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == branchId);
                            }
                            int HighRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 0).Count();
                            int MediumRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 1).Count();
                            int LowRiskCountForLoc = transactionsQuery.Where(entry => entry.ComplianceCategoryId == catagoryId && entry.Risk == 2).Count();
                            summary.Add(new { Name = "Low", Quantity = LowRiskCountForLoc });
                            summary.Add(new { Name = "Medium", Quantity = MediumRiskCountForLoc });
                            summary.Add(new { Name = "High", Quantity = HighRiskCountForLoc });

                            flag = (LowRiskCountForLoc + MediumRiskCountForLoc + HighRiskCountForLoc) != 0 ? true : false;
                        }

                        break;
                    case PerformanceSummaryForPerformer.Reportee:

                        var ReportyQuery = (from row in entities.ComplianceInstanceTransactionViews
                                            where row.ScheduledOn > fromdate && row.ScheduledOn < todate && row.UserID == catagoryId
                                            && row.ComplianceStatusID == 5 && row.RoleID == 3
                                            select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                        int ReportyHighRiskCount = ReportyQuery.Where(entry => entry.Risk == 0).Count();
                        int ReportyMediumRiskCount = ReportyQuery.Where(entry => entry.Risk == 1).Count();
                        int ReportyLowRiskCount = ReportyQuery.Where(entry => entry.Risk == 2).Count();

                        summary.Add(new { Name = "Low", Quantity = ReportyLowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = ReportyMediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = ReportyHighRiskCount });

                        flag = (ReportyLowRiskCount + ReportyMediumRiskCount + ReportyHighRiskCount) != 0 ? true : false;
                        break;
                }

                displayflag = flag;
                return summary;
            }
        }

        public static string GetSumofPenaltyForReviewer(int userID, DateTime fromdate, DateTime todate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();


                DateTime now = DateTime.UtcNow.Date;

                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join cm in entities.Compliances on row.ComplianceID equals cm.ID
                                         where row.UserID == userID && (row.IsActive != false || row.IsUpcomingNotDeleted != false) && reviewerRoleIDs.Contains((int) row.RoleID) && row.ScheduledOn > fromdate && row.ScheduledOn < todate
                                         && row.ComplianceStatusID == 6 && row.NonComplianceType == 0
                                         select new
                                         {
                                             row.ComplianceInstanceID,
                                             row.ComplianceStatusID,
                                             row.ComplianceID,
                                             cm.FixedMinimum,
                                             cm.FixedMaximum,
                                             row.ScheduledOnID
                                         }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                string penalty = "";
                if (transactionsQuery.Count() != 0)
                {
                    double? fixedMinimumValue = transactionsQuery.Sum(entity => entity.FixedMinimum);
                    double? fixedMaximumValue = transactionsQuery.Sum(entity => entity.FixedMaximum);

                    penalty = "*Sum of Penalty minimum " + fixedMinimumValue + " and maximum " + fixedMaximumValue + " Rupees is applicable for " + transactionsQuery.Count() + " Non-Compliances.";
                }

                return penalty;
            }
        }

        public static int GetBranchCountforReviewer(int userID, DateTime fromdate, DateTime todate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) && row.ScheduledOn > fromdate && row.ScheduledOn < todate
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row).GroupBy(entity => entity.CustomerBranchID).Select(entity => entity.FirstOrDefault());

                return transactionsQuery.Count();
            }
        }

        public static DataTable GetPerformanceSummaryofReporty(int userID, DateTime fromdate, DateTime todate, int branchId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();


                List<long> reporteeIds = ComplianceManagement.GetPerformerListUsingReviewer(userID);
                List<long> MappedinstanceId = ComplianceManagement.GetInstanceIDfromReviewer(userID);

                long delayedCount;
                long Intime;
                long pendingcount;
                DateTime now = DateTime.UtcNow.Date;

                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(string));
                table.Columns.Add("RiskCatagory", typeof(string));
                table.Columns.Add("Delayed", typeof(long));
                table.Columns.Add("InTime", typeof(long));
                table.Columns.Add("Pending", typeof(long));

                var ReportyList = (from row in entities.Users
                                   where reporteeIds.Contains(row.ID) && row.IsDeleted != true
                                   select row).ToList();

                List<ComplianceInstanceTransactionView> transactionQuery = (from row in entities.ComplianceInstanceTransactionViews
                                                                            where row.ScheduledOn > fromdate && row.ScheduledOn < todate && row.RoleID == performerRoleID
                                                                            && MappedinstanceId.Contains(row.ComplianceInstanceID) && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                                                            select row).ToList();

                if (branchId != -1)
                {
                    transactionQuery = transactionQuery.Where(entry => entry.CustomerBranchID == branchId).ToList();
                }

                foreach (User reporty in ReportyList)
                {
                    pendingcount = transactionQuery.Where(entry => entry.UserID == reporty.ID && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10 || entry.ComplianceStatusID == 6)).Select(entity => entity.ScheduledOnID).Distinct().Count();
                    delayedCount = transactionQuery.Where(entry => entry.UserID == reporty.ID && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).Count();
                    Intime = transactionQuery.Where(entry => entry.UserID == reporty.ID && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).Count();
                    table.Rows.Add(reporty.ID, reporty.FirstName + " " + reporty.LastName, delayedCount, Intime, pendingcount);
                }
                return table;
            }

        }
        //Functions for Drill Down Report. 15 FEB 2017
        public static List<ComplianceDashboardSummaryView> GetComplianceDashboardSummary(int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var transactionsQuery = (from row in entities.ComplianceDashboardSummaryViews
                                         where row.CustomerID == Customerid && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row).ToList();
                return transactionsQuery;
            }
        }


        public static List<SP_GetOverDueHighCompliance_Result> GetComplianceDashboardOverdueList(int Customerid,string isRole, int Userid = -1, bool ISApprover = false,int RiskId=0,int IsDashBoard=1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var transactionsQuery = (from row in entities.SP_GetOverDueHighCompliance(Userid,Customerid, RiskId, IsDashBoard, isRole)
                                         where row.ScheduledOn < DateTime.Now 
                                         select row).ToList();
              
                return transactionsQuery;
            }
        }


        //public static List<SP_GetOverDueHighComplianceApprover_Result> GetComplianceDashboardOverdueListApprover(int Customerid, int Userid = -1, bool ISApprover = false, int RiskId = 0, int IsDashBoard = 1)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        entities.Database.CommandTimeout = 180;
        //        var transactionsQuery = (from row in entities.SP_GetOverDueHighComplianceApprover(Userid, Customerid, RiskId, IsDashBoard)
        //                                 where row.ScheduledOn < DateTime.Now
        //                                 select row).ToList();

        //        return transactionsQuery;
        //    }
        //}
        //Functions for Drill Down Report.


        public static List<ComplianceDashboardSummaryView> GetComplianceDashboardSummary(int Customerid, int Userid = -1, bool ISApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<ComplianceDashboardSummaryView> transactionsQuery = new List<ComplianceDashboardSummaryView>();
                if (ISApprover == true)
                {
                    transactionsQuery = (from row in entities.ComplianceDashboardSummaryViews
                                         where row.CustomerID == Customerid
                                         && row.UserID == Userid && row.RoleID == 6
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceDashboardSummaryViews
                                         join row2 in entities.EntitiesAssignments
                                         on row.CustomerBranchID equals row2.BranchID
                                         where row.CustomerID == Customerid
                                         && row2.UserID == Userid
                                         && row.ComplianceCategoryId == row2.ComplianceCatagoryID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row).ToList();
                }

                return transactionsQuery;
            }
        }
        public static List<ComplianceDashboardSummaryView> GetSummaryDetails(int Customerid, int userID, DateTime fromDate, DateTime toDate, int role, List<int> status, int baranchId, string filter, string subfilter, int typeID, bool isReportee = false, long ReviewerID = -1)
        {

            List<ComplianceDashboardSummaryView> ComplianceTransaction = new List<ComplianceDashboardSummaryView>();
            if (!filter.ToString().Equals("Reportee"))
            {
                if (isReportee)
                {
                    var instanceIds = ComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                    ComplianceTransaction = DashboardManagement.GetComplianceDashboardSummary(Customerid).ToList()
                                            .Where(entry => entry.UserID == userID && entry.RoleID == role && entry.ScheduledOn > fromDate && entry.ScheduledOn < toDate && instanceIds.Contains(entry.ComplianceInstanceID))
                                            .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    ComplianceTransaction = DashboardManagement.GetComplianceDashboardSummary(Customerid).ToList()
                                           .Where(entry => entry.UserID == userID && entry.RoleID == role && entry.ScheduledOn > fromDate && entry.ScheduledOn < toDate)
                                           .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                if (status.Count > 0)
                {
                    ComplianceTransaction = ComplianceTransaction.Where(entry => status.Contains((int) entry.ComplianceStatusID)).ToList();
                }

                if (baranchId != -1)
                {
                    ComplianceTransaction = ComplianceTransaction.Where(entry => entry.CustomerBranchID == baranchId).ToList();
                }

                if (filter != null)
                {
                    if (filter.Equals("Category"))
                    {
                        ComplianceTransaction = ComplianceTransaction.Where(entry => entry.ComplianceCategoryId == typeID).ToList();
                    }
                    else if (filter.Equals("Risk"))
                    {
                        ComplianceTransaction = ComplianceTransaction.Where(entry => entry.Risk == typeID).ToList();
                    }
                    else
                    {
                        if (subfilter.Equals("Risk"))
                        {
                            ComplianceTransaction = ComplianceTransaction.Where(entry => entry.Risk == typeID).ToList();
                        }
                        else
                        {
                            ComplianceTransaction = ComplianceTransaction.Where(entry => entry.ComplianceCategoryId == typeID).ToList();
                        }
                    }
                }
            }
            else
            {
                if (isReportee)
                {
                    var instanceIds = ComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                    ComplianceTransaction = DashboardManagement.GetComplianceDashboardSummary(Customerid).ToList()
                                           .Where(entry => entry.UserID == userID && entry.RoleID == 3 && entry.ScheduledOn > fromDate && entry.ScheduledOn < toDate && instanceIds.Contains(entry.ComplianceInstanceID)).ToList();
                }
                else
                {
                    ComplianceTransaction = DashboardManagement.GetComplianceDashboardSummary(Customerid).ToList()
                                           .Where(entry => entry.UserID == userID && entry.RoleID == 3 && entry.ScheduledOn > fromDate && entry.ScheduledOn < toDate).ToList();
                }

                if (status.Count > 0)
                {
                    ComplianceTransaction = ComplianceTransaction.Where(entry => status.Contains((int) entry.ComplianceStatusID))
                                           .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

            }

            return ComplianceTransaction;
        }


        //checklist
        //Comment By rahul on 29 Augest 2018
        //public static List<CheckListInstanceTransactionView> DashboardDataForPerformer_Checklist(int userID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<CheckListInstanceTransactionView> transactionsQuery = new List<CheckListInstanceTransactionView>();
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();

        //        DateTime now = DateTime.UtcNow.Date;
        //        transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
        //                             where row.UserID == userID && row.RoleID == performerRoleID
        //                              && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                              && (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 1
        //                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<SP_GetCheckListCannedReportCompliancesSummary_Result> DashboardDataForPerformer_Checklist(int userID, long Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCheckListCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCheckListCannedReportCompliancesSummary_Result>();
                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in entities.SP_GetCheckListCannedReportCompliancesSummary(userID, (int)Customerid, "PERF")
                                     where (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 1

                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                return transactionsQuery.ToList();
            }
        }
        //Comment By rahul on 29 Augest 2018
        //public static List<CheckListInstanceTransactionView> DashboardDataForPerformer_ChecklistNew(int userID, int risk, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType)
        //{


        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<CheckListInstanceTransactionView> transactionsQuery = new List<CheckListInstanceTransactionView>();
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();

        //        DateTime now = DateTime.UtcNow.Date;
        //        transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
        //                             where row.UserID == userID && row.RoleID == performerRoleID
        //                              && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                              && (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 1
        //                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //        //Type Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
        //        }

        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId ==category)).ToList();
        //        }

        //        //category Filter
        //        if (ActID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
        //        }

        //        //Datr Filter
        //        if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
        //        }

        //        //Risk Filter
        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }
        //        // Find data through String contained in Description
        //        if (StringType != "")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}
        public static List<SP_GetCheckListCannedReportCompliancesSummary_Result> DashboardDataForPerformer_ChecklistNew(int userID, long Customerid, int risk, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType, int ComplianceType, string queryStringFlag, CheckListStatus Status)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCheckListCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCheckListCannedReportCompliancesSummary_Result>();

                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in entities.SP_GetCheckListCannedReportCompliancesSummary(userID, (int)Customerid, "PERF")
                                     where (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 1
                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                //Type Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }
                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
                }
                //Act Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
                }

                switch (Status)
                {
                    case CheckListStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case CheckListStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID != 4).ToList();
                        break;
                }
                //Date Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }
                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                if (queryStringFlag != "A")
                {
                    //Check Statutory Checklist
                    if (ComplianceType == -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == null)).ToList();
                    }
                    //Check EventBased Checklist
                    if (ComplianceType == 0)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }
                }

                return transactionsQuery.ToList();
            }
        }


        public static List<SP_GetCheckListCannedReportCompliancesSummary_Result> DashboardDataForReviewer_ChecklistNew(int userID, long Customerid, int risk, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType, int ComplianceType, string queryStringFlag, CheckListStatus Status)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCheckListCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCheckListCannedReportCompliancesSummary_Result>();

                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in entities.SP_GetCheckListCannedReportCompliancesSummary(userID, (int)Customerid, "RVW")
                                     where (row.ScheduledOn == now || row.ScheduledOn <= now) //&& row.ComplianceStatusID == 1
                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                //Type Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }
                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
                }

                switch (Status)
                {
                    case CheckListStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case CheckListStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID != 4).ToList();
                        break;
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
                }
                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
                }
                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }
                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                if (queryStringFlag != "A")
                {
                    //Check Statutory Checklist
                    if (ComplianceType == -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == null)).ToList();
                    }
                    //Check EventBased Checklist
                    if (ComplianceType == 0)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }
                }

                return transactionsQuery.ToList();
            }
        }


    }
}
