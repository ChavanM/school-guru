﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Web;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
   public class SendGridEmailManager
    {
        public static void SendGridMail(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message)
        {

            Task task = Execute(from, fromName, to, cc, bcc, subject, message, null);

            task.Wait();  
        }

        public static void SendGridMail(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {
            Task task = Execute(from, fromName, to, cc, bcc, subject, message, null);

            task.Wait();  
        }

        public static void SendGridNewsLetterMail(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {
            Task task = Execute(from, fromName, to, cc, bcc, subject, message, null);

            task.Wait(); 
        }

        public static void SendGridMailwithAttachment(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {
            Task task = Execute(from, fromName, to, cc, bcc, subject, message, null);

            task.Wait();  
        }
        public static void SendGridNewsLetterMail1(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {
            Task task = Execute(from, fromName, to, cc, bcc, subject, message, null);

            task.Wait();  
        }

        static async Task Execute(string From, string fromName, List<string> To, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> Attachment)
        {
            var sendGridAPIKey = ConfigurationManager.AppSettings["SendGridAPIKey"];

            var apiKey = sendGridAPIKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(From, fromName);
            var to = new EmailAddress(To.FirstOrDefault());
            var plainTextContent = message;
            var htmlContent = message;

            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);

            await client.SendEmailAsync(msg);
        }

    }
}
