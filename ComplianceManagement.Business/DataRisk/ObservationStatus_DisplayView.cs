//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class ObservationStatus_DisplayView
    {
        public Nullable<long> TESTID { get; set; }
        public Nullable<long> ATBDId { get; set; }
        public string ActivityTobeDone { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public string FinancialYear { get; set; }
        public string ForMonth { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public string Branch { get; set; }
        public long ProcessId { get; set; }
        public string ProcessName { get; set; }
        public string SubProcessName { get; set; }
        public string ActivityDescription { get; set; }
        public Nullable<long> ObservationCategory { get; set; }
        public string ObsCategory { get; set; }
        public Nullable<long> ObservatioRating { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<long> PersonResponsible { get; set; }
    }
}
