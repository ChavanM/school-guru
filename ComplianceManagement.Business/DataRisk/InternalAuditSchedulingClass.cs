﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    public  class AuditManagerClass
    {
        public int ID { get; set; }
        public string Name { get; set; }

    }
    public class AssignedUserwiseAuditList
    {
        public int ATBDID { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public string Branch { get; set; }
        public string ForMonth { get; set; }
        public Nullable<int> RoleID { get; set; }
        public Nullable<long> UserID { get; set; }
        public string Role { get; set; }
        public string User { get; set; }
        public string FinancialYear { get; set; }
        public Nullable<System.DateTime> ExpectedStartDate { get; set; }
        public Nullable<System.DateTime> ExpectedEndDate { get; set; }
        public long VerticalId { get; set; }
        public string VerticalName { get; set; }
        public string UserName { get; set; }
        public string ProcessName { get; set; }
        public Nullable<long> AuditID { get; set; }
        public string AuditName { get; set; }
        public int SubProcessID { get; set; }
        public int ProcessId { get; set; }
        public string SubProcessName { get; set; }
    }
    public class ICFRTransactionClassExport
    {
        public long ID { get; set; }
        public string Process { get; set; }
        public long NoOfControlKey { get; set; }
        public long NoOfControlNonKey { get; set; }
        public long TestResultPassKey { get; set; }
        public long TestResultPassNonKey { get; set; }
        public long TestResultFailKey { get; set; }
        public long TestResultFailNonKey { get; set; }
        public long Total { get; set; }
    }
    public class FetchDataAuditSchedulingReport
    {
        public string Process { get; set; }
        public string SubProcess { get; set; }
        public string Title { get; set; }
        public string OriginalDate { get; set; }
        public string RevisedDate { get; set; }
        public List<string> RevisedDateList { get; set; }
        public string Location { get; set; }
    }
    public class ControlTestingAssignmentDetails
    {
        public long RiskCreationId { get; set; }
        public int CustomerBranchID { get; set; }
        public string Branch { get; set; }
        public Nullable<long> ScheduledOnID { get; set; }
        public string ForMonth { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string Role { get; set; }
        public string User { get; set; }
        public string ActivityDescription { get; set; }
        public string ControlObjective { get; set; }
        public long ProcessId { get; set; }
        public long SubProcessId { get; set; }
        public string ControlDescription { get; set; }
        public string FinancialYear { get; set; }
        public string ControlNo { get; set; }
        public long AuditInstanceID { get; set; }
        public string ProcessName { get; set; }
        public string SubProcessName { get; set; }
    }
    public class InternalAuditInstanceData
    {
        public string location { get; set; }
        public string FinancialYear { get; set; }
        public string period { get; set; }
        public string verticalName { get; set; }

    }
    public class ReAssigningData
    {
        public string ForMonth { get; set; }
        public string FinancialYear { get; set; }

        public int ProcessID { get; set; }

        public string ProcessName { get; set; }

        public string Branch { get; set; }

        public int CustomerBranchID { get; set; }
    }

    public class ProcessReassing
    {
        public int ProcessId { get; set; }
        public string Name { get; set; }

    }
    public partial class RiskActivityTransactionForddlnew
    {
        public Nullable<long> VerticalsId { get; set; }
        public string VerticalName { get; set; }
    }
    public class AuditSpecialScheduling
    {
        public long Id { get; set; }
        public string ISAHQMP { get; set; }
        public string FinancialYear { get; set; }
        public long CustomerBranchId { get; set; }
        public string CustomerBranchName { get; set; }
        public long CustomerId { get; set; }
        public string TermName { get; set; }
        public Nullable<int> PhaseCount { get; set; }
        public Nullable<DateTime> StartDate { get; set; }
        public Nullable<DateTime> EndDate { get; set; }
        public Nullable<int> VerticalID { get; set; }
        public string VerticalName { get; set; }
        public string ProcessName { get; set; }
        public int ProcessId { get; set; }
        public string AssignedTo { get; set; }
        public Nullable<long> ExternalAuditorId { get; set; }
        public Nullable<long> AuditID { get; set; }
        
    }
    public class AuditSchedulingNewbind
    {
        public long Id { get; set; }
        public string ISAHQMP { get; set; }
        public string FinancialYear { get; set; }
        public long CustomerBranchId { get; set; }
        public string CustomerBranchName { get; set; }
        public long CustomerId { get; set; }
        public string TermName { get; set; }
        public Nullable<int> PhaseCount { get; set; }
        public Nullable<DateTime> StartDate { get; set; }
        public Nullable<DateTime> EndDate { get; set; }
        public Nullable<int> VerticalID { get; set; }
        public string VerticalName { get; set; }
        public int ProcessId { get; set; }
        public string AssignedTo { get; set; }
        public Nullable<long> ExternalAuditorId { get; set; }
        public Nullable<long> AuditID { get; set; }
    }
    public class OpenCloseInternalAuditDetailsCountClass
    {
        public int CustomerID { get; set; }
        public int CustomerBranchID { get; set; }        
        public string ForMonth { get; set; }        
        public long UserID { get; set; }        
        public string FinancialYear { get; set; }
        public Nullable<int> AuditStatusID { get; set; }
    }
    public class OpenCloseInternalAuditDetailsClass
    {
        public int CustomerID { get; set; }
        public int CustomerBranchID { get; set; }
        public string Branch { get; set; }
        public string ForMonth { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string Role { get; set; }
        public string User { get; set; }
        public string FinancialYear { get; set; }
        public long ScheduledOnID { get; set; }
        public Nullable<int> AuditStatusID { get; set; }
        public string UserName { get; set; }
        public string CProcessName { get; set; }
    }
    public partial class RiskActivityTransactionForddl
    {
        public long VerticalsId { get; set; }

        public string VerticalName { get; set; }
    }
    public class AuditassignmentDetails
    {
        public int Id { get; set; }
        public string VerticalName { get; set; }
        public long CustomerBranchid { get; set; }
        public string AssignedTo { get; set; }
        public Nullable<long> ExternalAuditorId { get; set; }
        public int CustomerId { get; set; }
        public Nullable<int> VerticalID { get; set; }
    }

    public class InternalAuditSchedulingClass
    {
        public long Id {get;set; }
        public string ISAHQMP { get; set; }
        public string FinancialYear { get; set; }
        public long CustomerBranchId { get; set; }
        public long CustomerId { get; set; }
        public string TermName { get; set; }
        public Nullable<int> PhaseCount { get; set; }

        public Nullable<DateTime> StartDate { get; set; }

        public Nullable<DateTime> EndDate { get; set; }
    }

    public class OpenCloseAuditDetailsClass
    {                
        public int ATBDID { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }      
        public string Branch { get; set; }      
        public string ForMonth { get; set; }       
        public Nullable<int> RoleID { get; set; }
        public Nullable<long> UserID { get; set; }
        public string Role { get; set; }
        public string User { get; set; }
        public string FinancialYear { get; set; }             
        public Nullable<System.DateTime> ExpectedStartDate { get; set; }
        public Nullable<System.DateTime> ExpectedEndDate { get; set; }
        public long VerticalId { get; set; }
        public string VerticalName { get; set; }
        public string UserName { get; set; }
        public string CProcessName { get; set; }
        public Nullable<long> AuditID { get; set; }
        public string AuditName { get; set; }
    }
    public class AuditObservationClass
    {        
        public Nullable<int> CustomerID { get; set; }
        public long CustomerBranchID { get; set; }
        public string BranchName { get; set; }
        public string FinancialYear { get; set; }
        public long VerticalID { get; set; }
        public string VerticalName { get; set; }
    }

    public class IMPOpenCloseAuditDetailsClass
    {

        public Nullable<int> CustomerID { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public string Branch { get; set; }
        public string ForMonth { get; set; }
        public Nullable<int> RoleID { get; set; }
        public Nullable<long> UserID { get; set; }
        public string Role { get; set; }
        public string User { get; set; }
        public string FinancialYear { get; set; }
        public Nullable<System.DateTime> ExpectedStartDate { get; set; }
        public Nullable<System.DateTime> ExpectedEndDate { get; set; }
        public long VerticalId { get; set; }
        public string VerticalName { get; set; }
        public string UserName { get; set; }
        public string CProcessName { get; set; }

        public Nullable<long> AuditID { get; set; }
    }

    public class ICFRAUDITSTEPCLOSE
    {

        public long ScheduledOnID { get; set; }
        public long RiskCreationID { get; set; }
        public string ForMonth { get; set; }
        public string FinancialYear { get; set; }
        public int StatusId { get; set; }
    }

    public class ProcessSubProcessScore
    {
        public int CustBranchID { get; set; }
        public int VerticalID { get; set; }
        public int ProcessID { get; set; }
        public int SubProcessID { get; set; }
        public Decimal SubProcessScore { get; set; }
    }
}
