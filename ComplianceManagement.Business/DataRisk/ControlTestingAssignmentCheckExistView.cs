//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class ControlTestingAssignmentCheckExistView
    {
        public long RiskCreationId { get; set; }
        public int CustomerBranchID { get; set; }
        public long ProcessId { get; set; }
        public Nullable<long> SubProcessId { get; set; }
        public string FinancialYear { get; set; }
        public bool IsDeleted { get; set; }
    }
}
