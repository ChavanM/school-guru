﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
   public class StateCityManagement
    {
        //added by sudarshan for id Excel Utility
        public static int GetStateIDByName(string stateName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var stateId = (from row in entities.States
                               where row.Name == stateName
                               select row.ID).SingleOrDefault();

                return stateId;
            }
        }

        //added by sudarshan for id Excel Utility
        public static int GetCityIdByName(string cityName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var cityId = (from row in entities.Cities
                               where row.Name == cityName
                               select row.ID).SingleOrDefault();

                return cityId;
            }
        }

        public static List<Country> GetAllContryData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjCountry = (from row in entities.Countries
                                  where row.Name != null
                                  select row);
                return ObjCountry.ToList();
            }
        }
        
        public static Country GetCountryDetailByID(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjCountry = (from row in entities.Countries
                                  where row.ID == ID
                                  select row).SingleOrDefault();
                return ObjCountry;
            }
        }

        public static void DeleteCountryDetail(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Country ObjCountry = (from row in entities.Countries
                                          where row.ID == ID
                                          select row).FirstOrDefault();

                entities.Countries.Remove(ObjCountry);
                entities.SaveChanges();
            }
        }


        public static bool ExitsCity(City newCity)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cities
                             where row.IsDeleted == false && row.Name.Equals(newCity.Name)
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static int CreateCity(City newCity)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Cities.Add(newCity);
                entities.SaveChanges();
                return newCity.ID;
            }
        }
        public static bool CheckCountryExist(string name)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjCountry = (from row in entities.Countries
                                  where row.Name.Equals(name)
                                  select row);
                return ObjCountry.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateContryDetails(Country objCountry)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Countries.Add(objCountry);
                entities.SaveChanges();
            }
        }

        public static void UpdateCountryDetails(Country objCountry)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Country updatestates = (from row in entities.Countries
                                            where row.ID == objCountry.ID
                                            select row).FirstOrDefault();

                updatestates.Name = objCountry.Name;
                entities.SaveChanges();
            }
        }

        public static void UpdateState(State objState)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                State updatestates = (from row in entities.States
                                      where row.ID == objState.ID
                                      select row).FirstOrDefault();

                updatestates.Name = objState.Name;
                updatestates.CountryID = objState.CountryID;
                entities.SaveChanges();
            }
        }

        public static List<Mst_StateCountry> GetAllState()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var StateList = (from row in entities.States
                                 join row1 in entities.Countries 
                                 on row.CountryID equals row1.ID
                                 where row.Name != null
                                 select new Mst_StateCountry
                                 {
                                     ID = row.ID,
                                     Name = row.Name,
                                     CountryID = row.CountryID,
                                     CountryName = row1.Name,
                                 }).ToList();

                return StateList.ToList();
            }
        }

        public static List<State> BindStateData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objstate = (from row in entities.States
                                where row.Name != null
                                && row.IsDeleted==false
                                select row);
                return objstate.ToList();
            }
        }

        public static object GetAllCitysByState(int v)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var cities = (from row in entities.Cities
                              where row.StateId == v
                              orderby row.Name ascending
                              select row).ToList();

                return cities.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static object getallCity()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var cities = (from row in entities.Cities
                              select row).ToList();

                return cities.OrderBy(entry => entry.Name).ToList();
            }
        }
    }
}
