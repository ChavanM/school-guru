﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ConsolidatedManagementRisk
    {

        

        public static int GetCompanyOverview(int customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditInstanceTransactionView> transactionsQuery = new List<AuditInstanceTransactionView>();
                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                     where row.CustomerID == customerid
                                        //&& (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                     select row).ToList();

                var userCount = transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;

                return userCount;

            }
        }
        public static AuditInstanceTransactionView GetApproverName(int customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where row.CustomerID == customerid && row.RoleID == 6                                          
                                         select row).ToList();

                if (transactionsQuery.Count != 0)
                {
                    return transactionsQuery[0];
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<AuditInstanceTransactionView> GetPerformerNameSatutoryNew(int customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where row.CustomerID == customerid && row.RoleID == 3                                           
                                         select row).ToList();

                return transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();               
            }
        }
        public static List<AuditInstanceTransactionView> GetReviwersName(int customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where row.CustomerID == customerid
                                         && (row.RoleID == 4 || row.RoleID == 5)                                             
                                         select row).ToList();

                return transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
            }
        }

        #region TOD
        public static DataTable GetCompliancesStatusTODWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(1);
            TODIds.Add(2);
            TODIds.Add(3);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear==financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Keycount;
                    long NonKeycount;                    
                    long totalcount;                   
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));                   
                    table.Columns.Add("Total", typeof(long));                   
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Keycount + NonKeycount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Keycount, NonKeycount, totalcount);
                    }                 
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth==periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Keycount;
                    long NonKeycount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Keycount + NonKeycount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Keycount, NonKeycount, totalcount);
                    }                 
                }
                return table;
            }
        }
        public static DataTable GetCompliancesStatusTODKEYWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(1);
            TODIds.Add(2);
            TODIds.Add(3);

            List<int> TODPassIds = new List<int>();           
            TODPassIds.Add(1);           
            TODPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.TOD==2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        //Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        //Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                return table;
            }
        }
        public static DataTable GetCompliancesStatusTODNONKEYWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(1);
            TODIds.Add(2);
            TODIds.Add(3);

            List<int> TODPassIds = new List<int>();
            TODPassIds.Add(1);
            TODPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                return table;
            }
        }
        #endregion
        #region TOE
        public static DataTable GetCompliancesTOEWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);           
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Keycount;
                    long NonKeycount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Keycount + NonKeycount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Keycount, NonKeycount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Keycount;
                    long NonKeycount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Keycount + NonKeycount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Keycount, NonKeycount, totalcount);
                    }
                }
                return table;
            }
        }
        public static DataTable GetCompliancesStatusTOEKEYWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);

            List<int> TOEPassIds = new List<int>();
            TOEPassIds.Add(1);
            TOEPassIds.Add(3);

            List<int> TOEFailIds = new List<int>();
            TOEFailIds.Add(-1);
            TOEFailIds.Add(2);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && TOEFailIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {                        
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && TOEFailIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                return table;
            }
        }
        public static DataTable GetCompliancesStatusTOENONKEYWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);

            List<int> TOEPassIds = new List<int>();
            TOEPassIds.Add(1);
            TOEPassIds.Add(3);

            List<int> TOEFailIds = new List<int>();
            TOEFailIds.Add(-1);
            TOEFailIds.Add(2);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && TOEFailIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && TOEFailIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                return table;
            }
        }
        #endregion
        public static DataTable GetManagementCompliancesSummary(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();                
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid
                                             && row.FinancialYear == financialyear
                                             select new
                                             {
                                                 RiskCreationId = row.RiskCreationId,
                                                 row.ProcessId,
                                                 row.AuditStatusID,
                                                 row.KeyId,
                                                 row.ScheduledOnID,
                                                 row.RoleID,
                                                 row.UserID
                                             }).ToList();

                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long KeyCount;
                    long NonKeyCount;
                    long totalcount;

                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        KeyCount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        NonKeyCount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = KeyCount + NonKeyCount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, KeyCount, NonKeyCount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerID == customerid
                                              && row.FinancialYear == financialyear
                                             && row.ForMonth == periodR
                                             select new
                                             {
                                                 RiskCreationId = row.RiskCreationId,
                                                 row.ProcessId,
                                                 row.AuditStatusID,
                                                 row.KeyId,
                                                 row.ScheduledOnID,
                                                 row.RoleID,
                                                 row.UserID
                                             }).ToList();

                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long KeyCount;
                    long NonKeyCount;
                    long totalcount;
                    
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        KeyCount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        NonKeyCount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = KeyCount + NonKeyCount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, KeyCount, NonKeyCount, totalcount);
                    }
                }
                return table;
            }
        }
        public static DataTable GetPastTwelveMonthSummary(int customerid, int year, int month, int userId = -1, bool approver = false)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //int startDateyear = year;
                //if (month < 4)
                //{
                //    startDateyear--;
                //}
                //DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
                //DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);

                //var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                //                         where row.CustomerID == customerid
                //                          //&& (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                //                         select row).ToList();

              
                //if (approver == true)
                //{
                //    transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                //}

                //transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                DataTable table = new DataTable();
                //table.Columns.Add("Compliances", typeof(string));
                //for (int i = 1; i < 13; i++)
                //{
                //    //table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                //    table.Columns.Add(i.ToString(), typeof(string));
                //}


                //long monthCount = 0;
                //DataRow CompletedInTime = table.NewRow();
                //CompletedInTime["Compliances"] = "Completed In Time";
                //for (int i = 1; i < 13; i++)
                //{
                //    DateTime previousDate = EndDate.AddMonths(-i);
                //    DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                //    DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));
                //    monthCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                //    //CompletedInTime[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = monthCount;
                //    CompletedInTime[i] = monthCount;
                //}
                //table.Rows.Add(CompletedInTime);


                //DataRow DelayedTime = table.NewRow();
                //DelayedTime["Compliances"] = "Completed after Due Date";
                //for (int i = 1; i < 13; i++)
                //{
                //    DateTime previousDate = EndDate.AddMonths(-i);
                //    DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                //    DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));
                //    monthCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                //    //DelayedTime[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = monthCount;
                //    DelayedTime[i] = monthCount;
                //}
                //table.Rows.Add(DelayedTime);


                //DataRow NotCompletedTime = table.NewRow();
                //NotCompletedTime["Compliances"] = "Not Yet Completed";
                //for (int i = 1; i < 13; i++)
                //{
                //    DateTime previousDate = EndDate.AddMonths(-i);
                //    DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                //    DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));

                //    monthCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)
                //                 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();

                //    //NotCompletedTime[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = monthCount;
                //    NotCompletedTime[i] = monthCount;
                //}
                //table.Rows.Add(NotCompletedTime);

                return table;


            }
        }
        public static List<AuditDashboardSummaryview> GetManagementDetailView(int customerid, string financialyear, string periodR, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, string TwelveMonth = "")
        {            
            List<AuditDashboardSummaryview> detailView = new List<AuditDashboardSummaryview>();           
            if (filter.Equals("Summary"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear==financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear  && entry.ForMonth == periodR                                   
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear  && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOD"))
            {
                List<int?> TODIds = new List<int?>();
                TODIds.Add(-1);
                TODIds.Add(1);
                TODIds.Add(2);
                TODIds.Add(3);
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear && TODIds.Contains((int?)entry.TOD)
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId) && TODIds.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && TODIds.Contains((int?)entry.TOD)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && TODIds.Contains((int?)entry.TOD)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOE"))
            {
                List<int?> TOEIds = new List<int?>();
                TOEIds.Add(-1);
                TOEIds.Add(1);
                TOEIds.Add(2);
                TOEIds.Add(3);
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear && TOEIds.Contains((int?)entry.TOE)
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId) && TOEIds.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && TOEIds.Contains((int?)entry.TOE)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && TOEIds.Contains((int?)entry.TOE)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TODNONKEY"))
            {               
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && entry.KeyId==2
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.KeyId == 2
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TODKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                
            }
            else if (filter.Equals("TOENONKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.KeyId == 2
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOEKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid
                            && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }             
            return detailView;
        }
        
    }
}
