﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.CertificateData;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Web;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using Newtonsoft.Json;
using System.Net;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ComplianceCertificate
    {

        public static void DeleteCertificateTemplate(int temID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceCertificateTransaction complianceToDelete = (from row in entities.ComplianceCertificateTransactions
                                                 where row.ID == temID
                                                   && row.IsActive == true
                                                  select row).FirstOrDefault();

                complianceToDelete.IsActive = false;

                entities.SaveChanges();
            }
        }
        public static bool ExistCertificateTemplate(ComplianceCertificateTransaction certemplate)
        {
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var query = (from row in entities.ComplianceCertificateTransactions
                //             where (row.FromDate >= certemplate.FromDate && row.FromDate <= certemplate.Enddate)
                //             || (row.Enddate >= certemplate.FromDate && row.Enddate <= certemplate.Enddate)
                //             && row.IsActive == true
                //             && row.CustomerID == certemplate.CustomerID
                //             select row);

                var query = (from row in entities.ComplianceCertificateTransactions
                             where
                             ((row.FromDate >= certemplate.FromDate && row.FromDate <= certemplate.Enddate) ||
                            (row.Enddate >= certemplate.FromDate && row.Enddate <= certemplate.Enddate) ||
                            (certemplate.FromDate >= row.FromDate && certemplate.FromDate <= row.Enddate) ||
                            (certemplate.Enddate >= row.FromDate && certemplate.Enddate <= row.Enddate))
                             && row.IsActive == true
                             && row.CustomerID == certemplate.CustomerID
                             select row); 

                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static string GetCertificateTemplateVersion(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.ComplianceCertificateTransactions
                                where row.CustomerID == CustomerID
                                && row.IsActive == true
                                select row).OrderByDescending(entry => entry.Enddate).ToList();

                if (fileData.Count == 0 || fileData == null)
                {
                    return "1.0";
                }
                else
                {
                    var version = fileData.OrderByDescending(entry => entry.Enddate).Take(1).FirstOrDefault();
                    int lastVersion = 0;
                    if (version.Version == null)
                    {
                        return "2.0";
                    }
                    else
                    {
                        lastVersion = Convert.ToInt32(version.Version.Split('.')[0]);
                        lastVersion += 1;
                        return lastVersion + ".0";
                    }
                }
            }
        }
        public static ComplianceCertificateTransaction GetCertificateTemplate(long TemplateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Template = (from row in entities.ComplianceCertificateTransactions
                                         where row.ID == TemplateID
                                         && row.IsActive == true
                                         select row).FirstOrDefault();

                return Template;
            }
        }

        public static ComplianceCertificateTransaction GetCertificateTemplateLatest(long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Template = (from row in entities.ComplianceCertificateTransactions
                                where row.CustomerID == CustomerID
                                  && row.IsActive == true
                                select row).OrderByDescending(entry => entry.Enddate).FirstOrDefault();

                return Template;
            }
        }

        public static bool ExistComplianceInstance(int ComplianceID, int Customerbranchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceInstances
                             where row.IsDeleted == false
                              && row.ComplianceId == ComplianceID
                              && row.CustomerBranchID == Customerbranchid
                             select row).ToList();
                if (query != null)
                {
                    if (query.Count == 0)
                    {
                        return false ;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public static List<Sp_GetStatutoryAssignment_Result> GetStatutoryComplianceAssigned(int CustomerID, int branch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var result = entities.Sp_GetStatutoryAssignment(CustomerID, "S").ToList();

                if (branch != -1)
                {
                    result = result.Where(entry => entry.BranchID == branch).ToList();
                }
                return result;
            }
        }
        public static List<Sp_GetStatutoryAssignment_Result> GetStatutoryEventBaesedComplianceAssigned(int CustomerID, int branch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var result = entities.Sp_GetStatutoryAssignment(CustomerID, "SE").ToList();

                if (branch != -1)
                {
                    result = result.Where(entry => entry.BranchID == branch).ToList();
                }
                return result;
            }
        }
        public static void DeleteFrequencyConfigration(int cerID,int UserID)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {

                Certificate_CustomerFrequencyMapping cerToUpdate = (from row in entities.Certificate_CustomerFrequencyMapping
                                                                    where row.ID == cerID
                                                                    select row).FirstOrDefault();

                cerToUpdate.IsDeleted = 1;
                cerToUpdate.UpdatedBy = UserID;
                cerToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
               
            }
        }
        public static bool ExistsTransaction(int CustomerID)
        {

            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                var query = (from row in entities.Certificate_ScheduleOn
                             join row1 in entities.Certificate_Transaction
                             on row.ID equals row1.Cer_ScheduleOnID
                             where row.CustomerID == CustomerID
                             && row.IsDeleted == 0
                             select row.ID).Distinct().ToList();

                if (query != null && query.Count() > 0)
                    return true;
                else
                    return false;
            }
        }
        public static Certificate_CustomerFrequencyMapping GetCustomerFrequencyMappingID(int FrequencyMappingID)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                var act = (from row in entities.Certificate_CustomerFrequencyMapping
                           where row.ID == FrequencyMappingID && row.IsDeleted == 0
                           select row).FirstOrDefault();

                return act;
            }
        }
        //public static ComplianceCertificateMapping GetComplianceCertificateMapping(long CustId)
        //{
        //    using (CertificateDataEntities entities = new CertificateDataEntities())
        //    {
        //        var Data = (from row in entities.ComplianceCertificateMappings
        //                    where row.CustomerID == CustId
        //                    && row.IsActive == true
        //                    select row).FirstOrDefault();

        //        return Data;
        //    }
        //}
        public static List<Certificate_Frequency> GetCertificateFrequency()
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                var FrequencyDetail = (from row in entities.Certificate_Frequency
                                       select row).ToList();


                return FrequencyDetail;
            }
        }

        public static List<SP_GetCertificate_FrequencyMappedCustomerDeatil_Result> GetFrequencyMappedCustomerDeatil(int UserID)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                var FrequencyDetail = (from row in entities.SP_GetCertificate_FrequencyMappedCustomerDeatil(UserID)
                                       select row).ToList();


                return FrequencyDetail;
            }
        }

        public static int Update(Certificate_CustomerFrequencyMapping cer)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {

                Certificate_CustomerFrequencyMapping cerToUpdate = (from row in entities.Certificate_CustomerFrequencyMapping
                                                                    where row.ID == cer.ID
                                                                    select row).FirstOrDefault();

                //cerToUpdate.Cer_FrequencyID = cer.Cer_FrequencyID;
                //cerToUpdate.StartDate = cer.StartDate;
                cerToUpdate.IsSecondGraceApplicable = cer.IsSecondGraceApplicable;
                cerToUpdate.GracePeriod = cer.GracePeriod;
                cerToUpdate.GracePeriodTwo = cer.GracePeriodTwo;
                cerToUpdate.UpdatedBy = cer.UpdatedBy;
                cerToUpdate.UpdatedOn = cer.UpdatedOn;
                entities.SaveChanges();
                return cer.ID;
            }
        }

        public static bool Exists(Certificate_CustomerFrequencyMapping cer)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                var query = (from row in entities.Certificate_CustomerFrequencyMapping
                             where row.CustomerID == cer.CustomerID && row.Cer_FrequencyID == cer.Cer_FrequencyID
                             && row.IsDeleted == 0
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static int Create(Certificate_CustomerFrequencyMapping cer)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                cer.IsDeleted = 0;

                entities.Certificate_CustomerFrequencyMapping.Add(cer);

                entities.SaveChanges();
                if (cer.ID > 0)
                {
                    return cer.ID;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static void CreateCertificateScheduleOn(Certificate_CustomerFrequencyMapping CustomerFrequencyMapping)
        {
            try
            {
                using (CertificateDataEntities entities = new CertificateDataEntities())
                {
                    if (CustomerFrequencyMapping.Cer_FrequencyID == 1) //Monthly
                    {
                        #region Monthly
                        DateTime nextDate = CustomerFrequencyMapping.StartDate;
                        DateTime curruntDate;

                        curruntDate = DateTime.Now.AddMonths(6);

                        while (nextDate < curruntDate)
                        {
                            string formonth = nextDate.ToString("MMMM-yy");
                            DateTime dtDate = nextDate.AddMonths(1);

                            DateTime DueDate = new DateTime(dtDate.Year, dtDate.Month, CustomerFrequencyMapping.DueDate);

                            DateTime FromDate = new DateTime(nextDate.Year, nextDate.Month, 1);

                            var lastDayOfMonth = DateTime.DaysInMonth(nextDate.Year, nextDate.Month);
                            DateTime EndDate = new DateTime(nextDate.Year, nextDate.Month, lastDayOfMonth);

                            Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                            cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                            cerScheduleon.ScheduleOn = DueDate;
                            cerScheduleon.FromDate = FromDate;
                            cerScheduleon.EndDate = EndDate;
                            cerScheduleon.ForMonth = formonth;
                            cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                            cerScheduleon.CreatedOn = DateTime.Now;
                            entities.Certificate_ScheduleOn.Add(cerScheduleon);
                            entities.SaveChanges();
                            nextDate = nextDate.AddMonths(1);
                        }
                        #endregion
                    }
                    else if (CustomerFrequencyMapping.Cer_FrequencyID == 2) //Quarterly
                    {
                        #region Quarterly
                        DateTime nextDate = CustomerFrequencyMapping.StartDate;

                        DateTime DateFirst1 = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        DateTime DateFirst2 = DateTime.ParseExact("31-03-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        DateTime DateFirst3 = DateTime.ParseExact("01-04-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        DateTime DateFirst4 = DateTime.ParseExact("30-06-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        DateTime DateFirst5 = DateTime.ParseExact("01-07-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        DateTime DateFirst6 = DateTime.ParseExact("30-09-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        DateTime DateFirst7 = DateTime.ParseExact("01-10-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        DateTime DateFirst8 = DateTime.ParseExact("31-12-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        int Quarter = 0;
                        if ((nextDate.Month  >= DateFirst1.Month) && (nextDate.Month  <= DateFirst2.Month))
                        {
                            Quarter = 1;
                        }
                        if ((nextDate.Month >= DateFirst3.Month) && (nextDate.Month <= DateFirst4.Month))
                        {
                            Quarter = 2;
                        }
                        if ((nextDate.Month >= DateFirst5.Month) && (nextDate.Month <= DateFirst6.Month))
                        {
                            Quarter = 3;
                        }
                        if ((nextDate.Month >= DateFirst7.Month) && (nextDate.Month <= DateFirst8.Month))
                        {
                            Quarter = 4;
                        }

                        DateTime curruntDate;
                        curruntDate = DateTime.Now.AddMonths(15);
                        while (nextDate < curruntDate)
                        {
                            if (Quarter == 1)                           
                            {
                                
                                DateTime DueDate = new DateTime(nextDate.Year, 4, CustomerFrequencyMapping.DueDate);
                                DateTime FromDate = new DateTime(nextDate.Year, 1, 1);
                                DateTime EndDate = new DateTime(nextDate.Year, 3, 31);

                                string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                cerScheduleon.ScheduleOn = DueDate;
                                cerScheduleon.FromDate = FromDate;
                                cerScheduleon.EndDate = EndDate;
                                cerScheduleon.ForMonth = formonth;
                                cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                cerScheduleon.CreatedOn = DateTime.Now;
                                entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDate.AddMonths(3);
                                Quarter = 2;
                            }
                            else if (Quarter == 2)
                            { 
                                DateTime DueDate = new DateTime(nextDate.Year, 7, CustomerFrequencyMapping.DueDate);

                                DateTime FromDate = new DateTime(nextDate.Year, 4, 1); 
                                DateTime EndDate = new DateTime(nextDate.Year, 6, 30);

                                string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                cerScheduleon.ScheduleOn = DueDate;
                                cerScheduleon.FromDate = FromDate;
                                cerScheduleon.EndDate = EndDate;
                                cerScheduleon.ForMonth = formonth;
                                cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                cerScheduleon.CreatedOn = DateTime.Now;
                                entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDate.AddMonths(3);
                                Quarter = 3;
                            }
                            else if (Quarter == 3)
                            {
                                DateTime DueDate = new DateTime(nextDate.Year, 10, CustomerFrequencyMapping.DueDate);

                                DateTime FromDate = new DateTime(nextDate.Year, 7, 1);
                                DateTime EndDate = new DateTime(nextDate.Year, 9, 30);

                                string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                cerScheduleon.ScheduleOn = DueDate;
                                cerScheduleon.FromDate = FromDate;
                                cerScheduleon.EndDate = EndDate;
                                cerScheduleon.ForMonth = formonth;
                                cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                cerScheduleon.CreatedOn = DateTime.Now;
                                entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDate.AddMonths(3);
                                Quarter = 4;
                            }
                            else if (Quarter == 4)
                            {
                                DateTime DueDate = new DateTime(nextDate.Year+1,1, CustomerFrequencyMapping.DueDate);

                                DateTime FromDate = new DateTime(nextDate.Year, 10, 1);
                                DateTime EndDate = new DateTime(nextDate.Year, 12, 31);

                                string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                cerScheduleon.ScheduleOn = DueDate;
                                cerScheduleon.FromDate = FromDate;
                                cerScheduleon.EndDate = EndDate;
                                cerScheduleon.ForMonth = formonth;
                                cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                cerScheduleon.CreatedOn = DateTime.Now;
                                entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDate.AddMonths(3);
                                Quarter = 1;
                            }
                        }
                        #endregion
                    }
                    else if (CustomerFrequencyMapping.Cer_FrequencyID == 3) //HalfYearly
                    {
                        #region HalfYearly
                        DateTime nextDate = CustomerFrequencyMapping.StartDate;

                        if(CustomerFrequencyMapping.IsFinancialYear == 1)
                        {
                            #region FinancialYear
                            DateTime DateFirst1 = DateTime.ParseExact("01-04-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            DateTime DateFirst2 = DateTime.ParseExact("30-09-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            DateTime DateFirst3 = DateTime.ParseExact("01-10-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            DateTime DateFirst4 = DateTime.ParseExact("31-03-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            int FirstHalf = 0;
                            if ((nextDate.Month >= DateFirst1.Month) && (nextDate.Month <= DateFirst2.Month))
                            {
                                FirstHalf = 1;
                            }
                            if ((nextDate.Month >= DateFirst3.Month) && (nextDate.Month <= DateFirst4.Month))
                            {
                                FirstHalf = 2;
                            }

                            DateTime curruntDate;
                            curruntDate = DateTime.Now.AddYears(2);
                            while (nextDate < curruntDate)
                            {
                                if (FirstHalf == 1)
                                {
                                    DateTime DueDate = new DateTime(nextDate.Year, 10, CustomerFrequencyMapping.DueDate);
                                    DateTime FromDate = new DateTime(nextDate.Year,4, 1);
                                    DateTime EndDate = new DateTime(nextDate.Year, 9, 30);

                                    string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                    Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                    cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                    cerScheduleon.ScheduleOn = DueDate;
                                    cerScheduleon.FromDate = FromDate;
                                    cerScheduleon.EndDate = EndDate;
                                    cerScheduleon.ForMonth = formonth;
                                    cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                    cerScheduleon.CreatedOn = DateTime.Now;
                                    entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                    entities.SaveChanges();
                                    nextDate = nextDate.AddMonths(6);
                                    FirstHalf = 2;
                                }
                                else if (FirstHalf == 2)
                                {
                                    DateTime DueDate = new DateTime(nextDate.Year, 4, CustomerFrequencyMapping.DueDate);

                                    DateTime FromDate = new DateTime(nextDate.Year, 10, 1);
                                    DateTime EndDate = new DateTime(nextDate.Year, 3, 31);

                                    string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                    Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                    cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                    cerScheduleon.ScheduleOn = DueDate;
                                    cerScheduleon.FromDate = FromDate;
                                    cerScheduleon.EndDate = EndDate;
                                    cerScheduleon.ForMonth = formonth;
                                    cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                    cerScheduleon.CreatedOn = DateTime.Now;
                                    entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                    entities.SaveChanges();
                                    nextDate = nextDate.AddMonths(6);
                                    FirstHalf = 1;
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region CalendarYear

                            DateTime DateFirst1 = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            DateTime DateFirst2 = DateTime.ParseExact("30-06-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                             
                            DateTime DateFirst3 = DateTime.ParseExact("01-07-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            DateTime DateFirst4 = DateTime.ParseExact("31-12-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            int FirstHalf = 0;
                            if ((nextDate.Month >= DateFirst1.Month) && (nextDate.Month <= DateFirst2.Month))
                            {
                                FirstHalf = 1;
                            }
                            if ((nextDate.Month >= DateFirst3.Month) && (nextDate.Month <= DateFirst4.Month))
                            {
                                FirstHalf = 2;
                            } 

                            DateTime curruntDate;
                            curruntDate = DateTime.Now.AddYears(2);
                            while (nextDate < curruntDate)
                            {
                                if (FirstHalf == 1)
                                {
                                    DateTime DueDate = new DateTime(nextDate.Year, 7, CustomerFrequencyMapping.DueDate);
                                    DateTime FromDate = new DateTime(nextDate.Year, 1, 1);
                                    DateTime EndDate = new DateTime(nextDate.Year,6, 30);

                                    string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                    Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                    cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                    cerScheduleon.ScheduleOn = DueDate;
                                    cerScheduleon.FromDate = FromDate;
                                    cerScheduleon.EndDate = EndDate;
                                    cerScheduleon.ForMonth = formonth;
                                    cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                    cerScheduleon.CreatedOn = DateTime.Now;
                                    entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                    entities.SaveChanges();
                                    nextDate = nextDate.AddMonths(6);
                                    FirstHalf = 2;
                                }
                                else if (FirstHalf == 2)
                                {
                                    DateTime DueDate = new DateTime(nextDate.Year+1, 1, CustomerFrequencyMapping.DueDate);

                                    DateTime FromDate = new DateTime(nextDate.Year,7, 1);
                                    DateTime EndDate = new DateTime(nextDate.Year, 12, 31);

                                    string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                    Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                    cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                    cerScheduleon.ScheduleOn = DueDate;
                                    cerScheduleon.FromDate = FromDate;
                                    cerScheduleon.EndDate = EndDate;
                                    cerScheduleon.ForMonth = formonth;
                                    cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                    cerScheduleon.CreatedOn = DateTime.Now;
                                    entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                    entities.SaveChanges();
                                    nextDate = nextDate.AddMonths(6);
                                    FirstHalf = 1;
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else if (CustomerFrequencyMapping.Cer_FrequencyID == 4)//Annual
                    {
                        #region Annual 
                        DateTime nextDate = CustomerFrequencyMapping.StartDate;

                        if (CustomerFrequencyMapping.IsFinancialYear == 1)
                        {
                            #region FinancialYear                              
                            DateTime curruntDate;
                            curruntDate = DateTime.Now.AddYears(3);

                            while (nextDate < curruntDate)
                            {
                                string formonth = "FY "+ nextDate.ToString("yy") + " "+ nextDate.AddYears(1).ToString("yy");
                                DateTime dtDate = nextDate.AddYears(1);

                                DateTime DueDate = new DateTime(dtDate.Year, dtDate.Month, CustomerFrequencyMapping.DueDate);

                                DateTime FromDate = new DateTime(nextDate.Year, 4, 1);
                                DateTime EndDate = new DateTime(nextDate.AddYears(1).Year, 3, 31);

                                Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                cerScheduleon.ScheduleOn = DueDate;
                                cerScheduleon.FromDate = FromDate;
                                cerScheduleon.EndDate = EndDate;
                                cerScheduleon.ForMonth = formonth;
                                cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                cerScheduleon.CreatedOn = DateTime.Now;
                                entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDate.AddYears(1);
                            }
                            #endregion
                        }
                        else
                        {
                            #region CalendarYear

                            DateTime curruntDate;
                            curruntDate = DateTime.Now.AddYears(3);

                            while (nextDate < curruntDate)
                            {
                                string formonth = "CY " + nextDate.ToString("yy");
                                DateTime dtDate = nextDate.AddYears(1);

                                DateTime DueDate = new DateTime(dtDate.Year, dtDate.Month, CustomerFrequencyMapping.DueDate);

                                DateTime FromDate = new DateTime(nextDate.Year, 1, 1);
                                DateTime EndDate = new DateTime(nextDate.AddYears(1).Year, 12, 31);

                                Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                cerScheduleon.ScheduleOn = DueDate;
                                cerScheduleon.FromDate = FromDate;
                                cerScheduleon.EndDate = EndDate;
                                cerScheduleon.ForMonth = formonth;
                                cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                cerScheduleon.CreatedOn = DateTime.Now;
                                entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDate.AddYears(1);
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else if (CustomerFrequencyMapping.Cer_FrequencyID == 5) //FourMonthly
                    {
                        #region FourMonthly
                        DateTime nextDate = CustomerFrequencyMapping.StartDate;

                        DateTime DateFirst1 = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        DateTime DateFirst2 = DateTime.ParseExact("30-04-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        DateTime DateFirst3 = DateTime.ParseExact("01-05-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        DateTime DateFirst4 = DateTime.ParseExact("31-08-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        DateTime DateFirst5 = DateTime.ParseExact("01-09-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        DateTime DateFirst6 = DateTime.ParseExact("31-12-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
 
                        int fourMonthly = 0;
                        if ((nextDate.Month >= DateFirst1.Month) && (nextDate.Month <= DateFirst2.Month))
                        {
                            fourMonthly = 1;
                        }
                        if ((nextDate.Month >= DateFirst3.Month) && (nextDate.Month <= DateFirst4.Month))
                        {
                            fourMonthly = 2;
                        }
                        if ((nextDate.Month >= DateFirst5.Month) && (nextDate.Month <= DateFirst6.Month))
                        {
                            fourMonthly = 3;
                        }
                        

                        DateTime curruntDate;
                        curruntDate = DateTime.Now.AddYears(2);
                        while (nextDate < curruntDate)
                        {
                            if (fourMonthly == 1)
                            {

                                DateTime DueDate = new DateTime(nextDate.Year, 5, CustomerFrequencyMapping.DueDate);
                                DateTime FromDate = new DateTime(nextDate.Year, 1, 1);
                                DateTime EndDate = new DateTime(nextDate.Year, 4, 30);

                                string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                cerScheduleon.ScheduleOn = DueDate;
                                cerScheduleon.FromDate = FromDate;
                                cerScheduleon.EndDate = EndDate;
                                cerScheduleon.ForMonth = formonth;
                                cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                cerScheduleon.CreatedOn = DateTime.Now;
                                entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDate.AddMonths(4);
                                fourMonthly = 2;
                            }
                            else if (fourMonthly == 2)
                            {
                                DateTime DueDate = new DateTime(nextDate.Year, 9, CustomerFrequencyMapping.DueDate);

                                DateTime FromDate = new DateTime(nextDate.Year, 5, 1);
                                DateTime EndDate = new DateTime(nextDate.Year, 8, 31);

                                string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                cerScheduleon.ScheduleOn = DueDate;
                                cerScheduleon.FromDate = FromDate;
                                cerScheduleon.EndDate = EndDate;
                                cerScheduleon.ForMonth = formonth;
                                cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                cerScheduleon.CreatedOn = DateTime.Now;
                                entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDate.AddMonths(4);
                                fourMonthly = 3;
                            }
                            else if (fourMonthly == 3)
                            {
                                DateTime DueDate = new DateTime(nextDate.AddDays(1).Year, 1, CustomerFrequencyMapping.DueDate);

                                DateTime FromDate = new DateTime(nextDate.Year, 9, 1);
                                DateTime EndDate = new DateTime(nextDate.Year, 12, 31);

                                string formonth = FromDate.ToString("MMM yy") + " - " + EndDate.ToString("MMM yy");

                                Certificate_ScheduleOn cerScheduleon = new Certificate_ScheduleOn();
                                cerScheduleon.CustomerID = CustomerFrequencyMapping.CustomerID;
                                cerScheduleon.ScheduleOn = DueDate;
                                cerScheduleon.FromDate = FromDate;
                                cerScheduleon.EndDate = EndDate;
                                cerScheduleon.ForMonth = formonth;
                                cerScheduleon.CreatedBy = CustomerFrequencyMapping.CreatedBy;
                                cerScheduleon.CreatedOn = DateTime.Now;
                                entities.Certificate_ScheduleOn.Add(cerScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDate.AddMonths(4);
                                fourMonthly = 4;
                            }
                             
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceCertificate";
                msg.FunctionName = "CreateCertificateScheduleOn" + "CustomerID=" + 1 + "complianceInstanceId=" + 1;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateCertificateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

            }
        }
    }
}
