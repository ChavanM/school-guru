﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class VerifyOTPManagement
    {
        public static SMSConfiguration GetSMSConfiguration(string smsType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var data = (from row in entities.SMSConfigurations
                            where row.SMSType == smsType
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static bool verifyOTP(long providedOTP, int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.VerifyOTPs
                                //where row.OTP == OTP &&
                            where row.UserId == userID
                            orderby row.VId descending
                            select row).Take(1).Where(entry => entry.OTP == providedOTP).FirstOrDefault();
                if (data == null)
                    return false;
                else
                    return true;
            }
        }
        public static void Create(VerifyOTP OTP)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                // OTP.CreatedOn = DateTime.UtcNow;

                entities.VerifyOTPs.Add(OTP);
                entities.SaveChanges();
            }
        }


        public static void SendOTPMail(string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message)
        {

            using (SmtpClient email = new SmtpClient())
            using (MailMessage mailMessage = new MailMessage())
            {
                email.DeliveryMethod = SmtpDeliveryMethod.Network;
                email.UseDefaultCredentials = false;
                NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                email.Credentials = credential;
                email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                email.Timeout = 60000;
                email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                MailMessage oMessage = new MailMessage();
                string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                mailMessage.From = new MailAddress(FromEmailId);
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));

                {
                    if (to != null)
                        to.ForEach(entry => mailMessage.To.Add(entry));
                    if (cc != null)
                        cc.ForEach(entry => mailMessage.CC.Add(entry));
                    if (bcc != null)
                        bcc.ForEach(entry => mailMessage.Bcc.Add(entry));
                }
                email.Send(mailMessage);
            }

        }

        public static bool Exists(long OTP, int Uid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var data = (from row in entities.VerifyOTPs
                            where row.OTP == OTP && row.UserId == Uid
                            orderby row.VId descending
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }

                //var query = (from row in entities.VerifyOTPs
                //             where row.OTP.Equals(OTP) && row.UserId.Equals(Uid) orderby row.VId descending
                //             select row);
                //return query.Select(entry => true).SingleOrDefault();
            }
        }


        public static VerifyOTP GetByID(int UserId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.VerifyOTPs
                            where row.UserId == UserId
                            orderby row.VId descending
                            select row).FirstOrDefault();
                return data;
            }
        }

        public static void UpdateVerifiedFlag(int UserId, long OTP)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.VerifyOTPs
                            where row.UserId == UserId
                            && row.OTP == OTP
                            select row).FirstOrDefault();

                if (data != null)
                {
                    data.IsVerified = true;
                    entities.SaveChanges();
                }
            }
        }

        public static List<VerifyOTP> GetResendOTPCount(int userID, int OTPValue)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.VerifyOTPs
                            where row.UserId == userID
                            && row.OTP == OTPValue
                            && row.IsVerified == false
                            select row).ToList();

                return data;
            }
        }
    }
}
