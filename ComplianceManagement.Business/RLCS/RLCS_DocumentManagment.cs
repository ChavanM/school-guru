﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCS_DocumentManagement
    {
        public static long RLCS_GenerateDocumentRequest_Register(int customerID, int branchID, string recordType, int userID, RLCS_Request_DownloadRegister objRecord)
        {
            long recordID = 0;
            try
            {
                RLCS_DocumentGeneration newRecord = new RLCS_DocumentGeneration()
                {
                    RecordType = recordType,
                    AVACOM_CustomerID = customerID,
                    AVACOM_BranchID = branchID,
                    Month = objRecord.MonthID,
                    Year = objRecord.YearID,
                    RegChallanReturnType = objRecord.status,
                    ClientID = objRecord.CM_ClientID,
                    StateIDCodeValue = objRecord.StateID,
                    LocationID = objRecord.LocationID,
                    Branch = objRecord.BranchID,
                    RegisterReturnID = objRecord.RegisterID,
                    //DownloadFormatType=objRecord.DownloadFormatType
                    IsActive = true,
                    IsProcessed = false,
                    CreatedBy = userID,
                    UpdatedBy = userID,
                };

                recordID = CreateUpdate_RLCS_DocumentGeneration(newRecord);
                return recordID;
            }
            catch(Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return recordID;
            }
        }

        public static long RLCS_GenerateDocumentRequest_Challan(int customerID, int branchID, string recordType, int userID, RLCS_Request_DownloadChallan objRecord)
        {
            long recordID = 0;
            try
            {
                RLCS_DocumentGeneration newRecord = new RLCS_DocumentGeneration()
                {
                    RecordType = recordType,
                    AVACOM_CustomerID = customerID,
                    AVACOM_BranchID = branchID,
                    Month = objRecord.Month,
                    Year = objRecord.Year,
                    RegChallanReturnType = objRecord.ChallanType,
                    ClientID = objRecord.ClientID,
                    StateIDCodeValue = objRecord.Code,
                    //LocationID = objRecord.LocationID,
                    //Branch = objRecord.BranchID,
                    //RegisterID = objRecord.RegisterID,
                    //DownloadFormatType=objRecord.DownloadFormatType
                    IsActive = true,
                    IsProcessed = false,
                    CreatedBy = userID,
                    UpdatedBy = userID,
                };

                recordID = CreateUpdate_RLCS_DocumentGeneration(newRecord);
                return recordID;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return recordID;
            }
        }

        public static long RLCS_GenerateDocumentRequest_Return(int customerID, int branchID, string recordType, int userID, RLCS_ReturnRecords_Post objRecord)
        {
            long recordID = 0;
            try
            {
                foreach (var record in objRecord.lst_ReturnFormats)
                {
                    RLCS_DocumentGeneration newRecord = new RLCS_DocumentGeneration();

                    newRecord.RecordType = recordType;
                    newRecord.AVACOM_CustomerID = customerID;
                    newRecord.AVACOM_BranchID = branchID;
                    newRecord.Month = record.Month;
                    newRecord.Year = record.Year;
                    newRecord.RegChallanReturnType = record.R_Type;
                    newRecord.ClientID = record.ClientId;
                    newRecord.StateIDCodeValue = record.SM_Code;
                    newRecord.LocationID = record.LocationID;
                    newRecord.Branch = record.BranchID;
                    newRecord.RegisterReturnID = record.FormID;
                    newRecord.Frequency = record.Frequency_ID;
                    newRecord.PF_ESI_Code = record.PF_ESI_Code;
                    newRecord.EMPID = record.EMPID;
                    newRecord.IsActive = true;
                    newRecord.IsProcessed = false;
                    newRecord.CreatedBy = userID;
                    newRecord.UpdatedBy = userID;
                    recordID = CreateUpdate_RLCS_DocumentGeneration(newRecord);
                }
                

                return recordID;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return recordID;
            }
        }

        public static long CreateUpdate_RLCS_DocumentGeneration(RLCS_DocumentGeneration _objRecord)
        {
            try
            {
                long newRecordID = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_DocumentGeneration
                                      where row.RecordType.Trim().ToUpper().Equals(_objRecord.RecordType.Trim().ToUpper())
                                      && row.RegChallanReturnType.Trim().ToUpper().Equals(_objRecord.RegChallanReturnType.Trim().ToUpper())
                                      && row.Month.Trim().ToUpper().Equals(_objRecord.Month.Trim().ToUpper())
                                      && row.Year.Trim().ToUpper().Equals(_objRecord.Year.Trim().ToUpper())
                                      && row.ClientID.Trim().ToUpper().Equals(_objRecord.ClientID.Trim().ToUpper())
                                      && row.StateIDCodeValue.Trim().ToUpper().Equals(_objRecord.StateIDCodeValue.Trim().ToUpper())
                                      && row.LocationID == _objRecord.LocationID
                                      && row.Branch == _objRecord.Branch
                                      && row.RegisterReturnID == _objRecord.RegisterReturnID
                                      && row.AVACOM_BranchID == _objRecord.AVACOM_BranchID
                                      && row.AVACOM_CustomerID == _objRecord.AVACOM_CustomerID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.IsProcessed = _objRecord.IsProcessed;
                        prevRecord.IsActive = _objRecord.IsActive;

                        prevRecord.AVACOM_CustomerID = _objRecord.AVACOM_CustomerID;
                        prevRecord.AVACOM_BranchID = _objRecord.AVACOM_BranchID;

                        prevRecord.UpdatedBy = _objRecord.UpdatedBy;
                        prevRecord.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();

                        newRecordID = prevRecord.ID;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;

                        entities.RLCS_DocumentGeneration.Add(_objRecord);
                        entities.SaveChanges();

                        newRecordID = _objRecord.ID;
                    }

                   
                    return newRecordID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool UpdateStatus_RLCS_DocumentGeneration(long recordID, int userID, bool isProcessed)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_DocumentGeneration
                                      where row.ID == recordID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.IsProcessed = isProcessed;
                        prevRecord.UpdatedOn = DateTime.Now;
                        prevRecord.UpdatedBy = userID;

                        entities.SaveChanges();
                        saveSuccess = true;
                    }

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static long RLCS_GenerateDocumentRequest_Return(int customerID, int branchID, string recordType, int userID, RLCS_Request_DownloadChallan objRecord)
        {
            long recordID = 0;
            try
            {
                RLCS_DocumentGeneration newRecord = new RLCS_DocumentGeneration()
                {
                    RecordType = recordType,
                    AVACOM_CustomerID = customerID,
                    AVACOM_BranchID = branchID,
                    Month = objRecord.Month,
                    Year = objRecord.Year,
                    RegChallanReturnType = objRecord.ChallanType,
                    ClientID = objRecord.ClientID,
                    StateIDCodeValue = objRecord.Code,
                    //LocationID = objRecord.LocationID,
                    //Branch = objRecord.BranchID,
                    //RegisterID = objRecord.RegisterID,
                    //DownloadFormatType=objRecord.DownloadFormatType
                    IsActive = true,
                    IsProcessed = false,
                    CreatedBy = userID,
                    UpdatedBy = userID,
                };

                recordID = CreateUpdate_RLCS_DocumentGeneration(newRecord);
                return recordID;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return recordID;
            }
        }

        public static bool checkDuplicateDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, int colNum, string providedText)
        {
            bool matchSuccess = false;
            try
            {
                if (xlWorksheet != null)
                {
                    string TextToCompare = string.Empty;
                    int lastRow = xlWorksheet.Dimension.End.Row;

                    for (int i = 2; i <= lastRow; i++)
                    {
                        if (i != currentRowNumber)
                        {
                            TextToCompare = xlWorksheet.Cells[i, colNum].Text.ToString().Trim();
                            if (String.Equals(providedText, TextToCompare, StringComparison.OrdinalIgnoreCase))
                            {
                                matchSuccess = true;
                                i = lastRow; //exit from for Loop
                            }
                        }
                    }
                }

                return matchSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return matchSuccess;
            }
        }


        public static bool checkMultipleDuplicateDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, int[] colNumers)
        {
            bool matchSuccess = false;
            try
            {
                if (xlWorksheet != null)
                {
                    string TextToCompare = string.Empty;
                    int lastRow = xlWorksheet.Dimension.End.Row;
                    string providedText = string.Empty;

                    for (int i = 2; i <= lastRow; i++)
                    {
                        if (i != currentRowNumber)
                        {
                            //TextToCompare = xlWorksheet.Cells[i, colNum].Text.ToString().Trim();
                            //if (String.Equals(providedText, TextToCompare, StringComparison.OrdinalIgnoreCase))
                            //{
                            //    matchSuccess = true;
                            //    i = lastRow; //exit from for Loop
                            //}

                            bool flgMatchAll = true;
                            foreach (int col in colNumers)
                            {
                                providedText = xlWorksheet.Cells[currentRowNumber, col].Text.ToString().Trim();
                                TextToCompare = xlWorksheet.Cells[i, col].Text.ToString().Trim();
                                if (!String.Equals(providedText, TextToCompare, StringComparison.OrdinalIgnoreCase))
                                {
                                    flgMatchAll = false;
                                    break;
                                }
                            }

                            if(flgMatchAll)
                            {
                                i = lastRow; //exit from for Loop
                                matchSuccess = true;
                            }
                                
                        }
                    }
                }

                return matchSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return matchSuccess;
            }
        }

        public static bool GetRLCSDocuments(string type, List<long> lstScheduleOnIDs)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile RLCSDocument = new ZipFile())
                {
                    int i = 0;

                    foreach (var eachScheduleOnOD in lstScheduleOnIDs)
                    {
                        List<int> objfile = RLCSManagement.GetFileIDs(eachScheduleOnOD);

                        if (objfile.Count > 0)
                        {
                            List<FileData> fileData = RLCSManagement.GetFileData(objfile);

                            if (fileData.Count > 0)
                            {
                                //int i = 0;
                                foreach (var files in fileData)
                                {
                                    string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.Name));

                                    if (files.FilePath != null && File.Exists(filePath))
                                    {
                                        documentSuccess = true;
                                        int idx = files.Name.LastIndexOf('.');
                                        //string str = files.Name.Substring(0, idx) + "_" + i + "." + files.Name.Substring(idx + 1);
                                        string str = files.Name.Substring(0, idx) + "." + files.Name.Substring(idx + 1);
                                        string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                                        if (!RLCSDocument.ContainsEntry("Document" + "/" + str))
                                        {
                                            if (files.EnType == "M")
                                            {
                                                RLCSDocument.AddEntry("Document" + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                RLCSDocument.AddEntry("Document" + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                    }

                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        RLCSDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + type + "-Documents-" + DateTime.Now.ToString("ddMMyy") + ".zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch(Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool GetChallanDocuments_RLCS_WithBranchName(string type, List<Tuple<string, List<long>>> lstScheduleOnIDsWithBranchName)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile RLCSDocument = new ZipFile())
                {
                    int folderCount = 0;

                    lstScheduleOnIDsWithBranchName.ForEach(eachRecord =>
                    {
                        folderCount++;

                        if (!string.IsNullOrEmpty(eachRecord.Item1) && lstScheduleOnIDsWithBranchName.Count > 0)
                        {
                            int i = 1;

                            string branchname = eachRecord.Item1;

                            foreach (var eachScheduleOnOD in eachRecord.Item2)
                            {
                                List<int> objfile = RLCSManagement.GetFileIDs(eachScheduleOnOD);

                                if (objfile.Count > 0)
                                {
                                    List<FileData> filesData = RLCSManagement.GetFileData(objfile);

                                    if (filesData.Count > 0)
                                    {
                                        //int i = 0;
                                        foreach (var file in filesData)
                                        {
                                            string filePath = Path.Combine(HostingEnvironment.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                documentSuccess = true;
                                                //int idx = files.Name.LastIndexOf('.');
                                                //string str = i + "_" + files.Name.Substring(0, idx)  + "." + files.Name.Substring(idx + 1);

                                                string str = i + "_" + file.Name;
                                                
                                                string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                                
                                                if (!RLCSDocument.ContainsEntry(branchname + "/" + str))
                                                {
                                                    if (file.EnType == "M")
                                                    {
                                                        RLCSDocument.AddEntry(branchname + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        RLCSDocument.AddEntry(branchname + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });

                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        RLCSDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + type + "-Documents-" + DateTime.Now.ToString("ddMMyy") + ".zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool GetRegisterDocuments_RLCS_WithBranchName(string type, List<Tuple<int, List<long>>> lstScheduleOnIDsWithBranchID)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile RLCSDocument = new ZipFile())
                {
                    int folderCount = 0;
                    lstScheduleOnIDsWithBranchID.ForEach(eachRecord =>
                    {
                        folderCount++;

                        if (eachRecord.Item1 != 0)
                        {
                            int i = 1;

                            string branchname = RLCSManagement.GetRLCSBranchNameByID(eachRecord.Item1, "B");

                            if (String.IsNullOrEmpty(branchname))
                                branchname = "Document" + folderCount;                           

                            foreach (var eachScheduleOnOD in eachRecord.Item2)
                            {
                                List<int> objfile = RLCSManagement.GetFileIDs(eachScheduleOnOD);

                                if (objfile.Count > 0)
                                {
                                    List<FileData> fileData = RLCSManagement.GetFileData(objfile);

                                    if (fileData.Count > 0)
                                    {
                                        //int i = 0;
                                        foreach (var files in fileData)
                                        {
                                            string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.Name));

                                            if (files.FilePath != null && File.Exists(filePath))
                                            {
                                                documentSuccess = true;
                                                //int idx = files.Name.LastIndexOf('.');
                                                //string str = i + "_" + files.Name.Substring(0, idx)  + "." + files.Name.Substring(idx + 1);

                                                string str = i + "_" + files.Name;
                                                string Dates = DateTime.Now.ToString("dd/mm/yyyy");


                                                if (!RLCSDocument.ContainsEntry(branchname + "/" + str))
                                                {
                                                    if (files.EnType == "M")
                                                    {
                                                        RLCSDocument.AddEntry(branchname + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        RLCSDocument.AddEntry(branchname + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });

                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        RLCSDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + type + "-Documents-" + DateTime.Now.ToString("ddMMyy") + ".zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool GetRLCSDocumentsByFileTypeId(string type, List<long> lstScheduleOnIDs, int FileTypeId, string StateCode)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile RLCSDocument = new ZipFile())
                {
                    foreach (var eachScheduleOnOD in lstScheduleOnIDs)
                    {
                        List<int> objfile = RLCSManagement.GetFileIDByFileType_StateCode(eachScheduleOnOD, FileTypeId, StateCode);

                        if (objfile.Count > 0)
                        {
                            List<FileData> fileData = RLCSManagement.GetFileData(objfile);

                            if (fileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var files in fileData)
                                {
                                    string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.Name));

                                    if (files.FilePath != null && File.Exists(filePath))
                                    {
                                        documentSuccess = true;
                                        int idx = files.Name.LastIndexOf('.');
                                        string str = files.Name.Substring(0, idx) + "_" + i + "." + files.Name.Substring(idx + 1);
                                        string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                                        if (!RLCSDocument.ContainsEntry("Document" + "/" + str))
                                        {
                                            if (files.EnType == "M")
                                            {
                                                RLCSDocument.AddEntry("Document" + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                RLCSDocument.AddEntry("Document" + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                    }

                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        RLCSDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + type + "-Documents.zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }


        public static bool GetRLCSDocumentsByFileTypeId(string type, List<long> lstScheduleOnIDs, int FileTypeId)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile RLCSDocument = new ZipFile())
                {
                    foreach (var eachScheduleOnOD in lstScheduleOnIDs)
                    {
                        List<int> objfile = RLCSManagement.GetFileIDByFileTypeId(eachScheduleOnOD, FileTypeId);

                        if (objfile.Count > 0)
                        {
                            List<FileData> fileData = RLCSManagement.GetFileData(objfile);

                            if (fileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var files in fileData)
                                {
                                    string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.Name));

                                    if (files.FilePath != null && File.Exists(filePath))
                                    {
                                        documentSuccess = true;
                                        int idx = files.Name.LastIndexOf('.');
                                        string str = files.Name.Substring(0, idx) + "_" + i + "." + files.Name.Substring(idx + 1);
                                        string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                                        if (!RLCSDocument.ContainsEntry("Document" + "/" + str))
                                        {
                                            if (files.EnType == "M")
                                            {
                                                RLCSDocument.AddEntry("Document" + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                RLCSDocument.AddEntry("Document" + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                    }

                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        RLCSDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + type + "-Documents.zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool GetRLCSDocumentsByFileTypeId(string type, List<long> lstScheduleOnIDs, int FileTypeId, string challanType, string Branch)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile RLCSDocument = new ZipFile())
                {
                    foreach (var eachScheduleOnOD in lstScheduleOnIDs)
                    {
                        List<int> objfile = new List<int>(); 

                        if (challanType.Trim().ToUpper().Equals("ESI") || challanType.Trim().ToUpper().Equals("EPF"))
                        {
                            objfile = RLCSManagement.GetFileIDByFileTypeId(eachScheduleOnOD, FileTypeId, Branch);
                        }
                        else
                        {
                            objfile = RLCSManagement.GetFileIDByFileTypeId(eachScheduleOnOD, FileTypeId);
                        }

                        if (objfile.Count > 0)
                        {
                            List<FileData> fileData = RLCSManagement.GetFileData(objfile);

                            if (fileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var files in fileData)
                                {
                                    string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.Name));

                                    if (files.FilePath != null && File.Exists(filePath))
                                    {
                                        documentSuccess = true;
                                        int idx = files.Name.LastIndexOf('.');
                                        string str = files.Name.Substring(0, idx) + "_" + i + "." + files.Name.Substring(idx + 1);
                                        string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                                        if (!RLCSDocument.ContainsEntry("Document" + "/" + str))
                                        {
                                            if (files.EnType == "M")
                                            {
                                                RLCSDocument.AddEntry("Document" + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                RLCSDocument.AddEntry("Document" + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                    }

                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        RLCSDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + type + "-Documents.zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool GetRLCSDocuments_Historical(long recordID, string docType, string fileName)
        {
            try
            {
                bool documentSuccess = false;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var file = GetHistoricalDocuments(recordID, docType);

                    if (file.Count > 0)
                    {
                        using (ZipFile RLCSDocument = new ZipFile())
                        {
                            int i = 0;
                            foreach (var files in file)
                            {
                                string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.FileName));
                                if (files.FilePath != null && File.Exists(filePath))
                                {
                                    documentSuccess = true;

                                    int idx = files.FileName.LastIndexOf('.');
                                    string str = files.FileName.Substring(0, idx) + "_" + i + "." + files.FileName.Substring(idx + 1);
                                    string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                    if (files.EnType == "M")
                                    {
                                        RLCSDocument.AddEntry("Document" + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        RLCSDocument.AddEntry("Document" + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));

                                    }
                                    i++;
                                }
                            }

                            if (documentSuccess)
                            {
                                HttpResponse response = HttpContext.Current.Response;
                                var zipMs = new MemoryStream();
                                RLCSDocument.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                response.Buffer = true;
                                response.ClearContent();
                                response.ClearHeaders();
                                response.Clear();
                                response.ContentType = "application/zip";
                                response.AddHeader("content-disposition", "attachment; filename=" + fileName + "Documents-" + DateTime.Now.ToString("ddMMyy") + ".zip");
                                response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }

                            return documentSuccess;
                        }
                    }
                    else
                    {
                        return documentSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool GetRLCSDocuments_Historical_Multiple(string[] recordIDs, string docType, string fileName)
        {
            try
            {
                bool documentSuccess = false;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (recordIDs.Length > 0)
                    {
                        using (ZipFile RLCSDocument = new ZipFile())
                        {
                            int i = 1;
                            int folderCount = 0;
                            foreach (var item in recordIDs)
                            {
                                folderCount++;

                                string branchname = RLCSManagement.GetRLCSBranchNameByHistoricalRecordID(Convert.ToInt64(item));

                                if (String.IsNullOrEmpty(branchname))
                                    branchname = "Document" + folderCount;

                                var file = GetHistoricalDocuments(Convert.ToInt64(item), docType);
                                if (file.Count > 0)
                                {
                                    foreach (var files in file)
                                    {
                                        string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.FileName));
                                        if (files.FilePath != null && File.Exists(filePath))
                                        {
                                            documentSuccess = true;

                                            //int idx = files.FileName.LastIndexOf('.');
                                            //string str = i + "_" + files.FileName.Substring(0, idx) + "." + files.FileName.Substring(idx + 1);

                                            string str = i + "_" + files.FileName;

                                            string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                            if (files.EnType == "M")
                                            {
                                                RLCSDocument.AddEntry(branchname + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                RLCSDocument.AddEntry(branchname + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }

                            if (documentSuccess)
                            {
                                HttpResponse response = HttpContext.Current.Response;
                                var zipMs = new MemoryStream();
                                RLCSDocument.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                response.Buffer = true;
                                response.ClearContent();
                                response.ClearHeaders();
                                response.Clear();
                                response.ContentType = "application/zip";
                                response.AddHeader("content-disposition", "attachment; filename=" + fileName + "-Documents.zip");
                                response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }

                            return documentSuccess;
                        }
                    }
                    else
                    {
                        return documentSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private static List<RLCS_HistoricalDocument_FileData> GetHistoricalDocuments(long recordID, string docType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var file = (from row in entities.RLCS_HistoricalDocument_FileData
                            where row.RecordID == recordID
                            && row.DocType == docType
                            && row.IsDeleted == false
                            select row).ToList();

                return file;
            }
        }

        public static bool DownloadFile(string urlAddress, string fileSavePath)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    //DownlaodFile method directely downlaod file on you given specific path ,Here i've saved in E: Drive
                    client.DownloadFile(urlAddress, fileSavePath);
                    client.Dispose();
                    return true;
                }
                //WebClient client = null;
                //client = new WebClient();                             
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteFile(string fileSavePath, int fileID)
        {
            try
            {
                if (System.IO.File.Exists(fileSavePath))
                {
                    System.IO.File.Delete(fileSavePath);
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var filetoDelete = entities.FileDatas.Where(row => row.ID == fileID).FirstOrDefault();

                    if (filetoDelete != null)
                    {
                        filetoDelete.IsDeleted = true;
                        entities.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        //Writer.Write(CryptographyHandler.Encrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }

                }
            }
        }

        public static bool DownloadFileAndSaveDocuments(string downloadFilePath, string fileName, out string downloadedFilePath)
        {
            bool downloadSuccess = false;
            downloadedFilePath = string.Empty;

            try
            {
                string directoryPath = string.Empty;
                string AppLocation = string.Empty;
                string DocAPIURlwithPATH = string.Empty;

                AppLocation = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
                AppLocation = AppLocation.Replace("file:\\", "");

                //directoryPath = AppLocation + "\\RLCSFiles\\" + downloadFilePath;
                directoryPath = AppLocation + "\\RLCSFiles\\" + Convert.ToString(DateTime.Now.ToString("ddMMyy")) + "\\" + Convert.ToString(DateTime.Now.ToString("HHmmss")) + DateTime.Now.ToString("ffff");

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                directoryPath = directoryPath + @"\" + fileName;

                DocAPIURlwithPATH = downloadFilePath;

                if (!string.IsNullOrEmpty(directoryPath))
                {
                    downloadSuccess = DownloadFile(DocAPIURlwithPATH, directoryPath);

                    if (downloadSuccess)
                        downloadedFilePath = directoryPath;
                }

                return downloadSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return downloadSuccess;
            }
        }

        public static bool SaveAsAVACOMDocument(string StoredFilePath, int customerID, int customerBranchID, long complianceID, long compInstanceID, long complianceScheduleOnID, long complianceTxnID, int fileType)
        {
            bool saveSuccess = false;
            try
            {
                if (File.Exists(StoredFilePath))
                {
                    string directoryPath = string.Empty;
                    string version = string.Empty;

                    //string fileName = Path.GetFileNameWithoutExtension(StoredFilePath);
                    string fileName = Path.GetFileName(StoredFilePath);

                    var InstanceData = RLCS_ComplianceManagement.GetComplianceInstanceData(Convert.ToInt32(compInstanceID));
                    //version = ComplianceManagement.GetDocumentVersion(Convert.ToInt32(complianceScheduleOnID));
                    //version = ComplianceManagement.GetDocumentVersion(Convert.ToInt32(complianceScheduleOnID), fileName);

                    version = RLCS_DocumentManagement.GetDocumentVersion(Convert.ToInt32(complianceScheduleOnID), fileName, fileType);

                    if (customerID != 0 && InstanceData != null)
                    {
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnID + "\\" + version;
                        }
                        else
                        {
                            directoryPath = ConfigurationManager.AppSettings["AVACOM_FileStorage_Path"] + "/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnID + "/" + version;
                        }

                        if (!Directory.Exists(directoryPath))
                            Directory.CreateDirectory(directoryPath);

                        Guid fileKey = Guid.NewGuid();
                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(fileName));

                        string filepathvalue = string.Empty;
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                            filepathvalue = vale.Replace(@"\", "/");
                        }
                        else
                        {
                            filepathvalue = directoryPath.Substring(ConfigurationManager.AppSettings["AVACOM_FileStorage_Path"].Length).Replace('\\', '/').Insert(0, "~/");
                        }

                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                        Stream fs = File.OpenRead(StoredFilePath);

                        long fileSize = fs.Length;

                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                        list.Add(new KeyValuePair<string, int>(fileName, fileType));

                        //List<FileData> files = new List<FileData>();
                        FileData newFile = new FileData()
                        {
                            Name = fileName,
                            FilePath = filepathvalue,
                            FileKey = fileKey.ToString(),
                            //Version = "1.0",
                            Version = version,
                            //VersionDate = DateTime.UtcNow,
                            VersionDate = DateTime.Now,
                            EnType = "A",
                            FileSize= fileSize
                        };

                        //files.Add(newFile);
                        //saveSuccess = ComplianceDocumentManagement.CreateFileDataMapping(complianceTxnID, complianceScheduleOnID, files, list, Filelist);
                        saveSuccess = CreateFileDataMapping(complianceTxnID, complianceScheduleOnID, newFile, list, Filelist);
                    }
                }

                return saveSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        public static bool CreateFileDataMapping(long transactionID, long complianceScheduleOnID, FileData fileDataRecord, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            SaveDocFiles(filesList);

                            int fileType = 2;

                            if (list != null)
                            {
                                fileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fileDataRecord.Name)).FirstOrDefault().Value);
                            }

                            if (fileDataRecord != null)
                            {
                                var previousFileDataMapping = CheckPreviousSameDocument(complianceScheduleOnID, fileType, fileDataRecord.Name);

                                if (previousFileDataMapping == null)
                                {
                                    fileDataRecord.IsDeleted = false;
                                    entities.FileDatas.Add(fileDataRecord);
                                    entities.SaveChanges();

                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = transactionID;
                                    fileMapping.FileID = fileDataRecord.ID;
                                    fileMapping.ScheduledOnID = complianceScheduleOnID;

                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(fileType);
                                    }

                                    entities.FileDataMappings.Add(fileMapping);
                                }
                                else //Same file Exists
                                {
                                    //Create New FileData
                                    fileDataRecord.IsDeleted = false;
                                    entities.FileDatas.Add(fileDataRecord);
                                    entities.SaveChanges();

                                    //UpdateFileDataMapping
                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = previousFileDataMapping.TransactionID;
                                    fileMapping.FileID = fileDataRecord.ID;
                                    fileMapping.ScheduledOnID = previousFileDataMapping.ScheduledOnID;

                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(fileType);
                                    }
                                    
                                    var prevFDMRecord = (from FDM in entities.FileDataMappings
                                                         join FD in entities.FileDatas
                                                         on FDM.FileID equals FD.ID
                                                         where FDM.TransactionID == fileMapping.TransactionID
                                                         && FDM.ScheduledOnID == fileMapping.ScheduledOnID
                                                         && FDM.FileType == fileType
                                                         && FD.Name.Trim().Equals(fileDataRecord.Name.Trim())
                                                         select FDM).FirstOrDefault();

                                    //var prevFDMRecord = (from FDM in entities.FileDataMappings
                                    //                     where FDM.TransactionID == fileMapping.TransactionID
                                    //                     && FDM.ScheduledOnID == fileMapping.ScheduledOnID
                                    //                     && FDM.FileType == fileType
                                    //                     select FDM).FirstOrDefault();


                                    if (prevFDMRecord != null)
                                    {
                                        prevFDMRecord.FileID = fileMapping.FileID;
                                        prevFDMRecord.FileType = fileMapping.FileType;
                                    }
                                    else
                                        entities.FileDataMappings.Add(fileMapping);
                                }
                            }

                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }

                            ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateFileDataMappingV2(long transactionID, long complianceScheduleOnID, FileData fileDataRecord, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Statutory_SaveDocFiles(filesList);

                            int fileType = 2;

                            if (list != null)
                            {
                                fileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fileDataRecord.Name)).FirstOrDefault().Value);
                            }

                            if (fileDataRecord != null)
                            {
                                fileDataRecord.IsDeleted = false;
                                fileDataRecord.EnType = "A";

                                entities.FileDatas.Add(fileDataRecord);
                                entities.SaveChanges();

                                FileDataMapping fileMapping = new FileDataMapping();
                                fileMapping.TransactionID = transactionID;
                                fileMapping.FileID = fileDataRecord.ID;
                                fileMapping.ScheduledOnID = complianceScheduleOnID;
                                if (list != null)
                                {
                                    fileMapping.FileType = Convert.ToInt16(fileType);
                                }
                                entities.FileDataMappings.Add(fileMapping);

                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }

                            ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static FileDataMapping CheckPreviousSameDocument(long scheduledOnID, int fileType, string fileName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from FD in entities.FileDatas
                                join FDM in entities.FileDataMappings
                                on FD.ID equals FDM.FileID
                                where FDM.ScheduledOnID == scheduledOnID
                                //&& FDM.TransactionID == transactionID
                                && FDM.FileType == fileType
                                && FD.Name.Equals(fileName)
                                select FDM).FirstOrDefault();

                return fileData;
            }
        }

        public static string GetDocumentVersion(int scheduledOnID, string fileName, int fileType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from FD in entities.FileDatas
                                join FDM in entities.FileDataMappings
                                on FD.ID equals FDM.FileID
                                where FDM.ScheduledOnID == scheduledOnID
                                //&& FDM.TransactionID == transactionID
                                && FDM.FileType == fileType
                                && FD.Name.Equals(fileName)
                                select FD).ToList();

                //var fileData = (from row in entities.ComplianceDocumentsViews
                //                where row.ScheduledOnID == scheduledOnID
                //                && row.FileName.Equals(fileName)                                                                
                //                select row).GroupBy(entry => entry.FileID).Select(en => en.FirstOrDefault()).ToList();

                if (fileData.Count == 0 || fileData == null)
                {
                    return "1.0";
                }
                else
                {
                    var version = fileData.OrderByDescending(entry => entry.ID).Take(1).FirstOrDefault();
                    int lastVersion = 0;
                    if (version.Version == null)
                    {
                        return "2.0";
                    }
                    else
                    {
                        lastVersion = Convert.ToInt32(version.Version.Split('.')[0]);
                        lastVersion += 1;
                        return lastVersion + ".0";
                    }
                }
            }
        }
    }
}

