﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    
    public class RLCS_ComplianceScheduleManagement
    {
        public static long ExistsComplianceTransaction(ComplianceTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var existComplianceTxnRecord = (from CT in entities.ComplianceTransactions
                                                    where CT.ComplianceInstanceId == transaction.ComplianceInstanceId
                                                    && CT.StatusId == transaction.StatusId
                                                    && CT.CreatedBy == transaction.CreatedBy
                                                    && CT.ComplianceScheduleOnID == transaction.ComplianceScheduleOnID
                                                    select CT).FirstOrDefault();

                    if (existComplianceTxnRecord != null)
                        return existComplianceTxnRecord.ID;
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static long CreateTransaction(ComplianceTransaction transaction)
        {
            long transactionId = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();

                            transactionId = transaction.ID;
                            dbtransaction.Commit();

                            return transactionId;
                        }
                        catch (Exception ex)
                        {
                            dbtransaction.Rollback();
                            ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            return transactionId;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RLCS_LogMessage msg = new RLCS_LogMessage();

                msg.RecordID = transactionId.ToString();
                msg.ClassName = "RLCS_ComplianceScheduleManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;

                ContractManagement.InsertLogMessage(msg);

                List<string> TO = new List<string>();
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                TO.Add("sachin@avantis.info");
                TO.Add("ankit@avantis.info");

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured-RLCS-CreateTransaction Function", "CreateTransaction");

                return transactionId;
            }
        }

        public static void CreateInstances(List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments, long createdByID, string creatdByName, RLCS_RegulatoryLocationWiseMonthlyTracker_Mapping objRecord, int regID)
        {
            long CreateInstancesErrorcomplianceInstanceID = -1;
            long CreateInstancesErrorComplianceId = -1;

            List<long> lstComplianceScheduleOn = new List<long>();

            try
            {
                if (assignments.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int customerBranchID = 0;
                        long complianceInstanceID = 0;
                        long ComplianceID = 0;
                        DateTime ScheduledOn = new DateTime();

                        assignments.ForEach(entry =>
                        {
                            customerBranchID = entry.Item1.CustomerBranchID;
                            ComplianceID = entry.Item1.ComplianceId;
                            ScheduledOn = entry.Item1.ScheduledOn;

                            //Compliance Instance
                            complianceInstanceID = (from row in entities.ComplianceInstances
                                                    where row.ComplianceId == entry.Item1.ComplianceId
                                                    && row.CustomerBranchID == entry.Item1.CustomerBranchID
                                                    && row.IsDeleted == false
                                                    select row.ID).FirstOrDefault();

                            if (complianceInstanceID <= 0)
                            {
                                entry.Item1.CreatedOn = DateTime.UtcNow;
                                entry.Item1.IsDeleted = false;

                                entities.ComplianceInstances.Add(entry.Item1);
                                entities.SaveChanges();

                                complianceInstanceID = entry.Item1.ID;

                                if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                {
                                    CreateInstancesErrorcomplianceInstanceID = complianceInstanceID;
                                    CreateInstancesErrorComplianceId = entry.Item1.ComplianceId;
                                }
                            }
                            else
                            {
                                entry.Item1.ID = complianceInstanceID;
                            }

                            //Compliance Assignment
                            long complianceAssignment = (from row in entities.ComplianceAssignments
                                                         where row.ComplianceInstanceID == entry.Item1.ID
                                                         && row.RoleID == entry.Item2.RoleID
                                                         && row.UserID == entry.Item2.UserID //TBD
                                                         select row.UserID).FirstOrDefault();

                            if (complianceAssignment <= 0)
                            {
                                entry.Item2.ComplianceInstanceID = complianceInstanceID;
                                entities.ComplianceAssignments.Add(entry.Item2);
                                entities.SaveChanges();

                                if (entry.Item2.RoleID != 6)
                                {
                                    if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                    {
                                        //CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID); --TBD
                                    }
                                }
                            }

                            //CreateScheduleOn(entry.Item1.ScheduledOn, complianceInstanceID, entry.Item1.ComplianceId, customerBranchID, createdByID, creatdByName, objRecord, regID); //TBD                                                                      
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                RLCS_LogMessage msg = new RLCS_LogMessage();

                msg.RecordID = CreateInstancesErrorcomplianceInstanceID.ToString();
                msg.ClassName = MethodBase.GetCurrentMethod().DeclaringType.ToString();
                msg.FunctionName = "CreateInstances-ComplianceID-" + CreateInstancesErrorComplianceId + "-ComplianceInstanceID-" + CreateInstancesErrorcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;

                ContractManagement.InsertLogMessage(msg);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                TO.Add("ankit@avantis.info");

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                null, "Error Occured-RLCS-CreateInstances Function", "CreateInstances-ComplianceID-" + CreateInstancesErrorComplianceId + "-ComplianceInstanceID-" + CreateInstancesErrorcomplianceInstanceID);
            }
        }

        public static long Generate_InstanceAssignments(long complianceID, int customerBranchID, int userID, int roleID, DateTime ScheduledOnDate)
        {
            long CreateInstancesErrorcomplianceInstanceID = -1;
            long CreateInstancesErrorComplianceId = -1;
            long complianceInstanceID = 0;

            try
            {
                List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();

                ComplianceInstance instance = new ComplianceInstance()
                {
                    ComplianceId = complianceID,
                    CustomerBranchID = customerBranchID,
                    ScheduledOn = ScheduledOnDate,
                    GenerateSchedule = false
                };

                //Performer
                ComplianceAssignment perfAssignment = new ComplianceAssignment()
                {
                    UserID = userID,
                    RoleID = roleID,
                };

                assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, perfAssignment));

                if (assignments.Count != 0)
                {
                    //RLCS_ComplianceScheduleManagement.CreateInstanceAssignments(assignments, 1, "Admin User");

                    if (assignments.Count > 0)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            assignments.ForEach(entry =>
                            {
                                //Compliance Instance
                                complianceInstanceID = (from row in entities.ComplianceInstances
                                                        where row.ComplianceId == entry.Item1.ComplianceId
                                                        && row.CustomerBranchID == entry.Item1.CustomerBranchID
                                                        && row.IsDeleted == false
                                                        select row.ID).FirstOrDefault();

                                if (complianceInstanceID <= 0)
                                {
                                    entry.Item1.CreatedOn = DateTime.UtcNow;
                                    entry.Item1.IsDeleted = false;

                                    entities.ComplianceInstances.Add(entry.Item1);
                                    entities.SaveChanges();

                                    complianceInstanceID = entry.Item1.ID;



                                    if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                    {
                                        CreateInstancesErrorcomplianceInstanceID = complianceInstanceID;
                                        CreateInstancesErrorComplianceId = entry.Item1.ComplianceId;
                                    }
                                }
                                else
                                {
                                    entry.Item1.ID = complianceInstanceID;
                                }

                                CreateInstancesErrorcomplianceInstanceID = complianceInstanceID;
                                CreateInstancesErrorComplianceId = complianceID;

                                //Compliance Assignment
                                long complianceAssignment = (from row in entities.ComplianceAssignments
                                                             where row.ComplianceInstanceID == entry.Item1.ID
                                                             && row.RoleID == entry.Item2.RoleID
                                                             && row.UserID == entry.Item2.UserID //TBD
                                                             select row.UserID).FirstOrDefault();

                                if (complianceAssignment <= 0)
                                {
                                    entry.Item2.ComplianceInstanceID = complianceInstanceID;
                                    entities.ComplianceAssignments.Add(entry.Item2);
                                    entities.SaveChanges();

                                    if (entry.Item2.RoleID != 6)
                                    {
                                        if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                        {
                                            //CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID); --TBD
                                        }
                                    }
                                }

                                //CreateScheduleOn(entry.Item1.ScheduledOn, complianceInstanceID, entry.Item1.ComplianceId, customerBranchID, createdByID, creatdByName, objRecord, regID); //TBD                                                                      
                            });                         
                        }
                    }
                }

                return complianceInstanceID;
            }
            catch (Exception ex)
            {
                RLCS_LogMessage msg = new RLCS_LogMessage();

                msg.RecordID = CreateInstancesErrorcomplianceInstanceID.ToString();
                msg.ClassName = MethodBase.GetCurrentMethod().DeclaringType.ToString();
                msg.FunctionName = "CreateInstances-ComplianceID-" + CreateInstancesErrorComplianceId + "-ComplianceInstanceID-" + CreateInstancesErrorcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;

                ContractManagement.InsertLogMessage(msg);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                TO.Add("ankit@avantis.info");

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                null, "Error Occured-RLCS-CreateInstances Function", "CreateInstances-ComplianceID-" + CreateInstancesErrorComplianceId + "-ComplianceInstanceID-" + CreateInstancesErrorcomplianceInstanceID);

                return complianceInstanceID;
            }
        }

        //public static long SubmitCompliance(long complianceID, long complianceInstanceID, long complianceScheduleOnID, DateTime complianceScheduleOnDate, List<ComplianceAssignment> complianceAssignments)
        //{
        //    long comTransactionID = 0;
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            if (complianceScheduleOnDate != null)
        //            {
        //                int perfStatusID = 2;
        //                int revwStatusID = 4;

        //                string remarks = string.Empty;

        //                //DateTime closeDate = Convert.ToDateTime(objRecord.LM_CompletionDate);

        //                if (DateTime.Now < complianceScheduleOnDate)
        //                {
        //                    perfStatusID = 3; //Complied Delayed but pending review
        //                    revwStatusID = 5; //Closed-Delayed                            
        //                }
        //                else if (DateTime.Now >= complianceScheduleOnDate)
        //                {
        //                    perfStatusID = 2; //Complied but pending review
        //                    revwStatusID = 4; //Closed-Timely                            
        //                }

        //                //var complianceAssignments = (from row in entities.ComplianceAssignments
        //                //                             where row.ComplianceInstanceID == complianceInstanceID
        //                //                             select row).ToList();

        //                if (complianceAssignments.Count > 0)
        //                {
        //                    //Performer
        //                    var performerRecord = complianceAssignments.Where(row => row.RoleID == 3).FirstOrDefault();

        //                    if (performerRecord != null)
        //                    {
        //                        var UserRecord = RLCS_ComplianceManagement.GetUserByID(Convert.ToInt32(performerRecord.UserID));

        //                        if (UserRecord != null)
        //                        {
        //                            ComplianceTransaction transaction = new ComplianceTransaction()
        //                            {
        //                                ComplianceInstanceId = complianceInstanceID,
        //                                ComplianceScheduleOnID = complianceScheduleOnID,
        //                                CreatedBy = UserRecord.ID,
        //                                CreatedByText = UserRecord.FirstName + " " + UserRecord.LastName,
        //                                StatusId = perfStatusID,
        //                                StatusChangedOn = DateTime.Now,
        //                                Remarks = remarks,
        //                            };

        //                            comTransactionID = RLCS_ComplianceScheduleManagement.ExistsComplianceTransaction(transaction);

        //                            if (comTransactionID == 0)
        //                                comTransactionID = RLCS_ComplianceScheduleManagement.CreateTransaction(transaction);
        //                        }
        //                    }
        //                }
        //            }

        //            return comTransactionID;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        RLCS_LogMessage msg = new RLCS_LogMessage();

        //        msg.RecordID = comTransactionID.ToString();
        //        msg.ClassName = MethodBase.GetCurrentMethod().DeclaringType.ToString();
        //        msg.FunctionName = "CloseCompliance-ComplianceID-" + complianceID + "-ComplianceInstanceID-" + complianceInstanceID + "-ComplianceTxnID-" + comTransactionID;
        //        msg.CreatedOn = DateTime.Now;
        //        msg.Message = ex.Message + "----\r\n" + ex.InnerException;
        //        msg.StackTrace = ex.StackTrace;

        //        ContractManagement.InsertLogMessage(msg);

        //        List<string> TO = new List<string>();
        //        TO.Add("sachin@avantis.info");
        //        TO.Add("narendra@avantis.info");
        //        TO.Add("rahul@avantis.info");
        //        TO.Add("ankit@avantis.info");

        //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
        //        null, "Error Occured-RLCS-CloseCompliance Function", "CloseCompliance-ComplianceID-" + complianceID + "-ComplianceInstanceID-" + complianceInstanceID);

        //        return comTransactionID;
        //    }
        //}

        public static long SubmitOrCloseCompliance(bool closeCompliance, long complianceID, long complianceInstanceID, long complianceScheduleOnID, DateTime complianceScheduleOnDate, List<ComplianceAssignment> complianceAssignments)
        {
            long comTransactionID = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (complianceScheduleOnDate != null)
                    {
                        int perfStatusID = 2;
                        int revwStatusID = 4;

                        string remarks = string.Empty;

                        //DateTime closeDate = Convert.ToDateTime(objRecord.LM_CompletionDate);

                        if (DateTime.Now < complianceScheduleOnDate)
                        {
                            perfStatusID = 3; //Complied Delayed but pending review
                            revwStatusID = 5; //Closed-Delayed                 
                        }
                        else if (DateTime.Now >= complianceScheduleOnDate)
                        {
                            perfStatusID = 2; //Complied but pending review
                            revwStatusID = 4; //Closed-Timely                                                           
                        }

                        if (complianceAssignments.Count > 0)
                        {
                            //Performer
                            var performerRecord = complianceAssignments.Where(row => row.RoleID == 3).FirstOrDefault();

                            if (performerRecord != null)
                            {
                                var UserRecord = RLCS_ComplianceManagement.GetUserByID(Convert.ToInt32(performerRecord.UserID));

                                if (UserRecord != null)
                                {
                                    ComplianceTransaction transaction = new ComplianceTransaction()
                                    {
                                        ComplianceInstanceId = complianceInstanceID,
                                        ComplianceScheduleOnID = complianceScheduleOnID,
                                        CreatedBy = UserRecord.ID,
                                        CreatedByText = UserRecord.FirstName + " " + UserRecord.LastName,
                                        StatusId = perfStatusID,
                                        StatusChangedOn = DateTime.Now,
                                        Remarks = remarks,
                                    };

                                    comTransactionID = RLCS_ComplianceScheduleManagement.ExistsComplianceTransaction(transaction);

                                    if (comTransactionID == 0)
                                        comTransactionID = RLCS_ComplianceScheduleManagement.CreateTransaction(transaction);
                                }
                            }

                            if (closeCompliance)
                            {
                                //Reviewer
                                var reviewerRecord = complianceAssignments.Where(row => row.RoleID == 4).FirstOrDefault();

                                if (reviewerRecord != null)
                                {
                                    var UserRecord = RLCS_ComplianceManagement.GetUserByID(Convert.ToInt32(reviewerRecord.UserID));

                                    if (UserRecord != null)
                                    {
                                        ComplianceTransaction transaction = new ComplianceTransaction()
                                        {
                                            ComplianceInstanceId = complianceInstanceID,
                                            ComplianceScheduleOnID = complianceScheduleOnID,
                                            CreatedBy = UserRecord.ID,
                                            CreatedByText = UserRecord.FirstName + " " + UserRecord.LastName,
                                            StatusId = revwStatusID,
                                            StatusChangedOn = DateTime.Now,
                                            Remarks = remarks,
                                        };

                                        comTransactionID = RLCS_ComplianceScheduleManagement.ExistsComplianceTransaction(transaction);

                                        if (comTransactionID == 0)
                                        {
                                            //System.Threading.Thread.Sleep(1000);
                                            Thread.Sleep((int)TimeSpan.FromSeconds(2).TotalMilliseconds);
                                            comTransactionID = RLCS_ComplianceScheduleManagement.CreateTransaction(transaction);
                                        }
                                    }
                                }
                            }

                        }
                    }

                    return comTransactionID;
                }
            }
            catch (Exception ex)
            {
                RLCS_LogMessage msg = new RLCS_LogMessage();

                msg.RecordID = comTransactionID.ToString();
                msg.ClassName = MethodBase.GetCurrentMethod().DeclaringType.ToString();
                msg.FunctionName = "CloseCompliance-ComplianceID-" + complianceID + "-ComplianceInstanceID-" + complianceInstanceID + "-ComplianceTxnID-" + comTransactionID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;

                ContractManagement.InsertLogMessage(msg);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                TO.Add("ankit@avantis.info");
				TO.Add("rohan@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                null, "Error Occured-RLCS-CloseCompliance Function", "CloseCompliance-ComplianceID-" + complianceID + "-ComplianceInstanceID-" + complianceInstanceID);

                return comTransactionID;
            }
        }

        public static long CloseCompliance(int perfUserID, int revUserID, long complianceID, long complianceInstanceID, long complianceScheduleOnID,
            RLCS_RegulatoryLocationWiseMonthlyTracker_Mapping objRecord)
        {
            long comTransactionID = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (objRecord.LM_CompletionDate != null || objRecord.RM_ClosedDate != null
                        || objRecord.LM_ActivityTo != null || objRecord.RM_ActivityTo != null)
                    {
                        DateTime? closeDate = null;

                        if (objRecord.LM_CompletionDate != null)
                            closeDate = Convert.ToDateTime(objRecord.LM_CompletionDate);
                        else if (objRecord.RM_ClosedDate != null)
                            closeDate = Convert.ToDateTime(objRecord.RM_ClosedDate);
                        else if (objRecord.LM_Status.Trim().Equals("C") && objRecord.LM_CompletionDate == null && objRecord.LM_ActivityTo != null)
                            closeDate = Convert.ToDateTime(objRecord.LM_ActivityTo);
                        else if (objRecord.RM_ActivityStatus.Trim().Equals("C") && objRecord.RM_ClosedDate == null && objRecord.RM_ActivityTo != null)
                            closeDate = Convert.ToDateTime(objRecord.RM_ActivityTo);

                        if (closeDate != null)
                        {
                            int perfStatusID = 2;
                            int revwStatusID = 4;
                            string remarks = string.Empty;

                            if (!string.IsNullOrEmpty(objRecord.LM_DelayReason))
                                remarks = objRecord.LM_DelayReason;
                            else
                                remarks = "NA";

                            //DateTime closeDate = Convert.ToDateTime(objRecord.LM_CompletionDate);

                            if (objRecord.LM_ActivityTo < closeDate) //if (scheduleOnDate <= closeDate)
                            {
                                perfStatusID = 3; //Complied Delayed but pending review
                                revwStatusID = 5; //Closed-Delayed                            
                            }
                            else if (objRecord.LM_ActivityTo >= closeDate)//else if (scheduleOnDate > closeDate)
                            {
                                perfStatusID = 2; //Complied but pending review
                                revwStatusID = 4; //Closed-Timely                            
                            }

                            if (perfUserID != 0 && revUserID != 0)
                            {
                                //Performer TXN
                                if (perfUserID != 0)
                                {
                                    var UserRecord = RLCS_ComplianceManagement.GetUserByID(perfUserID);

                                    if (UserRecord != null)
                                    {
                                        ComplianceTransaction transaction = new ComplianceTransaction()
                                        {
                                            ComplianceInstanceId = complianceInstanceID,
                                            ComplianceScheduleOnID = complianceScheduleOnID,
                                            CreatedBy = UserRecord.ID,
                                            CreatedByText = UserRecord.FirstName + " " + UserRecord.LastName,
                                            StatusId = perfStatusID,
                                            StatusChangedOn = closeDate,
                                            Remarks = remarks,
                                        };

                                        comTransactionID = RLCS_ComplianceScheduleManagement.ExistsComplianceTransaction(transaction);

                                        if (comTransactionID == 0)
                                            comTransactionID = RLCS_ComplianceScheduleManagement.CreateTransaction(transaction);
                                    }
                                }

                                //Reviewer TXN
                                if (revUserID != 0)
                                {
                                    var UserRecord = RLCS_ComplianceManagement.GetUserByID(revUserID);

                                    if (UserRecord != null)
                                    {
                                        ComplianceTransaction transaction = new ComplianceTransaction()
                                        {
                                            ComplianceInstanceId = complianceInstanceID,
                                            ComplianceScheduleOnID = complianceScheduleOnID,
                                            CreatedBy = UserRecord.ID,
                                            CreatedByText = UserRecord.FirstName + " " + UserRecord.LastName,
                                            StatusId = revwStatusID,
                                            StatusChangedOn = closeDate,
                                            Remarks = remarks,
                                        };

                                        comTransactionID = RLCS_ComplianceScheduleManagement.ExistsComplianceTransaction(transaction);

                                        if (comTransactionID == 0)
                                        {
                                            //System.Threading.Thread.Sleep(1000);
                                            Thread.Sleep((int)TimeSpan.FromSeconds(2).TotalMilliseconds);
                                            comTransactionID = RLCS_ComplianceScheduleManagement.CreateTransaction(transaction);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    return comTransactionID;
                }
            }
            catch (Exception ex)
            {
                RLCS_LogMessage msg = new RLCS_LogMessage();

                msg.RecordID = comTransactionID.ToString();
                msg.ClassName = MethodBase.GetCurrentMethod().DeclaringType.ToString();
                msg.FunctionName = "CloseCompliance-ComplianceID-" + complianceID + "-ComplianceInstanceID-" + complianceInstanceID + "-ComplianceTxnID-" + comTransactionID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;

                ContractManagement.InsertLogMessage(msg);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                TO.Add("ankit@avantis.info");

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                null, "Error Occured-RLCS-CloseCompliance Function", "CloseCompliance-ComplianceID-" + complianceID + "-ComplianceInstanceID-" + complianceInstanceID);

                return comTransactionID;
            }
        }

        public static bool DeActiveComplianceSchedule(long complianceInstanceID, long complianceScheduleOnID, string rlcsPayrollMonth, string rlcsPayrollYear)
        {
            bool deActiveSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevComplianceScheduleOnRecord = (from row in entities.ComplianceScheduleOns
                                                          where row.ID == complianceScheduleOnID
                                                          && row.ComplianceInstanceID == complianceInstanceID
                                                          && row.RLCS_PayrollMonth.Trim().ToUpper().Equals(rlcsPayrollMonth)
                                                          && row.RLCS_PayrollYear.Trim().ToUpper().Equals(rlcsPayrollYear)
                                                          // && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                          select row).FirstOrDefault();

                    if (prevComplianceScheduleOnRecord != null)
                    {
                        prevComplianceScheduleOnRecord.IsActive = false;
                        prevComplianceScheduleOnRecord.IsUpcomingNotDeleted = false;
                        entities.SaveChanges();

                        deActiveSuccess = true;
                    }

                    return deActiveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deActiveSuccess;
            }
        }

        public static long CreateScheduleOn(DateTime scheduledOn, long complianceInstanceID, long complianceID, long createdByID, string creatdByName, string rlcsPayrollMonth, string rlcsPayrollYear, DateTime? rlcsActivityEndDate)
        {
            long CreateScheduleOnErroComplianceid = -1;
            long CreateScheduleOnErrocomplianceInstanceId = -1;

            long ComplianceScheduleOnID = 0;

            int monthNumber = Convert.ToInt32(rlcsPayrollMonth.Trim().ToUpper());

            CreateScheduleOnErroComplianceid = complianceID;
            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceID;

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    DateTime nextDate = scheduledOn;

                    var complianceRecord = (from row in entities.Compliances
                                            where row.ID == complianceID
                                            select row).FirstOrDefault();

                    var complianceSchedule = (from row in entities.ComplianceSchedules
                                              where row.ComplianceID == complianceID
                                              && row.ForMonth == monthNumber
                                              select row).FirstOrDefault();

                    if (complianceRecord != null && complianceSchedule != null)
                    {
                        string forMonth = string.Empty;

                        //DateTime specialDate = new DateTime(Convert.ToInt32(rlcsPayrollYear) + 1, Convert.ToInt32(complianceSchedule.SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule.SpecialDate.Substring(0, 2)));
                        DateTime specialDate = new DateTime();

                        if (rlcsPayrollMonth == "12")
                            specialDate = new DateTime(Convert.ToInt32(rlcsPayrollYear) + 1, Convert.ToInt32(complianceSchedule.SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule.SpecialDate.Substring(0, 2)));
                        else
                            specialDate = new DateTime(Convert.ToInt32(rlcsPayrollYear), Convert.ToInt32(complianceSchedule.SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule.SpecialDate.Substring(0, 2)));

                        forMonth = GetForMonth(specialDate, Convert.ToInt32(rlcsPayrollMonth), complianceRecord.Frequency);
                        nextDate = specialDate;

                        //complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();

                        //Tuple<DateTime, string, long> nextDateMonth;
                        //nextDateMonth = RLCS_ComplianceScheduleManagement.GetNextDate(nextDate, complianceID, complianceInstanceID, complianceRecord, Convert.ToInt32(rlcsPayrollMonth));

                        ComplianceScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                  where row.ComplianceInstanceID == complianceInstanceID
                                                  && row.ScheduleOn == specialDate
                                                  //&& row.ScheduleOn == nextDateMonth.Item1 --Commented
                                                  && row.ForPeriod == monthNumber
                                                  //&& row.ForPeriod == nextDateMonth.Item3 --Commented
                                                  && row.RLCS_PayrollMonth.Trim().ToUpper().Equals(rlcsPayrollMonth)
                                                  && row.RLCS_PayrollYear.Trim().ToUpper().Equals(rlcsPayrollYear)
                                                  // && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                  select row.ID).SingleOrDefault();

                        if (ComplianceScheduleOnID <= 0)
                        {
                            ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                            complianceScheduleon.ComplianceInstanceID = complianceInstanceID;
                            complianceScheduleon.ScheduleOn = specialDate.Date;
                            //complianceScheduleon.ScheduleOn = nextDateMonth.Item1;
                            complianceScheduleon.ForMonth = forMonth;
                            complianceScheduleon.ForPeriod = monthNumber;
                            complianceScheduleon.IsActive = true;
                            complianceScheduleon.IsUpcomingNotDeleted = true;
                            complianceScheduleon.RLCS_PayrollMonth = rlcsPayrollMonth.Trim().ToUpper();
                            complianceScheduleon.RLCS_PayrollYear = rlcsPayrollYear.Trim().ToUpper();

                            if (rlcsActivityEndDate != null)
                                complianceScheduleon.RLCS_ActivityEndDate = rlcsActivityEndDate.Value.Date;

                            entities.ComplianceScheduleOns.Add(complianceScheduleon);
                            entities.SaveChanges();

                            ComplianceTransaction transaction = new ComplianceTransaction()
                            {
                                ComplianceInstanceId = complianceInstanceID,
                                ComplianceScheduleOnID = complianceScheduleon.ID,
                                CreatedBy = createdByID,
                                CreatedByText = creatdByName,
                                StatusId = 1,
                                Remarks = "New Compliance Assigned."
                            };

                            CreateTransaction(transaction);
                            ComplianceScheduleOnID = complianceScheduleon.ID;
                        }
                    }

                    return ComplianceScheduleOnID;
                }
            }
            catch (Exception ex)
            {
                RLCS_LogMessage msg = new RLCS_LogMessage();

                msg.RecordID = ComplianceScheduleOnID.ToString();
                msg.ClassName = "RLCS_ComplianceScheduleManagement";
                msg.FunctionName = "CreateScheduleOn" + "ComplianceID=" + CreateScheduleOnErroComplianceid + "-ComplianceInstanceID=" + CreateScheduleOnErrocomplianceInstanceId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;

                ContractManagement.InsertLogMessage(msg);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                TO.Add("ankit@avantis.info");

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                    "Error Occured as RLCS-CreateScheduleOn", "CreateScheduleOn" + "ComplianceID=" + CreateScheduleOnErroComplianceid + "-ComplianceInstanceID=" + CreateScheduleOnErrocomplianceInstanceId);

                return ComplianceScheduleOnID;
            }
        }

        public static string GetForMonth(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:
                    string year = date.ToString("yy");
                    if (date > DateTime.Today.Date)
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            if (forMonth == 11)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else if (forMonth == 12)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else
                            {
                                year = (date).ToString("yy");
                            }
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    else
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else
                        {
                            yearq = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    if (forMonth == 7)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + date.ToString("yy");
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }

                    break;

                case 3:

                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }
                    else if (date.Month >= 3)
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                    }

                    if (forMonth == 1)
                    {
                        forMonthName = "CY - " + endFinancial1Year;
                    }
                    else
                    {
                        forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    }
                    break;

                case 4:

                    string year2 = date.ToString("yy");
                    if (forMonth == 9)
                    {
                        if (Convert.ToInt32(year2) >= Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year2 = (date.AddYears(-1)).ToString("yy");
                        }
                        else
                        {
                            year2 = (date).ToString("yy");
                        }

                    }
                    if (forMonth == 12)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
                                       " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
                    }
                    break;
                case 5:

                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    else if (forMonth == 4)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                        forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-8)).ToString("yy");
                        endFinancial7Year = (date.AddYears(-1)).ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }

        public static long GetNextMonthFromFrequency(long LastforMonth, byte? Frequency)
        {
            long NextPeriod;
            switch (Frequency)
            {
                case 0:
                    if (LastforMonth == 12)
                        NextPeriod = 1;
                    else
                        NextPeriod = LastforMonth + 1;
                    break;

                case 1:
                    if (LastforMonth == 10)
                        NextPeriod = 1;
                    else
                        NextPeriod = LastforMonth + 3;
                    break;

                case 2:
                    if (LastforMonth == 7)
                        NextPeriod = 1;
                    else if (LastforMonth == 10)
                        NextPeriod = 4;
                    else
                        NextPeriod = LastforMonth + 6;
                    break;

                case 3:
                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                case 4:

                    if (LastforMonth == 9)
                        NextPeriod = 1;
                    else if (LastforMonth == 12)
                        NextPeriod = 4;
                    else
                        NextPeriod = LastforMonth + 4;

                    break;
                case 5:

                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                case 6:

                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                default:
                    NextPeriod = 0;
                    break;
            }

            return NextPeriod;
        }

        public static long GetLastPeriodFromDate(int forMonth, byte? Frequency)
        {
            long forMonthName = 0;
            switch (Frequency)
            {
                case 0:
                    if (forMonth == 1)
                    {
                        forMonthName = 12;
                    }
                    else
                    {
                        forMonthName = forMonth - 1;
                    }
                    break;
                case 1:
                    if (forMonth == 1)
                    {
                        forMonthName = 10;
                    }
                    else
                    {
                        forMonthName = forMonth - 3;
                    }
                    break;
                case 2:
                    if (forMonth == 1)
                    {
                        forMonthName = 7;
                    }
                    else
                    {
                        forMonthName = forMonth - 6;
                    }
                    break;

                case 3:
                    forMonthName = 1;
                    break;

                case 4:

                    if (forMonth == 1)
                    {
                        forMonthName = 9;
                    }
                    else
                    {
                        forMonthName = forMonthName - 4;
                    }

                    break;
                case 5:
                    forMonthName = 1;
                    break;
                case 6:

                    forMonthName = 1;
                    break;

                default:
                    forMonthName = 0;
                    break;
            }

            return forMonthName;
        }

        public static Compliance GetByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.Compliances.Include("ComplianceParameters")
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }

        public static List<ComplianceSchedule> GetScheduleByComplianceID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceSchedules
                                    where row.ComplianceID == complianceID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }

        public static List<ComplianceSchedule> GetScheduleByComplianceID(long complianceID, int forMonth)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceSchedules
                                    where row.ComplianceID == complianceID
                                    && row.ForMonth == forMonth
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }

        public static long GetScheduledOnPresentOrNot(long ComplianceInstanceID, DateTime ScheduledOn)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                     where row.ComplianceInstanceID == ComplianceInstanceID && row.ScheduleOn == ScheduledOn
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row.ID).FirstOrDefault();
                return ScheduleOnID;
            }
        }

        public static Tuple<DateTime, string, long> GetNextDatePeriodically(DateTime scheduledOn, long complianceID, long ComplianceInstanceID)
        {
            int formonthscheduleR = -1;
            var complianceSchedule = GetScheduleByComplianceID(complianceID);//.OrderBy(entry => entry.ForMonth).ToList();
            var objCompliance = GetByID(complianceID);
            DateTime date = complianceSchedule.Select(row => new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2)))).Where(row => row > scheduledOn).FirstOrDefault();
            var lastSchedule = GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            if (objCompliance.Frequency == 3 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (objCompliance.Frequency == 5 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (objCompliance.Frequency == 6 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 6, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (objCompliance.Frequency == 2)
            {
                List<Tuple<DateTime, long>> complianceScheduleDates;
                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "07").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "07").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" || row.SpecialDate.Substring(2, 2) == "10").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "10").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "12" || row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                long ActualForMonth = 0;
                if (objCompliance.Frequency == 2)
                {

                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {

                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 > Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderBy(x => x.Item1).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.Frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.Frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }
            string forMonth = string.Empty;

            if (complianceSchedule.Count > 0)
            {
                string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

                var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                forMonth = GetForMonthPeriodically(date, ForMonthSchedule.ForMonth, objCompliance.Frequency);
                formonthscheduleR = ForMonthSchedule.ForMonth;
            }


            return new Tuple<DateTime, string, long>(date, forMonth, formonthscheduleR);
        }

        public static string GetForMonthPeriodically(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:
                    string year = date.ToString("yy");

                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 10;
                        yearq = (date.AddYears(-1)).ToString("yy");
                    }


                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 2)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");

                    if (forMonth == 4)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 10)
                    {

                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (Convert.ToInt32(year1) - 1) +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 7)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 1)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 7)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (Convert.ToInt32(year1) - 1) +
                                " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 6)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 5)).Substring(0, 3) + " " + year1 +
                                " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 12)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 5)).Substring(0, 3) + " " + (Convert.ToInt32(year1)) +
                                " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1;
                    }
                    break;

                case 3:

                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial1Year = date.ToString("yy");
                        endFinancial1Year = (date.AddYears(1)).ToString("yy"); date.ToString("yy");
                    }
                    else
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    break;

                case 4:
                    string year2 = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 9;
                        year2 = (date.AddYears(-1)).ToString("yy");
                    }


                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 3)).Substring(0, 3) + " " + year2 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2;
                    break;
                case 5:

                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial2Year = (date.AddYears(1)).ToString("yy");
                    }
                    else
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;

                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-6)).ToString("yy");
                        endFinancial7Year = (date.AddYears(1)).ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }

        public static Tuple<DateTime, string, long> GetNextDate(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, Compliance objCompliance)
        {
            var complianceSchedule = GetScheduleByComplianceID(complianceID);
            var lastSchedule = GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(lastSchedule.ScheduleOn.Month), objCompliance.Frequency);
                }
            }
            else
            {
                #region Monthly
                if (objCompliance.Frequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (objCompliance.Frequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (objCompliance.Frequency == 3)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Quarterly
                else if (objCompliance.Frequency == 1)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (objCompliance.Frequency == 4)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (objCompliance.Frequency == 5)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            if (objCompliance.Frequency != 0 || objCompliance.Frequency != 1)
            {
                #region  First Part
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                else
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                #endregion

                #region Second Part
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.Frequency == 1)
                        {
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            if (objCompliance.Frequency == 0)
                            {

                                if (lastPeriod == 11)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                if (lastPeriod == 7)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.SpecialDate.Substring(0, 2) == "10" && row.ForMonth == 8).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                            }
                            if (objCompliance.Frequency == 2)
                            {
                                if (lastPeriod == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else if (lastPeriod != 10)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "07" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        if (scheduledOn.Year <= DateTime.Today.Year - 1)
                                        {
                                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                        }
                                    }
                                }
                            }
                            if (objCompliance.Frequency == 4)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 9).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" || row.SpecialDate.Substring(2, 2) == "07" || row.SpecialDate.Substring(2, 2) == "08" || row.SpecialDate.Substring(2, 2) == "09" || row.SpecialDate.Substring(2, 2) == "10" || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02" || entry.SpecialDate.Substring(2, 2) == "07" || entry.SpecialDate.Substring(2, 2) == "08" || entry.SpecialDate.Substring(2, 2) == "09" || entry.SpecialDate.Substring(2, 2) == "10" || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    if (objCompliance.Frequency == 2)
                    {
                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                    else
                    {
                        if (objCompliance.Frequency == 0)
                        {
                            if (lastPeriod == 11)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else if (objCompliance.Frequency == 1)
                        {
                            if (objCompliance.Frequency == 1)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && objCompliance.Frequency == 3).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            else
            {
                #region Third Part
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (objCompliance.Frequency == 0)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 1)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 2)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 3)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 4)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 5)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                if (objCompliance.Frequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 0)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduleOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.Frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.Frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.Frequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }

        public static Tuple<DateTime, string, long> GetNextDate(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, Compliance objCompliance, int scheduleMonth)
        {
            var complianceSchedule = GetScheduleByComplianceID(complianceID, scheduleMonth);
            var lastSchedule = GetLastScheduleOnByInstance(ComplianceInstanceID);
            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(lastSchedule.ScheduleOn.Month), objCompliance.Frequency);
                }
            }
            else
            {
                #region Monthly
                if (objCompliance.Frequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (objCompliance.Frequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(scheduledOn).Month >= 1 && Convert.ToDateTime(scheduledOn).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(scheduledOn).Month >= 7 && Convert.ToDateTime(scheduledOn).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(scheduledOn).Month >= 1 && Convert.ToDateTime(scheduledOn).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(scheduledOn).Month >= 7 && Convert.ToDateTime(scheduledOn).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(scheduledOn).Month >= 4 && Convert.ToDateTime(scheduledOn).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(scheduledOn).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(scheduledOn).Month >= 4 && Convert.ToDateTime(scheduledOn).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(scheduledOn).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (objCompliance.Frequency == 3)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Quarterly
                else if (objCompliance.Frequency == 1)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(scheduledOn).Month >= 1 && Convert.ToDateTime(scheduledOn).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(scheduledOn).Month >= 11 && Convert.ToDateTime(scheduledOn).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(scheduledOn).Month >= 8 && Convert.ToDateTime(scheduledOn).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(scheduledOn).Month >= 1 && Convert.ToDateTime(scheduledOn).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(scheduledOn).Month >= 11 && Convert.ToDateTime(scheduledOn).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(scheduledOn).Month >= 8 && Convert.ToDateTime(scheduledOn).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (objCompliance.Frequency == 4)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            if (Convert.ToDateTime(scheduledOn).Month >= 1 && Convert.ToDateTime(scheduledOn).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(scheduledOn).Month >= 6 && Convert.ToDateTime(scheduledOn).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(scheduledOn).Month >= 10 && Convert.ToDateTime(scheduledOn).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(scheduledOn).Month >= 1 && Convert.ToDateTime(scheduledOn).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(scheduledOn).Month >= 6 && Convert.ToDateTime(scheduledOn).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(scheduledOn).Month >= 10 && Convert.ToDateTime(scheduledOn).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (objCompliance.Frequency == 5)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            if (objCompliance.Frequency != 0 || objCompliance.Frequency != 1)
            {
                #region  First Part
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                else
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                #endregion

                #region Second Part
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.Frequency == 1)
                        {
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            if (objCompliance.Frequency == 0)
                            {

                                if (lastPeriod == 11)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                if (lastPeriod == 7)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.SpecialDate.Substring(0, 2) == "10" && row.ForMonth == 8).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                            }
                            if (objCompliance.Frequency == 2)
                            {
                                if (lastPeriod == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else if (lastPeriod != 10)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "07" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        if (scheduledOn.Year <= DateTime.Today.Year - 1)
                                        {
                                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                        }
                                    }
                                }
                            }
                            if (objCompliance.Frequency == 4)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 9).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" || row.SpecialDate.Substring(2, 2) == "07" || row.SpecialDate.Substring(2, 2) == "08" || row.SpecialDate.Substring(2, 2) == "09" || row.SpecialDate.Substring(2, 2) == "10" || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02" || entry.SpecialDate.Substring(2, 2) == "07" || entry.SpecialDate.Substring(2, 2) == "08" || entry.SpecialDate.Substring(2, 2) == "09" || entry.SpecialDate.Substring(2, 2) == "10" || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    if (objCompliance.Frequency == 2)
                    {
                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                    else
                    {
                        if (objCompliance.Frequency == 0)
                        {
                            if (lastPeriod == 11)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else if (objCompliance.Frequency == 1)
                        {
                            if (objCompliance.Frequency == 1)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && objCompliance.Frequency == 3).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            else
            {
                #region Third Part
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (objCompliance.Frequency == 0)
            {
                if (lastSchedule == null)
                {
                    if (scheduledOn != null)
                    {
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 1)
            {
                if (lastSchedule == null)
                {
                    if (scheduledOn != null)
                    {
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 2)
            {
                if (lastSchedule == null)
                {
                    if (scheduledOn != null)
                    {
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 3)
            {
                if (lastSchedule == null)
                {
                    if (scheduledOn != null)
                    {
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 4)
            {
                if (lastSchedule == null)
                {
                    if (scheduledOn != null)
                    {
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 5)
            {
                if (lastSchedule == null)
                {
                    if (scheduledOn != null)
                    {
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                if (objCompliance.Frequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (scheduledOn != null)
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (scheduledOn != null)
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (scheduledOn != null)
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (scheduledOn != null)
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (scheduledOn != null)
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 0)
                {
                    if (lastSchedule == null)
                    {
                        if (scheduledOn != null)
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduleOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.Frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.Frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.Frequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }

        public static List<ComplianceAssignment> GetAssignedUsers(int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedUsers = (from row in entities.ComplianceAssignments
                                     where row.ComplianceInstanceID == ComplianceInstanceID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

                return AssignedUsers;
            }
        }

        public static ComplianceScheduleOn GetLastScheduleOnByInstance(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleObj = (from row in entities.ComplianceScheduleOns
                                   where row.ComplianceInstanceID == complianceInstanceID
                                   && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                   orderby row.ScheduleOn descending, row.ID descending
                                   select row).FirstOrDefault();

                return scheduleObj;
            }
        }

        public static Tuple<DateTime, string, long> GetNextDate1(DateTime scheduledOn, long complianceID, long ComplianceInstanceID)
        {
            return new Tuple<DateTime, string, long>(scheduledOn, Convert.ToString(complianceID), ComplianceInstanceID);
        }
        
        public static void CreateReminders(long complianceID, long ComplianceInstanceId, int ComplianceAssignmentId, DateTime scheduledOn)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    Compliance compliance = (from row in entities.Compliances
                                             where row.ID == complianceID
                                             select row).FirstOrDefault();

                    if (compliance.Frequency == 7)
                    {
                        #region Frequency Daily
                        ComplianceReminder reminder = new ComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date,
                        };
                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.ComplianceReminders.Add(reminder);
                        #endregion
                    }
                    else if (compliance.Frequency == 8)
                    {
                        #region Frequency Weekly
                        ComplianceReminder reminder = new ComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date,
                        };
                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.ComplianceReminders.Add(reminder);
                        ComplianceReminder reminder1 = new ComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                        };
                        reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.ComplianceReminders.Add(reminder1);
                        #endregion
                    }
                    else
                    {
                        #region Normal Frequency
                        if (compliance.SubComplianceType == 0)
                        {
                            #region SubComplianceType == 0
                            if (compliance.ReminderType == 0)
                            {
                                #region SubComplianceType=0 && ReminderType=0                         
                                if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                {
                                    ComplianceReminder reminder = new ComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date,
                                    };
                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.ComplianceReminders.Add(reminder);
                                }//added by rahul on 4 FEB 2016
                                else
                                {
                                    ComplianceReminder reminder = new ComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                                    };
                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.ComplianceReminders.Add(reminder);


                                    ComplianceReminder reminder1 = new ComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                                    };
                                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.ComplianceReminders.Add(reminder1);
                                }
                                #endregion
                            }
                            else
                            {
                                #region 
                                if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                {
                                    ComplianceReminder reminder = new ComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date,
                                    };
                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.ComplianceReminders.Add(reminder);
                                }//added by rahul on 4 FEB 2016
                                else
                                {
                                    DateTime TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.ReminderBefore)));
                                    while (TempScheduled.Date < scheduledOn)
                                    {
                                        ComplianceReminder reminder = new ComplianceReminder()
                                        {
                                            ComplianceAssignmentID = ComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = TempScheduled.Date,
                                        };

                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.ComplianceReminders.Add(reminder);
                                        TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.ReminderGap));
                                    }//added by rahul on 8 FEB 2016
                                }
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {

                            if (compliance.ReminderType == 0)
                            {
                                if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 0)
                                {

                                    ComplianceReminder reminder = new ComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date,
                                    };
                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.ComplianceReminders.Add(reminder);
                                }
                                else if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 1)
                                {

                                    ComplianceReminder reminder = new ComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date,
                                    };
                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.ComplianceReminders.Add(reminder);

                                }
                                else if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                {

                                    ComplianceReminder reminder = new ComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date,
                                    };
                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.ComplianceReminders.Add(reminder);

                                }
                                else
                                {
                                    if (compliance.Frequency.HasValue)
                                    {
                                        var reminders = (from row in entities.ReminderTemplates
                                                         where row.Frequency == compliance.Frequency.Value && row.IsSubscribed == true
                                                         select row).ToList();

                                        reminders.ForEach(day =>
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = day.ID,
                                                ComplianceDueDate = scheduledOn,
                                                RemindOn = scheduledOn.Date.AddDays(-1 * day.TimeInDays),
                                            };

                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                            entities.ComplianceReminders.Add(reminder);

                                        });
                                    }
                                    else//added by rahul on 16 FEB 2016 for Event
                                    {
                                        if (compliance.ComplianceType == 0 && compliance.EventID != null)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = scheduledOn,
                                                RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);


                                            ComplianceReminder reminder1 = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = scheduledOn,
                                                RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                                            };
                                            reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder1);
                                        }
                                    }//added by rahul on 16 FEB 2016 for Event
                                }
                            }
                            else
                            {
                                DateTime TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.ReminderBefore)));
                                while (TempScheduled.Date < scheduledOn)
                                {
                                    ComplianceReminder reminder = new ComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = TempScheduled.Date,
                                    };

                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                    entities.ComplianceReminders.Add(reminder);

                                    TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.ReminderGap));
                                }
                            }
                        }
                        #endregion
                    }
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                RLCS_LogMessage msg = new RLCS_LogMessage();

                msg.RecordID = ComplianceAssignmentId.ToString();
                msg.ClassName = "RLCS_ComplianceScheduleManagement";
                msg.FunctionName = "CreateReminders-" + "ComplianceAssignmentID=" + ComplianceAssignmentId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;

                ContractManagement.InsertLogMessage(msg);

                List<string> TO = new List<string>();
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                TO.Add("sachin@avantis.info");
                TO.Add("ankit@avantis.info");

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured-RLCS-CreateReminders Function", "CreateReminders-ComplianceAssignmentID=" + ComplianceAssignmentId);
            }
        }

        public static bool IsComplianceAssignmentAvailable(long complianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.ComplianceAssignments
                                      where row.ComplianceInstanceID == complianceInstanceId
                                      select row).ToList();

                if (ComplianceData.Count > 0)
                    return true;
                else
                    return false;
            }
        }

        public static long GetScheduledOnPresentOrNot(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                     where row.ComplianceInstanceID == ComplianceInstanceID
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row.ID).FirstOrDefault();
                return ScheduleOnID;
            }
        }

        public static DateTime? GetComplianceInstanceScheduledon(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime ScheduleOnID = (from row in entities.ComplianceInstances
                                         where row.ID == ComplianceInstanceID
                                         select row.ScheduledOn).FirstOrDefault();
                if (ScheduleOnID != null)
                {
                    return ScheduleOnID;
                }
                else
                {
                    return null;
                }
            }
        }
             
        public static bool ComplianceIsEventBased(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.Compliances
                                      where row.ID == complianceID
                                      select row).FirstOrDefault();

                if (ComplianceData.EventFlag == null)
                    return true;
                else
                    return false;
            }
        }
    }
}
