﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Sockets;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCSAPIClasses
    {

        public partial class MappedCompliance
        {
            public long rowID { get; set; }
            public Nullable<long> AVACOM_ComplianceID { get; set; }
            public string ActCode { get; set; }
        }
        public static bool TestWebAPIConnection(string requestURi)
        {
            try
            {
                System.Net.WebClient client = new System.Net.WebClient();
                string result = client.DownloadString(requestURi);

                return true;
            }
            catch (System.Net.WebException ex)
            {
                //do something here to make the site unusable, e.g:
                return false;              
            }
        }

        public static string Invoke(string Method, string Uri, string Body)
        {
            try
            {
                HttpClient cl = new HttpClient();
                cl.BaseAddress = new Uri(Uri);
                int _TimeoutSec = 90;
                cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
                string _ContentType = "application/json";
                cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
                var authValue = "5E8E3D45-172D-4D58-8DF8-EB0B0E";
                cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));
                var _UserAgent = "f2UKmAm0KlI98bEYGGxEHQ==";
                // You can actually also set the User-Agent via a built-in property
                cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);
                // You get the following exception when trying to set the "Content-Type" header like this:
                // cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
                // "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

                HttpResponseMessage response;
                var _Method = new HttpMethod(Method);

                switch (_Method.ToString().ToUpper())
                {
                    case "GET":
                    case "HEAD":
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.GetAsync(Uri).Result;
                        break;
                    case "POST":
                        {
                            // Construct an HttpContent from a StringContent
                            HttpContent _Body = new StringContent(Body);
                            // and add the header to this object instance
                            // optional: add a formatter option to it as well
                            _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                            // synchronous request without the need for .ContinueWith() or await
                            response = cl.PostAsync(Uri, _Body).Result;
                        }
                        break;
                    case "PUT":
                        {
                            // Construct an HttpContent from a StringContent
                            HttpContent _Body = new StringContent(Body);
                            // and add the header to this object instance
                            // optional: add a formatter option to it as well
                            _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                            // synchronous request without the need for .ContinueWith() or await
                            response = cl.PutAsync(Uri, _Body).Result;
                        }
                        break;
                    case "DELETE":
                        response = cl.DeleteAsync(Uri).Result;
                        break;
                    default:
                        throw new NotImplementedException();
                        break;
                }
                // either this - or check the status to retrieve more information
                string responseContent = string.Empty;

                if (response.IsSuccessStatusCode)
                    // get the rest/content of the response in a synchronous way
                    responseContent = response.Content.ReadAsStringAsync().Result;

                return responseContent;
            }
            catch(Exception ex)
            {
                return string.Empty;
            }
        }

        public static string InvokeWithAuthKey(string Method, string Uri, string authKey, string encryptedProfileID, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            //int _TimeoutSec = 90;
            int _TimeoutSec = 60;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));

            var authValue = authKey;
            cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));

            var _UserAgent = encryptedProfileID;
            // You can actually also set the User-Agent via a built-in property
            cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);

            // You get the following exception when trying to set the "Content-Type" header like this:
            // cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
            // "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }

        public static string Invoke_Live(string Method, string Uri, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            var authValue = "1C552685-692A-4792-AC9D-CFE22F";
            cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));
            var _UserAgent = "E43SC3fopmDfizK5kv2chQ==";
            // You can actually also set the User-Agent via a built-in property
            cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);
            // You get the following exception when trying to set the "Content-Type" header like this:
            // cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
            // "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }

        private static bool IsNetworkError(Exception ex)
        {
            // Check if it's a network error
            if (ex is SocketException)
                return true;
            if (ex.InnerException != null)
                return IsNetworkError(ex.InnerException);
            return false;
        }
    }

    public class API_AuthKey_Response
    {
        public string ProfileId { get; set; }
        public string AuthKey { get; set; }
    }

    public class DashboardCount
    {
        public string OverDueCount { get; set; }
        public string UpcomingCount { get; set; }
        public string DueTodayCount { get; set; }
        public string HighRiskCount { get; set; }
    }

    public class MonthlyComplianceColumnChart
    {
        public List<string> labels { get; set; }
        public List<List<double>> Compliance { get; set; }
        public object NotCompliance { get; set; }
    }

    public class MonthlyCompliancePieChart
    {
        public List<string> labels { get; set; }
        [JsonProperty("Compliance")]
        public List<double> ComplianceStatus { get; set; }
        public string FooterLabel { get; set; }
    }

    public class ClientInfo
    {
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        public string Branches { get; set; }
        public string Employees { get; set; }
        public string Contractors { get; set; }
        public string Joinees { get; set; }
        public string Resignees { get; set; }
    }

    public class Profile_BasicInfo
    {
        public string ClientCount { get; set; }
        public string ProfileName { get; set; }
        public string Designation { get; set; }
        public List<ClientInfo> lstBasicInfrmation { get; set; }
    }

    public class PanIndiaState
    {
        [JsonProperty("PanIndiaStateId")]
        public string id { get; set; }
        [JsonProperty("PanIndiaStateName")]
        public string StateName { get; set; }
    }

    public class ActDetail
    {
        [JsonProperty("ActNameDetail")]
        public string ActName { get; set; }
        [JsonProperty("ActGroupDetail")]
        public string ActGroup { get; set; }
    }

    public class InvoiceDetail
    {
        public string Current_Month_Invoice { get; set; }
        public string Total_Invoice { get; set; }
        public string OutStanding_Invoice { get; set; }
    }

    public class ActivityDetails
    {
        public string ActivityName { get; set; }
        public string AssignedTo { get; set; }
        public string Location { get; set; }
        public string Branch { get; set; }
        public string DueDate { get; set; }
    }

    public class RegisterReturnChallanDetail
    {
        public string ClientName { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string State { get; set; }
        public string Location { get; set; }
        public string Branch { get; set; }
        public string button { get; set; }

        [JsonProperty("Download")]
        public string Download { get; set; }
        [JsonProperty("DownloadE")]
        public string DownloadESI { get; set; }
        public string button1 { get; set; }
        public string EstablishmentType { get; set; }
        public string DownloadAllPath { get; set; }
    }

    public class LocationWiseComplianceStatus
    {
        public object RowNumber { get; set; }
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        public string StateName { get; set; }
        public string LocationName { get; set; }
        public string BranchName { get; set; }
        public string Compliance { get; set; }
        public string NotCompliance { get; set; }
    }

    public class DetailedComplianceStatus
    {
        [JsonProperty("Map")]
        public List<LocationWiseComplianceStatus> Map { get; set; }
        public string Status { get; set; }
        public int StatusCode { get; set; }
        public object error { get; set; }
    }

    public class Model_CorporateClientMaster
    {
        public string CorporateId { get; set; }
        public string CorporateName { get; set; }        
        public string Status { get; set; }
        public string CO_CreatedBy { get; set; }
        public Nullable<DateTime> CO_CreatedDate { get; set; }
        public string CO_ModifiedBy { get; set; }
        public Nullable<DateTime> CO_ModifiedDate { get; set; }
        public int? CorporateFlag { get; set; }
    }

    public class RLCS_API_Response
    {
        public string Status { get; set; }
        public int StatusCode { get; set; }
    }

    public class RLCS_Request_DownloadRegister
    {
        public string CM_ClientID { get; set; }
        public string StateID { get; set; }
        public string LocationID { get; set; }
        public string BranchID { get; set; }
        public string MonthID { get; set; }
        public string YearID { get; set; }
        public string RegisterID { get; set; }
        public string DownloadFormatType { get; set; }
        public string status { get; set; }
        public int IsAventisAPICall { get; set; }
    }

    public class RLCS_Response_DownloadRegister
    {
        public string Status { get; set; }
        public int StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public List<string> DocumentPath { get; set; }
        public List<string> RegistersId { get; set; }
    }

    public class RLCS_Request_DownloadChallan
    {
        public string ClientID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string ChallanType { get; set; }
        public string Code { get; set; }
        public int IsAventisAPICall { get; set; }
    }

    public class RLCS_Response_DownloadChallan
    {
        public string Status { get; set; }
        public int StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public List<string> DocumentPath { get; set; }
    }
    public class Client
    {
        public string CM_ClientID { get; set; }
        public string CO_CorporateID { get; set; }
        public string CM_ClientName { get; set; }
        public string CM_AgreementID { get; set; }
        public string CM_ContractType { get; set; }
        public string CM_EstablishmentType { get; set; }
        public string CM_DocumentType { get; set; }
        public Nullable<DateTime> CM_ServiceStartDate { get; set; }
        public string CM_NaureOfBusiness { get; set; }
        public string CM_TLDeliveryLocation { get; set; }
        public string CM_Country { get; set; }
        public string CM_State { get; set; }
        public string CM_City { get; set; }
        public string CM_Pincode { get; set; }
        public string CM_Address { get; set; }
        public string CB_SPOCSanitation { get; set; }
        public string CB_SPOC_Name { get; set; }
        public string CB_SPOC_LastName { get; set; }
        public string CB_SPOC_Contact { get; set; }
        public string CB_SPOC_Email { get; set; }
        public string CB_SPOC_Designation { get; set; }
        public string CB_EP1_Sanitation { get; set; }
        public string CB_EP1_Name { get; set; }
        public string CB_EP1LastName { get; set; }
        public string CB_EP1_Contact { get; set; }
        public string CB_EP1_Email { get; set; }
        public string CB_EP1_Designation { get; set; }
        public string CB_EP2_Sanitation { get; set; }
        public string CB_EP2_Name { get; set; }
        public string CB_EP2LastName { get; set; }
        public string CB_EP2_Contact { get; set; }
        public string CB_EP2_Email { get; set; }
        public string CB_EP2_Designation { get; set; }
        public string CM_Status { get; set; }
        public string CM_RLCSAnchor { get; set; }
        public string CM_BDAnchor { get; set; }
        public string CM_InteractionID { get; set; }
        public string CM_CreatedBy { get; set; }
        public Nullable<DateTime> CM_CreatedDate { get; set; }
        public string CM_ModifiedBy { get; set; }
        public Nullable<DateTime> CM_ModifiedDate { get; set; }
        public string CM_Title { get; set; }
        public Nullable<int> CM_Version { get; set; }
        public string CM_PAN_Number { get; set; }
        public string CM_TAN_Number { get; set; }
        public string CM_ActType { get; set; }
        public string CM_ProcessAnchor { get; set; }
        public Nullable<bool> CM_Excemption { get; set; }
        public Nullable<decimal> CM_BonusPercentage { get; set; }
    }

    public class Response_Clients_Master
    {
        public List<Client> Clients { get; set; }
    }

    public class LocationFilter
    {
        public int? ID { get; set; }
        public string Name { get; set; }
    }

    public class ActVModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class RLCS_Request_DownloadReturn
    {
        public string ClientId { get; set; }
        public string SM_Code { get; set; }
        public string SM_Name { get; set; }
        public string LocationID { get; set; }
        public string LM_Name { get; set; }
        public string BranchID { get; set; }
        public string Month  { get; set; }
        public string Year { get; set; }        
        public string FormID { get; set; }
        public string ActId { get; set; }
        public string Frequency_ID { get; set; }
        public string FormName { get; set; }
        public string Return_DownloadType { get; set; }
        public string R_Type { get; set; }
        public string PF_ESI_Code { get; set; }
        public string EMPID { get; set; }
        public string Employee { get; set; }
        public string Add_parameter { get; set; }
        public string Sectionid { get; set; }
        public string Type { get; set; }
        public string Return_Path { get; set; }
    }
    public class RLCS_ReturnRecords_Post
    {
        public RLCS_ReturnRecords_Post()
        {
            lst_ReturnFormats = new List<RLCS_Request_DownloadReturn>();
        }
       public List<RLCS_Request_DownloadReturn> lst_ReturnFormats { get; set; }
    }
    public class RLCS_Response_DownloadReturn
    {
        public string Status { get; set; }
        public int StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public List<GeneratedReturnDetails> Returns { get; set; }
    }

    public class GeneratedReturnDetails
    {
        public string ReturnId { get; set; }
        public string Frequency_ID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string DocumentPath { get; set; }
    }

}
