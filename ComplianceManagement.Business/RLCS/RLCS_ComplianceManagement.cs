﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Reflection;
using static com.VirtuosoITech.ComplianceManagement.Business.RLCS.RLCSAPIClasses;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCS_ComplianceManagement
    {
        public static int GetAssignedComplianceRole(int customerID, int userID, long complianceInstanceID)
        {
            int roleID = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var complianceAssignmentRecord = entities.USP_RLCS_GetUserComplianceAssignment(userID, customerID).Where(row => row.ComplianceInstanceID == complianceInstanceID).OrderByDescending(row => row.RoleID).FirstOrDefault();

                    if (complianceAssignmentRecord != null)
                    {
                        roleID = complianceAssignmentRecord.RoleID;
                    }
                }

                return roleID;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return roleID;
            }
        }
        public static bool CheckComplianceAssignment(int customerID, int userID)
        {
            bool IsComplianceAssigned = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var numberAssignmentRecords = (from row in entities.USP_RLCS_GetUserComplianceAssignment(userID, customerID)
                                                   select row).Count();

                    if (numberAssignmentRecords > 0)
                    {
                        IsComplianceAssigned= true;
                    }
                }

                return IsComplianceAssigned;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return IsComplianceAssigned;
            }
        }

        public static bool UpdateComplianceAssignment(List<Tuple<long, int, int>> lstNewAssignment)
        {
            bool saveSuccess = false;
            try
            {
                if (lstNewAssignment.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        lstNewAssignment.ForEach(eachRecord =>
                        {
                            var prevAssignmentRecord = (from row in entities.ComplianceAssignments
                                                        where row.ComplianceInstanceID == eachRecord.Item1
                                                        && row.RoleID == eachRecord.Item2
                                                        select row).FirstOrDefault();

                            if (prevAssignmentRecord != null)
                            {
                                prevAssignmentRecord.UserID = eachRecord.Item3;
                            }
                        });

                        entities.SaveChanges();
                        saveSuccess = true;
                    }
                }
                return saveSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }
        
        public static List<RLCS_HRApplicableComplianceView> GetApplicableHRCompliancesByCustomerID(int customerID, List<int> lstBranches)
        {
            List<RLCS_HRApplicableComplianceView> lstApplicableCompliances = new List<RLCS_HRApplicableComplianceView>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstApplicableCompliances = (from row in entities.RLCS_HRApplicableComplianceView
                                                where row.CustomerID == customerID
                                                select row).ToList();

                    if (lstApplicableCompliances.Count > 0 && lstBranches.Count > 0)
                        lstApplicableCompliances = lstApplicableCompliances.Where(row => row.AVACOM_BranchID != null && lstBranches.Contains((int)row.AVACOM_BranchID)).ToList();
                }

                return lstApplicableCompliances;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstApplicableCompliances;
            }
        }

        public static User GetUserByID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.ID == userID
                            select row).SingleOrDefault();

                return user;
            }
        }

        public static SP_RLCS_ComplianceInstanceTransactionCount_Result GetPeriodBranchLocation(int customerID, int userID, string profileID, int scheduledOnID, int complianceInstanceID)
        {
            SP_RLCS_ComplianceInstanceTransactionCount_Result result = new SP_RLCS_ComplianceInstanceTransactionCount_Result();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if(scheduledOnID >0 && complianceInstanceID >0)
                {
                    result = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, profileID)
                                 where row.ScheduledOnID == scheduledOnID
                                && row.ComplianceInstanceID == complianceInstanceID
                                 select row).FirstOrDefault();

                }
                else
                {
                    result = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, profileID)
                                 where //row.ScheduledOnID == scheduledOnID
                                  row.ComplianceInstanceID == complianceInstanceID
                                 select row).FirstOrDefault();
                }
                

                return result;
            }
        }

        public static ComplianceInstance GetComplianceInstanceData(int instanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.ComplianceInstances
                                    where row.ID == instanceID && row.IsDeleted == false
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }

        public static List<ComplianceAssignment> GetComplianceAssignment(long complianceID, int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long complianceInstanceID = (from row in entities.ComplianceInstances
                                             where row.ComplianceId == complianceID
                                             && row.CustomerBranchID == customerBranchID
                                             && row.IsDeleted == false
                                             select row.ID).FirstOrDefault();

                List<ComplianceAssignment> complianceAssignment = null;

                if (complianceInstanceID > 0)
                {
                    complianceAssignment = (from row in entities.ComplianceAssignments
                                            where row.ComplianceInstanceID == complianceInstanceID
                                            select row).ToList();
                }

                return complianceAssignment;
            }
        }

        public static void SaveTempAssignments(List<TempAssignmentTable> lstTempAssignments)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstTempAssignments.ForEach(entry =>
                    {
                        var prevRecord = (from row in entities.TempAssignmentTables
                                          where row.ComplianceId == entry.ComplianceId
                                          && row.CustomerBranchID == entry.CustomerBranchID
                                          && row.RoleID == entry.RoleID
                                          && row.UserID == entry.UserID
                                          select row).FirstOrDefault();

                        if (prevRecord == null)
                        {
                            entities.TempAssignmentTables.Add(entry);
                            entities.SaveChanges();
                        }
                        else
                        {
                            prevRecord.IsActive = entry.IsActive;
                            entities.SaveChanges();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByPFOrESICCodeOrPTState(string clientID, string challanType, string codeValue, string branchType)
        {
            List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (!string.IsNullOrEmpty(challanType))
                    {
                        if (challanType.Trim().ToUpper().Equals("EPF"))
                        {
                            lstCustomerBranches = (from RCB in entities.RLCS_Client_BasicDetails
                                                   join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                   on RCB.CB_ClientID equals RCCM.CM_ClientID
                                                   where RCB.CB_PF_Code != null
                                                   && RCCM.AVACOM_BranchID != null
                                                   && RCB.CB_PF_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                                   && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                   && RCCM.BranchType.Equals(branchType)
                                                   && RCB.IsProcessed == true
                                                   && RCCM.CM_Status == "A"
                                                   select RCCM).Distinct().ToList();
                        }
                        else if (challanType.Trim().ToUpper().Equals("ESI"))
                        {
                            lstCustomerBranches = (from REM in entities.RLCS_Employee_Master
                                                   join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                   on REM.EM_ClientID equals RCCM.CM_ClientID
                                                   where REM.AVACOM_BranchID == RCCM.AVACOM_BranchID
                                                   && RCCM.AVACOM_BranchID != null
                                                   && REM.EM_Client_ESI_Number != null
                                                   && REM.EM_Client_ESI_Number.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                                   && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                   && RCCM.BranchType.Equals(branchType)
                                                   && REM.IsProcessed == true
                                                   && REM.EM_Status == "A"
                                                   && RCCM.CM_Status == "A"
                                                   select RCCM).Distinct().ToList();
                        }
                        else if (challanType.Trim().ToUpper().Equals("PT"))
                        {
                            lstCustomerBranches = (from REM in entities.RLCS_Employee_Master
                                                   join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                   on REM.EM_ClientID equals RCCM.CM_ClientID
                                                   where REM.AVACOM_BranchID == RCCM.AVACOM_BranchID
                                                   && REM.EM_Client_PT_State != null
                                                   && REM.EM_Client_PT_State.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                                   && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                   && RCCM.BranchType.Equals(branchType)
                                                   && REM.IsProcessed == true
                                                   && REM.EM_Status == "A"
                                                   && RCCM.CM_Status == "A"
                                                   select RCCM).Distinct().ToList();
                        }
                    }

                    return lstCustomerBranches;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstCustomerBranches;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByPTState(int customerID, string clientID, string ptState, string branchType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
                //List<CustomerBranch> lstCustomerBranches = new List<CustomerBranch>();

                lstCustomerBranches = (from row in entities.CustomerBranches
                                       join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                       on row.ID equals row1.AVACOM_BranchID
                                       where row1.AVACOM_BranchID != null
                                        && row.CustomerID == customerID
                                       && row1.CL_PT_State.Trim().ToUpper().Equals(ptState.Trim().ToUpper())
                                       && row1.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                       && row1.BranchType.Equals(branchType)
                                       select row1).ToList();
                return lstCustomerBranches;
            }
        }

        public static List<long> GetAll_Registers_Compliance_StateWise(string stateName, string establishmentType, bool? IsCLRAApplicable)
        {
            List<long> lstComplianceList = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstRegisters = (from row in entities.RLCS_Register_Compliance_Mapping
                                    where row.AVACOM_ComplianceID != null
                                    && row.RegisterID != null
                                    && row.Register_Status == "A"
                                    select row).ToList();

                if (lstRegisters.Count > 0)
                {
                    if (!string.IsNullOrEmpty(stateName))
                    {
                        lstRegisters = lstRegisters.Where(row => row.StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper()) || row.StateID.Trim().ToUpper().Equals("CENTR")).ToList();
                    }

                    if (!string.IsNullOrEmpty(establishmentType))
                    {
                        if (establishmentType.Equals("FACT"))
                        {
                            if (IsCLRAApplicable == true)
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT") || row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                            else
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                        }
                        else if (establishmentType.Equals("SEA"))
                        {
                            if (IsCLRAApplicable == true)
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA") || row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                            else
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                        }
                        else if (establishmentType.Equals("SF"))
                        {
                            lstRegisters = (from row in lstRegisters
                                            where row.Register_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                    }

                    lstComplianceList = lstRegisters.Select(row => (long)row.AVACOM_ComplianceID).ToList();
                }

                return lstComplianceList.Distinct().ToList();
            }
        }

        public static RLCS_Register_Compliance_Mapping Get_RegistersComplianceRecordsByRegisterID(int registerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var registerRecord = (from row in entities.RLCS_Register_Compliance_Mapping
                                      where row.RegisterID == registerID
                                      && row.RegisterID != null
                                      && row.AVACOM_ComplianceID != null
                                      && row.Register_Status == "A"
                                      select row).FirstOrDefault();

                return registerRecord;
            }
        }

        public static RLCS_Returns_Compliance_Mapping Get_ReturnsComplianceRecordsByReturnID(int returnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var returnRecord = (from row in entities.RLCS_Returns_Compliance_Mapping
                                    where row.ReturnID == returnID
                                    && row.ReturnID != null
                                    && row.AVACOM_ComplianceID != null
                                    && row.RM_Status == "A"
                                    select row).FirstOrDefault();

                return returnRecord;
            }
        }

        public static List<RLCS_Register_Compliance_Mapping> GetAll_Registers_StateWise(int loggedInUserID, string stateName, string establishmentType, string register_Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstRegisters = (from row in entities.RLCS_Register_Compliance_Mapping
                                    where row.AVACOM_ComplianceID != null
                                    && row.RegisterID != null
                                    && row.Register_Status == "A"
                                    select row).ToList();

                if (lstRegisters.Count > 0)
                {
                    if (!string.IsNullOrEmpty(stateName))
                    {
                        lstRegisters = lstRegisters.Where(row => row.StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper())).ToList();
                    }

                    if (!string.IsNullOrEmpty(establishmentType))
                    {
                        if (establishmentType.Equals("FACT"))
                        {
                            lstRegisters = (from row in lstRegisters
                                            where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT"))
                                            && row.Act_Type != null
                                            && row.Register_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                        else if (establishmentType.Equals("SEA"))
                        {
                            lstRegisters = (from row in lstRegisters
                                            where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA"))
                                            && row.Act_Type != null
                                            && row.Register_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                        else if (establishmentType.Equals("SF"))
                        {
                            lstRegisters = (from row in lstRegisters
                                            where row.Register_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(register_Type))
                    {
                        lstRegisters = (from row in lstRegisters
                                        where (row.Register_Type.Trim().ToUpper().Equals(register_Type))
                                        && row.Act_Type != null
                                        && row.Register_Status == "A"
                                        && row.AVACOM_ComplianceID != null
                                        select row).ToList();
                    }
                }

                if (lstRegisters.Count > 0 && loggedInUserID != 0)
                {
                    var myAssignedCompliances = GetAll_AssignedCompliances(loggedInUserID);

                    if (myAssignedCompliances != null)
                        if (myAssignedCompliances.Count > 0)
                            lstRegisters = lstRegisters.Where(row => myAssignedCompliances.Contains((long)row.AVACOM_ComplianceID)).ToList();
                }

                return lstRegisters.Distinct().ToList();
            }
        }

        public static List<long> GetAll_AssignedCompliances(int userID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var myAssignedCompliances = (from CI in entities.ComplianceInstances
                                                 join CA in entities.ComplianceAssignments
                                                 on CI.ID equals CA.ComplianceInstanceID
                                                 where CA.UserID == userID
                                                 && CI.IsDeleted == false
                                                 select CI.ComplianceId).Distinct().ToList();

                    return myAssignedCompliances;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
        public static List<long> GetAll_ChallanRelatedCompliance(string stateCode, string challanType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstChallanCompliances = (from row in entities.RLCS_Challan_Compliance_Mapping
                                             where row.AVACOM_ComplianceID != null
                                             && row.IsActive == true
                                             && row.AVACOM_ComplianceID != null
                                             select row).ToList();

                if (!string.IsNullOrEmpty(stateCode))
                    lstChallanCompliances = lstChallanCompliances.Where(row => row.State_Code.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())).ToList();

                if (!string.IsNullOrEmpty(challanType))
                    lstChallanCompliances = lstChallanCompliances.Where(row => row.ChallanType.Trim().ToUpper().Equals(challanType.Trim().ToUpper())).ToList();

                return lstChallanCompliances.Select(row => (long)row.AVACOM_ComplianceID).Distinct().ToList();
            }
        }

        public static List<long> GetAll_ReturnsRelatedCompliance_SectionWise(string taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstReturnCompliances = (from row in entities.RLCS_Returns_Compliance_Mapping
                                            where
                                            //&&  row.RM_StateID.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())   &&                                 
                                            row.AVACOM_ComplianceID != null
                                            && row.ReturnID != null
                                            && row.RM_Status == "A"
                                            select row).ToList();

                if (!string.IsNullOrEmpty(taskID))
                    lstReturnCompliances = lstReturnCompliances.Where(row => row.RM_SectionID.Trim().ToUpper().Equals(taskID.Trim().ToUpper())).ToList();

                return lstReturnCompliances.Select(row => (long)row.AVACOM_ComplianceID).Distinct().ToList();
            }
        }

        public static bool Check_Register_Compliance_Map(long complianceID, string stateName, string establishmentType, bool? IsCLRAApplicable = false)
        {
            bool mapCompliance = false;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstRegisters = (from row in entities.RLCS_Register_Compliance_Mapping
                                    where row.AVACOM_ComplianceID == complianceID
                                    && row.Register_Status == "A"
                                    select row).ToList();

                if (lstRegisters.Count > 0)
                {
                    if (!string.IsNullOrEmpty(stateName))
                    {
                        lstRegisters = lstRegisters.Where(row => row.StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper()) || row.StateID.Trim().ToUpper().Equals("CENTR")).ToList();
                    }

                    if (!string.IsNullOrEmpty(establishmentType))
                    {
                        if (establishmentType.Equals("FACT"))
                        {
                            if (IsCLRAApplicable == true)
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT") || row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                            else
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                        }
                        else if (establishmentType.Equals("SEA"))
                        {
                            if (IsCLRAApplicable == true)
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA") || row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                            else
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                        }
                        else if (establishmentType.Equals("SF"))
                        {
                            lstRegisters = (from row in lstRegisters
                                            where row.Register_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                    }

                    if (lstRegisters.Count > 0)
                        mapCompliance = true;
                }

                return mapCompliance;
            }
        }

        public static List<SP_RLCS_RegisterReturnChallanCompliance_Result> GetHRComplianceList_Assignment(int customerID, string scopeType, List<int> lstBranches, List<string> lstStates, string selectedStateCode, string establishmentType, string branchState, bool setApprover = false, List<int> actIdList = null, string filter = null, bool? IsCLRAApplicable=false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<long> complianceIDs = new List<long>();

                if (!string.IsNullOrEmpty(scopeType))
                {
                    if (scopeType.Trim().ToUpper().Equals("SOW03"))
                    {
                        if (!string.IsNullOrEmpty(establishmentType) && !string.IsNullOrEmpty(branchState))
                        {
                            complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType, IsCLRAApplicable);
                        }

                        //if (lstBranches.Count == 1)
                        //{
                        //    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(lstBranches[0]);

                        //    if (custBranchDetails != null)
                        //    {
                        //        if (!string.IsNullOrEmpty(custBranchDetails.CM_EstablishmentType) && !string.IsNullOrEmpty(custBranchDetails.CM_State))
                        //        {
                        //            establishmentType = custBranchDetails.CM_EstablishmentType;
                        //            branchState = custBranchDetails.CM_State;

                        //            complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                        //        }
                        //    }
                        //}

                        //complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                    }
                    else if (scopeType.Trim().ToUpper().Equals("SOW13") || scopeType.Trim().ToUpper().Equals("SOW14")
                        || scopeType.Trim().ToUpper().Equals("SOW15") || scopeType.Trim().ToUpper().Equals("SOW17"))
                    {
                        string challanType = string.Empty;

                        if (scopeType.Trim().ToUpper().Equals("SOW13"))
                            challanType = "EPF";
                        else if (scopeType.Trim().ToUpper().Equals("SOW14"))
                            challanType = "ESI";
                        else if (scopeType.Trim().ToUpper().Equals("SOW15"))
                            challanType = "LWF";
                        else if (scopeType.Trim().ToUpper().Equals("SOW17"))
                            challanType = "PT";

                        complianceIDs = RLCS_ComplianceManagement.GetAll_ChallanRelatedCompliance(string.Empty, challanType);
                    }
                    else if (scopeType.Trim().ToUpper().Equals("SOW05"))
                    {
                        complianceIDs = RLCS_ComplianceManagement.GetAll_ReturnsRelatedCompliance_SectionWise(string.Empty);
                    }
                }

                List<SP_RLCS_RegisterReturnChallanCompliance_Result> hrCompliances = new List<SP_RLCS_RegisterReturnChallanCompliance_Result>();

                if (complianceIDs.Count > 0)
                {
                    hrCompliances = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance()
                                     where row.IsDeleted == false
                                     && row.ScopeID == scopeType                                     
                                     && row.EventFlag == null && row.Status == null
                                     select row).ToList();

                    if (hrCompliances.Count > 0)
                    {
                        var lstApplicableCompliances = RLCS_ComplianceManagement.GetApplicableHRCompliancesByCustomerID(customerID, lstBranches);

                        if (lstApplicableCompliances != null)
                            if (lstApplicableCompliances.Count > 0)
                                hrCompliances.RemoveAll(row => !lstApplicableCompliances.Select(a => a.MasterID).Contains(row.MasterID));
                    }

                    if (actIDs.Count > 0 && hrCompliances.Count > 0)
                    {
                        hrCompliances = (from row in hrCompliances
                                         where actIDs.Contains(row.ActID)
                                         select row).ToList();
                    }

                    if (hrCompliances.Count > 0)
                    {
                        hrCompliances = (from row in hrCompliances
                                         where complianceIDs.Contains(row.ID)
                                         select row).ToList();
                    }

                    if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW03")))
                    {
                        hrCompliances = (from row in hrCompliances
                                         where lstStates.Contains(row.StateID)
                                         select row).ToList();
                    }
                    else if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW17")))
                    {
                        hrCompliances = (from row in hrCompliances
                                         where lstStates.Contains(row.StateID)
                                         select row).ToList();
                    }
                    else if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW05")))
                    {
                        hrCompliances = (from row in hrCompliances
                                         where (lstStates.Contains(row.StateID) || row.StateID == "Central")
                                         && row.RM_Type == "Return"
                                         select row).ToList();
                    }

                    if (!string.IsNullOrEmpty(selectedStateCode) && selectedStateCode != "-1")
                    {
                        hrCompliances = hrCompliances.Where(row => row.StateID.Equals(selectedStateCode)).ToList();
                    }

                    var TempQuery = (from row in entities.TempAssignmentTables
                                     where lstBranches.Contains(row.CustomerBranchID)
                                     && row.IsActive == true
                                     && row.UserID != -1
                                     select row.ComplianceId).ToList();

                    hrCompliances.RemoveAll(row => TempQuery.Contains(row.ID));

                    var prevMappedCompliances = (from row in entities.ComplianceInstances
                                                 join row1 in entities.ComplianceAssignments
                                                 on row.ID equals row1.ComplianceInstanceID
                                                 where lstBranches.Contains(row.CustomerBranchID)
                                                 && row.IsDeleted == false
                                                 select row.ComplianceId).ToList();

                    hrCompliances.RemoveAll(row => prevMappedCompliances.Contains(row.ID));

                    if (!string.IsNullOrEmpty(filter))
                    {
                        hrCompliances = hrCompliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }

                return hrCompliances.Distinct().OrderBy(row => row.ActID).ToList();
            }
        }

        public static List<SP_RLCS_RegisterReturnChallanCompliance_New_Result> GetHRComplianceList_Assignment_New(int customerID, string scopeType, List<int> lstBranches, List<string> lstStates, string selectedStateCode, string establishmentType, string branchState, bool setApprover = false, List<string> actIdList = null, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<long> complianceIDs = new List<long>();

                //if (!string.IsNullOrEmpty(scopeType))
                //{
                //    if (scopeType.Trim().ToUpper().Equals("SOW03"))
                //    {
                //        if (!string.IsNullOrEmpty(establishmentType) && !string.IsNullOrEmpty(branchState))
                //        {
                //            complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                //        }

                //        //if (lstBranches.Count == 1)
                //        //{
                //        //    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(lstBranches[0]);

                //        //    if (custBranchDetails != null)
                //        //    {
                //        //        if (!string.IsNullOrEmpty(custBranchDetails.CM_EstablishmentType) && !string.IsNullOrEmpty(custBranchDetails.CM_State))
                //        //        {
                //        //            establishmentType = custBranchDetails.CM_EstablishmentType;
                //        //            branchState = custBranchDetails.CM_State;

                //        //            complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                //        //        }
                //        //    }
                //        //}

                //        //complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                //    }
                //    else if (scopeType.Trim().ToUpper().Equals("SOW13") || scopeType.Trim().ToUpper().Equals("SOW14")
                //        || scopeType.Trim().ToUpper().Equals("SOW15") || scopeType.Trim().ToUpper().Equals("SOW17"))
                //    {
                //        string challanType = string.Empty;

                //        if (scopeType.Trim().ToUpper().Equals("SOW13"))
                //            challanType = "EPF";
                //        else if (scopeType.Trim().ToUpper().Equals("SOW14"))
                //            challanType = "ESI";
                //        else if (scopeType.Trim().ToUpper().Equals("SOW15"))
                //            challanType = "LWF";
                //        else if (scopeType.Trim().ToUpper().Equals("SOW17"))
                //            challanType = "PT";

                //        complianceIDs = RLCS_ComplianceManagement.GetAll_ChallanRelatedCompliance(string.Empty, challanType);
                //    }
                //    else if (scopeType.Trim().ToUpper().Equals("SOW05"))
                //    {
                //        complianceIDs = RLCS_ComplianceManagement.GetAll_ReturnsRelatedCompliance_SectionWise(string.Empty);
                //    }
                //}

                List<SP_RLCS_RegisterReturnChallanCompliance_New_Result> hrCompliances = new List<SP_RLCS_RegisterReturnChallanCompliance_New_Result>();
                //List<SP_RLCS_RegisterReturnChallanCompliance_Result> hrCompliances = new List<SP_RLCS_RegisterReturnChallanCompliance_Result>();

                //if (complianceIDs.Count > 0)
                //{

                hrCompliances = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance_New(customerID)
                                     //hrCompliances = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance() 
                                 where row.IsDeleted == false
                                 && row.ScopeID == scopeType
                                 && row.EventFlag == null && row.Status == null
                                 select row).ToList();

                if (hrCompliances.Count > 0)
                {
                    var lstApplicableCompliances = RLCS_ComplianceManagement.GetApplicableHRCompliancesByCustomerID(customerID, lstBranches);

                    if (lstApplicableCompliances != null)
                        if (lstApplicableCompliances.Count > 0)
                            hrCompliances.RemoveAll(row => !lstApplicableCompliances.Select(a => a.MasterID).Contains(row.MasterID));
                }

                if (actIDs.Count > 0 && hrCompliances.Count > 0)
                {
                    hrCompliances = (from row in hrCompliances
                                     where actIDs.Contains(row.ActID)
                                     select row).ToList();
                }

                //if (hrCompliances.Count > 0)
                //{
                //    hrCompliances = (from row in hrCompliances
                //                     where complianceIDs.Contains(row.ID)
                //                     select row).ToList();
                //}

                //if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW03")))
                //{
                //    hrCompliances = (from row in hrCompliances
                //                     where lstStates.Contains(row.StateID)
                //                     select row).ToList();
                //}
                //else if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW17")))
                //{
                //    hrCompliances = (from row in hrCompliances
                //                     where lstStates.Contains(row.StateID)
                //                     select row).ToList();
                //}
                //else if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW05")))
                //{
                //    hrCompliances = (from row in hrCompliances
                //                     where (lstStates.Contains(row.StateID) || row.StateID == "Central")
                //                     && row.RM_Type == "Return"
                //                     select row).ToList();
                //}

                hrCompliances = (from row in hrCompliances
                                 where scopeType.Contains(row.ScopeID) 
                                 && lstBranches.Contains(Convert.ToInt32(row.AVACOM_BranchID))
                                 && row.RM_Type != "Covering Letter"
                                 select row).ToList();

                if (actIdList.Count > 0)
                {
                    hrCompliances = (from row in hrCompliances
                                     where actIdList.Contains(row.ActGroup) 
                                     select row).ToList();
                }

                if (lstStates.Count > 0)
                {
                    hrCompliances = (from row in hrCompliances
                                     where (lstStates.Contains(row.StateID) || row.StateID == "Central")
                                     select row).ToList();
                }


                if (!string.IsNullOrEmpty(selectedStateCode) && selectedStateCode != "-1")
                {
                    hrCompliances = hrCompliances.Where(row => row.StateID.Equals(selectedStateCode)).ToList();
                }

                var TempQuery = (from row in entities.TempAssignmentTables
                                 where lstBranches.Contains(row.CustomerBranchID)
                                 && row.IsActive == true
                                 && row.UserID != -1
                                 select new { row.ComplianceId, row.CustomerBranchID }).ToList();

                //hrCompliances.RemoveAll(row => TempQuery.Contains(row.ID));
                foreach (var TempQry in TempQuery)
                {
                    hrCompliances.RemoveAll(row => row.ID == TempQry.ComplianceId && row.AVACOM_BranchID == TempQry.CustomerBranchID);
                }

                var prevMappedCompliances = (from row in entities.ComplianceInstances
                                             join row1 in entities.ComplianceAssignments
                                             on row.ID equals row1.ComplianceInstanceID
                                             where lstBranches.Contains(row.CustomerBranchID)
                                             && row.IsDeleted == false
                                             select new { row.ComplianceId, row.CustomerBranchID }).ToList();

                foreach (var prevMappedCompliance in prevMappedCompliances)
                {
                    hrCompliances.RemoveAll(row => row.ID == prevMappedCompliance.ComplianceId && row.AVACOM_BranchID == prevMappedCompliance.CustomerBranchID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    hrCompliances = hrCompliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                }
                //}

                return hrCompliances.Distinct().OrderBy(row => row.AVACOM_BranchID).ToList();
            }
        }


        #region RLCS Document Generation

        public static bool Save_GeneratedRegisters(int customerID, string scopeID, string month, string year, string clientID, string stateCode, string city, string branchName, RLCS_Register_Compliance_Mapping lstRegisters, string documentPath)
        {
            try
            {
                bool processSuccess = false;

                RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails = null;

                if (!string.IsNullOrEmpty(clientID))
                {
                    if (customerID != 0)
                    {
                        //Register
                        #region Register

                        if (scopeID.Trim().ToUpper().Equals("SOW03"))
                        {
                            customerBranchDetails = RLCS_ClientsManagement.GetClientLocationDetails(clientID, stateCode, city, branchName, "B");

                            if (customerBranchDetails != null && customerBranchDetails.CM_EstablishmentType != null)
                            {
                                //Get All Compliances To Map  
                                //Commented by shraddha ...
                               // var lstRegisterComplianceRecord = Get_RegistersComplianceRecordsByRegisterID(generatedRegID);

                                //var lstRegisterMappedCompliances = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(customerBranchDetails.CM_State, customerBranchDetails.CM_EstablishmentType.Trim().ToUpper());

                                if (lstRegisters != null)
                                {
                                    if (lstRegisters.AVACOM_ComplianceID != null)
                                    {
                                        processSuccess = Generate_InstanceAssignmentScheduleTransaction_Register(customerID, month, year, scopeID, customerBranchDetails, lstRegisters, documentPath);
                                        
                                        //Deduction 
                                        if (processSuccess)
                                        {
                                            if (customerBranchDetails.AVACOM_BranchID != null)
                                                PaymentManagement.ProcessPaymentDeduction_New(customerID, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID), month, year);
                                            
                                                //PaymentManagement.ProcessPaymentDeduction(customerID, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID), month, year);
                                        }
                                    }
                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "lstRegisterMappedCompliances=null-EstablishmentType-" + customerBranchDetails.CM_EstablishmentType,
                                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                            else
                                ContractManagement.InsertErrorMsg_DBLog("", "customerBranchDetails=null",
                                    MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }

                        #endregion
                    }
                    else
                        ContractManagement.InsertErrorMsg_DBLog("", "customerID=0",
                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "objRecord.LM_ClientID=NULL",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool Generate_InstanceAssignmentScheduleTransaction_Register(int customerID, string month, string year, string scopeID,
            RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails, RLCS_Register_Compliance_Mapping objRegDetailRecord, string documentPath)
        {
            bool processSuccess = false;
            try
            {
                if (customerBranchDetails.AVACOM_BranchID != null && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(year))
                {
                    DateTime dtStartDate = new DateTime();

                    if (customerBranchDetails.CM_ServiceStartDate != null)
                        dtStartDate = Convert.ToDateTime(customerBranchDetails.CM_ServiceStartDate);
                    else
                        dtStartDate = Convert.ToDateTime("2018-01-01");

                    int customerBranchID = Convert.ToInt32(customerBranchDetails.AVACOM_BranchID);

                    List<long> lstPerformers = new List<long>();
                    List<long> lstReviewers = new List<long>();

                    bool saveAsAVACOMDocumentSuccess = false;

                    //Each Compliance Generate ComplianceInstance and Assignment Records 

                    long complianceID = Convert.ToInt64(objRegDetailRecord.AVACOM_ComplianceID);

                    //Get Assignment Compliance Wise
                    var complianceAssignments = GetComplianceAssignment(complianceID, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID));

                    if (complianceAssignments != null)
                    {

                        if (complianceAssignments.Count > 0)
                        {
                            lstPerformers = complianceAssignments.Where(row => row.RoleID == 3).Select(row => row.UserID).ToList();
                            lstReviewers = complianceAssignments.Where(row => row.RoleID == 4).Select(row => row.UserID).ToList();

                            if (lstPerformers.Count > 0 || lstReviewers.Count > 0)
                            {
                                long complianceInstanceID = 0;
                                long complianceScheduleOnID = 0;
                                long complianceTxnID = 0;

                                string StartDate = Convert.ToDateTime(dtStartDate).ToString("dd-MM-yyyy");
                                DateTime ScheduledOn = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                #region Compliance Instance Assignment

                                //Performer
                                lstPerformers.ForEach(eachPerformer =>
                                {
                                    complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3);

                                    if (complianceInstanceID == 0)
                                        complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3, ScheduledOn);
                                });

                                //Reviewer
                                lstReviewers.ForEach(eachReviewer =>
                                {
                                    complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4);

                                    if (complianceInstanceID == 0)
                                        complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4, ScheduledOn);
                                });

                                #endregion

                                #region Compliance Schedule

                                if (complianceInstanceID != 0)
                                {
                                    complianceScheduleOnID = RLCS_ComplianceManagement.ExistsComplianceSchedule(complianceInstanceID, month, year);

                                    DateTime? rlcsActivityEndDate = DateTime.Now;

                                    //DateTime? rlcsActivityEndDate = null;
                                    //if (objRecord.LM_ActivityTo != null)
                                    //    rlcsActivityEndDate = Convert.ToDateTime(objRecord.LM_ActivityTo);

                                    if (complianceScheduleOnID == 0)
                                        complianceScheduleOnID = RLCS_ComplianceScheduleManagement.CreateScheduleOn(ScheduledOn, complianceInstanceID, complianceID, 1, "Admin User", month, year, rlcsActivityEndDate); //TBD  
                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "complianceInstanceID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                #endregion

                                #region Document Download

                                if (complianceScheduleOnID > 0)
                                {
                                    //complianceTxnID = RLCS_ComplianceScheduleManagement.CloseCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, objRecord);
                                    //complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, DateTime.Now, complianceAssignments);
                                    complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitOrCloseCompliance(true, complianceID, complianceInstanceID, complianceScheduleOnID, DateTime.Now, complianceAssignments);

                                    if (complianceTxnID != 0)
                                    {
                                        //Download Document
                                        bool downloadSuccess = false;

                                        string downloadFilePath = string.Empty;
                                        string fileName = string.Empty;

                                        string rlcsAPIURL = ConfigurationSettings.AppSettings["RLCSAPIURL"] + "Masters/";

                                        List<Tuple<string, int>> lstPath = new List<Tuple<string, int>>();

                                        if (documentPath != null) //2--Working Documents, 1--Compliance Document
                                        {
                                            if (!string.IsNullOrEmpty(documentPath))
                                                lstPath.Add(new Tuple<string, int>(documentPath.Replace("|", ""), 1));
                                        }

                                        if (scopeID.Trim().ToUpper().Equals("SOW03")) //Register
                                        {
                                            bool ISEmpWiseRegister = false;
                                            if (objRegDetailRecord != null)
                                            {
                                                if (objRegDetailRecord.Register_Type.Trim().Equals("I") || objRegDetailRecord.Register_Type.Trim().Equals("IN"))
                                                {
                                                    ISEmpWiseRegister = true;
                                                    //fileName = objRegDetailRecord.StateID + " " + objRegDetailRecord.Register_Name + ".zip";
                                                    fileName = objRegDetailRecord.Register_Name + ".zip";
                                                }
                                                else
                                                {
                                                    ISEmpWiseRegister = false;
                                                    fileName = objRegDetailRecord.StateID + " " + objRegDetailRecord.Register_Name + ".xls";
                                                }

                                                lstPath.ForEach(eachPath =>
                                                {
                                                    downloadFilePath = string.Empty;

                                                    if (ISEmpWiseRegister)
                                                    {
                                                        downloadFilePath += rlcsAPIURL + "GenerateZipFilesEmployeewise?FilePath=" + eachPath.Item1;
                                                    }
                                                    else
                                                    {
                                                        downloadFilePath += rlcsAPIURL + "Generate?FilePath=" + eachPath.Item1;
                                                    }

                                                    //if (ISEmpWiseRegister)
                                                    //    fileName = fileName + ".zip";

                                                    if (!string.IsNullOrEmpty(downloadFilePath) && !string.IsNullOrEmpty(fileName))
                                                    {
                                                        string downloadedFilePath = string.Empty;
                                                        downloadSuccess = RLCS_DocumentManagement.DownloadFileAndSaveDocuments(downloadFilePath, fileName, out downloadedFilePath);

                                                        if (downloadSuccess && !string.IsNullOrEmpty(downloadedFilePath))
                                                        {
                                                            saveAsAVACOMDocumentSuccess = RLCS_DocumentManagement.SaveAsAVACOMDocument(downloadedFilePath, customerID, customerBranchID,
                                                                complianceID, complianceInstanceID, complianceScheduleOnID, complianceTxnID, eachPath.Item2);

                                                            processSuccess = saveAsAVACOMDocumentSuccess;
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                    else
                                        ContractManagement.InsertErrorMsg_DBLog("", "complianceTxnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "complianceScheduleOnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                #endregion
                            }
                        }
                        else
                            ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                                MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    else
                        ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "FirstConditionFalse",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return processSuccess;
            }
        }

        public static bool Generate_Challans(int customerID, string challanType, string month, string year, string clientID, string codeValue, List<string> documentPath,out string messge)
        {
            try
            {
                bool processSuccess = false;
                string msg = "";
                messge = "";
                Tuple<bool, string> tpl = new Tuple<bool, string>(false, "");
                
                if (!string.IsNullOrEmpty(clientID))
                {
                    if (customerID != 0)
                    {
                        //Challan
                        #region Challan

                        if (challanType.Trim().ToUpper().Contains("PF") || challanType.Trim().ToUpper().Equals("ESI") || challanType.Trim().ToUpper().Equals("PT"))
                        {
                            List<long> lstChallan_Compliances = new List<long>();
                            List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();

                            if (challanType.Trim().ToUpper().Contains("PF")) //PF
                            {
                                challanType = "EPF";
                                lstChallan_Compliances = RLCS_ComplianceManagement.GetAll_ChallanRelatedCompliance(string.Empty, challanType);
                                lstCustomerBranches = RLCS_ComplianceManagement.GetCustomerBranchesByPFOrESICCodeOrPTState(clientID, challanType, codeValue, "E");
                            }
                            else if (challanType.Trim().ToUpper().Equals("ESI")) //ESI
                            {
                                challanType = "ESI";
                                lstChallan_Compliances = RLCS_ComplianceManagement.GetAll_ChallanRelatedCompliance(string.Empty, challanType);
                                lstCustomerBranches = RLCS_ComplianceManagement.GetCustomerBranchesByPFOrESICCodeOrPTState(clientID, challanType, codeValue, "B");
                            }
                            else if (challanType.Trim().ToUpper().Equals("PT")) //PT
                            {
                                challanType = "PT";
                                lstChallan_Compliances = RLCS_ComplianceManagement.GetAll_ChallanRelatedCompliance(codeValue, challanType);
                                lstCustomerBranches = RLCS_ComplianceManagement.GetCustomerBranchesByPFOrESICCodeOrPTState(clientID, challanType, codeValue, "B");
                            }

                            if (lstChallan_Compliances.Count > 0)
                            {
                                if (lstCustomerBranches != null)
                                {
                                    if (lstCustomerBranches.Count > 0)
                                    {
                                        lstCustomerBranches.ForEach(eachCustomerBranch =>
                                        {
                                           tpl  = Generate_InstanceAssignmentScheduleTransaction_Challan(customerID, month, year, eachCustomerBranch, lstChallan_Compliances, documentPath);
                                            
                                            if(tpl.Item1)
                                            {
                                                if (eachCustomerBranch.AVACOM_BranchID != null)
                                                    PaymentManagement.ProcessPaymentDeduction_New(customerID, Convert.ToInt32(eachCustomerBranch.AVACOM_BranchID), month, year);
                                                //PaymentManagement.ProcessPaymentDeduction(customerID, Convert.ToInt32(eachCustomerBranch.AVACOM_BranchID), month, year);
                                            }
                                        });
                                    }
                                    else
                                        ContractManagement.InsertErrorMsg_DBLog("", "NoBranch(s)Found-ClientID-" + clientID + "-ChallanType-" + challanType +
                                            "-Code-" + codeValue, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "NoBranch(s)Found-ClientID-" + clientID + "-ChallanType-" + challanType +
                                        "-Code-" + codeValue, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                messge = tpl.Item2;
                                if (messge == "")
                                {
                                    processSuccess = true;
                                }
                               
                            }
                            else
                                ContractManagement.InsertErrorMsg_DBLog("", challanType.Trim() + "-lstChallan_Compliances.Count=0",
                                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }

                        #endregion
                    }
                    else
                        ContractManagement.InsertErrorMsg_DBLog("", "customerID=0",
                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "objRecord.LM_ClientID=NULL",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                messge = "";
                return false;
            }
        }

        public static Tuple<bool,string> Generate_InstanceAssignmentScheduleTransaction_Challan(int customerID, string month, string year,
           RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails, List<long> lstChallanCompliances, List<string> lstDocumentPath)
        {
            bool processSuccess = false;
            string msg = "";
            try
            {
                if (customerBranchDetails.AVACOM_BranchID != null && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(year))
                {
                    DateTime dtStartDate = new DateTime();

                    if (customerBranchDetails.CM_ServiceStartDate != null)
                        dtStartDate = Convert.ToDateTime(customerBranchDetails.CM_ServiceStartDate);
                    else
                        dtStartDate = Convert.ToDateTime("2018-01-01");

                    int customerBranchID = Convert.ToInt32(customerBranchDetails.AVACOM_BranchID);

                    List<long> lstPerformers = new List<long>();
                    List<long> lstReviewers = new List<long>();

                    bool saveAsAVACOMDocumentSuccess = false;

                    if (lstChallanCompliances.Count > 0)
                    {
                        lstChallanCompliances.ForEach(eachChallanCompliance =>
                        {
                            //Each Compliance Generate ComplianceInstance and Assignment Records 
                            long complianceID = eachChallanCompliance;

                            //Get Assignment Compliance Wise
                            var complianceAssignments = GetComplianceAssignment(complianceID, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID));

                            if (complianceAssignments != null)
                            {

                                if (complianceAssignments.Count > 0)
                                {
                                    lstPerformers = complianceAssignments.Where(row => row.RoleID == 3).Select(row => row.UserID).ToList();
                                    lstReviewers = complianceAssignments.Where(row => row.RoleID == 4).Select(row => row.UserID).ToList();

                                    if (lstPerformers.Count > 0 && lstReviewers.Count > 0)
                                    {
                                        long complianceInstanceID = 0;
                                        long complianceScheduleOnID = 0;
                                        long complianceTxnID = 0;

                                        string StartDate = Convert.ToDateTime(dtStartDate).ToString("dd-MM-yyyy");
                                        DateTime ScheduledOn = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                        #region Compliance Instance Assignment

                                        //Performer
                                        lstPerformers = lstPerformers.Take(1).ToList();
                                        lstPerformers.ForEach(eachPerformer =>
                                        {
                                            complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3);

                                            if (complianceInstanceID == 0)
                                                complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3, ScheduledOn);
                                        });

                                        //Reviewer
                                        lstReviewers = lstReviewers.Take(1).ToList();
                                        lstReviewers.ForEach(eachReviewer =>
                                        {
                                            complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4);

                                            if (complianceInstanceID == 0)
                                                complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4, ScheduledOn);
                                        });

                                        #endregion

                                        #region Compliance Schedule

                                        DateTime scheduleOnDate = DateTime.Now;

                                        if (complianceInstanceID != 0)
                                        {
                                            var complianceScheduleOnRecord = RLCS_ComplianceManagement.ExistsComplianceScheduleRecord(complianceInstanceID, month, year);
                                            //complianceScheduleOnID = RLCS_ComplianceManagement.ExistsComplianceSchedule(complianceInstanceID, month, year);

                                            if (complianceScheduleOnRecord != null)
                                            {
                                                complianceScheduleOnID = complianceScheduleOnRecord.ID;
                                                scheduleOnDate = complianceScheduleOnRecord.ScheduleOn;

                                                DateTime? rlcsActivityEndDate = null;

                                                //DateTime? rlcsActivityEndDate = null;
                                                //if (objRecord.LM_ActivityTo != null)
                                                //    rlcsActivityEndDate = Convert.ToDateTime(objRecord.LM_ActivityTo);

                                                if (complianceScheduleOnID == 0)
                                                    complianceScheduleOnID = RLCS_ComplianceScheduleManagement.CreateScheduleOn(ScheduledOn, complianceInstanceID, complianceID, 1, "Admin User", month, year, rlcsActivityEndDate); //TBD  
                                            }
                                        }
                                        else
                                            ContractManagement.InsertErrorMsg_DBLog("", "complianceInstanceID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                        #endregion

                                        #region Document Download

                                        if (complianceScheduleOnID > 0)
                                        {
                                            //complianceTxnID = RLCS_ComplianceScheduleManagement.CloseCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, objRecord);
                                            //complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, scheduleOnDate, complianceAssignments);
                                            complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitOrCloseCompliance(false, complianceID, complianceInstanceID, complianceScheduleOnID, DateTime.Now, complianceAssignments);

                                            if (complianceTxnID != 0)
                                            {
                                                //Download Document
                                                bool downloadSuccess = false;

                                                string downloadFilePath = string.Empty;
                                                string fileName = string.Empty;

                                                string rlcsAPIURL = ConfigurationSettings.AppSettings["RLCSAPIURL"] + "Masters/";

                                                lstDocumentPath.ForEach(eachPath =>
                                                {
                                                    downloadFilePath = string.Empty;

                                                    fileName = new DirectoryInfo(eachPath).Name;
                                                    downloadFilePath += rlcsAPIURL + "Generate?FilePath=" + eachPath;

                                                    //downloadFilePath = downloadFilePath + @"\" + fileName;                                                   

                                                    if (!string.IsNullOrEmpty(downloadFilePath) && !string.IsNullOrEmpty(fileName))
                                                    {
                                                        string downloadedFilePath = string.Empty;
                                                        downloadSuccess = RLCS_DocumentManagement.DownloadFileAndSaveDocuments(downloadFilePath, fileName, out downloadedFilePath);

                                                        if (downloadSuccess && !string.IsNullOrEmpty(downloadedFilePath))
                                                        {
                                                            saveAsAVACOMDocumentSuccess = RLCS_DocumentManagement.SaveAsAVACOMDocument(downloadedFilePath, customerID, customerBranchID,
                                                                complianceID, complianceInstanceID, complianceScheduleOnID, complianceTxnID, 2);
                                                        }

                                                        if (saveAsAVACOMDocumentSuccess)
                                                            processSuccess = saveAsAVACOMDocumentSuccess;
                                                        else
                                                            ContractManagement.InsertErrorMsg_DBLog("", "DownloadError-" + eachPath, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                    }
                                                });
                                            }
                                            else
                                                ContractManagement.InsertErrorMsg_DBLog("", "complianceTxnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }
                                        else
                                        {
                                            ContractManagement.InsertErrorMsg_DBLog("", "complianceScheduleOnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            msg = "There are no schedules for selected criteria,Please try later";
                                        }

                                        #endregion
                                    }
                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                            else
                                ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                                    MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        });
                    }
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "FirstConditionFalse",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return Tuple.Create(processSuccess,msg);
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Tuple.Create(false,msg);
            }
        }

        public static bool Save_GeneratedReturns(int customerID, int branchID, string month, string year, RLCS_Returns_Compliance_Mapping returnMasterRecord, string documentPath)
        {
            try
            {
                bool processSuccess = false;

                RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails = null;

                if (customerID != 0)
                {
                    customerBranchDetails = RLCS_ClientsManagement.GetClientLocationDetails(branchID, "B");

                    if (customerBranchDetails != null)
                    {
                        if (returnMasterRecord != null)
                        {
                            if (returnMasterRecord.AVACOM_ComplianceID != null)
                            {
                                processSuccess = Generate_InstanceAssignmentScheduleTransaction_Returns(customerID, month, year, customerBranchDetails, returnMasterRecord, documentPath);

                                //Deduction 
                                if (processSuccess)
                                {
                                    PaymentManagement.ProcessPaymentDeduction_New(customerID, branchID, month, year);

                                    //PaymentManagement.ProcessPaymentDeduction(customerID, branchID, month, year);
                                }
                            }
                        }
                        else
                            ContractManagement.InsertErrorMsg_DBLog("", "returnMasterRecord=null",
                                MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    else
                        ContractManagement.InsertErrorMsg_DBLog("", "customerBranchDetails=null",
                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "customerID=0",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool Generate_InstanceAssignmentScheduleTransaction_Returns(int customerID, string month, string year,
            RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails, RLCS_Returns_Compliance_Mapping objRetDetailRecord, string documentPath)
        {
            bool processSuccess = false;
            try
            {
                if (customerBranchDetails.AVACOM_BranchID != null && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(year))
                {
                    DateTime dtStartDate = new DateTime();

                    if (customerBranchDetails.CM_ServiceStartDate != null)
                        dtStartDate = Convert.ToDateTime(customerBranchDetails.CM_ServiceStartDate);
                    else
                        dtStartDate = Convert.ToDateTime("2018-01-01");

                    int customerBranchID = Convert.ToInt32(customerBranchDetails.AVACOM_BranchID);

                    List<long> lstPerformers = new List<long>();
                    List<long> lstReviewers = new List<long>();

                    bool saveAsAVACOMDocumentSuccess = false;

                    //Each Compliance Generate ComplianceInstance and Assignment Records 

                    long complianceID = Convert.ToInt64(objRetDetailRecord.AVACOM_ComplianceID);

                    //Get Assignment Compliance Wise
                    var complianceAssignments = GetComplianceAssignment(complianceID, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID));

                    if (complianceAssignments != null)
                    {
                        if (complianceAssignments.Count > 0)
                        {
                            lstPerformers = complianceAssignments.Where(row => row.RoleID == 3).Select(row => row.UserID).ToList();
                            lstReviewers = complianceAssignments.Where(row => row.RoleID == 4).Select(row => row.UserID).ToList();

                            if (lstPerformers.Count > 0 || lstReviewers.Count > 0)
                            {
                                long complianceInstanceID = 0;
                                long complianceScheduleOnID = 0;
                                long complianceTxnID = 0;

                                string StartDate = Convert.ToDateTime(dtStartDate).ToString("dd-MM-yyyy");
                                DateTime ScheduledOn = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                #region Compliance Instance Assignment

                                //Performer
                                lstPerformers.ForEach(eachPerformer =>
                                {
                                    complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3);

                                    if (complianceInstanceID == 0)
                                        complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3, ScheduledOn);
                                });

                                //Reviewer
                                lstReviewers.ForEach(eachReviewer =>
                                {
                                    complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4);

                                    if (complianceInstanceID == 0)
                                        complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4, ScheduledOn);
                                });

                                #endregion

                                #region Compliance Schedule

                                if (complianceInstanceID != 0)
                                {
                                    complianceScheduleOnID = RLCS_ComplianceManagement.ExistsComplianceSchedule(complianceInstanceID, month, year);

                                    DateTime? rlcsActivityEndDate = DateTime.Now;

                                    //DateTime? rlcsActivityEndDate = null;
                                    //if (objRecord.LM_ActivityTo != null)
                                    //    rlcsActivityEndDate = Convert.ToDateTime(objRecord.LM_ActivityTo);

                                    if (complianceScheduleOnID == 0)
                                        complianceScheduleOnID = RLCS_ComplianceScheduleManagement.CreateScheduleOn(ScheduledOn, complianceInstanceID, complianceID, 1, "Admin User", month, year, rlcsActivityEndDate); //TBD  
                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "complianceInstanceID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                #endregion

                                #region Document Download

                                if (complianceScheduleOnID > 0)
                                {
                                    //complianceTxnID = RLCS_ComplianceScheduleManagement.CloseCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, objRecord);
                                    //complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, DateTime.Now, complianceAssignments);
                                    complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitOrCloseCompliance(false, complianceID, complianceInstanceID, complianceScheduleOnID, DateTime.Now, complianceAssignments);

                                    if (complianceTxnID != 0)
                                    {
                                        //Download Document
                                        bool downloadSuccess = false;

                                        string downloadFilePath = string.Empty;
                                        string fileName = string.Empty;

                                        string rlcsAPIURL = ConfigurationSettings.AppSettings["RLCSAPIURL"] + "Masters/";

                                        List<Tuple<string, int>> lstPath = new List<Tuple<string, int>>();

                                        if (documentPath != null) //2--Working Documents, 1--Compliance Document
                                        {
                                            if (!string.IsNullOrEmpty(documentPath))
                                                lstPath.Add(new Tuple<string, int>(documentPath.Replace("|", ""), 2));
                                        }


                                        bool ISEmpWiseRegister = false;
                                        if (objRetDetailRecord != null)
                                        {
                                            if (objRetDetailRecord.RM_Return_Category.Trim().Equals("I") || objRetDetailRecord.RM_Return_Category.Trim().Equals("IN"))
                                            {
                                                ISEmpWiseRegister = true;
                                                //fileName = objRegDetailRecord.StateID + " " + objRegDetailRecord.Register_Name + ".zip";
                                                fileName = objRetDetailRecord.RM_Name + ".zip";
                                            }
                                            else
                                            {
                                                ISEmpWiseRegister = false;
                                                if (!string.IsNullOrEmpty(objRetDetailRecord.RM_Doc_Format))
                                                {
                                                    if (objRetDetailRecord.RM_Doc_Format.Trim().ToUpper().Equals("DOC"))
                                                        fileName = objRetDetailRecord.RM_Name + ".doc";
                                                    else if (objRetDetailRecord.RM_Doc_Format.Trim().ToUpper().Equals("EXCEL"))
                                                        fileName = objRetDetailRecord.RM_Name + ".xls";
                                                }
                                            }

                                            lstPath.ForEach(eachPath =>
                                            {
                                                downloadFilePath = string.Empty;

                                                if (ISEmpWiseRegister)
                                                {
                                                    downloadFilePath += rlcsAPIURL + "GenerateZipFilesEmployeewise?FilePath=" + eachPath.Item1;
                                                }
                                                else
                                                {
                                                    downloadFilePath += rlcsAPIURL + "Generate?FilePath=" + eachPath.Item1;
                                                }

                                                //if (ISEmpWiseRegister)
                                                //    fileName = fileName + ".zip";

                                                if (!string.IsNullOrEmpty(downloadFilePath) && !string.IsNullOrEmpty(fileName))
                                                {
                                                    string downloadedFilePath = string.Empty;
                                                    downloadSuccess = RLCS_DocumentManagement.DownloadFileAndSaveDocuments(downloadFilePath, fileName, out downloadedFilePath);

                                                    if (downloadSuccess && !string.IsNullOrEmpty(downloadedFilePath))
                                                    {
                                                        saveAsAVACOMDocumentSuccess = RLCS_DocumentManagement.SaveAsAVACOMDocument(downloadedFilePath, customerID, customerBranchID,
                                                            complianceID, complianceInstanceID, complianceScheduleOnID, complianceTxnID, eachPath.Item2);

                                                        processSuccess = saveAsAVACOMDocumentSuccess;
                                                    }
                                                }
                                            });
                                        }

                                    }
                                    else
                                        ContractManagement.InsertErrorMsg_DBLog("", "complianceTxnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "complianceScheduleOnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                #endregion
                            }
                        }
                        else
                            ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                                MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    else
                        ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "FirstConditionFalse",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return processSuccess;
            }
        }

        public static long ExistsComplianceInstanceAssignment(long complianceID, int customerBranchID, int userID, int roleID)
        {
            long complianceInstanceID = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    complianceInstanceID = (from CI in entities.ComplianceInstances
                                            join CA in entities.ComplianceAssignments
                                            on CI.ID equals CA.ComplianceInstanceID
                                            where CI.ComplianceId == complianceID
                                            && CI.CustomerBranchID == customerBranchID
                                            && CA.UserID == userID
                                            && CA.RoleID == roleID
                                            && CI.IsDeleted == false
                                            select CI.ID).FirstOrDefault();

                    return complianceInstanceID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return complianceInstanceID;
            }
        }

        public static long ExistsComplianceSchedule(long complianceInstanceID, string rlcsPayrollMonth, string rlcsPayrollYear)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var existComplianceSchduleRecord = (from CSO in entities.ComplianceScheduleOns
                                                        where CSO.ComplianceInstanceID == complianceInstanceID
                                                        && CSO.RLCS_PayrollMonth.Trim().ToUpper().Equals(rlcsPayrollMonth.Trim().ToUpper())
                                                        && CSO.RLCS_PayrollYear.Trim().ToUpper().Equals(rlcsPayrollYear.Trim().ToUpper())
                                                        && CSO.IsActive == true && CSO.IsUpcomingNotDeleted == true
                                                        select CSO).FirstOrDefault();

                    if (existComplianceSchduleRecord != null)
                    {
                        return existComplianceSchduleRecord.ID;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static ComplianceScheduleOn ExistsComplianceScheduleRecord(long complianceInstanceID, string rlcsPayrollMonth, string rlcsPayrollYear)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var existComplianceSchduleRecord = (from CSO in entities.ComplianceScheduleOns
                                                        where CSO.ComplianceInstanceID == complianceInstanceID
                                                        && CSO.RLCS_PayrollMonth.Trim().ToUpper().Equals(rlcsPayrollMonth.Trim().ToUpper())
                                                        && CSO.RLCS_PayrollYear.Trim().ToUpper().Equals(rlcsPayrollYear.Trim().ToUpper())
                                                        && CSO.IsActive == true && CSO.IsUpcomingNotDeleted == true
                                                        select CSO).FirstOrDefault();

                    return existComplianceSchduleRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion

        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Result> GetRLCSMyWorkspace(int customerID, int userID, string queryStringFlag, int risk,string month,int year, string status, int location, string stringSearchText, string complianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_RLCS_ComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Result>();

                int reviewerRoleID = 4;

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Upcoming")
                {
                    //ComplianceInstanceTransactionViews
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where 
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "DueButNotSubmitted" || status == "Overdue")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "ClosedTimely")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 4)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "ClosedDelayed")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 5)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status.Trim().ToUpper().Equals("0")) //0-ALL
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                          //row.UserID == userID
                                          row.IsActive == true && row.IsUpcomingNotDeleted == true                                          
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                if (!string.IsNullOrEmpty(complianceType))
                {
                    List<long> lstComplianceIDs = new List<long>();

                    if (complianceType.Trim().ToUpper().Equals("SOW03"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Register_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.RegisterID != null
                                            && row.Register_Status == "A"
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }
                    else if (complianceType.Trim().ToUpper().Equals("EPF") || complianceType.Trim().ToUpper().Equals("ESI") || complianceType.Trim().ToUpper().Equals("PT"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Challan_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.IsActive == true
                                            && row.ChallanType.Equals(complianceType)
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }
                    else if (complianceType.Trim().ToUpper().Equals("SOW05"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Returns_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.IsActive == true
                                            && row.RM_Status == "A"
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }

                    if (lstComplianceIDs.Count > 0)
                    {
                        transactionsQuery = transactionsQuery.Where(row => lstComplianceIDs.Contains(row.ComplianceID)).ToList();
                    }
                }

                if (queryStringFlag == "C")
                {
                    //transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }
                //Month
                if (month != "" && month != "-1")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.RLCS_PayrollMonth == month)).ToList();
                }
                //Year
                if (year != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.RLCS_PayrollYear == Convert.ToString(year))).ToList();
                }

                // Find data through String contained in Description
                if (!string.IsNullOrEmpty(stringSearchText.Trim()))
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(stringSearchText))).ToList();
                }

                return transactionsQuery.ToList();
            }
        }

        //returns statewise
        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Result> GetRLCSMyWorkspace(int customerID, int userID, string queryStringFlag, int risk, string status, int location, string stringSearchText, string complianceType, DateTime? fromDate, DateTime? toDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_RLCS_ComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Result>();

                int reviewerRoleID = 4;

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Upcoming")
                {
                    //ComplianceInstanceTransactionViews
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "DueButNotSubmitted" || status == "Overdue")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "ClosedTimely")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 4)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "ClosedDelayed")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 5)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status.Trim().ToUpper().Equals("0")) //0-ALL
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                if (!string.IsNullOrEmpty(complianceType))
                {
                    List<long> lstComplianceIDs = new List<long>();

                    if (complianceType.Trim().ToUpper().Equals("SOW03"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Register_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.RegisterID != null
                                            && row.Register_Status == "A"
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }
                    else if (complianceType.Trim().ToUpper().Equals("EPF") || complianceType.Trim().ToUpper().Equals("ESI") || complianceType.Trim().ToUpper().Equals("PT"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Challan_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.IsActive == true
                                            && row.ChallanType.Equals(complianceType)
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }
                    else if (complianceType.Trim().ToUpper().Equals("SOW05"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Returns_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.IsActive == true
                                            && row.RM_Status == "A"
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }

                    if (lstComplianceIDs.Count > 0)
                    {
                        transactionsQuery = transactionsQuery.Where(row => lstComplianceIDs.Contains(row.ComplianceID)).ToList();
                    }
                }

                if (queryStringFlag == "C")
                {
                    //transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                // Find data through String contained in Description
                if (!string.IsNullOrEmpty(stringSearchText.Trim()))
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(stringSearchText))).ToList();
                }

                if (fromDate != null && toDate != null)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn.Date >= fromDate && entry.ScheduledOn <= toDate)).ToList();
                }

                return transactionsQuery.ToList();
            }
        }

        public static List<RLCS_Returns_Compliance_Mapping> GetAll_Returns_StateWise(string stateName, string establishmentType, string register_Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstReturns = (from row in entities.RLCS_Returns_Compliance_Mapping
                                    where row.AVACOM_ComplianceID != null
                                    && row.ReturnID != null
                                    && row.RM_Status == "A"
                                    select row).ToList();

                if (lstReturns.Count > 0)
                {
                    if (!string.IsNullOrEmpty(stateName))
                    {
                        lstReturns = lstReturns.Where(row => row.RM_StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper())).ToList();
                    }

                    if (!string.IsNullOrEmpty(establishmentType))
                    {
                        if (establishmentType.Equals("FACT"))
                        {
                            lstReturns = (from row in lstReturns
                                            where (row.RM_Act.Trim().ToUpper().Equals("SF") || row.RM_Act.Trim().ToUpper().Equals("FACT"))
                                            && row.RM_Act != null
                                            && row.RM_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                        else if (establishmentType.Equals("SEA"))
                        {
                            lstReturns = (from row in lstReturns
                                            where (row.RM_Act.Trim().ToUpper().Equals("SF") || row.RM_Act.Trim().ToUpper().Equals("SEA"))
                                            && row.RM_Act != null
                                            && row.RM_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                        else if (establishmentType.Equals("SF"))
                        {
                            lstReturns = (from row in lstReturns
                                          where row.RM_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                    }

                    //if (!string.IsNullOrEmpty(register_Type))
                    //{
                    //    lstReturns = (from row in lstReturns
                    //                    where (row.RM_Return_Type.Trim().ToUpper().Equals(register_Type))
                    //                    && row.RM_Act != null
                    //                    && row.RM_Status == "A"
                    //                    && row.AVACOM_ComplianceID != null
                    //                    select row).ToList();
                    //}
                }

                if (lstReturns.Count > 0)
                {

                }

                return lstReturns.Distinct().ToList();
            }
        }

        //public static List<object> GetReturn_Act_Frequency(int custBranchID, string bindType, List<SP_RLCS_ReturnMaster_Result> lstReturnMaster, string returnType, string selectActType, string selectedFreq)
        //{
        //    try
        //    {
        //        List<object> lstItems = new List<object>();

        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            if (!string.IsNullOrEmpty(bindType))
        //            {
        //                if (bindType == "Act")
        //                {
        //                    #region Act

        //                    if (!string.IsNullOrEmpty(returnType))
        //                    {
        //                        if (returnType == "Central")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID == "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.RS_ActGroup,
        //                                            Name = RRCM.RS_ActName,
        //                                        }).Distinct().OrderBy(item => item.Name).ToList<object>();
        //                        }
        //                        else if (returnType == "State")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID != "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.RS_ActGroup,
        //                                            Name = RRCM.RS_ActName,
        //                                        }).Distinct().OrderBy(item => item.Name).ToList<object>();
        //                        }
        //                    }

        //                    #endregion
        //                }
        //                else if (bindType == "Frequency")
        //                {
        //                    #region Frequency
        //                    if (!string.IsNullOrEmpty(selectActType))
        //                    {
        //                        if (returnType == "Central")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID == "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && RRCM.RM_Act == selectActType
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.RM_Frequency,
        //                                            Name = RRCM.RM_Frequency,
        //                                        }).Distinct().ToList<object>();
        //                        }
        //                        else if (returnType == "State")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID != "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && RRCM.RM_Act == selectActType
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.RM_Frequency,
        //                                            Name = RRCM.RM_Frequency,
        //                                        }).Distinct().ToList<object>();
        //                        }
        //                    }

        //                    #endregion
        //                }
        //                else if (bindType == "Return")
        //                {
        //                    #region Return

        //                    if (!string.IsNullOrEmpty(selectedFreq))
        //                    {
        //                        if (returnType == "Central")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID == "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && RRCM.RM_Act == selectActType
        //                                        && RRCM.RM_Frequency == selectedFreq
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.ReturnID,
        //                                            Name = RRCM.RM_Name,
        //                                        }).Distinct().ToList<object>();
        //                        }
        //                        else if (returnType == "State")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID != "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && RRCM.RM_Act == selectActType
        //                                        && RRCM.RM_Frequency == selectedFreq
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.ReturnID,
        //                                            Name = RRCM.RM_Name,
        //                                        }).Distinct().ToList<object>();
        //                        }
        //                    }

        //                    #endregion
        //                }
        //            }

        //            return lstItems;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return null;
        //    }
        //}

        public static RLCS_Returns_Compliance_Mapping GetReturnDetails(long returnID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstMasterRecords = (from RM in entities.RLCS_Returns_Compliance_Mapping
                                            where RM.ReturnID == returnID
                                            select RM).FirstOrDefault();

                    return lstMasterRecords;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public static List<RLCS_Returns_Compliance_Mapping> GetAllReturnsDetail(long custBranchID, string Type)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstReturnMaster = GetAll_ReturnMaster();

                    var lstItems = (from RRCM in lstReturnMaster
                                    join CI in entities.ComplianceInstances
                                    on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                    where RRCM.RM_StateID == Type
                                    && RRCM.RM_Status == "A"
                                    && CI.CustomerBranchID == custBranchID
                                    select new RLCS_Returns_Compliance_Mapping()
                                    {
                                        ReturnID = RRCM.ReturnID,
                                        RM_Frequency = RRCM.RM_Frequency,
                                        RM_Act = RRCM.RM_Act,
                                        RM_Return_Category = RRCM.RM_Return_Category,
                                        RM_Doc_Format = RRCM.RM_Doc_Format,
                                        RM_Name=RRCM.RM_Name,
                                        RM_Download_Path = RRCM.RM_Download_Path
                                    }).Distinct().ToList();

                    return lstItems;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<RLCS_Returns_Compliance_Mapping> GetAllReturnsDetail(long custBranchID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstReturnMaster = GetAll_ReturnMaster();

                    var lstItems = (from RRCM in lstReturnMaster
                                    join CI in entities.ComplianceInstances
                                    on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                    where RRCM.RM_Status == "A"
                                    && CI.CustomerBranchID == custBranchID
                                    select new RLCS_Returns_Compliance_Mapping()
                                    {
                                        ReturnID = RRCM.ReturnID,
                                        RM_Frequency = RRCM.RM_Frequency,
                                        RM_Act = RRCM.RM_Act,
                                        RM_Return_Category = RRCM.RM_Return_Category,
                                        RM_Doc_Format = RRCM.RM_Doc_Format,
                                        RM_Name = RRCM.RM_Name,
                                        RM_Download_Path = RRCM.RM_Download_Path
                                    }).Distinct().ToList();

                    return lstItems;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<RLCS_Returns_Compliance_Mapping> GetAllReturnsDetailForEntity(List<int>Branches)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstReturnMaster = GetAll_ReturnMaster();

                    var lstItems = (from RRCM in lstReturnMaster
                                    join CI in entities.ComplianceInstances
                                    on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                    where RRCM.RM_Status == "A"
                                    && Branches.Contains(CI.CustomerBranchID)
                                    select new RLCS_Returns_Compliance_Mapping()
                                    {
                                        ReturnID = RRCM.ReturnID,
                                        RM_Frequency = RRCM.RM_Frequency,
                                        RM_Act = RRCM.RM_Act,
                                        RM_Return_Category = RRCM.RM_Return_Category,
                                        RM_Doc_Format = RRCM.RM_Doc_Format,
                                        RM_Name = RRCM.RM_Name,
                                        RM_Download_Path = RRCM.RM_Download_Path
                                    }).Distinct().ToList();

                    return lstItems;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public static List<SP_RLCS_ReturnMaster_Result> GetAll_ReturnMaster()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstMasterRecords = (from RM in entities.SP_RLCS_ReturnMaster()
                                            where RM.AVACOM_ComplianceID != null
                                            select RM).ToList();

                    return lstMasterRecords;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<SP_RLCS_ReturnMaster_Result> GetAll_ReturnMasterByType(string ReturnChallanType)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstMasterRecords = (from RM in entities.SP_RLCS_ReturnMaster()
                                            where RM.AVACOM_ComplianceID != null && RM.RS_ActGroup.Trim().ToUpper() == ReturnChallanType.ToUpper()
                                            select RM).ToList();

                    return lstMasterRecords;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<object> GetReturn_Act_Frequency(int custBranchID, string bindType, List<SP_RLCS_ReturnMaster_Result> lstReturnMaster, string returnType, string selectActType, string selectedFreq)
        {
            try
            {
                List<object> lstItems = new List<object>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (!string.IsNullOrEmpty(bindType))
                    {
                        if (bindType == "Act")
                        {
                            #region Act

                            if (!string.IsNullOrEmpty(returnType))
                            {
                                if (returnType == "Central")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID == "Central"
                                                && RRCM.RM_Status == "A"
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.RS_ActGroup,
                                                    Name = RRCM.RS_ActName,
                                                }).Distinct().OrderBy(item => item.Name).ToList<object>();
                                }
                                else if (returnType == "State")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID != "Central"
                                                && RRCM.RM_Status == "A"
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.RS_ActGroup,
                                                    Name = RRCM.RS_ActName,
                                                }).Distinct().OrderBy(item => item.Name).ToList<object>();
                                }
                            }

                            #endregion
                        }
                        else if (bindType == "Frequency")
                        {
                            #region Frequency
                            if (!string.IsNullOrEmpty(selectActType))
                            {
                                if (returnType == "Central")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID == "Central"
                                                && RRCM.RM_Status == "A"
                                                && RRCM.RM_Act == selectActType
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.RM_Frequency,
                                                    Name = RRCM.RM_Frequency,
                                                }).Distinct().ToList<object>();
                                }
                                else if (returnType == "State")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID != "Central"
                                                && RRCM.RM_Status == "A"
                                                && RRCM.RM_Act == selectActType
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.RM_Frequency,
                                                    Name = RRCM.RM_Frequency,
                                                }).Distinct().ToList<object>();
                                }
                            }

                            #endregion
                        }
                        else if (bindType == "Return")
                        {
                            #region Return

                            if (!string.IsNullOrEmpty(selectedFreq))
                            {
                                if (returnType == "Central")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID == "Central"
                                                && RRCM.RM_Status == "A"
                                                && RRCM.RM_Act == selectActType
                                                && RRCM.RM_Frequency == selectedFreq
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.ReturnID,
                                                    Name = RRCM.RM_Name,
                                                }).Distinct().ToList<object>();
                                }
                                else if (returnType == "State")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID != "Central"
                                                && RRCM.RM_Status == "A"
                                                && RRCM.RM_Act == selectActType
                                                && RRCM.RM_Frequency == selectedFreq
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.ReturnID,
                                                    Name = RRCM.RM_Name,
                                                }).Distinct().ToList<object>();
                                }
                            }

                            #endregion
                        }
                    }

                    return lstItems;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<object> GetReturnList(int custBranchID, List<SP_RLCS_ReturnMaster_Result> lstReturnMaster, string selectedFreq)
        {
            try
            {
                List<object> lstItems = new List<object>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (custBranchID > 0)
                    {
                        var returns = (from RRCM in lstReturnMaster
                                    join CI in entities.ComplianceInstances
                                    on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                    where RRCM.RM_Status == "A"
                                    && CI.CustomerBranchID == custBranchID
                                    select new
                                    {
                                        ID = RRCM.ReturnID,
                                        Name = RRCM.RM_Name,
                                        State = RRCM.RM_StateID,
                                        Act = RRCM.RM_Act,
                                        Frequency = RRCM.RM_Frequency
                                    }).Distinct().ToList();

                        if (returns != null)
                        {
                            string state = (from CB in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                            where CB.AVACOM_BranchID == custBranchID
                                            select CB.CM_State).FirstOrDefault();

                            string[] arrReturnTypes = new string[] { "Central", state };

                            var statewisereturns = (from lst in returns.Where(x => arrReturnTypes.Contains(x.State))
                                                    select new
                                                    {
                                                        ID = lst.ID,
                                                        Name = lst.Name,
                                                        Act = lst.Act,
                                                        Frequency = lst.Frequency
                                                    }).Distinct().ToList();

                            if (!string.IsNullOrEmpty(selectedFreq))
                            {
                                statewisereturns = (from lst in statewisereturns
                                                    where lst.Frequency == selectedFreq
                                                    select new
                                                    {
                                                        ID = lst.ID,
                                                        Name = lst.Name,
                                                        Act = lst.Act,
                                                        Frequency = lst.Frequency
                                                    }).Distinct().ToList();
                            }

                            foreach (var statewisereturn in statewisereturns)
                            {
                                lstItems.Add(statewisereturn);
                            }
                        }
                    }

                    return lstItems;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<object> GetReturnChallanList(int branch, List<SP_RLCS_ReturnMaster_Result> lstReturnMaster, string selectedFreq)
        {
            try
            {
                List<object> lstItems = new List<object>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (branch > 0)
                    {
                        var Branches = RLCS_ComplianceManagement.GetBranchesForEntity(branch);
                        if (Branches != null)
                        {
                            //Branches.ForEach(eachRow =>
                            //{
                                var returns = (from RRCM in lstReturnMaster
                                               join CI in entities.ComplianceInstances
                                               on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                               where RRCM.RM_Status == "A"
                                               && Branches.Contains(CI.CustomerBranchID) 
                                               select new
                                               {
                                                   ID = RRCM.ReturnID,
                                                   Name = RRCM.RM_Name,
                                                   State = RRCM.RM_StateID,
                                                   Act = RRCM.RM_Act,
                                                   Frequency = RRCM.RM_Frequency
                                               }).Distinct().ToList();

                                if (returns != null)
                                {
                                    //string state = (from CB in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    //                where Branches.Contains(CB.AVACOM_BranchID.Value)
                                    //                select CB.CM_State).FirstOrDefault();

                                    //string[] arrReturnTypes = new string[] { "Central", state };

                                    //var statewisereturns = (from lst in returns.Where(x => arrReturnTypes.Contains(x.State))
                                    //                        select new
                                    //                        {
                                    //                            ID = lst.ID,
                                    //                            Name = lst.Name,
                                    //                            Act = lst.Act,
                                    //                            Frequency = lst.Frequency
                                    //                        }).Distinct().ToList();

                                    if (!string.IsNullOrEmpty(selectedFreq))
                                    {
                                       var statewisereturns = (from lst in returns
                                                            where lst.Frequency == selectedFreq
                                                            select new
                                                            {
                                                                ID = lst.ID,
                                                                Name = lst.Name,
                                                                Act = lst.Act,
                                                                Frequency = lst.Frequency
                                                            }).Distinct().ToList();
                                    

                                    foreach (var statewisereturn in statewisereturns)
                                    {
                                        lstItems.Add(statewisereturn);
                                    }
                                    
                                }
                                    if(lstItems.Count <=0)
                                {
                                    foreach (var r in returns)
                                    {
                                        lstItems.Add(r);
                                    }
                                }
                            }
                            
                        }
                    }
                }
                return lstItems;

            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<int> GetBranchesForEntity(int entity)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var branches = (from b in entities.CustomerBranches
                                    where b.ParentID == entity
                                    select b.ID).ToList();

                    return branches;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static SP_RLCS_RegisterReturnChallanCompliance_Result GetRegisterReturnIDComType(int customerID, int branchID, int actID, int complianceID)
        {
            SP_RLCS_RegisterReturnChallanCompliance_Result record = new SP_RLCS_RegisterReturnChallanCompliance_Result();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //Get State Code
                    var branchRecord = RLCS_ClientsManagement.GetStateLocationByBranch(branchID);

                    //Get ComType and Register,Return,ChallanID
                    if (branchRecord != null)
                    {
                        string stateCode = branchRecord.CM_State;

                        record = entities.SP_RLCS_RegisterReturnChallanCompliance().Where(row => row.ActID == actID
                                     && row.ID == complianceID
                                     && (row.StateID == stateCode || row.StateID == "Central")).FirstOrDefault();
                        
                    }
                }

                return record;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
    }
}
