﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class WebAPIUtility
    {
        public static bool TestWebAPIConnection(string requestURi)
        {
            try
            {
                System.Net.WebClient client = new System.Net.WebClient();
                string result = client.DownloadString(requestURi);

                return true;
            }
            catch (System.Net.WebException ex)
            {
                //do something here to make the site unusable, e.g:
                return false;
            }
        }

        public static string Invoke(string Method, string Uri, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            var authValue = "5E8E3D45-172D-4D58-8DF8-EB0B0E";
            cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));
            var _UserAgent = "f2UKmAm0KlI98bEYGGxEHQ==";
            // You can actually also set the User-Agent via a built-in property
            cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);
            // You get the following exception when trying to set the "Content-Type" header like this:
            // cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
            // "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "POSTJson":
                    {


                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }

        public static string POST_Corporate(string RequestURI, Model_CorporateClientMaster corpRecord)
        {
            try
            {
                string responseContent = string.Empty;
                string _ContentType = "application/json";

                HttpClient client = new HttpClient();
                int _TimeoutSec = 90;
                client.Timeout = new TimeSpan(0, 0, _TimeoutSec);

                client.BaseAddress = new Uri(RequestURI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));

                HttpResponseMessage response = client.PostAsJsonAsync(RequestURI, corpRecord).Result;

                if (response.IsSuccessStatusCode)
                    // get the rest/content of the response in a synchronous way
                    responseContent = response.Content.ReadAsStringAsync().Result;

                return responseContent;
            }
            catch(Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public static string POST_RLCS_Common<TypeOfValue>(string RequestURI, TypeOfValue objRecord)
        {
            try
            {
                string responseContent = string.Empty;
                string _ContentType = "application/json";

                HttpClient client = new HttpClient();
                int _TimeoutSec = 180;//90
                client.Timeout = new TimeSpan(0, 0, _TimeoutSec);

                client.BaseAddress = new Uri(RequestURI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));

                HttpResponseMessage response = client.PostAsJsonAsync(RequestURI, objRecord).Result;

                if (response.IsSuccessStatusCode)
                    // get the rest/content of the response in a synchronous way
                    responseContent = response.Content.ReadAsStringAsync().Result;

                return responseContent;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        //public static string PostJson(string Method, string Uri, ViewRegulatoryLocationWiseMonthlyTracker objRecord)
        //{
        //    HttpClient cl = new HttpClient();
        //    cl.BaseAddress = new Uri(Uri);

        //    int _TimeoutSec = 90;
        //    cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);

        //    string _ContentType = "application/json";
        //    cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));

        //    var authValue = "5E8E3D45-172D-4D58-8DF8-EB0B0E";
        //    cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));
        //    var _UserAgent = "f2UKmAm0KlI98bEYGGxEHQ==";
        //    // You can actually also set the User-Agent via a built-in property
        //    cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);

        //    // You get the following exception when trying to set the "Content-Type" header like this:
        //    // cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
        //    // "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

        //    HttpResponseMessage response;
        //    var _Method = new HttpMethod(Method);

        //    switch (_Method.ToString().ToUpper())
        //    {
        //        case "GET":
        //        case "HEAD":
        //            // synchronous request without the need for .ContinueWith() or await
        //            response = cl.GetAsync(Uri).Result;
        //            break;                
        //        case "POSTJSON":
        //            {
        //                response = cl.PostAsJsonAsync(Uri, objRecord).Result;                       
        //            }
        //            break;               
        //        case "DELETE":
        //            response = cl.DeleteAsync(Uri).Result;
        //            break;
        //        default:
        //            throw new NotImplementedException();
        //            break;
        //    }
        //    // either this - or check the status to retrieve more information
        //    string responseContent = string.Empty;

        //    if (response.IsSuccessStatusCode)
        //        // get the rest/content of the response in a synchronous way
        //        responseContent = response.Content.ReadAsStringAsync().Result;

        //    return responseContent;
        //}

        private static bool IsNetworkError(Exception ex)
        {
            // Check if it's a network error
            if (ex is SocketException)
                return true;
            if (ex.InnerException != null)
                return IsNetworkError(ex.InnerException);
            return false;
        }
    }
}
