﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class SAMLManagement
    {
        public static SAMLConfiguration CheckExistsIdentifierEntityID(string IDEntity)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var existsRecord = (from row in entities.SAMLConfigurations
                                        where row.IdentifierEntityID_idpid == IDEntity
                                        && row.IsActive == true
                                        select row).FirstOrDefault();

                    return existsRecord;
                }
            }
            catch(Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static SAMLConfiguration CheckCustomerExists(int custID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var existsRecord = (from row in entities.SAMLConfigurations
                                        where row.CustomerID == custID
                                        && row.IsActive == true
                                        select row).FirstOrDefault();

                    return existsRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

    }
}
