//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaskReminder
    {
        public int ID { get; set; }
        public System.DateTime TaskDueDate { get; set; }
        public System.DateTime RemindOn { get; set; }
        public Nullable<byte> Status { get; set; }
        public int TaskAssignmentID { get; set; }
        public Nullable<int> ReminderTemplateID { get; set; }
        public Nullable<long> ComplianceScheduleOnID { get; set; }
    
        public virtual ReminderTemplate ReminderTemplate { get; set; }
        public virtual TaskAssignment TaskAssignment { get; set; }
    }
}
