//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class LIC_PreviouslyLicenseDetails_Result
    {
        public long LicenseID { get; set; }
        public string LicensetypeName { get; set; }
        public string LicenseNo { get; set; }
        public string Licensetitle { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public long LicenseTypeID { get; set; }
        public long CustomerID { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public bool IsStatutory { get; set; }
        public Nullable<System.DateTime> ApplicationDate { get; set; }
        public bool IsPermanantActive { get; set; }
        public Nullable<decimal> Cost { get; set; }
    }
}
