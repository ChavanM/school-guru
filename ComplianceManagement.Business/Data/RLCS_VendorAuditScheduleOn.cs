//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RLCS_VendorAuditScheduleOn
    {
        public long ID { get; set; }
        public long AuditID { get; set; }
        public Nullable<System.DateTime> ScheduleOn { get; set; }
        public string ForMonth { get; set; }
        public Nullable<long> ForPeriod { get; set; }
        public long CustomerBranchID { get; set; }
        public bool IsActive { get; set; }
        public int AuditStatusID { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> ScheduleOnEndDate { get; set; }
        public Nullable<int> AuditorID { get; set; }
        public Nullable<int> VendorID { get; set; }
        public string PONO { get; set; }
        public string InVoiceNo { get; set; }
        public string Observation { get; set; }
        public string Recommendation { get; set; }
        public Nullable<bool> IsOffline { get; set; }
    }
}
