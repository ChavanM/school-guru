//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CancelledStatutoryComplianceAssignment
    {
        public long Id { get; set; }
        public Nullable<long> ComplianceInstanceID { get; set; }
        public Nullable<long> RoleID { get; set; }
        public Nullable<long> UserID { get; set; }
        public Nullable<long> ScheduleonID { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> ScheduledOn { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
    }
}
