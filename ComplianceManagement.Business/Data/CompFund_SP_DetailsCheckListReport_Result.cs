//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class CompFund_SP_DetailsCheckListReport_Result
    {
        public string SNO { get; set; }
        public string ResultStatus { get; set; }
        public string Remarks { get; set; }
        public long AuditID { get; set; }
        public int CustomerID { get; set; }
        public int CustomerBranchID { get; set; }
        public Nullable<System.DateTime> ScheduledOn { get; set; }
        public int RoleID { get; set; }
        public bool IsActive { get; set; }
        public long UserID { get; set; }
        public long ScheduledOnID { get; set; }
        public string Branch { get; set; }
        public string ForMonth { get; set; }
        public int ChecklistStatusID { get; set; }
        public string ChecklistStatus { get; set; }
        public int ResultStatusID { get; set; }
        public string Frequency { get; set; }
        public int Year { get; set; }
        public string Period { get; set; }
        public Nullable<long> InvestorID { get; set; }
        public string InvestorName { get; set; }
        public string Comp_Que { get; set; }
        public string Clause_ref_num { get; set; }
    }
}
