//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_GetInternalLicenseDocument_Result
    {
        public int ID { get; set; }
        public int FileID { get; set; }
        public long TransactionID { get; set; }
        public Nullable<long> ScheduledOnID { get; set; }
        public string FileName { get; set; }
        public Nullable<short> FileType { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
        public string Version { get; set; }
        public Nullable<System.DateTime> VersionDate { get; set; }
        public string VersionComment { get; set; }
        public bool IsDeleted { get; set; }
        public long LicenseID { get; set; }
        public string EnType { get; set; }
        public bool ISLink { get; set; }
    }
}
