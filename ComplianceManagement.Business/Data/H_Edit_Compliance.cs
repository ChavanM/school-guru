//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class H_Edit_Compliance
    {
        public int ID { get; set; }
        public long ComplianceID { get; set; }
        public long PrevComplianceInstanceID { get; set; }
        public System.DateTime EffectiveDate { get; set; }
        public int Createdby { get; set; }
        public System.DateTime CreatedOn { get; set; }
    }
}
