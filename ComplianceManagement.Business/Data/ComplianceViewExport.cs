//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ComplianceViewExport
    {
        public long ID { get; set; }
        public int ActID { get; set; }
        public string ActName { get; set; }
        public string Description { get; set; }
        public string Sections { get; set; }
        public string ComplianceType { get; set; }
        public string UploadDocument { get; set; }
        public string NatureOfCompliance { get; set; }
        public string RequiredForms { get; set; }
        public string Frequency { get; set; }
        public Nullable<int> DueDate { get; set; }
        public string RiskType { get; set; }
        public string NonComplianceType { get; set; }
        public string NonComplianceEffects { get; set; }
        public string ShortDescription { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ComplianceCategoryId { get; set; }
        public Nullable<int> ComplianceTypeId { get; set; }
        public string SubComplianceType { get; set; }
        public Nullable<int> FixedGap { get; set; }
        public string ReminderType { get; set; }
        public string Imprisonment { get; set; }
        public string Designation { get; set; }
        public Nullable<int> MinimumYears { get; set; }
        public Nullable<int> MaximumYears { get; set; }
        public string Others { get; set; }
        public Nullable<int> ReminderBefore { get; set; }
        public Nullable<int> ReminderGap { get; set; }
        public string VariableAmountPercentMax { get; set; }
        public Nullable<double> VariableAmountPerDay { get; set; }
        public Nullable<double> VariableAmountPerDayMax { get; set; }
        public string VariableAmountPercent { get; set; }
        public Nullable<double> FixedMaximum { get; set; }
        public Nullable<double> FixedMinimum { get; set; }
        public string VariableAmountPerMonth { get; set; }
        public string sampleform { get; set; }
        public string spdate { get; set; }
        public string calflag { get; set; }
        public string iseventbased { get; set; }
        public string EventName { get; set; }
        public string EventID { get; set; }
        public string SubEventName { get; set; }
        public string EventComplianceType { get; set; }
        public Nullable<long> SubEventID { get; set; }
        public string ComplianceCategory { get; set; }
        public string State { get; set; }
        public string Ctype { get; set; }
        public string ActionableOrInformative { get; set; }
        public string PenaltyDescription { get; set; }
        public string ReferenceMaterialText { get; set; }
        public string Name { get; set; }
        public string ShortForm { get; set; }
        public string SampleFormLink { get; set; }
        public string ModeofCompliance { get; set; }
        public string Authority { get; set; }
        public string ScheduleType { get; set; }
    }
}
