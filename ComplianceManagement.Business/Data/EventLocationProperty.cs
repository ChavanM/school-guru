﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    public class EventLocationProperty
    {
        private int eventID;
        private string branchIds;
        private string branchNames;

        public int EventID
        {
            get { return eventID; }
            set { eventID = value; }
        }

        public string BranchIds
        {
            get { return branchIds; }
            set { branchIds = value; }
        }

        public string BranchNames
        {
            get { return branchNames; }
            set { branchNames = value; }
        }

    }
}
