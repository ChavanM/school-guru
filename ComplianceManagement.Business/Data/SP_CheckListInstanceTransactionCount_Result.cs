//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_CheckListInstanceTransactionCount_Result
    {
        public long ComplianceInstanceID { get; set; }
        public Nullable<int> CheckListTypeID { get; set; }
        public int CustomerID { get; set; }
        public int CustomerBranchID { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public long ScheduledOnID { get; set; }
        public long ComplianceTransactionID { get; set; }
        public int ComplianceStatusID { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsUpcomingNotDeleted { get; set; }
    }
}
