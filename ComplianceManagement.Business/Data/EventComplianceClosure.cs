//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class EventComplianceClosure
    {
        public long ID { get; set; }
        public long EventScheduleOnID { get; set; }
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public System.DateTime FromDate { get; set; }
        public string Remark { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreateBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<long> ParentEventD { get; set; }
        public Nullable<long> IntermediateEventID { get; set; }
        public Nullable<long> SubEventID { get; set; }
    }
}
