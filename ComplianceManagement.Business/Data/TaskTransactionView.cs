//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaskTransactionView
    {
        public Nullable<int> CustomerBranchID { get; set; }
        public Nullable<long> TaskId { get; set; }
        public long TaskTransactionID { get; set; }
        public long TaskInstanceId { get; set; }
        public Nullable<long> TaskScheduleOnID { get; set; }
        public Nullable<long> ComplianceScheduleOnID { get; set; }
        public Nullable<int> ComplianceStatusID { get; set; }
        public string Status { get; set; }
        public Nullable<int> FileID { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByText { get; set; }
        public string Remarks { get; set; }
        public string FileName { get; set; }
        public System.DateTime Dated { get; set; }
        public Nullable<System.DateTime> StatusChangedOn { get; set; }
    }
}
