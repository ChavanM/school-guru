﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    public class NameValueHierarchy
    {
        public Nullable<long> RparentID { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public List<NameValueHierarchy> Children { get; set; }

        public NameValueHierarchy()
        {
            Children = new List<NameValueHierarchy>();
        }
    }
}
