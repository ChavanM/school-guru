//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RLCS_Client_Paycode_Mapping
    {
        public long ID { get; set; }
        public string CPM_ID { get; set; }
        public Nullable<int> AVACOM_CustomerID { get; set; }
        public string CPM_ClientID { get; set; }
        public string CPM_PayGroup { get; set; }
        public string CPM_CreatedBy { get; set; }
        public System.DateTime CPM_CreatedDate { get; set; }
        public string CPM_ModifiedBy { get; set; }
        public Nullable<System.DateTime> CPM_ModifiedDate { get; set; }
        public string CPM_Status { get; set; }
        public bool IsProcessed { get; set; }
    }
}
