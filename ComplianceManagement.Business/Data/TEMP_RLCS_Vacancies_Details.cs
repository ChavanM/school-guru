//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TEMP_RLCS_Vacancies_Details
    {
        public int VD_ID { get; set; }
        public string VD_ClientID { get; set; }
        public string VD_StateID { get; set; }
        public string VD_LocationID { get; set; }
        public string VD_BranchName { get; set; }
        public int VD_Year { get; set; }
        public int VD_VacanciesOccured { get; set; }
        public int VD_NoofVacanciesFilled { get; set; }
        public string VD_Source { get; set; }
        public string VD_CreatedBy { get; set; }
        public Nullable<System.DateTime> VD_CreatedDate { get; set; }
        public string VD_ModifiedBy { get; set; }
        public Nullable<System.DateTime> VD_ModifiedDate { get; set; }
        public Nullable<int> VD_Version { get; set; }
        public string VD_Quarter { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> fileid { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<System.DateTime> deleted_at { get; set; }
        public Nullable<int> AVACOM_CustomerID { get; set; }
        public Nullable<int> AVACOM_BranchID { get; set; }
        public Nullable<bool> ISProcessed { get; set; }
        public Nullable<bool> ISActive { get; set; }
        public Nullable<bool> ISValidated { get; set; }
        public Nullable<int> excel_row_no { get; set; }
    }
}
