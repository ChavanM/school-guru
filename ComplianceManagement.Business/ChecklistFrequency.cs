﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
   public class ChecklistFrequency
    {       
        public static int GetCompliance(long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.Compliances
                                      where row.ID == ComplianceID
                                      select (int)row.Frequency).FirstOrDefault();
                return eventMappedIDs;
            }
        }       
        public static List<NameValue> GetAllClientFrequency<EnumType>() where EnumType : struct
        {
            var enumerationType = typeof(EnumType);
            if (!enumerationType.IsEnum)
                throw new ArgumentException("Enumeration type is expected.");

            var dictionary = new List<NameValue>();

            foreach (var value in Enum.GetValues(enumerationType))
            {
                var memInfo = value.GetType().GetMember(value.ToString());
                var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                var description = attributes.Length > 0 ? ((DescriptionAttribute)attributes[0]).Value : value.ToString();

                dictionary.Add(new NameValue() { ID = (byte)value, Name = description });
            }


            if (enumerationType.Name == "Month" || enumerationType.Name.Equals("PerformanceSummaryForPerformer") || enumerationType.Name.Equals("CustomerStatus") || enumerationType.Name.Equals("CheckListReportFilterForPerformer"))
                return dictionary.ToList();
            else
                return dictionary.OrderBy(entry=>entry.Name).ToList();
        }
        public static List<object> GetAllAct()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts                            
                            where row.IsDeleted == false                             
                            orderby row.Name ascending
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();            
                return acts;
            }
        }        
        public static List<object> GetShortdescription(long ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var description = (from row in entities.Compliances
                                   where row.IsDeleted == false && row.ComplianceType == 1
                                   && row.CheckListTypeID !=0
                                   && row.Status == null && row.Frequency != null
                                   && row.ActID == ActID
                                   orderby row.ShortDescription ascending
                                   select new { ID = row.ID, ShortDescription = row.ShortDescription }).OrderBy(entry => entry.ShortDescription).ToList<object>();


                return description;
            }
        }

    }
}
