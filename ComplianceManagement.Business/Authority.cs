﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class Authority
    {
        public static void CreateAuthority(Authorityofthestate objAuth)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Authorityofthestates.Add(objAuth);
                entities.SaveChanges();
            }
        }
        public static void UpdateAuthoritys(Authorityofthestate objAuth)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Authorityofthestate objAuths = (from row in entities.Authorityofthestates
                                         where row.Id == objAuth.Id
                                          
                                         select row).FirstOrDefault();

                objAuths.Name = objAuth.Name;
                objAuths.Designation = objAuth.Designation;
      
                entities.SaveChanges();
            }
        }
        public static Authorityofthestate GetAuthority(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Objcase = (from row in entities.Authorityofthestates
                               where row.Id == ID
                             
                               select row).SingleOrDefault();
                return Objcase;
            }
        }

        public static List<Authorityofthestate> GetAllAuthority(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Objcase = (from row in entities.Authorityofthestates
                               where row.CustomerId == CustomerID
                               select row).ToList();
                return Objcase;
            }
        }
        public static void DeleteAuthority(int ID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Authorityofthestate objCase = (from row in entities.Authorityofthestates
                                           where row.Id == ID
                                           && row.CustomerId == CustomerID
                                         
                                           select row).FirstOrDefault();


                entities.Authorityofthestates.Remove(objCase);
                entities.SaveChanges();
            }
        }
    }
}
