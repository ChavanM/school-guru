﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using EntityFramework.BulkInsert.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class CityStateContryManagementRisk
    {
        public static bool CityExist(string name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_City
                             where row.Name.Equals(name)
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateCity(mst_City objCity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_City.Add(objCity);
                entities.SaveChanges();
            }
        }

        public static void UpdateCity(mst_City objCity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_City mstdeptMasterToUpdate = (from row in entities.mst_City
                                                  where row.ID == objCity.ID
                                                  select row).FirstOrDefault();

                mstdeptMasterToUpdate.Name = objCity.Name;
                mstdeptMasterToUpdate.StateId = objCity.StateId;
                entities.SaveChanges();
            }
        }

        public static void DeleteCity(int cityID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_City CityDelete = (from row in entities.mst_City
                                       where row.ID == cityID
                                       select row).FirstOrDefault();

                entities.mst_City.Remove(CityDelete);
                entities.SaveChanges();
            }
        }

        //State

        public static bool StateExist(string name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_State
                             where row.Name.Equals(name)
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateState(mst_State objState)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_State.Add(objState);
                entities.SaveChanges();
            }
        }

        public static void UpdateState(mst_State objState)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_State updatestates = (from row in entities.mst_State
                                          where row.ID == objState.ID
                                          select row).FirstOrDefault();

                updatestates.Name = objState.Name;
                updatestates.CountryID = objState.CountryID;
                entities.SaveChanges();
            }
        }

        public static void DeleteState(int iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_State StateDalete = (from row in entities.mst_State
                                         where row.ID == iD
                                         select row).FirstOrDefault();
                StateDalete.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        public static void DeleteCountryDetail(int ID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Country ObjCountry = (from row in entities.mst_Country
                                      where row.ID == ID
                                      select row).FirstOrDefault();

                ObjCountry.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        public static bool CheckCountryExist(string name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ObjCountry = (from row in entities.mst_Country
                                  where row.Name.Equals(name)
                                  select row);
                return ObjCountry.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateContryDetails(mst_Country objCountry)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_Country.Add(objCountry);
                entities.SaveChanges();
            }
        }

        public static void UpdateCountryDetails(mst_Country objCountry)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Country updatestates = (from row in entities.mst_Country
                                            where row.ID == objCountry.ID
                                        select row).FirstOrDefault();

                updatestates.Name = objCountry.Name;
                entities.SaveChanges();
            }
        }

    }
}
