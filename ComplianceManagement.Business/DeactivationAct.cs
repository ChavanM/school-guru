﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class DeactivationAct
    {
        public static long GetActComplianceCount(int Deactivateactid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCount = (from row in entities.Compliances
                                       where row.ActID == Deactivateactid
                                       && row.IsDeleted == false
                                       && row.Status == null
                                       select row).ToList();

                return complianceCount.Count();
            }
        }

        public static long GetCustomerCount(int Deactivateactid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCount = (from row in entities.Compliances
                                       join row1 in entities.ComplianceInstances
                                       on row.ID equals row1.ComplianceId
                                       join row4 in entities.ComplianceAssignments
                                       on row1.ID equals row4.ComplianceInstanceID
                                       join row2 in entities.CustomerBranches
                                       on row1.CustomerBranchID equals row2.ID
                                       join row3 in entities.Customers
                                       on row2.CustomerID equals row3.ID
                                       where row.ActID == Deactivateactid
                                       && row.IsDeleted == false
                                       && row4.RoleID == 3
                                       //&& row3.ServiceProviderID ==95
                                       && row3.ComplianceProductType !=1
                                       select row3.ID).Distinct().ToList();

                return complianceCount.Count();
            }
        }

        public static bool CheckActDeactivationRequest(long ActId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.Act_Deactivation
                                      where row.Old_Act_ID == ActId
                                      select row).FirstOrDefault();
                if (ComplianceData != null)
                {
                    if (ComplianceData.Status == "A" || ComplianceData.Status == "D")
                    {
                        return false;
                    }
                    else if(ComplianceData.Status == "R")
                    {
                        return true;
                    }                       
                    else
                        return true;
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool ExistsMappingRLCSorHRProduct(string type,long actorComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (type.Equals("A"))
                {
                    var existsActMapping = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance()
                                            where row.ActID == actorComplianceID
                                            select row).FirstOrDefault();
                    if (existsActMapping != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    var existsComplianceMapping = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance()
                                                   where row.ID == actorComplianceID
                                                   select row).FirstOrDefault();
                    if (existsComplianceMapping != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public static void RejectActStatus(int ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Act_Deactivation complianceStatus = (from row in entities.Act_Deactivation
                                               where row.Old_Act_ID == ActID
                                               select row).FirstOrDefault();

                complianceStatus.Status = "R";
                entities.SaveChanges();
            }
        }
        public static void ChangeActStatus(int actID, DateTime? DeactiveDate, string Status, string Desc)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Act_Deactivation complianceStatus = (from row in entities.Act_Deactivation
                                               where row.Old_Act_ID == actID
                                               select row).FirstOrDefault();

                complianceStatus.Status = Status;
                complianceStatus.DeactivationDate = DeactiveDate;
                complianceStatus.DeactivationDescription = Desc;
                entities.SaveChanges();
            }
        }
        public static List<Compliance> GetCompliance(int ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Compliances
                            where row.ActID == ActID && 
                            row.IsDeleted ==false
                            && row.Status == null
                            select row).ToList();
                return acts;
            }
        }
        public static Compliance GetByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.Compliances
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }

        public static bool Exists(ClientBasedCheckListFrequency ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ClientBasedCheckListFrequencies
                             where row.IsDeleted == false
                             && row.ClientID == ltlm.ClientID
                             && row.ComplianceID == ltlm.ComplianceID
                             select row);

                if (ltlm.ID > 0)
                {
                    query = query.Where(entry => entry.ID != ltlm.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static List<object> GetAllAct()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts
                            join row1 in entities.Compliances
                            on row.ID equals row1.ActID
                            where row.IsDeleted == false
                            && row.IsDeleted == false
                            select new { ID = row.ID, Name = row.Name }).Distinct().OrderBy(entry => entry.Name).ToList<object>();

                return acts;
            }
        }

        public static long GetByOLDAssignedClient(long Deactivateactid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from CI in entities.ComplianceInstances
                             join CA in entities.ComplianceAssignments
                             on CI.ID equals CA.ComplianceInstanceID
                             join C in entities.Compliances
                             on CI.ComplianceId equals C.ID                             
                             join CB in entities.CustomerBranches
                             on CI.CustomerBranchID equals CB.ID                             
                             where C.ActID == Deactivateactid
                             && C.IsDeleted==false
                             && C.Status == null
                             && CB.IsDeleted==false
                             && CI.IsDeleted==false
                             select CB.CustomerID).ToList();
                return query.Count();
            }
        }

        public static long GetByNewAssignedClient(long Altenateactid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from CI in entities.ComplianceInstances
                             join CA in entities.ComplianceAssignments
                             on CI.ID equals CA.ComplianceInstanceID
                             join C in entities.Compliances
                             on CI.ComplianceId equals C.ID
                             join CB in entities.CustomerBranches
                             on CI.CustomerBranchID equals CB.ID
                             where C.ActID == Altenateactid
                             && C.IsDeleted == false
                             && C.Status == null
                             && CB.IsDeleted == false
                             && CI.IsDeleted == false
                             select CB.CustomerID).ToList();
                return query.Count();
            }
        }

       

        public static long GetByActID(long Deactivateactid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCount = (from row in entities.Compliances
                               where row.ActID == Deactivateactid
                                       select row).ToList();
                
                return complianceCount.Count();
            }
        }
       
        public static List<ComplianceType> GetAllComplianceType(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTypes = (from row in entities.ComplianceTypes
                                           //where row.IsDeleted == false
                                       select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceTypes = complianceTypes.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceTypes = complianceTypes.OrderBy(entry => entry.Name);

                return complianceTypes.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<ComplianceCategory> GetAllCatagory(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.ComplianceCategories
                                               //where row.IsDeleted == false
                                           select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
                
            }
        }

        public static List<object> GetFilterCatagory()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var catagory = (from row in entities.ComplianceCategories
                                select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return catagory;
            }
        }

        public static List<City> GetAllCitiesByState(int stateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var cities = (from row in entities.Cities
                              where row.StateId == stateID
                              orderby row.Name ascending
                              select row).ToList();

                return cities.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<State> GetAllStates()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var states = (from row in entities.States
                              orderby row.Name ascending
                              select row).ToList();

                return states.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<Industry> GetAllIndustry()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.Industries;

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static object GetAllActDepartment()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_Department
                             where row.IsDeleted == false
                             select new { row.ID, row.Name }).ToList();

                return query;
            }
        }

        public static object GetAllMinistry()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Ministries
                             where row.IsDeleted == false
                             select new { row.ID, row.Name }).ToList();

                return query;
            }
        }

        public static object GetAllRegulator()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Regulators
                             where row.IsDeleted == false
                             select new { row.ID, row.Name }).ToList();

                return query;
            }
        }

       
        public static ComplianceType GetByID(int complianceTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceType = (from row in entities.ComplianceTypes
                                      where row.ID == complianceTypeID
                                      select row).SingleOrDefault();

                return complianceType;
            }
        }
        public static bool AssignmentExists(long CustomerID, long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from CI in entities.ComplianceInstances
                             join CA in entities.ComplianceAssignments
                             on CI.ID equals CA.ComplianceInstanceID
                             join CO in entities.Compliances
                             on CI.ComplianceId equals CO.ID
                             join CB in entities.CustomerBranches
                             on CI.CustomerBranchID equals CB.ID
                             join C in entities.Customers
                             on CB.CustomerID equals C.ID
                             where CI.IsDeleted == false
                             && CO.ID == complianceID
                             && CA.RoleID == 3
                             && CB.CustomerID == CustomerID
                             select CI).ToList();

                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void Delete(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance complianceToDelete = (from row in entities.Compliances
                                                 join row1 in entities.Acts
                                                 on row.ActID equals row1.ID
                                                 where row.ID == complianceID
                                                 select row).FirstOrDefault();

                complianceToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }

        public static void ChangeStatus(long complianceID, DateTime? DeactiveDate, string Status, string Desc)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance complianceStatus = (from row in entities.Compliances
                                               where row.ID == complianceID
                                               select row).FirstOrDefault();

                complianceStatus.Status = Status;
                complianceStatus.DeactivateOn = DeactiveDate;
                complianceStatus.DeactivateDesc = Desc;
                entities.SaveChanges();
            }
        }

        public static void UpdateComplianceDate(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance complianceStatus = (from row in entities.Compliances
                                               where row.ID == complianceID
                                               select row).FirstOrDefault();

                complianceStatus.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }


        public static void RemoveUpcomingSchudule(int complianceID, DateTime deactiveDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance compliance = (from row in entities.Compliances
                                         where row.ID == complianceID
                                         select row).FirstOrDefault();

                //if (ComplianceIsEventBased(complianceID))
                //{
                var complianceInstances = (from row in entities.ComplianceInstances
                                           where row.IsDeleted == false && row.ComplianceId == complianceID
                                           select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                //ComplianceInstances
                complianceInstances.ForEach(complianceInstancesentry =>
                {
                    var complianceScheduleOns = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstancesentry.ID && row.ScheduleOn >= deactiveDate
                                                 select row).ToList();
                    //ComplianceScheduleOns
                    complianceScheduleOns.ForEach(complianceScheduleOnssentry =>
                    {
                        var complianceAssignments = (from row in entities.ComplianceAssignments
                                                     where row.ComplianceInstanceID == complianceScheduleOnssentry.ComplianceInstanceID
                                                     select row).ToList();

                        var complianceTransactions = (from row in entities.ComplianceTransactions
                                                      where row.ComplianceInstanceId == complianceScheduleOnssentry.ComplianceInstanceID
                                                      && row.ComplianceScheduleOnID == complianceScheduleOnssentry.ID //&& row.StatusId == 1
                                                      select row).ToList();

                        if (complianceTransactions.Count < 2)
                        {

                            //Remove ComplianceTransactions
                            complianceTransactions.ForEach(entry =>
                            entities.ComplianceTransactions.Remove(entry));
                            entities.SaveChanges();

                            var taskTransactions = (from row in entities.TaskTransactions
                                                    where row.ComplianceScheduleOnID == complianceScheduleOnssentry.ID
                                                    select row).ToList();

                            var taskReminders = (from row in entities.TaskReminders
                                                 where row.ComplianceScheduleOnID == complianceScheduleOnssentry.ID
                                                 && row.TaskDueDate >= deactiveDate
                                                 select row).ToList();

                            if (taskTransactions.Count < 2)
                            {
                                //Remove TaskTransactions
                                taskTransactions.ForEach(taskentry =>
                                entities.TaskTransactions.Remove(taskentry));
                                entities.SaveChanges();

                                //Remove TaskReminders
                                taskReminders.ForEach(taskReminderentry =>
                                entities.TaskReminders.Remove(taskReminderentry));
                                entities.SaveChanges();
                            }

                            complianceAssignments.ForEach(complianceAssignmentsentry =>
                            {
                                var complianceReminders = (from row in entities.ComplianceReminders
                                                           where row.ComplianceAssignmentID == complianceAssignmentsentry.ID
                                                           && row.ComplianceDueDate >= deactiveDate
                                                           select row).ToList();
                                //Remove ComplianceReminders
                                complianceReminders.ForEach(entry =>
                                    entities.ComplianceReminders.Remove(entry));
                                entities.SaveChanges();
                            });
                            //Remove ComplianceScheduleOns
                            entities.ComplianceScheduleOns.Remove(complianceScheduleOnssentry);
                            entities.SaveChanges();
                        }
                    });
                });
                //}

            }
        }

    }
}
