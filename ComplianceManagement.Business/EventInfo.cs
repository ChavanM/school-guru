﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public partial class InternalComForm
    {
        public int ID { get; set; }
        public long ComplianceID { get; set; }
        public string Name { get; set; }
     
    }
    public class EventInfo
    {
        public Nullable<long> EventID { get; set; }
        public string Name { get; set; }
        public Nullable<long> ParentID { get; set; }
        public long SubEventID { get; set; }   
    }
}

