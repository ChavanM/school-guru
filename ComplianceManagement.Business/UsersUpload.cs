﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class UsersUpload
    {
        public static List<Customer> GetClient()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             where row.IsDeleted == false && row.ServiceProviderID == 95
                             && row.Status == 1
                             select row);
                return users.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static object GetAllCustomer(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             join row1 in entities.CustomerAssignmentDetails
                             on row.ID equals row1.CustomerID
                             where row1.UserID == userID
                             && row.IsDeleted == false
                             && row1.IsDeleted == false
                              && row.Status == 1
                             select row);
                return users.OrderBy(entry => entry.Name).ToList(); 
            }
        }

        public static bool Exists(string customerBranchName, int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.Name.Equals(customerBranchName) && row.CustomerID == CustomerId
                             select row);
                
                return query.Select(entry => true).FirstOrDefault();
            }
        }
        #region[create user]
        public static int CreateCompliance(User user)
        {
            int Addid = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID)).Status;
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.UtcNow;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.UtcNow;
                        user.EnType = "A";
                        user.ChangePasswordFlag = true;
                        entities.Users.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                        dbtransaction.Commit();
                        return Addid;
                    }
                    catch (Exception ex)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDUser(Addid);
                        }
                        return Addid = 0;
                    }
                }
            }
        }
        #endregion

        #region[create audit user]
        public static bool CreateAuditUser(mst_User user)
        {
            int Addid = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagementRisk.GetByID(Convert.ToInt32(user.CustomerID)).Status;

                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.UtcNow;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.UtcNow;
                        user.EnType = "A";
                        user.ChangePasswordFlag = true;
                        entities.mst_User.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDMstUser(Addid);
                        }
                        return false;
                    }
                }
            }
        }

        #endregion

        #region[Create Branch Compliance]
        public static int CreateBranchCompliance(CustomerBranch customerBranch)
        {
            int Addid = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagement.GetByID(Convert.ToInt32(customerBranch.CustomerID)).Status;
                        customerBranch.IsDeleted = false;
                        customerBranch.CreatedOn = DateTime.UtcNow;
                        customerBranch.AuditPR = false;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            customerBranch.Status = Convert.ToInt32(true);
                        }
                        else
                        {
                            if (customerBranch.ID == 2)
                                customerBranch.Status = Convert.ToInt32(true);
                            else
                                customerBranch.Status = Convert.ToInt32(false);
                        }                      
                        entities.CustomerBranches.Add(customerBranch);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(customerBranch.ID);
                        dbtransaction.Commit();
                        return Addid;
                    }
                    catch (Exception ex)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDCustomerBranch(Addid);
                        }
                        return Addid = 0;
                    }
                }
            }
        }

        #endregion

        #region[Create Branch AuditUser]
        public static bool CreateBranchAuditUser(mst_CustomerBranch customerBranch)
        {
            int Addid = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagementRisk.GetByID(Convert.ToInt32(customerBranch.CustomerID)).Status;

                        customerBranch.IsDeleted = false;
                        customerBranch.CreatedOn = DateTime.UtcNow;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            customerBranch.Status = Convert.ToInt32(true);
                        }
                        else
                        {
                            if (customerBranch.ID == 2)
                                customerBranch.Status = Convert.ToInt32(true);
                            else
                                customerBranch.Status = Convert.ToInt32(true);
                        }                       
                        entities.mst_CustomerBranch.Add(customerBranch);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(customerBranch.ID);
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDmstCustomerBranch(Addid);
                        }
                        return false;
                    }
                }
            }
        }
       #endregion

        #region[Methods]        
     
        public static void CreateDepartmentMapping(List<DepartmentMapping> deptMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var item in deptMapping)
                {
                    var DeptIDs = (from row in entities.DepartmentMappings
                                   where row.UserID == item.UserID
                                   && row.DepartmentID == item.DepartmentID
                                   select row).FirstOrDefault();

                    if (DeptIDs == null)
                    {
                        entities.DepartmentMappings.Add(item);
                        entities.SaveChanges();
                    }
                    else
                    {
                        if (!DeptIDs.IsActive)
                        {
                            DeptIDs.IsActive = true;
                            DeptIDs.UpdatedBy = item.CreatedBy;
                            DeptIDs.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                    }
                }
            }
        }
        public static int GetOwnerUserIDByEmail(List<User> lstUsers, int customerID, string email)
        {
            var UserID = (from row in lstUsers
                          where row.Email.Trim().ToUpper().Equals(email.Trim().ToUpper())
                          && row.CustomerID == customerID
                          && row.IsDeleted == false
                          && row.IsActive == true
                          select row.ID).FirstOrDefault();

            return Convert.ToInt32(UserID);
        }
        public static int GetRoleIDByCode(List<Role> lstUsers, string rcode)
        {
            var UserID = (from row in lstUsers
                          where row.Code.Trim().ToUpper().Equals(rcode.Trim().ToUpper())
                          select row.ID).FirstOrDefault();

            return Convert.ToInt32(UserID);
        }
        public static int GetBranchIDByName(List<CustomerBranch> lstCustomerBranches, int customerID, string branchName)
        {
            var EntityLocationBranchID = (from row in lstCustomerBranches
                                          where row.Name.Trim().ToUpper().Equals(branchName.Trim().ToUpper())
                                          && row.IsDeleted == false
                                          && row.Status == 1
                                          && row.CustomerID == customerID
                                          select row.ID).FirstOrDefault();

            return Convert.ToInt32(EntityLocationBranchID);
        }
        public static int GetDepartmentIDByName(List<Department> lstDepts, string deptName, int customerID)
        {
            var DepartmentID = (from row in lstDepts
                                where row.Name.Trim().ToUpper().Equals(deptName.Trim().ToUpper())
                                && row.IsDeleted == false
                                //&& row.CustomerID == customerID
                                select row.ID).FirstOrDefault();

            return Convert.ToInt32(DepartmentID);
        }
        public static int GetSubEntityType(List<NodeType> lstLocationName, string SubEntityType)
        {
            var UserID = (from row in lstLocationName
                          where row.Name.Trim().ToUpper().Equals(SubEntityType.Trim().ToUpper())
                          select row.ID).FirstOrDefault();

            return Convert.ToInt32(UserID);
        }
        public static int GetIndustryIDByName(List<Industry> lstIndustry, string Name, int customerID)
        {
            var IndustryID = (from row in lstIndustry
                              where row.Name.Trim().ToUpper().Equals(Name.Trim().ToUpper())
                              select row.ID).FirstOrDefault();
            return Convert.ToInt32(IndustryID);
        }
        public static int GetLocationName(List<M_LegalEntityType> lstMLegalEntityType, string Location)
        {
            var LocationID = (from row in lstMLegalEntityType
                              where row.EntityTypeName.Trim().ToUpper().Equals(Location.Trim().ToUpper())
                              select row.ID).FirstOrDefault();
            return Convert.ToInt32(LocationID);
        }
        public static int GetType(List<CompanyType> lstCompanyType, string Type)
        {
            var TypeID = (from row in lstCompanyType
                          where row.Name.Trim().ToUpper().Equals(Type.Trim().ToUpper())
                          select row.ID).FirstOrDefault();

            return Convert.ToInt32(TypeID);
        }
        public static int GetStateID(List<State> lstState, string StateName)
        {
            var StateID = (from row in lstState
                           where row.Name.Trim().ToUpper().Equals(StateName.Trim().ToUpper())
                     && row.IsDeleted == false
                           select row.ID).FirstOrDefault();

            return Convert.ToInt32(StateID);
        }
        public static int GetCityID(List<City> lstCity, string CityName)
        {

            var CityID = (from row in lstCity
                          where row.Name.Trim().ToUpper().Equals(CityName.Trim().ToUpper())
                                             && row.IsDeleted == false
                          select row.ID).FirstOrDefault();
            return Convert.ToInt32(CityID);

        }
        public static int GetParentID(List<CustomerBranch> lstparentID, string ParentName)
        {

            var parentID = (from row in lstparentID
                            where row.Name.Trim().ToUpper().Equals(ParentName.Trim().ToUpper())
                            select row.ID).FirstOrDefault();

            return Convert.ToInt32(parentID);



        }

        public static int GetParentID(long CustomerID, string ParentName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var parentID = (from row in entities.CustomerBranches
                                where row.Name.Trim().ToUpper().Equals(ParentName.Trim().ToUpper())
                                && row.CustomerID == CustomerID
                                && row.IsDeleted == false
                                select row.ID).FirstOrDefault();

                return Convert.ToInt32(parentID);

            }

        }
        public static void CreateIndustryMapping(List<CustomerBranchIndustryMapping> IndMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var item in IndMapping)
                {
                    var IndIDs = (from row in entities.CustomerBranchIndustryMappings
                                  where row.CustomerBranchID == item.CustomerBranchID
                                   && row.IndustryID == item.IndustryID
                                  select row).FirstOrDefault();

                    if (IndIDs == null)
                    {
                        entities.CustomerBranchIndustryMappings.Add(item);
                        entities.SaveChanges();
                    }
                    else
                    {
                        if (!IndIDs.IsActive)
                        {
                            IndIDs.IsActive = true;
                            IndIDs.EditedBy = item.EditedBy;
                            IndIDs.EditedDate =DateTime.Now;
                            entities.SaveChanges();
                        }
                    }
                }
            }
        }

        #endregion



        public static bool IsValidPhoneNumber(string phoneNumber)
        {
            return Regex.Match(phoneNumber, @"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$").Success;
        }

    }
}




      
