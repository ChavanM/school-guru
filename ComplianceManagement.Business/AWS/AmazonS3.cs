﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using System.IO;
using Amazon.S3.Model;
using Amazon.S3.IO;
using System.Web;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business.AWS
{
    public class AmazonS3
    {
        // Specify your bucket region (an example region is shown).
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USWest2;
        private static IAmazonS3 s3Client;

        public static void SaveDocFilesAWSStorage(List<KeyValuePair<string, Byte[]>> filesList, string DirectoryPath,long ScheduleOnID,string bucketName,string awsAccessKey,string awsSecretKey)
        {
            try
            {
                var TempDocData = Business.ComplianceManagement.GetTempComplianceDocumentDataAWS(ScheduleOnID);
                if (TempDocData.Count > 0)
                {
                    IAmazonS3 client = new AmazonS3Client(awsAccessKey, awsSecretKey, RegionEndpoint.APSouth1);
                    S3DirectoryInfo di = new S3DirectoryInfo(client, bucketName, DirectoryPath);
                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    foreach (var item in TempDocData)
                    {
                        string path = System.Web.HttpContext.Current.Server.MapPath(item.DocPath);
                        FileInfo localFile = new FileInfo(path);

                        string AWSpath = "";
                        if (item.DocType == "S" || item.DocType == "C")
                        {
                            AWSpath = DirectoryPath + "\\ComplianceDoc_" + item.DocName;
                        }
                        else
                        {
                            AWSpath = DirectoryPath + "\\WorkingFiles_" + item.DocName;
                        }
                        //CryptographyHandler.AESEncrypt(item.DocData);
                        S3FileInfo s3File = new S3FileInfo(client, bucketName, AWSpath);
                        if (!s3File.Exists)
                        {
                            using (var s3Stream = s3File.Create()) // <-- create file in S3  
                            {
                                //localFile.Encrypt();
                                localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public static void SaveDocFilesAWSStorageInternal(List<KeyValuePair<string, Byte[]>> filesList, string DirectoryPath, long ScheduleOnID, string bucketName, string awsAccessKey, string awsSecretKey)
        {
            try
            {
                var TempDocData = Business.ComplianceManagement.GetTempInternalComplianceDocumentDataAWS(ScheduleOnID);
                if (TempDocData.Count > 0)
                {
                    IAmazonS3 client = new AmazonS3Client(awsAccessKey, awsSecretKey, RegionEndpoint.APSouth1);
                    S3DirectoryInfo di = new S3DirectoryInfo(client, bucketName, DirectoryPath);
                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    foreach (var item in TempDocData)
                    {
                        string path = System.Web.HttpContext.Current.Server.MapPath(item.DocPath);
                        FileInfo localFile = new FileInfo(path);

                        string AWSpath = "";
                        if (item.DocType == "IS" || item.DocType == "IC")
                        {
                            AWSpath = DirectoryPath + "\\InternalComplianceDoc_" + item.DocName;
                        }
                        else
                        {
                            AWSpath = DirectoryPath + "\\InternalWorkingFiles_" + item.DocName;
                        }
                        //CryptographyHandler.AESEncrypt(item.DocData);
                        S3FileInfo s3File = new S3FileInfo(client, bucketName, AWSpath);
                        if (!s3File.Exists)
                        {
                            using (var s3Stream = s3File.Create()) // <-- create file in S3  
                            {
                                //localFile.Encrypt();
                                localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public static void SaveDocFilesAWSStorageTask(List<KeyValuePair<string, Byte[]>> filesList, string DirectoryPath, long ScheduleOnID, string bucketName, string awsAccessKey, string awsSecretKey)
        {
            try
            {
                var TempDocData = Business.ComplianceManagement.GetTempTaskDocumentDataAWS(ScheduleOnID);
                if (TempDocData.Count > 0)
                {
                    IAmazonS3 client = new AmazonS3Client(awsAccessKey, awsSecretKey, RegionEndpoint.APSouth1);
                    S3DirectoryInfo di = new S3DirectoryInfo(client, bucketName, DirectoryPath);
                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    foreach (var item in TempDocData)
                    {
                        string path = System.Web.HttpContext.Current.Server.MapPath(item.DocPath);
                        FileInfo localFile = new FileInfo(path);

                        string AWSpath = "";
                        if (item.DocType == "T")
                        {
                            AWSpath = DirectoryPath + "\\TaskDoc_" + item.DocName;
                        }
                        
                        S3FileInfo s3File = new S3FileInfo(client, bucketName, AWSpath);
                        if (!s3File.Exists)
                        {
                            using (var s3Stream = s3File.Create()) // <-- create file in S3  
                            {
                                //localFile.Encrypt();
                                localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public static Client_StorageDetail GetAWSStorageDetail(long CustId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.Client_StorageDetail
                                    where row.CustomerID == CustId
                                    select row).FirstOrDefault();

                return Data;
            }
        }
    }
}
