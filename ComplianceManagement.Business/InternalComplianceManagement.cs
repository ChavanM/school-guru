﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.Reflection;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class InternalComplianceManagement
    {
        public static bool GetLogDetails(int ComplianceID, int ComplianceInstanceID, int SID)
        {
            bool ans = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceScheduledOn obj1 = (from row in entities.InternalComplianceScheduledOns
                                                      where row.ID == SID
                                                      && row.IsActive == true
                                                      && row.InternalComplianceInstanceID == ComplianceInstanceID
                                                      select row).FirstOrDefault();
                if (obj1 != null)
                {
                    DateTime ScheduledOndt = Convert.ToDateTime(obj1.ScheduledOn);

                    var details = (from row in entities.CancelledInternalLogDetails
                                   where row.ComplianceInstanceID == ComplianceInstanceID
                                   && row.IsForDocument == true
                                   && row.IsActive == true
                                   select row).ToList();
                    if (details.Count > 0)
                    {
                        foreach (var item in details)
                        {
                            if (ScheduledOndt >= item.Statuschanged)
                            {
                                if (item.IsPerformer)
                                {
                                    ans = true;
                                }
                                else
                                {
                                    ans = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        InternalCompliance obj = (from row in entities.InternalCompliances
                                                  where row.ID == ComplianceID
                                                  && row.IsDeleted == false
                                                  select row).FirstOrDefault();

                        if (obj != null)
                        {
                            if (obj.IsDocumentRequired != null && obj.IsDocumentRequired == true)
                            {
                                ans = true;
                            }
                        }
                    }
                }
            }
            return ans;
        }
        public static void BindInternalStatutoryCheckListDatainRedis(long InternalComplianceInstanceID, int Customerid)
        {
            try
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (true)
                    {
                        var statusList = (from row in entities.InternalComplianceAssignments
                                          where row.InternalComplianceInstanceID == InternalComplianceInstanceID
                                          select row).ToList();
                        if (statusList.Count > 0)
                        {
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetInternalCheckListData_" + userID + "_" + Customerid + "_PRA_3";
                                }
                                else
                                {
                                    CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_PRA_3";
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetInternalCheckListData_" + userID + "_" + Customerid + "_REV_3";
                                }
                                else
                                {
                                    CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_REV_3";
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Select(y => y.UserID).FirstOrDefault());
                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetInternalCheckListData_" + userID + "_" + Customerid + "_APPR_3";
                                }
                                else
                                {
                                    CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_APPR_3";
                                }
                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                        }

                        var UserList = (from row in entities.InternalComplianceInstances
                                        join row1 in entities.InternalCompliances
                                        on row.InternalComplianceID equals row1.ID
                                        join row3 in entities.EntitiesAssignmentInternals
                                        on (long)row1.IComplianceCategoryID equals row3.ComplianceCatagoryID
                                        join row4 in entities.Users
                                        on row3.UserID equals row4.ID
                                        where row.ID == InternalComplianceInstanceID && row1.IsDeleted == false
                                        && row4.IsDeleted == false
                                         && row4.IsActive == true && row4.RoleID == 8
                                        select row3.UserID).ToList();

                        UserList = UserList.Distinct().ToList();

                        foreach (var item in UserList)
                        {
                            string CacheName = string.Empty;

                            if (objlocal == "Local")
                            {
                                CacheName = "Cache_GetInternalCheckListData_" + item + "_" + Customerid + "_MGMT_3";
                            }
                            else
                            {
                                CacheName = "CacheGetInternalCheckListData_" + item + "_" + Customerid + "_MGMT_3";
                            }
                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                {
                                    StackExchangeRedisExtensions.Remove(CacheName);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void BindInternalDatainRedis(long InternalComplianceInstanceID, int Customerid)
        {
            try
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string type = string.Empty;
                    type = "Internal";

                    if (!string.IsNullOrEmpty(type))
                    {
                        var statusList = (from row in entities.InternalComplianceAssignments
                                          where row.InternalComplianceInstanceID == InternalComplianceInstanceID
                                          select row).ToList();
                        if (statusList.Count > 0)
                        {
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetInternalData_" + userID + "_" + Customerid + "_PRA_" + type;
                                }
                                else
                                {
                                    CacheName = "CacheGetInternalData_" + userID + "_" + Customerid + "_PRA_" + type;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetInternalData_" + userID + "_" + Customerid + "_REV_" + type;
                                }
                                else
                                {
                                    CacheName = "CacheGetInternalData_" + userID + "_" + Customerid + "_REV_" + type;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Select(y => y.UserID).FirstOrDefault());
                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetInternalData_" + userID + "_" + Customerid + "_APPR_" + type;
                                }
                                else
                                {
                                    CacheName = "CacheGetInternalData_" + userID + "_" + Customerid + "_APPR_" + type;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                        }

                        var UserList = (from row in entities.InternalComplianceInstances
                                        join row1 in entities.InternalCompliances
                                        on row.InternalComplianceID equals row1.ID
                                        join row3 in entities.EntitiesAssignmentInternals
                                        on (long)row1.IComplianceCategoryID equals row3.ComplianceCatagoryID
                                        join row4 in entities.Users
                                        on row3.UserID equals row4.ID
                                        where row.ID == InternalComplianceInstanceID && row1.IsDeleted == false
                                        && row4.IsDeleted == false
                                         && row4.IsActive == true && row4.RoleID == 8
                                        select row3.UserID).ToList();

                        UserList = UserList.Distinct().ToList();

                        foreach (var item in UserList)
                        {
                            string CacheName = string.Empty;

                            if (objlocal == "Local")
                            {
                                CacheName = "Cache_GetInternalData_" + item + "_" + Customerid + "_MGMT_" + type;
                            }
                            else
                            {
                                CacheName = "CacheGetInternalData_" + item + "_" + Customerid + "_MGMT_" + type;
                            }

                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                {
                                    StackExchangeRedisExtensions.Remove(CacheName);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void CreateScheduleOn(DateTime InternalscheduledOn, long InternalcomplianceInstanceId, long InternalcomplianceID, long createdByID, string creatdByName)
        {
            long CreateScheduleOnErroInternalcomplianceID = -1;
            long CreateScheduleOnErroInternalcomplianceInstanceId = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    DateTime nextDate = InternalscheduledOn;

                    var compliances = (from row in entities.InternalCompliances
                                       where row.ID == InternalcomplianceID
                                       select row).FirstOrDefault();
                    DateTime curruntDate;
                    if (compliances.IFrequency == 7)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(1);
                    }
                    else if (compliances.IFrequency == 8)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(30);
                    }
                    else
                    {
                        curruntDate = DateTime.UtcNow;
                    }

                    while (nextDate < curruntDate)
                    {
                        Tuple<DateTime, string, long> nextDateMonth;

                        if (compliances.IFrequency == 7)
                        {
                            #region Frequancy Daily
                            CreateScheduleOnErroInternalcomplianceID = InternalcomplianceID;
                            CreateScheduleOnErroInternalcomplianceInstanceId = InternalcomplianceInstanceId;
                            nextDateMonth = Business.InternalComplianceManagement.GetNextDateDaily(nextDate, InternalcomplianceID, InternalcomplianceInstanceId, false);
                            //}
                            long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
                                                 where row.InternalComplianceInstanceID == InternalcomplianceInstanceId && row.ScheduledOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                if (nextDateMonth.Item1.Date.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    InternalComplianceScheduledOn complianceScheduleon = new InternalComplianceScheduledOn();
                                    complianceScheduleon.InternalComplianceInstanceID = InternalcomplianceInstanceId;
                                    complianceScheduleon.ScheduledOn = nextDateMonth.Item1;
                                    complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                    complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                    complianceScheduleon.IsActive = true;
                                    complianceScheduleon.IsUpcomingNotDeleted = true;
                                    entities.InternalComplianceScheduledOns.Add(complianceScheduleon);
                                    entities.SaveChanges();
                                    nextDate = nextDateMonth.Item1;

                                    InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                    {
                                        InternalComplianceInstanceID = InternalcomplianceInstanceId,
                                        InternalComplianceScheduledOnID = complianceScheduleon.ID,
                                        CreatedBy = createdByID,
                                        CreatedByText = creatdByName,
                                        StatusId = 1,
                                        Remarks = "New internal compliance assigned."
                                    };

                                    CreateTransaction(transaction);
                                }
                                else
                                {
                                    if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        nextDate = nextDateMonth.Item1.AddDays(1);
                                    }
                                    else
                                    {
                                        nextDate = nextDateMonth.Item1;
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (compliances.IFrequency == 8)
                        {
                            #region Frequancy Weekly
                            CreateScheduleOnErroInternalcomplianceID = InternalcomplianceID;
                            CreateScheduleOnErroInternalcomplianceInstanceId = InternalcomplianceInstanceId;
                            nextDateMonth = Business.InternalComplianceManagement.GetNextDateWeekly(nextDate, InternalcomplianceID, InternalcomplianceInstanceId, Convert.ToInt32(compliances.DueWeekDay));
                            //}
                            long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
                                                 where row.InternalComplianceInstanceID == InternalcomplianceInstanceId && row.ScheduledOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                DateTime dtNewScheduleOn;
                                if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    dtNewScheduleOn = nextDateMonth.Item1.Date.AddDays(1);
                                }
                                else
                                {
                                    dtNewScheduleOn = nextDateMonth.Item1.Date;
                                }
                                InternalComplianceScheduledOn complianceScheduleon = new InternalComplianceScheduledOn();
                                complianceScheduleon.InternalComplianceInstanceID = InternalcomplianceInstanceId;
                                complianceScheduleon.ScheduledOn = dtNewScheduleOn; // nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                entities.InternalComplianceScheduledOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    nextDate = nextDateMonth.Item1.AddDays(1);
                                }
                                else
                                {
                                    nextDate = nextDateMonth.Item1;
                                }
                                //nextDate = nextDateMonth.Item1;

                                InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                {
                                    InternalComplianceInstanceID = InternalcomplianceInstanceId,
                                    InternalComplianceScheduledOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New internal compliance assigned."
                                };

                                CreateTransaction(transaction);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Frequancy 
                            if (compliances.ISubComplianceType == 0)
                            {
                                nextDateMonth = new Tuple<DateTime, string, long>(nextDate.AddDays(Convert.ToDouble(compliances.IDueDate)), "", 0);
                            }
                            else if (compliances.ISubComplianceType == 1 || compliances.ISubComplianceType == 2)
                            {
                                CreateScheduleOnErroInternalcomplianceID = InternalcomplianceID;
                                nextDateMonth = Business.InternalComplianceManagement.GetNextDatePeriodically(nextDate, InternalcomplianceID);
                            }
                            else
                            {
                                CreateScheduleOnErroInternalcomplianceID = InternalcomplianceID;
                                CreateScheduleOnErroInternalcomplianceInstanceId = InternalcomplianceInstanceId;

                                if (compliances.ScheduleType == 1) // Inbetween
                                {
                                    var complianceSchedule = GetScheduleByComplianceID(compliances.ID);
                                    var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(InternalcomplianceInstanceId);
                                    if (lastSchedule != null)
                                    {
                                        nextDateMonth = NewScheduleInbetweenLogicInternal.GetNextDate(lastSchedule.ScheduledOn, InternalcomplianceID, InternalcomplianceInstanceId, complianceSchedule, (int)compliances.IFrequency, lastSchedule, InternalscheduledOn);
                                    }
                                    else
                                    {
                                        nextDateMonth = NewScheduleInbetweenLogicInternal.GetNextDate(InternalscheduledOn, InternalcomplianceID, InternalcomplianceInstanceId, complianceSchedule, (int)compliances.IFrequency, lastSchedule, InternalscheduledOn);
                                    }
                                }
                                else if (compliances.ScheduleType == 2) //Before
                                {
                                    var complianceSchedule = GetScheduleByComplianceID(compliances.ID);
                                    var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(InternalcomplianceInstanceId);
                                    if (lastSchedule != null)
                                    {
                                        nextDateMonth = NewSchedulePreLogicInternal.GetNextDate(lastSchedule.ScheduledOn, InternalcomplianceID, InternalcomplianceInstanceId, complianceSchedule, (int)compliances.IFrequency, lastSchedule, InternalscheduledOn);
                                    }
                                    else
                                    {
                                        nextDateMonth = NewSchedulePreLogicInternal.GetNextDate(InternalscheduledOn, InternalcomplianceID, InternalcomplianceInstanceId, complianceSchedule, (int)compliances.IFrequency, lastSchedule, InternalscheduledOn);
                                    }

                                }
                                else  //After
                                {
                                    nextDateMonth = Business.InternalComplianceManagement.GetNextDate(nextDate, InternalcomplianceID, InternalcomplianceInstanceId, false);
                                }

                            }
                            long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
                                                 where row.InternalComplianceInstanceID == InternalcomplianceInstanceId && row.ScheduledOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                InternalComplianceScheduledOn complianceScheduleon = new InternalComplianceScheduledOn();
                                complianceScheduleon.InternalComplianceInstanceID = InternalcomplianceInstanceId;
                                complianceScheduleon.ScheduledOn = nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                entities.InternalComplianceScheduledOns.Add(complianceScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDateMonth.Item1;

                                InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                {
                                    InternalComplianceInstanceID = InternalcomplianceInstanceId,
                                    InternalComplianceScheduledOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New internal compliance assigned."
                                };

                                CreateTransaction(transaction);

                                #region Task Scheduele
                                long CustomerBranchID = GetCustomerBranchID(InternalcomplianceInstanceId);
                                var Taskapplicable = TaskManagment.TaskApplicable(CustomerBranchID);
                                if (Taskapplicable != null)
                                {

                                    if (Taskapplicable == 1)
                                    {
                                        var TaskIDList = (from row in entities.Tasks
                                                          join row1 in entities.TaskComplianceMappings
                                                          on row.ID equals row1.TaskID
                                                          where row1.ComplianceID == InternalcomplianceID
                                                          && row.TaskType == 2 && row.ParentID == null
                                                          && row.Isdeleted == false && row.IsActive == true
                                                          && (((row.Status != "A") || (row.Status == null))
                                                          || ((row.Status == "A") && (row.DeactivateOn > DateTime.Now)))
                                                          select row.ID).ToList();

                                        if (TaskIDList.Count > 0)
                                        {
                                            TaskIDList.ForEach(entrytask =>
                                            {

                                                List<TaskNameValue> TaskSubTaskList = new List<TaskNameValue>();

                                                var taskSubTaskList = (from row in entities.Tasks
                                                                       join row1 in entities.TaskInstances
                                                                       on row.ID equals row1.TaskId
                                                                       where row.MainTaskID == entrytask && row1.CustomerBranchID == CustomerBranchID
                                                                       select new { row.ID, row.ParentID, row.DueDays }).ToList();

                                                taskSubTaskList.ForEach(entrysubtask =>
                                                {
                                                    TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long)entrysubtask.ID, ParentID = (int?)entrysubtask.ParentID, ActualDueDays = (int)entrysubtask.DueDays });

                                                });

                                                TaskSubTaskList.ForEach(entryTaskSubTaskList =>
                                                {
                                                    var taskInstances = (from row in entities.TaskInstances
                                                                         where row.IsDeleted == false && row.TaskId == entryTaskSubTaskList.TaskID
                                                                         && row.ScheduledOn <= nextDateMonth.Item1
                                                                         && row.CustomerBranchID == CustomerBranchID
                                                                         select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

                                                    taskInstances.ForEach(entrytaskinstance =>
                                                    {
                                                        TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
                                                        taskScheduleOn.TaskInstanceID = entrytaskinstance.ID;
                                                        taskScheduleOn.ComplianceScheduleOnID = complianceScheduleon.ID;
                                                        taskScheduleOn.ForMonth = nextDateMonth.Item2;
                                                        taskScheduleOn.ForPeriod = nextDateMonth.Item3;
                                                        taskScheduleOn.ScheduleOn = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                        taskScheduleOn.IsActive = true;
                                                        taskScheduleOn.IsUpcomingNotDeleted = true;
                                                        entities.TaskScheduleOns.Add(taskScheduleOn);
                                                        entities.SaveChanges();

                                                        var AssignedTaskRole = GetAssignedTaskUsers((int)entrytaskinstance.ID);
                                                        var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                                        if (performerTaskRole != null)
                                                        {
                                                            var user = UserManagement.GetByID((int)performerTaskRole.UserID);
                                                            TaskTransaction tasktransaction = new TaskTransaction()
                                                            {
                                                                TaskInstanceId = entrytaskinstance.ID,
                                                                TaskScheduleOnID = taskScheduleOn.ID,
                                                                ComplianceScheduleOnID = complianceScheduleon.ID,
                                                                CreatedBy = performerTaskRole.UserID,
                                                                CreatedByText = user.FirstName + " " + user.LastName,
                                                                StatusId = 1,
                                                                Remarks = "New task assigned."
                                                            };

                                                            TaskManagment.CreateTaskTransaction(tasktransaction);

                                                            foreach (var roles in AssignedTaskRole)
                                                            {
                                                                if (roles.RoleID != 6)
                                                                {
                                                                    CreateTaskReminders(entrytaskinstance.TaskId, InternalcomplianceID, roles.ID, nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays)), complianceScheduleon.ID);
                                                                }
                                                            }
                                                        }

                                                    });
                                                });
                                            });
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "InternalComplianceManagement";
                msg.FunctionName = "CreateScheduleOn" + "InternalComplianceid=" + CreateScheduleOnErroInternalcomplianceID + "InternalcomplianceInstanceId=" + CreateScheduleOnErroInternalcomplianceInstanceId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);
                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                   null, "Error Occured as Internal CreateScheduleOn Function", "Internal CreateScheduleOn" + "InternalComplianceid=" + CreateScheduleOnErroInternalcomplianceID + "InternalcomplianceInstanceId=" + CreateScheduleOnErroInternalcomplianceInstanceId);
            }
        }

        public static long? GetInternalComplianceInstanceID(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceInstanceID = (from row in entities.InternalComplianceScheduledOns
                                            where row.ID == ScheduleOnID
                                            select row.InternalComplianceInstanceID).FirstOrDefault();
                return ComplianceInstanceID;
            }
        }
        public static bool ComplianceIsEventBased(long complianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.InternalCompliances
                                      where row.ID == complianceId
                                      select row).FirstOrDefault();

                if (ComplianceData.EventFlag == null)
                    return true;
                else
                    return false;
            }
        }
        //public static void CreateEventBasedInternalComplinceReminders(long complianceID, long ComplianceInstanceId, int ComplianceAssignmentId, DateTime eventDueDate, DateTime scheduledOn, long EventScheduledOnID)
        //{
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            InternalComplianceReminder reminder = new InternalComplianceReminder()
        //            {
        //                ComplianceAssignmentID = ComplianceAssignmentId,
        //                ReminderTemplateID = 1,
        //                ComplianceDueDate = scheduledOn,
        //                EventScheduledOnID = EventScheduledOnID,
        //                RemindOn = scheduledOn.Date.AddDays(-1 * 2),
        //            };
        //            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
        //            entities.InternalComplianceReminders.Add(reminder);


        //            InternalComplianceReminder reminder1 = new InternalComplianceReminder()
        //            {
        //                ComplianceAssignmentID = ComplianceAssignmentId,
        //                ReminderTemplateID = 1,
        //                ComplianceDueDate = scheduledOn,
        //                EventScheduledOnID = EventScheduledOnID,
        //                RemindOn = scheduledOn.Date.AddDays(-1 * 7),
        //            };
        //            reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
        //            entities.InternalComplianceReminders.Add(reminder1);


        //            InternalComplianceReminder reminder2 = new InternalComplianceReminder()
        //            {
        //                ComplianceAssignmentID = ComplianceAssignmentId,
        //                ReminderTemplateID = 1,
        //                ComplianceDueDate = scheduledOn,
        //                EventScheduledOnID = EventScheduledOnID,
        //                RemindOn = scheduledOn.Date.AddDays(-1 * 15),
        //            };
        //            reminder2.Status = (byte)(reminder2.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
        //            entities.InternalComplianceReminders.Add(reminder2);

        //            entities.SaveChanges();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();

        //        LogMessage msg = new LogMessage();
        //        msg.ClassName = "InternalComplianceManagement";
        //        //msg.FunctionName = "CreateReminders";
        //        msg.FunctionName = "InternalCreateReminders" + "InternalComplianceAssignmentId" + ComplianceAssignmentId;
        //        msg.CreatedOn = DateTime.Now;
        //        msg.Message = ex.Message + "----\r\n" + ex.InnerException;
        //        msg.StackTrace = ex.StackTrace;
        //        msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
        //        ReminderUserList.Add(msg);
        //        InsertLogToDatabase(ReminderUserList);

        //        List<string> TO = new List<string>();
        //        TO.Add("sachin@avantis.info");
        //        TO.Add("narendra@avantis.info");
        //        TO.Add("rahul@avantis.info");
        //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
        //            null, "Error Occured as InternalCreateReminders Function", "InternalCreateReminders" + "InternalComplianceAssignmentId" + ComplianceAssignmentId);

        //    }
        //}


        public static void CreateEventBasedInternalComplinceReminders(long complianceID, long ComplianceInstanceId, int ComplianceAssignmentId, DateTime eventDueDate, DateTime scheduledOn, long EventScheduledOnID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<long> getdays = GetUpcomingReminderEventDaysDetail(ComplianceAssignmentId, "IE");
                    if (getdays.Count > 0)
                    {
                        foreach (var item in getdays)
                        {
                            InternalComplianceReminder reminder2 = new InternalComplianceReminder()
                            {
                                ComplianceAssignmentID = ComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = scheduledOn,
                                EventScheduledOnID = EventScheduledOnID,
                                RemindOn = scheduledOn.Date.AddDays(-item),
                            };
                            reminder2.Status = (byte)(reminder2.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                            entities.InternalComplianceReminders.Add(reminder2);

                            entities.SaveChanges();
                        }
                    }
                    else
                    {
                        InternalComplianceReminder reminder = new InternalComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            EventScheduledOnID = EventScheduledOnID,
                            RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                        };
                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.InternalComplianceReminders.Add(reminder);


                        InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            EventScheduledOnID = EventScheduledOnID,
                            RemindOn = scheduledOn.Date.AddDays(-1 * 7),
                        };
                        reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.InternalComplianceReminders.Add(reminder1);


                        InternalComplianceReminder reminder2 = new InternalComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            EventScheduledOnID = EventScheduledOnID,
                            RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                        };
                        reminder2.Status = (byte)(reminder2.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.InternalComplianceReminders.Add(reminder2);

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();

                LogMessage msg = new LogMessage();
                msg.ClassName = "InternalComplianceManagement";
                //msg.FunctionName = "CreateReminders";
                msg.FunctionName = "InternalCreateReminders" + "InternalComplianceAssignmentId" + ComplianceAssignmentId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as InternalCreateReminders Function", "InternalCreateReminders" + "InternalComplianceAssignmentId" + ComplianceAssignmentId);

            }
        }

        public static List<long> GetUpcomingReminderEventDaysDetail(long ComplianceAssignmentId, string Type)
        {
            List<long> output = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User User = (from row in entities.Users
                             join row1 in entities.ComplianceAssignments
                             on row.ID equals row1.UserID
                             where row1.ID == ComplianceAssignmentId
                             && row.IsActive == true
                             && row.IsDeleted == false
                             select row).FirstOrDefault();
                if (User != null)
                {
                    long customerID = Convert.ToInt32(User.CustomerID);
                    long UserID = Convert.ToInt32(User.ID);

                    var UserObj = (from row in entities.ReminderTemplate_UserMapping
                                   where row.CustomerID == customerID
                                   && row.UserID == UserID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   && row.Flag == "UN"
                                   select row).ToList();

                    if (UserObj.Count > 0)
                    {
                        output.Clear();
                        output = UserObj.Select(x => x.TimeInDays).ToList();
                    }
                    else
                    {
                        var CustObj = (from row in entities.ReminderTemplate_CustomerMapping
                                       where row.CustomerID == customerID
                                       && row.IsActive == true
                                       && row.Type == Type
                                       && row.Flag == "UN"
                                       select row).ToList();

                        if (CustObj.Count > 0)
                        {
                            output.Clear();
                            output = CustObj.Select(x => x.TimeInDays).ToList();
                        }
                        else
                        {
                            output.Clear();
                        }
                    }
                }
                return output;
            }
        }

        public static bool GetInternalcomplianceByType(long ComplianceID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Boolean flag = false;
                var form = (from row in entities.InternalCompliances
                            join row1 in entities.AuditChecklistComplianceMappings
                            on row.ID equals row1.ComplianceID
                            where row1.ComplianceType == "I"
                            && row1.Isactive == false && CustomerID == row1.CustomerID
                            && row1.ComplianceID == ComplianceID
                            select row1).FirstOrDefault();

                if (form == null)
                {
                    flag = false;
                }
                else
                {
                    flag = true;
                }

                return flag;
            }
        }

      

        public static InternalFileData GetFile(int fileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var file = (from row in entities.InternalFileDatas
                            where row.ID == fileID
                            select row).FirstOrDefault();

                return file;
            }
        }
        public static InternalComplianceForm GetSelectedInternalComplianceFormByID(int ID,long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.InternalComplianceForms
                            where row.IComplianceID == complianceID
                            && row.ID == ID
                            select row).FirstOrDefault();

                return form;
            }
        }

        public static List<InternalComForm> GetMultipleInternalComplianceForm(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.InternalComplianceForms
                            where row.IComplianceID == complianceID
                            select new InternalComForm
                            {
                                ID =row.ID,
                                ComplianceID= row.IComplianceID,
                                Name=row.Name,
                            }
                            ).ToList();

                return form;
            }
        }
        public static List<InternalComplianceForm> GetMultipleInternalComplianceFormByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.InternalComplianceForms
                            where row.IComplianceID == complianceID
                            select row).ToList();

                return form;
            }
        }
        public static void DeleteInternalComplianceForm(int ID, int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalComplianceForms
                             where row.IComplianceID == complianceID && row.ID == ID
                             select row).FirstOrDefault();

                entities.InternalComplianceForms.Remove(query);
                entities.SaveChanges();
            }
        }

        public static InternalComplianceForm GetSelectedInternalComplianceFileName(int ID, int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalComplianceForms
                             where row.IComplianceID == complianceID && row.ID == ID
                             select row).FirstOrDefault();
                return query;
            }
        }
        public static void UpdateComplianceScheduleOn(long ComplianceID, long ComplianceInstanceID, long complianceScheduleOnID, DateTime Oldscheduleon, DateTime Newscheduleon, int forperiod)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceScheduledOn complianceScheduleOn = (from row in entities.InternalComplianceScheduledOns
                                                                      where row.ID == complianceScheduleOnID
                                                                      select row).FirstOrDefault();

                complianceScheduleOn.ScheduledOn = Newscheduleon;
                entities.SaveChanges();


                #region Update ComplianceSchedule
                //var complianceSchedule = (from row in entities.InternalComplianceSchedules
                //                          where row.IComplianceID == ComplianceID
                //                          && row.ForMonth == forperiod
                //                          select row).FirstOrDefault();

                //if (complianceSchedule != null)
                //{
                //    string SpecialDates = string.Format("{0}{1}", Convert.ToByte(Newscheduleon.Day).ToString("D2"), Convert.ToByte(Newscheduleon.Month).ToString("D2"));
                //    complianceSchedule.SpecialDate = SpecialDates;
                //    entities.SaveChanges();
                //}
                #endregion
              
                #region Update Task Schedule
                var taskScheduleIDList = (from row in entities.TaskScheduleOns
                                          where row.ComplianceScheduleOnID == complianceScheduleOnID
                                          && row.ForMonth == complianceScheduleOn.ForMonth
                                          && row.IsUpcomingNotDeleted == true && row.IsActive == true
                                          select row.ID).ToList();
                bool taskTransactionFlag = false;

                if (taskScheduleIDList.Count > 0)
                {
                    taskScheduleIDList.ForEach(entrytaskScheduleID =>
                    {
                        var complianceTransactions = (from row in entities.TaskTransactions
                                                      where row.TaskScheduleOnID == entrytaskScheduleID
                                                      && row.ComplianceScheduleOnID == complianceScheduleOnID
                                                      select row).ToList();

                        if (complianceTransactions.Count >= 2)
                        {
                            taskTransactionFlag = true;
                        }
                    });

                    if (taskTransactionFlag == false)
                    {
                        taskScheduleIDList.ForEach(entrytaskScheduleID =>
                        {

                            TaskScheduleOn taskScheduleOn = (from row in entities.TaskScheduleOns
                                                             where row.ID == entrytaskScheduleID
                                                               && row.ForMonth == complianceScheduleOn.ForMonth
                                                             select row).FirstOrDefault();

                            TaskInstance taskInstance = (from row in entities.TaskInstances
                                                         where row.ID == taskScheduleOn.TaskInstanceID
                                                         select row).FirstOrDefault();

                            Task task = (from row in entities.Tasks
                                         where row.ID == taskInstance.TaskId
                                         select row).FirstOrDefault();

                            DateTime taskOldScheduleOn;
                            if (task.IsAfter == true)
                            {
                                taskScheduleOn.ScheduleOn = Newscheduleon.AddDays(Convert.ToInt64(task.DueDays));
                                taskOldScheduleOn = Oldscheduleon.AddDays(Convert.ToInt64(task.DueDays));
                            }
                            else
                            {
                                taskScheduleOn.ScheduleOn = Newscheduleon.AddDays(-Convert.ToInt64(task.DueDays));
                                taskOldScheduleOn = Oldscheduleon.AddDays(-Convert.ToInt64(task.DueDays));
                            }
                            entities.SaveChanges();

                            var taskAssignments = (from row in entities.TaskAssignments
                                                   where row.TaskInstanceID == taskInstance.ID
                                                   select row).ToList();

                            taskAssignments.ForEach(taskAssignmentsentry =>
                            {
                                var taskReminders = (from row in entities.TaskReminders
                                                     where row.TaskAssignmentID == taskAssignmentsentry.ID
                                                     && row.TaskDueDate == taskOldScheduleOn
                                                     select row).ToList();
                                //Remove taskReminders
                                taskReminders.ForEach(entry =>
                                entities.TaskReminders.Remove(entry));
                                entities.SaveChanges();
                            });

                            var AssignedTaskRole = TaskManagment.GetAssignedTaskUsers((int)taskInstance.ID);

                            foreach (var roles in AssignedTaskRole)
                            {
                                if (roles.RoleID != 6)
                                {
                                    TaskManagment.CreateTaskReminders(taskInstance.ID, ComplianceID, roles.ID, Newscheduleon.AddDays(-Convert.ToInt64(task.DueDays)), complianceScheduleOnID, task.IsAfter);
                                }
                            }

                        });
                    }
                }

                #endregion

                var complianceAssignments = (from row in entities.InternalComplianceAssignments
                                             where row.InternalComplianceInstanceID == ComplianceInstanceID
                                             select row).ToList();

                complianceAssignments.ForEach(complianceAssignmentsentry =>
                {
                    var complianceReminders = (from row in entities.InternalComplianceReminders
                                               where row.ComplianceAssignmentID == complianceAssignmentsentry.ID
                                               && row.ComplianceDueDate == Oldscheduleon
                                               select row).ToList();                    
                    complianceReminders.ForEach(entry =>
                    entities.InternalComplianceReminders.Remove(entry));
                    entities.SaveChanges();
                });

                var AssignedRole = GetAssignedUsers(Convert.ToInt32(ComplianceInstanceID));
                foreach (var roles in AssignedRole)
                {
                    if (roles.RoleID != 6)
                    {
                        CreateReminders(ComplianceID, ComplianceInstanceID, roles.ID, Newscheduleon);
                    }
                }
            }
        }

        public static List<object> GetAllInternalComplianceAssignedPeriods(long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row1 in entities.InternalCompliances
                            join row2 in entities.InternalComplianceInstances
                            on row1.ID equals row2.InternalComplianceID
                            join row3 in entities.InternalComplianceAssignments
                            on row2.ID equals row3.InternalComplianceInstanceID
                            join row4 in entities.InternalComplianceScheduledOns
                            on row3.InternalComplianceInstanceID equals row4.InternalComplianceInstanceID
                            where row1.IsDeleted == false
                            && row1.ID == ComplianceID && row4.IsUpcomingNotDeleted == true && row4.IsActive == true
                            orderby row1.IShortDescription ascending
                            select new { ID = row4.ForMonth, Name = row4.ForMonth }).Distinct().OrderBy(entry => entry.Name).ToList<object>();
                return acts;
            }
        }

        public static List<object> GetAllInternalComplainceAssignedCustomers(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row1 in entities.InternalCompliances
                            join row2 in entities.InternalComplianceInstances
                            on row1.ID equals row2.InternalComplianceID
                            join row3 in entities.InternalComplianceAssignments
                            on row2.ID equals row3.InternalComplianceInstanceID
                            join row4 in entities.CustomerBranches
                             on row2.CustomerBranchID equals row4.ID
                            join row5 in entities.Customers
                            on row4.CustomerID equals row5.ID
                            join row6 in entities.CustomerAssignmentDetails
                            on row5.ID equals row6.CustomerID
                            where row5.IsDeleted == false
                            && row6.UserID==UserID
                            && row6.IsDeleted == false
                            && row1.IsDeleted == false
                            orderby row5.Name ascending
                            select new { ID = row5.ID, Name = row5.Name }).Distinct().OrderBy(entry => entry.Name).ToList<object>();
                return acts;
            }
        }
        public static List<object> GetAllAssignedInternalCompliances(long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row1 in entities.InternalCompliances
                            join row2 in entities.InternalComplianceInstances
                            on row1.ID equals row2.InternalComplianceID
                            join row3 in entities.InternalComplianceAssignments
                            on row2.ID equals row3.InternalComplianceInstanceID
                            join row4 in entities.CustomerBranches
                            on row2.CustomerBranchID equals row4.ID
                            join row5 in entities.Customers
                            on row4.CustomerID equals row5.ID
                            where row5.IsDeleted == false && row1.IsDeleted == false
                            && row5.ID == CustomerID
                            orderby row5.Name ascending
                            select new { ID = row1.ID, Name = row1.IShortDescription }).Distinct().OrderBy(entry => entry.Name).ToList<object>();

                return acts;
            }
        }
        public static InternalCompliance GetInternalComplianceType(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.InternalComplianceInstances
                                  join row1 in entities.InternalCompliances
                                  on row.InternalComplianceID equals row1.ID
                                  where row.ID == complianceInstanceID
                                  select row1).SingleOrDefault();

                return compliance;
            }
        }
        public static InternalCompliance GetCompliance(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.InternalCompliances
                            where row.ID == complianceID
                            select row).SingleOrDefault();

                return form;
            }
        }
        public static long GetInternalComplianceInstanceIDFromScheduleOn(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long internalComplianceInstanceID = (from row in entities.InternalComplianceTransactions
                                             where row.InternalComplianceScheduledOnID == ScheduledOnID
                                             select row.InternalComplianceInstanceID).FirstOrDefault();

                return internalComplianceInstanceID;
            }
        }
        public static bool InternalComplianceIsStatutory(long ComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.InternalCompliances
                                      join row1 in entities.InternalComplianceInstances
                                      on row.ID equals row1.InternalComplianceID
                                      where row1.ID == ComplianceInstanceId
                                      select row).FirstOrDefault();

                if (ComplianceData.IComplianceType == 1)
                    return false;
                else
                    return true;
            }
        }
        public static bool ExistsR(string ShortDescription, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalCompliances
                             where row.CustomerID == CustomerID
                             && row.IShortDescription.Equals(ShortDescription) && row.IsDeleted == false
                             select row);
                return query.Select(entry => true).FirstOrDefault();

            }
        }

        public static bool ExistsShortForm(string ShortForm, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalCompliances
                             where row.CustomerID == CustomerID
                             && row.IShortForm.Equals(ShortForm) && row.IsDeleted == false
                             select row);
                return query.Select(entry => true).FirstOrDefault();

            }
        }

        public static bool ExistsSavedCompliance(string ShortDescription, int ComplianceID, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalCompliances
                             where row.IShortDescription.Equals(ShortDescription) && row.IsDeleted == false
                             && row.ID != ComplianceID && row.CustomerID == CustomerID
                             select row);
                return query.Select(entry => true).FirstOrDefault();

            }
        }
        public static bool ExistsShortDescription(string ShortForm, int ComplianceID,int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalCompliances
                             where row.IShortForm.Equals(ShortForm) && row.IsDeleted == false
                             && row.ID != ComplianceID && row.CustomerID == CustomerID
                             select row);
                return query.Select(entry => true).FirstOrDefault();

            }
        }

        public static InternalComplianceInstance GetInternalComplianceInstance(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleObj = (from row in entities.InternalComplianceInstances
                                   where row.ID == ComplianceInstanceID
                                   && row.IsDeleted == false 
                                   select row).FirstOrDefault();

                return scheduleObj;
            }
        }
        public static void CreateSampleFile(InternalComplianceForm form, bool deleteOldFiles = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (deleteOldFiles)
                {
                    var formIDsToDelete = (from row in entities.InternalComplianceForms
                                           where row.IComplianceID == form.IComplianceID
                                           select row.ID).ToList();

                    formIDsToDelete.ForEach(entry =>
                    {
                        //var formToDelete = new InternalComplianceForm() { ID = entry };
                        //entities.InternalComplianceForms.Attach(formToDelete);

                        var formToDelete = (from row in entities.InternalComplianceForms
                                            where row.IComplianceID == form.IComplianceID
                                            select row).FirstOrDefault();

                        entities.InternalComplianceForms.Add(formToDelete);
                    });
                }

                entities.InternalComplianceForms.Add(form);
                entities.SaveChanges();
            }

        }
        public static InternalComplianceForm GetComplianceFormByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.InternalComplianceForms
                            where row.IComplianceID == complianceID
                            select row).FirstOrDefault();

                return form;
            }
        }

        public static void GenerateDefaultScheduleForComplianceIDNEW(long complianceID, byte? subcomplincetype, bool deleteOldData = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string calflag = "N";
                int step = 1;
                InternalCompliance compliance = new InternalCompliance();
                if (!(subcomplincetype == 0))
                {
                    if (deleteOldData)
                    {
                        var ids = (from row in entities.InternalComplianceSchedules
                                   where row.IComplianceID == complianceID
                                   select row.ID).ToList();


                        compliance = (from row in entities.InternalCompliances
                                      where row.ID == complianceID
                                      select row).First();

                        var Formonths = (from row in entities.InternalComplianceSchedules
                                         where row.IComplianceID == complianceID
                                         select row.ForMonth).ToList();

                        switch ((Frequency) compliance.IFrequency.Value)
                        {
                            case Frequency.Quarterly:
                                calflag = "N";
                                step = 3;
                                break;
                            case Frequency.FourMonthly:
                                if (Formonths.Contains(12) || Formonths.Contains(8) || Formonths.Contains(4))
                                {
                                    calflag = "Y";
                                }
                                else
                                {
                                    calflag = "N";
                                }
                                step = 4;
                                break;
                            case Frequency.HalfYearly:
                                if (Formonths.Contains(4) || Formonths.Contains(10))
                                {
                                    calflag = "Y";
                                }
                                else
                                {
                                    calflag = "N";
                                }
                                step = 6;
                                break;
                            case Frequency.Annual:
                                if (Formonths.Contains(4))
                                {
                                    calflag = "Y";
                                }
                                else
                                {
                                    calflag = "N";
                                }
                                step = 12;
                                break;
                            case Frequency.TwoYearly:
                                if (Formonths.Contains(4))
                                {
                                    calflag = "Y";
                                }
                                else
                                {
                                    calflag = "N";
                                }
                                step = 12;
                                break;
                            case Frequency.SevenYearly:
                                if (Formonths.Contains(4))
                                {
                                    calflag = "Y";
                                }
                                else
                                {
                                    calflag = "N";
                                }
                                step = 12;
                                break;
                        }
                        ids.ForEach(entry =>
                        {
                            InternalComplianceSchedule schedule = (from row in entities.InternalComplianceSchedules
                                                           where row.ID == entry
                                                           select row).FirstOrDefault();

                            entities.InternalComplianceSchedules.Remove(schedule);
                        });

                        entities.SaveChanges();
                    }
                    int SpecialMonth;
                    #region Financial Year And Calender
                    if (calflag == "Y")
                    {
                        for (int month = 4; month <= 12; month += step)
                        {
                            InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                            complianceShedule.IComplianceID = complianceID;
                            complianceShedule.ForMonth = month;
                            if (subcomplincetype == 1 || subcomplincetype == 2)
                            {
                                int monthCopy = month;
                                if (monthCopy == 1)
                                {
                                    monthCopy = 12;
                                    complianceShedule.ForMonth = monthCopy;
                                }
                                else
                                {
                                    monthCopy = month - 1;
                                    complianceShedule.ForMonth = monthCopy;
                                }

                                if (compliance.IFrequency == 3 || compliance.IFrequency == 5 || compliance.IFrequency == 6)
                                {
                                    monthCopy = 3;
                                    complianceShedule.ForMonth = monthCopy;
                                }

                                int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                                complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");

                            }
                            else
                            {
                                SpecialMonth = month;
                                if (compliance.IFrequency == 0 && SpecialMonth == 12)
                                {
                                    SpecialMonth = 1;
                                }
                                else if (compliance.IFrequency == 1 && SpecialMonth == 10)
                                {
                                    SpecialMonth = 1;
                                }
                                else if (compliance.IFrequency == 2 && SpecialMonth == 10)
                                {
                                    SpecialMonth = 4;
                                }
                                else if (compliance.IFrequency == 2 && SpecialMonth == 4)
                                {
                                    SpecialMonth = 10;
                                }
                                else if (compliance.IFrequency == 4 && SpecialMonth == 4)
                                {
                                    SpecialMonth = 8;
                                }
                                else if (compliance.IFrequency == 4 && SpecialMonth == 8)
                                {
                                    SpecialMonth = 12;
                                }
                                else if (compliance.IFrequency == 4 && SpecialMonth == 12)
                                {
                                    SpecialMonth = 4;
                                }
                                else if (compliance.IFrequency == 3 && SpecialMonth == 1)
                                {
                                    SpecialMonth = 4;
                                }
                                else if (compliance.IFrequency == 3 && SpecialMonth == 4)
                                {
                                    SpecialMonth = 4;
                                }
                                else if ((compliance.IFrequency == 5 || compliance.IFrequency == 6) && SpecialMonth == 1)
                                {
                                    SpecialMonth = 1;
                                }
                                else
                                {
                                    SpecialMonth = SpecialMonth + step;
                                }

                                int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, month);
                                if (Convert.ToInt32(compliance.IDueDate.Value.ToString("D2")) > lastdate)
                                {
                                    complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                }
                                else
                                {
                                    complianceShedule.SpecialDate = compliance.IDueDate.Value.ToString("D2") + SpecialMonth.ToString("D2");
                                }

                            }
                            entities.InternalComplianceSchedules.Add(complianceShedule);
                        }//for loop end
                    }//Financial Year close
                    else
                    {
                        for (int month = 1; month <= 12; month += step)
                        {
                            InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                            complianceShedule.IComplianceID = complianceID;
                            complianceShedule.ForMonth = month;

                            if (subcomplincetype == 1 || subcomplincetype == 2)
                            {
                                int monthCopy = month;
                                if (monthCopy == 1)
                                {
                                    monthCopy = 12;
                                    complianceShedule.ForMonth = monthCopy;
                                }
                                else
                                {
                                    monthCopy = month - 1;
                                    complianceShedule.ForMonth = monthCopy;
                                }
                                if (compliance.IFrequency == 3 || compliance.IFrequency == 5 || compliance.IFrequency == 6)
                                {
                                    monthCopy = 3;
                                    complianceShedule.ForMonth = monthCopy;
                                }
                                int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                                complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");
                            }
                            else
                            {
                                SpecialMonth = month;
                                if (compliance.IFrequency == 0 && SpecialMonth == 12)
                                {
                                    SpecialMonth = 1;
                                }
                                else if (compliance.IFrequency == 1 && SpecialMonth == 10)
                                {
                                    SpecialMonth = 1;
                                }
                                else if (compliance.IFrequency == 2 && SpecialMonth == 7)
                                {
                                    SpecialMonth = 1;
                                }
                                else if (compliance.IFrequency == 4 && SpecialMonth == 9)
                                {
                                    SpecialMonth = 1;
                                }
                                else if ((compliance.IFrequency == 3 || compliance.IFrequency == 5 || compliance.IFrequency == 6) && SpecialMonth == 1)
                                {
                                    SpecialMonth = 1;
                                }
                                else
                                {
                                    SpecialMonth = SpecialMonth + step;
                                }

                                int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                                if (Convert.ToInt32(compliance.IDueDate.Value.ToString("D2")) > lastdate)
                                {
                                    complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                }
                                else
                                {
                                    complianceShedule.SpecialDate = compliance.IDueDate.Value.ToString("D2") + SpecialMonth.ToString("D2");
                                }
                            }
                            entities.InternalComplianceSchedules.Add(complianceShedule);
                        }
                    }//Calender Year Close
                    #endregion
                    entities.SaveChanges();
                }
            }
        }

        public static void EditCompliance(InternalCompliance compliance, InternalComplianceForm form, long createdByID, string creatdByName)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var oldData = (from row in entities.InternalCompliances
                                   where row.ID == compliance.ID
                                   select new { row.IFrequency, row.IDueDate, row.IComplianceType, row.ISubComplianceType, row.IReminderType, row.IReminderBefore, row.IReminderGap, row.DueWeekDay }).SingleOrDefault();

                    InternalCompliance complianceToUpdate = (from row in entities.InternalCompliances
                                                             where row.ID == compliance.ID
                                                             select row).FirstOrDefault();

                    complianceToUpdate.IShortDescription = compliance.IShortDescription;
                    complianceToUpdate.IUploadDocument = compliance.IUploadDocument;
                    complianceToUpdate.IComplianceType = compliance.IComplianceType;
                    complianceToUpdate.IRequiredFrom = compliance.IRequiredFrom;
                    complianceToUpdate.IFrequency = compliance.IFrequency;
                    complianceToUpdate.IDueDate = compliance.IDueDate;
                    complianceToUpdate.IRiskType = compliance.IRiskType;
                    complianceToUpdate.ISubComplianceType = compliance.ISubComplianceType;
                    complianceToUpdate.IReminderType = compliance.IReminderType;
                    complianceToUpdate.IReminderBefore = compliance.IReminderBefore;
                    complianceToUpdate.IReminderGap = compliance.IReminderGap;
                    complianceToUpdate.EffectiveDate = compliance.EffectiveDate;
                    complianceToUpdate.DueWeekDay = compliance.DueWeekDay;
                    complianceToUpdate.IShortForm = compliance.IShortForm;
                    entities.SaveChanges();
                    if (form != null)
                    {
                        form.IComplianceID = compliance.ID;
                        CreateSampleFile(form, true);
                    }
                    else
                    {
                        InternalComplianceForm Complianceform = GetComplianceFormByID (compliance.ID);
                        if (Complianceform != null)
                        {
                            entities.InternalComplianceForms.Attach(Complianceform);
                            entities.InternalComplianceForms.Remove(Complianceform);
                            entities.SaveChanges();
                        }
                    }
                    if (compliance.IFrequency != 7)
                    {
                        if (compliance.IFrequency != 8)
                        {
                            if (oldData.IFrequency != compliance.IFrequency || oldData.IDueDate != compliance.IDueDate)
                            {
                                GenerateDefaultScheduleForComplianceIDNEW(compliance.ID, compliance.ISubComplianceType, true);
                            }
                        }
                    }

                    if ((oldData.IFrequency != compliance.IFrequency) || (oldData.IDueDate != compliance.IDueDate) || (oldData.IComplianceType != compliance.IComplianceType) ||
                        (oldData.ISubComplianceType != compliance.ISubComplianceType) || (oldData.IReminderType != compliance.IReminderType) ||
                        (oldData.IReminderBefore != compliance.IReminderBefore) || (oldData.IReminderGap != compliance.IReminderGap) || (oldData.DueWeekDay != compliance.DueWeekDay))
                    {
                        DateTime dEffectiveDate = (DateTime) compliance.EffectiveDate;

                        //ComplianceScheduleOnUpdateAsNotActive(compliance.ID, createdByID, dEffectiveDate);
                        UpdateOldData(compliance.ID, createdByID, creatdByName, dEffectiveDate);
                    }
                }
            }
            catch (Exception ex)
            {
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //public static List<InternalComplianceInstanceCheckListTransactionView> DashboardDataForPerformer_ChecklistInternal(int userID, int risk, int location, int type, int category, DateTime FromDate, DateTime ToDate, string StringType)
        //{


        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();

        //        DateTime now = DateTime.UtcNow.Date;
        //        transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
        //                             where row.UserID == userID && row.RoleID == performerRoleID
        //                              && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                              && (row.InternalScheduledOn == now || row.InternalScheduledOn <= now) && row.InternalComplianceStatusID == 1
        //                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //        //Type Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
        //        }

        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
        //        }

        //        //Datr Filter
        //        if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
        //        }

        //        //Risk Filter
        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }
        //        // Find data through String contained in Description
        //        if (StringType != "")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<SP_GetInternalCheckListCannedReportCompliancesSummary_Result> DashboardDataForPerformer_ChecklistInternal(int userID,int CustomerID, int risk, int location, int type, int category, DateTime FromDate, DateTime ToDate, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetInternalCheckListCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetInternalCheckListCannedReportCompliancesSummary_Result>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in entities.SP_GetInternalCheckListCannedReportCompliancesSummary(userID, CustomerID, "PERF")
                                     where row.UserID == userID && row.RoleID == performerRoleID
                                      && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                      && (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 1
                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                //Type Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        //public static List<InternalComplianceInstanceCheckListTransactionView> DashboardDataForReviewer_ChecklistInternal(int userID, int risk, int location, int type, int category, DateTime FromDate, DateTime ToDate, string StringType)
        //{


        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
        //        int ReviewerRoleID = (from row in entities.Roles
        //                               where row.Code == "RVW1"
        //                               select row.ID).Single();

        //        DateTime now = DateTime.UtcNow.Date;
        //        transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
        //                             where row.UserID == userID && row.RoleID == ReviewerRoleID
        //                              && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                              //&& (row.InternalScheduledOn == now || row.InternalScheduledOn <= now) && row.InternalComplianceStatusID == 1
        //                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //        //Type Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
        //        }

        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
        //        }

        //        //Datr Filter
        //        if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
        //        }

        //        //Risk Filter
        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }
        //        // Find data through String contained in Description
        //        if (StringType != "")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<SP_GetInternalCheckListCannedReportCompliancesSummary_Result> DashboardDataForReviewer_ChecklistInternal(int userID,int CustomerID, int risk, int location, int type, int category, DateTime FromDate, DateTime ToDate, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetInternalCheckListCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetInternalCheckListCannedReportCompliancesSummary_Result>();
                int ReviewerRoleID = (from row in entities.Roles
                                      where row.Code == "RVW1"
                                      select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in entities.SP_GetInternalCheckListCannedReportCompliancesSummary(userID, CustomerID, "RVW")
                                     where row.UserID == userID && row.RoleID == ReviewerRoleID
                                      && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                     //&& (row.InternalScheduledOn == now || row.InternalScheduledOn <= now) && row.InternalComplianceStatusID == 1
                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                //Type Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<InternalComplianceInstanceCheckListTransactionView> GetMappedComplianceCheckListInternal(long UserID,
           List<InternalComplianceInstanceCheckListTransactionView> MaterInternalCheckListTransactionsQuery, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
                if (IsPerformer == true)
                {
                    int performerRoleID = (from row in entities.Roles
                                           where row.Code == "PERF"
                                           select row.ID).Single();


                    DateTime now = DateTime.UtcNow.Date;
                    transactionsQuery = (from row in MaterInternalCheckListTransactionsQuery //entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == UserID && row.RoleID == performerRoleID
                                         && (row.InternalScheduledOn == now || row.InternalScheduledOn <= now)
                                         && row.InternalComplianceStatusID == 1
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList();

                    if (!string.IsNullOrEmpty(filter))
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }
                return transactionsQuery;
            }
        }

        public static List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> SPGetMappedComplianceCheckListInternal(long UserID,
           List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> MaterInternalCheckListTransactionsQuery, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> transactionsQuery = new List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result>();
                if (IsPerformer == true)
                {
                    int performerRoleID = (from row in entities.Roles
                                           where row.Code == "PERF"
                                           select row.ID).Single();


                    DateTime now = DateTime.UtcNow.Date;
                    transactionsQuery = (from row in MaterInternalCheckListTransactionsQuery //entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == UserID && row.RoleID == performerRoleID
                                         && (row.InternalScheduledOn == now || row.InternalScheduledOn <= now)
                                         && row.InternalComplianceStatusID == 1
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList();                 
                }
                return transactionsQuery;
            }
        }

        public static void UpdateInternalScheduleOn(int ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceScheduledOn TempAssignment = (from row in entities.InternalComplianceScheduledOns
                                                                where row.ID == ScheduleOnID
                                                                select row).FirstOrDefault();
                TempAssignment.IsActive = true;
                TempAssignment.IsUpcomingNotDeleted = true;
                entities.SaveChanges();
            }
        }
        public static List<InternalComplianceScheduledOn> GetInternalComplianceScheduleWithEffectiveDate(long InternalComplianceID, long InternalComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduledOns = (from row in entities.InternalComplianceScheduledOns
                            where row.InternalComplianceInstanceID == InternalComplianceInstanceID
                            && row.IsActive == false && row.IsUpcomingNotDeleted == false
                            select row).ToList();

                return ScheduledOns;
            }
        }
        public static List<SP_GetInternalComplianceEffectiveDate_Result> GetInternalComplianceEffectiveDate(int CustBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceList = (from row in entities.SP_GetInternalComplianceEffectiveDate(Convert.ToInt32(CustBranchID))
                                      select row).ToList();

                return ComplianceList;
            }
        }

        public static bool TratsactionExists(int complianceInstanceid, int compliancescheduleonid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> statusids = new List<int>();
                statusids.Add(2);
                statusids.Add(3);
                statusids.Add(10);
                statusids.Add(4);
                statusids.Add(5);
                statusids.Add(11);
                var IsUpcomingtoDeleteusingtran = (from row in entities.InternalComplianceTransactions
                                                   where row.InternalComplianceInstanceID == complianceInstanceid
                                                   && row.InternalComplianceScheduledOnID == compliancescheduleonid
                                                   && statusids.Contains(row.StatusId)
                                                   select row.ID).FirstOrDefault();

                if (IsUpcomingtoDeleteusingtran != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void CreateHistoryEditCompliance(H_Edit_InternalCompliance HistoryEditCompliance)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.H_Edit_InternalCompliance.Add(HistoryEditCompliance);
                entities.SaveChanges();
            }
        }
        public static void ComplianceScheduleOnUpdateAsNotActive(long ComplianceID, long createdByID, DateTime EffectiveDate)
        {
            try
            {
                bool chktran = true;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var instanceIds = (from row in entities.InternalComplianceInstances
                                       where row.InternalComplianceID == ComplianceID
                                       // && row.ID== 14943
                                       select row.ID).ToList();

                    if (instanceIds.Count > 0)
                    {

                        DateTime startDate = new DateTime(EffectiveDate.Year, EffectiveDate.Month, EffectiveDate.Day);
                        instanceIds.ForEach(entry =>
                        {


                            var ScheduleOnIds = (from row in entities.InternalComplianceScheduledOns
                                                 where row.InternalComplianceInstanceID == entry
                                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                 select row.ID).ToList();

                            if (ScheduleOnIds.Count > 0)
                            {

                                ScheduleOnIds.ForEach(entryScheduleOn =>
                                {
                                    InternalComplianceScheduledOn scheduleon = (from row in entities.InternalComplianceScheduledOns
                                                                       where row.ID == entryScheduleOn
                                                                       select row).FirstOrDefault();

                                    chktran = TratsactionExists(Convert.ToInt32(entry), Convert.ToInt32(entryScheduleOn));
                                    if (chktran == false)
                                    {
                                        scheduleon.IsActive = false;

                                        TaskScheduleOn taskscheduleon = (from row in entities.TaskScheduleOns
                                                                                    where row.ComplianceScheduleOnID == entryScheduleOn
                                                                                    select row).FirstOrDefault();
                                        if (taskscheduleon != null)
                                        {
                                            taskscheduleon.IsActive = false;
                                        }
                                    }
                                    InternalComplianceScheduledOn IsUpcomingtoDelete = (from row in entities.InternalComplianceScheduledOns
                                                                               where row.ID == entryScheduleOn && row.ScheduledOn >= startDate
                                                                               select row).FirstOrDefault();
                                    if (IsUpcomingtoDelete != null)
                                    {
                                        IsUpcomingtoDelete.IsUpcomingNotDeleted = false;

                                        TaskScheduleOn TaskIsUpcomingtoDelete = (from row in entities.TaskScheduleOns
                                                                                 where row.ComplianceScheduleOnID == entryScheduleOn && row.ScheduleOn >= startDate
                                                                                 select row).FirstOrDefault();

                                        if (TaskIsUpcomingtoDelete != null)
                                        {
                                            TaskIsUpcomingtoDelete.IsUpcomingNotDeleted = false;
                                        }

                                    }
                                    else
                                    {
                                        chktran = TratsactionExists(Convert.ToInt32(entry), Convert.ToInt32(entryScheduleOn));
                                        if (chktran == false)
                                        {
                                            InternalComplianceScheduledOn UsingTranIsUpcomingtoDelete = (from row in entities.InternalComplianceScheduledOns
                                                                                                         where row.ID == entryScheduleOn
                                                                                                         select row).FirstOrDefault();
                                            UsingTranIsUpcomingtoDelete.IsUpcomingNotDeleted = false;

                                            TaskScheduleOn TaskUsingTranIsUpcomingtoDelete = (from row in entities.TaskScheduleOns
                                                                                          where row.ComplianceScheduleOnID == entryScheduleOn
                                                                                                         select row).FirstOrDefault();
                                            if (TaskUsingTranIsUpcomingtoDelete != null)
                                            {
                                                TaskUsingTranIsUpcomingtoDelete.IsUpcomingNotDeleted = false;
                                            }

                                        }
                                    }
                                });
                                entities.SaveChanges();
                            }
                            H_Edit_InternalCompliance H_Edit_Compliance = new H_Edit_InternalCompliance()
                            {
                                ComplianceID = ComplianceID,
                                PrevComplianceInstanceID = entry,
                                EffectiveDate = DateTime.UtcNow,
                                Createdby = Convert.ToInt32(createdByID),
                                CreatedOn = DateTime.Now,

                            };
                            CreateHistoryEditCompliance(H_Edit_Compliance);
                        });

                        entities.SaveChanges();

                    }
                }
            }
            catch (Exception ex)
            {
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        public static InternalComplianceScheduledOn GetLastScheduleOnByInstance(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleObj = (from row in entities.InternalComplianceScheduledOns
                                   where row.InternalComplianceInstanceID == complianceInstanceID
                                   && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                   orderby row.ScheduledOn descending, row.ID descending
                                   select row).FirstOrDefault();

                return scheduleObj;
            }
        }

        public static DateTime? GetComplianceInstanceScheduledon(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime ScheduleOnID = (from row in entities.InternalComplianceInstances
                                         where row.ID == ComplianceInstanceID
                                         select row.StartDate).FirstOrDefault();
                if (ScheduleOnID != null)
                {
                    return ScheduleOnID;
                }
                else
                {
                    return null;
                }

            }
        }
        public static Tuple<DateTime, string, long> GetNextDatePeriodically(DateTime scheduledOn, long complianceID, long ComplianceInstanceID)
        {
            int formonthscheduleR = -1;
            var complianceSchedule = GetScheduleByComplianceID(complianceID);//.OrderBy(entry => entry.ForMonth).ToList();
            var objCompliance = GetByID(complianceID);
            DateTime date = complianceSchedule.Select(row => new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2)))).Where(row => row > scheduledOn).FirstOrDefault();
            var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            if (objCompliance.IFrequency == 3 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (objCompliance.IFrequency == 5 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (objCompliance.IFrequency == 6 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 6, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (objCompliance.IFrequency == 2)
            {
                List<Tuple<DateTime, long>> complianceScheduleDates;
                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "07").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "07").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" || row.SpecialDate.Substring(2, 2) == "10").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "10").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                long ActualForMonth = 0;
                if (objCompliance.IFrequency == 2)
                {

                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 > Convert.ToDateTime(lastSchedule.ScheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.IFrequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.IFrequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                };
            }
            string forMonth = string.Empty;

            if (complianceSchedule.Count > 0)
            {
                string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

                var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                forMonth = GetForMonthPeriodically(date, ForMonthSchedule.ForMonth, objCompliance.IFrequency);
                formonthscheduleR = ForMonthSchedule.ForMonth;
            }


            return new Tuple<DateTime, string, long>(date, forMonth, formonthscheduleR);


        }

        public static void CreateScheduleOnEffectiveDate(DateTime scheduledOn, long complianceInstanceId, long complianceID, long createdByID, string creatdByName)
        {
            long CreateScheduleOnErroComplianceid = -1;
            long CreateScheduleOnErrocomplianceInstanceId = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    DateTime nextDate = scheduledOn;
                    DateTime curruntDate = DateTime.UtcNow;


                    var compliances = (from row in entities.InternalCompliances
                                       where row.ID == complianceID
                                       select row).FirstOrDefault();

                    if (compliances.IFrequency == 7)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(1);
                    }
                    else if (compliances.IFrequency == 8)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(30);
                    }
                    else
                    {
                        curruntDate = DateTime.UtcNow;
                    }

                    while (nextDate < curruntDate)
                    {
                        Tuple<DateTime, string, long> nextDateMonth;

                        if (compliances.IFrequency == 7)
                       {
                            #region Frequancy Daily
                            
                            #endregion

                        }
                        else if (compliances.IFrequency == 8)
                        {
                            #region Frequancy Weekly
                            CreateScheduleOnErroComplianceid = complianceID;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = Business.InternalComplianceManagement.GetNextDateWeekly(nextDate, complianceID, complianceInstanceId, Convert.ToInt32(compliances.DueWeekDay));
                            long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
                                                 where row.InternalComplianceInstanceID == complianceInstanceId && row.ScheduledOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                InternalComplianceScheduledOn complianceScheduleon = new InternalComplianceScheduledOn();
                                complianceScheduleon.InternalComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduledOn = nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                entities.InternalComplianceScheduledOns.Add(complianceScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDateMonth.Item1;

                                InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                {
                                    InternalComplianceInstanceID = complianceInstanceId,
                                    InternalComplianceScheduledOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,                                    
                                    Remarks = "New internal compliance assigned."
                                };

                                CreateTransaction(transaction);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Frequency

                            if (compliances.ISubComplianceType == 0)
                            {
                                nextDateMonth = new Tuple<DateTime, string, long>(nextDate.AddDays(Convert.ToDouble(compliances.IDueDate)), "", 0);

                            }
                            else if (compliances.ISubComplianceType == 1 || compliances.ISubComplianceType == 2)
                            {
                                CreateScheduleOnErroComplianceid = complianceID;
                                nextDateMonth = Business.InternalComplianceManagement.GetNextDatePeriodically(nextDate, complianceID, complianceInstanceId);

                            }
                            else
                            {
                                CreateScheduleOnErroComplianceid = complianceID;
                                CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                                nextDateMonth = Business.InternalComplianceManagement.GetEffectiveNextDate(nextDate, complianceID, complianceInstanceId);

                            }
                            long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
                                                 where row.InternalComplianceInstanceID == complianceInstanceId
                                                 // && row.ScheduleOn == nextDateMonth.Item1 
                                                 && row.ForMonth == nextDateMonth.Item2
                                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                InternalComplianceScheduledOn complianceScheduleon = new InternalComplianceScheduledOn();
                                complianceScheduleon.InternalComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduledOn = nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                entities.InternalComplianceScheduledOns.Add(complianceScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDateMonth.Item1;

                                InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                {
                                    InternalComplianceInstanceID = complianceInstanceId,
                                    InternalComplianceScheduledOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                CreateTransaction(transaction);
                                #region Task Scheduele
                                long CustomerBranchID = GetCustomerBranchID(complianceInstanceId);
                                var Taskapplicable = TaskManagment.TaskApplicable(CustomerBranchID);
                                if (Taskapplicable != null)
                                {

                                    if (Taskapplicable == 1)
                                    {
                                        var TaskIDList = (from row in entities.Tasks
                                                          join row1 in entities.TaskComplianceMappings
                                                          on row.ID equals row1.TaskID
                                                          where row1.ComplianceID == complianceID
                                                          && row.TaskType == 2 && row.ParentID == null
                                                          && row.Isdeleted == false && row.IsActive == true
                                                          select row.ID).ToList();

                                        if (TaskIDList.Count > 0)
                                        {
                                            TaskIDList.ForEach(entrytask =>
                                            {
                                                List<TaskNameValue> TaskSubTaskList = new List<TaskNameValue>();

                                                var task = (from row in entities.Tasks
                                                            where row.ID == entrytask
                                                            select new { row.ID, row.ParentID, row.DueDays }).SingleOrDefault();

                                                TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long) task.ID, ParentID = (int?) task.ParentID, ActualDueDays = (int) task.DueDays });


                                                long? parentID = entrytask;
                                                while (parentID.HasValue)
                                                {
                                                    var subtask = (from row in entities.Tasks
                                                                   where row.ParentID == parentID.Value
                                                                   select new { row.ID, row.ParentID, row.DueDays }).SingleOrDefault();

                                                    if (subtask != null)
                                                    {
                                                        TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long) subtask.ID, ParentID = (int?) subtask.ParentID, ActualDueDays = (int) subtask.DueDays });
                                                        parentID = subtask.ID;
                                                    }
                                                    else
                                                    {
                                                        break;
                                                    }
                                                }

                                                TaskSubTaskList.ForEach(entryTaskSubTaskList =>
                                                {

                                                    var taskInstances = (from row in entities.TaskInstances
                                                                         where row.IsDeleted == false && row.TaskId == entryTaskSubTaskList.TaskID
                                                                         select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

                                                    taskInstances.ForEach(entrytaskinstance =>
                                                    {
                                                        TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
                                                        taskScheduleOn.TaskInstanceID = entrytaskinstance.ID;
                                                        taskScheduleOn.ComplianceScheduleOnID = complianceScheduleon.ID;
                                                        taskScheduleOn.ForMonth = nextDateMonth.Item2;
                                                        taskScheduleOn.ForPeriod = nextDateMonth.Item3;
                                                        taskScheduleOn.ScheduleOn = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                        taskScheduleOn.IsActive = true;
                                                        entities.TaskScheduleOns.Add(taskScheduleOn);
                                                        entities.SaveChanges();

                                                        var AssignedTaskRole = GetAssignedTaskUsers((int) entrytaskinstance.ID);
                                                        var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                                        if (performerTaskRole != null)
                                                        {
                                                            var user = UserManagement.GetByID((int) performerTaskRole.UserID);
                                                            TaskTransaction tasktransaction = new TaskTransaction()
                                                            {
                                                                TaskInstanceId = entrytaskinstance.ID,
                                                                TaskScheduleOnID = taskScheduleOn.ID,
                                                                ComplianceScheduleOnID = complianceScheduleon.ID,
                                                                CreatedBy = performerTaskRole.UserID,
                                                                CreatedByText = user.FirstName + " " + user.LastName,
                                                                StatusId = 1,
                                                                Remarks = "New task assigned."
                                                            };

                                                            TaskManagment.CreateTaskTransaction(tasktransaction);

                                                            foreach (var roles in AssignedTaskRole)
                                                            {
                                                                if (roles.RoleID != 6)
                                                                {
                                                                    CreateTaskReminders(entrytaskinstance.TaskId, complianceID, roles.ID, nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays)), complianceScheduleon.ID);
                                                                }
                                                            }
                                                        }

                                                    });
                                                });
                                            });
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "InternalComplianceManagement";
                msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

            }
        }
        public static List<long> GetUpcomingReminderDaysDetail(long ComplianceAssignmentId, int FreqID, string Type)
        {
            List<long> output = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User User = (from row in entities.Users
                             join row1 in entities.InternalComplianceAssignments
                             on row.ID equals row1.UserID
                             where row1.ID == ComplianceAssignmentId
                             && row.IsActive == true
                             && row.IsDeleted == false
                             select row).FirstOrDefault();
                if (User != null)
                {
                    long customerID = Convert.ToInt32(User.CustomerID);
                    long UserID = Convert.ToInt32(User.ID);

                    var UserObj = (from row in entities.ReminderTemplate_UserMapping
                                   where row.CustomerID == customerID
                                   && row.UserID == UserID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   && row.Frequency == FreqID
                                   && row.Flag == "UN"
                                   select row).ToList();

                    if (UserObj.Count > 0)
                    {
                        output.Clear();
                        output = UserObj.Select(x => x.TimeInDays).ToList();
                    }
                    else
                    {
                        var CustObj = (from row in entities.ReminderTemplate_CustomerMapping
                                       where row.CustomerID == customerID
                                       && row.IsActive == true
                                       && row.Type == Type
                                       && row.Frequency == FreqID
                                       && row.Flag == "UN"
                                       select row).ToList();

                        if (CustObj.Count > 0)
                        {
                            output.Clear();
                            output = CustObj.Select(x => x.TimeInDays).ToList();
                        }
                        else
                        {
                            output.Clear();
                            //var StdObj = (from row in entities.ReminderTemplates
                            //              where row.Frequency == FreqID
                            //              && row.IsSubscribed == true
                            //              select row).ToList();

                            //if (StdObj.Count > 0)
                            //{
                            //    output.Clear();
                            //    output = StdObj.Select(x => Convert.ToInt64(x.TimeInDays)).ToList();
                            //}
                        }
                    }
                }
                return output;
            }
        }

        private static void CreateRemindersEffetiveDate(long complianceID, long ComplianceInstanceId, int ComplianceAssignmentId)
        {
            long CreateRemindersErrorcomplianceInstanceID = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var compliance = (from row in entities.InternalCompliances
                                      where row.ID == complianceID
                                      select row).FirstOrDefault();
                    //Time Based
                    if (compliance.IFrequency == 7)
                    {
                        #region Frequency Daily
                        #endregion
                    }
                    else if (compliance.IFrequency == 8)
                    {
                        #region Frequency Weekly
                        List<InternalComplianceScheduledOn> ScheduledOnList = (from row in entities.InternalComplianceScheduledOns
                                                                               where row.InternalComplianceInstanceID == ComplianceInstanceId
                                                                               select row).ToList();
                        CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;

                        ScheduledOnList.ForEach(entry =>
                        {
                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                            {
                                ComplianceAssignmentID = ComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = Convert.ToDateTime(entry.ScheduledOn),
                                RemindOn = Convert.ToDateTime(entry.ScheduledOn).Date,
                            };

                            reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                            entities.InternalComplianceReminders.Add(reminder);

                            InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                            {
                                ComplianceAssignmentID = ComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = Convert.ToDateTime(entry.ScheduledOn),
                                RemindOn = Convert.ToDateTime(entry.ScheduledOn).Date.AddDays(-1 * 2),
                            };

                            reminder1.Status = (byte) (reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                            entities.InternalComplianceReminders.Add(reminder1);
                        });
                        #endregion
                    }
                    else
                    {
                        List<long> getdays = GetUpcomingReminderDaysDetail(ComplianceAssignmentId, Convert.ToInt32(compliance.IFrequency), "I");

                        List<InternalComplianceScheduledOn> ScheduledOnList = (from row in entities.InternalComplianceScheduledOns
                                                                               where row.InternalComplianceInstanceID == ComplianceInstanceId
                                                                               && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                                               select row).ToList();

                        if (getdays.Count > 0)
                        {
                            foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                            {
                                foreach (var item in getdays)
                                {
                                    InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = sco.ScheduledOn,
                                        RemindOn = sco.ScheduledOn.Date.AddDays(-item),
                                    };
                                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.InternalComplianceReminders.Add(reminder1);
                                }
                            }
                        }
                        else
                        {
                            #region Normal Frequancy
                            if (compliance.ISubComplianceType == 0)
                            {

                                var transactiondoneScheduleid = (from row in entities.ComplianceTransactions
                                                                 where row.ComplianceInstanceId == ComplianceInstanceId
                                                                 && row.StatusId == 2
                                                                 select row.ID).ToList();

                                ScheduledOnList = ScheduledOnList.Where(entryr => !transactiondoneScheduleid.Contains(entryr.ID)).ToList();

                                CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;
                                //Standard
                                if (compliance.IReminderType == 0)
                                {
                                    if (compliance.IComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduledOn,
                                                RemindOn = sco.ScheduledOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.InternalComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else
                                    {
                                        foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduledOn,
                                                RemindOn = sco.ScheduledOn.Date.AddDays(-1 * 2),
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.InternalComplianceReminders.Add(reminder);

                                            InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduledOn,
                                                RemindOn = sco.ScheduledOn.Date.AddDays(-1 * 15),
                                            };
                                            reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled); //updated by Rahul on 26 April 2016
                                            entities.InternalComplianceReminders.Add(reminder1);
                                        }
                                    }
                                }
                                else//Custom
                                {
                                    if (compliance.IComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduledOn,
                                                RemindOn = sco.ScheduledOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.InternalComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else
                                    {
                                        ScheduledOnList.ForEach(entry =>
                                        {
                                            DateTime TempScheduled = entry.ScheduledOn.AddDays(-(Convert.ToDouble(compliance.IReminderBefore)));

                                            while (TempScheduled.Date < entry.ScheduledOn)
                                            {
                                                InternalComplianceReminder reminder = new InternalComplianceReminder()
                                                {
                                                    ComplianceAssignmentID = ComplianceAssignmentId,
                                                    ReminderTemplateID = 1,
                                                    ComplianceDueDate = entry.ScheduledOn,
                                                    RemindOn = TempScheduled.Date,
                                                };

                                                reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                                entities.InternalComplianceReminders.Add(reminder);

                                                TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.IReminderGap));
                                            }

                                        });

                                    }
                                }

                            }//Function Based Start
                            else
                            {
                                //List<InternalComplianceScheduledOn> ScheduledOnList = (from row in entities.InternalComplianceScheduledOns
                                //                                                       where row.InternalComplianceInstanceID == ComplianceInstanceId
                                //                                                       && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                //                                                       select row).ToList();


                                var transactiondoneScheduleid = (from row in entities.InternalComplianceTransactions
                                                                 where row.InternalComplianceInstanceID == ComplianceInstanceId
                                                                 && row.StatusId == 2
                                                                 select row.ID).ToList();

                                ScheduledOnList = ScheduledOnList.Where(entryr => !transactiondoneScheduleid.Contains(entryr.ID)).ToList();

                                CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;
                                if (compliance.IReminderType == 0)
                                {
                                    if (compliance.IComplianceType == 1 && compliance.CheckListTypeID == 0)
                                    {
                                        foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduledOn,
                                                RemindOn = sco.ScheduledOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.InternalComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else if (compliance.IComplianceType == 1 && compliance.CheckListTypeID == 1)
                                    {
                                        foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduledOn,
                                                RemindOn = sco.ScheduledOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.InternalComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else if (compliance.IComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduledOn,
                                                RemindOn = sco.ScheduledOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.InternalComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else
                                    {
                                        if (compliance.IFrequency.HasValue)
                                        {
                                            var reminders = (from row in entities.ReminderTemplates
                                                             where row.Frequency == compliance.IFrequency.Value && row.IsSubscribed == true
                                                             select row).ToList();

                                            foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                                            {
                                                reminders.ForEach(day =>
                                                {
                                                    InternalComplianceReminder reminder = new InternalComplianceReminder()
                                                    {
                                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                                        ReminderTemplateID = day.ID,
                                                        ComplianceDueDate = sco.ScheduledOn,
                                                        RemindOn = sco.ScheduledOn.Date.AddDays(-1 * day.TimeInDays),
                                                    };

                                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                                    entities.InternalComplianceReminders.Add(reminder);

                                                });
                                            }
                                        }
                                        else //added by rahul on 16 FEB 2016 for Event
                                        {
                                            if (compliance.IComplianceType == 0) //&& compliance.EventID != null
                                            {
                                                foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                                                {
                                                    InternalComplianceReminder reminder = new InternalComplianceReminder()
                                                    {
                                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                                        ReminderTemplateID = 1,
                                                        ComplianceDueDate = sco.ScheduledOn,
                                                        RemindOn = sco.ScheduledOn.Date.AddDays(-1 * 2),
                                                    };
                                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                    entities.InternalComplianceReminders.Add(reminder);

                                                    InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                                                    {
                                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                                        ReminderTemplateID = 1,
                                                        ComplianceDueDate = sco.ScheduledOn,
                                                        RemindOn = sco.ScheduledOn.Date.AddDays(-1 * 15),
                                                    };
                                                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                    entities.InternalComplianceReminders.Add(reminder1);
                                                }
                                            }
                                        } //added by rahul on 16 FEB 2016 for Event
                                    }//Added by rahul on 4 FEB 2016
                                }
                                else
                                {

                                    ScheduledOnList.ForEach(entry =>
                                    {
                                        DateTime TempScheduled = entry.ScheduledOn.AddDays(-(Convert.ToDouble(compliance.IReminderBefore)));

                                        while (TempScheduled.Date < entry.ScheduledOn)
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = entry.ScheduledOn,
                                                RemindOn = TempScheduled.Date,
                                            };

                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                            entities.InternalComplianceReminders.Add(reminder);

                                            TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.IReminderGap));
                                        }

                                    });
                                }
                            }
                            #endregion
                        }
                    }
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "InternalComplianceManagement";
                msg.FunctionName = "CreateReminders" + "complianceInstanceID" + CreateRemindersErrorcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as InternalCreateReminders Function", "Internal CreateReminders" + "complianceInstanceID" + CreateRemindersErrorcomplianceInstanceID);


            }
        }

        public static void UpdateOldData(long ComplianceID, long createdByID, string creatdByName, DateTime EffectiveDate)
        {
            try
            {
                var complianceList = new List<ComplianceAsignmentProperties>();
                List<Tuple<InternalComplianceInstance, InternalComplianceAssignment>> assignments = new List<Tuple<InternalComplianceInstance, InternalComplianceAssignment>>();
                ComplianceAsignmentProperties rmdata = new ComplianceAsignmentProperties();
                Dictionary<int, DateTime> startDates = new Dictionary<int, DateTime>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var instanceIds = (from row in entities.InternalComplianceInstances
                                       where row.InternalComplianceID == ComplianceID && row.IsDeleted == false
                                       //  && row.ID== 14943
                                       select row.ID).ToList();
                    if (instanceIds.Count > 0)
                    {

                        //IsDeleted must by 1 of selected instanceIds
                        instanceIds.ForEach(entry =>
                        {

                            InternalComplianceInstance Existinstance = (from row in entities.InternalComplianceInstances
                                                                where row.ID == entry && row.IsDeleted == false
                                                                select row).FirstOrDefault();

                            Existinstance.StartDate = EffectiveDate;//edit effective date of Instance
                            entities.SaveChanges();
                            CreateScheduleOnEffectiveDate(EffectiveDate, entry, Existinstance.InternalComplianceID, createdByID, creatdByName);//create ScheduleOn according to new Effective Date

                            var assignmentIds = (from row in entities.InternalComplianceAssignments
                                                 where row.InternalComplianceInstanceID == entry
                                                 select row.ID).ToList();

                            if (assignmentIds.Count > 0)
                            {
                                assignmentIds.ForEach(entryAssign =>
                                {
                                    var remindersIds = (from row in entities.InternalComplianceReminders
                                                        where row.ComplianceAssignmentID == entryAssign
                                                        select row.ID).ToList();

                                    if (remindersIds.Count > 0)
                                    {
                                        remindersIds.ForEach(reminderentry =>
                                        {

                                            InternalComplianceReminder ExitReminder = (from row in entities.InternalComplianceReminders
                                                                               where row.ID == reminderentry
                                                                               select row).FirstOrDefault();

                                            ExitReminder.Status = 2;
                                        });
                                    }
                                    CreateRemindersEffetiveDate(Existinstance.InternalComplianceID, entry, entryAssign);//for each assignment create Reminders
                                });
                            }
                        });
                        entities.SaveChanges();
                    }
                }//--test
            }
            catch (Exception ex)
            {
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void UpdateComplianceUpdatedOn(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.InternalCompliances
                                         where row.ID == complianceID
                                         select row).FirstOrDefault();

                compliance.UpdatedOn = DateTime.Now;             
                entities.SaveChanges();


            }
        }
        public static DateTime? GetInternalComplianceInstanceScheduledon(long InternalComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime ScheduleOnID = (from row in entities.InternalComplianceInstances
                                         where row.ID == InternalComplianceInstanceId
                                         select row.StartDate).FirstOrDefault();
                if (ScheduleOnID != null)
                {
                    return ScheduleOnID;
                }
                else
                {
                    return null;
                }

            }
        }
        public static List<long> GetInstanceIDfromReviewer(long UserID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {


               var InstanceIDs = (from row in entities.InternalComplianceAssignments
                                  where row.UserID == UserID && row.RoleID == 4
                                  select row.InternalComplianceInstanceID).ToList();

               return InstanceIDs;
           }
       }

       public static List<long> GetPerformerListUsingReviewer(long UserID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {


               var InstanceIDs = (from row in entities.InternalComplianceAssignments
                                  where row.UserID == UserID && row.RoleID == 4
                                  select row.InternalComplianceInstanceID).ToList();

               var performerList = (from row in entities.InternalComplianceAssignments
                                    where InstanceIDs.Contains(row.InternalComplianceInstanceID) && row.RoleID == 3
                                    select row.UserID).ToList();

               return performerList;
           }
       }
       //GetComplianceReminderNotificationsByUserID
       public static List<InternalComplianceReminderListView> GetComplianceReminderNotificationsByUserID(int userID, string filter = null)
       {
           DateTime date = DateTime.Now.Date;

           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
              
               List<InternalComplianceReminderListView> complianceReminders = (from row in entities.InternalComplianceReminderListViews
                                                                       where row.UserID == userID && row.RemindOn >= date && row.Status == 0
                                                                       select row).OrderBy(entry => entry.RemindOn).ToList();


               if (!string.IsNullOrEmpty(filter))
               {
                   complianceReminders = complianceReminders.Where(entry => entry.CustomerBranchName.ToUpper().Contains(filter.ToUpper()) || entry.IShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Description.ToUpper().Contains(filter.ToUpper()) || entry.Role.ToUpper().Contains(filter.ToUpper())).ToList();
               }

               return complianceReminders;
           }

       }

       public static InternalComplianceReminderView GetComplianceReminderNotificationsByID(int reminderID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var complianceReminder = (from row in entities.InternalComplianceReminderViews
                                         where row.ComplianceReminderID == reminderID
                                         select row).FirstOrDefault();

               return complianceReminder;
           }
       }

       public static void CreateReminder(InternalComplianceReminder reminder)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               reminder.ReminderTemplateID = null;
               reminder.Status = (byte)ReminderStatus.Pending;

               entities.InternalComplianceReminders.Add(reminder);
               entities.SaveChanges();
           }
       }

       public static void UpdateReminder(InternalComplianceReminder reminder)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {


               InternalComplianceReminder reminderToUpdate = (from row in entities.InternalComplianceReminders
                                                      where row.ID == reminder.ID
                                                      select row).FirstOrDefault();

               reminderToUpdate.ComplianceAssignmentID = reminder.ComplianceAssignmentID;
               reminderToUpdate.RemindOn = reminder.RemindOn;

               entities.SaveChanges();
           }
       }

       public static List<InternalComplianceReminder> GetAllReminders_WithID()
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var Reminders_WithIDQuery = (from row in entities.InternalComplianceReminders              
                select row).ToList();

               return Reminders_WithIDQuery.OrderByDescending(entry => entry.ID).ToList();
           }
       }


       public static List<InternalComplianceInstanceAssignmentView> GetAllInstances(int branchID = -1, int userID = -1, int customerId = -1, string filter = null)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var complianceInstancesQuery = (from row in entities.InternalComplianceInstanceAssignmentViews
                                               select row);

               if (customerId != -1)
               {
                   complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
               }

               if (branchID != -1)
               {
                   complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
               }

               if (userID != -1)
               {
                   complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
               }

               if (!string.IsNullOrEmpty(filter))
               {
                   complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.IShortDescription.Contains(filter) || entry.Branch.Contains(filter) || entry.Role.Contains(filter));
               }

               return complianceInstancesQuery.OrderByDescending(entry => entry.ScheduledOn).ToList();

           }
       }

        public static List<InternalCompliance> GetByTypeTempInternal(int Customerid,int complianceTypeID, int complianceCatagoryId, bool setApprover = false, int lcCustomerBranch = -1, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalCompliance> Icompliances = (from row in entities.InternalCompliances
                                                         where row.IsDeleted == false && row.IComplianceType != 1
                                                         && row.CustomerID== Customerid
                                                         select row).ToList();

                var licensecompiancedetails= (from row in entities.Lic_tbl_LicType_InternalCompliance_Mapping
                                              where row.IsDeleted == false
                                              && row.CustomerID == Customerid
                                              select row.ComplianceID).ToList();

                if (Icompliances.Count>0)
                {
                    Icompliances = Icompliances.Where(entry => !licensecompiancedetails.Contains(entry.ID)).ToList();
                }

                if (complianceTypeID != -1)
                {
                    Icompliances = Icompliances.Where(entry => entry.IComplianceTypeID == complianceTypeID).ToList();
                }
                if (complianceCatagoryId != -1)
                {
                    Icompliances = Icompliances.Where(entry => entry.IComplianceCategoryID == complianceCatagoryId).ToList();
                }

                var TempQuery = (from row in entities.TempAssignmentTableInternals

                                 where row.CustomerBranchID == lcCustomerBranch && row.IsActive == true && row.ComplianceFlag == "S"
                                                                && row.UserID != -1
                                 select row.ComplianceId).ToList();

                Icompliances.RemoveAll(a => TempQuery.Contains(a.ID));

                var TempQueryAssignInstance = (from row in entities.InternalComplianceInstances
                                               join row2 in entities.InternalCompliances
                                               on row.InternalComplianceID equals row2.ID
                                               join row1 in entities.InternalComplianceAssignments
                                               on row.ID equals row1.InternalComplianceInstanceID
                                               where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                               && row2.IComplianceType !=1
                                               select row.InternalComplianceID).ToList();

                Icompliances.RemoveAll(b => TempQueryAssignInstance.Contains(b.ID));

                
                if (!string.IsNullOrEmpty(filter))
                {
                    Icompliances = Icompliances.Where(entry => entry.IShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.IRequiredFrom.ToUpper().Contains(filter.ToUpper())).ToList();
                }
                return Icompliances;
            }
        }

        public static List<InternalCompliance> GetByTypeTempInternalCheckList(int complianceTypeID, int complianceCatagoryId, bool setApprover = false, int lcCustomerBranch = -1, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalCompliance> Icompliances = (from row in entities.InternalCompliances
                                                         where row.IsDeleted == false && row.IComplianceType == 1
                                                         select row).ToList();
                if (complianceTypeID != -1)
                {
                    Icompliances = Icompliances.Where(entry => entry.IComplianceTypeID == complianceTypeID).ToList();
                }
                if (complianceCatagoryId != -1)
                {
                    Icompliances = Icompliances.Where(entry => entry.IComplianceCategoryID == complianceCatagoryId).ToList();
                }

                var TempQuery = (from row in entities.TempAssignmentTableInternals

                                 where row.CustomerBranchID == lcCustomerBranch && row.IsActive == true && row.ComplianceFlag == "C"
                                                               && row.UserID != -1
                                 select row.ComplianceId).ToList();

                Icompliances.RemoveAll(a => TempQuery.Contains(a.ID));

                var TempQueryAssignInstance = (from row in entities.InternalComplianceInstances
                                               join row2 in entities.InternalCompliances
                                               on row.InternalComplianceID equals row2.ID
                                               join row1 in entities.InternalComplianceAssignments
                                               on row.ID equals row1.InternalComplianceInstanceID
                                               where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                               && row2.IComplianceType == 1
                                               select row.InternalComplianceID).ToList();

                Icompliances.RemoveAll(b => TempQueryAssignInstance.Contains(b.ID));

                if (!string.IsNullOrEmpty(filter))
                {
                    Icompliances = Icompliances.Where(entry => entry.IShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.IRequiredFrom.ToUpper().Contains(filter.ToUpper())).ToList();
                }
                return Icompliances;
            }
        }
        public static List<InternalCompliance> GetByType(int CustomerID,int complianceTypeID, int complianceCatagoryId, bool setApprover = false, int lcCustomerBranch = -1, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalCompliance> Icompliances = (from row in entities.InternalCompliances
                                                         where row.IsDeleted == false
                                                         select row).ToList();

                var licensecompiancedetails = (from row in entities.Lic_tbl_LicType_InternalCompliance_Mapping
                                               where row.IsDeleted ==false
                                               && row.CustomerID == CustomerID
                                               select row.ComplianceID).ToList();

                if (Icompliances.Count > 0)
                {
                    Icompliances = Icompliances.Where(entry => !licensecompiancedetails.Contains(entry.ID)).ToList();
                }


                if (complianceTypeID != -1)
                {
                    Icompliances = Icompliances.Where(entry => entry.IComplianceTypeID == complianceTypeID).ToList();
                }
                if (complianceCatagoryId != -1)
                {
                    Icompliances = Icompliances.Where(entry => entry.IComplianceCategoryID == complianceCatagoryId).ToList();
                }
                if (setApprover != false)
                {
                    List<long> query = (from row in entities.InternalComplianceInstances
                                        join ca in entities.InternalComplianceAssignments.Where(entry => entry.RoleID == 3 || entry.RoleID == 4 || entry.RoleID == 5)
                                        on row.ID equals ca.InternalComplianceInstanceID
                                        where row.CustomerBranchID == lcCustomerBranch
                                        select row.InternalComplianceID).ToList();
                    Icompliances = Icompliances.Where(entry => query.Contains(entry.ID)).ToList();
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    Icompliances = Icompliances.Where(entry => entry.IShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.IRequiredFrom.ToUpper().Contains(filter.ToUpper())).ToList();
                }
                return Icompliances;
            }
        }
        public static List<InternalCompliance> GetByType(int complianceTypeID, int complianceCatagoryId, bool setApprover = false, int lcCustomerBranch = -1, string filter = null)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {             
               List<InternalCompliance>  Icompliances = (from row in entities.InternalCompliances
                                                         where row.IsDeleted == false
                                                         select row).ToList();
                

                if (complianceTypeID != -1)
               {
                   Icompliances = Icompliances.Where(entry => entry.IComplianceTypeID == complianceTypeID).ToList();
               }
               if (complianceCatagoryId != -1)
               {
                   Icompliances = Icompliances.Where(entry => entry.IComplianceCategoryID == complianceCatagoryId).ToList();
               }
               if (setApprover != false)
               {
                   List<long> query = (from row in entities.InternalComplianceInstances
                                       join ca in entities.InternalComplianceAssignments.Where(entry => entry.RoleID == 3 || entry.RoleID == 4 || entry.RoleID == 5)
                                       on row.ID equals ca.InternalComplianceInstanceID
                                       where row.CustomerBranchID == lcCustomerBranch
                                       select row.InternalComplianceID).ToList();
                   Icompliances = Icompliances.Where(entry => query.Contains(entry.ID)).ToList();
               }
               if (!string.IsNullOrEmpty(filter))
               {
                   Icompliances = Icompliances.Where(entry => entry.IShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.IRequiredFrom.ToUpper().Contains(filter.ToUpper())).ToList();
               }
               return Icompliances;
           }
       }
        public static void CreateInstances(List<Tuple<InternalComplianceInstance, InternalComplianceAssignment>> assignments, long createdByID, string creatdByName)
        {
            long CreateInstancesErrorInternalcomplianceInstanceID = -1;
            long CreateInstancesErrorInternalComplianceID = -1;
            try
            {
                if (assignments.Count > 0)
                {

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        assignments.ForEach(entry =>
                        {
                            //long InternalcomplianceInstanceID = (from row in entities.InternalComplianceInstances
                            //                                     where row.InternalComplianceID == entry.Item1.InternalComplianceID && row.CustomerBranchID == entry.Item1.CustomerBranchID
                            //                                     select row.ID).SingleOrDefault();

                            long InternalcomplianceInstanceID = (from CI in entities.InternalComplianceInstances
                                                                 where CI.InternalComplianceID == entry.Item1.InternalComplianceID
                                                         && CI.CustomerBranchID == entry.Item1.CustomerBranchID
                                                         && CI.IsDeleted == false
                                                         && CI.SequenceID == entry.Item1.SequenceID
                                                         select CI.ID).SingleOrDefault();
                            if (InternalcomplianceInstanceID <= 0)
                            {
                                entry.Item1.CreatedOn = DateTime.UtcNow;
                                entry.Item1.IsDeleted = false;
                                entities.InternalComplianceInstances.Add(entry.Item1);
                                entities.SaveChanges();
                                InternalcomplianceInstanceID = entry.Item1.ID;

                                var compliances = (from row in entities.InternalCompliances
                                                   where row.ID == entry.Item1.InternalComplianceID
                                                   select row).FirstOrDefault();

                                if (ComplianceIsEventBased(entry.Item1.InternalComplianceID))
                                {
                                    if (compliances.IComplianceOccurrence == 0)
                                    {
                                        InternalComplianceScheduledOn complianceScheduleon = new InternalComplianceScheduledOn();
                                        complianceScheduleon.InternalComplianceInstanceID = InternalcomplianceInstanceID;
                                        if (compliances.IOneTimeDate.HasValue == true)
                                        {
                                            complianceScheduleon.ScheduledOn = (DateTime)compliances.IOneTimeDate;
                                        }

                                        complianceScheduleon.ForMonth = null;
                                        complianceScheduleon.ForPeriod = null;
                                        complianceScheduleon.IsActive = true;
                                        complianceScheduleon.IsUpcomingNotDeleted = true;
                                        entities.InternalComplianceScheduledOns.Add(complianceScheduleon);
                                        entities.SaveChanges();


                                        InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                        {
                                            InternalComplianceInstanceID = InternalcomplianceInstanceID,
                                            InternalComplianceScheduledOnID = complianceScheduleon.ID,
                                            CreatedBy = createdByID,
                                            CreatedByText = creatdByName,
                                            StatusId = 1,
                                            Remarks = "New internal compliance assigned."
                                        };

                                        CreateTransaction(transaction);
                                    }
                                    else
                                    {
                                        CreateInstancesErrorInternalcomplianceInstanceID = InternalcomplianceInstanceID;
                                        CreateInstancesErrorInternalComplianceID = entry.Item1.InternalComplianceID;
                                        CreateScheduleOn(entry.Item1.StartDate, InternalcomplianceInstanceID, entry.Item1.InternalComplianceID, createdByID, creatdByName);
                                      
                                    }
                                }
                            }
                            else
                            {
                                entry.Item1.ID = InternalcomplianceInstanceID;
                            }
                            long complianceAssignment = (from row in entities.InternalComplianceAssignments
                                                         where row.InternalComplianceInstanceID == entry.Item1.ID && row.RoleID == entry.Item2.RoleID
                                                         select row.UserID).FirstOrDefault();

                            if (entry.Item2.RoleID != 6)
                            {
                                if (complianceAssignment <= 0)
                                {
                                    entry.Item2.InternalComplianceInstanceID = InternalcomplianceInstanceID;
                                    entities.InternalComplianceAssignments.Add(entry.Item2);
                                    entities.SaveChanges();
                                    if (ComplianceIsEventBased(entry.Item1.InternalComplianceID))
                                    {
                                        CreateReminders(entry.Item1.InternalComplianceID, InternalcomplianceInstanceID, entry.Item2.ID);

                                    }
                                }

                            }
                            else
                            {
                                
                                    entry.Item2.InternalComplianceInstanceID = InternalcomplianceInstanceID;
                                    entities.InternalComplianceAssignments.Add(entry.Item2);
                                    entities.SaveChanges();

                                    if (entry.Item2.RoleID != 6)
                                    {
                                        if (ComplianceIsEventBased(entry.Item1.InternalComplianceID))
                                        {
                                            CreateReminders(entry.Item1.InternalComplianceID, InternalcomplianceInstanceID, entry.Item2.ID);

                                        }
                                    }
                            }
                        });
                    }


                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "InternalCompliance";
                msg.FunctionName = "CreateInstances" + "InternalComplianceID" + CreateInstancesErrorInternalComplianceID + "InternalcomplianceInstanceID" + CreateInstancesErrorInternalcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as Internal CreateInstances Function", "Internal CreateInstances" + "InternalComplianceID" + CreateInstancesErrorInternalComplianceID + "InternalcomplianceInstanceID" + CreateInstancesErrorInternalcomplianceInstanceID);

            }
        }


        //added by Rahul for save Error Data in Database
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
       {

           using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
           {
               objEscalation.ForEach(entry =>
               {
                   entities.LogMessages.Add(entry);
                   entities.SaveChanges();

               });
           }

       }

      

        //Holiday Change ScheduleOn
        //public static void CreateScheduleOn(DateTime InternalscheduledOn, long InternalcomplianceInstanceId, long InternalcomplianceID, long createdByID, string creatdByName)
        //{
        //    long CreateScheduleOnErroInternalcomplianceID = -1;
        //    long CreateScheduleOnErroInternalcomplianceInstanceId = -1;
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            DateTime nextDate = InternalscheduledOn;

        //            var compliances = (from row in entities.InternalCompliances
        //                               where row.ID == InternalcomplianceID
        //                               select row).FirstOrDefault();
        //            DateTime curruntDate;
        //            if (compliances.IFrequency == 7)
        //            {
        //                curruntDate = DateTime.UtcNow.AddDays(1);
        //            }
        //            else if (compliances.IFrequency == 8)
        //            {
        //                curruntDate = DateTime.UtcNow.AddDays(30);
        //            }
        //            else
        //            {
        //                curruntDate = DateTime.UtcNow;
        //            }

        //            while (nextDate < curruntDate)
        //            {
        //                Tuple<DateTime, string, long> nextDateMonth;

        //                if (compliances.IFrequency == 7)
        //                {
        //                    #region Frequancy Daily
        //                    CreateScheduleOnErroInternalcomplianceID = InternalcomplianceID;
        //                    CreateScheduleOnErroInternalcomplianceInstanceId = InternalcomplianceInstanceId;
        //                    nextDateMonth = Business.InternalComplianceManagement.GetNextDateDaily(nextDate, InternalcomplianceID, InternalcomplianceInstanceId, false);
        //                    //}
        //                    long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
        //                                         where row.InternalComplianceInstanceID == InternalcomplianceInstanceId && row.ScheduledOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
        //                                         select row.ID).SingleOrDefault();

        //                    if (ScheduleOnID <= 0)
        //                    {
        //                        InternalComplianceScheduledOn complianceScheduleon = new InternalComplianceScheduledOn();
        //                        complianceScheduleon.InternalComplianceInstanceID = InternalcomplianceInstanceId;
        //                        complianceScheduleon.ScheduledOn = nextDateMonth.Item1;
        //                        complianceScheduleon.ForMonth = nextDateMonth.Item2;
        //                        complianceScheduleon.ForPeriod = nextDateMonth.Item3;
        //                        complianceScheduleon.IsActive = true;
        //                        complianceScheduleon.IsUpcomingNotDeleted = true;
        //                        entities.InternalComplianceScheduledOns.Add(complianceScheduleon);
        //                        entities.SaveChanges();
        //                        nextDate = nextDateMonth.Item1;

        //                        InternalComplianceTransaction transaction = new InternalComplianceTransaction()
        //                        {
        //                            InternalComplianceInstanceID = InternalcomplianceInstanceId,
        //                            InternalComplianceScheduledOnID = complianceScheduleon.ID,
        //                            CreatedBy = createdByID,
        //                            CreatedByText = creatdByName,
        //                            StatusId = 1,
        //                            StatusChangedOn = DateTime.UtcNow,
        //                            Remarks = "New internal compliance assigned."
        //                        };

        //                        CreateTransaction(transaction, null, null, null);
        //                    }
        //                    #endregion
        //                }
        //                else if (compliances.IFrequency == 8)
        //                {
        //                    #region Frequancy Weekly
        //                    CreateScheduleOnErroInternalcomplianceID = InternalcomplianceID;
        //                    CreateScheduleOnErroInternalcomplianceInstanceId = InternalcomplianceInstanceId;
        //                    nextDateMonth = Business.InternalComplianceManagement.GetNextDateWeekly(nextDate, InternalcomplianceID, InternalcomplianceInstanceId, Convert.ToInt32(compliances.DueWeekDay));
        //                    //}
        //                    long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
        //                                         where row.InternalComplianceInstanceID == InternalcomplianceInstanceId && row.ScheduledOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
        //                                         select row.ID).SingleOrDefault();

        //                    if (ScheduleOnID <= 0)
        //                    {
        //                        #region Holiday Logic 

        //                        var HolidayCount = HolidayMasterManagement.CheckHolidayInternal(InternalcomplianceID, InternalcomplianceInstanceId, nextDateMonth.Item1);

        //                        DateTime ScheduleOn;
        //                        if (HolidayCount > 0)
        //                        {
        //                            ScheduleOn = nextDateMonth.Item1.AddDays(HolidayCount);
        //                        }
        //                        else
        //                        {
        //                            ScheduleOn = nextDateMonth.Item1;
        //                        }

        //                        #endregion

        //                        InternalComplianceScheduledOn complianceScheduleon = new InternalComplianceScheduledOn();
        //                        complianceScheduleon.InternalComplianceInstanceID = InternalcomplianceInstanceId;
        //                        complianceScheduleon.ScheduledOn = ScheduleOn; // nextDateMonth.Item1;
        //                        complianceScheduleon.ForMonth = nextDateMonth.Item2;
        //                        complianceScheduleon.ForPeriod = nextDateMonth.Item3;
        //                        complianceScheduleon.IsActive = true;
        //                        complianceScheduleon.IsUpcomingNotDeleted = true;
        //                        entities.InternalComplianceScheduledOns.Add(complianceScheduleon);
        //                        entities.SaveChanges();
        //                        nextDate = nextDateMonth.Item1;

        //                        InternalComplianceTransaction transaction = new InternalComplianceTransaction()
        //                        {
        //                            InternalComplianceInstanceID = InternalcomplianceInstanceId,
        //                            InternalComplianceScheduledOnID = complianceScheduleon.ID,
        //                            CreatedBy = createdByID,
        //                            CreatedByText = creatdByName,
        //                            StatusId = 1,
        //                            StatusChangedOn = DateTime.UtcNow,
        //                            Remarks = "New internal compliance assigned."
        //                        };

        //                        CreateTransaction(transaction, null, null, null);
        //                    }
        //                    #endregion
        //                }
        //                else
        //                {
        //                    #region Normal Frequancy 
        //                    if (compliances.ISubComplianceType == 0)
        //                    {
        //                        nextDateMonth = new Tuple<DateTime, string, long>(nextDate.AddDays(Convert.ToDouble(compliances.IDueDate)), "", 0);
        //                    }
        //                    else if (compliances.ISubComplianceType == 1 || compliances.ISubComplianceType == 2)
        //                    {
        //                        CreateScheduleOnErroInternalcomplianceID = InternalcomplianceID;
        //                        nextDateMonth = Business.InternalComplianceManagement.GetNextDatePeriodically(nextDate, InternalcomplianceID);
        //                    }
        //                    else
        //                    {
        //                        CreateScheduleOnErroInternalcomplianceID = InternalcomplianceID;
        //                        CreateScheduleOnErroInternalcomplianceInstanceId = InternalcomplianceInstanceId;
        //                        nextDateMonth = Business.InternalComplianceManagement.GetNextDate(nextDate, InternalcomplianceID, InternalcomplianceInstanceId, false);
        //                    }
        //                    long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
        //                                         where row.InternalComplianceInstanceID == InternalcomplianceInstanceId && row.ScheduledOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
        //                                         select row.ID).SingleOrDefault();

        //                    if (ScheduleOnID <= 0)
        //                    {
        //                        #region Holiday Logic 

        //                        var HolidayCount = HolidayMasterManagement.CheckHolidayInternal(InternalcomplianceID, InternalcomplianceInstanceId, nextDateMonth.Item1);

        //                        DateTime ScheduleOn;
        //                        if (HolidayCount > 0)
        //                        {
        //                            ScheduleOn = nextDateMonth.Item1.AddDays(HolidayCount);
        //                        }
        //                        else
        //                        {
        //                            ScheduleOn = nextDateMonth.Item1;
        //                        }

        //                        #endregion

        //                        InternalComplianceScheduledOn complianceScheduleon = new InternalComplianceScheduledOn();
        //                        complianceScheduleon.InternalComplianceInstanceID = InternalcomplianceInstanceId;
        //                        complianceScheduleon.ScheduledOn = ScheduleOn; // nextDateMonth.Item1;
        //                        complianceScheduleon.ForMonth = nextDateMonth.Item2;
        //                        complianceScheduleon.ForPeriod = nextDateMonth.Item3;
        //                        complianceScheduleon.IsActive = true;
        //                        complianceScheduleon.IsUpcomingNotDeleted = true;
        //                        entities.InternalComplianceScheduledOns.Add(complianceScheduleon);
        //                        entities.SaveChanges();
        //                        nextDate = nextDateMonth.Item1;

        //                        InternalComplianceTransaction transaction = new InternalComplianceTransaction()
        //                        {
        //                            InternalComplianceInstanceID = InternalcomplianceInstanceId,
        //                            InternalComplianceScheduledOnID = complianceScheduleon.ID,
        //                            CreatedBy = createdByID,
        //                            CreatedByText = creatdByName,
        //                            StatusId = 1,
        //                            StatusChangedOn = DateTime.UtcNow,
        //                            Remarks = "New internal compliance assigned."
        //                        };

        //                        CreateTransaction(transaction, null, null, null);

        //                        #region Task Scheduele
        //                        long CustomerBranchID = GetCustomerBranchID(InternalcomplianceInstanceId);
        //                        var Taskapplicable = TaskManagment.TaskApplicable(CustomerBranchID);
        //                        if (Taskapplicable != null)
        //                        {
        //                            if (Taskapplicable == 1)
        //                            {
        //                                var TaskIDList = (from row in entities.Tasks
        //                                                  join row1 in entities.TaskComplianceMappings
        //                                                  on row.ID equals row1.TaskID
        //                                                  where row1.ComplianceID == InternalcomplianceID
        //                                                  && row.TaskType == 2 && row.ParentID == null
        //                                                  && row.Isdeleted == false && row.IsActive == true
        //                                                  && (((row.Status != "A") || (row.Status == null))
        //                                                  || ((row.Status == "A") && (row.DeactivateOn > DateTime.Now)))
        //                                                  select row.ID).ToList();

        //                                if (TaskIDList.Count > 0)
        //                                {
        //                                    TaskIDList.ForEach(entrytask =>
        //                                    {

        //                                        List<TaskNameValue> TaskSubTaskList = new List<TaskNameValue>();

        //                                        bool? IsAfter = TaskManagment.GetTaskBeforeAfter(entrytask);

        //                                        if (IsAfter != null && IsAfter != true)
        //                                        {
        //                                            IsAfter = false;
        //                                        }

        //                                        var taskSubTaskList = (from row in entities.Tasks
        //                                                               join row1 in entities.TaskInstances
        //                                                               on row.ID equals row1.TaskId
        //                                                               where row.MainTaskID == entrytask && row1.CustomerBranchID == CustomerBranchID
        //                                                               select new { row.ID, row.ParentID, row.DueDays }).ToList();

        //                                        taskSubTaskList.ForEach(entrysubtask =>
        //                                        {
        //                                            TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long) entrysubtask.ID, ParentID = (int?) entrysubtask.ParentID, ActualDueDays = (int) entrysubtask.DueDays });

        //                                        });

        //                                        TaskSubTaskList.ForEach(entryTaskSubTaskList =>
        //                                        {
        //                                            var taskInstances = (from row in entities.TaskInstances
        //                                                                 where row.IsDeleted == false && row.TaskId == entryTaskSubTaskList.TaskID
        //                                                                 && row.ScheduledOn <= nextDateMonth.Item1
        //                                                                 && row.CustomerBranchID == CustomerBranchID
        //                                                                 select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

        //                                            DateTime schedueon;
        //                                            if (IsAfter == null)
        //                                            {
        //                                                schedueon = nextDateMonth.Item1.AddDays(Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
        //                                            }
        //                                            else
        //                                            {
        //                                                schedueon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
        //                                            }
        //                                            taskInstances.ForEach(entrytaskinstance =>
        //                                            {
        //                                                #region Holiday Logic 
        //                                                var HolidayTaskCount = HolidayMasterManagement.CheckHolidayInternal(InternalcomplianceID, InternalcomplianceInstanceId, schedueon);

        //                                                DateTime ScheduleOn1;
        //                                                if (HolidayTaskCount > 0)
        //                                                {
        //                                                    ScheduleOn1 = schedueon.AddDays(HolidayTaskCount);
        //                                                }
        //                                                else
        //                                                {
        //                                                    ScheduleOn1 = schedueon;
        //                                                }
        //                                                #endregion

        //                                                TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
        //                                                taskScheduleOn.TaskInstanceID = entrytaskinstance.ID;
        //                                                taskScheduleOn.ComplianceScheduleOnID = complianceScheduleon.ID;
        //                                                taskScheduleOn.ForMonth = nextDateMonth.Item2;
        //                                                taskScheduleOn.ForPeriod = nextDateMonth.Item3;
        //                                                taskScheduleOn.ScheduleOn = ScheduleOn1; // nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
        //                                                taskScheduleOn.IsActive = true;
        //                                                taskScheduleOn.IsUpcomingNotDeleted = true;
        //                                                entities.TaskScheduleOns.Add(taskScheduleOn);
        //                                                entities.SaveChanges();

        //                                                var AssignedTaskRole = GetAssignedTaskUsers((int) entrytaskinstance.ID);
        //                                                var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
        //                                                if (performerTaskRole != null)
        //                                                {
        //                                                    var user = UserManagement.GetByID((int) performerTaskRole.UserID);
        //                                                    TaskTransaction tasktransaction = new TaskTransaction()
        //                                                    {
        //                                                        TaskInstanceId = entrytaskinstance.ID,
        //                                                        TaskScheduleOnID = taskScheduleOn.ID,
        //                                                        ComplianceScheduleOnID = complianceScheduleon.ID,
        //                                                        CreatedBy = performerTaskRole.UserID,
        //                                                        CreatedByText = user.FirstName + " " + user.LastName,
        //                                                        StatusId = 1,
        //                                                        Remarks = "New task assigned."
        //                                                    };

        //                                                    ComplianceManagement.CreateTaskTransaction(tasktransaction, null, null, null);

        //                                                    foreach (var roles in AssignedTaskRole)
        //                                                    {
        //                                                        if (roles.RoleID != 6)
        //                                                        {
        //                                                            CreateTaskReminders(entrytaskinstance.TaskId, InternalcomplianceID, roles.ID, nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays)), complianceScheduleon.ID);
        //                                                        }
        //                                                    }
        //                                                }

        //                                            });
        //                                        });
        //                                    });
        //                                }
        //                            }
        //                        }
        //                        #endregion
        //                    }
        //                    #endregion
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
        //        LogMessage msg = new LogMessage();
        //        msg.ClassName = "InternalComplianceManagement";
        //        msg.FunctionName = "CreateScheduleOn" + "InternalComplianceid=" + CreateScheduleOnErroInternalcomplianceID + "InternalcomplianceInstanceId=" + CreateScheduleOnErroInternalcomplianceInstanceId;
        //        msg.CreatedOn = DateTime.Now;
        //        msg.Message = ex.Message + "----\r\n" + ex.InnerException;
        //        msg.StackTrace = ex.StackTrace;
        //        msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
        //        ReminderUserList.Add(msg);
        //        InsertLogToDatabase(ReminderUserList);
        //        List<string> TO = new List<string>();
        //        TO.Add("sachin@avantis.info");
        //        TO.Add("narendra@avantis.info");
        //        TO.Add("rahul@avantis.info");
        //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
        //           null, "Error Occured as Internal CreateScheduleOn Function", "Internal CreateScheduleOn" + "InternalComplianceid=" + CreateScheduleOnErroInternalcomplianceID + "InternalcomplianceInstanceId=" + CreateScheduleOnErroInternalcomplianceInstanceId);
        //    }
        //}
        public static Tuple<DateTime, string, long> GetNextDatePeriodicallyDaily(DateTime InternalscheduledOn, long InternalComplianceID)
        {
            int formonthscheduleR = -1;
            DateTime date = DateTime.Now;
            date = new DateTime(InternalscheduledOn.Year, Convert.ToInt32(InternalscheduledOn.Month), Convert.ToInt32(InternalscheduledOn.Day));
            string forMonth = string.Empty;
            return new Tuple<DateTime, string, long>(date, forMonth, formonthscheduleR);
        }

        public static Tuple<DateTime, string, long> GetNextDatePeriodically(DateTime InternalscheduledOn, long InternalComplianceID)
        {
            int formonthscheduleR = -1;
            var complianceSchedule = GetScheduleByComplianceID(InternalComplianceID).OrderBy(entry => entry.ForMonth).ToList();

            var objCompliance = GetByID(InternalComplianceID);

            DateTime date = complianceSchedule.Select(row => new DateTime(InternalscheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2)))).Where(row => row > InternalscheduledOn).FirstOrDefault();

           if (objCompliance.IFrequency == 3 && date != DateTime.MinValue)
           {
               date = new DateTime(InternalscheduledOn.Year, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
           }

           if (objCompliance.IFrequency == 5 && date != DateTime.MinValue)
           {
               date = new DateTime(InternalscheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
           }

           if (objCompliance.IFrequency == 6 && date != DateTime.MinValue)
           {
               date = new DateTime(InternalscheduledOn.Year + 6, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
           }

           if (date == DateTime.MinValue)
           {
               if (complianceSchedule.Count > 0)
               {
                   date = new DateTime(InternalscheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                   if (objCompliance.IFrequency == 5)
                   {
                       date = new DateTime(InternalscheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                   }

                   if (objCompliance.IFrequency == 6)
                   {
                       date = new DateTime(InternalscheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                   }
               }
           }
           string forMonth = string.Empty;         
           if (complianceSchedule.Count > 0)
           {
               string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");
               var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

               forMonth = GetForMonthPeriodically(date, ForMonthSchedule.ForMonth, objCompliance.IFrequency);
               formonthscheduleR = ForMonthSchedule.ForMonth;
           }      
           return new Tuple<DateTime, string, long>(date, forMonth, formonthscheduleR);
       }

       public static List<InternalComplianceSchedule> GetScheduleByComplianceID(long internalcomplianceID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var scheduleList = (from row in entities.InternalComplianceSchedules
                                   where row.IComplianceID == internalcomplianceID
                                   orderby row.ForMonth
                                   select row).ToList();

               return scheduleList;
           }
       }

       public static InternalCompliance GetByID(long InternalcomplianceID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var compliance = (from row in entities.InternalCompliances
                                 where row.ID == InternalcomplianceID
                                 select row).SingleOrDefault();

               return compliance;
           }
       }

        public static List<SP_GetAuditLogTransactionInternal_Result> GetAllTransactionLog(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.SP_GetAuditLogTransactionInternal(ScheduledOnID)
                                  where row.InternalComplianceScheduledOnID == ScheduledOnID
                                  orderby row.Dated descending
                                  select row).ToList();

                return statusList;
            }
        }
        public static List<InternalComplianceTransactionView> GetAllTransactions(int ScheduledOnID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var statusList = (from row in entities.InternalComplianceTransactionViews
                                 where row.InternalComplianceScheduledOnID == ScheduledOnID
                                 orderby row.ComplianceTransactionID descending
                                 select row).ToList();

               return statusList;
           }
       }
       public static string GetForMonthPeriodically(DateTime date, int forMonth, byte? Frequency)
       {
           string forMonthName = string.Empty;
           switch (Frequency)
           {
               case 0:
                   string year = date.ToString("yy");
                   if (forMonth == 1)
                   {
                       forMonth = 12;
                       year = (date.AddYears(-1)).ToString("yy");
                   }
                   forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                   break;
               case 1:
                   string yearq = date.ToString("yy");
                   if (forMonth == 1)
                   {
                       forMonth = 10;
                       yearq = (date.AddYears(-1)).ToString("yy");
                   }


                   forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 2)).Substring(0, 3) + " " + yearq +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq;
                   break;
               case 2:
                   string year1 = date.ToString("yy");
                   if (forMonth == 1)
                   {
                       forMonth = 7;
                       year1 = (date.AddYears(-1)).ToString("yy");
                   }

                   forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 5)).Substring(0, 3) + " " + year1 +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1;
                   break;

               case 3:

                   string startFinancial1Year = date.ToString("yy");
                   string endFinancial1Year = date.ToString("yy");
                   if (date.Month >= 4)
                   {
                       startFinancial1Year = date.ToString("yy");
                       endFinancial1Year = (date.AddYears(1)).ToString("yy"); date.ToString("yy");
                   }
                   else
                   {
                       startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                       endFinancial1Year = date.ToString("yy");
                   }

                   forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                   break;

               case 4:
                   string year2 = date.ToString("yy");
                   if (forMonth == 1)
                   {
                       forMonth = 9;
                       year2 = (date.AddYears(-1)).ToString("yy");
                   }


                   forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 3)).Substring(0, 3) + " " + year2 +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2;
                   break;
               case 5:

                   string startFinancial2Year = date.ToString("yy");
                   string endFinancial2Year = date.ToString("yy");
                   if (date.Month >= 4)
                   {
                       startFinancial2Year = (date.AddYears(-1)).ToString("yy");
                       endFinancial2Year = (date.AddYears(1)).ToString("yy");
                   }
                   else
                   {
                       startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                       endFinancial2Year = date.ToString("yy");
                   }

                   forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;

                   break;
               case 6:

                   string startFinancial7Year = date.ToString("yy");
                   string endFinancial7Year = date.ToString("yy");
                   if (date.Month >= 4)
                   {
                       startFinancial7Year = (date.AddYears(-6)).ToString("yy");
                       endFinancial7Year = (date.AddYears(1)).ToString("yy");
                   }
                   else
                   {
                       startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                       endFinancial7Year = date.ToString("yy");
                   }

                   forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                   break;

               default:
                   forMonthName = string.Empty;
                   break;
           }

           return forMonthName;
       }
        public static Tuple<DateTime, string, long> GetNextDate1(DateTime scheduledOn, long complianceID, long ComplianceInstanceID)
        {
            return new Tuple<DateTime, string, long>(scheduledOn, Convert.ToString(complianceID), ComplianceInstanceID);
        }
        #region Comment by rahul on 9 JAN 2017 bcz new code integration
        // public static Tuple<DateTime, string, long> GetNextDate(DateTime scheduledOn, long InternalComplianceID, long InternalComplianceInstanceID)
        //{

        //    var complianceSchedule = GetScheduleByComplianceID(InternalComplianceID);

        //    var objCompliance = GetByID(InternalComplianceID);

        //    var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(InternalComplianceInstanceID);

        //    long lastPeriod = 0;
        //    if (lastSchedule != null)
        //    {
        //        if (lastSchedule.ForPeriod != null)
        //        {
        //           lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);

        //        }
        //        else
        //        {
        //            lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(Convert.ToDateTime(lastSchedule.ScheduledOn).Month), objCompliance.IFrequency);                   
        //        }
        //    }
        //    else
        //    {
        //        if (objCompliance.IFrequency == 0)
        //        {
        //            lastPeriod = scheduledOn.Month - 1;
        //        }
        //        else
        //        {
        //            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
        //            {
        //                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
        //                {                        
        //                    lastPeriod = 1;
        //                }
        //                else
        //                {
        //                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
        //                }
        //            }

        //            //////lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
        //            //////added by Manisha
        //            ////int nSchedule = Convert.ToInt32(scheduledOn.Month.ToString("00"));
        //            ////if (complianceSchedule.Where(row => Convert.ToInt32(row.SpecialDate.Substring(2, 2)) >= nSchedule).FirstOrDefault() != null)//--added by Manisha
        //            ////{
        //            ////    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
        //            ////}
        //            //////added By Manisha
        //        }
        //    }

        //    List<Tuple<DateTime, long>> complianceScheduleDates;
        //    int ForMonth = 0; //added by Manisha
        //    if (objCompliance.IFrequency != 0 || objCompliance.IFrequency != 1)
        //    {
        //        if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
        //        {
        //            if (objCompliance.IFrequency == 3)
        //                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
        //            else if (objCompliance.IFrequency == 5)
        //                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
        //            else if (objCompliance.IFrequency == 6)
        //                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();                 
        //            else
        //                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
        //            ForMonth = 1;//added by Manisha for Calender year               
        //        }
        //        else
        //        {
        //            if (objCompliance.IFrequency == 3)
        //                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
        //            else if (objCompliance.IFrequency == 5)
        //                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
        //            else if (objCompliance.IFrequency == 6)
        //                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
        //            else
        //                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
        //            ForMonth = 4;//added by Manisha for Financial year
        //        }

        //        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
        //        {
        //            if (objCompliance.IFrequency == 5)
        //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
        //            else if (objCompliance.IFrequency == 6)
        //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
        //            else
        //            {
        //                if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
        //                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());                      
        //                else
        //                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
        //            }
        //        }
        //        //Added by Rahul on 5 Jan 2015
        //        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03" || row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
        //        {
        //            if (objCompliance.IFrequency == 5)
        //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
        //            else if (objCompliance.IFrequency == 6)
        //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
        //            else
        //            {
        //                if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
        //                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
        //                else
        //                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
        //            }
        //        }
        //    }
        //    else
        //    {
        //        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();

        //        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
        //        {
        //            if (objCompliance.IFrequency == 5)
        //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
        //            else if (objCompliance.IFrequency == 6)
        //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
        //            else
        //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

        //        }
        //    }

        //    long ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency,ForMonth);
        //    DateTime date = DateTime.UtcNow;
        //    if (complianceScheduleDates.Count > 0)
        //    {
        //        //added by rahul on 7 Jan 2015 for Quarterly
        //        if (objCompliance.IFrequency == 1)
        //        {
        //            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;

        //            long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
        //            if (scheduledonidpresent > 0)
        //            {
        //                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
        //            }                   
        //        }
        //        else
        //        {
        //            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
        //        }
        //    }

        //    //DateTime date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;

        //    if (date == DateTime.MinValue)
        //    {
        //        if (complianceSchedule.Count > 0)
        //        {
        //            date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

        //            if (objCompliance.IFrequency == 5)
        //            {
        //                date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
        //            }

        //            if (objCompliance.IFrequency == 6)
        //            {
        //                date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
        //            }
        //        }
        //    }

        //    string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

        //    string forMonth = string.Empty;
        //    forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.IFrequency);

        //    return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        //}
        #endregion
        public static Tuple<DateTime, string, long> GetEffectiveNextDate(DateTime scheduledOn, long InternalComplianceID, long InternalComplianceInstanceID)
        {
            var complianceSchedule = GetScheduleByComplianceID(InternalComplianceID);
            var objCompliance = GetByID(InternalComplianceID);
            var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(InternalComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetInternalComplianceInstanceScheduledon(InternalComplianceInstanceID);

            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(lastSchedule.ScheduledOn.Month), objCompliance.IFrequency);
                }
            }
            else
            {
                #region Monthly
                if (objCompliance.IFrequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (objCompliance.IFrequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (objCompliance.IFrequency == 3)//updated by rahul on 22 april 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 2)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 4;
                            }
                        }

                    }
                }
                #endregion
                #region Quarterly
                else if (objCompliance.IFrequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (objCompliance.IFrequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (objCompliance.IFrequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            int ForMonth = 0; //added by Manisha                            
            if (objCompliance.IFrequency != 0 || objCompliance.IFrequency != 1)
            {
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    ForMonth = 1;//added by Manisha for Calender year 
                }
                else
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    ForMonth = 4;//added by Manisha for Calender year 
                }

                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        else
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    }
                }
                //Added by Rahul on 5 Jan 2015
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03" || row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
                        {

                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                            {
                                if (objCompliance.IFrequency == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                //Added by Rahul on 15 Nov 2016
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" || row.SpecialDate.Substring(2, 2) == "11").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
                        {
                            if (objCompliance.IFrequency == 1)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }

                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" || row.SpecialDate.Substring(2, 2) == "07" || row.SpecialDate.Substring(2, 2) == "08" || row.SpecialDate.Substring(2, 2) == "09" || row.SpecialDate.Substring(2, 2) == "10" || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02" || entry.SpecialDate.Substring(2, 2) == "07" || entry.SpecialDate.Substring(2, 2) == "08" || entry.SpecialDate.Substring(2, 2) == "09" || entry.SpecialDate.Substring(2, 2) == "10" || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
            }
            else
            {
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();

                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

                }
            }

            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (objCompliance.IFrequency == 0)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {
                    // ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);

                    complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                    complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                    ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                }
            }
            else if (objCompliance.IFrequency == 1)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {
                    // ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                    // Comment by rahul on 7 JUNE 2017 In Case Of Effective Date Change
                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else if (objCompliance.IFrequency == 2)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {
                    //ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                    // Comment by rahul on 7 JUNE 2017 In Case Of Effective Date Change
                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else if (objCompliance.IFrequency == 3)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    // ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                    // Comment by rahul on 7 JUNE 2017 In Case Of Effective Date Change
                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else if (objCompliance.IFrequency == 4)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    //ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);

                    // Comment by rahul on 7 JUNE 2017 In Case Of Effective Date Change
                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else if (objCompliance.IFrequency == 5)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }

                    }
                }
                else
                {
                    // ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);

                    // Comment by rahul on 7 JUNE 2017 In Case Of Effective Date Change
                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else
            {
                //ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                // Comment by rahul on 7 JUNE 2017 In Case Of Effective Date Change
                if (complianceScheduleDates.Count > 0)
                {
                    complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                    complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                    ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                }
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                //added by rahul on 7 Jan 2015 for Quarterly
                if (objCompliance.IFrequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;

                        // complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        //date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        //complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 0)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    //added by rahul on 6 OCT 2016
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduledOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.IFrequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.IFrequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.IFrequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }

        public static Tuple<DateTime, string, long> GetNextDate(DateTime scheduledOn, long InternalComplianceID, long InternalComplianceInstanceID, bool iseffectivedate)
        {
            var complianceSchedule = GetScheduleByComplianceID(InternalComplianceID);
            var objCompliance = GetByID(InternalComplianceID);
            var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(InternalComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetInternalComplianceInstanceScheduledon(InternalComplianceInstanceID);

            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    if (iseffectivedate)
                    {
                        #region Monthly
                        if (objCompliance.IFrequency == 0)
                        {
                            lastPeriod = scheduledOn.Month - 1;
                        }
                        #endregion
                        #region   Half Yearly
                        else if (objCompliance.IFrequency == 2)
                        {
                            if (complianceSchedule[0].ForMonth == 1)
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Annaully
                        else if (objCompliance.IFrequency == 3)//updated by rahul on 22 april 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {

                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 2)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 4;
                                    }
                                }

                            }
                        }
                        #endregion
                        #region Quarterly
                        else if (objCompliance.IFrequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region FourMonthly
                        else if (objCompliance.IFrequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                                    {
                                        lastPeriod = 5;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 9;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                                    {
                                        lastPeriod = 5;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 9;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Two Yearly
                        else if (objCompliance.IFrequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                }
                            }
                        }
                        #endregion
                        else
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {
                                    lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                    }
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(lastSchedule.ScheduledOn.Month), objCompliance.IFrequency);
                }
            }
            else
            {
                #region Monthly
                if (objCompliance.IFrequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (objCompliance.IFrequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (objCompliance.IFrequency == 3)//updated by rahul on 22 april 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 2)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 4;
                            }
                        }

                    }
                }
                #endregion
                #region Quarterly
                else if (objCompliance.IFrequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (objCompliance.IFrequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (objCompliance.IFrequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            int ForMonth = 0; //added by Manisha                            
            if (objCompliance.IFrequency != 0 || objCompliance.IFrequency != 1)
            {
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    ForMonth = 1;//added by Manisha for Calender year 
                }
                else
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    ForMonth = 4;//added by Manisha for Calender year 
                }

                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        else
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    }
                }
                //Added by Rahul on 5 Jan 2015
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03" || row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
                        {

                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                            {
                                if (objCompliance.IFrequency == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                //Added by Rahul on 15 Nov 2016
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" || row.SpecialDate.Substring(2, 2) == "11").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
                        {
                            if (objCompliance.IFrequency == 1)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }

                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" || row.SpecialDate.Substring(2, 2) == "07" || row.SpecialDate.Substring(2, 2) == "08" || row.SpecialDate.Substring(2, 2) == "09" || row.SpecialDate.Substring(2, 2) == "10" || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02" || entry.SpecialDate.Substring(2, 2) == "07" || entry.SpecialDate.Substring(2, 2) == "08" || entry.SpecialDate.Substring(2, 2) == "09" || entry.SpecialDate.Substring(2, 2) == "10" || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    if (objCompliance.IFrequency == 2)
                    {
                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
            }
            else
            {
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();

                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

                }
            }

            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (objCompliance.IFrequency == 0)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else if (objCompliance.IFrequency == 1)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else if (objCompliance.IFrequency == 2)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else if (objCompliance.IFrequency == 3)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else if (objCompliance.IFrequency == 4)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else if (objCompliance.IFrequency == 5)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }

                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                //added by rahul on 7 Jan 2015 for Quarterly
                if (objCompliance.IFrequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 0)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    //added by rahul on 6 OCT 2016
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduledOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.IFrequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.IFrequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.IFrequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }

        public static Tuple<DateTime, string, long> GetNextDateWeekly(DateTime scheduledOn, long InternalComplianceID, long InternalComplianceInstanceID, int WeeklyDayFromCompliance)
        {
            var objCompliance = GetByID(InternalComplianceID);
            var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(InternalComplianceInstanceID);
            var getinstancescheduleondate = GetInternalComplianceInstance(InternalComplianceInstanceID);
            DateTime date = DateTime.Now;
            if (lastSchedule == null)
            {
                DateTime curruntDate;
                curruntDate = DateTime.UtcNow.AddDays(30);
                DateTime dtinstance = getinstancescheduleondate.StartDate.Date;
                while (dtinstance < curruntDate)
                {
                    int weekdayFromInstance = Convert.ToInt32(dtinstance.DayOfWeek);
                    if (weekdayFromInstance == WeeklyDayFromCompliance)
                    {
                        date = new DateTime(dtinstance.Year, Convert.ToInt32(dtinstance.Month), Convert.ToInt32(dtinstance.Day));
                        date = dtinstance; 
                        if (date >= getinstancescheduleondate.StartDate)
                        {
                            break;
                        }
                        else
                        {
                            dtinstance = dtinstance.AddDays(1);
                        }
                    }
                    else
                    {
                        dtinstance = dtinstance.AddDays(1);
                    }
                }
            }
            else
            {
                date = scheduledOn.AddDays(7); 
            }
            string forMonth = string.Empty;
            long ActualForMonth = 0;
            //forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.IFrequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }

        public static Tuple<DateTime, string, long> GetNextDateDaily(DateTime scheduledOn, long InternalComplianceID, long InternalComplianceInstanceID, bool iseffectivedate)
        {        
            var objCompliance = GetByID(InternalComplianceID);
            var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(InternalComplianceInstanceID);
            var getinstancescheduleondate = GetInternalComplianceInstance(InternalComplianceInstanceID);
            DateTime date = DateTime.Now;
            if (lastSchedule == null)
            {
                if (getinstancescheduleondate.StartDate.Date < scheduledOn.Date)
                {
                    date = new DateTime(scheduledOn.Year, Convert.ToInt32(scheduledOn.Month), Convert.ToInt32(scheduledOn.Day));
                }
                else
                {
                    date = new DateTime(getinstancescheduleondate.StartDate.Year, Convert.ToInt32(getinstancescheduleondate.StartDate.Month), Convert.ToInt32(getinstancescheduleondate.StartDate.Day));
                }

                //date = new DateTime(getinstancescheduleondate.StartDate.Year, Convert.ToInt32(getinstancescheduleondate.StartDate.Month), Convert.ToInt32(getinstancescheduleondate.StartDate.Day));
            }
            else
            {
                date = new DateTime(scheduledOn.Year, Convert.ToInt32(scheduledOn.Month), Convert.ToInt32(scheduledOn.Day + 1 ));
            }
            string forMonth = string.Empty;
            long ActualForMonth = 0;
            //forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.IFrequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }
        public static Tuple<DateTime, string, long> GetNextDateOLD(DateTime scheduledOn, long InternalComplianceID, long InternalComplianceInstanceID)
        {
            var complianceSchedule = GetScheduleByComplianceID(InternalComplianceID);
            var objCompliance = GetByID(InternalComplianceID);
            var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(InternalComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetInternalComplianceInstanceScheduledon(InternalComplianceInstanceID);
            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(Convert.ToDateTime(lastSchedule.ScheduledOn).Month), objCompliance.IFrequency);
                }
            }
            else
            {
                #region Monthly
                if (objCompliance.IFrequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (objCompliance.IFrequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                //lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (objCompliance.IFrequency == 3)//updated by rahul on 22 april 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 2)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 4;
                            }
                        }

                    }
                }
                #endregion
                #region Quarterly
                else if (objCompliance.IFrequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (objCompliance.IFrequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (objCompliance.IFrequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            int ForMonth = 0; //added by Manisha
            if (objCompliance.IFrequency != 0 || objCompliance.IFrequency != 1)
            {
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        // complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    ForMonth = 1;//added by Manisha for Calender year 
                }
                else
                {
                    if (objCompliance.IFrequency == 3)
                        // complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.IFrequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    ForMonth = 4;//added by Manisha for Calender year 
                }

                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        else
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    }
                }
                //Added by Rahul on 5 Jan 2015
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03" || row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
                        {

                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                            {
                                if (objCompliance.IFrequency == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                //Added by Rahul on 15 Nov 2016
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" || row.SpecialDate.Substring(2, 2) == "11").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
                        {
                            if (objCompliance.IFrequency == 1)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }

                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" || row.SpecialDate.Substring(2, 2) == "07" || row.SpecialDate.Substring(2, 2) == "08" || row.SpecialDate.Substring(2, 2) == "09" || row.SpecialDate.Substring(2, 2) == "10" || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02" || entry.SpecialDate.Substring(2, 2) == "07" || entry.SpecialDate.Substring(2, 2) == "08" || entry.SpecialDate.Substring(2, 2) == "09" || entry.SpecialDate.Substring(2, 2) == "10" || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
            }
            else
            {
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();

                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (objCompliance.IFrequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.IFrequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

                }
            }
            #region prvious code
            //if (objCompliance.IFrequency != 0 || objCompliance.IFrequency != 1)
            //{
            //    if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
            //    {
            //        if (objCompliance.IFrequency == 3)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.IFrequency == 5)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.IFrequency == 6)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else
            //            complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        ForMonth = 1;//added by Manisha for Calender year 
            //    }
            //    else
            //    {
            //        if (objCompliance.IFrequency == 3)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.IFrequency == 5)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.IFrequency == 6)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else
            //            complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        ForMonth = 4;//added by Manisha for Financial year
            //    }


            //    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
            //    {
            //        if (objCompliance.IFrequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (objCompliance.IFrequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //        {
            //            if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            else
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        }
            //    }
            //    //Added by Rahul on 5 Jan 2015
            //    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03" || row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
            //    {
            //        if (objCompliance.IFrequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (objCompliance.IFrequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //        {
            //            if (objCompliance.IFrequency == 0 || objCompliance.IFrequency == 1)
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            else
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        }
            //    }
            //}
            //else
            //{
            //    complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();

            //    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
            //    {
            //        if (objCompliance.IFrequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (objCompliance.IFrequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

            //    }
            //}
            #endregion
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (objCompliance.IFrequency == 0)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else if (objCompliance.IFrequency == 1)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else if (objCompliance.IFrequency == 2)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else if (objCompliance.IFrequency == 3)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else if (objCompliance.IFrequency == 4)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else if (objCompliance.IFrequency == 5)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }

                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
                }
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.IFrequency, ForMonth);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                //added by rahul on 7 Jan 2015 for Quarterly
                if (objCompliance.IFrequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }

                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }

                }
                else if (objCompliance.IFrequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.IFrequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    //added by rahul on 6 OCT 2016
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduledOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }
            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.IFrequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.IFrequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.IFrequency);

            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);


        }
        public static long GetLastPeriodFromDate(int forMonth, byte? Frequency)
       {
           long forMonthName = 0;
           switch (Frequency)
           {
               case 0:
                   if (forMonth == 1)
                   {
                       forMonthName = 12;
                   }
                   else
                   {
                       forMonthName = forMonth - 1;
                   }
                   break;
               case 1:
                   if (forMonth == 1)
                   {
                       forMonthName = 10;
                   }
                   else
                   {
                       forMonthName = forMonth - 3;
                   }
                   break;
               case 2:
                   if (forMonth == 1)
                   {
                       forMonthName = 7;
                   }
                   else
                   {
                       forMonthName = forMonth - 6;
                   }
                   break;

               case 3:
                   forMonthName = 1;
                   break;

               case 4:

                   if (forMonth == 1)
                   {
                       forMonthName = 9;
                   }
                   else
                   {
                       forMonthName = forMonthName - 4;
                   }

                   break;
               case 5:
                   forMonthName = 1;
                   break;
               case 6:

                   forMonthName = 1;
                   break;

               default:
                   forMonthName = 0;
                   break;
           }

           return forMonthName;
       }

       public static long GetScheduledOnPresentOrNot(long InternalComplianceInstanceID, DateTime ScheduledOn)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
                                    where row.InternalComplianceInstanceID == InternalComplianceInstanceID && row.ScheduledOn == ScheduledOn
                                    && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                    select row.ID).FirstOrDefault();
               return ScheduleOnID;
           }
       }
   

       public static InternalComplianceScheduledOn GetLastScheduleOnByInstanceInternal(long complianceInstanceID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var scheduleObj = (from row in entities.InternalComplianceScheduledOns
                                  where row.InternalComplianceInstanceID == complianceInstanceID
                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                  // orderby row.ID descending //change by rahul on 30 DEC 2015
                                  orderby row.ScheduledOn descending  
                                  select row).FirstOrDefault();

               return scheduleObj;
           }
       }
       //modify By Manisha
       public static long GetNextMonthFromFrequency(long LastforMonth, byte? Frequency, int nForMonth)
       {
           long NextPeriod;
           switch (Frequency)
           {
               case 0:
                   if (LastforMonth == 12)
                       NextPeriod = 1;
                   else
                       NextPeriod = LastforMonth + 1;
                   break;

               case 1:
                   if (LastforMonth == 10)
                       NextPeriod = 1;
                   //-----added By Manisha----------------------------
                   else if (LastforMonth == 0)
                       if (nForMonth == 4)
                           NextPeriod = 4;
                       else
                           NextPeriod = 1;
                   //------added By Manisha-----------------------------
                   else
                       NextPeriod = LastforMonth + 3;
                   break;

               case 2:
                   if (LastforMonth == 7)
                       NextPeriod = 1;
                   else if (LastforMonth == 10)
                       NextPeriod = 4;
                   //-----added By Manisha----------------------------
                   else if (LastforMonth == 0)
                       if (nForMonth == 4)
                           NextPeriod = 4;
                       else
                           NextPeriod = 1;
                   //------added By Manisha-----------------------------
                   else
                       NextPeriod = LastforMonth + 6;
                   break;
                    

               case 3:
                   if (LastforMonth == 4)
                   {
                       NextPeriod = 4;
                   }
                   //-----added By Manisha----------------------------
                   else if (LastforMonth == 0)
                       if (nForMonth == 4)
                           NextPeriod = 4;
                       else
                           NextPeriod = 1;
                   //------added By Manisha-----------------------------
                   else
                   {
                       NextPeriod = 1;
                   }
                   break;

               case 4:

                   if (LastforMonth == 9)
                       NextPeriod = 1;
                   else if (LastforMonth == 12)
                       NextPeriod = 4;
                   //-----added By Manisha----------------------------
                   else if (LastforMonth == 0)
                       if (nForMonth == 4)
                           NextPeriod = 4;
                       else
                           NextPeriod = 1;
                   //------added By Manisha-----------------------------
                   else
                       NextPeriod = LastforMonth + 4;
                   
                   break;
               case 5:

                   if (LastforMonth == 4)
                   {
                       NextPeriod = 4;
                   }
                   else
                   {
                       NextPeriod = 1;
                   }
                   break;

               case 6:

                   if (LastforMonth == 4)
                   {
                       NextPeriod = 4;
                   }
                   else
                   {
                       NextPeriod = 1;
                   }
                   break;

               default:
                   NextPeriod = 0;
                   break;
           }

           return NextPeriod;
       }
        //public static long GetNextMonthFromFrequency(long LastforMonth, byte? Frequency)
        //{
        //    long NextPeriod;
        //    switch (Frequency)
        //    {
        //        case 0:
        //            if (LastforMonth == 12)
        //                NextPeriod = 1;
        //            else
        //                NextPeriod = LastforMonth + 1;
        //            break;

        //        case 1:
        //            if (LastforMonth == 10)
        //                NextPeriod = 1;
        //            else
        //                NextPeriod = LastforMonth + 3;
        //            break;

        //        case 2:
        //            if (LastforMonth == 7)
        //                NextPeriod = 1;
        //            else if (LastforMonth == 10)
        //                NextPeriod = 4;
        //            else
        //                NextPeriod = LastforMonth + 6;
        //            break;

        //        case 3:
        //            if (LastforMonth == 4)
        //            {
        //                NextPeriod = 4;
        //            }
        //            else
        //            {
        //                NextPeriod = 1;
        //            }
        //            break;

        //        case 4:

        //            if (LastforMonth == 9)
        //                NextPeriod = 1;
        //            else if (LastforMonth == 12)
        //                NextPeriod = 4;
        //            else
        //                NextPeriod = LastforMonth + 4;




        //            //string year2 = date.ToString("yy");
        //            //if (forMonth == 1)
        //            //{
        //            //    forMonth = 9;
        //            //    year2 = (date.AddYears(-1)).ToString("yy");
        //            //}
        //            //else
        //            //{
        //            //    forMonth = forMonth - 4;
        //            //}

        //            //forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
        //            //               " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
        //            break;
        //        case 5:

        //            if (LastforMonth == 4)
        //            {
        //                NextPeriod = 4;
        //            }
        //            else
        //            {
        //                NextPeriod = 1;
        //            }
        //            break;

        //        case 6:

        //            if (LastforMonth == 4)
        //            {
        //                NextPeriod = 4;
        //            }
        //            else
        //            {
        //                NextPeriod = 1;
        //            }
        //            break;

        //        default:
        //            NextPeriod = 0;
        //            break;
        //    }

        //    return NextPeriod;
        //}

        //public static string GetForMonth(DateTime date, int forMonth, byte? Frequency)
        //{
        //    string forMonthName = string.Empty;
        //    switch (Frequency)
        //    {
        //        case 0:
        //            string year = date.ToString("yy");
        //            if (forMonth == 12)
        //            {
        //                year = (date.AddYears(-1)).ToString("yy");
        //            }
        //            forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
        //            break;
        //        case 1:
        //            string yearq = date.ToString("yy");
        //            if (forMonth == 10)
        //            {
        //                //yearq = (date.AddYears(-1)).ToString("yy");
        //                if (date.ToString("MM") == "12")
        //                {
        //                    yearq = (date).ToString("yy");
        //                }
        //                else
        //                {
        //                    yearq = (date.AddYears(-1)).ToString("yy");
        //                }
        //            }
        //            forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
        //                           " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
        //            break;
        //        case 2:
        //            string year1 = date.ToString("yy");
        //            if (forMonth == 7)
        //            {
        //                year1 = (date.AddYears(-1)).ToString("yy");

        //            }

        //            if (forMonth == 10)
        //            {
        //                year1 = (date.AddYears(-1)).ToString("yy");
        //                forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
        //                           " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(4).Substring(0, 3) + " " + date.ToString("yy");
        //            }
        //            else
        //            {
        //                forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
        //                           " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
        //            }

        //            break;

        //        case 3:

        //            string startFinancial1Year = date.ToString("yy");
        //            string endFinancial1Year = date.ToString("yy");
        //            if (date.Month >= 4)
        //            {
        //                startFinancial1Year = (date.AddYears(-1)).ToString("yy");
        //                endFinancial1Year = date.ToString("yy");
        //            }
        //            else
        //            {
        //                startFinancial1Year = (date.AddYears(-2)).ToString("yy");
        //                endFinancial1Year = (date.AddYears(-1)).ToString("yy");
        //            }

        //            if (forMonth == 1)
        //            {
        //                forMonthName = "CY - " + endFinancial1Year;
        //            }
        //            else
        //            {
        //                forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
        //            }
        //            break;

        //        case 4:

        //            string year2 = date.ToString("yy");
        //            if (forMonth == 9)
        //            {
        //                year2 = (date.AddYears(-1)).ToString("yy");

        //            }

        //            if (forMonth == 12)
        //            {
        //                forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
        //                          " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
        //            }
        //            else
        //            {
        //                forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
        //                               " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
        //            }

        //            break;
        //        case 5:

        //            string startFinancial2Year = date.ToString("yy");
        //            string endFinancial2Year = date.ToString("yy");
        //            if (date.Month >= 4)
        //            {
        //                startFinancial2Year = (date.AddYears(-2)).ToString("yy");
        //                endFinancial2Year = date.ToString("yy");
        //            }
        //            else
        //            {
        //                startFinancial2Year = (date.AddYears(-3)).ToString("yy");
        //                endFinancial2Year = (date.AddYears(-1)).ToString("yy");
        //            }

        //            forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;

        //            break;
        //        case 6:

        //            string startFinancial7Year = date.ToString("yy");
        //            string endFinancial7Year = date.ToString("yy");
        //            if (date.Month >= 4)
        //            {
        //                startFinancial7Year = (date.AddYears(-7)).ToString("yy");
        //                endFinancial7Year = date.ToString("yy");
        //            }
        //            else
        //            {
        //                startFinancial7Year = (date.AddYears(-8)).ToString("yy");
        //                endFinancial7Year = (date.AddYears(-1)).ToString("yy");
        //            }

        //            forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
        //            break;

        //        default:
        //            forMonthName = string.Empty;
        //            break;
        //    }

        //    return forMonthName;
        //}
        public static string GetForMonth(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:

                    string year = date.ToString("yy");
                    if (date > DateTime.Today.Date)
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            if (forMonth == 11)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else if (forMonth == 12)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else
                            {
                                year = (date).ToString("yy");
                            }
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    else
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else
                        {
                            yearq = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
                    //string yearq = date.ToString("yy");
                    //if (forMonth == 10)
                    //{
                    //    //yearq = (date.AddYears(-1)).ToString("yy");
                    //    if (date.ToString("MM") == "12")
                    //    {
                    //        yearq = (date).ToString("yy");
                    //    }
                    //    else
                    //    {
                    //        yearq = (date.AddYears(-1)).ToString("yy");
                    //    }
                    //}
                    //forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                    //               " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    if (forMonth == 7)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }

                        //year1 = (date.AddYears(-1)).ToString("yy");

                    }
                    if (forMonth == 10)
                    {
                        // year1 = (date.AddYears(-1)).ToString("yy");
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + date.ToString("yy");
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }

                    //string year1 = date.ToString("yy");
                    //if (forMonth == 7)
                    //{
                    //    year1 = (date.AddYears(-1)).ToString("yy");

                    //}

                    //if (forMonth == 10)
                    //{
                    //    year1 = (date.AddYears(-1)).ToString("yy");
                    //    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                    //               " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(4).Substring(0, 3) + " " + date.ToString("yy");
                    //}
                    //else
                    //{
                    //    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                    //               " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    //}

                    break;

                case 3:

                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                    }

                    if (forMonth == 1)
                    {
                        forMonthName = "CY - " + endFinancial1Year;
                    }
                    else
                    {
                        forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    }
                    break;

                case 4:

                    string year2 = date.ToString("yy");
                    if (forMonth == 9)
                    {
                        year2 = (date.AddYears(-1)).ToString("yy");

                    }

                    if (forMonth == 12)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
                                       " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
                    }

                    break;
                case 5:
                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    else if (forMonth == 4)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                        forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    //string startFinancial2Year = date.ToString("yy");
                    //string endFinancial2Year = date.ToString("yy");
                    //if (date.Month >= 4)
                    //{
                    //    startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                    //    endFinancial2Year = date.ToString("yy");
                    //}
                    //else
                    //{
                    //    startFinancial2Year = (date.AddYears(-3)).ToString("yy");
                    //    endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                    //}

                    //forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;

                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-8)).ToString("yy");
                        endFinancial7Year = (date.AddYears(-1)).ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }
       

        public static RecentInternalComplianceTransactionView GetCurrentStatusByInternalComplianceID(int InternalComplianceScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var currentStatusID = (from row in entities.RecentInternalComplianceTransactionViews
                                       where row.InternalComplianceScheduledOnID == InternalComplianceScheduledOnID
                                       select row).FirstOrDefault();

                return currentStatusID;
            }
        }
        public static List<InternalComplianceInstanceTransactionView> GetReviseCompliancesInternal(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ReviceCompliances = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == 3 &&
                                         (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                                         select row).ToList();

                return ReviceCompliances;
            }
        }        
        public static InternalCompliance GetComplianceByInstanceIDInternal(int InternalScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long InternalcomplianceInstanceID = (from row in entities.InternalComplianceTransactions
                                                     where row.InternalComplianceScheduledOnID == InternalScheduledOnID
                                                     select row.InternalComplianceInstanceID).FirstOrDefault();

                long InternalcomplianceID = (from row in entities.InternalComplianceInstances
                                             where row.ID == InternalcomplianceInstanceID && row.IsDeleted == false
                                             select row.InternalComplianceID).FirstOrDefault();

                var Internalcompliance = (from row in entities.InternalCompliances
                                          where row.ID == InternalcomplianceID
                                          select row).SingleOrDefault();

                return Internalcompliance;
            }
        }
     
        public static bool CreateTransaction(InternalComplianceTransaction transaction, List<InternalFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            try
                            {
                                try
                                {
                                   
                                        DocumentManagement.Internal_SaveDocFiles(filesList);
                                }
                                catch (Exception ex)
                                {
                                }
                                transaction.Dated = DateTime.Now;
                                entities.InternalComplianceTransactions.Add(transaction);
                                entities.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                var aa = ex.Message.ToString();
                            }
                            long transactionId = Convert.ToInt64(transaction.ID);
                            if (files != null)
                            {
                                foreach (InternalFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping fileMapping = new InternalFileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.InternalScheduledOnID = transaction.InternalComplianceScheduledOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();                          
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }

                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                   null, "Error Occured as Internal CreateTransaction Function", "Internal CreateTransaction");

                return false;
            }
        }

        public static bool CreateTransactionRevise(InternalComplianceTransaction transaction, List<InternalFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList,int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            try
                            {
                                try
                                {
                                    var AWSData = AmazonS3.GetAWSStorageDetail(CustomerID);
                                    if (AWSData == null)
                                    {
                                        DocumentManagement.Internal_SaveDocFiles(filesList);
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                                transaction.Dated = DateTime.Now;
                                entities.InternalComplianceTransactions.Add(transaction);
                                entities.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                var aa = ex.Message.ToString();
                            }
                            long transactionId = Convert.ToInt64(transaction.ID);
                            if (files != null)
                            {
                                foreach (InternalFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping fileMapping = new InternalFileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.InternalScheduledOnID = transaction.InternalComplianceScheduledOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }

                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                   null, "Error Occured as Internal CreateTransaction Function", "Internal CreateTransaction");

                return false;
            }
        }

        public static bool CreateTransaction(InternalComplianceTransaction transaction, List<InternalFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, string DirectoryPath, long ScheduleOnID, int CustId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            try
                            {
                                try
                                {
                                    var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                                    if (AWSData != null)
                                    {
                                        AmazonS3.SaveDocFilesAWSStorageInternal(filesList, DirectoryPath, ScheduleOnID, AWSData.BucketName, AWSData.AccessKeyID, AWSData.SecretKeyID);
                                    }
                                    else
                                    {
                                        DocumentManagement.Internal_SaveDocFiles(filesList);
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                                transaction.Dated = DateTime.Now;
                                entities.InternalComplianceTransactions.Add(transaction);
                                entities.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                var aa = ex.Message.ToString();
                            }
                            long transactionId = Convert.ToInt64(transaction.ID);
                            if (files != null)
                            {
                                foreach (InternalFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping fileMapping = new InternalFileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.InternalScheduledOnID = transaction.InternalComplianceScheduledOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }

                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                   null, "Error Occured as Internal CreateTransaction Function", "Internal CreateTransaction");

                return false;
            }
        }

        public static bool CreateTransaction(InternalComplianceTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            try
                            {                               
                                transaction.Dated = DateTime.Now;
                                entities.InternalComplianceTransactions.Add(transaction);
                                entities.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                var aa = ex.Message.ToString();
                            }                            
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();                                                        
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                   null, "Error Occured as Internal CreateTransaction Function", "Internal CreateTransaction");

                return false;
            }
        }
        public static void CreateTransaction(List<InternalComplianceTransaction> transaction)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.InternalComplianceTransactions.Add(entry);
                });
                entities.SaveChanges();
            }
        }

       private static void CreateReminders(long InternalcomplianceID, long InternalComplianceInstanceId, int InternalComplianceAssignmentId)
       {
           long CreateRemindersErrorInternalcomplianceInstanceID = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var compliance = (from row in entities.InternalCompliances
                                      where row.ID == InternalcomplianceID
                                      select row).FirstOrDefault();


                    if (compliance.IFrequency == 7)
                    {
                        #region Frequency Daily

                        List<InternalComplianceScheduledOn> ScheduledOnList = (from row in entities.InternalComplianceScheduledOns
                                                                               where row.InternalComplianceInstanceID == InternalComplianceInstanceId
                                                                               select row).ToList();
                        CreateRemindersErrorInternalcomplianceInstanceID = InternalComplianceInstanceId;

                        ScheduledOnList.ForEach(entry =>
                        {
                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                            {
                                ComplianceAssignmentID = InternalComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = Convert.ToDateTime(entry.ScheduledOn),
                                RemindOn = Convert.ToDateTime(entry.ScheduledOn).Date,
                            };

                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                            entities.InternalComplianceReminders.Add(reminder);
                        });

                        #endregion
                    }
                    else if (compliance.IFrequency == 8)
                    {
                        #region Frequency Weekly
                        List<InternalComplianceScheduledOn> ScheduledOnList = (from row in entities.InternalComplianceScheduledOns
                                                                               where row.InternalComplianceInstanceID == InternalComplianceInstanceId
                                                                               select row).ToList();
                        CreateRemindersErrorInternalcomplianceInstanceID = InternalComplianceInstanceId;

                        ScheduledOnList.ForEach(entry =>
                        {
                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                            {
                                ComplianceAssignmentID = InternalComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = Convert.ToDateTime(entry.ScheduledOn),
                                RemindOn = Convert.ToDateTime(entry.ScheduledOn).Date,
                            };

                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                            entities.InternalComplianceReminders.Add(reminder);

                            InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                            {
                                ComplianceAssignmentID = InternalComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = Convert.ToDateTime(entry.ScheduledOn),
                                RemindOn = Convert.ToDateTime(entry.ScheduledOn).Date.AddDays(-1 * 2),
                            };

                            reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                            entities.InternalComplianceReminders.Add(reminder1);
                        });
                        #endregion
                    }
                    else
                    {
                        List<long> getdays = GetUpcomingReminderDaysDetail(InternalComplianceAssignmentId, Convert.ToInt32(compliance.IFrequency), "I");
                        if (getdays.Count > 0)
                        {
                            List<InternalComplianceScheduledOn> ScheduledOnList = (from row in entities.InternalComplianceScheduledOns
                                                                                   where row.InternalComplianceInstanceID == InternalComplianceInstanceId
                                                                                   && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                                                                   select row).ToList();
                            foreach (var item1 in ScheduledOnList)
                            {
                                foreach (var item in getdays)
                                {
                                    InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                                    {
                                        ComplianceAssignmentID = InternalComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = item1.ScheduledOn,
                                        RemindOn = item1.ScheduledOn.Date.AddDays(-item),
                                    };
                                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.InternalComplianceReminders.Add(reminder1);
                                }
                            }
                        }
                        else
                        {
                            #region Normal Frequency
                            if (compliance.ISubComplianceType == 0)
                            {
                                List<InternalComplianceScheduledOn> ScheduledOnList = (from row in entities.InternalComplianceScheduledOns
                                                                                       where row.InternalComplianceInstanceID == InternalComplianceInstanceId
                                                                                       && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                                                                       select row).ToList();

                                CreateRemindersErrorInternalcomplianceInstanceID = InternalComplianceInstanceId;
                                //Standard
                                if (compliance.IReminderType == 0)
                                {
                                    foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                                    {
                                        InternalComplianceReminder reminder = new InternalComplianceReminder()
                                        {
                                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = Convert.ToDateTime(sco.ScheduledOn),
                                            RemindOn = Convert.ToDateTime(sco.ScheduledOn).Date.AddDays(-1 * 2),
                                        };
                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.InternalComplianceReminders.Add(reminder);

                                        InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                                        {
                                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = Convert.ToDateTime(sco.ScheduledOn),
                                            RemindOn = Convert.ToDateTime(sco.ScheduledOn).Date.AddDays(-1 * 15),
                                        };
                                        reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.InternalComplianceReminders.Add(reminder1);
                                    }
                                }
                                else//Custom
                                {
                                    ScheduledOnList.ForEach(entry =>
                                    {
                                        DateTime TempScheduled = Convert.ToDateTime(entry.ScheduledOn).AddDays(-(Convert.ToDouble(compliance.IReminderBefore)));

                                        while (TempScheduled.Date < entry.ScheduledOn)
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = InternalComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = Convert.ToDateTime(entry.ScheduledOn),
                                                RemindOn = TempScheduled.Date,
                                            };

                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                            entities.InternalComplianceReminders.Add(reminder);

                                            TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.IReminderGap));
                                        }

                                    });

                                }
                            }
                            else
                            {
                                List<InternalComplianceScheduledOn> ScheduledOnList = (from row in entities.InternalComplianceScheduledOns
                                                                                       where row.InternalComplianceInstanceID == InternalComplianceInstanceId
                                                                                       select row).ToList();
                                CreateRemindersErrorInternalcomplianceInstanceID = InternalComplianceInstanceId;
                                if (compliance.IReminderType == 0)
                                {
                                    if (compliance.IFrequency.HasValue)
                                    {
                                        var reminders = (from row in entities.ReminderTemplates
                                                         where row.Frequency == compliance.IFrequency.Value && row.IsSubscribed == true
                                                         select row).ToList();

                                        foreach (InternalComplianceScheduledOn sco in ScheduledOnList)
                                        {
                                            reminders.ForEach(day =>
                                            {
                                                InternalComplianceReminder reminder = new InternalComplianceReminder()
                                                {
                                                    ComplianceAssignmentID = InternalComplianceAssignmentId,
                                                    ReminderTemplateID = day.ID,
                                                    ComplianceDueDate = Convert.ToDateTime(sco.ScheduledOn),
                                                    RemindOn = Convert.ToDateTime(sco.ScheduledOn).Date.AddDays(-1 * day.TimeInDays),
                                                };

                                                reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                                entities.InternalComplianceReminders.Add(reminder);

                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    ScheduledOnList.ForEach(entry =>
                                    {
                                        DateTime TempScheduled = Convert.ToDateTime(entry.ScheduledOn).AddDays(-(Convert.ToDouble(compliance.IReminderBefore)));

                                        while (TempScheduled.Date < entry.ScheduledOn)
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = InternalComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = Convert.ToDateTime(entry.ScheduledOn),
                                                RemindOn = TempScheduled.Date,
                                            };

                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                            entities.InternalComplianceReminders.Add(reminder);

                                            TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.IReminderGap));
                                        }

                                    });
                                }
                            }
                            #endregion
                        }
                    }
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "InternalComplianceManagement";
                msg.FunctionName = "CreateReminders" + "InternalcomplianceInstanceID" + CreateRemindersErrorInternalcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as Internal CreateReminders Function", "Internal CreateReminders" + "InternalcomplianceInstanceID" + CreateRemindersErrorInternalcomplianceInstanceID);
                
            }
       }

       public static void CreateReminders(long InternalComplianceID, long InternalComplianceInstanceId, int InternalComplianceAssignmentId, DateTime scheduledOn)
       {
           try
           {
               using (ComplianceDBEntities entities = new ComplianceDBEntities())
               {
                   InternalCompliance compliance = (from row in entities.InternalCompliances
                                                    where row.ID == InternalComplianceID
                                                    select row).FirstOrDefault();
                    if (compliance.IFrequency == 7)
                    {
                        #region Frequency Daily
                        InternalComplianceReminder reminder = new InternalComplianceReminder()
                        {
                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date,
                        };
                        reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.InternalComplianceReminders.Add(reminder);
                        #endregion
                    }
                    else if (compliance.IFrequency == 8)
                    {
                        #region Frequency Weekly
                        InternalComplianceReminder reminder = new InternalComplianceReminder()
                        {
                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date,
                        };
                        reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.InternalComplianceReminders.Add(reminder);
                        InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                        {
                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                        };
                        reminder1.Status = (byte) (reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.InternalComplianceReminders.Add(reminder1);
                        #endregion
                    }
                    else
                    {
                        List<long> getdays = GetUpcomingReminderDaysDetail(InternalComplianceAssignmentId, Convert.ToInt32(compliance.IFrequency), "I");
                        if (getdays.Count > 0)
                        {
                            foreach (var item in getdays)
                            {
                                InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                                {
                                    ComplianceAssignmentID = InternalComplianceAssignmentId,
                                    ReminderTemplateID = 1,
                                    ComplianceDueDate = scheduledOn,
                                    RemindOn = scheduledOn.Date.AddDays(-item),
                                };
                                reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                entities.InternalComplianceReminders.Add(reminder1);
                            }
                        }
                        else
                        {
                            #region Normal Frequency
                            if (compliance.ISubComplianceType == 0)
                            {
                                if (compliance.IReminderType == 0)
                                {
                                    InternalComplianceReminder reminder = new InternalComplianceReminder()
                                    {
                                        ComplianceAssignmentID = InternalComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                                    };
                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.InternalComplianceReminders.Add(reminder);


                                    InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                                    {
                                        ComplianceAssignmentID = InternalComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                                    };
                                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.InternalComplianceReminders.Add(reminder1);
                                }
                                else
                                {
                                    DateTime TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.IReminderBefore)));
                                    while (TempScheduled.Date < scheduledOn)
                                    {
                                        InternalComplianceReminder reminder = new InternalComplianceReminder()
                                        {
                                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = TempScheduled.Date,
                                        };

                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                        entities.InternalComplianceReminders.Add(reminder);

                                        TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.IReminderGap));
                                    }
                                }
                            }//Function Based
                            else
                            {

                                if (compliance.IReminderType == 0)
                                {
                                    if (compliance.IFrequency.HasValue)
                                    {
                                        var reminders = (from row in entities.ReminderTemplates
                                                         where row.Frequency == compliance.IFrequency.Value && row.IsSubscribed == true
                                                         select row).ToList();

                                        reminders.ForEach(day =>
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = InternalComplianceAssignmentId,
                                                ReminderTemplateID = day.ID,
                                                ComplianceDueDate = scheduledOn,
                                                RemindOn = scheduledOn.Date.AddDays(-1 * day.TimeInDays),
                                            };

                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                            entities.InternalComplianceReminders.Add(reminder);

                                        });
                                    }
                                }
                                else
                                {
                                    DateTime TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.IReminderBefore)));
                                    while (TempScheduled.Date < scheduledOn)
                                    {
                                        InternalComplianceReminder reminder = new InternalComplianceReminder()
                                        {
                                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = TempScheduled.Date,
                                        };

                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                        entities.InternalComplianceReminders.Add(reminder);

                                        TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.IReminderGap));
                                    }
                                }
                            }//function Based End
                            #endregion
                        }
                    }
                    entities.SaveChanges();
                }
           }
           catch (Exception ex)
           {
               List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
               LogMessage msg = new LogMessage();
               msg.ClassName = "InternalComplianceManagement";
               //msg.FunctionName = "CreateReminders";
               msg.FunctionName = "CreateReminders" + "InternalComplianceAssignmentId" + InternalComplianceAssignmentId;
               msg.CreatedOn = DateTime.Now;
               msg.Message = ex.Message + "----\r\n" + ex.InnerException;
               msg.StackTrace = ex.StackTrace;
               msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
               ReminderUserList.Add(msg);
               InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                   null, "Error Occured as Internal CreateReminders Function", "Internal CreateReminders" + "InternalComplianceAssignmentId" + InternalComplianceAssignmentId);
                
           }
       }

        public static List<InternalComplianceAssignedInstancesView> GetAllAssignedInstancesReassginInternalExcludeNew(int branchID = -1, int userID = -1, int customerId = -1, string filter = "")
        {
            IQueryable<InternalComplianceAssignedInstancesView> complianceInstancesQuery = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceinstanceidrevirwer = (from row in entities.InternalComplianceAssignments
                                                    where row.RoleID == 4 && row.UserID == userID
                                                    select row.InternalComplianceInstanceID).ToList();

                var userid = (from row in entities.InternalComplianceAssignments
                              where row.RoleID == 3 && complianceinstanceidrevirwer.Contains(row.InternalComplianceInstanceID)
                              select row.UserID
                                ).Distinct().ToList();




                complianceInstancesQuery = (from row in entities.InternalComplianceAssignedInstancesViews
                                            where userid.Contains((long)row.UserID)
                                            select row);


                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }
                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.RoleID == 3);
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ShortDescription.Contains(filter) || entry.Branch.Contains(filter) || entry.Role.Contains(filter) || entry.User.Contains(filter));
                }

                return complianceInstancesQuery.OrderByDescending(entry => entry.InternalComplianceInstanceID).ToList();

            }
        }
        public static List<InternalComplianceAssignedInstancesView> GetAllAssignedInstances(int branchID = -1, int userID = -1, int customerId = -1, string filter = "")
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var complianceInstancesQuery = (from row in entities.InternalComplianceAssignedInstancesViews
                                               select row);

               if (customerId != -1)
               {
                   complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
               }

               if (branchID != -1)
               {
                   complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
               }

               if (userID != -1)
               {
                   complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
               }

               return complianceInstancesQuery.OrderByDescending(entry => entry.InternalComplianceInstanceID).ToList();

           }
       }
        public static List<InternalComplianceAssignedInstancesView> GetAllAssignedInstances( long customerId,int branchID, int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.InternalComplianceAssignedInstancesViews
                                                where row.CustomerID== customerId
                                                select row);

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }

                return complianceInstancesQuery.OrderByDescending(entry => entry.InternalComplianceInstanceID).ToList();

            }
        }

        public static List<InternalComplianceAssignedInstancesView> GetAllAssignedInternalInstances(string IsChecklist,int branchID = -1, int userID = -1, int customerId = -1, string filter = "")
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.InternalComplianceAssignedInstancesViews
                                                select row);

              
                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }
                if (IsChecklist == "Y")
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.IComplianceType == 1);

                }
                //if (IsChecklist == "N")
                //{
                //    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.IComplianceType != 1);

                //}

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ShortDescription.Contains(filter) || entry.Role.Contains(filter));
                }
                return complianceInstancesQuery.OrderByDescending(entry => entry.InternalComplianceInstanceID).ToList();

            }
        }

        public static List<InternalComplianceAssignment> GetAssignedCompliances(long InternalcomplianceID, int customerBranchID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {

               long complianceInstanceID = (from row in entities.InternalComplianceInstances
                                            where row.InternalComplianceID == InternalcomplianceID && row.CustomerBranchID == customerBranchID
                                            select row.ID).SingleOrDefault();

               List<InternalComplianceAssignment> complianceAssignment = null;
               if (complianceInstanceID > 0)
               {
                   complianceAssignment = (from row in entities.InternalComplianceAssignments
                                           where row.InternalComplianceInstanceID == complianceInstanceID
                                           && (row.RoleID == 3 || row.RoleID == 4 || row.RoleID == 5)
                                           select row).ToList();
               }

               return complianceAssignment;
           }
       }


       public static List<NameValue> GetUserRoles(int userID = -1)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               List<NameValue> AssignedRoles = new List<NameValue>();
               if (userID != -1)
               {
                   AssignedRoles = (from row in entities.InternalComplianceAssignments
                                    join ro in entities.Roles
                                    on row.RoleID equals ro.ID
                                    where row.UserID == userID
                                    select new NameValue()
                                    {
                                        ID = row.RoleID,
                                        Name = ro.Name
                                    }).GroupBy(entry => entry.ID).Select(entry => entry.FirstOrDefault()).ToList();
               }




               return AssignedRoles;
           }
       }

      
       public static List<InternalComplianceAssignment> GetAssignedUsers(int InternalComplianceInstanceID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var AssignedUsers = (from row in entities.InternalComplianceAssignments
                                    where row.InternalComplianceInstanceID == InternalComplianceInstanceID
                                    select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

               return AssignedUsers;
           }
       }



        public static bool IsInternalComplianceAssignmentAvailable(long internalcomplianceinstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.InternalComplianceAssignments
                                      where row.InternalComplianceInstanceID == internalcomplianceinstanceID
                                      select row).ToList();

                if (ComplianceData.Count > 0)
                    return true;
                else
                    return false;
            }
        }
        public static bool GetComplianceH_Edit_InternalCompliance(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.H_Edit_InternalCompliance
                            where row.PrevComplianceInstanceID == ComplianceInstanceID
                            select row).FirstOrDefault();
                if (data != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static void GenerateScheduleOnForUpcomingInternal()
       {
           long testid = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    DateTime curruntDate = DateTime.UtcNow;
                    var internalcomplianceInstances = (from row in entities.InternalComplianceInstances
                                                       where row.IsDeleted == false //&& row.ID== 12419
                                                       select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

                    internalcomplianceInstances.ForEach(entry =>
                    {
                        if (IsInternalComplianceAssignmentAvailable(entry.ID))
                        {
                            var Icompliances = (from row in entities.InternalCompliances
                                                where row.ID == entry.InternalComplianceID
                                               && row.IComplianceOccurrence == 1
                                                select row).FirstOrDefault();
                            if (Icompliances != null)
                            {
                                if (Icompliances.IFrequency == 7)
                                {
                                    #region Schedule Daily Frequency 
                                    testid = entry.ID;
                                    string ic = "1/1/0001 12:00:00 AM";
                                    Tuple<DateTime, string, long> nextDateMonth;
                                    var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(entry.ID);
                                    if (lastSchedule != null)
                                    {
                                        nextDateMonth = new Tuple<DateTime, string, long>(lastSchedule.ScheduledOn.AddDays(1).Date, "", 0);

                                        if (nextDateMonth.Item1 != Convert.ToDateTime(ic))
                                        {
                                            DateTime ToDate = DateTime.UtcNow.AddDays(1);
                                            while (nextDateMonth.Item1 < ToDate)
                                            {
                                                long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
                                                                     where row.InternalComplianceInstanceID == entry.ID
                                                                    && row.ScheduledOn == nextDateMonth.Item1
                                                                    && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                                     select row.ID).FirstOrDefault();

                                                if (ScheduleOnID <= 0)
                                                {
                                                    InternalComplianceScheduledOn internalcomplianceScheduleon = new InternalComplianceScheduledOn();
                                                    internalcomplianceScheduleon.InternalComplianceInstanceID = entry.ID;
                                                    internalcomplianceScheduleon.ScheduledOn = nextDateMonth.Item1.Date;
                                                    internalcomplianceScheduleon.ForMonth = nextDateMonth.Item2;
                                                    internalcomplianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                                    internalcomplianceScheduleon.IsActive = true;
                                                    internalcomplianceScheduleon.IsUpcomingNotDeleted = true;
                                                    entities.InternalComplianceScheduledOns.Add(internalcomplianceScheduleon);
                                                    entities.SaveChanges();

                                                    var AssignedRole = GetAssignedUsers((int) entry.ID);
                                                    var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                                    if (performerRole != null)
                                                    {
                                                        var user = UserManagement.GetByID((int) performerRole.UserID);
                                                        InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                                        {
                                                            InternalComplianceInstanceID = entry.ID,
                                                            InternalComplianceScheduledOnID = internalcomplianceScheduleon.ID,
                                                            CreatedBy = performerRole.UserID,
                                                            CreatedByText = user.FirstName + " " + user.LastName,
                                                            StatusId = 1,
                                                            Remarks = "New Internal compliance assigned."
                                                        };

                                                        CreateTransaction(transaction);

                                                        foreach (var roles in AssignedRole)
                                                        {
                                                            if (roles.RoleID != 6)
                                                            {
                                                                CreateReminders(entry.InternalComplianceID, entry.ID, roles.ID, nextDateMonth.Item1);
                                                            }
                                                        }
                                                    }
                                                }
                                                nextDateMonth = new Tuple<DateTime, string, long>(nextDateMonth.Item1.AddDays(1).Date, "", 0);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else if (Icompliances.IFrequency == 8)
                                {
                                    #region Schedule Weekly Frequency
                                    var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(entry.ID);
                                    testid = entry.ID;
                                    string ic = "1/1/0001 12:00:00 AM";
                                    Tuple<DateTime, string, long> nextDateMonth;
                                    if (lastSchedule != null)
                                    {
                                        nextDateMonth = new Tuple<DateTime, string, long>(lastSchedule.ScheduledOn.AddDays(7).Date, "", 0);
                                        if (nextDateMonth.Item1 != Convert.ToDateTime(ic))
                                        {
                                            DateTime ToDate = DateTime.UtcNow.AddDays(30);
                                            while (nextDateMonth.Item1 < ToDate)
                                            {
                                                long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
                                                                     where row.InternalComplianceInstanceID == entry.ID
                                                                    && row.ScheduledOn == nextDateMonth.Item1
                                                                    && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                                     select row.ID).FirstOrDefault();

                                                if (ScheduleOnID <= 0)
                                                {
                                                    InternalComplianceScheduledOn internalcomplianceScheduleon = new InternalComplianceScheduledOn();
                                                    internalcomplianceScheduleon.InternalComplianceInstanceID = entry.ID;
                                                    internalcomplianceScheduleon.ScheduledOn = nextDateMonth.Item1.Date;
                                                    internalcomplianceScheduleon.ForMonth = nextDateMonth.Item2;
                                                    internalcomplianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                                    internalcomplianceScheduleon.IsActive = true;
                                                    internalcomplianceScheduleon.IsUpcomingNotDeleted = true;
                                                    entities.InternalComplianceScheduledOns.Add(internalcomplianceScheduleon);
                                                    entities.SaveChanges();

                                                    var AssignedRole = GetAssignedUsers((int) entry.ID);
                                                    var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                                    if (performerRole != null)
                                                    {
                                                        var user = UserManagement.GetByID((int) performerRole.UserID);
                                                        InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                                        {
                                                            InternalComplianceInstanceID = entry.ID,
                                                            InternalComplianceScheduledOnID = internalcomplianceScheduleon.ID,
                                                            CreatedBy = performerRole.UserID,
                                                            CreatedByText = user.FirstName + " " + user.LastName,
                                                            StatusId = 1,
                                                            Remarks = "New Internal compliance assigned."
                                                        };

                                                        CreateTransaction(transaction);

                                                        foreach (var roles in AssignedRole)
                                                        {
                                                            if (roles.RoleID != 6)
                                                            {
                                                                CreateReminders(entry.InternalComplianceID, entry.ID, roles.ID, nextDateMonth.Item1);
                                                            }
                                                        }
                                                    }
                                                }
                                                nextDateMonth = new Tuple<DateTime, string, long>(nextDateMonth.Item1.AddDays(7).Date, "", 0);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Schedule Normal Frequency
                                    testid = entry.ID;
                                    string ic = "1/1/0001 12:00:00 AM";
                                    Tuple<DateTime, string, long> nextDateMonth;

                                    if (Icompliances.ISubComplianceType == 0)
                                    {
                                        var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(entry.ID);
                                        if (lastSchedule != null)
                                        {
                                            if (lastSchedule.ScheduledOn.AddDays(Convert.ToDouble(Icompliances.IDueDate)).Date <= curruntDate.AddDays(30).Date)
                                            {
                                                nextDateMonth = new Tuple<DateTime, string, long>(lastSchedule.ScheduledOn.AddDays(Convert.ToDouble(Icompliances.IDueDate)).Date, "", 0);
                                            }
                                            else
                                            {
                                                nextDateMonth = new Tuple<DateTime, string, long>(lastSchedule.ScheduledOn.Date, "", 0);
                                            }
                                        }
                                        else
                                        {
                                            if (Icompliances.EffectiveDate != null)
                                            {
                                                DateTime rahuldt = Convert.ToDateTime(Icompliances.EffectiveDate);
                                                nextDateMonth = new Tuple<DateTime, string, long>(rahuldt.AddDays(Convert.ToDouble(Icompliances.IDueDate)), "", 0);
                                            }
                                            else
                                            {
                                                DateTime rahuldt = Convert.ToDateTime(entry.StartDate);
                                                nextDateMonth = new Tuple<DateTime, string, long>(rahuldt.AddDays(Convert.ToDouble(Icompliances.IDueDate)), "", 0);
                                            }
                                        }
                                    }
                                    else if (Icompliances.ISubComplianceType == 1 || Icompliances.ISubComplianceType == 2)
                                    {
                                        var parameterDate = DateTime.ParseExact(curruntDate.ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                                        var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(entry.ID);

                                        if (lastSchedule != null)
                                        {
                                            if (lastSchedule.ScheduledOn.Date > parameterDate.Date)
                                            {
                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDate1(Convert.ToDateTime(ic), entry.InternalComplianceID, entry.ID);
                                            }
                                            else
                                            {
                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDatePeriodically(lastSchedule.ScheduledOn, entry.InternalComplianceID);
                                            }
                                        }
                                        else
                                        {
                                            DateTime? getinstancescheduleondate = GetInternalComplianceInstanceScheduledon(entry.ID);
                                            if (getinstancescheduleondate != null)
                                            {
                                                DateTime rahuldt = new DateTime();
                                                bool checkisedit = GetComplianceH_Edit_InternalCompliance(entry.ID);
                                                if (checkisedit)
                                                {
                                                    rahuldt = Convert.ToDateTime(Icompliances.EffectiveDate);
                                                }
                                                else
                                                {
                                                    rahuldt = Convert.ToDateTime(getinstancescheduleondate);
                                                }

                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDatePeriodically(rahuldt, entry.InternalComplianceID);
                                            }
                                            else
                                            {
                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDatePeriodically(curruntDate, entry.InternalComplianceID);
                                            }
                                        }
                                    }
                                    else if (Icompliances.ISubComplianceType == null && Icompliances.IFrequency == 5)
                                    {
                                        var parameterDate = DateTime.ParseExact(curruntDate.ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                                        var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(entry.ID);
                                        if (lastSchedule != null)
                                        {
                                            if (lastSchedule.ScheduledOn.Date > parameterDate.Date)
                                            {
                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDate1(Convert.ToDateTime(ic), entry.InternalComplianceID, entry.ID);
                                            }
                                            else
                                            {
                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDate(lastSchedule.ScheduledOn, entry.InternalComplianceID, entry.ID, false);
                                            }
                                        }
                                        else
                                        {
                                            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(entry.ID);
                                            if (getinstancescheduleondate != null)
                                            {
                                                DateTime rahuldt = new DateTime();
                                                bool checkisedit = GetComplianceH_Edit_InternalCompliance(entry.ID);
                                                if (checkisedit)
                                                {
                                                    rahuldt = Convert.ToDateTime(Icompliances.EffectiveDate);
                                                }
                                                else
                                                {
                                                    rahuldt = Convert.ToDateTime(getinstancescheduleondate);
                                                }

                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDate(Convert.ToDateTime(rahuldt), entry.InternalComplianceID, entry.ID, false);
                                            }
                                            else
                                            {
                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDate(curruntDate, entry.InternalComplianceID, entry.ID, false);
                                            }
                                        }
                                    }
                                    else if (Icompliances.IFrequency == 0 || Icompliances.IFrequency == 1 || Icompliances.IFrequency == 2 || Icompliances.IFrequency == 3 || Icompliances.IFrequency == 4)
                                    {
                                        var parameterDate = DateTime.ParseExact(curruntDate.ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                                        var lastSchedule = InternalComplianceManagement.GetLastScheduleOnByInstanceInternal(entry.ID);
                                        if (lastSchedule != null)
                                        {
                                            if (lastSchedule.ScheduledOn.Date > parameterDate.Date)
                                            {
                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDate1(Convert.ToDateTime(ic), entry.InternalComplianceID, entry.ID);
                                            }
                                            else if (Icompliances.EffectiveDate != null)
                                            {
                                                if (Icompliances.EffectiveDate > lastSchedule.ScheduledOn.Date)
                                                {
                                                    nextDateMonth = Business.InternalComplianceManagement.GetNextDate(Convert.ToDateTime(Icompliances.EffectiveDate), entry.InternalComplianceID, entry.ID, true);
                                                }
                                                else
                                                {
                                                    nextDateMonth = Business.InternalComplianceManagement.GetNextDate(Convert.ToDateTime(lastSchedule.ScheduledOn.Date), entry.InternalComplianceID, entry.ID, false);
                                                }
                                            }
                                            else
                                            {
                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDate(lastSchedule.ScheduledOn, entry.InternalComplianceID, entry.ID, false);
                                            }
                                        }
                                        else
                                        {
                                            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(entry.ID);
                                            if (getinstancescheduleondate != null)
                                            {
                                                if (Icompliances.EffectiveDate != null)
                                                {
                                                    DateTime rahuldt = new DateTime();
                                                    bool checkisedit = GetComplianceH_Edit_InternalCompliance(entry.ID);
                                                    if (checkisedit)
                                                    {
                                                        rahuldt = Convert.ToDateTime(Icompliances.EffectiveDate);
                                                    }
                                                    else
                                                    {
                                                        rahuldt = Convert.ToDateTime(getinstancescheduleondate);
                                                    }
                                                    nextDateMonth = Business.InternalComplianceManagement.GetNextDate(Convert.ToDateTime(rahuldt), entry.InternalComplianceID, entry.ID, false);
                                                }
                                                else
                                                {
                                                    nextDateMonth = Business.InternalComplianceManagement.GetNextDate(Convert.ToDateTime(getinstancescheduleondate), entry.InternalComplianceID, entry.ID, false);
                                                }
                                            }
                                            else
                                            {
                                                nextDateMonth = Business.InternalComplianceManagement.GetNextDate(curruntDate, entry.InternalComplianceID, entry.ID, false);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        nextDateMonth = Business.InternalComplianceManagement.GetNextDate(curruntDate, entry.InternalComplianceID, entry.ID, false);
                                    }

                                    if (nextDateMonth.Item1 != Convert.ToDateTime(ic))
                                    {
                                        long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
                                                             where row.InternalComplianceInstanceID == entry.ID
                                                            && row.ForMonth == nextDateMonth.Item2
                                                            && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                             select row.ID).FirstOrDefault();

                                        if (ScheduleOnID <= 0)
                                        {
                                            InternalComplianceScheduledOn internalcomplianceScheduleon = new InternalComplianceScheduledOn();
                                            internalcomplianceScheduleon.InternalComplianceInstanceID = entry.ID;
                                            internalcomplianceScheduleon.ScheduledOn = nextDateMonth.Item1.Date;
                                            internalcomplianceScheduleon.ForMonth = nextDateMonth.Item2;
                                            internalcomplianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                            internalcomplianceScheduleon.IsActive = true;
                                            internalcomplianceScheduleon.IsUpcomingNotDeleted = true;
                                            entities.InternalComplianceScheduledOns.Add(internalcomplianceScheduleon);
                                            entities.SaveChanges();

                                            var AssignedRole = GetAssignedUsers((int) entry.ID);
                                            var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                            if (performerRole != null)
                                            {
                                                var user = UserManagement.GetByID((int) performerRole.UserID);
                                                InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                                {
                                                    InternalComplianceInstanceID = entry.ID,
                                                    InternalComplianceScheduledOnID = internalcomplianceScheduleon.ID,
                                                    CreatedBy = performerRole.UserID,
                                                    CreatedByText = user.FirstName + " " + user.LastName,
                                                    StatusId = 1,
                                                    Remarks = "New Internal compliance assigned."
                                                };

                                                CreateTransaction(transaction);

                                                foreach (var roles in AssignedRole)
                                                {
                                                    if (roles.RoleID != 6)
                                                    {
                                                        CreateReminders(entry.InternalComplianceID, entry.ID, roles.ID, nextDateMonth.Item1);
                                                    }
                                                }
                                            }

                                            #region Task Scheduele
                                            // long CustomerBranchID = GetCustomerBranchID(entry.CustomerBranchID);
                                            var Taskapplicable = TaskManagment.TaskApplicable(entry.CustomerBranchID);
                                            if (Taskapplicable != null)
                                            {
                                                if (Taskapplicable == 1)
                                                {
                                                    var TaskIDList = (from row in entities.Tasks
                                                                      join row1 in entities.TaskComplianceMappings
                                                                      on row.ID equals row1.TaskID
                                                                      where row1.ComplianceID == Icompliances.ID
                                                                      && row.TaskType == 2 && row.ParentID == null
                                                                      && row.Isdeleted == false && row.IsActive == true
                                                                      select row.ID).ToList();

                                                    if (TaskIDList.Count > 0)
                                                    {
                                                        TaskIDList.ForEach(entrytask =>
                                                        {
                                                            List<TaskNameValue> TaskSubTaskList = new List<TaskNameValue>();

                                                            var taskSubTaskList = (from row in entities.Tasks
                                                                                   join row1 in entities.TaskInstances
                                                                                   on row.ID equals row1.TaskId
                                                                                   where row.MainTaskID == entrytask && row1.CustomerBranchID == entry.CustomerBranchID
                                                                                   select new { row.ID, row.ParentID, row.DueDays }).ToList();

                                                            taskSubTaskList.ForEach(entrysubtask =>
                                                            {
                                                                TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long) entrysubtask.ID, ParentID = (int?) entrysubtask.ParentID, ActualDueDays = (int) entrysubtask.DueDays });

                                                            });

                                                            TaskSubTaskList.ForEach(entryTaskSubTaskList =>
                                                            {

                                                                var taskInstances = (from row in entities.TaskInstances
                                                                                     where row.IsDeleted == false && row.TaskId == entryTaskSubTaskList.TaskID
                                                                                     && row.ScheduledOn <= nextDateMonth.Item1.Date
                                                                                     && row.CustomerBranchID == entry.CustomerBranchID
                                                                                     select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

                                                                taskInstances.ForEach(entrytaskinstance =>
                                                                {
                                                                    TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
                                                                    taskScheduleOn.TaskInstanceID = entrytaskinstance.ID;
                                                                    taskScheduleOn.ComplianceScheduleOnID = internalcomplianceScheduleon.ID;
                                                                    taskScheduleOn.ForMonth = nextDateMonth.Item2;
                                                                    taskScheduleOn.ForPeriod = nextDateMonth.Item3;
                                                                    taskScheduleOn.ScheduleOn = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    taskScheduleOn.IsActive = true;
                                                                    taskScheduleOn.IsUpcomingNotDeleted = true;
                                                                    entities.TaskScheduleOns.Add(taskScheduleOn);
                                                                    entities.SaveChanges();

                                                                    var AssignedTaskRole = GetAssignedTaskUsers((int) entrytaskinstance.ID);
                                                                    var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                                                    if (performerTaskRole != null)
                                                                    {
                                                                        var user = UserManagement.GetByID((int) performerTaskRole.UserID);
                                                                        TaskTransaction tasktransaction = new TaskTransaction()
                                                                        {
                                                                            TaskInstanceId = entrytaskinstance.ID,
                                                                            TaskScheduleOnID = taskScheduleOn.ID,
                                                                            ComplianceScheduleOnID = internalcomplianceScheduleon.ID,
                                                                            CreatedBy = performerTaskRole.UserID,
                                                                            CreatedByText = user.FirstName + " " + user.LastName,
                                                                            StatusId = 1,
                                                                            Remarks = "New task assigned."
                                                                        };

                                                                        TaskManagment.CreateTaskTransaction(tasktransaction);

                                                                        foreach (var roles in AssignedTaskRole)
                                                                        {
                                                                            if (roles.RoleID != 6)
                                                                            {
                                                                                CreateTaskReminders(entrytaskinstance.TaskId, Icompliances.ID, roles.ID, nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays)), internalcomplianceScheduleon.ID);
                                                                            }
                                                                        }
                                                                    }

                                                                });
                                                            });
                                                        });
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                    }//added by rahul
                                    #endregion
                                }
                            }
                        }
                   });
                }
            }
            catch (Exception ex)
            {

                long newtestid = testid;
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "InternalComplianceManagement";
                msg.FunctionName = "GenerateScheduleOnForUpcomingInternal" + "InternalComplianceInstanceID Error" + testid;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as Internal CreateReminders Function", " Internal GenerateScheduleOnForUpcomingInternal" + "InternalComplianceInstanceID Error" + testid);


            }
       }
        public static List<TaskAssignment> GetAssignedTaskUsers(int TAskInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedUsers = (from row in entities.TaskAssignments
                                     where row.TaskInstanceID == TAskInstanceID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

                return AssignedUsers;
            }
        }
        public static long GetCustomerBranchID(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long branchid = (from row in entities.InternalComplianceInstances
                                 where row.ID == complianceInstanceID
                                 select row.CustomerBranchID).FirstOrDefault();

                return branchid;
            }
        }
        public static void CreateTaskReminders(long TaskID, long InternalComplianceID, int TaskAssignmentId, DateTime scheduledOn, long ComplianceScheduleOnID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    InternalCompliance compliance = (from row in entities.InternalCompliances
                                                     where row.ID == InternalComplianceID
                                                     select row).FirstOrDefault();

                    if (compliance.ISubComplianceType == 0)
                    {

                        if (compliance.IReminderType == 0)
                        {
                            TaskReminder reminder = new TaskReminder()
                            {
                                TaskAssignmentID = TaskAssignmentId,
                                ComplianceScheduleOnID = ComplianceScheduleOnID,
                                ReminderTemplateID = 1,
                                TaskDueDate = scheduledOn,
                                RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                            };
                            reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                            entities.TaskReminders.Add(reminder);


                            TaskReminder reminder1 = new TaskReminder()
                            {
                                TaskAssignmentID = TaskAssignmentId,
                                ComplianceScheduleOnID = ComplianceScheduleOnID,
                                ReminderTemplateID = 1,
                                TaskDueDate = scheduledOn,
                                RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                            };
                            reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                            entities.TaskReminders.Add(reminder1);
                        }
                        else
                        {
                            DateTime TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.IReminderBefore)));
                            while (TempScheduled.Date < scheduledOn)
                            {
                                TaskReminder reminder = new TaskReminder()
                                {
                                    TaskAssignmentID = TaskAssignmentId,
                                    ComplianceScheduleOnID = ComplianceScheduleOnID,
                                    ReminderTemplateID = 1,
                                    TaskDueDate = scheduledOn,
                                    RemindOn = TempScheduled.Date,
                                };

                                reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                entities.TaskReminders.Add(reminder);

                                TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.IReminderGap));
                            }
                        }
                    }//Function Based
                    else
                    {

                        if (compliance.IReminderType == 0)
                        {
                            if (compliance.IFrequency.HasValue)
                            {
                                var reminders = (from row in entities.ReminderTemplates
                                                 where row.Frequency == compliance.IFrequency.Value && row.IsSubscribed == true
                                                 select row).ToList();

                                reminders.ForEach(day =>
                                {
                                    TaskReminder reminder = new TaskReminder()
                                    {
                                        TaskAssignmentID = TaskAssignmentId,
                                        ComplianceScheduleOnID = ComplianceScheduleOnID,
                                        ReminderTemplateID = 1,
                                        TaskDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date.AddDays(-1 * day.TimeInDays),
                                    };

                                    reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                    entities.TaskReminders.Add(reminder);

                                });
                            }
                        }
                        else
                        {
                            DateTime TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.IReminderBefore)));
                            while (TempScheduled.Date < scheduledOn)
                            {
                                TaskReminder reminder = new TaskReminder()
                                {
                                    TaskAssignmentID = TaskAssignmentId,
                                    ComplianceScheduleOnID = ComplianceScheduleOnID,
                                    ReminderTemplateID = 1,
                                    TaskDueDate = scheduledOn,
                                    RemindOn = TempScheduled.Date,
                                };

                                reminder.Status = (byte) (reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                entities.TaskReminders.Add(reminder);
                                TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.IReminderGap));
                            }
                        }
                    }//function Based End

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "TaskManagement";
                //msg.FunctionName = "CreateReminders";
                msg.FunctionName = "CreateTAskReminders" + "TaskAssignmentId" + TaskAssignmentId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as task CreateReminders Function", "Task CreateReminders" + "TaskAssignmentId" + TaskAssignmentId);

            }
        }

        public static List<InternalComplianceReminderView> ProcessComplianceUpcomingRemindersInternal(DateTime date)
       {
           date = date.Date;
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {

               var complianceReminders = (from row in entities.InternalComplianceReminderViews
                                          where row.Status == 0 && row.ScheduledOn >= date && row.RemindOn == date
                                          && (row.ComplianceStatusID == 1 && row.RoleID == 3 || (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3) && (row.RoleID == 4 || row.RoleID == 5))
                                          select row).ToList();
              
               return complianceReminders;
           }

       }

       public static void UpdateRemindersInternal(List<int> reminders)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               reminders.ForEach(entry =>
               {

                   InternalComplianceReminder reminder = (from row in entities.InternalComplianceReminders
                                                  where row.ID == entry
                                                  select row).FirstOrDefault();

                   reminder.Status = 1;
               });

               entities.SaveChanges();
           }
       }


       public static List<InternalComplianceReminderView> ProcessComplianceOverdueRemindersInternal(DateTime date)
       {
           date = date.Date;
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {

               var complianceReminders = (from row in entities.InternalComplianceReminderViews
                                          where row.ScheduledOn <= date && ((row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.RoleID == 3 || (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3) && (row.RoleID == 4 || row.RoleID == 5))
                                          select row).GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
              
               return complianceReminders;
           }

       }

       public static bool CheckComplianceReminderNotificationInternal(long userID, DateTime currentDate)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var data = (from row in entities.InternalComplianceReminderNotifications
                           where row.UserID == userID && row.SentOn == currentDate
                           select row).FirstOrDefault();
               if (data == null)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
       }

       //added by Rahul for sumary email escalation notification
       public static void AddComplianceReminderNotificationInternal(List<InternalComplianceReminderNotification> objEscalation)
       {

           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               objEscalation.ForEach(entry =>
               {
                   entities.InternalComplianceReminderNotifications.Add(entry);
                   entities.SaveChanges();

               });
           }

       }

       //added by Rahul for sumary email escalation notification
       public static void AddEscalationDetailTableInternal(List<InternalComplianceEscalationTable> objEscalation)
       {

           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               objEscalation.ForEach(entry =>
               {
                   entities.InternalComplianceEscalationTables.Add(entry);
                   entities.SaveChanges();

               });
           }

       }

       public static long GetbyID_USERID(long IScheduledOnID, string flag)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               long getuserid = 0;
               if (flag == "P")
               {

                   getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                where row.InternalScheduledOnID == IScheduledOnID && row.RoleID == 3
                                select row.UserID).FirstOrDefault();

                   // getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 3).Select(x => x.UserID);
               }
               else if (flag == "R")
               {
                   getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                where row.InternalScheduledOnID == IScheduledOnID && row.RoleID == 4
                                select row.UserID).FirstOrDefault();
                   //getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 4).Select(x => x.UserID);
               }
               else if (flag == "A")
               {
                   getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                where row.InternalScheduledOnID == IScheduledOnID && row.RoleID == 6
                                select row.UserID).FirstOrDefault();
                   //getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 6).Select(x => x.UserID);
               }
               return getuserid;
           }
       }
       public static string GetbyName_USERNAME(long IScheduledOnID, string flag)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               string getuserid = "";
               if (flag == "P")
               {

                   getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                where row.InternalScheduledOnID == IScheduledOnID && row.RoleID == 3
                                select row.User).FirstOrDefault();

                   // getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 3).Select(x => x.UserID);
               }
               else if (flag == "R")
               {
                   getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                where row.InternalScheduledOnID == IScheduledOnID && row.RoleID == 4
                                select row.User).FirstOrDefault();
                   //getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 4).Select(x => x.UserID);
               }
               else if (flag == "A")
               {
                   getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                where row.InternalScheduledOnID == IScheduledOnID && row.RoleID == 6
                                select row.User).FirstOrDefault();
                   //getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 6).Select(x => x.UserID);
               }
               return getuserid;
           }
       }
       public static List<InternalComplianceCannedReport> GetEscalationNotificationsForDay1Internal(DateTime date)
       {

           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var ComplianceEscalationRecords = entities.InternalComplianceEscalationNotificationViews.AsEnumerable()
                                                   .Select(r => new { r.User, r.UserID, r.CustomerID, r.ReportingToID, r.InternalScheduledOnID, r.InternalComplianceStatusID, r.InternalScheduledOn, r.RoleID, r.Role, r.Risk, r.InternalComplianceInstanceID, r.ForMonth, r.CustomerBranchID, r.Branch, r.ShortDescription })
                                                   .Where(r => r.InternalScheduledOn < date)
                                                   .GroupBy(x => new { x.InternalScheduledOnID })
                                                   .Select(g => new InternalComplianceCannedReport()
                                                   {
                                                       InternalComplianceInstanceID = g.First().InternalComplianceInstanceID,
                                                       CustomerBranchID = g.First().CustomerBranchID,
                                                       CustomerID = (int)g.First().CustomerID,
                                                       InternalComplianceStatusID = (int)g.First().InternalComplianceStatusID,
                                                       InternalScheduledOn = g.First().InternalScheduledOn,
                                                       ForMonth = g.First().ForMonth,
                                                       Risk = g.First().Risk,
                                                       ShortDescription = g.First().ShortDescription,
                                                       CustomerBranchName = g.First().Branch,
                                                       IPerformerID = GetbyID_USERID((long)g.First().InternalScheduledOnID, "P"),// g.Where(n => n.RoleID == 3).Select(x => x.UserID).FirstOrDefault(),
                                                       IReviewerID = GetbyID_USERID((long)g.First().InternalScheduledOnID, "R"),//g.Where(n => n.RoleID == 4).Select(x => x.UserID).FirstOrDefault(),
                                                       IApproverID = GetbyID_USERID((long)g.First().InternalScheduledOnID, "A"), //g.Where(n => n.RoleID == 6).Select(x => x.UserID).FirstOrDefault(),
                                                       IPerformer = GetbyName_USERNAME((long)g.First().InternalScheduledOnID, "P"),// g.Where(n => n.RoleID == 3).Select(x => x.User).FirstOrDefault(),
                                                       IReviewer = GetbyName_USERNAME((long)g.First().InternalScheduledOnID, "R"), //g.Where(n => n.RoleID == 4).Select(x => x.User).FirstOrDefault(),
                                                       IApprover = GetbyName_USERNAME((long)g.First().InternalScheduledOnID, "A"),//g.Where(n => n.RoleID == 6).Select(x => x.User).FirstOrDefault(),

                                                   }).ToList();

               return ComplianceEscalationRecords;
           }

       }
       
       public static bool CheckEscalationNotification1Internal(long userID, DateTime currentDate)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var data = (from row in entities.InternalComplianceEscalationTables
                           where row.UserID == userID && row.SentOn == currentDate
                           select row).FirstOrDefault();
               if (data == null)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
       }

       public static List<InternalComplianceAssignment> GetAssignedRoles(int userID = -1)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               List<InternalComplianceAssignment> AssignedRoles = new List<InternalComplianceAssignment>();
               if (userID != -1)
               {
                   AssignedRoles = (from row in entities.InternalComplianceAssignments
                                    where row.UserID == userID
                                    select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();
               }
               else
               {
                   AssignedRoles = (from row in entities.InternalComplianceAssignments
                                    select row).ToList();
               }

               return AssignedRoles;
           }
       }

       public static void AddApproverNotificationInternal(List<InternalApproverNotification> objApproverNotification)
       {

           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               objApproverNotification.ForEach(entry =>
               {
                   entities.InternalApproverNotifications.Add(entry);
                   entities.SaveChanges();

               });
           }

       }
       
       public static bool CheckApproverNotificationInternal(long userId, DateTime sentOn)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               return (from row in entities.InternalApproverNotifications
                       where row.UserId == userId && row.SentOn == sentOn
                       select true).FirstOrDefault();
           }
       }

       public static bool IsCustomerActive(long customerID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var query = (from row in entities.Customers
                            where row.IsDeleted == false && row.Status == 1 && row.ID == customerID
                            select row);

               if (customerID > 0)
               {
                   query = query.Where(entry => entry.ID == customerID);
               }

               return query.Select(entry => true).SingleOrDefault();
           }
       }

       public static InternalCompliance GetInternalComplianceByInstanceID(int ScheduledOnID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {

               long complianceInstanceID = (from row in entities.InternalComplianceTransactions
                                            where row.InternalComplianceScheduledOnID == ScheduledOnID
                                            select row.InternalComplianceInstanceID).FirstOrDefault();

               long complianceID = (from row in entities.InternalComplianceInstances
                                    where row.ID == complianceInstanceID
                                    select row.InternalComplianceID).FirstOrDefault();

               var compliance = (from row in entities.InternalCompliances
                                 where row.ID == complianceID
                                 select row).FirstOrDefault();

               return compliance;
           }
       }
        public static InternalCompliancesCategory GetInternalComplianceCategoryByInstanceID(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long complianceInstanceID = (from row in entities.InternalComplianceTransactions
                                             where row.InternalComplianceScheduledOnID == ScheduledOnID
                                             select row.InternalComplianceInstanceID).FirstOrDefault();

                long complianceID = (from row in entities.InternalComplianceInstances
                                     where row.ID == complianceInstanceID
                                     select row.InternalComplianceID).FirstOrDefault();

                var compliance = (from row in entities.InternalCompliances
                                  join row2 in entities.InternalCompliancesCategories
                                  on row.IComplianceCategoryID equals row2.ID
                                  where row.ID == complianceID
                                  select row2).FirstOrDefault();

                return compliance;
            }
        }
        public static InternalCompliance GetInternalCompliance(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Compliance = (from row in entities.InternalCompliances
                            where row.ID == complianceID
                            select row).FirstOrDefault();

                return Compliance;
            }
        }
        public static InternalComplianceForm GetInternalComplianceFormByID(long complianceID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var form = (from row in entities.InternalComplianceForms
                           where row.IComplianceID == complianceID
                           select row).FirstOrDefault();

               return form;
           }
       }
       public static string GetPanalty(InternalCompliance compliance)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               string Penalty = "";
               #region Comment by Rahul 9 Dec 2015
               //if (compliance.NonComplianceType != null)
               //{
               //    if (compliance.NonComplianceType == 0)
               //    {
               //        Penalty = "Penalty of minimum " + compliance.FixedMinimum + " and maximum " + compliance.FixedMaximum + " Rupees is applicable.";
               //    }
               //    else if (compliance.NonComplianceType == 1)
               //    {
               //        if (Convert.ToBoolean(compliance.Imprisonment))
               //        {
               //            Penalty = "Penalty of minimum " + compliance.MinimumYears + " and maximum " + compliance.MaximumYears + " years of Imprisonment for " + compliance.Designation + " of the company.";
               //        }
               //        else
               //        {
               //            Penalty = compliance.Others;
               //        }

               //    }
               //    else if (compliance.NonComplianceType == 2)
               //    {
               //        string Panalty1 = "Penalty of minimum " + compliance.FixedMinimum + " and maximum " + compliance.FixedMaximum + " Rupees is applicable";
               //        string Panalty2 = "minimum " + compliance.MinimumYears + " and maximum " + compliance.MaximumYears + " years of Imprisonment for " + compliance.Designation + " of the company.";
               //        Penalty = Panalty1 + " and " + Panalty2;
               //    }
               //}
               //else
               //{
               //    Penalty = "No Penalty Defined.";
               //}
               #endregion
               return Penalty;
           }
       }
      
        public static string GetRiskType(InternalCompliance compliance)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string risk = "";
                if (compliance.IRiskType == 0)
                    risk = "HIGH";
                else if (compliance.IRiskType == 1)
                    risk = "MEDIUM";
                else if (compliance.IRiskType == 2)
                    risk = "LOW";
                else if (compliance.IRiskType == 3)
                    risk = "CRITICAL";

                return risk;
            }
        }

        public static string GetRisk(InternalCompliance compliance)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string risk = "";
                if (compliance.IRiskType == 0)
                    risk = "HIGH risk compliance.";
                else if (compliance.IRiskType == 1)
                    risk = "MEDIUM risk compliance.";
                else if (compliance.IRiskType == 2)
                    risk = "LOW risk compliance.";
                else if (compliance.IRiskType == 3)
                    risk = "CRITICAL risk compliance.";

                return risk;
            }
        }


        public static InternalComplianceInstanceTransactionView GetInstanceTransactionInternalCompliance(long ScheduledOnID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var ReviceCompliances = (from row in entities.InternalComplianceInstanceTransactionViews
                                        where row.InternalScheduledOnID == ScheduledOnID && row.RoleID == 3
                                        && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                        select row).FirstOrDefault();

               return ReviceCompliances;
           }
       }

       public static InternalComplianceTransactionView GetClosedTransactionInternal(int ScheduledOnID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var statusList = (from row in entities.InternalComplianceTransactionViews
                                 where row.InternalComplianceScheduledOnID == ScheduledOnID
                                 && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 7 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 9)
                                 orderby row.ComplianceTransactionID descending
                                 select row).ToList();



               return statusList.FirstOrDefault();
           }
       }

      

       //added By manisha
       public static List<GetInternalComplianceDocumentsView> GetInternalDocumnets(long ScheduledOnID, long transactionID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               DateTime curruntDate = DateTime.Now.AddDays(-1);

               var DocumentList = (from row in entities.GetInternalComplianceDocumentsViews
                                   where row.InternalComplianceScheduledOnID == ScheduledOnID && row.TransactionID == transactionID
                                   select row).ToList();

               return DocumentList;
           }
       }

        public static List<GetInternalComplianceDocumentsView> GetInternalDocumnetsKendo(long ScheduledOnID, long transactionID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime curruntDate = DateTime.Now.AddDays(-1);

                var DocumentList = (from row in entities.GetInternalComplianceDocumentsViews
                                    where row.InternalComplianceScheduledOnID == ScheduledOnID 
                                    select row).ToList();

                return DocumentList;
            }
        }
        // added By Manisha
        //public static List<ComplianceInstanceTransactionView> GetTransactionsByUserID(long userID, string filter = null)
        public static List<InternalComplianceInstanceTransactionView> GetTransactionsByUserID(long userID, string filter = null)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
              
               var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        where (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                        select row);

               DateTime nextMonthDate = DateTime.UtcNow.AddMonths(1);

               if (userID != -1)
               {
                   transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID);
               }

               if (!string.IsNullOrEmpty(filter))
               {
                   transactionsQuery = transactionsQuery.Where(entry => entry.Branch.Contains(filter) || entry.ShortDescription.Contains(filter) || entry.Status.Contains(filter) || entry.Role.Contains(filter) || entry.User.Contains(filter));
               }

               return transactionsQuery.Where(entry => entry.InternalScheduledOn <= nextMonthDate).OrderByDescending(entry => entry.InternalScheduledOn).ToList();
           }
       }
       //added By Manisha
       public static void DeleteComplianceAssignment(int UserID, List<Tuple<int, string>> ComplianceInstanceIdList,int Customerid)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               foreach (var cil in ComplianceInstanceIdList)
               {
                   int role = -1;
                   if (cil.Item2.Equals("Performer"))
                       role = 3;
                   if (cil.Item2.Equals("Reviewer1"))
                       role = 4;
                   if (cil.Item2.Equals("Reviewer2"))
                       role = 5;
                   if (cil.Item2.Equals("Approver"))
                       role = 6;

                    #region Task Assignmnet Internal Exclude

                    var compliance = TaskManagment.GetInternalComplianceIDFromInstance(Convert.ToInt32(cil.Item1));

                    TaskManagment.DeleteTaskAssignmentFromInternalCompliance(compliance.InternalComplianceID, compliance.CustomerBranchID, role, Customerid);

                    #endregion

                    var assignmentData = (from row in entities.InternalComplianceAssignments
                                         where row.InternalComplianceInstanceID == cil.Item1 && row.RoleID == role && row.UserID == UserID
                                         select row).FirstOrDefault();


                   List<InternalComplianceReminder> reminderData = (from row in entities.InternalComplianceReminders
                                                                    where row.ComplianceAssignmentID == assignmentData.ID
                                                                    select row).ToList();

                   reminderData.ForEach(entry =>
                          entities.InternalComplianceReminders.Remove(entry)
                       );


                   entities.InternalComplianceAssignments.Remove(assignmentData);
               }
               entities.SaveChanges();
           }
       }

       //added by Manisha
       public static void ReplaceUserForComplianceAssignment(int oldUserID, int newUserID, List<Tuple<int, string>> ComplianceInstanceIdList)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {


               foreach (var cil in ComplianceInstanceIdList)
               {
                   int role = -1;
                   if (cil.Item2.Equals("Performer"))
                       role = 3;
                   if (cil.Item2.Equals("Reviewer1"))
                       role = 4;
                   if (cil.Item2.Equals("Reviewer2"))
                       role = 5;
                   if (cil.Item2.Equals("Approver"))
                       role = 6;

                  
                   var assignmentData = (from row in entities.InternalComplianceAssignments
                                         where row.InternalComplianceInstanceID == cil.Item1 && row.RoleID == role && row.UserID == oldUserID
                                         select row).FirstOrDefault();

                   if (assignmentData != null)
                   {
                       assignmentData.UserID = newUserID;
                   }
               }


               entities.SaveChanges();
           }
       }
        public static void ReplaceUserForComplianceAssignmentNew(int oldUserID, int newUserID, List<Tuple<int, string, int>> ComplianceInstanceIdList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                foreach (var cil in ComplianceInstanceIdList)
                {
                    int role = -1;
                    if (cil.Item2.Equals("Performer"))
                        role = 3;
                    if (cil.Item2.Equals("Reviewer1"))
                        role = 4;
                    if (cil.Item2.Equals("Reviewer2"))
                        role = 5;
                    if (cil.Item2.Equals("Approver"))
                        role = 6;


                    var assignmentData = (from row in entities.InternalComplianceAssignments
                                          where row.InternalComplianceInstanceID == cil.Item1 && row.RoleID == role && row.UserID == cil.Item3
                                          select row).FirstOrDefault();

                    if (assignmentData != null)
                    {
                        assignmentData.UserID = newUserID;
                    }
                }


                entities.SaveChanges();
            }
        }

        
        public static void CreateHistoryDueDate(H_Internal_ChangeDueDate HistoryDueDate)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               entities.H_Internal_ChangeDueDate.Add(HistoryDueDate);
               entities.SaveChanges();
           }
       }


        public static List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> GetInternalComplianceDetailsDashboard(int customerid, List<long> Branchlist, int RoleID, int CustomerBranchID, int CategoryID, int Userid, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> ComplianceDetails = new List<sp_InternalComplianceInstanceAssignmentViewDetails_Result>();

              
                if (IsApprover == true)
                {
                    ComplianceDetails = (from row in entities.sp_InternalComplianceInstanceAssignmentViewDetails(Userid, "APPR")
                                         select row).ToList();
                }
                else
                {
                    ComplianceDetails = (from row in entities.sp_InternalComplianceInstanceAssignmentViewDetails(Userid, "MGMT")
                                         select row).ToList();
                   
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }
               
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.InternalComplianceCategoryID == CategoryID).Distinct().ToList();
                }
                
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.InternalComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }
        //public static List<InternalComplianceAssignedInstancesView> GetInternalComplianceDetailsDashboard(int customerid, List<long> branchlist, int RoleID, int CustomerBranchID, int CategoryID, int Userid, bool IsApprover = false)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<InternalComplianceAssignedInstancesView> ComplianceDetails = new List<InternalComplianceAssignedInstancesView>();
        //        if (IsApprover == true)
        //        {
        //            ComplianceDetails = (from row in entities.InternalComplianceAssignedInstancesViews
        //                                 join row1 in entities.CustomerBranches
        //                                  on row.CustomerBranchID equals row1.ID
        //                                 where row.CustomerID == customerid && row.RoleID == 6
        //                                 && row.UserID == Userid
        //                                 select row).Distinct().ToList();
        //        }
        //        else
        //        {
        //            ComplianceDetails = (from row in entities.InternalComplianceAssignedInstancesViews
        //                                 join row1 in entities.EntitiesAssignmentInternals
        //                                  on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
        //                                 where row.CustomerID == customerid && row.CustomerBranchID == row1.BranchID
        //                                 && row1.UserID == Userid && row.RoleID == RoleID
        //                                 select row).Distinct().ToList();
        //        }


        //        //if (RoleID != -1)
        //        //{
        //        //    if (RoleID == 6)
        //        //    {
        //        //        ComplianceDetails = ComplianceDetails.Where(row => row.RoleID == RoleID).Distinct().ToList();
        //        //    }
        //        //}

        //        if (branchlist.Count > 0)
        //        {
        //            ComplianceDetails = ComplianceDetails.Where(row => branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
        //        }
        //        //if (CustomerBranchID != -1)
        //        //{
        //        //    ComplianceDetails = ComplianceDetails.Where(row => row.CustomerBranchID == CustomerBranchID).Distinct().ToList();
        //        //}

        //        if (CategoryID != -1)
        //        {
        //            ComplianceDetails = ComplianceDetails.Where(row => row.InternalComplianceCategoryID == CategoryID).Distinct().ToList();
        //        }
        //        ComplianceDetails = ComplianceDetails.GroupBy(a => a.InternalComplianceID).Select(a => a.First()).ToList();
        //        return ComplianceDetails;
        //    }
        //}


        public static long GetComplianceID(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long id = (from row in entities.InternalComplianceInstances
                           where row.ID == complianceInstanceID
                           select row.InternalComplianceID).FirstOrDefault();

                return id;
            }
        }
     
        public static InternalComplianceInfoByCustomer GetDetailsCustomerID(int CustID, int CompID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var details = (from row in entities.InternalComplianceInfoByCustomers
                               where row.CustomerID == CustID
                               && row.ComplianceID == CompID
                               && row.IsActive == true
                               select row).FirstOrDefault();

                return details;
            }
        }
        public static CancelledInternalComplianceAssignment GetDetailsComplianceScheduled(int CompInstanceID, int ScheduleonID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var details = (from row in entities.CancelledInternalComplianceAssignments
                               where row.ComplianceInstanceID == CompInstanceID
                               && row.ScheduleonID == ScheduleonID
                               select row).FirstOrDefault();

                return details;
            }
        }

        public static List<SP_GetInternalComplianceManagementMaster_Result> GetComplianceManagementMaster(int CustId)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var compliancesQuery = (from row in entities.SP_GetInternalComplianceManagementMaster(CustId)
                                        select row).ToList();

                return compliancesQuery;

            }
        }
        public static List<SP_GetAllInstanceLogDetail_Result> GetComplianceLogDetails(List<long> InstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var compliancesQuery = (from row in entities.SP_GetAllInstanceLogDetail(0)
                                        select row).ToList();

                compliancesQuery = compliancesQuery.Where(x => InstanceId.Contains(x.ComplianceInstanceID)).ToList();

                return compliancesQuery;

            }
        }
        public static List<SP_GetAllInstanceDetail_Result> GetComplianceScheduledOnDetails(int InstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var compliancesQuery = (from row in entities.SP_GetAllInstanceDetail(InstanceId)
                                        select row).ToList();

                return compliancesQuery;

            }
        }

    }
}
