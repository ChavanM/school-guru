﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Threading;
using System.Net.Mail;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Web;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class UserManagement
    {
        public static bool IsSelectedUserUniverse(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var assignedCount = (from row in entities.Mst_FolderMaster
                                     where row.CreatedBy == userID
                                     && row.IsUniverse == 1
                                     && row.ParentID == null
                                     select 1).Count();

                return assignedCount > 0;
            }
        }
        
        public static bool HasInternalCompliancesAssigned(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var assignedCount = (from row in entities.InternalComplianceInstanceViews
                                     where row.UserID == userID
                                     select 1).Count();

                return assignedCount > 0;
            }
        }
        public static bool HasEventOwnerAssigned(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var assignedCount = (from row in entities.EventAssignments
                                     where row.UserID == userID
                                     select 1).Count();

                return assignedCount > 0;
            }
        }

        public static bool HasTaskAssigned(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var assignedCount = (from row in entities.TaskAssignments
                                     where row.UserID == userID
                                     select 1).Count();

                return assignedCount > 0;
            }
        }
        public static bool ComplianceAssigned(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var assignedCount = (from row in entities.TempAssignmentTableInternals
                                     where row.UserID == userID
                                     select 1).Count();

                return assignedCount > 0;
            }
        }

        public static string GetEmailByUserID(int userID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())

            {

                var UsersList = (from row in entities.Users

                                 where row.ID == userID

                                 && row.IsActive == true

                                 && row.IsDeleted == false

                                 select row.Email).FirstOrDefault();



                return UsersList;

            }

        }

        public static bool GetMst_SkipOTPByCustID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UsersList = (from row in entities.Mst_SkipOTP
                                 where row.IsActive == true
                                 && row.Customerid == customerID
                                 select row).FirstOrDefault();
                if (UsersList != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static ClientCustomization GetAssignedEntity(string customizationname, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ClientCustomization CustomerToUpdate = (from Row in entities.ClientCustomizations
                                                        where Row.ClientID == customerid
                                                        && Row.CustomizationName == customizationname
                                                        select Row).FirstOrDefault();

                return CustomerToUpdate;
            }
        }
        public static int ExistBranchAssignment(int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.BranchAssignments
                                 where row.IsActive == true
                                     && row.CustomerID == CustomerID
                                 select row).FirstOrDefault();

                    if (query != null)
                        return 1;
                    else
                        return 0;
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        public static RLCS_User_Mapping GetByRLCSUsername(string username)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from RMU in entities.RLCS_User_Mapping
                            join U in entities.Users
                            on RMU.AVACOM_UserID equals U.ID
                            join C in entities.Customers on U.CustomerID equals C.ID
                            where RMU.UserID.Trim().ToUpper() == username.Trim().ToUpper()
                            && U.IsDeleted == false
                            && C.IsDeleted == false
                            select RMU).FirstOrDefault();

                return user;
            }
        }
        public static Customer GetCustomerforLogoCustomer(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Customer CustomerToUpdate = (from Row in entities.Customers
                                             where Row.ID == CustomerID
                                             select Row).FirstOrDefault();
                if (CustomerToUpdate == null)
                {
                    Customer objcust = (from Row in entities.Customers
                                        join row1 in entities.Customers
                                            on Row.ID equals row1.ID
                                        select Row).FirstOrDefault();
                    return objcust;
                }
                else
                {
                    return CustomerToUpdate;
                }
            }
        }

        public static Customer GetCustomerforLogo(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Customer CustomerToUpdate = (from Row in entities.Customers
                                             where Row.ID == customerID
                                             select Row).FirstOrDefault();

                return CustomerToUpdate;
            }
        }
        public static void UpdateCustomerLogo(int ID, string filepath)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Customer CustomerToUpdate = (from Row in entities.Customers
                                             where Row.ID == ID
                                             select Row).FirstOrDefault();

                if (CustomerToUpdate != null)
                {
                    CustomerToUpdate.LogoPath = filepath;
                    entities.SaveChanges();
                }
            }
        }
        public static ComplianceCertificateMapping GetComplianceCertificateMapping(long CustId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.ComplianceCertificateMappings
                            where row.CustomerID == CustId
                            && row.IsActive == true
                            select row).FirstOrDefault();

                return Data;
            }
        }
        public static bool CheckProductmapping(long customerID, int ProductId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productid = GetByProductID(Convert.ToInt32(customerID));
                List<long?> parameterIDs = productid.Select(entry => entry.ProductID).ToList();
                if (parameterIDs.Contains(ProductId))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static List<ProductMapping> GetByProductID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productmapping = (from row in entities.ProductMappings
                                      where row.CustomerID == customerID && row.IsActive == false
                                       select row).ToList();

                return productmapping;
            }
        }
        public static bool CheckProductmapping(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productid = GetByProductID(Convert.ToInt32(customerID));
                List<long?> parameterIDs = productid.Select(entry => entry.ProductID).ToList();
                if (parameterIDs.Contains(3))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static List<object> GetAllManagmentLitigationUser(int customerID = -1, int complianceProductType = 0)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.IsActive == true && row.LitigationRoleID !=null
                             select new { row.ID, row.FirstName, row.LastName, row.Email, row.CustomerID });

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }



                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).ToList<object>();

                return users;
            }
        }
        public static List<object> GetAllManagmentContractUser(int customerID = -1, int complianceProductType = 0)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.IsActive == true && row.ContractRoleID !=null
                             select new { row.ID, row.FirstName, row.LastName, row.Email, row.CustomerID });

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }



                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).ToList<object>();

                return users;
            }
        }

        #region Added by sagar more on 18-05-2020 for historical Observation

        public static int CreateNewAuditUser(User user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Users.Add(user);
                entities.SaveChanges();
                return Convert.ToInt32(user.ID);
            }
        }
        public static bool UserExistsByEmail(string Email, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                                 && row.Email == Email
                                 && row.CustomerID == CustomerID
                             select row);

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static User GetPersonalResponsibleDetailsByEmail(string Email, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                                 && row.Email == Email
                                 && row.CustomerID == CustomerID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return query;
                }
                else
                {
                    return null;
                }
            }
        }

        public static mst_User GetPersonalResponsibleDetailsByEmailInAuditDb(string Email, int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_User
                             where row.IsDeleted == false
                                 && row.Email == Email
                                 && row.CustomerID == CustomerID
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return query;
                }
                else
                {
                    return null;
                }


            }
        }

        public static bool UserExistsByEmailInAuditDb(string Email, int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_User
                             where row.IsDeleted == false
                                 && row.Email == Email
                                 && row.CustomerID == CustomerID
                             select row);

                return query.Select(entry => true).SingleOrDefault();
            }
        }


        //public static int GetUserIdByEmail(string Email, int customerID, int customerBranchID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var UserId = (from row in entities.mst_User
        //                      where row.Email == Email && row.CustomerID == customerID && row.CustomerBranchID == customerBranchID && row.IsDeleted == false && row.IsActive==true
        //                      select row.ID).SingleOrDefault();

        //        return Convert.ToInt32(UserId);
        //    }
        //}

        public static int GetUserIdByEmail(string Email, int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var UserData = (from row in entities.mst_User
                                where row.Email == Email.Trim()
                                && row.CustomerID == customerID
                                && row.IsDeleted == false
                                && row.IsActive == true
                                select row).FirstOrDefault();
                if (UserData != null)
                {
                    return Convert.ToInt32(UserData.ID);
                }
                return 0;
            }
        }

        public static bool CreateNewHistoryObservation(User user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Users.Add(user);
                entities.SaveChanges();
                return true;
            }
        }

        public static bool CreateNewHistoryObservationInAuditDb(mst_User user)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_User.Add(user);
                entities.SaveChanges();
                return true;
            }
        }
        #endregion
        public static bool ChangePassword_RLCS(int userID, string passwordText, string enType)
        {
            bool successChangePwd = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var usersToUpdate = (from RUM in entities.RLCS_User_Mapping
                                             where RUM.AVACOM_UserID == userID
                                             select RUM).ToList();

                        if (usersToUpdate != null)
                        {
                            if (usersToUpdate.Count > 0)
                            {
                                usersToUpdate.ForEach(row => row.Password = passwordText);
                                usersToUpdate.ForEach(row => row.UpdatedOn = DateTime.Now);
                                usersToUpdate.ForEach(row => row.EnType = enType);

                                entities.SaveChanges();
                                dbtransaction.Commit();
                                successChangePwd = true;
                            }
                        }
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        successChangePwd = false;
                    }
                }
            }
            return successChangePwd;
        }

        public static bool IsRLCSUser(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var userExists = (from RUM in entities.RLCS_User_Mapping
                                  where RUM.AVACOM_UserID == userID
                                  select RUM).FirstOrDefault();
                if (userExists != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool IsPageAutherzation(int userID, int custID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.PageAuthorizations
                             where row.UserID == userID
                             && row.CustomerID == custID
                             && row.PageName.Trim().Equals("LockingDetails")
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static List<User> GetUserRollWise(int customerID, int ddlModuleid, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             select row);

              
                if (ddlModuleid == -1)
                {
                    users = users.Where(entry => entry.RoleID == -1);
                }
                if (ddlModuleid == 1)
                {
                    users = users.Where(entry => entry.RoleID == -1);
                }
                else if (ddlModuleid == 2)
                {
                    users = users.Where(entry => entry.LitigationRoleID.Equals(null));
                }
                else if (ddlModuleid == 3)
                {
                    users = users.Where(entry => entry.ContractRoleID.Equals(null));
                }
                else if (ddlModuleid == 4)
                {
                    users = users.Where(entry => entry.LicenseRoleID.Equals(null));
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter));
                }

                return users.OrderBy(entry => entry.ID).ToList();
            }
        }     
        public static void CreateUserLoginTrack(UserLoginTrack obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.UserLoginTracks.Add(obj);
                entities.SaveChanges();
            }
        }
        public static bool RLCSChangePassword(RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        var userToUpdate = (from entry in entities.RLCS_User_Mapping
                                            where entry.UserID == user.UserID
                                            select entry).ToList();
                        userToUpdate.ForEach(entry =>
                        {
                            entry.Password = user.Password;
                            entry.EnType = "A";
                        });
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }
      
        public static RLCS_User_Mapping GetRLCSByID(string UserName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var emailQuery = (from RMU in entities.RLCS_User_Mapping
                                  join U in entities.Users
                                  on RMU.AVACOM_UserID equals U.ID
                                  join C in entities.Customers on U.CustomerID equals C.ID
                                  where RMU.UserID.Trim().ToUpper() == UserName.Trim().ToUpper()
                                  //&& C.ServiceProviderID == 94
                                  && U.IsDeleted == false
                                  && C.IsDeleted == false
                                  select RMU).FirstOrDefault();
                return emailQuery;

            }
        }
        public static int GetCustomerIDRLCS(string CorporateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productmapping = (from row in entities.RLCS_Customer_Corporate_Mapping
                                      where row.CO_CorporateID.Trim().ToUpper() == CorporateID.Trim().ToUpper()
                                      select row.AVACOM_CustomerID).FirstOrDefault();
                if (productmapping != null)
                {
                    return Convert.ToInt32(productmapping);
                }
                else
                {
                    return -1;
                }
            }
        }
        public static int GetRLCSRoleID(string code)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productmapping = (from row in entities.Roles
                                      where row.Code.Trim().ToUpper() == code.Trim().ToUpper()
                                      select row.ID).FirstOrDefault();
                if (productmapping != null)
                {
                    return Convert.ToInt32(productmapping);
                }
                else
                {
                    return -1;
                }
            }
        }

        //Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>>()
        public static Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> GetBlockIpAddress(long customerid,long userid,string IpAddressName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_CheckValidUserForIPAddress_Result> emailQuery = new List<SP_CheckValidUserForIPAddress_Result>();
                emailQuery = (from RMU in entities.SP_CheckValidUserForIPAddress(customerid, userid)
                              select RMU).ToList();
               
                if (emailQuery.Count > 0)
                {
                    //check because of left join on 9 MAR 2020
                    //var data = emailQuery.Where(a => a.UserID != null).ToList();
                    //if (data.Count == 0)
                    //{
                    //    Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> tuple = new Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>>(true, emailQuery);
                    //    return tuple;
                    //}
                    //else
                    //{
                        List<string> list = new List<string>();
                        list.Add(IpAddressName);
                        var result = emailQuery.Where(x => list.Contains(x.IPAddress)).ToList();
                        if (result.Count > 0)
                        {
                            Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> tuple = new Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>>(true, emailQuery);
                            return tuple;
                        }
                        else
                        {
                            Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> tuple = new Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>>(false, emailQuery);
                            return tuple;
                        }
                    //}
                }
                else
                {
                    Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> tuple = new Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>>(true, emailQuery);
                    return tuple;
                }
            }
        }
        public static List<string> GetBlockIpAddress()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<string> emailQuery = new List<string>();
                emailQuery = (from RMU in entities.Mst_BlockIP
                              select RMU.IPAddress).ToList();
                if (emailQuery.Count > 0)
                {
                    return emailQuery.ToList();
                }
                else
                {
                    return emailQuery.ToList();
                }
            }
        }
        public static bool EmailIDExists(string UserName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var emailQuery = (from RUM in entities.RLCS_User_Mapping
                                  join U in entities.Users on RUM.AVACOM_UserID equals U.ID
                                  join C in entities.Customers on U.CustomerID equals C.ID
                                  where RUM.Email.Trim().ToUpper() == UserName.Trim().ToUpper()
                                  && U.IsDeleted == false
                                  && C.IsDeleted == false
                                  //&& (C.ComplianceProductType == 1 || C.ComplianceProductType == 3)
                                  && ((C.ComplianceProductType != null && C.ComplianceProductType > 0) || U.CustomerID == 94)
                                  && (RUM.UserType == "T" || RUM.UserType == "R")
                                  select RUM).ToList();
                if (emailQuery.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }                  
        public static void Create(MaintainLoginDetail obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.MaintainLoginDetails.Add(obj);
                entities.SaveChanges();
            }
        }
        public static List<object> GetAllDepartmentHeadUser(int customerID = -1)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_User
                             where row.IsDeleted == false && row.IsActive == true
                             && row.IsHead == true
                             select new { row.ID, row.FirstName, row.LastName, row.Email, row.CustomerID });

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }
                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).ToList<object>();

                return users;
            }
        }
        public static List<object> GetAllUserAMorMGMTorDH(int customerID = -1)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_User
                             where row.IsDeleted == false && row.IsActive == true
                             && (row.IsAuditHeadOrMgr == "AM" || row.IsAuditHeadOrMgr == "AH" || row.RoleID == 8 || row.IsHead == true)
                             select new { row.ID, row.FirstName, row.LastName, row.Email, row.CustomerID });

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }
                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).ToList<object>();

                return users;
            }
        }        
        public static List<object> GetAllUserAM(int customerID = -1)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_User
                             where row.IsDeleted == false && row.IsActive == true
                             && (row.IsAuditHeadOrMgr == "AM" || row.IsAuditHeadOrMgr == "AH")
                             select new { row.ID, row.FirstName, row.LastName, row.Email, row.CustomerID });

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }
                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).ToList<object>();

                return users;
            }
        }
        public static List<object> GetAllUserMGMT(int customerID = -1)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_User
                             where row.IsDeleted == false && row.IsActive == true
                             && row.RoleID == 8
                             select new { row.ID, row.FirstName, row.LastName, row.Email, row.CustomerID });

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }
                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).ToList<object>();

                return users;
            }
        }       
        public static bool GetUserTypeInternalExternalByUserID(int userID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserDetails = (from row in entities.Users
                                   where row.CustomerID == customerID
                                   && row.ID == userID
                                   select row.IsExternal).FirstOrDefault();

                if (UserDetails != null)
                    return Convert.ToBoolean(UserDetails);
                else
                    return false;
            }
        }
        public static List<long> GetByProductIDList(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productmapping = (from row in entities.ProductMappings
                                      where row.CustomerID == customerID && row.IsActive == false
                                      select (long) row.ProductID).ToList();

                return productmapping;
            }
        }
        public static List<User> GetCompanyAdminDataByCustID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UsersList = (from row in entities.Users
                                 where row.CustomerID == customerID
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                   && row.RoleID == 2
                                 select row).ToList();

                return UsersList;
            }
        }
        public static int CreateNew(User user, List<UserParameterValue> parameters, string SenderEmailAddress, string message, bool isActive = true)
        {
            int Addid = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {               
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID)).Status;
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.UtcNow;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            user.IsActive = isActive;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.UtcNow;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        parameters.ForEach(entry => user.UserParameterValues.Add(entry));
                        entities.Users.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);                        
                        dbtransaction.Commit();
                        return Addid;
                    }
                    catch (Exception ex)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDUser(Addid);
                        }
                        return Addid = 0;
                    }
                }
            }
        }

        public static void deleteUser(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var tet = entities.Users.Where(x => x.ID == Userid).FirstOrDefault();
                    entities.Users.Remove(tet);
                    entities.SaveChanges();
                    Userid--;
                    entities.SP_ResetIDUser(Userid);
                }
                catch (Exception ex)
                {
                }
            }
        }

        public static void CreateSubEntity(NodeType objSubEntity)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.NodeTypes.Add(objSubEntity);
                entities.SaveChanges();
            }
        }
        public static void UpdateSubEntity(NodeType objSubEntity)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                NodeType objLegEntity = (from row in entities.NodeTypes
                                         where row.ID == objSubEntity.ID
                                             select row).FirstOrDefault();

                objLegEntity.Name = objSubEntity.Name;
                entities.SaveChanges();
            }
        }
        public static void UpdateUserPhoto(int ID, String FilePath, String FileName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User userToUpdate = (from Row in entities.Users
                                     where Row.ID == ID
                                     select Row).FirstOrDefault();

                userToUpdate.ImagePath = FilePath;
                userToUpdate.ImageName = FileName;
                entities.SaveChanges();
            }
        }
       
        public static List<UserView> GetAllUser(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.UserViews                             
                             select row);

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter));
                }

                return users.OrderBy(entry => entry.FirstName).ToList();
            }
        }

        public static List<User> GetAll(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Users
                             join cus in entities.Customers
                             on  row.CustomerID equals cus.ID
                             where cus.Status == 1 
                             select row);

                users = users.Where(entry => entry.IsDeleted != true);
                users = users.Where(entry => entry.IsActive != false);

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter)|| entry.ContactNumber.Contains(filter));
                }

                return users.OrderBy(entry=>entry.FirstName).ToList();
            }
        }
        
        public static object GetAllByCustomerID(int customerID, string roleCode)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.RoleID!=8
                             select row);

                if (!string.IsNullOrEmpty(roleCode))
                {
                    var roleID = (from row in entities.Roles
                                  where row.Code == roleCode
                                  select row.ID).FirstOrDefault();
                    if (roleID!=7)
                    query = query.Where(entry => entry.RoleID == roleID);
                }

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName}).OrderBy(entry=>entry.Name).ToList<object>();

                return users;
            }
        }

        public static object GetAllUserCustomerID(int customerID, string roleCode)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.RoleID !=19
                             select row);

                if (!string.IsNullOrEmpty(roleCode))
                {
                    var roleID = (from row in entities.Roles
                                  where row.Code == roleCode
                                  select row.ID).FirstOrDefault();
                    if (roleID != 7)
                        query = query.Where(entry => entry.RoleID == roleID);
                }

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static object GetAllUserBranchAssigned(long UserID, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var BranchList = (from row in entities.ComplianceAssignments
                             join row1 in entities.ComplianceInstances
                             on row.ComplianceInstanceID equals row1.ID
                             where row.UserID == UserID 
                             select (int?)row1.CustomerBranchID).Distinct().ToList();

                var BranchIList = (from row in entities.InternalComplianceAssignments
                                  join row1 in entities.InternalComplianceInstances
                                  on row.InternalComplianceInstanceID equals row1.ID
                                  where row.UserID == UserID
                                  select (int?)row1.CustomerBranchID).Distinct().ToList();

                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && (BranchList.Contains(row.CustomerBranchID) || BranchIList.Contains(row.CustomerBranchID))
                             && row.RoleID != 19
                             select row);

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }


        public static List<object> GetAllNVP(int customerID = -1, List<long> ids = null, bool Flags=false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.IsActive == true
                             && row.RoleID != 19
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }

                if (Flags == true)
                {
                    query = query.Where(entry => entry.RoleID != 8);
                }

                if (ids != null)
                {
                    query = query.Where(entry => ids.Contains(entry.ID));
                }

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();

                return users.ToList<object>();
            }
        }

        public static List<object> HRGetAllNVP(int customerID = -1, List<long> ids = null, bool Flags = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.IsActive == true
                             && row.RoleID != 19 && row.HRRoleID !=null
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }

                if (Flags == true)
                {
                    query = query.Where(entry => entry.RoleID != 8);
                }

                if (ids != null)
                {
                    query = query.Where(entry => ids.Contains(entry.ID));
                }

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();

                return users.ToList<object>();
            }
        }

        public static List<object> GetAllImplementaionUsers()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.IsActive == true && row.RoleID ==12
                             select row);

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();

                return users.ToList<object>();
            }
        }
        public static List<object> GetAllLitigationManagmentUser(int customerID = -1, int complianceProductType = 0)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.IsActive == true
                             && row.LitigationRoleID!=null
                             select new { row.ID, row.FirstName, row.LastName, row.Email, row.CustomerID });

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }

                //if (complianceProductType != 0)
                //{
                //    query = query.Where(entry => (
                //                                   entry.Code.Equals("MGMT") 
                //                                 ));
                //}
                //else
                //    query = query.Where(entry => (entry.Code.Equals("MGMT") ));

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).ToList<object>();

                return users;
            }
        }
        public static List<object> GetAllManagmentUser(int customerID = -1, int complianceProductType = 0)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             join rl in entities.Roles on row.RoleID equals rl.ID
                             where row.IsDeleted == false && row.IsActive == true
                             select new { row.ID, row.FirstName, row.LastName, row.Email, row.CustomerID, rl.Code });

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }

                if (complianceProductType != 0)
                {
                    query = query.Where(entry => (
                                                   entry.Code.Equals("MGMT") || entry.Code.Equals("AUDT") ||
                                                   entry.Code.Equals("HMGMT") || entry.Code.Equals("HMGR")
                                                 ));
                }
                else
                    query = query.Where(entry => (entry.Code.Equals("MGMT") || entry.Code.Equals("AUDT")));

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).ToList<object>();

                return users;
            }
        }

        public static bool IsValidUserIDRLCS(string Email, out RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                user = (from userRow in entities.Users
                        join row1 in entities.RLCS_User_Mapping
                        on userRow.ID equals row1.AVACOM_UserID
                        where userRow.IsDeleted == false
                        && row1.Email.ToUpper().Trim() == Email.ToUpper().Trim()
                        select row1).FirstOrDefault();

            }
            return user != null;
        }
        public static bool IsValidUserID(string username, out RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                user = (from userRow in entities.Users
                        join row1 in entities.RLCS_User_Mapping
                        on userRow.ID equals row1.AVACOM_UserID
                        where userRow.IsDeleted == false
                        && row1.UserID.ToUpper().Trim() == username.ToUpper().Trim()
                        select row1).FirstOrDefault();

            }
            return user != null;
        }
        public static bool IsValidUserID(string username, string password, out RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                user = null;
                string encpassword = string.Empty;
                var userdetails = (from userRow in entities.Users
                                   join RUM in entities.RLCS_User_Mapping
                                   on userRow.ID equals RUM.AVACOM_UserID
                                   where userRow.IsDeleted == false
                                   && (RUM.UserID.ToUpper().Trim() == username.ToUpper().Trim() || RUM.Email.ToUpper().Trim().Equals(username.ToUpper().Trim()))
                                   //&& RUM.Password == password
                                   && RUM.Status == "A"
                                   select RUM).ToList();

                if (userdetails != null)
                {
                    if (userdetails.Count > 0)
                    {


                        if (userdetails.FirstOrDefault().EnType == "M")
                        {
                            encpassword = Util.CalculateMD5Hash(password);
                        }
                        else if (userdetails.FirstOrDefault().EnType == "A")
                        {
                            encpassword = Util.CalculateAESHash(password);
                        }

                        user = (from RUM in userdetails
                                where (RUM.UserID.ToUpper().Trim() == username.ToUpper().Trim() || RUM.Email.ToUpper().Trim().Equals(username.ToUpper().Trim()))
                                && RUM.Password == encpassword
                                select RUM).FirstOrDefault();
                    }
                }
            }
            return user != null;
        }             
        public static bool IsValidRLCSUserID(string username, out User user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                user = (from userRow in entities.Users
                        join row1 in entities.RLCS_User_Mapping
                        on userRow.ID equals row1.AVACOM_UserID
                        where userRow.IsDeleted == false
                        && row1.UserID.ToUpper().Trim() == username.ToUpper().Trim()
                        select userRow).FirstOrDefault();
            }
            return user != null;
        }
        public static bool IsValidProfileID(string profileID, out RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                user = (from userRow in entities.Users
                        join row1 in entities.RLCS_User_Mapping
                        on userRow.ID equals row1.AVACOM_UserID
                        where userRow.IsDeleted == false
                        && row1.ProfileID.ToUpper().Trim() == profileID.ToUpper().Trim()
                        select row1).FirstOrDefault();

            }
            return user != null;
        }
        public static bool IsValidUser(string username, string password, out User user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                user = null;
                string encpassword = string.Empty;
                var userdetails = (from userRow in entities.Users
                                   where userRow.IsDeleted == false
                                         && userRow.Email.ToUpper().Trim() == username.ToUpper().Trim()
                                   select userRow).ToList();

                if (userdetails != null)
                {
                    if (userdetails.Count > 0)
                    {
                        if (userdetails.FirstOrDefault().EnType == "M")
                        {
                            encpassword = Util.CalculateMD5Hash(password);
                        }
                        else if (userdetails.FirstOrDefault().EnType == "A")
                        {
                            encpassword = Util.CalculateAESHash(password);
                        }

                        user = (from userRow in userdetails
                                where userRow.IsDeleted == false
                                      && userRow.Email.ToUpper().Trim() == username.ToUpper().Trim()
                                      && userRow.Password == encpassword
                                select userRow).SingleOrDefault();
                    }
                }
                //user = (from userRow in entities.Users
                //        where userRow.IsDeleted == false
                //              && userRow.Email.ToUpper().Trim() == username.ToUpper().Trim()
                //              && userRow.Password == encpassword
                //        select userRow).SingleOrDefault();
            }
            return user != null;
        }
        public static bool IsValidUser(string username, out User user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                user = (from userRow in entities.Users
                        where userRow.IsDeleted == false
                              && userRow.Email.ToUpper().Trim() == username.ToUpper().Trim()
                        select userRow).SingleOrDefault();
            }
            return user != null;
        }
        public static List<String> GetCompanyAdminByCustID(int? CustID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UsersList = (from row in entities.Users
                                 where row.CustomerID == CustID
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                   && row.RoleID == 2
                                 select row.Email).ToList();

                return UsersList;
            }
        }
        public static string GetUserName(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.ID == userID
                            select row).FirstOrDefault();

                return user.FirstName +' ' + user.LastName;
            }
        }

        public static User GetByID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.ID == userID
                            select row).SingleOrDefault();

                return user;
            }
        }
        public static RLCS_User_Mapping rlcsGetByID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var emailQuery = (from RMU in entities.RLCS_User_Mapping                                 
                                  where RMU.AVACOM_UserID == userID
                                  select RMU).FirstOrDefault();
                return emailQuery;

            }
        }
        public static User GetISAuditManagerID(int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.CustomerID == customerid && row.IsDeleted == false
                            && (row.IsAuditHeadOrMgr=="AM" || row.IsAuditHeadOrMgr == "AH")
                            select row).FirstOrDefault();

                return user;
            }
        }
    public static string showAssignedUser(string AssignedUser)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<string> Opposition_Lawyers = new List<string>();
                string combindedString = "";
                if (!string.IsNullOrEmpty(AssignedUser))
                {
                    AssignedUser = AssignedUser.TrimEnd(',');
                    string[] split = AssignedUser.Split(',');
                    if (split.Length > 0)
                    {
                        string opplawid = "";
                        for (int rs = 0; rs < split.Length; rs++)
                        {
                            opplawid = Convert.ToString(UserManagement.GetUserName(Convert.ToInt32(split[rs].ToString().Trim())));
                            if (opplawid == "0" || opplawid == "-1")
                            {
                            }
                            else
                            {
                                Opposition_Lawyers.Add(opplawid);
                            }
                            combindedString = string.Join(",", Opposition_Lawyers);
                        }
                    }
                }
                return combindedString;
            }
        }

        public static void Delete(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //User userToDelete = new User() { ID = userID };
                //entities.Users.Attach(userToDelete);
                User userToDelete = (from row in entities.Users
                                      where row.ID == userID
                                      select row).FirstOrDefault();

                userToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }
        public static void DeleteTempAssignmentTableInternal(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {   
                TempAssignmentTableInternal userToDelete = (from row in entities.TempAssignmentTableInternals
                                                            where row.UserID == userID
                                                            select row).FirstOrDefault();

                if (userToDelete != null)
                {
                    userToDelete.IsActive = false;
                    entities.SaveChanges();
                }
            }
        }
        public static void DeleteTempAssignmentTable(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTable userToDelete = (from row in entities.TempAssignmentTables
                                                            where row.UserID == userID
                                                    select row).FirstOrDefault();
                if (userToDelete != null)
                {
                    userToDelete.IsActive = false;
                    entities.SaveChanges();
                }
            }
        }
        public static void DeleteTempAssignmentTableCheckList(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTableCheckList userToDelete = (from row in entities.TempAssignmentTableCheckLists
                                                    where row.UserID == userID
                                                    select row).FirstOrDefault();
                if (userToDelete != null)
                {
                    userToDelete.IsActive = false;
                    entities.SaveChanges();
                }
            }
        }

        public static List<int> GetAssignedPerformerAndReviewer(int Userid, long CustomerID)
        {
            List<int> a = new List<int>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (CustomerID == 63)
                {
                    var parameters = (from row in entities.InternalComplianceAssignments
                                      where row.UserID == Userid
                                      select row.RoleID).Distinct().ToList();

                    if (parameters != null)
                    {
                        a = parameters.ToList();
                        return a;
                    }
                    else
                    {
                        return a;
                    }
                }
                else
                {
                    var parameters = (from row in entities.ComplianceAssignments
                                      where row.UserID == Userid
                                      select row.RoleID).Distinct().ToList();

                    if (parameters != null)
                    {
                        a = parameters.ToList();
                        return a;
                    }
                    else
                    {
                        return a;
                    }
                }
            }
        }
        
        public static List<UserParameter> GetAllUserParameters(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var parameters = (from row in entities.UserParameters
                                  where row.IsDeleted == false
                                  select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    parameters = parameters.Where(entry => entry.Name.Contains(filter));
                }

                return parameters.ToList();
            }
        }

        public static UserParameter GetUserParameterByID(int userParameterID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var userParameter = (from row in entities.UserParameters
                                     where row.ID == userParameterID
                                     select row).SingleOrDefault();

                return userParameter;
            }

        }

        public static void DeleteUserParameter(int userParameterID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //UserParameter userParameterToDelete = new UserParameter() { ID = userParameterID };
                //entities.UserParameters.Attach(userParameterToDelete);
                UserParameter userParameterToDelete = (from row in entities.UserParameters
                                                       where row.ID == userParameterID
                                                       select row).FirstOrDefault();

                userParameterToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }

        public static bool UserParameterExists(UserParameter userParameter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.UserParameters
                             where row.IsDeleted == false
                                  && row.Name.Equals(userParameter.Name)
                             select row);

                if (userParameter.ID > 0)
                {
                    query = query.Where(entry => entry.ID != userParameter.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateUserParameter(UserParameter userParameter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                userParameter.IsDeleted = false;
                userParameter.CreatedOn = DateTime.UtcNow;
                entities.UserParameters.Add(userParameter);
                entities.SaveChanges();
            }
        }

        public static void UpdateUserParameter(UserParameter userParameter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //UserParameter userParameterToUpdate = new UserParameter() { ID = userParameter.ID };
                //entities.UserParameters.Attach(userParameterToUpdate);

                UserParameter userParameterToUpdate = (from row in entities.UserParameters
                                                       where row.ID == userParameter.ID
                                                       select row).FirstOrDefault();

                userParameterToUpdate.Name = userParameter.Name;
                userParameterToUpdate.DataType = userParameter.DataType;
                userParameterToUpdate.Length = userParameter.Length;

                entities.SaveChanges();
            }
        }

        public static void Exists(User user,out bool emailExists)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var emailQuery = (from row in entities.Users
                                  where row.IsDeleted == false
                                       && row.Email.ToUpper().Trim().Equals(user.Email.ToUpper().Trim())
                                  select row);

                if (user.ID > 0)
                {
                    emailQuery = emailQuery.Where(entry => entry.ID != user.ID);
                }

                emailExists = emailQuery.Select(entry => true).FirstOrDefault();



                //var usernameQuery = (from row in entities.Users
                //                     where row.IsDeleted == false
                //                          && row.Username.Equals(user.Username)
                //                     select row);

                //if (user.ID > 0)
                //{
                //    usernameQuery = usernameQuery.Where(entry => entry.ID != user.ID);
                //}

                //usernameExists = usernameQuery.Select(entry => true).FirstOrDefault();

            }
        }
        public static bool Create(widget wid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                try
                {
                    entities.widgets.Add(wid);
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        public static bool Create(User user, List<UserParameterValue> parameters,string SenderEmailAddress, string message)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //entities.Connection.Open();
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID)).Status;
                       
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.UtcNow;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.UtcNow;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        parameters.ForEach(entry => user.UserParameterValues.Add(entry));
                        entities.Users.Add(user);
                        entities.SaveChanges();

                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "AVACOM account created.", message);
                      
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        //entities.Connection.Close();
                        return false;
                    }
                }
            }
        }

        public static bool Update(User user, List<UserParameterValue> parameters)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {               
                User userToUpdate = (from row in entities.Users
                                     where row.ID == user.ID
                                     select row).FirstOrDefault();


                userToUpdate.IsHead = user.IsHead;
                userToUpdate.FirstName = user.FirstName;
                userToUpdate.LastName = user.LastName;
                userToUpdate.Designation = user.Designation;
                userToUpdate.Email = user.Email;
                userToUpdate.ContactNumber = user.ContactNumber;
                userToUpdate.Address = user.Address;
                //userToUpdate.CustomerID = user.CustomerID;
                userToUpdate.CustomerBranchID = user.CustomerBranchID;
                userToUpdate.ReportingToID = user.ReportingToID;
                userToUpdate.RoleID = user.RoleID;
                userToUpdate.DepartmentID = user.DepartmentID;
                userToUpdate.IsAuditHeadOrMgr = user.IsAuditHeadOrMgr;
                userToUpdate.AuditorID = user.AuditorID;
                userToUpdate.AuditEndPeriod = user.AuditEndPeriod;
                userToUpdate.AuditStartPeriod = user.AuditStartPeriod;
                userToUpdate.Startdate = user.Startdate;
                userToUpdate.Enddate = user.Enddate;
                userToUpdate.VendorRoleID = user.VendorRoleID;
                userToUpdate.IMEIChecked = user.IMEIChecked;
                userToUpdate.IMEINumber = user.IMEINumber;
                userToUpdate.DesktopRestrict = user.DesktopRestrict;
                userToUpdate.MobileAccess = user.MobileAccess;
                userToUpdate.VendorRoleID = user.VendorRoleID;
                userToUpdate.HRRoleID = user.HRRoleID;
                userToUpdate.SecretarialRoleID = user.SecretarialRoleID;
                userToUpdate.Cer_OwnerRoleID = user.Cer_OwnerRoleID;
                userToUpdate.Cer_OfficerRoleID = user.Cer_OfficerRoleID;
                userToUpdate.IsCertificateVisible = user.IsCertificateVisible;
                userToUpdate.SSOAccess = user.SSOAccess;

                List<int> parameterIDs = parameters.Select(entry => entry.ID).ToList();

                var existingParameters = (from parameterRow in entities.UserParameterValues
                                          where parameterRow.UserId == user.ID
                                          select parameterRow).ToList();

                existingParameters.ForEach(entry =>
                {
                    if (parameterIDs.Contains(entry.ID))
                    {
                        UserParameterValue parameter = parameters.Find(param1 => param1.ID == entry.ID);
                        entry.Value = parameter.Value;
                    }
                    else
                    {
                        entities.UserParameterValues.Remove(entry);
                    }
                });

                parameters.Where(entry => entry.ID == -1).ToList().ForEach(entry => userToUpdate.UserParameterValues.Add(entry));

                entities.SaveChanges();
                return true;
            }
        }

        public static void Update(User user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
              
                User userToUpdate = (from Row in entities.Users
                                     where Row.ID == user.ID
                                     select Row).FirstOrDefault();
              
                userToUpdate.LastLoginTime = user.LastLoginTime;
                userToUpdate.WrongAttempt = 0;
                entities.SaveChanges();
            }
        }

        public static void UpdateUserStatus(int CustomerId, bool status)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<User> userToUpdate = (from Row in entities.Users
                                           where Row.CustomerID == CustomerId
                                           select Row).ToList();
                userToUpdate.ForEach(entry =>
                {
                    entry.IsActive = status;
                });
                entities.SaveChanges();
            }
        }
        
        public static void ToggleStatus(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool isActive = !(from row in entities.Users
                                  where row.ID == userID
                                  select row.IsActive).SingleOrDefault();

                //User userToUpdate = new User() { ID = userID };
                //entities.Users.Attach(userToUpdate);

                User userToUpdate = (from row in entities.Users
                                     where row.ID == userID
                                     select row).FirstOrDefault();

                userToUpdate.IsActive = isActive;

                if (isActive)
                {
                    userToUpdate.DeactivatedOn = null;
                }
                else
                {
                    userToUpdate.DeactivatedOn = DateTime.UtcNow;
                }

                entities.SaveChanges();
            }
        }
     
        public static List<UserParameterValueInfo> GetParameterValuesByUserID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var result = (
                                from parameterRow in entities.UserParameters
                                join parameterValueRow in entities.UserParameterValues.Where(entry => entry.UserId == userID)
                                  on parameterRow.ID equals parameterValueRow.UserParameterId into parameterValues

                                from valueRow in parameterValues.DefaultIfEmpty()

                                where parameterRow.IsDeleted == false

                                select new { ParameterRow = parameterRow, ValueRow = valueRow }

                              ).ToList().Select(entry => new UserParameterValueInfo()
                              {
                                  UserID = entry.ValueRow == null ? -1 : (long)entry.ValueRow.UserId,
                                  ParameterID = entry.ParameterRow.ID,
                                  ValueID = entry.ValueRow == null ? -1 : entry.ValueRow.ID,
                                  Name = entry.ParameterRow.Name,
                                  DataType = (DataType)entry.ParameterRow.DataType,
                                  Length = (int)entry.ParameterRow.Length,
                                  Value = entry.ValueRow == null ? string.Empty : entry.ValueRow.Value
                              }).ToList();


                return result;
            }
        }

        public static bool ChangePassword(User user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {                       
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);                       
                        User userToUpdate = new User();

                        userToUpdate = (from entry in entities.Users
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();


                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = DateTime.UtcNow;                        
                        userToUpdate.EnType = "A";
                        userToUpdate.ChangePasswordFlag = true;                            
                        entities.SaveChanges();                       
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();                        
                        return false;
                    }
                }
            }
        }

        public static bool HasCompliancesAssigned(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var assignedCount = (from row in entities.ComplianceInstanceViews
                                     where row.UserID == userID
                                     select 1).Count();

                return assignedCount > 0;
            }
        }

        public static bool IsActive(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var isActive = (from row in entities.Users
                                where row.ID == userID
                                select row.IsActive).FirstOrDefault();

                return isActive;
            }
        }

        
        public static void DeleteUserReminder(int userReminderID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DeleteReminder = (from RM in entities.ComplianceReminders
                                      where RM.ID == userReminderID
                                      select RM).SingleOrDefault();

                entities.ComplianceReminders.Remove(DeleteReminder);
                entities.SaveChanges();
            }
        }
        public static void DeleteUserReminderInternal(int userReminderID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DeleteReminder = (from RM in entities.InternalComplianceReminders
                                      where RM.ID == userReminderID
                                      select RM).SingleOrDefault();

                entities.InternalComplianceReminders.Remove(DeleteReminder);
                entities.SaveChanges();
            }
        }
        public static User GetByUsername(string username)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.Email.ToUpper().Trim() == username.ToUpper().Trim() && row.IsDeleted == false
                            select row).SingleOrDefault();

                return user;
            }
        }
        
        // For Security Questions
        public static bool HasUserSecurityQuestion(long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var isActive = (from row in entities.SecurityQuestionAnswars
                                where row.UserID == userID
                                select true).FirstOrDefault();

                return isActive;
            }
        }

        public static List<SecurityQuestion> GetSecurityQuestions()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SecurityQuestions
                                select row).ToList();

                return data;
            }
        }

        public static bool Create(List<SecurityQuestionAnswar> securityQuestionAns)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                securityQuestionAns.ForEach(entry=>
                    entities.SecurityQuestionAnswars.Add(entry)
                );
                entities.SaveChanges();

                return true;
            }
        }

        public static List<SecurityQuestion> GetRandomQuestions(long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> ids = (from row in entities.SecurityQuestionAnswars
                                 where row.UserID == userID
                                 select (int)row.SQID).ToList();

                                Random rndNumber = new Random();
                                IEnumerable<int> randomids = ids.OrderBy(x => rndNumber.Next()).Take(2);


                List<SecurityQuestion> randomQuestions = (from row in entities.SecurityQuestions
                                                          where randomids.Contains(row.ID)
                                                          select row).ToList();


                return randomQuestions;
            }
        }

        public static int ValidateQuestions(List<SecurityQuestionAnswar> securityQuestionAns)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long userID = Convert.ToInt64(securityQuestionAns[0].UserID);
                List<SecurityQuestionAnswar> data = (from row in entities.SecurityQuestionAnswars
                                                     where row.UserID == userID
                                                     select row).ToList();

                if (securityQuestionAns[0] != null)
                {
                    var AnserceData = data.Where(entry => entry.SQID == securityQuestionAns[0].SQID && entry.UserID == securityQuestionAns[0].UserID).FirstOrDefault();
                    if (!AnserceData.Answar.Equals(securityQuestionAns[0].Answar))
                        return 1;

                } 

                if (securityQuestionAns[1] != null)
                {
                    var AnserceData = data.Where(entry => entry.SQID == securityQuestionAns[1].SQID && entry.UserID == securityQuestionAns[1].UserID).FirstOrDefault();
                    if (!AnserceData.Answar.Equals(securityQuestionAns[1].Answar))
                        return 2;
                }               
                return 0;
            }
        }

        public static bool Delete(long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<SecurityQuestionAnswar> securityQuestionAns = (from row in entities.SecurityQuestionAnswars
                                                                    where row.UserID == userID
                                                                    select row).ToList();

                securityQuestionAns.ForEach(entry =>
                    entities.SecurityQuestionAnswars.Remove(entry)
                );
                entities.SaveChanges();

                return true;
            }
        }

        public static void WrongUpdate(string emailID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                User userToUpdate = (from Row in entities.Users
                                     where Row.Email.ToUpper().Trim() == emailID.ToUpper().Trim() && Row.IsDeleted == false && Row.IsActive ==true
                                     select Row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.WrongAttempt += 1;

                    entities.SaveChanges();
                }
            }
        }

        public static int WrongAttemptCount(string emailID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int count  = (from Row in entities.Users
                             where Row.Email.ToUpper().Trim() == emailID.ToUpper().Trim() && Row.IsDeleted == false && Row.IsActive == true
                             select Row.WrongAttempt).FirstOrDefault();

                return count;
            }
        }

        public static void WrongAttemptCountUpdate(long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                User userToUpdate = (from Row in entities.Users
                                     where Row.ID == userID && Row.IsDeleted == false && Row.IsActive == true
                                     select Row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.WrongAttempt =0;

                    entities.SaveChanges();
                }
            }
        }
    
        public static void DeleteAuditorTeamUser(int AuditerUserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                User AuditorUserMastertoDelete = (from row in entities.Users
                                                  where row.ID == AuditerUserID
                                                  select row).FirstOrDefault();

                AuditorUserMastertoDelete.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        
        public static List<User> GetUserDetailsByText(String TextToSearch, int CustID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserDetails = (from row in entities.Users
                                   where row.CustomerID == CustID
                                   && (row.FirstName.ToLower().Contains(TextToSearch) || row.LastName.ToLower().Contains(TextToSearch))
                                   select row).ToList();

                return UserDetails;
            }
        }        
    }
}
