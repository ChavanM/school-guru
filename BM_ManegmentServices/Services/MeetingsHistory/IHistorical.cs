﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.MeetingsHistory
{
    public interface IHistorical
    {
        List<VM_HistoricalMeetings> GetHistoricalData(int customerID, int userID, string roleCode);
        VM_HistoricalMeetings CreateHistoricalData(VM_HistoricalMeetings _objHistoricalMeetings, int customerId, int userId);
        VM_HistoricalMeetings GetHistoricalDataByID(int iD);
        List<BM_SP_GetHistoricalFileData_Result> GetHistoricalFileData(int customerId, int HistoricalID,string status);
        VM_HistoricalMeetings UpdateHistoricalData(VM_HistoricalMeetings _objHistoricalMeetings, int customerId, int userId);
        string Viewdocument(long id);
        bool Downloadrecords(long id);
        List<FYearVM> BindFY();
        bool DeleteHisData(int iD,int UserId);
        bool DeleteHisFile(int iD, int userId);
    }
}
