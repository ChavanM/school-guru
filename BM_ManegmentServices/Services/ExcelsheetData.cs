﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;

namespace BM_ManegmentServices.Services
{
    public abstract class  ExcelsheetData
    {
        public abstract bool checkExcelData(ExcelPackage PrivatePublicexcel, string data);
    }
}