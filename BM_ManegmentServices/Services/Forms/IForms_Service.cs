﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Forms
{
    public interface IForms_Service
    {
        List<FormsVM> GetMeetingFormList(long meetingId, long meetingAgendaMappingId, long complianceId);
        string GenerateMeetingFormData(long meetingAgendaMappingId, long formMappingId);

        #region Generate E-Forms
        EFormVM GenerateForms(long meetingFormMappingId, long meetingId);

        EFormVM GenerateFormsforDirector(long DirectorID);
        EFormVM GenerateFormforDirectorDIR3(long directorId);
        #endregion
    }
}
