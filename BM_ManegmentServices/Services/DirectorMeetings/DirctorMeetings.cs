﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Dashboard;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.DirectorMeetings
{
    public class DirctorMeetings : IDirectorMeetings
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public List<MeetingListVM> GetMeetingsForUpcommingMeeting(int UserID, int CustomerId, string Role)
        {
            var result = new List<MeetingListVM>();
            try
            {
                if (Role == SecretarialConst.Roles.DRCTR || Role == SecretarialConst.Roles.SMNGT)
                {
                    result = (from row in entities.BM_SP_MyMeetingsUpcoming(CustomerId, UserID, Role)
                              where row.IsVirtualMeeting == false
                              select new MeetingListVM
                              {
                                  EntityId = row.EntityId,
                                  EntityName = row.EntityName,

                                  MeetingID = row.MeetingId,
                                  MeetingTypeName = row.MeetingTypeName,
                                  MeetingSrNo = row.MeetingSrNo,
                                  IsAdjourned = row.IsAdjourned,
                                  MeetingTitle = row.MeetingTitle,
                                  IsShorter = row.IsShorter,

                                  MeetingDate = row.MeetingDate,
                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  MeetingstartDate = row.MeetingStartDate,

                                  Day = row.Day_,
                                  DayName = row.DayName_,
                                  Month = row.Month_,
                                  YearName = row.Year_,

                                  FY = row.FYText,
                                  CanStartMeeting = row.CanStart,
                                  IsConcluded = row.IsCompleted,
                                  participantId = (long)row.ParticipantId,
                                  RSPV = row.RSPV,
                                  ReasonforRSPV = row.ReasonforRSPV
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        #region Upcomming Meeting Created by Ruchi on 8th of May 2020

        //public List<MeetingListVM> GetMeetingsForUpcommingMeeting(int UserID, int CustomerId, string Role)
        //{
        //    var result = new List<MeetingListVM>();
        //    try
        //    {
        //        IEnumerable<MeetingVM> meetings = null;

        //        if (Role == SecretarialConst.Roles.DRCTR)
        //        {
        //            var today = DateTime.Today;
        //            meetings = (from row in entities.BM_Meetings
        //                          join participant in entities.BM_MeetingParticipant on row.MeetingID equals participant.Meeting_ID
        //                          join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
        //                          join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
        //                          where row.IsDeleted == false && row.IsNoticeSent == true && row.IsCompleted == false &&
        //                          participant.UserId == UserID && participant.IsDeleted == false && participant.IsInvited == null
        //                          && row.MeetingCircular == "M" && row.MeetingDate > today
        //                        select new MeetingVM
        //                          {
        //                              MeetingID = row.MeetingID,
        //                              Type = row.MeetingCircular.Trim(),
        //                              MeetingSrNo = row.MeetingSrNo,
        //                              MeetingTypeId = row.MeetingTypeId,
        //                              MeetingTypeName = meeting.MeetingTypeName,
        //                              Quarter_ = row.Quarter_,
        //                              MeetingTitle = row.MeetingTitle,
        //                              MeetingDate = row.MeetingDate,
        //                              MeetingTime = row.MeetingTime,
        //                              MeetingVenue = row.MeetingVenue,
        //                              IsSeekAvailability = row.IsSeekAvailability,
        //                              Availability_ShortDesc = row.Availability_ShortDesc,
        //                              TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
        //                              Entityt_Id = row.EntityId,
        //                              EntityName = entity.CompanyName,
        //                              IsShorter = row.IsShorter,
        //                              ParticipantId = participant.MeetingParticipantId,
        //                              StartMeetingDate = row.StartMeetingDate,
        //                              FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
        //                          }).ToList();

        //            if (meetings != null)
        //            {
        //                result = (from r in meetings
        //                          orderby r.MeetingDate
        //                          select new MeetingListVM
        //                          {
        //                              MeetingID = r.MeetingID,
        //                              MeetingTypeName = r.MeetingTypeName,
        //                              MeetingSrNo = r.MeetingSrNo,
        //                              EntityName = r.EntityName,
        //                              IsShorter = r.IsShorter,
        //                              MeetingDate = r.MeetingDate,
        //                              Day = r.MeetingDate.HasValue ? r.MeetingDate.Value.Day.ToString() : "",
        //                              DayName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("dddd") : "",
        //                              Month = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("MMM") : "",
        //                              MeetingTime = r.MeetingTime,
        //                              MeetingVenue = r.MeetingVenue,
        //                              Stage = r.Stage,
        //                              EntityId = r.Entityt_Id,
        //                              MeetingstartDate = r.StartMeetingDate,
        //                              participantId = r.ParticipantId,
        //                              RSPV = (from att in entities.BM_MeetingAttendance where att.MeetingId == r.MeetingID && att.MeetingParticipantId == r.ParticipantId select att.RSVP).FirstOrDefault(),
        //                              ReasonforRSPV = (from att in entities.BM_MeetingAttendance where att.MeetingId == r.MeetingID && att.MeetingParticipantId == r.ParticipantId select att.RSVP_Reason).FirstOrDefault(),
        //                          }).ToList();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //    return result;
        //}

        public IEnumerable<MeetingVM> GetMeetingsForParticipant(int UserID)
        {
            try
            {
                var today = DateTime.Today;
                var result = (from row in entities.BM_Meetings
                              join participant in entities.BM_MeetingParticipant on row.MeetingID equals participant.Meeting_ID
                              join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                              join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                              //from minutes in entities.BM_MeetingMinutesDetailsTransaction.Where( k=> k.MeetingParticipantId == participant.MeetingParticipantId).DefaultIfEmpty()
                              where //row.Customer_Id == CustomerId && 
                              row.IsDeleted == false && row.IsNoticeSent == true &&
                              participant.UserId == UserID && participant.IsDeleted == false && participant.IsInvited == null
                              && row.MeetingCircular == "M"
                              orderby row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn descending
                              select new MeetingVM
                              {
                                  MeetingID = row.MeetingID,
                                  Type = row.MeetingCircular.Trim(),
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = meeting.MeetingTypeName,
                                  Quarter_ = row.Quarter_,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  IsSeekAvailability = row.IsSeekAvailability,
                                  Availability_ShortDesc = row.Availability_ShortDesc,
                                  TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                  Entityt_Id = row.EntityId,
                                  EntityName = entity.CompanyName,
                                  IsShorter = row.IsShorter,
                                  ParticipantId = participant.MeetingParticipantId,
                                  StartMeetingDate = row.StartMeetingDate,
                                  IsMeetingStarted = row.IsMeetingStarted,
                                  FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
                                  Stage = row.MeetingDate == null ? "" : row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past"
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public MeetingListVM saveupdateupcomingMeetingdtlsbydir(MeetingListVM _objmeetingvm, int UserId)
        {
            try
            {
                var IsExsist = (from row in entities.BM_MeetingAttendance where row.MeetingId == _objmeetingvm.MeetingID && row.MeetingParticipantId == _objmeetingvm.participantId && row.IsActive == true select row).FirstOrDefault();
                if (IsExsist != null)
                {
                    IsExsist.RSVP = _objmeetingvm.RSPV;
                    IsExsist.RSVP_Reason = _objmeetingvm.ReasonforRSPV;
                    IsExsist.Updatedby = UserId;
                    IsExsist.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
                else
                {
                    BM_MeetingAttendance objmeetingattendence = new BM_MeetingAttendance();
                    objmeetingattendence.MeetingId = _objmeetingvm.MeetingID;
                    objmeetingattendence.MeetingParticipantId = _objmeetingvm.participantId;
                    objmeetingattendence.RSVP = _objmeetingvm.RSPV;
                    objmeetingattendence.CreatedOn = DateTime.Now;
                    objmeetingattendence.Createdby = UserId;
                    objmeetingattendence.Attendance = "P";
                    objmeetingattendence.RSVP_Reason = _objmeetingvm.ReasonforRSPV;
                    objmeetingattendence.IsActive = true;
                    entities.BM_MeetingAttendance.Add(objmeetingattendence);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objmeetingvm;
        }

        public List<CommitteeCompVM> GetMeeting(int CustomerId)
        {
            try
            {

                var result = new List<CommitteeCompVM>();

                result = (from row in entities.BM_CommitteeComp
                          where row.IsDeleted == false
                          &&( row.Customer_Id== CustomerId || row.Customer_Id==null)
                          orderby row.SerialNo
                          select new CommitteeCompVM
                          {
                              Id = row.Id,
                              //Name = row.Name,
                              MeetingTypeName = row.MeetingTypeName
                          }).ToList();


                return result;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        #region Circular created by Ruchi on 22May2020
        public List<MeetingListVM> GetCircularResolution(int UserID, int CustomerId, string Role)
        {
            try
            {
                var result = new Dashboard_MeetingVM();
                IEnumerable<MeetingVM> meetings = null;

                if (Role == SecretarialConst.Roles.DRCTR || Role == SecretarialConst.Roles.SMNGT)
                {
                    meetings = GetCircularForParticipant(UserID);
                }

                if (meetings != null)
                {
                    result.Upcoming = (from r in meetings
                                       where
                                       //r.Stage == SecretarialConst.MeetingStages.UPCOMING
                                       (r.MeetingDate != null && r.MeetingDate >= DateTime.Now.Date)
                                       orderby r.MeetingDate
                                       select new MeetingListVM
                                       {
                                           MeetingID = r.MeetingID,
                                           MeetingTypeName = r.MeetingTypeName,
                                           MeetingSrNo = r.MeetingSrNo,
                                           EntityName = r.EntityName,
                                           IsShorter = r.IsShorter,
                                           MeetingDate = r.MeetingDate,
                                           Day = r.MeetingDate.HasValue ? r.MeetingDate.Value.Day.ToString() : "",
                                           DayName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("ddd") : "",
                                           Month = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("MMM") : "",
                                           YearName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("yyyy") : "",
                                           MeetingTime = r.MeetingTime,
                                           //MeetingVenue = r.MeetingVenue,
                                           MeetingTitle = r.MeetingTitle,
                                           FY = r.FY_CY,
                                           srno = r.MeetingSrNo,
                                           Stage = r.Stage,
                                           EntityId = r.Entityt_Id,
                                           participantId = r.ParticipantId,
                                           ParticipantType = r.ParticipantType,
                                           RSPV = (from att in entities.BM_MeetingAttendance where att.MeetingId == r.MeetingID && att.MeetingParticipantId == r.ParticipantId select att.RSVP).FirstOrDefault(),
                                           ReasonforRSPV = (from att in entities.BM_MeetingAttendance where att.MeetingId == r.MeetingID && att.MeetingParticipantId == r.ParticipantId select att.RSVP_Reason).FirstOrDefault(),
                                       }).ToList();
                    if (result.Upcoming.Count > 0)
                    {
                        foreach (var item in result.Upcoming)
                        {
                            if (item.srno == 1)
                            {
                                item.sirialnumber = 1 + "st";
                            }
                            else if (item.srno == 2)
                            {
                                item.sirialnumber = 2 + "nd";
                            }
                            else if (item.srno == 3)
                            {
                                item.sirialnumber = 2 + "rd";
                            }
                            else if (item.srno > 3)
                            {
                                item.sirialnumber = item.srno + "th";
                            }
                        }
                    }
                }

                return result.Upcoming;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<MeetingVM> GetCircularForParticipant(int UserID)
        {
            try
            {
                var today = DateTime.Today;
                var result = (from row in entities.BM_Meetings
                              join participant in entities.BM_MeetingParticipant on row.MeetingID equals participant.Meeting_ID
                              join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                              join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                              where //row.Customer_Id == CustomerId && 
                              row.IsDeleted == false 
                              && row.IsNoticeSent == true 
                              && participant.UserId == UserID 
                              && participant.IsDeleted == false 
                              && participant.IsInvited == null 
                              && row.MeetingCircular == "C"
                              && row.IsVirtualMeeting == false
                              && row.IsCompleted == false
                              orderby row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn descending
                              select new MeetingVM
                              {
                                  MeetingID = row.MeetingID,
                                  Type = row.MeetingCircular.Trim(),
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = meeting.Name,
                                  Quarter_ = row.Quarter_,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  IsSeekAvailability = row.IsSeekAvailability,
                                  Availability_ShortDesc = row.Availability_ShortDesc,
                                  TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                  Entityt_Id = row.EntityId,
                                  EntityName = entity.CompanyName,
                                  IsShorter = row.IsShorter,
                                  ParticipantId = participant.MeetingParticipantId,
                                  ParticipantType = participant.ParticipantType,
                                  FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
                                  Stage = row.MeetingDate == null ? "" : row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past"
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<MeetingListVM> GetPastCircularResolution(int UserID, int CustomerId, string Role)
        {
            try
            {
                var result = new Dashboard_MeetingVM();
                IEnumerable<MeetingVM> meetings = null;

                if (Role == SecretarialConst.Roles.DRCTR || Role == SecretarialConst.Roles.SMNGT)
                {
                    meetings = GetPastCircularForParticipant(UserID);
                }

                if (meetings != null)
                {
                    result.Upcoming = (from r in meetings
                                           //where r.Stage == SecretarialConst.MeetingStages.COMPLETED                                       
                                       //orderby r.MeetingDate
                                       orderby r.EndMeetingDate descending
                                       select new MeetingListVM
                                       {
                                           MeetingID = r.MeetingID,
                                           MeetingTypeName = r.MeetingTypeName,
                                           MeetingSrNo = r.MeetingSrNo,
                                           EntityName = r.EntityName,
                                           IsShorter = r.IsShorter,
                                           MeetingDate = r.MeetingDate,
                                           
                                           Day = r.EndMeetingDate.HasValue ? r.EndMeetingDate.Value.Day.ToString() : "",
                                           DayName = r.EndMeetingDate.HasValue ? r.EndMeetingDate.Value.ToString("ddd") : "",
                                           Month = r.EndMeetingDate.HasValue ? r.EndMeetingDate.Value.ToString("MMM") : "",
                                           YearName = r.EndMeetingDate.HasValue ? r.EndMeetingDate.Value.ToString("yyyy") : "",

                                           //Day = r.MeetingDate.HasValue ? r.MeetingDate.Value.Day.ToString() : "",
                                           //DayName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("ddd") : "",
                                           //Month = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("MMM") : "",
                                           //YearName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("yyyy") : "",

                                           MeetingTime = r.MeetingTime,
                                           //MeetingVenue = r.MeetingVenue,
                                           MeetingTitle = r.MeetingTitle,

                                           Stage = r.Stage,
                                           EntityId = r.Entityt_Id,
                                           participantId = r.ParticipantId,
                                           MappingID = (from a in entities.BM_Meetings_Agenda_Responses where a.Meeting_ID == r.MeetingID && a.MeetingParticipantID == r.ParticipantId select a.MeetingAgendaMappingId).FirstOrDefault(),
                                           AgendaID = (from a in entities.BM_Meetings_Agenda_Responses where a.Meeting_ID == r.MeetingID && a.MeetingParticipantID == r.ParticipantId select a.AgendaId).FirstOrDefault(),
                                           RSPV = (from att in entities.BM_MeetingAttendance where att.MeetingId == r.MeetingID && att.MeetingParticipantId == r.ParticipantId select att.RSVP).FirstOrDefault(),
                                           ReasonforRSPV = (from att in entities.BM_MeetingAttendance where att.MeetingId == r.MeetingID && att.MeetingParticipantId == r.ParticipantId select att.RSVP_Reason).FirstOrDefault(),
                                       }).ToList();
                }

                return result.Upcoming;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<MeetingVM> GetPastCircularForParticipant(int UserID)
        {
            try
            {
                var today = DateTime.Today;
                var result = (from row in entities.BM_Meetings
                              join participant in entities.BM_MeetingParticipant on row.MeetingID equals participant.Meeting_ID
                              join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                              join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                              where //row.Customer_Id == CustomerId && 
                              row.IsDeleted == false
                              && row.IsNoticeSent == true
                              && row.IsCompleted == true
                              && participant.UserId == UserID
                              && participant.IsDeleted == false
                              && participant.IsInvited == null
                              && row.MeetingCircular == "C"
                              orderby row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn descending
                              select new MeetingVM
                              {
                                  MeetingID = row.MeetingID,
                                  Type = row.MeetingCircular.Trim(),
                                  TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = meeting.MeetingTypeName,
                                  Quarter_ = row.Quarter_,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  MeetingTime = row.MeetingTime,
                                  EndMeetingDate = row.EndMeetingDate,
                                  MeetingVenue = row.MeetingVenue,
                                  IsSeekAvailability = row.IsSeekAvailability,
                                  Availability_ShortDesc = row.Availability_ShortDesc,
                                 
                                  Entityt_Id = row.EntityId,
                                  EntityName = entity.CompanyName,
                                  IsShorter = row.IsShorter,
                                  ParticipantId = participant.MeetingParticipantId,

                                  FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
                                  Stage = row.MeetingDate == null ? "" : row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past",
                                  
                                  //Stage = row.CircularDate == null ? "" : row.CircularDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past"
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion Circular created by Ruchi on 22May2020

        public MeetingAgendaSummary AddCircularVotting(MeetingAgendaSummary vottings)
        {
            try
            {
                var checkvotingexist = (from row in entities.BM_Meetings_Agenda_Responses where row.Meeting_ID == vottings.MeetingId && row.MeetingParticipantID == vottings.ParticipantId && row.MeetingAgendaMappingId == vottings.MeetingAgendaMappingId select row).FirstOrDefault();
                if (checkvotingexist != null)
                {
                    checkvotingexist.Meeting_ID = vottings.MeetingId;
                    checkvotingexist.MeetingParticipantID = vottings.ParticipantId;
                    checkvotingexist.AgendaId = (long)vottings.AgendaId;
                    checkvotingexist.Response = vottings.AgendaResponse;
                    checkvotingexist.MeetingAgendaMappingId = (long)vottings.MeetingAgendaMappingId;
                    checkvotingexist.CircularResponseTime = DateTime.Now.TimeOfDay.ToString();
                    checkvotingexist.IsDeleted = false;
                    checkvotingexist.CreatedBy = vottings.userID;
                    checkvotingexist.CreatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
                else
                {
                    BM_Meetings_Agenda_Responses _objvotting = new BM_Meetings_Agenda_Responses();
                    _objvotting.Meeting_ID = vottings.MeetingId;
                    _objvotting.MeetingParticipantID = vottings.ParticipantId;
                    _objvotting.AgendaId = (long)vottings.AgendaId;
                    _objvotting.Response = vottings.AgendaResponse;
                    _objvotting.MeetingAgendaMappingId = (long)vottings.MeetingAgendaMappingId;
                    _objvotting.CircularResponseTime = DateTime.Now.TimeOfDay.ToString();
                    _objvotting.IsDeleted = false;
                    _objvotting.CreatedBy = vottings.userID;
                    _objvotting.CreatedOn = DateTime.Now;
                    entities.BM_Meetings_Agenda_Responses.Add(_objvotting);
                    entities.SaveChanges();
                }
                GetAllAgendaDetails(vottings.MeetingId);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return vottings;
        }

        public void GetAllAgendaDetails(long parentID)
        {
            try
            {

                BM_Meetings_Agenda_Responses _objcircular = new BM_Meetings_Agenda_Responses();
                CircularAgendaData obj = new CircularAgendaData();

                obj.CircularAgendaResultVM = new CircularAgendaResult();
                var getdata = (from rows in entities.BM_MeetingAgendaMapping

                               where rows.MeetingID == parentID
                               select new CircularAgendaData
                               {
                                   MeetingAgendaMappingId = rows.MeetingAgendaMappingID,
                                   AgendaName = rows.AgendaItemText,
                                   AgendaId = rows.AgendaID,
                                   MeetingId = parentID
                               }).ToList();
                if (getdata.Count > 0)
                {
                    foreach (var item in getdata)
                    {
                        var getCirculardata = (from row in entities.BM_Meetings_Agenda_Responses
                                               join MP in entities.BM_MeetingParticipant on row.MeetingParticipantID equals MP.MeetingParticipantId
                                               where row.MeetingAgendaMappingId == item.MeetingAgendaMappingId && row.Meeting_ID == item.MeetingId && row.AgendaId == item.AgendaId
                                               select row).FirstOrDefault();
                        var counttotalresponses = (from per in entities.BM_Meetings_Agenda_Responses where per.Meeting_ID == parentID && per.AgendaId == item.AgendaId && per.MeetingAgendaMappingId == item.MeetingAgendaMappingId && per.MeetingParticipantID == getCirculardata.MeetingParticipantID select per).Count();
                        item.CircularAgendaResultVM = new CircularAgendaResult();
                        var getresultlist = (from row in entities.BM_SP_CircularAgendaCountResponse(item.MeetingId, item.AgendaId, item.MeetingAgendaMappingId) select row).FirstOrDefault();
                        if (counttotalresponses == getresultlist.Participant)
                        {

                            if (getresultlist != null)
                            {
                                int totalApproved = (int)Math.Round((double)(100 * getresultlist.Approve) / getresultlist.Participant);
                                int totaldisaproved = (int)Math.Round((double)(100 * getresultlist.Disapprove) / getresultlist.Participant);
                                int totalatmeet = (int)Math.Round((double)(100 * getresultlist.Atmeet) / getresultlist.Participant);
                                int totalAbstain = (int)Math.Round((double)(100 * getresultlist.Abstain) / getresultlist.Participant);
                                if (totalApproved > 50)
                                {
                                    item.CircularAgendaResultVM.Result = "A";
                                    item.CircularAgendaResultVM.Remark = "Passed";
                                }
                                if (totalApproved == 100)
                                {
                                    item.CircularAgendaResultVM.Result = "A";
                                    item.CircularAgendaResultVM.Remark = "Unanimously";
                                }
                                else if (totalAbstain > 50)
                                {
                                    item.CircularAgendaResultVM.Result = "D";
                                    item.CircularAgendaResultVM.Remark = "Not Passed";
                                }
                                else if (totaldisaproved > 50)
                                {
                                    item.CircularAgendaResultVM.Result = "D";
                                    item.CircularAgendaResultVM.Remark = "Not Passed";
                                }

                                else if (totalatmeet > 50)
                                {
                                    item.CircularAgendaResultVM.Result = "Atmeet";
                                    item.CircularAgendaResultVM.Remark = "Discuss in meeting";
                                }
                                else if (totalApproved == 0 && totaldisaproved == 0 && totalatmeet == 0 || totalAbstain == 0 && totaldisaproved == 0 && totalatmeet == 0)
                                {
                                    if (getresultlist.CircularDuedate <= DateTime.Now)
                                    {
                                        item.CircularAgendaResultVM.Result = "D";
                                        item.CircularAgendaResultVM.Remark = "Not Passed";
                                    }
                                    else
                                    {
                                        item.CircularAgendaResultVM.Result = "No Responses";
                                    }
                                }
                                else if ((totalApproved == 50 && totaldisaproved == 50) || (totalatmeet == 50 && totalApproved == 50))
                                {
                                    item.CircularAgendaResultVM.Result = "D";
                                    item.CircularAgendaResultVM.Remark = "Not Passed";
                                }
                                if (item.CircularAgendaResultVM.Result != null)
                                {
                                    var checkResultforVoting = (from mappin in entities.BM_MeetingAgendaMapping
                                                                where mappin.AgendaID == item.AgendaId
                                                                && mappin.MeetingID == item.MeetingId
                                                                && mappin.MeetingAgendaMappingID == item.MeetingAgendaMappingId
                                                                select mappin).FirstOrDefault();
                                    if (checkResultforVoting != null)
                                    {
                                        checkResultforVoting.Result = item.CircularAgendaResultVM.Result;
                                        checkResultforVoting.ResultRemark = item.CircularAgendaResultVM.Remark;
                                        entities.SaveChanges();
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        #endregion

        #region Pending Availability
        public List<MeetingVM> GetPedingAvailability(long UserId)
        {
            try
            {
                var date = DateTime.Now.Date;
                var result = (from row in entities.BM_Meetings
                              join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                              join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                              join participants in entities.BM_MeetingParticipant on row.MeetingID equals participants.Meeting_ID

                              where participants.UserId == UserId && row.IsSeekAvailabilitySent == true && row.AvailabilityDueDate >= date && row.IsDeleted == false && participants.IsDeleted == false
                              select new MeetingVM
                              {
                                  MeetingID = row.MeetingID,
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = meeting.MeetingTypeName,
                                  Quarter_ = row.Quarter_,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  IsSeekAvailability = row.IsSeekAvailability,
                                  SeekAvailability_DueDate = row.AvailabilityDueDate,
                                  Availability_ShortDesc = row.Availability_ShortDesc,
                                  ParticipantId = participants.MeetingParticipantId,
                                  Entityt_Id = row.EntityId,
                                  EntityName = entity.CompanyName,
                                  MeetingAviability = (from ava in entities.BM_MeetingAvailability where ava.MeetingID == row.MeetingID select new Aviability { SeekAviabiltyDate = ava.AvailabilityDate, Isavialale = false, seekavaTime = ava.AvailabilityTime + "" + " - " + "" + ava.Availability_ToTime + "   " + "IST", MeetingId = ava.MeetingID, meetingavaiId = ava.MeetingAvailabilityId, ParticipantId = participants.MeetingParticipantId }).ToList()

                              }).ToList();
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        if (item.MeetingAviability != null)
                        {
                            foreach (var items in item.MeetingAviability)
                            {
                                items.ResponseAviabilityId = (from y in entities.BM_MeetingAvailabilityResponse where y.Meeting_ID == items.MeetingId && y.MeetingParticipantID == items.ParticipantId && y.MeetingAvailabilityID == items.meetingavaiId select y.AvailabilityResponseID).FirstOrDefault();
                                items.AviabilityResponses = (from x in entities.BM_MeetingAvailabilityResponse where x.Meeting_ID == items.MeetingId && x.MeetingParticipantID == items.ParticipantId && x.MeetingAvailabilityID == items.meetingavaiId select x.Response).FirstOrDefault();
                                items.seekavadate = items.SeekAviabiltyDate.ToString("dddd, MMM  dd");
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public Aviability UpdateSeekaviabilityResponses(Aviability item, int userID)
        {
            try
            {
                var check = (from row in entities.BM_MeetingAvailabilityResponse where row.Meeting_ID == item.MeetingId && row.MeetingAvailabilityID == item.meetingavaiId && row.MeetingParticipantID == item.ParticipantId select row).FirstOrDefault();
                if (check != null)
                {
                    if (item.AviabilityResponses == true)
                    {
                        check.Response = true;

                    }
                    else
                    {
                        check.Response = false;
                    }
                    check.UpdatedOn = DateTime.Now;
                    check.UpdatedBy = userID;
                    entities.SaveChanges();

                }
                else
                {
                    BM_MeetingAvailabilityResponse objaviability = new BM_MeetingAvailabilityResponse();
                    objaviability.Meeting_ID = item.MeetingId;
                    objaviability.MeetingAvailabilityID = item.meetingavaiId;
                    objaviability.MeetingParticipantID = item.ParticipantId;
                    if (item.AviabilityResponses == true)
                    {
                        objaviability.Response = true;
                    }
                    else
                    {
                        objaviability.Response = false;
                    }
                    objaviability.CreatedOn = DateTime.Now;
                    objaviability.CreatedBy = userID;
                    entities.BM_MeetingAvailabilityResponse.Add(objaviability);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return item;
        }

        public bool MarkAttendence(long meetingId, long participantId, int UserId)
        {
            try
            {
                var checkAttendence = (from row in entities.BM_MeetingAttendance where row.MeetingId == meetingId && row.MeetingParticipantId == participantId select row).FirstOrDefault();
                if (checkAttendence != null)
                {
                    checkAttendence.Attendance = "P";
                    checkAttendence.UpdatedOn = DateTime.Now;
                    checkAttendence.Updatedby = UserId;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    BM_MeetingAttendance _objattendence = new BM_MeetingAttendance();
                    _objattendence.MeetingId = meetingId;
                    _objattendence.MeetingParticipantId = participantId;
                    _objattendence.Attendance = "P";
                    _objattendence.CreatedOn = DateTime.Now;
                    _objattendence.Createdby = UserId;
                    entities.BM_MeetingAttendance.Add(_objattendence);
                    entities.SaveChanges();
                    return true;


                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;

            }
        }


        #endregion
    }
}