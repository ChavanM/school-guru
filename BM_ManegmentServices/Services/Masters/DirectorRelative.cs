﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class DirectorRelative : IDirectorRelative
    {
        public DirectorRelativesVM AddRelatives(DirectorRelativesVM obj)
        {
            try
            {
                obj.Message = new Response();

                if(obj.Relation == "HUF" || obj.Relation == "Spouse" || obj.Relation == "Father" || obj.Relation == "Mother")
                {
                    if(CheckRelativeExist(obj.Director_ID, obj.Relation))
                    {
                        obj.Message.Error = true;
                        obj.Message.Message = "Relation allready exists.";
                        return obj;
                    }
                }

                if(obj.Minor_or_Adult == null)
                {
                    obj.Minor_or_Adult = true;
                }

                if(obj.MaritalStatus == null)
                {
                    obj.MaritalStatus = false;
                }

                if(obj.MaritalStatus == true)
                {
                    obj.Minor_or_Adult = false;
                }
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_Directors_Relatives _obj = new BM_Directors_Relatives();
                    _obj.Director_Id = obj.Director_ID;
                    _obj.Name = obj.Name;
                    _obj.IsMinor = (bool) obj.Minor_or_Adult;
                    _obj.IsMarried = (bool)obj.MaritalStatus;
                    _obj.Spouse_Name = obj.Spouse;
                    _obj.Relation = obj.Relation;

                    _obj.IsActive = true;
                    _obj.IsDeleted = false;
                    _obj.CreatedBy = obj.CreatedBy;
                    _obj.CreatedOn = DateTime.Now;

                    #region Relation Type
                    switch (obj.Relation)
                    {
                        case "HUF":
                            _obj.Relative_Type = "1";
                            break;
                        case "Spouse":
                            _obj.Relative_Type = "2";
                            break;
                        case "Father":
                            _obj.Relative_Type = "3";
                            break;
                        case "Mother":
                            _obj.Relative_Type = "4";
                            break;
                        case "Son":
                            _obj.Relative_Type = "5";
                            break;
                        case "Daughter":
                            _obj.Relative_Type = "6";
                            break;
                        case "Brother":
                            _obj.Relative_Type = "7";
                            break;
                        case "Sister":
                            _obj.Relative_Type = "8";
                            break;
                    }
                    #endregion

                    #region Marital Staus
                    switch (obj.Relation)
                    {
                        case "HUF":
                        case "Spouse":
                        case "Father":
                        case "Mother":

                            _obj.IsMinor = false;
                            _obj.IsMarried = true;
                            break;
                        default:
                            break;
                    }
                            #endregion

                    entities.BM_Directors_Relatives.Add(_obj);
                    entities.SaveChanges();

                    obj.Id = _obj.Id;

                    obj.Message.Success = true;
                    obj.Message.Message = "Saved Successfully.";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.Message.Error = true;
                obj.Message.Message = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public DirectorRelativesVM UpdateRelatives(DirectorRelativesVM obj)
        {
            try
            {
                obj.Message = new Response();
                obj.IsActive = true;

                if (obj.Minor_or_Adult == null)
                {
                    obj.Minor_or_Adult = true;
                }

                if (obj.MaritalStatus == null)
                {
                    obj.MaritalStatus = false;
                }

                if (obj.MaritalStatus == true)
                {
                    obj.Minor_or_Adult = false;
                }

                if (obj.ChangeTypeId == 1)
                {
                    obj.IsActive = false;
                }
                else
                {
                    obj.IsActive = true;
                }

                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_Directors_Relatives _obj = entities.BM_Directors_Relatives.Where(k => k.Id == obj.Id).FirstOrDefault();

                    if (_obj != null)
                    {
                        _obj.Name = obj.Name;
                        _obj.IsMinor = (bool)obj.Minor_or_Adult;
                        _obj.IsMarried = (bool)obj.MaritalStatus;
                        _obj.Spouse_Name = obj.Spouse;
                        _obj.ChangeType = obj.ChangeTypeId;

                        _obj.IsActive = obj.IsActive;
                        _obj.ChangeDate = obj.ChangeDate;



                        _obj.UpdatedBy = obj.UpdatedBy;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                    else
                    {
                        obj.Message.Error = true;
                        obj.Message.Message = "Something wents wrong";
                    }

                    obj.Message.Success = true;
                    obj.Message.Message = "Updated Successfully.";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.Message.Error = true;
                obj.Message.Message = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;

        }

        public DirectorRelativesVM Delete(DirectorRelativesVM obj)
        {
            try
            {
                obj.Message = new Response();
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_Directors_Relatives _obj = entities.BM_Directors_Relatives.Where(k => k.Id == obj.Id).FirstOrDefault();

                    if (_obj != null)
                    {
                        _obj.IsDeleted = true;

                        _obj.UpdatedBy = obj.UpdatedBy;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                    else
                    {
                        obj.Message.Error = true;
                        obj.Message.Message = SecretarialConst.Messages.serverError;
                        //obj.Message.Message = "Something wents wrong";
                    }

                    obj.Message.Success = true;
                    obj.Message.Message = "Deleted Successfully";
                }
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public List<DirectorRelativesVM> GetAllRelativesEnum(long DirectorId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {

                var relation = Enum.GetValues(typeof(RelationEnum)).Cast<RelationEnum>()
                            .Select(r => new { Value = (int)r, Name = r.ToString() }).ToList();

                var result = (
                              from relative in entities.BM_Directors_Relatives
                              where relative.Director_Id == DirectorId
                              orderby relative.Relative_Type
                              select relative).ToList();

                var r3 = (from r1 in result
                         join r2 in relation on r1.Relative_Type equals r2.Value.ToString()
                         select new DirectorRelativesVM
                         {
                             Id = r1.Id,
                             Name = r1.Name,
                             Director_ID = r1.Director_Id,
                             Minor_or_Adult = r1.IsMinor,
                             MaritalStatus = r1.IsMarried,
                             Spouse = r1.Spouse_Name,
                             Relation = r2.Name
                         }).ToList();

                return r3;

            }
        }

        public List<DirectorRelativesVM> GetAllRelatives(long DirectorId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {

                var result =(from row in entities.BM_Directors_Relatives
                             where row.Director_Id == DirectorId  && row.IsDeleted == false && row.IsActive == true
                             orderby row.Relative_Type , row.Id
                              select new DirectorRelativesVM
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  Director_ID = row.Director_Id,
                                  Minor_or_Adult = row.IsMinor,
                                  MaritalStatus = row.IsMarried,
                                  Spouse = row.Spouse_Name,
                                  Relation = row.Relation
                                  //ChangeTypeId = row.ChangeType
                              }).ToList();

                return result;

            }
        }

        public List<RelationVM> GetRelation()
        {
            List<RelationVM> lst = new List<RelationVM>()
            {
                new RelationVM() {id = "HUF", text = "HUF" },
                new RelationVM() {id = "Spouse", text = "Spouse" },
                new RelationVM() {id = "Father", text = "Father" },
                new RelationVM() {id = "Mother", text = "Mother" },
                new RelationVM() {id = "Son", text = "Son" },
                new RelationVM() {id = "Daughter" , text = "Daughter" },
                new RelationVM() {id = "Brother", text = "Brother" },
                new RelationVM() {id = "Sister", text = "Sister" }
            };

            return lst;
        }

        public List<RelationVM> GetRelation(long Director_Id)
        {
            List<RelationVM> lst = new List<RelationVM>();

            #region check relative exists or not
            if(!CheckRelativeExist(Director_Id, "HUF"))
            {
                lst.Add(new RelationVM() { id = "HUF", text = "HUF" });
            }
            if (!CheckRelativeExist(Director_Id, "Spouse"))
            {
                lst.Add(new RelationVM() { id = "Spouse", text = "Spouse" });
            }
            if (!CheckRelativeExist(Director_Id, "Father"))
            {
                lst.Add(new RelationVM() { id = "Father", text = "Father" });
            }
            if (!CheckRelativeExist(Director_Id, "Mother"))
            {
                lst.Add(new RelationVM() { id = "Mother", text = "Mother" });
            }
            #endregion

            lst.Add(new RelationVM() { id = "Son", text = "Son" });
            lst.Add(new RelationVM() { id = "Daughter", text = "Daughter" });
            lst.Add(new RelationVM() { id = "Brother", text = "Brother" });
            lst.Add(new RelationVM() { id = "Sister", text = "Sister" });

            return lst;
        }

        public bool CheckRelativeExist(long Director_Id, string Relation)
        {
            var result = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = entities.BM_Directors_Relatives.Where(k => k.Director_Id == Director_Id && k.IsDeleted == false && k.Relation == Relation).Any();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
    }
}