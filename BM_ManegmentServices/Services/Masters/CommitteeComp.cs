﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class CommitteeComp : ICommitteeComp
    {
        public CommitteeCompVM CreateOrUpdate(CommitteeCompVM obj)
        {
            obj.Response = new Response();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var isExist = false;
                    if(obj.Customer_Id == null)
                    {
                        isExist = entities.BM_CommitteeComp.Where(k => k.Id != obj.Id && k.Name == obj.Name && k.IsDeleted == false).Any();
                    }
                    else
                    {
                        isExist = entities.BM_CommitteeComp.Where(k => k.Id != obj.Id && k.Name == obj.Name && (k.Customer_Id == null || k.Customer_Id == obj.Customer_Id) && k.IsDeleted == false).Any();
                    }
                    
                    if (isExist == false)
                    {
                        BM_CommitteeComp _obj = new BM_CommitteeComp();
                        _obj.Id = obj.Id;
                        _obj.Name = obj.Name;
                        _obj.Customer_Id = obj.Customer_Id;
                        _obj.Description_ = obj.Description_;
                        _obj.IsCommittee = obj.IsCommittee;
                        _obj.IsMeeting = true;
                        _obj.IsCircular = true;
                        _obj.CompositionType = obj.CompositionType;
                        _obj.MeetingTypeName = obj.Name;// + " Meeting";
                        _obj.IsDeleted = false;
                        _obj.CreatedBy = obj.CreatedBy;
                        _obj.CreatedOn = obj.CreatedOn;

                        entities.BM_CommitteeComp.Add(_obj);
                        entities.SaveChanges();
                        obj.Id = _obj.Id;
                        obj.Response.Message = "Saved Successfully.";
                        obj.Response.Success = true;
                    }
                    else
                    {
                        obj.Response.Success = false;
                        obj.Response.Error = true;
                        obj.Response.Message = "Committee Name Exist.";
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Response.Message = "Server Error Occure";
                obj.Response.Error = true;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public CommitteeCompVM Update(CommitteeCompVM obj)
        {
            obj.Response = new Response();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var isExist = false;
                    if (obj.Customer_Id == null)
                    {
                        isExist = entities.BM_CommitteeComp.Where(k => k.Id != obj.Id && k.Name == obj.Name && k.IsDeleted == false).Any();
                    }
                    else
                    {
                        isExist = entities.BM_CommitteeComp.Where(k => k.Id != obj.Id && k.Name == obj.Name && (k.Customer_Id == null || k.Customer_Id == obj.Customer_Id) && k.IsDeleted == false).Any();
                    }
                    if (isExist == false)
                    {
                        BM_CommitteeComp _obj = entities.BM_CommitteeComp.Where(k => k.Id == obj.Id && k.IsDeleted == false).FirstOrDefault();
                        if(_obj != null)
                        {
                            _obj.Name = obj.Name;
                            _obj.Description_ = obj.Description_;
                            _obj.MeetingTypeName = obj.Name + " Meeting";
                            _obj.IsDeleted = false;
                            _obj.UpdatedBy = obj.UpdatedBy;
                            _obj.UpdatedOn = obj.UpdatedOn;

                            entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                            entities.SaveChanges();

                            obj.Response.Message = "Updated Successfully.";
                            obj.Response.Success = true;
                        }
                        else
                        {
                            obj.Response.Message = "Something went wrong";
                            obj.Response.Error = true;
                            obj.Response.Success = false;
                        }
                    }
                    else
                    {
                        obj.Response.Success = false;
                        obj.Response.Error = true;
                        obj.Response.Message = "Committee Name Exist.";
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Response.Message = "Server Error Occure";
                obj.Response.Error = true;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public bool Delete(int id)
        {
            var value = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var isExist = (from director in entities.BM_DirectorMaster
                                   join committee in entities.BM_Directors_DetailsOfCommiteePosition on director.Id equals committee.Director_Id
                                   where director.Is_Deleted == false 
                                   && committee.Committee_Id == id
                                   && committee.IsDeleted == false
                                   select director.Id).Any();

                    if(isExist == false)
                    {
                        var result = entities.BM_CommitteeComp.Find(id);
                        if (result != null)
                        {
                            result.IsDeleted = true;
                            entities.Entry(result).State = System.Data.Entity.EntityState.Modified;
                            entities.SaveChanges();
                            value = true;
                        }
                        else
                        {
                            value = false;
                        }
                    }
                    else
                    {
                        value = false;
                    }                    
                }
            }
            catch (Exception ex)
            {
                value = false;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return value;
        }

        public bool CheckAsPerStandard(int CommitteeId, int? CustomerId, int? EntityId)
        {
            var result = true;
            if(CustomerId == null)
            {
                result = false;
            }
            else
            {
                var committee = GetCommitteeComp(CommitteeId);
                if (committee != null)
                {
                    if (committee.Customer_Id == null)
                    {
                        using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                        {
                            var IsExists = false;
                            IsExists = entities.BM_Committee_Rule.Where(k => k.CommitteeId == CommitteeId && k.Customer_Id == CustomerId && k.EntityId == EntityId && k.IsDeleted == false).Any();
                            if (IsExists == false)
                            {
                                IsExists = entities.BM_Committee_MeetingRule.Where(k => k.CommitteeId == CommitteeId && k.Customer_Id == CustomerId && k.EntityId == EntityId && k.IsDeleted == false).Any();
                            }

                            if (IsExists)
                            {
                                result = false;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public List<CommitteeCompVM> GetAllCommitteeCompList(int customerID)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = (from row in entities.BM_CommitteeComp
                              where row.IsCommittee == true 
                              && row.CompositionType == "C" 
                              && row.IsDeleted == false
                              orderby row.Name
                              select new CommitteeCompVM()
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  Description_ = row.Description_
                              }).ToList();
                return result;
            }
        }

        public IEnumerable<CommitteeCompVM> GetAllCommitteeComp(int? customerID)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = new List<CommitteeCompVM>();

                if (customerID == null)
                {
                    result = (from row in entities.BM_CommitteeComp
                              where row.Customer_Id == customerID 
                              && row.IsCommittee == true 
                              && row.CompositionType == "C" 
                              && row.IsDeleted == false
                              orderby row.Name
                              select new CommitteeCompVM()
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  Description_ = row.Description_,
                                  Customer_Id = row.Customer_Id
                              }).ToList();
                }
                else
                {
                    //Load defaults committees
                    result = (from row in entities.BM_CommitteeComp
                              where (row.Customer_Id == customerID  || row.Customer_Id == null ) 
                              && row.IsCommittee == true 
                              && row.CompositionType == "C" 
                              && row.IsDeleted == false
                              orderby row.Customer_Id, row.Name
                              select new CommitteeCompVM()
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  Description_ = row.Description_,
                                  Customer_Id = row.Customer_Id
                              }).ToList();
                }

                return result;
            }
        }

        public IEnumerable<CommitteeCompVM> GetAllCommitteeCompForMaster(int? customerID)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result  = new List<CommitteeCompVM>();

                if(customerID == null)
                {
                    result = (from row in entities.BM_CommitteeComp
                              where row.Customer_Id == customerID 
                              && row.IsCommittee == true 
                              && row.CompositionType == "C" 
                              && row.IsDeleted == false 
                              orderby row.Name
                              select new CommitteeCompVM()
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  Description_ = row.Description_,
                                  Customer_Id = row.Customer_Id
                              }).ToList();
                }
                else
                {
                    //Load defaults committees
                    result = (from row in entities.BM_CommitteeComp
                                  //where (row.Customer_Id == customerID  || row.Customer_Id == null ) && row.IsCommittee == true && row.CompositionType == "C" && row.IsDeleted == false
                              where (row.Customer_Id == customerID ) 
                              && row.IsCommittee == true 
                              && row.CompositionType == "C" 
                              && row.IsDeleted == false
                              orderby row.Customer_Id, row.Name
                              select new CommitteeCompVM()
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  Description_ = row.Description_,
                                  Customer_Id = row.Customer_Id
                              }).ToList();
                }
                
                return result;
            }
        }

        public CommitteeCompVM GetCommitteeComp(int Committee_Id)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = (from row in entities.BM_CommitteeComp
                              where row.Id == Committee_Id 
                              && row.IsCommittee == true 
                              && row.CompositionType == "C" 
                              && row.IsDeleted == false
                              select new CommitteeCompVM()
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  Description_ = row.Description_,
                                  Customer_Id= row.Customer_Id
                              }).FirstOrDefault();
                return result;
            }
        }

        public Composition GetComposition(int RuleId, int CommitteeId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = (from row in entities.BM_Committee_Rule
                              where row.Id == RuleId 
                              && row.CommitteeId == CommitteeId 
                              && row.IsDeleted == false
                              select new Composition()
                              {
                                  RuleId = row.Id,
                                  CommitteeId = row.CommitteeId,
                                  Entity_Type = row.Entity_Type,
                                  RuleType = row.RuleType,
                                  DesignationId = row.DesignationId,
                                  DesignationCount = row.Strength_Count,
                                  DesignationDenominator = row.Strength_Denominator,
                                  DesignationNumerator = row.Strength_Numerator,
                                  Customer_Id = row.Customer_Id,
                                  Entity_Id = row.EntityId
                              }).FirstOrDefault();
                return result;
            }
        }

        public List<Composition> GetAllComposition(int CommitteeId, int EntityType, string RuleType, int? CustomerId, int? EntityId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = new List<Composition>();

                if(CustomerId == null)
                {
                    EntityId = null;
                }

                result = (from row in entities.BM_Committee_Rule
                              from designation in entities.BM_Directors_Designation.Where(k => k.Id == row.DesignationId).DefaultIfEmpty()
                              where row.CommitteeId == CommitteeId 
                              && row.Entity_Type == EntityType 
                              && row.RuleType == RuleType
                              //&& row.Customer_Id == CustomerId 
                              && row.EntityId == EntityId 
                              && row.IsDeleted == false
                              select new Composition()
                              {
                                  RuleId = row.Id,
                                  CommitteeId = row.CommitteeId,
                                  Entity_Type = row.Entity_Type,
                                  RuleType = row.RuleType,
                                  DesignationId = row.DesignationId,
                                  Designation = designation.Name,
                                  DesignationCount = row.Strength_Count,
                                  DesignationDenominator = row.Strength_Denominator,
                                  DesignationNumerator = row.Strength_Numerator,
                                  Customer_Id = row.Customer_Id,
                                  Entity_Id = row.EntityId,

                                  StdRefId = row.Std_RefId
                              }).ToList();

                #region Generate Rules
                foreach (var item in result)
                {
                    string Sup = "";
                    if (item.DesignationDenominator == 3)
                    {
                        Sup = "<sup>rd</sup>";
                    }
                    else if (item.DesignationDenominator >= 4 && item.DesignationDenominator <= 9)
                    {
                        Sup = "<sup>th</sup>";
                    }

                    if (item.DesignationCount > 0 && item.DesignationId == null && item.DesignationNumerator == null && item.DesignationDenominator == null)
                    {
                        item.Rule = "Minimum <b>" + item.DesignationCount + "</b> member";
                    }
                    else if (item.DesignationCount > 0 && item.DesignationId > 0 && item.DesignationNumerator == null && item.DesignationDenominator == null)
                    {
                        if (item.DesignationCount == 1)
                        {
                            item.Rule = "At least <b>" + item.DesignationCount + " " + item.Designation + "</b> Director";
                        }
                        else
                        {
                            item.Rule = "Minimum <b>" + item.DesignationCount + " " + item.Designation + "</b> Director";
                        }
                    }
                    else if (item.DesignationCount == null && item.DesignationId > 0 && item.DesignationNumerator > 0 && item.DesignationDenominator > 0)
                    {
                        item.Rule = "<b>" + item.Designation + "</b> Director " +
                            "<br>More than <b>" + item.DesignationNumerator + "/" + item.DesignationDenominator + Sup+"</b> of total strength";
                    }
                    else if (item.DesignationCount > 0 && item.DesignationId == null && item.DesignationNumerator > 0 && item.DesignationDenominator > 0)
                    {
                        item.Rule = "<b>" + item.DesignationCount + "</b> member " +
                            " or <b>" + item.DesignationNumerator + "/" + item.DesignationDenominator + Sup+"</b> of the member whichever is higher.";
                    }
                    else if (item.DesignationCount > 0 && item.DesignationId > 0 && item.DesignationNumerator > 0 && item.DesignationDenominator > 0)
                    {
                        item.Rule = "<b>" + item.Designation + "</b> Director " +
                            "<br><b>" + item.DesignationCount + "</b> " +
                            " or <b>" + item.DesignationNumerator + "/" + item.DesignationDenominator + Sup+"</b> whichever is higher.";
                    }
                    else
                    {
                        item.Rule = "";
                    }
                }
                #endregion

                return result;
            }
        }

        public Composition CreateRule(Composition obj)
        {
            obj.Message = new Response();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_Committee_Rule compRule = new BM_Committee_Rule();
                    compRule.Id = obj.RuleId;
                    compRule.CommitteeId = obj.CommitteeId;
                    compRule.Entity_Type = obj.Entity_Type;

                    compRule.RuleType = obj.RuleType;
                    if(obj.DesignationId == null && obj.DesignationCount ==null && obj.DesignationDenominator == null && obj.DesignationNumerator== null)
                    {
                        obj.Message.Error = true;
                        obj.Message.Message = "Invalid data";
                    }
                    else
                    {
                        compRule.DesignationId = obj.DesignationId;
                        compRule.Strength_Count = obj.DesignationCount;
                        compRule.Strength_Denominator = obj.DesignationDenominator;
                        compRule.Strength_Numerator = obj.DesignationNumerator;

                        compRule.Customer_Id = obj.Customer_Id;
                        compRule.EntityId = obj.Entity_Id;

                        compRule.IsDeleted = false;
                        compRule.CreatedBy = obj.CreatedBy;
                        compRule.CreatedOn = obj.CreatedOn;

                        entities.BM_Committee_Rule.Add(compRule);
                        int count = entities.SaveChanges();

                        obj.RuleId = compRule.Id;

                        if (count > 0)
                        {
                            obj.Message.Success = true;
                            obj.Message.Message = "Saved Successfully.";
                        }
                        else
                        {
                            obj.Message.Error = true;
                            obj.Message.Message = "Something went wrong";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public Composition UpdateRule(Composition obj)
        {
            obj.Message = new Response();
            try
            {
                if (obj.DesignationId == null && obj.DesignationCount == null && obj.DesignationDenominator == null && obj.DesignationNumerator == null)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Invalid data";
                }
                else if (obj.DesignationDenominator == null && obj.DesignationNumerator != null)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Invalid data";
                }
                else if (obj.DesignationDenominator != null && obj.DesignationNumerator == null)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Invalid data";
                }
                else
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        
                        BM_Committee_Rule compRule = entities.BM_Committee_Rule.Where(k => k.Id == obj.RuleId && k.CommitteeId == obj.CommitteeId && k.Entity_Type == obj.Entity_Type && k.IsDeleted == false).FirstOrDefault();

                        if (compRule != null)
                        {
                            if(compRule.Std_RefId !=null)
                            {
                                var StdcompRule = entities.BM_Committee_Rule.Where(k => k.Id == compRule.Std_RefId && k.IsDeleted == false).FirstOrDefault();

                                if(StdcompRule.DesignationId == null && obj.DesignationId != null)
                                {
                                    obj.Message.Error = true;
                                    obj.Message.Message = "Standard rule does not contains Designation";
                                }
                                else if (StdcompRule.DesignationId != null && obj.DesignationId == null)
                                {
                                    obj.Message.Error = true;
                                    obj.Message.Message = "Standard rule contains Designation";
                                }
                                else if (StdcompRule.DesignationId != obj.DesignationId)
                                {
                                    obj.Message.Error = true;
                                    obj.Message.Message = "Designation does not match with Standard rule.";
                                }
                                else if (StdcompRule.Strength_Count > obj.DesignationCount)
                                {
                                    obj.Message.Error = true;
                                    obj.Message.Message = "Minimum count not match as per Standard";
                                }
                                else if ((StdcompRule.Strength_Numerator == null && obj.DesignationNumerator != null) || (StdcompRule.Strength_Numerator != null && obj.DesignationNumerator == null))
                                {
                                    obj.Message.Error = true;
                                    obj.Message.Message = "Minimum Strength not match as per Standard";
                                }
                                else if ((StdcompRule.Strength_Denominator == null && obj.DesignationDenominator != null) || (StdcompRule.Strength_Denominator != null && obj.DesignationDenominator == null))
                                {
                                    obj.Message.Error = true;
                                    obj.Message.Message = "Minimum Strength not match as per Standard";
                                }
                                else if (StdcompRule.Strength_Denominator > 0)
                                {
                                    if(obj.DesignationDenominator == null)
                                    {
                                        obj.Message.Error = true;
                                        obj.Message.Message = "Minimum Strength not match as per Standard";
                                    }
                                    else
                                    {
                                        if(((decimal)StdcompRule.Strength_Numerator / (decimal)StdcompRule.Strength_Denominator) > ((decimal)obj.DesignationNumerator / (decimal)obj.DesignationDenominator))
                                        {
                                            obj.Message.Error = true;
                                            obj.Message.Message = "Minimum Strength not match as per Standard";
                                        }
                                    }
                                }
                            }

                            if(obj.Message.Error == true)
                            {
                                return obj;
                            }

                            compRule.RuleType = obj.RuleType;
                            compRule.DesignationId = obj.DesignationId;
                            compRule.Strength_Count = obj.DesignationCount;
                            compRule.Strength_Denominator = obj.DesignationDenominator;
                            compRule.Strength_Numerator = obj.DesignationNumerator;
                            compRule.UpdatedBy = obj.UpdatedBy;
                            compRule.UpdatedOn = obj.UpdatedOn;

                            entities.Entry(compRule).State = System.Data.Entity.EntityState.Modified;
                            int count = entities.SaveChanges();
                            if (count > 0)
                            {
                                obj.Message.Success = true;
                                obj.Message.Message = "Updated Successfully.";
                            }
                            else
                            {
                                obj.Message.Error = true;
                                obj.Message.Message = "Something wents wrong";
                            }
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";// : "Something wents wrong";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public bool DeleteRule(int id, int CommitteeId, int UserId)
        {
            var result = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_Committee_Rule compRule = entities.BM_Committee_Rule.Where(k => k.Id == id && k.CommitteeId == CommitteeId && k.IsDeleted == false).FirstOrDefault();

                    if (compRule != null)
                    {
                        compRule.IsDeleted = true;
                        compRule.UpdatedBy = UserId;
                        compRule.UpdatedOn = DateTime.Now;
                        entities.Entry(compRule).State = System.Data.Entity.EntityState.Modified;
                        int count = entities.SaveChanges();
                        if (count > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public MeetingDetails GetMeetingRule(int CommitteeId, int EntityType, int? CustomerId, int? EntityId)
        {
            MeetingDetails obj = new MeetingDetails() { CommitteeId = CommitteeId, Entity_Type = EntityType };
            try
            {
                if(CustomerId == null)
                {
                    EntityId = null;
                }

                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var result = (from row in entities.BM_Committee_MeetingRule
                                  where row.CommitteeId == CommitteeId 
                                  && row.Entity_Type == EntityType
                                 // && row.Customer_Id == CustomerId 
                                  && row.EntityId == EntityId 
                                  && row.IsDeleted == false
                                  select new MeetingDetails
                                  {
                                      MeetingId = row.Id,
                                      CommitteeId = row.CommitteeId,
                                      Entity_Type = row.Entity_Type,
                                      MinMeeting = row.MinMeeting,
                                      MeetingGapInDays = row.MeetingGapInDays,
                                      VotingRequirement = row.VotingRequirement,
                                      DisclosureRequirement = row.DisclosureRequirement,
                                      IsMeetingSchedule = row.IsMeetingSchedule,
                                      StdRefId = row.Std_RefId,

                                      Customer_Id = row.Customer_Id,
                                      Entity_Id = row.EntityId
                                  }).FirstOrDefault();

                    if(result !=null)
                    {
                        obj = result;

                        var lstMeetingSchedule = (from row in entities.BM_Committee_MeetingSchedule
                                                 where row.CommitteeId == CommitteeId 
                                                 && row.Entity_Type == EntityType 
                                                 && row.Customer_Id == CustomerId 
                                                 && row.EntityId == EntityId 
                                                 && row.IsDeleted == false
                                                 select new MeetingSchedule
                                                 {
                                                     MeetingScheduleId = row.Id,
                                                     CommitteeId = row.CommitteeId,
                                                     Entity_Type = row.Entity_Type,

                                                     Quarter_ = row.Quarter_,
                                                     Details = row.details,
                                                     Day = row.Day_,
                                                     Month = row.Month_,
                                                     
                                                     Customer_Id = row.Customer_Id,
                                                     Entity_Id = row.EntityId
                                                 }).ToList();

                        obj.lstMeetingSchedule = lstMeetingSchedule;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Message = new Response();
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public MeetingDetails CreateOrUpdate(MeetingDetails obj)
        {
            obj.Message = new Response();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = new BM_Committee_MeetingRule();
                    _obj.Id = obj.MeetingId;

                    #region Save/ Update Meeting Details
                    if (obj.MeetingId >0)
                    {
                        _obj = entities.BM_Committee_MeetingRule.Where(k => k.Id == obj.MeetingId && k.CommitteeId == obj.CommitteeId && k.Entity_Type == obj.Entity_Type && k.Customer_Id == obj.Customer_Id && k.EntityId == obj.Entity_Id  && k.IsDeleted == false).FirstOrDefault();

                        if(_obj !=null)
                        {
                            _obj.CommitteeId = obj.CommitteeId;
                            _obj.Entity_Type = obj.Entity_Type;

                            _obj.MinMeeting = obj.MinMeeting;
                            _obj.MeetingGapInDays = obj.MeetingGapInDays;
                            _obj.VotingRequirement = obj.VotingRequirement;
                            _obj.DisclosureRequirement = obj.DisclosureRequirement;

                            _obj.IsMeetingSchedule = obj.IsMeetingSchedule;
                            _obj.UpdatedBy = obj.UpdatedBy;
                            _obj.UpdatedOn = DateTime.Now;
                            entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                            entities.SaveChanges();

                            obj.Message.Success = true;
                            obj.Message.Message = "Update Successfully.";
                        }
                        else
                        {
                            obj.Message.Error = true;
                            obj.Message.Message = "Something wents wrong";
                        }

                    }
                    else
                    {
                        _obj.CommitteeId = obj.CommitteeId;
                        _obj.Entity_Type = obj.Entity_Type;

                        _obj.MinMeeting = obj.MinMeeting;
                        _obj.MeetingGapInDays = obj.MeetingGapInDays;
                        _obj.VotingRequirement = obj.VotingRequirement;
                        _obj.DisclosureRequirement = obj.DisclosureRequirement;

                        _obj.IsMeetingSchedule = obj.IsMeetingSchedule;
                        _obj.CreatedBy = obj.CreatedBy;
                        _obj.CreatedOn = DateTime.Now;
                        _obj.IsDeleted = false;

                        _obj.Customer_Id = obj.Customer_Id;
                        _obj.EntityId = obj.Entity_Id;

                        entities.BM_Committee_MeetingRule.Add(_obj);
                        entities.SaveChanges();

                        obj.MeetingId = _obj.Id;

                        obj.Message.Success = true;
                        obj.Message.Message = "Save Successfully.";
                    }
                    #endregion

                    if(obj.lstMeetingSchedule != null)
                    {
                        foreach (var objItem in obj.lstMeetingSchedule)
                        {
                            #region Save / Update Meeting schedule Details

                            var _objItem = new BM_Committee_MeetingSchedule();
                            _objItem.Id = objItem.MeetingScheduleId;

                            if (objItem.MeetingScheduleId > 0)
                            {
                                _objItem = entities.BM_Committee_MeetingSchedule.Where(k => k.Id == objItem.MeetingScheduleId && k.CommitteeId == objItem.CommitteeId && k.Entity_Type == objItem.Entity_Type && k.IsDeleted == false).FirstOrDefault();

                                if (_objItem != null)
                                {
                                    _objItem.CommitteeId = objItem.CommitteeId;
                                    _objItem.Entity_Type = objItem.Entity_Type;

                                    _objItem.Quarter_ = objItem.Quarter_;
                                    _objItem.details = objItem.Details;
                                    _objItem.Day_ = objItem.Day;
                                    _objItem.Month_ = objItem.Month;

                                    _objItem.UpdatedBy = objItem.UpdatedBy;
                                    _objItem.UpdatedOn = DateTime.Now;
                                    entities.Entry(_objItem).State = System.Data.Entity.EntityState.Modified;
                                    entities.SaveChanges();

                                }
                            }
                            else
                            {
                                _objItem.CommitteeId = objItem.CommitteeId;
                                _objItem.Entity_Type = objItem.Entity_Type;

                                _objItem.Quarter_ = objItem.Quarter_;
                                _objItem.details = objItem.Details;
                                _objItem.Day_ = objItem.Day;
                                _objItem.Month_ = objItem.Month;

                                _objItem.CreatedBy = objItem.CreatedBy;
                                _objItem.CreatedOn = DateTime.Now;
                                _objItem.IsDeleted = false;
                                _objItem.Customer_Id = objItem.Customer_Id;
                                _objItem.EntityId = objItem.Entity_Id;

                                entities.BM_Committee_MeetingSchedule.Add(_objItem);
                                entities.SaveChanges();

                                objItem.MeetingScheduleId = _objItem.Id;
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public bool CopyRules(int CommitteeId, int CustomerId, int EntityID, int UserId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var compRule = entities.BM_Committee_Rule.Where(k => k.CommitteeId == CommitteeId && k.Customer_Id == null && k.IsDeleted == false).ToList();

                if (compRule != null)
                {
                    List<BM_Committee_Rule> lstComp = new List<BM_Committee_Rule>();
                    foreach(var item in compRule)
                    {
                        lstComp.Add(new BM_Committee_Rule()
                        {
                            Id = 0,
                            Std_RefId = item.Id,
                            Customer_Id = CustomerId,
                            EntityId = EntityID,
                            IsDeleted = false,
                            CreatedBy = UserId,
                            CreatedOn = DateTime.Now,
                            UpdatedOn = null,

                            CommitteeId = item.CommitteeId,
                            Entity_Type = item.Entity_Type,
                            RuleType = item.RuleType,
                            DesignationId = item.DesignationId,
                            Strength_Count = item.Strength_Count,
                            Strength_Denominator = item.Strength_Denominator,
                            Strength_Numerator = item.Strength_Numerator

                        });
                    }

                    entities.BM_Committee_Rule.AddRange(lstComp);
                    entities.SaveChanges();
                }

                #region Copy Meeting data

                var compMeetingRule = entities.BM_Committee_MeetingRule.Where(k => k.CommitteeId == CommitteeId && k.Customer_Id == null && k.IsDeleted == false).ToList();

                if (compMeetingRule != null)
                {
                    List<BM_Committee_MeetingRule> lstCompMeetingRule = new List<BM_Committee_MeetingRule>();
                    foreach (var item in compMeetingRule)
                    {
                        lstCompMeetingRule.Add(new BM_Committee_MeetingRule()
                        {
                            Id = 0,
                            Customer_Id = CustomerId,
                            EntityId = EntityID,
                            IsDeleted = false,
                            CreatedBy = UserId,
                            CreatedOn = DateTime.Now,
                            UpdatedOn = null,

                            CommitteeId = item.CommitteeId,
                            Entity_Type = item.Entity_Type,
                            details = item.details,
                            MinMeeting = item.MinMeeting,
                            MeetingGapInDays = item.MeetingGapInDays,
                            VotingRequirement = item.VotingRequirement,
                            DisclosureRequirement = item.DisclosureRequirement,
                            IsMeetingSchedule = item.IsMeetingSchedule,

                            Std_RefId = item.Id
                        });
                    }

                    entities.BM_Committee_MeetingRule.AddRange(lstCompMeetingRule);
                    entities.SaveChanges();
                }

                #endregion

                #region Copy Meeting Schedule

                var compMeetingScheduleRule = entities.BM_Committee_MeetingSchedule.Where(k => k.CommitteeId == CommitteeId && k.Customer_Id == null && k.IsDeleted == false).ToList();

                if (compMeetingScheduleRule != null)
                {
                    List<BM_Committee_MeetingSchedule> lstcompMeetingScheduleRule = new List<BM_Committee_MeetingSchedule>();

                    foreach (var item in compMeetingScheduleRule)
                    {
                        lstcompMeetingScheduleRule.Add(new BM_Committee_MeetingSchedule()
                        {
                            Id = 0,
                            Customer_Id = CustomerId,
                            EntityId = EntityID,
                            IsDeleted = false,
                            CreatedBy = UserId,
                            CreatedOn = DateTime.Now,
                            UpdatedOn = null,

                            CommitteeId = item.CommitteeId,
                            Entity_Type = item.Entity_Type,
                            details = item.details,
                            Day_ = item.Day_,
                            Month_ = item.Month_
                        });
                    }

                    entities.BM_Committee_MeetingSchedule.AddRange(lstcompMeetingScheduleRule);
                    entities.SaveChanges();
                }

                #endregion

            }

            return true;
        }

        public IEnumerable<CommitteeCompVM> GetAllMeetingType()
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = new List<CommitteeCompVM>();

                    result = (from row in entities.BM_CommitteeComp
                              where row.Customer_Id == null 
                              && (row.CompositionType == "C" || row.CompositionType == "B") 
                              && row.IsDeleted == false
                              orderby row.SerialNo
                              select new CommitteeCompVM()
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  MeetingTypeName = row.MeetingTypeName
                              }).ToList();
                return result;
            }
        }

        public IEnumerable<CommitteeCompVM> GetAll_MeetingTypeByCustomer(int customerID, bool showAll)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = new List<CommitteeCompVM>();

                result = (from row in entities.BM_CommitteeComp
                          where (row.Customer_Id == null || row.Customer_Id == customerID)
                          && (row.CompositionType == "C" || row.CompositionType == "B")
                          && row.IsDeleted == false
                          orderby row.MeetingTypeName
                          select new CommitteeCompVM()
                          {
                              Id = row.Id,
                              Name = row.Name,
                              MeetingTypeName = row.MeetingTypeName
                          }).ToList();

                if (showAll)
                    result.Insert(0, new CommitteeCompVM() { Id = 0, Name = "All", MeetingTypeName = "All" });

                return result;
            }
        }

        public IEnumerable<CommitteeCompVM> GetAllMeetingType(int customerID, bool showAll, string role)
        {
            if(role == SecretarialConst.Roles.CSIMP)
            {
                return GetAllMeetingType();
            }
            else
            {
                return GetAll_MeetingTypeByCustomer(customerID, showAll);
            }
        }

        public IEnumerable<CommitteeCompVM> GetMeetingTypes(string Type, int customerID, int entityID, bool showGM)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = new List<CommitteeCompVM>();

                var entityType = (from row in entities.BM_EntityMaster
                                 where row.Id == entityID
                                 select row.Entity_Type).FirstOrDefault();

                var meetingTypes = (from row in entities.BM_CommitteeComp
                          where (row.Customer_Id == null || row.Customer_Id == customerID) 
                          && row.IsDeleted == false
                          orderby row.SerialNo == null ? 1 : 0, row.SerialNo
                          select new CommitteeCompVM()
                          {
                              Id = row.Id,
                              Name = row.Name,
                              MeetingTypeName = row.MeetingTypeName,
                              IsMeeting = row.IsMeeting,
                              IsCircular = row.IsCircular,
                              CompositionType = row.CompositionType
                          }).ToList();

                if (meetingTypes !=null)
                {
                    if(entityType == SecretarialConst.EntityTypeID.LLP)
                    {
                        if (Type.Trim().ToUpper().Equals("M"))
                        {
                            result.AddRange(meetingTypes.Where(k => k.CompositionType == "B" && k.IsMeeting == true && (k.Id == SecretarialConst.MeetingTypeID.PARTNERS || k.Id == SecretarialConst.MeetingTypeID.DESIGNATED_PARTNERS)));
                        }
                        else
                        {
                            result.AddRange(meetingTypes.Where(k => k.CompositionType == "B" && k.IsCircular == true && (k.Id == SecretarialConst.MeetingTypeID.PARTNERS || k.Id == SecretarialConst.MeetingTypeID.DESIGNATED_PARTNERS)));
                        }
                    }
                    else
                    {
                        if(showGM == false)
                        {
                            if (Type.Trim().ToUpper().Equals("M"))
                            {
                                result.AddRange(meetingTypes.Where(k => k.CompositionType == "B" && k.IsMeeting == true && k.Id != SecretarialConst.MeetingTypeID.PARTNERS && k.Id != SecretarialConst.MeetingTypeID.DESIGNATED_PARTNERS));
                            }
                            else
                            {
                                result.AddRange(meetingTypes.Where(k => k.CompositionType == "B" && k.IsCircular == true && k.Id != SecretarialConst.MeetingTypeID.PARTNERS && k.Id != SecretarialConst.MeetingTypeID.DESIGNATED_PARTNERS));
                            }
                        }
                        else
                        {
                            if (Type.Trim().ToUpper().Equals("M"))
                            {
                                result.AddRange(meetingTypes.Where(k => k.CompositionType == "B" && (k.IsMeeting == true || k.Id == SecretarialConst.MeetingTypeID.AGM || k.Id == SecretarialConst.MeetingTypeID.EGM) && k.Id != SecretarialConst.MeetingTypeID.PARTNERS && k.Id != SecretarialConst.MeetingTypeID.DESIGNATED_PARTNERS));
                            }
                            else
                            {
                                result.AddRange(meetingTypes.Where(k => k.CompositionType == "B" && (k.IsCircular == true || k.Id == SecretarialConst.MeetingTypeID.AGM || k.Id == SecretarialConst.MeetingTypeID.EGM) && k.Id != SecretarialConst.MeetingTypeID.PARTNERS && k.Id != SecretarialConst.MeetingTypeID.DESIGNATED_PARTNERS));
                            }
                        }
                        
                    }

                    var lstCommitteeMaster = (from row in entities.BM_Directors_DetailsOfCommiteePosition
                                              where row.EntityId == entityID 
                                              && row.IsDeleted == false
                                              select row.Committee_Id).Distinct().ToList();

                    if (lstCommitteeMaster != null)
                    {
                        var lstCommittees = (from row in meetingTypes
                                             join rows in lstCommitteeMaster on row.Id equals rows
                                             select new CommitteeCompVM()
                                             {
                                                 Id = row.Id,
                                                 Name = row.Name,
                                                 MeetingTypeName = row.MeetingTypeName,
                                                 IsMeeting = row.IsMeeting,
                                                 IsCircular = row.IsCircular,
                                                 CompositionType = row.CompositionType
                                             });

                        if (Type.Trim().ToUpper().Equals("M"))
                        {
                            result.AddRange(lstCommittees.Where(k => k.CompositionType == "C" && k.IsMeeting == true));
                        }
                        else
                        {
                            result.AddRange(lstCommittees.Where(k => k.CompositionType == "C" && k.IsCircular == true));
                        }
                    }
                }

                return result;
            }
        }

        public List<CommitteeCompVM> GetCommitteeCompEntitywise(long entityId, int customerId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                //var result = (from row in entities.BM_SP_GetCommitteeOrCompositionEntitywise(Convert.ToInt32(entityId), customerId, -1)                              
                //              select new CommitteeCompVM
                //              {
                //                  Id = row.Committee_Id,
                //                  Name = row.Name,
                //                  //Description_ = row.Description_
                //              }).ToList();

                //if (result != null)
                //{
                //    result = result.Distinct().ToList();
                //}

                //return result;

                var result = (from row in entities.BM_Directors_DetailsOfCommiteePosition
                              join row_e in entities.BM_EntityMaster on row.EntityId equals row_e.Id
                              join committee in entities.BM_CommitteeComp on row.Committee_Id equals committee.Id
                              where row_e.Customer_Id == customerId
                              && row.EntityId == entityId
                              && row_e.Is_Deleted == false
                              && row.IsDeleted == false
                              orderby committee.Name
                              select new CommitteeCompVM
                              {
                                  Id = committee.Id,
                                  Name = committee.Name,
                                  Description_ = committee.Description_
                              }).Distinct().ToList();

                return result;

                //var result = (from director in entities.BM_DirectorMaster
                //              join row in entities.BM_Directors_DetailsOfCommiteePosition on director.Id equals row.Director_Id
                //              join row_e in entities.BM_EntityMaster on row.EntityId equals row_e.Id
                //              join committee in entities.BM_CommitteeComp on row.Committee_Id equals committee.Id
                //              where director.Customer_Id == customerId && row.EntityId== entityId
                //              && director.Is_Deleted == false && row.IsDeleted == false
                //              orderby committee.Name
                //              select new CommitteeCompVM
                //              {
                //                Id=committee.Id,
                //                Name=committee.Name,
                //                Description_=committee.Description_
                //              }
                //              ).ToList();
                //return result;
            }
        }
    }
}