﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using BM_ManegmentServices.VM;
using System.Data.Entity.SqlServer;
using System.Reflection;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text;
using System.IO;
using System.Web.Hosting;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using BM_ManegmentServices.Services.Setting;
using Ionic.Zip;
using System.Net;
using OfficeOpenXml;
using System.Globalization;

namespace BM_ManegmentServices.Services.Masters
{
    public class EntityMaster : ExcelsheetData, IEntityMaster
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        ISettingService objISettingService;
        EntityMasterVM obj = new EntityMasterVM();
        IDirectorMaster _objdirector;
        IShareholdingMaster objIShareholdingMaster;
        bool CheckCustomerBranchLimit = true;
        int? serviceProviderId = 0;

        const int TwentyFiveMB = 25 * 1024 * 1024;

        public EntityMaster(ISettingService objSettingService, IDirectorMaster _objdirector, IShareholdingMaster objShareholdingMaster)
        {
            objISettingService = objSettingService;
            this._objdirector = _objdirector;
            objIShareholdingMaster = objShareholdingMaster;
        }
        public List<Customer_VM> GetCustomers()
        {
            return (from row in entities.Customers
                    orderby row.Name ascending
                    select new Customer_VM
                    {
                        CustomerId = row.ID,
                        Name = row.Name
                    }).ToList();
        }

        public string GetEntityName(int EntityID, int CustomerID)
        {
            return (from row in entities.BM_EntityMaster
                    where row.Id == EntityID
                    && row.Customer_Id == CustomerID
                    && row.Is_Deleted == false
                    select row.CompanyName.ToUpper()).FirstOrDefault();
        }

        public string AddEntityMasterdtls(EntityMasterVM _objentity, int CustomerId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    return "Entity Saved Successfully.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Something went wrong.";
            }
        }

        public void Dispose()
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                entities.Dispose();
            }
        }

        #region Role based entity Details
        public List<EntityMasterVM> GetAllEntityMaster(int customerID)
        {
            try
            {
                var customerBranches = (from row in entities.BM_EntityMaster
                                        join rows in entities.BM_EntityType
                                        on row.Entity_Type equals (rows.Id)
                                        where row.Is_Deleted == false
                                        && row.Customer_Id == customerID
                                        orderby row.CompanyName ascending
                                        select new EntityMasterVM
                                        {
                                            Id = row.Id,
                                            EntityName = row.CompanyName.ToUpper(),
                                            Type = rows.EntityName,
                                            LLPI_CIN = row.CIN_LLPIN != "0" ? row.CIN_LLPIN : row.Registration_No,
                                            RegistrationNO = row.Registration_No,
                                            CIN = row.CIN_LLPIN,
                                            Entity_Type = row.Entity_Type,
                                            PAN = row.PAN,
                                            Islisted = row.IS_Listed,
                                        });
                return customerBranches.ToList();
            }
            catch (Exception ex)
            {
                List<EntityMasterVM> obj = new List<EntityMasterVM>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;

            }
        }

        public List<EntityMasterVM> GetAllEntityMaster(int customerID, int userID, string role)
        {

            List<EntityMasterVM> _objentity = new List<EntityMasterVM>();
            try
            {
                if (role == SecretarialConst.Roles.CS)
                {
                    _objentity = (from row in entities.BM_EntityMaster
                                  join rows in entities.BM_EntityType on row.Entity_Type equals (rows.Id)
                                  join view in entities.BM_AssignedEntitiesView on row.Id equals view.EntityId
                                  where row.Is_Deleted == false
                                  && row.Customer_Id == customerID
                                  && view.userId == userID
                                  orderby row.CompanyName ascending
                                  select new EntityMasterVM
                                  {
                                      Id = row.Id,
                                      EntityName = row.CompanyName.ToUpper(),
                                      Type = rows.EntityName,
                                      LLPI_CIN = row.CIN_LLPIN != "" ? row.CIN_LLPIN : row.Registration_No,
                                      RegistrationNO = row.Registration_No,
                                      CIN = row.CIN_LLPIN,
                                      Entity_Type = row.Entity_Type,
                                      PAN = row.PAN,
                                      Islisted = row.IS_Listed,
                                      CustomerId = row.Customer_Id,
                                  }).ToList();
                }
                else
                {
                    _objentity = (from row in entities.BM_EntityMaster
                                  join rows in entities.BM_EntityType
                                  on row.Entity_Type equals (rows.Id)
                                  where row.Is_Deleted == false
                                  && row.Customer_Id == customerID
                                  orderby row.CompanyName ascending
                                  select new EntityMasterVM
                                  {
                                      Id = row.Id,
                                      EntityName = row.CompanyName.ToUpper(),
                                      Type = rows.EntityName,
                                      LLPI_CIN = row.CIN_LLPIN != "" ? row.CIN_LLPIN : row.Registration_No,
                                      RegistrationNO = row.Registration_No,
                                      CIN = row.CIN_LLPIN,
                                      Entity_Type = row.Entity_Type,
                                      PAN = row.PAN,
                                      Islisted = row.IS_Listed,
                                      CustomerId = row.Customer_Id,
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objentity;
        }

        public List<EntityMasterVM> GetEntityForTask(int customerID, int userID, string role)
        {
            List<EntityMasterVM> objentitydetails = new List<EntityMasterVM>();
            try
            {
                if (role == SecretarialConst.Roles.CS)
                {
                    objentitydetails = (from row in entities.BM_SP_EntityListforTaskUser(userID, customerID)
                                        orderby row.CompanyName ascending
                                        select new EntityMasterVM
                                        {
                                            EntityName = row.CompanyName.ToUpper(),
                                            Id = row.Id
                                        }).ToList();
                }
                else if (role == SecretarialConst.Roles.DRCTR || role == SecretarialConst.Roles.SMNGT)
                {
                    objentitydetails = (from row in entities.BM_SP_EntityListforBOD(userID, customerID)
                                        orderby row.CompanyName ascending
                                        select new EntityMasterVM
                                        {
                                            EntityName = row.CompanyName.ToUpper(),
                                            Id = row.ID
                                        }).ToList();
                }
                else if (role == SecretarialConst.Roles.HDCS)
                {
                    objentitydetails = (from row in entities.BM_EntityMaster
                                        where row.Customer_Id == customerID
                                        orderby row.CompanyName ascending
                                        select new EntityMasterVM
                                        {
                                            EntityName = row.CompanyName.ToUpper(),
                                            Id = row.Id
                                        }).ToList();
                }
                return objentitydetails;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return objentitydetails;
            }
        }

        //public List<EntityMasterVM> GetEntityForTask(int customerID, int userID, string role)
        //{
        //    List<EntityMasterVM> objentitydetails = new List<EntityMasterVM>();
        //    try
        //    {
        //        if (role == SecretarialConst.Roles.CS || role == SecretarialConst.Roles.HDCS)
        //        {
        //            objentitydetails = (from row in entities.BM_SP_EntityListforTaskUser(userID, customerID)
        //                                select new EntityMasterVM
        //                                {
        //                                    EntityName = row.CompanyName,
        //                                    Id = row.Id
        //                                }).ToList();

        //        }
        //        else if (role == SecretarialConst.Roles.DRCTR)
        //        {
        //            objentitydetails = (from row in entities.BM_SP_EntityListforBOD(userID, customerID)
        //                                select new EntityMasterVM
        //                                {
        //                                    EntityName = row.CompanyName,
        //                                    Id = row.ID
        //                                }).ToList();
        //        }
        //        return objentitydetails;
        //    }
        //    catch (Exception ex)
        //    {

        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return objentitydetails;
        //    }
        //}

        public List<EntityMasterVM> GetAllEntityMasterForMeeting(int customerID)
        {
            try
            {
                var customerBranches = (from row in entities.BM_EntityMaster
                                        join rows in entities.BM_EntityType
                                        on row.Entity_Type equals (rows.Id)
                                        where row.Is_Deleted == false
                                        && row.Customer_Id == customerID
                                        && rows.IsForMeeting == true
                                        orderby row.CompanyName ascending
                                        select new EntityMasterVM
                                        {
                                            Id = row.Id,
                                            EntityName = row.CompanyName.ToUpper(),
                                            Type = rows.EntityName,
                                            LLPI_CIN = row.CIN_LLPIN != "0" ? row.CIN_LLPIN : row.Registration_No,
                                            RegistrationNO = row.Registration_No,
                                            CIN = row.CIN_LLPIN,
                                            Entity_Type = row.Entity_Type,
                                            PAN = row.PAN,
                                            Islisted = row.IS_Listed,
                                        });
                return customerBranches.ToList();
            }
            catch (Exception ex)
            {
                List<EntityMasterVM> obj = new List<EntityMasterVM>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;

            }
        }
        public List<EntityMasterVM> GetAllEntityMasterForMeeting(int customerID, int userID, string role)
        {
            try
            {
                if (role == SecretarialConst.Roles.CS)
                {
                    return (from row in entities.BM_EntityMaster
                            join rows in entities.BM_EntityType on row.Entity_Type equals (rows.Id)
                            join view in entities.BM_AssignedEntitiesView on row.Id equals view.EntityId
                            where row.Is_Deleted == false
                            && row.Customer_Id == customerID
                            && rows.IsForMeeting == true
                            && view.userId == userID
                            orderby row.CompanyName ascending
                            select new EntityMasterVM
                            {
                                Id = row.Id,
                                Entity_Type = row.Entity_Type,
                                EntityName = row.CompanyName.ToUpper(),
                                Type = rows.EntityName,
                                Islisted = row.IS_Listed,
                                DefaultVirtualMeeting = false //row.DefaultVirtualMeeting
                            }).ToList();
                }
                else
                {
                    return (from row in entities.BM_EntityMaster
                            join rows in entities.BM_EntityType
                            on row.Entity_Type equals (rows.Id)
                            where row.Is_Deleted == false
                            && row.Customer_Id == customerID
                            && rows.IsForMeeting == true
                            orderby row.CompanyName ascending
                            select new EntityMasterVM
                            {
                                Id = row.Id,
                                EntityName = row.CompanyName.ToUpper(),
                                Type = rows.EntityName,
                                LLPI_CIN = row.CIN_LLPIN != "0" ? row.CIN_LLPIN : row.Registration_No,
                                RegistrationNO = row.Registration_No,
                                CIN = row.CIN_LLPIN,
                                Entity_Type = row.Entity_Type,
                                PAN = row.PAN,
                                Islisted = row.IS_Listed,
                                DefaultVirtualMeeting = false //row.DefaultVirtualMeeting
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                List<EntityMasterVM> obj = new List<EntityMasterVM>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }
        public List<EntityMasterVM> GetAllEntityMasterForMeetingNew(int customerID, int userID, string role)
        {
            try
            {
                var result = GetAllEntityMasterForMeeting(customerID, userID, role);
                //var accessToDirector = objISettingService.GetAccessToDirector(customerID);
                var defaultVirtualMeeting = objISettingService.CheckDefaultVirtualMeeting(customerID);

                if (result != null && defaultVirtualMeeting)
                {
                    result.ForEach(k => k.DefaultVirtualMeeting = true);
                }

                if (result.Count > 0)
                    result = result.OrderBy(row => row.EntityName).ToList();

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new List<EntityMasterVM>();
            }
        }
        #endregion

        public List<EntityAddressVM> GetEntityAddressForMeeting(int Entity_Id, int customerID)
        {
            List<EntityAddressVM> obj = new List<EntityAddressVM>();
            try
            {
                var entity = (from row in entities.BM_EntityMaster
                              from city in entities.Cities.Where(k => k.ID == row.Regi_CityId).DefaultIfEmpty()
                              from state in entities.States.Where(k => k.ID == row.Regi_StateId).DefaultIfEmpty()
                              from corp_city in entities.Cities.Where(k => k.ID == row.Corp_CityId).DefaultIfEmpty()
                              from corp_state in entities.States.Where(k => k.ID == row.Corp_StateId).DefaultIfEmpty()
                              where row.Id == Entity_Id
                              && row.Is_Deleted == false
                              select new
                              {
                                  row.Regi_Address_Line1,
                                  row.Regi_Address_Line2,
                                  Regi_City = city.Name,
                                  Regi_State = state.Name,
                                  row.Regi_PINCode,

                                  row.IsCorp_Office,

                                  row.Corp_Address_Line1,
                                  row.Corp_Address_Line2,
                                  Corp_City = corp_city.Name,
                                  Corp_State = corp_state.Name,
                                  row.Corp_PINCode,
                              }).FirstOrDefault();

                if (entity != null)
                {
                    obj = (from row in entities.BM_SP_GetAllMeetingVenue(Entity_Id, customerID)
                           select new EntityAddressVM
                           {
                               EntityId = Entity_Id,
                               EntityAddressType = row.ID,
                               EntityAddressTypeName = row.MeetingVenue,
                               EntityAddress = row.MeetingVenue
                           }).ToList();

                    if (obj == null)
                    {
                        obj = new List<EntityAddressVM>();
                    }

                    #region Registered Office
                    var cityName = "";
                    if (!string.IsNullOrWhiteSpace(entity.Regi_City))
                    {
                        cityName = ", " + entity.Regi_City;
                    }

                    var stateName = "";
                    if (!string.IsNullOrWhiteSpace(entity.Regi_State))
                    {
                        stateName = ", " + entity.Regi_State;
                    }
                    var pinCode = "";
                    if (!string.IsNullOrEmpty(entity.Regi_PINCode))
                    {
                        pinCode = " - " + entity.Regi_PINCode;
                    }

                    var item = new EntityAddressVM() { EntityId = Entity_Id, EntityAddressType = "R", EntityAddressTypeName = "Registered Office", EntityAddress = entity.Regi_Address_Line1 + " " + entity.Regi_Address_Line2 + cityName + stateName + pinCode };
                    obj.Insert(0, item);
                    #endregion

                    #region Co-Office
                    if (entity.IsCorp_Office)
                    {
                        cityName = "";
                        if (!string.IsNullOrWhiteSpace(entity.Corp_City))
                        {
                            cityName = ", " + entity.Corp_City;
                        }

                        stateName = "";
                        if (!string.IsNullOrWhiteSpace(entity.Corp_State))
                        {
                            stateName = ", " + entity.Corp_State;
                        }
                        pinCode = "";
                        if (!string.IsNullOrEmpty(entity.Corp_PINCode))
                        {
                            pinCode = " - " + entity.Regi_PINCode;
                        }

                        item = new EntityAddressVM() { EntityId = Entity_Id, EntityAddressType = "C", EntityAddressTypeName = "Corporate Office", EntityAddress = entity.Corp_Address_Line1 + " " + entity.Corp_Address_Line2 + cityName + stateName + pinCode };
                        obj.Insert(1, item);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            obj.Add(new EntityAddressVM() { EntityId = Entity_Id, EntityAddressType = "O", EntityAddressTypeName = "Other", EntityAddress = "" });
            return obj;
        }

        public IEnumerable<BM_StockExchange> GetAllStockExchange(bool addNew)
        {
            try
            {

                var ListofStockExchange = (from row in entities.BM_StockExchange
                                           where row.isActive == true
                                           orderby row.Name ascending
                                           select row).ToList();
                if (ListofStockExchange == null)
                {
                    ListofStockExchange = new List<BM_StockExchange>();
                }



                return ListofStockExchange;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_BusinessActivity> GetBusinessActivityCode()
        {
            try
            {

                var ListofBusinessCode = (from row in entities.BM_BusinessActivity.OrderBy(x => x.Description) select row).ToList();

                return ListofBusinessCode;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<VMCity> GetCityList(int? stateId)
        {
            try
            {
                var ListofCity = (dynamic)null;
                if (stateId != null && stateId > 0)
                {

                    ListofCity = (from row in entities.Cities.OrderBy(x => x.Name)
                                  where row.StateId == stateId
                                  select new VMCity
                                  {
                                      Reg_cityId = row.ID,
                                      Name = row.Name,
                                  }).ToList();
                    if (ListofCity == null)
                    {
                        ListofCity = new List<VMCity>();
                    }

                    ListofCity.Insert(0, new VMCity() { Reg_cityId = 0, Name = "NA" });
                    return ListofCity;
                }

                else
                {
                    if (ListofCity == null)
                    {
                        ListofCity = new List<VMCity>();
                    }

                    ListofCity.Insert(0, new VMCity() { Reg_cityId = 0, Name = "NA" });
                    return ListofCity;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_CompanyCategory> GetCompanyCategory()
        {
            try
            {

                var ListofCompCategory = (from row in entities.BM_CompanyCategory.OrderBy(x => x.Category)
                                          select row).ToList();
                if (ListofCompCategory == null)
                {
                    ListofCompCategory = new List<BM_CompanyCategory>();
                }

                ListofCompCategory.Insert(0, new BM_CompanyCategory() { Id = 0, Category = "Select" });
                return ListofCompCategory;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_EntityType> GetCompanyType()
        {
            try
            {

                var ListofCompType = (from row in entities.BM_EntityType
                                      where row.ForEntityMaster == true
                                      orderby row.EntityName
                                      select row
                                      ).ToList();
                if (ListofCompType == null)
                {
                    ListofCompType = new List<BM_EntityType>();
                }

                ListofCompType.Insert(0, new BM_EntityType() { Id = 0, EntityName = "Select" });
                return ListofCompType;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<EntityTypeVM> GetCompanyTypeVM()
        {
            try
            {

                var ListofCompType = (from row in entities.BM_EntityType
                                      where row.ForEntityMaster == true
                                      orderby row.srno
                                      select new EntityTypeVM
                                      {
                                          EntityTypeId = row.Id,
                                          EntityTypeName = row.EntityName
                                      }).ToList();
                if (ListofCompType == null)
                {
                    ListofCompType = new List<EntityTypeVM>();
                }


                //ListofCompType.Insert(0, new EntityTypeVM() { EntityTypeId = 0, EntityTypeName = "All" });
                return ListofCompType;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<EntityTypeVM> EntityTypeVMForMeeting()
        {
            try
            {

                var ListofCompType = (from row in entities.BM_EntityType
                                      where row.IsForMeeting == true
                                      orderby row.EntityName
                                      select new EntityTypeVM
                                      {
                                          EntityTypeId = row.Id,
                                          EntityTypeName = row.EntityName.ToUpper()
                                      }).ToList();
                if (ListofCompType == null)
                {
                    ListofCompType = new List<EntityTypeVM>();
                }
                return ListofCompType;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_ROC_code> GetRocCode()
        {
            try
            {

                var ListofRocCode = (from row in entities.BM_ROC_code.OrderBy(x => x.Name)
                                     where row.isDeleted == false
                                     select row).ToList();
                if (ListofRocCode == null)
                {
                    ListofRocCode = new List<BM_ROC_code>();
                }

                ListofRocCode.Insert(0, new BM_ROC_code() { Id = 0, Name = "Select" });
                return ListofRocCode;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<VMState> GetStateList()
        {
            try
            {
                List<VMState> Listofstate = new List<VMState>();

                Listofstate = (from row in entities.States.Where(row => row.CountryID == 1 && row.IsDeleted == false).OrderBy(x => x.Name)
                               select new VMState
                               {
                                   SIdPublic = row.ID,
                                   Name = row.Name
                               }).ToList();

                Listofstate.Insert(0, new VMState() { SIdPublic = 0, Name = "Select State" });
                return Listofstate;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public string UpdateEntity(EntityMasterVM _objentity)
        {
            try
            {
                var _objUpdatebyType = (from row in entities.BM_EntityMaster
                                        where row.CompanyCategory_Id == _objentity.Entity_Type
                                        && row.Is_Deleted == false
                                        select row).FirstOrDefault();

                if (_objUpdatebyType != null)
                {
                    if (_objUpdatebyType.CompanyCategory_Id == 1)
                    {
                        return "Updated successfully";
                    }
                }

                return "Updated successfully";
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Something went Wrong";
            }
        }

        public IEnumerable<BM_BusinessActivity> GetBusinessActivDesc(int businessId)
        {
            try
            {
                if (businessId > 0)
                {
                    var objgetBADesc = (from row in entities.BM_BusinessActivity.OrderBy(x => x.Description)
                                        where row.Id == businessId
                                        select row).ToList();

                    return objgetBADesc;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<EntityMasterVM> GetAllEntityMasterForCommittee(long Director_Id)
        {
            var customerBranches = (from row in entities.BM_EntityMaster
                                    join rows in entities.BM_Directors_DetailsOfInterest
                                    on row.Id equals (rows.EntityId)
                                    where rows.Director_Id == Director_Id
                                    && row.Is_Deleted == false
                                    && rows.IsDeleted == false
                                    && rows.IsActive == true
                                    && (rows.NatureOfInterest == 2 || rows.NatureOfInterest == 9 || rows.NatureOfInterest == 10)
                                    select new EntityMasterVM
                                    {
                                        Id = row.Id,
                                        EntityName = row.CompanyName.ToUpper()
                                    });
            return customerBranches.ToList();

        }

        public Pravate_PublicVM CreatePublicPrivate(Pravate_PublicVM _objentity, int customerId, int UserId)
        {
            try
            {
                bool checklogo = CheckCompanyLogo(_objentity.files);
                if (checklogo)
                {
                    #region Upper Case
                    try
                    {
                        if (_objentity.EntityName != null)
                        {
                            _objentity.EntityName = _objentity.EntityName.ToUpper();
                        }

                        if (_objentity.CIN != null)
                        {
                            _objentity.CIN = _objentity.CIN.ToUpper();
                        }

                        if (_objentity.PAN != null)
                        {
                            _objentity.PAN = _objentity.PAN.ToUpper();
                        }

                        if (_objentity.GST != null)
                        {
                            _objentity.GST = _objentity.GST.ToUpper();
                        }

                        if (_objentity.Regi_Address_Line1 != null)
                        {
                            _objentity.Regi_Address_Line1 = _objentity.Regi_Address_Line1.ToUpper();
                        }

                        if (_objentity.Regi_Address_Line2 != null)
                        {
                            _objentity.Regi_Address_Line2 = _objentity.Regi_Address_Line2.ToUpper();
                        }

                        if (_objentity.Corp_Address_Line1 != null)
                        {
                            _objentity.Corp_Address_Line1 = _objentity.Corp_Address_Line1.ToUpper();
                        }

                        if (_objentity.Corp_Address_Line2 != null)
                        {
                            _objentity.Corp_Address_Line2 = _objentity.Corp_Address_Line2.ToUpper();
                        }


                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    #endregion

                    string Message = "";
                    #region Check EntityFor Avacom 
                    com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                    {
                        ID = _objentity.CustomerBranchId,
                        Name = _objentity.EntityName,
                        //Type = Convert.ToByte(ddlType.SelectedValue),
                        //ComType = Convert.ToByte(comType),
                        AddressLine1 = _objentity.Regi_Address_Line1,
                        AddressLine2 = _objentity.Regi_Address_Line2,
                        StateID = _objentity.Regi_StateId,
                        CityID = _objentity.Regi_CityId,
                        Others = "",
                        PinCode = _objentity.Regi_PINCode,
                        ContactPerson = "",
                        Landline = "",
                        Mobile = "",
                        EmailID = _objentity.Email_Id,
                        CustomerID = customerId,
                        //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, _objentity.IS_Listed, out Message);
                    #endregion

                    if (customerBranch != null)
                    {
                        if (customerBranch.ID > 0)
                        {
                            #region Check Entity For AvaSec if Customer Branch Id is greater then zero
                            try
                            {

                                var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                        where row.Is_Deleted == false
                                                        && row.CIN_LLPIN == _objentity.CIN
                                                        && row.Customer_Id == customerId
                                                        select row).FirstOrDefault();
                                if (_objCreateEntity == null)
                                {
                                    BM_EntityMaster _bmentity = new BM_EntityMaster();


                                    if (_objentity.Entity_Type == 1 || _objentity.Entity_Type == 2 || _objentity.Entity_Type == 9 || _objentity.Entity_Type == 10)
                                    {

                                        #region Assign Values
                                        if (!(string.IsNullOrEmpty(_objentity.EntityName)))
                                            _bmentity.CompanyName = _objentity.EntityName.ToUpper();
                                        if (_objentity.Entity_Type > 0)
                                            _bmentity.Entity_Type = _objentity.Entity_Type;
                                        if (!string.IsNullOrEmpty(_objentity.CIN))
                                            _bmentity.CIN_LLPIN = _objentity.CIN;
                                        if (!string.IsNullOrEmpty(_objentity.GLN))
                                        {
                                            _bmentity.GLN = _objentity.GLN;
                                        }
                                        else
                                        {
                                            _bmentity.GLN = "";
                                        }
                                        if (!string.IsNullOrEmpty(_objentity.FY))
                                            _bmentity.FY_CY = _objentity.FY;
                                        if (_objentity.IncorporationDate != null)
                                            _bmentity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.IncorporationDate);
                                        if (!string.IsNullOrEmpty(_objentity.Regi_Address_Line1))
                                            _bmentity.Regi_Address_Line1 = _objentity.Regi_Address_Line1;
                                        if (!string.IsNullOrEmpty(_objentity.Regi_Address_Line2))
                                        {
                                            _bmentity.Regi_Address_Line2 = _objentity.Regi_Address_Line2;
                                        }
                                        else
                                        {
                                            _bmentity.Regi_Address_Line2 = "";
                                        }
                                        if (_objentity.Regi_StateId > 0)
                                            _bmentity.Regi_StateId = _objentity.Regi_StateId;
                                        if (_objentity.Regi_CityId > 0)
                                            _bmentity.Regi_CityId = _objentity.Regi_CityId;
                                        if (!string.IsNullOrEmpty(_objentity.Regi_PINCode))
                                            _bmentity.Regi_PINCode = _objentity.Regi_PINCode;
                                        if (_objentity.ROC_Code > 0)
                                            _bmentity.ROC_Code = _objentity.ROC_Code;
                                        if (_objentity.IsCorp_Office)
                                        {
                                            if (!string.IsNullOrEmpty(_objentity.Corp_Address_Line1))
                                                _bmentity.Corp_Address_Line1 = _objentity.Corp_Address_Line1;
                                            if (!string.IsNullOrEmpty(_objentity.Corp_Address_Line2))
                                                _bmentity.Corp_Address_Line2 = _objentity.Corp_Address_Line2;
                                            if (_objentity.Corp_StateId > 0)
                                                _bmentity.Corp_StateId = _objentity.Corp_StateId;
                                            if (_objentity.Corp_CityId > 0)
                                                _bmentity.Corp_CityId = _objentity.Corp_CityId;
                                            if (!string.IsNullOrEmpty(_objentity.Corp_PINCode))
                                                _bmentity.Corp_PINCode = _objentity.Corp_PINCode;
                                            _bmentity.IsCorp_Office = _objentity.IsCorp_Office;
                                            _bmentity.IsCorporateSameAddress = _objentity.Is_SameAddress;
                                        }
                                        else
                                        {
                                            _bmentity.Corp_Address_Line1 = "";
                                            _bmentity.Corp_Address_Line2 = "";
                                            _bmentity.Corp_StateId = 0;
                                            _bmentity.Corp_CityId = 0;
                                            _bmentity.Corp_PINCode = "";
                                            _bmentity.IsCorp_Office = _objentity.IsCorp_Office;
                                            _bmentity.IsCorporateSameAddress = _objentity.Is_SameAddress;
                                        }
                                        if (!string.IsNullOrEmpty(_objentity.Registration_No))
                                            _bmentity.Registration_No = _objentity.Registration_No;
                                        if (!string.IsNullOrEmpty(_objentity.Registration_No))
                                            _bmentity.Registration_No = _objentity.Registration_No;
                                        if (!string.IsNullOrEmpty(_objentity.Registration_No))
                                            _bmentity.Registration_No = _objentity.Registration_No;
                                        if (!string.IsNullOrEmpty(_objentity.Email_Id))
                                            _bmentity.Email_Id = _objentity.Email_Id;
                                        if (!string.IsNullOrEmpty(_objentity.PAN))
                                            _bmentity.PAN = _objentity.PAN;
                                        if (!string.IsNullOrEmpty(_objentity.WebSite))
                                            _bmentity.WebSite = _objentity.WebSite;
                                        if (!string.IsNullOrEmpty(_objentity.GST))
                                            _bmentity.GST = _objentity.GST;
                                        if (!string.IsNullOrEmpty(_objentity.GST))
                                            _bmentity.GST = _objentity.GST;
                                        if (_objentity.CompanyCategory_Id > 0)
                                            _bmentity.CompanyCategory_Id = _objentity.CompanyCategory_Id;
                                        if (_objentity.NICCode > 0)
                                        {
                                            _bmentity.NIC_Code = _objentity.NICCode;
                                        }
                                        else
                                        {
                                            _bmentity.NIC_Code = 0;
                                        }
                                        if (!string.IsNullOrEmpty(_objentity.Description))
                                        {
                                            _bmentity.NIC_Discription = _objentity.Description;
                                        }
                                        else
                                        {
                                            _bmentity.NIC_Discription = "";
                                        }
                                        _bmentity.IS_Listed = _objentity.IS_Listed;
                                        if (_objentity.IS_Listed)
                                        {
                                            _bmentity.ISIN = _objentity.ISIN;
                                            _bmentity.RTA_CIN = _objentity.RTA_CIN;
                                            _bmentity.RTA_Address = _objentity.RTA_Address;
                                            _bmentity.RTA_CompanyName = _objentity.RTA_Name;
                                        }
                                        else
                                        {
                                            _bmentity.ISIN = "";
                                        }
                                        _bmentity.CompanySubCategory = _objentity.CompanyCategorySub_Id;
                                        _bmentity.Is_Deleted = false;
                                        _bmentity.Customer_Id = customerId;
                                        _bmentity.CustomerBranchId = customerBranch.ID;
                                        _bmentity.CreatedBy = UserId;
                                        _bmentity.CreatedOn = DateTime.Now;

                                        _bmentity.IsCompanyBranch = _objentity.IsanyBranch;
                                        _bmentity.OthersCompanyDetails = _objentity.OthersDetails;
                                        #endregion


                                        entities.BM_EntityMaster.Add(_bmentity);
                                        entities.SaveChanges();
                                        _objentity.Id = _bmentity.Id;

                                        #region Upload Company logo
                                        if (_objentity.files != null && _objentity.Id > 0)
                                        {
                                            if (_objentity.files.Count() > 0)
                                            {
                                                bool uploadlogo = FileUpload.SaveEntityLogo(_objentity.files, _objentity.Id, customerId, UserId);
                                            }
                                        }
                                        #endregion


                                        #region Regulatory Autority
                                        if (_objentity.RegulatoryAuthorityId != null)
                                        {
                                            if (_objentity.RegulatoryAuthorityId.Count > 0)
                                            {
                                                foreach (var reg in _objentity.RegulatoryAuthorityId)
                                                {
                                                    var checkregulatoryId = (from x in entities.BM_EntitySubIndustryMapping
                                                                             where x.EntityId == _bmentity.Id
                                                                             && x.SubIndustryID == reg
                                                                             select x).FirstOrDefault();
                                                    if (checkregulatoryId == null)
                                                    {
                                                        BM_EntitySubIndustryMapping _objautority = new BM_EntitySubIndustryMapping();
                                                        _objautority.EntityId = _objentity.Id;
                                                        _objautority.SubIndustryID = (int)reg;
                                                        _objautority.IsActive = true;
                                                        _objautority.Createdon = DateTime.Now;
                                                        _objautority.Createdby = UserId;
                                                        entities.BM_EntitySubIndustryMapping.Add(_objautority);
                                                        entities.SaveChanges();
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        _objentity.Message = true;
                                        _objentity.successErrorMessage = "Entity Saved Successfully";

                                    }
                                    else
                                    {
                                        _objentity.errorMessage = true;
                                        _objentity.successErrorMessage = "Please Select Entity Type";
                                        return _objentity;
                                    }
                                }
                                else
                                {
                                    _objentity.errorMessage = true;
                                    _objentity.successErrorMessage = "Entity/Company with same CIN already exists";
                                    return _objentity;
                                }
                            }
                            catch (Exception ex)
                            {

                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Server Error Occur";
                                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                return _objentity;
                            }
                            #endregion
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong !Please Check Customer Branch ";

                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong !Please Check Customer Branch ";

                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Uploaded Logo size should not be greater than 25MB and allowed file extensions are .png, .jpeg, .jpg, .gif";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.serverError;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objentity;
            }
            return _objentity;
        }

        public Pravate_PublicVM UpdatePublicPrivateEntity(Pravate_PublicVM _objentity, int customerId, int UserId)
        {
            try
            {
                bool checklogo = CheckCompanyLogo(_objentity.files);
                if (checklogo)
                {

                    string Message = "";
                    var checkForEntity = (from row in entities.BM_EntityMaster where row.Id == _objentity.Id && row.Is_Deleted == false select row).FirstOrDefault();

                    if (checkForEntity != null)
                    {
                        if (_objentity.Entity_Type == 1 || _objentity.Entity_Type == 2 || _objentity.Entity_Type == 9 || _objentity.Entity_Type == 10)
                        {
                            #region Check customer branch ID
                            if (checkForEntity.CustomerBranchId == null || checkForEntity.CustomerBranchId == 0)
                            {
                                #region Check EntityFor Avacom 
                                com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                                {
                                    ID = _objentity.CustomerBranchId,
                                    Name = _objentity.EntityName,
                                    //Type = Convert.ToByte(ddlType.SelectedValue),
                                    //ComType = Convert.ToByte(comType),
                                    AddressLine1 = _objentity.Regi_Address_Line1,
                                    AddressLine2 = _objentity.Regi_Address_Line2,
                                    StateID = _objentity.Regi_StateId,
                                    CityID = _objentity.Regi_CityId,
                                    Others = "",
                                    PinCode = _objentity.Regi_PINCode,
                                    ContactPerson = "",
                                    Landline = "",
                                    Mobile = "",
                                    EmailID = _objentity.Email_Id,
                                    CustomerID = customerId,
                                    //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                                    //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                                };

                                customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, _objentity.IS_Listed, out Message);
                                #endregion

                                if (customerBranch.ID > 0)
                                {
                                    checkForEntity.CustomerBranchId = customerBranch.ID;
                                }
                                else
                                {
                                    _objentity.errorMessage = true;
                                    _objentity.successErrorMessage = "Something went wrong";
                                    return _objentity;
                                }
                            }
                            #endregion

                            #region Assign Values

                            checkForEntity.CompanyName = _objentity.EntityName.ToUpper();

                            checkForEntity.Entity_Type = _objentity.Entity_Type;

                            checkForEntity.CIN_LLPIN = _objentity.CIN;
                            if (!string.IsNullOrEmpty(_objentity.GLN))
                            {
                                checkForEntity.GLN = _objentity.GLN;
                            }
                            else
                            {
                                checkForEntity.GLN = "";
                            }
                            if (!string.IsNullOrEmpty(_objentity.FY))
                                checkForEntity.FY_CY = _objentity.FY;
                            if (_objentity.IncorporationDate != null)
                                checkForEntity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.IncorporationDate);
                            if (!string.IsNullOrEmpty(_objentity.Regi_Address_Line1))
                                checkForEntity.Regi_Address_Line1 = _objentity.Regi_Address_Line1;
                            if (!string.IsNullOrEmpty(_objentity.Regi_Address_Line2))
                            {
                                checkForEntity.Regi_Address_Line2 = _objentity.Regi_Address_Line2;
                            }
                            else
                            {
                                checkForEntity.Regi_Address_Line2 = "";
                            }
                            if (_objentity.Regi_StateId > 0)
                                checkForEntity.Regi_StateId = _objentity.Regi_StateId;
                            if (_objentity.Regi_CityId > 0)
                                checkForEntity.Regi_CityId = _objentity.Regi_CityId;
                            if (!string.IsNullOrEmpty(_objentity.Regi_PINCode))
                                checkForEntity.Regi_PINCode = _objentity.Regi_PINCode;
                            if (_objentity.ROC_Code > 0)
                                checkForEntity.ROC_Code = _objentity.ROC_Code;
                            if (_objentity.IsCorp_Office)
                            {
                                if (!string.IsNullOrEmpty(_objentity.Corp_Address_Line1))
                                    checkForEntity.Corp_Address_Line1 = _objentity.Corp_Address_Line1;
                                if (!string.IsNullOrEmpty(_objentity.Corp_Address_Line2))
                                    checkForEntity.Corp_Address_Line2 = _objentity.Corp_Address_Line2;
                                if (_objentity.Corp_StateId > 0)
                                    checkForEntity.Corp_StateId = _objentity.Corp_StateId;
                                if (_objentity.Corp_CityId > 0)
                                    checkForEntity.Corp_CityId = _objentity.Corp_CityId;
                                if (!string.IsNullOrEmpty(_objentity.Corp_PINCode))
                                    checkForEntity.Corp_PINCode = _objentity.Corp_PINCode;
                                checkForEntity.IsCorp_Office = _objentity.IsCorp_Office;
                                checkForEntity.IsCorporateSameAddress = _objentity.Is_SameAddress;
                            }
                            else
                            {
                                checkForEntity.Corp_Address_Line1 = "";
                                checkForEntity.Corp_Address_Line2 = "";
                                checkForEntity.Corp_StateId = 0;
                                checkForEntity.Corp_CityId = 0;
                                checkForEntity.Corp_PINCode = "";
                                checkForEntity.IsCorp_Office = _objentity.IsCorp_Office;
                                checkForEntity.IsCorporateSameAddress = _objentity.Is_SameAddress;
                            }
                            if (!string.IsNullOrEmpty(_objentity.Registration_No))
                                checkForEntity.Registration_No = _objentity.Registration_No;
                            if (!string.IsNullOrEmpty(_objentity.Registration_No))
                                checkForEntity.Registration_No = _objentity.Registration_No;
                            if (!string.IsNullOrEmpty(_objentity.Registration_No))
                                checkForEntity.Registration_No = _objentity.Registration_No;
                            if (!string.IsNullOrEmpty(_objentity.Email_Id))
                                checkForEntity.Email_Id = _objentity.Email_Id;
                            if (!string.IsNullOrEmpty(_objentity.PAN))
                                checkForEntity.PAN = _objentity.PAN;
                            if (!string.IsNullOrEmpty(_objentity.WebSite))
                                checkForEntity.WebSite = _objentity.WebSite;
                            if (!string.IsNullOrEmpty(_objentity.GST))
                                checkForEntity.GST = _objentity.GST;
                            if (!string.IsNullOrEmpty(_objentity.GST))
                                checkForEntity.GST = _objentity.GST;
                            if (_objentity.CompanyCategory_Id > 0)
                                checkForEntity.CompanyCategory_Id = _objentity.CompanyCategory_Id;
                            if (_objentity.NICCode > 0)
                            {
                                checkForEntity.NIC_Code = _objentity.NICCode;
                            }
                            else
                            {
                                checkForEntity.NIC_Code = 0;
                            }
                            if (!string.IsNullOrEmpty(_objentity.Description))
                            {
                                checkForEntity.NIC_Discription = _objentity.Description;
                            }
                            else
                            {
                                checkForEntity.NIC_Discription = "";
                            }
                            checkForEntity.IS_Listed = _objentity.IS_Listed;
                            if (_objentity.IS_Listed)
                            {
                                checkForEntity.ISIN = _objentity.ISIN;
                                checkForEntity.RTA_CIN = _objentity.RTA_CIN;
                                checkForEntity.RTA_Address = _objentity.RTA_Address;
                                checkForEntity.RTA_CompanyName = _objentity.RTA_Name;
                            }
                            else
                            {
                                checkForEntity.ISIN = "";
                            }
                            checkForEntity.CompanySubCategory = _objentity.CompanyCategorySub_Id;
                            checkForEntity.Is_Deleted = false;
                            checkForEntity.Customer_Id = customerId;

                            checkForEntity.UpdatedBy = UserId;
                            checkForEntity.UpdatedOn = DateTime.Now;
                            checkForEntity.IsCompanyBranch = _objentity.IsanyBranch;
                            checkForEntity.OthersCompanyDetails = _objentity.OthersDetails;
                            //checkForEntity.CustomerBranchId = customerBranch.ID;

                            #endregion
                            entities.SaveChanges();

                            if (_objentity.files != null && _objentity.Id > 0)
                            {
                                if (_objentity.files.Count() > 0)
                                {
                                    bool uploadlogo = FileUpload.SaveEntityLogo(_objentity.files, _objentity.Id, customerId, UserId);
                                }
                            }

                            #region Regulatory Autority added by Ruchi on 03-Feb-2021
                            if (_objentity.RegulatoryAuthorityId != null)
                            {
                                if (_objentity.RegulatoryAuthorityId.Count > 0)
                                {
                                    foreach (var reg in _objentity.RegulatoryAuthorityId)
                                    {
                                        var checkregulatoryId = (from x in entities.BM_EntitySubIndustryMapping
                                                                 where x.EntityId == _objentity.Id
                                                                 // && x.ID == reg
                                                                 select x).ToList();

                                        if (checkregulatoryId.Count > 0)
                                        {
                                            foreach (var item in checkregulatoryId)
                                            {
                                                //checkregulatoryId.EntityId = _objentity.Id;
                                                //checkregulatoryId.RegulatoryId = reg;
                                                item.IsActive = false;
                                                item.Updatedon = DateTime.Now;
                                                item.Updatedby = UserId;
                                                entities.SaveChanges();
                                            }
                                        }
                                    }
                                    foreach (var reg1 in _objentity.RegulatoryAuthorityId)
                                    {
                                        var checkIddetails = (from x in entities.BM_EntitySubIndustryMapping
                                                              where x.EntityId == _objentity.Id
                                                              && x.SubIndustryID == reg1
                                                              select x).FirstOrDefault();
                                        if (checkIddetails != null)
                                        {
                                            checkIddetails.EntityId = _objentity.Id;
                                            checkIddetails.SubIndustryID = (int)reg1;
                                            checkIddetails.IsActive = true;
                                            checkIddetails.Updatedby = UserId;
                                            checkIddetails.Updatedon = DateTime.Now;
                                            entities.SaveChanges();
                                        }
                                        else
                                        {
                                            BM_EntitySubIndustryMapping _objautority = new BM_EntitySubIndustryMapping();
                                            _objautority.EntityId = _objentity.Id;
                                            _objautority.SubIndustryID = (int)reg1;
                                            _objautority.IsActive = true;
                                            _objautority.Createdby = UserId;
                                            _objautority.Createdon = DateTime.Now;
                                            entities.BM_EntitySubIndustryMapping.Add(_objautority);
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                            }
                            #endregion

                            _objentity.Message = true;
                            _objentity.successErrorMessage = "Entity updated successfully";

                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Please Select Entity Type";
                            return _objentity;
                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong";
                        return _objentity;
                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Uploaded Logo size should not be greater than 25MB and allowed file extensions are .png, .jpeg, .jpg, .gif";
                    return _objentity;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objentity;
        }

        public LLPVM CreateLLP(LLPVM _objentity, int customerId, int UserId)
        {
            try
            {
                bool checklogo = CheckCompanyLogo(_objentity.files);
                if (checklogo)
                {
                    string Message = "";
                    com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                    {
                        ID = (int)_objentity.CustomerBranchId,
                        Name = _objentity.EntityName,
                        //Type = Convert.ToByte(ddlType.SelectedValue),
                        //ComType = Convert.ToByte(comType),
                        AddressLine1 = _objentity.llpRegi_Address_Line1,
                        AddressLine2 = _objentity.llpRegi_Address_Line2,
                        StateID = _objentity.llpRegi_StateId,
                        CityID = _objentity.llpRegi_CityId,
                        Others = "",
                        PinCode = _objentity.llpRegi_PINCode,
                        ContactPerson = "",
                        Landline = "",
                        Mobile = "",
                        EmailID = _objentity.llpEmail_Id,
                        CustomerID = customerId,
                        //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                    if (customerBranch != null)
                    {
                        if (customerBranch.ID > 0)
                        {
                            if (_objentity.EntityName != null)
                            {
                                _objentity.EntityName = _objentity.EntityName.ToUpper();
                            }

                            if (_objentity.LLPIN != null)
                            {
                                _objentity.LLPIN = _objentity.LLPIN.ToUpper();
                            }

                            if (_objentity.llpPAN != null)
                            {
                                _objentity.llpPAN = _objentity.llpPAN.ToUpper();
                            }

                            if (_objentity.llpGST != null)
                            {
                                _objentity.llpGST = _objentity.llpGST.ToUpper();
                            }

                            if (_objentity.llpRegi_Address_Line1 != null)
                            {
                                _objentity.llpRegi_Address_Line1 = _objentity.llpRegi_Address_Line1.ToUpper();
                            }

                            if (_objentity.llpRegi_Address_Line2 != null)
                            {
                                _objentity.llpRegi_Address_Line2 = _objentity.llpRegi_Address_Line2.ToUpper();
                            }

                            if (_objentity.llpCorp_Address_Line1 != null)
                            {
                                _objentity.llpCorp_Address_Line1 = _objentity.llpCorp_Address_Line1.ToUpper();
                            }

                            if (_objentity.llpCorp_Address_Line2 != null)
                            {
                                _objentity.llpCorp_Address_Line2 = _objentity.llpCorp_Address_Line2.ToUpper();
                            }
                            var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                    where row.Is_Deleted == false
                                                    && row.CompanyName == _objentity.EntityName
                                                    && row.Entity_Type == _objentity.Entity_Type
                                                    && row.CIN_LLPIN == _objentity.LLPIN
                                                    && row.Email_Id == _objentity.llpEmail_Id
                                                    && row.Customer_Id == customerId
                                                    select row).FirstOrDefault();
                            if (_objCreateEntity == null)
                            {
                                if (_objentity.Entity_Type == 3)
                                {
                                    BM_EntityMaster _bmentity = new BM_EntityMaster();
                                    if (!(string.IsNullOrEmpty(_objentity.EntityName)))
                                        _bmentity.CompanyName = _objentity.EntityName;
                                    if (_objentity.Entity_Type > 0)
                                        _bmentity.Entity_Type = _objentity.Entity_Type;
                                    if (!string.IsNullOrEmpty(_objentity.llpFY))
                                        _bmentity.FY_CY = _objentity.llpFY;
                                    if (!string.IsNullOrEmpty(_objentity.LLPIN))
                                        _bmentity.CIN_LLPIN = _objentity.LLPIN;
                                    if (!string.IsNullOrEmpty(_objentity.llpGLN))
                                        _bmentity.GLN = _objentity.llpGLN;
                                    if (_objentity.llpIncorporationDate != null)
                                        _bmentity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.llpIncorporationDate);
                                    if (!string.IsNullOrEmpty(_objentity.llpRegi_Address_Line1))
                                        _bmentity.Regi_Address_Line1 = _objentity.llpRegi_Address_Line1;
                                    if (!string.IsNullOrEmpty(_objentity.llpRegi_Address_Line2))
                                        _bmentity.Regi_Address_Line2 = _objentity.llpRegi_Address_Line2;
                                    if (_objentity.llpRegi_StateId > 0)
                                        _bmentity.Regi_StateId = _objentity.llpRegi_StateId;
                                    if (_objentity.llpRegi_CityId > 0)
                                        _bmentity.Regi_CityId = _objentity.llpRegi_CityId;
                                    if (!string.IsNullOrEmpty(_objentity.llpRegi_PINCode))
                                        _bmentity.Regi_PINCode = _objentity.llpRegi_PINCode;
                                    if (_objentity.llpROC_Code > 0)
                                        _bmentity.ROC_Code = _objentity.llpROC_Code;
                                    if (_objentity.llpNo_Of_Partners > 0)
                                        _bmentity.No_Of_Partners = _objentity.llpNo_Of_Partners;
                                    if (_objentity.llpNo_Of_DesignatedPartners > 0)
                                        _bmentity.No_Of_DesignatedPartners = _objentity.llpNo_Of_DesignatedPartners;
                                    if (_objentity.llpObligation_Of_Contribution > 0)
                                        _bmentity.Obligation_Of_Contribution = _objentity.llpObligation_Of_Contribution;

                                    if (_objentity.llpIscorporateforllp)
                                    {
                                        if (!string.IsNullOrEmpty(_objentity.llpCorp_Address_Line1))
                                            _bmentity.Corp_Address_Line1 = _objentity.llpCorp_Address_Line1;
                                        if (!string.IsNullOrEmpty(_objentity.llpCorp_Address_Line2))
                                            _bmentity.Corp_Address_Line2 = _objentity.llpCorp_Address_Line2;
                                        if (_objentity.llpCorp_StateId > 0)
                                            _bmentity.Corp_StateId = _objentity.llpCorp_StateId;
                                        if (_objentity.llpCorp_CityId > 0)
                                            _bmentity.Corp_CityId = _objentity.llpCorp_CityId;
                                        if (!string.IsNullOrEmpty(_objentity.llpCorp_PINCode))
                                            _bmentity.Corp_PINCode = _objentity.llpCorp_PINCode;
                                        _bmentity.IsCorp_Office = _objentity.llpIscorporateforllp;
                                        _bmentity.IsCorporateSameAddress = _objentity.Is_SameAddressllp;

                                    }
                                    else
                                    {
                                        _bmentity.Corp_Address_Line1 = "";
                                        _bmentity.Corp_Address_Line2 = "";
                                        _bmentity.Corp_StateId = 0;
                                        _bmentity.Corp_CityId = 0;
                                        _bmentity.Corp_PINCode = "";
                                        _bmentity.IsCorp_Office = _objentity.llpIscorporateforllp;
                                        _bmentity.IsCorporateSameAddress = _objentity.Is_SameAddressllp;
                                    }
                                    if (!string.IsNullOrEmpty(_objentity.llpEmail_Id))
                                        _bmentity.Email_Id = _objentity.llpEmail_Id;
                                    if (!string.IsNullOrEmpty(_objentity.llpPAN))
                                        _bmentity.PAN = _objentity.llpPAN;
                                    if (!string.IsNullOrEmpty(_objentity.llpWebSite))
                                        _bmentity.WebSite = _objentity.llpWebSite;
                                    if (!string.IsNullOrEmpty(_objentity.llpGST))
                                        _bmentity.GST = _objentity.llpGST;
                                    _bmentity.Registration_No = "0";
                                    _bmentity.NIC_Discription = "0";
                                    _bmentity.Is_Deleted = false;
                                    _bmentity.Customer_Id = customerId;
                                    _bmentity.CustomerBranchId = customerBranch.ID;
                                    _bmentity.Maindivisonbusiness = _objentity.llpbusinessActivityID;
                                    _bmentity.CreatedOn = DateTime.Now;
                                    _bmentity.CreatedBy = UserId;
                                    _bmentity.IsCompanyBranch = _objentity.llpIsanyBranch;
                                    _bmentity.OthersCompanyDetails = _objentity.llpOthersDetails;
                                    _bmentity.PrincipalbusinessActivity = _objentity.businessActivitydec;
                                    _bmentity.Businessclassification = _objentity.businessClassificationId;

                                    entities.BM_EntityMaster.Add(_bmentity);
                                    entities.SaveChanges();

                                    _objentity.Id = _bmentity.Id;

                                    if (_objentity.files != null && _objentity.Id > 0)
                                    {
                                        if (_objentity.files.Count() > 0)
                                        {
                                            bool uploadlogo = FileUpload.SaveEntityLogo(_objentity.files, _objentity.Id, customerId, UserId);
                                        }
                                    }

                                    #region Regulatory Autority
                                    if (_objentity.Id > 0)
                                    {
                                        if (_objentity.llpRegulatoryAuthorityId != null)
                                        {
                                            if (_objentity.llpRegulatoryAuthorityId.Count > 0)
                                            {
                                                foreach (var reg in _objentity.llpRegulatoryAuthorityId)
                                                {
                                                    var checkregulatoryId = (from x in entities.BM_EntitySubIndustryMapping
                                                                             where x.EntityId == _bmentity.Id
                                                                             && x.SubIndustryID == reg
                                                                             select x).FirstOrDefault();
                                                    if (checkregulatoryId == null)
                                                    {
                                                        BM_EntitySubIndustryMapping _objautority = new BM_EntitySubIndustryMapping();
                                                        _objautority.EntityId = _objentity.Id;
                                                        _objautority.SubIndustryID = (int)reg;
                                                        _objautority.IsActive = true;
                                                        _objautority.Createdby = UserId;
                                                        _objautority.Createdon = DateTime.Now;
                                                        entities.BM_EntitySubIndustryMapping.Add(_objautority);
                                                        entities.SaveChanges();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                _objentity.Message = true;
                                _objentity.successErrorMessage = "Saved Successfully.";

                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Entity/Comapny with same LLPIN already exists";
                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong!Please check Customer Branch";

                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong!Please check Customer Branch";
                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Uploaded Logo size should not be greater than 25MB and allowed file extensions are .png, .jpeg, .jpg, .gif";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Server Error Occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objentity;
        }

        public LLPVM UpdateLLP(LLPVM _objentity, int customerId, int UserId)
        {
            try
            {
                bool checklogo = CheckCompanyLogo(_objentity.files);
                if (checklogo)
                {
                    string Message = "";

                    #region Check EntityFor Avacom 
                    com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                    {
                        ID = (int)_objentity.CustomerBranchId,
                        Name = _objentity.EntityName,
                        //Type = Convert.ToByte(ddlType.SelectedValue),
                        //ComType = Convert.ToByte(comType),
                        AddressLine1 = _objentity.llpRegi_Address_Line1,
                        AddressLine2 = _objentity.llpRegi_Address_Line2,
                        StateID = _objentity.llpRegi_StateId,
                        CityID = _objentity.llpRegi_CityId,
                        Others = "",
                        PinCode = _objentity.llpRegi_PINCode,
                        ContactPerson = "",
                        Landline = "",
                        Mobile = "",
                        EmailID = _objentity.llpEmail_Id,
                        CustomerID = customerId,
                        //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                    #endregion

                    if (customerBranch != null)
                    {
                        if (customerBranch.ID > 0)
                        {
                            if (_objentity.EntityName != null)
                                _objentity.EntityName = _objentity.EntityName.ToUpper();
                            if (_objentity.LLPIN != null)
                                _objentity.LLPIN = _objentity.LLPIN.ToUpper();

                            if (_objentity.llpPAN != null)
                                _objentity.llpPAN = _objentity.llpPAN.ToUpper();

                            if (_objentity.llpGST != null)
                                _objentity.llpGST = _objentity.llpGST.ToUpper();

                            if (_objentity.llpRegi_Address_Line1 != null)
                                _objentity.llpRegi_Address_Line1 = _objentity.llpRegi_Address_Line1.ToUpper();

                            if (!string.IsNullOrEmpty(_objentity.llpRegi_Address_Line2))
                            {
                                _objentity.llpRegi_Address_Line2 = _objentity.llpRegi_Address_Line2.ToUpper();
                            }
                            else
                            {
                                _objentity.llpRegi_Address_Line2 = "";
                            }

                            if (_objentity.llpCorp_Address_Line1 != null)
                                _objentity.llpCorp_Address_Line1 = _objentity.llpCorp_Address_Line1.ToUpper();
                            if (_objentity.llpCorp_Address_Line2 != null)

                                _objentity.llpCorp_Address_Line2 = _objentity.llpCorp_Address_Line2.ToUpper();

                            var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                    where row.Is_Deleted == false
                                                    && row.Entity_Type == _objentity.Entity_Type
                                                    && row.Id == _objentity.Id
                                                    select row).FirstOrDefault();
                            if (_objCreateEntity != null)
                            {
                                if (_objCreateEntity.Entity_Type == 3)
                                {
                                    _objCreateEntity.CompanyName = _objentity.EntityName.ToUpper();
                                    _objCreateEntity.Entity_Type = _objentity.Entity_Type;
                                    _objCreateEntity.FY_CY = _objentity.llpFY;
                                    _objCreateEntity.CIN_LLPIN = _objentity.LLPIN;

                                    _objCreateEntity.GLN = _objentity.llpGLN;
                                    if (_objentity.llpIncorporationDate != null)
                                        _objCreateEntity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.llpIncorporationDate);

                                    _objCreateEntity.Regi_Address_Line1 = _objentity.llpRegi_Address_Line1;
                                    if (!string.IsNullOrEmpty(_objentity.llpRegi_Address_Line2))
                                    {
                                        _objCreateEntity.Regi_Address_Line2 = _objentity.llpRegi_Address_Line2;
                                    }
                                    else
                                    {
                                        _objCreateEntity.Regi_Address_Line2 = "";
                                    }
                                    if (_objentity.llpRegi_StateId > 0)
                                        _objCreateEntity.Regi_StateId = _objentity.llpRegi_StateId;
                                    if (_objentity.llpRegi_CityId > 0)
                                        _objCreateEntity.Regi_CityId = _objentity.llpRegi_CityId;
                                    if (!string.IsNullOrEmpty(_objentity.llpRegi_PINCode))
                                        _objCreateEntity.Regi_PINCode = _objentity.llpRegi_PINCode;
                                    if (_objentity.llpROC_Code > 0)
                                        _objCreateEntity.ROC_Code = _objentity.llpROC_Code;
                                    if (_objentity.llpNo_Of_Partners > 0)
                                        _objCreateEntity.No_Of_Partners = _objentity.llpNo_Of_Partners;
                                    if (_objentity.llpNo_Of_DesignatedPartners > 0)
                                        _objCreateEntity.No_Of_DesignatedPartners = _objentity.llpNo_Of_DesignatedPartners;
                                    if (_objentity.llpObligation_Of_Contribution > 0)
                                        _objCreateEntity.Obligation_Of_Contribution = _objentity.llpObligation_Of_Contribution;

                                    if (_objentity.llpIscorporateforllp)
                                    {
                                        if (!string.IsNullOrEmpty(_objentity.llpCorp_Address_Line1))
                                            _objCreateEntity.Corp_Address_Line1 = _objentity.llpCorp_Address_Line1;
                                        if (!string.IsNullOrEmpty(_objentity.llpCorp_Address_Line2))
                                            _objCreateEntity.Corp_Address_Line2 = _objentity.llpCorp_Address_Line2;
                                        if (_objentity.llpCorp_StateId > 0)
                                            _objCreateEntity.Corp_StateId = _objentity.llpCorp_StateId;
                                        if (_objentity.llpCorp_CityId > 0)
                                            _objCreateEntity.Corp_CityId = _objentity.llpCorp_CityId;
                                        if (!string.IsNullOrEmpty(_objentity.llpCorp_PINCode))
                                            _objCreateEntity.Corp_PINCode = _objentity.llpCorp_PINCode;
                                        _objCreateEntity.IsCorp_Office = _objentity.llpIscorporateforllp;
                                        _objCreateEntity.IsCorporateSameAddress = _objentity.Is_SameAddressllp;
                                    }
                                    else
                                    {
                                        _objCreateEntity.Corp_Address_Line1 = "";
                                        _objCreateEntity.Corp_Address_Line2 = "";
                                        _objCreateEntity.Corp_StateId = 0;
                                        _objCreateEntity.Corp_CityId = 0;
                                        _objCreateEntity.Corp_PINCode = "";
                                        _objCreateEntity.IsCorp_Office = _objentity.llpIscorporateforllp;
                                        _objCreateEntity.IsCorporateSameAddress = _objentity.Is_SameAddressllp;
                                    }
                                    if (!string.IsNullOrEmpty(_objentity.llpEmail_Id))
                                        _objCreateEntity.Email_Id = _objentity.llpEmail_Id;
                                    if (!string.IsNullOrEmpty(_objentity.llpPAN))
                                        _objCreateEntity.PAN = _objentity.llpPAN;
                                    if (!string.IsNullOrEmpty(_objentity.llpWebSite))
                                        _objCreateEntity.WebSite = _objentity.llpWebSite;
                                    if (!string.IsNullOrEmpty(_objentity.llpGST))
                                        _objCreateEntity.GST = _objentity.llpGST;
                                    _objCreateEntity.Registration_No = "0";
                                    _objCreateEntity.NIC_Discription = "";
                                    _objCreateEntity.Is_Deleted = false;
                                    _objCreateEntity.Customer_Id = customerId;
                                    _objCreateEntity.CustomerBranchId = customerBranch.ID;
                                    _objCreateEntity.Maindivisonbusiness = _objentity.llpIndustrilabussinAID;
                                    _objCreateEntity.IsCompanyBranch = _objentity.llpIsanyBranch;
                                    _objCreateEntity.OthersCompanyDetails = _objentity.llpOthersDetails;
                                    _objCreateEntity.PrincipalbusinessActivity = _objentity.businessActivitydec;
                                    _objCreateEntity.Businessclassification = _objentity.businessClassificationId;
                                    entities.SaveChanges();

                                    if (_objentity.files != null && _objentity.Id > 0)
                                    {
                                        if (_objentity.files.Count() > 0)
                                        {
                                            bool uploadlogo = FileUpload.SaveEntityLogo(_objentity.files, _objentity.Id, customerId, UserId);
                                        }
                                    }

                                    #region Regulatory Autority added by Ruchi on 03-Feb-2021
                                    if (_objentity.Id > 0)
                                    {
                                        if (_objentity.llpRegulatoryAuthorityId != null)
                                        {
                                            if (_objentity.llpRegulatoryAuthorityId.Count > 0)
                                            {
                                                foreach (var reg in _objentity.llpRegulatoryAuthorityId)
                                                {
                                                    var checkregulatoryId = (from x in entities.BM_EntitySubIndustryMapping
                                                                             where x.EntityId == _objentity.Id
                                                                             // && x.ID == reg
                                                                             select x).ToList();

                                                    if (checkregulatoryId.Count > 0)
                                                    {
                                                        foreach (var item in checkregulatoryId)
                                                        {
                                                            //checkregulatoryId.EntityId = _objentity.Id;
                                                            //checkregulatoryId.RegulatoryId = reg;
                                                            item.IsActive = false;
                                                            item.Updatedon = DateTime.Now;
                                                            item.Updatedby = UserId;
                                                            entities.SaveChanges();
                                                        }
                                                    }
                                                }
                                                foreach (var reg1 in _objentity.llpRegulatoryAuthorityId)
                                                {
                                                    var checkIddetails = (from x in entities.BM_EntitySubIndustryMapping
                                                                          where x.EntityId == _objentity.Id
                                                                          && x.SubIndustryID == reg1
                                                                          select x).FirstOrDefault();
                                                    if (checkIddetails != null)
                                                    {
                                                        checkIddetails.EntityId = _objentity.Id;
                                                        checkIddetails.SubIndustryID = (int)reg1;
                                                        checkIddetails.IsActive = true;
                                                        checkIddetails.Updatedby = UserId;
                                                        checkIddetails.Updatedon = DateTime.Now;
                                                        entities.SaveChanges();
                                                    }
                                                    else
                                                    {
                                                        BM_EntitySubIndustryMapping _objautority = new BM_EntitySubIndustryMapping();
                                                        _objautority.EntityId = _objentity.Id;
                                                        _objautority.SubIndustryID = (int)reg1;
                                                        _objautority.IsActive = true;
                                                        _objautority.Createdby = UserId;
                                                        _objautority.Createdon = DateTime.Now;
                                                        entities.BM_EntitySubIndustryMapping.Add(_objautority);
                                                        entities.SaveChanges();
                                                    }
                                                }


                                            }
                                        }
                                    }
                                    #endregion

                                }

                                _objentity.Message = true;
                                _objentity.successErrorMessage = SecretarialConst.Messages.updateSuccess;

                                return _objentity;
                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "LLP Data Not Found";
                                return _objentity;
                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong";
                            return _objentity;
                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong";
                        return _objentity;
                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Uploaded Logo size should not be greater than 25MB and allowed file extensions are .png, .jpeg, .jpg, .gif";
                    return _objentity;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return _objentity;
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.serverError;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objentity;
            }
        }

        public TrustVM Createtrusts(TrustVM _objentity, int customerId, int UserId)
        {
            bool checklogo = CheckCompanyLogo(_objentity.files);
            if (checklogo)
            {
                string Message = "";
                try
                {

                    com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                    {
                        ID = _objentity.CustomerBranchId,
                        Name = _objentity.trustEntityName,
                        //Type = Convert.ToByte(ddlType.SelectedValue),
                        //ComType = Convert.ToByte(comType),
                        AddressLine1 = _objentity.trustRegi_Address_Line1,
                        AddressLine2 = _objentity.trustRegi_Address_Line2,
                        StateID = _objentity.trustRegi_StateId,
                        CityID = _objentity.trustRegi_CityId,
                        Others = "",
                        PinCode = _objentity.trustRegi_PINCode,
                        ContactPerson = "",
                        Landline = "",
                        Mobile = "",
                        EmailID = _objentity.trustEmail_Id,
                        CustomerID = customerId,
                        //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                    if (customerBranch != null)
                    {
                        if (customerBranch.ID > 0)
                        {
                            if (_objentity.trustEntityName != null)
                            {
                                _objentity.trustEntityName = _objentity.trustEntityName.ToUpper();
                            }
                            if (_objentity.trustPAN != null)
                            {
                                _objentity.trustPAN = _objentity.trustPAN.ToUpper();
                            }
                            if (_objentity.trustGST != null)
                            {
                                _objentity.trustGST = _objentity.trustGST.ToUpper();
                            }

                            if (_objentity.trustRegi_Address_Line1 != null)
                            {
                                _objentity.trustRegi_Address_Line1 = _objentity.trustRegi_Address_Line1.ToUpper();
                            }
                            if (_objentity != null)
                            {
                                var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                        where row.Is_Deleted == false
                                                         && row.Entity_Type == _objentity.Entity_Type
                                                         && row.Registration_No == _objentity.trustRegistration_No
                                                         && row.PAN == _objentity.trustPAN
                                                         && row.Customer_Id == customerId
                                                        select row).FirstOrDefault();
                                if (_objCreateEntity == null)
                                {
                                    if (_objentity.Entity_Type == 4)
                                    {
                                        BM_EntityMaster _bmentity = new BM_EntityMaster();
                                        if (!(string.IsNullOrEmpty(_objentity.trustEntityName)))
                                            _bmentity.CompanyName = _objentity.trustEntityName;
                                        if (_objentity.Entity_Type > 0)
                                            _bmentity.Entity_Type = _objentity.Entity_Type;

                                        if (!string.IsNullOrEmpty(_objentity.trustRegistration_No))
                                            _bmentity.Registration_No = _objentity.trustRegistration_No;

                                        if (_objentity.trustRegi_Address_Line1 != null)
                                            _bmentity.Regi_Address_Line1 = _objentity.trustRegi_Address_Line1;
                                        if (!string.IsNullOrEmpty(_objentity.trustRegi_Address_Line2))
                                            _bmentity.Regi_Address_Line2 = _objentity.trustRegi_Address_Line2;
                                        if (_objentity.trustRegi_StateId > 0)
                                            _bmentity.Regi_StateId = _objentity.trustRegi_StateId;
                                        if (_objentity.trustRegi_CityId > 0)
                                            _bmentity.Regi_CityId = _objentity.trustRegi_CityId;

                                        if (!string.IsNullOrEmpty(_objentity.trustRegi_PINCode))
                                            _bmentity.Regi_PINCode = _objentity.trustRegi_PINCode;

                                        if (!(string.IsNullOrEmpty(_objentity.trustEmail_Id)))
                                            _bmentity.Email_Id = _objentity.trustEmail_Id;
                                        if (!string.IsNullOrEmpty(_objentity.trustWebSite))
                                            _bmentity.WebSite = _objentity.trustWebSite;
                                        if (!string.IsNullOrEmpty(_objentity.trustPAN))
                                            _bmentity.PAN = _objentity.trustPAN;
                                        if (_objentity.trustNICCode > 0)
                                            _bmentity.NIC_Code = _objentity.trustNICCode;
                                        if (!string.IsNullOrEmpty(_objentity.trustDescription))
                                            _bmentity.NIC_Discription = _objentity.trustDescription;
                                        if (!string.IsNullOrEmpty(_objentity.trustGST))
                                            _bmentity.GST = _objentity.trustGST;
                                        if (_objentity.trustNo_Of_Trustees > 0)
                                            _bmentity.No_Of_Trustees = _objentity.trustNo_Of_Trustees;
                                        if (_objentity.trustRegistrationDate != null)
                                            _bmentity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.trustRegistrationDate);
                                        if (_objentity.trusttType > 0)
                                            _bmentity.trust_Type = _objentity.trusttType;

                                        if (!string.IsNullOrEmpty(_objentity.TrustFY))
                                            _bmentity.FY_CY = _objentity.TrustFY;
                                        _bmentity.IsCorporateSameAddress = false;
                                        _bmentity.Is_Deleted = false;
                                        _bmentity.Customer_Id = customerId;
                                        _bmentity.CIN_LLPIN = "";
                                        _bmentity.GLN = "";
                                        _bmentity.NIC_Discription = "";
                                        _bmentity.CustomerBranchId = customerBranch.ID;
                                        _bmentity.CreatedBy = UserId;
                                        _bmentity.CreatedOn = DateTime.Now;
                                        entities.BM_EntityMaster.Add(_bmentity);
                                        entities.SaveChanges();
                                        _objentity.Id = _bmentity.Id;

                                        if (_objentity.files != null && _objentity.Id > 0)
                                        {
                                            if (_objentity.files.Count() > 0)
                                            {
                                                bool uploadlogo = FileUpload.SaveEntityLogo(_objentity.files, _objentity.Id, customerId, UserId);
                                            }
                                        }
                                    }
                                    _objentity.Message = true;
                                    _objentity.successErrorMessage = "Saved Successfully.";

                                }
                                else
                                {
                                    _objentity.errorMessage = true;
                                    //_objentity.successErrorMessage = "Trust Data not Found";
                                    _objentity.successErrorMessage = "Trust with same Registration_No already exists";
                                }
                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Something went wrong";

                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong!Please Check Customer Branch Data";

                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong!Please Check Customer Branch Data";

                    }
                }
                catch (Exception ex)
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Server Error Occurred";
                    LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
            }
            else
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Uploaded Logo size should not be greater than 25MB and allowed file extensions are .png, .jpeg, .jpg, .gif";
            }
            return _objentity;
        }

        public TrustVM Updatetrusts(TrustVM _objentity, int customerId, int userID)
        {
            try
            {
                string Message = "";
                #region Check EntityFor Avacom 
                com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                {
                    ID = _objentity.CustomerBranchId,
                    Name = _objentity.trustEntityName,
                    //Type = Convert.ToByte(ddlType.SelectedValue),
                    //ComType = Convert.ToByte(comType),
                    AddressLine1 = _objentity.trustRegi_Address_Line1,
                    AddressLine2 = _objentity.trustRegi_Address_Line2,
                    StateID = _objentity.trustRegi_StateId,
                    CityID = _objentity.trustRegi_CityId,
                    Others = "",
                    PinCode = _objentity.trustRegi_PINCode,
                    ContactPerson = "",
                    Landline = "",
                    Mobile = "",
                    EmailID = _objentity.trustEmail_Id,
                    CustomerID = customerId,
                    //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };

                customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                #endregion

                if (customerBranch != null)
                {
                    if (customerBranch.ID > 0)
                    {
                        if (_objentity.trustEntityName != null)
                            _objentity.trustEntityName = _objentity.trustEntityName.ToUpper();
                        if (_objentity.trustPAN != null)
                            _objentity.trustPAN = _objentity.trustPAN.ToUpper();
                        if (_objentity.trustGST != null)
                            _objentity.trustGST = _objentity.trustGST.ToUpper();
                        if (_objentity.trustRegi_Address_Line1 != null)
                            _objentity.trustRegi_Address_Line1 = _objentity.trustRegi_Address_Line1.ToUpper();
                        if (_objentity != null)
                        {
                            var _objUpdateEntity = (from row in entities.BM_EntityMaster
                                                    where row.Is_Deleted == false
                                                     && row.Id == _objentity.Id
                                                    select row).FirstOrDefault();
                            if (_objUpdateEntity != null)
                            {
                                if (_objentity.Entity_Type == 4)
                                {

                                    _objUpdateEntity.CompanyName = _objentity.trustEntityName;
                                    _objUpdateEntity.Entity_Type = _objentity.Entity_Type;
                                    _objUpdateEntity.Registration_No = _objentity.trustRegistration_No;
                                    _objUpdateEntity.Regi_Address_Line1 = _objentity.trustRegi_Address_Line1;
                                    _objUpdateEntity.Regi_Address_Line2 = _objentity.trustRegi_Address_Line2;
                                    if (_objentity.trustRegi_StateId > 0)
                                        _objUpdateEntity.Regi_StateId = _objentity.trustRegi_StateId;
                                    if (_objentity.trustRegi_CityId > 0)
                                        _objUpdateEntity.Regi_CityId = _objentity.trustRegi_CityId;
                                    if (!string.IsNullOrEmpty(_objentity.trustRegi_PINCode))
                                        _objUpdateEntity.Regi_PINCode = _objentity.trustRegi_PINCode;
                                    _objUpdateEntity.Email_Id = _objentity.trustEmail_Id;
                                    _objUpdateEntity.WebSite = _objentity.trustWebSite;

                                    _objUpdateEntity.PAN = _objentity.trustPAN;
                                    if (_objentity.trustNICCode > 0)
                                        _objUpdateEntity.NIC_Code = _objentity.trustNICCode;
                                    _objUpdateEntity.NIC_Discription = _objentity.trustDescription;
                                    _objUpdateEntity.GST = _objentity.trustGST;
                                    if (_objentity.trustNo_Of_Trustees > 0)
                                        _objUpdateEntity.No_Of_Trustees = _objentity.trustNo_Of_Trustees;
                                    if (_objentity.trustRegistrationDate != null)
                                        _objUpdateEntity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.trustRegistrationDate);
                                    _objUpdateEntity.trust_Type = _objentity.trusttType;
                                    if (!string.IsNullOrEmpty(_objentity.TrustFY))
                                        _objUpdateEntity.FY_CY = _objentity.TrustFY;
                                    _objUpdateEntity.Is_Deleted = false;
                                    _objUpdateEntity.Customer_Id = customerId;
                                    _objUpdateEntity.CIN_LLPIN = "";
                                    _objUpdateEntity.GLN = "";
                                    _objUpdateEntity.NIC_Discription = "";
                                    _objUpdateEntity.CustomerBranchId = customerBranch.ID;
                                    _objUpdateEntity.IsCorporateSameAddress = false;
                                    _objUpdateEntity.UpdatedOn = DateTime.Now;
                                    _objUpdateEntity.UpdatedBy = userID;

                                    _objUpdateEntity.IsCompanyBranch = _objentity.TIsanyBranch;
                                    _objUpdateEntity.OthersCompanyDetails = _objentity.TOthersDetails;
                                    entities.SaveChanges();

                                    if (_objentity.files != null && _objentity.Id > 0)
                                    {
                                        if (_objentity.files.Count() > 0)
                                        {
                                            bool uploadlogo = FileUpload.SaveEntityLogo(_objentity.files, _objentity.Id, customerId, userID);
                                        }
                                    }
                                }
                                _objentity.Message = true;
                                _objentity.successErrorMessage = "Trust update successfully";
                                return _objentity;
                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Trust already exist";
                                return _objentity;
                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong";
                            return _objentity;
                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong";
                        return _objentity;
                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Something went wrong";
                    return _objentity;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

                return _objentity;
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.serverError;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objentity;
            }
        }

        public BodyCorporateVM Createbody_Corporate(BodyCorporateVM _objentity, int customerId, int UserId)
        {
            try
            {

                #region Create Entity 
                string Message = "";
                com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                {
                    ID = _objentity.CustomerBranchId,
                    Name = _objentity.BodyCorporateEntityName,
                    //Type = Convert.ToByte(ddlType.SelectedValue),
                    //ComType = Convert.ToByte(comType),
                    AddressLine1 = _objentity.BodyCorporateRegi_Address_Line1,
                    AddressLine2 = _objentity.BodyCorporateRegi_Address_Line2,
                    //StateID = _objentity,
                    CityID = _objentity.BodyCorporateRegi_CityId,
                    Others = "",
                    PinCode = _objentity.BodyCorporateRegi_PINCode,
                    ContactPerson = "",
                    Landline = "",
                    Mobile = "",
                    EmailID = _objentity.BodyCorporateEmail_Id,
                    CustomerID = customerId,
                    //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };

                customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                #endregion

                if (customerBranch != null)
                {
                    if (customerBranch.ID > 0)
                    {
                        if (_objentity.BodyCorporateEntityName != null)
                            _objentity.BodyCorporateEntityName = _objentity.BodyCorporateEntityName.ToUpper();

                        if (_objentity.BodyCorporateRegi_Address_Line1 != null)
                            _objentity.BodyCorporateRegi_Address_Line1 = _objentity.BodyCorporateRegi_Address_Line1.ToUpper();

                        if (_objentity.BodyCorporateRegi_Address_Line2 != null)
                            _objentity.BodyCorporateRegi_Address_Line2 = _objentity.BodyCorporateRegi_Address_Line2.ToUpper();

                        if (_objentity != null)
                        {
                            var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                    where row.Is_Deleted == false
                                                     && row.Entity_Type == _objentity.Entity_Type
                                                     && row.Email_Id == _objentity.BodyCorporateEmail_Id
                                                     && row.Registration_No == _objentity.BodyCorporateRegistration_No
                                                     && row.Customer_Id == customerId
                                                    select row).FirstOrDefault();
                            if (_objCreateEntity == null)
                            {
                                if (_objentity.Entity_Type == 5)
                                {
                                    BM_EntityMaster _bmentity = new BM_EntityMaster();
                                    _bmentity.CompanyName = _objentity.BodyCorporateEntityName;
                                    if (!string.IsNullOrEmpty(_objentity.BCFY))
                                        _bmentity.FY_CY = _objentity.BCFY;
                                    _bmentity.Entity_Type = _objentity.Entity_Type;
                                    _bmentity.Registration_No = _objentity.BodyCorporateRegistration_No;
                                    _bmentity.GLN = _objentity.BodyCorporateGLN;
                                    _bmentity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.BodyCorporateDateofIncorporate);
                                    _bmentity.Regi_Address_Line1 = _objentity.BodyCorporateRegi_Address_Line1;
                                    _bmentity.Regi_Address_Line2 = _objentity.BodyCorporateRegi_Address_Line2;
                                    _bmentity.Regi_CityId = _objentity.BodyCorporateRegi_CityId;
                                    _bmentity.Regi_PINCode = _objentity.BodyCorporateRegi_PINCode;
                                    _bmentity.CountryId = _objentity.BodyCorporateRegi_CountryID;
                                    _bmentity.Email_Id = _objentity.BodyCorporateEmail_Id;
                                    _bmentity.WebSite = _objentity.BodyCorporateWebSite;
                                    _bmentity.GoverningBody = _objentity.GoverningBody;
                                    _bmentity.Gov_Body_Members = _objentity.Gov_Body_Members;
                                    _bmentity.TIN = _objentity.TIN;
                                    _bmentity.CIN_LLPIN = "";
                                    _bmentity.PAN = "";
                                    _bmentity.GST = "";
                                    _bmentity.NIC_Discription = "";
                                    _bmentity.Customer_Id = customerId;
                                    _bmentity.Is_Deleted = false;
                                    _bmentity.CustomerBranchId = customerBranch.ID;
                                    _bmentity.IsCorporateSameAddress = false;
                                    _bmentity.CreatedBy = UserId;
                                    _bmentity.CreatedOn = DateTime.Now;

                                    _bmentity.IsCompanyBranch = _objentity.BIsanyBranch;
                                    _bmentity.OthersCompanyDetails = _objentity.BOthersDetails;

                                    entities.BM_EntityMaster.Add(_bmentity);
                                    entities.SaveChanges();
                                    _objentity.Id = _bmentity.Id;

                                    if (_objentity.files != null && _objentity.Id > 0)
                                    {
                                        if (_objentity.files.Count() > 0)
                                        {
                                            bool uploadlogo = FileUpload.SaveEntityLogo(_objentity.files, _objentity.Id, customerId, UserId);
                                        }
                                    }

                                    _objentity.Message = true;
                                    _objentity.successErrorMessage = "Body Corporate Saved Successfully";

                                }
                                else
                                {
                                    _objentity.errorMessage = true;
                                    _objentity.successErrorMessage = "Something went wrong";

                                }

                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Boby corporate with Same Registration No already exists";

                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong";

                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong";

                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Something went wrong";

                }
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.serverError;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objentity;
        }

        public BodyCorporateVM UpdateBody_Corporate(BodyCorporateVM _objentity, int customerId, int userId)
        {
            try
            {
                string Message = "";
                com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                {
                    ID = _objentity.CustomerBranchId,
                    Name = _objentity.BodyCorporateEntityName,
                    //Type = Convert.ToByte(ddlType.SelectedValue),
                    //ComType = Convert.ToByte(comType),
                    AddressLine1 = _objentity.BodyCorporateRegi_Address_Line1,
                    AddressLine2 = _objentity.BodyCorporateRegi_Address_Line2,
                    //StateID = _objentity,
                    CityID = _objentity.BodyCorporateRegi_CityId,
                    Others = "",
                    PinCode = _objentity.BodyCorporateRegi_PINCode,
                    ContactPerson = "",
                    Landline = "",
                    Mobile = "",
                    EmailID = _objentity.BodyCorporateEmail_Id,
                    CustomerID = customerId,
                    //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };

                customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                if (customerBranch != null)
                {
                    if (customerBranch.ID > 0)
                    {
                        if (_objentity.BodyCorporateEntityName != null)

                            _objentity.BodyCorporateEntityName = _objentity.BodyCorporateEntityName.ToUpper();

                        if (_objentity.BodyCorporateRegi_Address_Line1 != null)

                            _objentity.BodyCorporateRegi_Address_Line1 = _objentity.BodyCorporateRegi_Address_Line1.ToUpper();

                        if (_objentity.BodyCorporateRegi_Address_Line2 != null)

                            _objentity.BodyCorporateRegi_Address_Line2 = _objentity.BodyCorporateRegi_Address_Line2.ToUpper();

                        if (_objentity != null)
                        {
                            var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                    where row.Is_Deleted == false
                                                     && row.Entity_Type == _objentity.Entity_Type
                                                      && row.Id == _objentity.Id
                                                    select row).FirstOrDefault();
                            if (_objCreateEntity != null)
                            {
                                if (_objCreateEntity.Entity_Type == 5)
                                {

                                    _objCreateEntity.CompanyName = _objentity.BodyCorporateEntityName;
                                    if (!string.IsNullOrEmpty(_objentity.BCFY))
                                        _objCreateEntity.FY_CY = _objentity.BCFY;
                                    _objCreateEntity.Entity_Type = _objentity.Entity_Type;
                                    _objCreateEntity.Registration_No = _objentity.BodyCorporateRegistration_No;
                                    _objCreateEntity.GLN = _objentity.BodyCorporateGLN;
                                    _objCreateEntity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.BodyCorporateDateofIncorporate);
                                    _objCreateEntity.Regi_Address_Line1 = _objentity.BodyCorporateRegi_Address_Line1;
                                    _objCreateEntity.Regi_Address_Line2 = _objentity.BodyCorporateRegi_Address_Line2;
                                    _objCreateEntity.Regi_CityId = _objentity.BodyCorporateRegi_CityId;
                                    _objCreateEntity.Regi_PINCode = _objentity.BodyCorporateRegi_PINCode;
                                    _objCreateEntity.CountryId = _objentity.BodyCorporateRegi_CountryID;
                                    _objCreateEntity.Email_Id = _objentity.BodyCorporateEmail_Id;
                                    _objCreateEntity.WebSite = _objentity.BodyCorporateWebSite;
                                    _objCreateEntity.GoverningBody = _objentity.GoverningBody;
                                    _objCreateEntity.Gov_Body_Members = _objentity.Gov_Body_Members;
                                    _objCreateEntity.RegulatoryJurisdiction = _objentity.BodyCorporatRegulatoryJurisdiction;
                                    _objCreateEntity.TIN = _objentity.TIN;
                                    _objCreateEntity.CIN_LLPIN = "";
                                    _objCreateEntity.PAN = "";
                                    _objCreateEntity.GST = "";
                                    _objCreateEntity.NIC_Discription = "";
                                    _objCreateEntity.Customer_Id = customerId;
                                    _objCreateEntity.Is_Deleted = false;
                                    _objCreateEntity.CustomerBranchId = customerBranch.ID;
                                    _objCreateEntity.IsCorporateSameAddress = false;
                                    _objCreateEntity.UpdatedBy = userId;
                                    _objCreateEntity.UpdatedOn = DateTime.Now;
                                    _objCreateEntity.IsCompanyBranch = _objentity.BIsanyBranch;
                                    _objCreateEntity.OthersCompanyDetails = _objentity.BOthersDetails;
                                    entities.SaveChanges();
                                    if (_objentity.files != null && _objentity.Id > 0)
                                    {
                                        if (_objentity.files.Count() > 0)
                                        {
                                            bool uploadlogo = FileUpload.SaveEntityLogo(_objentity.files, _objentity.Id, customerId, userId);
                                        }
                                    }
                                    _objentity.Message = true;
                                    _objentity.successErrorMessage = "Body Corporate update successfully";
                                    return _objentity;
                                }
                                else
                                {
                                    _objentity.errorMessage = true;
                                    _objentity.successErrorMessage = "Something went wrong";
                                    return _objentity;
                                }

                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Fields already exist";
                                return _objentity;
                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong";
                            return _objentity;
                        }

                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong";
                        return _objentity;
                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Something went wrong";
                    return _objentity;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

                return _objentity;
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = SecretarialConst.Messages.serverError;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objentity;
            }
        }

        public IEnumerable<VMCountry> GetAllCountryDtls()
        {
            try
            {
                var Listofcountry = (from row in entities.Countries.OrderBy(x => x.Name)
                                     where row.IsDeleted == false
                                     select new VMCountry
                                     {
                                         CountryId = row.ID,
                                         CountryName = row.Name,
                                         Nationality = row.Nationality
                                     }).ToList();
                Listofcountry.Insert(0, new VMCountry() { CountryId = 0, CountryName = "Select Country", Nationality = "Select Nationality" });
                return Listofcountry;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public Pravate_PublicVM GetprivatepublicDtls(int Id, int EntityTypeId)
        {
            try
            {
                if (Id > 0 && EntityTypeId > 0)
                {
                    var objgetentitydtls = (from row in entities.BM_EntityMaster
                                            where row.Entity_Type == EntityTypeId
                                                  && row.Id == Id
                                                  && row.Is_Deleted == false
                                            select new Pravate_PublicVM
                                            {
                                                Id = row.Id,
                                                CustomerBranchId = row.CustomerBranchId != null ? (int)row.CustomerBranchId : 0,
                                                Entity_Type = row.Entity_Type,
                                                EntityName = row.CompanyName,
                                                PAN = row.PAN,
                                                CompanyCategory_Id = row.CompanyCategory_Id,
                                                CIN = row.CIN_LLPIN,
                                                Corp_Address_Line1 = row.Corp_Address_Line1,
                                                Corp_Address_Line2 = row.Corp_Address_Line2,
                                                Corp_CityId = row.Corp_CityId,
                                                Corp_StateId = row.Corp_StateId,
                                                Corp_PINCode = row.Corp_PINCode,
                                                IsCorp_Office = row.IsCorp_Office,
                                                Email_Id = row.Email_Id,
                                                GLN = row.GLN,
                                                GST = row.GST,
                                                ISIN = row.ISIN,
                                                CompanyCategorySub_Id = row.CompanySubCategory,
                                                Description = row.NIC_Discription,
                                                IncorporationDate = SqlFunctions.DateName("day", row.IncorporationDate) + "/" + SqlFunctions.DateName("month", row.IncorporationDate) + "/" + SqlFunctions.DateName("year", row.IncorporationDate),
                                                IS_Listed = row.IS_Listed,
                                                Is_SameAddress = row.IsCorporateSameAddress,
                                                Registration_No = row.Registration_No,
                                                Regi_Address_Line1 = row.Regi_Address_Line1,
                                                Regi_Address_Line2 = row.Regi_Address_Line2,
                                                Regi_CityId = row.Regi_CityId,
                                                Regi_PINCode = row.Regi_PINCode,
                                                Regi_StateId = row.Regi_StateId,
                                                ROC_Code = row.ROC_Code,
                                                NICCode = row.NIC_Code,
                                                WebSite = row.WebSite,
                                                FY = row.FY_CY,
                                                RTA_CIN = row.RTA_CIN,
                                                RTA_Name = row.RTA_CompanyName,
                                                RTA_Address = row.RTA_Address,
                                                EntityLogoname = (from x in entities.BM_FileData where x.EntityId == Id && x.IsDeleted == false select x.FileName).FirstOrDefault(),
                                                logopath = (from x in entities.BM_FileData where x.EntityId == Id && x.IsDeleted == false select x.FilePath).FirstOrDefault(),

                                                IsanyBranch = row.IsCompanyBranch,
                                                OthersDetails = row.OthersCompanyDetails,
                                                RegulatoryAuthorityId = (from y in entities.BM_EntitySubIndustryMapping where y.EntityId == Id && y.IsActive == true select y.SubIndustryID).ToList(),

                                            }).FirstOrDefault();
                    //objgetentitydtls.SubCompanyDetails = new List<SubCompanyType>();

                    return objgetentitydtls;
                }
                else
                {
                    Pravate_PublicVM obj = new Pravate_PublicVM();
                    obj.errorMessage = true;
                    obj.successErrorMessage = "Something went wrong";
                    return obj;
                }
            }
            catch (Exception ex)
            {
                Pravate_PublicVM obj = new Pravate_PublicVM();
                obj.errorMessage = true;
                obj.successErrorMessage = "Server error occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public LLPVM GetLLPDtls(int Id, int EntityTypeId)
        {
            try
            {
                var objgetentitydtls = (from row in entities.BM_EntityMaster
                                        where row.Entity_Type == EntityTypeId
                                        && row.Id == Id
                                        && row.Is_Deleted == false
                                        select new LLPVM
                                        {
                                            Id = row.Id,
                                            //CustomerBranchId = row.CustomerBranchId != null ? Convert.ToInt32(row.CustomerBranchId) : 0,
                                            CustomerBranchId = row.CustomerBranchId != 0 ? (int)row.CustomerBranchId : 0,
                                            EntityName = row.CompanyName,
                                            Entity_Type = row.Entity_Type,
                                            llpEmail_Id = row.Email_Id,
                                            llpGLN = row.GLN,
                                            llpGST = row.GST,
                                            LLPIN = row.CIN_LLPIN,
                                            llpIncorporationDate = SqlFunctions.DateName("day", row.IncorporationDate) + "/" + SqlFunctions.DateName("month", row.IncorporationDate) + "/" + SqlFunctions.DateName("year", row.IncorporationDate),
                                            llpNo_Of_DesignatedPartners = row.No_Of_DesignatedPartners,
                                            llpNo_Of_Partners = row.No_Of_Partners,
                                            llpCorp_Address_Line1 = row.Corp_Address_Line1,
                                            llpCorp_Address_Line2 = row.Corp_Address_Line2,
                                            Is_SameAddressllp = row.IsCorporateSameAddress,
                                            llpCorp_StateId = row.Corp_StateId,
                                            llpCorp_CityId = row.Corp_CityId,
                                            llpCorp_PINCode = row.Corp_PINCode,
                                            llpPAN = row.PAN,
                                            llpObligation_Of_Contribution = row.Obligation_Of_Contribution,
                                            llpRegistration_No = row.Registration_No,
                                            llpRegi_Address_Line1 = row.Regi_Address_Line1,
                                            llpRegi_Address_Line2 = row.Regi_Address_Line2,
                                            llpRegi_CityId = row.Regi_CityId,
                                            llpIscorporateforllp = row.IsCorp_Office,
                                            llpRegi_PINCode = row.Regi_PINCode,
                                            llpRegi_StateId = row.Regi_StateId,
                                            llpROC_Code = row.ROC_Code,
                                            llpWebSite = row.WebSite,
                                            llpFY = row.FY_CY,
                                            llpIndustrilabussinAID = row.Maindivisonbusiness,
                                            llpIsanyBranch = row.IsCompanyBranch,
                                            llpOthersDetails = row.OthersCompanyDetails,
                                            llpIndustrilabussinADesc = row.Maindivisonbusiness,
                                            businessActivitydec = row.PrincipalbusinessActivity,
                                            businessClassificationId = row.Businessclassification,
                                            EntityLogoname = (from x in entities.BM_FileData where x.EntityId == Id && x.IsDeleted == false select x.FileName).FirstOrDefault(),
                                            logopath = (from x in entities.BM_FileData where x.EntityId == Id && x.IsDeleted == false select x.FilePath).FirstOrDefault(),
                                            llpRegulatoryAuthorityId = (from y in entities.BM_EntitySubIndustryMapping where y.EntityId == Id && y.IsActive == true select y.SubIndustryID).ToList(),

                                        }).FirstOrDefault();

                return objgetentitydtls;

            }
            catch (Exception ex)
            {
                LLPVM obj = new LLPVM();
                obj.errorMessage = true;
                obj.successErrorMessage = SecretarialConst.Messages.serverError;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public IEnumerable<VMCity> GetAllCountryCitydtlsForBodyCorporate(int? CountryId)
        {
            try
            {
                var getcityforbodyCorporate = (from row in entities.Countries.OrderBy(x => x.Name)
                                               join rows in entities.States.Where(x => x.CountryID == CountryId)
                                               on row.ID equals (rows.CountryID)
                                               join rowss in entities.Cities
                                               on rows.ID equals (rowss.StateId)
                                               select new VMCity
                                               {
                                                   Reg_cityId = rowss.ID,
                                                   Name = rowss.Name,
                                               });
                return getcityforbodyCorporate;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public TrustVM GetTustDtls(int Id, int EntityTypeId)
        {
            try
            {
                var objgetentitydtls = (from row in entities.BM_EntityMaster
                                        where row.Entity_Type == EntityTypeId
                                        && row.Id == Id
                                        && row.Is_Deleted == false
                                        select new TrustVM
                                        {
                                            Id = row.Id,
                                            //CustomerBranchId = row.CustomerBranchId != null ? Convert.ToInt32(row.CustomerBranchId) : 0,
                                            CustomerBranchId = row.CustomerBranchId != null ? (int)row.CustomerBranchId : 0,//Change by Ruchi on Dec 02 2019
                                            trustEntityName = row.CompanyName,
                                            Entity_Type = row.Entity_Type,
                                            //trustEntity_Type=row.,
                                            trustRegistration_No = row.Registration_No,
                                            trustRegistrationDate = SqlFunctions.DateName("day", row.IncorporationDate) + "/" + SqlFunctions.DateName("month", row.IncorporationDate) + "/" + SqlFunctions.DateName("year", row.IncorporationDate),
                                            trustRegi_Address_Line1 = row.Regi_Address_Line1,
                                            trustRegi_Address_Line2 = row.Regi_Address_Line2,
                                            trustRegi_StateId = row.Regi_StateId,
                                            trustRegi_CityId = row.Regi_CityId,
                                            trustRegi_PINCode = row.Regi_PINCode,
                                            trustNo_Of_Trustees = row.No_Of_Trustees,
                                            trustEmail_Id = row.Email_Id,
                                            trustWebSite = row.WebSite,
                                            trustPAN = row.PAN,
                                            trustGST = row.GST,
                                            trusttType = (int)row.trust_Type,
                                            TrustFY = row.FY_CY,
                                            EntityLogoname = (from x in entities.BM_FileData where x.EntityId == Id && x.IsDeleted == false select x.FileName).FirstOrDefault(),
                                            logopath = (from x in entities.BM_FileData where x.EntityId == Id && x.IsDeleted == false select x.FilePath).FirstOrDefault(),
                                            TIsanyBranch = row.IsCompanyBranch,
                                            TOthersDetails = row.OthersCompanyDetails,
                                        }).FirstOrDefault();

                return objgetentitydtls;
            }
            catch (Exception ex)
            {
                TrustVM obj = new TrustVM();
                obj.errorMessage = true;
                obj.successErrorMessage = SecretarialConst.Messages.serverError;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public BodyCorporateVM GetBodyCorporate(int Id, int entityTypeId)
        {
            try
            {
                var objgetentitydtls = (from row in entities.BM_EntityMaster
                                        where row.Entity_Type == entityTypeId
                                        && row.Id == Id
                                        && row.Is_Deleted == false
                                        select new BodyCorporateVM
                                        {
                                            Id = row.Id,
                                            //CustomerBranchId = row.CustomerBranchId != null ? Convert.ToInt32(row.CustomerBranchId) : 0,
                                            CustomerBranchId = row.CustomerBranchId > 0 ? (int)row.CustomerBranchId : 0,//change by ruchi on 24th mar-2021
                                            BodyCorporateEntityName = row.CompanyName,
                                            Entity_Type = row.Entity_Type,
                                            BodyCorporateRegistration_No = row.Registration_No,
                                            BodyCorporateGLN = row.GLN,
                                            BodyCorporateDateofIncorporate = SqlFunctions.DateName("day", row.IncorporationDate) + "/" + SqlFunctions.DateName("month", row.IncorporationDate) + "/" + SqlFunctions.DateName("year", row.IncorporationDate),
                                            BodyCorporateRegi_Address_Line1 = row.Regi_Address_Line1,
                                            BodyCorporateRegi_Address_Line2 = row.Regi_Address_Line2,
                                            BodyCorporateRegi_CountryID = (int)row.CountryId,
                                            BodyCorporateRegi_CityId = row.Regi_CityId,
                                            BodyCorporateRegi_PINCode = row.Regi_PINCode,
                                            BodyCorporatRegulatoryJurisdiction = row.RegulatoryJurisdiction,
                                            BodyCorporateEmail_Id = row.Email_Id,
                                            BodyCorporateWebSite = row.WebSite,
                                            TIN = row.TIN,
                                            GoverningBody = row.GoverningBody,
                                            Gov_Body_Members = row.Gov_Body_Members,
                                            BCFY = row.FY_CY,
                                            BIsanyBranch = row.IsCompanyBranch,
                                            BOthersDetails = row.OthersCompanyDetails,
                                            EntityLogoname = (from x in entities.BM_FileData where x.EntityId == Id && x.IsDeleted == false select x.FileName).FirstOrDefault(),
                                            logopath = (from x in entities.BM_FileData where x.EntityId == Id && x.IsDeleted == false select x.FilePath).FirstOrDefault(),
                                        }).FirstOrDefault();
                //obj.Message = true;
                return objgetentitydtls;
            }
            catch (Exception ex)
            {
                BodyCorporateVM obj = new BodyCorporateVM();
                obj.errorMessage = true;
                obj.successErrorMessage = SecretarialConst.Messages.serverError;

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        #region Applicability

        public decimal GetCapital(int Entity_Id, int customerId)
        {
            decimal result = 0;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    //Commented on 29 Apr 2021 due to preference capital not match on applicability master
                    //var PreferenceShare = entities.BM_CapitalMaster.Where(k => k.Entity_Id == Entity_Id && k.CustomerId == customerId && k.IsActive == true).Select(k => k.TotamtNoPaidupCapital + k.Pri_TotamtNoPaidupCapital).FirstOrDefault();
                    var PreferenceShare = objIShareholdingMaster.gettotalpaidupcapitalinamCapital(Entity_Id, customerId);
                    //End
                    var getApplicabilitydetails = entities.BM_EntityMaster.Where(k => k.Id == Entity_Id && k.Customer_Id == customerId && k.Is_Deleted == false).FirstOrDefault();
                    if (getApplicabilitydetails != null)
                    {
                        if (getApplicabilitydetails.AmountInVariable != null)
                        {
                            if (getApplicabilitydetails.AmountInVariable == "L")
                            {
                                result = Math.Round(PreferenceShare / 100000, 4);
                            }
                            else
                            {
                                result = Math.Round(PreferenceShare / 10000000, 4);
                            }
                        }
                        else
                        {
                            result = Math.Round(PreferenceShare / 10000000, 4);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public BM_Rule GetBM_Applicability(int Entity_Id, decimal PreferenceShare, int customerId)
        {
            var result = new BM_Rule() { Entity_Id = Entity_Id };
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                result = (from row in entities.BM_EntityMaster
                          where row.Id == Entity_Id
                          select new BM_Rule
                          {
                              Entity_Id = row.Id,
                              Entity_Type = row.Entity_Type,
                              PaidupShareCapital = PreferenceShare, //row.PaidupCapital,
                              Turnover = row.Turnover,
                              NetWorth = row.NetWorth,
                              NetProfit = row.NetProfit,
                              Borrowings = row.Borrowings,
                              Deposit = row.DepositFromMember,
                              ShareHolders = row.ShareHolders,
                              AmountIn = row.AmountIn,
                              AmountInvariable = row.AmountInVariable,
                          }).FirstOrDefault();

                if (result != null)
                {
                    if (result.AmountInvariable != null)
                    {
                        if (result.AmountInvariable == "L")
                        {
                            result.NetProfit = Math.Round(Convert.ToDecimal(result.NetProfit * Convert.ToDecimal(100)), 2);
                            result.Turnover = Math.Round(Convert.ToDecimal(result.Turnover * Convert.ToDecimal(100)), 2);
                            result.NetWorth = Math.Round(Convert.ToDecimal(result.NetWorth * Convert.ToDecimal(100)), 2);
                            result.Borrowings = Math.Round(Convert.ToDecimal(result.Borrowings * Convert.ToDecimal(100)), 2);
                            result.Deposit = Math.Round(Convert.ToDecimal(result.Deposit * Convert.ToDecimal(100)), 2);
                        }
                        else if (result.AmountInvariable == "T")
                        {
                            result.NetProfit = Math.Round(Convert.ToDecimal(result.NetProfit * Convert.ToDecimal(10000)), 2);
                            result.Turnover = Math.Round(Convert.ToDecimal(result.Turnover * Convert.ToDecimal(10000)), 2);
                            result.NetWorth = Math.Round(Convert.ToDecimal(result.NetWorth * Convert.ToDecimal(10000)), 2);
                            result.Borrowings = Math.Round(Convert.ToDecimal(result.Borrowings * Convert.ToDecimal(10000)), 2);
                            result.Deposit = Math.Round(Convert.ToDecimal(result.Deposit * Convert.ToDecimal(10000)), 2);
                        }
                    }
                }
            }

            return GetBM_Applicability(result, customerId);
        }

        public BM_Rule GetBM_Applicability(BM_Rule objBM_Rule, int customerId)
        {
            double Crore = 0.01;
            double thousand = 0.0001;

            BM_Rule obj = new BM_Rule();
            obj.Rules = new List<VM.Rules>();

            obj.Entity_Id = objBM_Rule.Entity_Id;
            obj.Entity_Type = objBM_Rule.Entity_Type;
            if (objBM_Rule.PaidupShareCapital > 0)
            {
                obj.PaidupShareCapital = Math.Round((Convert.ToDecimal(objBM_Rule.PaidupShareCapital)), 2);
            }
            if (objBM_Rule.Turnover > 0)
            {
                obj.Turnover = Math.Round((Convert.ToDecimal(objBM_Rule.Turnover)), 2);
            }
            obj.Turnover_rule = objBM_Rule.Turnover;

            //Changed on 22 Apr 2021
            //if (objBM_Rule.NetWorth > 0)
            //{
            //    obj.NetWorth = Math.Round((Convert.ToDecimal(objBM_Rule.NetWorth)), 2);
            //}
            //obj.NetWorth_rule = objBM_Rule.NetWorth;

            //if (objBM_Rule.NetProfit > 0)
            //{
            //    obj.NetProfit = Math.Round((Convert.ToDecimal(objBM_Rule.NetProfit)), 2);
            //}
            //obj.NetProfit_rule = objBM_Rule.NetProfit;

            if (objBM_Rule.NetWorth != null)
            {
                obj.NetWorth = Math.Round((Convert.ToDecimal(objBM_Rule.NetWorth)), 2);
            }
            obj.NetWorth_rule = objBM_Rule.NetWorth;

            if (objBM_Rule.NetProfit != null)
            {
                obj.NetProfit = Math.Round((Convert.ToDecimal(objBM_Rule.NetProfit)), 2);
            }
            obj.NetProfit_rule = objBM_Rule.NetProfit;

            //End

            if (objBM_Rule.Borrowings > 0)
            {
                obj.Borrowings = Math.Round((Convert.ToDecimal(objBM_Rule.Borrowings)), 2);
            }

            obj.Borrowings_rule = objBM_Rule.Borrowings;

            obj.ShareHolders = objBM_Rule.ShareHolders;

            obj.ShareHolders_rule = objBM_Rule.ShareHolders;
            if (objBM_Rule.Deposit > 0)
            {
                obj.Deposit = Math.Round((Convert.ToDecimal(objBM_Rule.Deposit)), 2);
            }
            obj.Deposit_rule = objBM_Rule.Deposit_rule;
            obj.AmountInvariable = objBM_Rule.AmountInvariable;
            if (obj.AmountInvariable == "L")
            {
                obj.AmountIn = 100000;
                obj.NetProfit_rule = obj.NetProfit_rule * Convert.ToDecimal(Crore);
                obj.Turnover_rule = obj.Turnover_rule * Convert.ToDecimal(Crore);
                obj.NetWorth_rule = obj.NetWorth_rule * Convert.ToDecimal(Crore);
                obj.Borrowings_rule = obj.Borrowings_rule * Convert.ToDecimal(Crore);
                obj.ShareHolders_rule = obj.ShareHolders_rule * Convert.ToDecimal(Crore);
                obj.Deposit_rule = obj.Deposit_rule * Convert.ToDecimal(Crore);
            }
            else if (obj.AmountInvariable == "T")
            {
                obj.AmountIn = 1000;
                obj.NetProfit_rule = obj.NetProfit_rule * Convert.ToDecimal(thousand);
                obj.Turnover_rule = obj.Turnover_rule * Convert.ToDecimal(thousand);
                obj.NetWorth_rule = obj.NetWorth_rule * Convert.ToDecimal(thousand);
                obj.Borrowings_rule = obj.Borrowings_rule * Convert.ToDecimal(thousand);
                obj.ShareHolders_rule = obj.ShareHolders_rule * Convert.ToDecimal(thousand);
                obj.Deposit_rule = obj.Deposit_rule * Convert.ToDecimal(thousand);
            }
            else
            {
                obj.AmountIn = 10000000;
            }
            //Commented on 25 May 2021 due to preference capital not match on applicability master
            //var PreferenceShare = entities.BM_CapitalMaster.Where(k => k.Entity_Id == obj.Entity_Id && k.IsActive == true).Select(k => k.TotamtNoPaidupCapital + k.Pri_TotamtNoPaidupCapital).FirstOrDefault();
            var PreferenceShare = objIShareholdingMaster.gettotalpaidupcapitalinamCapital(obj.Entity_Id, customerId);
            //End
            if (obj.AmountIn > 0)
            {
                obj.PaidupShareCapital = Math.Round((PreferenceShare / Convert.ToDecimal(obj.AmountIn)), 2);
            }
            else
            {

                obj.PaidupShareCapital = Math.Round((PreferenceShare / 10000000), 2);
            }

            obj.PaidupShareCapital_rule = Math.Round((PreferenceShare / 10000000), 2);

            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = (from row in entities.BM_Applicability
                              join rows in entities.BM_Applicability_Entity on row.Id equals rows.ApplicabilityId into app_entity
                              from r in app_entity.Where(k => k.EntityId == objBM_Rule.Entity_Id).DefaultIfEmpty()
                                  //where objBM_Rule.Entity_Type == 1 ? row.Entity_Private == true : row.Entity_Public == true
                              where objBM_Rule.Entity_Type == 1 ? row.Entity_Private == true : objBM_Rule.Entity_Type == 2 ? row.Entity_Public == true : objBM_Rule.Entity_Type == 10 ? row.Entity_Listed == true : 1 == 2
                              select new BM_ApplicabilityVM
                              {
                                  SrNo = r.Id,
                                  Id = row.Id,
                                  Applicability = row.Applicability,
                                  PaidupCapital = row.PaidupCapital,
                                  ApplicabilityValue = r.ApplicabilityValue == null ? false : (r.ApplicabilityValue == true ? true : false),
                                  Turnover = row.Turnover,
                                  NetWorth = row.NetWorth,
                                  NetProfit = row.NetProfit,
                                  Borrowings = row.Borrowings,
                                  DepositFromMember = row.DepositFromMember,
                                  ShareHolders = row.ShareHolders,
                                  Formula = objBM_Rule.Entity_Type == 10 ? row.FormulaListed : row.Formula,
                                  AmountIn = objBM_Rule.AmountIn
                              }).ToList();

                foreach (var item in result)
                {
                    bool prevFlag = false, flag = false;
                    //bool defaultValue = objBM_Rule != null ? (objBM_Rule.Rules !=null ? (objBM_Rule.Rules.Where(k=>k.ApplicabilityId == item.Id).Select(k => k.Answer).FirstOrDefault()) : false): false;
                    bool defaultValue = objBM_Rule != null ? (objBM_Rule.Rules != null ? (objBM_Rule.Rules.Where(k => k.ApplicabilityId == item.Id).Select(k => k.Answer).FirstOrDefault()) : item.ApplicabilityValue) : item.ApplicabilityValue;

                    if (!string.IsNullOrEmpty(item.Formula))
                    {
                        int i = 0;
                        string prevOperator = "";
                        #region Conditions
                        foreach (var condition in item.Formula.Trim(',').Split(','))
                        {
                            switch (condition)
                            {
                                case "C":
                                    flag = obj.PaidupShareCapital_rule >= item.PaidupCapital ? true : false;
                                    break;
                                case "T":
                                    flag = obj.Turnover_rule >= item.Turnover ? true : false;
                                    break;
                                case "W":
                                    flag = obj.NetWorth_rule >= item.NetWorth ? true : false;
                                    break;
                                case "P":
                                    flag = obj.NetProfit_rule >= item.NetProfit ? true : false;
                                    break;
                                case "B":
                                    flag = obj.Borrowings_rule >= item.Borrowings ? true : false;
                                    break;
                                case "D":
                                    flag = obj.Deposit_rule >= item.DepositFromMember ? true : false;
                                    break;
                                case "H":
                                    flag = obj.ShareHolders_rule >= item.ShareHolders ? true : false;
                                    break;
                                case "&":
                                    prevOperator = "&";
                                    //prevFlag = prevFlag && flag;
                                    break;
                                case "|":
                                    prevOperator = "|";
                                    //prevFlag = prevFlag || flag;
                                    break;
                                default:
                                    break;
                            }

                            if (i == 0)
                            {
                                prevFlag = flag;
                                i++;
                            }

                            if (condition == "|" || condition == "&")
                                continue;

                            switch (prevOperator)
                            {
                                case "&":
                                    prevFlag = prevFlag && flag;
                                    prevOperator = "";
                                    break;
                                case "|":
                                    prevFlag = prevFlag || flag;
                                    prevOperator = "";
                                    break;
                                default:
                                    break;
                            }

                        }
                        #endregion

                        obj.Rules.Add(new Rules() { SrNo = item.SrNo, ApplicabilityId = item.Id, Name = item.Applicability, Editable = !prevFlag, Answer = prevFlag ? true : defaultValue });
                    }
                    else
                    {
                        if (item.Formula == string.Empty)
                        {
                            obj.Rules.Add(new Rules() { SrNo = item.SrNo, ApplicabilityId = item.Id, Name = item.Applicability, Editable = false, Answer = true });
                        }
                        else
                        {
                            obj.Rules.Add(new Rules() { SrNo = item.SrNo, ApplicabilityId = item.Id, Name = item.Applicability, Editable = true, Answer = defaultValue });
                        }
                    }

                }
            }

            if(obj != null)
            {
                if(obj.Rules != null)
                {
                    obj.Rules = obj.Rules.OrderByDescending(k => k.Editable).ToList();
                }
            }
            return obj;
        }

        public bool UpdateBM_Applicability(BM_Rule objBM_Rule)
        {
            try
            {
                double Crore = 0.01;
                double thousand = 0.0001;
                BM_EntityMaster objBM_EntityMaster = new BM_EntityMaster();
                objBM_EntityMaster.Id = objBM_Rule.Entity_Id;

                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    // entities.BM_EntityMaster.Attach(objBM_EntityMaster);
                    objBM_EntityMaster = entities.BM_EntityMaster.Where(k => k.Id == objBM_Rule.Entity_Id && k.Is_Deleted == false).FirstOrDefault();

                    //Find(objBM_Rule.Entity_Id);

                    objBM_EntityMaster.Turnover = objBM_Rule.Turnover;
                    objBM_EntityMaster.NetWorth = objBM_Rule.NetWorth;
                    objBM_EntityMaster.NetProfit = objBM_Rule.NetProfit;
                    objBM_EntityMaster.Borrowings = objBM_Rule.Borrowings;
                    objBM_EntityMaster.DepositFromMember = objBM_Rule.Deposit;
                    objBM_EntityMaster.AmountInVariable = objBM_Rule.AmountInvariable;
                    if (objBM_Rule.AmountInvariable == "L")
                    {
                        objBM_EntityMaster.AmountIn = 100000;

                        objBM_EntityMaster.NetProfit = objBM_EntityMaster.NetProfit * Convert.ToDecimal(Crore);
                        objBM_EntityMaster.Turnover = objBM_EntityMaster.Turnover * Convert.ToDecimal(Crore);
                        objBM_EntityMaster.NetWorth = objBM_EntityMaster.NetWorth * Convert.ToDecimal(Crore);
                        objBM_EntityMaster.Borrowings = objBM_EntityMaster.Borrowings * Convert.ToDecimal(Crore);
                        objBM_EntityMaster.DepositFromMember = objBM_EntityMaster.DepositFromMember * Convert.ToDecimal(Crore);


                    }
                    else if (objBM_Rule.AmountInvariable == "T")
                    {
                        objBM_EntityMaster.AmountIn = 1000;

                        objBM_EntityMaster.NetProfit = objBM_EntityMaster.NetProfit * Convert.ToDecimal(thousand);
                        objBM_EntityMaster.Turnover = objBM_EntityMaster.Turnover * Convert.ToDecimal(thousand);
                        objBM_EntityMaster.NetWorth = objBM_EntityMaster.NetWorth * Convert.ToDecimal(thousand);
                        objBM_EntityMaster.Borrowings = objBM_EntityMaster.Borrowings * Convert.ToDecimal(thousand);
                        objBM_EntityMaster.DepositFromMember = objBM_EntityMaster.DepositFromMember * Convert.ToDecimal(thousand);
                    }
                    else
                    {
                        objBM_EntityMaster.AmountIn = 10000000;
                        //objBM_EntityMaster.NetProfit = objBM_EntityMaster.NetProfit * Convert.ToDecimal(100);
                        //objBM_EntityMaster.Turnover = objBM_EntityMaster.Turnover * Convert.ToDecimal(100);
                        //objBM_EntityMaster.NetWorth = objBM_EntityMaster.NetWorth * Convert.ToDecimal(100);
                        //objBM_EntityMaster.Borrowings = objBM_EntityMaster.Borrowings * Convert.ToDecimal(100);
                        //objBM_EntityMaster.DepositFromMember = objBM_EntityMaster.DepositFromMember * Convert.ToDecimal(100);
                    }


                    //entities.Entry(objBM_EntityMaster).Property(K => K.Turnover).IsModified = true;
                    //entities.Entry(objBM_EntityMaster).Property(K => K.NetWorth).IsModified = true;
                    //entities.Entry(objBM_EntityMaster).Property(K => K.NetProfit).IsModified = true;
                    //entities.Entry(objBM_EntityMaster).Property(K => K.Borrowings).IsModified = true;
                    //entities.Entry(objBM_EntityMaster).Property(K => K.DepositFromMember).IsModified = true;

                    entities.SaveChanges();

                    entities.BM_Applicability_Entity.Where(k => k.EntityId == objBM_Rule.Entity_Id).ToList().ForEach(k => entities.BM_Applicability_Entity.Remove(k));
                    entities.SaveChanges();

                    foreach (var item in objBM_Rule.Rules)
                    {
                        var data = new BM_Applicability_Entity() { EntityId = objBM_Rule.Entity_Id, ApplicabilityId = item.ApplicabilityId, ApplicabilityValue = item.Answer };
                        if (item.SrNo <= 0)
                        {
                            entities.Entry(data).State = System.Data.Entity.EntityState.Modified;
                            entities.SaveChanges();
                        }
                        else
                        {
                            data.Id = Convert.ToInt64(item.SrNo);
                            entities.BM_Applicability_Entity.Add(data); // new BM_Applicability_Entity() {EntityId= objBM_Rule.Entity_Id, ApplicabilityId = item.Id, ApplicabilityValue = item.Answer  });
                            entities.SaveChanges();
                        }

                    }
                }
                return true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        public bool DeleteEntityInfo(int Id, int customerId)
        {
            try
            {
                if (Id > 0)
                {
                    var _objEntityDlt = (from row in entities.BM_EntityMaster
                                         where row.Id == Id && row.Is_Deleted == false
                                         && row.Customer_Id == customerId
                                         select row).FirstOrDefault();
                    if (_objEntityDlt != null)
                    {
                        _objEntityDlt.Is_Deleted = true;
                        _objEntityDlt.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        //obj.Message = true;
                        //obj.successErrorMessage = "Deleted Successfully";
                        return true;
                    }
                    else
                    {
                        //obj.errorMessage = true;
                        //obj.successErrorMessage = "Some error Occure in Deleting Fields";
                        return false;

                    }
                }
                else
                {
                    //obj.errorMessage = true;
                    //obj.successErrorMessage = "Some error Occure in Deleting Fields";
                    return false;
                }
            }
            catch (Exception ex)
            {
                //obj.successErrorMessage = "Some error Occure in Deleting Fields";
                //obj.errorMessage = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public EntityMaster GetEntityMaster(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<VMState> GetStateListforMaster(int countryId)
        {
            try
            {

                var Listofstate = (from row in entities.States.OrderBy(x => x.Name)
                                   where row.CountryID == countryId
                                   select new VMState
                                   {
                                       SIdPublic = row.ID,
                                       Name = row.Name
                                   }).ToList();
                if (Listofstate == null)
                {
                    Listofstate = new List<VMState>();
                }

                Listofstate.Insert(0, new VMState() { SIdPublic = 0, Name = "NA" });
                return Listofstate;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public IEnumerable<BM_Nationality> GetNationality()
        {
            try
            {

                var ListofNationality = (from row in entities.BM_Nationality.OrderBy(x => x.Nationality)
                                         where row.IsDeleted == false
                                         select row).ToList();

                return ListofNationality;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public bool CreateEntityFromLegal(CustomerBranch obj)
        {
            var result = false;

            try
            {
                BM_EntityMaster _obj = new BM_EntityMaster();

                _obj.CompanyName = obj.Name.ToUpper();
                _obj.Entity_Type = 0;
                _obj.CIN_LLPIN = "0";
                _obj.IncorporationDate = DateTime.Now;
                _obj.ROC_Code = 0;

                _obj.Regi_Address_Line1 = obj.AddressLine1.ToUpper();
                _obj.Regi_Address_Line2 = obj.AddressLine2.ToUpper();
                _obj.Regi_StateId = obj.StateID;
                _obj.Regi_CityId = obj.CityID;
                _obj.IsCorp_Office = false;
                _obj.CompanyCategory_Id = 0;

                _obj.IS_Listed = false;
                _obj.Email_Id = obj.EmailID;
                _obj.PAN = "";

                _obj.Customer_Id = obj.CustomerID;
                _obj.CustomerBranchId = obj.ID;

                _obj.Is_Deleted = false;
                _obj.CreatedOn = DateTime.Now;

                _obj.Entity_Type = 0;

                #region set Entity Type from Company Type

                if (obj.ComType == 2)
                {
                    _obj.Entity_Type = 1;
                }
                else if (obj.ComType == 1)
                {
                    _obj.Entity_Type = 2;
                }
                else if (obj.ComType == 3)
                {
                    _obj.Entity_Type = 2;
                    _obj.IS_Listed = true;
                }

                #endregion

                #region set Entity Type from Legal entity Type
                if (obj.LegalEntityTypeID == 6)//Private Limited Company
                {
                    _obj.Entity_Type = 1;
                }
                else if (obj.LegalEntityTypeID == 4)  //Limited Liability Partnership(LLP)
                {
                    _obj.Entity_Type = 3;
                }
                else if (obj.LegalEntityTypeID == 2)  //Public Limited Company (Listed)
                {
                    _obj.Entity_Type = 10;
                    _obj.IS_Listed = true;
                }
                else if (obj.LegalEntityTypeID == 3)  //Public Limited Company (Unlisted)
                {
                    _obj.Entity_Type = 2;
                    _obj.IS_Listed = false;
                }
                else if (obj.LegalEntityTypeID == 1)//Partnership
                {
                    _obj.Entity_Type = 7;
                }
                else if (obj.LegalEntityTypeID == 5)//Proprietorship
                {
                    _obj.Entity_Type = 6;
                }
                else if (obj.LegalEntityTypeID == 8)//One Person Company (O.P.C.)
                {
                    _obj.Entity_Type = 8;
                }
                #endregion

                if (_obj.Entity_Type > 0)
                {
                    entities.BM_EntityMaster.Add(_obj);
                    entities.SaveChanges();
                    result = true;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                result = false;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public bool UpdateEntityFromLegal(CustomerBranch obj)
        {
            var result = false;

            try
            {
                BM_EntityMaster _obj = entities.BM_EntityMaster.Where(k => k.CustomerBranchId == obj.ID && k.Customer_Id == obj.CustomerID && k.Is_Deleted == false).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.CompanyName = obj.Name.ToUpper();
                    _obj.Entity_Type = 0;
                    _obj.IncorporationDate = DateTime.Now;

                    _obj.Regi_Address_Line1 = obj.AddressLine1.ToUpper();
                    _obj.Regi_Address_Line2 = obj.AddressLine2.ToUpper();
                    _obj.Regi_StateId = obj.StateID;
                    _obj.Regi_CityId = obj.CityID;
                    _obj.Email_Id = obj.EmailID;

                    _obj.Is_Deleted = false;
                    _obj.UpdatedOn = DateTime.Now;

                    _obj.Entity_Type = 0;

                    #region set Entity Type from Company Type

                    if (obj.ComType == 2)
                    {
                        _obj.Entity_Type = 1;
                    }
                    else if (obj.ComType == 1)
                    {
                        _obj.Entity_Type = 2;
                    }
                    else if (obj.ComType == 3)
                    {
                        _obj.Entity_Type = 2;
                        _obj.IS_Listed = true;
                    }

                    #endregion

                    #region set Entity Type from Legal entity Type
                    if (obj.LegalEntityTypeID == 6)//Private Limited Company
                    {
                        _obj.Entity_Type = 1;
                    }
                    else if (obj.LegalEntityTypeID == 4)  //Limited Liability Partnership(LLP)
                    {
                        _obj.Entity_Type = 3;
                    }
                    else if (obj.LegalEntityTypeID == 2)  //Public Limited Company (Listed)
                    {
                        _obj.Entity_Type = 10;
                        _obj.IS_Listed = true;
                    }
                    else if (obj.LegalEntityTypeID == 3)  //Public Limited Company (Unlisted)
                    {
                        _obj.Entity_Type = 2;
                        _obj.IS_Listed = false;
                    }
                    else if (obj.LegalEntityTypeID == 1)//Partnership
                    {
                        _obj.Entity_Type = 7;
                    }
                    else if (obj.LegalEntityTypeID == 5)//Proprietorship
                    {
                        _obj.Entity_Type = 6;
                    }
                    else if (obj.LegalEntityTypeID == 8)//One Person Company (O.P.C.)
                    {
                        _obj.Entity_Type = 8;
                    }
                    #endregion

                    if (_obj.Entity_Type > 0)
                    {
                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                result = false;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public string GetFY(long entityId)
        {
            try
            {
                var getFyEntitywise = (from row in entities.BM_EntityMaster where row.Id == entityId && row.Is_Deleted == false select row.FY_CY).FirstOrDefault();
                return getFyEntitywise;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VM_QuestionForShareholding SaveSharesDetails(VM_QuestionForShareholding objsharesholder)
        {
            try
            {
                var savequestionsrelatedShares = (from row in entities.BM_EntityMaster where row.Id == objsharesholder.EntityId select row).FirstOrDefault();
                if (savequestionsrelatedShares != null)
                {
                    savequestionsrelatedShares.IsDematerialisedForm = objsharesholder.rdDematerialised;
                    savequestionsrelatedShares.SharesIsComplitedorPatial = objsharesholder.rdIsdemat;
                    entities.SaveChanges();

                }
                return objsharesholder;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return objsharesholder;
            }
        }

        #region Listed stockExchange
        public IEnumerable<ListofstockExchange> readListedStockExchangeData(int entityID)
        {
            try
            {
                return GetAllstockExchangerelatedvalue(entityID);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }
        #endregion
        private IEnumerable<ListofstockExchange> GetAllstockExchangerelatedvalue(int entityID)
        {
            try
            {
                var getAllstockexchangeMappingDetails = (from row in entities.BM_EntityStockMapping
                                                         where row.EntityId == entityID && row.IsDeleted == false
                                                         select new ListofstockExchange
                                                         {
                                                             EntityId = entityID,
                                                             Id = row.Id,
                                                             staockExchange = row.StockExchangeId,
                                                             scriptCode = row.ScriptCode,
                                                             scriptSymbol = row.ScriptSymbol,
                                                             stockExchangeName = (from st in entities.BM_StockExchange where st.Id == row.StockExchangeId select st.Name).FirstOrDefault()

                                                         }
                                                         ).ToList();
                return getAllstockexchangeMappingDetails;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        public ListofstockExchange Create(ListofstockExchange objlistofstockexc)
        {
            try
            {
                var IsExist = (from row in entities.BM_EntityStockMapping
                               where row.StockExchangeId == objlistofstockexc.staockExchange
                               && row.EntityId == objlistofstockexc.EntityId
                               && row.IsDeleted == false
                               select row).FirstOrDefault();
                if (IsExist == null)
                {
                    BM_EntityStockMapping obj = new BM_EntityStockMapping();
                    obj.StockExchangeId = objlistofstockexc.staockExchange;
                    obj.EntityId = objlistofstockexc.EntityId;
                    obj.ScriptCode = objlistofstockexc.scriptCode;
                    obj.ScriptSymbol = objlistofstockexc.scriptSymbol;
                    obj.IsDeleted = false;
                    entities.BM_EntityStockMapping.Add(obj);
                    entities.SaveChanges();

                    objlistofstockexc.Success = true;
                    objlistofstockexc.Message = "Record saved successfully";
                }
                else
                {
                    objlistofstockexc.Error = true;
                    objlistofstockexc.Message = "Stock Exchange already exist";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                objlistofstockexc.Error = true;
                objlistofstockexc.Message = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objlistofstockexc.Error = true;
                objlistofstockexc.Message = SecretarialConst.Messages.serverError;
            }
            return objlistofstockexc;
        }

        public ListofstockExchange Update(ListofstockExchange objlistofstockexc)
        {
            try
            {
                var IsExist = (from row in entities.BM_EntityStockMapping
                               where row.Id == objlistofstockexc.Id
                               select row).FirstOrDefault();
                if (IsExist != null)
                {

                    IsExist.StockExchangeId = objlistofstockexc.staockExchange;
                    IsExist.ScriptCode = objlistofstockexc.scriptCode;
                    IsExist.ScriptSymbol = objlistofstockexc.scriptSymbol;
                    IsExist.EntityId = objlistofstockexc.EntityId;
                    IsExist.IsDeleted = false;
                    entities.SaveChanges();
                    objlistofstockexc.Success = true;
                    objlistofstockexc.Message = "Record updated successfully";
                }
                else
                {
                    objlistofstockexc.Success = true;
                    objlistofstockexc.Message = "Record not found";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                objlistofstockexc.Error = true;
                objlistofstockexc.Message = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objlistofstockexc.Success = true;
                objlistofstockexc.Message = SecretarialConst.Messages.serverError;
            }
            return objlistofstockexc;
        }

        public ListofstockExchange Destroy(ListofstockExchange objlistofstockexc)
        {
            try
            {
                var IsExist = (from row in entities.BM_EntityStockMapping
                               where row.Id == objlistofstockexc.Id
                               select row)
                               .FirstOrDefault();
                if (IsExist != null)
                {

                    IsExist.IsDeleted = true;
                    entities.SaveChanges();

                    objlistofstockexc.Success = true;
                    objlistofstockexc.Message = "Record deleted successfully";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objlistofstockexc.Error = true;
                objlistofstockexc.Message = SecretarialConst.Messages.serverError;
            }
            return objlistofstockexc;
        }


        #region File Uploading
        public int getCountryId(string country)
        {
            try
            {

                var getCountryId = (from row in entities.Countries
                                    where row.Name.ToLower() == country.ToLower()
                                    && row.IsDeleted == false
                                    select row.ID).FirstOrDefault();
                return getCountryId;
            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public int getCityId(string regi_City)
        {
            try
            {

                var _objcity = (from row in entities.Cities
                                where row.Name.ToLower() == regi_City.ToLower()
                                && row.IsDeleted == false
                                select row.ID).FirstOrDefault();
                return _objcity;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public bool CheckSaveorUpdateforEntity(BM_EntityMaster objentity)
        {
            try
            {

                var checkEntity_Data = (from row in entities.BM_EntityMaster
                                        where row.CIN_LLPIN == objentity.CIN_LLPIN
                                        && row.CompanyName == objentity.CompanyName
                                        && row.Is_Deleted == false
                                        select row).FirstOrDefault();
                if (checkEntity_Data != null)
                {

                    objentity.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
                else
                {
                    if (objentity.Entity_Type == 4 || objentity.Entity_Type == 5)
                    {
                        objentity.CIN_LLPIN = "0";
                    }
                    objentity.Is_Deleted = false;
                    objentity.CreatedOn = DateTime.Now;
                    entities.BM_EntityMaster.Add(objentity);
                    entities.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public int getStateId(string regi_State)
        {
            try
            {

                var _objstateID = (from row in entities.States
                                   where row.Name.ToLower() == regi_State.ToLower()
                                   && row.IsDeleted == false
                                   select row.ID).FirstOrDefault();
                return _objstateID;
            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public int GetRocCode(string ROC_Code)
        {
            try
            {

                var getRocData = (from row in entities.BM_ROC_code
                                  where row.Name.ToLower() == ROC_Code.ToLower()
                                  && row.isDeleted == false
                                  select row.Id).FirstOrDefault();
                return getRocData;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public int GetCompanyCategory(string companyCategory)
        {
            try
            {

                var _objgetcompanyCategoryId = (from row in entities.BM_CompanyCategory
                                                where row.Category == companyCategory
                                                select row.Id).FirstOrDefault();
                return _objgetcompanyCategoryId;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        #region Import MGT7
        public VMEntityFileUpload ImportDatafrom_MGT7(VMEntityFileUpload objfileupload)
        {
            objfileupload.Response = new Response();
            try
            {
                #region variable define for Mgt-7 Read for Entity
                long EntityID = -1;
                string Message = "";
                string EntityName = string.Empty;
                string CIN = string.Empty;
                bool IsListed = false;
                string PAN = string.Empty;
                string GLN = string.Empty;
                string RegisterAddress = string.Empty;
                string RegisterAddressLine1 = string.Empty;
                string RegisterAddressLine2 = string.Empty;
                int stateId = -1;
                int CityId = -1;
                string PIN = string.Empty;
                string EmilId = string.Empty;
                long PhoneNo = 0;
                DateTime dateofIncorporate = DateTime.Now;
                string Website = string.Empty;
                string TypeofCompany = string.Empty;
                int Type = -1;
                string CategoryofCompany = string.Empty;
                string CategoryofCompanySub = string.Empty;
                int CompanyCategoryType = -1;
                int CompanyCategorysubType = -1;
                string RegistrationNumber = string.Empty;
                string FY = string.Empty;
                #region Company is Listed
                string RTA_CIN = string.Empty;
                string RTA_Name = string.Empty;
                string RTA_Address = string.Empty;
                int noofsubcompany = -1;

                List<SubCompanyType> objlistofsubcompany = new List<SubCompanyType>();
                #endregion Company is Listed


                #endregion

                #region variable define for Mgt7 for Capital Master Equity shares
                string IsCapitalShares = string.Empty;
                decimal TotNoofEquAuthorized = 0;
                decimal TotNoofEquIssuedAuthorized = 0;
                decimal TotNoofEquSubscribed = 0;
                decimal TotNoofEqupaidUp = 0;

                decimal TotAmtofEquAuthorized = 0;
                decimal TotAmtofEquIssued = 0;
                decimal TotAmtofEquSubscribe = 0;
                decimal TotAmtofEquPaidup = 0;

                int NoofClass = 1;
                long NoofEquAuthorized = 0;
                long NoofEquIssued = 0;
                long NoofSubscribed = 0;
                long NoofPaidup = 0;

                long NomvalperSahreEquAuthorized = 0;
                long NomvalperSahreEquIssued = 0;
                long NomvalperSahreEquSubscribed = 0;
                long NomvalperSahreEquPaidup = 0;
                int j = 0;
                #endregion

                #region variable define for Mgt7 for Capital Master Prefrence shares 04-02-2021

                decimal TotNoofPreAuthorized = 0;
                decimal TotNoofPreIssuedAuthorized = 0;
                decimal TotNoofPreSubscribed = 0;
                decimal TotNoofPrepaidUp = 0;

                decimal TotAmtofPreAuthorized = 0;
                decimal TotAmtofPreIssued = 0;
                decimal TotAmtofPreSubscribe = 0;
                decimal TotAmtofPrePaidup = 0;

                //int NoofClass = 1;
                long NoofPreAuthorized = 0;
                long NoofPreIssued = 0;
                long NoofPreSubscribed = 0;

                long NoofPrePaidup = 0;

                long NomvalperSahrePreAuthorized = 0;
                long NomvalperSahrePreIssued = 0;
                long NomvalperSahrePreSubscribed = 0;
                long NomvalperSahrePrePaidup = 0;
                int k = 0;
                #endregion

                #region variable define for Mgt7 for Capital Master Debenture added on 04-02-2021

                long NO_UNITS_NCD = 0;
                decimal NOM_VAL_UNIT_NCD = 0;
                decimal TOTAL_VAL_NCD = 0;

                long NO_UNITS_PCD = 0;
                decimal NOM_VAL_UNIT_PCD = 0;
                decimal TOTAL_VAL_PCD = 0;

                long NO_UNITS_FCD = 0;
                decimal NOM_VAL_UNIT_FCD = 0;
                decimal TOTAL_VAL_FCD = 0;

                decimal TOT_TOTAL_VAL = 0;

                #endregion


                if (objfileupload.File != null)
                {
                    if (objfileupload.File.ContentLength > 0)
                    {
                        string myFilePath = objfileupload.File.FileName;
                        string ext = System.IO.Path.GetExtension(myFilePath);
                        if (ext == ".pdf")
                        {

                            string _path;

                            _path = GetFormsPath(objfileupload);
                            if (!(string.IsNullOrEmpty(_path)))
                            {
                                PdfReader pdfReader = new PdfReader(_path);
                                SubCompanyType objcompanysubtype = new SubCompanyType();
                                if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
                                {
                                    foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                    {
                                        #region capture entity name
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NAME_OF_COMPANY[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                EntityName = (de.Value.InnerText).Trim();
                                            }
                                        }
                                        #endregion

                                        #region capture cin and entitytype
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].CIN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                CIN = (de.Value.InnerText).Trim();
                                                if (CIN.IndexOf("L") == 0)
                                                {
                                                    IsListed = true;

                                                    Type = 10;
                                                }
                                                else if (CIN.IndexOf("U") == 0)
                                                {
                                                    IsListed = false;
                                                }

                                                RegistrationNumber = CIN.Substring(15);
                                            }
                                        }
                                        #endregion

                                        #region capture PAN
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].IT_PAN_OF_COMPNY[0]")
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                PAN = de.Value.InnerText;
                                            }
                                        }
                                        #endregion

                                        #region capture GLN
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].GLN[0]")
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                GLN = de.Value.InnerText;
                                            }
                                        }
                                        #endregion

                                        #region capture address
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].REG_OFFC_ADDRESS[0]")
                                        {
                                            string pattern = @"^[1-9][0-9]{5}$";
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                RegisterAddress = de.Value.InnerText;

                                                List<string> address = RegisterAddress.Split('\n').ToList();
                                                if (address.Count() > 0)
                                                {
                                                    foreach (var item in address)
                                                    {
                                                        var matchTuple = Regex.Match(item, pattern, RegexOptions.IgnoreCase);
                                                        if (matchTuple.Success)
                                                        {
                                                            PIN = matchTuple.ToString();
                                                        }
                                                    }
                                                    if (address[0] != null)
                                                    {
                                                        RegisterAddressLine1 = address[0].ToString();
                                                    }
                                                    if (address.Count() > 1)
                                                    {
                                                        if (address[1] != null)
                                                        {
                                                            RegisterAddressLine2 = address[1].ToString();
                                                        }
                                                        if (address[4] != null)
                                                        {
                                                            stateId = GetRegisterStateId(address[4]);
                                                            if (stateId == 0)
                                                            {
                                                                stateId = GetRegisterStateId(address[3]);
                                                            }
                                                            if (stateId == 0)
                                                            {
                                                                stateId = GetRegisterStateId(address[2]);
                                                            }
                                                        }
                                                        if (address[2] != null && stateId > 0)
                                                        {
                                                            CityId = getCityRegisterID(stateId, address[2].ToString());
                                                            if (CityId == 0)
                                                            {
                                                                CityId = getCityRegisterID(stateId, address[1].ToString());
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        #endregion

                                        #region capture email
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].EMAIL_ID_COMPANY[0]")
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                EmilId = de.Value.InnerText;
                                            }
                                        }
                                        #endregion

                                        #region capture phone No
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].PHONE_NUMBER[0]")
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                PhoneNo = Convert.ToInt64(de.Value.InnerText);
                                            }
                                        }
                                        #endregion

                                        #region capture date of Incorporation
                                        if (de.Key.ToString() == "data[0].INCORPORATION_DATE[0]")
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                dateofIncorporate = Convert.ToDateTime(de.Value.InnerText);
                                            }
                                        }
                                        #endregion

                                        #region capture website
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].WEBSITE[0]")
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                Website = de.Value.InnerText.Trim();
                                            }
                                        }
                                        #endregion

                                        #region capture company Type
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TYPE_OF_COMPANY[0]" && (Type == 0 || Type == -1))
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TypeofCompany = de.Value.InnerText.Trim();
                                                if (TypeofCompany.Trim() == "PRIV")
                                                {
                                                    Type = 1;
                                                }
                                                else if (TypeofCompany.Trim() == "PUBC")
                                                {
                                                    Type = 2;
                                                }

                                            }
                                        }
                                        #endregion

                                        #region capture company cateory
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].CATEGORY_COMPANY[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                CategoryofCompany = de.Value.InnerText;
                                            }
                                            if (CategoryofCompany == "CLAG")
                                            {
                                                CompanyCategoryType = 2;
                                            }
                                            else if (CategoryofCompany == "ULCM")
                                            {
                                                CompanyCategoryType = 3;
                                            }
                                            else
                                            {
                                                CompanyCategoryType = 1;
                                            }
                                        }
                                        #endregion

                                        #region capture sub company
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].SUB_CATEGORY_COM[0]")
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                CategoryofCompanySub = de.Value.InnerText;
                                            }
                                            if (CategoryofCompanySub == "NGOV")
                                            {
                                                CompanyCategorysubType = 1;
                                            }
                                            else if (CategoryofCompanySub == "UGCM")
                                            {
                                                CompanyCategorysubType = 2;
                                            }
                                            else if (CategoryofCompanySub == "STGM")
                                            {
                                                CompanyCategorysubType = 3;
                                            }
                                            else if (CategoryofCompanySub == "GACM")
                                            {
                                                CompanyCategorysubType = 4;
                                            }
                                            else if (CategoryofCompanySub == "SUFR")
                                            {
                                                CompanyCategorysubType = 5;
                                            }
                                        }
                                        #endregion

                                        #region capture FY
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].FY_FROM_DATE[0]" || de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].FY_END_DATE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                DateTime FYDate = Convert.ToDateTime(de.Value.InnerText);

                                                int Month = FYDate.Month;
                                                if (Month == 3 || Month == 4)
                                                {
                                                    FY = "FY";
                                                }
                                                else if (Month == 1 || Month == 12)
                                                {
                                                    FY = "CY";
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Details Fill for RTA For Listed Company
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].CIN_REG_TA[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                RTA_CIN = de.Value.InnerText.Trim();
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NAME_REG_TA[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                RTA_Name = de.Value.InnerText.Trim();
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].OFFICE_ADDRESS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                RTA_Address = de.Value.InnerText.Trim();
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NO_COMPANIES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                noofsubcompany = Convert.ToInt32(de.Value.InnerText.Trim());
                                            }
                                        }

                                        #endregion

                                        #region Capital Shares Details Equity shares
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].RB_SHARE_CAPITAL[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                IsCapitalShares = de.Value.InnerText.Trim();
                                            }
                                        }


                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_A_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotNoofEquAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_I_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotNoofEquIssuedAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_S_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotNoofEquSubscribed = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_P_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotNoofEqupaidUp = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                            Console.WriteLine("Total number of equity shares Paid up" + " : " + de.Value.InnerText);
                                        }

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_A_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotAmtofEquAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_I_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotAmtofEquIssued = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_S_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotAmtofEquSubscribe = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_P_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotAmtofEquPaidup = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NO_OF_CLASSES_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NoofClass = Convert.ToInt16(de.Value.InnerText.Trim());
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_A_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NoofEquAuthorized = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_I_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NoofEquIssued = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_S_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NoofSubscribed = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_P_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NoofPaidup = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_A_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NomvalperSahreEquAuthorized = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_I_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NomvalperSahreEquIssued = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_S_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NomvalperSahreEquSubscribed = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_P_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NomvalperSahreEquPaidup = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }

                                        #endregion

                                        #region Capital Shares Details Prefrence shares

                                        #region Capital Shares Total Number of Authorized prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NO_PS_A_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotNoofPreAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        #endregion


                                        #region Capital Shares Total Number of Issued shares prefrence
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NO_PS_I_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotNoofPreIssuedAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        #endregion

                                        #region Capital Shares Total Number of subscribed shares prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NO_PS_S_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotNoofPreSubscribed = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        #endregion

                                        #region Capital Shares Total Number of Paid up shares prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NO_PS_P_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotNoofPrepaidUp = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }

                                        }
                                        #endregion

                                        #region Capital Shares Total amount of Authorized  prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].TOT_AMT_PS_A_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotAmtofPreAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        #endregion

                                        #region Capital Shares Total amount of Issued prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].TOT_AMT_PS_I_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotAmtofPreIssued = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        #endregion

                                        #region Capital Shares Total amount of subscribed prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].TOT_AMT_PS_S_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotAmtofPreSubscribe = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        #endregion

                                        #region Capital Shares Total amount of paidup prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].TOT_AMT_ES_P_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotAmtofPrePaidup = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        #endregion


                                        #region Capital Shares Total number of Authorized prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NO_PS_A_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NoofPreAuthorized = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        #endregion

                                        #region Capital Shares Total number of Issued prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NO_PS_I_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NoofPreIssued = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        #endregion

                                        #region Capital Shares Total number of Subscribed prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NO_PS_S_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NoofPreSubscribed = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }

                                        #endregion

                                        #region Capital Shares Total number of paidup prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NO_PS_P_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NoofPrePaidup = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        #endregion

                                        #region Capital Shares Total number of paidup prefrence

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NOM_VAL_PS_A_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NomvalperSahrePreAuthorized = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        #endregion

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NOM_VAL_PS_I_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NomvalperSahrePreIssued = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NOM_VAL_PS_S_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NomvalperSahrePreSubscribed = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_B[0].DATA[0].NOM_VAL_PS_P_CAP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NomvalperSahrePrePaidup = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }

                                        #endregion

                                        #region Capital Shares Details Debenture shares

                                        #region No Unit Non Convertibele

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NO_UNITS_NCD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NO_UNITS_NCD = Convert.ToInt64(de.Value.InnerText.Trim());
                                            }
                                        }
                                        #endregion


                                        #region Nominal value Non Convertible
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NOM_VAL_UNIT_NCD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NOM_VAL_UNIT_NCD = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        #endregion

                                        #region Total value Non Convertible

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOTAL_VAL_NCD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TOTAL_VAL_NCD = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        #endregion

                                        #region NO Unit Partialy Convertable

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NO_UNITS_PCD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NO_UNITS_PCD = Convert.ToInt64(de.Value.InnerText.Trim());
                                            }

                                        }
                                        #endregion

                                        #region Nominal value Partialy Convertable

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NOM_VAL_UNIT_PCD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NOM_VAL_UNIT_PCD = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        #endregion

                                        #region Total value Partialy Convertable

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOTAL_VAL_PCD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TOTAL_VAL_PCD = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        #endregion

                                        #region No Unit Fully Convertable

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NO_UNITS_FCD[0]")
                                        {
                                            decimal number;
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                number = Convert.ToDecimal(de.Value.InnerText.Trim());
                                                NO_UNITS_FCD = (long)number;
                                            }
                                        }

                                        #endregion

                                        #region Nomina value Fully Convertable

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NOM_VAL_UNIT_FCD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                NOM_VAL_UNIT_FCD = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        #endregion


                                        #region Total value Fully Convertable

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOTAL_VAL_FCD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TOTAL_VAL_FCD = (decimal)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        #endregion

                                        #region Total Debenture

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_TOTAL_VAL[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TOT_TOTAL_VAL = (decimal)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        #endregion


                                        #endregion
                                    }

                                    if (!String.IsNullOrEmpty(CIN) && Type > 0)
                                    {
                                        com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                                        {
                                            //ID = _objentity.CustomerBranchId,
                                            Name = EntityName,
                                            //Type = Convert.ToByte(ddlType.SelectedValue),
                                            // ComType = Convert.ToByte(comType),
                                            //AddressLine1 = _objentity.Regi_Address_Line1,
                                            //AddressLine2 = _objentity.Regi_Address_Line2,
                                            //StateID = _objentity.Regi_StateId,
                                            //CityID = _objentity.Regi_CityId,
                                            //Others = "",
                                            //PinCode = _objentity.Regi_PINCode,
                                            ContactPerson = "",
                                            Landline = "",
                                            Mobile = "",
                                            EmailID = EmilId,
                                            CustomerID = objfileupload.CustomerId,
                                            //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                                            //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                                        };

                                        customerBranch = CreateCustomerBranch(customerBranch, Type, IsListed, out Message);

                                        if (customerBranch.ID > 0)
                                        {
                                            BM_SubEntityMapping objsubentity = new BM_SubEntityMapping();
                                            var checkEntity = (from row in entities.BM_EntityMaster where row.CIN_LLPIN == CIN && row.Is_Deleted == false && row.Customer_Id == objfileupload.CustomerId select row).FirstOrDefault();
                                            if (checkEntity == null)
                                            {
                                                checkEntity = (from row in entities.BM_EntityMaster where row.CompanyName == EntityName && row.Is_Deleted == false && row.Customer_Id == objfileupload.CustomerId select row).FirstOrDefault();//check for entity Name 
                                            }
                                            //var checkEntity = (from row in entities.BM_EntityMaster where row.CIN_LLPIN == CIN && row.Is_Deleted == false select row).FirstOrDefault();
                                            if (checkEntity == null)
                                            {

                                                BM_EntityMaster objentityMaster = new BM_EntityMaster
                                                {
                                                    CompanyName = EntityName,

                                                    CIN_LLPIN = CIN,
                                                    GLN = GLN,
                                                    IncorporationDate = dateofIncorporate,
                                                    Registration_No = RegistrationNumber,
                                                    Regi_Address_Line1 = RegisterAddressLine1,
                                                    Regi_Address_Line2 = RegisterAddressLine2,
                                                    Regi_StateId = stateId,
                                                    Regi_CityId = CityId,
                                                    Regi_PINCode = PIN,
                                                    CompanySubCategory = CompanyCategorysubType,
                                                    CompanyCategory_Id = CompanyCategoryType,
                                                    IS_Listed = IsListed,
                                                    Email_Id = EmilId,
                                                    FY_CY = FY,
                                                    WebSite = Website,
                                                    PAN = PAN,
                                                    CreatedOn = DateTime.Now,
                                                    CreatedBy = objfileupload.UserId,
                                                    Is_Deleted = false,
                                                    Customer_Id = objfileupload.CustomerId,
                                                    CustomerBranchId = customerBranch.ID,
                                                };
                                                if (IsListed)
                                                {
                                                    objentityMaster.RTA_CompanyName = RTA_Name;
                                                    objentityMaster.RTA_Address = RTA_Name;
                                                    objentityMaster.RTA_CIN = RTA_CIN;
                                                    objentityMaster.Entity_Type = 10;
                                                }
                                                else
                                                {
                                                    objentityMaster.Entity_Type = Type;
                                                }
                                                entities.BM_EntityMaster.Add(objentityMaster);
                                                entities.SaveChanges();
                                                if (objentityMaster.Id > 0)
                                                {
                                                    #region for add subsidary Company joint Company 
                                                    while (j < noofsubcompany)
                                                    {
                                                        foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                                        {

                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].NAME_COMPANY[0]")
                                                            {

                                                                if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                                {
                                                                    objcompanysubtype.SubCompanyName = de.Value.InnerText.Trim();

                                                                }
                                                            }

                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].CIN_FCRN[0]")
                                                            {
                                                                if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                                {
                                                                    objcompanysubtype.CIN = de.Value.InnerText.Trim();

                                                                }
                                                            }



                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].HOLD_SUB_ASSOC[0]")
                                                            {
                                                                if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                                {
                                                                    if (de.Value.InnerText.Trim() == "SUBS")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 3;
                                                                    }
                                                                    else if (de.Value.InnerText.Trim() == "JVEN")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 4;
                                                                    }
                                                                    else if (de.Value.InnerText.Trim() == "ASSC")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 1;
                                                                    }
                                                                    else
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 2;
                                                                    }
                                                                }
                                                            }

                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].PERCENT_SHARE[0]")
                                                            {
                                                                if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                                {
                                                                    objcompanysubtype.PersentageofShareholding = Convert.ToDecimal(de.Value.InnerText.Trim());
                                                                }
                                                            }
                                                        }

                                                        if (objcompanysubtype.CIN != null && objcompanysubtype.CIN != "")
                                                        {
                                                            var checkdetailsofsubcompany = (from s in entities.BM_SubEntityMapping where s.CIN == objcompanysubtype.CIN select s).FirstOrDefault();
                                                            if (checkdetailsofsubcompany == null)
                                                            {
                                                                objsubentity.EntityId = objentityMaster.Id;
                                                                objsubentity.CIN = objcompanysubtype.CIN;
                                                                objsubentity.ParentCIN = objentityMaster.CIN_LLPIN;
                                                                objsubentity.NameofCompany = objcompanysubtype.SubCompanyName;
                                                                objsubentity.CompanysubType = objcompanysubtype.subcompanyTypeID;
                                                                objsubentity.PerofShareHeld = Convert.ToInt64(objcompanysubtype.PersentageofShareholding);
                                                                objsubentity.isActive = true;
                                                                objsubentity.Createdon = DateTime.Now;
                                                                objsubentity.CustomerId = (int)objentityMaster.Customer_Id;
                                                                entities.BM_SubEntityMapping.Add(objsubentity);
                                                                entities.SaveChanges();
                                                            }
                                                            else
                                                            {
                                                                checkdetailsofsubcompany.EntityId = objentityMaster.Id;
                                                                checkdetailsofsubcompany.CIN = objcompanysubtype.CIN;
                                                                checkdetailsofsubcompany.ParentCIN = objentityMaster.CIN_LLPIN;
                                                                checkdetailsofsubcompany.NameofCompany = objcompanysubtype.SubCompanyName;
                                                                checkdetailsofsubcompany.CompanysubType = objcompanysubtype.subcompanyTypeID;
                                                                checkdetailsofsubcompany.PerofShareHeld = Convert.ToInt64(objcompanysubtype.PersentageofShareholding);
                                                                checkdetailsofsubcompany.isActive = true;
                                                                checkdetailsofsubcompany.Createdon = DateTime.Now;
                                                                checkdetailsofsubcompany.CustomerId = (int)objentityMaster.Customer_Id;
                                                                //entities.BM_SubEntityMapping.Add(objsubentity);
                                                                entities.SaveChanges();

                                                            }
                                                        }
                                                        j++;
                                                    }
                                                    #endregion

                                                    EntityID = objentityMaster.Id;

                                                }
                                            }
                                            else
                                            {
                                                checkEntity.CompanyName = EntityName;
                                                checkEntity.Entity_Type = Type;
                                                checkEntity.CIN_LLPIN = CIN;
                                                checkEntity.GLN = GLN;
                                                checkEntity.IncorporationDate = dateofIncorporate;
                                                checkEntity.Registration_No = RegistrationNumber;
                                                checkEntity.Regi_Address_Line1 = RegisterAddressLine1;
                                                checkEntity.Regi_Address_Line2 = RegisterAddressLine2;
                                                checkEntity.Regi_StateId = stateId;
                                                checkEntity.Regi_CityId = CityId;
                                                checkEntity.CompanyCategory_Id = CompanyCategorysubType;
                                                checkEntity.IS_Listed = IsListed;
                                                checkEntity.Email_Id = EmilId;
                                                checkEntity.WebSite = Website;
                                                checkEntity.PAN = PAN;
                                                checkEntity.UpdatedOn = DateTime.Now;
                                                checkEntity.UpdatedBy = objfileupload.UserId;
                                                // checkEntity.CustomerBranchId = customerBranch.ID;
                                                checkEntity.Is_Deleted = false;
                                                checkEntity.Customer_Id = objfileupload.CustomerId;
                                                checkEntity.FY_CY = FY;
                                                if (IsListed)
                                                {
                                                    checkEntity.RTA_CompanyName = RTA_Name;
                                                    checkEntity.RTA_Address = RTA_Address;
                                                    checkEntity.RTA_CIN = RTA_CIN;
                                                }
                                                entities.SaveChanges();
                                                if (checkEntity.Id > 0)
                                                {
                                                    #region for add subsidary Company joint Company 
                                                    while (j < noofsubcompany)
                                                    {
                                                        foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                                        {

                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].NAME_COMPANY[0]")
                                                            {

                                                                if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                                {
                                                                    objcompanysubtype.SubCompanyName = de.Value.InnerText.Trim();


                                                                }
                                                            }

                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].CIN_FCRN[0]")
                                                            {
                                                                //if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                                //{
                                                                objcompanysubtype.CIN = de.Value.InnerText.Trim();

                                                                //}
                                                            }



                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].HOLD_SUB_ASSOC[0]")
                                                            {
                                                                if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                                {
                                                                    if (de.Value.InnerText.Trim() == "SUBS")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 3;
                                                                    }
                                                                    else if (de.Value.InnerText.Trim() == "JVEN")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 4;
                                                                    }
                                                                    else if (de.Value.InnerText.Trim() == "ASSC")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 1;
                                                                    }
                                                                    else
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 2;
                                                                    }

                                                                }
                                                            }
                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].PERCENT_SHARE[0]")
                                                            {
                                                                if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                                {
                                                                    objcompanysubtype.PersentageofShareholding = Convert.ToDecimal(de.Value.InnerText.Trim());

                                                                }
                                                            }
                                                        }
                                                        if (objcompanysubtype.CIN != null && objcompanysubtype.CIN != "")
                                                        {
                                                            var checkdetailsofsubcompany = (from s in entities.BM_SubEntityMapping where s.CIN == objcompanysubtype.CIN select s).FirstOrDefault();
                                                            if (checkdetailsofsubcompany == null)
                                                            {
                                                                objsubentity.EntityId = checkEntity.Id;
                                                                objsubentity.CIN = objcompanysubtype.CIN;
                                                                objsubentity.ParentCIN = checkEntity.CIN_LLPIN;
                                                                objsubentity.NameofCompany = objcompanysubtype.SubCompanyName;
                                                                objsubentity.CompanysubType = objcompanysubtype.subcompanyTypeID;
                                                                objsubentity.PerofShareHeld = objcompanysubtype.PersentageofShareholding;
                                                                objsubentity.isActive = true;
                                                                objsubentity.Createdon = DateTime.Now;
                                                                objsubentity.CustomerId = (int)checkEntity.Customer_Id;
                                                                entities.BM_SubEntityMapping.Add(objsubentity);
                                                                entities.SaveChanges();
                                                            }
                                                            else
                                                            {
                                                                checkdetailsofsubcompany.EntityId = checkEntity.Id;
                                                                checkdetailsofsubcompany.CIN = objcompanysubtype.CIN;
                                                                checkdetailsofsubcompany.ParentCIN = checkEntity.CIN_LLPIN;
                                                                checkdetailsofsubcompany.NameofCompany = objcompanysubtype.SubCompanyName;
                                                                checkdetailsofsubcompany.CompanysubType = objcompanysubtype.subcompanyTypeID;
                                                                checkdetailsofsubcompany.PerofShareHeld = Convert.ToInt64(objcompanysubtype.PersentageofShareholding);
                                                                checkdetailsofsubcompany.isActive = true;
                                                                checkdetailsofsubcompany.Createdon = DateTime.Now;
                                                                checkdetailsofsubcompany.CustomerId = (int)checkEntity.Customer_Id;
                                                                //entities.BM_SubEntityMapping.Add(objsubentity);
                                                                entities.SaveChanges();

                                                            }
                                                        }
                                                        j++;
                                                    }
                                                    #endregion
                                                    EntityID = checkEntity.Id;
                                                }
                                            }
                                            long CapitalId = 0;
                                            if (EntityID > 0)
                                            {
                                                BM_CapitalMaster objCapitalMaster = new BM_CapitalMaster();
                                                var checkforCapitalMaster = (from C in entities.BM_CapitalMaster where C.Entity_Id == EntityID select C).FirstOrDefault();
                                                if (checkforCapitalMaster == null)
                                                {
                                                    objCapitalMaster.Entity_Id = (int)EntityID;
                                                    objCapitalMaster.CustomerId = objfileupload.CustomerId;
                                                    objCapitalMaster.IsDebenger = false;
                                                    objCapitalMaster.IsPrefrence = false;
                                                    objCapitalMaster.IsUnclassified = false;

                                                    objCapitalMaster.TotNoAuthorizedCapita = TotNoofEquAuthorized;
                                                    objCapitalMaster.TotNoIssuedCapital = TotNoofEquIssuedAuthorized;
                                                    objCapitalMaster.TotNoSubscribCapital = TotNoofEquSubscribed;
                                                    objCapitalMaster.TotNoPaidupCapital = TotNoofEqupaidUp;

                                                    objCapitalMaster.TotamtAuthorizedCapita = TotAmtofEquAuthorized;
                                                    objCapitalMaster.TotamtNoIssuedCapital = TotAmtofEquIssued;
                                                    objCapitalMaster.TotamtNoSubscribCapital = TotAmtofEquSubscribe;
                                                    objCapitalMaster.TotamtNoPaidupCapital = TotAmtofEquPaidup;



                                                    objCapitalMaster.Pri_TotamtAuthorizedCapita = TotAmtofPreAuthorized;
                                                    objCapitalMaster.Pri_TotamtNoIssuedCapital = TotAmtofPreIssued;
                                                    objCapitalMaster.Pri_TotamtNoSubscribCapital = TotAmtofPreSubscribe;
                                                    objCapitalMaster.Pri_TotamtNoPaidupCapital = TotAmtofPrePaidup;

                                                    objCapitalMaster.Unclassified_AuhorisedCapital = 0;

                                                    objCapitalMaster.Pri_TotNoAuthorizedCapita = TotNoofPreAuthorized;
                                                    objCapitalMaster.Pri_TotNoIssuedCapital = TotNoofPreIssuedAuthorized;
                                                    objCapitalMaster.Pri_TotNoSubscribCapital = TotNoofPreSubscribed;
                                                    objCapitalMaster.Pri_TotNoPaidupCapital = TotNoofPrepaidUp;


                                                    objCapitalMaster.IsActive = true;
                                                    objCapitalMaster.Createdby = objfileupload.UserId;
                                                    objCapitalMaster.CreatedOn = DateTime.Now;
                                                    objCapitalMaster.AuthorizedCapital = objCapitalMaster.TotamtAuthorizedCapita + objCapitalMaster.Pri_TotamtAuthorizedCapita;
                                                    if (objCapitalMaster.Pri_TotamtAuthorizedCapita > 0)
                                                    {
                                                        objCapitalMaster.IsPrefrence = true;
                                                    }
                                                    if (TOT_TOTAL_VAL > 0)
                                                    {
                                                        objCapitalMaster.IsDebenger = true;
                                                    }
                                                    entities.BM_CapitalMaster.Add(objCapitalMaster);
                                                    entities.SaveChanges();

                                                    CapitalId = objCapitalMaster.Id;
                                                }
                                                else
                                                {
                                                    checkforCapitalMaster.Entity_Id = (int)EntityID;
                                                    checkforCapitalMaster.CustomerId = objfileupload.CustomerId;
                                                    checkforCapitalMaster.IsDebenger = false;
                                                    checkforCapitalMaster.IsPrefrence = false;
                                                    checkforCapitalMaster.IsUnclassified = false;

                                                    checkforCapitalMaster.TotNoAuthorizedCapita = TotNoofEquAuthorized;
                                                    checkforCapitalMaster.TotNoIssuedCapital = TotNoofEquIssuedAuthorized;
                                                    checkforCapitalMaster.TotNoSubscribCapital = TotNoofEquSubscribed;
                                                    checkforCapitalMaster.TotNoPaidupCapital = TotNoofEqupaidUp;

                                                    checkforCapitalMaster.TotamtAuthorizedCapita = TotAmtofEquAuthorized;
                                                    checkforCapitalMaster.TotamtNoIssuedCapital = TotAmtofEquIssued;
                                                    checkforCapitalMaster.TotamtNoSubscribCapital = TotAmtofEquSubscribe;
                                                    checkforCapitalMaster.TotamtNoPaidupCapital = TotAmtofEquPaidup;

                                                    checkforCapitalMaster.Pri_TotamtAuthorizedCapita = TotAmtofPreAuthorized;
                                                    checkforCapitalMaster.Pri_TotamtNoIssuedCapital = TotAmtofPreIssued;
                                                    checkforCapitalMaster.Pri_TotamtNoSubscribCapital = TotAmtofPreSubscribe;
                                                    checkforCapitalMaster.Pri_TotamtNoPaidupCapital = TotAmtofPrePaidup;

                                                    checkforCapitalMaster.Unclassified_AuhorisedCapital = 0;

                                                    checkforCapitalMaster.Pri_TotNoAuthorizedCapita = TotNoofPreAuthorized;
                                                    checkforCapitalMaster.Pri_TotNoIssuedCapital = TotNoofPreIssuedAuthorized;
                                                    checkforCapitalMaster.Pri_TotNoSubscribCapital = TotNoofPreSubscribed;
                                                    checkforCapitalMaster.Pri_TotNoPaidupCapital = TotNoofPrepaidUp;


                                                    checkforCapitalMaster.IsActive = true;
                                                    checkforCapitalMaster.Createdby = objfileupload.UserId;
                                                    checkforCapitalMaster.CreatedOn = DateTime.Now;
                                                    checkforCapitalMaster.AuthorizedCapital = checkforCapitalMaster.TotamtAuthorizedCapita + checkforCapitalMaster.Pri_TotamtAuthorizedCapita;
                                                    if (checkforCapitalMaster.Pri_TotamtAuthorizedCapita > 0)
                                                    {
                                                        checkforCapitalMaster.IsPrefrence = true;
                                                    }
                                                    if (TOT_TOTAL_VAL > 0)
                                                    {
                                                        checkforCapitalMaster.IsDebenger = true;
                                                    }
                                                    entities.SaveChanges();
                                                    CapitalId = checkforCapitalMaster.Id;
                                                }

                                                if (CapitalId > 0 && EntityID > 0)
                                                {
                                                    var checkforCapitalMasterShares = (from CS in entities.BM_Share where CS.CapitalMasterId == CapitalId && CS.Sharetype == "E" select CS).FirstOrDefault();
                                                    if (checkforCapitalMasterShares == null)
                                                    {
                                                        BM_Share objshares = new BM_Share();
                                                        objshares.CapitalMasterId = (int)CapitalId;
                                                        objshares.CustomerId = objfileupload.CustomerId;
                                                        objshares.Sharetype = "E";
                                                        objshares.Shares_Class = 1;
                                                        objshares.No_AuhorisedCapital = NoofEquAuthorized;
                                                        objshares.No_IssuedCapital = NoofEquIssued;
                                                        objshares.No_SubscribedCapital = NoofSubscribed;
                                                        objshares.No_Paid_upCapital = NoofPaidup;
                                                        objshares.Nominalper_AuhorisedCapital = NomvalperSahreEquAuthorized;
                                                        objshares.Nominalper_IssuedCapital = NomvalperSahreEquIssued;
                                                        objshares.Nominalper_SubscribedCapital = NomvalperSahreEquSubscribed;
                                                        objshares.Nominalper_Paid_upCapital = NomvalperSahreEquPaidup;
                                                        objshares.totamt_AuhorisedCapital = NoofEquAuthorized * NomvalperSahreEquAuthorized;
                                                        objshares.totamt_IssuedCapital = NoofEquIssued * NomvalperSahreEquIssued;
                                                        objshares.totamt_SubscribedCapital = NoofSubscribed * NomvalperSahreEquSubscribed;
                                                        objshares.totamt_upCapital = NoofPaidup * NomvalperSahreEquPaidup;
                                                        objshares.IsActive = true;
                                                        entities.BM_Share.Add(objshares);
                                                        entities.SaveChanges();
                                                    }
                                                    else
                                                    {
                                                        checkforCapitalMasterShares.CapitalMasterId = (int)CapitalId;
                                                        checkforCapitalMasterShares.CustomerId = objfileupload.CustomerId;
                                                        checkforCapitalMasterShares.Sharetype = "E";
                                                        checkforCapitalMasterShares.Shares_Class = 1;
                                                        checkforCapitalMasterShares.No_AuhorisedCapital = NoofEquAuthorized;
                                                        checkforCapitalMasterShares.No_IssuedCapital = NoofEquIssued;
                                                        checkforCapitalMasterShares.No_SubscribedCapital = NoofSubscribed;
                                                        checkforCapitalMasterShares.No_Paid_upCapital = NoofPaidup;
                                                        checkforCapitalMasterShares.Nominalper_AuhorisedCapital = NomvalperSahreEquAuthorized;
                                                        checkforCapitalMasterShares.Nominalper_IssuedCapital = NomvalperSahreEquIssued;
                                                        checkforCapitalMasterShares.Nominalper_SubscribedCapital = NomvalperSahreEquSubscribed;
                                                        checkforCapitalMasterShares.Nominalper_Paid_upCapital = NomvalperSahreEquPaidup;
                                                        checkforCapitalMasterShares.totamt_AuhorisedCapital = NoofEquAuthorized * NomvalperSahreEquAuthorized;
                                                        checkforCapitalMasterShares.totamt_IssuedCapital = NoofEquIssued * NomvalperSahreEquIssued;
                                                        checkforCapitalMasterShares.totamt_SubscribedCapital = NoofSubscribed * NomvalperSahreEquSubscribed;
                                                        checkforCapitalMasterShares.totamt_upCapital = NoofPaidup * NomvalperSahreEquPaidup;
                                                        checkforCapitalMasterShares.IsActive = true;

                                                        entities.SaveChanges();
                                                    }

                                                    var checkforCapitalMasterPreShares = (from CS in entities.BM_Share where CS.CapitalMasterId == CapitalId && CS.Sharetype == "P" select CS).FirstOrDefault();
                                                    if (checkforCapitalMasterPreShares == null)
                                                    {
                                                        BM_Share objshares = new BM_Share();
                                                        objshares.CapitalMasterId = (int)CapitalId;
                                                        objshares.CustomerId = objCapitalMaster.CustomerId;
                                                        objshares.Sharetype = "P";
                                                        objshares.Shares_Class = 1;
                                                        objshares.No_AuhorisedCapital = NoofPreAuthorized;
                                                        objshares.No_IssuedCapital = NoofPreIssued;
                                                        objshares.No_SubscribedCapital = NoofPreSubscribed;
                                                        objshares.No_Paid_upCapital = NoofPrePaidup;
                                                        objshares.Nominalper_AuhorisedCapital = NomvalperSahrePreAuthorized;
                                                        objshares.Nominalper_IssuedCapital = NomvalperSahrePreIssued;
                                                        objshares.Nominalper_SubscribedCapital = NomvalperSahrePreSubscribed;
                                                        objshares.Nominalper_Paid_upCapital = NomvalperSahrePrePaidup;
                                                        objshares.totamt_AuhorisedCapital = NoofPreAuthorized * NomvalperSahrePreAuthorized;
                                                        objshares.totamt_IssuedCapital = NoofPreIssued * NomvalperSahrePreIssued;
                                                        objshares.totamt_SubscribedCapital = NoofPreSubscribed * NomvalperSahrePreSubscribed;
                                                        objshares.totamt_upCapital = NoofPrePaidup * NomvalperSahrePrePaidup;
                                                        objshares.IsActive = true;
                                                        entities.BM_Share.Add(objshares);
                                                        entities.SaveChanges();
                                                    }
                                                    else
                                                    {
                                                        checkforCapitalMasterPreShares.CapitalMasterId = (int)CapitalId;
                                                        checkforCapitalMasterPreShares.CustomerId = objCapitalMaster.CustomerId;
                                                        checkforCapitalMasterPreShares.Sharetype = "P";
                                                        checkforCapitalMasterPreShares.Shares_Class = 1;
                                                        checkforCapitalMasterPreShares.No_AuhorisedCapital = NoofPreAuthorized;
                                                        checkforCapitalMasterPreShares.No_IssuedCapital = NoofPreIssued;
                                                        checkforCapitalMasterPreShares.No_SubscribedCapital = NoofPreSubscribed;
                                                        checkforCapitalMasterPreShares.No_Paid_upCapital = NoofPrePaidup;
                                                        checkforCapitalMasterPreShares.Nominalper_AuhorisedCapital = NomvalperSahrePreAuthorized;
                                                        checkforCapitalMasterPreShares.Nominalper_IssuedCapital = NomvalperSahrePreIssued;
                                                        checkforCapitalMasterPreShares.Nominalper_SubscribedCapital = NomvalperSahrePreSubscribed;
                                                        checkforCapitalMasterPreShares.Nominalper_Paid_upCapital = NomvalperSahrePrePaidup;
                                                        checkforCapitalMasterPreShares.totamt_AuhorisedCapital = NoofPreAuthorized * NomvalperSahrePreAuthorized;
                                                        checkforCapitalMasterPreShares.totamt_IssuedCapital = NoofPreIssued * NomvalperSahrePreIssued;
                                                        checkforCapitalMasterPreShares.totamt_SubscribedCapital = NoofPreSubscribed * NomvalperSahrePreSubscribed;
                                                        checkforCapitalMasterPreShares.totamt_upCapital = NoofPrePaidup * NomvalperSahrePrePaidup;
                                                        checkforCapitalMasterPreShares.IsActive = true;

                                                        entities.SaveChanges();
                                                    }
                                                }
                                                #region  Capture data of Debenture Capital
                                                if (CapitalId > 0)
                                                {
                                                    var checkdebentureexisting = (from x in entities.BM_Debentures
                                                                                  where x.CapitalId == CapitalId
                                                                                   && x.IsActive == true
                                                                                  select x).FirstOrDefault();
                                                    if (checkdebentureexisting == null)
                                                    {
                                                        BM_Debentures _objdebenture = new BM_Debentures();

                                                        _objdebenture.No_NonConDebUnits = NO_UNITS_NCD;
                                                        _objdebenture.Nom_NonConDebperUnits = NOM_VAL_UNIT_NCD;
                                                        _objdebenture.tot_NonConDeb = TOTAL_VAL_NCD;

                                                        _objdebenture.No_PartiallyConDebUnit = NO_UNITS_PCD;
                                                        _objdebenture.Nom_NonConDebperUnits = NOM_VAL_UNIT_PCD;
                                                        _objdebenture.tot_NonConDeb = TOTAL_VAL_PCD;

                                                        _objdebenture.No_FullyConDebUnit = NO_UNITS_FCD;
                                                        _objdebenture.Nom_FullyConDebperUnit = NOM_VAL_UNIT_FCD;
                                                        _objdebenture.tot_FullyConDebper = TOTAL_VAL_FCD;

                                                        _objdebenture.Total = TOT_TOTAL_VAL;

                                                        _objdebenture.CustomerId = objfileupload.CustomerId;
                                                        _objdebenture.CapitalId = CapitalId;
                                                        _objdebenture.IsActive = true;

                                                        entities.BM_Debentures.Add(_objdebenture);
                                                        entities.SaveChanges();
                                                    }
                                                    else
                                                    {
                                                        checkdebentureexisting.No_NonConDebUnits = NO_UNITS_NCD;
                                                        checkdebentureexisting.Nom_NonConDebperUnits = NOM_VAL_UNIT_NCD;
                                                        checkdebentureexisting.tot_NonConDeb = TOTAL_VAL_NCD;

                                                        checkdebentureexisting.No_PartiallyConDebUnit = NO_UNITS_PCD;
                                                        checkdebentureexisting.Nom_NonConDebperUnits = NOM_VAL_UNIT_PCD;
                                                        checkdebentureexisting.tot_NonConDeb = TOTAL_VAL_PCD;

                                                        checkdebentureexisting.No_FullyConDebUnit = NO_UNITS_FCD;
                                                        checkdebentureexisting.Nom_FullyConDebperUnit = NOM_VAL_UNIT_FCD;
                                                        checkdebentureexisting.tot_FullyConDebper = TOTAL_VAL_FCD;

                                                        checkdebentureexisting.Total = TOT_TOTAL_VAL;

                                                        checkdebentureexisting.CustomerId = objfileupload.CustomerId;
                                                        checkdebentureexisting.CapitalId = CapitalId;
                                                        checkdebentureexisting.IsActive = true;

                                                        entities.SaveChanges();
                                                    }
                                                }
                                                #endregion


                                                objfileupload.Response.Success = true;
                                                objfileupload.Response.Message = "MGT7 Upload successfully";
                                            }
                                        }
                                        else
                                        {
                                            objfileupload.Response.Error = true;
                                            objfileupload.Response.Message = "Entity already exist";
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(CIN))
                                        {
                                            objfileupload.Response.Error = true;
                                            objfileupload.Response.Message = "CIN can't  null";
                                        }
                                        else if (Type <= 0)
                                        {
                                            objfileupload.Response.Error = true;
                                            objfileupload.Response.Message = "Entity Type not Found(Private,Public,Listed)";
                                        }
                                    }
                                }
                                else
                                {
                                    objfileupload.Response.Error = true;
                                    objfileupload.Response.Message = "Please check uploaded form is MGT-7";
                                }
                            }
                            else
                            {
                                objfileupload.Response.Error = true;
                                objfileupload.Response.Message = "Something went wrong";
                            }
                        }
                        else
                        {
                            objfileupload.Response.Error = true;
                            objfileupload.Response.Message = "Please check uploaded form is MGT-7";
                        }
                    }
                    else
                    {
                        objfileupload.Response.Error = true;
                        objfileupload.Response.Message = "Form not found";
                    }
                }
                else
                {
                    objfileupload.Response.Error = true;
                    objfileupload.Response.Message = "Form not found";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                objfileupload.Response.Error = true;
                objfileupload.Response.Message = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                objfileupload.Response.Error = true;
                objfileupload.Response.Message = SecretarialConst.Messages.serverError;
            }
            return objfileupload;
        }

        //public VMEntityFileUpload ImportDatafrom_MGT7(VMEntityFileUpload objfileupload)
        //{
        //    objfileupload.Response = new Response();
        //    try
        //    {
        //        var ICSI_MODE = Convert.ToInt32(WebConfigurationManager.AppSettings["AVASEC_IS_ICSI_MODE"]);

        //        int? parentID = GetParentID(objfileupload.CustomerId);
        //        if (ICSI_MODE == 1 && parentID > 0)
        //        {
        //            CheckCustomerBranchLimit = ICIAManagement.CheckCustomerEntityLimitByDistributor(Convert.ToInt32(parentID));
        //            //CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(Convert.ToInt32(parentID), objfileupload.CustomerId);
        //        }
        //        if (CheckCustomerBranchLimit)
        //        {

        //            #region variable define for Mgt-7 Read for Entity
        //            long EntityID = -1;
        //            string Message = "";
        //            string EntityName = string.Empty;
        //            string CIN = string.Empty;
        //            bool IsListed = false;
        //            string PAN = string.Empty;
        //            string GLN = string.Empty;
        //            string RegisterAddress = string.Empty;
        //            string RegisterAddressLine1 = string.Empty;
        //            string RegisterAddressLine2 = string.Empty;
        //            int stateId = -1;
        //            int CityId = -1;
        //            string PIN = string.Empty;
        //            string EmilId = string.Empty;
        //            long PhoneNo = 0;
        //            DateTime dateofIncorporate = DateTime.Now;
        //            string Website = string.Empty;
        //            string TypeofCompany = string.Empty;
        //            int Type = -1;
        //            string CategoryofCompany = string.Empty;
        //            string CategoryofCompanySub = string.Empty;
        //            int CompanyCategoryType = -1;
        //            int CompanyCategorysubType = -1;
        //            string RegistrationNumber = string.Empty;
        //            string FY = string.Empty;
        //            #region Company is Listed
        //            string RTA_CIN = string.Empty;
        //            string RTA_Name = string.Empty;
        //            string RTA_Address = string.Empty;
        //            int noofsubcompany = -1;

        //            List<SubCompanyType> objlistofsubcompany = new List<SubCompanyType>();
        //            #endregion Company is Listed


        //            #endregion

        //            #region variable define for Mgt7 for Capital Master
        //            string IsCapitalShares = string.Empty;
        //            decimal TotNoofEquAuthorized = 0;
        //            decimal TotNoofEquIssuedAuthorized = 0;
        //            decimal TotNoofEquSubscribed = 0;
        //            decimal TotNoofEqupaidUp = 0;

        //            decimal TotAmtofEquAuthorized = 0;
        //            decimal TotAmtofEquIssued = 0;
        //            decimal TotAmtofEquSubscribe = 0;
        //            decimal TotAmtofEquPaidup = 0;

        //            int NoofClass = 1;
        //            long NoofEquAuthorized = 0;
        //            long NoofEquIssued = 0;
        //            long NoofSubscribed = 0;
        //            long NoofPaidup = 0;

        //            long NomvalperSahreEquAuthorized = 0;
        //            long NomvalperSahreEquIssued = 0;
        //            long NomvalperSahreEquSubscribed = 0;
        //            long NomvalperSahreEquPaidup = 0;
        //            int j = 0;
        //            #endregion


        //            if (objfileupload.File != null)
        //            {
        //                if (objfileupload.File.ContentLength > 0)
        //                {
        //                    string myFilePath = objfileupload.File.FileName;
        //                    string ext = System.IO.Path.GetExtension(myFilePath);
        //                    if (ext == ".pdf")
        //                    {
        //                        string excelfileName = string.Empty;
        //                        string path = "~/Areas/BM_Management/Documents/" + objfileupload.CustomerId + "/EntityMaster/";
        //                        string _file_Name = System.IO.Path.GetFileName(objfileupload.File.FileName);
        //                        string _path = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(path), _file_Name);
        //                        bool exists = System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(path));
        //                        if (!exists)
        //                        {
        //                            System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(path));
        //                        }
        //                        DirectoryInfo di = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.MapPath(path));
        //                        FileInfo[] TXTFiles = di.GetFiles(_file_Name);
        //                        if (TXTFiles.Length == 0)
        //                        {
        //                            objfileupload.File.SaveAs(_path);
        //                        }
        //                        PdfReader pdfReader = new PdfReader(_path);
        //                        SubCompanyType objcompanysubtype = new SubCompanyType();
        //                        if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
        //                        {
        //                            foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
        //                            {
        //                                #region capture entity name
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NAME_OF_COMPANY[0]")
        //                                {
        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        EntityName = (de.Value.InnerText).Trim();
        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture cin and entitytype
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].CIN[0]")
        //                                {
        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        CIN = (de.Value.InnerText).Trim();
        //                                        if (CIN.IndexOf("L") == 0)
        //                                        {
        //                                            IsListed = true;

        //                                            Type = 10;
        //                                        }
        //                                        else if (CIN.IndexOf("U") == 0)
        //                                        {
        //                                            IsListed = false;
        //                                        }

        //                                        RegistrationNumber = CIN.Substring(15);
        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture PAN
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].IT_PAN_OF_COMPNY[0]")
        //                                {

        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        PAN = de.Value.InnerText;
        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture GLN
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].GLN[0]")
        //                                {

        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        GLN = de.Value.InnerText;
        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture address
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].REG_OFFC_ADDRESS[0]")
        //                                {
        //                                    string pattern = @"^[1-9][0-9]{5}$";
        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        RegisterAddress = de.Value.InnerText;

        //                                        List<string> address = RegisterAddress.Split('\n').ToList();
        //                                        if (address.Count() > 0)
        //                                        {
        //                                            foreach (var item in address)
        //                                            {
        //                                                var matchTuple = Regex.Match(item, pattern, RegexOptions.IgnoreCase);
        //                                                if (matchTuple.Success)
        //                                                {
        //                                                    PIN = matchTuple.ToString();
        //                                                }
        //                                            }
        //                                            if (address[0] != null)
        //                                            {
        //                                                RegisterAddressLine1 = address[0].ToString();
        //                                            }
        //                                            if (address.Count() > 1)
        //                                            {
        //                                                if (address[1] != null)
        //                                                {
        //                                                    RegisterAddressLine2 = address[1].ToString();
        //                                                }
        //                                                if (address[4] != null)
        //                                                {
        //                                                    stateId = GetRegisterStateId(address[4]);
        //                                                    if (stateId == 0)
        //                                                    {
        //                                                        stateId = GetRegisterStateId(address[3]);
        //                                                    }
        //                                                    if (stateId == 0)
        //                                                    {
        //                                                        stateId = GetRegisterStateId(address[2]);
        //                                                    }
        //                                                }
        //                                                if (address[2] != null && stateId > 0)
        //                                                {
        //                                                    CityId = getCityRegisterID(stateId, address[2].ToString());
        //                                                    if (CityId == 0)
        //                                                    {
        //                                                        CityId = getCityRegisterID(stateId, address[1].ToString());
        //                                                    }
        //                                                }
        //                                            }
        //                                        }

        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture email
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].EMAIL_ID_COMPANY[0]")
        //                                {

        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        EmilId = de.Value.InnerText;
        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture phone No
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].PHONE_NUMBER[0]")
        //                                {

        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        PhoneNo = Convert.ToInt64(de.Value.InnerText);
        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture date of Incorporation
        //                                if (de.Key.ToString() == "data[0].INCORPORATION_DATE[0]")
        //                                {

        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        dateofIncorporate = Convert.ToDateTime(de.Value.InnerText);
        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture website
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].WEBSITE[0]")
        //                                {

        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        Website = de.Value.InnerText.Trim();
        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture company Type
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TYPE_OF_COMPANY[0]" && (Type == 0 || Type == -1))
        //                                {

        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        TypeofCompany = de.Value.InnerText.Trim();
        //                                        if (TypeofCompany.Trim() == "PRIV")
        //                                        {
        //                                            Type = 1;
        //                                        }
        //                                        else if (TypeofCompany.Trim() == "PUBC")
        //                                        {
        //                                            Type = 2;
        //                                        }

        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture company cateory
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].CATEGORY_COMPANY[0]")
        //                                {
        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        CategoryofCompany = de.Value.InnerText;
        //                                    }
        //                                    if (CategoryofCompany == "CLAG")
        //                                    {
        //                                        CompanyCategoryType = 2;
        //                                    }
        //                                    else if (CategoryofCompany == "ULCM")
        //                                    {
        //                                        CompanyCategoryType = 3;
        //                                    }
        //                                    else
        //                                    {
        //                                        CompanyCategoryType = 1;
        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture sub company
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].SUB_CATEGORY_COM[0]")
        //                                {

        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        CategoryofCompanySub = de.Value.InnerText;
        //                                    }
        //                                    if (CategoryofCompanySub == "NGOV")
        //                                    {
        //                                        CompanyCategorysubType = 1;
        //                                    }
        //                                    else if (CategoryofCompanySub == "UGCM")
        //                                    {
        //                                        CompanyCategorysubType = 2;
        //                                    }
        //                                    else if (CategoryofCompanySub == "STGM")
        //                                    {
        //                                        CompanyCategorysubType = 3;
        //                                    }
        //                                    else if (CategoryofCompanySub == "GACM")
        //                                    {
        //                                        CompanyCategorysubType = 4;
        //                                    }
        //                                    else if (CategoryofCompanySub == "SUFR")
        //                                    {
        //                                        CompanyCategorysubType = 5;
        //                                    }
        //                                }
        //                                #endregion

        //                                #region capture FY
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].FY_FROM_DATE[0]" || de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].FY_END_DATE[0]")
        //                                {
        //                                    if (de.Value.InnerText != null)
        //                                    {
        //                                        DateTime FYDate = Convert.ToDateTime(de.Value.InnerText);

        //                                        int Month = FYDate.Month;
        //                                        if (Month == 3 || Month == 4)
        //                                        {
        //                                            FY = "FY";
        //                                        }
        //                                        else if (Month == 1 || Month == 12)
        //                                        {
        //                                            FY = "CY";
        //                                        }
        //                                    }
        //                                }
        //                                #endregion

        //                                #region Details Fill for RTA For Listed Company
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].CIN_REG_TA[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        RTA_CIN = de.Value.InnerText.Trim();
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NAME_REG_TA[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        RTA_Name = de.Value.InnerText.Trim();
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].OFFICE_ADDRESS[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        RTA_Address = de.Value.InnerText.Trim();
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NO_COMPANIES[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        noofsubcompany = Convert.ToInt32(de.Value.InnerText.Trim());
        //                                    }
        //                                }

        //                                #endregion

        //                                #region Capital Shares Details
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].RB_SHARE_CAPITAL[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        IsCapitalShares = de.Value.InnerText.Trim();
        //                                    }
        //                                }


        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_A_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        TotNoofEquAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_I_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        TotNoofEquIssuedAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
        //                                    }
        //                                }

        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_S_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        TotNoofEquSubscribed = Convert.ToDecimal(de.Value.InnerText.Trim());
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_P_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        TotNoofEqupaidUp = Convert.ToDecimal(de.Value.InnerText.Trim());
        //                                    }
        //                                    Console.WriteLine("Total number of equity shares Paid up" + " : " + de.Value.InnerText);
        //                                }

        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_A_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        TotAmtofEquAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
        //                                    }
        //                                }

        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_I_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        TotAmtofEquIssued = Convert.ToDecimal(de.Value.InnerText.Trim());
        //                                    }
        //                                }

        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_S_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        TotAmtofEquSubscribe = Convert.ToDecimal(de.Value.InnerText.Trim());
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_P_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        TotAmtofEquPaidup = Convert.ToDecimal(de.Value.InnerText.Trim());
        //                                    }
        //                                }

        //                                if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NO_OF_CLASSES_ES[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        NoofClass = Convert.ToInt16(de.Value.InnerText.Trim());
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_A_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        NoofEquAuthorized = (long)Convert.ToDecimal(de.Value.InnerText);
        //                                    }
        //                                }

        //                                if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_I_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        NoofEquIssued = (long)Convert.ToDecimal(de.Value.InnerText);
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_S_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        NoofSubscribed = (long)Convert.ToDecimal(de.Value.InnerText);
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_P_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        NoofPaidup = (long)Convert.ToDecimal(de.Value.InnerText);
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_A_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        NomvalperSahreEquAuthorized = (long)Convert.ToDecimal(de.Value.InnerText);
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_I_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        NomvalperSahreEquIssued = (long)Convert.ToDecimal(de.Value.InnerText);
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_S_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        NomvalperSahreEquSubscribed = (long)Convert.ToDecimal(de.Value.InnerText);
        //                                    }
        //                                }
        //                                if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_P_CAP[0]")
        //                                {
        //                                    if (de.Value.InnerText.Trim() != null)
        //                                    {
        //                                        NomvalperSahreEquPaidup = (long)Convert.ToDecimal(de.Value.InnerText);
        //                                    }
        //                                }

        //                                #endregion
        //                            }

        //                            if (!String.IsNullOrEmpty(CIN))
        //                            {
        //                                com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
        //                                {
        //                                    //ID = _objentity.CustomerBranchId,
        //                                    Name = EntityName,
        //                                    //Type = Convert.ToByte(ddlType.SelectedValue),
        //                                    // ComType = Convert.ToByte(comType),
        //                                    //AddressLine1 = _objentity.Regi_Address_Line1,
        //                                    //AddressLine2 = _objentity.Regi_Address_Line2,
        //                                    //StateID = _objentity.Regi_StateId,
        //                                    //CityID = _objentity.Regi_CityId,
        //                                    //Others = "",
        //                                    //PinCode = _objentity.Regi_PINCode,
        //                                    ContactPerson = "",
        //                                    Landline = "",
        //                                    Mobile = "",
        //                                    EmailID = EmilId,
        //                                    CustomerID = objfileupload.CustomerId,
        //                                    //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
        //                                    //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
        //                                };

        //                                customerBranch = CreateCustomerBranch(customerBranch, Type, IsListed, out Message);

        //                                if (customerBranch.ID > 0)
        //                                {
        //                                    BM_SubEntityMapping objsubentity = new BM_SubEntityMapping();
        //                                    var checkEntity = (from row in entities.BM_EntityMaster where row.CIN_LLPIN == CIN && row.Is_Deleted == false && row.Customer_Id == objfileupload.CustomerId select row).FirstOrDefault();
        //                                    //var checkEntity = (from row in entities.BM_EntityMaster where row.CIN_LLPIN == CIN && row.Is_Deleted == false select row).FirstOrDefault();
        //                                    if (checkEntity == null)
        //                                    {

        //                                        BM_EntityMaster objentityMaster = new BM_EntityMaster
        //                                        {
        //                                            CompanyName = EntityName,

        //                                            CIN_LLPIN = CIN,
        //                                            GLN = GLN,
        //                                            IncorporationDate = dateofIncorporate,
        //                                            Registration_No = RegistrationNumber,
        //                                            Regi_Address_Line1 = RegisterAddressLine1,
        //                                            Regi_Address_Line2 = RegisterAddressLine2,
        //                                            Regi_StateId = stateId,
        //                                            Regi_CityId = CityId,
        //                                            Regi_PINCode = PIN,
        //                                            CompanySubCategory = CompanyCategorysubType,
        //                                            CompanyCategory_Id = CompanyCategoryType,
        //                                            IS_Listed = IsListed,
        //                                            Email_Id = EmilId,
        //                                            FY_CY = FY,
        //                                            WebSite = Website,
        //                                            PAN = PAN,
        //                                            CreatedOn = DateTime.Now,
        //                                            CreatedBy = objfileupload.UserId,
        //                                            Is_Deleted = false,
        //                                            Customer_Id = objfileupload.CustomerId,
        //                                            CustomerBranchId = customerBranch.ID,
        //                                        };
        //                                        if (IsListed)
        //                                        {
        //                                            objentityMaster.RTA_CompanyName = RTA_Name;
        //                                            objentityMaster.RTA_Address = RTA_Name;
        //                                            objentityMaster.RTA_CIN = RTA_CIN;
        //                                            objentityMaster.Entity_Type = 10;
        //                                        }
        //                                        else
        //                                        {
        //                                            objentityMaster.Entity_Type = Type;
        //                                        }
        //                                        entities.BM_EntityMaster.Add(objentityMaster);
        //                                        entities.SaveChanges();
        //                                        if (objentityMaster.Id > 0)
        //                                        {
        //                                            #region for add subsidary Company joint Company 
        //                                            while (j < noofsubcompany)
        //                                            {
        //                                                foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
        //                                                {

        //                                                    if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].NAME_COMPANY[0]")
        //                                                    {

        //                                                        if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
        //                                                        {
        //                                                            objcompanysubtype.SubCompanyName = de.Value.InnerText.Trim();


        //                                                        }
        //                                                    }

        //                                                    if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].CIN_FCRN[0]")
        //                                                    {
        //                                                        if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
        //                                                        {
        //                                                            objcompanysubtype.CIN = de.Value.InnerText.Trim();


        //                                                        }
        //                                                    }



        //                                                    if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].HOLD_SUB_ASSOC[0]")
        //                                                    {
        //                                                        if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
        //                                                        {
        //                                                            if (de.Value.InnerText.Trim() == "SUBS")
        //                                                            {
        //                                                                objcompanysubtype.subcompanyTypeID = 3;
        //                                                            }
        //                                                            else if (de.Value.InnerText.Trim() == "JVEN")
        //                                                            {
        //                                                                objcompanysubtype.subcompanyTypeID = 4;
        //                                                            }
        //                                                            else if (de.Value.InnerText.Trim() == "ASSC")
        //                                                            {
        //                                                                objcompanysubtype.subcompanyTypeID = 1;
        //                                                            }
        //                                                            else
        //                                                            {
        //                                                                objcompanysubtype.subcompanyTypeID = 2;
        //                                                            }
        //                                                        }
        //                                                    }

        //                                                    if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].PERCENT_SHARE[0]")
        //                                                    {
        //                                                        if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
        //                                                        {
        //                                                            objcompanysubtype.PersentageofShareholding = Convert.ToDecimal(de.Value.InnerText.Trim());
        //                                                        }
        //                                                    }
        //                                                }

        //                                                if (objcompanysubtype.CIN != null && objcompanysubtype.CIN != "")
        //                                                {
        //                                                    var checkdetailsofsubcompany = (from s in entities.BM_SubEntityMapping where s.CIN == objcompanysubtype.CIN select s).FirstOrDefault();
        //                                                    if (checkdetailsofsubcompany == null)
        //                                                    {
        //                                                        objsubentity.EntityId = objentityMaster.Id;
        //                                                        objsubentity.CIN = objcompanysubtype.CIN;
        //                                                        objsubentity.ParentCIN = objentityMaster.CIN_LLPIN;
        //                                                        objsubentity.NameofCompany = objcompanysubtype.SubCompanyName;
        //                                                        objsubentity.CompanysubType = objcompanysubtype.subcompanyTypeID;
        //                                                        objsubentity.PerofShareHeld = Convert.ToInt64(objcompanysubtype.PersentageofShareholding);
        //                                                        objsubentity.isActive = true;
        //                                                        objsubentity.Createdon = DateTime.Now;
        //                                                        objsubentity.CustomerId = (int)objentityMaster.Customer_Id;
        //                                                        entities.BM_SubEntityMapping.Add(objsubentity);
        //                                                        entities.SaveChanges();
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        checkdetailsofsubcompany.EntityId = objentityMaster.Id;
        //                                                        checkdetailsofsubcompany.CIN = objcompanysubtype.CIN;
        //                                                        checkdetailsofsubcompany.ParentCIN = objentityMaster.CIN_LLPIN;
        //                                                        checkdetailsofsubcompany.NameofCompany = objcompanysubtype.SubCompanyName;
        //                                                        checkdetailsofsubcompany.CompanysubType = objcompanysubtype.subcompanyTypeID;
        //                                                        checkdetailsofsubcompany.PerofShareHeld = Convert.ToInt64(objcompanysubtype.PersentageofShareholding);
        //                                                        checkdetailsofsubcompany.isActive = true;
        //                                                        checkdetailsofsubcompany.Createdon = DateTime.Now;
        //                                                        checkdetailsofsubcompany.CustomerId = (int)objentityMaster.Customer_Id;
        //                                                        //entities.BM_SubEntityMapping.Add(objsubentity);
        //                                                        entities.SaveChanges();

        //                                                    }
        //                                                }
        //                                                j++;
        //                                            }
        //                                            #endregion

        //                                            EntityID = objentityMaster.Id;

        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        checkEntity.CompanyName = EntityName;
        //                                        checkEntity.Entity_Type = Type;
        //                                        checkEntity.CIN_LLPIN = CIN;
        //                                        checkEntity.GLN = GLN;
        //                                        checkEntity.IncorporationDate = dateofIncorporate;
        //                                        checkEntity.Registration_No = RegistrationNumber;
        //                                        checkEntity.Regi_Address_Line1 = RegisterAddressLine1;
        //                                        checkEntity.Regi_Address_Line2 = RegisterAddressLine2;
        //                                        checkEntity.Regi_StateId = stateId;
        //                                        checkEntity.Regi_CityId = CityId;
        //                                        checkEntity.CompanyCategory_Id = CompanyCategorysubType;
        //                                        checkEntity.IS_Listed = IsListed;
        //                                        checkEntity.Email_Id = EmilId;
        //                                        checkEntity.WebSite = Website;
        //                                        checkEntity.PAN = PAN;
        //                                        checkEntity.UpdatedOn = DateTime.Now;
        //                                        checkEntity.UpdatedBy = objfileupload.UserId;
        //                                        // checkEntity.CustomerBranchId = customerBranch.ID;
        //                                        checkEntity.Is_Deleted = false;
        //                                        checkEntity.Customer_Id = objfileupload.CustomerId;
        //                                        checkEntity.FY_CY = FY;
        //                                        if (IsListed)
        //                                        {
        //                                            checkEntity.RTA_CompanyName = RTA_Name;
        //                                            checkEntity.RTA_Address = RTA_Address;
        //                                            checkEntity.RTA_CIN = RTA_CIN;
        //                                        }
        //                                        entities.SaveChanges();
        //                                        if (checkEntity.Id > 0)
        //                                        {
        //                                            #region for add subsidary Company joint Company 
        //                                            while (j < noofsubcompany)
        //                                            {
        //                                                foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
        //                                                {

        //                                                    if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].NAME_COMPANY[0]")
        //                                                    {

        //                                                        if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
        //                                                        {
        //                                                            objcompanysubtype.SubCompanyName = de.Value.InnerText.Trim();


        //                                                        }
        //                                                    }

        //                                                    if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].CIN_FCRN[0]")
        //                                                    {
        //                                                        //if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
        //                                                        //{
        //                                                        objcompanysubtype.CIN = de.Value.InnerText.Trim();

        //                                                        //}
        //                                                    }



        //                                                    if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].HOLD_SUB_ASSOC[0]")
        //                                                    {
        //                                                        if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
        //                                                        {
        //                                                            if (de.Value.InnerText.Trim() == "SUBS")
        //                                                            {
        //                                                                objcompanysubtype.subcompanyTypeID = 3;
        //                                                            }
        //                                                            else if (de.Value.InnerText.Trim() == "JVEN")
        //                                                            {
        //                                                                objcompanysubtype.subcompanyTypeID = 4;
        //                                                            }
        //                                                            else if (de.Value.InnerText.Trim() == "ASSC")
        //                                                            {
        //                                                                objcompanysubtype.subcompanyTypeID = 1;
        //                                                            }
        //                                                            else
        //                                                            {
        //                                                                objcompanysubtype.subcompanyTypeID = 2;
        //                                                            }

        //                                                        }
        //                                                    }
        //                                                    if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].PERCENT_SHARE[0]")
        //                                                    {
        //                                                        if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
        //                                                        {
        //                                                            objcompanysubtype.PersentageofShareholding = Convert.ToDecimal(de.Value.InnerText.Trim());


        //                                                        }
        //                                                    }
        //                                                }
        //                                                if (objcompanysubtype.CIN != null && objcompanysubtype.CIN != "")
        //                                                {
        //                                                    var checkdetailsofsubcompany = (from s in entities.BM_SubEntityMapping where s.CIN == objcompanysubtype.CIN select s).FirstOrDefault();
        //                                                    if (checkdetailsofsubcompany == null)
        //                                                    {
        //                                                        objsubentity.EntityId = checkEntity.Id;
        //                                                        objsubentity.CIN = objcompanysubtype.CIN;
        //                                                        objsubentity.ParentCIN = checkEntity.CIN_LLPIN;
        //                                                        objsubentity.NameofCompany = objcompanysubtype.SubCompanyName;
        //                                                        objsubentity.CompanysubType = objcompanysubtype.subcompanyTypeID;
        //                                                        objsubentity.PerofShareHeld = objcompanysubtype.PersentageofShareholding;
        //                                                        objsubentity.isActive = true;
        //                                                        objsubentity.Createdon = DateTime.Now;
        //                                                        objsubentity.CustomerId = (int)checkEntity.Customer_Id;
        //                                                        entities.BM_SubEntityMapping.Add(objsubentity);
        //                                                        entities.SaveChanges();
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        checkdetailsofsubcompany.EntityId = checkEntity.Id;
        //                                                        checkdetailsofsubcompany.CIN = objcompanysubtype.CIN;
        //                                                        checkdetailsofsubcompany.ParentCIN = checkEntity.CIN_LLPIN;
        //                                                        checkdetailsofsubcompany.NameofCompany = objcompanysubtype.SubCompanyName;
        //                                                        checkdetailsofsubcompany.CompanysubType = objcompanysubtype.subcompanyTypeID;
        //                                                        checkdetailsofsubcompany.PerofShareHeld = Convert.ToInt64(objcompanysubtype.PersentageofShareholding);
        //                                                        checkdetailsofsubcompany.isActive = true;
        //                                                        checkdetailsofsubcompany.Createdon = DateTime.Now;
        //                                                        checkdetailsofsubcompany.CustomerId = (int)checkEntity.Customer_Id;
        //                                                        //entities.BM_SubEntityMapping.Add(objsubentity);
        //                                                        entities.SaveChanges();

        //                                                    }
        //                                                }
        //                                                j++;
        //                                            }
        //                                            #endregion
        //                                            EntityID = checkEntity.Id;
        //                                        }
        //                                    }
        //                                    if (EntityID > 0)
        //                                    {
        //                                        BM_CapitalMaster objCapitalMaster = new BM_CapitalMaster();
        //                                        var checkforCapitalMaster = (from C in entities.BM_CapitalMaster where C.Entity_Id == EntityID select C).FirstOrDefault();
        //                                        if (checkforCapitalMaster == null)
        //                                        {
        //                                            objCapitalMaster.Entity_Id = (int)EntityID;
        //                                            objCapitalMaster.CustomerId = objfileupload.CustomerId;
        //                                            objCapitalMaster.IsDebenger = false;
        //                                            objCapitalMaster.IsPrefrence = false;
        //                                            objCapitalMaster.IsUnclassified = false;
        //                                            objCapitalMaster.TotNoAuthorizedCapita = TotNoofEquAuthorized;
        //                                            objCapitalMaster.TotNoIssuedCapital = TotNoofEquIssuedAuthorized;
        //                                            objCapitalMaster.TotNoSubscribCapital = TotNoofEquSubscribed;
        //                                            objCapitalMaster.TotNoPaidupCapital = TotNoofEqupaidUp;
        //                                            objCapitalMaster.TotamtAuthorizedCapita = TotAmtofEquAuthorized;
        //                                            objCapitalMaster.TotamtNoIssuedCapital = TotAmtofEquIssued;
        //                                            objCapitalMaster.TotamtNoSubscribCapital = TotAmtofEquSubscribe;
        //                                            objCapitalMaster.TotamtNoPaidupCapital = TotAmtofEquPaidup;
        //                                            objCapitalMaster.Pri_TotNoIssuedCapital = 0;
        //                                            objCapitalMaster.Pri_TotNoSubscribCapital = 0;
        //                                            objCapitalMaster.Pri_TotNoPaidupCapital = 0;
        //                                            objCapitalMaster.Pri_TotamtAuthorizedCapita = 0;
        //                                            objCapitalMaster.Pri_TotamtNoIssuedCapital = 0;
        //                                            objCapitalMaster.Pri_TotamtNoSubscribCapital = 0;
        //                                            objCapitalMaster.Pri_TotamtNoPaidupCapital = 0;
        //                                            objCapitalMaster.Unclassified_AuhorisedCapital = 0;
        //                                            objCapitalMaster.IsActive = true;
        //                                            objCapitalMaster.Createdby = objfileupload.UserId;
        //                                            objCapitalMaster.CreatedOn = DateTime.Now;
        //                                            objCapitalMaster.AuthorizedCapital = objCapitalMaster.TotamtAuthorizedCapita + objCapitalMaster.Pri_TotamtAuthorizedCapita;
        //                                            entities.BM_CapitalMaster.Add(objCapitalMaster);
        //                                            entities.SaveChanges();
        //                                            if (objCapitalMaster.Id > 0 && EntityID > 0)
        //                                            {
        //                                                var checkforCapitalMasterShares = (from CS in entities.BM_Share where CS.CapitalMasterId == objCapitalMaster.Id select CS).ToList();
        //                                                if (checkforCapitalMasterShares.Count == 0)
        //                                                {
        //                                                    BM_Share objshares = new BM_Share();
        //                                                    objshares.CapitalMasterId = (int)objCapitalMaster.Id;
        //                                                    objshares.CustomerId = objCapitalMaster.CustomerId;
        //                                                    objshares.Sharetype = "E";
        //                                                    objshares.Shares_Class = 1;
        //                                                    objshares.No_AuhorisedCapital = NoofEquAuthorized;
        //                                                    objshares.No_IssuedCapital = NoofEquIssued;
        //                                                    objshares.No_SubscribedCapital = NoofSubscribed;
        //                                                    objshares.No_Paid_upCapital = NoofPaidup;
        //                                                    objshares.Nominalper_AuhorisedCapital = NomvalperSahreEquAuthorized;
        //                                                    objshares.Nominalper_IssuedCapital = NomvalperSahreEquIssued;
        //                                                    objshares.Nominalper_SubscribedCapital = NomvalperSahreEquSubscribed;
        //                                                    objshares.Nominalper_Paid_upCapital = NomvalperSahreEquPaidup;
        //                                                    objshares.totamt_AuhorisedCapital = NoofEquAuthorized * NomvalperSahreEquAuthorized;
        //                                                    objshares.totamt_IssuedCapital = NoofEquIssued * NomvalperSahreEquIssued;
        //                                                    objshares.totamt_SubscribedCapital = NoofSubscribed * NomvalperSahreEquSubscribed;
        //                                                    objshares.totamt_upCapital = NoofPaidup * NomvalperSahreEquPaidup;
        //                                                    objshares.IsActive = true;
        //                                                    entities.BM_Share.Add(objshares);
        //                                                    entities.SaveChanges();

        //                                                }
        //                                            }
        //                                        }

        //                                        objfileupload.Response.Success = true;
        //                                        objfileupload.Response.Message = "MGT7 Upload successfully";
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    objfileupload.Response.Error = true;
        //                                    objfileupload.Response.Message = "Entity already exist";
        //                                }
        //                            }
        //                            else
        //                            {
        //                                objfileupload.Response.Error = true;
        //                                objfileupload.Response.Message = "CIN can't  null";
        //                            }
        //                        }
        //                        else
        //                        {
        //                            objfileupload.Response.Error = true;
        //                            objfileupload.Response.Message = "Please check uploaded form is MGT-7";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        objfileupload.Response.Error = true;
        //                        objfileupload.Response.Message = "Please check uploaded form is MGT-7";
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                objfileupload.Response.Error = true;
        //                objfileupload.Response.Message = "Form not found";
        //            }
        //        }
        //        else
        //        {
        //            objfileupload.Response.Error = true;
        //            objfileupload.Response.Message = "You are not allowed to create more entity/company, Contact Avantis to allow to create more";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        objfileupload.Response.Error = true;
        //        objfileupload.Response.Message = "Server error occurred";
        //    }
        //    return objfileupload;
        //}

        private string GetFormsPath(VMEntityFileUpload objfileupload)
        {
            string excelfileName = string.Empty;
            string path = "~/Areas/BM_Management/Documents/" + objfileupload.CustomerId + "/EntityMaster/";
            string _file_Name = System.IO.Path.GetFileName(objfileupload.File.FileName);
            string _path = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(path), _file_Name);
            bool exists = System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(path));
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(path));
            }
            DirectoryInfo di = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.MapPath(path));
            FileInfo[] TXTFiles = di.GetFiles(_file_Name);
            if (TXTFiles.Length == 0)
            {
                objfileupload.File.SaveAs(_path);
            }
            return _path;
        }

        #endregion

        #region Import LLP 11 
        //public VMEntityFileUpload ImportDatafrom_LLP11(VMEntityFileUpload objfileupload)
        //{
        //    objfileupload.Response = new Response();
        //    try
        //    {
        //        var ICSI_MODE = Convert.ToInt32(WebConfigurationManager.AppSettings["AVASEC_IS_ICSI_MODE"]);

        //        int? parentID = GetParentID(objfileupload.CustomerId);
        //        if (ICSI_MODE == 1 && parentID > 0)
        //        {
        //            CheckCustomerBranchLimit = ICIAManagement.CheckCustomerEntityLimitByDistributor(Convert.ToInt32(parentID));
        //            //CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(Convert.ToInt32(parentID), objfileupload.CustomerId);
        //        }

        //        if (CheckCustomerBranchLimit)
        //        {

        //            #region variable define for LLP-11 Read for Entity
        //            long EntityID = -1;
        //            string Message = "";
        //            string EntityName = string.Empty;
        //            string LLPIN = string.Empty;
        //            string Email = string.Empty;
        //            string RegisteredAddress1 = string.Empty;
        //            string RegisteredAddress2 = string.Empty;
        //            string state = string.Empty;
        //            int stateId = 0;
        //            string City = string.Empty;
        //            int CityId = 0;
        //            string pin = string.Empty;
        //            long TotalMonateryvalContributionbyparterar = 0;
        //            string PrincipanBusinessActivity = string.Empty;
        //            string Contribution = string.Empty;
        //            string ContributionRecived = string.Empty;
        //            string businessclass = string.Empty;
        //            int? businessclassificationId = 0;


        //            #endregion

        //            if (objfileupload.File != null)
        //            {
        //                if (objfileupload.File.ContentLength > 0)
        //                {
        //                    string myFilePath = objfileupload.File.FileName;
        //                    string ext = System.IO.Path.GetExtension(myFilePath);
        //                    if (ext == ".pdf")
        //                    {
        //                        string _path;
        //                        _path = GetFormsPath(objfileupload);

        //                        if (!string.IsNullOrEmpty(_path))
        //                        {
        //                            PdfReader pdfReader = new PdfReader(_path);
        //                            SubCompanyType objcompanysubtype = new SubCompanyType();
        //                            if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
        //                            {
        //                                foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
        //                                {
        //                                    #region capture entity name
        //                                    if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM11[0].NAME_OF_LLP[0]")
        //                                    {
        //                                        if (!string.IsNullOrEmpty(de.Value.InnerText))
        //                                        {
        //                                            EntityName = (de.Value.InnerText).Trim();
        //                                        }
        //                                    }
        //                                    #endregion

        //                                    #region capture LLPIN
        //                                    if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM11[0].LLPIN[0]")
        //                                    {
        //                                        if (!string.IsNullOrEmpty(de.Value.InnerText))
        //                                        {
        //                                            LLPIN = (de.Value.InnerText).Trim();

        //                                        }
        //                                    }
        //                                    #endregion

        //                                    #region capture EmailId
        //                                    if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM11[0].EMAIL_ID[0]")
        //                                    {

        //                                        if (!string.IsNullOrEmpty(de.Value.InnerText))
        //                                        {
        //                                            Email = de.Value.InnerText;
        //                                        }
        //                                    }
        //                                    #endregion

        //                                    #region capture address Line 1
        //                                    if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM11[0].STREET[0]")
        //                                    {
        //                                        // string pattern = @"^[1-9][0-9]{5}$";
        //                                        if (!string.IsNullOrEmpty(de.Value.InnerText))
        //                                        {
        //                                            RegisteredAddress1 = de.Value.InnerText.Trim();

        //                                            //    List<string> address = RegisteredAddress1.Split('\n').ToList();
        //                                            //    if (address.Count() > 0)
        //                                            //    {
        //                                            //        foreach (var item in address)
        //                                            //        {
        //                                            //            var matchTuple = Regex.Match(item, pattern, RegexOptions.IgnoreCase);
        //                                            //            if (matchTuple.Success)
        //                                            //            {
        //                                            //                pin = matchTuple.ToString();
        //                                            //            }
        //                                            //        }
        //                                            //        if (address[0] != null)
        //                                            //        {
        //                                            //            RegisteredAddress1 = address[0].ToString();
        //                                            //        }
        //                                            //        if (address.Count() > 1)
        //                                            //        {
        //                                            //            if (address[1] != null)
        //                                            //            {
        //                                            //                RegisteredAddress2 = address[1].ToString();
        //                                            //            }
        //                                            //            if (address[4] != null)
        //                                            //            {
        //                                            //                stateId = GetRegisterStateId(address[4]);
        //                                            //                if (stateId == 0)
        //                                            //                {
        //                                            //                    stateId = GetRegisterStateId(address[3]);
        //                                            //                }
        //                                            //                if (stateId == 0)
        //                                            //                {
        //                                            //                    stateId = GetRegisterStateId(address[2]);
        //                                            //                }
        //                                            //            }
        //                                            //            if (address[2] != null && stateId > 0)
        //                                            //            {
        //                                            //                CityId = getCityRegisterID(stateId, address[2].ToString());
        //                                            //                if (CityId == 0)
        //                                            //                {
        //                                            //                    CityId = getCityRegisterID(stateId, address[1].ToString());
        //                                            //                }
        //                                            //            }
        //                                            //        }
        //                                            //    }

        //                                            //}
        //                                        }
        //                                    }
        //                                    #endregion

        //                                    #region state
        //                                    if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM11[0].REGION_STATE[0]")
        //                                    {
        //                                        if (!string.IsNullOrEmpty(de.Value.InnerText))
        //                                        {
        //                                            stateId = GetRegisterStateId(de.Value.InnerText.Trim());
        //                                        }
        //                                    }
        //                                    #endregion

        //                                    #region city
        //                                    if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM11[0].CITY[0]")
        //                                    {
        //                                        if (!string.IsNullOrEmpty(de.Value.InnerText))
        //                                        {
        //                                            CityId = getCityRegisterID(stateId, de.Value.InnerText.Trim());

        //                                        }
        //                                    }
        //                                    #endregion

        //                                    #region Contribution
        //                                    if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM11[0].TOT_OBLIGATION[0]")
        //                                    {

        //                                        if (!string.IsNullOrEmpty(de.Value.InnerText))
        //                                        {
        //                                            Contribution = de.Value.InnerText;
        //                                        }
        //                                    }
        //                                    #endregion

        //                                    #region Contribution Recived
        //                                    if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM11[0].TOT_CONTRI_REC[0]")
        //                                    {

        //                                        if (!string.IsNullOrEmpty(de.Value.InnerText))
        //                                        {
        //                                            ContributionRecived = de.Value.InnerText;
        //                                        }
        //                                    }
        //                                    #endregion

        //                                    #region Principal business Activity
        //                                    if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM11[0].PRIN_ACTIVITY[0]")
        //                                    {

        //                                        if (!string.IsNullOrEmpty(de.Value.InnerText))
        //                                        {
        //                                            PrincipanBusinessActivity = de.Value.InnerText.Trim();
        //                                        }
        //                                    }
        //                                    #endregion

        //                                    #region Business Classification

        //                                    if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM11[0].BUSINESS_CLASS[0]")
        //                                    {

        //                                        if (!string.IsNullOrEmpty(de.Value.InnerText))
        //                                        {
        //                                            businessclass = de.Value.InnerText.Trim();
        //                                            if (businessclass == "SERV")
        //                                            {
        //                                                businessclassificationId = 3;
        //                                            }
        //                                            else if (businessclass == "PROF")
        //                                            {
        //                                                businessclassificationId = 2;
        //                                            }
        //                                            else if (businessclass == "BUSS")
        //                                            {
        //                                                businessclassificationId = 1;
        //                                            }
        //                                            else if (businessclass == "OCCU")
        //                                            {
        //                                                businessclassificationId = 4;
        //                                            }
        //                                            else
        //                                            {
        //                                                businessclassificationId = 5;
        //                                            }
        //                                        }
        //                                    }
        //                                    #endregion

        //                                }

        //                                if (!(string.IsNullOrEmpty(LLPIN)) && !(string.IsNullOrEmpty(EntityName)))
        //                                {
        //                                    com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
        //                                    {
        //                                        //ID = _objentity.CustomerBranchId,
        //                                        Name = EntityName,
        //                                        //Type = Convert.ToByte(ddlType.SelectedValue),
        //                                        // ComType = Convert.ToByte(comType),
        //                                        //AddressLine1 = _objentity.Regi_Address_Line1,
        //                                        //AddressLine2 = _objentity.Regi_Address_Line2,
        //                                        //StateID = _objentity.Regi_StateId,
        //                                        //CityID = _objentity.Regi_CityId,
        //                                        //Others = "",
        //                                        //PinCode = _objentity.Regi_PINCode,
        //                                        ContactPerson = "",
        //                                        Landline = "",
        //                                        Mobile = "",
        //                                        EmailID = Email,
        //                                        CustomerID = objfileupload.CustomerId,
        //                                        //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
        //                                        //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
        //                                    };

        //                                    customerBranch = CreateCustomerBranch(customerBranch, 3, false, out Message);

        //                                    if (customerBranch.ID > 0)
        //                                    {
        //                                        BM_SubEntityMapping objsubentity = new BM_SubEntityMapping();
        //                                        var checkEntity = (from row in entities.BM_EntityMaster
        //                                                           where row.CIN_LLPIN == LLPIN
        //                                                           && row.Is_Deleted == false
        //                                                           && row.Customer_Id == objfileupload.CustomerId
        //                                                           select row).FirstOrDefault();

        //                                        if (checkEntity == null)
        //                                        {
        //                                            checkEntity = (from row in entities.BM_EntityMaster where row.CompanyName == EntityName && row.Is_Deleted == false && row.Customer_Id == objfileupload.CustomerId select row).FirstOrDefault();
        //                                        }

        //                                        //var checkEntity = (from row in entities.BM_EntityMaster where row.CIN_LLPIN == CIN && row.Is_Deleted == false select row).FirstOrDefault();
        //                                        if (checkEntity == null)
        //                                        {

        //                                            BM_EntityMaster objentityMaster = new BM_EntityMaster
        //                                            {
        //                                                CompanyName = EntityName,
        //                                                Entity_Type = 3,
        //                                                CIN_LLPIN = LLPIN,

        //                                                IncorporationDate = DateTime.Now,
        //                                                Registration_No = "",
        //                                                Regi_Address_Line1 = RegisteredAddress1,
        //                                                Regi_Address_Line2 = "",
        //                                                Regi_StateId = stateId,
        //                                                Regi_CityId = CityId,
        //                                                Regi_PINCode = pin,
        //                                                // CompanySubCategory = CompanyCategorysubType,
        //                                                CompanyCategory_Id = 0,
        //                                                IS_Listed = false,
        //                                                Email_Id = Email,
        //                                                FY_CY = "FY",
        //                                                //  WebSite = Website,
        //                                                PAN = "",
        //                                                CreatedOn = DateTime.Now,
        //                                                CreatedBy = objfileupload.UserId,
        //                                                Is_Deleted = false,
        //                                                Customer_Id = objfileupload.CustomerId,
        //                                                CustomerBranchId = customerBranch.ID,
        //                                                PrincipalbusinessActivity = PrincipanBusinessActivity,
        //                                                Businessclassification = businessclassificationId,
        //                                            };

        //                                            //    No_Of_Partners = _objentity.llpNo_Of_Partners;

        //                                            //    No_Of_DesignatedPartners = _objentity.llpNo_Of_DesignatedPartners;

        //                                            //    Obligation_Of_Contribution = _objentity.llpObligation_Of_Contribution;



        //                                            entities.BM_EntityMaster.Add(objentityMaster);
        //                                            entities.SaveChanges();
        //                                            if (objentityMaster.Id > 0)
        //                                            {

        //                                                EntityID = objentityMaster.Id;

        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            checkEntity.CompanyName = EntityName;
        //                                            checkEntity.Entity_Type = 3;
        //                                            checkEntity.CIN_LLPIN = LLPIN;
        //                                            //checkEntity.GLN = GLN;
        //                                            checkEntity.IncorporationDate = DateTime.Now;
        //                                            checkEntity.Registration_No = "";
        //                                            checkEntity.Regi_Address_Line1 = RegisteredAddress1;
        //                                            checkEntity.Regi_Address_Line2 = "";
        //                                            checkEntity.Regi_StateId = stateId;
        //                                            checkEntity.Regi_CityId = CityId;
        //                                            checkEntity.CompanyCategory_Id = 0;
        //                                            checkEntity.IS_Listed = false;
        //                                            checkEntity.Email_Id = Email;
        //                                            // checkEntity.WebSite = Website;
        //                                            checkEntity.PAN = "";
        //                                            checkEntity.UpdatedOn = DateTime.Now;
        //                                            checkEntity.UpdatedBy = objfileupload.UserId;
        //                                            checkEntity.CustomerBranchId = customerBranch.ID;
        //                                            checkEntity.Is_Deleted = false;
        //                                            checkEntity.Customer_Id = objfileupload.CustomerId;
        //                                            checkEntity.FY_CY = "FY";
        //                                            checkEntity.PrincipalbusinessActivity = PrincipanBusinessActivity;
        //                                            checkEntity.Businessclassification = businessclassificationId;
        //                                            entities.SaveChanges();

        //                                        }
        //                                    }
        //                                    objfileupload.Response.Success = true;
        //                                    objfileupload.Response.Message = "LLP 11 Upload successfully";
        //                                }

        //                                else
        //                                {
        //                                    objfileupload.Response.Error = true;
        //                                    objfileupload.Response.Message = "LLPIN can't null";
        //                                }
        //                            }
        //                            else
        //                            {
        //                                objfileupload.Response.Error = true;
        //                                objfileupload.Response.Message = "Please check uploaded form is LLp-11";
        //                            }
        //                        }
        //                        else
        //                        {
        //                            objfileupload.Response.Error = true;
        //                            objfileupload.Response.Message = "Please check uploaded form is LLp-11";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        objfileupload.Response.Error = true;
        //                        objfileupload.Response.Message = "Please check uploaded form is LLP 11";
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                objfileupload.Response.Error = true;
        //                objfileupload.Response.Message = "Form not found";
        //            }
        //        }

        //    }
        //    catch (System.Data.Entity.Validation.DbEntityValidationException e)
        //    {
        //        objfileupload.Response.Error = true;
        //        objfileupload.Response.Message = SecretarialConst.Messages.validationError;

        //        string errMsg = string.Empty;
        //        foreach (var eve in e.EntityValidationErrors)
        //        {
        //            //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //            errMsg = string.Empty;
        //            foreach (var ve in eve.ValidationErrors)
        //            {
        //                errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
        //                LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        objfileupload.Response.Error = true;
        //        objfileupload.Response.Message = SecretarialConst.Messages.serverError;                
        //    }
        //    return objfileupload;
        //}

        #endregion

        #region Import LLp FLiip
        public VMEntityFileUpload ImportDatafrom_LLPFliip(VMEntityFileUpload objfileupload)
        {
            objfileupload.Response = new Response();
            try
            {
                #region variable define for LLP-11 Read for Entity
                long EntityID = -1;
                string Message = "";
                string EntityName = string.Empty;
                string LLPIN = string.Empty;
                string Email = string.Empty;
                string RegisteredAddress1 = string.Empty;
                string RegisteredAddress2 = string.Empty;
                string state = string.Empty;
                int stateId = 0;
                string City = string.Empty;
                int CityId = 0;
                string pin = string.Empty;
                long TotalMonateryvalContributionbyparterar = 0;
                string PrincipanBusinessActivity = string.Empty;
                string Contribution = string.Empty;
                string ContributionRecived = string.Empty;
                long Maindivisionbusiness = 0;
                decimal ObligationOfContribution = 0;
                int TotalDegignetedPartner = 0;
                int i = 0;
                int k = 0;
                LLpPartnarVM _objpartner = new LLpPartnarVM();
                LLpBodyCorporateVM _objbodycorporate = new LLpBodyCorporateVM();
                BodyCorporateNomineeVM _objnominee = new BodyCorporateNomineeVM();
                #endregion

                if (objfileupload.File != null)
                {
                    if (objfileupload.File.ContentLength > 0)
                    {
                        string myFilePath = objfileupload.File.FileName;
                        string ext = System.IO.Path.GetExtension(myFilePath);
                        if (ext == ".pdf")
                        {
                            string _path;

                            _path = GetFormsPath(objfileupload);

                            if (!string.IsNullOrEmpty(_path))
                            {
                                PdfReader pdfReader = new PdfReader(_path);
                                SubCompanyType objcompanysubtype = new SubCompanyType();
                                if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
                                {
                                    foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                    {

                                        #region capture EntityName of LLP
                                        if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM2[0].LLP_PROP_NAME1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                EntityName = (de.Value.InnerText).Trim();

                                            }
                                        }
                                        #endregion

                                        #region capture EmailId
                                        if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM2[0].REG_OFC_EMAIL_ID[0]")
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                Email = de.Value.InnerText;
                                            }
                                        }
                                        #endregion

                                        #region capture address Line 1
                                        if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM2[0].REG_OFC_LINE1[0]")
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                RegisteredAddress1 = de.Value.InnerText.Trim();

                                            }
                                        }
                                        #endregion

                                        #region capture address Line 2
                                        if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM2[0].REG_OFC_LINE2[0]")
                                        {

                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                RegisteredAddress1 = de.Value.InnerText.Trim();

                                            }
                                        }
                                        #endregion

                                        #region state
                                        if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM2[0].REG_OFC_STATE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                stateId = GetRegisterStateId(de.Value.InnerText.Trim());
                                            }
                                        }
                                        #endregion

                                        #region city
                                        if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM2[0].REG_OFC_CITY[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                CityId = getCityRegisterID(stateId, de.Value.InnerText.Trim());

                                            }
                                        }
                                        #endregion

                                        #region PIN
                                        if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM2[0].REG_OFC_PIN_CODE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                pin = de.Value.InnerText.Trim();

                                            }
                                        }
                                        #endregion
                                        // data[0].ZMCA_LLP_FORM2[0].DESC_INDUS_ACT[0]
                                        #region Main division of business activity
                                        if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM2[0].DESC_INDUS_ACT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                Maindivisionbusiness = GetMaindivisionActivity(de.Value.InnerText.Trim());

                                            }
                                        }
                                        #endregion

                                        #region obligation of contribution
                                        if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM2[0].MONETARY_CONTRI[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                ObligationOfContribution = Convert.ToDecimal(de.Value.InnerText);

                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_LLP_FORM2[0].TOTAL_DESIG_PART[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                TotalDegignetedPartner = Convert.ToInt32(de.Value.InnerText);

                                            }
                                        }
                                        #endregion

                                    }


                                    if (!string.IsNullOrEmpty(EntityName))
                                    {
                                        com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                                        {
                                            //ID = _objentity.CustomerBranchId,
                                            Name = EntityName,
                                            //Type = Convert.ToByte(ddlType.SelectedValue),
                                            // ComType = Convert.ToByte(comType),
                                            //AddressLine1 = _objentity.Regi_Address_Line1,
                                            //AddressLine2 = _objentity.Regi_Address_Line2,
                                            //StateID = _objentity.Regi_StateId,
                                            //CityID = _objentity.Regi_CityId,
                                            //Others = "",
                                            //PinCode = _objentity.Regi_PINCode,
                                            ContactPerson = "",
                                            Landline = "",
                                            Mobile = "",
                                            EmailID = Email,
                                            CustomerID = objfileupload.CustomerId,
                                            //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                                            //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                                        };

                                        customerBranch = CreateCustomerBranch(customerBranch, 3, false, out Message);

                                        if (customerBranch.ID > 0)
                                        {
                                            BM_SubEntityMapping objsubentity = new BM_SubEntityMapping();
                                            var checkEntity = (from row in entities.BM_EntityMaster where row.CompanyName == EntityName && row.Is_Deleted == false && row.Customer_Id == objfileupload.CustomerId select row).FirstOrDefault();

                                            //var checkEntity = (from row in entities.BM_EntityMaster where row.CIN_LLPIN == CIN && row.Is_Deleted == false select row).FirstOrDefault();
                                            if (checkEntity == null)
                                            {

                                                BM_EntityMaster objentityMaster = new BM_EntityMaster
                                                {
                                                    CompanyName = EntityName,
                                                    Entity_Type = 3,
                                                    CIN_LLPIN = LLPIN,

                                                    IncorporationDate = DateTime.Now,
                                                    Registration_No = "",
                                                    Regi_Address_Line1 = RegisteredAddress1,
                                                    Regi_Address_Line2 = "",
                                                    Regi_StateId = stateId,
                                                    Regi_CityId = CityId,
                                                    Regi_PINCode = pin,
                                                    // CompanySubCategory = CompanyCategorysubType,
                                                    CompanyCategory_Id = 0,
                                                    IS_Listed = false,
                                                    Email_Id = Email,
                                                    FY_CY = "FY",
                                                    //  WebSite = Website,
                                                    PAN = "",
                                                    CreatedOn = DateTime.Now,
                                                    CreatedBy = objfileupload.UserId,
                                                    Is_Deleted = false,
                                                    Customer_Id = objfileupload.CustomerId,
                                                    CustomerBranchId = customerBranch.ID,
                                                    Maindivisonbusiness = Maindivisionbusiness > 0 ? (int)Maindivisionbusiness : 0,
                                                    Obligation_Of_Contribution = ObligationOfContribution
                                                };
                                                //    No_Of_Partners = _objentity.llpNo_Of_Partners;

                                                //    No_Of_DesignatedPartners = _objentity.llpNo_Of_DesignatedPartners;

                                                //    Obligation_Of_Contribution = _objentity.llpObligation_Of_Contribution;



                                                entities.BM_EntityMaster.Add(objentityMaster);
                                                entities.SaveChanges();
                                                if (objentityMaster.Id > 0)
                                                {
                                                    EntityID = objentityMaster.Id;
                                                }
                                            }
                                            else
                                            {
                                                checkEntity.CompanyName = EntityName;
                                                checkEntity.Entity_Type = 3;
                                                checkEntity.CIN_LLPIN = LLPIN;
                                                //checkEntity.GLN = GLN;
                                                checkEntity.IncorporationDate = DateTime.Now;
                                                checkEntity.Registration_No = "";
                                                checkEntity.Regi_Address_Line1 = RegisteredAddress1;
                                                checkEntity.Regi_Address_Line2 = "";
                                                checkEntity.Regi_StateId = stateId;
                                                checkEntity.Regi_CityId = CityId;
                                                checkEntity.CompanyCategory_Id = 0;
                                                checkEntity.IS_Listed = false;
                                                checkEntity.Email_Id = Email;
                                                // checkEntity.WebSite = Website;
                                                checkEntity.PAN = "";
                                                checkEntity.UpdatedOn = DateTime.Now;
                                                checkEntity.UpdatedBy = objfileupload.UserId;
                                                checkEntity.CustomerBranchId = customerBranch.ID;
                                                checkEntity.Is_Deleted = false;
                                                checkEntity.Customer_Id = objfileupload.CustomerId;
                                                checkEntity.FY_CY = "FY";
                                                checkEntity.Maindivisonbusiness = Maindivisionbusiness > 0 ? (int)Maindivisionbusiness : 0;
                                                checkEntity.Obligation_Of_Contribution = ObligationOfContribution;
                                                entities.SaveChanges();
                                                if (checkEntity.Id > 0)
                                                {
                                                    EntityID = checkEntity.Id;
                                                }
                                            }
                                            if (EntityID > 0)
                                            {

                                                while (i < TotalDegignetedPartner)
                                                {
                                                    foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                                    {
                                                        #region Partner
                                                        #region Name
                                                        if (de.Key.ToString() == "data[0].T_IDP_DIN[0].DATA[" + i + "].PARTNER_NAME[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objpartner.Name = de.Value.InnerText.Trim();
                                                                // _objpartner.Add(new LLpPartnarVM { Name = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion

                                                        #region EmailId
                                                        if (de.Key.ToString() == "data[0].T_IDP_DIN[0].DATA[" + i + "].EMAIL_ID[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objpartner.EmailId = de.Value.InnerText.Trim();
                                                                //_objpartner.Add(new LLpPartnarVM { EmailId = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion

                                                        #region DPIN
                                                        if (de.Key.ToString() == "data[0].T_IDP_DIN[" + i + "].DATA[0].DPIN[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objpartner.DPIN = de.Value.InnerText.Trim();
                                                                //_objpartner.Add(new LLpPartnarVM { DPIN = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Nationality
                                                        if (de.Key.ToString() == "data[0].T_IDP_DIN[0].DATA[" + i + "].PARTNER_NATION[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objpartner.Nationality = de.Value.InnerText.Trim();
                                                                // _objpartner.Add(new LLpPartnarVM { Nationality = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Partner Resident in India
                                                        if (de.Key.ToString() == "data[0].T_IDP_DIN[0].DATA[" + i + "].PARTNER_IND_RES[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                string ResidentinIndia = "";

                                                                ResidentinIndia = de.Value.InnerText.Trim();

                                                                if (ResidentinIndia.ToUpper() == "YES")
                                                                {
                                                                    _objpartner.IsResidentinIndia = true;
                                                                }
                                                                else
                                                                {
                                                                    _objpartner.IsResidentinIndia = false;
                                                                }
                                                                // _objpartner.Add(new LLpPartnarVM { IsResidentinIndia = Convert.ToBoolean(de.Value.InnerText.Trim()) });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Date of Birdth
                                                        if (de.Key.ToString() == "data[0].T_IDP_DIN[0].DATA[" + i + "].PARTNER_DOB[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objpartner.DOB = Convert.ToDateTime(de.Value.InnerText);
                                                                // _objpartner.Add(new LLpPartnarVM { DOB = Convert.ToDateTime(de.Value.InnerText) });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Occupation
                                                        if (de.Key.ToString() == "data[0].T_IDP_DIN[0].DATA[" + i + "].PARTNER_OCCU[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objpartner.Occupation = de.Value.InnerText.Trim();
                                                                // _objpartner.Add(new LLpPartnarVM { Occupation = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Gender
                                                        if (de.Key.ToString() == "data[0].T_IDP_DIN[0].DATA[" + i + "].GENDER[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objpartner.Gender = de.Value.InnerText.Trim();
                                                                // _objpartner.Add(new LLpPartnarVM { Gender = de.Value.InnerText.Trim() });
                                                            }
                                                        }
                                                        #endregion

                                                        #region Form of Contribution
                                                        if (de.Key.ToString() == "data[0].T_IDP_DIN[0].DATA[" + i + "].FORM_CONTRI[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objpartner.FormContribution = de.Value.InnerText.Trim();
                                                                // _objpartner.Add(new LLpPartnarVM { FormContribution = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Contribution Recived
                                                        if (de.Key.ToString() == "data[0].T_IDP_DIN[0].DATA[" + i + "].MONETARY_CONTRI[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objpartner.ContributionRecived = Convert.ToDecimal(de.Value.InnerText);
                                                                // _objpartner.Add(new LLpPartnarVM { ContributionRecived = Convert.ToDecimal(de.Value.InnerText) });

                                                            }
                                                        }
                                                        #endregion

                                                        #endregion

                                                        k = 0;
                                                        #region Body Corporate
                                                        #region Type
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].BODY_CORP_TYPE[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {

                                                                _objbodycorporate.Type = de.Value.InnerText.Trim();
                                                                if (_objbodycorporate.Type.ToUpper() == "CMP")
                                                                {
                                                                    _objbodycorporate.BodyCType = 1;
                                                                }

                                                            }
                                                        }
                                                        #endregion

                                                        #region CIN / FCIN / LLPIN
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].ID_NUM[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objbodycorporate.CIN_FCIN_LLPIN = de.Value.InnerText.Trim();

                                                            }
                                                        }
                                                        #endregion

                                                        #region DPIN
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].BODY_CORP_NAME[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objbodycorporate.ENTITYNAME = de.Value.InnerText.Trim();

                                                            }
                                                        }
                                                        #endregion

                                                        #region Nationality
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].BODY_CORP_CNTRY[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                string nationality = "";
                                                                nationality = de.Value.InnerText.Trim();
                                                                if (!string.IsNullOrEmpty(nationality))
                                                                {
                                                                    _objbodycorporate.Country = 1;
                                                                }
                                                                //_objpartner.Add(new LLpPartnarVM { Nationality = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion


                                                        #region Form of Contribution
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].FORM_CONTRI[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objbodycorporate.FormofContribution = de.Value.InnerText.Trim();
                                                                //_objpartner.Add(new LLpPartnarVM { FormContribution = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Contribution Recived
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].MONETARY_CONTRI[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objbodycorporate.MonetryofContribution = Convert.ToDecimal(de.Value.InnerText);
                                                                //_objpartner.Add(new LLpPartnarVM { ContributionRecived = Convert.ToDecimal(de.Value.InnerText) });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Address Line1
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].ADD_LINE1_BODY[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objbodycorporate.presentResidentAddressLine1 = de.Value.InnerText.Trim().ToString();
                                                                //_objpartner.Add(new LLpPartnarVM { ContributionRecived = Convert.ToDecimal(de.Value.InnerText) });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Address Line2
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].ADD_LINE2_BODY[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objbodycorporate.presentResidentAddressLine2 = de.Value.InnerText.Trim().ToString();
                                                                //_objpartner.Add(new LLpPartnarVM { ContributionRecived = Convert.ToDecimal(de.Value.InnerText) });

                                                            }
                                                        }
                                                        #endregion

                                                        #region State
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].ADD_STATE_BODY[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                string state1 = "";

                                                                state1 = de.Value.InnerText.Trim().ToString();
                                                                _objbodycorporate.stateId = getStateId(de.Value.InnerText.Trim());
                                                                //_objpartner.Add(new LLpPartnarVM { ContributionRecived = Convert.ToDecimal(de.Value.InnerText) });

                                                            }
                                                        }
                                                        #endregion

                                                        #region City
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].ADD_CITY_BODY[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                string city = "";

                                                                city = de.Value.InnerText.Trim().ToString();
                                                                _objbodycorporate.cityID = getCityId(de.Value.InnerText.Trim());
                                                                //_objpartner.Add(new LLpPartnarVM { ContributionRecived = Convert.ToDecimal(de.Value.InnerText) });

                                                            }
                                                        }
                                                        #endregion

                                                        #region PIN
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].ADD_PIN_BODY[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {

                                                                _objbodycorporate.PIN = Convert.ToInt32(de.Value.InnerText.Trim());

                                                            }
                                                        }
                                                        #endregion

                                                        #region Email
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[" + k + "].BODY_CORP_EMAIL[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {

                                                                _objbodycorporate.EmailId = de.Value.InnerText.Trim();

                                                            }
                                                        }
                                                        #endregion
                                                        #endregion


                                                        #region Body Corporate Nominee
                                                        #region Name
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[0].PARTNER_NAME[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objnominee.Name = de.Value.InnerText.Trim();

                                                            }
                                                        }
                                                        #endregion

                                                        #region EmailId
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[0].EMAIL_ID[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objnominee.email = de.Value.InnerText.Trim();
                                                                //_objpartner.Add(new LLpPartnarVM { EmailId = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion

                                                        #region DPIN
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[0].DPIN[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objnominee.DPIN_DIN = de.Value.InnerText.Trim();
                                                                //_objpartner.Add(new LLpPartnarVM { DPIN = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Nationality
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[0].PARTNER_NATION[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objnominee.Nationality = 1;

                                                            }
                                                        }
                                                        #endregion

                                                        #region Partner Resident in India
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[0].PARTNER_IND_RES[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                string ResidentinIndia = "";

                                                                ResidentinIndia = de.Value.InnerText.Trim();

                                                                if (ResidentinIndia.ToUpper() == "YES")
                                                                {
                                                                    _objnominee.IsResidentinIndia = true;
                                                                }
                                                                else
                                                                {
                                                                    _objnominee.IsResidentinIndia = false;
                                                                }
                                                                // _objpartner.Add(new LLpPartnarVM { IsResidentinIndia = Convert.ToBoolean(de.Value.InnerText.Trim()) });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Date of Birdth
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[0].PARTNER_DOB[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objnominee.DOB = Convert.ToDateTime(de.Value.InnerText);

                                                            }
                                                        }
                                                        #endregion

                                                        #region Occupation
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[0].PARTNER_OCCU[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objnominee.Occupation = de.Value.InnerText.Trim();
                                                                // _objpartner.Add(new LLpPartnarVM { Occupation = de.Value.InnerText.Trim() });

                                                            }
                                                        }
                                                        #endregion

                                                        #region Gender
                                                        if (de.Key.ToString() == "data[0].T_BCDP_DIN[0].DATA[0].GENDER[0]")
                                                        {
                                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                                            {
                                                                _objnominee.Gender = 1;
                                                                // _objpartner.Add(new LLpPartnarVM { Gender = de.Value.InnerText.Trim() });
                                                            }
                                                        }
                                                        #endregion
                                                        #endregion

                                                    }
                                                    long llpPartnerId = 0;
                                                    #region check for partner
                                                    if (!string.IsNullOrEmpty(_objpartner.Name) && !string.IsNullOrEmpty(_objpartner.EmailId))
                                                    {

                                                        var checkforpartner = (from x in entities.BM_Partner
                                                                               where x.EmailId == _objpartner.EmailId
                                                                               && x.DPIN == _objpartner.DPIN
                                                                               && x.CustomerId == objfileupload.CustomerId
                                                                               select x).FirstOrDefault();
                                                        if (checkforpartner == null)
                                                        {
                                                            BM_Partner _obj_partnermaster = new BM_Partner
                                                            {
                                                                Name = _objpartner.Name,
                                                                EmailId = _objpartner.EmailId,
                                                                DPIN = _objpartner.DPIN,
                                                                PAN = _objpartner.Pan,
                                                                NationalityId = 1,
                                                                IsIndianResidency = true,
                                                                DOB = _objpartner.DOB,
                                                                Occupation = _objpartner.Occupation,
                                                                CustomerId = objfileupload.CustomerId,
                                                                IsActive = true,
                                                                CreatedOn = DateTime.Now,
                                                                Createby = objfileupload.UserId

                                                            };
                                                            entities.BM_Partner.Add(_obj_partnermaster);
                                                            entities.SaveChanges();
                                                            llpPartnerId = _obj_partnermaster.Id;
                                                        }
                                                        else
                                                        {
                                                            checkforpartner.Name = _objpartner.Name;
                                                            checkforpartner.EmailId = _objpartner.EmailId;
                                                            checkforpartner.DPIN = _objpartner.DPIN;
                                                            checkforpartner.PAN = _objpartner.Pan;
                                                            checkforpartner.NationalityId = 1;
                                                            checkforpartner.IsIndianResidency = true;
                                                            checkforpartner.DOB = _objpartner.DOB;
                                                            checkforpartner.Occupation = _objpartner.Occupation;
                                                            checkforpartner.CustomerId = objfileupload.CustomerId;
                                                            checkforpartner.IsActive = true;
                                                            checkforpartner.CreatedOn = DateTime.Now;
                                                            checkforpartner.Createby = objfileupload.UserId;

                                                            entities.SaveChanges();
                                                            llpPartnerId = checkforpartner.Id;
                                                        }

                                                        #endregion

                                                        #region Check for partner Entity Details
                                                        if (llpPartnerId > 0)
                                                        {
                                                            var checkpartnerdetails = (from y in entities.BM_LLPPartnerDetails
                                                                                       where y.PartnerId == llpPartnerId
                                                                                       && y.EntityId == EntityID
                                                                                       && y.CustomerId == objfileupload.CustomerId
                                                                                       select y).FirstOrDefault();
                                                            if (checkpartnerdetails == null)
                                                            {
                                                                BM_LLPPartnerDetails _objdetail = new BM_LLPPartnerDetails
                                                                {
                                                                    PartnerId = (int)llpPartnerId,
                                                                    EntityId = (int)EntityID,
                                                                    ObligationofContribution = _objpartner.ObligationofContribution,
                                                                    FormofContribution = _objpartner.FormContribution,
                                                                    IsActive = true,
                                                                    CustomerId = objfileupload.CustomerId,
                                                                    Createdby = objfileupload.UserId,
                                                                    CreatedOn = DateTime.Now,
                                                                };
                                                                entities.BM_LLPPartnerDetails.Add(_objdetail);
                                                                entities.SaveChanges();
                                                            }
                                                            else
                                                            {
                                                                checkpartnerdetails.PartnerId = (int)llpPartnerId;
                                                                checkpartnerdetails.EntityId = (int)EntityID;
                                                                checkpartnerdetails.ObligationofContribution = _objpartner.ObligationofContribution;
                                                                checkpartnerdetails.FormofContribution = _objpartner.FormContribution;
                                                                checkpartnerdetails.IsActive = true;
                                                                checkpartnerdetails.CustomerId = objfileupload.CustomerId;
                                                                checkpartnerdetails.Updatedby = objfileupload.UserId;
                                                                checkpartnerdetails.UpdatedOn = DateTime.Now;
                                                                entities.SaveChanges();
                                                            }
                                                        }

                                                    }
                                                    #endregion


                                                    #region Body Corporate Details 

                                                    long llpbodycorporateId = 0;
                                                    if ((!string.IsNullOrEmpty(_objbodycorporate.ENTITYNAME)) && (!string.IsNullOrEmpty(_objbodycorporate.CIN_FCIN_LLPIN)))
                                                    {

                                                        var checkforbodycorporate = (from x in entities.BM_Partner
                                                                                     where x.EmailId == _objbodycorporate.EmailId
                                                                                     && x.CIN_LLPIN_FCIN == _objbodycorporate.CIN_FCIN_LLPIN
                                                                                     && x.CustomerId == objfileupload.CustomerId
                                                                                     select x).FirstOrDefault();
                                                        if (checkforbodycorporate == null)
                                                        {
                                                            BM_Partner _obj_bodycorporatemaster = new BM_Partner
                                                            {
                                                                Name = _objbodycorporate.ENTITYNAME,
                                                                EmailId = _objbodycorporate.EmailId,
                                                                //DPIN = _objbodycorporate.CIN_FCIN_LLPIN,
                                                                //PAN = _objbodycorporate.pa,
                                                                NationalityId = 1,
                                                                IsIndianResidency = true,
                                                                // DOB = _objbodycorporate.,
                                                                //  Occupation = _objbodycorporate.Occupation,
                                                                CustomerId = objfileupload.CustomerId,
                                                                IsActive = true,
                                                                CreatedOn = DateTime.Now,
                                                                Createby = objfileupload.UserId

                                                            };
                                                            entities.BM_Partner.Add(_obj_bodycorporatemaster);
                                                            entities.SaveChanges();
                                                            llpbodycorporateId = _obj_bodycorporatemaster.Id;
                                                        }
                                                        else
                                                        {
                                                            checkforbodycorporate.Name = _objbodycorporate.ENTITYNAME;
                                                            checkforbodycorporate.EmailId = _objbodycorporate.EmailId;
                                                            // checkforbodycorporate.DPIN = _objbodycorporate.CIN_FCIN_LLPIN;
                                                            // checkforbodycorporate.PAN = _objbodycorporate.Pan;
                                                            checkforbodycorporate.NationalityId = 1;
                                                            checkforbodycorporate.IsIndianResidency = true;
                                                            // checkforbodycorporate.DOB = _objbodycorporate.DOB;
                                                            // checkforbodycorporate.Occupation = _objbodycorporate.Occupation;
                                                            checkforbodycorporate.CustomerId = objfileupload.CustomerId;
                                                            checkforbodycorporate.IsActive = true;
                                                            checkforbodycorporate.CreatedOn = DateTime.Now;
                                                            checkforbodycorporate.Createby = objfileupload.UserId;

                                                            entities.SaveChanges();
                                                            llpbodycorporateId = checkforbodycorporate.Id;
                                                        }
                                                        if (llpbodycorporateId > 0)
                                                        {
                                                            var checkpartnerdetails = (from y in entities.BM_LLPPartnerDetails
                                                                                       where y.PartnerId == llpbodycorporateId
                                                                                       && y.EntityId == EntityID
                                                                                       && y.CustomerId == objfileupload.CustomerId
                                                                                       select y).FirstOrDefault();
                                                            if (checkpartnerdetails == null)
                                                            {
                                                                BM_LLPPartnerDetails _objdetail = new BM_LLPPartnerDetails
                                                                {
                                                                    PartnerId = (int)llpbodycorporateId,
                                                                    EntityId = (int)EntityID,
                                                                    ObligationofContribution = _objbodycorporate.MonetryofContribution,
                                                                    FormofContribution = _objbodycorporate.FormofContribution,
                                                                    IsActive = true,
                                                                    CustomerId = objfileupload.CustomerId,
                                                                    Createdby = objfileupload.UserId,
                                                                    CreatedOn = DateTime.Now,
                                                                };
                                                                entities.BM_LLPPartnerDetails.Add(_objdetail);
                                                                entities.SaveChanges();
                                                            }
                                                            else
                                                            {
                                                                checkpartnerdetails.PartnerId = (int)llpbodycorporateId;
                                                                checkpartnerdetails.EntityId = (int)EntityID;
                                                                checkpartnerdetails.ObligationofContribution = _objbodycorporate.MonetryofContribution;
                                                                checkpartnerdetails.FormofContribution = _objbodycorporate.FormofContribution;
                                                                checkpartnerdetails.IsActive = true;
                                                                checkpartnerdetails.CustomerId = objfileupload.CustomerId;
                                                                checkpartnerdetails.Updatedby = objfileupload.UserId;
                                                                checkpartnerdetails.UpdatedOn = DateTime.Now;
                                                                entities.SaveChanges();
                                                            }

                                                            #region BodyCorporate Nominee
                                                            var checkbodycorporatenomnee = (from row in entities.BM_BodyCorporateNominee
                                                                                            where row.BodyCorporateId == llpbodycorporateId
                                                                                            // && row.dp
                                                                                            select row).FirstOrDefault();
                                                            if (checkbodycorporatenomnee == null)
                                                            {
                                                                BM_BodyCorporateNominee _objdetail = new BM_BodyCorporateNominee
                                                                {
                                                                    BodyCorporateId = (int)llpbodycorporateId,
                                                                    Name = _objnominee.Name,
                                                                    Gender = _objnominee.Gender,
                                                                    DOB = _objnominee.DOB,
                                                                    IsActive = true,
                                                                    CustomerId = objfileupload.CustomerId,
                                                                    Nationality = _objnominee.Nationality,
                                                                    IsIndianResidendy = _objnominee.IsResidentinIndia,
                                                                    Occupation = _objnominee.Occupation,
                                                                    Email = _objnominee.email,
                                                                    Createdby = objfileupload.UserId,
                                                                    CreatedOn = DateTime.Now,
                                                                };
                                                                entities.BM_BodyCorporateNominee.Add(_objdetail);
                                                                entities.SaveChanges();
                                                            }
                                                            else
                                                            {
                                                                checkbodycorporatenomnee.BodyCorporateId = (int)llpbodycorporateId;
                                                                checkbodycorporatenomnee.Name = _objnominee.Name;
                                                                checkbodycorporatenomnee.Gender = _objnominee.Gender;
                                                                checkbodycorporatenomnee.DOB = _objnominee.DOB;
                                                                checkbodycorporatenomnee.IsActive = true;
                                                                checkbodycorporatenomnee.CustomerId = objfileupload.CustomerId;
                                                                checkbodycorporatenomnee.Nationality = _objnominee.Nationality;
                                                                checkbodycorporatenomnee.IsIndianResidendy = _objnominee.IsResidentinIndia;
                                                                checkbodycorporatenomnee.Occupation = _objnominee.Occupation;
                                                                checkbodycorporatenomnee.Email = _objnominee.email;
                                                                checkbodycorporatenomnee.Updatedby = objfileupload.UserId;
                                                                checkbodycorporatenomnee.UpdatedOn = DateTime.Now;
                                                                entities.SaveChanges();
                                                            }
                                                            #endregion

                                                        }
                                                    }
                                                    #endregion
                                                    i++;
                                                }

                                            }

                                        }
                                        objfileupload.Response.Success = true;
                                        objfileupload.Response.Message = "LLP 11 Upload successfully";
                                    }

                                    else
                                    {
                                        objfileupload.Response.Error = true;
                                        objfileupload.Response.Message = "Entity Name can't be blank";
                                    }
                                }
                                else
                                {
                                    objfileupload.Response.Error = true;
                                    objfileupload.Response.Message = "Please check uploaded form is LLp-Fillip";
                                }
                            }
                            else
                            {
                                objfileupload.Response.Error = true;
                                objfileupload.Response.Message = "Something went wrong";
                            }
                        }
                        else
                        {
                            objfileupload.Response.Error = true;
                            objfileupload.Response.Message = "Please check uploaded form is LLp-Fillip";
                        }
                    }
                }
                else
                {
                    objfileupload.Response.Error = true;
                    objfileupload.Response.Message = "Form not found";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                objfileupload.Response.Error = true;
                objfileupload.Response.Message = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                objfileupload.Response.Error = true;
                objfileupload.Response.Message = SecretarialConst.Messages.serverError;
            }
            return objfileupload;
        }
        #endregion

        #endregion

        private long GetMaindivisionActivity(string mainbusinessactivity)
        {
            long businessactId = 0;
            try
            {
                businessactId = (from x in entities.BM_BusinessActivity
                                 where x.Description.Equals(mainbusinessactivity)
                                 select x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return businessactId;
        }

        private int getCityRegisterID(int stateId, string CityName)
        {
            var cityId = (from row in entities.Cities where row.Name.Contains(CityName) && row.StateId == stateId select row.ID).FirstOrDefault();
            return cityId;
        }

        private int GetRegisterStateId(string registerAddress)
        {
            var stateId = (from row in entities.States where row.Name.Contains(registerAddress) select row.ID).FirstOrDefault();
            return stateId;
        }

        //private CustomerBranch CreateCustomerBranch(CustomerBranch customerBranch, int Entity_Type, bool isListed, int customerId, out string message)
        //{
        //    throw new NotImplementedException();
        //}

        public com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch CreateCustomerBranch(com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch, int Entity_Type, bool IsListed, out string Message)
        {
            Message = "";
            customerBranch.LegalEntityTypeID = 1;
            customerBranch.Status = 1;
            customerBranch.Landline = "0";
            customerBranch.Mobile = "0";
            customerBranch.Type = 1;

            customerBranch.ContactPerson = " ";

            #region set Company Type
            customerBranch.ComType = null;

            if (IsListed == true)
            {
                customerBranch.ComType = 3;
            }
            else if (Entity_Type == 1 || Entity_Type == 9)
            {
                customerBranch.ComType = 2;
            }
            else if (Entity_Type == 2)
            {
                customerBranch.ComType = 1;
            }
            else
            {
                customerBranch.ComType = 4;
            }
            #endregion

            #region set Legal Entity type

            if (Entity_Type == 1)
            {
                customerBranch.LegalEntityTypeID = 6; //Private Limited Company
                customerBranch.LegalEntityTypeID = null;
            }
            else if (Entity_Type == 3)
            {
                customerBranch.LegalEntityTypeID = 4; //Limited Liability Partnership(LLP)
            }
            else if (Entity_Type == 2 && IsListed == true)
            {
                customerBranch.LegalEntityTypeID = 2; //Public Limited Company (Listed)
            }
            else if (Entity_Type == 2 && IsListed == false)
            {
                customerBranch.LegalEntityTypeID = 3; //Public Limited Company (Unlisted)
            }
            else if (Entity_Type == 10 && IsListed == true)
            {
                customerBranch.LegalEntityTypeID = 2; //Public Limited Company (Listed)
            }
            else if (Entity_Type == 10 && IsListed == false)
            {
                customerBranch.LegalEntityTypeID = 3; //Public Limited Company (Unlisted)
            }
            else if (Entity_Type == 7)
            {
                customerBranch.LegalEntityTypeID = 1; //Partnership
            }
            else if (Entity_Type == 6)
            {
                customerBranch.LegalEntityTypeID = 5; //Proprietorship
            }
            else if (Entity_Type == 8)
            {
                customerBranch.LegalEntityTypeID = 8; //One Person Company (O.P.C.)
            }
            else
            {
                customerBranch.LegalEntityTypeID = null;
            }
            #endregion

            #region Create Entity

            long CheckCustomerBranchIdExist = ExistsCustomerBranch_Compliance(customerBranch, customerBranch.CustomerID);

            com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch1 = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch()
            {
                ID = customerBranch.ID,
                Name = customerBranch.Name,
                Type = customerBranch.Type,
                ComType = customerBranch.ComType,
                AddressLine1 = customerBranch.AddressLine1,
                AddressLine2 = customerBranch.AddressLine2,
                StateID = customerBranch.StateID,
                CityID = customerBranch.CityID,
                Others = customerBranch.Others,
                PinCode = customerBranch.PinCode,
                ContactPerson = customerBranch.ContactPerson,
                Landline = customerBranch.Landline,
                Mobile = customerBranch.Mobile,
                EmailID = customerBranch.EmailID,
                CustomerID = customerBranch.CustomerID,
                //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                Status = customerBranch.Status
            };

            long checkCustomerBranchExist1 = ExistsCustomerBranch_Audit(customerBranch1, customerBranch1.CustomerID);
            if (CheckCustomerBranchIdExist > 0 && checkCustomerBranchExist1 > 0)
            {
                //CustomerBranchManagement.UpdateFromSecretarial(customerBranch);

                //CustomerBranchManagement.Update1FromSecretarial(customerBranch1);
                customerBranch.ID = (int)CheckCustomerBranchIdExist;
            }
            else
            {
                if (customerBranch.ID <= 0)
                {
                    int BranchId = CustomerBranchManagement.Create(customerBranch);

                    if (BranchId > 0)
                    {
                        customerBranch.ID = BranchId;
                        CustomerBranchManagement.Create1(customerBranch1);
                    }
                }
            }
            #endregion

            return customerBranch;
        }

        private long ExistsCustomerBranch_Compliance(com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch, int customerID)
        {
            var query = (from row in entities.CustomerBranches
                         where row.IsDeleted == false
                         && row.Name.Equals(customerBranch.Name) && row.CustomerID == customerID
                         select row).FirstOrDefault();

            if (query != null)
            {
                return query.ID;
            }
            else
            {
                return 0;
            }

        }

        public long ExistsCustomerBranch_Audit(com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch mst_CustomerBranch, int CustomerId)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false
                             && row.Name.Equals(mst_CustomerBranch.Name) && row.CustomerID == CustomerId
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return query.ID;
                }
                else
                {
                    return 0;
                }
            }
        }

        public IEnumerable<BM_CompanySubCategory> GetDropDownforCompanySubCategory()
        {
            try
            {
                var getCompanysubcategory = (from row in entities.BM_CompanySubCategory select row).ToList();
                return getCompanysubcategory;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<SubCompanyType> GetSubEntityDetails(long entityID, long customerID)
        {
            List<SubCompanyType> _objSubCompanyType = new List<SubCompanyType>();
            try
            {
                _objSubCompanyType = (from row in entities.BM_SubEntityMapping
                                      where row.EntityId == entityID
                                      && row.CustomerId == customerID
                                      && row.isActive == true
                                      select new SubCompanyType
                                      {
                                          EntityID = row.EntityId,
                                          SubcompanyId = row.Id,
                                          subcompanyTypeID = row.CompanysubType,
                                          SubCompanyName = row.NameofCompany,
                                          PersentageofShareholding = row.PerofShareHeld,
                                          subcompanyTypeName = (from x in entities.BM_CompanySubType where x.Id == row.CompanysubType select x.Name).FirstOrDefault(),
                                          CIN = row.CIN,
                                          ParentCIN = (from e in entities.BM_EntityMaster where e.Id == entityID select e.CIN_LLPIN).FirstOrDefault(),
                                          CustomerId = (int)customerID,
                                          //AssociateCompany = new 
                                          //{
                                          //    Id = row.CompanysubType,
                                          //    Name = (from x in entities.BM_CompanySubType where x.Id == row.CompanysubType select x.Name).FirstOrDefault(),
                                          //}
                                          AssociateCompany = new AssosiateType
                                          {
                                              Id = row.Id,
                                              Name = (from x in entities.BM_CompanySubType where x.Id == row.CompanysubType select x.Name).FirstOrDefault(),
                                          }
                                      }).ToList();
                return _objSubCompanyType;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objSubCompanyType;
            }
        }

        public List<AssosiateType> GetAllEntityMaster()
        {
            try
            {
                var getCompanysubType = (from row in entities.BM_CompanySubType
                                         where row.IsActive == true
                                         orderby row.Name
                                         select new AssosiateType
                                         {
                                             Id = row.Id,
                                             Name = row.Name
                                         }
                                         ).ToList();
                return getCompanysubType;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public void addCompanySubType(SubCompanyType items)
        {
            string CIN = String.Empty;
            long MappingId = 0;

            try
            {
                if (items.CIN != null)
                {
                    CIN = items.CIN.Trim();
                    var CheckIsCompanyExist = (from row in entities.BM_EntityMaster where row.CIN_LLPIN == CIN && row.Customer_Id == items.CustomerId && row.Is_Deleted == false select row).FirstOrDefault();
                    if (CheckIsCompanyExist != null)
                    {
                        #region Check Holding and Subsidiary
                        if (items.AssociateCompany.Id == 3 || items.AssociateCompany.Id == 2)
                        {



                            var checkExist = (from x in entities.BM_SubEntityMapping where x.EntityId == items.EntityID && x.CIN == CIN && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                            #region Create or Update Holding and Subsidiary
                            if (checkExist == null)
                            {
                                BM_SubEntityMapping objsubentitymapping = new BM_SubEntityMapping();
                                objsubentitymapping.EntityId = items.EntityID;
                                objsubentitymapping.ParentCIN = items.ParentCIN;
                                objsubentitymapping.CIN = CIN;
                                objsubentitymapping.CompanysubType = (int)items.AssociateCompany.Id;
                                objsubentitymapping.NameofCompany = items.SubCompanyName;
                                objsubentitymapping.PerofShareHeld = items.PersentageofShareholding;
                                objsubentitymapping.CustomerId = items.CustomerId;
                                objsubentitymapping.Createdon = DateTime.Now;
                                objsubentitymapping.isActive = true;
                                entities.BM_SubEntityMapping.Add(objsubentitymapping);
                                entities.SaveChanges();
                                MappingId = objsubentitymapping.Id;
                            }
                            else
                            {
                                checkExist.EntityId = items.EntityID;
                                checkExist.ParentCIN = items.ParentCIN;
                                checkExist.CIN = CIN;
                                checkExist.CompanysubType = (int)items.AssociateCompany.Id;
                                checkExist.NameofCompany = items.SubCompanyName;
                                checkExist.PerofShareHeld = items.PersentageofShareholding;
                                checkExist.CustomerId = items.CustomerId;
                                checkExist.Createdon = DateTime.Now;
                                checkExist.isActive = true;
                                entities.SaveChanges();
                                MappingId = checkExist.Id;
                            }
                            if (MappingId > 0)
                            {
                                var checkcomexist = (from x in entities.BM_SubEntityMapping where x.EntityId == CheckIsCompanyExist.Id && x.CIN == CheckIsCompanyExist.CIN_LLPIN && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                                if (checkcomexist == null)
                                {
                                    BM_SubEntityMapping objsubentitymapping1 = new BM_SubEntityMapping();
                                    objsubentitymapping1.EntityId = CheckIsCompanyExist.Id;
                                    objsubentitymapping1.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    objsubentitymapping1.CIN = CIN;
                                    if (items.AssociateCompany.Id == 3)
                                    {
                                        objsubentitymapping1.CompanysubType = 2;
                                    }
                                    else
                                    {
                                        objsubentitymapping1.CompanysubType = 3;
                                    }
                                    objsubentitymapping1.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID select a.CompanyName).FirstOrDefault();
                                    objsubentitymapping1.PerofShareHeld = items.PersentageofShareholding;
                                    objsubentitymapping1.CustomerId = items.CustomerId;
                                    objsubentitymapping1.Createdon = DateTime.Now;
                                    objsubentitymapping1.isActive = true;
                                    entities.BM_SubEntityMapping.Add(objsubentitymapping1);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkcomexist.EntityId = CheckIsCompanyExist.Id;
                                    checkcomexist.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    checkcomexist.CIN = CIN;
                                    if (items.AssociateCompany.Id == 3)
                                    {
                                        checkcomexist.CompanysubType = 2;
                                    }
                                    else
                                    {
                                        checkcomexist.CompanysubType = 3;
                                    }
                                    checkcomexist.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID select a.CompanyName).FirstOrDefault();
                                    checkcomexist.PerofShareHeld = items.PersentageofShareholding;
                                    checkcomexist.CustomerId = items.CustomerId;
                                    checkcomexist.Createdon = DateTime.Now;
                                    checkcomexist.isActive = true;

                                    entities.SaveChanges();
                                }
                            }
                        }

                        #endregion
                        #endregion

                        #region check Associate and Investing
                        else if (items.AssociateCompany.Id == 1 || items.AssociateCompany.Id == 5)
                        {
                            var checkExist = (from x in entities.BM_SubEntityMapping where x.CIN == CIN && x.EntityId == items.EntityID && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                            if (checkExist == null)
                            {
                                BM_SubEntityMapping objsubentitymapping = new BM_SubEntityMapping();
                                objsubentitymapping.EntityId = items.EntityID;
                                objsubentitymapping.ParentCIN = items.ParentCIN;
                                objsubentitymapping.CIN = CIN;
                                objsubentitymapping.CompanysubType = (int)items.AssociateCompany.Id;
                                objsubentitymapping.NameofCompany = items.SubCompanyName;
                                objsubentitymapping.PerofShareHeld = items.PersentageofShareholding;
                                objsubentitymapping.CustomerId = items.CustomerId;
                                objsubentitymapping.Createdon = DateTime.Now;
                                objsubentitymapping.isActive = true;
                                entities.BM_SubEntityMapping.Add(objsubentitymapping);
                                entities.SaveChanges();
                                MappingId = objsubentitymapping.Id;
                            }
                            else
                            {
                                checkExist.EntityId = items.EntityID;
                                checkExist.ParentCIN = items.ParentCIN;
                                checkExist.CIN = CIN;
                                checkExist.CompanysubType = (int)items.AssociateCompany.Id;
                                checkExist.NameofCompany = items.SubCompanyName;
                                checkExist.PerofShareHeld = items.PersentageofShareholding;
                                checkExist.CustomerId = items.CustomerId;
                                checkExist.Createdon = DateTime.Now;
                                checkExist.isActive = true;
                                entities.SaveChanges();
                                MappingId = checkExist.Id;
                            }

                            if (MappingId > 0)
                            {
                                var checkcomexist = (from x in entities.BM_SubEntityMapping where x.EntityId == CheckIsCompanyExist.Id && x.CIN == CheckIsCompanyExist.CIN_LLPIN && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                                if (checkcomexist == null)
                                {
                                    BM_SubEntityMapping objsubentitymapping1 = new BM_SubEntityMapping();
                                    objsubentitymapping1.EntityId = CheckIsCompanyExist.Id;
                                    objsubentitymapping1.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    objsubentitymapping1.CIN = CIN;

                                    if (items.AssociateCompany.Id == 1)
                                    {
                                        objsubentitymapping1.CompanysubType = 5;
                                    }
                                    else
                                    {
                                        objsubentitymapping1.CompanysubType = 1;
                                    }
                                    objsubentitymapping1.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID select a.CompanyName).FirstOrDefault();
                                    objsubentitymapping1.PerofShareHeld = items.PersentageofShareholding;
                                    objsubentitymapping1.CustomerId = items.CustomerId;
                                    objsubentitymapping1.Createdon = DateTime.Now;
                                    objsubentitymapping1.isActive = true;
                                    entities.BM_SubEntityMapping.Add(objsubentitymapping1);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkcomexist.EntityId = CheckIsCompanyExist.Id;
                                    checkcomexist.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    checkcomexist.CIN = CIN;

                                    if (items.AssociateCompany.Id == 1)
                                    {
                                        checkcomexist.CompanysubType = 5;
                                    }
                                    else
                                    {
                                        checkcomexist.CompanysubType = 1;
                                    }
                                    checkcomexist.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID select a.CompanyName).FirstOrDefault();
                                    checkcomexist.PerofShareHeld = items.PersentageofShareholding;
                                    checkcomexist.CustomerId = items.CustomerId;
                                    checkcomexist.Createdon = DateTime.Now;
                                    checkcomexist.isActive = true;

                                    entities.SaveChanges();
                                }
                            }

                        }
                        #endregion

                        #region JointVenture
                        else if (items.AssociateCompany.Id == 4)
                        {

                            var checkExist = (from x in entities.BM_SubEntityMapping where x.CIN == CIN && x.EntityId == items.EntityID && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                            if (checkExist == null)
                            {
                                BM_SubEntityMapping objsubentitymapping = new BM_SubEntityMapping();
                                objsubentitymapping.EntityId = items.EntityID;
                                objsubentitymapping.ParentCIN = items.ParentCIN;
                                objsubentitymapping.CIN = CIN;
                                objsubentitymapping.CompanysubType = (int)items.AssociateCompany.Id;
                                objsubentitymapping.NameofCompany = items.SubCompanyName;
                                objsubentitymapping.PerofShareHeld = items.PersentageofShareholding;
                                objsubentitymapping.CustomerId = items.CustomerId;
                                objsubentitymapping.Createdon = DateTime.Now;
                                objsubentitymapping.isActive = true;
                                entities.BM_SubEntityMapping.Add(objsubentitymapping);
                                entities.SaveChanges();
                                MappingId = objsubentitymapping.Id;
                            }
                            else
                            {

                                checkExist.EntityId = items.EntityID;
                                checkExist.ParentCIN = items.ParentCIN;
                                checkExist.CIN = CIN;
                                checkExist.CompanysubType = (int)items.AssociateCompany.Id;
                                checkExist.NameofCompany = items.SubCompanyName;
                                checkExist.PerofShareHeld = items.PersentageofShareholding;
                                checkExist.CustomerId = items.CustomerId;
                                checkExist.Updatedon = DateTime.Now;
                                checkExist.isActive = true;

                                entities.SaveChanges();
                                MappingId = checkExist.Id;

                                //BM_SubEntityMapping objsubentitymapping = new BM_SubEntityMapping();
                                //objsubentitymapping.EntityId = items.EntityID;
                                //objsubentitymapping.ParentCIN = items.ParentCIN;
                                //objsubentitymapping.CIN = CIN;
                                //objsubentitymapping.CompanysubType = (int)items.AssociateCompany.Id;
                                //objsubentitymapping.NameofCompany = items.SubCompanyName;
                                //objsubentitymapping.PerofShareHeld = items.PersentageofShareholding;
                                //objsubentitymapping.CustomerId = items.CustomerId;
                                //objsubentitymapping.Createdon = DateTime.Now;
                                //objsubentitymapping.isActive = true;
                                //entities.BM_SubEntityMapping.Add(objsubentitymapping);
                                //entities.SaveChanges();
                                //MappingId = objsubentitymapping.Id;
                            }


                            if (MappingId > 0)
                            {
                                var checkcomexist = (from x in entities.BM_SubEntityMapping where x.EntityId == CheckIsCompanyExist.Id && x.CIN == CheckIsCompanyExist.CIN_LLPIN && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                                if (checkcomexist == null)
                                {
                                    BM_SubEntityMapping objsubentitymapping1 = new BM_SubEntityMapping();
                                    objsubentitymapping1.EntityId = CheckIsCompanyExist.Id;
                                    objsubentitymapping1.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    objsubentitymapping1.CIN = CIN;
                                    objsubentitymapping1.CompanysubType = 4;
                                    objsubentitymapping1.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID && a.Customer_Id == items.CustomerId select a.CompanyName).FirstOrDefault();
                                    objsubentitymapping1.PerofShareHeld = items.PersentageofShareholding;
                                    objsubentitymapping1.CustomerId = items.CustomerId;
                                    objsubentitymapping1.Createdon = DateTime.Now;
                                    objsubentitymapping1.isActive = true;
                                    entities.BM_SubEntityMapping.Add(objsubentitymapping1);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkcomexist.EntityId = CheckIsCompanyExist.Id;
                                    checkcomexist.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    checkcomexist.CIN = CIN;
                                    checkcomexist.CompanysubType = 4;
                                    checkcomexist.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID && a.Customer_Id == items.CustomerId select a.CompanyName).FirstOrDefault();
                                    checkcomexist.PerofShareHeld = items.PersentageofShareholding;
                                    checkcomexist.CustomerId = items.CustomerId;
                                    checkcomexist.Createdon = DateTime.Now;
                                    checkcomexist.isActive = true;

                                    entities.SaveChanges();
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        var checkIsExistsubcompany = (from x in entities.BM_SubEntityMapping where x.EntityId == items.EntityID && x.CIN == items.CIN && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                        if (checkIsExistsubcompany == null)
                        {
                            BM_SubEntityMapping objsubentitymapping = new BM_SubEntityMapping();
                            objsubentitymapping.EntityId = items.EntityID;
                            objsubentitymapping.ParentCIN = items.ParentCIN;
                            objsubentitymapping.CIN = CIN;
                            objsubentitymapping.CompanysubType = (int)items.AssociateCompany.Id;
                            objsubentitymapping.NameofCompany = items.SubCompanyName;
                            objsubentitymapping.PerofShareHeld = items.PersentageofShareholding;
                            objsubentitymapping.CustomerId = items.CustomerId;
                            objsubentitymapping.Createdon = DateTime.Now;
                            objsubentitymapping.isActive = true;
                            entities.BM_SubEntityMapping.Add(objsubentitymapping);
                            entities.SaveChanges();
                        }
                        else
                        {
                            checkIsExistsubcompany.EntityId = items.EntityID;
                            checkIsExistsubcompany.ParentCIN = items.ParentCIN;
                            checkIsExistsubcompany.CIN = CIN;
                            checkIsExistsubcompany.CompanysubType = (int)items.AssociateCompany.Id;
                            checkIsExistsubcompany.NameofCompany = items.SubCompanyName;
                            checkIsExistsubcompany.PerofShareHeld = items.PersentageofShareholding;
                            checkIsExistsubcompany.CustomerId = items.CustomerId;
                            checkIsExistsubcompany.Createdon = DateTime.Now;
                            checkIsExistsubcompany.isActive = true;

                            entities.SaveChanges();
                        }
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State)
                            + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        // LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DeleteSubEntityMapping(SubCompanyType items)
        {
            try
            {
                var CheckIsCompanyExist = (from row in entities.BM_SubEntityMapping where row.CIN == items.CIN.Trim() && row.isActive == true && row.Id == items.SubcompanyId select row).FirstOrDefault();
                if (CheckIsCompanyExist != null)
                {
                    CheckIsCompanyExist.isActive = false;
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public SubCompanyType getDetailsofentity(long entityId, string cIN, int CustomerId)
        {
            try
            {
                var getentityDetils = (from row in entities.BM_EntityMaster
                                       where row.CIN_LLPIN == cIN

                                       select new SubCompanyType
                                       {
                                           CIN = row.CIN_LLPIN,
                                           EntityID = entityId,
                                           ParentCIN = cIN,
                                           SubCompanyName = row.CompanyName,

                                       }).FirstOrDefault();
                return getentityDetils;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #region Entity Details According to BOD Created by Ruchi
        public List<EntityMasterVM> GetAllEntityForBOD(int customerId, int userID)
        {
            try
            {
                var getlistofEntity = (from row in entities.BM_SP_EntityListforBOD(userID, customerId)
                                       orderby row.CompanyName
                                       select new EntityMasterVM
                                       {
                                           Id = row.ID,
                                           EntityName = row.CompanyName,
                                           LLPI_CIN = row.CIN_LLPIN,
                                           Type = row.EntityType
                                       }).ToList();
                return getlistofEntity;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<EntityMasterVM> EntityListdirectorwise(int userID)
        {
            try
            {
                var getentityforCommittee = (from row in entities.BM_SP_EntityListDirectorwiseforcommitee(userID)
                                             select new EntityMasterVM
                                             {
                                                 Id = (int)row.Id,
                                                 EntityName = row.CompanyName
                                             }).ToList();
                return getentityforCommittee;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public List<EntityMasterVM> GetAllEntityMasterbyid(int customerId, int userID, string role, long entityId)
        {
            DateTime date;
            var customerBranches = (from row in entities.BM_EntityMaster
                                    join rows in entities.BM_EntityType
                                    on row.Entity_Type equals (rows.Id)
                                    where row.Is_Deleted == false && row.Customer_Id == customerId && row.Id == entityId
                                    orderby row.CompanyName ascending
                                    select new EntityMasterVM
                                    {
                                        Id = row.Id,
                                        EntityName = row.CompanyName,
                                        Type = rows.EntityName,
                                        LLPI_CIN = row.CIN_LLPIN != "0" ? row.CIN_LLPIN : row.Registration_No,
                                        RegistrationNO = row.Registration_No,
                                        CIN = row.CIN_LLPIN,
                                        Entity_Type = row.Entity_Type,
                                        dateofIncorporaion = row.IncorporationDate,
                                        PAN = row.PAN,
                                        Islisted = row.IS_Listed,
                                        RocCode = (from r in entities.BM_ROC_code where r.Id == row.ROC_Code select r.Name).FirstOrDefault(),
                                        CompanyCategory = (from c in entities.BM_CompanyCategory where c.Id == row.CompanyCategory_Id select c.Category).FirstOrDefault(),
                                        CompanysubCategory = (from cs in entities.BM_CompanySubCategory where cs.Id == row.CompanySubCategory select cs.SubCategoryName).FirstOrDefault(),
                                        classofCompany = rows.EntityName,
                                        RegisterAddress = row.Regi_Address_Line1 + " " + row.Regi_Address_Line2,
                                        emailId = row.Email_Id

                                    });
            return customerBranches.ToList();
        }

        public List<EntityMasterVM> GetAssignedEntities(int customerID, int userID, string userRole)
        {
            try
            {
                var lstEntities = (from row in entities.BM_SP_GetAssignedEntities(userID, customerID, userRole)
                                   select new EntityMasterVM
                                   {
                                       Id = row.ID,
                                       EntityName = row.CompanyName,
                                       LLPI_CIN = row.CIN_LLPIN,
                                       Type = row.EntityType
                                   }).ToList();
                return lstEntities;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<EntityMasterVM> GetEntitiesDropDown(int customerID, int userID, string userRole)
        {
            try
            {
                var lstEntities = (from row in entities.BM_SP_GetEntitiesDropDown(userID, customerID, userRole)
                                   select new EntityMasterVM
                                   {
                                       Id = row.ID,
                                       EntityName = row.CompanyName,
                                       LLPI_CIN = row.CIN_LLPIN,
                                       Type = row.EntityType
                                   }).ToList();
                return lstEntities;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }


        #endregion
        public int? GetServiseProviderId(int CustomerId)
        {
            int? ServiceproviderId = 0;
            try
            {

                ServiceproviderId = (from cust in entities.Customers
                                     where cust.ID == CustomerId
                                     select cust.ServiceProviderID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return ServiceproviderId;
        }

        public int? GetParentID(int custID)
        {
            int? ParentID = 0;
            try
            {
                ParentID = (from cust in entities.Customers
                            where cust.ID == custID
                            select cust.ParentID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return ParentID;
        }

        #region Import Entity from AVACOM
        public List<ParentEntitySelectVM> GetEntityListForImport(int customerId)
        {
            var result = new List<ParentEntitySelectVM>();
            try
            {
                result = (from row in entities.BM_SP_EntityListForImportFromAVACOM(customerId)
                          select new ParentEntitySelectVM
                          {
                              ID = row.ID,
                              Name = row.Name,
                              Address = row.AddressLine1,
                              Email = row.EmailID,
                              IsCheked = false
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public IEnumerable<ParentEntitySelectVM> ImportEntityFromAVACOM(IEnumerable<ParentEntitySelectVM> lstEntities, int createdBy)
        {
            try
            {
                foreach (var item in lstEntities)
                {
                    BM_EntityMaster _obj = (from row in entities.BM_EntityMaster
                                            where row.CustomerBranchId == item.ID
                                            select row).FirstOrDefault();
                    if (_obj == null)
                    {
                        var objCustomerBranche = (from row in entities.CustomerBranches
                                                  where row.ID == item.ID && row.IsDeleted == false
                                                  select row).FirstOrDefault();
                        if (objCustomerBranche != null)
                        {
                            _obj = new BM_EntityMaster();

                            _obj.CompanyName = objCustomerBranche.Name.ToUpper();
                            _obj.Entity_Type = 0;
                            _obj.CIN_LLPIN = "0";
                            _obj.IncorporationDate = DateTime.Now;
                            _obj.ROC_Code = 0;

                            if (objCustomerBranche.AddressLine1 != "" && objCustomerBranche.AddressLine1 != null)
                                _obj.Regi_Address_Line1 = objCustomerBranche.AddressLine1.ToUpper();

                            if (objCustomerBranche.AddressLine2 != "" && objCustomerBranche.AddressLine2 != null)
                                _obj.Regi_Address_Line2 = objCustomerBranche.AddressLine2.ToUpper();
                            else
                                _obj.Regi_Address_Line2 = "NA";

                            _obj.Regi_StateId = objCustomerBranche.StateID;
                            _obj.Regi_CityId = objCustomerBranche.CityID;
                            _obj.IsCorp_Office = false;
                            _obj.CompanyCategory_Id = 0;

                            _obj.IS_Listed = false;
                            _obj.Email_Id = objCustomerBranche.EmailID;
                            _obj.PAN = "";

                            _obj.Customer_Id = objCustomerBranche.CustomerID;
                            _obj.CustomerBranchId = objCustomerBranche.ID;

                            _obj.Is_Deleted = false;
                            _obj.CreatedBy = createdBy;
                            _obj.CreatedOn = DateTime.Now;

                            _obj.Entity_Type = 0;

                            #region set Entity Type from Company Type

                            if (objCustomerBranche.ComType == 2)
                            {
                                _obj.Entity_Type = 1;
                            }
                            else if (objCustomerBranche.ComType == 1)
                            {
                                _obj.Entity_Type = 2;
                                _obj.IS_Listed = false;
                            }
                            else if (objCustomerBranche.ComType == 3)
                            {
                                _obj.Entity_Type = 10;
                                _obj.IS_Listed = true;
                            }

                            #endregion

                            #region set Entity Type from Legal entity Type
                            if (objCustomerBranche.LegalEntityTypeID == 6)//Private Limited Company
                            {
                                _obj.Entity_Type = 1;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 4)  //Limited Liability Partnership(LLP)
                            {
                                _obj.Entity_Type = 3;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 2)  //Public Limited Company (Listed)
                            {
                                _obj.Entity_Type = 10;
                                _obj.IS_Listed = true;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 3)  //Public Limited Company (Unlisted)
                            {
                                _obj.Entity_Type = 2;
                                _obj.IS_Listed = false;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 1)//Partnership
                            {
                                _obj.Entity_Type = 7;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 5)//Proprietorship
                            {
                                _obj.Entity_Type = 6;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 8)//One Person Company (O.P.C.)
                            {
                                _obj.Entity_Type = 8;
                            }

                            #endregion

                            if (_obj.Entity_Type > 0)
                            {
                                entities.BM_EntityMaster.Add(_obj);
                                entities.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return lstEntities;
        }

        public EntityDocumentVM CreateEntityDocuments(EntityDocumentVM _objdoc, int userId, int customerId)
        {
            bool checkExist = true;
            try
            {
                if (_objdoc.files != null)
                {
                    if (_objdoc.DocumentType == "MOA" || _objdoc.DocumentType == "AOA" || _objdoc.DocumentType == "COI")
                    {
                        checkExist = (from row in entities.BM_FileData
                                      join rows in entities.BM_EntityDocuments
                                      on new { Source = (long)row.EntityDocId }
                                      equals new { Source = rows.Id }
                                      where row.DocType == _objdoc.DocumentType
                                      && rows.EntityId == _objdoc.EntityId
                                      && rows.CustomerId == customerId
                                      && rows.IsActive == true
                                      select row
                                          ).Any();
                    }
                    else if (_objdoc.DocumentType == "LR")
                    {
                        checkExist = (from row in entities.BM_FileData
                                      join rows in entities.BM_EntityDocuments
                                      on new { Source = (long)row.EntityDocId }
                                      equals new { Source = rows.Id }
                                      where row.DocType == _objdoc.DocumentType
                                      && rows.EntityId == _objdoc.EntityId
                                      && rows.CustomerId == customerId
                                      && rows.IsActive == true
                                      && rows.License_RegNumber == _objdoc.LicenceNo
                                      select row
                                         ).Any();
                    }
                    else if (_objdoc.DocumentType == "AR")
                    {
                        checkExist = (from row in entities.BM_FileData
                                      join rows in entities.BM_EntityDocuments
                                      on new { Source = (long)row.EntityDocId }
                                      equals new { Source = rows.Id }
                                      where row.DocType == _objdoc.DocumentType
                                      && rows.EntityId == _objdoc.EntityId
                                      && rows.CustomerId == customerId
                                      && rows.IsActive == true
                                      && rows.License_RegNumber == _objdoc.LicenceNo
                                      && rows.FYID == _objdoc.FYID
                                      select row
                                         ).Any();
                    }
                    else if (_objdoc.DocumentType == "PO")
                    {
                        checkExist = (from row in entities.BM_FileData
                                      join rows in entities.BM_EntityDocuments
                                      on new { Source = (long)row.EntityDocId }
                                      equals new { Source = rows.Id }
                                      where row.DocType == _objdoc.DocumentType
                                      && rows.EntityId == _objdoc.EntityId
                                      && rows.CustomerId == customerId
                                      && rows.IsActive == true
                                      && rows.Description == _objdoc.Discription
                                      && rows.FYID == _objdoc.FYID
                                      select row
                                         ).Any();
                    }

                    if (!checkExist)
                    {
                        BM_EntityDocuments _objdocdetails = new BM_EntityDocuments
                        {
                            Description = _objdoc.Discription,
                            License_RegNumber = _objdoc.LicenceNo,
                            FYID = _objdoc.FYID,
                            StartTimeAGM = _objdoc.startTiming,
                            EndTimeAGM = _objdoc.EndTiming,
                            BMDate_LastRenew = _objdoc.BMDate,
                            GMDate_UpcomingRenew = _objdoc.GMDate,

                            IsActive = true,
                            CreatedBy = userId,
                            CreatedOn = DateTime.Now,
                            CustomerId = customerId,
                            EntityId = _objdoc.EntityId,
                        };
                        entities.BM_EntityDocuments.Add(_objdocdetails);
                        entities.SaveChanges();
                        if (_objdocdetails.Id > 0 && _objdoc.files != null)
                        {
                            if (_objdoc.files.Count() > 0)
                            {
                                bool savechange = FileUpload.SaveEntityDocument(_objdoc.files, _objdocdetails.Id, userId, customerId, _objdoc.DocumentType, _objdoc.EntityId);
                            }
                        }
                        _objdoc.Success = true;
                        _objdoc.Message = "Record saved successfully";
                    }
                    else
                    {
                        _objdoc.Error = true;
                        _objdoc.Message = "Record already exist";
                    }
                }
                else
                {
                    _objdoc.Error = true;
                    _objdoc.Message = "Please select File";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return _objdoc;
        }

        public List<BM_SP_GetEntityDocuments_Result> GetEntityDocuments(int UserId, int customerId, string Role, int entityId, string doctype)
        {
            List<BM_SP_GetEntityDocuments_Result> _obj = new List<BM_SP_GetEntityDocuments_Result>();
            _obj = (from x in entities.BM_SP_GetEntityDocuments(customerId, entityId)
                    select x).ToList();
            if (_obj.Count > 0)
            {
                if (doctype == "M")
                {
                    _obj = _obj.Where(x => x.DocType == "MOA" || x.DocType == "AOA" || x.DocType == "COI").ToList();
                }
                else
                {
                    _obj = _obj.Where(x => x.DocType == doctype).ToList();
                }
            }

            return _obj;
        }

        public IEnumerable<EntityDocTypeVM> GetDocumentType(int entityType)
        {
            List<EntityDocTypeVM> _objdoctype = new List<EntityDocTypeVM>();
            _objdoctype = (from x in entities.BM_SP_GetDocType(entityType)
                           select new EntityDocTypeVM
                           {
                               Id = x.ID,
                               DocCode = x.TypeCode,
                               Type = x.TypeDesc
                           }).ToList();

            return _objdoctype;
        }

        //public IEnumerable<EntityDocTypeVM> GetDocumentType()
        //{
        //    List<EntityDocTypeVM> _objdocType = new List<EntityDocTypeVM>();
        //    try
        //    {
        //        _objdocType = (from row in entities.BM_TypeMaster
        //                       where row.IsForEntity == true
        //                       select new EntityDocTypeVM
        //                       {
        //                           Id = row.ID,
        //                           DocCode = row.TypeCode,
        //                           Type = row.TypeDesc
        //                       }).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //    return _objdocType;
        //}

        public string Viewdocument(long id)
        {
            string EntityDoc = "";
            var CheckData = (from row in entities.BM_FileData
                             where row.Id == id
                             select row).FirstOrDefault();
            if (CheckData != null)
            {
                string filePath = System.IO.Path.Combine(HostingEnvironment.MapPath(CheckData.FilePath), CheckData.FileKey + System.IO.Path.GetExtension(CheckData.FileName));
                if (CheckData.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles/EntityDocumentData";
                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + File;

                    string extension = System.IO.Path.GetExtension(filePath);

                    //Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(HostingEnvironment.MapPath(DateFolder));
                    }

                    //string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = 36 + "" + 5 + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(HostingEnvironment.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(Masters.CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    bw.Close();

                    EntityDoc = FileName;
                    EntityDoc = EntityDoc.Substring(2, EntityDoc.Length - 2);

                }
            }
            return EntityDoc;
        }

        public bool Downloadrecords(long id)
        {
            try
            {
                var CheckData = (from row in entities.BM_FileData
                                 where row.Id == id
                                 select row).ToList();

                if (CheckData.Count > 0)
                {
                    bool IsDocumentDownload = downloadEntityDoc(CheckData);
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private bool downloadEntityDoc(List<BM_FileData> checkData)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile Document = new ZipFile())
                {
                    foreach (var files in checkData)
                    {
                        string filePath = System.IO.Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + System.IO.Path.GetExtension(files.FileName));

                        if (files.FilePath != null && File.Exists(filePath))
                        {
                            documentSuccess = true;
                            int idx = files.FileName.LastIndexOf('.');
                            string str = files.FileName.Substring(0, idx) + "_" + "." + files.FileName.Substring(idx + 1);
                            string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                            if (!Document.ContainsEntry("Document" + "/" + str))
                                Document.AddEntry("Document" + "/" + str, Masters.CryptographyHandler.AESDecrypt(ReadDocFiles(filePath)));
                        }
                    }
                    if (documentSuccess)
                    {
                        string fileNAme = "" + DateTime.Now;
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        Document.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + fileNAme + ".zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }

        public string GetFilePath(long id)
        {
            string path = string.Empty;
            try
            {
                path = (from x in entities.BM_FileData
                        where x.EntityId == id
                        && x.IsDeleted == false
                        select x.FilePath).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return path;
        }
        #endregion

        public bool CheckCompanyLogo(IEnumerable<HttpPostedFileBase> files)
        {
            bool success = false;
            try
            {
                #region check company logo

                if (files != null)
                {
                    if (files.Count() > 0)
                    {
                        foreach (var file in files)
                        {
                            string ext = System.IO.Path.GetExtension(file.FileName);

                            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".tif" || ext.ToLower() == ".png")
                            {
                                if (file.ContentLength <= TwentyFiveMB)
                                {
                                    success = true;
                                }
                                else
                                {
                                    success = false;
                                }
                            }
                            else
                            {
                                success = false;
                            }
                        }
                    }
                    else
                    {
                        success = true;
                    }
                }
                else
                {
                    success = true;
                }
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return success;
        }

        public bool DeleteDocdat(int id, int userId)
        {
            var checkExist = (from x in entities.BM_EntityDocuments
                              where x.Id == id
                              select x).FirstOrDefault();
            if (checkExist != null)
            {
                checkExist.IsActive = false;
                checkExist.UpdatedOn = DateTime.Now;
                checkExist.UpdatedBy = userId;
                entities.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<EntityBranchDetailsVM> GetEntityBranchList(int customerId, int userID, string role, int entityId)
        {
            List<EntityBranchDetailsVM> _objbranchdetails = new List<EntityBranchDetailsVM>();
            try
            {
                _objbranchdetails = (from x in entities.BM_SP_GetEntityBranchDetails(entityId, customerId)
                                     select new EntityBranchDetailsVM
                                     {
                                         location = x.Location,
                                         Type = x.Type,
                                         ContactPersone = x.ContactPerson,
                                         ContactNo = x.ContactNumber,
                                         EmailId = x.EmailID,
                                         Address = x.AddressLine1,
                                         Status = x.Status,
                                         BranchID = x.ID
                                     }
                                   ).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objbranchdetails;
        }

        public EntityBranchDetailsVM GetBranchDetails(int entityID, int branchId)
        {
            EntityBranchDetailsVM _objentitybranch = new EntityBranchDetailsVM();
            try
            {
                _objentitybranch = (from x in entities.CustomerBranches
                                    where x.ID == branchId
                                    select new EntityBranchDetailsVM
                                    {
                                        location = x.Name,
                                        TypeID = x.Type,
                                        ContactPersone = x.ContactPerson,
                                        ContactNo = x.Mobile,
                                        EmailId = x.EmailID,
                                        Address = x.AddressLine1,
                                        StatusId = x.Status
                                    }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objentitybranch;
        }

        public IEnumerable<SubIndustriesVM> GetRegulatoryAuthority()
        {
            List<SubIndustriesVM> _objregauth = new List<SubIndustriesVM>();
            _objregauth = (from x in entities.SubIndustries
                           where x.IndustryId == 11
                           select new SubIndustriesVM
                           {
                               Id = (int)x.ID,
                               Name = x.Name
                           }).ToList();

            return _objregauth;
        }

        public List<Body_CorporateVM> GetBodyCorporateNomnee(int customerId)
        {
            IQueryable<Body_CorporateVM> _objbodycor = null;
            List<Body_CorporateVM> _objlist = new List<Body_CorporateVM>();


            try
            {
                _objbodycor =
                       //(from x in entities.BM_EntityMaster.Where(x => x.Entity_Type.Equals(5) && x.Is_Deleted.Equals(false)
                       //                                       );


                       (from x in entities.BM_EntityMaster
                        where x.Is_Deleted == false
                     && x.Customer_Id == customerId
                     && x.Entity_Type == 5
                        select new Body_CorporateVM
                        {
                            Id = x.Id,
                            Name = x.CompanyName
                        }
                             );
                if (_objbodycor != null)
                {
                    _objlist = (from row in _objbodycor
                                select new Body_CorporateVM
                                {
                                    Id = row.Id,
                                    Name = row.Name
                                }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objlist;
        }


        #region Import LLp Form 11 Excel

        #endregion
        public VMEntityFileUpload ImportDatafrom_LLP11(VMEntityFileUpload objfileupload, int CustomerID, int UserId)
        {
            objfileupload.Response = new Response();

            objfileupload.Response.Success = true;

            List<string> message;
            string Message;
            int EntityId;
            long DirectorId;
            if (objfileupload.File != null)
            {
                if (objfileupload.File.ContentLength > 0)
                {
                    string excelfileName = string.Empty;
                    string path = "~/Areas/BM_Management/Documents/" + CustomerID + "/EntityMaster/";
                    string _file_Name = System.IO.Path.GetFileName(objfileupload.File.FileName);
                    string _path = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(path), _file_Name);
                    string ext = System.IO.Path.GetExtension(_file_Name);
                    if (ext == ".xlsx")
                    {
                        bool exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(path));
                        if (!exists)
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(path));
                        objfileupload.File.SaveAs(_path);

                        FileInfo excelfile = new FileInfo(_path);

                        if (excelfile != null)
                        {
                            using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                            {
                                bool flagPublic = checkExcelData(xlWorkbook, "Entity Master - LLP");

                                if (flagPublic == true)
                                {
                                    UploadEntityLLpData(xlWorkbook, out message, out Message, CustomerID, UserId, out EntityId);
                                    objfileupload.Response.Message = Message;
                                    objfileupload.Response.message = message;

                                    if (EntityId > 0)
                                    {
                                        bool flagcheckpartner = checkExcelData(xlWorkbook, "Partner Master");
                                        if (flagcheckpartner == true)
                                        {
                                            _objdirector.UploadPartner(xlWorkbook, out message, out Message, CustomerID, ref EntityId, UserId, out DirectorId);
                                            objfileupload.Response.Message = Message;
                                            objfileupload.Response.message = message;
                                            if (DirectorId > 0)
                                            {
                                                bool flagcheckBodyCorporate = checkExcelData(xlWorkbook, "Body corporate as partner");
                                                if (flagcheckpartner == true)
                                                {
                                                    UploadBodyCorporate(xlWorkbook, out message, out Message, CustomerID, ref DirectorId, ref EntityId, UserId);
                                                    objfileupload.Response.Message = Message;
                                                    objfileupload.Response.message = message;
                                                }
                                                else
                                                {
                                                    Message = "No Data Found in Excel Document or Sheet Name must be 'Partner Master'";
                                                    objfileupload.Response.Message = Message;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Message = "No Data Found in Excel Document or Sheet Name must be 'Body corporate as partner'";
                                            objfileupload.Response.Message = Message;
                                        }
                                    }

                                    else
                                    {
                                        objfileupload.Response.message = message;
                                        objfileupload.Response.Message = Message;
                                    }

                                }

                                else
                                {
                                    Message = "No Data Found in Excel Document or Sheet Name must be 'Entity Master - LLP'";
                                    objfileupload.Response.Message = Message;
                                }
                            }
                        }
                    }
                    else
                    {
                        Message = "File is only in Excel Format'";
                        objfileupload.Response.Message = Message;
                    }
                }
            }


            return objfileupload;
        }

        private void UploadBodyCorporate(ExcelPackage xlWorkbook, out List<string> message, out string Message, int customerID, ref long directorId, ref int EntityId, int userId)
        {
            List<string> errorMessage = new List<string>();
            message = new List<string>();
            Message = "";
            int EntityID = EntityId;
            int BodyCorporateId = -1;
            CultureInfo culture = new CultureInfo("en-US");

            try
            {
                bool saveSuccess = false;
                ExcelWorksheet excelAuditor = xlWorkbook.Workbook.Worksheets["Body corporate as partner"];

                if (excelAuditor != null)
                {
                    #region Data for Body Corporate as Partner in Form LLp 11

                    int count = 0;
                    int xlrow2 = excelAuditor.Dimension.End.Row;


                    string Type = string.Empty;
                    string CIN_FCIN = string.Empty;
                    string Name_Of_B_CORPT = string.Empty;

                    string AddressLine1 = string.Empty;
                    int Country = -1;

                    string PAN = string.Empty;
                    string MobileNO = string.Empty;

                    string EmailId = string.Empty;

                    long Obligation_Of_Contribution = -1;
                    long Contri_Rec_and_Accountant = -1;

                    int Category = -1;
                    string DPIN = string.Empty;
                    string Name = string.Empty;

                    string FirstName = string.Empty;
                    string MiddleName = string.Empty;
                    string LastName = string.Empty;

                    string Father_FirstName = string.Empty;
                    string Father_MiddleName = string.Empty;
                    string Father_LastName = string.Empty;

                    string ParmanentResidentialAddress = string.Empty;
                    string PresentResidentialAddress = string.Empty;

                    int Nationality = -1;

                    DateTime DateofAppintment = DateTime.Now;
                    DateTime DateofCessation = DateTime.Now;
                    DateTime DateofChangeinDesignation = DateTime.Now;

                    int DirectorDesignationId = -1;

                    bool Resident = false;

                    int NoofLLpPartner = -1;

                    int NoofCompany = -1;

                    int Designation = -1;
                    #endregion

                    #region Validations for Body Corporate

                    for (int i = 2; i <= xlrow2; i++)
                    {
                        #region 1 Type of Body Corporate 

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 1].Text.Trim())))
                        {
                            Type = Convert.ToString(excelAuditor.Cells[i, 1].Text).Trim();
                        }

                        else if (string.IsNullOrEmpty(Type))
                        {
                            errorMessage.Add("Please Check the Body Corporate Type can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 2 CIN/FCIN
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 2].Text.Trim())))
                        {
                            CIN_FCIN = Convert.ToString(excelAuditor.Cells[i, 2].Text).Trim();

                            if (string.IsNullOrEmpty(CIN_FCIN))
                            {
                                errorMessage.Add("Please Check the CIN/FCIN can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 3 Name of Body Corporate
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 3].Text.Trim())))
                        {
                            Name_Of_B_CORPT = Convert.ToString(excelAuditor.Cells[i, 3].Text).Trim();
                            if (string.IsNullOrEmpty(Name_Of_B_CORPT))
                            {
                                errorMessage.Add("Please Check the Body Corporate Name can not be empty at row - " + i + "");
                            }
                        }
                        #endregion


                        #region 4 Address

                        if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 4].Text.Trim())))
                        {
                            AddressLine1 = Convert.ToString(excelAuditor.Cells[i, 4].Text).Trim();


                            if (string.IsNullOrEmpty(AddressLine1))
                            {
                                errorMessage.Add("Please Check the Address can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 5 Country

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 5].Text.Trim())))
                        {
                            string CountryName = Convert.ToString(excelAuditor.Cells[i, 5].Text.Trim());
                            if (!string.IsNullOrEmpty(CountryName))
                            {
                                Country = getCountryId(CountryName);
                            }
                            if (string.IsNullOrEmpty(CountryName))
                            {
                                errorMessage.Add("Please Check the Address can not be empty at row - " + i + "");
                            }
                        }

                        #endregion

                        #region 6 PAN

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim())))
                        {
                            PAN = Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim());

                        }

                        #endregion

                        #region 7 Mobile Number


                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 7].Text.Trim())))
                        {
                            MobileNO = Convert.ToString(excelAuditor.Cells[i, 7].Text.Trim());

                        }


                        #endregion

                        #region 8 Email
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 8].Text.Trim())))
                        {
                            EmailId = Convert.ToString(excelAuditor.Cells[i, 8].Text.Trim());
                        }

                        #endregion

                        #region 9 Obligation_of_Contribution

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 9].Text.Trim())))
                        {
                            Obligation_Of_Contribution = Convert.ToInt32(excelAuditor.Cells[i, 9].Text.Trim().Replace(",", ""));

                        }

                        #endregion

                        #region 10 Contribution Recived and accounted For

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 10].Text.Trim())))
                        {
                            Contri_Rec_and_Accountant = Convert.ToInt32(excelAuditor.Cells[i, 10].Text.Trim().Replace(",", ""));

                        }

                        #endregion
                        #region 11 Name

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim())))
                        {
                            Name = Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim());
                            if (Name != null)
                            {
                                var splitName = Name.Split(' ');
                                if (splitName.Length > 0)
                                {
                                    if (splitName.Length == 2)
                                    {
                                        FirstName = splitName[0];
                                        LastName = splitName[1];
                                    }
                                    else if (splitName.Length == 3)
                                    {
                                        FirstName = splitName[0];
                                        MiddleName = splitName[1];
                                        LastName = splitName[2];
                                    }
                                }
                            }

                        }

                        #endregion


                        #region 12 DPIN

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim())))
                        {
                            DPIN = Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim());

                        }

                        #endregion



                        #region 13 Father Name

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim())))
                        {
                            string FatherName = "";

                            FatherName = Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim());
                            if (Name != null)
                            {
                                var splitFName = Name.Split(' ');
                                if (splitFName.Length > 0)
                                {
                                    if (splitFName.Length == 2)
                                    {
                                        Father_FirstName = splitFName[0];
                                        Father_LastName = splitFName[1];
                                    }
                                    else if (splitFName.Length == 3)
                                    {
                                        Father_FirstName = splitFName[0];
                                        Father_MiddleName = splitFName[1];
                                        Father_LastName = splitFName[2];
                                    }
                                }
                            }
                        }

                        #endregion

                        #region 14 Permanenet Residential address

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 14].Text.Trim())))
                        {
                            ParmanentResidentialAddress = Convert.ToString(excelAuditor.Cells[i, 14].Text.Trim());

                        }

                        #endregion

                        #region 15 Present Residential address

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 15].Text.Trim())))
                        {
                            PresentResidentialAddress = Convert.ToString(excelAuditor.Cells[i, 15].Text.Trim());

                        }

                        #endregion

                        #region 16 Nationality

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 16].Text.Trim())))
                        {
                            string Nation = "";
                            Nation = Convert.ToString(excelAuditor.Cells[i, 16].Text.Trim());
                            if (!string.IsNullOrEmpty(Nation))
                            {
                                Nationality = getCountryId(Nation);
                            }
                        }

                        #endregion


                        #region 17 Date of Appointment

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 17].Text.Trim())))
                        {

                            DateofAppintment = Convert.ToDateTime((excelAuditor.Cells[i, 17].Text.Trim()), culture);

                        }

                        #endregion

                        #region 18 Date of Cessation

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 18].Text.Trim())))
                        {
                            DateofCessation = Convert.ToDateTime((excelAuditor.Cells[i, 18].Text.Trim()), culture);
                        }

                        #endregion

                        #region 19 Date of Change in Designation

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 19].Text.Trim())))
                        {
                            DateofChangeinDesignation = Convert.ToDateTime((excelAuditor.Cells[i, 19].Text.Trim()), culture);
                        }

                        #endregion

                        #region 20 Resident in India

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 20].Text.Trim())))
                        {
                            string Residence = "";
                            Residence = Convert.ToString(excelAuditor.Cells[i, 20].Text.Trim());
                            if (!string.IsNullOrEmpty(Residence))
                            {
                                if (Residence.ToLower() == "yes")
                                {
                                    Resident = true;
                                }
                                else if (Residence.ToLower() == "no")
                                {
                                    Resident = false;
                                }
                            }
                        }

                        #endregion

                        #region 21 Designation

                        if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 21].Text.Trim())))
                        {
                            string Desig = "";
                            Desig = Convert.ToString(excelAuditor.Cells[i, 21].Text).Trim();
                            if (!string.IsNullOrEmpty(Desig))
                            {
                                if (Desig.ToLower() == "partner")
                                {
                                    Designation = 3;
                                }
                                else if (Desig.ToLower() == "designated partner")
                                {
                                    Designation = 4;
                                }
                                else if (Desig.ToLower() == "designated partner")
                                {
                                    Designation = 4;
                                }
                                else if (Desig.ToLower() == "designated partner & partner")
                                {
                                    Designation = 16;
                                }
                                else
                                {
                                    Designation = 7;
                                }
                            }

                            if (string.IsNullOrEmpty(Desig))
                            {
                                errorMessage.Add("Please Check the Designation can not be empty at row - " + i + "");
                            }
                        }


                        #endregion
                    }
                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        #region Data for Body Corporate as Partner in Form LLp 11

                        //int count = 0;
                        //int xlrow2 = excelAuditor.Dimension.End.Row;


                        //string Type = string.Empty;
                        //string CIN_FCIN = string.Empty;
                        //string Name_Of_B_CORPT = string.Empty;

                        //string AddressLine1 = string.Empty;
                        //int Country = -1;

                        //string PAN = string.Empty;
                        //string MobileNO = string.Empty;

                        //string EmailId = string.Empty;

                        //long Obligation_Of_Contribution = -1;
                        //long Contri_Rec_and_Accountant = -1;

                        //string Category = string.Empty;
                        //string DPIN = string.Empty;
                        //string Name = string.Empty;

                        //string FirstName = string.Empty;
                        //string MiddleName = string.Empty;
                        //string LastName = string.Empty;

                        //string Father_FirstName = string.Empty;
                        //string Father_MiddleName = string.Empty;
                        //string Father_LastName = string.Empty;

                        //string ParmanentResidentialAddress = string.Empty;
                        //string PresentResidentialAddress = string.Empty;

                        //int Nationality = -1;

                        //DateTime DateofAppintment = DateTime.Now;
                        //DateTime DateofCessation = DateTime.Now;
                        //DateTime DateofChangeinDesignation = DateTime.Now;

                        //bool Resident = false;

                        //int NoofLLpPartner = -1;

                        //int NoofCompany = -1;

                        #endregion

                        #region Validations for Body Corporate

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            #region 1 Type of Body Corporate 

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 1].Text.Trim())))
                            {
                                Type = Convert.ToString(excelAuditor.Cells[i, 1].Text).Trim();
                            }

                            else if (string.IsNullOrEmpty(Type))
                            {
                                errorMessage.Add("Please Check the Body Corporate Type can not be empty at row - " + i + "");
                            }
                            #endregion

                            #region 2 CIN/FCIN
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 2].Text.Trim())))
                            {
                                CIN_FCIN = Convert.ToString(excelAuditor.Cells[i, 2].Text).Trim();

                                if (string.IsNullOrEmpty(CIN_FCIN))
                                {
                                    errorMessage.Add("Please Check the CIN/FCIN can not be empty at row - " + i + "");
                                }
                            }
                            #endregion

                            #region 3 Name of Body Corporate
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 3].Text.Trim())))
                            {
                                Name_Of_B_CORPT = Convert.ToString(excelAuditor.Cells[i, 3].Text).Trim();
                                if (string.IsNullOrEmpty(Name_Of_B_CORPT))
                                {
                                    errorMessage.Add("Please Check the Body Corporate Name can not be empty at row - " + i + "");
                                }
                            }
                            #endregion


                            #region 4 Address

                            if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 4].Text.Trim())))
                            {
                                AddressLine1 = Convert.ToString(excelAuditor.Cells[i, 4].Text).Trim();


                                if (string.IsNullOrEmpty(AddressLine1))
                                {
                                    errorMessage.Add("Please Check the Address can not be empty at row - " + i + "");
                                }
                            }
                            #endregion

                            #region 5 Country

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 5].Text.Trim())))
                            {
                                string CountryName = Convert.ToString(excelAuditor.Cells[i, 5].Text.Trim());
                                if (!string.IsNullOrEmpty(CountryName))
                                {
                                    Country = getCountryId(CountryName);
                                }
                                if (string.IsNullOrEmpty(CountryName))
                                {
                                    errorMessage.Add("Please Check the Address can not be empty at row - " + i + "");
                                }
                            }

                            #endregion

                            #region 6 PAN

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim())))
                            {
                                PAN = Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim());

                            }

                            #endregion

                            #region 7 Mobile Number


                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 7].Text.Trim())))
                            {
                                MobileNO = Convert.ToString(excelAuditor.Cells[i, 7].Text.Trim());

                            }


                            #endregion

                            #region 8 Email
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 8].Text.Trim())))
                            {
                                EmailId = Convert.ToString(excelAuditor.Cells[i, 8].Text.Trim());
                            }

                            #endregion

                            #region 9 Obligation_of_Contribution

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 9].Text.Trim())))
                            {
                                Obligation_Of_Contribution = Convert.ToInt32(excelAuditor.Cells[i, 9].Text.Trim().Replace(",", ""));

                            }

                            #endregion

                            #region 10 Contribution Recived and accounted For

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 10].Text.Trim())))
                            {
                                Contri_Rec_and_Accountant = Convert.ToInt32(excelAuditor.Cells[i, 10].Text.Trim().Replace(",", ""));

                            }

                            #endregion
                            #region 11 Name

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim())))
                            {
                                Name = Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim());
                                if (Name != null)
                                {
                                    var splitName = Name.Split(' ');
                                    if (splitName.Length > 0)
                                    {
                                        if (splitName.Length == 2)
                                        {
                                            FirstName = splitName[0];
                                            LastName = splitName[1];
                                        }
                                        else if (splitName.Length == 3)
                                        {
                                            FirstName = splitName[0];
                                            MiddleName = splitName[1];
                                            LastName = splitName[2];
                                        }
                                    }
                                }

                            }

                            #endregion


                            #region 12 DPIN

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim())))
                            {
                                DPIN = Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim());

                            }

                            #endregion



                            #region 13 Father Name

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim())))
                            {
                                string FatherName = "";

                                FatherName = Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim());
                                if (Name != null)
                                {
                                    var splitFName = Name.Split(' ');
                                    if (splitFName.Length > 0)
                                    {
                                        if (splitFName.Length == 2)
                                        {
                                            Father_FirstName = splitFName[0];
                                            Father_LastName = splitFName[1];
                                        }
                                        else if (splitFName.Length == 3)
                                        {
                                            Father_FirstName = splitFName[0];
                                            Father_MiddleName = splitFName[1];
                                            Father_LastName = splitFName[2];
                                        }
                                    }
                                }
                            }

                            #endregion

                            #region 14 Permanenet Residential address

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 14].Text.Trim())))
                            {
                                ParmanentResidentialAddress = Convert.ToString(excelAuditor.Cells[i, 14].Text.Trim());

                            }

                            #endregion

                            #region 15 Present Residential address

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 15].Text.Trim())))
                            {
                                PresentResidentialAddress = Convert.ToString(excelAuditor.Cells[i, 15].Text.Trim());

                            }

                            #endregion

                            #region 16 Nationality

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 16].Text.Trim())))
                            {
                                string Nation = "";
                                Nation = Convert.ToString(excelAuditor.Cells[i, 16].Text.Trim());
                                if (!string.IsNullOrEmpty(Nation))
                                {
                                    Nationality = getCountryId(Nation);
                                }
                            }

                            #endregion


                            #region 17 Date of Appointment

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 17].Text.Trim())))
                            {

                                DateofAppintment = Convert.ToDateTime((excelAuditor.Cells[i, 17].Text.Trim()), culture);

                            }

                            #endregion

                            #region 18 Date of Cessation

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 18].Text.Trim())))
                            {
                                DateofCessation = Convert.ToDateTime((excelAuditor.Cells[i, 18].Text.Trim()), culture);
                            }

                            #endregion

                            #region 19 Date of Change in Designation

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 19].Text.Trim())))
                            {
                                DateofChangeinDesignation = Convert.ToDateTime((excelAuditor.Cells[i, 19].Text.Trim()), culture);
                            }

                            #endregion

                            #region 20 Resident in India

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 20].Text.Trim())))
                            {
                                string Residence = "";
                                Residence = Convert.ToString(excelAuditor.Cells[i, 20].Text.Trim());
                                if (!string.IsNullOrEmpty(Residence))
                                {
                                    if (Residence.ToLower() == "yes")
                                    {
                                        Resident = true;
                                    }
                                    else if (Residence.ToLower() == "no")
                                    {
                                        Resident = false;
                                    }
                                }
                            }

                            #endregion

                            #region 21 Designation

                            if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 21].Text.Trim())))
                            {
                                string Desig = "";
                                Desig = Convert.ToString(excelAuditor.Cells[i, 21].Text).Trim();
                                if (!string.IsNullOrEmpty(Desig))
                                {
                                    if (Desig.ToLower() == "partner")
                                    {
                                        Designation = 3;
                                    }
                                    else if (Desig.ToLower() == "designated partner")
                                    {
                                        Designation = 4;
                                    }
                                    else if (Desig.ToLower() == "designated partner")
                                    {
                                        Designation = 4;
                                    }
                                    else if (Desig.ToLower() == "designated partner & partner")
                                    {
                                        Designation = 16;
                                    }
                                    else
                                    {
                                        Designation = 7;
                                    }
                                }

                                if (string.IsNullOrEmpty(Desig))
                                {
                                    errorMessage.Add("Please Check the Designation can not be empty at row - " + i + "");
                                }
                            }


                            #endregion


                        }
                        #endregion

                        #endregion

                        if (!String.IsNullOrEmpty(Name_Of_B_CORPT) && !String.IsNullOrEmpty(CIN_FCIN))
                        {
                            //com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                            //{
                            //    //ID = _objentity.CustomerBranchId,
                            //    Name = Name_Of_B_CORPT,
                            //    //Type = Convert.ToByte(ddlType.SelectedValue),
                            //    // ComType = Convert.ToByte(comType),
                            //    //AddressLine1 = _objentity.Regi_Address_Line1,
                            //    //AddressLine2 = _objentity.Regi_Address_Line2,
                            //    //StateID = _objentity.Regi_StateId,
                            //    //CityID = _objentity.Regi_CityId,
                            //    //Others = "",
                            //    //PinCode = _objentity.Regi_PINCode,
                            //    ContactPerson = "",
                            //    Landline = "",
                            //    Mobile = "",
                            //    EmailID = EmailId,
                            //    CustomerID = customerID,
                            //    //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                            //    //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                            //};

                            //customerBranch = CreateCustomerBranch(customerBranch, 5, false, out Message);
                            //if (customerBranch.ID > 0)
                            //{
                            dynamic isbcrptExist = null;
                            isbcrptExist = (from row in entities.BM_EntityMaster
                                            where row.Is_Deleted == false
                                            && row.CIN_LLPIN == CIN_FCIN
                                            // && row.Customer_Id == customerID
                                            select row).FirstOrDefault();
                            if (isbcrptExist == null)
                            {
                                isbcrptExist = (from row in entities.BM_EntityMaster
                                                where row.Is_Deleted == false
                                                && row.CompanyName == Name_Of_B_CORPT
                                                // && row.Customer_Id == customerID
                                                select row).FirstOrDefault();
                            }
                            if (isbcrptExist == null)
                            {
                                BM_EntityMaster _objEntity = new BM_EntityMaster
                                {
                                    CIN_LLPIN = CIN_FCIN,
                                    CompanyName = Name_Of_B_CORPT,
                                    IncorporationDate = DateTime.Now,
                                    Regi_Address_Line1 = AddressLine1,
                                    Regi_Address_Line2 = "",
                                    Regi_CityId = 0,
                                    Regi_StateId = 0,
                                    Email_Id = EmailId,
                                    Corp_Address_Line1 = "",
                                    PAN = PAN,
                                    GST = "",
                                    ROC_Code = -1,
                                    Businessclassification = -1,
                                    PrincipalbusinessActivity = "",
                                    No_Of_DesignatedPartners = -1,
                                    No_Of_Partners = -1,
                                    Obligation_Of_Contribution = Obligation_Of_Contribution,
                                    OblicationRecived = Contri_Rec_and_Accountant,
                                    CustomerBranchId = -1,
                                    Customer_Id = -1,
                                    Entity_Type = 5,
                                    CreatedBy = userId,
                                    CreatedOn = DateTime.Now,
                                    Is_Deleted = false,
                                    Type_bodyCorporate = Type

                                };
                                entities.BM_EntityMaster.Add(_objEntity);
                                entities.SaveChanges();
                                BodyCorporateId = _objEntity.Id;
                            }
                            else
                            {
                                BodyCorporateId = isbcrptExist.Id;
                            }

                            bool checkpartner = false;
                            long DirectorId = -1;
                            dynamic prevRecordExists = null;
                            if (EntityId > 0)
                            {
                                if (!string.IsNullOrEmpty(DPIN))
                                {
                                    checkpartner = _objdirector.CheckDIN(customerID, 0, DPIN);
                                }
                                if (!string.IsNullOrEmpty(PAN))
                                {
                                    checkpartner = _objdirector.CheckPanNo(customerID, 0, PAN);
                                }
                                if (checkpartner == false)
                                {
                                    BM_DirectorMaster _objDirector = new BM_DirectorMaster
                                    {
                                        DIN = DPIN,
                                        Salutation = "",
                                        FirstName = FirstName,
                                        MiddleName = MiddleName,
                                        LastName = LastName,
                                        DOB = DateTime.Now,
                                        Gender = 0,
                                        MobileNo = MobileNO,
                                        EmailId_Personal = "",
                                        EmailId_Official = "",
                                        ResidentInIndia = Resident,
                                        Nationality = 1,
                                        PAN = PAN,
                                        Aadhaar = "",
                                        PassportNo = "",
                                        Permenant_Address_Line1 = ParmanentResidentialAddress,
                                        Permenant_Address_Line2 = "",
                                        Permenant_StateId = 0,
                                        Permenant_CityId = 0,
                                        Permenant_PINCode = "",
                                        IsSameAddress = false,
                                        Present_Address_Line1 = PresentResidentialAddress,
                                        Present_Address_Line2 = "",
                                        Present_StateId = 0,
                                        Present_CityId = 0,
                                        FSalutations = "Mr",
                                        Present_PINCode = "",
                                        Father = Father_FirstName,
                                        FatherMiddleName = Father_MiddleName,
                                        FatherLastName = Father_LastName,
                                        Customer_Id = customerID,
                                        IsActive = true,
                                        Is_Deleted = false,
                                        IsDirecor = false,
                                        CreatedBy = userId,
                                        UserID = -1,
                                        CreatedOn = DateTime.Now,
                                    };
                                    entities.BM_DirectorMaster.Add(_objDirector);
                                    entities.SaveChanges();
                                    DirectorId = _objDirector.Id;

                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(DPIN))
                                    {
                                        prevRecordExists = (from row in entities.BM_DirectorMaster
                                                            where row.DIN == DPIN

                                                            && row.Is_Deleted == false
                                                            && row.Customer_Id == customerID
                                                            select row).FirstOrDefault();
                                        DirectorId = prevRecordExists.Id;
                                    }
                                    else if (!string.IsNullOrEmpty(PAN))
                                    {
                                        prevRecordExists = (from row in entities.BM_DirectorMaster
                                                            where row.PAN == PAN

                                                            && row.Is_Deleted == false
                                                            && row.Customer_Id == customerID
                                                            select row).FirstOrDefault();
                                        DirectorId = prevRecordExists.Id;
                                    }
                                }
                                if (DirectorId > 0)
                                {
                                    bool checkDetailsExists = (from x in entities.BM_Directors_DetailsOfInterest
                                                                   //join nature in entities.BM_Directors_NatureOfInterest on x.NatureOfInterest equals nature.Id
                                                               where
                                                              x.EntityId == EntityID
                                                              && x.Director_Id == DirectorId
                                                              && x.IsDeleted == false
                                                              && x.IsActive == true
                                                               // && nature.IsMNGT == false
                                                               select x.Id).Any();
                                    if (!checkDetailsExists && Designation > 0)
                                    {
                                        checkDetailsExists = (from x in entities.BM_Directors_DetailsOfInterest
                                                              join nature in entities.BM_Directors_NatureOfInterest on x.NatureOfInterest equals nature.Id
                                                              where
                                                             x.EntityId == EntityID
                                                             && x.Director_Id == DirectorId
                                                             && x.IsDeleted == false
                                                             && x.IsActive == true
                                                             && nature.IsMNGT == false
                                                              select x.Id).Any();
                                    }



                                    if (checkDetailsExists == false)
                                    {
                                        int EID = EntityID;
                                        var checkentity = (from y in entities.BM_EntityMaster where y.Id == EID && y.Is_Deleted == false select y).FirstOrDefault();
                                        BM_Directors_DetailsOfInterest obj = new BM_Directors_DetailsOfInterest();

                                        obj.Director_Id = DirectorId;
                                        obj.EntityId = EntityId;
                                        obj.Entity_Type = 3;
                                        obj.NameOfOther = "";
                                        obj.CIN = checkentity.CIN_LLPIN;
                                        obj.PAN = checkentity.PAN;
                                        obj.NatureOfInterest = Designation;
                                        obj.IfDirector = 0;
                                        obj.IfInterestThroughRelatives = 0;
                                        obj.PercentagesOfShareHolding = 0;
                                        obj.ArosedOrChangedDate = DateofChangeinDesignation;
                                        obj.IsDeleted = false;
                                        obj.CreatedOn = DateTime.Now;
                                        obj.CreatedBy = userId;
                                        obj.IsActive = true;
                                        ///obj.DateOfResolution = item.BoardofResolution;
                                        obj.DateOfAppointment = DateofAppintment;
                                        obj.DirectorDesignationId = 13;
                                        obj.OriginalDirector_Id = 0;
                                        obj.NumberofSharesHeld = Contri_Rec_and_Accountant;
                                        obj.Contribution_Obligation = Obligation_Of_Contribution;
                                        obj.BodyCorporateID = BodyCorporateId;
                                        entities.BM_Directors_DetailsOfInterest.Add(obj);
                                        entities.SaveChanges();

                                    }
                                    Message = "Save successfully";
                                }
                                else
                                {
                                    Message = "Save successfully";
                                }
                            }
                        }
                        else
                        {
                            Message = "Body Corporate CIN or FCIN not found";
                        }

                    }
                    else
                    {
                        Message = "Body Corporate Name and FRCIN not found";
                    }
                }
                else
                {

                    if (errorMessage.Count > 0)
                    {
                        message = errorMessage;
                    }

                    else
                    {
                        Message = "data not found";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                errorMessage.Add("" + ex.Message + "");
                message = errorMessage;
            }
        }

        private void UploadEntityLLpData(ExcelPackage xlWorkbook, out List<string> message, out string Message, int CustomerId, int UserId, out int EntityId)
        {
            List<string> errorMessage = new List<string>();
            message = new List<string>();
            Message = "";
            EntityId = 0;
            CultureInfo culture = new CultureInfo("en-US");
            try
            {
                bool saveSuccess = false;
                ExcelWorksheet excelAuditor = xlWorkbook.Workbook.Worksheets["Entity Master - LLP"];

                if (excelAuditor != null)
                {
                    //#region Save Auditor Details

                    int count = 0;
                    int xlrow2 = excelAuditor.Dimension.End.Row;

                    #region LLP FORM 11  Data
                    string LLPIN = string.Empty;
                    string CompanyName = string.Empty;
                    string DateofIncorporation = string.Empty;

                    string AddressLine1 = string.Empty;
                    string AddressLine2 = string.Empty;
                    int countryId = -1;
                    int StateId = -1;
                    int cityId = -1;
                    string pin;

                    string OtherAddress = string.Empty;

                    string EmailId = string.Empty;
                    string PAN = string.Empty;

                    string GSTIN = string.Empty;
                    string ROC = string.Empty;
                    int BusinessClasification = -1;
                    string principalBusinessActivity = string.Empty;

                    int DesignetedPartner = 0;
                    int TotalnumofPartner = 0;
                    int TotalobligationofContribution = 0;
                    int TotalobligationofContributionrecived = 0;


                    #endregion

                    #region Validations for Entity Master LLp
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        #region 1 LLPIN
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 1].Text.Trim())))
                        {
                            LLPIN = Convert.ToString(excelAuditor.Cells[i, 1].Text).Trim();
                        }
                        else if (string.IsNullOrEmpty(LLPIN))
                        {
                            errorMessage.Add("Please Check the LLPIN can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 2 Company Name
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 2].Text.Trim())))
                        {
                            CompanyName = Convert.ToString(excelAuditor.Cells[i, 2].Text).Trim();
                            if (string.IsNullOrEmpty(CompanyName))
                            {
                                errorMessage.Add("Please Check the Company Name can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 3 Date of Incorporate
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 3].Text.Trim())))
                        {
                            DateofIncorporation = Convert.ToString(excelAuditor.Cells[i, 3].Text).Trim();
                            if (string.IsNullOrEmpty(DateofIncorporation))
                            {
                                errorMessage.Add("Please Check the Date of Incorporate can not be empty at row - " + i + "");
                            }
                        }
                        #endregion


                        #region 4 Address

                        if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 4].Text.Trim())))
                        {
                            AddressLine1 = Convert.ToString(excelAuditor.Cells[i, 4].Text).Trim();
                        }

                        if (string.IsNullOrEmpty(AddressLine1))
                        {
                            errorMessage.Add("Please Check the Address can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 5 Address2

                        if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 5].Text.Trim())))
                        {
                            AddressLine2 = Convert.ToString(excelAuditor.Cells[i, 5].Text).Trim();
                        }

                        //if (string.IsNullOrEmpty(AddressLine1))
                        //{
                        //    errorMessage.Add("Please Check the Address Line2 can not be empty at row - " + i + "");
                        //}
                        #endregion

                        #region 6 Country
                        if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim())))
                        {
                            string Country = "";
                            Country = Convert.ToString(excelAuditor.Cells[i, 6].Text).Trim();
                            if (!string.IsNullOrEmpty(Country))
                            {
                                countryId = getCountryId(Country);
                            }
                        }

                        if (countryId <= 0)
                        {
                            errorMessage.Add("Please Check the Country can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 7 state
                        if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 7].Text.Trim())))
                        {
                            string state = "";
                            state = Convert.ToString(excelAuditor.Cells[i, 7].Text).Trim();
                            if (!string.IsNullOrEmpty(state))
                            {
                                StateId = getStateId(state);
                            }
                        }

                        if (StateId <= 0)
                        {
                            errorMessage.Add("Please Check the State can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 8 City
                        if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 8].Text.Trim())))
                        {
                            string City = "";
                            City = Convert.ToString(excelAuditor.Cells[i, 8].Text).Trim();
                            if (!string.IsNullOrEmpty(City))
                            {
                                cityId = getCityId(City);
                            }
                        }

                        if (cityId <= 0)
                        {
                            errorMessage.Add("Please Check the city can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 9 PIN
                        if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim())))
                        {
                            string PIN = "";
                            PIN = Convert.ToString(excelAuditor.Cells[i, 6].Text).Trim();

                        }


                        #endregion

                        #region 10 Email

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 10].Text.Trim())))
                        {
                            EmailId = Convert.ToString(excelAuditor.Cells[i, 10].Text.Trim());
                        }
                        #endregion

                        #region 11 Other Address
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim())))
                        {
                            OtherAddress = Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim());
                        }

                        #endregion

                        #region 12 PAN


                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim())))
                        {
                            PAN = Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim());

                        }


                        #endregion

                        #region 13 GSTIN
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim())))
                        {
                            GSTIN = Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim());
                        }

                        #endregion

                        #region 14 ROC
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 14].Text.Trim())))
                        {
                            ROC = Convert.ToString(excelAuditor.Cells[i, 14].Text.Trim());
                            if (string.IsNullOrEmpty(ROC))
                            {
                                errorMessage.Add("Please Check the ROC can not be empty at row - " + i + "");
                            }
                        }

                        #endregion

                        #region 15 Business Classification

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 15].Text.Trim())))
                        {
                            string Businesscla = "";
                            Businesscla = Convert.ToString(excelAuditor.Cells[i, 15].Text.Trim());
                            if (!string.IsNullOrEmpty(Businesscla))
                            {
                                if (Businesscla.ToLower() == "business")
                                {
                                    BusinessClasification = 1;
                                }
                                else if (Businesscla.ToLower() == "profession")
                                {
                                    BusinessClasification = 2;
                                }
                                else if (Businesscla.ToLower() == "service")
                                {
                                    BusinessClasification = 3;
                                }
                                else if (Businesscla.ToLower() == "occupation")
                                {
                                    BusinessClasification = 4;
                                }
                                else
                                {
                                    BusinessClasification = 5;
                                }

                            }

                        }

                        #endregion

                        #region 16 Principal Business Activity

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 16].Text.Trim())))
                        {
                            principalBusinessActivity = Convert.ToString(excelAuditor.Cells[i, 16].Text.Trim());

                        }

                        #endregion

                        #region 17 Total Number of designeted partner

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 17].Text.Trim())))
                        {
                            DesignetedPartner = Convert.ToInt32(excelAuditor.Cells[i, 17].Text.Trim());

                        }

                        #endregion

                        #region 18 Total Number of partner

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 18].Text.Trim())))
                        {
                            TotalnumofPartner = Convert.ToInt32(excelAuditor.Cells[i, 18].Text.Trim());

                        }

                        #endregion

                        #region 19 Total Number of Obligation

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 19].Text.Trim())))
                        {
                            TotalobligationofContribution = Convert.ToInt32(excelAuditor.Cells[i, 20].Text.Trim().Replace(",", ""));

                        }

                        #endregion

                        #region 20 Total Number of Obligation Recived

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 20].Text.Trim())))
                        {
                            TotalobligationofContributionrecived = Convert.ToInt32(excelAuditor.Cells[i, 20].Text.Trim().Replace(",", ""));

                        }

                        #endregion






                    }
                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        #region LLP FORM 11  Data
                        LLPIN = string.Empty;
                        CompanyName = string.Empty;
                        DateofIncorporation = string.Empty;

                        AddressLine1 = string.Empty;
                        AddressLine2 = string.Empty;

                        EmailId = string.Empty;
                        PAN = string.Empty;

                        GSTIN = string.Empty;
                        ROC = string.Empty;
                        BusinessClasification = -1;
                        principalBusinessActivity = string.Empty;

                        DesignetedPartner = 0;
                        TotalnumofPartner = 0;
                        TotalobligationofContribution = 0;


                        #endregion

                        #region Validations for Entity Master LLp
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            #region 1 LLPIN
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 1].Text.Trim())))
                            {
                                LLPIN = Convert.ToString(excelAuditor.Cells[i, 1].Text).Trim();
                            }
                            else if (string.IsNullOrEmpty(LLPIN))
                            {
                                errorMessage.Add("Please Check the LLPIN can not be empty at row - " + i + "");
                            }
                            #endregion

                            #region 2 Company Name
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 2].Text.Trim())))
                            {
                                CompanyName = Convert.ToString(excelAuditor.Cells[i, 2].Text).Trim();
                                if (string.IsNullOrEmpty(CompanyName))
                                {
                                    errorMessage.Add("Please Check the Company Name can not be empty at row - " + i + "");
                                }
                            }
                            #endregion

                            #region 3 Date of Incorporate
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 3].Text.Trim())))
                            {
                                DateofIncorporation = Convert.ToString(excelAuditor.Cells[i, 3].Text).Trim();
                                if (string.IsNullOrEmpty(DateofIncorporation))
                                {
                                    errorMessage.Add("Please Check the Date of Incorporate can not be empty at row - " + i + "");
                                }
                            }
                            #endregion


                            #region 4 Address

                            if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 4].Text.Trim())))
                            {
                                AddressLine1 = Convert.ToString(excelAuditor.Cells[i, 4].Text).Trim();
                            }

                            if (string.IsNullOrEmpty(AddressLine1))
                            {
                                errorMessage.Add("Please Check the Address can not be empty at row - " + i + "");
                            }
                            #endregion

                            #region 5 Address2

                            if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 5].Text.Trim())))
                            {
                                AddressLine2 = Convert.ToString(excelAuditor.Cells[i, 5].Text).Trim();
                            }

                            //if (string.IsNullOrEmpty(AddressLine1))
                            //{
                            //    errorMessage.Add("Please Check the Address Line2 can not be empty at row - " + i + "");
                            //}
                            #endregion

                            #region 6 Country
                            if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim())))
                            {
                                string Country = "";
                                Country = Convert.ToString(excelAuditor.Cells[i, 6].Text).Trim();
                                if (!string.IsNullOrEmpty(Country))
                                {
                                    countryId = getCountryId(Country);
                                }
                            }

                            if (countryId <= 0)
                            {
                                errorMessage.Add("Please Check the Country can not be empty at row - " + i + "");
                            }
                            #endregion

                            #region 7 state
                            if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 7].Text.Trim())))
                            {
                                string state = "";
                                state = Convert.ToString(excelAuditor.Cells[i, 7].Text).Trim();
                                if (!string.IsNullOrEmpty(state))
                                {
                                    StateId = getStateId(state);
                                }
                            }

                            if (StateId <= 0)
                            {
                                errorMessage.Add("Please Check the State can not be empty at row - " + i + "");
                            }
                            #endregion

                            #region 8 City
                            if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 8].Text.Trim())))
                            {
                                string City = "";
                                City = Convert.ToString(excelAuditor.Cells[i, 8].Text).Trim();
                                if (!string.IsNullOrEmpty(City))
                                {
                                    cityId = getCityId(City);
                                }
                            }

                            if (cityId <= 0)
                            {
                                errorMessage.Add("Please Check the city can not be empty at row - " + i + "");
                            }
                            #endregion

                            #region 9 PIN
                            if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim())))
                            {
                                string PIN = "";
                                PIN = Convert.ToString(excelAuditor.Cells[i, 6].Text).Trim();

                            }


                            #endregion

                            #region 10 Email

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 10].Text.Trim())))
                            {
                                EmailId = Convert.ToString(excelAuditor.Cells[i, 10].Text.Trim());
                            }
                            #endregion

                            #region 11 Other Address
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim())))
                            {
                                OtherAddress = Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim());
                            }

                            #endregion

                            #region 12 PAN


                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim())))
                            {
                                PAN = Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim());

                            }


                            #endregion

                            #region 13 GSTIN
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim())))
                            {
                                GSTIN = Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim());
                            }

                            #endregion

                            #region 14 ROC
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 14].Text.Trim())))
                            {
                                ROC = Convert.ToString(excelAuditor.Cells[i, 14].Text.Trim());
                                if (string.IsNullOrEmpty(ROC))
                                {
                                    errorMessage.Add("Please Check the ROC can not be empty at row - " + i + "");
                                }
                            }

                            #endregion

                            #region 15 Business Classification

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 15].Text.Trim())))
                            {
                                string Businesscla = "";
                                Businesscla = Convert.ToString(excelAuditor.Cells[i, 15].Text.Trim());
                                if (!string.IsNullOrEmpty(Businesscla))
                                {
                                    if (Businesscla.ToLower() == "business")
                                    {
                                        BusinessClasification = 1;
                                    }
                                    else if (Businesscla.ToLower() == "profession")
                                    {
                                        BusinessClasification = 2;
                                    }
                                    else if (Businesscla.ToLower() == "service")
                                    {
                                        BusinessClasification = 3;
                                    }
                                    else if (Businesscla.ToLower() == "occupation")
                                    {
                                        BusinessClasification = 4;
                                    }
                                    else
                                    {
                                        BusinessClasification = 5;
                                    }

                                }

                            }

                            #endregion

                            #region 16 Principal Business Activity

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 16].Text.Trim())))
                            {
                                principalBusinessActivity = Convert.ToString(excelAuditor.Cells[i, 16].Text.Trim());

                            }

                            #endregion

                            #region 17 Total Number of designeted partner

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 17].Text.Trim())))
                            {
                                DesignetedPartner = Convert.ToInt32(excelAuditor.Cells[i, 17].Text.Trim());

                            }

                            #endregion

                            #region 18 Total Number of partner

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 18].Text.Trim())))
                            {
                                TotalnumofPartner = Convert.ToInt32(excelAuditor.Cells[i, 18].Text.Trim());

                            }

                            #endregion

                            #region 19 Total Number of Obligation

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 19].Text.Trim())))
                            {
                                TotalobligationofContribution = Convert.ToInt32(excelAuditor.Cells[i, 20].Text.Trim().Replace(",", ""));

                            }

                            #endregion

                            #region 20 Total Number of Obligation Recived

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 20].Text.Trim())))
                            {
                                TotalobligationofContributionrecived = Convert.ToInt32(excelAuditor.Cells[i, 20].Text.Trim().Replace(",", ""));

                            }

                            #endregion


                        }
                        #endregion

                        #endregion

                        if (!String.IsNullOrEmpty(LLPIN))
                        {
                            com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                            {
                                //ID = _objentity.CustomerBranchId,
                                Name = CompanyName,
                                //Type = Convert.ToByte(ddlType.SelectedValue),
                                // ComType = Convert.ToByte(comType),
                                //AddressLine1 = _objentity.Regi_Address_Line1,
                                //AddressLine2 = _objentity.Regi_Address_Line2,
                                //StateID = _objentity.Regi_StateId,
                                //CityID = _objentity.Regi_CityId,
                                //Others = "",
                                //PinCode = _objentity.Regi_PINCode,
                                ContactPerson = "",
                                Landline = "",
                                Mobile = "",
                                EmailID = EmailId,
                                CustomerID = CustomerId,
                                //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                                //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                            };

                            customerBranch = CreateCustomerBranch(customerBranch, 3, false, out Message);
                            if (customerBranch.ID > 0)
                            {
                                var isEntityExist = (from row in entities.BM_EntityMaster
                                                     where row.Is_Deleted == false
                                                     && row.CIN_LLPIN == LLPIN
                                                     && row.Customer_Id == CustomerId
                                                     select row).FirstOrDefault();
                                if (isEntityExist == null)
                                {
                                    BM_EntityMaster _objEntity = new BM_EntityMaster
                                    {
                                        CIN_LLPIN = LLPIN,
                                        CompanyName = CompanyName,
                                        IncorporationDate = Convert.ToDateTime(DateofIncorporation),
                                        Regi_Address_Line1 = AddressLine1,
                                        Regi_Address_Line2 = AddressLine2,
                                        Regi_CityId = cityId,
                                        Regi_StateId = StateId,
                                        Email_Id = EmailId,
                                        Corp_Address_Line1 = OtherAddress,
                                        PAN = PAN,
                                        GST = GSTIN,
                                        ROC_Code = GetRocCode(ROC),
                                        Businessclassification = BusinessClasification,
                                        PrincipalbusinessActivity = principalBusinessActivity,
                                        No_Of_DesignatedPartners = DesignetedPartner,
                                        No_Of_Partners = TotalnumofPartner,
                                        Obligation_Of_Contribution = TotalobligationofContribution,
                                        OblicationRecived=TotalobligationofContributionrecived,
                                        CustomerBranchId = customerBranch.ID,
                                        Customer_Id = CustomerId,
                                        Entity_Type = 3,
                                        CreatedBy = UserId,
                                        CreatedOn = DateTime.Now,
                                        Is_Deleted = false

                                    };
                                    entities.BM_EntityMaster.Add(_objEntity);
                                    entities.SaveChanges();
                                    EntityId = _objEntity.Id;
                                }
                                else
                                {
                                    isEntityExist.CIN_LLPIN = LLPIN;
                                    isEntityExist.CompanyName = CompanyName;
                                    isEntityExist.IncorporationDate = Convert.ToDateTime(DateofIncorporation);
                                    isEntityExist.Regi_Address_Line1 = AddressLine1;
                                    isEntityExist.Regi_Address_Line2 = "";
                                    isEntityExist.Regi_CityId = 0;
                                    isEntityExist.Regi_StateId = 0;
                                    isEntityExist.Email_Id = EmailId;
                                    isEntityExist.Corp_Address_Line1 = AddressLine2;
                                    isEntityExist.PAN = PAN;
                                    isEntityExist.GST = GSTIN;
                                    isEntityExist.ROC_Code = GetRocCode(ROC);
                                    isEntityExist.Businessclassification = BusinessClasification;
                                    isEntityExist.PrincipalbusinessActivity = principalBusinessActivity;
                                    isEntityExist.No_Of_DesignatedPartners = DesignetedPartner;
                                    isEntityExist.No_Of_Partners = TotalnumofPartner;
                                    isEntityExist.Obligation_Of_Contribution = TotalobligationofContribution;
                                    isEntityExist.OblicationRecived = TotalobligationofContributionrecived;
                                    isEntityExist.CustomerBranchId = customerBranch.ID;
                                    isEntityExist.Customer_Id = CustomerId;
                                    isEntityExist.Entity_Type = 3;
                                    isEntityExist.UpdatedBy = UserId;
                                    isEntityExist.UpdatedOn = DateTime.Now;
                                    isEntityExist.Is_Deleted = false;
                                    entities.SaveChanges();
                                    EntityId = isEntityExist.Id;
                                }
                                Message = SecretarialConst.Messages.saveSuccess;
                            }
                            else
                            {
                                Message = "Entity Branch details not found";
                            }
                        }
                        else
                        {
                            Message = "Entity LLPIN not found";
                        }
                    }

                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            message = errorMessage;
                        }
                    }

                }
                else
                {
                    Message = "Data not found";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                errorMessage.Add("" + ex.Message + "");
                message = errorMessage;
            }
        }

        private int? getbusinessClassification(string businessClasification)
        {
            long businessclassification = 0;
            try
            {
                businessclassification = (from x in entities.BM_BusinessActivity
                                          where x.Description == businessClasification
                                          select x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
            return (int?)businessclassification;
        }





        #region Save Body corporate from form 11
        public Body_CorporateVM Createbodycorporate(Body_CorporateVM _objbodycorporate, int userId, int customerId)
        {
            try
            {
                dynamic IsBodyCorporateexist = null;
                IsBodyCorporateexist = (from row in entities.BM_EntityMaster
                                        where row.CIN_LLPIN == _objbodycorporate.CIN_FCRN
                                        && row.Is_Deleted == false
                                        select row).FirstOrDefault();
                if (IsBodyCorporateexist != null)
                {
                    IsBodyCorporateexist = (from row in entities.BM_EntityMaster
                                            where row.CompanyName == _objbodycorporate.Name
                                            && row.Is_Deleted == false
                                            select row).FirstOrDefault();
                }

                if (IsBodyCorporateexist == null)
                {
                    BM_EntityMaster _objentitymaster = new BM_EntityMaster
                    {
                        CompanyName = _objbodycorporate.Name,
                        Entity_Type = 5,
                        CIN_LLPIN = _objbodycorporate.CIN_FCRN,
                        Regi_Address_Line1 = _objbodycorporate.Address,
                        Regi_Address_Line2 = "",
                        Regi_StateId = -1,
                        Regi_CityId = -1,
                        CountryId = _objbodycorporate.CountryId,
                        Obligation_Of_Contribution = _objbodycorporate.obligationofContribution,
                        OblicationRecived= _objbodycorporate.ContributionRecivedandAccounted,
                        Type_bodyCorporate= _objbodycorporate.Type,
                        Is_Deleted = false,
                        CreatedBy = userId,
                        CreatedOn = DateTime.Now,
                        CustomerBranchId = -1,
                        Customer_Id = -1,
                        IncorporationDate = DateTime.Now,
                        ROC_Code = -1,
                        CompanyCategory_Id = -1,
                        Email_Id = "",
                        PAN = "",

                    };
                    entities.BM_EntityMaster.Add(_objentitymaster);
                    entities.SaveChanges();
                    _objbodycorporate.Success = true;
                    _objbodycorporate.Message = SecretarialConst.Messages.saveSuccess;
                }
                else
                {
                    _objbodycorporate.Success = true;
                    _objbodycorporate.Message = SecretarialConst.Messages.alreadyExist;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objbodycorporate.Error = true;
                _objbodycorporate.Message = SecretarialConst.Messages.serverError;
            }
            return _objbodycorporate;
        }

        public override bool checkExcelData(ExcelPackage PrivatePublicexcel, string data)
        {
            bool flag = false;
            try
            {

                foreach (ExcelWorksheet sheet in PrivatePublicexcel.Workbook.Worksheets)
                {
                    if (data.Equals("Body corporate as partner"))
                    {
                        if (sheet.Name.Trim().Equals("Body corporate as partner"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            //break;
                        }
                    }
                    else if (data.Equals("Entity Master - LLP"))
                    {
                        if (sheet.Name.Trim().Equals("Entity Master - LLP"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            //break;
                        }
                    }
                    else if (data.Equals("Partner Master"))
                    {
                        if (sheet.Name.Trim().Equals("Partner Master"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            //break;
                        }
                    }
                    else
                    {
                        flag = false;
                    }

                }

            }
            catch (Exception ex)
            {
                flag = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return flag;
        }
        #endregion

        #region Copy Meetings
        public List<EntityMasterVM> GetEntitiesListForCopyMeeting(int entityId, int entityTypeID, int customerID)
        {
            try
            {
                var lstEntities = (from row in entities.BM_EntityMaster
                                   where row.Customer_Id == customerID && row.Entity_Type == entityTypeID && row.Id != entityId
                                   select new EntityMasterVM
                                   {
                                       Id = row.Id,
                                       EntityName = row.CompanyName
                                   }).ToList();
                return lstEntities;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion
    }
}