﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IAgendaMasterTemplate
    {
        IEnumerable<CategoryViewModel> GetCategory();
        IEnumerable<ImportTemplateViewModel> ImportTemplates(long? AgendaMasterId);
        IEnumerable<TemplateViewModel> GetAll(long AgendaMasterId);
        long? GetParentId(long? AgendaMasterId);
        TemplateViewModel Create(TemplateViewModel template);
        TemplateViewModel Update(TemplateViewModel template);
        TemplateViewModel Delete(TemplateViewModel template);
    }
}
