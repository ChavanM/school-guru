﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Masters
{
    public class AdditionalComplianceMaster : IAdditionalComplianceMaster
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        #region GetAdditional Compliances Added by Ruchi on 28th of July 2020
        public List<ComplianceList_VM> GetAdditionalCompliances()
        {
            List<ComplianceList_VM> _objaddicomp = new List<ComplianceList_VM>();
            try
            {
                _objaddicomp = (from row in entities.BM_SP_GetAdditionalCompliance()                               
                                select new ComplianceList_VM
                                {
                                    AdditionalCompId = row.Id,
                                    ComplianceId = row.AdditionalComplianceId,
                                    ShortDesc = row.ShortDescription,
                                    Type = row.AdditionalcompType
                                }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objaddicomp;
        }

        public AdditionalCompliances_VM AddAdditionalCompliances(AdditionalCompliances_VM _objadditionalcomp, int userId)
        {
            AdditionalCompliances_VM objadditioncom = new AdditionalCompliances_VM();
            try
            {
                bool sucess = false;
                int complId = 0;
                BM_AdditionalCompliance _objcomp = new BM_AdditionalCompliance();

                if (_objadditionalcomp.ComplianceNumber != null)
                {

                    string s = _objadditionalcomp.ComplianceNumber;
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = values[i].Trim();
                        if (values[i] != null)
                            complId = Convert.ToInt32(values[i]);

                        var checkcomplianceexist = (from row in entities.BM_AdditionalCompliance where row.ComplianceId == complId select row).FirstOrDefault();
                        if (checkcomplianceexist == null)
                        {
                            _objcomp.ComplianceId = complId;
                            _objcomp.ComplianceType = _objadditionalcomp.SEBI;
                            _objcomp.IsActive = true;
                            _objcomp.Createdby = userId;
                            _objcomp.CreatedOn = DateTime.Now;

                            entities.BM_AdditionalCompliance.Add(_objcomp);
                            entities.SaveChanges();

                            sucess = true;
                        }
                        else
                        {
                            checkcomplianceexist.ComplianceType = _objadditionalcomp.SEBI;
                            checkcomplianceexist.IsActive = true;
                            checkcomplianceexist.Updatedby = userId;
                            checkcomplianceexist.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();

                            sucess = true;
                        }
                    }

                    if (sucess)
                    {
                        objadditioncom.Success = true;
                        objadditioncom.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        objadditioncom.Success = false;
                        objadditioncom.Message = SecretarialConst.Messages.alreadyExist;
                    }
                }
                else
                {
                    objadditioncom.Success = false;
                    objadditioncom.Message = "Please enter at least one ComplianceID";
                }
            }
            catch (Exception ex)
            {
                objadditioncom.Error = true;
                objadditioncom.Message = SecretarialConst.Messages.serverError;
            }
            return objadditioncom;
        }

        public AdditionalCompliances_VM EditAdditionalCompliances(long Id)
        {
            AdditionalCompliances_VM objadditcom = new AdditionalCompliances_VM();
            try
            {
                objadditcom = (from row in entities.BM_AdditionalCompliance
                               where row.Id == Id
                               select new AdditionalCompliances_VM
                               {
                                   Id = row.Id,
                                   ComplianceNumber = row.ComplianceId.ToString(),
                                   SEBI = row.ComplianceType,
                               }).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return objadditcom;
        }

        public AdditionalCompliances_VM UpdateAdditionalCompliances(AdditionalCompliances_VM _objadditionalcomp, int userId)
        {
            try
            {
                int complId = 0;
                BM_AdditionalCompliance _objcomp = new BM_AdditionalCompliance();
                string s = _objadditionalcomp.ComplianceNumber;
                string[] values = s.Split(',');
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = values[i].Trim();
                    if (values[i] != null)
                        complId = Convert.ToInt32(values[i]);


                    var checkcomplianceexist = (from row in entities.BM_AdditionalCompliance where row.Id == _objadditionalcomp.Id select row).FirstOrDefault();

                    if (checkcomplianceexist != null)
                    {
                        checkcomplianceexist.ComplianceId = complId;
                        checkcomplianceexist.ComplianceType = _objadditionalcomp.SEBI;
                        checkcomplianceexist.IsActive = true;
                        checkcomplianceexist.Createdby = userId;
                        checkcomplianceexist.CreatedOn = DateTime.Now;
                        entities.BM_AdditionalCompliance.Add(_objcomp);
                        entities.SaveChanges();
                        _objadditionalcomp.Success = true;
                        _objadditionalcomp.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        _objadditionalcomp.Error = true;
                        _objadditionalcomp.Message = SecretarialConst.Messages.noRecordFound;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objadditionalcomp.Error = true;
                _objadditionalcomp.Message = SecretarialConst.Messages.serverError;
            }
            return _objadditionalcomp;
        }

        public bool DeleteAdditionalComp(long id, int userId)
        {
            bool success = false;
            try
            {
                var checkcomp = (from row in entities.BM_AdditionalCompliance where row.Id == id select row).FirstOrDefault();
                if (checkcomp != null)
                {
                    checkcomp.IsActive = false;
                    checkcomp.UpdatedOn = DateTime.Now;
                    checkcomp.Updatedby = userId;
                    entities.SaveChanges();
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }
        #endregion

    }
}