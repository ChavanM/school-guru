﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IDirectorRelative
    {
        List<DirectorRelativesVM> GetAllRelatives(long DirectorId);
        DirectorRelativesVM AddRelatives(DirectorRelativesVM obj);
        DirectorRelativesVM UpdateRelatives(DirectorRelativesVM obj);
        DirectorRelativesVM Delete(DirectorRelativesVM obj);


        List<RelationVM> GetRelation();
        List<RelationVM> GetRelation(long Director_Id);
    }
}
