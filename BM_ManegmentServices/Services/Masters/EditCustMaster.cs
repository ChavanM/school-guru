﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
    public class EditCustMaster : IEditCustMaster
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        
        public List<EditCust_VM> GetCustomer()
        {
            List<EditCust_VM> _objaddicust = new List<EditCust_VM>();
            try
            {
                _objaddicust = (from row in entities.BM_SP_GetCustomerConfiguration()
                                select new EditCust_VM
                                {
                                    ID = row.ID,
                                    CustomerID = row.CustomerID,
                                    Name = row.Name,
                                    SentMailtoDirector = row.SentMailtoDirector,
                                    DefaultVirtualMeeting = row.DefaultVirtualMeeting,
                                    IsVirtualMeeting = row.IsVirtualMeeting,
                                    VideoConferencingApplicable = row.VideoConferencingApplicable,
                                    VideoConferencingProviderName = row.VideoConferencingProviderName,
                                }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objaddicust;
        }

        public static EditCust_VM GetCustomerEdit(int customerID)
        {
            EditCust_VM userToEdit = new EditCust_VM();
            try
            {
                
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                     userToEdit = (from row in entities.BM_CustomerConfiguration
                                                        where row.CustomerID == customerID
                                                        select new EditCust_VM
                                                        {
                                                            ID=row.ID,
                                                            CustomerID=row.CustomerID,
                                                        }
                                                        ).FirstOrDefault();

                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return userToEdit;
        }

        public static bool CreateUpdate_BMCustomerConfiguration(BM_CustomerConfiguration _objRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var prevRecord = (from row in entities.BM_CustomerConfiguration
                                      where row.CustomerID == _objRecord.CustomerID                                     
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {                         
                        prevRecord.SentMailtoDirector = _objRecord.SentMailtoDirector;
                        prevRecord.DefaultVirtualMeeting = _objRecord.DefaultVirtualMeeting;
                        prevRecord.IsVirtualMeeting = _objRecord.IsVirtualMeeting;
                        prevRecord.VideoConferencingApplicable = _objRecord.VideoConferencingApplicable;
                        prevRecord.ProviderName = _objRecord.ProviderName;
                        prevRecord.ClientId = _objRecord.ClientId;
                        prevRecord.ClientSecret = _objRecord.ClientSecret;
                        prevRecord.ZoomtokenExpiryDate = _objRecord.ZoomtokenExpiryDate;
                        prevRecord.Zoomtoken = _objRecord.Zoomtoken;
                        prevRecord.Refresh_Token = _objRecord.Refresh_Token;
                        prevRecord.Refresh_TokenExpiryDate = _objRecord.Refresh_TokenExpiryDate;

                        prevRecord.UpdatedBy = _objRecord.UpdatedBy;
                        prevRecord.UpdatedOn = _objRecord.UpdatedOn;

                        saveSuccess = true;
                    }
                    else
                    {
                        entities.BM_CustomerConfiguration.Add(_objRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
               LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static BM_CustomerConfiguration GetCustomerConfiguration(int custID)
        {
            try
            {
                
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var prevRecord = (from row in entities.BM_CustomerConfiguration
                                      where row.CustomerID == custID
                                      select row).FirstOrDefault();
                    
                    return prevRecord;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public bool DeleteCust(long id, int userId)
        {
            bool success = false;
            try
            {
                var checkcomp = (from row in entities.BM_CustomerConfiguration where row.ID == id select row).FirstOrDefault();
                if (checkcomp != null)
                {
                    checkcomp.IsActive = false;
                    checkcomp.UpdatedOn = DateTime.Now;
                    checkcomp.UpdatedBy = userId;
                    entities.SaveChanges();
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }
    }
}