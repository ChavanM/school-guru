﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.UIForms;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IDirectorMaster
    {
        bool CheckDIN(int customerId,long id , string DIN);
        bool CheckofficealEmailID(int customerId, long id, string officealEmailID);
        bool CheckEmailID(int customerId, long id, string EmailId);
        bool CheckUserExistsInOtherCustomer(int customerId, string EmailId);
        bool CheckMobileNo(int customerId, long id, string MobileNumber);
        bool CheckPanNo(int customerId, long id, string Pannumber);
        bool Create(BM_DirectorMaster obj, out bool hasError);
        Director_MasterVM GetDirectorVM(int customerId, long id);
        bool SetUserId(long DirectorID, long UserId, out bool hasError);
        bool Update_DirectorDoc(BM_DirectorMaster obj);
        List<Director_MasterVM> GetDirectors(int customerIds);

        List<DirectorMasterListVM> DirectorMasterList(int customerId);

        List<DirectorMasterListVM> BOD(int EntityId, int customerId, bool showAllOption);
        List<DirectorMasterListVM> GetManagementEntityWise(int entityID, int customerID, bool showAllOption);
        BM_DirectorMaster GetDirector(int customerId,long id);
        long Update_DirectorDetails(BM_DirectorTypeChangesMapping obj, out bool hasError);
        bool Delete(long id, out bool hasError);

        #region Get Relatives
        List<Relatives> GetRelatives(int id);

        List<Relatives> GetRelative_Son(long id);
        List<Relatives> GetRelative_Daughter(long id);
        Director_MasterVM GetManegmentDetails(int customerId, long id);
        List<Relatives> GetRelative_Brother(long id);
        List<Relatives> GetRelative_Sister(long id);
        string AddMember(CommitteeMasterVM _obj, int userID, int customerId);
        IEnumerable<Relatives> GetAllRelativeId(long Director_Id);
        IEnumerable<Relatives> GetRelativeForDropdown(long id);
        #endregion

        #region Create or Update
        bool UpdateRelative(long Director_Id, string huf, string Spouse, string Father, string Mother,out bool hasError);
        bool CreateRelative_Son(List<Relatives> objlstRelatives, long Director_Id, out bool hasError);

        bool CreateRelative_Daughter(List<Relatives> objlstRelatives, long Director_Id, out bool hasError);

        bool CreateRelative_Brother(List<Relatives> objlstRelatives, long Director_Id, out bool hasError);

        bool CreateRelative_Sister(List<Relatives> objlstRelatives, long Director_Id, out bool hasError);

        bool CreateRelative(List<Relatives> objlstRelatives, long Director_Id, string type, out bool hasError);

        bool DeleteRelative(long id, long Director_Id);
        #endregion

        string CheckChairpersonExists(DetailsOfCommiteePosition item, long Director_ID, out bool hasError);

        bool UpdateIsPositionHeld(long Director_Id, bool value);

        bool CreateDetailsOfCommitteePosition(DetailsOfCommiteePosition item, long Director_ID, out bool hasError);

        bool DeleteDetailsOfCommitteePosition(long id, int UserId, out bool hasError);
        List<CommitteeMasterVM> GetCommitteeMembers(int Entity_Id, int CommitteeId, bool showAllCommittees);
        List<CommitteeMasterVM> GetMemberListForAddNewById(int Entity_Id, int CommitteeId, int customerId, bool? isNew);
        List<DirectorList_ForEntity> GetDirectorListOfEntity(int Entity_Id, int CommitteeId);
        List<DirectorList_ForEntity> GetDirectorForEntity(int customerId,int Entity_Id,int? CommitteeId, int? DetailsOfCommitteeId);
        List<DirectorList_ForEntity> GetKMPForEntity(int customerId, int Entity_Id);
        List<DirectorList_ForEntity> GetEntityUserCommitteeAssignment(int entityId, int meetingTypeId);
        List<DirectorList_ForEntity> GetCommitteeMember(int customerId, int Entity_Id, int CommitteeId);

        List<CommitteeList_ForEntity> GetCommitteeForEntity(int customerId, int Entity_Id);
        List<CommitteeMasterVM> GetCommitteeMasterNew(int customerId, int Entity_Id);
        List<CommitteeMasterVM> GetCommitteeMasterNew(int customerId, int Entity_Id, int CommitteeId);
        string GetDirectorResignation(long directorID, long entityID);
        List<CommitteeMasterVM> GetCommitteeMasterdtls(int customerId, int director_Id);
       
        CommitteeMasterVM UpdateCommitteeDtls(int customerId, CommitteeMasterVM _objcommity);
        CommitteeMasterVM CreateCommitteeDtls(int customerId, CommitteeMasterVM _objcommity);
        IEnumerable<BM_DirectorPosition> GetDirectorPosition();
        IEnumerable<NatureOfInterestVM> GetNatureOfInterestByEntityType(int entityType);
        List<DetailsOfInterest> DetailsOfIntrestList(long directorId, int customerId);

        Pravate_PublicVM CreateOtherEntity(Pravate_PublicVM _objentity);

        List<EntityMasterVM> GetAllEntityMaster(int customerID, long Director_Id);
        DetailsOfInterest CreateDetailsOfInterest(DetailsOfInterest obj, int customerId, int userID);

        DetailsOfInterest  UpdateDetailsOfInterest(DetailsOfInterest obj, int customerId, int userID);
        string DeleteDetailsOfInterest(int Id, int userId);

        List<Directors> DirectorDetails(int EntityId, int customerId);

        bool CreateDirector_Security(BM_SecurityDetail obj_Kmpsecurity);
        VMDirectorFileUpload ImportDataFormDIR3(VMDirectorFileUpload objfileupload);

        List<Director_MasterVM> GetDirectorsHistory(long DirectorID);
        Director_MasterVM GetDirectorHistory(long id);

        ResignationofDirector DirectorResignation(ResignationofDirector _objResignation);
        OnEditingDirector SaveDirectorTypeChanges(OnEditingDirector _objonEditingDirector);
        OnEditingDirector getDirectorTypeChanges(long directorId);
        BM_DirectorTypeOfChanges CheckIsChangeDone(long iD);
        int CountProfileUpdate(int userID);
        List<DirectorList_ForEntity> BODforEntity(int userID, int entityId, int CustomerId, int v2);

        #region Appointment/Cessation/Change in designation
        List<DirectorMasterListVM> DirectorListForAppointmentCessation(int entityId, int customerId, int? designationId, long? detailsId, string listFor);
        #endregion

        #region UI Form
        UIForm_DirectorMasterVM AppointmentOfDirector(UIForm_DirectorMasterVM obj, int userId, int customerId);
        UIForm_DirectorMasterCessationVM CessationOfDirector(UIForm_DirectorMasterCessationVM obj, int userId, int customerId);
        #endregion

        #region Resignation Of Director
        Director_ResignationVM ResignationOfDirector(long id);
        Director_ResignationVM SaveResignationOfDirector(Director_ResignationVM obj, int userId);
        Message RevertResignationOfDirector(long id, int userId);
        #endregion

        bool Update_Director(BM_DirectorMaster obj_BM_DirectorMaster, out bool hasError);
        OnEditingDirector checkIscheckedornot(long directorID);

        bool checkchangebydirectorornot(int userID);
        List<Director_MasterVM> GetDirectorListinCompany(int customerId, int userId);

        #region Get Director User Id
        long? GetUserId(long directorId);
        string GetEmail(long directorId);
        #endregion

        #region Get Director User Id
        long GetDirectorIdByUserId(int userId, int customerId);
        #endregion

        #region Check user already exists in case of old user
        long? GetUserIdForCreateNewDirector(string emailId, int customerId);
        bool Update_DirectorDocInfo(BM_DirectorTypeChangesMapping obj_BM_DirectorMaster);
        bool GetDetailsofIntrest(long id);
        Director_MasterVM UpdateDirectorsOtherDetails(Director_MasterVM obj,bool IsotherthenDetails);
        bool Update_DirectorDocforothers(BM_DirectorTypeChangesMapping obj_BM_DirectorMaster);
        DirectorprofiledetailsVM SaveDirectorProfileDetails(DirectorprofiledetailsVM _objdetails, int customerId);//Interface method for save director profile in brief 
        void UploadPartner(ExcelPackage xlWorkbook, out List<string> message1, out string message2, int customerID, ref int entityId,int UserId,out long DirectorID);
        DetailsOfInterest GetDOIbyID(int directorId, int intrestId);
        Body_CorporateVM GetNomneeDetails(int directorId, int intrestId);
        #endregion
    }
}
