﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Hosting;

namespace BM_ManegmentServices.Services.Masters
{
    public class FileUpload
    {
        public static Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public static bool SaveTaskFile(IEnumerable<HttpPostedFileBase> files, int customerId, int userId, long TaskId)
        {

            //try
            //{
            string fileName = string.Empty;
            string directoryPath = string.Empty;
            if (files != null)
            {
                foreach (var file in files)
                {
                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                    if (file.ContentLength > 0)
                    {
                        IEnumerable<HttpPostedFileBase> uploadfile1 = files;

                        directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/Document/" + customerId + "/Task/" + TaskId);

                        if (!Directory.Exists(directoryPath))
                            Directory.CreateDirectory(directoryPath);

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(file.FileName));
                        Stream fs = file.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                        fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                        SaveDocFiles(fileList);

                        BM_FileData _objTaskDocument = new BM_FileData();
                        _objTaskDocument.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        _objTaskDocument.FileKey = fileKey1.ToString();
                        _objTaskDocument.FileName = file.FileName;
                        _objTaskDocument.TaskId = TaskId;
                        _objTaskDocument.UploadedBy = userId;
                        _objTaskDocument.UploadedOn = DateTime.Now;
                        if (file.ContentLength > 0)
                            _objTaskDocument.FileSize = (file.ContentLength).ToString();
                        entities.BM_FileData.Add(_objTaskDocument);
                        entities.SaveChanges();

                    }
                }
            }
            return true;
        }
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }

                }
            }
        }
        public static byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }
        public static bool downloadTaskData(List<BM_FileData> checkData)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile TaskDocument = new ZipFile())
                {
                    foreach (var files in checkData)
                    {
                        string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.FileName));

                        if (files.FilePath != null && File.Exists(filePath))
                        {
                            documentSuccess = true;
                            int idx = files.FileName.LastIndexOf('.');
                            string str = files.FileName.Substring(0, idx) + "_" + "." + files.FileName.Substring(idx + 1);
                            string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                            if (!TaskDocument.ContainsEntry("Document" + "/" + str))
                                TaskDocument.AddEntry("Document" + "/" + str, CryptographyHandler.AESDecrypt(ReadDocFiles(filePath)));
                        }
                    }
                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        TaskDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + "TaskDocuments.zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool downloadAgendaDocument(List<BM_FileData> checkData)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile TaskDocument = new ZipFile())
                {
                    foreach (var files in checkData)
                    {
                        string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.FileName));

                        if (files.FilePath != null && File.Exists(filePath))
                        {
                            documentSuccess = true;
                            int idx = files.FileName.LastIndexOf('.');
                            string str = files.FileName.Substring(0, idx) + "_" + "." + files.FileName.Substring(idx + 1);
                            string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                            if (!TaskDocument.ContainsEntry("Document" + "/" + str))
                                TaskDocument.AddEntry("Document" + "/" + str, CryptographyHandler.AESDecrypt(ReadDocFiles(filePath)));
                        }
                    }
                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        TaskDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + "AgendaDocuments.zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public FileDataVM GetAgendaItemsFile(long fileID, int userId)
        {
            var obj = new FileDataVM();
            try
            {
                var result = (from row in entities.BM_FileData
                              where row.Id == fileID
                              select new FileDataVM
                              {
                                  FileName = row.FileName,
                                  FileKey = row.FileKey,
                                  FilePath = row.FilePath,
                                  Version = row.Version
                              }).FirstOrDefault();

                if (result != null)
                {
                    string path = System.Web.Hosting.HostingEnvironment.MapPath(result.FilePath);
                    string fileExtension = Path.GetExtension(result.FileName);
                    string fileName = result.FileKey + fileExtension;

                    string filePath = Path.Combine(path, fileName);

                    if (File.Exists(filePath))
                    {
                        obj.FileKey = result.FileKey;
                        obj.FileName = result.FileName;
                        //obj.FileData = ReadDocFiles(filePath);
                        obj.FileData = CryptographyHandler.AESDecrypt(ReadDocFiles(filePath));
                        obj.Success = true;
                    }
                    else
                    {
                        obj.Error = true;
                    }
                }
                else
                {
                    obj.Error = true;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public static bool SaveAgendaFile(VM_AgendaDocument _vmagendaDocument, int uploadedBy, int customerId)
        {
            try
            {
                string fileName = string.Empty;
                string directoryPath = string.Empty;
                var fileFlag = false;
                if (_vmagendaDocument.files != null)
                {
                    using (Compliance_SecretarialEntities context = new Compliance_SecretarialEntities())
                    {
                        foreach (var file in _vmagendaDocument.files)
                        {
                            List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                            if (file.ContentLength > 0)
                            {
                                IEnumerable<HttpPostedFileBase> uploadfile1 = _vmagendaDocument.files;

                                directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + _vmagendaDocument.AgendaDocumentMeetingId);

                                if (!Directory.Exists(directoryPath))
                                    Directory.CreateDirectory(directoryPath);

                                Guid fileKey1 = Guid.NewGuid();
                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(file.FileName));
                                Stream fs = file.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                SaveDocFiles(fileList);

                                BM_FileData _objTaskDocument = new BM_FileData();
                                _objTaskDocument.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                _objTaskDocument.FileKey = fileKey1.ToString();
                                _objTaskDocument.FileName = file.FileName;
                                _objTaskDocument.AgendaMappingId = _vmagendaDocument.MeetingAggendaMappingId;
                                _objTaskDocument.UploadedBy = uploadedBy;
                                _objTaskDocument.UploadedOn = DateTime.Now;

                                if (_vmagendaDocument.AgendaDocumentMeetingId != null)
                                    _objTaskDocument.MeetingId = _vmagendaDocument.AgendaDocumentMeetingId;

                                if (file.ContentLength > 0)
                                    _objTaskDocument.FileSize = (file.ContentLength).ToString();

                                context.BM_FileData.Add(_objTaskDocument);
                                context.SaveChanges();

                                fileFlag = true;
                            }
                        }

                        if (fileFlag)
                        {
                            context.BM_SP_MeetingAgendaSetGenerationFlag(_vmagendaDocument.AgendaDocumentMeetingId);
                            context.SaveChanges();
                            //var AgendaFlag = (from row in entities.BM_SP_MeetingAgendaSetGenerationFlag(_vmagendaDocument.AgendaDocumentMeetingId)
                            //              select row);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool SaveHistoricalFile(IEnumerable<HttpPostedFileBase> files, long HistoricalID, int UserId, int CustomerID, string doctype)
        {
            bool success = false;
            try
            {
                string fileName = string.Empty;
                string directoryPath = string.Empty;
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                        if (file.ContentLength > 0)
                        {
                            IEnumerable<HttpPostedFileBase> uploadfile1 = files;

                            directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/Document/" + CustomerID + "/HistoricalDataFiles/");

                            if (!Directory.Exists(directoryPath))
                                Directory.CreateDirectory(directoryPath);

                            Guid fileKey1 = Guid.NewGuid();
                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(file.FileName));
                            Stream fs = file.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                            SaveDocFiles(fileList);

                            BM_FileData _objTaskDocument = new BM_FileData();
                            _objTaskDocument.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            _objTaskDocument.FileKey = fileKey1.ToString();
                            _objTaskDocument.FileName = file.FileName;
                            _objTaskDocument.HistoricalDtataID = (int)HistoricalID;
                            _objTaskDocument.UploadedBy = UserId;
                            _objTaskDocument.UploadedOn = DateTime.Now;
                            _objTaskDocument.DocType = doctype;
                            if (file.ContentLength > 0)
                                _objTaskDocument.FileSize = (file.ContentLength).ToString();
                            entities.BM_FileData.Add(_objTaskDocument);
                            entities.SaveChanges();
                            success = true;
                        }
                    }
                }
                else
                {
                    success = false;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }

        public static bool SaveEntityDocument(IEnumerable<HttpPostedFileBase> files, long entityDocID, int userId, int customerId, string documentType, int entityID)
        {
            bool success = false;
            try
            {
                string fileName = string.Empty;
                string directoryPath = string.Empty;
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                        if (file.ContentLength > 0)
                        {
                            IEnumerable<HttpPostedFileBase> uploadfile1 = files;

                            directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/Document/" + customerId + "/" + entityID + "/Documents/");

                            if (!Directory.Exists(directoryPath))
                                Directory.CreateDirectory(directoryPath);

                            Guid fileKey1 = Guid.NewGuid();
                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(file.FileName));
                            Stream fs = file.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                            SaveDocFiles(fileList);

                            BM_FileData _objTaskDocument = new BM_FileData();
                            _objTaskDocument.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            _objTaskDocument.FileKey = fileKey1.ToString();
                            _objTaskDocument.FileName = file.FileName;
                            _objTaskDocument.EntityDocId = (int)entityDocID;
                            _objTaskDocument.UploadedBy = userId;
                            _objTaskDocument.UploadedOn = DateTime.Now;

                                _objTaskDocument.DocType = documentType;
                          
                            if (file.ContentLength > 0)
                                _objTaskDocument.FileSize = (file.ContentLength).ToString();

                            entities.BM_FileData.Add(_objTaskDocument);
                            entities.SaveChanges();
                            success = true;
                        }
                    }
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }

        public static bool SaveEntityLogo(IEnumerable<HttpPostedFileBase> files, int EntityId, int customerId, int userId)
        {
            bool success = false;
            try
            {
                string fileName = string.Empty;
                string directoryPath = string.Empty;
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                        if (file.ContentLength > 0)
                        {
                            IEnumerable<HttpPostedFileBase> uploadfile1 = files;

                            directoryPath = "~/Areas/Documents/" + customerId + "/" + EntityId + "/EntityLogo/";
                            fileName= Path.GetFileName(file.FileName);
                            bool exists = System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(directoryPath));
                            if (!exists)
                            {
                                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(directoryPath));
                            }
                            string _path = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(directoryPath), fileName);
                            DirectoryInfo di = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.MapPath(directoryPath));
                            FileInfo[] TXTFiles = di.GetFiles(fileName);
                            if (TXTFiles.Length == 0)
                            {
                                file.SaveAs(_path);
                            }
                            // file.SaveAs(HostingEnvironment.MapPath(_path));
                            var checkexist = (from x in entities.BM_FileData where x.EntityId == EntityId && x.IsDeleted == false select x).FirstOrDefault();
                            if (checkexist == null)
                            {
                                BM_FileData _objTaskDocument = new BM_FileData();
                                _objTaskDocument.FilePath = directoryPath;
                              _objTaskDocument.FileKey = "";
                                _objTaskDocument.FileName = file.FileName;
                                _objTaskDocument.EntityId = (int)EntityId;
                                _objTaskDocument.UploadedBy = userId;
                                _objTaskDocument.UploadedOn = DateTime.Now;
                                _objTaskDocument.IsDeleted = false;
                                if (file.ContentLength > 0)
                                    _objTaskDocument.FileSize = (file.ContentLength).ToString();
                                entities.BM_FileData.Add(_objTaskDocument);
                                entities.SaveChanges();
                            }
                            else
                            {
                                checkexist.FilePath = directoryPath;
                                checkexist.FileKey = "";
                                checkexist.FileName = file.FileName;
                                checkexist.EntityId = (int)EntityId;
                                checkexist.UploadedBy = userId;
                                checkexist.UploadedOn = DateTime.Now;
                                checkexist.IsDeleted = false;
                                if (file.ContentLength > 0)
                                    checkexist.FileSize = (file.ContentLength).ToString();
                                entities.SaveChanges();
                            }
                            success = true;
                        }
                    }
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }
    }
}