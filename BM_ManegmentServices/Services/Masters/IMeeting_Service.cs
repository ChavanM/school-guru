﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Dashboard;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IMeeting_Service
    {

        #region Meeting
        IEnumerable<MeetingVM> GetMeetings(int CustomerId, int UserId, string Role);
        Dashboard_MeetingVM GetMeetingsForDashboard(int UserID, int CustomerId, string Role);
        List<CompletedMeetingsVM> GetCompletedMeetings(int entityId, int customerId);
        IEnumerable<MeetingVM> GetMeetingsForParticipant(int UserID);
        MeetingVM GetMeeting(long CustomerId, long MeetingId);
        MeetingVM GetMeetingDetails(int CustomerId, long MeetingId);
        MeetingVM GetMeetingForDocumentName(long CustomerId, long MeetingId);
        Meeting_NewVM Create(Meeting_NewVM obj);
        MeetingVM Update(MeetingVM obj);
        #endregion

        #region Upcoming Meetings
        List<MeetingListVM> GetUpcomingMeetings(long CustomerId, int UserId, string Role);
        #endregion

        #region Meeting For Dashboard
        List<DraftMeetingListVM> GetDraftMeetingsForDirectorApproval(int customerId, int userID);
        List<DraftMeetingListVM> GetDraftMeetingsForDirectorApproval(int userID);
        List<DraftMeetingListVM> GetConcludedMeetingsForDirector1(int UserID);
        List<DraftMeetingListVM> GetConcludedMeetingsForDirector(int customerId, int userId);
        #endregion

        #region Delete Meeting
        DeleteMeetingVM GetDetailsMeetingCanDelete(long meetingId);
        DeleteMeetingVM DeleteMeeting(DeleteMeetingVM obj, int userId,int CustomerId);
        #endregion

        #region Meeting Postponded
        Message Postpond(long MeetingId, int postpondedBy);
        #endregion

        #region Virtual Meeting
        Message ReOpenVirtualMeeting(long meetingId, int userId);
        #endregion

        #region Seek Availability
        IEnumerable<MeetingAvailabilityVM> GetAllSeekAvailability(long parentID);
        MeetingAvailabilityVM GetSeekAvailability(long id);
        MeetingAvailabilityVM Create(MeetingAvailabilityVM obj);
        MeetingAvailabilityVM Update(MeetingAvailabilityVM obj);
        bool UpdateAvailabilityDueDate(long MeetingId, DateTime date_, int UserId);
        MeetingVM ForceEditSeekAvailability(long CustomerId, long MeetingId);

        MeetingAvailabilityVM SelectSeekAvailability(MeetingAvailabilityVM obj, int UserId);
        #endregion

        #region Meeting Participants
        List<MeetingParticipants_ResultVM> GetMeetingParticipants(long Meeting_Id);
        bool SaveMeetingParticipants(int customerID, int entityId, int meetingTypeId, long meetingID, int userID, DateTime? meetingDate);
        OtherScheduledMeetings_ResultVM GetParticipantOtherScheduledMeetings(long meetingId);
        #endregion

        #region Pending Availability
        IEnumerable<MeetingVM> GetPedingAvailability(long UserId);
        IEnumerable<CommitteeCompVM> GetAllMeetingType(string type);
        #endregion

        #region Meeting Agenda
        List<AgendaComplianceMappingPendingVM> CheckComplianceAssignedOrNot(long AgendaId, long meetingId, int customerId, int? entityId);
        IEnumerable<MeetingAgendaMappingVM> GetMeetingAgenda(long MeetingId, string Part,string Type);
        AgendaItemSelect AddAgendaItem(AgendaItemSelect obj, int createdBy, int customerID);
        MeetingAgendaMappingVM GetAgendaItem(long MeetingAgendaMappingID);
        MeetingAgendaMappingVM UpdateAgendaItem(MeetingAgendaMappingVM obj, int updatedBy);
        MeetingAgendaMappingVM SaveOrderAgendaItem(MeetingAgendaMappingVM obj, int SrNo);
        PreviewAgendaVM PreviewAgenda(long MeetingId, int CustomerId);
        void SetDefaultItemNumbers(long MeetingId);
        bool IsVideoConferencingApplicable_CustomerWise(int customerId);
        #endregion

        #region Move Any Other business Agenda Items to Part B
        void MoveAnyOtherAgendaItems(long MeetingId);
        #endregion

        #region Delete Agenda
        DeleteAgendaVM GetAllowAgendaDelete(long meetingId, long meetingAgendaMappingId, bool IsMeetingDelete);
        DeleteAgendaVM DeleteAgendaItem(DeleteAgendaVM obj, int userId);
        #endregion

        #region Meeting Pre Committee Agenda
        List<MeetingPendingPreCommitteeVM> GetPendingPreCommittee(long MeetingId);
        List<PreCommiitteeDraftMeetingVM> GetDraftCommitteeMeeting(long PreCommitteeId, int EntityId, int CustomerId);
        string SavePreCommitteeMeetingID(int MeetingTypeID, long SourceMeetingID, long SourceMeetingAgendaMappingID, long? PostAgendaId, long TargetMeetingID, int UserId);
        #endregion

        #region Meeting Availability Mail
        MeetingAvailabilityMailVM UpdateAvailabilityMail(MeetingAvailabilityMailVM obj);
        MeetingAvailabilityMailVM PreviewAvailabilityMail(MeetingAvailabilityMailVM obj);
        MeetingAvailabilityMailVM SendAvailabilityMail(MeetingAvailabilityMailVM obj, out string MailFormat);

        #endregion

        #region Seek Availability Response
        List<AvailabilityResponse_ResultVM> GetAvailabilityResponse(long MeetingId, int CustomerId, int? UserId);
        AvailabilityResponse_ResultVM UpdateAvailabilityResponse(AvailabilityResponse_ResultVM obj, int CreatedBy);
        List<AvailabilityResponseChartData_ResultVM> GetAvailabilityResponseChartData(long MeetingId, long MeetingAvailabilityId);
        List<AvailabilityResponseByAvailabilityId_ResultVM> GetAvailabilityResponseByAvailabilityId(long MeetingId, long MeetingAvailabilityId);
        #endregion

        #region Meeting Agenda Item Data Fields Template Generation
        MeetingAgendaTemplateVM GetAgendaItemTemplate(long MeetingAgendaMappingID);
        MeetingAgendaTemplateVM SaveUpdateAgendaItemTemplateList(MeetingAgendaTemplateVM obj, int userID);
        bool InsertTemplates(long? targetMappingId, long? sourceMappingId, int createdBy);
        #endregion

        #region Create New Agenda Item
        AgendaMasterVM CreateNewAgendaItem(long meetingId, int CustomerId);
        AgendaMasterVM CreateNewAgendaItem(AgendaMasterVM obj, int Customer_Id);
        AgendaMasterVM Update(AgendaMasterVM obj);
        #endregion

        #region Meeting Agenda change Items No
        Message CheckAgendaItemNumberIsValid(long meetingId);
        List<ChangeAgendaItemNumberVM> GetAgendaItemForChangeNumber(long meetingId);
        IEnumerable<ChangeAgendaItemNumberVM> SaveAgendaItemNumber(IEnumerable<ChangeAgendaItemNumberVM> obj, int updatedBy);
        #endregion

        #region Meeting Notice Mail
        MeetingNoticeMailVM UpdateNoticeMail(MeetingNoticeMailVM obj);
        MeetingNoticeMailVM PreviewNoticeMail(MeetingNoticeMailVM obj);
        MeetingNoticeMailVM SendNoticeMail(MeetingNoticeMailVM obj, out string MailFormat);
        long CreateNoticeLog(long meetingId, string templateType, string mailTemplate, string senderEmail, long? fileId, int createdBy);
        bool CreateNoticeTransaction(long noticeLogId, long participantId, DateTime dated, bool success);
        List<MeetingNoticeLogResultVM> GetNoticeAgendaLog(long meetingId, int customerId, string templateType);
        Message GetNoticeAgendaLogDetails(long id);
        MeetingNoticeMailVM NoticeAgendaTemplate(MeetingNoticeMailVM obj);
        string GetNoticeFormatByMeetingId(long meetingId);
        #endregion

        #region Invitee Mail
        MeetingInviteeMailVM UpdateInviteeMail(MeetingInviteeMailVM obj, int updatedBy);
        MeetingInviteeMailVM PreviewInviteeMail(MeetingInviteeMailVM obj);
        MeetingInviteeMailVM SendInviteeMail(MeetingInviteeMailVM obj, out string MailFormat);
        #endregion

        #region Circular Meeting Response
        List<Agenda_SummitResponse> GetAgendaResponse(long meetingId, int customerId, int? userId,int? RoleId);
        Agenda_SummitResponse UpdateAgendaResponse(Agenda_SummitResponse item, int userId);
        FinalizeResolutionVM FinalizeResolution(FinalizeResolutionVM obj, int userId, int customerId);
        List<CircularAgendaData> GetAllAgendaDetails(long parentID);
		List<AvailabilityResponseChartData_ResultVM> GetAgendaResponseChartData(long MeetingId, long AgendaId, long MappingId, int UserId);
        #endregion

        #region Meeting Adjourned
        Meeting_AdjournVM Adjourned(long meetingId, int customerId);
        Message Adjourned(long meetingId, string adjournedTime, DateTime adjournedToDate, string adjournedToTime, int adjournedBy, int customerId);
        #endregion

        #region Conclude meeting (Compliance activation, Open Agenda items)
        void ConculdeMeeting(long meetingId, int userId, int customerId);
        void ConculdeAGM(long meetingId, int userId, int customerId);
        #endregion

        #region Create AGM from Board
        void CreateAGMFromBoard(long meetingId, int entityId, int customerId, int createdBy);
        #endregion

        #region Agenda Documents
        bool AgendaFileUpload(VM_AgendaDocument _vmagendaDocument, int uploadedBy, int customerId);
        bool agendaDocumentDownload(long id);
        List<VM_AgendaDocument> GetAgendaDocument(long agendaMappingID, int customerId);
        #endregion

        #region Minutes of Meeting
        PreviewAgendaVM PreviewDraftMinutes(long MeetingId, int CustomerId, long DraftCirculationID, int UserID);
        PreviewAgendaVM PreviewMOM(long MeetingId, int CustomerId);
        MeetingAgendaMappingMOMVM PreviewMOM(long MappingId);
        MeetingAgendaMappingMOMVM GetMOM(long MeetingId, long MappingId, int CustomerId);
        MeetingAgendaMappingMOMVM SaveMOM(MeetingAgendaMappingMOMVM obj, int updatedBy);
        #endregion

        #region Get Minutes Formates For Default Agenda Item (Quorum)
        string GetMinutesForDefaultAgendaItem(int usedFor, List<string> lstTemplateFieldData);
        #endregion

        #region Minutes Formates For Default Agenda Item (Quorum)
        bool SetMinutesForDefaultAgendaItem(long meetingId, int usedFor, List<string> lstLeaveOfAbsence, int userId);
        #endregion

        #region Minutes Details (date of entering etc)
        MeetingMinutesDetailsVM GetMinutesDetails(long meetingId);
        MeetingMinutesDetailsVM CirculateMinutes(MeetingMinutesDetailsVM obj, int userId, out string mailFormat);
        MeetingMinutesDetailsVM SaveMinutesDetails(MeetingMinutesDetailsVM obj, int userId);
        MeetingMinutesDetailsVM FinalizedMinutes(MeetingMinutesDetailsVM obj, int userId);
        MeetingMinutesDetailsVM FinalizedMinutesOfVirtualMeeting(MeetingMinutesDetailsVM obj, FileDataVM objFileData, int userId);
        #endregion

        #region Meeting level compliance reopen for  Virtaul Meeting
        bool ReOpenMeetingLevelCompliance(long meetingId, string meetingEvent);
        #endregion

        #region Minutes Comments
        MeetingMinutesApproveVM ApproveDraftMinutes(MeetingMinutesApproveVM obj, int userId);
        bool IsDraftMinutesApproved(long draftCirculationID);
        #endregion

        #region Director UI
        IEnumerable<MeetingVM> GetTodaysMeetingDirectorwise(int customerId, int userId);
        #endregion

        #region Meeting Details
        MeetingDetailsVM GetMeetingOtherDetails(long meetingId, int entityId);
        MeetingDetailsVM SaveMeetingOtherDetails(MeetingDetailsVM obj, int userId);
        #endregion

        #region Check AGM Exists by FY
        bool CheckAGMExistsOrNotByFY(int entityId, int fyId);
        bool CheckAGMExistsOrNotBySrNo(int entityId, int SrNo);
        int? GetNewAGMSrNo(int entityId);
        int? GetNextMeetingSrNo(int entityId, int meetingTypeId, string meetingCircular, int fyId);
        #endregion

        #region General meeting Notes
        MeetingNotesVM SaveNotes(MeetingNotesVM obj, int userId);
        #endregion

        #region Meeting Documents
        Message UploadMeetingDocuments(VM_AgendaDocument obj, int userId, int customerId);
        #endregion
        #region Agenda - Performer/Reviewer
        AgendaReviewVM GetAgendaReviewDetails(long mappingId);
        #endregion

        #region Set Agenda document generation flag
        void MeetingAgendaSetGenerationFlag(long meetingId);
        #endregion

		#region Draft Minutes viewer using Syncfusion
        MinutesViewerVM GetDraftMinutesDetails(long meetingId);
        #endregion

        #region Copy Meetings
        List<CopyMeetingSourceMeetingVM> GetSourceMeetings(int CustomerId);
        List<MeetingAgendaListForCopyMeeting_ResultVM> GetSourceMeetingsAgendaList(long meetingId, int customerId);
        CopyMeetingVM SaveCopyMeetings(CopyMeetingVM obj, int customerID, int UserID);
        CopyMeetingMessageVM CopyMeetingCheckComplianceAssigned(CopyMeetingCheckComplianceAssignedVM obj, int userId, int customerId);
        List<CopyMeetingLogVM> GetCopyMeetingLog(long meetingId, int customerId);
        #endregion
    }
}