﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM.UIForms;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IKMP_Master
    {
        Director_MasterVM_KMP CreateKMP(Director_MasterVM_KMP _objDirector_MasterVM_KMP, int coustomerId);
        Director_MasterVM_KMP UpdateKMP(Director_MasterVM_KMP _objDirector_MasterVM_KMP, int coustomerId);
        Director_MasterVM_KMP DeleteKMP(Director_MasterVM_KMP _objDirector_MasterVM_KMP, int coustomerId);
        List<Director_MasterVM_KMP> GetKMP_Dtls(int coustomerId);
        Director_Master_KMPCompanySecurity CreateKMP_CompanySecurity(Director_Master_KMPCompanySecurity _obj_KMP, int coustomerId);
        Director_Master_KMPCompanySecurity UpdateKMP_CompanySecurity(Director_Master_KMPCompanySecurity _obj_KMP, int coustomerId);

        Director_Master_KMPCompanySecurity DeleteKMPdtls(Director_Master_KMPCompanySecurity _obj_KMP, int coustomerId);
        Director_MasterVM_KMP GetKMP_Dtls(int id, int customerId);
        Director_Master_KMPCompanySecurity GetKMPCompanyDtls(int id, int customerId);
        bool DeleteKMPInfo(int id, int customerId);
        void KMP_SecuritiesDetails(global::OfficeOpenXml.ExcelPackage xlWorkbook, int id, string current_Date);
        bool CreateKMP_SecurityDetails(BM_SecurityDetail obj_Kmpsecurity);
        List<BM_SecurityDetail> GetKMPseurityDtails(int customerId,long KMPId);
        Director_Master_KMPCompanySecurity GetsecuritybyId(long kmpID, long securityId);
        #region UI Form

        List<Director_MasterVM_KMP> GetKMPByDesignation(int designationId, long? entityId, int customerId);
        UIForm_KMPMasterVM CreateUpdateKMP(UIForm_KMPMasterVM obj, int userId, int customerId);

        UIForm_KMPMasterCessationVM ResignationOfKMP(UIForm_KMPMasterCessationVM obj, int userId, int customerId);
        #endregion
    }
}
