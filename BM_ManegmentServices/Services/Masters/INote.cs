﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public interface INote
    {
        string AddNewNotes(VMNotes _objnotes, int customerId);
        List<VMNotes> GetAllNotes(int customerId);
        IEnumerable<BM_NotesTypes> GetAllNotesType(int customerId);
        VMNotes UpdateNotes(VMNotes _objnotes);
        string DeleteNotes(VMNotes _objnotes, int customerId);
        VMNotes CreateNotes(VMNotes objnotes);
        List<VMNotes> GetsNotes(int userId, int customerId, long meetingId, long agendaId);
        VMNotes GetNotesbyId(long id, long meetingId, long agendaId);
        List<VMNotes> GetsSharedNotes(int userId, int customerId, long meetingId);
    }
}
