﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Masters
{
    public class DesignationMaster : IDesignationMaster
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public List<VMDesignation> GetDesignationData()
        {
            var result = (from row in entities.BM_DesignationMaster
                          where row.IsDeleted == false
                          select new VMDesignation
                          {
                              ID = row.ID,
                              Name = row.DesignationName
                          }).ToList();

            return result;
        }

        public VMDesignation GetDesignationMaster(int id)
        {
            var result = (from row in entities.BM_DesignationMaster
                          where row.IsDeleted == false
                          && row.ID == id
                          select new VMDesignation
                          {
                              ID = row.ID,
                              Name = row.DesignationName
                          }).FirstOrDefault();

            return result;
        }

        public VMDesignation SaveUpdateDesignationDetials(VMDesignation objVMDesignation)
        {
            try
            {
                if (objVMDesignation != null)
                {
                    BM_DesignationMaster objDesignation = new BM_DesignationMaster();

                    var QueryResult = (from row in entities.BM_DesignationMaster
                                       where row.IsDeleted == false &&
                                       row.ID == objVMDesignation.ID
                                       select row).FirstOrDefault();

                    if (QueryResult != null)
                    {
                        var Result = (from row in entities.BM_DesignationMaster
                                      where row.IsDeleted == false &&
                                      row.ID != objVMDesignation.ID &&
                                      row.DesignationName == objVMDesignation.Name
                                      select row).FirstOrDefault();
                        if (Result == null)
                        {
                            QueryResult.DesignationName = objVMDesignation.Name;
                            QueryResult.IsDeleted = objVMDesignation.IsDeleted;
                            //QueryResult.CreatedBy = objVMDesignation.CreatedBy;
                            //QueryResult.CreatedOn = objVMDesignation.CreatedOn;
                            QueryResult.UpdatedBy = objVMDesignation.UpdatedBy;
                            QueryResult.UpdatedOn = objVMDesignation.UpdatedOn;
                            entities.SaveChanges();
                            objVMDesignation.SuccessMessage = "Designation Details updated successfully.";
                            return objVMDesignation;
                        }
                        else
                        {
                            objVMDesignation.ErrorMessage = "Designation with same name already exist.";
                            return objVMDesignation;
                        }
                    }
                    else
                    {
                        var Result = (from row in entities.BM_DesignationMaster
                                      where row.IsDeleted == false &&
                                      row.DesignationName == objVMDesignation.Name
                                      select row).FirstOrDefault();
                        if (Result == null)
                        {
                            objDesignation.DesignationName = objVMDesignation.Name;
                            objDesignation.IsDeleted = objVMDesignation.IsDeleted;
                            objDesignation.CreatedBy = objVMDesignation.CreatedBy;
                            objDesignation.CreatedOn = objVMDesignation.CreatedOn;
                            //objDesignation.UpdatedBy = objVMDesignation.UpdatedBy;
                            //objDesignation.UpdatedOn = objVMDesignation.UpdatedOn;
                            entities.BM_DesignationMaster.Add(objDesignation);
                            entities.SaveChanges();
                            objVMDesignation.SuccessMessage = "Designation Details added successfully.";
                            return objVMDesignation;
                        }
                        else
                        {
                            objVMDesignation.ErrorMessage = "Designation with same name already exist.";
                            return objVMDesignation;
                        }
                    }
                }
                else
                {
                    objVMDesignation.ErrorMessage = "Add Designation details currect format.";
                    return objVMDesignation;
                }
            }
            catch (Exception ex)
            {
                objVMDesignation.ErrorMessage = "Something went wrong...";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return objVMDesignation;
            }
        }

        public bool DeleteDesignationData(int id)//, int UserID
        {
            try
            {
                if (id > 0)
                {
                    BM_DesignationMaster objDesignation = new BM_DesignationMaster();

                    var QueryResult = (from row in entities.BM_DesignationMaster
                                       where row.IsDeleted == false &&
                                       row.ID == id
                                       select row).FirstOrDefault();

                    if (QueryResult != null)
                    {
                        QueryResult.IsDeleted = true;
                        //QueryResult.UpdatedBy = UserID;
                        QueryResult.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public List<Director_DesignationVM> GetDirectorDesignation()
        {
            var result = (from row in entities.BM_Directors_Designation
                          select new Director_DesignationVM
                          {
                              Id = row.Id,
                              Name = row.Name
                          }).ToList();

            return result;
        }

        public List<VMDesignation> GetDesignationForDirector(string insertSelect)
        {
            var result = new List<VMDesignation>();
            try
            {
                result = (from row in entities.BM_DesignationMaster
                          where row.IsActive == true && row.IsDeleted == false && row.IsForDirector == true
                          select new VMDesignation
                          {
                              ID = row.ID,
                              Name = row.DesignationName
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            if (result == null)
            {
                result = new List<VMDesignation>();
            }
            if (insertSelect == "1")
            {
                result.Insert(0, new VMDesignation() { ID = 0, Name = "Select" });
            }
            return result;
        }
        public List<VMDesignation> GetDesignation(bool isDirector, bool isMNGT, string insertSelect,bool isforLLp)//isforLLp parameter added by Ruchi on 18th of Feb 2021!
        {
            var result = new List<VMDesignation>();
            try
            {
                if (isforLLp)
                {
                    result = (from row in entities.BM_DesignationMaster
                              where row.IsActive == true && row.IsDeleted == false
                              //&& row.IsForDirector == isDirector
                              //&& row.IsForKMP == isMNGT
                              && row.IsForLLP == isforLLp
                              select new VMDesignation
                              {
                                  ID = row.ID,
                                  Name = row.DesignationName
                              }).ToList();
                }
                else
                {
                    result = (from row in entities.BM_DesignationMaster
                              where row.IsActive == true && row.IsDeleted == false && row.IsForDirector == isDirector
                              && row.IsForKMP == isMNGT
                              && row.IsForLLP == isforLLp
                              select new VMDesignation
                              {
                                  ID = row.ID,
                                  Name = row.DesignationName
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            if (result == null)
            {
                result = new List<VMDesignation>();
            }
            if (insertSelect == "1")
            {
                result.Insert(0, new VMDesignation() { ID = 0, Name = "Select" });
            }
            return result;
        }
        public List<DirectorTypeOfMembershipVM> GetDirectorTypeOfMembership(int designationId, string insertSelect)
        {
            var result = new List<DirectorTypeOfMembershipVM>();
            try
            {
                result = (from row in entities.BM_Designation_TypeOfDirectorShipMapping
                          join rows in entities.BM_Directors_Designation on row.TypeOfDirectorShipId equals rows.Id
                          where row.DesignationId == designationId && row.IsActive == true && row.IsDeleted == false
                          select new DirectorTypeOfMembershipVM
                          {
                              ID = rows.Id,
                              Name = rows.Name,
                              IsChairman = rows.IsChairman
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            if (result == null)
            {
                result = new List<DirectorTypeOfMembershipVM>();
            }
            if (insertSelect == "1")
            {
                result.Insert(0, new DirectorTypeOfMembershipVM() { ID = 0, Name = "Select", IsChairman = false });
            }
            return result;
        }
    }
}