﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IEntityuserAssignment
    {
        VM_EntityuserAssignment UpdateUserAssignment(VM_EntityuserAssignment _objentityassignment, int updatedBy, int customerId);
        VM_EntityuserAssignment AddUserAssignment(VM_EntityuserAssignment _objentityassignment, int createdBy);
        VM_EntityuserAssignment GetUserAssignmentbyId(int id);
        List<VM_EntityuserAssignment> GetEntityAssignment(int customerId);
        IEnumerable<UserforDropdown> GetUser(string Role,int CustomerId);
        IEnumerable<UserforDropdown> GetUserForEntityAssignment(string role, int customerId);
        IEnumerable<UserforDropdown> GetUserforTask(string role,int CustomerId,int userId);
        bool DeleteEntityAssignment(int Id, int EntityId);
        UserMeetingTypeAssignmentVM GetUserMeetingTypeAssignmentById(int assignmentId, int entityId);
        UserMeetingTypeAssignmentVM SaveMeetingTypeAssignment(UserMeetingTypeAssignmentVM obj, int userId);
    }
}
