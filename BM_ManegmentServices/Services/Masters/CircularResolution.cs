﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public class CircularResolution : ICircularResolution
    {
        public List<VM_CircularResolution> GetAll_CircularResolution(int customerID)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var lstCirculars = (from row in entities.BM_CircularMasterList
                                           where row.CustomerID == customerID                                           
                                           && row.IsActive == true
                                           select new VM_CircularResolution
                                           {
                                               CircularID = row.CircularID,
                                               meetingTypeName = (from x in entities.BM_CommitteeComp where x.Id == row.MeetingTypeID && x.IsDeleted == false select x.MeetingTypeName).FirstOrDefault(),
                                               companyName = (from y in entities.BM_EntityMaster where y.Id == row.CompanyID && y.Is_Deleted == false select y.CompanyName).FirstOrDefault(),
                                               CircularDate = row.CircularDate,
                                               DueDate = row.DueDate
                                           }).ToList();

                    return lstCirculars;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public VM_CircularResolution GetCircularDetailByID(int ID)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _objCircular = (from row in entities.BM_CircularMasterList
                                        where row.CircularID == ID
                                        && row.IsActive == true
                                        select new VM_CircularResolution
                                        {
                                            CircularID = row.CircularID,
                                            meetingTypeID = (int)row.MeetingTypeID,
                                            companyID = row.CompanyID,
                                            CircularDate = row.CircularDate,
                                            DueDate = row.DueDate,
                                            customerID = row.CustomerID,
                                            lstAgendas = (from y in entities.BM_CircularAgendaMapping
                                                          where y.CircularID == row.CircularID 
                                                          && y.IsActive == true
                                                          select y.AgendaID).ToList(),
                                        }).FirstOrDefault();

                    return _objCircular;
                }
            }
            catch (Exception ex)
            {
                VM_CircularResolution obj = new VM_CircularResolution();
                obj.errorMessage = true;
                obj.Message = "Something went wrong, Please try again";
                return obj;
            }
        }

        public VM_CircularResolution CreateUpdate_CircularResolution(VM_CircularResolution record)
        {
            try
            {
                long circularID = 0;
                bool saveSuccess = false;
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var prevCircular = (from row in entities.BM_CircularMasterList
                                        where row.CircularID == record.CircularID
                                        && row.IsActive == true
                                        select row).FirstOrDefault();

                    if (prevCircular != null)
                    {
                        prevCircular.CompanyID = record.companyID;
                        prevCircular.MeetingTypeID = record.meetingTypeID;
                        prevCircular.CircularNo = record.CircularNo;
                        prevCircular.CircularDate = record.CircularDate;
                        prevCircular.DueDate = record.DueDate;
                        prevCircular.CustomerID = record.customerID;
                        prevCircular.UpdatedOn = DateTime.Now;
                        prevCircular.UpdatedBy = record.loggedinUserID;
                        prevCircular.IsActive = true;
                        entities.SaveChanges();

                        circularID = prevCircular.CircularID;
                    }
                    else
                    {
                        BM_CircularMasterList newRecord = new BM_CircularMasterList()
                        {
                            CompanyID = record.companyID,
                            MeetingTypeID = record.meetingTypeID,
                            CircularNo = record.CircularNo,
                            CircularDate = record.CircularDate,
                            DueDate = record.DueDate,
                            CustomerID = record.customerID,
                            CreatedOn = DateTime.Now,
                            CreatedBy = record.loggedinUserID,
                            IsActive = true
                        };

                        entities.BM_CircularMasterList.Add(newRecord);
                        entities.SaveChanges();

                        circularID = newRecord.CircularID;

                        record.saveSuccess = true;
                        record.Message = "Details Save Successfully";
                    }

                    if (record.lstAgendas.Count > 0 && circularID != 0)
                    {
                        List<BM_CircularAgendaMapping> lstAgendaMapping = new List<BM_CircularAgendaMapping>();

                        record.lstAgendas.ForEach(eachAgenda =>
                        {
                            BM_CircularAgendaMapping objCirAgenda = new BM_CircularAgendaMapping()
                            {
                                CircularID = prevCircular.CircularID,
                                AgendaID = eachAgenda,
                                IsActive = true
                            };

                            lstAgendaMapping.Add(objCirAgenda);                            
                        });

                        if (lstAgendaMapping.Count > 0)
                            saveSuccess = CreateUpdate_CircularAgendaMapping(lstAgendaMapping);
                    }

                    if (saveSuccess)
                    {
                        record.saveSuccess = true;
                        record.Message = "Details Save Successfully";
                    }

                    return record;
                }
            }
            catch (Exception ex)
            {
                VM_CircularResolution obj = new VM_CircularResolution();
                obj.errorMessage = true;
                obj.Message = "Something went wrong, Please try again";
                return obj;
            }
        }

        public bool CreateUpdate_CircularAgendaMapping(List<BM_CircularAgendaMapping> lstRecords)
        {
            bool saveSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstRecords.ForEach(record =>
                    {
                        var prevMapping = (from row in entities.BM_CircularAgendaMapping
                                           where row.CircularID == record.CircularID
                                           && row.AgendaID == record.AgendaID
                                           select row).FirstOrDefault();

                        if (prevMapping != null)
                        {
                            prevMapping.IsActive = true;
                            entities.SaveChanges();
                            saveSuccess = true;
                        }
                        else
                        {
                            record.CreatedOn = DateTime.Now;
                            entities.BM_CircularAgendaMapping.Add(record);
                            entities.SaveChanges();
                            saveSuccess = true;
                        }
                    });

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                return saveSuccess;
            }
        }
    }
}