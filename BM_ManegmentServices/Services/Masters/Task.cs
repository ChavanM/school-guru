﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using System.Reflection;
using System.IO;
using System.Web.Hosting;
using BM_ManegmentServices.Data;
using System.Data.Entity.SqlServer;
using System.Data.Entity;

namespace BM_ManegmentServices.Services.Masters
{
    public class Task : ITask
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public static string DocPath = "";
        public IEnumerable<TaskMeeting> GetMeetingList(long EntityId, int loggedInUserID, string userRole)
        {
            try
            {
                var _objGetMeetingList = (from row in entities.BM_SP_GetMeetingList(loggedInUserID, userRole)
                                          where row.EntityId == EntityId
                                          select new TaskMeeting
                                          {
                                              MeetingId = row.MeetingID,
                                              MeetingTitle = row.MeetingTitle
                                          }).ToList();

                return _objGetMeetingList;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_MeetingAgendaMapping> GetAgendaList(int MeetingId)
        {
            try
            {
                var _objGetMeetingList = (from row in entities.BM_MeetingAgendaMapping
                                          where row.IsDeleted == false && row.MeetingID == MeetingId
                                          select row).ToList();

                return _objGetMeetingList;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMTask UpdateTask(VMTask vmtask)
        {
            try
            {
                var CheckExist = (from row in entities.BM_Task
                                  where row.ID == vmtask.Id
                                  //where row.EntityId == vmtask.EntityId && row.TaskType == row.TaskType
                                  select row).FirstOrDefault();
                if (CheckExist != null)
                {
                    if (vmtask.EntityId > 0)
                    {
                        CheckExist.EntityId = (long)vmtask.EntityId;
                    }

                    CheckExist.CustomerId = vmtask.CustomerId;
                    CheckExist.TaskType = vmtask.TaskType;
                    CheckExist.TaskTitle = vmtask.TaskTitle;
                    CheckExist.TaskDesc = vmtask.Description;

                    CheckExist.AssignOn = DateTime.Now;
                    CheckExist.DueDate = Convert.ToDateTime(vmtask.DueDate);
                    CheckExist.MeetingId = vmtask.MeetingType;
                    CheckExist.AgendaId = vmtask.AgendaId;
                    CheckExist.IsActive = true;
                    CheckExist.UpdatedOn = DateTime.Now;
                    CheckExist.Updatedby = vmtask.UserId;

                    entities.SaveChanges();

                    if (vmtask.files != null)
                    {
                        bool TaskUpload = FileUpload.SaveTaskFile(vmtask.files, vmtask.CustomerId, vmtask.UserId, vmtask.Id);
                    }

                    vmtask.successMessage = true;
                    vmtask.successErrorMsg = "Updated Successfully";
                }
                return vmtask;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                vmtask.errorMessage = true;
                vmtask.successErrorMsg = SecretarialConst.Messages.validationError;
                
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

                return vmtask;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                vmtask.errorMessage = true;
                vmtask.successErrorMsg = SecretarialConst.Messages.serverError;
                return vmtask;
            }
        }

        public VMTask CreateTask(VMTask vmtask)
        {
            try
            {
                var CheckExist = (from row in entities.BM_Task
                                  where row.EntityId == vmtask.EntityId
                                  && row.TaskTitle == vmtask.TaskTitle
                                  select row).FirstOrDefault();

                if (CheckExist == null && vmtask.DueDate != null)
                {
                    BM_Task bm_task = new BM_Task();
                    if (vmtask.EntityId > 0)
                    {
                        bm_task.EntityId = (long)vmtask.EntityId;
                    }
                    bm_task.CustomerId = vmtask.CustomerId;
                    bm_task.TaskType = vmtask.TaskType;
                    bm_task.TaskTitle = vmtask.TaskTitle;
                    if (vmtask.Description == null)
                    {
                        bm_task.TaskDesc = "";
                    }
                    else
                    {
                        bm_task.TaskDesc = vmtask.Description;
                    }
                    bm_task.AssignOn = DateTime.Now;
                    bm_task.DueDate = DateTime.ParseExact(vmtask.DueDate, "dd/MM/yyyy", null);
                    if (vmtask.TaskType == 1)
                    {
                        bm_task.MeetingId = vmtask.MeetingType;
                    }
                    else if (vmtask.TaskType == 2)
                    {
                        bm_task.MeetingId = vmtask.MeetingType;
                        bm_task.AgendaId = vmtask.AgendaId;
                    }
                    if (vmtask.Tasklevel == "M" && vmtask.RoleName == SecretarialConst.Roles.DRCTR)
                    {
                        bm_task.IsATR = true;
                    }
                    else
                    {
                        bm_task.IsATR = false;
                    }

                    bm_task.IsActive = true;
                    bm_task.CreatedOn = DateTime.Now;
                    bm_task.Createdby = vmtask.Createby;
                    bm_task.status = "Pending";
                    entities.BM_Task.Add(bm_task);
                    entities.SaveChanges();
                    vmtask.Id = bm_task.ID;
                    if (vmtask.files != null)
                    {
                        bool TaskUpload = FileUpload.SaveTaskFile(vmtask.files, vmtask.CustomerId, vmtask.UserId, vmtask.Id);
                    }
                    if (vmtask.Id > 0)
                    {
                        BM_TaskAssignment objtaskAssignment = new BM_TaskAssignment();
                        objtaskAssignment.UserID = (long)vmtask.Createby;
                        objtaskAssignment.RoleId = SecretarialConst.RoleID.REVIEWER;
                        //objtaskAssignment.RoleId = 4;
                        objtaskAssignment.TaskInstanceID = vmtask.Id;
                        objtaskAssignment.CreatedOn = DateTime.Now;
                        objtaskAssignment.Createdby = vmtask.Createby;
                        objtaskAssignment.IsActive = true;
                        entities.BM_TaskAssignment.Add(objtaskAssignment);
                        entities.SaveChanges();
                        if (objtaskAssignment.ID > 0)
                        {
                            BM_TaskAssignment objtaskAssignmentper = new BM_TaskAssignment();
                            objtaskAssignmentper.UserID = vmtask.UserId;
                            objtaskAssignmentper.RoleId = SecretarialConst.RoleID.PERFORMER;
                            objtaskAssignmentper.TaskInstanceID = vmtask.Id;
                            objtaskAssignmentper.CreatedOn = DateTime.Now;
                            objtaskAssignmentper.Createdby = vmtask.Createby;
                            objtaskAssignmentper.IsActive = true;
                            entities.BM_TaskAssignment.Add(objtaskAssignmentper);
                            entities.SaveChanges();
                        }
                    }

                    vmtask.successMessage = true;
                    vmtask.successErrorMsg = SecretarialConst.Messages.saveSuccess;
                    return vmtask;
                }
                else
                {
                    vmtask.errorMessage = true;
                    vmtask.successErrorMsg = "Task with same title already exists";
                    return vmtask;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                vmtask.errorMessage = true;
                vmtask.successErrorMsg = SecretarialConst.Messages.serverError;
                return vmtask;
            }
        }

        public Message DeleteTask(long id, int userId)
        {
            var result = new Message();
            try
            {
                var CheckExist = (from row in entities.BM_Task
                                  where row.ID == id
                                  select row).FirstOrDefault();
                if (CheckExist != null)
                {
                    CheckExist.IsActive = false;
                    CheckExist.Deletedby = userId;
                    CheckExist.DeletedOn = DateTime.Now;
                    entities.SaveChanges();

                    result.Success = true;
                    result.Message = SecretarialConst.Messages.deleteSuccess;
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public VMTask CreateTaskAssinment(VMTask vmtask)
        {
            try
            {

                var checkTaskAssignment = (from row in entities.BM_TaskAssignment
                                           where row.TaskInstanceID == vmtask.TaskId
                                           && row.IsActive == true
                                           && row.UserID == vmtask.UserId
                                           && row.RoleId == vmtask.Role
                                           select row).ToList();

                if (checkTaskAssignment.Count > 2)
                {
                    vmtask.errorMessage = true;
                    vmtask.successErrorMsg = "Performer and Reviewer is not greater then three";
                    return vmtask;
                }
                else
                {
                    var entity = new BM_TaskAssignment();
                    entity.RoleId = vmtask.Role;
                    entity.UserID = vmtask.AssignTo;
                    entity.TaskInstanceID = vmtask.TaskId;
                    entity.CreatedOn = DateTime.Now;
                    entity.Createdby = vmtask.UserId;
                    entity.IsActive = true;
                    // entity.Status = "Pending";
                    entities.BM_TaskAssignment.Add(entity);
                    entities.SaveChanges();
                    vmtask.TaskAssinmentId = entity.ID;
                    vmtask.AssignTo = (int)entity.UserID;
                    vmtask.Role = entity.RoleId;
                    if (entity.ID > 0)
                    {
                        BM_TaskTransaction bm_taskTransaction = new BM_TaskTransaction();
                        bm_taskTransaction.TaskID = vmtask.TaskId;
                        bm_taskTransaction.IsActive = true;
                        bm_taskTransaction.Status = "Open";
                        bm_taskTransaction.StatusChangeOn = DateTime.Now;
                        bm_taskTransaction.UserID = vmtask.AssignTo;
                        bm_taskTransaction.RoleID = vmtask.Role;
                        bm_taskTransaction.CreatedOn = DateTime.Now;
                        bm_taskTransaction.CreatedBy = vmtask.UserId;
                        entities.BM_TaskTransaction.Add(bm_taskTransaction);
                        entities.SaveChanges();
                        vmtask.TaskTransactionId = bm_taskTransaction.ID;
                    }
                    GetAll(Convert.ToInt32(vmtask.TaskId)).Insert(0, vmtask);
                    return vmtask;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return vmtask;
            }
        }

        public List<VMTask> GetAll(int TaskId)
        {
            var result = entities.BM_TaskAssignment.Where(x => x.TaskInstanceID == TaskId).Select(assinment => new VMTask
            {
                AssignTo = (int)assinment.UserID,
                Role = assinment.RoleId,
                TaskAssinmentId = assinment.ID,
                TaskId = assinment.TaskInstanceID,
                UserName = (from x in entities.Users where x.ID == assinment.UserID select x.FirstName + "  " + x.LastName).FirstOrDefault(),
                RoleName = assinment.RoleId == 4 ? "Reviwer" : "Performer"
            }).ToList();

            return result;
        }

        public List<VMTask> ReadData(int TaskId)
        {
            try
            {
                return GetAll(TaskId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<VMTask> GetTaskDetails(int customerId, int userId, int RoleId, bool? loadTask, bool? loadReview)
        {
            try
            {
                var today = DateTime.Today;
                List<VMTask> _obj = new List<VMTask>();
                var getTaskDetails = (from row in entities.BM_SP_Task(userId, loadTask, loadReview) orderby row.ID descending select row).ToList();
                var getAssignedUser = (from a in getTaskDetails
                                       orderby a.ID descending
                                       where a.RoleId == RoleId
                                       select new VMTask
                                       {
                                           UserId = (int)a.UserID,
                                           Createby = a.Createdby,
                                           Id = a.ID,
                                           TaskAssinmentId = a.TaskAssignmentID,
                                           MeetingType = a.MeetingId,
                                           TaskTitle = a.TaskTitle,
                                           status = a.Status,
                                           DueDate = a.DueDate.ToString("dd-MMM-yyyy"),
                                           TaskTypes = a.TaskType,
                                           RoleName = a.RoleName,
                                           Taskcreatedby = (from u in entities.Users where u.ID == a.Createdby select u.FirstName + "  " + u.LastName).FirstOrDefault(),
                                           TaskAssignTo = (from u in entities.Users where u.ID == a.UserID select u.FirstName + "  " + u.LastName).FirstOrDefault(),
                                           CompanyName = a.CompanyName,
                                           EntityId = (int)a.EntityId,
                                           IsExpire = a.IsExpire == 1 ? true : false,
                                           StatusId = a.StatusId
                                       }).ToList();

                if (getAssignedUser.Count > 0)
                {
                    getAssignedUser = (from g in getAssignedUser
                                       group g by new
                                       {
                                           g.TaskId,
                                           g.UserId,
                                           g.Createby,
                                           g.Id,
                                           g.TaskAssinmentId,
                                           g.TaskTitle,
                                           g.Description,
                                           g.EntityId,
                                           g.CompanyName,
                                           g.DueDate,
                                           g.AssignOnDate,
                                           g.status,
                                           g.RoleName,
                                           g.TaskTypes,
                                           g.TaskAssignTo,
                                           g.Taskcreatedby,
                                           g.MeetingType,
                                           g.IsExpire,
                                           g.StatusId

                                       } into GCS
                                       select new VMTask()
                                       {
                                           Id = GCS.Key.Id,
                                           TaskAssinmentId = GCS.Key.TaskAssinmentId,
                                           UserId = GCS.Key.UserId,
                                           Createby = GCS.Key.Createby,
                                           TaskTitle = GCS.Key.TaskTitle,
                                           Description = GCS.Key.Description,
                                           EntityId = GCS.Key.EntityId,
                                           CompanyName = GCS.Key.CompanyName,
                                           DueDate = GCS.Key.DueDate,
                                           AssignOnDate = GCS.Key.AssignOnDate,
                                           status = GCS.Key.status,
                                           RoleName = GCS.Key.RoleName,
                                           TaskTypes = GCS.Key.TaskTypes,
                                           TaskAssignTo = GCS.Key.TaskAssignTo,
                                           Taskcreatedby = GCS.Key.Taskcreatedby,
                                           MeetingType = GCS.Key.MeetingType,
                                           IsExpire = GCS.Key.IsExpire,
                                           StatusId = GCS.Key.StatusId
                                       }).ToList();
                }

                return getAssignedUser;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMTask gettaskbyId(int id, int userId, int customerId, string Role)
        {
            try
            {
                VMTask obj = new VMTask();
                DateTime duedate;
                DateTime assigndate;
                //= DateTime.Parse(something);
                int per = 4;
                int rev = 3;
                //DateTime now = DateTime.Now;
                //string asString = now.ToString("dd MMMM yyyy hh:mm:ss tt");
                var gettaskdata = (from row in entities.BM_Task
                                   where row.ID == id
                                   select row).FirstOrDefault();
                if (gettaskdata != null)
                {
                    obj.TaskId = gettaskdata.ID;
                    obj.Id = gettaskdata.ID;
                    // assigndate = DateTime.Parse(gettaskdata.AssignOn);
                    // row.AssignOn == null ? " " : ((DateTime)row.AssignOn).ToString("dd/MM/yyyy"),
                    //DueDate = row.DueDate.ToString("dd/MM/yyyy"),
                    //date= DateTime.Parse(row.DueDate).ToString()
                    duedate = gettaskdata.DueDate;
                    obj.MeetingType = gettaskdata.MeetingId;
                    obj.AgendaId = gettaskdata.AgendaId;
                    obj.DueDate = duedate.ToString("dd/MM/yyyy");
                    obj.TaskType = gettaskdata.TaskType;
                    obj.EntityId = (int)gettaskdata.EntityId;
                    obj.TaskTitle = gettaskdata.TaskTitle;
                    obj.Description = gettaskdata.TaskDesc;
                    obj.Createby = gettaskdata.Createdby;
                    obj.Role = (from x in entities.BM_TaskAssignment where x.UserID == userId && x.TaskInstanceID == id select x.RoleId).FirstOrDefault();
                    obj.Remark = (from y in entities.BM_TaskAssignment where y.UserID == userId && y.TaskInstanceID == id select y.Remark).FirstOrDefault();

                }

                if ((obj.Createby == userId) && ((obj.Role != per) && (obj.Role != rev)))
                {
                    obj.View = "_AddEditTasknew";
                }
                else if (Role == "CS" && obj.Role != per && obj.Role != rev)
                {
                    obj.View = "_AddEditTasknew";
                }
                else
                {
                    if (obj.Role == 4)
                    {
                        obj.View = "_EditAssignTask";
                    }
                    else if (obj.Role == 3)
                    {
                        obj.View = "_EditTaskAssignmentPerformer";
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public VMTask UpdateAssigTask(VMTask _objvmtask)
        {
            try
            {
                var checktaskAssign = (from row in entities.BM_TaskAssignment
                                       where row.ID == _objvmtask.TaskAssinmentId
                                       && row.IsActive == true
                                       && row.TaskInstanceID == _objvmtask.TaskId
                                       select row).FirstOrDefault();
                if (checktaskAssign != null)
                {
                    checktaskAssign.RoleId = _objvmtask.Role;
                    checktaskAssign.UserID = _objvmtask.AssignTo;
                    checktaskAssign.TaskInstanceID = _objvmtask.TaskId;
                    entities.SaveChanges();
                    _objvmtask.successMessage = true;
                    _objvmtask.successErrorMsg = "Updated Successfully.";
                    //return _objvmtask;
                }
                return _objvmtask;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objvmtask;
            }
        }

        public bool DownloadTaskFile(int id)
        {
            try
            {
                var CheckData = (from row in entities.BM_FileData where row.TaskId == id select row).ToList();
                if (CheckData.Count > 0)
                {
                    bool IsDocumentDownload = FileUpload.downloadTaskData(CheckData);
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public bool DownloadTaskFile1(int id)
        {
            try
            {
                var CheckData = (from row in entities.BM_FileData where row.TaskId == id select row).ToList();
                if (CheckData.Count > 0)
                {
                    bool IsDocumentDownload = FileUpload.downloadTaskData(CheckData);
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public bool ViewTaskFile(int id)
        {
            try
            {
                string file = string.Empty;
                var getTaskDocument = (from row in entities.BM_FileData where row.TaskId == id select row).ToList();
                if (getTaskDocument.Count > 0)
                {
                    foreach (var files in getTaskDocument)
                    {
                        string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.FileName));

                        if (files.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string currentDate = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + currentDate;

                            string extension = System.IO.Path.GetExtension(filePath);
                            if (extension != ".zip")
                            {
                                //Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(HostingEnvironment.MapPath(DateFolder));
                                }

                                //string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string currentTime = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                //string User = 6244 + "" + 5 + "" + currentTime;

                                string FileName = DateFolder + "/" + currentTime + "" + extension;

                                FileStream fs = new FileStream(HostingEnvironment.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                bw.Write(CryptographyHandler.Decrypt(FileUpload.ReadDocFiles(filePath)));
                                bw.Close();

                                DocPath = FileName;

                                var documentViewer = new DocumentViewer
                                {
                                    Width = 800,
                                    Height = 600,
                                    Resizable = true,
                                    Document = DocPath
                                };
                            }
                            //else
                            //{
                            //    //lblDocuments.Text = "Zip file cannot be viewed here. Please use the download option.";
                            //    //DocumentViewer1.Visible = false;
                            //}
                        }

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public List<TaskRemarks> GetPerformerDetails(int customerId, int userId, int taskId)
        {
            try
            {
                var getfiledata = (from row in entities.BM_TaskAssignment
                                   where row.TaskInstanceID == taskId
                                   select new TaskRemarks
                                   {
                                       Id = row.ID,
                                       TaskId = row.TaskInstanceID,
                                       User = (from x in entities.Users where x.ID == row.UserID select x.FirstName + " " + x.LastName).FirstOrDefault(),
                                       Remark = row.Remark,
                                       Role = row.RoleId == 3 ? "Performer" : "Reviewer",
                                       //UploadedBy = (from F in entities.Users where F.ID == row.FileUploadedBy select F.FirstName + " " + F.LastName).FirstOrDefault(),
                                       //UploadedOn = (from U in entities.BM_FileData where U.Id == row.fileId select U.UploadedOn).FirstOrDefault()

                                   }).ToList();

                return getfiledata;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMTask UpdateTaskPerformer(VMTask vmtask)
        {
            throw new NotImplementedException();
        }

        public VMTask CreateTaskPerformer(VMTask vmtask)
        {
            try
            {
                var getTasassignkData = (from row in entities.BM_TaskAssignment where row.TaskInstanceID == vmtask.Id && row.UserID == vmtask.UserId select row).FirstOrDefault();
                var getTaskData = (from row in entities.BM_Task where row.ID == vmtask.Id && row.IsActive == true select row).FirstOrDefault();
                //   var getTaskremark = (from x in entities.BM_TaskAssignment where x.TaskId == getTaskData.ID  select x).FirstOrDefault();
                if (getTaskData != null)
                {
                    getTasassignkData.Remark = vmtask.Remark;
                    entities.SaveChanges();

                    if (getTaskData != null)
                    {
                        getTaskData.status = vmtask.status;
                        entities.SaveChanges();
                    }
                    if (vmtask.status.ToLower() == "Approved")
                    {
                        getTaskData.Isclose = true;
                        entities.SaveChanges();
                    }
                    BM_TaskTransaction bm_taskTransaction = new BM_TaskTransaction();
                    bm_taskTransaction.TaskID = vmtask.Id;
                    bm_taskTransaction.IsActive = true;
                    bm_taskTransaction.Status = vmtask.Remark;
                    bm_taskTransaction.StatusChangeOn = DateTime.Now;
                    bm_taskTransaction.UserID = vmtask.UserId;
                    bm_taskTransaction.RoleID = vmtask.Role;
                    bm_taskTransaction.CreatedOn = DateTime.Now;
                    bm_taskTransaction.CreatedBy = vmtask.UserId;
                    entities.BM_TaskTransaction.Add(bm_taskTransaction);

                    entities.SaveChanges();
                    if (vmtask.files != null)
                    {
                        bool savechange = FileUpload.SaveTaskFile(vmtask.files, vmtask.CustomerId, vmtask.UserId, vmtask.Id);
                    }
                    vmtask.successMessage = true;
                    vmtask.successErrorMsg = "Saved successfully";
                }
                return vmtask;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                vmtask.errorMessage = true;
                vmtask.successErrorMsg = "Server error occurred";
                return vmtask;
            }
        }

        public bool ExportExcel(byte[] fileBytes)
        {
            HttpResponse response = HttpContext.Current.Response;
            response.Buffer = true;
            response.ClearContent();
            response.ClearHeaders();
            response.Clear();
            response.AddHeader("content-disposition", "attachment;filename=TaskReport.xlsx");
            response.Charset = "";
            response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            response.BinaryWrite(fileBytes);
            //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "TaskReport.xlsx");
            response.Flush(); // Sends all currently buffered output to the client.
            response.End();
            return true;
        }

        public List<BM_Task_SP_Status_Result> GetPerformerstatus(int customerId, int userId, int taskId)
        {
            try
            {
                List<BM_Task_SP_Status_Result> obj = new List<BM_Task_SP_Status_Result>();

                obj = (from row in entities.BM_Task_SP_Status(taskId) select row).ToList();
                return obj;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMTask GetTask(int taskId)
        {
            try
            {
                var getTask = (from row in entities.BM_Task
                               where row.ID == taskId
                               where row.IsActive == true
                               select new VMTask
                               {
                                   Id = row.ID,
                                   TaskTitle = row.TaskTitle,
                                   //DueDate = row.DueDate.ToString("dd/MM/yyyy"),
                                   DueDate = SqlFunctions.DateName("day", row.DueDate) + "/" + SqlFunctions.DateName("month", row.DueDate) + "/" + SqlFunctions.DateName("year", row.DueDate),
                                   Description = row.TaskDesc
                               }
                               ).FirstOrDefault();

                return getTask;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VMTask> GetTaskType(int taskId, int userId)
        {
            try
            {

                var getTaskType = (from row in entities.BM_SP_Task(userId, true, false)
                                   where row.ID == taskId
                                   select new VMTask
                                   {
                                       TaskTypes = row.TaskType,
                                       TaskTypeDetails = (from x in entities.BM_Meetings where x.MeetingID == row.MeetingId select x.MeetingTitle).FirstOrDefault(),
                                   }).ToList();
                return getTaskType;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public TaskTypeVM getTaskTypeDetails(int taskId)
        {
            try
            {
                TaskTypeVM _tasktype = new TaskTypeVM();
                var objTask = (from row in entities.BM_Task where row.ID == taskId select row).FirstOrDefault();
                if (objTask != null)
                {
                    _tasktype.TaskId = objTask.ID;
                    if (objTask.TaskType == 1 && objTask.MeetingId > 0)
                    {
                        var getMeetingDetails = (from x in entities.BM_Meetings join y in entities.BM_CommitteeComp on x.MeetingTypeId equals y.Id where x.MeetingID == objTask.MeetingId select new { x, y }).FirstOrDefault();
                        if (getMeetingDetails != null)
                        {
                            _tasktype.TaskTypeNameforMeeting = "Meeting";
                            _tasktype.MeetingType = getMeetingDetails.y.MeetingTypeName;
                            _tasktype.MettingDate = getMeetingDetails.x.MeetingDate;
                            _tasktype.MeetingPurpose = getMeetingDetails.x.MeetingTitle;
                        }
                    }
                    else if (objTask.TaskType == 2 && objTask.MeetingId > 0 && objTask.AgendaId > 0)
                    {
                        _tasktype.TaskTypeNameforAgenda = "Agenda";
                        _tasktype.TaskTypeNameforMeeting = "Meeting";
                        var getMeetingDetails = (from x in entities.BM_Meetings join y in entities.BM_CommitteeComp on x.MeetingTypeId equals y.Id where x.MeetingID == objTask.MeetingId select new { x, y }).FirstOrDefault();
                        if (getMeetingDetails != null)
                        {
                            _tasktype.TaskTypeNameforMeeting = "Meeting";
                            _tasktype.MeetingType = getMeetingDetails.y.MeetingTypeName;
                            _tasktype.MettingDate = getMeetingDetails.x.MeetingDate;
                            _tasktype.MeetingPurpose = getMeetingDetails.x.MeetingTitle;
                        }
                        _tasktype.AgendaName = (from x in entities.BM_MeetingAgendaMapping where x.MeetingAgendaMappingID == objTask.AgendaId && x.MeetingID == objTask.MeetingId select x.AgendaItemText).FirstOrDefault();
                    }
                    else if (objTask.TaskType == 4)
                    {
                        _tasktype.TaskTypeName = "Other";
                    }

                }

                return _tasktype;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<string> GetTaskFile(long id)
        {
            try
            {
                var getFileName = (from row in entities.BM_FileData where row.TaskId == id && row.IsDeleted == false select row.FileName).ToList();
                return getFileName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<TaskFile> GetTaskFileDetails(long taskId, int userId, int customerId)
        {
            try
            {
                var gettaskFiledata = (from row in entities.BM_FileData
                                       where row.TaskId == taskId
                                       && row.IsDeleted == false
                                       select new TaskFile
                                       {
                                           FileId = row.Id,
                                           TaskId = row.TaskId,
                                           FileName = row.FileName
                                       }).ToList();
                return gettaskFiledata;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public bool DeleteTask(long fileId)
        {
            try
            {
                var deletefiledata = (from row in entities.BM_FileData where row.Id == fileId && row.IsDeleted == false select row).FirstOrDefault();

                if (deletefiledata != null)
                {
                    deletefiledata.IsDeleted = true;
                    entities.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public string getAgendatitle(int? agendaId)
        {
            try
            {
                var getagendatitle = (from row in entities.BM_AgendaMaster where row.BM_AgendaMasterId == agendaId select row.Agenda).FirstOrDefault();
                return getagendatitle;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #region Agenda/Minutes review
        public VMTask SaveTask(VMTask obj, int createdBy, int customerId, out string mailFormat)
        {
            mailFormat = "";
            try
            {
                if (obj != null)
                {
                    if (obj.lstReviewer != null)
                    {
                        if (obj.lstReviewer.Count == 0)
                        {
                            obj.errorMessage = true;
                            obj.successErrorMsg = "Please select User";
                            return obj;
                        }
                    }
                    else
                    {
                        obj.errorMessage = true;
                        obj.successErrorMsg = "Please select User";
                        return obj;
                    }

                    if (String.IsNullOrEmpty(obj.DueDate))
                    {
                        obj.errorMessage = true;
                        obj.successErrorMsg = "Please enter due date";
                        return obj;
                    }

                    //if (obj.files == null)
                    //{
                    //    obj.errorMessage = true;
                    //    obj.successErrorMsg = "Please Upload Document.";
                    //    return obj;
                    //}
                }

                var checkReviewExist = (from row in entities.BM_Task
                                          where row.EntityId == obj.EntityId && row.MeetingId == obj.MeetingType && row.TaskType == obj.TaskType
                                          && row.IsActive == true && row.StatusId != SecretarialConst.TaskStaus.COMPLETED && row.StatusId != SecretarialConst.TaskStaus.CLOSED
                                          select row).Any();

                if(checkReviewExist == false)
                {
                    var bm_task = new BM_Task();
                    bm_task.EntityId = (long)obj.EntityId;
                    bm_task.CustomerId = customerId;
                    bm_task.TaskType = obj.TaskType;
                    bm_task.TaskTitle = obj.TaskTitle;
                    bm_task.TaskDesc = obj.Description == null ? "" : obj.Description;
                    bm_task.AssignOn = DateTime.Now;
                    bm_task.DueDate = DateTime.ParseExact(obj.DueDate, "dd/MM/yyyy", null);

                    bm_task.MeetingId = obj.MeetingType;

                    bm_task.IsActive = true;
                    bm_task.CreatedOn = DateTime.Now;
                    bm_task.Createdby = createdBy;
                    bm_task.status = "Pending For Review";
                    bm_task.StatusId = 1;
                    entities.BM_Task.Add(bm_task);
                    entities.SaveChanges();
                    obj.Id = bm_task.ID;

                    if (obj.Id > 0)
                    {
                        if (obj.files != null)
                        {
                            bool TaskUpload = FileUpload.SaveTaskFile(obj.files, customerId, createdBy, obj.Id);
                        }

                        BM_TaskAssignment objtaskAssignment = new BM_TaskAssignment();
                        objtaskAssignment.UserID = createdBy;
                        objtaskAssignment.RoleId = SecretarialConst.RoleID.PERFORMER;
                        objtaskAssignment.TaskInstanceID = obj.Id;
                        objtaskAssignment.CreatedOn = DateTime.Now;
                        objtaskAssignment.Createdby = createdBy;
                        objtaskAssignment.IsActive = true;
                        objtaskAssignment.StatusId = 1;
                        objtaskAssignment.Status = bm_task.status;
                        entities.BM_TaskAssignment.Add(objtaskAssignment);
                        entities.SaveChanges();

                        #region Transaction
                        var tran = new BM_TaskTransaction();
                        tran.TaskID = objtaskAssignment.TaskInstanceID;
                        tran.StatusId = objtaskAssignment.StatusId;
                        tran.Status = objtaskAssignment.Status;
                        tran.Comment = "";
                        tran.UserID = createdBy;
                        tran.RoleID = objtaskAssignment.RoleId;
                        tran.StatusChangeOn = DateTime.Now;
                        tran.IsActive = true;
                        tran.CreatedBy = createdBy;
                        tran.CreatedOn = DateTime.Now;
                        entities.BM_TaskTransaction.Add(tran);
                        entities.SaveChanges();
                        #endregion

                        if (objtaskAssignment.ID > 0)
                        {
                            foreach (var item in obj.lstReviewer)
                            {
                                var objtaskAssignmentper = new BM_TaskAssignment();
                                objtaskAssignmentper.UserID = item;
                                objtaskAssignmentper.RoleId = SecretarialConst.RoleID.REVIEWER;
                                objtaskAssignmentper.TaskInstanceID = obj.Id;
                                objtaskAssignmentper.CreatedOn = DateTime.Now;
                                objtaskAssignmentper.Createdby = createdBy;
                                objtaskAssignmentper.IsActive = true;
                                objtaskAssignmentper.StatusId = 1;
                                objtaskAssignmentper.Status = bm_task.status;
                                entities.BM_TaskAssignment.Add(objtaskAssignmentper);
                                entities.SaveChanges();
                            }
                        }
                    }
                    #region Get Mail template
                    mailFormat = (from row in entities.BM_SP_GenerateAgendaMinuteReviewMail(obj.MeetingType, obj.TaskId, null, SecretarialConst.MeetingTemplatType.AGENDA_REVIEW_ASSIGNED)
                                  select row).FirstOrDefault();
                    #endregion

                    obj.successMessage = true;
                    obj.successErrorMsg = SecretarialConst.Messages.saveSuccess;
                    return obj;
                }
                else
                {
                    obj.errorMessage = true;
                    obj.successErrorMsg = "Agenda Review process already in-progress, please complete or cancel to create new.";
                    return obj;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.errorMessage = true;
                obj.successErrorMsg = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
               
                return obj;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                obj.errorMessage = true;
                obj.successErrorMsg = SecretarialConst.Messages.serverError;
                return obj;
            }
        }

        public List<VMTask> GetTaskDetails(long meetingId, int entityId, int taskType)
        {
            var result = new List<VMTask>();
            try
            {
                result = (from row in entities.BM_Task
                        where row.EntityId == entityId && row.MeetingId == meetingId && row.TaskType == taskType
                        && row.IsActive == true
                        select new VMTask
                        {
                            Id = row.ID,
                            EntityId = entityId,
                            MeetingType = row.MeetingId,
                            TaskType = row.TaskType,
                            TaskTitle = row.TaskTitle,
                            StatusId = row.StatusId,
                            status = row.status,
                            TaskDueDate = row.DueDate,
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public VMTask GetTaskDetails(long meetingId, int entityId, int taskId, int taskType)
        {
            var obj = new VMTask();
            try
            {
                obj.TaskId = taskId;
                obj.MeetingType = (int)meetingId;
                obj.EntityId = entityId;
                obj.TaskType = taskType;
                obj.lstReviewer = new List<int>();

                if(taskId > 0)
                {
                    var result = (from row in entities.BM_Task
                                  where row.ID == taskId && row.EntityId == entityId && row.MeetingId == meetingId && row.TaskType == taskType
                                  && row.IsActive == true
                                  select new
                                  {
                                      Id = row.ID,
                                      EntityId = entityId,
                                      MeetingType = row.MeetingId,
                                      TaskType = row.TaskType,
                                      TaskTitle = row.TaskTitle,
                                      StatusId = row.StatusId,
                                      status = row.status,
                                      DueDate = row.DueDate,
                                  }).FirstOrDefault();

                    if (result != null)
                    {
                        obj.Id = result.Id;
                        obj.TaskTitle = result.TaskTitle;
                        obj.StatusId = result.StatusId;
                        obj.status = result.status;
                        obj.DueDate = result.DueDate.ToString("dd/MM/yyyy");
                    }
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                obj.errorMessage = true;
                obj.successErrorMsg = SecretarialConst.Messages.serverError;
                return obj;
            }
            return obj;
        }

        public List<ReviewerRemark> GetReviewerTaskDetails(long taskId)
        {
            var result = new List<ReviewerRemark>();
            try
            {
                result = (from t in entities.BM_Task
                          join row in entities.BM_TaskAssignment on t.ID equals row.TaskInstanceID
                          join user in entities.Users on row.UserID equals user.ID
                          join status in entities.BM_TaskStatus on row.StatusId equals status.Id
                          where t.ID == taskId && row.IsActive == true && row.RoleId == SecretarialConst.RoleID.REVIEWER
                          select new ReviewerRemark
                          {
                              Id = row.ID,
                              TaskId = row.TaskInstanceID,
                              MeetingId = t.MeetingId,
                              ReviewerId = (int)user.ID,
                              RoleId = row.RoleId,
                              ReviewerName = user.FirstName + " " + user.LastName,
                              StatusId = row.StatusId,
                              Status = status.Name,
                              Remark = row.Remark,
                              UpdatedOn = row.UpdatedOn
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<ReviewerRemark> GetReviewerTaskDetails(long entityId, long meetingId, int taskType)
        {
            var result = new List<ReviewerRemark>();
            try
            {
                result = (from t in entities.BM_Task
                          join row in entities.BM_TaskAssignment on t.ID equals row.TaskInstanceID
                          join user in entities.Users on row.UserID equals user.ID
                          join status in entities.BM_TaskStatus on row.StatusId equals status.Id
                          where t.EntityId == entityId && t.MeetingId == meetingId && t.TaskType == taskType && row.IsActive == true && row.RoleId == SecretarialConst.RoleID.REVIEWER
                          orderby (row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn) descending
                          select new ReviewerRemark
                          {
                              Id = row.ID,
                              TaskId = row.TaskInstanceID,
                              MeetingId = t.MeetingId,
                              ReviewerId = (int)user.ID,
                              RoleId = row.RoleId,
                              ReviewerName = user.FirstName + " " + user.LastName,
                              StatusId = row.StatusId,
                              Status = status.Name,
                              Remark = row.Remark,
                              UpdatedOn = row.UpdatedOn,
                              DueDate = t.DueDate
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public ReviewerRemark GetAgendaMinutesReview(long taskId, long assignmentId, int userId, bool canAcceptChanges, bool allowFinalizeReview)
        {
            var result = new ReviewerRemark();
            try
            {
                result = (from row in entities.BM_TaskAssignment
                          join rows in entities.BM_Task on row.TaskInstanceID equals rows.ID
                          where row.ID == assignmentId && row.TaskInstanceID == taskId
                          && row.IsActive == true
                          select new ReviewerRemark
                          {
                              Id = row.ID,
                              TaskId = row.TaskInstanceID,
                              MeetingId = rows.MeetingId,
                              TaskTittle = rows.TaskTitle,
                              //Remark = row.Remark,
                              RoleId = row.RoleId,
                              StatusId = row.StatusId,
                              ReviewerId = (int)row.UserID,
                              AllowApproveReview = row.UserID == userId ? true : false,
                              CanAcceptChanges = canAcceptChanges,
                              AllowFinalizeReview = allowFinalizeReview,
                              DueDate = rows.DueDate
                          }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                return result;
            }
            return result;
        }
        public ReviewerRemark SaveAgendaMinutesReview(ReviewerRemark obj, int userId, out string mailFormat)
        {
            mailFormat = "";
            try
            {
                long? meetingId = null;
                var result = (from row in entities.BM_TaskAssignment
                              where row.ID == obj.Id && row.UserID == userId && row.IsActive == true
                              select row).FirstOrDefault();
                if (result != null)
                {
                    var status = "";
                    switch (obj.StatusId)
                    {
                        case SecretarialConst.TaskStaus.APPROVED:
                            status = "Approved";
                            break;
                        case SecretarialConst.TaskStaus.REJECTED:
                            status = "Rejected";
                            break;
                        default:
                            break;
                    }
                    result.Remark = obj.Remark;
                    result.StatusId = obj.StatusId;
                    result.Status = status;
                    result.Remark = (string.IsNullOrEmpty(result.Remark) ? "" : result.Remark);
                    result.Updatedby = userId;
                    result.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();

                    obj.StatusId = result.StatusId;
                    #region Transaction
                    var tran = new BM_TaskTransaction();
                    tran.TaskID = result.TaskInstanceID;
                    tran.StatusId = result.StatusId;
                    tran.Status = result.Status;
                    tran.Comment = (string.IsNullOrEmpty(result.Remark) ? "" : result.Remark);
                    tran.UserID = userId;
                    tran.RoleID = result.RoleId;
                    tran.StatusChangeOn = DateTime.Now;
                    tran.IsActive = true;
                    tran.CreatedBy = userId;
                    tran.CreatedOn = DateTime.Now;
                    entities.BM_TaskTransaction.Add(tran);
                    entities.SaveChanges();
                    #endregion

                    #region Update Status in task
                    var isApprovalPending = (from row in entities.BM_TaskAssignment
                                             where row.TaskInstanceID == result.TaskInstanceID &&
                                             row.RoleId == SecretarialConst.RoleID.REVIEWER &&
                                             row.IsActive == true && row.StatusId == 1
                                             select row.ID).Any();
                    if (isApprovalPending == false)
                    {
                        var task = (from row in entities.BM_Task
                                    where row.ID == result.TaskInstanceID && row.IsActive == true
                                    select row).FirstOrDefault();
                        if (task != null)
                        {
                            task.StatusId = obj.StatusId; //7;
                            task.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                            meetingId = task.MeetingId;
                        }
                    }
                    #endregion

                    #region Get Mail template
                    if (meetingId == null)
                    {
                        meetingId = (from row in entities.BM_Task
                                     where row.ID == result.TaskInstanceID && row.IsActive == true
                                     select row.MeetingId).FirstOrDefault();
                    }
                    if(obj.StatusId == SecretarialConst.TaskStaus.APPROVED)
                    {
                        mailFormat = (from row in entities.BM_SP_GenerateAgendaMinuteReviewMail(meetingId, result.TaskInstanceID, obj.Id, SecretarialConst.MeetingTemplatType.AGENDA_REVIEW_APPROVED)
                                      select row).FirstOrDefault();
                    }
                    else if(obj.StatusId == SecretarialConst.TaskStaus.REJECTED)
                    {
                        mailFormat = (from row in entities.BM_SP_GenerateAgendaMinuteReviewMail(meetingId, result.TaskInstanceID, obj.Id, SecretarialConst.MeetingTemplatType.AGENDA_REVIEW_REJECTED)
                                      select row).FirstOrDefault();
                    }
                    #endregion
                    obj.Success = true;
                    obj.Message = SecretarialConst.Messages.saveSuccess;
                }
                else
                {
                    obj.Error = true;
                    obj.Message = SecretarialConst.Messages.serverError;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
            }
            return obj;
        }
        public Message CloseAgendaMinutesReview(long taskId, long meetingId, int userId)
        {
            var obj = new Message();
            try
            {
                var result = (from row in entities.BM_Task
                              where row.ID == taskId && row.MeetingId == meetingId && row.IsActive == true
                              select row).FirstOrDefault();
                if (result != null)
                {
                    result.StatusId = SecretarialConst.TaskStaus.CLOSED;
                    result.status = "Closed";
                    result.Updatedby = userId;
                    result.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();

                    #region Transaction
                    var tran = new BM_TaskTransaction();
                    tran.TaskID = result.ID;
                    tran.StatusId = result.StatusId;
                    tran.Status = result.status;
                    tran.Comment = result.Remark;
                    tran.UserID = userId;
                    tran.RoleID = SecretarialConst.RoleID.PERFORMER;
                    tran.StatusChangeOn = DateTime.Now;
                    tran.IsActive = true;
                    tran.CreatedBy = userId;
                    tran.CreatedOn = DateTime.Now;
                    entities.BM_TaskTransaction.Add(tran);
                    entities.SaveChanges();
                    #endregion

                    var lstAssignment = (from row in entities.BM_TaskAssignment
                                         where row.TaskInstanceID == taskId && row.IsActive == true && row.StatusId == SecretarialConst.TaskStaus.PENDING_FOR_REVIEW
                                         select row).ToList();
                    if(lstAssignment != null)
                    {
                        foreach (var item in lstAssignment)
                        {
                            item.StatusId = SecretarialConst.TaskStaus.CLOSED;
                            item.UpdatedOn = DateTime.Now;
                            item.Updatedby = userId;
                            entities.SaveChanges();
                        }
                    }

                    obj.Success = true;
                    obj.Message = "Review closed successfully";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = SecretarialConst.Messages.serverError;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
            }
            return obj;
        }
        #endregion

        #region Get Performer/Reviewer
        public List<TaskAssignmentUserVM> GetUserListByTaskId(long taskId)
        {
            var result = new List<TaskAssignmentUserVM>();
            try
            {
                result = (from row in entities.BM_Task
                          join rows in entities.BM_TaskAssignment on row.ID equals rows.TaskInstanceID
                          join u in entities.Users on rows.UserID equals u.ID
                          where row.ID == taskId && row.IsActive == true && rows.IsActive == true
                          select new TaskAssignmentUserVM
                          {
                              UserId = (int)u.ID,
                              UserName = u.FirstName + " " + u.LastName,
                              EmailId = u.Email,
                              RoleId = rows.RoleId
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<TaskAssignmentUserVM> GetReviewerListByTaskAssignmentId(long taskAssignmentId)
        {
            var result = new List<TaskAssignmentUserVM>();
            try
            {
                result = (from row in entities.BM_TaskAssignment
                          join u in entities.Users on row.UserID equals u.ID
                          where row.ID == taskAssignmentId && row.IsActive == true && row.RoleId == SecretarialConst.RoleID.REVIEWER
                          select new TaskAssignmentUserVM
                          {
                              UserId = (int)u.ID,
                              UserName = u.FirstName + " " + u.LastName,
                              EmailId = u.Email,
                              RoleId = row.RoleId
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public TaskAssignmentUserVM GetPerformerByTaskAssignmentId(long taskAssignmentId)
        {
            var result = new TaskAssignmentUserVM();
            try
            {
                var taskId = (from row in entities.BM_TaskAssignment
                              where row.ID == taskAssignmentId
                              select row.TaskInstanceID).FirstOrDefault();
                if(taskId > 0)
                {
                    result = (from row in entities.BM_Task
                              join rows in entities.BM_TaskAssignment on row.ID equals rows.TaskInstanceID
                              join u in entities.Users on rows.UserID equals u.ID
                              where row.ID == taskId && row.IsActive == true && rows.IsActive == true && rows.RoleId == SecretarialConst.RoleID.PERFORMER
                              select new TaskAssignmentUserVM
                              {
                                  UserId = (int)u.ID,
                                  UserName = u.FirstName + " " + u.LastName,
                                  EmailId = u.Email,
                                  RoleId = rows.RoleId
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Review Log
        public List<ReviewTransactionVM> GetReviewLog(long taskId, int customerId)
        {
            var result = new List<ReviewTransactionVM>();
            try
            {
                result = (from row in entities.BM_Task
                          join rows in entities.BM_TaskTransaction on row.ID equals rows.TaskID                          
                          join t in entities.BM_TaskStatus on rows.StatusId equals t.Id
                          where row.ID == taskId && row.CustomerId == customerId && row.IsActive == true && rows.IsActive == true
                          orderby rows.StatusChangeOn descending
                          select new ReviewTransactionVM
                          {
                              TaskId = row.ID,
                              Id = rows.ID,
                              StatusId = rows.StatusId,
                              Status = t.Name,
                              //Remark = rows.Comment == null ? rows.Status : rows.Comment,
                              Remark = rows.Comment,
                              RoleId = rows.RoleID,
                              UpdatedOn = rows.StatusChangeOn,
                              UserId = rows.UserID,
                              UserName = (from x in entities.Users where x.ID == rows.UserID select x.FirstName + " " + x.LastName).FirstOrDefault(),
                          }).ToList();
                if(result != null)
                {
                    result = result.Where(k => !String.IsNullOrEmpty(k.Remark)).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }


        public List<TimelineEventModel> GetReviewLogs(long taskId, int customerId)
        {
            var result = new List<TimelineEventModel>();
            try
            {
                result = (from row in entities.BM_Task
                          join rows in entities.BM_TaskTransaction on row.ID equals rows.TaskID
                          join t in entities.BM_TaskStatus on rows.StatusId equals t.Id
                          where row.ID == taskId && row.CustomerId == customerId && row.IsActive == true && rows.IsActive == true
                          orderby rows.StatusChangeOn descending
                          select new TimelineEventModel
                          {
                              Title = (from x in entities.Users where x.ID == rows.UserID select x.FirstName + " " + x.LastName).FirstOrDefault(),
                              Subtitle = rows.StatusChangeOn != null ? rows.StatusChangeOn.ToString() :"",
                              Description = rows.Comment,
                              EventDate = rows.StatusChangeOn != null ? Convert.ToDateTime(rows.StatusChangeOn) : DateTime.Now,

                              //TaskId = row.ID,
                              //Id = rows.ID,
                              //StatusId = rows.StatusId,
                              //Status = t.Name,
                              ////Remark = rows.Comment == null ? rows.Status : rows.Comment,
                              //Remark = rows.Comment,
                              //RoleId = rows.RoleID,
                              //UpdatedOn = rows.StatusChangeOn,
                              //UserId = rows.UserID,
                              //UserName = (from x in entities.Users where x.ID == rows.UserID select x.FirstName + " " + x.LastName).FirstOrDefault(),
                          }).ToList();

                if (result != null)
                {
                    result = result.Where(k => !String.IsNullOrEmpty(k.Description)).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion
    }
}