﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using BM_ManegmentServices.VM;
using System.Data.Entity.SqlServer;
using System.Reflection;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text;
using System.IO;
using System.Web.Hosting;

using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using BM_ManegmentServices.Services.Setting;


namespace BM_ManegmentServices.Services.Masters
{
    public class DocumentFormat : IDocumentFormat
    {
        private Compliance_SecretarialEntities entity = new Compliance_SecretarialEntities();

        public List<BM_DocumentFormat> GetDocumentFormat(string TemplateFormate, int CustomerId)
        {
            try
            {
                if (TemplateFormate == "")
                {
                    var GetFomateDetails = (from Value in entity.BM_DocumentFormat
                                            where Value.CustomerId == CustomerId &&
                                             Value.IsActive == true
                                            select Value).ToList();


                    return GetFomateDetails;
                }
                else
                {
                    var GetFomateDetails = (from Value in entity.BM_DocumentFormat
                                            where Value.TemplateFormate == TemplateFormate && Value.CustomerId == CustomerId &&
                                             Value.IsActive == true
                                            select Value).ToList();


                    return GetFomateDetails;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public void SaveDocumentFormat(BM_DocumentFormat FormatDetails)
        {
            try
            {
                var CheckFomateDetails = (from Value in entity.BM_DocumentFormat
                                          where Value.TemplateFormate == FormatDetails.TemplateFormate && Value.CustomerId == FormatDetails.CustomerId
                                          select Value).FirstOrDefault();

                if (CheckFomateDetails == null)
                {
                    BM_DocumentFormat documentFormat = new BM_DocumentFormat();
                    documentFormat.CustomerId = FormatDetails.CustomerId;
                    documentFormat.EntityId = 0;
                    documentFormat.TemplateType = "";
                    documentFormat.TemplateFormate = FormatDetails.TemplateFormate;
                    documentFormat.FontName = FormatDetails.FontName;
                    documentFormat.FontSize = FormatDetails.FontSize;
                    documentFormat.FontColor = FormatDetails.FontColor;
                    documentFormat.IsBold = FormatDetails.IsBold;
                    documentFormat.IsItalic = FormatDetails.IsItalic;
                    documentFormat.IsUnderline = FormatDetails.IsUnderline;
                    documentFormat.IsActive = true;
                    documentFormat.CrDate = FormatDetails.CrDate;
                    documentFormat.CrBy = FormatDetails.CrBy;
                    documentFormat.ModDate = DateTime.Now;
                    documentFormat.ModBy = 0;


                    entity.BM_DocumentFormat.Add(documentFormat);
                    entity.SaveChanges();
                }
                else
                {
                    CheckFomateDetails.CustomerId = FormatDetails.CustomerId;
                    CheckFomateDetails.EntityId = 0;
                    CheckFomateDetails.TemplateType = "";
                    CheckFomateDetails.TemplateFormate = FormatDetails.TemplateFormate;
                    CheckFomateDetails.FontName = FormatDetails.FontName;
                    CheckFomateDetails.FontSize = FormatDetails.FontSize;
                    CheckFomateDetails.FontColor = FormatDetails.FontColor;
                    CheckFomateDetails.IsBold = FormatDetails.IsBold;
                    CheckFomateDetails.IsItalic = FormatDetails.IsItalic;
                    CheckFomateDetails.IsUnderline = FormatDetails.IsUnderline;
                    CheckFomateDetails.IsActive = true;

                    CheckFomateDetails.ModDate = DateTime.Now;
                    CheckFomateDetails.ModBy = FormatDetails.CrBy;

                    entity.SaveChanges();
                }

            }
            catch (Exception ex)
            {

            }
        }



    }
}