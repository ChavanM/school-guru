﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
    public interface ICountryStateCityMaster
    {
        List<VMCountry> GetAllCountryDtls();
        List<VMState> GetAllstateDtls(int countryId);
        List<VMCity> GetAllcityDtls(int stateId);
        VMCountry AddCountry(VMCountry _objcountry);

        VMCountry EditCountry(VMCountry _objcountry);
        string DeleteCountry(VMCountry _objcountry);
        VMState AddState(VMState _objstate);
        VMState EditState(VMState _objstate);
        string DeleteState(VMState _objstate);
        VMCity AddCity(VMCity _objcity);
        VMCity EditCity(VMCity _objcity);
        string DeleteCity(VMCity _objcity);
        VMCountry GetCountryById(int id);
        VMState GetSateById(int id);
        VMCity GetCityById(int id);
    }
}
