﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using System.Reflection;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Meetings;

namespace BM_ManegmentServices.Services.Masters
{

    public class NotesMaster_Service : INotesMaster_Service
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public NotesMasterVM Save(NotesMasterVM obj, int customerId, int createdBy)
        {
            try
            {
                var _obj = (from row in entities.BM_NotesMaster
                                        where row.CustomerId == customerId && row.eVoting == obj.eVoting
                                        select row).FirstOrDefault();

                if (_obj == null)
                {
                    _obj = new BM_NotesMaster();
                    _obj.NotesFormat = obj.NotesFormat;
                    _obj.eVoting = obj.eVoting;
                    _obj.IsDeleted = false;

                    _obj.CustomerId = customerId;
                    _obj.CreatedBy = createdBy;
                    _obj.CreatedOn = DateTime.Now;
                    entities.BM_NotesMaster.Add(_obj);
                    entities.SaveChanges();

                    obj.Success = true;
                    obj.Message = SecretarialConst.Messages.saveSuccess;
                }
                else
                {
                    _obj.NotesFormat = obj.NotesFormat;
                    _obj.IsDeleted = false;

                    _obj.UpdatedBy = createdBy;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();

                    obj.Success = true;
                    obj.Message = SecretarialConst.Messages.updateSuccess;
                }

            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public NotesMasterVM GetNotesMaster(NotesMasterVM obj, int customerId)
        {
            try
            {
                var NotesFormat = (from row in entities.BM_NotesMaster
                                    where row.CustomerId == customerId && row.eVoting == obj.eVoting && row.IsDeleted == false
                                    select row.NotesFormat).FirstOrDefault();

                obj.NotesFormat = NotesFormat;
                obj.Success = true;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
    }
}
