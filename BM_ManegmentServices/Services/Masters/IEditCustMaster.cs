﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IEditCustMaster
    {

        List<EditCust_VM> GetCustomer();
        bool DeleteCust(long id,int UserId);
    }
}
