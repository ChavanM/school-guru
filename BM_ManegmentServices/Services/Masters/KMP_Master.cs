﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using OfficeOpenXml;
using System.Reflection;
using BM_ManegmentServices.VM.UIForms;
using System.Globalization;

namespace BM_ManegmentServices.Services.Masters
{
    public class KMP_Master : IKMP_Master
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        #region KMP_Details
        public Director_MasterVM_KMP CreateKMP(Director_MasterVM_KMP _objVM_KMP, int customerId)
        {
            try
            {
                bool checkforEmailId = false;
                bool checkforMobileNo = false;
                bool checkforPAN = false;
                if (_objVM_KMP != null)
                {
                    checkforEmailId = (from email in entities.BM_DirectorMaster where email.IsKMP == true && email.EmailId_Personal == _objVM_KMP.EmailId select email).Any();
                    checkforMobileNo = (from mobile in entities.BM_DirectorMaster where mobile.IsKMP == true && mobile.MobileNo == _objVM_KMP.MobileNo select mobile).Any();
                    checkforPAN = (from pan in entities.BM_DirectorMaster where pan.IsKMP == true && pan.PAN == _objVM_KMP.PAN select pan).Any();

                    BM_DirectorMaster obj = new BM_DirectorMaster();
                    var check_kmpdtls = (from row in entities.BM_DirectorMaster
                                         where row.PAN == _objVM_KMP.PAN
                                         && row.EmailId_Personal == _objVM_KMP.EmailId && row.IsKMP == true
                                         && row.Customer_Id == customerId
                                         select row).FirstOrDefault();
                    if (checkforEmailId == false && checkforMobileNo == false && checkforPAN == false)
                    {
                        if (check_kmpdtls == null)
                        {

                            obj.Customer_Id = customerId;
                            obj.EmailId_Personal = _objVM_KMP.EmailId;
                            obj.Salutation = _objVM_KMP.Salutation;
                            obj.FirstName = _objVM_KMP.FirstName;
                            obj.MiddleName = _objVM_KMP.MiddleName;
                            obj.LastName = _objVM_KMP.LastName;
                            obj.Former_Name = _objVM_KMP.formerName;
                            obj.Father = _objVM_KMP.FatherName;
                            obj.Mother = _objVM_KMP.MotherName;
                            obj.Spouse = _objVM_KMP.SpouseName;
                            obj.Merital_status = _objVM_KMP.Marital_Status;
                            obj.Entity_Id = _objVM_KMP.Entity_Id;
                            obj.Register_officeAddress = _objVM_KMP.Reg_officeAddress;
                            obj.Gender = _objVM_KMP.Gender;
                            obj.DOB = _objVM_KMP.DateOfBirth;
                            obj.MobileNo = _objVM_KMP.MobileNo;
                            obj.PAN = _objVM_KMP.PAN;
                            obj.Permenant_Address_Line1 = _objVM_KMP.Permenant_Address_Line1;
                            obj.Permenant_Address_Line2 = _objVM_KMP.Permenant_Address_Line2;
                            obj.Permenant_CityId = _objVM_KMP.Permenant_CityId;
                            obj.Permenant_StateId = _objVM_KMP.Permenant_StateId;
                            obj.Permenant_PINCode = _objVM_KMP.Permenant_PINCode;
                            obj.Present_Address_Line1 = _objVM_KMP.Present_Address_Line1;
                            obj.Present_Address_Line2 = _objVM_KMP.Present_Address_Line2;
                            obj.Kmp_appointment_Date = _objVM_KMP.appointment_Date;
                            obj.Present_StateId = _objVM_KMP.Present_StateId;
                            obj.Present_CityId = _objVM_KMP.Present_CityId;
                            obj.Present_PINCode = _objVM_KMP.Present_PINCode;
                            obj.IsSameAddress = _objVM_KMP.IsSameAddress;
                            obj.Occupation = _objVM_KMP.Occupation;
                            obj.Nationality = _objVM_KMP.Nationality;

                            obj.IsKMP = true;
                            obj.Is_KMP_CS = _objVM_KMP.IsCAmember;
                            if (_objVM_KMP.IsCAmember)
                            {
                                obj.CS_MemberNo = _objVM_KMP.CS_Membersip;
                            }
                            else
                            {
                                obj.CS_MemberNo = "";
                            }
                            obj.board_ResolutionDate = _objVM_KMP.Date_BordResulaion;
                            obj.EducationalQualification = 0;
                            obj.AreaOfOccupation = 0;
                            obj.DIN = "0";
                            if (_objVM_KMP.ID > 0)
                            {
                                obj.cessation_Date = _objVM_KMP.Date_Cessation;
                                obj.Reasonfor_Cessation = _objVM_KMP.ReasonforCessation;
                            }
                            obj.Is_Deleted = false;
                            obj.IsActive = true;
                            entities.BM_DirectorMaster.Add(obj);
                            entities.SaveChanges();
                            _objVM_KMP.ID = obj.Id;
                            _objVM_KMP.Success = true;
                            _objVM_KMP.Message = "Saved successfully";

                        }
                        else
                        {
                            _objVM_KMP.ID = check_kmpdtls.Id;
                            _objVM_KMP.Error = true;
                            _objVM_KMP.Message = "Data already exist";

                        }
                    }
                    else
                    {
                        if (checkforEmailId)
                        {
                            _objVM_KMP.Error = true;
                            _objVM_KMP.Message = "Email Id already exist";
                        }
                        else if (checkforMobileNo)
                        {
                            _objVM_KMP.Error = true;
                            _objVM_KMP.Message = "Mobile number already exist";
                        }
                        else if (checkforPAN)
                        {
                            _objVM_KMP.Error = true;
                            _objVM_KMP.Message = "PAN number already exist";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _objVM_KMP.Error = true;
                _objVM_KMP.Message = "server error occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objVM_KMP;
        }

        public Director_MasterVM_KMP UpdateKMP(Director_MasterVM_KMP _obj_KMP, int customerId)
        {
            try
            {
                bool checkforEmailId = false;
                bool checkforMobileNo = false;
                bool checkforPAN = false;
                checkforEmailId = (from email in entities.BM_DirectorMaster where email.Id != _obj_KMP.ID && email.IsKMP == true && email.EmailId_Personal == _obj_KMP.EmailId select email).Any();
                checkforMobileNo = (from mobile in entities.BM_DirectorMaster where mobile.Id != _obj_KMP.ID && mobile.IsKMP == true && mobile.MobileNo == _obj_KMP.MobileNo select mobile).Any();
                checkforPAN = (from pan in entities.BM_DirectorMaster where pan.Id != _obj_KMP.ID && pan.IsKMP == true && pan.PAN == _obj_KMP.PAN select pan).Any();
                if (checkforEmailId == false && checkforMobileNo == false && checkforPAN == false)
                {
                    var _objKMPExists = (from row in entities.BM_DirectorMaster
                                         where
                                         row.Id == _obj_KMP.ID
                                         && row.Customer_Id == customerId
                                         && row.IsKMP == true
                                         select row).FirstOrDefault();

                    if (_objKMPExists != null)
                    {
                        _objKMPExists.Salutation = _obj_KMP.Salutation;
                        _objKMPExists.EmailId_Personal = _obj_KMP.EmailId;
                        _objKMPExists.Former_Name = _obj_KMP.formerName;
                        _objKMPExists.FirstName = _obj_KMP.FirstName;
                        _objKMPExists.MiddleName = _obj_KMP.MiddleName;
                        _objKMPExists.LastName = _obj_KMP.LastName;
                        _objKMPExists.Father = _obj_KMP.FatherName;
                        _objKMPExists.Mother = _obj_KMP.MotherName;
                        _objKMPExists.Gender = _obj_KMP.Gender;
                        _objKMPExists.PAN = _obj_KMP.PAN;
                        _objKMPExists.Spouse = _obj_KMP.SpouseName;
                        _objKMPExists.MobileNo = _obj_KMP.MobileNo;
                        _objKMPExists.DOB = _obj_KMP.DateOfBirth;
                        _objKMPExists.Permenant_Address_Line1 = _obj_KMP.Permenant_Address_Line1;
                        _objKMPExists.Permenant_Address_Line2 = _obj_KMP.Permenant_Address_Line2;
                        _objKMPExists.Permenant_CityId = _obj_KMP.Permenant_CityId;
                        _objKMPExists.Permenant_StateId = _obj_KMP.Permenant_StateId;
                        _objKMPExists.Permenant_PINCode = _obj_KMP.Permenant_PINCode;
                        _objKMPExists.Present_Address_Line1 = _obj_KMP.Present_Address_Line1;
                        _objKMPExists.Present_Address_Line2 = _obj_KMP.Present_Address_Line2;
                        _objKMPExists.Present_StateId = _obj_KMP.Present_StateId;
                        _objKMPExists.Present_CityId = _obj_KMP.Present_CityId;
                        _objKMPExists.Present_PINCode = _obj_KMP.Present_PINCode;
                        _objKMPExists.IsSameAddress = _obj_KMP.IsSameAddress;
                        _objKMPExists.Occupation = _obj_KMP.Occupation;
                        _objKMPExists.Nationality = _obj_KMP.Nationality;
                        _objKMPExists.cessation_Date = _obj_KMP.Date_Cessation;
                        _objKMPExists.Reasonfor_Cessation = _obj_KMP.ReasonforCessation;
                        _objKMPExists.Is_KMP_CS = _obj_KMP.IsCAmember;
                        if ((bool)_objKMPExists.Is_KMP_CS)
                        {
                            _objKMPExists.CS_MemberNo = _obj_KMP.CS_Membersip;
                        }
                        else
                        {
                            _objKMPExists.CS_MemberNo = "";
                        }
                        _objKMPExists.board_ResolutionDate = _obj_KMP.Date_BordResulaion;
                        _objKMPExists.Kmp_appointment_Date = _objKMPExists.Kmp_appointment_Date;
                        _objKMPExists.IsActive = true;
                        _objKMPExists.Is_Deleted = false;
                        entities.SaveChanges();
                        _obj_KMP.Success = true;
                        _obj_KMP.Message = "Updated successfully.";

                    }
                    else
                    {
                        _obj_KMP.Error = true;
                        _obj_KMP.Message = "Something went wrong";

                    }
                }
                else
                {
                    if (checkforEmailId)
                    {
                        _obj_KMP.Error = true;
                        _obj_KMP.Message = "Email Id already exist";
                    }
                    else if (checkforMobileNo)
                    {
                        _obj_KMP.Error = true;
                        _obj_KMP.Message = "Mobile number already exist";
                    }
                    else if (checkforPAN)
                    {
                        _obj_KMP.Error = true;
                        _obj_KMP.Message = "PAN number already exist";
                    }
                }
            }
            catch (Exception ex)
            {
                _obj_KMP.Error = true;
                _obj_KMP.Message = "Server error occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _obj_KMP;
        }

        public Director_MasterVM_KMP DeleteKMP(Director_MasterVM_KMP _obj_KMP, int coustomerId)
        {
            try
            {
                var _objdatafor_Delete = (from row in entities.BM_DirectorMaster
                                          where
                                          row.Id == _obj_KMP.ID &&
                                          row.PAN == _obj_KMP.PAN
                                          select row
                                          ).FirstOrDefault();
                if (_objdatafor_Delete != null)
                {
                    _objdatafor_Delete.Is_Deleted = true;
                    _obj_KMP.Success = true;
                    _obj_KMP.Message = "Record Deleted Successfully";
                    return _obj_KMP;
                }
                else
                {
                    _obj_KMP.Error = true;
                    _obj_KMP.Message = "Something went wrong";
                    return _obj_KMP;
                }
            }
            catch (Exception ex)
            {

                _obj_KMP.Error = true;
                _obj_KMP.Message = "Server error occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _obj_KMP;
            }
        }

        public List<Director_MasterVM_KMP> GetKMP_Dtls(int customerId)
        {
            try
            {
                var _objKMPdtls = (from row in entities.BM_DirectorMaster
                                   where row.IsKMP == true
                                   && row.Is_Deleted == false
                                   && row.Customer_Id == customerId
                                   select new Director_MasterVM_KMP
                                   {
                                       ID = row.Id,
                                       FirstName = row.FirstName + " " + row.LastName,
                                       PAN = row.PAN,
                                       DateOfBirth = row.DOB
                                   }).ToList();
                return _objKMPdtls;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public Director_MasterVM_KMP GetKMP_Dtls(int id, int customerId)
        {
            try
            {
                var _objgetKMPdtls = (from row in entities.BM_DirectorMaster
                                      where row.Id == id && row.IsKMP == true
                                      && row.Is_Deleted == false
                                      select new Director_MasterVM_KMP
                                      {
                                          ID = row.Id,
                                          EmailId = row.EmailId_Personal,
                                          appointment_Date = row.Kmp_appointment_Date,
                                          Salutation = row.Salutation,
                                          formerName = row.Former_Name,
                                          FirstName = row.FirstName,
                                          MiddleName = row.MiddleName,
                                          LastName = row.LastName,
                                          DateOfBirth = row.DOB,
                                          Gender = row.Gender,
                                          FatherName = row.Father,
                                          MotherName = row.Mother,
                                          SpouseName = row.Spouse,
                                          MobileNo = row.MobileNo,
                                          Entity_Id = (int)row.Entity_Id,
                                          Reg_officeAddress = row.Register_officeAddress,
                                          Marital_Status = row.Merital_status,
                                          PAN = row.PAN,
                                          Permenant_Address_Line1 = row.Permenant_Address_Line1,
                                          Permenant_Address_Line2 = row.Permenant_Address_Line2,
                                          Permenant_StateId = row.Permenant_StateId,
                                          Permenant_CityId = row.Permenant_CityId,
                                          Permenant_PINCode = row.Permenant_PINCode,
                                          Present_Address_Line1 = row.Present_Address_Line1,
                                          Present_Address_Line2 = row.Present_Address_Line2,
                                          Present_StateId = row.Present_StateId,
                                          Present_CityId = row.Present_CityId,
                                          Present_PINCode = row.Present_PINCode,
                                          IsSameAddress = row.IsSameAddress,
                                          Nationality = row.Nationality,
                                          Occupation = row.Occupation,
                                          Date_BordResulaion = (DateTime)row.board_ResolutionDate,
                                          Date_Cessation = row.cessation_Date,
                                          ReasonforCessation = row.Reasonfor_Cessation,
                                          IsCAmember = (bool)row.Is_KMP_CS,
                                          CS_Membersip = row.CS_MemberNo
                                      }).FirstOrDefault();
                return _objgetKMPdtls;
            }
            catch (Exception ex)
            {
                Director_MasterVM_KMP obj = new Director_MasterVM_KMP();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }


        #endregion KMP_Details

        #region KMP_CompanySecurity
        public Director_Master_KMPCompanySecurity CreateKMP_CompanySecurity(Director_Master_KMPCompanySecurity _obj_KMP, int customerId)
        {
            _obj_KMP.Response = new Response();
            try
            {
                if (_obj_KMP != null)
                {
                    var check_kmpdtls = (from row in entities.BM_SecurityDetail
                                         where row.Director_KmpId == _obj_KMP.KMP_Id && row.CompanyName == _obj_KMP.CompanyName
                                         && row.IsDeleted == false
                                         select row).FirstOrDefault();

                    if (check_kmpdtls == null)
                    {
                        BM_SecurityDetail obj = new BM_SecurityDetail();
                        obj.CustomerId = customerId;
                        obj.Director_KmpId = (int)_obj_KMP.KMP_Id;
                        obj.CompanyName = _obj_KMP.CompanyName;
                        obj.securities_No = _obj_KMP.securities_No;
                        obj.Dis_of_securities = _obj_KMP.Dis_of_securities;
                        obj.Nominam_valSecurities = _obj_KMP.Nominam_valSecurities;
                        obj.acquisition_Date = _obj_KMP.acquisition_Date;
                        obj.acquisition_pricePaid = _obj_KMP.acquisition_pricePaid;
                        obj.Other_ConsiPaid_acquisition = _obj_KMP.Other_ConsiPaid_acquisition;
                        obj.Date_disposal = _obj_KMP.Date_disposal;
                        obj.Price_Received_disposal = _obj_KMP.Price_Received_disposal;
                        obj.otherconsideration_disposal_RescPrice = _obj_KMP.otherconsideration_disposal_RescPrice;
                        obj.Cbal_No_securities_afterTransaction = _obj_KMP.Cbal_No_securities_afterTransaction;
                        obj.Mode_of_acquisitionSecurities = _obj_KMP.Mode_of_acquisitionSecurities;
                        obj.DematerializedorPhysical = _obj_KMP.Isdematerialized;
                        obj.Securities_encumbrance = _obj_KMP.Securities_encumbrance;

                        obj.IsDeleted = false;
                        //obj.Createdby=
                        obj.Createdon = DateTime.Now;
                        entities.BM_SecurityDetail.Add(obj);
                        entities.SaveChanges();
                        
                        _obj_KMP.Response.Success = true;
                        _obj_KMP.Response.Message = "Security Details Saved Successfully";
                        return _obj_KMP;
                    }
                    else
                    {
                        _obj_KMP.Response.Error = true;
                        _obj_KMP.Response.Message = "Security details already exist";
                        return _obj_KMP;
                    }
                }
                else
                {
                    _obj_KMP.Response.Error = true;
                    _obj_KMP.Response.Message = "Something went wrong";
                    return _obj_KMP;
                }
            }
            catch (Exception ex)
            {
                _obj_KMP.Response.Error = true;
                _obj_KMP.Response.Message = "Server error occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _obj_KMP;
            }
        }

        public Director_Master_KMPCompanySecurity UpdateKMP_CompanySecurity(Director_Master_KMPCompanySecurity _obj_KMP, int coustomerId)
        {
            _obj_KMP.Response = new Response();
            try
            {
                var _objKMPExists = (from row in entities.BM_SecurityDetail
                                     where row.IsDeleted == false && row.Id == _obj_KMP.Id_sec
                                     && row.Director_KmpId == _obj_KMP.KMP_Id
                                     select row).FirstOrDefault();

                if (_objKMPExists != null)
                {
                    _objKMPExists.CompanyName = _obj_KMP.CompanyName;
                    _objKMPExists.securities_No = _obj_KMP.securities_No;
                    _objKMPExists.Dis_of_securities = _obj_KMP.Dis_of_securities;
                    _objKMPExists.Nominam_valSecurities = _obj_KMP.Nominam_valSecurities;
                    _objKMPExists.acquisition_Date = _obj_KMP.acquisition_Date;
                    _objKMPExists.acquisition_pricePaid = _obj_KMP.acquisition_pricePaid;
                    _objKMPExists.Other_ConsiPaid_acquisition = _obj_KMP.Other_ConsiPaid_acquisition;
                    _objKMPExists.Date_disposal = _obj_KMP.Date_disposal;
                    _objKMPExists.Price_Received_disposal = _obj_KMP.Price_Received_disposal;
                    _objKMPExists.otherconsideration_disposal_RescPrice = _obj_KMP.otherconsideration_disposal_RescPrice;
                    _objKMPExists.Cbal_No_securities_afterTransaction = _obj_KMP.Cbal_No_securities_afterTransaction;
                    _objKMPExists.Mode_of_acquisitionSecurities = _obj_KMP.Mode_of_acquisitionSecurities;
                    _objKMPExists.DematerializedorPhysical = _obj_KMP.Isdematerialized;
                    _objKMPExists.Securities_encumbrance = _obj_KMP.Securities_encumbrance;
                    _objKMPExists.IsDeleted = false;
                    entities.SaveChanges();
                    _obj_KMP.Response.Success = true;
                    _obj_KMP.Response.Message = "Security Details Updated Successfully";
                    return _obj_KMP;
                }
                else
                {
                    _obj_KMP.Response.Error = true;
                    _obj_KMP.Response.Message = "Something went wrong";
                    return _obj_KMP;
                }
            }
            catch (Exception ex)
            {
                _obj_KMP.Response.Error = true;
                _obj_KMP.Response.Message = "Server error occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _obj_KMP;
            }
        }

        public Director_Master_KMPCompanySecurity DeleteKMPdtls(Director_Master_KMPCompanySecurity _obj_KMP, int coustomerId)
        {
            _obj_KMP.Response = new Response();
            try
            {
                var _objdatafor_Delete = (from row in entities.BM_SecurityDetail
                                          where row.IsDeleted == false
                                          && row.Id == _obj_KMP.Id_sec
                                          && row.Director_KmpId == _obj_KMP.KMP_Id
                                          select row
                                          ).FirstOrDefault();
                if (_objdatafor_Delete != null)
                {
                    _objdatafor_Delete.IsDeleted = true;
                    _obj_KMP.Response.Success = true;
                    _obj_KMP.Response.Message = "Record Deleted Successfully";
                    return _obj_KMP;
                }
                else
                {
                    _obj_KMP.Response.Error = true;
                    _obj_KMP.Response.Message = "Something wents wrong";
                    return _obj_KMP;
                }
            }
            catch (Exception ex)
            {
                _obj_KMP.Response.Error = true;
                _obj_KMP.Response.Message = "server error occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _obj_KMP;
            }
        }
        
        public Director_Master_KMPCompanySecurity GetKMPCompanyDtls(int id, int customerId)
        {
            try
            {
                var _objGetCompanySecuritydtls = (from row in entities.BM_SecurityDetail
                                                  where row.Director_KmpId == id && row.IsDeleted == false
                                                  select new Director_Master_KMPCompanySecurity
                                                  {
                                                      Id_sec = row.Id,
                                                      CompanyName = row.CompanyName,
                                                      securities_No = (long)row.securities_No,
                                                      Dis_of_securities = row.Dis_of_securities,
                                                      Nominam_valSecurities = row.Nominam_valSecurities,
                                                      acquisition_Date = (DateTime)row.acquisition_Date,
                                                      acquisition_pricePaid = row.acquisition_pricePaid,
                                                      Other_ConsiPaid_acquisition = row.Other_ConsiPaid_acquisition,
                                                      Date_disposal = (DateTime)row.Date_disposal,
                                                      Price_Received_disposal = row.Price_Received_disposal,
                                                      otherconsideration_disposal_RescPrice = row.otherconsideration_disposal_RescPrice,
                                                      Cbal_No_securities_afterTransaction = row.Cbal_No_securities_afterTransaction,
                                                      Mode_of_acquisitionSecurities = row.Mode_of_acquisitionSecurities,
                                                      Isdematerialized = row.DematerializedorPhysical,
                                                      Securities_encumbrance = row.Securities_encumbrance,
                                                  }).FirstOrDefault();
                return _objGetCompanySecuritydtls;
            }
            catch (Exception ex)
            {
                Director_Master_KMPCompanySecurity obj = new Director_Master_KMPCompanySecurity();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public bool DeleteKMPInfo(int id, int customerId)
        {
            try
            {
                var _objdatafor_Delete = (from row in entities.BM_DirectorMaster
                                          where
                                          // row.Is_Deleted == false && row.IsActive == true
                                          row.Id == id &&
                                          row.Customer_Id == customerId
                                          select row
                                          ).FirstOrDefault();
                if (_objdatafor_Delete != null)
                {
                    _objdatafor_Delete.Is_Deleted = true;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public void KMP_SecuritiesDetails(ExcelPackage xlWorkbook, int id, string current_Date)
        {

        }

        public bool CreateKMP_SecurityDetails(BM_SecurityDetail obj_Kmpsecurity)
        {
            bool succces = false;
            try
            {
                if (obj_Kmpsecurity != null)
                {
                    var checkKMP_Security = (from row in entities.BM_SecurityDetail
                                             where row.Director_KmpId == obj_Kmpsecurity.Director_KmpId
&& row.CompanyName == obj_Kmpsecurity.CompanyName
&& row.Mode_of_acquisitionSecurities == obj_Kmpsecurity.Mode_of_acquisitionSecurities
&& row.acquisition_Date == obj_Kmpsecurity.acquisition_Date
                                             select row).FirstOrDefault();
                    if (checkKMP_Security == null)
                    {
                        entities.BM_SecurityDetail.Add(obj_Kmpsecurity);
                        entities.SaveChanges();
                        succces = true;
                    }
                    else
                    {
                        succces = false;
                    }
                }
                return succces;
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public List<BM_SecurityDetail> GetKMPseurityDtails(int customerId, long KMPId)
        {
            try
            {
                var _objKMPsecurities = (from row in entities.BM_SecurityDetail
                                         where
                                         row.Director_KmpId == KMPId
                                         && row.CustomerId == customerId
                                         && row.IsDeleted == false
                                         select row).ToList();
                return _objKMPsecurities;
            }
            catch(Exception ex)
            {
                List<BM_SecurityDetail> obj = new List<BM_SecurityDetail>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public Director_Master_KMPCompanySecurity GetsecuritybyId(long kmpID, long securityId)
        {
            try
            {
                var getSecurityID = (from row in entities.BM_SecurityDetail
                                     where row.Id == securityId
&& row.Director_KmpId == kmpID
                                     select new Director_Master_KMPCompanySecurity
                                     {
                                         Id_sec=row.Id,
                                         KMP_Id=row.Director_KmpId,
                                         CompanyName=row.CompanyName,
                                         securities_No=row.securities_No,
                                         Dis_of_securities=row.Dis_of_securities,
                                         Nominam_valSecurities=row.Nominam_valSecurities,
                                         acquisition_Date=row.acquisition_Date,
                                         acquisition_pricePaid=row.acquisition_pricePaid,
                                         Other_ConsiPaid_acquisition=row.Other_ConsiPaid_acquisition,
                                         Date_disposal=row.Date_disposal,
                                         Price_Received_disposal=row.Price_Received_disposal,
                                         otherconsideration_disposal_RescPrice=row.otherconsideration_disposal_RescPrice,
                                         Mode_of_acquisitionSecurities=row.Mode_of_acquisitionSecurities,
                                         Cbal_No_securities_afterTransaction=row.Cbal_No_securities_afterTransaction,
                                         Isdematerialized=row.DematerializedorPhysical,
                                         Securities_encumbrance=row.Securities_encumbrance,
                                     }).FirstOrDefault();
                return getSecurityID;
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                 Director_Master_KMPCompanySecurity obj= new Director_Master_KMPCompanySecurity();
                return obj;
            }
        }

        #endregion

        #region UI Form

        public List<Director_MasterVM_KMP> GetKMPByDesignation(int designationId, long? entityId, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_DirectorMaster
                                   where row.IsKMP == true
                                   && row.Is_Deleted == false
                                   && row.Entity_Id == entityId && row.DesignationId == designationId
                                   && row.Customer_Id == customerId
                                   select new Director_MasterVM_KMP
                                   {
                                       ID = row.Id,
                                       FirstName = row.FirstName + " " + row.LastName,
                                       PAN = row.PAN,
                                       DateOfBirth = row.DOB
                                   }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public UIForm_KMPMasterVM CreateUpdateKMP(UIForm_KMPMasterVM obj, int userId, int customerId)
        {
            try
            {
                if(obj.RefMasterID > 0)
                {
                    var _obj = (from row in entities.BM_DirectorMaster
                                where row.Id == obj.RefMasterID && row.IsActive == false && row.Is_Deleted == false && row.Customer_Id == customerId
                                select row).FirstOrDefault();
                    if(_obj != null)
                    {
                        _obj.Salutation = obj.Salutation;
                        _obj.FirstName = obj.FirstName;
                        _obj.MiddleName = obj.MiddleName;
                        _obj.LastName = obj.LastName;

                        _obj.FSalutations = obj.FSalutations;
                        _obj.Father = obj.Father;
                        _obj.FatherMiddleName = obj.FatherMiddleName;
                        _obj.FatherLastName = obj.FatherLastName;

                        _obj.Present_Address_Line1 = obj.Present_Address_Line1;
                        _obj.Present_Address_Line2 = obj.Present_Address_Line2;
                        _obj.Present_StateId = obj.Present_StateId;
                        _obj.Present_CityId = obj.Present_CityId;
                        _obj.Present_PINCode = obj.Present_PINCode;

                        _obj.PAN = obj.PAN;

                        DateTime Kmp_appointment_Date;
                        DateTime Kmp_appointment_Date_To;

                        if (!string.IsNullOrEmpty(obj.Kmp_appointment_Date))
                        {
                            if (!DateTime.TryParseExact(obj.Kmp_appointment_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Kmp_appointment_Date))
                            {
                                _obj.Kmp_appointment_Date = null;
                                obj.Kmp_appointment_Date = "";
                            }
                            else
                            {
                                _obj.Kmp_appointment_Date = Kmp_appointment_Date;
                            }
                        }

                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        if(obj.DesignationId == 2)
                        {
                            if (!string.IsNullOrEmpty(obj.Kmp_appointment_Date_To))
                            {
                                if (!DateTime.TryParseExact(obj.Kmp_appointment_Date_To, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Kmp_appointment_Date_To))
                                {
                                    //_obj.Kmp_appointment_Date_To = null;
                                    obj.Kmp_appointment_Date_To = "";
                                }
                                else
                                {
                                    //_obj.Kmp_appointment_Date_To = Kmp_appointment_Date_To;
                                }
                            }
                        }

                        if(obj.DesignationId == 3)
                        {
                            _obj.CS_MemberNo = obj.CS_MemberNo;
                        }

                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();

                        obj.Success = true;
                    }
                }
                else
                {
                    var _obj = new BM_DirectorMaster();
                    _obj.Salutation = obj.Salutation;
                    _obj.FirstName = obj.FirstName;
                    _obj.MiddleName = obj.MiddleName;
                    _obj.LastName = obj.LastName;

                    _obj.FSalutations = obj.FSalutations;
                    _obj.Father = obj.Father;
                    _obj.FatherMiddleName = obj.FatherMiddleName;
                    _obj.FatherLastName = obj.FatherLastName;

                    _obj.Present_Address_Line1 = obj.Present_Address_Line1;
                    _obj.Present_Address_Line2 = obj.Present_Address_Line2;
                    _obj.Present_StateId = obj.Present_StateId;
                    _obj.Present_CityId = obj.Present_CityId;
                    _obj.Present_PINCode = obj.Present_PINCode;

                    _obj.PAN = obj.PAN;

                    DateTime Kmp_appointment_Date;
                    DateTime Kmp_appointment_Date_To;

                    if (!string.IsNullOrEmpty(obj.Kmp_appointment_Date))
                    {
                        if (!DateTime.TryParseExact(obj.Kmp_appointment_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Kmp_appointment_Date))
                        {
                            _obj.Kmp_appointment_Date = null;
                            obj.Kmp_appointment_Date = "";
                        }
                        else
                        {
                            _obj.Kmp_appointment_Date = Kmp_appointment_Date;
                        }
                    }
                    _obj.DesignationId = obj.DesignationId;

                    if (obj.DesignationId == 2)
                    {
                        if (!string.IsNullOrEmpty(obj.Kmp_appointment_Date_To))
                        {
                            if (!DateTime.TryParseExact(obj.Kmp_appointment_Date_To, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Kmp_appointment_Date_To))
                            {
                                //_obj.Kmp_appointment_Date_To = null;
                                obj.Kmp_appointment_Date_To = "";
                            }
                            else
                            {
                                //_obj.Kmp_appointment_Date_To = Kmp_appointment_Date_To;
                            }
                        }
                    }

                    if (obj.DesignationId == 3)
                    {
                        _obj.CS_MemberNo = obj.CS_MemberNo;
                        _obj.Is_KMP_CS = true;
                    }

                    #region Default values for Master
                    _obj.DOB = new DateTime(1900, 1, 1);
                    _obj.board_ResolutionDate = new DateTime(1900, 1, 1);
                    _obj.Entity_Id = 0;
                    _obj.Gender = 0;
                    _obj.EducationalQualification = 0;
                    _obj.Occupation = -1;
                    _obj.AreaOfOccupation = -1;
                    _obj.ResidentInIndia = true;
                    _obj.Nationality = 1;
                    _obj.IsSameAddress = true;
                    _obj.Permenant_StateId = 0;
                    _obj.Permenant_CityId = 0;

                    _obj.Present_StateId = 0;
                    _obj.Present_CityId = 0;
                    _obj.IsPositionHeld = false;
                    _obj.Customer_Id = customerId;
                    _obj.IsKMP = true;
                    _obj.Is_KMP_CS = false;

                    _obj.IsActive = false;
                    _obj.Is_Deleted = false;

                    _obj.CreatedBy = userId;
                    _obj.CreatedOn = DateTime.Now;
                    #endregion

                    entities.BM_DirectorMaster.Add(_obj);
                    entities.SaveChanges();

                    obj.RefMasterID = _obj.Id;

                    var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MappingID && k.IsDeleted == false).FirstOrDefault();
                    if(mapping != null)
                    {
                        mapping.RefMasterID = _obj.Id;
                        entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }

                    obj.Success = true;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }


        public UIForm_KMPMasterCessationVM ResignationOfKMP(UIForm_KMPMasterCessationVM obj, int userId, int customerId)
        {
            try
            {
                if (obj.RefMasterID > 0)
                {
                    var _obj = (from row in entities.BM_DirectorMaster
                                where row.Id == obj.RefMasterID && row.IsActive == true && row.Is_Deleted == false && row.Customer_Id == customerId
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {
                        DateTime Kmp_cessation_Date;

                        if (!string.IsNullOrEmpty(obj.cessation_Date))
                        {
                            if (!DateTime.TryParseExact(obj.cessation_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Kmp_cessation_Date))
                            {
                                _obj.cessation_Date = null;
                            }
                            else
                            {
                                _obj.cessation_Date = Kmp_cessation_Date;
                                //_obj.IsActive = false;
                            }
                        }
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion
    }
}