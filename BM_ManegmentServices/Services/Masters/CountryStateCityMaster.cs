﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class CountryStateCityMaster : ICountryStateCityMaster
    {
        ComplianceDBEntities entities = new ComplianceDBEntities();

        #region Contry
        public List<VMCountry> GetAllCountryDtls()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Listofcountry = (from row in entities.Countries.OrderBy(x => x.Name)
                                         where row.IsDeleted == false
                                         select new VMCountry
                                         {
                                             CountryId = row.ID,
                                             CountryName = row.Name
                                         }).ToList();
                    return Listofcountry;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public VMCountry AddCountry(VMCountry _objcountry)
        {
            try
            {
                var Checkforcounrty = (from row in entities.Countries
                                       where row.Name == _objcountry.CountryName
                                       && row.IsDeleted == false
                                       select row).FirstOrDefault();
                if (Checkforcounrty == null)
                {
                    Country obj = new Country();
                    obj.Name = _objcountry.CountryName;
                    obj.IsDeleted = false;
                    entities.Countries.Add(obj);
                    entities.SaveChanges();
                    _objcountry.success = true;
                    _objcountry.Message = "Save Country Successfully";
                    return _objcountry;

                }
                else
                {
                    _objcountry.error = true;
                    _objcountry.Message = "Somthing wents wrong";
                    return _objcountry;
                }
            }
            catch (Exception ex)
            {
                _objcountry.error = true;
                _objcountry.Message = "Server error occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objcountry;
            }
        }

        public VMCountry EditCountry(VMCountry _objcountry)
        {
            try
            {
                var Checkforcounrty = (from row in entities.Countries
                                       where row.IsDeleted == false
                                       && row.ID == _objcountry.CountryId
                                       select row).FirstOrDefault();
                if (Checkforcounrty != null)
                {

                    Checkforcounrty.Name = _objcountry.CountryName;
                    Checkforcounrty.IsDeleted = false;

                    entities.SaveChanges();
                    _objcountry.success = true;
                    _objcountry.Message = "Country Update successfully";
                    return _objcountry;
                }
                else
                {
                    _objcountry.success = true;
                    _objcountry.Message = "Country Name not found";
                    return _objcountry;

                }
            }
            catch (Exception ex)
            {
                _objcountry.success = true;
                _objcountry.Message = "Server error occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objcountry;

            }
        }

        public string DeleteCountry(VMCountry _objcountry)
        {
            try
            {
                var Checkforcounrty = (from row in entities.Countries
                                       where row.Name == _objcountry.CountryName
                                       && row.IsDeleted == false
                                       && row.ID == _objcountry.CountryId
                                       select row).FirstOrDefault();
                if (Checkforcounrty != null)
                {

                    Checkforcounrty.IsDeleted = true;

                    entities.SaveChanges();
                    return "Country Deleted successfully";
                }
                else
                {
                    return "Country Name not found";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server error occured";
            }
        }


        #endregion Counrty

        #region state
        public List<VMState> GetAllstateDtls(int countryID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Listofstate = (from row in entities.States.OrderBy(x => x.Name)
                                       where row.IsDeleted == false
                                       && row.CountryID == countryID
                                       select new VMState
                                       {
                                           SIdPublic = row.ID,
                                           Name = row.Name,
                                           countryId = (int)row.CountryID
                                       }).ToList();



                    return Listofstate;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMState AddState(VMState _objstate)
        {
            try
            {
                var Checkforstate = (from row in entities.States
                                     where row.Name == _objstate.Name
                                     && row.CountryID == _objstate.countryId
                                     && row.IsDeleted == false
                                     select row).FirstOrDefault();
                if (Checkforstate == null)
                {
                    State obj = new State();
                    obj.Name = _objstate.Name;
                    obj.CountryID = _objstate.countryId;
                    obj.IsDeleted = false;
                    entities.States.Add(obj);
                    entities.SaveChanges();
                    _objstate.success = true;
                    _objstate.Message = "State Save successfully";
                    return _objstate;
                }
                else
                {
                    _objstate.error = true;
                    _objstate.Message = "State Name allredy exist";
                    return _objstate;

                }
            }
            catch (Exception ex)
            {
                _objstate.error = true;
                _objstate.Message = "Server error occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objstate;
            }
        }

        public VMState EditState(VMState _objstate)
        {
            try
            {
                var Checkforstate = (from row in entities.States
                                     where
                                     row.ID == _objstate.SIdPublic
                                     && row.CountryID == _objstate.countryId
                                     && row.IsDeleted == false
                                     select row).FirstOrDefault();
                if (Checkforstate != null)
                {
                    Checkforstate.ID = _objstate.SIdPublic;
                    Checkforstate.Name = _objstate.Name;
                    Checkforstate.CountryID = _objstate.countryId;
                    Checkforstate.IsDeleted = false;

                    entities.SaveChanges();
                    _objstate.success = true;
                    _objstate.Message = "State Update successfully";
                    return _objstate;
                }
                else
                {
                    _objstate.error = true;
                    _objstate.Message = "Somthing wents wrong";
                    return _objstate;
                }
            }
            catch (Exception ex)
            {
                _objstate.error = true;
                _objstate.Message = "Server error occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objstate;
            }
        }

        public string DeleteState(VMState _objstate)
        {
            try
            {
                var Checkforstate = (from row in entities.States
                                     where row.Name == _objstate.Name
                                     && row.CountryID == _objstate.countryId
                                     && row.IsDeleted == false
                                     select row).FirstOrDefault();
                if (Checkforstate != null)
                {
                    Checkforstate.IsDeleted = true;

                    entities.SaveChanges();
                    return "State Deleted successfully";
                }
                else
                {
                    return "State Name does not exist";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server error occured";
            }
        }
        #endregion state

        #region city
        public List<VMCity> GetAllcityDtls(int stateId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Listofcity = (from row in entities.Cities.OrderBy(x => x.Name)
                                      join rows in entities.States on row.StateId equals rows.ID
                                      where row.IsDeleted == false
                                      && row.StateId == stateId
                                      select new VMCity
                                      {
                                          Reg_cityId = row.ID,
                                          Name = row.Name,
                                          stateId = row.StateId,
                                          stateName = rows.Name
                                      }).ToList();
                    return Listofcity;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }


        public VMCity AddCity(VMCity _objcity)
        {
            try
            {
                var Checkforcity = (from row in entities.Cities
                                    where row.Name == _objcity.Name
                                    && row.StateId == _objcity.stateId
                                    && row.IsDeleted == false
                                    select row).FirstOrDefault();
                if (Checkforcity == null)
                {
                    City obj = new City();
                    obj.Name = _objcity.Name;
                    obj.StateId = _objcity.stateId;
                    obj.IsDeleted = false;
                    entities.Cities.Add(obj);
                    entities.SaveChanges();
                    _objcity.success = true;
                    _objcity.Message= "City Save successfully";
                    return _objcity;
                }
                else
                {
                  
                    _objcity.success = true;
                    _objcity.Message = "City Name allredy exist";
                    return _objcity;
                }
            }
            catch (Exception ex)
            {
                _objcity.success = true;
                _objcity.Message = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objcity;
            }
        }

        public VMCity EditCity(VMCity _objcity)
        {
            try
            {
                var Checkforcity = (from row in entities.Cities
                                    where row.Name == _objcity.Name
                                    && row.StateId == _objcity.stateId
                                    && row.IsDeleted == false
                                    && row.ID == _objcity.Reg_cityId
                                    select row).FirstOrDefault();
                if (Checkforcity != null)
                {

                    Checkforcity.Name = _objcity.Name;
                    Checkforcity.StateId = _objcity.stateId;
                    Checkforcity.IsDeleted = false;

                    entities.SaveChanges();
                    _objcity.success = true;
                    _objcity.Message= "City Update successfully";
                    return _objcity;
                }
                else
                {
                    _objcity.error = true;
                    _objcity.Message = "City Name allready exist";
                    return _objcity;
                }
            }
            catch (Exception ex)
            {
                _objcity.error = true;
                _objcity.Message = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objcity;
            }
        }

        public string DeleteCity(VMCity _objcity)
        {
            try
            {
                var Checkforcity = (from row in entities.Cities
                                    where row.Name == _objcity.Name
                                    && row.StateId == _objcity.stateId
                                    && row.IsDeleted == false
                                    && row.ID == _objcity.Reg_cityId
                                    select row).FirstOrDefault();
                if (Checkforcity != null)
                {

                    Checkforcity.IsDeleted = false;

                    entities.SaveChanges();
                    return "City Delete successfully";
                }
                else
                {
                    return "City Name does not exist";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server error occured";
            }
        }

        public VMCountry GetCountryById(int id)
        {
            VMCountry vmcountry = new VMCountry();
            try
            {

                var obj = (from row in entities.Countries
                           where row.ID == id
                           && row.IsDeleted == false
                           select row).FirstOrDefault();
                if (obj != null)
                {
                    vmcountry.CountryId = obj.ID;
                    vmcountry.CountryName = obj.Name;

                }
                else
                {
                    vmcountry.error = true;
                    vmcountry.Message = "Data not found";
                }
                return vmcountry;
            }
            catch (Exception ex)
            {
                vmcountry.error = true;
                vmcountry.Message = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return vmcountry;
            }
        }

        public VMState GetSateById(int id)
        {
            VMState vmstate = new VMState();
            try
            {

                var obj = (from row in entities.States
                           where row.ID == id
                           && row.IsDeleted == false
                           select row).FirstOrDefault();
                if (obj != null)
                {
                    vmstate.SIdPublic = obj.ID;
                    vmstate.countryId = (int)obj.CountryID;
                    vmstate.Name = obj.Name;

                }
                else
                {
                    vmstate.error = true;
                    vmstate.Message = "Data not found";
                }
                return vmstate;
            }
            catch (Exception ex)
            {
                vmstate.error = true;
                vmstate.Message = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return vmstate;
            }
        }

        public VMCity GetCityById(int id)
        {
            VMCity vmcity = new VMCity();
            try
            {

                var obj = (from row in entities.Cities
                           join rows in entities.States on row.StateId equals rows.ID
                           where row.ID == id
                           && row.IsDeleted == false
                           select new { row, rows }).FirstOrDefault();
                if (obj != null)
                {
                    vmcity.Reg_cityId = obj.row.ID;
                    vmcity.countryId = (int)obj.rows.CountryID;
                    vmcity.stateId = obj.row.StateId;
                    vmcity.Name = obj.row.Name;

                }
                else
                {
                    vmcity.error = true;
                    vmcity.Message = "Data not found";
                }
                return vmcity;
            }
            catch (Exception ex)
            {
                vmcity.error = true;
                vmcity.Message = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return vmcity;
            }
        }
        #endregion city

    }
}