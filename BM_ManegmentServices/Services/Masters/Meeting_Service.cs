﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using System.Reflection;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.VM.Compliance;
using BM_ManegmentServices.VM.Dashboard;
using System.Globalization;
using System.Dynamic;
using System.Web.Mvc;
using Newtonsoft.Json;
using BM_ManegmentServices.Services.DocumentManagenemt;
using System.IO;

using System.Net;
using RestSharp;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;

namespace BM_ManegmentServices.Services.Masters
{
    public class Meeting_Service : IMeeting_Service
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        IMeeting_Compliances objIMeetingCompliance;
        IComplianceTransaction_Service objIComplianceTransaction_Service;
        IDirectorMaster objIDirectorMaster;

        IFileData_Service objIFileData_Service;
        IMeeting_Compliances objIMeeting_Compliances;
        IAuditorMaster objIAuditorMaster;

        VideoMeeting _objvideomeeting = new VideoMeeting();


        public Meeting_Service(IMeeting_Compliances obj, IComplianceTransaction_Service obj1, IDirectorMaster objDirectorMaster, IFileData_Service objFileData_Service, IMeeting_Compliances objMeeting_Compliances, IAuditorMaster objAuditorMaster)
        {
            objIMeetingCompliance = obj;
            objIComplianceTransaction_Service = obj1;
            objIDirectorMaster = objDirectorMaster;
            objIFileData_Service = objFileData_Service;
            objIMeeting_Compliances = objMeeting_Compliances;
            objIAuditorMaster = objAuditorMaster;
        }

        #region Meeting
        public IEnumerable<MeetingVM> GetMeetings(int CustomerId, int UserId, string Role)
        {
            try
            {
                #region Commented on 04 Sep 2020
                /*
                var today = DateTime.Today;
                if (Role == SecretarialConst.Roles.CS)
                {
                    return (from row in entities.BM_Meetings
                            join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                            join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                            join view in entities.BM_AssignedEntitiesView on entity.Id equals view.EntityId
                            from minutes in entities.BM_MeetingMinutesDetails.Where(k=> k.MeetingId == row.MeetingID && k.IsDeleted == false).DefaultIfEmpty()
                            where row.Customer_Id == CustomerId && row.IsDeleted == false &&
                            view.userId == UserId
                            orderby row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn descending
                            select new MeetingVM
                            {
                                MeetingID = row.MeetingID,
                                IsVirtualMeeting = row.IsVirtualMeeting,
                                Type = row.MeetingCircular.Trim(),
                                MeetingSrNo = row.MeetingSrNo,
                                MeetingTypeId = row.MeetingTypeId,
                                MeetingTypeName = meeting.MeetingTypeName,
                                Quarter_ = row.Quarter_,
                                MeetingTitle = row.MeetingTitle,
                                MeetingDate = row.MeetingDate,
                                MeetingTime = row.MeetingTime,
                                MeetingVenue = row.MeetingVenue,
                                IsSeekAvailability = row.IsSeekAvailability,
                                Availability_ShortDesc = row.Availability_ShortDesc,
                                TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                Entityt_Id = row.EntityId,
                                EntityName = entity.CompanyName,
                                FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
                                Stage = row.IsCompleted == true ? "Completed" : row.IsNoticeSent == true ? (row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Pending") : "Draft",

                                IsNoticeSent = row.IsNoticeSent,
                                IsMeetingCompleted = row.IsCompleted,
                                IsComplianceClosed = row.IsComplianceClosed,
                                IsAdjourned = row.IsAdjourned,

                                IsCirculate = minutes.IsCirculate,
                                IsFinalized = minutes.IsFinalized,
                            }).ToList();
                }
                else
                {
                    return (from row in entities.BM_Meetings
                            join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                            join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                            from minutes in entities.BM_MeetingMinutesDetails.Where(k => k.MeetingId == row.MeetingID && k.IsDeleted == false).DefaultIfEmpty()
                            where row.Customer_Id == CustomerId && row.IsDeleted == false
                            orderby row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn descending
                            select new MeetingVM
                            {
                                MeetingID = row.MeetingID,
                                IsVirtualMeeting = row.IsVirtualMeeting,
                                Type = row.MeetingCircular.Trim(),
                                MeetingSrNo = row.MeetingSrNo,
                                MeetingTypeId = row.MeetingTypeId,
                                MeetingTypeName = meeting.MeetingTypeName,
                                Quarter_ = row.Quarter_,
                                MeetingTitle = row.MeetingTitle,
                                MeetingDate = row.MeetingDate,
                                MeetingTime = row.MeetingTime,
                                MeetingVenue = row.MeetingVenue,
                                IsSeekAvailability = row.IsSeekAvailability,
                                Availability_ShortDesc = row.Availability_ShortDesc,
                                TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                Entityt_Id = row.EntityId,
                                EntityName = entity.CompanyName,
                                FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
                                Stage = row.IsCompleted == true ? "Completed" : row.IsNoticeSent == true ? (row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Pending") : "Draft",
                                IsNoticeSent = row.IsNoticeSent,
                                IsMeetingCompleted = row.IsCompleted,
                                IsComplianceClosed = row.IsComplianceClosed,
                                IsAdjourned = row.IsAdjourned,

                                IsCirculate = minutes.IsCirculate,
                                IsFinalized = minutes.IsFinalized,
                            }).ToList();
                }
                */
                #endregion

                return (from row in entities.BM_SP_GetMeetings(UserId, Role, CustomerId)
                        select new MeetingVM
                        {
                            MeetingID = row.MeetingID,
                            IsVirtualMeeting = row.IsVirtualMeeting,
                            Type = row.MeetingCircular.Trim(),
                            MeetingSrNo = row.MeetingSrNo,
                            MeetingTypeId = row.MeetingTypeId,
                            MeetingTypeName = row.MeetingTypeName,
                            Quarter_ = row.Quarter_,
                            MeetingTitle = row.MeetingTitle,
                            MeetingDate = row.MeetingDate,
                            MeetingTime = row.MeetingTime,
                            MeetingVenue = row.MeetingVenue,
                            IsSeekAvailability = row.IsSeekAvailability,
                            Availability_ShortDesc = row.Availability_ShortDesc,
                            TypeName = row.TypeName,
                            Entityt_Id = row.EntityId,
                            EntityName = row.CompanyName,
                            FY_CY = row.FYText,
                            Stage = row.Stage,
                            IsNoticeSent = row.IsNoticeSent,
                            IsMeetingCompleted = row.IsCompleted,
                            IsComplianceClosed = row.IsComplianceClosed,
                            IsAdjourned = row.IsAdjourned,

                            IsCirculate = row.IsCirculate,
                            IsFinalized = row.IsFinalized,
                            CanStart = row.CanStart,

                            IsVideoMeeting = row.IsVideoMeeting,
                            #region added by Ruchi on 09-03-2021
                            IsRecording = (bool)row.IsRecording
                            #endregion

                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new List<MeetingVM>();
            }
        }

        public Dashboard_MeetingVM GetMeetingsForDashboard(int UserID, int CustomerId, string Role)
        {
            try
            {
                var result = new Dashboard_MeetingVM();
                IEnumerable<MeetingVM> meetings = null;

                if (Role == SecretarialConst.Roles.DRCTR)
                {
                    meetings = GetMeetingsForParticipant(UserID);
                }
                else
                {
                    meetings = GetMeetings(CustomerId, UserID, Role);
                }

                if (meetings != null)
                {
                    if (meetings.Count() > 0)
                    {
                        //result.Upcoming = meetings.Where(k => k.Stage == SecretarialConst.Dashboard.Upcoming).ToList();
                        result.Todays = (from r in meetings
                                         where r.Stage == SecretarialConst.MeetingStages.TODAYS
                                         select new MeetingListVM
                                         {

                                             MeetingID = r.MeetingID,
                                             MeetingTypeName = r.MeetingTypeName,
                                             MeetingSrNo = r.MeetingSrNo,
                                             EntityName = r.EntityName,

                                             MeetingDate = r.MeetingDate,
                                             Day = r.MeetingDate.HasValue ? r.MeetingDate.Value.Day.ToString() : "",
                                             DayName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("dddd") : "",
                                             Month = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("MMM") : "",
                                             MeetingTime = r.MeetingTime,
                                             MeetingVenue = r.MeetingVenue,
                                             Stage = r.Stage

                                         }).ToList();

                        result.Upcoming = (from r in meetings
                                           where r.Stage == SecretarialConst.MeetingStages.UPCOMING
                                           orderby r.MeetingDate
                                           select new MeetingListVM
                                           {
                                               MeetingID = r.MeetingID,
                                               MeetingTypeName = r.MeetingTypeName,
                                               MeetingSrNo = r.MeetingSrNo,
                                               EntityName = r.EntityName,

                                               MeetingDate = r.MeetingDate,
                                               Day = r.MeetingDate.HasValue ? r.MeetingDate.Value.Day.ToString() : "",
                                               DayName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("dddd") : "",
                                               Month = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("MMM") : "",
                                               MeetingTime = r.MeetingTime,
                                               MeetingVenue = r.MeetingVenue,
                                               Stage = r.Stage

                                           }).ToList();
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<CompletedMeetingsVM> GetCompletedMeetings(int entityId, int customerId)
        {
            try
            {
                return (from row in entities.BM_SP_GetCompletedMeetings(entityId, customerId)
                        select new CompletedMeetingsVM
                        {
                            MeetingID = row.MeetingID,
                            MeetingCircular = row.MeetingCircular,
                            MeetingSrNo = row.MeetingSrNo,
                            MeetingDate = row.MeetingDate,
                            Day_ = row.Day_,
                            DName = row.DName,
                            Month_ = row.Month_,
                            MeetingTime = row.MeetingTime,
                            FY = row.FY,
                            FYText = row.FYText,
                            MeetingTypeId = row.MeetingTypeId,
                            MeetingTypeName = row.MeetingTypeName,
                            Customer_Id = row.Customer_Id,
                            SerialNo = row.SerialNo,
                            Quarter_ = row.Quarter_
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<MeetingVM> GetMeetingsForParticipant(int UserID)
        {
            try
            {
                var today = DateTime.Today;
                var result = (from row in entities.BM_Meetings
                              join participant in entities.BM_MeetingParticipant on row.MeetingID equals participant.Meeting_ID
                              join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                              join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                              where //row.Customer_Id == CustomerId && 
                              row.IsDeleted == false && row.IsNoticeSent == true &&
                              participant.UserId == UserID && participant.IsDeleted == false && participant.IsInvited == null
                              orderby row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn descending
                              select new MeetingVM
                              {
                                  MeetingID = row.MeetingID,
                                  Type = row.MeetingCircular.Trim(),
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = meeting.MeetingTypeName,
                                  Quarter_ = row.Quarter_,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  IsSeekAvailability = row.IsSeekAvailability,
                                  Availability_ShortDesc = row.Availability_ShortDesc,
                                  TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                  Entityt_Id = row.EntityId,
                                  EntityName = entity.CompanyName,
                                  FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
                                  Stage = row.MeetingDate == null ? "" : row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Pending"
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public MeetingVM GetMeeting(long CustomerId, long MeetingId)
        {
            try
            {
                var result = (from row in entities.BM_Meetings
                                  //join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                                  //join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                  //join year in entities.BM_YearMaster on row.FY equals year.FYID
                              where row.MeetingID == MeetingId && row.Customer_Id == CustomerId && row.IsDeleted == false
                              select new MeetingVM
                              {
                                  MeetingID = row.MeetingID,
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = row.BM_CommitteeComp.MeetingTypeName, //
                                  Quarter_ = row.Quarter_,
                                  IsShorter = row.IsShorter,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  CircularMeetingDetails = new CircularMeeting()
                                  {
                                      DueDate = row.MeetingDate,
                                      CircularDate = row.CircularDate,
                                      CircularDueTime = row.MeetingTime
                                  },
                                  MeetingTime = row.MeetingTime,
                                  MeetingAddressType = row.MeetingVenueType,
                                  MeetingVenue = row.MeetingVenue,
                                  IsSeekAvailability = row.IsSeekAvailability,
                                  Availability_ShortDesc = row.Availability_ShortDesc,
                                  Type = row.MeetingCircular,
                                  TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                  SeekAvailability_DueDate = row.AvailabilityDueDate,
                                  IsSeekAvailabilitySent = row.IsSeekAvailabilitySent,
                                  IsNoticeSent = row.IsNoticeSent,
                                  IsAgendaSent = row.IsAgendaSent,

                                  AvailabilityMailFormat = new MeetingAvailabilityMailVM()
                                  {
                                      AvailabilityMailID = row.MeetingID,
                                      AvailabilityMail = row.AvailabilityMail,
                                      IsSeekAvailabilitySent = row.IsSeekAvailabilitySent
                                  },
                                  NoticeMailFormat = new MeetingNoticeMailVM()
                                  {
                                      NoticeMailID = row.MeetingID,
                                      NoticeMail = row.NoticeMail,
                                      TemplateType = row.TemplateType,
                                      IsNoticeSent = row.IsNoticeSent,
                                      IsQuarter_ = string.IsNullOrEmpty(row.Quarter_) ? false : true,
                                      IsShorter_ = row.IsShorter,
                                      Type = row.MeetingCircular
                                  },
                                  Entityt_Id = row.EntityId,
                                  EntityName = row.BM_EntityMaster.CompanyName,
                                  EntityAddressLine1 = row.BM_EntityMaster.Regi_Address_Line1,
                                  EntityAddressLine2 = row.BM_EntityMaster.Regi_Address_Line2,

                                  CreatedDate = row.CreatedOn,
                                  SendDateAvailability = row.SendDateAvailability,
                                  SendDateAgenda = row.SendDateAgenda,
                                  SendDateNotice = row.SendDateNotice,

                                  FY_CY = row.BM_YearMaster.FYText,

                                  IsMeetingCompleted = row.IsCompleted,
                                  IsComplianceClosed = row.IsComplianceClosed,
                                  StartMeetingDate = row.StartMeetingDate,
                                  StartMeetingTime = row.StartTime,
                                  EndMeetingDate = row.EndMeetingDate

                              }).FirstOrDefault();

                if (result != null)
                {
                    if (result.IsSeekAvailabilitySent == true)
                    {
                        if (result.IsAgendaSent == true || result.IsNoticeSent == true)
                        {
                            result.CanReSendSeekAvailability = false;
                            result.CanMarkSeekAvailability = false;
                        }
                        else if (result.SeekAvailability_DueDate >= DateTime.Now.Date)
                        {
                            result.CanReSendSeekAvailability = false;
                            result.CanMarkSeekAvailability = true;
                        }
                        else if (result.SeekAvailability_DueDate < DateTime.Now.Date)
                        {
                            result.CanReSendSeekAvailability = true;
                            result.CanMarkSeekAvailability = false;
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public MeetingVM GetMeetingDetails(int CustomerId, long MeetingId)
        {
            bool IsCreatedVMeeting = false;
            try
            {
                var meeting = (from row in entities.BM_SP_GetMeetingById(MeetingId, CustomerId)
                               select row
                          ).FirstOrDefault();

                if (meeting != null)
                {
                    var result = new MeetingVM()
                    {
                        MeetingID = meeting.MeetingID,
                        MeetingSrNo = meeting.MeetingSrNo,
                        MeetingTypeId = meeting.MeetingTypeId,
                        MeetingTypeName = meeting.MeetingTypeName,
                        FYID = (int)meeting.FY,
                        FY_CY = meeting.FYText,
                        Quarter_ = meeting.Quarter_,
                        IsShorter = meeting.IsShorter,
                        NoticeType = meeting.IsShorter != null ? meeting.IsShorter == true ? "Shorter" : "Regular" : "",
                        MeetingTitle = meeting.MeetingTitle,
                        MeetingDate = meeting.MeetingDate,

                        MeetingTime = meeting.MeetingTime,
                        MeetingAddressType = meeting.MeetingVenueType,
                        MeetingVenue = meeting.MeetingVenue,
                        IsSeekAvailability = meeting.IsSeekAvailability,
                        //Availability_ShortDesc = meeting.Availability_ShortDesc,
                        Type = meeting.MeetingCircular,
                        TypeName = meeting.MeetingCircular == "C" ? "Circular" : "Meeting",
                        SeekAvailability_DueDate = meeting.AvailabilityDueDate,
                        IsSeekAvailabilitySent = meeting.IsSeekAvailabilitySent,
                        IsNoticeSent = meeting.IsNoticeSent,
                        IsAgendaSent = meeting.IsAgendaSent,

                        IsPostponded = meeting.IsPostponded,

                        Entityt_Id = meeting.EntityId,
                        EntityName = meeting.EntityName,

                        CreatedDate = meeting.CreatedOn,
                        SendDateAvailability = meeting.SendDateAvailability,
                        SendDateAgenda = meeting.SendDateAgenda,
                        SendDateNotice = meeting.SendDateNotice,

                        IsMeetingCompleted = meeting.IsCompleted,
                        IsComplianceClosed = meeting.IsComplianceClosed,
                        StartMeetingDate = meeting.StartMeetingDate,
                        StartMeetingTime = meeting.StartTime,
                        EndMeetingDate = meeting.EndMeetingDate,
                        EndMeetingTime = meeting.EndMeetingTime,
                        IsVirtualMeeting = meeting.IsVirtualMeeting,
                        IsEVoting = meeting.IsEVoting,
                        IsVideoMeeting = meeting.IsVideoMeeting,
                        VMeetingId = (from x in entities.BM_VideoMeeting where x.MeetingID == meeting.MeetingID select x.V_MeetingId).FirstOrDefault()
                    };

                    if (meeting.IsVirtualMeeting == true)
                    {
                        result.CanPostpond = true;
                        var objMinutesDetails = GetMinutesDetails(meeting.MeetingID);
                        if (objMinutesDetails != null)
                        {
                            result.MinutesDetailsNew = objMinutesDetails;
                        }
                        else
                        {
                            result.MinutesDetailsNew = new MeetingMinutesDetailsVM() { MOM_MeetingId = meeting.MeetingID };
                        }

                        #region Set temp Meeting SrNo in case of Virtual meeting
                        if (meeting.MeetingSrNo == null && meeting.IsVirtualMeeting == true)
                        {
                            int? Meeting_SrNo = null;
                            int? temp_SrNo = null;
                            long? temp_FY = null;
                            var IsContinuosSrNo = true;

                            var config = (from row in entities.BM_ConfigurationMaster
                                          where row.MeetingType == meeting.MeetingTypeId && row.EntityID == meeting.EntityId && row.IsDeleted == false
                                          select new { row.LastMeetingNo, row.LastCircularNo, row.MeetingNoType, row.CircularNoType, row.MeetingFY, row.CircularFY }).FirstOrDefault();

                            if (config != null)
                            {
                                if (config.MeetingNoType == "FY")
                                {
                                    IsContinuosSrNo = false;
                                }

                                if (meeting.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.MEETING)
                                {
                                    temp_SrNo = config.LastMeetingNo;
                                    temp_FY = config.MeetingFY;
                                }
                                else if (meeting.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.CIRCULAR)
                                {
                                    temp_SrNo = config.LastCircularNo;
                                    temp_FY = config.CircularFY;
                                }
                            }

                            if (IsContinuosSrNo)
                            {
                                //Continueous
                                Meeting_SrNo = (from row in entities.BM_Meetings
                                                where row.MeetingTypeId == meeting.MeetingTypeId && row.EntityId == meeting.EntityId && row.MeetingCircular == meeting.MeetingCircular && row.IsDeleted == false
                                                select row.MeetingSrNo).Max();
                            }
                            else
                            {
                                //Financial year-wise
                                Meeting_SrNo = (from row in entities.BM_Meetings
                                                where row.MeetingTypeId == meeting.MeetingTypeId && row.EntityId == meeting.EntityId && row.MeetingCircular == meeting.MeetingCircular && row.IsDeleted == false &&
                                                row.FY == meeting.FY
                                                select row.MeetingSrNo).Max();
                            }

                            if (Meeting_SrNo == null && config != null)
                            {
                                Meeting_SrNo = IsContinuosSrNo ? temp_SrNo : meeting.FY == temp_FY ? temp_SrNo : null;
                            }

                            result.MeetingSrNo = Meeting_SrNo == null ? 1 : Meeting_SrNo + 1;

                        }
                        #endregion
                    }
                    else
                    {
                        result.CanPostpond = false;
                        if (meeting.MeetingCircular == SecretarialConst.MeetingCircular.MEETING && meeting.IsNoticeSent == true)
                        {
                            if (meeting.MeetingDate != null && !string.IsNullOrEmpty(meeting.MeetingTime))
                            {
                                var actualMeetingDateTime = Convert.ToDateTime(Convert.ToDateTime(meeting.MeetingDate).ToString("dd/MMM/yyyy") + " " + meeting.MeetingTime);
                                if (actualMeetingDateTime > DateTime.Now)
                                {
                                    result.CanPostpond = true;
                                }
                            }
                        }
                    }

                    result.CircularMeetingDetails = new CircularMeeting()
                    {
                        DueDate = meeting.MeetingDate,
                        CircularDate = meeting.CircularDate,
                        CircularDueTime = meeting.MeetingTime
                    };

                    result.FinalizeResolutionDetails = new FinalizeResolutionVM()
                    {
                        ResolutionFinalizeId = meeting.MeetingID,
                        ResolutionFinalizeDate = meeting.EndMeetingDate,
                        IsVirtualResolution = meeting.IsVirtualMeeting,
                        IsResolutionFinalized = meeting.IsCompleted
                    };
                    result.AvailabilityMailFormat = new MeetingAvailabilityMailVM()
                    {
                        AvailabilityMailID = meeting.MeetingID,
                        AvailabilityMail = meeting.AvailabilityMail,
                        IsSeekAvailabilitySent = result.IsSeekAvailabilitySent
                    };

                    result.NoticeMailFormat = new MeetingNoticeMailVM()
                    {
                        NoticeMailID = meeting.MeetingID,
                        NoticeMail = meeting.NoticeMail,
                        TemplateType = meeting.TemplateType,
                        IsNoticeSent = meeting.IsNoticeSent,
                        IsQuarter_ = string.IsNullOrEmpty(meeting.Quarter_) ? false : true,
                        IsShorter_ = meeting.IsShorter,
                        Type = meeting.MeetingCircular,
                        FillPostpondedOptions = meeting.IsPostponded,
                        IsVirtualMeeting_ = meeting.IsVirtualMeeting,
                        MeetingType_Id = meeting.MeetingTypeId
                    };

                    result.InviteeMailFormat = new MeetingInviteeMailVM()
                    {
                        InviteeMailID = meeting.MeetingID,
                        InviteeMail = meeting.InviteeMail
                    };

                    result.NotesFormat = new MeetingNotesVM()
                    {
                        NotesID = meeting.MeetingID,
                        Notes = meeting.Notes
                    };

                    result.AgendaTaskReview = new AgendaMinutesTaskVM()
                    {
                        TaskMeetingId = result.MeetingID,
                        TaskEnitytId = result.Entityt_Id,
                        TaskTypeId = SecretarialConst.TaskType.DRAFT_AGENDA
                    };

                    if (meeting.SendDateNotice != null)
                    {
                        result.NoticeMailFormat.NoticeSendDate = meeting.SendDateNotice.Value.Date;
                        result.NoticeMailFormat.NoticeSendTime = Convert.ToDateTime(meeting.SendDateNotice).ToString("hh:mm tt");
                    }

                    if (result.IsSeekAvailabilitySent == true)
                    {
                        if (result.IsAgendaSent == true || result.IsNoticeSent == true)
                        {
                            result.CanReSendSeekAvailability = false;
                            result.CanMarkSeekAvailability = false;
                        }
                        else if (result.SeekAvailability_DueDate >= DateTime.Now.Date)
                        {
                            result.CanReSendSeekAvailability = false;
                            result.CanMarkSeekAvailability = true;
                        }
                        else if (result.SeekAvailability_DueDate < DateTime.Now.Date)
                        {
                            result.CanReSendSeekAvailability = true;
                            result.CanMarkSeekAvailability = false;
                        }
                    }

                    #region Check configuration master saved or not
                    var meetingCount = (from row in entities.BM_Meetings
                                        where row.EntityId == result.Entityt_Id
                                        && row.MeetingCircular == result.Type
                                        && row.MeetingTypeId == result.MeetingTypeId
                                        && row.IsDeleted == false
                                        select row.MeetingID).Count();
                    if (meetingCount <= 1)
                    {
                        var query = (from row in entities.BM_ConfigurationMaster
                                     where row.EntityID == result.Entityt_Id && row.MeetingType == result.MeetingTypeId &&
                                     row.IsDeleted == false && row.IsActive == true
                                     select row);
                        if (result.Type == SecretarialConst.MeetingCircular.CIRCULAR)
                        {
                            query = query.Where(k => k.IsCircularMeeting == true);
                        }

                        result.IsConfigurationMasterSaved = query.Any();
                    }
                    else
                    {
                        result.IsConfigurationMasterSaved = true;
                    }

                    #endregion

                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public MeetingVM GetMeetingForDocumentName(long CustomerId, long MeetingId)
        {
            try
            {
                var result = (from row in entities.BM_Meetings
                              where row.MeetingID == MeetingId && row.Customer_Id == CustomerId && row.IsDeleted == false
                              select new MeetingVM
                              {
                                  MeetingID = row.MeetingID,
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingDate = row.MeetingDate,
                                  MeetingTypeId = row.MeetingTypeId,
                                  TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                  MeetingTypeName = row.BM_CommitteeComp.MeetingTypeName,
                                  Quarter_ = row.Quarter_,
                                  FY_CY = row.BM_YearMaster.FYText,
                                  Entityt_Id = row.EntityId,
                                  EntityTypeId_ = row.Entity_Type
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public Meeting_NewVM Create(Meeting_NewVM obj)
        {
            try
            {
                var NoticeFormat = (dynamic)null;
                //Delete code on 14 Nov 2019 & Generate Meeting SrNo on Send Notice

                var entity_type = (from row in entities.BM_EntityMaster
                                   where row.Id == obj.Entityt_Id
                                   select row.Entity_Type).FirstOrDefault();

                var meetingTypeName = (from row in entities.BM_CommitteeComp
                                       where row.Id == obj.MeetingTypeId
                                       select row.MeetingTypeName).FirstOrDefault();

                var meetingTitle = "Draft";

                if (entity_type != 10)
                {
                    obj.Quarter_ = null;
                }

                if (obj.MeetingSrNo != null && (obj.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || obj.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM))
                {
                    var FYText = entities.BM_YearMaster.Where(k => k.FYID == obj.FYID).Select(k => k.FYText).FirstOrDefault();
                    meetingTitle = (obj.Type == SecretarialConst.MeetingCircular.MEETING ? "Meeting - " : "Circular - ") + meetingTypeName + " [ " + obj.MeetingSrNo + " ] " + FYText;
                }
                else if (obj.Type == SecretarialConst.MeetingCircular.CIRCULAR)
                {
                    obj.IsShorter = false;
                    meetingTitle = "Draft Circular " + meetingTypeName;
                }
                else
                {
                    if (obj.IsAdjourned != true)
                    {
                        #region Check Same meeting exists in draft
                        var IsExists = (from row in entities.BM_Meetings
                                        where row.EntityId == obj.Entityt_Id && row.MeetingTypeId == obj.MeetingTypeId && row.MeetingCircular == SecretarialConst.MeetingCircular.MEETING &&
                                              row.Quarter_ == obj.Quarter_ && row.IsNoticeSent == false && row.IsDeleted == false
                                        select row.MeetingID).Any();

                        if (IsExists == true)
                        {
                            obj.Error = true;
                            obj.Message = string.Format("New {0} Meeting can not create, meeting already exists in Draft. ", meetingTypeName);
                            return obj;
                        }
                        #endregion

                        meetingTitle = "Draft Meeting " + meetingTypeName;
                    }

                    if (obj.IsAdjourned == true)
                    {
                        var FYText = entities.BM_YearMaster.Where(k => k.FYID == obj.FYID).Select(k => k.FYText).FirstOrDefault();
                        meetingTitle = "Meeting - " + meetingTypeName + " [ " + obj.MeetingSrNo + " ] " + FYText + " (Adjourned)";
                    }
                }

                BM_Meetings _obj = new BM_Meetings()
                {
                    MeetingID = obj.MeetingID,
                    FY = obj.FYID,
                    MeetingSrNo = obj.MeetingSrNo,
                    MeetingTypeId = obj.MeetingTypeId,
                    MeetingCircular = obj.Type,
                    Quarter_ = obj.Quarter_,
                    IsShorter = obj.IsShorter,
                    IsVirtualMeeting = obj.IsVirtualMeeting,
                    IsSeekAvailability = false,

                    IsAgendaSent = false,
                    IsNoticeSent = false,
                    IsSeekAvailabilitySent = false,

                    EntityId = obj.Entityt_Id,
                    Entity_Type = entity_type,
                    Customer_Id = obj.CustomerId,
                    IsDeleted = false,
                    CreatedBy = obj.UserId,
                    CreatedOn = DateTime.Now,

                    IsAGMConclude = false,
                    IsCompleted = false,
                    IsPostponded = false,
                    IsComplianceClosed = false,
                    Stage = "Draft",

                    MeetingTitle = meetingTitle,
                    MeetingDate = obj.MeetingDate,
                    MeetingTime = obj.MeetingTime,
                    MeetingVenue = obj.MeetingVenue,
                    MeetingVenueType = obj.MeetingAddressType,
                    IsVideoMeeting = obj.IsVideoconfrence,
                    GenerateAgendaDocument = false,
                    GenerateAgendaDocumentOnNotice = false
                };

                #region AGM/EGM e-voting
                if (_obj.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || _obj.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                {
                    if (_obj.Entity_Type == SecretarialConst.EntityTypeID.LISTED)
                    {
                        obj.IsEVoting = true;
                    }

                    _obj.IsEVoting = obj.IsEVoting;

                    var notes = (from row in entities.BM_NotesMaster
                                 where row.CustomerId == obj.CustomerId && row.eVoting == obj.IsEVoting && row.IsDeleted == false
                                 select new
                                 {
                                     row.NotesFormat
                                 }).FirstOrDefault();

                    if (notes != null)
                    {
                        _obj.Notes = notes.NotesFormat;
                    }
                    else
                    {
                        if(_obj.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                        {
                            _obj.Notes = (from row in entities.BM_SP_MeetingDefaultFormatByType(SecretarialConst.MeetingFormatType.GM_NOTES)
                                          select row).FirstOrDefault();
                        }
                        else
                        {
                            _obj.Notes = (from row in entities.BM_SP_MeetingDefaultFormatByType(SecretarialConst.MeetingFormatType.EGM_NOTES)
                                          select row).FirstOrDefault();
                        }
                        
                    }

                }
                #endregion

                #region Get Seek Availability & Notice Mail Formats
                var SAFormat = (from row in entities.BM_TemplateMaster
                                where row.TemplateType == SecretarialConst.MeetingTemplatType.SEEK_AVAILABILITY && row.MeetingTypeId == obj.MeetingTypeId && row.IsDeleted == false
                                select row.MailTemplate).FirstOrDefault();
                if (string.IsNullOrEmpty(SAFormat))
                {
                    SAFormat = (from row in entities.BM_SP_MeetingDefaultFormatByType(SecretarialConst.MeetingFormatType.SEEK_AVAILABILITY)
                                select row).FirstOrDefault();
                }

                _obj.AvailabilityMail = SAFormat;

                var IsQuarter = !string.IsNullOrEmpty(obj.Quarter_);
                if (obj.Type.Trim() == SecretarialConst.MeetingCircular.MEETING)
                {
                    if (obj.IsShorter == false)
                    {
                        NoticeFormat = (from row in entities.BM_TemplateMaster
                                        where row.TemplateType == SecretarialConst.MeetingTemplatType.NOTICE && row.MeetingTypeId == obj.MeetingTypeId && row.IsQuarter == IsQuarter && row.IsShorter == obj.IsShorter && row.IsDeleted == false
                                        select row.MailTemplate).FirstOrDefault();

                        _obj.TemplateType = SecretarialConst.MeetingTemplatType.NOTICE;
                    }
                    else
                    {
                        NoticeFormat = (from row in entities.BM_TemplateMaster
                                        where row.TemplateType == SecretarialConst.MeetingTemplatType.AGENDA_PLUS_NOTICE && row.MeetingTypeId == obj.MeetingTypeId && row.IsQuarter == IsQuarter && row.IsShorter == obj.IsShorter && row.IsDeleted == false
                                        select row.MailTemplate).FirstOrDefault();

                        _obj.TemplateType = SecretarialConst.MeetingTemplatType.AGENDA_PLUS_NOTICE;
                    }

                    if (string.IsNullOrEmpty(NoticeFormat))
                    {
                        NoticeFormat = (from row in entities.BM_SP_MeetingDefaultFormatByType(SecretarialConst.MeetingFormatType.NOTICE_MAIL)
                                        select row).FirstOrDefault();
                    }

                    #region Invittee Mail Format
                    var invitteeMailFormat = "";
                    invitteeMailFormat = (from row in entities.BM_TemplateMaster
                                          where row.TemplateType == SecretarialConst.MeetingTemplatType.INVITEE_MAIL && row.MeetingTypeId == obj.MeetingTypeId && row.IsDeleted == false
                                          select row.MailTemplate).FirstOrDefault();
                    if (string.IsNullOrEmpty(invitteeMailFormat))
                    {
                        invitteeMailFormat = (from row in entities.BM_SP_MeetingDefaultFormatByType(SecretarialConst.MeetingFormatType.INVITEE_MAIL)
                                              select row).FirstOrDefault();
                    }
                    _obj.InviteeMail = invitteeMailFormat;
                    #endregion
                }
                else if (obj.Type.Trim() == SecretarialConst.MeetingCircular.CIRCULAR)
                {
                    NoticeFormat = (from row in entities.BM_TemplateMaster
                                    where row.TemplateType == SecretarialConst.MeetingTemplatType.CIRCULAR && row.MeetingTypeId == obj.MeetingTypeId && row.IsQuarter == false && row.IsDeleted == false
                                    select row.MailTemplate).FirstOrDefault();

                    _obj.TemplateType = SecretarialConst.MeetingTemplatType.CIRCULAR;
                }
                _obj.NoticeMail = NoticeFormat;
                #endregion

                entities.BM_Meetings.Add(_obj);
                entities.SaveChanges();

                obj.MeetingID = _obj.MeetingID;
                obj.Success = true;
                obj.Message = "Saved Successfully.";

                if (obj.MeetingID > 0)
                {
                    if (obj.Type.Trim() == SecretarialConst.MeetingCircular.MEETING)
                    {
                        #region Add Default Agenda Items

                        var SrNoNew = 1;

                        #region Default Agenda Items -> QUORUM
                        var agendaItemQuorum = (from agenda in entities.BM_AgendaMaster
                                                where agenda.EntityType == entity_type && agenda.MeetingTypeId == obj.MeetingTypeId &&
                                                      agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.QUORUM && agenda.IsDeleted == false
                                                select new AgendaItemSelect
                                                {
                                                    Meeting_Id = obj.MeetingID,
                                                    BM_AgendaMasterId = agenda.BM_AgendaMasterId,
                                                    PartID = agenda.PartId,
                                                    Agenda = agenda.Agenda,
                                                    IsCheked = true
                                                }
                                                 ).FirstOrDefault();

                        if (agendaItemQuorum != null)
                        {
                            var Agenda = entities.BM_SP_AgendaFormatesByCustomerId(obj.CustomerId, agendaItemQuorum.BM_AgendaMasterId).FirstOrDefault();

                            var _agendaItemQuorum = new BM_MeetingAgendaMapping();
                            _agendaItemQuorum.MeetingID = obj.MeetingID;
                            _agendaItemQuorum.AgendaID = agendaItemQuorum.BM_AgendaMasterId;
                            _agendaItemQuorum.SrNo = SrNoNew;
                            _agendaItemQuorum.AgendaItemText = agendaItemQuorum.Agenda;
                            //_agendaItemQuorum.RefPendingMappingID = obj.RefPendingMappingID;
                            _agendaItemQuorum.IsClosed = true;
                            _agendaItemQuorum.ResultRemark = null;
                            _agendaItemQuorum.IsActive = true;
                            _agendaItemQuorum.IsDeleted = false;

                            _agendaItemQuorum.PartId = 1;
                            _agendaItemQuorum.CreatedBy = obj.UserId;
                            _agendaItemQuorum.CreatedOn = DateTime.Now;

                            if (Agenda != null)
                            {
                                _agendaItemQuorum.AgendaItemText = Agenda.AgendaItemText;
                                _agendaItemQuorum.AgendaFormat = Agenda.AgendaFormat;

                                _agendaItemQuorum.ResolutionFormatHeading = Agenda.ResolutionFormatHeading;
                                _agendaItemQuorum.ResolutionFormat = Agenda.ResolutionFormat;

                                _agendaItemQuorum.MinutesFormatHeading = Agenda.MinutesFormatHeading;
                                _agendaItemQuorum.MinutesFormat = Agenda.MinutesFormat;
                                _agendaItemQuorum.MinutesDisApproveFormat = Agenda.MinutesDisApproveFormat;
                                _agendaItemQuorum.MinutesDifferFormat = Agenda.MinutesDifferFormat;
                            }
                            entities.BM_MeetingAgendaMapping.Add(_agendaItemQuorum);
                            entities.SaveChanges();
                        }

                        #endregion

                        #region Default Agenda Items -> Confirmation of minutes of the Board meeting / Same type of Meeting

                        //var finalizedMeetings = (from meeting in entities.BM_Meetings
                        //                    join details in entities.BM_MeetingMinutesDetails on meeting.MeetingID equals details.MeetingId
                        //                    where meeting.EntityId == obj.Entityt_Id && meeting.MeetingTypeId == obj.MeetingTypeId && meeting.IsCompleted == true &&
                        //                            details.IsFinalized == true && details.ConfirmMeetingId == null
                        //                    select new
                        //                    {
                        //                        meeting.MeetingID,
                        //                        meeting.MeetingSrNo,
                        //                        meeting.MeetingDate
                        //                    }).ToList();

                        //if (finalizedMeetings!= null)
                        //{
                        //    if(finalizedMeetings.Count > 0)
                        //    {
                        //    var agendaItemConfirmation = (from agenda in entities.BM_AgendaMaster
                        //                                  where agenda.EntityType == entity_type && agenda.MeetingTypeId == obj.MeetingTypeId &&
                        //                                        agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.CONFIRMATION_OF_MINUTES && agenda.IsDeleted == false
                        //                                  select new AgendaItemSelect
                        //                                  {
                        //                                      Meeting_Id = obj.MeetingID,
                        //                                      BM_AgendaMasterId = agenda.BM_AgendaMasterId,
                        //                                      PartID = agenda.PartId,
                        //                                      Agenda = agenda.Agenda,
                        //                                      IsCheked = true
                        //                                  }
                        //                     ).FirstOrDefault();

                        //    if (agendaItemConfirmation != null)
                        //    {
                        //        SrNoNew++;
                        //        var Agenda = entities.BM_SP_AgendaFormatesByCustomerId(obj.CustomerId, agendaItemConfirmation.BM_AgendaMasterId).FirstOrDefault();

                        //        var _agendaItemConfirmation = new BM_MeetingAgendaMapping();
                        //        _agendaItemConfirmation.MeetingID = obj.MeetingID;
                        //        _agendaItemConfirmation.AgendaID = agendaItemConfirmation.BM_AgendaMasterId;
                        //        _agendaItemConfirmation.SrNo = SrNoNew;
                        //        _agendaItemConfirmation.AgendaItemText = agendaItemConfirmation.Agenda;
                        //        //_agendaItemConfirmation.RefPendingMappingID = obj.RefPendingMappingID;
                        //        _agendaItemConfirmation.IsClosed = true;
                        //        _agendaItemConfirmation.ResultRemark = null;
                        //        _agendaItemConfirmation.IsActive = true;
                        //        _agendaItemConfirmation.IsDeleted = false;

                        //        _agendaItemConfirmation.PartId = 1;
                        //        _agendaItemConfirmation.CreatedBy = obj.UserId;
                        //        _agendaItemConfirmation.CreatedOn = DateTime.Now;

                        //        if (Agenda != null)
                        //        {
                        //            _agendaItemConfirmation.AgendaItemText = Agenda.AgendaItemText;
                        //            _agendaItemConfirmation.AgendaFormat = Agenda.AgendaFormat;

                        //            _agendaItemConfirmation.ResolutionFormatHeading = Agenda.ResolutionFormatHeading;
                        //            _agendaItemConfirmation.ResolutionFormat = Agenda.ResolutionFormat;

                        //            _agendaItemConfirmation.MinutesFormatHeading = Agenda.MinutesFormatHeading;
                        //            _agendaItemConfirmation.MinutesFormat = Agenda.MinutesFormat;
                        //            _agendaItemConfirmation.MinutesDisApproveFormat = Agenda.MinutesDisApproveFormat;
                        //            _agendaItemConfirmation.MinutesDifferFormat = Agenda.MinutesDifferFormat;
                        //        }

                        //        string tempAgendaFormat = "", tempMinutesFormat = "";

                        //        foreach (var item in finalizedMeetings)
                        //        {
                        //            tempAgendaFormat += _agendaItemConfirmation.AgendaFormat.Replace("{{ NUMBER_OF_LAST_MEETING }}", Convert.ToString(item.MeetingSrNo)).Replace("{{ Last Meeting Date }}", Convert.ToDateTime(item.MeetingDate).ToString("dd/MM/yyyy"));
                        //            tempMinutesFormat += _agendaItemConfirmation.MinutesFormat.Replace("{{ NUMBER_OF_LAST_MEETING }}", Convert.ToString(item.MeetingSrNo)).Replace("{{ Last Meeting Date }}", Convert.ToDateTime(item.MeetingDate).ToString("dd/MM/yyyy"));
                        //        }

                        //        _agendaItemConfirmation.AgendaFormat = tempAgendaFormat;
                        //        _agendaItemConfirmation.MinutesFormat = tempMinutesFormat;

                        //        entities.BM_MeetingAgendaMapping.Add(_agendaItemConfirmation);
                        //        entities.SaveChanges();

                        //        foreach (var item in finalizedMeetings)
                        //        {
                        //            entities.BM_MeetingMinutesDetails.Where(k => k.MeetingId == item.MeetingID).FirstOrDefault().ConfirmMeetingId = obj.MeetingID;
                        //            entities.SaveChanges();
                        //        }
                        //    }
                        //    }
                        //}

                        #endregion

                        #region Default Agenda Items -> Confirmation of minutes of the committee(s) meeting

                        //if (obj.MeetingTypeId == 10)
                        //{
                        //    var finalizedCommitteeMeetings = (from meeting in entities.BM_Meetings
                        //                                      join committee in entities.BM_CommitteeComp on meeting.MeetingTypeId equals committee.Id
                        //                             join details in entities.BM_MeetingMinutesDetails on meeting.MeetingID equals details.MeetingId
                        //                             where meeting.EntityId == obj.Entityt_Id && meeting.MeetingTypeId != obj.MeetingTypeId && meeting.IsCompleted == true &&
                        //                             committee.IsCommittee == true &&
                        //                             details.IsFinalized == true && details.ConfirmBoardMeetingId == null
                        //                             select new
                        //                             {
                        //                                 meeting.MeetingID,
                        //                                 meeting.MeetingSrNo,
                        //                                 meeting.MeetingDate,
                        //                                 committee.Name
                        //                             }).ToList();

                        //    if (finalizedCommitteeMeetings != null)
                        //    {
                        //        if(finalizedCommitteeMeetings.Count > 0 )
                        //        {
                        //            var agendaItemConfirmationOfCommittee = (from agenda in entities.BM_AgendaMaster
                        //                                                     where agenda.EntityType == entity_type && agenda.MeetingTypeId == obj.MeetingTypeId &&
                        //                                                           agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.COMMITTEES_CONFIRMATION_OF_MINUTES && agenda.IsDeleted == false
                        //                                                     select new AgendaItemSelect
                        //                                                     {
                        //                                                         Meeting_Id = obj.MeetingID,
                        //                                                         BM_AgendaMasterId = agenda.BM_AgendaMasterId,
                        //                                                         PartID = agenda.PartId,
                        //                                                         Agenda = agenda.Agenda,
                        //                                                         IsCheked = true
                        //                                                     }
                        //                         ).FirstOrDefault();

                        //            if (agendaItemConfirmationOfCommittee != null)
                        //            {
                        //                SrNoNew++;
                        //                var Agenda = entities.BM_SP_AgendaFormatesByCustomerId(obj.CustomerId, agendaItemConfirmationOfCommittee.BM_AgendaMasterId).FirstOrDefault();

                        //                var _agendaItemConfirmation = new BM_MeetingAgendaMapping();
                        //                _agendaItemConfirmation.MeetingID = obj.MeetingID;
                        //                _agendaItemConfirmation.AgendaID = agendaItemConfirmationOfCommittee.BM_AgendaMasterId;
                        //                _agendaItemConfirmation.SrNo = SrNoNew;
                        //                _agendaItemConfirmation.AgendaItemText = agendaItemConfirmationOfCommittee.Agenda;
                        //                //_agendaItemConfirmation.RefPendingMappingID = obj.RefPendingMappingID;
                        //                _agendaItemConfirmation.IsClosed = true;
                        //                _agendaItemConfirmation.ResultRemark = null;
                        //                _agendaItemConfirmation.IsActive = true;
                        //                _agendaItemConfirmation.IsDeleted = false;

                        //                _agendaItemConfirmation.PartId = 1;
                        //                _agendaItemConfirmation.CreatedBy = obj.UserId;
                        //                _agendaItemConfirmation.CreatedOn = DateTime.Now;

                        //                if (Agenda != null)
                        //                {
                        //                    _agendaItemConfirmation.AgendaItemText = Agenda.AgendaItemText;
                        //                    _agendaItemConfirmation.AgendaFormat = Agenda.AgendaFormat;

                        //                    _agendaItemConfirmation.ResolutionFormatHeading = Agenda.ResolutionFormatHeading;
                        //                    _agendaItemConfirmation.ResolutionFormat = Agenda.ResolutionFormat;

                        //                    _agendaItemConfirmation.MinutesFormatHeading = Agenda.MinutesFormatHeading;
                        //                    _agendaItemConfirmation.MinutesFormat = Agenda.MinutesFormat;
                        //                    _agendaItemConfirmation.MinutesDisApproveFormat = Agenda.MinutesDisApproveFormat;
                        //                    _agendaItemConfirmation.MinutesDifferFormat = Agenda.MinutesDifferFormat;
                        //                }

                        //                string tempAgendaFormat = "", tempMinutesFormat = "";

                        //                foreach (var item in finalizedCommitteeMeetings)
                        //                {
                        //                    tempAgendaFormat += _agendaItemConfirmation.AgendaFormat.Replace("{{ Type of Last Meeting }}", item.Name).Replace("{{ NUMBER_OF_LAST_MEETING }}", Convert.ToString(item.MeetingSrNo)).Replace("{{ Last Meeting Date }}", Convert.ToDateTime(item.MeetingDate).ToString("dd/MM/yyyy"));
                        //                    tempMinutesFormat += _agendaItemConfirmation.MinutesFormat.Replace("{{ Type of Last Meeting }}", item.Name).Replace("{{ NUMBER_OF_LAST_MEETING }}", Convert.ToString(item.MeetingSrNo)).Replace("{{ Last Meeting Date }}", Convert.ToDateTime(item.MeetingDate).ToString("dd/MM/yyyy"));
                        //                }

                        //                _agendaItemConfirmation.AgendaFormat = tempAgendaFormat;
                        //                _agendaItemConfirmation.MinutesFormat = tempMinutesFormat;

                        //                entities.BM_MeetingAgendaMapping.Add(_agendaItemConfirmation);
                        //                entities.SaveChanges();

                        //                foreach (var item in finalizedCommitteeMeetings)
                        //                {
                        //                    entities.BM_MeetingMinutesDetails.Where(k => k.MeetingId == item.MeetingID).FirstOrDefault().ConfirmBoardMeetingId = obj.MeetingID;
                        //                    entities.SaveChanges();
                        //                }
                        //            }
                        //        }
                        //    }
                        //}
                        #endregion

                        var isFinalizedMinutesAgendaItemAdded = false;
                        var isCommitteeFinalizedMinutesAgendaItemAdded = false;
                        #region Default Agenda Items -> Confirmation of minutes of the Board meeting / Same type of Meeting

                        var finalizedMeetings = (from meeting in entities.BM_Meetings
                                                 join details in entities.BM_MeetingMinutesDetails on meeting.MeetingID equals details.MeetingId
                                                 where meeting.EntityId == obj.Entityt_Id && meeting.MeetingTypeId == obj.MeetingTypeId && meeting.IsCompleted == true &&
                                                         details.IsFinalized == true && details.ConfirmMeetingId == null
                                                 select new
                                                 {
                                                     meeting.MeetingID,
                                                     meeting.MeetingSrNo,
                                                     meeting.MeetingDate
                                                 }).ToList();

                        if (finalizedMeetings != null)
                        {
                            if (finalizedMeetings.Count > 0)
                            {
                                isFinalizedMinutesAgendaItemAdded = true;
                                var agendaItemConfirmation = (from agenda in entities.BM_AgendaMaster
                                                              where agenda.EntityType == entity_type && agenda.MeetingTypeId == obj.MeetingTypeId &&
                                                                    agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.CONFIRMATION_OF_MINUTES && agenda.IsDeleted == false
                                                              select new AgendaItemSelect
                                                              {
                                                                  Meeting_Id = obj.MeetingID,
                                                                  BM_AgendaMasterId = agenda.BM_AgendaMasterId,
                                                                  PartID = agenda.PartId,
                                                                  Agenda = agenda.Agenda,
                                                                  IsCheked = true
                                                              }
                                                 ).FirstOrDefault();

                                if (agendaItemConfirmation != null)
                                {
                                    var Agenda = entities.BM_SP_AgendaFormatesByCustomerId(obj.CustomerId, agendaItemConfirmation.BM_AgendaMasterId).FirstOrDefault();

                                    string tempAgendaFormat = "", tempMinutesFormat = "";

                                    foreach (var item in finalizedMeetings)
                                    {
                                        SrNoNew++;

                                        var _agendaItemConfirmation = new BM_MeetingAgendaMapping();
                                        _agendaItemConfirmation.MeetingID = obj.MeetingID;
                                        _agendaItemConfirmation.AgendaID = agendaItemConfirmation.BM_AgendaMasterId;
                                        _agendaItemConfirmation.SrNo = SrNoNew;
                                        _agendaItemConfirmation.AgendaItemText = agendaItemConfirmation.Agenda;
                                        _agendaItemConfirmation.IsClosed = true;
                                        _agendaItemConfirmation.ResultRemark = null;
                                        _agendaItemConfirmation.IsActive = true;
                                        _agendaItemConfirmation.IsDeleted = false;

                                        _agendaItemConfirmation.PartId = 1;
                                        _agendaItemConfirmation.CreatedBy = obj.UserId;
                                        _agendaItemConfirmation.CreatedOn = DateTime.Now;

                                        if (Agenda != null)
                                        {
                                            _agendaItemConfirmation.AgendaItemText = Agenda.AgendaItemText;
                                            _agendaItemConfirmation.AgendaFormat = Agenda.AgendaFormat;

                                            _agendaItemConfirmation.ResolutionFormatHeading = Agenda.ResolutionFormatHeading;
                                            _agendaItemConfirmation.ResolutionFormat = Agenda.ResolutionFormat;

                                            _agendaItemConfirmation.MinutesFormatHeading = Agenda.MinutesFormatHeading;
                                            _agendaItemConfirmation.MinutesFormat = Agenda.MinutesFormat;
                                            _agendaItemConfirmation.MinutesDisApproveFormat = Agenda.MinutesDisApproveFormat;
                                            _agendaItemConfirmation.MinutesDifferFormat = Agenda.MinutesDifferFormat;
                                        }

                                        tempAgendaFormat = _agendaItemConfirmation.AgendaFormat.Replace("{{NUMBER_OF_LAST_MEETING}}", Convert.ToString(item.MeetingSrNo)).Replace("{{DATE_OF_LAST_MEETING}}", Convert.ToDateTime(item.MeetingDate).ToString("dd/MM/yyyy"));
                                        tempMinutesFormat = _agendaItemConfirmation.MinutesFormat.Replace("{{NUMBER_OF_LAST_MEETING}}", Convert.ToString(item.MeetingSrNo)).Replace("{{DATE_OF_LAST_MEETING}}", Convert.ToDateTime(item.MeetingDate).ToString("dd/MM/yyyy"));

                                        _agendaItemConfirmation.AgendaFormat = tempAgendaFormat;
                                        _agendaItemConfirmation.MinutesFormat = tempMinutesFormat;

                                        entities.BM_MeetingAgendaMapping.Add(_agendaItemConfirmation);
                                        entities.SaveChanges();

                                        if (_agendaItemConfirmation.MeetingAgendaMappingID > 0)
                                        {
                                            var lstControls = (from row in entities.BM_AgendaMasterTemplate
                                                               where row.AgendaID == _agendaItemConfirmation.AgendaID && row.IsDeleted == false
                                                               select row
                                                               ).ToList();
                                            if (lstControls != null)
                                            {
                                                var meetingNo = lstControls.Where(k => k.TemplateName == "NUMBER_OF_LAST_MEETING").FirstOrDefault();
                                                if (meetingNo != null)
                                                {
                                                    var _objTemplate = new BM_MeetingAgendaTemplateList();
                                                    _objTemplate.TemplateListValue = Convert.ToString(item.MeetingSrNo);
                                                    _objTemplate.TemplateID = meetingNo.TemplateID;
                                                    _objTemplate.MeetingAgendaMappingID = _agendaItemConfirmation.MeetingAgendaMappingID;
                                                    _objTemplate.IsDeleted = false;
                                                    _objTemplate.CreatedBy = obj.UserId;
                                                    _objTemplate.CreatedOn = DateTime.Now;

                                                    entities.BM_MeetingAgendaTemplateList.Add(_objTemplate);
                                                    entities.SaveChanges();
                                                }

                                                var meetingDate = lstControls.Where(k => k.TemplateName == "DATE_OF_LAST_MEETING").FirstOrDefault();
                                                if (meetingDate != null && item.MeetingDate != null)
                                                {
                                                    var _objTemplate = new BM_MeetingAgendaTemplateList();
                                                    _objTemplate.TemplateListValue = Convert.ToDateTime(item.MeetingDate).ToString("dd/MM/yyyy");
                                                    _objTemplate.TemplateID = meetingDate.TemplateID;
                                                    _objTemplate.MeetingAgendaMappingID = _agendaItemConfirmation.MeetingAgendaMappingID;
                                                    _objTemplate.IsDeleted = false;
                                                    _objTemplate.CreatedBy = obj.UserId;
                                                    _objTemplate.CreatedOn = DateTime.Now;

                                                    entities.BM_MeetingAgendaTemplateList.Add(_objTemplate);
                                                    entities.SaveChanges();
                                                }
                                            }
                                        }

                                        entities.BM_MeetingMinutesDetails.Where(k => k.MeetingId == item.MeetingID).FirstOrDefault().ConfirmMeetingId = obj.MeetingID;
                                        entities.SaveChanges();

                                    }

                                }
                            }
                        }

                        #endregion

                        #region Default Agenda Items -> Confirmation of minutes of the committee(s) meeting

                        if (obj.MeetingTypeId == 10)
                        {
                            var finalizedCommitteeMeetings = (from meeting in entities.BM_Meetings
                                                              join committee in entities.BM_CommitteeComp on meeting.MeetingTypeId equals committee.Id
                                                              join details in entities.BM_MeetingMinutesDetails on meeting.MeetingID equals details.MeetingId
                                                              where meeting.EntityId == obj.Entityt_Id && meeting.MeetingTypeId != obj.MeetingTypeId && meeting.IsCompleted == true &&
                                                              committee.IsCommittee == true &&
                                                              details.IsFinalized == true && details.ConfirmBoardMeetingId == null
                                                              select new
                                                              {
                                                                  meeting.MeetingID,
                                                                  meeting.MeetingSrNo,
                                                                  meeting.MeetingDate,
                                                                  committee.Name
                                                              }).ToList();

                            if (finalizedCommitteeMeetings != null)
                            {
                                if (finalizedCommitteeMeetings.Count > 0)
                                {
                                    isCommitteeFinalizedMinutesAgendaItemAdded = true;
                                    var agendaItemConfirmationOfCommittee = (from agenda in entities.BM_AgendaMaster
                                                                             where agenda.EntityType == entity_type && agenda.MeetingTypeId == obj.MeetingTypeId &&
                                                                                   agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.COMMITTEES_CONFIRMATION_OF_MINUTES && agenda.IsDeleted == false
                                                                             select new AgendaItemSelect
                                                                             {
                                                                                 Meeting_Id = obj.MeetingID,
                                                                                 BM_AgendaMasterId = agenda.BM_AgendaMasterId,
                                                                                 PartID = agenda.PartId,
                                                                                 Agenda = agenda.Agenda,
                                                                                 IsCheked = true
                                                                             }
                                                 ).FirstOrDefault();

                                    if (agendaItemConfirmationOfCommittee != null)
                                    {
                                        var Agenda = entities.BM_SP_AgendaFormatesByCustomerId(obj.CustomerId, agendaItemConfirmationOfCommittee.BM_AgendaMasterId).FirstOrDefault();

                                        string tempAgendaFormat = "", tempMinutesFormat = "";

                                        foreach (var item in finalizedCommitteeMeetings)
                                        {
                                            SrNoNew++;
                                            var _agendaItemConfirmation = new BM_MeetingAgendaMapping();
                                            _agendaItemConfirmation.MeetingID = obj.MeetingID;
                                            _agendaItemConfirmation.AgendaID = agendaItemConfirmationOfCommittee.BM_AgendaMasterId;
                                            _agendaItemConfirmation.SrNo = SrNoNew;
                                            _agendaItemConfirmation.AgendaItemText = agendaItemConfirmationOfCommittee.Agenda;
                                            //_agendaItemConfirmation.RefPendingMappingID = obj.RefPendingMappingID;
                                            _agendaItemConfirmation.IsClosed = true;
                                            _agendaItemConfirmation.ResultRemark = null;
                                            _agendaItemConfirmation.IsActive = true;
                                            _agendaItemConfirmation.IsDeleted = false;

                                            _agendaItemConfirmation.PartId = 1;
                                            _agendaItemConfirmation.CreatedBy = obj.UserId;
                                            _agendaItemConfirmation.CreatedOn = DateTime.Now;

                                            if (Agenda != null)
                                            {
                                                _agendaItemConfirmation.AgendaItemText = Agenda.AgendaItemText;
                                                _agendaItemConfirmation.AgendaFormat = Agenda.AgendaFormat;

                                                _agendaItemConfirmation.ResolutionFormatHeading = Agenda.ResolutionFormatHeading;
                                                _agendaItemConfirmation.ResolutionFormat = Agenda.ResolutionFormat;

                                                _agendaItemConfirmation.MinutesFormatHeading = Agenda.MinutesFormatHeading;
                                                _agendaItemConfirmation.MinutesFormat = Agenda.MinutesFormat;
                                                _agendaItemConfirmation.MinutesDisApproveFormat = Agenda.MinutesDisApproveFormat;
                                                _agendaItemConfirmation.MinutesDifferFormat = Agenda.MinutesDifferFormat;
                                            }

                                            tempAgendaFormat = _agendaItemConfirmation.AgendaFormat.Replace("{{Type of Last Meeting}}", item.Name).Replace("{{NUMBER_OF_LAST_MEETING}}", Convert.ToString(item.MeetingSrNo)).Replace("{{DATE_OF_LAST_MEETING}}", Convert.ToDateTime(item.MeetingDate).ToString("dd/MM/yyyy"));
                                            tempMinutesFormat = _agendaItemConfirmation.MinutesFormat.Replace("{{TYPE_OF_LAST_MEETING}}", item.Name).Replace("{{NUMBER_OF_LAST_MEETING}}", Convert.ToString(item.MeetingSrNo)).Replace("{{DATE_OF_LAST_MEETING}}", Convert.ToDateTime(item.MeetingDate).ToString("dd/MM/yyyy"));

                                            _agendaItemConfirmation.AgendaFormat = tempAgendaFormat;
                                            _agendaItemConfirmation.MinutesFormat = tempMinutesFormat;

                                            entities.BM_MeetingAgendaMapping.Add(_agendaItemConfirmation);
                                            entities.SaveChanges();

                                            if (_agendaItemConfirmation.MeetingAgendaMappingID > 0)
                                            {
                                                var lstControls = (from row in entities.BM_AgendaMasterTemplate
                                                                   where row.AgendaID == _agendaItemConfirmation.AgendaID && row.IsDeleted == false
                                                                   select row
                                                                   ).ToList();
                                                if (lstControls != null)
                                                {
                                                    var meetingType = lstControls.Where(k => k.TemplateName == "TYPE_OF_LAST_MEETING").FirstOrDefault();
                                                    if (meetingType != null)
                                                    {
                                                        var _objTemplate = new BM_MeetingAgendaTemplateList();
                                                        _objTemplate.TemplateListValue = item.Name;
                                                        _objTemplate.TemplateID = meetingType.TemplateID;
                                                        _objTemplate.MeetingAgendaMappingID = _agendaItemConfirmation.MeetingAgendaMappingID;
                                                        _objTemplate.IsDeleted = false;
                                                        _objTemplate.CreatedBy = obj.UserId;
                                                        _objTemplate.CreatedOn = DateTime.Now;

                                                        entities.BM_MeetingAgendaTemplateList.Add(_objTemplate);
                                                        entities.SaveChanges();
                                                    }

                                                    var meetingNo = lstControls.Where(k => k.TemplateName == "NUMBER_OF_LAST_MEETING").FirstOrDefault();
                                                    if (meetingNo != null)
                                                    {
                                                        var _objTemplate = new BM_MeetingAgendaTemplateList();
                                                        _objTemplate.TemplateListValue = Convert.ToString(item.MeetingSrNo);
                                                        _objTemplate.TemplateID = meetingNo.TemplateID;
                                                        _objTemplate.MeetingAgendaMappingID = _agendaItemConfirmation.MeetingAgendaMappingID;
                                                        _objTemplate.IsDeleted = false;
                                                        _objTemplate.CreatedBy = obj.UserId;
                                                        _objTemplate.CreatedOn = DateTime.Now;

                                                        entities.BM_MeetingAgendaTemplateList.Add(_objTemplate);
                                                        entities.SaveChanges();
                                                    }

                                                    var meetingDate = lstControls.Where(k => k.TemplateName == "DATE_OF_LAST_MEETING").FirstOrDefault();
                                                    if (meetingDate != null && item.MeetingDate != null)
                                                    {
                                                        var _objTemplate = new BM_MeetingAgendaTemplateList();
                                                        _objTemplate.TemplateListValue = Convert.ToDateTime(item.MeetingDate).ToString("dd/MM/yyyy");
                                                        _objTemplate.TemplateID = meetingDate.TemplateID;
                                                        _objTemplate.MeetingAgendaMappingID = _agendaItemConfirmation.MeetingAgendaMappingID;
                                                        _objTemplate.IsDeleted = false;
                                                        _objTemplate.CreatedBy = obj.UserId;
                                                        _objTemplate.CreatedOn = DateTime.Now;

                                                        entities.BM_MeetingAgendaTemplateList.Add(_objTemplate);
                                                        entities.SaveChanges();
                                                    }
                                                }
                                            }

                                            entities.BM_MeetingMinutesDetails.Where(k => k.MeetingId == item.MeetingID).FirstOrDefault().ConfirmBoardMeetingId = obj.MeetingID;
                                            entities.SaveChanges();
                                        }

                                    }
                                }
                            }
                        }
                        #endregion

                        #region Add default agenda in case of virtual meeting if not added
                        if (obj.IsVirtualMeeting == true)
                        {
                            //Default Agenda Items -> Confirmation of minutes of the Board meeting / Same type of Meeting
                            if (isFinalizedMinutesAgendaItemAdded == false)
                            {
                                var agendaItemConfirmation = (from agenda in entities.BM_AgendaMaster
                                                              where agenda.EntityType == entity_type && agenda.MeetingTypeId == obj.MeetingTypeId &&
                                                                    agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.CONFIRMATION_OF_MINUTES && agenda.IsDeleted == false
                                                              select new AgendaItemSelect
                                                              {
                                                                  Meeting_Id = obj.MeetingID,
                                                                  BM_AgendaMasterId = agenda.BM_AgendaMasterId,
                                                                  PartID = agenda.PartId,
                                                                  Agenda = agenda.Agenda,
                                                                  IsCheked = true
                                                              }
                                                 ).FirstOrDefault();

                                if (agendaItemConfirmation != null)
                                {
                                    var Agenda = entities.BM_SP_AgendaFormatesByCustomerId(obj.CustomerId, agendaItemConfirmation.BM_AgendaMasterId).FirstOrDefault();

                                    SrNoNew++;

                                    var _agendaItemConfirmation = new BM_MeetingAgendaMapping();
                                    _agendaItemConfirmation.MeetingID = obj.MeetingID;
                                    _agendaItemConfirmation.AgendaID = agendaItemConfirmation.BM_AgendaMasterId;
                                    _agendaItemConfirmation.SrNo = SrNoNew;
                                    _agendaItemConfirmation.AgendaItemText = agendaItemConfirmation.Agenda;
                                    _agendaItemConfirmation.IsClosed = true;
                                    _agendaItemConfirmation.ResultRemark = null;
                                    _agendaItemConfirmation.IsActive = true;
                                    _agendaItemConfirmation.IsDeleted = false;

                                    _agendaItemConfirmation.PartId = 1;
                                    _agendaItemConfirmation.CreatedBy = obj.UserId;
                                    _agendaItemConfirmation.CreatedOn = DateTime.Now;

                                    if (Agenda != null)
                                    {
                                        _agendaItemConfirmation.AgendaItemText = Agenda.AgendaItemText;
                                        _agendaItemConfirmation.AgendaFormat = Agenda.AgendaFormat;

                                        _agendaItemConfirmation.ResolutionFormatHeading = Agenda.ResolutionFormatHeading;
                                        _agendaItemConfirmation.ResolutionFormat = Agenda.ResolutionFormat;

                                        _agendaItemConfirmation.MinutesFormatHeading = Agenda.MinutesFormatHeading;
                                        _agendaItemConfirmation.MinutesFormat = Agenda.MinutesFormat;
                                        _agendaItemConfirmation.MinutesDisApproveFormat = Agenda.MinutesDisApproveFormat;
                                        _agendaItemConfirmation.MinutesDifferFormat = Agenda.MinutesDifferFormat;
                                    }

                                    entities.BM_MeetingAgendaMapping.Add(_agendaItemConfirmation);
                                    entities.SaveChanges();


                                }
                            }

                            //Default Agenda Items -> Confirmation of minutes of the committee(s) meeting
                            if (isCommitteeFinalizedMinutesAgendaItemAdded == false && obj.MeetingTypeId == 10 && entity_type == 10 && IsQuarter == true)
                            {
                                var agendaItemConfirmationOfCommittee = (from agenda in entities.BM_AgendaMaster
                                                                         where agenda.EntityType == entity_type && agenda.MeetingTypeId == obj.MeetingTypeId &&
                                                                               agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.COMMITTEES_CONFIRMATION_OF_MINUTES && agenda.IsDeleted == false
                                                                         select new AgendaItemSelect
                                                                         {
                                                                             Meeting_Id = obj.MeetingID,
                                                                             BM_AgendaMasterId = agenda.BM_AgendaMasterId,
                                                                             PartID = agenda.PartId,
                                                                             Agenda = agenda.Agenda,
                                                                             IsCheked = true
                                                                         }
                                                 ).FirstOrDefault();

                                if (agendaItemConfirmationOfCommittee != null)
                                {
                                    var Agenda = entities.BM_SP_AgendaFormatesByCustomerId(obj.CustomerId, agendaItemConfirmationOfCommittee.BM_AgendaMasterId).FirstOrDefault();

                                    SrNoNew++;

                                    var _agendaItemConfirmation = new BM_MeetingAgendaMapping();
                                    _agendaItemConfirmation.MeetingID = obj.MeetingID;
                                    _agendaItemConfirmation.AgendaID = agendaItemConfirmationOfCommittee.BM_AgendaMasterId;
                                    _agendaItemConfirmation.SrNo = SrNoNew;
                                    _agendaItemConfirmation.AgendaItemText = agendaItemConfirmationOfCommittee.Agenda;
                                    _agendaItemConfirmation.IsClosed = true;
                                    _agendaItemConfirmation.ResultRemark = null;
                                    _agendaItemConfirmation.IsActive = true;
                                    _agendaItemConfirmation.IsDeleted = false;

                                    _agendaItemConfirmation.PartId = 1;
                                    _agendaItemConfirmation.CreatedBy = obj.UserId;
                                    _agendaItemConfirmation.CreatedOn = DateTime.Now;

                                    if (Agenda != null)
                                    {
                                        _agendaItemConfirmation.AgendaItemText = Agenda.AgendaItemText;
                                        _agendaItemConfirmation.AgendaFormat = Agenda.AgendaFormat;

                                        _agendaItemConfirmation.ResolutionFormatHeading = Agenda.ResolutionFormatHeading;
                                        _agendaItemConfirmation.ResolutionFormat = Agenda.ResolutionFormat;

                                        _agendaItemConfirmation.MinutesFormatHeading = Agenda.MinutesFormatHeading;
                                        _agendaItemConfirmation.MinutesFormat = Agenda.MinutesFormat;
                                        _agendaItemConfirmation.MinutesDisApproveFormat = Agenda.MinutesDisApproveFormat;
                                        _agendaItemConfirmation.MinutesDifferFormat = Agenda.MinutesDifferFormat;
                                    }

                                    entities.BM_MeetingAgendaMapping.Add(_agendaItemConfirmation);
                                    entities.SaveChanges();
                                }
                            }
                        }
                        #endregion

                        #endregion
                    }

                    //Generate ScheduleOn
                    objIMeetingCompliance.GenerateScheduleOn(obj.MeetingID, SecretarialConst.ComplianceMappingType.MEETING, obj.Entityt_Id, obj.CustomerId, obj.UserId, obj.IsEVoting);
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingVM Update(MeetingVM obj)
        {
            var calcualteScheduleOn = false;
            bool IsVideoMeeting = false;
            try
            {
                var _obj = (from row in entities.BM_Meetings
                            where row.MeetingID == obj.MeetingID && row.MeetingCircular == obj.Type && row.IsDeleted == false
                            select row
                           ).FirstOrDefault();

                if (obj.MeetingAddressType != null)
                {
                    if (obj.MeetingAddressType != "R" && obj.MeetingAddressType != "C")
                    {
                        obj.MeetingAddressType = "O";
                    }
                }

                if (_obj != null)
                {
                    if (_obj.IsAdjourned == true && _obj.MeetingTypeId == 10 && obj.MeetingDate != null)
                    {
                        var mDate = Convert.ToDateTime(obj.MeetingDate);

                        if ((mDate.Day == 26 && mDate.Month == 1) || (mDate.Day == 15 && mDate.Month == 8) || (mDate.Day == 2 && mDate.Month == 10))
                        {
                            obj.Error = true;
                            obj.Message = "A Board Meeting (Adjourned) can not be called on a National Holiday.";
                            return obj;
                        }
                    }

                    #region check Time in case Quarterly Listed meeting

                    if (_obj.Entity_Type == 10 && !string.IsNullOrEmpty(_obj.Quarter_) && _obj.MeetingCircular == SecretarialConst.MeetingCircular.MEETING && obj.MeetingDate != null && obj.MeetingTime != null)
                    {
                        var dependMeetingDateTime = entities.BM_SP_CheckMeetingTimeIsValid(_obj.EntityId, _obj.FY, _obj.Quarter_, _obj.MeetingTypeId).FirstOrDefault();
                        if (dependMeetingDateTime != null)
                        {
                            var actualMeetingDateTime = Convert.ToDateTime(Convert.ToDateTime(obj.MeetingDate).ToString("dd/MMM/yyyy") + " " + obj.MeetingTime);

                            if (_obj.MeetingTypeId == 10)
                            {
                                if (dependMeetingDateTime >= actualMeetingDateTime)
                                {
                                    obj.Error = true;
                                    obj.Message = "Meeting should be held after Committee Meeting.";
                                    return obj;
                                }
                            }
                            else if (dependMeetingDateTime <= actualMeetingDateTime)
                            {
                                obj.Error = true;
                                obj.Message = "Meeting should be held before Board Meeting.";
                                return obj;
                            }
                        }
                    }
                    #endregion

                    #region Check Serial number not duplicate in case of virtual meeting
                    if (obj.MeetingSrNo != null && _obj.IsVirtualMeeting == true)
                    {
                        bool SrNoIsExists = false;
                        int? temp_SrNo = null;
                        long? temp_FY = null;
                        var IsContinuosSrNo = true;

                        var config = (from row in entities.BM_ConfigurationMaster
                                      where row.MeetingType == _obj.MeetingTypeId && row.EntityID == _obj.EntityId && row.IsDeleted == false
                                      select new { row.LastMeetingNo, row.LastCircularNo, row.MeetingNoType, row.CircularNoType, row.MeetingFY, row.CircularFY }).FirstOrDefault();

                        if (config != null)
                        {
                            if (config.MeetingNoType == "FY")
                            {
                                IsContinuosSrNo = false;
                            }

                            if (obj.Type.Trim() == SecretarialConst.MeetingCircular.MEETING)
                            {
                                temp_SrNo = config.LastMeetingNo;
                                temp_FY = config.MeetingFY;
                            }
                            else if (obj.Type.Trim() == SecretarialConst.MeetingCircular.CIRCULAR)
                            {
                                temp_SrNo = config.LastCircularNo;
                                temp_FY = config.CircularFY;
                            }
                        }

                        if (IsContinuosSrNo)
                        {
                            //Continueous
                            SrNoIsExists = (from row in entities.BM_Meetings
                                            where row.MeetingTypeId == _obj.MeetingTypeId && row.EntityId == _obj.EntityId && row.MeetingCircular == _obj.MeetingCircular &&
                                            row.MeetingSrNo == obj.MeetingSrNo && row.MeetingID != _obj.MeetingID && row.IsDeleted == false
                                            select row.MeetingID).Any();
                        }
                        else
                        {
                            //Financial year-wise
                            SrNoIsExists = (from row in entities.BM_Meetings
                                            where row.MeetingTypeId == _obj.MeetingTypeId && row.EntityId == _obj.EntityId && row.MeetingCircular == _obj.MeetingCircular &&
                                            row.FY == _obj.FY &&
                                            row.MeetingSrNo == obj.MeetingSrNo && row.MeetingID != _obj.MeetingID && row.IsDeleted == false
                                            select row.MeetingID).Any();
                        }

                        if (SrNoIsExists)
                        {
                            obj.Error = true;
                            obj.Message = "Meeting allready exists with same Serial Number.";
                            return obj;
                        }
                    }
                    #endregion

                    #region Check start time & end time in case of virtual meeting
                    if (_obj.IsVirtualMeeting == true)
                    {
                        if (obj.MeetingDate != null && string.IsNullOrEmpty(obj.MeetingTime) == false && string.IsNullOrEmpty(obj.EndMeetingTime) == false)
                        {
                            var start = Convert.ToDateTime(Convert.ToDateTime(obj.MeetingDate).ToString("dd-MMM-yyyy") + " " + obj.MeetingTime);
                            var end = Convert.ToDateTime(Convert.ToDateTime(obj.MeetingDate).ToString("dd-MMM-yyyy") + " " + obj.EndMeetingTime);

                            if (start >= end)
                            {
                                obj.Error = true;
                                obj.Message = "Please check meeting start & end time.";
                                return obj;
                            }
                        }
                    }
                    #endregion

                    obj.CreatedDate = _obj.CreatedOn;

                    obj.SendDateAvailability = _obj.SendDateAvailability;
                    obj.SendDateAgenda = _obj.SendDateAgenda;
                    obj.SendDateNotice = _obj.SendDateNotice;

                    obj.IsSeekAvailabilitySent = _obj.IsSeekAvailabilitySent;
                    obj.IsAgendaSent = _obj.IsAgendaSent;
                    obj.IsNoticeSent = _obj.IsNoticeSent;
                    obj.Quarter_ = _obj.Quarter_;
                    obj.FYID = Convert.ToInt32(_obj.FY);

                    if (_obj.IsVirtualMeeting == true)
                    {
                        _obj.MeetingSrNo = obj.MeetingSrNo;
                        _obj.StartMeetingDate = obj.MeetingDate;
                        _obj.StartTime = obj.MeetingTime;
                        _obj.EndMeetingTime = obj.EndMeetingTime;

                        if (obj.MeetingSrNo != null)
                        {
                            var meetingTypeName = (from row in entities.BM_CommitteeComp
                                                   where row.Id == _obj.MeetingTypeId
                                                   select row.MeetingTypeName).FirstOrDefault();
                            var FYText = entities.BM_YearMaster.Where(k => k.FYID == _obj.FY).Select(k => k.FYText).FirstOrDefault();

                            _obj.MeetingTitle = (_obj.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.MEETING ? "Meeting - " : "Circular - ") + meetingTypeName + " [ " + _obj.MeetingSrNo + " ] " + FYText + (_obj.IsAdjourned == true ? " (Adjourned)" : "");
                        }
                        else
                        {
                            var meetingTypeName = (from row in entities.BM_CommitteeComp
                                                   where row.Id == _obj.MeetingTypeId
                                                   select row.MeetingTypeName).FirstOrDefault();

                            _obj.MeetingTitle = "Draft " + (_obj.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.MEETING ? "Meeting" : "Circular") + " " + meetingTypeName;
                        }
                    }

                    if (_obj.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.MEETING)
                    {
                        //_obj.MeetingTitle = obj.MeetingTitle;
                        //_obj.MeetingTypeId = obj.MeetingTypeId;
                        if (_obj.MeetingDate != obj.MeetingDate)
                        {
                            calcualteScheduleOn = true;
                        }
                        _obj.MeetingDate = obj.MeetingDate;
                        _obj.MeetingTime = obj.MeetingTime;
                        _obj.MeetingVenueType = obj.MeetingAddressType;
                        _obj.MeetingVenue = obj.MeetingVenue;
                        #region added by Ruchi on 14th dec 2020
                        _obj.IsVideoMeeting = obj.IsVideoMeeting;
                        #endregion
                        if (Convert.ToBoolean(_obj.IsSeekAvailabilitySent))
                        {
                            _obj.IsSeekAvailability = true;
                            obj.IsSeekAvailability = true;
                            obj.IsSeekAvailabilitySent = true;
                        }
                        else
                        {
                            _obj.IsSeekAvailability = obj.IsSeekAvailability;
                        }
                        _obj.Availability_ShortDesc = obj.Availability_ShortDesc;

                        #region Added on 14 May 2021
                        if(_obj.MeetingTypeId != SecretarialConst.MeetingTypeID.AGM && _obj.MeetingTypeId != SecretarialConst.MeetingTypeID.EGM)
                        {
                            _obj.IsShorter = obj.IsShorter;
                        }
                        #endregion

                        _obj.UpdatedBy = obj.UserId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = "Update Successfully.";

                        MeetingAgendaSetGenerationFlag(obj.MeetingID);

                        #region Check Last Board Meeting Date (Other than listed) & check maximum date in case quarter (Listed)
                        try
                        {
                            if (obj.MeetingTypeId == 10 && obj.MeetingDate != null && _obj.MeetingCircular == SecretarialConst.MeetingCircular.MEETING)
                            {
                                if (_obj.Entity_Type == 10)//Listed
                                {
                                    #region Listed
                                    if (!string.IsNullOrEmpty(_obj.Quarter_))
                                    {
                                        var MeetingDueDate = (from row in entities.BM_Committee_MeetingSchedule
                                                              where row.CommitteeId == _obj.MeetingTypeId && row.Entity_Type == _obj.Entity_Type && row.Quarter_ == _obj.Quarter_ && row.IsDeleted == false
                                                              select new
                                                              {
                                                                  row.Quarter_,
                                                                  row.Day_,
                                                              }).FirstOrDefault();

                                        var FY_StartDate = (from row in entities.BM_YearMaster
                                                            where row.FYID == _obj.FY
                                                            select row.FromDate).FirstOrDefault();

                                        if (MeetingDueDate != null && FY_StartDate.HasValue == true)
                                        {
                                            int quarter = 1;
                                            switch (_obj.Quarter_)
                                            {
                                                case "Q1":
                                                    quarter = 1;
                                                    break;
                                                case "Q2":
                                                    quarter = 2;
                                                    break;
                                                case "Q3":
                                                    quarter = 3;
                                                    break;
                                                case "Q4":
                                                    quarter = 4;
                                                    break;
                                                default:
                                                    break;
                                            }
                                            var minMeetingDate = Convert.ToDateTime(FY_StartDate).AddMonths(quarter * 3).AddDays(-1);
                                            var maxMeetingDate = minMeetingDate.AddDays(MeetingDueDate.Day_);

                                            if (minMeetingDate >= obj.MeetingDate || maxMeetingDate < obj.MeetingDate)
                                            {
                                                if (_obj.Quarter_ == "Q4")
                                                {
                                                    obj.Message = obj.Message + "<br>" + "Meeting should be held within " + MeetingDueDate.Day_ + " days from end of the quarter & financial year.";// + maxMeetingDate.ToString("dd MMM ") + ".";
                                                }
                                                else
                                                {
                                                    obj.Message = obj.Message + "<br>" + "Meeting should be held within " + MeetingDueDate.Day_ + " days from end of the quarter.";// + maxMeetingDate.ToString("dd MMM ") + ".";
                                                }
                                                obj.Error = true;
                                                obj.Success = false;
                                            }
                                        }
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Other than Listed
                                    var MeetingGapInDays = (from row in entities.BM_Committee_MeetingRule
                                                            where row.CommitteeId == _obj.MeetingTypeId && row.Entity_Type == _obj.Entity_Type && row.IsDeleted == false
                                                            select row.MeetingGapInDays).FirstOrDefault();
                                    if (MeetingGapInDays > 0)
                                    {
                                        DateTime? LastMeetingDate = null;
                                        var meetingDate = (from row in entities.BM_Meetings
                                                           where row.EntityId == _obj.EntityId && row.FY <= _obj.FY && row.MeetingCircular == SecretarialConst.MeetingCircular.MEETING && row.MeetingSrNo != null && row.IsDeleted == false
                                                           select row);

                                        if (_obj.MeetingSrNo == null)
                                        {
                                            LastMeetingDate = (from row in meetingDate
                                                               select row.MeetingDate).Max();
                                        }
                                        else
                                        {
                                            LastMeetingDate = (from row in meetingDate
                                                               where (row.FY < _obj.FY) || (row.FY == _obj.FY && row.MeetingSrNo < _obj.MeetingSrNo)
                                                               select row.MeetingDate).Max();
                                        }

                                        //Get Last Meeting date from Configuration master
                                        if (LastMeetingDate == null)
                                        {
                                            var config = (from row in entities.BM_ConfigurationMaster
                                                          where row.MeetingType == _obj.MeetingTypeId && row.EntityID == _obj.EntityId && row.IsDeleted == false
                                                          select new { row.LastMeetingDate, row.MeetingNoType, row.MeetingFY }).FirstOrDefault();

                                            if (config != null)
                                                LastMeetingDate = config.LastMeetingDate;
                                        }
                                        //End

                                        if (LastMeetingDate != null)
                                        {
                                            var maxMeetingDate = Convert.ToDateTime(LastMeetingDate).AddDays((int)MeetingGapInDays);
                                            if (maxMeetingDate <= Convert.ToDateTime(obj.MeetingDate))
                                            {
                                                obj.Message = obj.Message + "<br>" + "Gap between two meeting is not more than " + ((int)MeetingGapInDays) + " days.";
                                                obj.Error = true;
                                                obj.Success = false;
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                        #endregion

                        #region Comment by Ruchi on 09-03-2021
                        //bool IsforVideoConfrencing = false;
                        //IsforVideoConfrencing = IsVideoConferencingApplicable_CustomerWise(obj.CustomerId);
                        #endregion

                        if (obj.IsVideoMeeting)
                        {
                            string ProviderName = _objvideomeeting.GetProviderName(obj.CustomerId);
                            if (!string.IsNullOrEmpty(ProviderName))
                            {
                                string VMeetingId = _objvideomeeting.GetVMeetingId(obj.MeetingID);
                                var checkforzoomtoken = _objvideomeeting.GetToken_Zoom(obj.CustomerId);
                                if (checkforzoomtoken.token == null)
                                {
                                    if (ProviderName == SecretarialConst.Providers.Zoom)
                                    {
                                        _objvideomeeting.GenerateZoomjwtoken(obj.CustomerId);//Generate jwt for zoom
                                    }
                                    else if (ProviderName == SecretarialConst.Providers.Webex)
                                    {
                                        _objvideomeeting.GenerateCiscoToken(obj.CustomerId);//Generate jwt for zoom
                                    }
                                    else
                                    {
                                        obj.VMMessage = "Invalid provider name";
                                    }
                                }
                                else
                                {
                                    DateTime tokenexpidate = Convert.ToDateTime(checkforzoomtoken.ExpiryDate).Date;
                                    DateTime todaydate = DateTime.Now.Date;
                                    if (tokenexpidate <= todaydate)
                                    {
                                        if (ProviderName.ToLower() == SecretarialConst.Providers.Zoom)
                                        {
                                            _objvideomeeting.GenerateZoomjwtoken(obj.CustomerId);//Generate jwt for zoom  
                                        }
                                        else if (ProviderName.ToLower() == SecretarialConst.Providers.Webex)
                                        {
                                            _objvideomeeting.GenerateCiscoToken(obj.CustomerId);//Generate new jwt for cisco webex
                                        }
                                    }
                                }
                                if (checkforzoomtoken.token != null)
                                {
                                    checkforzoomtoken = _objvideomeeting.GetToken_Zoom(obj.CustomerId);
                                    #region Comment creating video meeting (Now creating v meeting in notice send)  --commented by Ruchi  on 05-03-2021
                                    //if (obj.IsVideoMeeting && string.IsNullOrEmpty(VMeetingId))
                                    //{
                                    //    if (ProviderName.ToLower() == SecretarialConst.Providers.Zoom)
                                    //    {
                                    //        obj.VMMessage = _objvideomeeting.CreateVideoMeeting(obj.MeetingID, obj.CustomerId, checkforzoomtoken.token,obj.UserId);
                                    //    }
                                    //    else if (ProviderName.ToLower() == SecretarialConst.Providers.Webex)
                                    //    {
                                    //        obj.VMMessage = _objvideomeeting.CreateVideoMeetingCisco(obj.MeetingID, obj.CustomerId, checkforzoomtoken.token,obj.UserId);
                                    //    }
                                    //    else
                                    //    {
                                    //        obj.VMMessage = "Invalid Provider Name";
                                    //    }
                                    //}
                                    #endregion

                                    if (obj.IsVideoMeeting && !string.IsNullOrEmpty(VMeetingId))
                                    {
                                        if (ProviderName.ToLower() == SecretarialConst.Providers.Zoom)
                                        {
                                            obj.VMMessage = _objvideomeeting.UpdateVideoMeeting(obj.MeetingID, obj.CustomerId, checkforzoomtoken.token, obj.UserId);
                                        }
                                        else if (ProviderName.ToLower() == SecretarialConst.Providers.Webex)
                                        {
                                            obj.VMMessage = _objvideomeeting.UpdateVideoMeetingCisco(obj.MeetingID, obj.CustomerId, checkforzoomtoken.token, obj.UserId);
                                        }
                                        else
                                        {
                                            obj.VMMessage = "Invalid Provider Name";
                                        }
                                    }
                                    else if ((!obj.IsVideoMeeting) && !string.IsNullOrEmpty(VMeetingId))
                                    {
                                        if (ProviderName.ToLower() == SecretarialConst.Providers.Zoom)
                                        {
                                            obj.VMMessage = _objvideomeeting.DeleteVideoMeeting(obj.MeetingID, obj.CustomerId, checkforzoomtoken.token, obj.UserId);
                                        }
                                        else if (ProviderName.ToLower() == SecretarialConst.Providers.Webex)
                                        {
                                            obj.VMMessage = _objvideomeeting.DeleteVideoMeetingCisco(obj.MeetingID, obj.CustomerId, checkforzoomtoken.token, obj.UserId);
                                        }
                                        else
                                        {
                                            obj.VMMessage = "Invalid Provider Name";
                                        }
                                    }
                                }
                                else
                                {
                                    obj.VMMessage = "Token not found";
                                }
                            }
                            else
                            {
                                obj.VMMessage = "Video conferencing provider not found.";
                            }
                        }
                    }
                    else if (_obj.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.CIRCULAR)
                    {
                        if (obj.CircularMeetingDetails.DueDate.HasValue && obj.CircularMeetingDetails.CircularDate.HasValue)
                        {
                            if (Convert.ToDateTime(obj.CircularMeetingDetails.DueDate) < Convert.ToDateTime(obj.CircularMeetingDetails.CircularDate))
                            {
                                obj.Error = true;
                                obj.Message = "Due Date must be greater than Circular Date";
                                return obj;
                            }
                            var days = Convert.ToDateTime(obj.CircularMeetingDetails.DueDate).Subtract(Convert.ToDateTime(obj.CircularMeetingDetails.CircularDate)).Days;
                            if (days >= 8)
                            {
                                obj.Error = true;
                                obj.Message = "Gap between due date and circular date not more than 7 days.";
                                return obj;
                            }
                        }
                        //_obj.MeetingTitle = obj.CircularMeetingDetails.Title;
                        _obj.MeetingDate = obj.CircularMeetingDetails.DueDate;
                        _obj.CircularDate = obj.CircularMeetingDetails.CircularDate;
                        _obj.SendDateNotice = obj.CircularMeetingDetails.CircularDate;
                        _obj.MeetingTime = obj.CircularMeetingDetails.CircularDueTime;
                        _obj.UpdatedBy = obj.UserId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                        obj.Success = true;
                        obj.Message = "Update Successfully";

                        MeetingAgendaSetGenerationFlag(obj.MeetingID);
                    }

                    if (obj.Success)
                    {
                        #region Check configuration master saved or not
                        var meetingCount = (from row in entities.BM_Meetings
                                            where row.EntityId == _obj.EntityId
                                            && row.MeetingCircular == _obj.MeetingCircular.Trim()
                                            && row.MeetingTypeId == _obj.MeetingTypeId
                                            && row.IsDeleted == false
                                            select row.MeetingID).Count();
                        if (meetingCount <= 1)
                        {
                            var query = (from row in entities.BM_ConfigurationMaster
                                         where row.EntityID == obj.Entityt_Id && row.MeetingType == _obj.MeetingTypeId &&
                                         row.IsDeleted == false && row.IsActive == true
                                         select row);
                            if (obj.Type == SecretarialConst.MeetingCircular.CIRCULAR)
                            {
                                query = query.Where(k => k.IsCircularMeeting == true);
                            }

                            obj.IsConfigurationMasterSaved = query.Any();
                        }
                        else
                        {
                            obj.IsConfigurationMasterSaved = true;
                        }
                        #endregion
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            if (calcualteScheduleOn)
            {
                objIMeetingCompliance.CalculateScheduleDate(obj.MeetingID, obj.UserId);
            }
            return obj;
        }
        #endregion

        #region Upcoming Meetings
        public List<MeetingListVM> GetUpcomingMeetings(long CustomerId, int UserId, string Role)
        {
            var result = new List<MeetingListVM>();
            try
            {
                result = (from row in entities.BM_SP_MyMeetingsUpcoming((int)CustomerId, UserId, Role)
                          select new MeetingListVM
                          {
                              EntityId = row.EntityId,
                              EntityName = row.EntityName,

                              MeetingID = row.MeetingId,
                              MeetingTypeName = row.MeetingTypeName,
                              MeetingSrNo = row.MeetingSrNo,
                              MeetingTitle = row.MeetingTitle,
                              IsShorter = row.IsShorter,

                              MeetingDate = row.MeetingDate,
                              MeetingTime = row.MeetingTime,
                              MeetingVenue = row.MeetingVenue,
                              MeetingstartDate = row.MeetingStartDate,

                              Day = row.Day_,
                              DayName = row.DayName_,
                              Month = row.Month_,
                              YearName = row.Year_,
                              FY = row.FYText,

                              participantId = (long)row.ParticipantId,
                              RSPV = row.RSPV,
                              ReasonforRSPV = row.ReasonforRSPV
                          }).ToList();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        //public List<MeetingListVM> GetUpcomingMeetings(long CustomerId, int UserId, string Role)
        //{
        //    try
        //    {
        //        IEnumerable<MeetingVM> meetings = null;
        //        var result = new List<MeetingListVM>();
        //        var today = DateTime.Today;

        //        if (Role == SecretarialConst.Roles.CS)
        //        {
        //            meetings = (from row in entities.BM_Meetings
        //                        join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
        //                        join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
        //                        join view in entities.BM_AssignedEntitiesView on entity.Id equals view.EntityId
        //                        where row.Customer_Id == CustomerId && row.IsDeleted == false && view.userId == UserId && entity.Is_Deleted == false
        //                        && row.MeetingDate > today && row.IsCompleted == false && row.IsNoticeSent == true
        //                        && row.MeetingCircular == "M"
        //                        orderby row.MeetingDate
        //                        select new MeetingVM
        //                        {
        //                            MeetingID = row.MeetingID,
        //                            Type = row.MeetingCircular.Trim(),
        //                            MeetingSrNo = row.MeetingSrNo,
        //                            MeetingTypeId = row.MeetingTypeId,
        //                            MeetingTypeName = meeting.MeetingTypeName,
        //                            Quarter_ = row.Quarter_,
        //                            MeetingTitle = row.MeetingTitle,
        //                            MeetingDate = row.MeetingDate,
        //                            MeetingTime = row.MeetingTime,
        //                            MeetingVenue = row.MeetingVenue,
        //                            IsSeekAvailability = row.IsSeekAvailability,
        //                            Availability_ShortDesc = row.Availability_ShortDesc,
        //                            TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
        //                            Entityt_Id = row.EntityId,
        //                            EntityName = entity.CompanyName,
        //                            FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
        //                            Stage = row.IsCompleted == true ? "Completed" : row.IsNoticeSent == true ? (row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past") : "Draft",

        //                            IsNoticeSent = row.IsNoticeSent,
        //                            IsMeetingCompleted = row.IsCompleted,
        //                            IsComplianceClosed = row.IsComplianceClosed,
        //                            IsAdjourned = row.IsAdjourned,

        //                        }).ToList();
        //        }
        //        else if (Role == SecretarialConst.Roles.DADMN)
        //        {
        //            meetings = (from row in entities.BM_Meetings
        //                        join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
        //                        join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
        //                        join cust in entities.Customers on entity.Customer_Id equals cust.ID
        //                        where (cust.ParentID == CustomerId)
        //                        && row.IsDeleted == false
        //                        && entity.Is_Deleted == false
        //                        && row.MeetingDate > today
        //                        && row.IsCompleted == false
        //                        && row.IsNoticeSent == true
        //                        && row.MeetingCircular == "M"
        //                        orderby row.MeetingDate
        //                        select new MeetingVM
        //                        {
        //                            MeetingID = row.MeetingID,
        //                            Type = row.MeetingCircular.Trim(),
        //                            MeetingSrNo = row.MeetingSrNo,
        //                            MeetingTypeId = row.MeetingTypeId,
        //                            MeetingTypeName = meeting.MeetingTypeName,
        //                            Quarter_ = row.Quarter_,
        //                            MeetingTitle = row.MeetingTitle,
        //                            MeetingDate = row.MeetingDate,
        //                            MeetingTime = row.MeetingTime,
        //                            MeetingVenue = row.MeetingVenue,
        //                            IsSeekAvailability = row.IsSeekAvailability,
        //                            Availability_ShortDesc = row.Availability_ShortDesc,
        //                            TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
        //                            Entityt_Id = row.EntityId,
        //                            EntityName = entity.CompanyName,
        //                            FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
        //                            Stage = row.IsCompleted == true ? "Completed" : row.IsNoticeSent == true ? (row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past") : "Draft",
        //                            IsNoticeSent = row.IsNoticeSent,
        //                            IsMeetingCompleted = row.IsCompleted,
        //                            IsComplianceClosed = row.IsComplianceClosed,
        //                            IsAdjourned = row.IsAdjourned,
        //                        }).ToList();
        //        }
        //        else if (Role == SecretarialConst.Roles.CSMGR)
        //        {
        //            var lstCustIDs = SecretarialManagement.GetAll_AssignedCustomerIDs(UserId, -1, Convert.ToInt32(CustomerId), Convert.ToInt32(CustomerId), "CSMGR", false);

        //            meetings = (from row in entities.BM_Meetings
        //                        join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
        //                        join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
        //                        join cust in entities.Customers on entity.Customer_Id equals cust.ID
        //                        where (lstCustIDs.Contains(cust.ID))
        //                        && row.IsDeleted == false
        //                        && entity.Is_Deleted == false
        //                        && row.MeetingDate > today
        //                        && row.IsCompleted == false
        //                        && row.IsNoticeSent == true
        //                        && row.MeetingCircular == "M"
        //                        orderby row.MeetingDate
        //                        select new MeetingVM
        //                        {
        //                            MeetingID = row.MeetingID,
        //                            Type = row.MeetingCircular.Trim(),
        //                            MeetingSrNo = row.MeetingSrNo,
        //                            MeetingTypeId = row.MeetingTypeId,
        //                            MeetingTypeName = meeting.MeetingTypeName,
        //                            Quarter_ = row.Quarter_,
        //                            MeetingTitle = row.MeetingTitle,
        //                            MeetingDate = row.MeetingDate,
        //                            MeetingTime = row.MeetingTime,
        //                            MeetingVenue = row.MeetingVenue,
        //                            IsSeekAvailability = row.IsSeekAvailability,
        //                            Availability_ShortDesc = row.Availability_ShortDesc,
        //                            TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
        //                            Entityt_Id = row.EntityId,
        //                            EntityName = entity.CompanyName,
        //                            FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
        //                            Stage = row.IsCompleted == true ? "Completed" : row.IsNoticeSent == true ? (row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past") : "Draft",
        //                            IsNoticeSent = row.IsNoticeSent,
        //                            IsMeetingCompleted = row.IsCompleted,
        //                            IsComplianceClosed = row.IsComplianceClosed,
        //                            IsAdjourned = row.IsAdjourned,
        //                        }).ToList();
        //        }
        //        else
        //        {
        //            meetings = (from row in entities.BM_Meetings
        //                        join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
        //                        join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
        //                        where row.Customer_Id == CustomerId && row.IsDeleted == false && entity.Is_Deleted == false
        //                        && row.MeetingDate > today && row.IsCompleted == false && row.IsNoticeSent == true
        //                        && row.MeetingCircular == "M"
        //                        orderby row.MeetingDate
        //                        select new MeetingVM
        //                        {
        //                            MeetingID = row.MeetingID,
        //                            Type = row.MeetingCircular.Trim(),
        //                            MeetingSrNo = row.MeetingSrNo,
        //                            MeetingTypeId = row.MeetingTypeId,
        //                            MeetingTypeName = meeting.MeetingTypeName,
        //                            Quarter_ = row.Quarter_,
        //                            MeetingTitle = row.MeetingTitle,
        //                            MeetingDate = row.MeetingDate,
        //                            MeetingTime = row.MeetingTime,
        //                            MeetingVenue = row.MeetingVenue,
        //                            IsSeekAvailability = row.IsSeekAvailability,
        //                            Availability_ShortDesc = row.Availability_ShortDesc,
        //                            TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
        //                            Entityt_Id = row.EntityId,
        //                            EntityName = entity.CompanyName,
        //                            FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
        //                            Stage = row.IsCompleted == true ? "Completed" : row.IsNoticeSent == true ? (row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past") : "Draft",
        //                            IsNoticeSent = row.IsNoticeSent,
        //                            IsMeetingCompleted = row.IsCompleted,
        //                            IsComplianceClosed = row.IsComplianceClosed,
        //                            IsAdjourned = row.IsAdjourned,

        //                        }).ToList();
        //        }

        //        if (meetings != null)
        //        {
        //            result = (from r in meetings
        //                      orderby r.MeetingDate
        //                      select new MeetingListVM
        //                      {
        //                          MeetingID = r.MeetingID,
        //                          MeetingTypeName = r.MeetingTypeName,
        //                          MeetingSrNo = r.MeetingSrNo,
        //                          MeetingTitle = r.MeetingTitle,
        //                          EntityName = r.EntityName,
        //                          IsShorter = r.IsShorter,
        //                          MeetingDate = r.MeetingDate,
        //                          Day = r.MeetingDate.HasValue ? r.MeetingDate.Value.Day.ToString() : "",
        //                          DayName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("dddd") : "",
        //                          Month = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("MMM") : "",
        //                          MeetingTime = r.MeetingTime,
        //                          MeetingVenue = r.MeetingVenue,
        //                          Stage = r.Stage,
        //                          EntityId = r.Entityt_Id,
        //                          MeetingstartDate = r.StartMeetingDate,
        //                          FY = r.FY_CY
        //                      }).ToList();
        //        }

        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return null;
        //    }
        //}
        #endregion

        #region Meeting For Dashboard
        public List<DraftMeetingListVM> GetDraftMeetingsForDirectorApproval(int customerId, int userId)
        {
            var result = new List<DraftMeetingListVM>();
            try
            {
                result = (from row in entities.BM_SP_MyMeetingsDraftMinutes(customerId, userId)
                          select new DraftMeetingListVM
                          {
                              MeetingID = row.MeetingID,
                              MeetingSrNo = row.MeetingSrNo,
                              MeetingTypeName = row.MeetingTypeName,
                              MeetingTypeId = row.MeetingTypeId,

                              Quarter_ = row.Quarter_,
                              MeetingTitle = row.MeetingTitle,
                              MeetingDate = row.MeetingDate,
                              Day = row.Day_,
                              DayName = row.Year_,
                              Month = row.Month_,

                              MeetingTime = row.MeetingTime,
                              MeetingVenue = row.MeetingVenue,
                              EntityName = row.CompanyName,
                              EntityId = row.EntityId,

                              participantId = row.MeetingParticipantId,
                              DraftCirculationID = row.DraftCirculationID,
                              MinutesDetailsID = row.MinutesDetailsID,
                              DateOfDraftCirculation = row.DateOfDraftCirculation,
                              DueDateOfDraftCirculation = row.DueDateOfDraftCirculation,

                              IsCirculate = row.IsCirculate,
                              IsFinalized = row.IsFinalized,

                              IsDelivered = row.IsDelivered,
                              IsRead = row.IsRead,
                              IsApproved = row.IsApproved,
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<DraftMeetingListVM> GetDraftMeetingsForDirectorApproval(int userID)
        {
            var result = new List<DraftMeetingListVM>();
            try
            {
                var today = DateTime.Today;
                var meetings = (from row in entities.BM_Meetings
                                join minutes in entities.BM_MeetingMinutesDetails on row.MeetingID equals minutes.MeetingId
                                join participant in entities.BM_MeetingParticipant on row.MeetingID equals participant.Meeting_ID
                                join log in entities.BM_MeetingMinutesDetailsTransaction on participant.MeetingParticipantId equals log.MeetingParticipantId
                                join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                                join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                where participant.UserId == userID && minutes.IsFinalized == false && minutes.IsCirculate == true && minutes.DueDateOfDraftCirculation >= today &&
                                row.IsDeleted == false && row.IsNoticeSent == true
                                 && participant.IsDeleted == false && participant.IsInvited == null
                                select new
                                {
                                    IsCirculate = minutes.IsCirculate,
                                    IsFinalized = minutes.IsFinalized,

                                    MeetingID = row.MeetingID,
                                    MeetingSrNo = row.MeetingSrNo,
                                    MeetingTypeName = meeting.MeetingTypeName,
                                    MeetingTypeId = row.MeetingTypeId,

                                    Quarter_ = row.Quarter_,
                                    MeetingTitle = row.MeetingTitle,
                                    MeetingDate = row.MeetingDate,

                                    MeetingTime = row.MeetingTime,
                                    MeetingVenue = row.MeetingVenue,
                                    EntityName = entity.CompanyName,
                                    EntiyId = row.EntityId,
                                    Stage = row.MeetingDate == null ? "" : row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Pending",


                                    DraftCirculationID = log.ID,
                                    MinutesDetailsID = minutes.Id,
                                    DateOfDraftCirculation = minutes.DateOfDraftCirculation,
                                    DueDateOfDraftCirculation = minutes.DueDateOfDraftCirculation,

                                    IsDelivered = log.DeliveredOn == null ? false : true,
                                    IsRead = log.ReadOn == null ? false : true,
                                    IsApproved = log.ApprovedOn == null ? false : true,
                                }).ToList();


                if (meetings != null)
                {
                    result = (from row in meetings
                              select new DraftMeetingListVM
                              {

                                  MeetingID = row.MeetingID,
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeName = row.MeetingTypeName,
                                  MeetingTypeId = row.MeetingTypeId,

                                  Quarter_ = row.Quarter_,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  Day = row.MeetingDate.HasValue ? row.MeetingDate.Value.Day.ToString() : "",
                                  DayName = row.MeetingDate.HasValue ? row.MeetingDate.Value.ToString("dddd") : "",
                                  Month = row.MeetingDate.HasValue ? row.MeetingDate.Value.ToString("MMM") : "",

                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  EntityName = row.EntityName,
                                  EntityId = row.EntiyId,
                                  Stage = row.Stage,


                                  DraftCirculationID = row.DraftCirculationID,
                                  MinutesDetailsID = row.MinutesDetailsID,
                                  DateOfDraftCirculation = row.DateOfDraftCirculation,
                                  DueDateOfDraftCirculation = row.DueDateOfDraftCirculation,

                                  IsDelivered = row.IsDelivered,
                                  IsRead = row.IsRead,
                                  IsApproved = row.IsApproved,

                              }).ToList();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<DraftMeetingListVM> GetConcludedMeetingsForDirector1(int userID)
        {
            var result = new List<DraftMeetingListVM>();
            try
            {
                var today = DateTime.Today;

                var meetings = (from row in entities.BM_Meetings
                                join participant in entities.BM_MeetingParticipant on row.MeetingID equals participant.Meeting_ID
                                join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                                join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                //from minutes in entities.BM_MeetingMinutesDetails.Where( k=> k.MeetingId == row.MeetingID).DefaultIfEmpty()
                                from minutes in entities.BM_MeetingMinutesDetails.Where(k => k.MeetingId == row.MeetingID).DefaultIfEmpty()
                                from log in entities.BM_MeetingMinutesDetailsTransaction.Where(k => k.MeetingParticipantId == participant.MeetingParticipantId).DefaultIfEmpty()
                                where participant.UserId == userID && row.IsCompleted == true && //minutes.IsFinalized == true &&
                                row.IsDeleted == false && row.IsNoticeSent == true && participant.IsDeleted == false && participant.IsInvited == null
                                select new
                                {
                                    minutes.IsCirculate,
                                    minutes.IsFinalized,

                                    MeetingID = row.MeetingID,
                                    MeetingSrNo = row.MeetingSrNo,
                                    MeetingTypeName = meeting.MeetingTypeName,
                                    MeetingTypeId = row.MeetingTypeId,

                                    Quarter_ = row.Quarter_,
                                    MeetingTitle = row.MeetingTitle,
                                    MeetingDate = row.MeetingDate,

                                    MeetingTime = row.MeetingTime,
                                    MeetingVenue = row.MeetingVenue,
                                    EntityName = entity.CompanyName,
                                    EntiyId = row.EntityId,
                                    Stage = row.MeetingDate == null ? "" : row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Pending",

                                    ParticipantId = participant.MeetingParticipantId,
                                    DraftCirculationID = log.ID,
                                    MinutesDetailsID = minutes.Id,
                                    DateOfDraftCirculation = minutes.DateOfDraftCirculation,
                                    DueDateOfDraftCirculation = minutes.DueDateOfDraftCirculation,

                                    IsDelivered = log.DeliveredOn == null ? false : true,
                                    IsRead = log.ReadOn == null ? false : true,
                                    IsApproved = log.ApprovedOn == null ? false : true,
                                }).ToList();


                if (meetings != null)
                {
                    result = (from row in meetings
                              select new DraftMeetingListVM
                              {

                                  MeetingID = row.MeetingID,
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeName = row.MeetingTypeName,
                                  MeetingTypeId = row.MeetingTypeId,

                                  Quarter_ = row.Quarter_,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  Day = row.MeetingDate.HasValue ? row.MeetingDate.Value.Day.ToString() : "",
                                  DayName = row.MeetingDate.HasValue ? row.MeetingDate.Value.ToString("dddd") : "",
                                  Month = row.MeetingDate.HasValue ? row.MeetingDate.Value.ToString("MMM") : "",

                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  EntityName = row.EntityName,
                                  EntityId = row.EntiyId,
                                  Stage = row.Stage,

                                  participantId = row.ParticipantId,
                                  DraftCirculationID = row.DraftCirculationID,
                                  MinutesDetailsID = row.MinutesDetailsID,
                                  DateOfDraftCirculation = row.DateOfDraftCirculation,
                                  DueDateOfDraftCirculation = row.DueDateOfDraftCirculation,

                                  IsCirculate = row.IsCirculate,
                                  IsFinalized = row.IsFinalized,

                                  IsDelivered = row.IsDelivered,
                                  IsRead = row.IsRead,
                                  IsApproved = row.IsApproved,

                              }).ToList();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<DraftMeetingListVM> GetConcludedMeetingsForDirector(int customerId, int userId)
        {
            var result = new List<DraftMeetingListVM>();
            try
            {
                result = (from row in entities.BM_SP_MyMeetingsConcluded(customerId, userId)
                          where row.MeetingCircular == "M"
                          orderby row.MeetingID descending
                          select new DraftMeetingListVM
                          {
                              MeetingID = row.MeetingID,
                              MeetingSrNo = row.MeetingSrNo,
                              IsAdjourned = row.IsAdjourned,
                              MeetingTypeName = row.MeetingTypeName,
                              MeetingTypeId = row.MeetingTypeId,

                              Quarter_ = row.Quarter_,
                              MeetingTitle = row.MeetingTitle,
                              MeetingDate = row.MeetingDate,
                              Day = row.Day_,
                              DayName = row.Year_,
                              Month = row.Month_,

                              MeetingTime = row.MeetingTime,
                              MeetingVenue = row.MeetingVenue,
                              EntityName = row.CompanyName,
                              EntityId = row.EntityId,

                              participantId = row.MeetingParticipantId,
                              DraftCirculationID = (long)row.DraftCirculationID,
                              MinutesDetailsID = (long)row.MinutesDetailsID,
                              DateOfDraftCirculation = row.DateOfDraftCirculation,
                              DueDateOfDraftCirculation = row.DueDateOfDraftCirculation,

                              IsCirculate = row.IsCirculate,
                              IsFinalized = row.IsFinalized,

                              IsDelivered = row.IsDelivered,
                              IsRead = row.IsRead,
                              IsApproved = row.IsApproved,

                              FY = row.FYText,
                              Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                              Present = row.Present,
                              StartMeetingTime = row.StartTime,
                              EndMeetingTime = row.EndMeetingTime,
                              row_num = row.row_num
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Delete Meetings
        public DeleteMeetingVM GetDetailsMeetingCanDelete(long meetingId)
        {
            var obj = new DeleteMeetingVM() { DeleteMeetingId = meetingId };
            obj.lstAgendaItems_ = new List<DeleteAgendaVM>();
            bool flag = true;
            try
            {
                var isConclude = (from row in entities.BM_Meetings
                                  where row.MeetingID == meetingId && row.IsVirtualMeeting == false && row.IsCompleted == true
                                  select row.MeetingID).Any();
                if (isConclude == true)
                {
                    flag = false;
                    obj.MessageDetails = "You can not delete once it is conclude.";
                }

                var agendaItems = (from row in entities.BM_MeetingAgendaMapping
                                   where row.MeetingID == meetingId && row.IsDeleted == false && row.IsActive == true
                                   select new
                                   {
                                       row.MeetingAgendaMappingID,
                                       row.AgendaItemTextNew
                                   }).ToList();

                foreach (var item in agendaItems)
                {
                    var delAgenda = (from row in entities.BM_SP_MeetingAgendaAllowDelete(meetingId, item.MeetingAgendaMappingID, true)
                                     select new
                                     {
                                         row.AllowDelete,
                                         row.Message
                                     }).FirstOrDefault();

                    if (delAgenda != null)
                    {
                        if (delAgenda.AllowDelete == false)
                        {
                            flag = false;
                            obj.lstAgendaItems_.Add(new DeleteAgendaVM()
                            {
                                MeetingAgendaMappingId_del = item.MeetingAgendaMappingID,
                                AgendaItem_ = item.AgendaItemTextNew,
                                AllowDelete = false,
                                MessageDetails = delAgenda.Message
                            });
                        }
                    }
                }

                if (flag)
                {
                    obj.MessageDetails = "Are you sure you want to delete this Meeting ?";
                }
                obj.AllowDeleteMeeting = flag;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public DeleteMeetingVM DeleteMeeting(DeleteMeetingVM obj, int userId, int CustomerId)
        {
            bool IsforVideoConfrencing = false;
            string ProviderName = "";
            string deletevmeeting = "";
            try
            {
                #region Delete all Agenda Items
                var agendaItems = (from row in entities.BM_MeetingAgendaMapping
                                   where row.MeetingID == obj.DeleteMeetingId && row.IsDeleted == false && row.IsActive == true
                                   select new
                                   {
                                       row.MeetingAgendaMappingID
                                   }).ToList();

                foreach (var item in agendaItems)
                {
                    var resultDeleteAgenda = (from row in entities.BM_SP_MeetingAgendaDelete(obj.DeleteMeetingId, item.MeetingAgendaMappingID, true, userId)
                                              select row).FirstOrDefault();
                }
                #endregion

                var result = (from row in entities.BM_SP_MeetingDelete(obj.DeleteMeetingId, userId)
                              select row).FirstOrDefault();
                if (result != null)
                {
                    if (result.Success == true)
                    {

                        IsforVideoConfrencing = IsVideoConferencingApplicable_CustomerWise(CustomerId);
                        var Tokendtls = _objvideomeeting.GetToken_Zoom(CustomerId);
                        ProviderName = _objvideomeeting.GetProviderName(CustomerId);
                        obj.AllowDeleteMeeting = false;
                        obj.Success = true;
                        obj.Message = "Meeting Deleted Successfully.";
                        if (IsforVideoConfrencing)
                        {
                            if (!string.IsNullOrEmpty(ProviderName))
                            {
                                if (ProviderName.ToLower() == SecretarialConst.Providers.Zoom)
                                {
                                    deletevmeeting = _objvideomeeting.DeleteVideoMeeting(obj.DeleteMeetingId, CustomerId, Tokendtls.token, userId);
                                }
                                else if (ProviderName.ToLower() == SecretarialConst.Providers.Webex)
                                {
                                    deletevmeeting = _objvideomeeting.DeleteVideoMeetingCisco(obj.DeleteMeetingId, CustomerId, Tokendtls.token, userId);
                                }
                            }
                        }

                    }
                    else
                    {
                        obj.AllowDeleteMeeting = false;
                        obj.Error = true;
                        obj.Message = "Something wents wrong.";
                    }
                }
                else
                {
                    obj.AllowDeleteMeeting = false;
                    obj.Error = true;
                    obj.Message = "Something wents wrong.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Meeting Postponded
        public Message Postpond(long MeetingId, int postpondedBy)
        {
            var result = new Message();
            bool isEvoting = false;
            try
            {
                var checkExternalComplianceClosed = (from row in entities.BM_ComplianceScheduleOnNew
                                                     join com in entities.BM_AdditionalCompliance on row.ComplianceID equals com.ComplianceId
                                                     where row.MeetingID == MeetingId && row.IsDeleted == false && row.IsActive == true && com.IsActive == true &&
                                                     (row.StatusId == 4 || row.StatusId == 5)
                                                     select row.Id).Any();
                if (checkExternalComplianceClosed == true)
                {
                    result.Error = true;
                    result.Message = "Meeting can not postpone, external compliance(s) are closed.";
                    return result;
                }

                var _obj = entities.BM_Meetings.Where(k => k.MeetingID == MeetingId && k.IsDeleted == false).FirstOrDefault();
                if (_obj != null)
                {
                    var meetingTypeName = (from row in entities.BM_CommitteeComp
                                           where row.Id == _obj.MeetingTypeId
                                           select row.MeetingTypeName).FirstOrDefault();

                    var obj = new BM_MeetingPostpondedLog();

                    obj.MeetingID = _obj.MeetingID;
                    obj.MeetingDate = _obj.MeetingDate;
                    obj.MeetingTime = _obj.MeetingTime;
                    obj.MeetingVenue = _obj.MeetingVenue;

                    obj.SendDateAgenda = _obj.SendDateAgenda;
                    obj.SendDateNotice = _obj.SendDateNotice;
                    obj.CreatedBy = postpondedBy;
                    obj.CreatedOn = DateTime.Now;

                    entities.BM_MeetingPostpondedLog.Add(obj);
                    entities.SaveChanges();

                    _obj.MeetingDate = null;
                    _obj.MeetingTime = null;
                    _obj.EndMeetingTime = null;
                    _obj.IsPostponded = true;
                    _obj.IsNoticeSent = false;
                    //_obj.MeetingTitle = "Draft " + (_obj.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.MEETING ? "Meeting" : "Circular") + " " + meetingTypeName;
                    _obj.UpdatedBy = postpondedBy;
                    _obj.UpdatedOn = DateTime.Now;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    isEvoting = _obj.IsEVoting;
                    //Move Any Other business Agenda Items to Part B
                    MoveAnyOtherAgendaItems(MeetingId);

                    #region Generate Compliance Schedule On
                    var resultComplianceDeactivate = (from row in entities.BM_SP_MeetingComplianceDeactivate(obj.MeetingID, postpondedBy)
                                                      select row).FirstOrDefault();
                    if (resultComplianceDeactivate != null)
                    {
                        if (resultComplianceDeactivate.Success == true)
                        {
                            objIMeetingCompliance.GenerateScheduleOn(obj.MeetingID, SecretarialConst.ComplianceMappingType.MEETING, _obj.EntityId, (int)_obj.Customer_Id, postpondedBy, isEvoting);
                            objIMeetingCompliance.GenerateScheduleOn(obj.MeetingID, SecretarialConst.ComplianceMappingType.AGENDA, (int)_obj.Customer_Id, postpondedBy);
                        }
                    }
                    #endregion

                    result.Success = true;
                    result.Message = "Meeting postpone successfully";
                }
                else
                {
                    result.Error = true;
                    result.Message = "Something wents wrong";
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion
        #region Virtual Meeting
        public Message ReOpenVirtualMeeting(long meetingId, int userId)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities ctx = new Compliance_SecretarialEntities())
                {
                    var meeting = (from row in ctx.BM_Meetings
                                   where row.MeetingID == meetingId
                                   && row.IsDeleted == false
                                   && row.IsVirtualMeeting == true
                                   && row.IsCompleted == true
                                   select row).FirstOrDefault();
                    if (meeting != null)
                    {
                        meeting.IsCompleted = false;
                        meeting.UpdatedBy = userId;
                        meeting.UpdatedOn = DateTime.Now;

                        ctx.Entry(meeting).State = System.Data.Entity.EntityState.Modified;
                        ctx.SaveChanges();
                    }
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Server Error Occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Meeting Participants
        public List<MeetingParticipants_ResultVM> GetMeetingParticipants(long Meeting_Id)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetMeetingParticipants(Meeting_Id)
                              select new MeetingParticipants_ResultVM
                              {
                                  MeetingID = row.MeetingID,
                                  MeetingParticipantId = row.MeetingParticipantId,
                                  Director_Id = row.Director_Id,
                                  UserId = row.UserId,
                                  ParticipantName = row.ParticipantName,
                                  Email = row.Email
                              });

                return result.ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public bool SaveMeetingParticipants(int customerID, int entityId, int meetingTypeId, long meetingID, int userID, DateTime? meetingDate)
        {
            var result = false;
            #region Save Participants
            try
            {
                List<DirectorList_ForEntity> lstParticipants = null;

                //if (meetingTypeId == SecretarialConst.MeetingTypeID.BOARD || meetingTypeId == SecretarialConst.MeetingTypeID.AGM || meetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                //{
                //    lstParticipants = objIDirectorMaster.GetDirectorForEntity(customerID, entityId, -1, -1);
                //}
                //else
                //{
                //    lstParticipants = objIDirectorMaster.GetCommitteeMember(customerID, entityId, meetingTypeId);
                //}
                lstParticipants = objIDirectorMaster.GetDirectorListOfEntity(entityId, meetingTypeId);

                if (meetingTypeId == SecretarialConst.MeetingTypeID.AGM || meetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                {
                    #region KMP
                    var lstKMP = objIDirectorMaster.GetKMPForEntity(customerID, entityId);
                    if (lstKMP != null)
                    {
                        foreach (var item in lstKMP)
                        {
                            lstParticipants.Add(new DirectorList_ForEntity()
                            {
                                Director_ID = item.Director_ID,
                                UserID = item.UserID,
                                Designation = item.Designation,
                                DesignationId = item.DesignationId,
                                PositionId = item.PositionId,
                                ParticipantType = item.ParticipantType,
                                ParticipantDesignation = item.Designation,
                                DirectorshipIdForQuorum = 0,
                                DateofAppointment = item.DateofAppointment
                            });
                        }
                    }
                    #endregion
                }

                #region Management
                var lstMNGT = objIDirectorMaster.GetEntityUserCommitteeAssignment(entityId, meetingTypeId);
                if (lstMNGT != null)
                {
                    foreach (var item in lstMNGT)
                    {
                        if (!lstParticipants.Where(k => k.UserID == item.UserID).Any())
                        {
                            lstParticipants.Add(new DirectorList_ForEntity()
                            {
                                Director_ID = item.Director_ID,
                                UserID = item.UserID,
                                Designation = item.Designation,
                                DesignationId = item.DesignationId,
                                PositionId = item.PositionId,
                                ParticipantType = item.ParticipantType,
                                ParticipantDesignation = item.Designation,
                                DateofAppointment = item.DateofAppointment,
                                DirectorshipIdForQuorum = 0
                            });
                        }
                    }
                }
                #endregion

                if (lstParticipants != null)
                {
                    foreach (var item in lstParticipants)
                    {
                        if (item.DateofAppointment.HasValue && meetingDate.HasValue)
                        {
                            if (item.DateofAppointment >= meetingDate)
                            {
                                #region mark IsDeleted = true if director/userId exists
                                if (item.Director_ID > 0)
                                {
                                    var participant = (from row in entities.BM_MeetingParticipant
                                                       where row.Meeting_ID == meetingID
                                                       && row.Director_Id == item.Director_ID && row.ParticipantType == item.ParticipantType
                                                       && row.IsDeleted == false
                                                       select row
                                                   ).FirstOrDefault();

                                    if (participant != null)
                                    {
                                        participant.IsDeleted = true;
                                        participant.UpdatedOn = DateTime.Now;
                                        participant.UpdatedBy = userID;
                                        entities.SaveChanges();
                                    }
                                }
                                else if (item.UserID > 0)
                                {
                                    var participant = (from row in entities.BM_MeetingParticipant
                                                       where row.Meeting_ID == meetingID
                                                       && row.UserId == item.UserID && row.ParticipantType == item.ParticipantType
                                                       && row.IsDeleted == false
                                                       select row
                                                   ).FirstOrDefault();

                                    if (participant != null)
                                    {
                                        participant.IsDeleted = true;
                                        participant.UpdatedOn = DateTime.Now;
                                        participant.UpdatedBy = userID;
                                        entities.SaveChanges();
                                    }
                                }
                                #endregion
                                continue;
                            }
                        }
                        var isParticipantExists = (from row in entities.BM_MeetingParticipant
                                                   where row.Meeting_ID == meetingID
                                                   //&& row.Director_Id == item.Director_ID
                                                   && ((row.Director_Id == item.Director_ID && row.Director_Id != 0) || (row.UserId == item.UserID && row.UserId != 0))
                                                   && row.IsDeleted == false
                                                   select row.MeetingParticipantId
                                                   ).Any();
                        if (isParticipantExists == false)
                        {
                            var participant = (from row in entities.BM_MeetingParticipant
                                               where row.Meeting_ID == meetingID
                                               //&& row.Director_Id == item.Director_ID && row.ParticipantType == item.ParticipantType
                                                && ((row.Director_Id == item.Director_ID && row.Director_Id != 0) || (row.UserId == item.UserID && row.UserId != 0))
                                               select row).FirstOrDefault();

                            if (participant != null)
                            {
                                //participant.UserId = item.UserID;
                                participant.IsDeleted = false;
                                participant.Designation = item.Designation;
                                participant.DesignationId = item.DesignationId;
                                participant.Position = item.PositionId;
                                participant.UpdatedOn = DateTime.Now;
                                participant.UpdatedBy = userID;
                                entities.SaveChanges();
                            }
                            else
                            {
                                participant = new BM_MeetingParticipant()
                                {
                                    MeetingParticipantId = 0,
                                    Meeting_ID = meetingID,
                                    Director_Id = item.Director_ID,
                                    UserId = item.UserID,
                                    IsDeleted = false,
                                    CreatedBy = userID,
                                    CreatedOn = DateTime.Now,
                                    //Designation = item.Designation,
                                    //DesignationId = item.DesignationId,
                                    Designation = item.ParticipantDesignation,
                                    DesignationId = item.DirectorshipIdForQuorum,
                                    Position = item.PositionId,
                                    ParticipantType = item.ParticipantType
                                };
                                entities.BM_MeetingParticipant.Add(participant);
                                entities.SaveChanges();
                            }
                        }
                    }
                }

                if (meetingTypeId == SecretarialConst.MeetingTypeID.AGM || meetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                {
                    #region Auditors
                    var lstAuditor = objIAuditorMaster.GetAuditorsForGM(customerID, entityId);
                    if (lstAuditor != null)
                    {
                        foreach (var item in lstAuditor)
                        {
                            var isParticipantExists = (from row in entities.BM_MeetingParticipant
                                                       where row.Meeting_ID == meetingID
                                                       && row.AuditorId == item.Id
                                                       && row.IsDeleted == false
                                                       select row.MeetingParticipantId
                                                       ).Any();
                            if (isParticipantExists == false)
                            {
                                BM_MeetingParticipant _objItem = new BM_MeetingParticipant()
                                {
                                    MeetingParticipantId = 0,
                                    Meeting_ID = meetingID,
                                    Director_Id = 0,
                                    UserId = 0,
                                    Designation = "",
                                    AuditorId = (int)item.Id,
                                    IsAuditor = true,
                                    IsDeleted = false,
                                    CreatedBy = userID,
                                    CreatedOn = DateTime.Now,
                                    ParticipantType = SecretarialConst.MeetingParticipantsTypes.AUDITOR
                                };

                                switch ((int)item.Auditor_Type)
                                {
                                    case 1:
                                        _objItem.Designation = "Statutory Auditor";
                                        break;

                                    case 2:
                                        _objItem.Designation = "Internal Auditor";
                                        break;

                                    case 3:
                                        _objItem.Designation = "Secretarial Auditor";
                                        break;
                                    case 4:
                                        _objItem.Designation = "Cost Auditor";
                                        break;
                                    default:
                                        break;
                                }
                                entities.BM_MeetingParticipant.Add(_objItem);
                                entities.SaveChanges();
                            }
                        }
                    }
                    #endregion
                }
                result = true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                result = false;
            }

            #endregion
            return result;
        }
        public OtherScheduledMeetings_ResultVM GetParticipantOtherScheduledMeetings(long meetingId)
        {
            var result = new OtherScheduledMeetings_ResultVM() { IsOtherMeetingScheduled = false };
            try
            {
                var lst = (from row in entities.BM_SP_MeetingCheckParticipantOtherMeetings(meetingId)
                           select new OtherScheduledMeetingList_ResultVM
                           {
                               MeetingID = row.MeetingID,
                               EntityId = row.EntityId,
                               Director_Id = row.Director_Id,
                               MeetingTitle = row.MeetingTitle,
                               MeetingTime = row.MeetingTime,
                               MeetingVenue = row.MeetingVenue,
                               CompanyName = row.CompanyName,
                               ParticipantName = row.Director_Name
                           }).ToList();
                if (lst != null)
                {
                    if (lst.Count > 0)
                    {
                        result.IsOtherMeetingScheduled = true;
                        result.lstMeetingList = lst;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Pending Availability
        public IEnumerable<MeetingVM> GetPedingAvailability(long UserId)
        {
            try
            {
                var date = DateTime.Now.Date;
                var result = (from row in entities.BM_Meetings
                              join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                              join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                              join participants in entities.BM_MeetingParticipant on row.MeetingID equals participants.Meeting_ID
                              where participants.UserId == UserId && row.IsSeekAvailabilitySent == true && row.AvailabilityDueDate >= date && row.IsDeleted == false && participants.IsDeleted == false
                              select new MeetingVM
                              {
                                  MeetingID = row.MeetingID,
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = meeting.MeetingTypeName,
                                  Quarter_ = row.Quarter_,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  IsSeekAvailability = row.IsSeekAvailability,
                                  SeekAvailability_DueDate = row.AvailabilityDueDate,
                                  Availability_ShortDesc = row.Availability_ShortDesc,

                                  Entityt_Id = row.EntityId,
                                  EntityName = entity.CompanyName
                              }).ToList();
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        if (item.MeetingAviability != null)
                        {
                            foreach (var items in item.MeetingAviability)
                            {

                                items.seekavadate = items.SeekAviabiltyDate.ToString("dddd, MMM  dd");
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public IEnumerable<CommitteeCompVM> GetAllMeetingType(string type)
        {
            try
            {
                var result = new List<CommitteeCompVM>();
                if (type == "C")
                {
                    result = (from row in entities.BM_CommitteeComp
                              where row.Customer_Id == null && (row.CompositionType == "C" || row.CompositionType == "B") && row.IsDeleted == false && row.IsCircular == true
                              orderby row.SerialNo
                              select new CommitteeCompVM
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  MeetingTypeName = row.MeetingTypeName
                              }).ToList();
                }
                else if (type == "M" || type == null)
                {
                    result = (from row in entities.BM_CommitteeComp
                              where row.Customer_Id == null && (row.CompositionType == "C" || row.CompositionType == "B") && row.IsDeleted == false
                              orderby row.SerialNo
                              select new CommitteeCompVM
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  MeetingTypeName = row.MeetingTypeName
                              }).ToList();
                }

                return result;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Seek Availability
        public IEnumerable<MeetingAvailabilityVM> GetAllSeekAvailability(long parentID)
        {
            var result = (from row in entities.BM_MeetingAvailability
                          where row.MeetingID == parentID && row.IsDeleted == false
                          select new MeetingAvailabilityVM()
                          {
                              MeetingID = row.MeetingID,
                              MeetingAvailabilityId = row.MeetingAvailabilityId,
                              AvailabilityDate = row.AvailabilityDate,
                              AvailabilityTime = row.AvailabilityTime,
                              AvailabilityVenue = row.AvailabilityVenue,
                              PreferenceId = row.PreferenceId
                          }).ToList();

            return result;
        }
        public MeetingAvailabilityVM GetSeekAvailability(long id)
        {
            var result = (from row in entities.BM_MeetingAvailability
                          where row.MeetingAvailabilityId == id && row.IsDeleted == false
                          select new MeetingAvailabilityVM()
                          {
                              MeetingID = row.MeetingID,
                              MeetingAvailabilityId = row.MeetingAvailabilityId,
                              AvailabilityDate = row.AvailabilityDate,
                              AvailabilityTime = row.AvailabilityTime,
                              AvailabilityAddressType = row.MeetingVenueType,
                              AvailabilityVenue = row.AvailabilityVenue,
                              PreferenceId = row.PreferenceId
                          }).FirstOrDefault();

            return result;
        }
        public MeetingAvailabilityVM Create(MeetingAvailabilityVM obj)
        {
            try
            {
                var checkPreference = (from row in entities.BM_MeetingAvailability
                                       where row.MeetingAvailabilityId != obj.MeetingAvailabilityId
                                       && row.MeetingID == obj.MeetingID
                                       && row.PreferenceId == obj.PreferenceId
                                       && row.IsDeleted == false
                                       select row
                                       ).Any();
                if (checkPreference)
                {
                    obj.Error = true;
                    obj.Message = "Availability Preference Exists.";
                    return obj;
                }

                if (obj.AvailabilityAddressType != "R" && obj.AvailabilityAddressType != "C")
                {
                    obj.AvailabilityAddressType = "O";
                }

                BM_MeetingAvailability _obj = new BM_MeetingAvailability()
                {
                    MeetingID = obj.MeetingID,
                    AvailabilityDate = obj.AvailabilityDate,
                    AvailabilityTime = obj.AvailabilityTime,
                    MeetingVenueType = obj.AvailabilityAddressType,
                    AvailabilityVenue = obj.AvailabilityVenue,
                    PreferenceId = obj.PreferenceId,

                    IsDeleted = false,
                    CreatedBy = obj.UserId,
                    CreatedOn = DateTime.Now
                };

                entities.BM_MeetingAvailability.Add(_obj);
                entities.SaveChanges();

                obj.MeetingAvailabilityId = _obj.MeetingAvailabilityId;
                obj.Success = true;
                obj.Message = "Saved Successfully.";
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.validationError;
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingAvailabilityVM Update(MeetingAvailabilityVM obj)
        {
            try
            {
                var _obj = (from row in entities.BM_MeetingAvailability
                            where row.MeetingAvailabilityId == obj.MeetingAvailabilityId && row.MeetingID == obj.MeetingID && row.IsDeleted == false
                            select row
                           ).FirstOrDefault();

                if (_obj != null)
                {
                    var checkPreference = (from row in entities.BM_MeetingAvailability
                                           where row.MeetingAvailabilityId != obj.MeetingAvailabilityId
                                           && row.MeetingID == obj.MeetingID
                                           && row.PreferenceId == obj.PreferenceId
                                           && row.IsDeleted == false
                                           select row
                                           ).Any();
                    if (checkPreference)
                    {
                        obj.Error = true;
                        obj.Message = "Availability Preference Exists.";
                        return obj;
                    }

                    if (obj.AvailabilityAddressType != "R" && obj.AvailabilityAddressType != "C")
                    {
                        obj.AvailabilityAddressType = "O";
                    }

                    _obj.AvailabilityDate = obj.AvailabilityDate;
                    _obj.AvailabilityTime = obj.AvailabilityTime;
                    _obj.Availability_ToTime = obj.AvailabilityToTime;
                    _obj.MeetingVenueType = obj.AvailabilityAddressType;
                    _obj.AvailabilityVenue = obj.AvailabilityVenue;
                    _obj.PreferenceId = obj.PreferenceId;

                    _obj.UpdatedBy = obj.UserId;
                    _obj.UpdatedOn = DateTime.Now;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    obj.Success = true;
                    obj.Message = "Update Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong.";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.validationError;
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public bool UpdateAvailabilityDueDate(long MeetingId, DateTime date_, int UserId)
        {
            var result = false;
            try
            {
                var _obj = (from row in entities.BM_Meetings
                            where row.MeetingID == MeetingId && row.IsSeekAvailabilitySent != true && row.IsDeleted == false
                            select row
                           ).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.AvailabilityDueDate = date_;
                    _obj.IsSeekAvailability = true;
                    _obj.UpdatedBy = UserId;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public MeetingVM ForceEditSeekAvailability(long CustomerId, long MeetingId)
        {
            var result = GetMeeting(CustomerId, MeetingId);
            try
            {
                if (result != null)
                {
                    if (result.CanReSendSeekAvailability == true)
                    {
                        var obj = entities.BM_Meetings.Where(k => k.MeetingID == result.MeetingID).FirstOrDefault();
                        if (obj != null)
                        {
                            entities.BM_MeetingAvailability.Where(k => k.MeetingID == result.MeetingID && k.IsDeleted == false).ToList().ForEach(v => v.IsDeleted = true);
                            entities.SaveChanges();

                            entities.BM_MeetingAvailabilityResponse.Where(k => k.Meeting_ID == result.MeetingID && k.IsDeleted == false).ToList().ForEach(v => v.IsDeleted = true);
                            entities.SaveChanges();

                            entities.BM_MeetingParticipant.Where(k => k.Meeting_ID == result.MeetingID && k.IsDeleted == false).ToList().ForEach(v => v.IsDeleted = true);
                            entities.SaveChanges();

                            obj.IsSeekAvailabilitySent = false;
                            entities.SaveChanges();
                        }

                        result.CanMarkSeekAvailability = false;
                        result.CanReSendSeekAvailability = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public MeetingAvailabilityVM SelectSeekAvailability(MeetingAvailabilityVM obj, int UserId)
        {
            try
            {
                var _obj = entities.BM_MeetingAvailability.Where(k => k.MeetingAvailabilityId == obj.MeetingAvailabilityId && k.MeetingID == obj.MeetingID && k.IsDeleted == false).FirstOrDefault();

                if (_obj != null)
                {
                    var meeting = (from row in entities.BM_Meetings
                                   where row.MeetingID == obj.MeetingID && row.IsDeleted == false
                                   select row
                           ).FirstOrDefault();

                    if (meeting != null)
                    {
                        meeting.MeetingDate = _obj.AvailabilityDate;
                        meeting.MeetingTime = _obj.AvailabilityTime;
                        meeting.MeetingVenueType = _obj.MeetingVenueType;
                        meeting.MeetingVenue = _obj.AvailabilityVenue;
                        meeting.UpdatedBy = UserId;
                        meeting.UpdatedOn = DateTime.Now;
                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();

                        obj.RefreshMailFormat = true;
                        obj.Success = true;
                        obj.Message = "Save Successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }
        #endregion

        #region Seek Availability Mail
        public MeetingAvailabilityMailVM UpdateAvailabilityMail(MeetingAvailabilityMailVM obj)
        {
            try
            {
                var _obj = (from row in entities.BM_Meetings
                            where row.MeetingID == obj.AvailabilityMailID && row.IsDeleted == false
                            select row
                           ).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.AvailabilityMail = obj.AvailabilityMail;
                    //_obj.AvailabilityDueDate = obj.SeekAvailabilityDueDate;
                    _obj.UpdatedBy = obj.UserId;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = "Update Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingAvailabilityMailVM PreviewAvailabilityMail(MeetingAvailabilityMailVM obj)
        {
            try
            {
                var result = (from row in entities.BM_SP_GenerateAvailabilityMail(obj.AvailabilityMailID, obj.AvailabilityMail)
                              select row
                            ).FirstOrDefault();

                if (result != null)
                {
                    obj.AvailabilityMail = result;
                    obj.Success = true;
                    obj.Message = "success";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingAvailabilityMailVM SendAvailabilityMail(MeetingAvailabilityMailVM obj, out string MailFormat)
        {
            MailFormat = "";
            try
            {
                var AvailabilityDueDate = (from row in entities.BM_Meetings
                                           where row.MeetingID == obj.AvailabilityMailID && row.IsDeleted == false
                                           select row.AvailabilityDueDate
                                                      ).FirstOrDefault();

                if (AvailabilityDueDate.HasValue)
                {
                    var SeekAvailabilityDueDate = Convert.ToDateTime(AvailabilityDueDate);
                    if (SeekAvailabilityDueDate < DateTime.Now.Date)
                    {
                        obj.Error = true;
                        obj.Message = "Please Enter Valid Seek Availability Due Date.";
                    }
                    else
                    {
                        var checkAvailabilityDates = (from row in entities.BM_MeetingAvailability
                                                      where row.MeetingID == obj.AvailabilityMailID && row.AvailabilityDate < SeekAvailabilityDueDate && row.IsDeleted == false
                                                      select row.MeetingID
                                                      ).Any();
                        if (checkAvailabilityDates)
                        {
                            obj.Error = true;
                            obj.Message = "Please Check Seek Availability Date.";
                        }
                        else
                        {
                            #region Save Details

                            MailFormat = (from row in entities.BM_SP_GenerateAvailabilityMail(obj.AvailabilityMailID, obj.AvailabilityMail)
                                          select row
                            ).FirstOrDefault();

                            var _obj = (from row in entities.BM_Meetings
                                        where row.MeetingID == obj.AvailabilityMailID && row.IsDeleted == false
                                        select row
                                       ).FirstOrDefault();

                            if (_obj != null)
                            {
                                //Save Meeting Participants
                                SaveMeetingParticipants((int)_obj.Customer_Id, (int)_obj.EntityId, _obj.MeetingTypeId, _obj.MeetingID, obj.UserId, AvailabilityDueDate);

                                _obj.IsSeekAvailabilitySent = true;
                                _obj.AvailabilityMail = obj.AvailabilityMail;
                                //_obj.AvailabilityDueDate = obj.SeekAvailabilityDueDate;
                                _obj.SendDateAvailability = DateTime.Now;

                                entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                                entities.SaveChanges();

                                obj.IsSeekAvailabilitySent = true;
                                obj.RefreshAvailability = true;

                                obj.Success = true;
                                obj.Message = "Update Successfully.";
                            }
                            else
                            {
                                obj.Error = true;
                                obj.Message = "Something went wrong.";
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Please Enter Seek Availability Due Date.";
                }

            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Seek Availability Response
        public List<AvailabilityResponse_ResultVM> GetAvailabilityResponse(long MeetingId, int CustomerId, int? UserId)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetAvailabilityResponse(MeetingId)
                              select new AvailabilityResponse_ResultVM
                              {
                                  MeetingID = row.MeetingID,
                                  MeetingAvailabilityId = row.MeetingAvailabilityId,
                                  AvailabilityResponseID = row.AvailabilityResponseID,
                                  MeetingParticipantId = row.MeetingParticipantId,
                                  Director_Id = row.Director_Id,
                                  UserId = row.UserId,
                                  ParticipantName = row.ParticipantName,
                                  Email = row.Email,
                                  AvailabilityDate = row.AvailabilityDate.ToString("dd/MMM/yyyy, dddd"),
                                  AvailabilityTime = row.AvailabilityTime,
                                  AvailabilityVenue = row.AvailabilityVenue,
                                  PreferenceId = row.PreferenceId,
                                  Response = row.Response
                              }
                            );

                if (UserId != null)
                {
                    result = result.Where(k => k.UserId == UserId);
                }

                return result.ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public AvailabilityResponse_ResultVM UpdateAvailabilityResponse(AvailabilityResponse_ResultVM obj, int CreatedBy)
        {
            try
            {
                var IsExists = entities.BM_MeetingAvailabilityResponse.Where(k => k.Meeting_ID == obj.MeetingID &&
                                k.MeetingAvailabilityID == obj.MeetingAvailabilityId &&
                                k.MeetingParticipantID == obj.MeetingParticipantId &&
                                k.IsDeleted == false).Any();
                if (IsExists == false)
                {
                    BM_MeetingAvailabilityResponse _obj = new BM_MeetingAvailabilityResponse()
                    {
                        Meeting_ID = obj.MeetingID,
                        MeetingAvailabilityID = obj.MeetingAvailabilityId,
                        MeetingParticipantID = obj.MeetingParticipantId,
                        Response = obj.Response,
                        IsDeleted = false,
                        CreatedOn = DateTime.Now,
                        CreatedBy = CreatedBy
                    };

                    entities.BM_MeetingAvailabilityResponse.Add(_obj);
                    entities.SaveChanges();

                    obj.AvailabilityResponseID = _obj.AvailabilityResponseID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public List<AvailabilityResponseChartData_ResultVM> GetAvailabilityResponseChartData(long MeetingId, long MeetingAvailabilityId)
        {
            var lstResponse = GetAvailabilityResponseByAvailabilityId(MeetingId, MeetingAvailabilityId);

            var result = (from row in entities.BM_SP_GetAvailabilityResponseChartData(MeetingId, MeetingAvailabilityId)
                          select new AvailabilityResponseChartData_ResultVM
                          {
                              Category = row.Category,
                              Response_Count = row.Response_Count,
                              Color = row.Color,
                              MeetingId = MeetingId,
                              MeetingAvailabilityId = MeetingAvailabilityId,
                              lstParticiapants = lstResponse.Where(k => k.Category == row.Category).ToList()
                              //lstParticiapants = (from reponse in entities.BM_SP_GetAvailabilityResponseByAvailabilityId(MeetingId, MeetingAvailabilityId)
                              //                    where reponse.Category == row.Category 
                              //                    select new AvailabilityResponseByAvailabilityId_ResultVM
                              //                    {
                              //                        ParticipantName = reponse.ParticipantName,
                              //                        Email = reponse.Email,
                              //                        Response = reponse.Response,
                              //                        Category = reponse.Category
                              //                    }).ToList()
                          });
            return result.ToList();
        }
        public List<AvailabilityResponseByAvailabilityId_ResultVM> GetAvailabilityResponseByAvailabilityId(long MeetingId, long MeetingAvailabilityId)
        {
            var result = (from row in entities.BM_SP_GetAvailabilityResponseByAvailabilityId(MeetingId, MeetingAvailabilityId)
                          select new AvailabilityResponseByAvailabilityId_ResultVM
                          {
                              ParticipantName = row.ParticipantName,
                              Email = row.Email,
                              Response = row.Response,
                              Category = row.Category
                          });
            return result.ToList();
        }
        #endregion

        #region Meeting Agenda
        public List<AgendaComplianceMappingPendingVM> CheckComplianceAssignedOrNot(long agendaId, long meetingId, int customerId, int? entityId)
        {
            try
            {
                return (from row in entities.BM_SP_MeetingCheckComplianceAssign(agendaId, meetingId, customerId, entityId)
                        select new AgendaComplianceMappingPendingVM
                        {
                            AgendaId = row.AgendaId,
                            Agenda = row.Agenda
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public IEnumerable<MeetingAgendaMappingVM> GetMeetingAgenda(long MeetingId, string Part, string Type)
        {
            var PartId = 2;
            if (!String.IsNullOrEmpty(Part))
            {
                if (Part == "A")
                {
                    PartId = 1;
                }
                else if (Part == "C")
                {
                    PartId = 3;
                }
            }

            var result = (from TemplateList in entities.BM_SP_MeetingAgendaTemplateList(MeetingId, PartId, false, null)
                          select new MeetingAgendaMappingVM
                          {
                              SrNo = (int)TemplateList.SrNo,
                              MeetingAgendaMappingID = (long)TemplateList.MeetingAgendaMappingID,
                              Meeting_Id = TemplateList.MeetingID,
                              AgendaID = TemplateList.BM_AgendaMasterId,
                              PartId = TemplateList.PartID,
                              AgendaItemHeading = TemplateList.AgendaItemHeading,
                              AgendaItemText = TemplateList.AgendaItemText,
                              HasTemplate = TemplateList.HasTemplate,
                              IsFillTemplateFields = TemplateList.IsFillTemplateFields,

                              HasCompliance = TemplateList.HasCompliance,
                              HasInfo = TemplateList.HasInfo,
                              Hasfoms = TemplateList.HasForm,
                              MayCompliancePenalty = TemplateList.MayCompliancePenalty,

                              UIFormID = TemplateList.UIFormID,

                          });
            return result.ToList();
        }
        public AgendaItemSelect AddAgendaItem(AgendaItemSelect obj, int createdBy, int customerID)
        {
            try
            {
                //var IsExists = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingID == obj.Meeting_Id && k.AgendaID == obj.BM_AgendaMasterId && k.IsDeleted == false).Any();
                var IsExists = (from mapping in entities.BM_MeetingAgendaMapping
                                join agenda in entities.BM_AgendaMaster on mapping.AgendaID equals agenda.BM_AgendaMasterId
                                where mapping.MeetingID == obj.Meeting_Id && mapping.AgendaID == obj.BM_AgendaMasterId && mapping.IsDeleted == false && agenda.CanMultiple == false
                                select mapping.MeetingAgendaMappingID
                                ).Any();

                if (IsExists == false)
                {
                    var SrNoNew = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingID == obj.Meeting_Id && k.PartId == obj.PartID && k.IsDeleted == false).Select(k => k.SrNo).DefaultIfEmpty().Max();
                    SrNoNew++;

                    var Agenda = entities.BM_SP_AgendaFormatesByCustomerId(customerID, obj.BM_AgendaMasterId).FirstOrDefault();

                    var _obj = new BM_MeetingAgendaMapping();

                    #region set open agenda reference from on compliance completion & event such as director resignation
                    if (obj.OpenAgendaId > 0)
                    {
                        _obj.RefOpenAgendaId = obj.OpenAgendaId;
                    }
                    #endregion

                    if (obj.IsNewStage == true)
                    {
                        //Update Meeting Id for New stage Agenda
                        #region Update Meeting Id for New stage (multistage) Agenda Items (excluding pending multistage)
                        if (obj.RefPendingMappingID != null)
                        {
                            _obj = (from row in entities.BM_MeetingAgendaMapping
                                    where row.MeetingAgendaMappingID == (long)obj.RefPendingMappingID && row.IsClosed == false && row.IsActive == false
                                    select row).FirstOrDefault();

                            if (_obj != null)
                            {
                                _obj.MeetingID = obj.Meeting_Id;
                                _obj.SrNo = SrNoNew;
                                _obj.IsClosed = true;
                                _obj.IsActive = true;
                                _obj.IsDeleted = false;

                                _obj.IsNewStage = false;

                                _obj.UpdatedBy = createdBy;
                                _obj.UpdatedOn = DateTime.Now;

                                entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                                entities.SaveChanges();

                                obj.CopyAgendaMappingId = _obj.MeetingAgendaMappingID;
                            }
                            else
                            {
                                //obj.Error = true;
                                //obj.Message = "Something went wrong.";
                                return obj;
                            }
                            obj.RefPendingMappingID = null;
                        }
                        else
                        {
                            //obj.Error = true;
                            //obj.Message = "Something went wrong.";
                            return obj;
                        }
                        #endregion
                    }
                    else
                    {
                        //Add new agenda items
                        #region Other than open multistage Agenda Items
                        _obj.MeetingID = obj.Meeting_Id;
                        _obj.AgendaID = obj.BM_AgendaMasterId;
                        _obj.SrNo = SrNoNew;
                        _obj.AgendaItemText = obj.Agenda;
                        _obj.AgendaItemTextNew = obj.Agenda;
                        _obj.RefPendingMappingID = obj.RefPendingMappingID;
                        _obj.IsClosed = true;
                        _obj.ResultRemark = null;
                        _obj.IsActive = true;
                        _obj.IsDeleted = false;

                        _obj.PartId = obj.PartID;
                        if (obj.ScheduleOnID > 0) // Opend Agenda on Compliance Completion
                        {
                            _obj.PartId = 1;
                        }

                        if (obj.RefMasterID > 0)
                        {
                            _obj.RefMasterID = obj.RefMasterID;
                        }

                        if (obj.RefMasterIDOriginal > 0)
                        {
                            _obj.RefMasterIDOriginal = obj.RefMasterIDOriginal;
                        }

                        _obj.CreatedBy = createdBy;
                        _obj.CreatedOn = DateTime.Now;

                        if (Agenda != null)
                        {
                            _obj.AgendaItemText = Agenda.AgendaItemText;
                            _obj.AgendaItemTextNew = Agenda.AgendaItemText;
                            _obj.AgendaFormat = Agenda.AgendaFormat;

                            _obj.ResolutionFormatHeading = Agenda.ResolutionFormatHeading;
                            _obj.ResolutionFormat = Agenda.ResolutionFormat;

                            _obj.MinutesFormatHeading = Agenda.MinutesFormatHeading;
                            _obj.MinutesFormat = Agenda.MinutesFormat;
                            _obj.MinutesDisApproveFormat = Agenda.MinutesDisApproveFormat;
                            _obj.MinutesDifferFormat = Agenda.MinutesDifferFormat;

                            _obj.SEBI_IntimationFormat = Agenda.SEBI_IntimationFormat;
                            _obj.SEBI_DisclosureFormat = Agenda.SEBI_DisclosureFormat;
                        }

                        #region set Sequence number 0 if multi stage agenda
                        var hasStages = (from row in entities.BM_AgendaMaster_SubAgendaMappging
                                         where row.AgendaMasterId == obj.BM_AgendaMasterId && row.IsDeleted == false
                                         select row.Agenda_SubAgendaMappgingID).Any();
                        if (hasStages == true)
                        {
                            //Commented due to if multistage agenda differed on first stage
                            //_obj.StartMeetingId = obj.Meeting_Id;
                            //_obj.StartAgendaId = obj.BM_AgendaMasterId;
                            _obj.StartMeetingId = obj.StartMeetingId;
                            _obj.StartAgendaId = obj.StartAgendaId;
                            _obj.StartMappingId = obj.StartMappingId;
                            _obj.SequenceNo = 0; //Set 0 from Multi stage agenda
                            _obj.IsNewStage = false;
                        }
                        else
                        {
                            _obj.StartMeetingId = obj.StartMeetingId;
                            _obj.StartAgendaId = obj.StartAgendaId;
                            _obj.StartMappingId = obj.StartMappingId;
                            _obj.SequenceNo = obj.SequenceNo;
                            _obj.IsNewStage = false;
                        }
                        #endregion

                        _obj.CopyMeetingId = obj.CopyMeetingId;
                        entities.BM_MeetingAgendaMapping.Add(_obj);
                        entities.SaveChanges();
                        obj.CopyAgendaMappingId = _obj.MeetingAgendaMappingID;

                        //Commented on 5-Mar-2020
                        if (hasStages == true && obj.StartMeetingId == _obj.MeetingID)
                        {
                            _obj.StartMappingId = _obj.MeetingAgendaMappingID;
                            entities.SaveChanges();
                        }
                        //End

                        #endregion

                        if (obj.RefPendingMappingID > 0)
                        {
                            #region Copy Template fields data from reference agenda mapping id
                            if (_obj.MeetingAgendaMappingID > 0)
                            {
                                InsertTemplates(_obj.MeetingAgendaMappingID, obj.RefPendingMappingID, createdBy);
                            }
                            #endregion
                        }
                    }

                    #region Update IsNoted flag for Open Agenda item due to Compliance Completion/ 
                    if (obj.OpenAgendaId > 0)
                    {
                        var refOpenAgendaMapping = entities.BM_MeetingOpenAgendaOnCompliance.Where(k => k.Id == obj.OpenAgendaId && k.IsNoted == false).FirstOrDefault();
                        if (refOpenAgendaMapping != null)
                        {
                            #region Generate format for take note on circular Agenda
                            if (refOpenAgendaMapping.RefMaster == SecretarialConst.OpenAgendaRefMaster.MEETING)
                            {
                                var circularDetails = (from row in entities.BM_Meetings
                                                       join meetingType in entities.BM_CommitteeComp on row.MeetingTypeId equals meetingType.Id
                                                       from fy in entities.BM_YearMaster.Where(k => k.FYID == row.FY).Select(k => k.FYText).DefaultIfEmpty()
                                                       where row.MeetingID == refOpenAgendaMapping.RefMasterID
                                                       select new
                                                       {
                                                           meetingType.Name,
                                                           row.MeetingSrNo,
                                                           row.CircularDate,
                                                           row.EndMeetingDate,
                                                           row.MeetingID,
                                                           fy
                                                       }).FirstOrDefault();

                                if (circularDetails != null)
                                {
                                    var meetingType = circularDetails.Name;
                                    var circularNo = circularDetails.MeetingSrNo == null ? "#" : Convert.ToString(circularDetails.MeetingSrNo);
                                    var circularFY = circularDetails.fy;
                                    var circularDate = string.Empty;
                                    var circularPassedDate = string.Empty;
                                    var resolutions = string.Empty;

                                    if (circularDetails.CircularDate.HasValue)
                                    {
                                        circularDate = Convert.ToDateTime(circularDetails.CircularDate).ToString("MMMM dd, yyyy");
                                    }

                                    if (circularDetails.EndMeetingDate.HasValue)
                                    {
                                        circularPassedDate = Convert.ToDateTime(circularDetails.EndMeetingDate).ToString("MMMM dd, yyyy");
                                    }

                                    #region Generate resolution table
                                    var lstResolutions = (from row in entities.BM_MeetingAgendaMapping
                                                          where row.MeetingID == circularDetails.MeetingID &&
                                                          row.IsDeleted == false &&
                                                          row.IsActive == true &&
                                                          (row.Result == SecretarialConst.AgendaResult.APPROVED || row.Result == SecretarialConst.AgendaResult.NOTED)
                                                          select new
                                                          {
                                                              row.MeetingAgendaMappingID,
                                                              row.AgendaItemTextNew,
                                                              row.AgendaItemText
                                                          }).ToList();

                                    resolutions = "<table>";
                                    if (lstResolutions != null)
                                    {
                                        var resolutionCount = 1;
                                        foreach (var item in lstResolutions)
                                        {
                                            var resolutionFormat = (from row in entities.BM_SP_GetAgendaTemplate(item.MeetingAgendaMappingID, null)
                                                                    select row.ResolutionFormat
                                                                   ).FirstOrDefault();

                                            resolutions += "<tr><td style='text-align:left;'><strong>Resolution No. " + resolutionCount + "</strong><br><br>" +
                                                (item.AgendaItemTextNew == null ? item.AgendaItemText : item.AgendaItemTextNew) +
                                                (string.IsNullOrEmpty(resolutions) ? "" : "<br>" + resolutions + "<br>") +
                                                "</td></tr>";

                                            resolutionCount++;
                                        }
                                    }
                                    resolutions += "</table>";
                                    #endregion

                                    #region update format
                                    _obj.AgendaItemText = _obj.AgendaItemText.Replace("{{SERIAL_NUMBER_OF_CIRCULAR}}", circularNo).Replace("{{DATE_OF_CIRCULAR}}", circularDate).Replace("{{DATE_OF_CIRCULAR_PASSED}}", circularPassedDate).Replace("{{FINANCIAL_YEAR_OF_CIRCULAR}}", circularFY);
                                    _obj.AgendaItemTextNew = _obj.AgendaItemText;

                                    if (_obj.AgendaFormat != null)
                                    {
                                        _obj.AgendaFormat = _obj.AgendaFormat.Replace("{{SERIAL_NUMBER_OF_CIRCULAR}}", circularNo).Replace("{{DATE_OF_CIRCULAR}}", circularDate).Replace("{{DATE_OF_CIRCULAR_PASSED}}", circularPassedDate).Replace("{{FINANCIAL_YEAR_OF_CIRCULAR}}", circularFY).Replace("{{LIST_OF_RESOLUTIONS}}", resolutions);
                                    }

                                    if (_obj.ResolutionFormat != null)
                                    {
                                        _obj.ResolutionFormat = _obj.ResolutionFormat.Replace("{{SERIAL_NUMBER_OF_CIRCULAR}}", circularNo).Replace("{{DATE_OF_CIRCULAR}}", circularDate).Replace("{{DATE_OF_CIRCULAR_PASSED}}", circularPassedDate).Replace("{{FINANCIAL_YEAR_OF_CIRCULAR}}", circularFY).Replace("{{LIST_OF_RESOLUTIONS}}", resolutions);
                                    }

                                    if (_obj.MinutesFormatHeading != null)
                                    {
                                        _obj.MinutesFormatHeading = _obj.MinutesFormatHeading.Replace("{{SERIAL_NUMBER_OF_CIRCULAR}}", circularNo).Replace("{{DATE_OF_CIRCULAR}}", circularDate).Replace("{{DATE_OF_CIRCULAR_PASSED}}", circularPassedDate).Replace("{{FINANCIAL_YEAR_OF_CIRCULAR}}", circularFY);
                                    }

                                    if (_obj.MinutesFormat != null)
                                    {
                                        _obj.MinutesFormat = _obj.MinutesFormat.Replace("{{SERIAL_NUMBER_OF_CIRCULAR}}", circularNo).Replace("{{DATE_OF_CIRCULAR}}", circularDate).Replace("{{DATE_OF_CIRCULAR_PASSED}}", circularPassedDate).Replace("{{FINANCIAL_YEAR_OF_CIRCULAR}}", circularFY).Replace("{{LIST_OF_RESOLUTIONS}}", resolutions);
                                    }
                                    entities.SaveChanges();
                                    #endregion
                                }
                            }
                            #endregion

                            refOpenAgendaMapping.IsNoted = true;
                            refOpenAgendaMapping.UpdatedBy = createdBy;
                            refOpenAgendaMapping.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                    }
                    #endregion

                    long? refPostMeetingId = null;
                    long? refPostAgendaId = null;
                    long? refPostMappingId = null;

                    //Update IsClosed flag for pending agenda items
                    if (obj.RefPendingMappingID > 0)
                    {
                        var refAgendaMapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.RefPendingMappingID).FirstOrDefault();

                        //Get Board Meeting Id & Agenda Id from obj.RefPendingMappingID
                        if (refAgendaMapping.PostMeetingID == null)
                        {
                            refPostMeetingId = refAgendaMapping.MeetingID;
                            refPostAgendaId = refAgendaMapping.AgendaID;
                            refPostMappingId = refAgendaMapping.MeetingAgendaMappingID;
                        }
                        else
                        {
                            refPostMeetingId = refAgendaMapping.PostMeetingID;
                            refPostAgendaId = refAgendaMapping.PostAgendaID;
                            refPostMappingId = refAgendaMapping.PostMappingId;
                        }
                        //End

                        refAgendaMapping.IsClosed = true;
                        entities.SaveChanges();
                    }
                    //End
                    var lstMandatoryCommittees = (from meeting in entities.BM_Meetings
                                                  join applicability in entities.BM_Applicability_Entity on meeting.EntityId equals applicability.EntityId
                                                  join master in entities.BM_Applicability on applicability.ApplicabilityId equals master.Id
                                                  where meeting.MeetingID == obj.Meeting_Id && meeting.IsDeleted == false && master.CommitteeId != null &&
                                                  applicability.ApplicabilityValue == true
                                                  select master.CommitteeId).ToList();

                    if (lstMandatoryCommittees != null)
                    {
                        if (lstMandatoryCommittees.Count > 0)
                        {
                            lstMandatoryCommittees.Add(10);
                        }
                    }
                    //End

                    #region Pre Committee Agenda Items
                    if (Agenda != null)
                    {
                        var query = (from preCommitteeAgenda in entities.BM_AgendaMaster
                                     where preCommitteeAgenda.Parent_Id == Agenda.BM_AgendaMasterId && preCommitteeAgenda.IsDeleted == false
                                     select new
                                     {
                                         MeetingTypeId = preCommitteeAgenda.MeetingTypeId,
                                         MeetingID = obj.Meeting_Id,
                                         AgendaID = preCommitteeAgenda.BM_AgendaMasterId,
                                         //SrNo = 0,
                                         //IsActive = true,
                                         //IsDeleted = false,
                                         //PartId = 0,
                                         AgendaItemText = preCommitteeAgenda.Agenda,
                                         AgendaFormat = preCommitteeAgenda.AgendaFormat,

                                         ResolutionFormatHeading = preCommitteeAgenda.ResolutionFormatHeading,
                                         ResolutionFormat = preCommitteeAgenda.ResolutionFormat,

                                         MinutesFormatHeading = preCommitteeAgenda.MinutesFormatHeading,
                                         MinutesFormat = preCommitteeAgenda.MinutesFormat,
                                         MinutesDisApproveFormat = preCommitteeAgenda.MinutesDisApproveFormat,
                                         MinutesDifferFormat = preCommitteeAgenda.MinutesDifferFormat
                                     });
                        var lstAllAgendaItems = query.ToList();

                        List<BM_MeetingAgendaMapping> lstPreCommitteeAgenda = null;

                        if (lstAllAgendaItems != null)
                        {
                            #region Get Agenda Items only for Applicable Committees
                            foreach (var r in lstAllAgendaItems)
                            {
                                //Check Pre- Committee Applicable or not 
                                if (lstMandatoryCommittees.Any(k => k == r.MeetingTypeId))
                                {
                                    if (lstPreCommitteeAgenda == null)
                                    {
                                        lstPreCommitteeAgenda = new List<BM_MeetingAgendaMapping>();
                                    }

                                    lstPreCommitteeAgenda.Add(
                                        new BM_MeetingAgendaMapping()
                                        {
                                            MeetingID = r.MeetingID,
                                            PostMeetingID = r.MeetingID,
                                            AgendaID = r.AgendaID,
                                            PostAgendaID = obj.BM_AgendaMasterId,
                                            PostMappingId = _obj.MeetingAgendaMappingID,
                                            SrNo = 0,
                                            IsClosed = true,
                                            IsActive = true,
                                            IsDeleted = false,
                                            IsNewStage = false,
                                            PartId = 0,

                                            AgendaItemText = r.AgendaItemText,
                                            AgendaItemTextNew = r.AgendaItemText,
                                            AgendaFormat = r.AgendaFormat,
                                            ResolutionFormatHeading = r.ResolutionFormatHeading,
                                            ResolutionFormat = r.ResolutionFormat,
                                            MinutesFormatHeading = r.MinutesFormatHeading,
                                            MinutesFormat = r.MinutesFormat,
                                            MinutesDisApproveFormat = r.MinutesDisApproveFormat,
                                            MinutesDifferFormat = r.MinutesDifferFormat,

                                            CreatedBy = createdBy,
                                            CreatedOn = DateTime.Now
                                        });
                                }
                            }
                            #endregion

                            if (lstPreCommitteeAgenda != null)
                            {
                                #region Get Customer-wise Agenda Templates
                                foreach (var item in lstPreCommitteeAgenda)
                                {
                                    var r = entities.BM_SP_AgendaFormatesByCustomerId(customerID, item.AgendaID).FirstOrDefault();
                                    if (r != null)
                                    {
                                        item.AgendaItemText = r.AgendaItemText;
                                        item.AgendaItemTextNew = r.AgendaItemText;
                                        item.AgendaFormat = r.AgendaFormat;
                                        item.ResolutionFormatHeading = r.ResolutionFormatHeading;
                                        item.ResolutionFormat = r.ResolutionFormat;
                                        item.MinutesFormatHeading = r.MinutesFormatHeading;
                                        item.MinutesFormat = r.MinutesFormat;
                                        item.MinutesDisApproveFormat = r.MinutesDisApproveFormat;
                                        item.MinutesDifferFormat = r.MinutesDifferFormat;

                                        item.SEBI_IntimationFormat = r.SEBI_IntimationFormat;
                                        item.SEBI_DisclosureFormat = r.SEBI_DisclosureFormat;
                                    }
                                }
                                #endregion

                                //Check previous Committee Agenda Items not closed 
                                if (obj.RefPendingMappingID != null)
                                {
                                    foreach (var item in lstPreCommitteeAgenda)
                                    {
                                        var committeeRefPendingMappingID = (from row in entities.BM_MeetingAgendaMapping
                                                                            where row.PostMeetingID == refPostMeetingId && row.PostAgendaID == refPostAgendaId && row.PostMappingId == refPostMappingId &&
                                                                            row.AgendaID == item.AgendaID && row.IsClosed == false && row.IsDeleted == false
                                                                            select row.MeetingAgendaMappingID).FirstOrDefault();

                                        if (committeeRefPendingMappingID > 0)
                                        {
                                            item.RefPendingMappingID = committeeRefPendingMappingID;

                                            var refAgendaItem = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == committeeRefPendingMappingID).FirstOrDefault();
                                            refAgendaItem.IsClosed = true;

                                            //Get StartMeetingId & StartAgendaId from committeeRefPendingMappingID
                                            item.StartMeetingId = refAgendaItem.StartMeetingId;
                                            item.StartAgendaId = refAgendaItem.StartAgendaId;
                                            item.StartMappingId = refAgendaItem.StartMappingId;

                                            item.SequenceNo = refAgendaItem.SequenceNo;
                                            item.IsNewStage = refAgendaItem.IsNewStage;
                                            //End
                                            //Added on 28 Apr 2020
                                            item.RefMasterID = refAgendaItem.RefMasterID;
                                            entities.SaveChanges();

                                            //Added on 04 Dec 2019
                                            //1)while adding pending agenda item in board & committee has differed flag then show popup for pre-committe agenda item
                                            entities.BM_MeetingAgendaMapping.Add(item);
                                            entities.SaveChanges();
                                            //End

                                            #region Copy Template fields data from reference agenda mapping id
                                            if (item.MeetingAgendaMappingID > 0)
                                            {
                                                InsertTemplates(item.MeetingAgendaMappingID, committeeRefPendingMappingID, createdBy);
                                            }
                                            #endregion
                                        }
                                        //Commented on 04 Dec 2019
                                        //entities.BM_MeetingAgendaMapping.Add(item);
                                        //entities.SaveChanges();
                                        //End
                                    }
                                }
                                else
                                {
                                    entities.BM_MeetingAgendaMapping.AddRange(lstPreCommitteeAgenda);
                                    entities.SaveChanges();
                                }
                            }
                        }
                    }
                    #endregion

                    #region Post meeting  after Committee Agenda Item
                    if (Agenda != null)
                    {
                        var parentId = entities.BM_AgendaMaster.Where(k => k.BM_AgendaMasterId == obj.BM_AgendaMasterId && k.Parent_Id != null).Select(k => k.Parent_Id).FirstOrDefault();

                        if (parentId != null)
                        {
                            var query = (from preCommitteeAgenda in entities.BM_AgendaMaster
                                         where ((preCommitteeAgenda.Parent_Id == parentId && preCommitteeAgenda.BM_AgendaMasterId != obj.BM_AgendaMasterId) || preCommitteeAgenda.BM_AgendaMasterId == (long)parentId)
                                         && preCommitteeAgenda.IsDeleted == false
                                         select new
                                         {
                                             MeetingTypeId = preCommitteeAgenda.MeetingTypeId,
                                             MeetingID = obj.Meeting_Id,
                                             AgendaID = preCommitteeAgenda.BM_AgendaMasterId,

                                             AgendaItemText = preCommitteeAgenda.Agenda,
                                             AgendaFormat = preCommitteeAgenda.AgendaFormat,

                                             ResolutionFormatHeading = preCommitteeAgenda.ResolutionFormatHeading,
                                             ResolutionFormat = preCommitteeAgenda.ResolutionFormat,

                                             MinutesFormatHeading = preCommitteeAgenda.MinutesFormatHeading,
                                             MinutesFormat = preCommitteeAgenda.MinutesFormat,
                                             MinutesDisApproveFormat = preCommitteeAgenda.MinutesDisApproveFormat,
                                             MinutesDifferFormat = preCommitteeAgenda.MinutesDifferFormat
                                         });

                            var lstAllAgendaItems = query.ToList();
                            List<BM_MeetingAgendaMapping> lstPreCommitteeAgenda = null;

                            if (lstAllAgendaItems != null)
                            {
                                #region Get Agenda Items only for Applicable Committees
                                foreach (var r in lstAllAgendaItems)
                                {
                                    //Check Pre- Committee Applicable or not 
                                    if (lstMandatoryCommittees.Any(k => k == r.MeetingTypeId))
                                    {
                                        if (lstPreCommitteeAgenda == null)
                                        {
                                            lstPreCommitteeAgenda = new List<BM_MeetingAgendaMapping>();
                                        }

                                        lstPreCommitteeAgenda.Add(
                                            new BM_MeetingAgendaMapping()
                                            {
                                                MeetingID = r.MeetingID,
                                                PostMeetingID = r.MeetingID,
                                                AgendaID = r.AgendaID,
                                                PostAgendaID = obj.BM_AgendaMasterId,
                                                PostMappingId = _obj.MeetingAgendaMappingID,
                                                SrNo = 0,
                                                IsClosed = true,
                                                IsActive = true,
                                                IsDeleted = false,
                                                IsNewStage = false,
                                                PartId = 0,

                                                AgendaItemText = r.AgendaItemText,
                                                AgendaItemTextNew = r.AgendaItemText,
                                                AgendaFormat = r.AgendaFormat,
                                                ResolutionFormatHeading = r.ResolutionFormatHeading,
                                                ResolutionFormat = r.ResolutionFormat,
                                                MinutesFormatHeading = r.MinutesFormatHeading,
                                                MinutesFormat = r.MinutesFormat,
                                                MinutesDisApproveFormat = r.MinutesDisApproveFormat,
                                                MinutesDifferFormat = r.MinutesDifferFormat,

                                                CreatedBy = createdBy,
                                                CreatedOn = DateTime.Now
                                            });
                                    }
                                }
                                #endregion

                                if (lstPreCommitteeAgenda != null)
                                {
                                    #region Get Customer-wise Agenda Templates
                                    foreach (var item in lstPreCommitteeAgenda)
                                    {
                                        var r = entities.BM_SP_AgendaFormatesByCustomerId(customerID, item.AgendaID).FirstOrDefault();
                                        if (r != null)
                                        {
                                            item.AgendaItemText = r.AgendaItemText;
                                            item.AgendaItemTextNew = r.AgendaItemText;
                                            item.AgendaFormat = r.AgendaFormat;
                                            item.ResolutionFormatHeading = r.ResolutionFormatHeading;
                                            item.ResolutionFormat = r.ResolutionFormat;
                                            item.MinutesFormatHeading = r.MinutesFormatHeading;
                                            item.MinutesFormat = r.MinutesFormat;
                                            item.MinutesDisApproveFormat = r.MinutesDisApproveFormat;
                                            item.MinutesDifferFormat = r.MinutesDifferFormat;

                                            item.SEBI_IntimationFormat = r.SEBI_IntimationFormat;
                                            item.SEBI_DisclosureFormat = r.SEBI_DisclosureFormat;
                                        }
                                    }
                                    #endregion

                                    //Check previous Committee Agenda Items not closed 
                                    if (obj.RefPendingMappingID != null)
                                    {
                                        foreach (var item in lstPreCommitteeAgenda)
                                        {
                                            var committeeRefPendingMappingID = (from row in entities.BM_MeetingAgendaMapping
                                                                                where ((row.PostMeetingID == refPostMeetingId && row.PostAgendaID == refPostAgendaId && row.PostMappingId == refPostMappingId) ||
                                                                                        (row.MeetingID == refPostMeetingId && row.AgendaID == refPostAgendaId && row.MeetingAgendaMappingID == refPostMappingId)) &&
                                                                                row.AgendaID == item.AgendaID && row.IsClosed == false && row.IsDeleted == false
                                                                                select row.MeetingAgendaMappingID).FirstOrDefault();

                                            if (committeeRefPendingMappingID > 0)
                                            {
                                                item.RefPendingMappingID = committeeRefPendingMappingID;

                                                var refAgendaItem = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == committeeRefPendingMappingID).FirstOrDefault();
                                                refAgendaItem.IsClosed = true;
                                                //Get StartMeetingId & StartAgendaId from committeeRefPendingMappingID
                                                item.StartMeetingId = refAgendaItem.StartMeetingId;
                                                item.StartAgendaId = refAgendaItem.StartAgendaId;
                                                item.StartMappingId = refAgendaItem.StartMappingId;
                                                item.SequenceNo = refAgendaItem.SequenceNo;
                                                item.IsNewStage = refAgendaItem.IsNewStage;
                                                //End
                                                //Added on 28 Apr 2020
                                                item.RefMasterID = refAgendaItem.RefMasterID;
                                                entities.SaveChanges();

                                                //Added on 04 Dec 2019
                                                //1)while adding pending agenda item in board & committee has differed flag then show popup for pre-committe agenda item
                                                entities.BM_MeetingAgendaMapping.Add(item);
                                                entities.SaveChanges();
                                                //End

                                                #region Copy Template fields data from reference agenda mapping id
                                                if (item.MeetingAgendaMappingID > 0)
                                                {
                                                    InsertTemplates(item.MeetingAgendaMappingID, committeeRefPendingMappingID, createdBy);
                                                }
                                                #endregion
                                            }
                                            //Commented on 04 Dec 2019
                                            //entities.BM_MeetingAgendaMapping.Add(item);
                                            //entities.SaveChanges();
                                            //End
                                        }
                                    }
                                    else
                                    {
                                        //Set Sequence number 0 in case Add agenda item( pre-committe) having multistage flag then set sequence
                                        foreach (var item in lstPreCommitteeAgenda)
                                        {
                                            #region set Sequence number 0 if multi stage agenda
                                            var hasStages = (from row in entities.BM_AgendaMaster_SubAgendaMappging
                                                             where row.AgendaMasterId == item.AgendaID && row.IsDeleted == false
                                                             select row.Agenda_SubAgendaMappgingID).Any();
                                            if (hasStages == true)
                                            {
                                                item.SequenceNo = 0; //Set 0 from Multi stage agenda
                                                item.IsNewStage = false;
                                            }
                                            #endregion
                                        }

                                        entities.BM_MeetingAgendaMapping.AddRange(lstPreCommitteeAgenda);
                                        entities.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public MeetingAgendaMappingVM GetAgendaItem(long MeetingAgendaMappingID)
        {
            try
            {
                var result = (from Agenda in entities.BM_AgendaMaster
                              join Mapping in entities.BM_MeetingAgendaMapping on Agenda.BM_AgendaMasterId equals Mapping.AgendaID
                              join Meeting in entities.BM_Meetings on Mapping.MeetingID equals Meeting.MeetingID
                              where Mapping.MeetingAgendaMappingID == MeetingAgendaMappingID && Mapping.IsDeleted == false && Mapping.IsActive == true
                              select new MeetingAgendaMappingVM
                              {
                                  MeetingAgendaMappingID = Mapping.MeetingAgendaMappingID,
                                  Meeting_Id = Meeting.MeetingID,
                                  AgendaID = Agenda.BM_AgendaMasterId,
                                  AgendaItemHeading = Agenda.AgendaItemHeading,
                                  AgendaItemText = Mapping.AgendaItemText,
                                  AgendaFormat = Mapping.AgendaFormat,
                                  ResolutionFormatHeading = Mapping.ResolutionFormatHeading,
                                  ResolutionFormat = Mapping.ResolutionFormat,
                                  MinutesFormatHeading = Mapping.MinutesFormatHeading,
                                  MinutesFormat = Mapping.MinutesFormat,
                                  MinutesDisApproveFormat = Mapping.MinutesDisApproveFormat,
                                  MinutesDifferFormat = Mapping.MinutesDifferFormat,

                                  SEBI_IntimationFormat = Mapping.SEBI_IntimationFormat,
                                  SEBI_DisclosureFormat = Mapping.SEBI_DisclosureFormat,

                                  ExplanatoryStatement = Mapping.ExplanatoryStatement,

                                  EntityTypeId_ = Meeting.Entity_Type,
                                  MeetingTypeId_ = Meeting.MeetingTypeId
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public MeetingAgendaMappingVM UpdateAgendaItem(MeetingAgendaMappingVM obj, int updatedBy)
        {
            try
            {
                var _obj = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MeetingAgendaMappingID && k.IsDeleted == false && k.IsActive == true).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.AgendaItemText = obj.AgendaItemText;
                    _obj.AgendaFormat = obj.AgendaFormat;

                    _obj.ResolutionFormatHeading = obj.ResolutionFormatHeading;
                    _obj.ResolutionFormat = obj.ResolutionFormat;

                    _obj.MinutesFormatHeading = obj.MinutesFormatHeading;
                    _obj.MinutesFormat = obj.MinutesFormat;
                    _obj.MinutesDisApproveFormat = obj.MinutesDisApproveFormat;
                    _obj.MinutesDifferFormat = obj.MinutesDifferFormat;

                    _obj.SEBI_IntimationFormat = obj.SEBI_IntimationFormat;
                    _obj.SEBI_DisclosureFormat = obj.SEBI_DisclosureFormat;

                    _obj.ExplanatoryStatement = obj.ExplanatoryStatement;

                    _obj.UpdatedBy = updatedBy;
                    _obj.UpdatedOn = DateTime.Now;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    UpdateAgendaItemFormatedText(obj.MeetingAgendaMappingID);

                    obj.Success = true;
                    obj.Message = "Update Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong.";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.validationError;
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingAgendaMappingVM SaveOrderAgendaItem(MeetingAgendaMappingVM obj, int SrNo)
        {
            try
            {
                if (SrNo == 1)
                {
                    entities.BM_MeetingAgendaMapping.Where(k => k.MeetingID == obj.Meeting_Id && k.PartId == obj.PartId).ToList().ForEach(v => v.SrNo = 0);
                    entities.SaveChanges();
                }
                var _obj = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MeetingAgendaMappingID && k.IsDeleted == false && k.IsActive == true).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.SrNo = SrNo;
                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    obj.Success = true;
                    obj.Message = "Update Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public PreviewAgendaVM PreviewAgenda(long MeetingId, int CustomerId)
        {
            return Preview(MeetingId, CustomerId, false);
        }
        public PreviewAgendaVM Preview(long MeetingId, int CustomerId, bool GenerateMinutes)
        {
            var result = (from row in entities.BM_Meetings
                          join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                          join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                          join fy in entities.BM_YearMaster on row.FY equals fy.FYID
                          from city in entities.Cities.Where(k => k.ID == entity.Regi_CityId).Select(k => k.Name).DefaultIfEmpty()
                          from state in entities.States.Where(k => k.ID == entity.Regi_StateId).Select(k => k.Name).DefaultIfEmpty()
                          where row.MeetingID == MeetingId && row.Customer_Id == CustomerId && row.IsDeleted == false
                          select new PreviewAgendaVM
                          {
                              MeetingID = row.MeetingID,
                              IsVirtual = row.IsVirtualMeeting,
                              IsEVoting = row.IsEVoting,
                              GenerateMinutes = GenerateMinutes,
                              MeetingSrNo = row.MeetingSrNo,
                              MeetingCircular = row.MeetingCircular,
                              MeetingTypeId = row.MeetingTypeId,
                              MeetingTypeName = meeting.MeetingTypeName,
                              Quarter_ = row.Quarter_,
                              MeetingTitle = row.MeetingTitle,
                              NoticeDate = row.SendDateNotice,
                              CircularDate = row.CircularDate,
                              MeetingDate = row.MeetingDate,
                              MeetingTime = row.MeetingTime,
                              MeetingAddressType = row.MeetingVenueType,
                              MeetingVenue = row.MeetingVenue,

                              MeetingStartDate = row.StartMeetingDate,
                              MeetingStartTime = row.StartTime,
                              MeetingEndDate = row.EndMeetingDate,
                              MeetingEndTime = row.EndMeetingTime,

                              GM_Notes = row.Notes,
                              TotalMemberPresentInAGM = row.TotalMemberPresentInAGM,
                              TotalProxyPresentInAGM = row.TotalProxyPresentInAGM,

                              IsAdjourned = row.IsAdjourned,

                              Entityt_Id = row.EntityId,
                              EntityCIN_LLPIN = entity.CIN_LLPIN,
                              EntityName = entity.CompanyName,
                              EntityAddressLine1 = entity.Regi_Address_Line1,
                              EntityAddressLine2 = entity.Regi_Address_Line2,
                              EntityCity = city == null ? "" : city,
                              EntityState = state == null ? "" : state,
                              FYText = fy.FYText
                          }).FirstOrDefault();

            if (result != null)
            {
                result.lstAgendaItems = (from TemplateList in entities.BM_SP_MeetingAgendaTemplateList(MeetingId, null, GenerateMinutes, null)
                                         select new MeetingAgendaMappingVM
                                         {
                                             SrNo = (int)TemplateList.SrNo,
                                             MeetingAgendaMappingID = (long)TemplateList.MeetingAgendaMappingID,
                                             Meeting_Id = TemplateList.MeetingID,
                                             AgendaID = TemplateList.BM_AgendaMasterId,
                                             PartId = TemplateList.PartID,
                                             AgendaItemHeading = TemplateList.AgendaItemHeading,
                                             AgendaItemText = TemplateList.AgendaItemText,

                                             AgendaFormat = TemplateList.AgendaFormat,
                                             ExplanatoryStatement = TemplateList.ExplanatoryStatement,
                                             IsOrdinaryBusiness = TemplateList.IsOrdinaryBusiness,
                                             IsSpecialResolution = TemplateList.IsSpecialResolution
                                         }).ToList();

                if (GenerateMinutes)
                {
                    var minutesDetails = (from row in entities.BM_MeetingMinutesDetails
                                          where row.MeetingId == MeetingId && row.IsDeleted == false
                                          select new
                                          {
                                              row.TotalMemberPresentInAGM,
                                              row.TotalProxyPresentInAGM,
                                              row.TotalMemberInAGM,

                                              //row.TotalValidProxy,
                                              //row.TotalMemberProxy,
                                              //row.TotalShareHeldByProxy,
                                              //row.FaceValueOfEnquityShare,
                                              //row.PercentageOfIssuedCapital
                                          }).FirstOrDefault();

                    if (minutesDetails != null)
                    {
                        result.TotalMemberPresentInAGM = minutesDetails.TotalMemberPresentInAGM;
                        result.TotalProxyPresentInAGM = minutesDetails.TotalProxyPresentInAGM;
                        result.TotalMemberInAGM = minutesDetails.TotalMemberInAGM;

                        //result.TotalValidProxy = minutesDetails.TotalValidProxy;
                        //result.TotalMemberProxy = minutesDetails.TotalMemberProxy;
                        //result.TotalShareHeldByProxy = minutesDetails.TotalShareHeldByProxy;
                        //result.FaceValueOfEnquityShare = minutesDetails.FaceValueOfEnquityShare;
                        //result.PercentageOfIssuedCapital = minutesDetails.PercentageOfIssuedCapital;
                    }

                    if (result.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                    {
                        var hasConsolidatedFinancial = (from row in entities.BM_Meetings
                                                        join mapping in entities.BM_MeetingAgendaMapping on row.MeetingID equals mapping.MeetingID
                                                        join agenda in entities.BM_AgendaMaster on mapping.AgendaID equals agenda.BM_AgendaMasterId
                                                        where row.MeetingID == MeetingId && mapping.IsDeleted == false && mapping.IsActive == true &&
                                                        agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.AUDITED_CONSOLIDATED_FINANCIAL_STATEMENTS
                                                        select row.MeetingID).Any();
                        result.HasConsolidatedFinancial = hasConsolidatedFinancial;
                    }
                }
                else if (result.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || result.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                {
                    result.MeetingSigningAuthorityForAGM = (from row in entities.BM_SP_MeetingSigningAuthorityForAGM(result.MeetingID, result.Entityt_Id)
                                                            select new MeetingSigningAuthorityForAGM_VM
                                                            {
                                                                AuthorityName = row.AuthorityName,
                                                                Designation = row.Designation,
                                                                DIN_PAN = row.DIN_PAN,
                                                                MembershipNo = row.MembershipNo,
                                                                IsCS = row.IsCS,
                                                                IsChairman = row.IsChairman,
                                                                IsAuthorisedSignotory = row.IsAuthorisedSignotory
                                                            }).FirstOrDefault();
                }
            }
            return result;
        }

        public void UpdateAgendaItemFormatedText(long? MeetingAgendaMappingID)
        {
            try
            {
                var mapping = (from row in entities.BM_MeetingAgendaMapping
                               where row.MeetingAgendaMappingID == MeetingAgendaMappingID && row.IsDeleted == false
                               select row).FirstOrDefault();
                if (mapping != null)
                {
                    var formatedText = (from rows in entities.BM_SP_GetAgendaTemplate(MeetingAgendaMappingID, null)
                                        select rows.AgendaItemText).FirstOrDefault();

                    mapping.AgendaItemTextNew = formatedText;

                    entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    MeetingAgendaSetGenerationFlag(mapping.MeetingID);
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void SetDefaultItemNumbers(long MeetingId)
        {
            try
            {
                using (Compliance_SecretarialEntities context = new Compliance_SecretarialEntities())
                {
                    context.BM_SP_MeetingAgendaSetItemNo(MeetingId);
                    context.SaveChanges();
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            MeetingAgendaSetGenerationFlag(MeetingId);
        }
        #endregion

        #region Move Any Other business Agenda Items to Part B
        public void MoveAnyOtherAgendaItems(long MeetingId)
        {
            try
            {
                entities.BM_SP_MeetingAnyOtherAgendaChangePart(MeetingId);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Delete Agenda
        public DeleteAgendaVM GetAllowAgendaDelete(long meetingId, long meetingAgendaMappingId, bool IsMeetingDelete)
        {
            var obj = new DeleteAgendaVM();
            try
            {
                obj = (from row in entities.BM_SP_MeetingAgendaAllowDelete(meetingId, meetingAgendaMappingId, IsMeetingDelete)
                       select new DeleteAgendaVM
                       {
                           MeetingId_del = meetingId,
                           MeetingAgendaMappingId_del = meetingAgendaMappingId,
                           AllowDelete = row.AllowDelete,
                           MessageDetails = row.Message,
                       }).FirstOrDefault();

                if (obj == null)
                {
                    obj = new DeleteAgendaVM()
                    {
                        MeetingId_del = meetingId,
                        MeetingAgendaMappingId_del = meetingAgendaMappingId,
                        AllowDelete = false,
                        MessageDetails = "Something wents wrong."
                    };
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public DeleteAgendaVM DeleteAgendaItem(DeleteAgendaVM obj, int userId)
        {
            try
            {
                var result = (from row in entities.BM_SP_MeetingAgendaDelete(obj.MeetingId_del, obj.MeetingAgendaMappingId_del, false, userId)
                              select row).FirstOrDefault();

                if (result == true)
                {
                    //Set Item Numbers
                    SetDefaultItemNumbers(obj.MeetingId_del);

                    obj.AllowDelete = false;
                    obj.Success = true;
                    obj.Message = "Agenda Item Deleted Successfully.";
                }
                else
                {
                    obj.AllowDelete = false;
                    obj.Error = true;
                    obj.Message = "Something wents wrong.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Meeting Pre Committee Agenda
        public List<MeetingPendingPreCommitteeVM> GetPendingPreCommittee(long MeetingId)
        {
            try
            {
                //var result = (from com in entities.BM_CommitteeComp
                //              join preAgenda in entities.BM_AgendaMaster on com.Id equals preAgenda.MeetingTypeId
                //              join mapping in entities.BM_MeetingAgendaMapping on preAgenda.BM_AgendaMasterId equals mapping.AgendaID
                //              join meeting in entities.BM_Meetings on mapping.MeetingID equals meeting.MeetingID
                //              where mapping.MeetingID == MeetingId && mapping.PartId == 0 && mapping.IsDeleted == false
                //              orderby com.Id
                //              select new MeetingPendingPreCommitteeVM
                //              {
                //                  MeetingTypeID = com.Id,
                //                  SourceMeetingID = mapping.MeetingID,
                //                  SourceMeetingAgendaMappingID = mapping.MeetingAgendaMappingID,
                //                  //Commented on 11 Dec 2019
                //                  //PostAgendaID = mapping.PostAgendaID,
                //                  MeetingTypeName = com.MeetingTypeName,
                //                  EntityID = meeting.EntityId

                //              }
                //              ).Distinct().ToList();

                var result = (from meeting in entities.BM_Meetings
                              join mapping in entities.BM_MeetingAgendaMapping on meeting.MeetingID equals mapping.MeetingID
                              join preAgenda in entities.BM_AgendaMaster on mapping.AgendaID equals preAgenda.BM_AgendaMasterId
                              join com in entities.BM_CommitteeComp on preAgenda.MeetingTypeId equals com.Id
                              where meeting.MeetingID == MeetingId && mapping.PartId == 0 && mapping.IsDeleted == false
                              orderby com.Id
                              select new MeetingPendingPreCommitteeVM
                              {
                                  MeetingTypeID = com.Id,
                                  SourceMeetingID = meeting.MeetingID,
                                  //SourceMeetingAgendaMappingID = mapping.MeetingAgendaMappingID,
                                  //Commented on 11 Dec 2019
                                  //PostAgendaID = mapping.PostAgendaID,
                                  MeetingTypeName = com.MeetingTypeName,
                                  EntityID = meeting.EntityId

                              }
                              ).Distinct().ToList();

                if (result != null)
                {
                    foreach (var committee in result)
                    {
                        committee.lstPreCommiteeAgenda = (from com in entities.BM_CommitteeComp
                                                          join preAgenda in entities.BM_AgendaMaster on com.Id equals preAgenda.MeetingTypeId
                                                          join mapping in entities.BM_MeetingAgendaMapping on preAgenda.BM_AgendaMasterId equals mapping.AgendaID
                                                          where mapping.MeetingID == MeetingId && mapping.PartId == 0 && mapping.IsDeleted == false
                                                          && com.Id == committee.MeetingTypeID
                                                          orderby com.Id
                                                          select new MeetingPreCommitteeAgendaItemVM
                                                          {
                                                              PreCommitteeAgendaID = preAgenda.BM_AgendaMasterId,
                                                              PreCommitteeAgendaItem = preAgenda.Agenda
                                                          }).ToList();
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public List<PreCommiitteeDraftMeetingVM> GetDraftCommitteeMeeting(long PreCommitteeId, int EntityId, int CustomerId)
        {
            var result = (from committee in entities.BM_CommitteeComp
                          join meeting in entities.BM_Meetings on committee.Id equals meeting.MeetingTypeId
                          //where committee.Id == PreCommitteeId && committee.CompositionType == "C" && committee.IsDeleted == false
                          where committee.Id == PreCommitteeId && committee.IsDeleted == false
                          && meeting.IsAgendaSent == false && meeting.EntityId == EntityId && meeting.Customer_Id == CustomerId
                          && meeting.IsDeleted == false && (meeting.IsCompleted == false || meeting.IsVirtualMeeting == true)
                          orderby committee.SerialNo
                          select new PreCommiitteeDraftMeetingVM()
                          {
                              MeetingID = meeting.MeetingID,
                              MeetingTitle = meeting.MeetingTitle == null ? "Draft - " + committee.MeetingTypeName : meeting.MeetingTitle
                          }).ToList();

            if (result == null)
            {
                result = new List<PreCommiitteeDraftMeetingVM>();
            }
            result.Insert(0, new PreCommiitteeDraftMeetingVM() { MeetingID = 0, MeetingTitle = "New" });

            return result;
        }

        public string SavePreCommitteeMeetingID(int MeetingTypeID, long SourceMeetingID, long SourceMeetingAgendaMappingID, long? PostAgendaId, long TargetMeetingID, int UserId)
        {
            var result = "";
            try
            {
                long? PostAgendaIdNew = null;

                long? PostMappingId = null;
                long? PostMappingIdNew = null;

                int customerId = 0;
                if (TargetMeetingID == 0)
                {
                    var meeting = entities.BM_Meetings.Where(k => k.MeetingID == SourceMeetingID && k.IsDeleted == false).FirstOrDefault();
                    if (meeting != null)
                    {
                        var obj = new Meeting_NewVM()
                        {
                            MeetingSrNo = null,
                            MeetingTypeId = MeetingTypeID,
                            Type = meeting.MeetingCircular,
                            Quarter_ = meeting.Quarter_,
                            IsShorter = meeting.IsShorter,

                            Entityt_Id = (int)meeting.EntityId,
                            CustomerId = (int)meeting.Customer_Id,

                            UserId = UserId,
                            FYID = meeting.FY,
                            IsVirtualMeeting = meeting.IsVirtualMeeting
                        };

                        obj = Create(obj);
                        if (obj.Success)
                        {
                            TargetMeetingID = obj.MeetingID;
                            customerId = (int)meeting.Customer_Id;
                        }
                        else
                        {
                            result = obj.Message;
                            return result;
                        }
                    }
                }

                if (TargetMeetingID > 0)
                {
                    var query = (from com in entities.BM_CommitteeComp
                                 join preAgenda in entities.BM_AgendaMaster on com.Id equals preAgenda.MeetingTypeId
                                 join mapping in entities.BM_MeetingAgendaMapping on preAgenda.BM_AgendaMasterId equals mapping.AgendaID
                                 join meeting in entities.BM_Meetings on mapping.MeetingID equals meeting.MeetingID
                                 where mapping.MeetingID == SourceMeetingID && mapping.PartId == 0 && mapping.IsDeleted == false
                                 && com.Id == MeetingTypeID
                                 orderby com.Id
                                 select new
                                 {
                                     MeetingAgendaMappingID = mapping.MeetingAgendaMappingID,
                                     CustomerId = meeting.Customer_Id,
                                     PartId = 2
                                 }).ToList();

                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            customerId = Convert.ToInt32(item.CustomerId);
                            var _obj = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == item.MeetingAgendaMappingID && k.IsDeleted == false).FirstOrDefault();
                            if (_obj != null)
                            {
                                _obj.MeetingID = TargetMeetingID;
                                _obj.PartId = item.PartId;

                                //Check agenda items not differed in items
                                if (_obj.RefPendingMappingID == null)
                                {
                                    //For trace agenda flow (history)
                                    _obj.StartAgendaId = _obj.AgendaID;
                                    _obj.StartMeetingId = TargetMeetingID;
                                    //Commented on 09 ar 2020
                                    //_obj.StartMappingId = _obj.MeetingAgendaMappingID;
                                    //End
                                }

                                if (MeetingTypeID == 10)
                                {
                                    //Add on 11 Dec 2019
                                    PostAgendaId = _obj.PostAgendaID;
                                    //End
                                    PostMappingId = _obj.PostMappingId;

                                    _obj.PostMeetingID = null;
                                    _obj.PostAgendaID = null;
                                    _obj.PostMappingId = null;
                                }

                                PostAgendaIdNew = _obj.AgendaID;
                                PostMappingIdNew = _obj.MeetingAgendaMappingID;

                                entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                                entities.SaveChanges();

                                if (MeetingTypeID == 10 && PostAgendaIdNew != null)
                                {
                                    //Set new PostMeeting Id & Post Agenda Id from Board Meeting
                                    entities.BM_MeetingAgendaMapping.Where(k => (k.PostMeetingID == SourceMeetingID && k.PostAgendaID == PostAgendaId && k.PostMappingId == PostMappingId) || (k.MeetingID == SourceMeetingID && k.AgendaID == PostAgendaId && k.MeetingAgendaMappingID == PostMappingId)).ToList().ForEach(k => { k.PostMeetingID = TargetMeetingID; k.PostAgendaID = PostAgendaIdNew; k.PostMappingId = PostMappingIdNew; });
                                    entities.SaveChanges();
                                }
                            }
                        }
                    }
                    result = "true";

                    //Commented on 11 Dec 2019
                    //if (MeetingTypeID == 10 && PostAgendaIdNew != null)
                    //{
                    //    //Set new PostMeeting Id & Post Agenda Id from Board Meeting
                    //    entities.BM_MeetingAgendaMapping.Where(k => (k.PostMeetingID == SourceMeetingID && k.PostAgendaID == PostAgendaId) || (k.MeetingID == SourceMeetingID && k.AgendaID == PostAgendaId)).ToList().ForEach(k => { k.PostMeetingID = TargetMeetingID; k.PostAgendaID = PostAgendaIdNew; });
                    //    entities.SaveChanges();
                    //}

                    #region Added on 03 Sep 2020
                    SetDefaultItemNumbers(TargetMeetingID);

                    objIMeeting_Compliances.GenerateScheduleOn(TargetMeetingID, SecretarialConst.ComplianceMappingType.AGENDA, customerId, UserId);

                    #region Compliance Activation before Meeting
                    objIComplianceTransaction_Service.CreateTransactionOnNoticeSend(TargetMeetingID, UserId);
                    #endregion
                    #endregion
                }
                else
                {
                    result = "false";
                }
            }
            catch (Exception ex)
            {
                result = "error";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Meeting Agenda Item Data Fields Template Generation
        public MeetingAgendaTemplateVM GetAgendaItemTemplate(long MeetingAgendaMappingID)
        {
            try
            {
                var result = (from Meeting in entities.BM_Meetings
                              join Mapping in entities.BM_MeetingAgendaMapping on Meeting.MeetingID equals Mapping.MeetingID
                              where Mapping.MeetingAgendaMappingID == MeetingAgendaMappingID && Meeting.IsDeleted == false && Mapping.IsDeleted == false && Mapping.IsActive == true
                              select new MeetingAgendaTemplateVM
                              {
                                  MeetingAgendaMappingID = Mapping.MeetingAgendaMappingID,
                                  Meeting_Id = Mapping.MeetingID,
                                  AgendaID = Mapping.AgendaID,
                                  EntityID_Ref = Meeting.EntityId
                              }).FirstOrDefault();

                if (result != null)
                {
                    result.lstControls = (from row in entities.BM_AgendaMasterTemplate
                                          from templateValue in entities.BM_MeetingAgendaTemplateList.Where(k => k.MeetingAgendaMappingID == result.MeetingAgendaMappingID && k.TemplateID == row.TemplateID && k.IsDeleted == false).DefaultIfEmpty()
                                          where row.AgendaID == result.AgendaID && row.IsTemplateField == true && row.IsDeleted == false
                                          select new MeetingAgendaTemplateListVM
                                          {
                                              TemplateID = row.TemplateID,
                                              CategoryID = row.CategoryID,
                                              TemplateName = row.TemplateName,
                                              TemplateLabel = row.TemplateLabel,
                                              TemplateListID = templateValue == null ? 0 : templateValue.TemplateListID,
                                              TemplateValue = templateValue == null ? "" : templateValue.TemplateListValue,
                                              MeetingAgendaMapping_ID = result.MeetingAgendaMappingID
                                          }
                                          ).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public MeetingAgendaTemplateVM SaveUpdateAgendaItemTemplateList(MeetingAgendaTemplateVM obj, int userID)
        {
            try
            {
                if (obj.lstControls != null)
                {
                    foreach (var item in obj.lstControls)
                    {
                        if (item.TemplateListID == 0)
                        {
                            var _obj = new BM_MeetingAgendaTemplateList()
                            {
                                TemplateListID = item.TemplateListID,
                                TemplateListValue = item.TemplateValue,
                                TemplateID = item.TemplateID,
                                MeetingAgendaMappingID = item.MeetingAgendaMapping_ID,
                                IsDeleted = false,
                                CreatedBy = userID,
                                CreatedOn = DateTime.Now
                            };
                            entities.BM_MeetingAgendaTemplateList.Add(_obj);
                            entities.SaveChanges();
                            item.TemplateListID = _obj.TemplateListID;
                        }
                        else
                        {
                            var _obj = entities.BM_MeetingAgendaTemplateList.Where(k => k.TemplateListID == item.TemplateListID && k.MeetingAgendaMappingID == item.MeetingAgendaMapping_ID && k.IsDeleted == false).FirstOrDefault();
                            if (_obj != null)
                            {
                                _obj.TemplateListValue = item.TemplateValue;
                                _obj.UpdatedBy = userID;
                                _obj.UpdatedOn = DateTime.Now;
                                entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                                entities.SaveChanges();
                            }
                        }
                    }
                    obj.Success = true;
                    obj.Message = "Saved Successfully.";

                    if (obj.lstControls.Count > 0)
                    {
                        var sourceMappingId = obj.lstControls.First().MeetingAgendaMapping_ID;
                        //entities.BM_SP_MeetingAgendaTemplateFieldsOnChanged(sourceMappingId, userID);
                        //entities.SaveChanges();
                        var result = (from row in entities.BM_SP_MeetingAgendaTemplateFieldsOnChanged(sourceMappingId, userID)
                                      select row).FirstOrDefault();


                        UpdateAgendaItemFormatedText(sourceMappingId);
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.validationError;
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public bool InsertTemplates(long? targetMappingId, long? sourceMappingId, int createdBy)
        {
            try
            {
                //var result = entities.BM_SP_MeetingAgendaTemplateFieldsFromRef(targetMappingId, sourceMappingId, createdBy);
                ////entities.SaveChanges();
                //return true;
                using (Compliance_SecretarialEntities context = new Compliance_SecretarialEntities())
                {
                    context.BM_SP_MeetingAgendaTemplateFieldsFromRef(targetMappingId, sourceMappingId, createdBy);
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Meeting New Agenda Item Create/Update
        public AgendaMasterVM CreateNewAgendaItem(long meetingId, int CustomerId)
        {
            try
            {
                return (from row in entities.BM_Meetings
                        join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                        where row.MeetingID == meetingId && row.Customer_Id == CustomerId && row.IsDeleted == false
                        select new AgendaMasterVM
                        {
                            MeetingTypeId = row.MeetingTypeId,
                            EntityType = row.Entity_Type,
                            MeetingId = meetingId,
                            PartId = 2,
                            IsMultiStageAgenda = false,
                            CanPassedByCircularResolution = false,
                            IsPostBallotRequired = false,
                        }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public AgendaMasterVM CreateNewAgendaItem(AgendaMasterVM obj, int Customer_Id)
        {
            try
            {
                obj.Message = new Response();

                BM_AgendaMaster _obj = new BM_AgendaMaster();
                _obj.AgendaItemHeading = obj.AgendaHeading;
                _obj.Agenda = obj.Agenda;
                _obj.EntityType = obj.EntityType;
                _obj.MeetingTypeId = obj.MeetingTypeId;

                _obj.Customer_Id = Customer_Id;
                _obj.IsLive = true;

                _obj.FrequencyId = obj.FrequencyId;
                _obj.PreCommitteeMeetingTypeId = obj.PreCommitteeMeetingTypeId;
                _obj.IsVoting = obj.IsVoting;
                _obj.IsNoting = obj.IsNoting;
                _obj.PartId = obj.PartId;
                _obj.IsMultiStageAgenda = obj.IsMultiStageAgenda;
                _obj.IsPostBallotRequired = obj.IsPostBallotRequired;
                _obj.CanPassedByCircularResolution = obj.CanPassedByCircularResolution;

                _obj.IsSubAgenda = obj.IsSubAgenda;

                _obj.IsDeleted = false;
                _obj.CreatedBy = obj.UserId;
                _obj.CreatedOn = DateTime.Now;

                _obj.AgendaFormat = obj.AgendaFormat;

                _obj.ResolutionFormatHeading = obj.ResolutionFormatHeading;
                _obj.ResolutionFormat = obj.ResolutionFormat;

                _obj.MinutesFormatHeading = obj.MinutesFormatHeading;
                _obj.MinutesFormat = obj.MinutesFormat;
                _obj.MinutesDisApproveFormat = obj.MinutesDisApproveFormat;
                _obj.MinutesDifferFormat = obj.MinutesDifferFormat;

                entities.BM_AgendaMaster.Add(_obj);
                entities.SaveChanges();

                obj.BM_AgendaMasterId = _obj.BM_AgendaMasterId;
                obj.Message.Success = true;
                obj.Message.Message = "Saved Successfully.";

                AgendaItemSelect objAgendaItemSelect = new AgendaItemSelect()
                {
                    BM_AgendaMasterId = obj.BM_AgendaMasterId,
                    Meeting_Id = (long)obj.MeetingId,
                    PartID = obj.PartId
                };

                AddAgendaItem(objAgendaItemSelect, obj.UserId, Customer_Id);
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public AgendaMasterVM Update(AgendaMasterVM obj)
        {
            try
            {
                obj.Message = new Response();
                BM_AgendaMaster _obj = entities.BM_AgendaMaster.Where(k => k.BM_AgendaMasterId == obj.BM_AgendaMasterId && k.IsDeleted == false).FirstOrDefault();
                if (_obj != null)
                {
                    _obj.AgendaItemHeading = obj.AgendaHeading;
                    _obj.Agenda = obj.Agenda;
                    //_obj.EntityType = obj.EntityType;
                    //_obj.MeetingTypeId = obj.MeetingTypeId;

                    _obj.FrequencyId = obj.FrequencyId;
                    //_obj.PreCommitteeMeetingTypeId = obj.PreCommitteeMeetingTypeId;
                    _obj.IsVoting = obj.IsVoting;
                    //_obj.IsNoting = obj.IsNoting;
                    //_obj.PartId = obj.PartId;
                    //_obj.IsMultiStageAgenda = obj.IsMultiStageAgenda;
                    //_obj.IsPostBallotRequired = obj.IsPostBallotRequired;
                    //_obj.CanPassedByCircularResolution = obj.CanPassedByCircularResolution;

                    //_obj.IsSubAgenda = obj.IsSubAgenda;

                    _obj.IsDeleted = false;
                    _obj.UpdatedBy = obj.UserId;
                    _obj.UpdatedOn = DateTime.Now;

                    _obj.AgendaFormat = obj.AgendaFormat;
                    _obj.ResolutionFormat = obj.ResolutionFormat;
                    _obj.MinutesFormat = obj.MinutesFormat;
                    _obj.MinutesDisApproveFormat = obj.MinutesDisApproveFormat;
                    _obj.MinutesDifferFormat = obj.MinutesDifferFormat;

                    _obj.ResolutionFormatHeading = obj.ResolutionFormatHeading;
                    _obj.MinutesFormatHeading = obj.MinutesFormatHeading;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    #region Update Agenda Mapping
                    var _objAgendaMapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingID == obj.MeetingId && k.AgendaID == obj.BM_AgendaMasterId && k.IsDeleted == false && k.IsActive == true).FirstOrDefault();

                    if (_objAgendaMapping != null)
                    {
                        _objAgendaMapping.AgendaItemText = obj.Agenda;
                        _objAgendaMapping.AgendaFormat = obj.AgendaFormat;
                        _objAgendaMapping.ResolutionFormat = obj.ResolutionFormat;
                        _objAgendaMapping.MinutesFormat = obj.MinutesFormat;
                        _objAgendaMapping.MinutesDisApproveFormat = obj.MinutesDisApproveFormat;
                        _objAgendaMapping.MinutesDifferFormat = obj.MinutesDifferFormat;

                        _objAgendaMapping.ResolutionFormatHeading = obj.ResolutionFormatHeading;
                        _objAgendaMapping.MinutesFormatHeading = obj.MinutesFormatHeading;

                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                    #endregion

                    obj.Message.Success = true;
                    obj.Message.Message = "Updated Successfully.";
                }
                else
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something wents wrong";
                }
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }
        #endregion

        #region Meeting Agenda change Items No
        public Message CheckAgendaItemNumberIsValid(long meetingId)
        {
            var result = new Message();
            try
            {
                var isvalid = (from row in entities.BM_SP_MeetingAgendaItemCheckSrNo(meetingId)
                               select row).FirstOrDefault();

                if (isvalid == true)
                {
                    result.Success = true;
                    result.Error = false;
                }
                else
                {
                    result.Success = false;
                    result.Error = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<ChangeAgendaItemNumberVM> GetAgendaItemForChangeNumber(long meetingId)
        {
            var result = new List<ChangeAgendaItemNumberVM>();
            try
            {
                result = (from row in entities.BM_SP_MeetingAgendaItemListForChangeItemNumber(meetingId)
                          select new ChangeAgendaItemNumberVM
                          {
                              ChangeMeetingAgendaMappingID = row.MeetingAgendaMappingID,
                              ItemNo = row.ItemNo,
                              ChangeAgendaItemText = row.AgendaItemText,
                              ReadOnly = (bool)row.IsComplianceClosed
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public IEnumerable<ChangeAgendaItemNumberVM> SaveAgendaItemNumber(IEnumerable<ChangeAgendaItemNumberVM> obj, int updatedBy)
        {
            try
            {
                var flag = false;
                long meetingId = 0;
                if (obj != null)
                {
                    foreach (var item in obj)
                    {
                        if (item.ReadOnly == false)
                        {
                            var _obj = (from row in entities.BM_MeetingAgendaMapping
                                        where row.MeetingAgendaMappingID == item.ChangeMeetingAgendaMappingID && row.IsDeleted == false && row.IsActive == true
                                        select row).FirstOrDefault();

                            if (_obj != null)
                            {
                                meetingId = _obj.MeetingID;
                                _obj.ItemNo = item.ItemNo;
                                _obj.UpdatedBy = updatedBy;
                                _obj.UpdatedOn = DateTime.Now;
                                entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                                entities.SaveChanges();
                                flag = true;
                            }
                        }
                    }

                    if(flag && meetingId > 0)
                    {
                        MeetingAgendaSetGenerationFlag(meetingId);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Meeting Notice Mail
        public MeetingNoticeMailVM UpdateNoticeMail(MeetingNoticeMailVM obj)
        {
            try
            {
                var _obj = (from row in entities.BM_Meetings
                            where row.MeetingID == obj.NoticeMailID && row.IsDeleted == false
                            select row
                           ).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.NoticeMail = obj.NoticeMail;
                    _obj.TemplateType = obj.TemplateType;
                    //_obj.AvailabilityDueDate = obj.SeekAvailabilityDueDate;
                    _obj.UpdatedBy = obj.UserId;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = "Update Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingNoticeMailVM PreviewNoticeMail(MeetingNoticeMailVM obj)
        {
            try
            {
                var result = (from row in entities.BM_SP_GenerateNoticeMail(obj.NoticeMailID, obj.NoticeMail, obj.Type.Trim())
                              select row
                            ).FirstOrDefault();

                if (result != null)
                {
                    obj.NoticeMail = result;
                    obj.Success = true;
                    obj.Message = "success";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingNoticeMailVM SendNoticeMail(MeetingNoticeMailVM obj, out string MailFormat)
        {
            MailFormat = "";
            try
            {
                var _obj = (from row in entities.BM_Meetings
                            where row.MeetingID == obj.NoticeMailID && row.IsDeleted == false
                            select row).FirstOrDefault();

                if (_obj != null)
                {
                    var noticeSendDate = DateTime.Now.Date;

                    #region Virtual meeting
                    if ((_obj.IsVirtualMeeting == true && _obj.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.MEETING) || (_obj.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || _obj.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM))
                    {
                        if (_obj.MeetingSrNo == null)
                        {
                            obj.Error = true;
                            obj.Message = "Please set meeting serial number before send notice.";
                            return obj;
                        }

                        if (obj.NoticeSendDate.HasValue)// && !string.IsNullOrEmpty(obj.NoticeSendTime))
                        {
                            noticeSendDate = Convert.ToDateTime(obj.NoticeSendDate);
                            //if (!DateTime.TryParse(Convert.ToDateTime(obj.NoticeSendDate).ToString("dd/MMM/yyyy") + " " + obj.NoticeSendTime, out noticeSendDate))
                            //{
                            //    obj.Error = true;
                            //    obj.Message = "Please enter valid notice date & time.";
                            //    return obj;
                            //}
                        }
                        else
                        {
                            obj.Error = true;
                            obj.Message = "Please set notice date & time.";
                            return obj;
                        }
                    }
                    #endregion

                    #region Virtual Circular
                    if (_obj.IsVirtualMeeting == true && _obj.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.CIRCULAR)
                    {
                        if (_obj.MeetingSrNo == null)
                        {
                            obj.Error = true;
                            obj.Message = "Please set circular serial number before send notice.";
                            return obj;
                        }
                    }
                    #endregion

                    var meetingDate = _obj.MeetingDate;
                    var meetingTime = _obj.MeetingTime;
                    if (_obj.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.MEETING)
                    {
                        if (meetingDate.HasValue && !string.IsNullOrEmpty(meetingTime))
                        {
                            var noticeDueDate = DateTime.Now.Date;
                            var actualMeetingDateTime = Convert.ToDateTime(Convert.ToDateTime(meetingDate).ToString("dd/MMM/yyyy") + " " + meetingTime);

                            var entityType = _obj.Entity_Type;
                            int? noticeDays = null;

                            #region Notice should send before Meeting Time
                            if (actualMeetingDateTime < DateTime.Now && _obj.IsVirtualMeeting != true && _obj.MeetingTypeId != SecretarialConst.MeetingTypeID.AGM && _obj.MeetingTypeId != SecretarialConst.MeetingTypeID.EGM)
                            {
                                obj.Error = true;
                                obj.Message = "Please check meeting Date & Time.";
                                return obj;
                            }
                            #endregion

                            #region check Time in case Quarterly Listed meeting
                            if (_obj.Entity_Type == 10 && !string.IsNullOrEmpty(_obj.Quarter_))
                            {
                                var dependMeetingDateTime = entities.BM_SP_CheckMeetingTimeIsValid(_obj.EntityId, _obj.FY, _obj.Quarter_, _obj.MeetingTypeId).FirstOrDefault();
                                if (dependMeetingDateTime != null)
                                {
                                    if (_obj.MeetingTypeId == 10)
                                    {
                                        if (dependMeetingDateTime >= actualMeetingDateTime)
                                        {
                                            obj.Error = true;
                                            obj.Message = "Meeting should be held after Committee Meeting.";
                                            return obj;
                                        }
                                    }
                                    else if (dependMeetingDateTime <= actualMeetingDateTime)
                                    {
                                        obj.Error = true;
                                        obj.Message = "Meeting should be held before Board Meeting.";
                                        return obj;
                                    }
                                }
                            }
                            #endregion

                            #region check notice days before the date of meeting
                            if (entityType == 10 && _obj.MeetingTypeId == 10 && _obj.IsShorter == true)
                            {
                                noticeDays = (from row in entities.BM_MeetingDueDates
                                              where row.UsedFor == "Notice" &&
                                              row.Entity_Type == entityType &&
                                              row.MeetingTypeID == _obj.MeetingTypeId &&
                                              row.IsShorter == _obj.IsShorter
                                              select row.Day_).FirstOrDefault();
                            }
                            else
                            {
                                noticeDays = (from row in entities.BM_MeetingDueDates
                                              where row.UsedFor == "Notice" &&
                                              row.Entity_Type == null &&
                                              row.MeetingTypeID == _obj.MeetingTypeId &&
                                              row.IsShorter == _obj.IsShorter
                                              select row.Day_).FirstOrDefault();
                            }

                            if (noticeDays != null)
                            {
                                //noticeDueDate = Convert.ToDateTime(meetingDate).AddDays((Convert.ToInt32(noticeDays) + 1) * -1);
                                noticeDueDate = Convert.ToDateTime(meetingDate).AddDays((Convert.ToInt32(noticeDays)) * -1);
                            }
                            #endregion

                            if (noticeDueDate < noticeSendDate)
                            {
                                obj.Error = true;
                                obj.Message = "The notice must be provided atleast " + Convert.ToInt32(noticeDays) + " days before the date of meeting.";
                            }
                            else if (Convert.ToDateTime(meetingDate) < noticeSendDate)
                            {
                                obj.Error = true;
                                obj.Message = "Please Check Meeting Date, Notice can be sent on or before Meeting Date-" + Convert.ToDateTime(meetingDate).ToString("dd/MMM/yyyy");
                                //obj.Message = "Please Enter Valid Meeting Date";
                            }
                            else
                            {
                                #region Generate Meeting SrNo
                                if (_obj.MeetingSrNo == null && _obj.IsVirtualMeeting != true)
                                {
                                    int? Meeting_SrNo = null;
                                    int? temp_SrNo = null;
                                    long? temp_FY = null;
                                    var IsContinuosSrNo = true;

                                    var config = (from row in entities.BM_ConfigurationMaster
                                                  where row.MeetingType == _obj.MeetingTypeId && row.EntityID == _obj.EntityId && row.IsDeleted == false
                                                  select new { row.LastMeetingNo, row.LastCircularNo, row.MeetingNoType, row.CircularNoType, row.MeetingFY, row.CircularFY }).FirstOrDefault();

                                    if (config != null)
                                    {
                                        if (config.MeetingNoType == "FY")
                                        {
                                            IsContinuosSrNo = false;
                                        }

                                        if (obj.Type.Trim() == SecretarialConst.MeetingCircular.MEETING)
                                        {
                                            temp_SrNo = config.LastMeetingNo;
                                            temp_FY = config.MeetingFY;
                                        }
                                        else if (obj.Type.Trim() == SecretarialConst.MeetingCircular.CIRCULAR)
                                        {
                                            temp_SrNo = config.LastCircularNo;
                                            temp_FY = config.CircularFY;
                                        }
                                    }

                                    if (IsContinuosSrNo)
                                    {
                                        //Continueous
                                        Meeting_SrNo = (from row in entities.BM_Meetings
                                                        where row.MeetingTypeId == _obj.MeetingTypeId && row.EntityId == _obj.EntityId && row.MeetingCircular == _obj.MeetingCircular && row.IsDeleted == false
                                                        select row.MeetingSrNo).Max();
                                    }
                                    else
                                    {
                                        //Financial year-wise
                                        Meeting_SrNo = (from row in entities.BM_Meetings
                                                        where row.MeetingTypeId == _obj.MeetingTypeId && row.EntityId == _obj.EntityId && row.MeetingCircular == _obj.MeetingCircular && row.IsDeleted == false &&
                                                        row.FY == _obj.FY
                                                        select row.MeetingSrNo).Max();
                                    }

                                    if (Meeting_SrNo == null && config != null)
                                    {
                                        Meeting_SrNo = IsContinuosSrNo ? temp_SrNo : _obj.FY == temp_FY ? temp_SrNo : null;
                                    }

                                    _obj.MeetingSrNo = Meeting_SrNo == null ? 1 : Meeting_SrNo + 1;

                                    //Set Meeting Title
                                    var meetingTypeName = (from row in entities.BM_CommitteeComp
                                                           where row.Id == _obj.MeetingTypeId
                                                           select row.MeetingTypeName).FirstOrDefault();
                                    var FYText = entities.BM_YearMaster.Where(k => k.FYID == _obj.FY).Select(k => k.FYText).FirstOrDefault();
                                    _obj.MeetingTitle = (obj.Type.Trim() == SecretarialConst.MeetingCircular.MEETING ? "Meeting - " : "Circular - ") + meetingTypeName + " [ " + _obj.MeetingSrNo + " ] " + FYText + (_obj.IsAdjourned == true ? " (Adjourned)" : "");
                                }
                                #endregion

                                #region Save Details

                                //Save Meeting Participants
                                SaveMeetingParticipants((int)_obj.Customer_Id, (int)_obj.EntityId, _obj.MeetingTypeId, _obj.MeetingID, obj.UserId, _obj.MeetingDate);
                                //if (_obj.IsVirtualMeeting == true)
                                //{
                                //    var isParticipant = (from row in entities.BM_MeetingParticipant
                                //                         where row.Meeting_ID == _obj.MeetingID && row.IsDeleted == false
                                //                         select row.MeetingParticipantId).Any();
                                //    if(isParticipant == false)
                                //    {
                                //        SaveMeetingParticipants((int)_obj.Customer_Id, (int)_obj.EntityId, _obj.MeetingTypeId, _obj.MeetingID, obj.UserId);
                                //    }
                                //}
                                //else
                                //{
                                //    SaveMeetingParticipants((int)_obj.Customer_Id, (int)_obj.EntityId, _obj.MeetingTypeId, _obj.MeetingID, obj.UserId);
                                //}

                                _obj.NoticeMail = obj.NoticeMail;
                                _obj.TemplateType = obj.TemplateType;

                                if (obj.TemplateType == "AN" || obj.TemplateType == "A")
                                {
                                    _obj.IsAgendaSent = true;
                                    _obj.SendDateAgenda = noticeSendDate; //DateTime.Now;
                                    if(_obj.GenerateAgendaDocumentOnNotice == true)
                                    {
                                        _obj.GenerateAgendaDocument = true;
                                        _obj.AgendaFilePath = null;
                                        _obj.GenerateAgendaDocumentOnNotice = false;
                                    }
                                }

                                if (obj.TemplateType == "AN" || obj.TemplateType == "N" || obj.TemplateType == "C")
                                {
                                    _obj.IsNoticeSent = true;
                                    _obj.SendDateNotice = noticeSendDate; //DateTime.Now;
                                }

                                if (_obj.IsVirtualMeeting == true)
                                {
                                    _obj.IsCompleted = false;
                                    _obj.IsComplianceClosed = false;
                                }

                                _obj.UpdatedOn = DateTime.Now;
                                _obj.UpdatedBy = obj.UserId;

                                entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                                entities.SaveChanges();

                                obj.IsNoticeSent = true;

                                obj.Success = true;
                                obj.Message = "Update Successfully.";

                                bool IsforVideoConfrencing = _obj.IsVideoMeeting;
                                //IsforVideoConfrencing = IsVideoConferencingApplicable_CustomerWise(obj.CustomerId);
                                #region Create Video Confrencing Meeting Ruchi on 08th March 2021
                                if (IsforVideoConfrencing)
                                {
                                    try
                                    {
                                        string ProviderName = _objvideomeeting.GetProviderName(obj.CustomerId);
                                        if (!string.IsNullOrEmpty(ProviderName))
                                        {
                                            string VMeetingId = _objvideomeeting.GetVMeetingId(_obj.MeetingID);
                                            var checkforzoomtoken = _objvideomeeting.GetToken_Zoom(obj.CustomerId);
                                            if (checkforzoomtoken.token == null)
                                            {
                                                if (ProviderName == SecretarialConst.Providers.Zoom)
                                                {
                                                    _objvideomeeting.GenerateZoomjwtoken(obj.CustomerId);//Generate jwt for zoom
                                                }
                                                else if (ProviderName == SecretarialConst.Providers.Webex)
                                                {
                                                    _objvideomeeting.GenerateCiscoToken(obj.CustomerId);//Generate jwt for zoom
                                                }
                                                else
                                                {
                                                    obj.VMMessage = "Invalid provider name";
                                                }
                                            }
                                            else
                                            {
                                                DateTime tokenexpidate = Convert.ToDateTime(checkforzoomtoken.ExpiryDate).Date;
                                                DateTime todaydate = DateTime.Now.Date;
                                                if (tokenexpidate <= todaydate)
                                                {
                                                    if (ProviderName.ToLower() == SecretarialConst.Providers.Zoom)
                                                    {
                                                        _objvideomeeting.GenerateZoomjwtoken(obj.CustomerId);//Generate jwt for zoom  
                                                    }
                                                    else if (ProviderName.ToLower() == SecretarialConst.Providers.Webex)
                                                    {
                                                        _objvideomeeting.GenerateCiscoToken(obj.CustomerId);//Generate new jwt for cisco webex
                                                    }
                                                }
                                            }
                                            if (checkforzoomtoken.token != null)
                                            {
                                                checkforzoomtoken = _objvideomeeting.GetToken_Zoom(obj.CustomerId);

                                                if (string.IsNullOrEmpty(VMeetingId))
                                                {
                                                    if (ProviderName.ToLower() == SecretarialConst.Providers.Zoom)
                                                    {
                                                        obj.VMMessage = _objvideomeeting.CreateVideoMeeting(obj.NoticeMailID, obj.CustomerId, checkforzoomtoken.token, obj.UserId);
                                                    }
                                                    else if (ProviderName.ToLower() == SecretarialConst.Providers.Webex)
                                                    {
                                                        obj.VMMessage = _objvideomeeting.CreateVideoMeetingCisco(obj.NoticeMailID, obj.CustomerId, checkforzoomtoken.token, obj.UserId);
                                                    }
                                                    else
                                                    {
                                                        obj.VMMessage = "Invalid Provider Name";
                                                    }
                                                }
                                                if (_obj.IsVideoMeeting && !string.IsNullOrEmpty(VMeetingId))
                                                {
                                                    if (ProviderName.ToLower() == SecretarialConst.Providers.Zoom)
                                                    {
                                                        obj.VMMessage = _objvideomeeting.UpdateVideoMeeting(obj.NoticeMailID, obj.CustomerId, checkforzoomtoken.token, obj.UserId);
                                                    }
                                                    else if (ProviderName.ToLower() == SecretarialConst.Providers.Webex)
                                                    {
                                                        obj.VMMessage = _objvideomeeting.UpdateVideoMeetingCisco(obj.NoticeMailID, obj.CustomerId, checkforzoomtoken.token, obj.UserId);
                                                    }
                                                    else
                                                    {
                                                        obj.VMMessage = "Invalid Provider Name";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                #endregion

                                if (obj.TemplateType == "AN" || obj.TemplateType == "N" || obj.TemplateType == "C")
                                {
                                    //Move Any Other business Agenda Items to Part B
                                    MoveAnyOtherAgendaItems(obj.NoticeMailID);
                                }

                                MailFormat = (from row in entities.BM_SP_GenerateNoticeMail(obj.NoticeMailID, obj.NoticeMail, obj.Type)
                                              select row).FirstOrDefault();
                                #endregion
                            }
                        }
                        else
                        {
                            obj.Error = true;
                            obj.Message = "Please Set Meeting Date";
                        }
                    }
                    else if (_obj.MeetingCircular.Trim() == SecretarialConst.MeetingCircular.CIRCULAR)
                    {
                        if (meetingDate.HasValue)
                        {
                            var actualMeetingDateTime = Convert.ToDateTime(meetingDate);
                            var dueDate = DateTime.Now.Date;
                            var circularDate = DateTime.Now.Date;
                            var noticeDueDate = Convert.ToDateTime(actualMeetingDateTime).AddDays(-7);

                            if (_obj.CircularDate == null)
                            {
                                obj.Error = true;
                                obj.Message = "Please enter circular date.";
                                return obj;
                            }

                            circularDate = Convert.ToDateTime(_obj.CircularDate);

                            if (circularDate > actualMeetingDateTime)
                            {
                                obj.Error = true;
                                obj.Message = "Due Date must be greater than Circular Date";
                                return obj;
                            }

                            if (circularDate < noticeDueDate)
                            {
                                obj.Error = true;
                                obj.Message = "Gap between due date and circular date not more than 7 days.";
                                return obj;
                            }

                            if (Convert.ToDateTime(meetingDate) < dueDate && _obj.IsVirtualMeeting == false)
                            {
                                obj.Error = true;
                                obj.Message = "Please Enter Valid Circular Due Date.";
                            }
                            else
                            {
                                #region Generate Meeting SrNo
                                if (_obj.MeetingSrNo == null && _obj.IsVirtualMeeting != true)
                                {
                                    int? Meeting_SrNo = null;
                                    int? temp_SrNo = null;
                                    long? temp_FY = null;
                                    var IsContinuosSrNo = true;

                                    var config = (from row in entities.BM_ConfigurationMaster
                                                  where row.MeetingType == _obj.MeetingTypeId && row.EntityID == _obj.EntityId && row.IsDeleted == false
                                                  select new { row.LastMeetingNo, row.LastCircularNo, row.MeetingNoType, row.CircularNoType, row.MeetingFY, row.CircularFY }).FirstOrDefault();

                                    if (config != null)
                                    {
                                        if (config.MeetingNoType == "FY")
                                        {
                                            IsContinuosSrNo = false;
                                        }

                                        if (obj.Type.Trim() == SecretarialConst.MeetingCircular.MEETING)
                                        {
                                            temp_SrNo = config.LastMeetingNo;
                                            temp_FY = config.MeetingFY;
                                        }
                                        else if (obj.Type.Trim() == SecretarialConst.MeetingCircular.CIRCULAR)
                                        {
                                            temp_SrNo = config.LastCircularNo;
                                            temp_FY = config.CircularFY;
                                        }
                                    }

                                    if (IsContinuosSrNo)
                                    {
                                        //Continueous
                                        Meeting_SrNo = (from row in entities.BM_Meetings
                                                        where row.MeetingTypeId == _obj.MeetingTypeId && row.EntityId == _obj.EntityId && row.MeetingCircular == _obj.MeetingCircular && row.IsDeleted == false
                                                        select row.MeetingSrNo).Max();
                                    }
                                    else
                                    {
                                        //Financial year-wise
                                        Meeting_SrNo = (from row in entities.BM_Meetings
                                                        where row.MeetingTypeId == _obj.MeetingTypeId && row.EntityId == _obj.EntityId && row.MeetingCircular == _obj.MeetingCircular && row.IsDeleted == false &&
                                                        row.FY == _obj.FY
                                                        select row.MeetingSrNo).Max();
                                    }

                                    if (Meeting_SrNo == null && config != null)
                                    {
                                        Meeting_SrNo = IsContinuosSrNo ? temp_SrNo : _obj.FY == temp_FY ? temp_SrNo : null;
                                    }

                                    _obj.MeetingSrNo = Meeting_SrNo == null ? 1 : Meeting_SrNo + 1;

                                    //Set Meeting Title
                                    var meetingTypeName = (from row in entities.BM_CommitteeComp
                                                           where row.Id == _obj.MeetingTypeId
                                                           select row.MeetingTypeName).FirstOrDefault();
                                    var FYText = entities.BM_YearMaster.Where(k => k.FYID == _obj.FY).Select(k => k.FYText).FirstOrDefault();
                                    _obj.MeetingTitle = (obj.Type.Trim() == SecretarialConst.MeetingCircular.MEETING ? "Meeting - " : "Circular - ") + meetingTypeName + " [ " + _obj.MeetingSrNo + " ] " + FYText + (_obj.IsAdjourned == true ? " (Adjourned)" : "");
                                }
                                #endregion

                                #region Save Details
                                //Save Meeting Participants
                                SaveMeetingParticipants((int)_obj.Customer_Id, (int)_obj.EntityId, _obj.MeetingTypeId, _obj.MeetingID, obj.UserId, _obj.MeetingDate);

                                _obj.NoticeMail = obj.NoticeMail;
                                _obj.TemplateType = obj.TemplateType;

                                if (obj.TemplateType == "AN" || obj.TemplateType == "N" || obj.TemplateType == "C")
                                {
                                    _obj.IsAgendaSent = true;
                                    _obj.SendDateAgenda = _obj.CircularDate;

                                    _obj.IsNoticeSent = true;
                                    _obj.SendDateNotice = _obj.CircularDate;

                                    if (_obj.GenerateAgendaDocumentOnNotice == true)
                                    {
                                        _obj.GenerateAgendaDocument = true;
                                        _obj.AgendaFilePath = null;
                                        _obj.GenerateAgendaDocumentOnNotice = false;
                                    }
                                }

                                _obj.UpdatedOn = DateTime.Now;
                                _obj.UpdatedBy = obj.UserId;

                                entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                                entities.SaveChanges();

                                obj.IsNoticeSent = true;

                                obj.Success = true;
                                obj.Message = "Update Successfully.";

                                if (obj.TemplateType == "AN" || obj.TemplateType == "N" || obj.TemplateType == "C")
                                {
                                    //Move Any Other business Agenda Items to Part B
                                    MoveAnyOtherAgendaItems(obj.NoticeMailID);
                                }

                                MailFormat = (from row in entities.BM_SP_GenerateNoticeMail(obj.NoticeMailID, obj.NoticeMail, obj.Type)
                                              select row).FirstOrDefault();
                                #endregion
                            }
                        }
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public long CreateNoticeLog(long meetingId, string templateType, string mailTemplate, string senderEmail, long? fileId, int createdBy)
        {
            long id = 0;
            try
            {
                BM_MeetingNoticeLog log = new BM_MeetingNoticeLog()
                {
                    MeetingID = meetingId,
                    TemplateType = templateType,
                    MailFormat = mailTemplate,
                    FileID = fileId,
                    CreatedBy = createdBy,
                    CreatedOn = DateTime.Now,
                    SenderEmail = senderEmail
                };
                entities.BM_MeetingNoticeLog.Add(log);
                entities.SaveChanges();
                id = log.MeetingNoticeLogID;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return id;
        }

        public bool CreateNoticeTransaction(long noticeLogId, long participantId, DateTime dated, bool success)
        {
            var result = false;
            try
            {
                BM_MeetingNoticeTransaction transaction = new BM_MeetingNoticeTransaction()
                {
                    MeetingNoticeLogID = noticeLogId,
                    MeetingParticipantId = participantId,
                    SentOn = dated,
                    Success = success
                };
                entities.BM_MeetingNoticeTransaction.Add(transaction);
                entities.SaveChanges();

                result = true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<MeetingNoticeLogResultVM> GetNoticeAgendaLog(long meetingId, int customerId, string templateType)
        {
            try
            {
                var result = (from row in entities.BM_SP_MeetingNoticeAgendaLog(meetingId, customerId)
                              select new MeetingNoticeLogResultVM
                              {
                                  MeetingID = row.MeetingID,
                                  MeetingNoticeLogID = row.MeetingNoticeLogID,
                                  ParticipantName = row.ParticipantName,
                                  LogType = row.LogType,
                                  LogTypeStr = row.LogTypeStr,
                                  SendOn = row.SendOn,
                                  DeliveredOn = row.DeliveredOn,
                                  ReadOn = row.ReadOn
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public Message GetNoticeAgendaLogDetails(long id)
        {
            var result = new Message() { Error = true };
            try
            {
                result.Message = (from row in entities.BM_MeetingNoticeLog
                                  where row.MeetingNoticeLogID == id
                                  select row.MailFormat).FirstOrDefault();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public MeetingNoticeMailVM NoticeAgendaTemplate(MeetingNoticeMailVM obj)
        {
            try
            {
                var IsQuarter = false;
                var IsShorter = false;
                var meetingDetails = (from row in entities.BM_Meetings
                                      where row.MeetingID == obj.NoticeMailID
                                      select new
                                      {
                                          row.Quarter_,
                                          row.IsShorter
                                      }).FirstOrDefault();
                if (meetingDetails != null)
                {
                    IsQuarter = !string.IsNullOrEmpty(meetingDetails.Quarter_);
                    IsShorter = meetingDetails.IsShorter;
                }

                var NoticeFormat = (from row in entities.BM_TemplateMaster
                                    where row.TemplateType == obj.TemplateType && row.MeetingTypeId == obj.MeetingType_Id && row.IsQuarter == IsQuarter && row.IsShorter == IsShorter && row.IsDeleted == false
                                    select row.MailTemplate).FirstOrDefault();

                if (NoticeFormat != null)
                {
                    obj.NoticeMail = NoticeFormat;
                    obj.Success = true;
                    obj.Message = "success";
                }
                else
                {
                    obj.NoticeMail = (from row in entities.BM_SP_MeetingDefaultFormatByType(SecretarialConst.MeetingFormatType.NOTICE_MAIL)
                                      select row).FirstOrDefault();
                    obj.Success = true;
                    obj.Message = "success";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public string GetNoticeFormatByMeetingId(long meetingId)
        {
            string mailFormat = "";
            try
            {
                var meeting = (from row in entities.BM_Meetings
                               where row.MeetingID == meetingId && row.IsDeleted == false
                               select new
                               {
                                   row.NoticeMail,
                                   row.MeetingCircular
                               }).FirstOrDefault();
                if (meeting != null)
                {
                    mailFormat = (from row in entities.BM_SP_GenerateNoticeMail(meetingId, meeting.NoticeMail, meeting.MeetingCircular)
                                  select row).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return mailFormat;
        }
        #endregion

        #region Invitee Mail
        public MeetingInviteeMailVM UpdateInviteeMail(MeetingInviteeMailVM obj, int updatedBy)
        {
            try
            {
                var _obj = (from row in entities.BM_Meetings
                            where row.MeetingID == obj.InviteeMailID && row.IsDeleted == false
                            select row
                           ).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.InviteeMail = obj.InviteeMail;
                    _obj.UpdatedBy = updatedBy;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = "Update Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingInviteeMailVM PreviewInviteeMail(MeetingInviteeMailVM obj)
        {
            try
            {
                if (!string.IsNullOrEmpty(obj.InviteeMail))
                {
                    var result = (from row in entities.BM_SP_GenerateNoticeMail(obj.InviteeMailID, obj.InviteeMail, SecretarialConst.MeetingTemplatType.INVITEE_MAIL)
                                  select row
                            ).FirstOrDefault();

                    if (result != null)
                    {
                        obj.InviteeMail = result;
                        obj.Success = true;
                        obj.Message = "success";
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Something went wrong.";
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingInviteeMailVM SendInviteeMail(MeetingInviteeMailVM obj, out string MailFormat)
        {
            MailFormat = "";

            try
            {
                MailFormat = (from row in entities.BM_SP_GenerateNoticeMail(obj.InviteeMailID, obj.InviteeMail, SecretarialConst.MeetingTemplatType.INVITEE_MAIL)
                              select row
                            ).FirstOrDefault();
            }
            catch (Exception ex)
            {
                //obj.Error = true;
                //obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Circular
        public List<Agenda_SummitResponse> GetAgendaResponse(long meetingId, int customerId, int? userId, int? RoleId)
        {
            try
            {
                var GetAgendaResponseData = (from row in entities.BM_SP_AgendaResponse(meetingId)
                                             select new Agenda_SummitResponse
                                             {
                                                 Id = row.BM_AgendaMasterId,
                                                 CircularResponseId = row.CircularResponceId,
                                                 MeetingParticipantId = row.MeetingParticipantId,
                                                 MeetingAgendaMappingId = row.MeetingAgendaMappingID,
                                                 MeetingId = row.MeetingID,
                                                 MeetingName = row.MeetingTypeName,
                                                 AgendaId = row.AgendaID,
                                                 AgendaName = row.Agenda,
                                                 ParticipantName = row.ParticipantName,
                                                 Degignation = row.Designation,
                                                 DueDate = row.Due_Date,
                                                 Response = row.Response,
                                                 Remark = row.Remark,
                                                 //logginUser = row.UserId,
                                                 //RoleId = row.SecretarialRoleId,

                                             }).ToList();
                //if (GetAgendaResponseData.Count > 0)
                //{
                //    if (RoleId == 0 && userId > 0)
                //    {
                //        GetAgendaResponseData = (from row in GetAgendaResponseData where row.logginUser == userId select row).ToList();
                //    }
                //}

                return GetAgendaResponseData;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Agenda_SummitResponse UpdateAgendaResponse(Agenda_SummitResponse item, int userId)
        {
            try
            {
                var IsExists = entities.BM_Meetings_Agenda_Responses.Where(k => k.Meeting_ID == item.MeetingId
                                                                             && k.MeetingParticipantID == item.MeetingParticipantId
                                                                             && k.AgendaId == item.AgendaId
                                                                             && k.MeetingAgendaMappingId == item.MeetingAgendaMappingId
                                                                             && k.IsDeleted == false).FirstOrDefault();
                if (IsExists == null)
                {
                    if (item.Response != null && item.Response != "")
                    {
                        BM_Meetings_Agenda_Responses _obj = new BM_Meetings_Agenda_Responses()
                        {
                            Meeting_ID = item.MeetingId,
                            AgendaId = (long)item.AgendaId,
                            MeetingParticipantID = item.MeetingParticipantId,
                            MeetingAgendaMappingId = item.MeetingAgendaMappingId,
                            CircularResponseTime = Convert.ToString(DateTime.Now.TimeOfDay),
                            //CircularResponseTime = DateTime.Now.ToString("HH:mm tt"),
                            Response = item.Response,
                            Remark = item.Remark,
                            IsDeleted = false,
                            CreatedOn = DateTime.Now,
                            CreatedBy = userId
                        };

                        entities.BM_Meetings_Agenda_Responses.Add(_obj);
                        entities.SaveChanges();
                    }
                    else
                    {
                        IsExists.Meeting_ID = item.MeetingId;
                        IsExists.AgendaId = (long)item.AgendaId;
                        IsExists.MeetingParticipantID = item.MeetingParticipantId;
                        IsExists.MeetingAgendaMappingId = item.MeetingAgendaMappingId;
                        IsExists.CircularResponseTime = DateTime.Now.TimeOfDay.ToString("HH:mm:ss");
                        IsExists.Response = item.Response;
                        IsExists.Remark = item.Remark;
                        IsExists.IsDeleted = false;
                        IsExists.UpdatedOn = DateTime.Now;
                        IsExists.UpdatedBy = userId;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return item;
        }


        //public Agenda_SummitResponse UpdateAgendaResponse(Agenda_SummitResponse item, int userId)
        //{
        //    try
        //    {
        //        var IsExists = entities.BM_Meetings_Agenda_Responses.Where(k => k.Meeting_ID == item.MeetingId &&
        //                        k.MeetingParticipantID == item.MeetingParticipantId && k.AgendaId == item.AgendaId && k.MeetingAgendaMappingId == item.MeetingAgendaMappingId &&
        //                        k.IsDeleted == false 
        //                        //&& k.Id == item.CircularResponseId
        //                        ).FirstOrDefault();
        //        if (IsExists == null)
        //        {
        //            BM_Meetings_Agenda_Responses _obj = new BM_Meetings_Agenda_Responses()
        //            {
        //                Meeting_ID = item.MeetingId,
        //                AgendaId = (long)item.AgendaId,
        //                MeetingParticipantID = item.MeetingParticipantId,
        //                MeetingAgendaMappingId = item.MeetingAgendaMappingId,
        //                CircularResponseTime = Convert.ToString(DateTime.Now.TimeOfDay),
        //                //CircularResponseTime = DateTime.Now.ToString("HH:mm tt"),
        //                Response = item.Response,
        //                Remark = item.Remark,
        //                IsDeleted = false,
        //                CreatedOn = DateTime.Now,
        //                CreatedBy = userId
        //            };

        //            entities.BM_Meetings_Agenda_Responses.Add(_obj);
        //            entities.SaveChanges();
        //        }
        //        else
        //        {
        //            IsExists.Meeting_ID = item.MeetingId;
        //            IsExists.AgendaId = (long)item.AgendaId;
        //            IsExists.MeetingParticipantID = item.MeetingParticipantId;
        //            IsExists.MeetingAgendaMappingId = item.MeetingAgendaMappingId;
        //            IsExists.CircularResponseTime = DateTime.Now.TimeOfDay.ToString("HH:mm:ss");
        //            IsExists.Response = item.Response;
        //            IsExists.Remark = item.Remark;
        //            IsExists.IsDeleted = false;
        //            IsExists.UpdatedOn = DateTime.Now;
        //            IsExists.UpdatedBy = userId;
        //            entities.SaveChanges();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //    return item;
        //}
        public FinalizeResolutionVM FinalizeResolution(FinalizeResolutionVM obj, int userId, int customerId)
        {
            try
            {
                if (obj.ResolutionFinalizeDate.HasValue == false)
                {
                    obj.Error = true;
                    obj.Message = "Please enter resolution finalize date.";
                    return obj;
                }
                var _obj = (from row in entities.BM_Meetings
                            where row.MeetingID == obj.ResolutionFinalizeId && row.IsDeleted == false
                            select row).FirstOrDefault();
                if (_obj != null)
                {
                    var openAgenda = (from row in entities.BM_MeetingOpenAgendaOnCompliance
                                      where row.RefMasterID == obj.ResolutionFinalizeId && row.RefMaster == SecretarialConst.OpenAgendaRefMaster.MEETING && row.IsDeleted == false
                                      select row).FirstOrDefault();
                    if (openAgenda != null)
                    {
                        if (openAgenda.IsNoted)
                        {
                            var meetingTitle = (from row in entities.BM_MeetingAgendaMapping
                                                join meeting in entities.BM_Meetings on row.MeetingID equals meeting.MeetingID
                                                where row.RefOpenAgendaId == openAgenda.Id && row.IsDeleted == false && row.IsActive == true &&
                                                meeting.IsDeleted == false
                                                select new { meeting.MeetingID, meeting.MeetingTitle }).FirstOrDefault();
                            if (meetingTitle != null)
                            {
                                obj.Error = true;
                                obj.Message = "Take note on circular agenda used in " + meetingTitle.MeetingTitle + ".";
                                return obj;
                            }
                        }
                    }

                    if (_obj.CircularDate == null)
                    {
                        obj.Error = true;
                        obj.Message = "Please enter circular date.";
                        return obj;
                    }

                    if (_obj.MeetingDate == null)
                    {
                        obj.Error = true;
                        obj.Message = "Please enter circular due date.";
                        return obj;
                    }

                    if (obj.ResolutionFinalizeDate < _obj.CircularDate)
                    {
                        obj.Error = true;
                        obj.Message = "Circular finalized date should greater than circular date.";
                        return obj;
                    }

                    var isValidCircular = (from row in entities.BM_SP_MeetingCheckIsValidCircularToFinalize(obj.ResolutionFinalizeId)
                                           select row).FirstOrDefault();
                    if (isValidCircular != null)
                    {
                        if (isValidCircular.AgendaCount == 0)
                        {
                            obj.Error = true;
                            obj.Message = "Please add atleast 1 agenda in circular";
                            return obj;
                        }

                        if (isValidCircular.IsVotingPending == true)
                        {
                            obj.Error = true;
                            obj.Message = "Please mark voting for all resolution(s).";
                            return obj;
                        }
                    }

                    _obj.EndMeetingDate = obj.ResolutionFinalizeDate;
                    _obj.IsCompleted = true;
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();

                    ConculdeMeeting(obj.ResolutionFinalizeId, userId, customerId);

                    obj.IsResolutionFinalized = true;
                    obj.Success = true;
                    obj.Message = SecretarialConst.Messages.saveSuccess;
                }
                else
                {
                    obj.Error = true;
                    obj.Message = SecretarialConst.Messages.serverError;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public List<CircularAgendaData> GetAllAgendaDetails(long parentID)
        {
            try
            {

                BM_Meetings_Agenda_Responses _objcircular = new BM_Meetings_Agenda_Responses();
                CircularAgendaData obj = new CircularAgendaData();
                List<CircularAgendaData> getdata = new List<CircularAgendaData>();

                obj.CircularAgendaResultVM = new CircularAgendaResult();

                getdata = (from rows in entities.BM_MeetingAgendaMapping
                           where rows.MeetingID == parentID
                           && rows.IsDeleted == false
                           && rows.IsActive == true
                           select new CircularAgendaData
                           {
                               MeetingAgendaMappingId = rows.MeetingAgendaMappingID,
                               AgendaName = rows.AgendaItemTextNew != null ? rows.AgendaItemTextNew : rows.AgendaItemText,
                               AgendaId = rows.AgendaID,
                               MeetingId = parentID
                           }).ToList();

                if (getdata != null)
                {
                    if (getdata.Count > 0)
                    {
                        foreach (var item in getdata)
                        {
                            var getCirculardata = (from row in entities.BM_Meetings_Agenda_Responses
                                                   join MP in entities.BM_MeetingParticipant on row.MeetingParticipantID equals MP.MeetingParticipantId
                                                   where row.MeetingAgendaMappingId == item.MeetingAgendaMappingId
                                                   && row.Meeting_ID == item.MeetingId
                                                   && row.AgendaId == item.AgendaId
                                                   select row).FirstOrDefault();

                            item.CircularAgendaResultVM = new CircularAgendaResult();

                            var getresultlist = (from row in entities.BM_SP_CircularAgendaCountResponse(item.MeetingId, item.AgendaId, item.MeetingAgendaMappingId) select row).FirstOrDefault();

                            if (getresultlist != null)
                            {
                                int totalApproved = (int)Math.Round((double)(100 * getresultlist.Approve) / getresultlist.Participant);
                                int totaldisaproved = (int)Math.Round((double)(100 * getresultlist.Disapprove) / getresultlist.Participant);
                                int totalatmeet = (int)Math.Round((double)(100 * getresultlist.Atmeet) / getresultlist.Participant);
                                int totalAbstain = (int)Math.Round((double)(100 * getresultlist.Abstain) / getresultlist.Participant);

                                if (totalApproved > 50)
                                {
                                    item.CircularAgendaResultVM.Result = "A";
                                    item.CircularAgendaResultVM.Remark = "Passed";
                                }
                                else if (totalApproved == 100)
                                {
                                    item.CircularAgendaResultVM.Result = "A";
                                    item.CircularAgendaResultVM.Remark = "Unanimously";
                                }
                                else if (totalAbstain > 50)
                                {
                                    item.CircularAgendaResultVM.Result = "D";
                                    item.CircularAgendaResultVM.Remark = "Not Passed";
                                }
                                else if (totaldisaproved >= 50)
                                {
                                    item.CircularAgendaResultVM.Result = "D";
                                    item.CircularAgendaResultVM.Remark = "Not Passed";
                                }
                                else if (totalatmeet > 50)
                                {
                                    item.CircularAgendaResultVM.Result = "Atmeet";
                                    item.CircularAgendaResultVM.Remark = "Discuss in meeting";
                                }
                                else if (totalApproved == 0 && totaldisaproved == 0 && totalatmeet == 0 || totalAbstain == 0 && totaldisaproved == 0 && totalatmeet == 0)
                                {
                                    if (getresultlist.CircularDuedate <= DateTime.Now)
                                    {
                                        item.CircularAgendaResultVM.Result = "D";
                                        item.CircularAgendaResultVM.Remark = "Not Passed";
                                    }
                                    else
                                    {
                                        //item.CircularAgendaResultVM.Result = "";
                                        item.CircularAgendaResultVM.Remark = "Awaiting Responses";
                                    }
                                }
                                else if (totalApproved == 50 && totaldisaproved == 50 && totalatmeet == 50 || totalApproved == 50 || totaldisaproved == 50)
                                {
                                    item.CircularAgendaResultVM.Result = "A";
                                    item.CircularAgendaResultVM.Remark = "Passed";
                                }
                            }

                            if (item.CircularAgendaResultVM.Result != "")
                            {
                                var checkResultforVoting = (from mappin in entities.BM_MeetingAgendaMapping
                                                            where mappin.AgendaID == item.AgendaId
                                                            && mappin.MeetingID == item.MeetingId
                                                            && mappin.MeetingAgendaMappingID == item.MeetingAgendaMappingId
                                                            select mappin).FirstOrDefault();
                                if (checkResultforVoting != null)
                                {
                                    checkResultforVoting.Result = item.CircularAgendaResultVM.Result;
                                    checkResultforVoting.ResultRemark = item.CircularAgendaResultVM.Remark;
                                    entities.SaveChanges();
                                }
                            }

                        }
                    }
                }
                return getdata;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public List<AvailabilityResponseChartData_ResultVM> GetAgendaResponseChartData(long MeetingId, long AgendaId, long MappingId, int UserId)
        {
            var lstResponse = GetCircularAgendaResponse(MeetingId, AgendaId, MappingId);

            var result = (from row in entities.BM_SP_MeetingAgendaResponseChartforcircular(MeetingId, AgendaId, MappingId)
                          select new AvailabilityResponseChartData_ResultVM
                          {
                              Category = row.Category,
                              Response_Count = row.Response_Count,
                              Color = row.Color,
                              MeetingId = MeetingId,
                              lstParticiapants = lstResponse.Where(k => k.Category == row.Category).ToList()
                          });
            return result.ToList();
        }
        private List<AvailabilityResponseByAvailabilityId_ResultVM> GetCircularAgendaResponse(long meetingId, long agendaId, long MappingId)
        {
            var result = (from row in entities.BM_SP_MeetingAgendaResponsesCircular(meetingId, agendaId, MappingId)
                          select new AvailabilityResponseByAvailabilityId_ResultVM
                          {
                              ParticipantName = row.ParticipantName,
                              Email = row.EmailId_Official,
                              Responses = row.Response,
                              Category = row.Category
                          });
            return result.ToList();
        }
        public bool AgendaFileUpload(VM_AgendaDocument _vmagendaDocument, int uploadedBy, int customerId)
        {
            try
            {
                bool savechange = FileUpload.SaveAgendaFile(_vmagendaDocument, uploadedBy, customerId);
                MeetingAgendaSetGenerationFlag(_vmagendaDocument.AgendaDocumentMeetingId);
                //MeetingAgendaSetGenerationFlag(_vmagendaDocument.MeetingAggendaMappingId);
                return savechange;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public bool agendaDocumentDownload(long id)
        {
            try
            {
                var checkData = (from row in entities.BM_FileData where row.Id == id select row).ToList();
                if (checkData.Count > 0)
                {
                    bool getdownloadData = FileUpload.downloadAgendaDocument(checkData);
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public List<VM_AgendaDocument> GetAgendaDocument(long agendaMappingID, int customerId)
        {
            try
            {
                var getAgendaDocument = (from row in entities.BM_FileData
                                         where row.AgendaMappingId == agendaMappingID && row.IsDeleted == false
                                         orderby row.FileSrNo
                                         select new VM_AgendaDocument
                                         {
                                             ID = row.Id,
                                             FileName = row.FileName
                                         }
                                       ).ToList();
                return getAgendaDocument;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Meeting Adjourned
        public Meeting_AdjournVM Adjourned(long meetingId, int customerId)
        {
            var result = new Meeting_AdjournVM() { MeetingAdjourn_ID = meetingId, CanAdjourn = false, CanCancel = false };
            try
            {
                var obj = (from row in entities.BM_Meetings.AsNoTracking()
                           where row.MeetingID == meetingId && row.Customer_Id == customerId
                           select row).FirstOrDefault();

                if (obj != null)
                {
                    if (obj.IsCompleted == true)
                    {
                        result.Error = true;
                        result.Message = "Meeting is already concluded.";
                        return result;
                    }

                    if (obj.IsAdjourned == true)
                    {
                        result.Error = true;
                        result.Message = "Meeting shall stand cancelled.";
                        result.CanCancel = true;
                        return result;
                    }

                    #region Adjourn meeting
                    if (obj.MeetingDate != null)
                    {
                        var mDate = obj.MeetingDate.Value.AddDays(7);

                        if ((mDate.Day == 26 && mDate.Month == 1) || (mDate.Day == 15 && mDate.Month == 8) || (mDate.Day == 2 && mDate.Month == 10))
                        {
                            mDate = mDate.AddDays(1);
                        }

                        var lstAdjournDetails = new List<string>();
                        lstAdjournDetails.Add(mDate.ToString("MMMM dd, yyyy dddd"));
                        lstAdjournDetails.Add(obj.MeetingTime);
                        lstAdjournDetails.Add(obj.MeetingVenue);

                        result.MeetingDate_Adjourn = mDate;
                        result.MeetingTime_Adjourn = obj.MeetingTime;
                        result.MeetingVenue_Adjourn = obj.MeetingVenue;

                        result.MinutesDetails = GetMinutesForDefaultAgendaItem(SecretarialConst.DefaultAgendaUsedFor.ADJOURNED, lstAdjournDetails);
                        result.CanAdjourn = true;
                    }
                    else
                    {
                        result.CanAdjourn = false;
                        result.Error = true;
                        result.Message = "Something wents wrong.";
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public Message Adjourned(long meetingId, string adjournedTime, DateTime adjournedToDate, string adjournedToTime, int adjournedBy, int customerId)
        {
            var result = new Message();
            try
            {
                var obj = (from row in entities.BM_Meetings
                           where row.MeetingID == meetingId && row.Customer_Id == customerId
                           select row).FirstOrDefault();

                if (obj != null)
                {
                    if (obj.IsCompleted == true)
                    {
                        result.Error = true;
                        result.Message = "Meeting is already completed.";
                        return result;
                    }

                    if (obj.IsAdjourned == true)
                    {
                        result.Error = true;
                        result.Message = "Meeting shall stand cancelled.";
                        SetMinutesForDefaultAgendaItem(meetingId, SecretarialConst.DefaultAgendaUsedFor.CANCELLED, new List<string>(), adjournedBy);
                        return result;
                    }

                    if (obj.MeetingDate >= adjournedToDate)
                    {
                        result.Error = true;
                        result.Message = "Please check Adjourned to date.";
                        return result;
                    }

                    if ((adjournedToDate.Day == 26 && adjournedToDate.Month == 1) || (adjournedToDate.Day == 15 && adjournedToDate.Month == 8) || (adjournedToDate.Day == 2 && adjournedToDate.Month == 10))
                    {
                        result.Error = true;
                        result.Message = "A Board Meeting (Adjourned) can not be called on a National Holiday.";
                        return result;
                    }

                    #region Adjourn meeting
                    var _obj = new Meeting_NewVM()
                    {
                        MeetingSrNo = obj.MeetingSrNo,
                        MeetingTypeId = obj.MeetingTypeId,
                        Type = obj.MeetingCircular,
                        Quarter_ = obj.Quarter_,
                        IsShorter = obj.IsShorter,

                        IsAdjourned = true,

                        Entityt_Id = (int)obj.EntityId,
                        CustomerId = (int)obj.Customer_Id,

                        UserId = adjournedBy,
                        FYID = obj.FY,

                        //MeetingDate = obj.MeetingDate,
                        //MeetingTime = obj.MeetingTime,
                        MeetingDate = adjournedToDate,
                        MeetingTime = adjournedToTime,
                        MeetingAddressType = obj.MeetingVenueType,
                        MeetingVenue = obj.MeetingVenue,
                        IsVirtualMeeting = false,
                    };

                    if (_obj.MeetingDate != null)
                    {
                        //var mDate = _obj.MeetingDate.Value.AddDays(7);

                        //if ((mDate.Day == 26 && mDate.Month == 1) || (mDate.Day == 15 && mDate.Month == 8) || (mDate.Day == 2 && mDate.Month == 10))
                        //{
                        //    mDate = mDate.AddDays(1);
                        //}
                        //_obj.MeetingDate = mDate;

                        var lstAdjournDetails = new List<string>();
                        //lstAdjournDetails.Add(mDate.ToString("MMMM dd, yyyy dddd"));
                        lstAdjournDetails.Add(adjournedToDate.ToString("MMMM dd, yyyy dddd"));
                        lstAdjournDetails.Add(_obj.MeetingTime);
                        lstAdjournDetails.Add(_obj.MeetingVenue);

                        SetMinutesForDefaultAgendaItem(meetingId, SecretarialConst.DefaultAgendaUsedFor.ADJOURNED, lstAdjournDetails, adjournedBy);
                    }

                    _obj = Create(_obj);

                    if (_obj.MeetingID > 0)
                    {
                        entities.BM_SP_MeetingAdjourn(meetingId, _obj.MeetingID, adjournedBy, customerId, adjournedTime);
                        entities.SaveChanges();

                        //Move Any Other business Agenda Items to Part B
                        MoveAnyOtherAgendaItems(_obj.MeetingID);

                        result.Success = true;
                        result.Message = "Save Successfully.";
                    }
                    else
                    {
                        result.Error = true;
                        result.Message = _obj.Message;
                    }
                    #endregion

                    //Conclude meeting (Compliance activation, Open Agenda items)
                    ConculdeMeeting(meetingId, adjournedBy, customerId);
                    result.Success = true;
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Conclude meeting (Compliance activation, Open Agenda items)
        public void ConculdeMeeting(long meetingId, int userId, int customerId)
        {
            try
            {
                #region Compliance Activation
                objIComplianceTransaction_Service.CreateTransactionOnMeetingEnd(meetingId, userId);
                #endregion

                #region Differed Agenda Items

                var lstDefferedAgenda = (from row in entities.BM_MeetingAgendaMapping
                                         where row.MeetingID == meetingId && row.Result == SecretarialConst.AgendaResult.DIFFERED &&
                                               row.IsDeleted == false && row.IsActive == true
                                         select new
                                         {
                                             MappingID = row.MeetingAgendaMappingID,
                                             BM_AgendaMasterId = (long)row.AgendaID,
                                             Meeting_Id = row.MeetingID
                                         }).ToList();

                if (lstDefferedAgenda != null)
                {
                    foreach (var item in lstDefferedAgenda)
                    {
                        #region Set Agenda Items closed flag
                        var IsParent = (from row in entities.BM_MeetingAgendaMapping
                                        where row.PostMappingId == item.MappingID && row.PostMeetingID == item.Meeting_Id && row.PostAgendaID == item.BM_AgendaMasterId && row.PartId > 0 && row.IsDeleted == false
                                        select row.MeetingAgendaMappingID
                                       ).Any();

                        //Check Agenda has Pre-Committee or Post-Board
                        var agenda = (from row in entities.BM_AgendaMaster
                                      where row.BM_AgendaMasterId == item.BM_AgendaMasterId && row.IsDeleted == false
                                      select new
                                      {
                                          row.Parent_Id,
                                          row.IsMultiStageAgenda
                                      }).FirstOrDefault();

                        long? Parent_Id = null;
                        var isClosed = true;

                        if (agenda != null)
                        {
                            Parent_Id = agenda.Parent_Id;
                        }

                        var preCommitteeColsedFlagChanged = false;
                        //Other than Pre-Committee agenda set DIFFERED
                        if (Parent_Id == null)
                        {
                            isClosed = false;
                            preCommitteeColsedFlagChanged = true;

                            var _obj = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == item.MappingID).FirstOrDefault();
                            if (_obj != null)
                            {
                                _obj.IsClosed = isClosed;
                                entities.SaveChanges();
                            }
                        }
                        //End
                        #endregion

                        #region Change Pre-Committe Closed flag
                        //if current agenda belongs to Board meeting and DIFFERED flag changed
                        //1) Committee voting other than Differed & board vote as Differed then show only in Board
                        if (IsParent && preCommitteeColsedFlagChanged)
                        {
                            entities.BM_MeetingAgendaMapping.Where(k => k.PostMappingId == item.MappingID && k.PostMeetingID == item.Meeting_Id && k.PostAgendaID == item.BM_AgendaMasterId && k.Result == SecretarialConst.AgendaResult.DIFFERED && k.PartId > 0 && k.IsDeleted == false).ToList().ForEach(k => k.IsClosed = isClosed);
                            entities.SaveChanges();
                        }
                        #endregion
                    }
                }
                #endregion

                #region Multi stage Agenda Items
                var lstAgenda = (from row in entities.BM_MeetingAgendaMapping
                                 where row.MeetingID == meetingId && row.Result == SecretarialConst.AgendaResult.APPROVED &&
                                       row.SequenceNo >= 0 && row.IsDeleted == false && row.IsActive == true &&
                                       row.IsStageClosed == false
                                 select new AgendaItemSelect
                                 {
                                     StartMeetingId = row.StartMeetingId,
                                     StartAgendaId = row.StartAgendaId,
                                     StartMappingId = row.StartMappingId,
                                     SequenceNo = row.SequenceNo,

                                     RefPendingMappingID = row.MeetingAgendaMappingID,
                                     RefMasterID = row.RefMasterID
                                 }).ToList();

                if (lstAgenda != null)
                {
                    foreach (var item in lstAgenda)
                    {
                        var agendaForNextStage = (from row in entities.BM_AgendaMaster_SubAgendaMappging
                                                  join subAgenda in entities.BM_AgendaMaster on row.SubAgendaMasterId equals subAgenda.BM_AgendaMasterId
                                                  where row.AgendaMasterId == item.StartAgendaId && row.SequenceNo > item.SequenceNo
                                                  && row.IsDeleted == false
                                                  orderby row.SequenceNo ascending
                                                  select new
                                                  {
                                                      subAgenda.PartId,
                                                      row.SubAgendaMasterId,
                                                      row.SequenceNo,
                                                  }).FirstOrDefault();

                        if (agendaForNextStage != null)
                        {
                            item.BM_AgendaMasterId = agendaForNextStage.SubAgendaMasterId;
                            item.SequenceNo = agendaForNextStage.SequenceNo;
                            item.PartID = agendaForNextStage.PartId;

                            #region Insert Next Stage Agenda

                            var Agenda = entities.BM_SP_AgendaFormatesByCustomerId(customerId, item.BM_AgendaMasterId).FirstOrDefault();
                            //End
                            BM_MeetingAgendaMapping _obj = new BM_MeetingAgendaMapping()
                            {
                                MeetingID = meetingId,
                                AgendaID = item.BM_AgendaMasterId,
                                SrNo = 0,
                                IsClosed = false,
                                IsActive = false,
                                IsDeleted = false,
                                IsNewStage = true,

                                StartMeetingId = item.StartMeetingId,
                                StartAgendaId = item.StartAgendaId,
                                StartMappingId = item.StartMappingId,
                                SequenceNo = agendaForNextStage.SequenceNo,

                                PartId = agendaForNextStage.PartId,
                                CreatedBy = userId,
                                CreatedOn = DateTime.Now,
                                //Added on 28 Apr 2020
                                RefMasterID = item.RefMasterID
                            };

                            if (Agenda != null)
                            {
                                _obj.AgendaItemText = Agenda.AgendaItemText;
                                _obj.AgendaItemTextNew = Agenda.AgendaItemText;
                                _obj.AgendaFormat = Agenda.AgendaFormat;

                                _obj.ResolutionFormatHeading = Agenda.ResolutionFormatHeading;
                                _obj.ResolutionFormat = Agenda.ResolutionFormat;

                                _obj.MinutesFormatHeading = Agenda.MinutesFormatHeading;
                                _obj.MinutesFormat = Agenda.MinutesFormat;
                                _obj.MinutesDisApproveFormat = Agenda.MinutesDisApproveFormat;
                                _obj.MinutesDifferFormat = Agenda.MinutesDifferFormat;

                                _obj.SEBI_IntimationFormat = Agenda.SEBI_IntimationFormat;
                                _obj.SEBI_DisclosureFormat = Agenda.SEBI_DisclosureFormat;
                            }

                            entities.BM_MeetingAgendaMapping.Add(_obj);
                            entities.SaveChanges();
                            #endregion

                            #region Close Multistage
                            var stage = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == item.RefPendingMappingID).FirstOrDefault();
                            stage.IsStageClosed = true;
                            entities.SaveChanges();
                            #endregion

                            #region Copy Template fields from reference agenda mapping id
                            if (_obj.MeetingAgendaMappingID > 0)
                            {
                                var lstTargetTemplateFields = new List<BM_MeetingAgendaTemplateList>();

                                var lstTemplateFields = (from mapping in entities.BM_MeetingAgendaMapping
                                                         join templateMaster in entities.BM_AgendaMasterTemplate on mapping.AgendaID equals templateMaster.AgendaID
                                                         where mapping.MeetingAgendaMappingID == _obj.MeetingAgendaMappingID && templateMaster.IsDeleted == false
                                                         select new
                                                         {
                                                             templateMaster.TemplateID,
                                                             templateMaster.TemplateName
                                                         }).ToList();

                                var lstSourceTemplateFields = (from mapping in entities.BM_MeetingAgendaTemplateList
                                                               join templateMaster in entities.BM_AgendaMasterTemplate on mapping.TemplateID equals templateMaster.TemplateID
                                                               where mapping.MeetingAgendaMappingID == item.RefPendingMappingID && templateMaster.IsDeleted == false
                                                               select new
                                                               {
                                                                   templateMaster.TemplateID,
                                                                   templateMaster.TemplateName,
                                                                   mapping.TemplateListValue
                                                               }).ToList();

                                if (lstTemplateFields != null && lstSourceTemplateFields != null)
                                {
                                    foreach (var templateField in lstTemplateFields)
                                    {
                                        var value = (from row in lstSourceTemplateFields
                                                     where row.TemplateName == templateField.TemplateName
                                                     select row.TemplateListValue).FirstOrDefault();

                                        if (!string.IsNullOrEmpty(value))
                                        {
                                            lstTargetTemplateFields.Add(new BM_MeetingAgendaTemplateList()
                                            {
                                                TemplateListValue = value,
                                                TemplateID = templateField.TemplateID,
                                                MeetingAgendaMappingID = _obj.MeetingAgendaMappingID,
                                                IsDeleted = false,
                                                CreatedBy = userId,
                                                CreatedOn = DateTime.Now
                                            });
                                        }
                                    }
                                }

                                if (lstTargetTemplateFields.Count > 0)
                                {
                                    entities.BM_MeetingAgendaTemplateList.AddRange(lstTargetTemplateFields);
                                    entities.SaveChanges();

                                    UpdateAgendaItemFormatedText(_obj.MeetingAgendaMappingID);
                                }
                            }
                            #endregion
                        }
                    }
                }
                #endregion

                #region Calculate scheduleon date
                objIMeetingCompliance.CalculateScheduleDate(meetingId, userId);
                #endregion

                #region Master Activate / Deactivate
                var lstMaster = (from row in entities.BM_MeetingAgendaMapping
                                 join meeting in entities.BM_Meetings on row.MeetingID equals meeting.MeetingID
                                 join agenda in entities.BM_AgendaMaster on row.AgendaID equals agenda.BM_AgendaMasterId
                                 join uiForm in entities.BM_UIForm on agenda.UIFormID equals uiForm.UIFormID
                                 where row.MeetingID == meetingId && row.RefMasterID != null &&
                                 (row.Result == SecretarialConst.AgendaResult.APPROVED || row.Result == SecretarialConst.AgendaResult.NOTED) &&
                                 row.IsDeleted == false && row.IsActive == true
                                 select new
                                 {
                                     uiForm.UIFormID,
                                     row.RefMasterID,
                                     meeting.MeetingDate,
                                     meeting.EntityId
                                 }).ToList();

                if (lstMaster != null)
                {
                    foreach (var item in lstMaster)
                    {
                        switch (item.UIFormID)
                        {
                            case SecretarialConst.UIForms.APPOINTMENT_OF_MD:
                            case SecretarialConst.UIForms.APPOINTMENT_OF_WTD:
                            case SecretarialConst.UIForms.APPOINTMENT_OF_ADDD:
                            case SecretarialConst.UIForms.APPOINTMENT_OF_ADDD_INDEPENDANT:
                            case SecretarialConst.UIForms.APPOINTMENT_OF_CAVD:
                            case SecretarialConst.UIForms.APPOINTMENT_OF_NOMD:
                            case SecretarialConst.UIForms.APPOINTMENT_OF_ALTD:

                            case SecretarialConst.UIForms.APPOINTMENT_OF_CFO:
                            case SecretarialConst.UIForms.APPOINTMENT_OF_CS:
                            case SecretarialConst.UIForms.APPOINTMENT_OF_CEO:
                            case SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER:

                                var details = entities.BM_Directors_DetailsOfInterest.Where(k => k.Id == item.RefMasterID).Select(k => k).FirstOrDefault();
                                if (details != null)
                                {
                                    details.DateOfResolution = item.MeetingDate;
                                    details.IsActive = true;

                                    details.UpdatedBy = userId;
                                    details.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();

                                    if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ALTD)
                                    {
                                        if (details.OriginalDirector_Id > 0)
                                        {
                                            var _objOriginalDirector = (from row in entities.BM_Directors_DetailsOfInterest
                                                                        where row.EntityId == details.EntityId && row.Director_Id == details.OriginalDirector_Id &&
                                                                        row.IsDeleted == false && row.IsActive == true
                                                                        select row).FirstOrDefault();

                                            _objOriginalDirector.OriginalDirector_Id = -1;
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                                break;
                            case SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR:
                            case SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE:
                            case SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE_INDEPENDANT:
                            case SecretarialConst.UIForms.RESIGNATION_OF_MD:
                            case SecretarialConst.UIForms.RESIGNATION_OF_WTD:
                            case SecretarialConst.UIForms.RESIGNATION_OF_ADDD:
                            case SecretarialConst.UIForms.RESIGNATION_OF_ADDD_INDEPENDANT:
                            case SecretarialConst.UIForms.RESIGNATION_OF_CAVD:
                            case SecretarialConst.UIForms.RESIGNATION_OF_NOMD:
                            case SecretarialConst.UIForms.RESIGNATION_OF_ALTD:

                            case SecretarialConst.UIForms.RESIGNATION_OF_CFO:
                            case SecretarialConst.UIForms.RESIGNATION_OF_CS:
                            case SecretarialConst.UIForms.RESIGNATION_OF_CEO:
                            case SecretarialConst.UIForms.RESIGNATION_OF_MANAGER:
                                var resignation = entities.BM_Directors_DetailsOfInterest.Where(k => k.Id == item.RefMasterID).Select(k => k).FirstOrDefault();
                                if (resignation != null)
                                {
                                    resignation.IsActive = false;
                                    resignation.IsDeleted = true;

                                    resignation.UpdatedBy = userId;
                                    resignation.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }
                                break;
                            case SecretarialConst.UIForms.APPOINTMENT_OF_INTERNAL_AUDITOR:
                            case SecretarialConst.UIForms.APPOINTMENT_OF_SECRETARIAL_AUDITOR:
                                var auditor = entities.BM_StatutoryAuditor.Where(k => k.Id == item.RefMasterID).Select(k => k).FirstOrDefault();
                                if (auditor != null)
                                {
                                    auditor.BoardMeeting_date = item.MeetingDate;
                                    auditor.IsActive = true;

                                    auditor.Updatedby = userId;
                                    auditor.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }
                                break;
                            case SecretarialConst.UIForms.APPOINTMENT_OF_FIRST_STATUTORY_AUDITOR:
                                var auditor_stat_board = entities.BM_StatutoryAuditor.Where(k => k.Id == item.RefMasterID).Select(k => k).FirstOrDefault();
                                if (auditor_stat_board != null)
                                {
                                    auditor_stat_board.BoardMeeting_date = item.MeetingDate;
                                    auditor_stat_board.IsActive = true;

                                    auditor_stat_board.Updatedby = userId;
                                    auditor_stat_board.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }
                                break;
                            case SecretarialConst.UIForms.APPOINTMENT_OR_REAPPOINTMENT_OF_STATUTORY_AUDITOR_IN_AGM:
                                var auditor_stat = entities.BM_StatutoryAuditor.Where(k => k.Id == item.RefMasterID).Select(k => k).FirstOrDefault();
                                if (auditor_stat != null)
                                {
                                    //auditor_stat.BoardMeeting_date = item.MeetingDate;
                                    auditor_stat.IsActive = true;

                                    auditor_stat.Updatedby = userId;
                                    auditor_stat.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }
                                break;
                            case SecretarialConst.UIForms.APPOINTMENT_OF_COST_AUDITOR:
                                var auditor_cost = entities.BM_StatutoryAuditor.Where(k => k.Id == item.RefMasterID).Select(k => k).FirstOrDefault();
                                if (auditor_cost != null)
                                {
                                    auditor_cost.BoardMeeting_date = item.MeetingDate;
                                    //auditor_cost.IsActive = true;

                                    auditor_cost.Updatedby = userId;
                                    auditor_cost.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }
                                break;
                            case SecretarialConst.UIForms.APPOINTMENT_OR_REAPPOINTMENT_OF_COST_AUDITOR_IN_AGM:
                                var auditor_cost_in_AGM = entities.BM_StatutoryAuditor.Where(k => k.Id == item.RefMasterID).Select(k => k).FirstOrDefault();
                                if (auditor_cost_in_AGM != null)
                                {
                                    //auditor_cost_in_AGM.BoardMeeting_date = item.MeetingDate;
                                    auditor_cost_in_AGM.IsActive = true;

                                    auditor_cost_in_AGM.Updatedby = userId;
                                    auditor_cost_in_AGM.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }
                                break;
                            case SecretarialConst.UIForms.AUDITOR_APPOINTED_IN_CASE_OF_CASUAL_VACANCY_RATIFICATION_IN_EGM:
                                var auditor_stat_in_GM = entities.BM_StatutoryAuditor_Mapping.Where(k => k.Id == item.RefMasterID).Select(k => k).FirstOrDefault();
                                if (auditor_stat_in_GM != null)
                                {
                                    auditor_stat_in_GM.DateOfBoardMeeting = item.MeetingDate;
                                    auditor_stat_in_GM.IsActive = true;

                                    //auditor_stat_in_GM.Updatedby = userId;
                                    //auditor_stat_in_GM.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }
                                break;
                            case SecretarialConst.UIForms.RESIGNATION_OF_INTERNAL_AUDITOR:
                            case SecretarialConst.UIForms.RESIGNATION_OF_SECRETARIAL_AUDITOR:
                            case SecretarialConst.UIForms.RESIGNATION_OF_COST_AUDITOR:
                            case SecretarialConst.UIForms.RESIGNATION_OF_STATUTORY_AUDITOR:
                                var auditorResignation = entities.BM_StatutoryAuditor_Mapping.Where(k => k.Id == item.RefMasterID).Select(k => k).FirstOrDefault();
                                if (auditorResignation != null)
                                {
                                    auditorResignation.IsDeleted = true;
                                    entities.SaveChanges();
                                }
                                break;
                            case SecretarialConst.UIForms.KEEPING_OF_BOOKS_OF_ACCOUNTS:
                                var booksDetails = entities.BM_EntityMasterBooksOfAccounts.Where(k => k.Id == item.RefMasterID).Select(k => k).FirstOrDefault();
                                if (booksDetails != null)
                                {
                                    booksDetails.IsActive = true;
                                    booksDetails.DateOfBoardMeeting = item.MeetingDate;
                                    booksDetails.UpdatedBy = userId;
                                    booksDetails.UpdatedOn = DateTime.Now;
                                    entities.SaveChanges();
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
                #endregion

                #region Create AGM
                var meetings = (from row in entities.BM_Meetings
                                where row.MeetingID == meetingId
                                select new
                                {
                                    row.MeetingTypeId,
                                    row.EntityId,
                                    row.MeetingCircular,
                                    row.Entity_Type
                                }).FirstOrDefault();
                if (meetings != null)
                {
                    #region Create AGM //Added on 5 Oct 2020
                    if (meetings.MeetingTypeId == SecretarialConst.MeetingTypeID.BOARD)
                    {
                        CreateAGMFromBoard(meetingId, meetings.EntityId, customerId, userId);
                    }
                    #endregion

                    #region Take note on Circular Open Agenda //Added on 12 Oct 2020
                    if (meetings.MeetingCircular == SecretarialConst.MeetingCircular.CIRCULAR)
                    {
                        var agendaId = (from agenda in entities.BM_AgendaMaster
                                        where agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.TAKE_NOTE_ON_CIRCULAR &&
                                        agenda.EntityType == meetings.Entity_Type && agenda.MeetingTypeId == meetings.MeetingTypeId &&
                                        agenda.IsDeleted == false
                                        select agenda.BM_AgendaMasterId
                                   ).FirstOrDefault();
                        if (agendaId > 0)
                        {
                            var openAgenda = (from row in entities.BM_MeetingOpenAgendaOnCompliance
                                              where row.RefMasterID == meetingId && row.RefMaster == SecretarialConst.OpenAgendaRefMaster.MEETING && row.IsDeleted == false
                                              select row).FirstOrDefault();
                            if (openAgenda == null)
                            {
                                openAgenda = new BM_MeetingOpenAgendaOnCompliance();
                                openAgenda.EntityId = meetings.EntityId;
                                openAgenda.RefMasterName = "Resolutions passed";
                                openAgenda.RefMasterID = meetingId;
                                openAgenda.ScheduleOnID = 0;
                                openAgenda.AgendaId = agendaId;
                                openAgenda.MeetingTypeId = meetings.MeetingTypeId;
                                openAgenda.IsNoted = false;
                                openAgenda.IsDeleted = false;
                                openAgenda.CreatedBy = userId;
                                openAgenda.CreatedOn = DateTime.Now;
                                openAgenda.RefMaster = SecretarialConst.OpenAgendaRefMaster.MEETING;

                                entities.BM_MeetingOpenAgendaOnCompliance.Add(openAgenda);
                                entities.SaveChanges();
                            }
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void ConculdeAGM(long meetingId, int userId, int customerId)
        {
            try
            {
                #region Meeting level Compliance Activation
                objIComplianceTransaction_Service.CreateTransactionOnMeetingEnd(meetingId, userId);
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Create AGM from Board
        public void CreateAGMFromBoard(long meetingId, int entityId, int customerId, int createdBy)
        {
            try
            {
                var meeting = (from row in entities.BM_Meetings
                               join mapping in entities.BM_MeetingAgendaMapping on row.MeetingID equals mapping.MeetingID
                               join agenda in entities.BM_AgendaMaster on mapping.AgendaID equals agenda.BM_AgendaMasterId
                               where row.MeetingID == meetingId && (row.IsCompleted == true || row.IsVirtualMeeting == true) &&
                               mapping.Result == SecretarialConst.AgendaResult.APPROVED &&
                               (agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.NOTICE_FOR_CALLING_OF_AGM ||
                               agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.NOTICE_FOR_CALLING_OF_EGM) &&
                               mapping.IsDeleted == false
                               select new
                               {
                                   row.EntityId,
                                   row.Entity_Type,
                                   row.FY,
                                   mapping.MeetingAgendaMappingID,
                                   mapping.RefMasterID,
                                   agenda.UIFormID
                               }).FirstOrDefault();
                if (meeting != null)
                {
                    if (meeting.MeetingAgendaMappingID > 0)
                    {
                        if (meeting.RefMasterID != null)
                        {
                            var isGMExists = (from row in entities.BM_Meetings
                                              where row.MeetingID == meeting.RefMasterID && row.IsDeleted == false
                                              select row.MeetingID).Any();

                            if (isGMExists)
                            {
                                return;
                            }
                        }
                        var lstTemplateFields = (from row in entities.BM_MeetingAgendaTemplateList
                                                 join category in entities.BM_AgendaMasterTemplate on row.TemplateID equals category.TemplateID
                                                 where row.MeetingAgendaMappingID == meeting.MeetingAgendaMappingID
                                                 select new
                                                 {
                                                     category.CategoryID,
                                                     category.TemplateID,
                                                     category.TemplateName,
                                                     row.TemplateListValue
                                                 }
                                     ).ToList();

                        #region Create GM
                        var meetingTypeId = SecretarialConst.MeetingTypeID.EGM;
                        string tSrNo = "NO_OF_THE_EGM", tDate = "DATE_OF_THE_EGM", tTime = "START_TIME_OF_THE_EGM", tVenue = "VENUE_OF_THE_EGM";
                        if (meeting.UIFormID == SecretarialConst.UIForms.CALLING_OF_AGM)
                        {
                            meetingTypeId = SecretarialConst.MeetingTypeID.AGM;
                            tSrNo = "NO_OF_THE_AGM"; tDate = "DATE_OF_THE_AGM"; tTime = "START_TIME_OF_THE_AGM"; tVenue = "VENUE_OF_THE_AGM";
                        }

                        var _obj = new Meeting_NewVM()
                        {
                            MeetingSrNo = null,
                            MeetingTypeId = meetingTypeId,
                            Type = SecretarialConst.MeetingCircular.MEETING,
                            Quarter_ = "",
                            IsShorter = false,

                            IsAdjourned = false,

                            Entityt_Id = entityId,
                            CustomerId = customerId,

                            UserId = createdBy,
                            FYID = meeting.FY,
                            MeetingAddressType = "O",
                            IsVirtualMeeting = false
                        };

                        if (lstTemplateFields != null)
                        {
                            #region Serial No
                            var srNo = lstTemplateFields.Where(k => k.TemplateName.ToUpper().Contains(tSrNo)).Select(k => k.TemplateListValue).FirstOrDefault();
                            if (!string.IsNullOrEmpty(srNo))
                            {
                                _obj.MeetingSrNo = Convert.ToInt32(srNo);
                            }
                            #endregion

                            #region Financial year
                            var y = lstTemplateFields.Where(k => k.TemplateName.ToUpper().Contains("FYID")).Select(k => k.TemplateListValue).FirstOrDefault();
                            if (!string.IsNullOrEmpty(y))
                            {
                                long fyId = meeting.FY;
                                if (long.TryParse(y, out fyId) == true)
                                {
                                    _obj.FYID = fyId;
                                }
                            }
                            #endregion

                            #region Date
                            var d = lstTemplateFields.Where(k => k.TemplateName.ToUpper().Contains(tDate)).Select(k => k.TemplateListValue).FirstOrDefault();
                            if (!string.IsNullOrEmpty(d))
                            {
                                DateTime date = DateTime.Now;
                                if (DateTime.TryParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                                {
                                    _obj.MeetingDate = date;
                                }
                            }
                            #endregion

                            #region Time
                            var t = lstTemplateFields.Where(k => k.TemplateName.ToUpper().Contains(tTime)).Select(k => k.TemplateListValue).FirstOrDefault();
                            if (!string.IsNullOrEmpty(t))
                            {
                                _obj.MeetingTime = t;
                            }
                            #endregion

                            #region Venue
                            var v = lstTemplateFields.Where(k => k.TemplateName.ToUpper().Contains(tVenue)).Select(k => k.TemplateListValue).FirstOrDefault();
                            if (!string.IsNullOrEmpty(v))
                            {
                                _obj.MeetingVenue = v;
                            }
                            #endregion

                            #region e-Voting
                            var eVoting = lstTemplateFields.Where(k => k.TemplateName.ToUpper().Contains("IS_E_VOTING")).Select(k => k.TemplateListValue).FirstOrDefault();
                            if (!string.IsNullOrEmpty(v))
                            {
                                if (eVoting == "Yes" || _obj.MeetingTypeId == SecretarialConst.EntityTypeID.LISTED)
                                {
                                    _obj.IsEVoting = true;
                                }
                                else
                                {
                                    _obj.IsEVoting = false;
                                }
                            }
                            else
                            {
                                _obj.IsEVoting = false;
                            }
                            #endregion
                        }

                        _obj = Create(_obj);
                        if (_obj.MeetingID > 0)
                        {
                            var mapping = (from row in entities.BM_MeetingAgendaMapping
                                           where row.MeetingAgendaMappingID == meeting.MeetingAgendaMappingID
                                           select row).FirstOrDefault();
                            if (mapping != null)
                            {
                                mapping.RefMasterID = _obj.MeetingID;
                                entities.SaveChanges();
                            }
                            objIMeetingCompliance.CalculateScheduleDate(_obj.MeetingID, createdBy);
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Minutes of Meeting
        public PreviewAgendaVM PreviewDraftMinutes(long MeetingId, int CustomerId, long DraftCirculationID, int UserID)
        {
            try
            {
                var minutesDetails = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                      where row.ID == DraftCirculationID && row.IsDeleted == false && row.ReadOn == null
                                      select row).FirstOrDefault();
                if (minutesDetails != null)
                {
                    minutesDetails.ReadOn = DateTime.Now;
                    minutesDetails.UpdatedBy = UserID;
                    minutesDetails.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Preview(MeetingId, CustomerId, true);
        }
        public PreviewAgendaVM PreviewMOM(long MeetingId, int CustomerId)
        {
            return Preview(MeetingId, CustomerId, true);
        }
        public MeetingAgendaMappingMOMVM PreviewMOM(long MappingId)
        {
            try
            {
                return (from row in entities.BM_SP_GetAgendaTemplate(MappingId, null)
                        select new MeetingAgendaMappingMOMVM
                        {
                            MeetingAgendaMappingID = (long)row.MeetingAgendaMappingID,
                            Meeting_Id = row.MeetingID,
                            ResolutionFormatHeading = row.ResolutionFormatHeading,
                            ResolutionFormat = row.ResolutionFormat,
                            MinutesFormatHeading = row.MinutesFormatHeading,
                            MinutesFormat = row.MinutesFormat,
                            //Result = row.Result,
                            //ResultRemark = row.ResultRemark
                        }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public MeetingAgendaMappingMOMVM GetMOM(long MeetingId, long MappingId, int CustomerId)
        {
            try
            {
                return (from row in entities.BM_SP_GetMeetingAgendaItemMinutes(MeetingId, MappingId, CustomerId)
                        select new MeetingAgendaMappingMOMVM
                        {
                            AgendaID = row.AgendaID,
                            MeetingAgendaMappingID = row.MeetingAgendaMappingID,
                            Meeting_Id = row.MeetingID,
                            ResolutionFormatHeading = row.ResolutionFormatHeading,
                            ResolutionFormat = row.ResolutionFormat,
                            MinutesFormatHeading = row.MinutesFormatHeading,
                            MinutesFormat = row.MinutesFormat,
                            Result = row.Result,
                            ResultRemark = row.ResultRemark
                        }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public MeetingAgendaMappingMOMVM SaveMOM(MeetingAgendaMappingMOMVM obj, int updatedBy)
        {
            try
            {
                var _obj = (from row in entities.BM_MeetingAgendaMapping
                            where row.MeetingAgendaMappingID == obj.MeetingAgendaMappingID && row.MeetingID == obj.Meeting_Id
                            select row).FirstOrDefault();
                if (_obj != null)
                {
                    _obj.MinutesFormatHeading = obj.MinutesFormatHeading;
                    _obj.UpdatedBy = updatedBy;
                    _obj.UpdatedOn = DateTime.Now;

                    if (obj.Result == SecretarialConst.AgendaResult.APPROVED || obj.Result == SecretarialConst.AgendaResult.NOTED)
                    {
                        _obj.MinutesFormat = obj.MinutesFormat;

                        _obj.ResolutionFormatHeading = obj.ResolutionFormatHeading;
                        _obj.ResolutionFormat = obj.ResolutionFormat;
                    }
                    else if (obj.Result == SecretarialConst.AgendaResult.DISAPPROVED)
                    {
                        _obj.MinutesDisApproveFormat = obj.MinutesFormat;
                    }
                    else if (obj.Result == SecretarialConst.AgendaResult.DIFFERED)
                    {
                        _obj.MinutesDifferFormat = obj.MinutesFormat;
                    }
                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    obj.Success = true;
                    obj.Message = "Saved Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Minutes Details (date of entering etc)
        public MeetingMinutesDetailsVM GetMinutesDetails(long meetingId)
        {
            try
            {
                var result = (from row in entities.BM_MeetingMinutesDetails
                              where row.MeetingId == meetingId && row.IsDeleted == false
                              select new MeetingMinutesDetailsVM
                              {
                                  MeetingMinutesDetailsId = row.Id,
                                  MOM_MeetingId = row.MeetingId,
                                  Place = row.Place,
                                  DateOfEntering = row.DateOfEntering,
                                  DateOfSigning = row.SigningOfEntering,
                                  IsCirculate = row.IsCirculate,
                                  IsFinalized = row.IsFinalized,
                                  ConfirmMeetingId = row.ConfirmMeetingId
                              }).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public MeetingMinutesDetailsVM CirculateMinutes(MeetingMinutesDetailsVM obj, int userId, out string mailFormat)
        {
            mailFormat = "";
            try
            {
                var _obj = (from row in entities.BM_MeetingMinutesDetails
                            where row.MeetingId == obj.MOM_MeetingId
                            select row).FirstOrDefault();

                #region check dates
                var meeting = (from row in entities.BM_Meetings
                               where row.MeetingID == obj.MOM_MeetingId
                               select new
                               {
                                   row.MeetingDate,
                                   row.IsShorter,
                                   row.MeetingTypeId
                               }).FirstOrDefault();
                if (meeting != null)
                {
                    var meetingDate = meeting.MeetingDate;
                    if (meetingDate != null)
                    {
                        if (obj.DateOfEntering != null)
                        {
                            if (meetingDate > obj.DateOfEntering)
                            {
                                obj.Error = true;
                                obj.Message = "Please check date of entering minutes";
                                return obj;
                            }
                            else
                            {
                                if (obj.DateOfSigning != null)
                                {
                                    if (obj.DateOfEntering > obj.DateOfSigning)
                                    {
                                        obj.Error = true;
                                        obj.Message = "Please check date of signing minutes";
                                        return obj;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (obj.DateOfSigning != null)
                            {
                                if (meetingDate > obj.DateOfSigning)
                                {
                                    obj.Error = true;
                                    obj.Message = "Please check date of signing minutes";
                                    return obj;
                                }
                            }
                        }
                    }
                }
                #endregion

                if (_obj != null)
                {
                    _obj.IsCirculate = true;
                    _obj.IsDeleted = false;
                    _obj.DateOfDraftCirculation = DateTime.Now.Date;
                    _obj.DueDateOfDraftCirculation = DateTime.Now.Date.AddDays(7);
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                    obj.IsCirculate = true;
                }
                else
                {
                    _obj = new BM_MeetingMinutesDetails();
                    _obj.MeetingId = obj.MOM_MeetingId;
                    _obj.DateOfEntering = obj.DateOfEntering;
                    _obj.Place = obj.Place;
                    _obj.SigningOfEntering = obj.DateOfSigning;
                    _obj.IsFinalized = false;
                    _obj.IsCirculate = true;
                    _obj.IsDeleted = false;
                    _obj.DateOfDraftCirculation = DateTime.Now.Date;
                    _obj.DueDateOfDraftCirculation = DateTime.Now.Date.AddDays(7);
                    _obj.CreatedBy = userId;
                    _obj.CreatedOn = DateTime.Now;

                    entities.BM_MeetingMinutesDetails.Add(_obj);
                    entities.SaveChanges();
                    obj.IsCirculate = true;
                }

                #region Save Participant Details
                if (_obj.Id > 0)
                {
                    var lstParticipants = (from row in entities.BM_MeetingParticipant
                                           where row.Meeting_ID == obj.MOM_MeetingId && row.IsDeleted == false &&
                                           (row.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR || row.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MEMBER || row.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MANAGEMENT)
                                           select new
                                           {
                                               row.MeetingParticipantId
                                           }).ToList();
                    if (lstParticipants != null)
                    {
                        foreach (var item in lstParticipants)
                        {
                            var participant = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                               where row.MinutesDetailsID == _obj.Id && row.MeetingParticipantId == item.MeetingParticipantId
                                               select row).FirstOrDefault();
                            if (participant == null)
                            {
                                participant = new BM_MeetingMinutesDetailsTransaction();
                                participant.ID = 0;
                                participant.MinutesDetailsID = _obj.Id;
                                participant.MeetingParticipantId = item.MeetingParticipantId;
                                participant.SentOn = DateTime.Now;
                                participant.IsDeleted = false;
                                participant.CreatedBy = userId;
                                participant.CreatedOn = DateTime.Now;

                                entities.BM_MeetingMinutesDetailsTransaction.Add(participant);
                                entities.SaveChanges();
                            }
                            else
                            {
                                participant.SentOn = DateTime.Now;
                                participant.IsDeleted = false;
                                participant.UpdatedBy = userId;
                                participant.CreatedOn = DateTime.Now;
                                entities.SaveChanges();
                            }
                        }
                    }

                }

                #endregion

                obj.Success = true;
                obj.Message = "Draft minutes send successfully.";

                #region Generate mail Format
                if (meeting != null)
                {
                    mailFormat = (from row in entities.BM_TemplateMaster
                                  where row.TemplateType == SecretarialConst.MeetingTemplatType.DRAFT_MINUTES && row.MeetingTypeId == meeting.MeetingTypeId &&
                                  row.IsShorter == meeting.IsShorter && row.IsDeleted == false
                                  select row.MailTemplate).FirstOrDefault();

                    mailFormat = (from row in entities.BM_SP_GenerateNoticeMail(obj.MOM_MeetingId, mailFormat, null)
                                  select row
                            ).FirstOrDefault();
                }
                #endregion
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingMinutesDetailsVM SaveMinutesDetails(MeetingMinutesDetailsVM obj, int userId)
        {
            try
            {
                var _obj = (from row in entities.BM_MeetingMinutesDetails
                            where row.MeetingId == obj.MOM_MeetingId
                            select row).FirstOrDefault();

                #region check dates
                var meetingDate = (from row in entities.BM_Meetings
                                   where row.MeetingID == obj.MOM_MeetingId
                                   select row.MeetingDate).FirstOrDefault();
                if (meetingDate != null)
                {
                    if (obj.DateOfEntering != null)
                    {
                        if (meetingDate > obj.DateOfEntering)
                        {
                            obj.Error = true;
                            obj.Message = "Please check date of entering minutes";
                            return obj;
                        }
                        else
                        {
                            if (obj.DateOfSigning != null)
                            {
                                if (obj.DateOfEntering > obj.DateOfSigning)
                                {
                                    obj.Error = true;
                                    obj.Message = "Please check date of signing minutes";
                                    return obj;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (obj.DateOfSigning != null)
                        {
                            if (meetingDate > obj.DateOfSigning)
                            {
                                obj.Error = true;
                                obj.Message = "Please check date of signing minutes";
                                return obj;
                            }
                        }
                    }
                }
                #endregion

                if (_obj == null)
                {
                    _obj = new BM_MeetingMinutesDetails();
                    _obj.MeetingId = obj.MOM_MeetingId;
                    _obj.DateOfEntering = obj.DateOfEntering;
                    _obj.Place = obj.Place;
                    _obj.SigningOfEntering = obj.DateOfSigning;
                    _obj.IsCirculate = false;
                    _obj.IsFinalized = false;
                    _obj.ConfirmMeetingId = null;
                    _obj.IsDeleted = false;
                    _obj.CreatedBy = userId;
                    _obj.CreatedOn = DateTime.Now;

                    entities.BM_MeetingMinutesDetails.Add(_obj);
                    obj.Success = true;
                    obj.Message = "Minutes details saved successfully.";
                }
                else
                {
                    _obj.MeetingId = obj.MOM_MeetingId;
                    _obj.DateOfEntering = obj.DateOfEntering;
                    _obj.Place = obj.Place;
                    _obj.SigningOfEntering = obj.DateOfSigning;
                    _obj.IsDeleted = false;
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    obj.IsCirculate = _obj.IsCirculate;

                    obj.Success = true;
                    obj.Message = "Minutes details updated successfully.";
                }

                entities.SaveChanges();
                obj.MeetingMinutesDetailsId = _obj.Id;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingMinutesDetailsVM FinalizedMinutes(MeetingMinutesDetailsVM obj, int userId)
        {
            try
            {
                var _obj = (from row in entities.BM_MeetingMinutesDetails
                            where row.MeetingId == obj.MOM_MeetingId
                            select row).FirstOrDefault();

                #region check dates
                var meetingDate = (from row in entities.BM_Meetings
                                   where row.MeetingID == obj.MOM_MeetingId
                                   select row.MeetingDate).FirstOrDefault();
                if (meetingDate != null)
                {
                    if (obj.DateOfEntering != null)
                    {
                        if (meetingDate > obj.DateOfEntering)
                        {
                            obj.Error = true;
                            obj.Message = "Please check date of entering minutes";
                            return obj;
                        }
                        else
                        {
                            if (obj.DateOfSigning != null)
                            {
                                if (obj.DateOfEntering > obj.DateOfSigning)
                                {
                                    obj.Error = true;
                                    obj.Message = "Please check date of signing minutes";
                                    return obj;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (obj.DateOfSigning != null)
                        {
                            if (meetingDate > obj.DateOfSigning)
                            {
                                obj.Error = true;
                                obj.Message = "Please check date of signing minutes";
                                return obj;
                            }
                        }
                    }
                }
                #endregion

                if (_obj != null)
                {
                    if (obj.DateOfEntering != null && obj.DateOfSigning != null && obj.Place != null)
                    {
                        _obj.IsFinalized = true;
                        obj.IsFinalized = true;

                        _obj.IsDeleted = false;
                        _obj.DateOfEntering = obj.DateOfEntering;
                        _obj.Place = obj.Place;
                        _obj.SigningOfEntering = obj.DateOfSigning;
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = "Save Successfully.";
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Please enter place & date.";
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public MeetingMinutesDetailsVM FinalizedMinutesOfVirtualMeeting(MeetingMinutesDetailsVM obj, FileDataVM objFileData, int userId)
        {
            try
            {
                #region check dates
                var meetingDate = (from row in entities.BM_Meetings
                                   where row.MeetingID == obj.MOM_MeetingId
                                   select row.MeetingDate).FirstOrDefault();
                if (meetingDate != null)
                {
                    if (obj.DateOfEntering != null)
                    {
                        if (meetingDate > obj.DateOfEntering)
                        {
                            obj.Error = true;
                            obj.Message = "Please check date of entering minutes";
                            return obj;
                        }
                        else
                        {
                            if (obj.DateOfSigning != null)
                            {
                                if (obj.DateOfEntering > obj.DateOfSigning)
                                {
                                    obj.Error = true;
                                    obj.Message = "Please check date of signing minutes";
                                    return obj;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (obj.DateOfSigning != null)
                        {
                            if (meetingDate > obj.DateOfSigning)
                            {
                                obj.Error = true;
                                obj.Message = "Please check date of signing minutes";
                                return obj;
                            }
                        }
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Please enter meeting date";
                    return obj;
                }
                #endregion

                var _obj = (from row in entities.BM_MeetingMinutesDetails
                            where row.MeetingId == obj.MOM_MeetingId
                            select row).FirstOrDefault();

                if (_obj != null)
                {
                    if (obj.DateOfEntering != null && obj.DateOfSigning != null && obj.Place != null)
                    {
                        _obj.IsFinalized = true;
                        obj.IsFinalized = true;

                        _obj.IsDeleted = false;
                        _obj.DateOfEntering = obj.DateOfEntering;
                        _obj.Place = obj.Place;
                        _obj.SigningOfEntering = obj.DateOfSigning;
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = "Save Successfully.";
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Please enter place & date.";
                    }
                }
                else
                {
                    if (obj.DateOfEntering != null && obj.DateOfSigning != null && obj.Place != null)
                    {
                        _obj = new BM_MeetingMinutesDetails();
                        _obj.MeetingId = obj.MOM_MeetingId;
                        _obj.IsCirculate = true;
                        _obj.IsFinalized = true;
                        obj.IsCirculate = true;
                        obj.IsFinalized = true;

                        _obj.IsDeleted = false;
                        _obj.DateOfEntering = obj.DateOfEntering;
                        _obj.Place = obj.Place;
                        _obj.SigningOfEntering = obj.DateOfSigning;
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.BM_MeetingMinutesDetails.Add(_obj);
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = "Minutes finalized successfully.";
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Please enter place & date.";
                    }
                }

                #region Compliance Activation
                if (obj.Success)
                {
                    #region set Meeting Conclude
                    var meeting = (from row in entities.BM_Meetings
                                   where row.MeetingID == obj.MOM_MeetingId && row.IsDeleted == false && row.IsVirtualMeeting == true
                                   select row).FirstOrDefault();
                    if (meeting != null)
                    {
                        meeting.IsCompleted = true;
                        meeting.UpdatedBy = userId;
                        meeting.UpdatedOn = DateTime.Now;

                        entities.Entry(meeting).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                    #endregion
                    objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(obj.MOM_MeetingId, "CirculateDraft", false);
                    objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(obj.MOM_MeetingId, "FinalizedMinutes", false, 0, DateTime.Now, objFileData, userId);
                }
                #endregion
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public void ComplianceClosedOnCirculateDraft(MeetingMinutesDetailsVM model, FileDataVM objFileData, int userID)
        {
            try
            {
                var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(model.MOM_MeetingId, "CirculateDraft");

                long? complianceScheduleOnID = null, transactionID = null;
                if (objFileData != null)
                {
                    int fileVersion = 0;
                    if (lstClosedTransactions != null)
                    {
                        if (lstClosedTransactions.Count > 0)
                        {
                            complianceScheduleOnID = lstClosedTransactions.FirstOrDefault().ComplianceScheduleOnID;
                            transactionID = lstClosedTransactions.FirstOrDefault().TransactionID;

                            fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);
                        }
                    }

                    fileVersion++;
                    objFileData.Version = Convert.ToString(fileVersion);
                    objFileData = objIFileData_Service.Save(objFileData, userID);

                    if (objFileData.FileID > 0 && complianceScheduleOnID > 0)
                    {
                        objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void ComplianceClosedOnMinutesFinalized(MeetingMinutesDetailsVM model, FileDataVM objFileData, int userID)
        {
            try
            {
                var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(model.MOM_MeetingId, "FinalizedMinutes");

                long? complianceScheduleOnID = null, transactionID = null;
                if (objFileData != null)
                {
                    int fileVersion = 0;
                    if (lstClosedTransactions != null)
                    {
                        if (lstClosedTransactions.Count > 0)
                        {
                            complianceScheduleOnID = lstClosedTransactions.FirstOrDefault().ComplianceScheduleOnID;
                            transactionID = lstClosedTransactions.FirstOrDefault().TransactionID;

                            fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);
                        }
                    }

                    fileVersion++;
                    objFileData.Version = Convert.ToString(fileVersion);
                    objFileData = objIFileData_Service.Save(objFileData, userID);

                    if (objFileData.FileID > 0 && complianceScheduleOnID > 0)
                    {
                        objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Meeting level compliance reopen for  Virtaul Meeting
        public bool ReOpenMeetingLevelCompliance(long meetingId, string meetingEvent)
        {
            var result = false;
            try
            {
                var isclosed = objIComplianceTransaction_Service.CheckMeetingComplianceClosed(meetingId, meetingEvent);
                if (isclosed)
                {
                    objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(meetingId, meetingEvent, false);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Minutes Comments
        public MeetingMinutesApproveVM ApproveDraftMinutes(MeetingMinutesApproveVM obj, int userId)
        {
            try
            {
                var _obj = (from row in entities.BM_MeetingMinutesDetailsTransaction
                            where row.ID == obj.DraftCirculationID && row.IsDeleted == false && row.ApprovedOn == null
                            select row).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.ApprovedOn = DateTime.Now;
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = "Approved Successfully.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public bool IsDraftMinutesApproved(long draftCirculationID)
        {
            var result = false;
            try
            {
                //result = (from row in entities.BM_MeetingMinutesDetailsTransaction
                //          where row.ID == draftCirculationID && row.ApprovedOn != null
                //          select row).Any();
                var details = (from row in entities.BM_MeetingMinutesDetailsTransaction
                               join participant in entities.BM_MeetingParticipant on row.MeetingParticipantId equals participant.MeetingParticipantId
                               where row.ID == draftCirculationID && row.IsDeleted == false && participant.IsDeleted == false
                               select new
                               {
                                   row.ApprovedOn,
                                   participant.ParticipantType
                               }).FirstOrDefault();
                if (details != null)
                {
                    if (details.ApprovedOn == null && (details.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR || details.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MEMBER))
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        #endregion

        #region Get Minutes Formates For Default Agenda Item (Quorum)
        public string GetMinutesForDefaultAgendaItem(int usedFor, List<string> lstTemplateFieldData)
        {
            try
            {
                var minutesFormat = (from row in entities.BM_AgendaMasterFormatesForDefaultAgenda
                                     where row.DefaultAgendaFor == SecretarialConst.DefaultAgenda.QUORUM && row.UsedFor == usedFor && row.IsDeleted == false
                                     select row.MinutesFormat).FirstOrDefault();

                if (!string.IsNullOrEmpty(minutesFormat))
                {
                    if (usedFor == SecretarialConst.DefaultAgendaUsedFor.LEAVE_OF_ABSENCE)
                    {
                        var strMember = "";
                        foreach (var item in lstTemplateFieldData)
                        {
                            strMember += item + "<br/>";
                        }
                        minutesFormat = minutesFormat.Replace("{{MEMBER_ABSENCE_LIST}}", strMember);
                    }
                    else if (usedFor == SecretarialConst.DefaultAgendaUsedFor.ADJOURNED)
                    {
                        if (lstTemplateFieldData.Count == 3)
                        {
                            minutesFormat = minutesFormat.Replace("{{ Meeting Date }}", lstTemplateFieldData[0]);
                            minutesFormat = minutesFormat.Replace("{{ Meeting Time }}", lstTemplateFieldData[1]);
                            minutesFormat = minutesFormat.Replace("{{ Meeting Venue }}", lstTemplateFieldData[2]);
                        }
                    }
                }
                return minutesFormat;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Minutes Formates For Default Agenda Item (Quorum)
        public bool SetMinutesForDefaultAgendaItem(long meetingId, int usedFor, List<string> lstLeaveOfAbsence, int userId)
        {
            try
            {
                var minutesFormat = GetMinutesForDefaultAgendaItem(usedFor, lstLeaveOfAbsence);

                if (!string.IsNullOrEmpty(minutesFormat))
                {
                    var defaultAgenda = (from meeting in entities.BM_Meetings
                                         join mapping in entities.BM_MeetingAgendaMapping on meeting.MeetingID equals mapping.MeetingID
                                         join agenda in entities.BM_AgendaMaster on mapping.AgendaID equals agenda.BM_AgendaMasterId
                                         where meeting.MeetingID == meetingId && meeting.IsDeleted == false && mapping.IsDeleted == false && agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.QUORUM && agenda.IsDeleted == false
                                         select mapping.MeetingAgendaMappingID).FirstOrDefault();

                    var _obj = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == defaultAgenda && k.IsDeleted == false).FirstOrDefault();

                    if (_obj != null)
                    {
                        _obj.MinutesFormat = minutesFormat;
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        #endregion

        #region Director UI
        public IEnumerable<MeetingVM> GetTodaysMeetingDirectorwise(int customerId, int userId)
        {
            try
            {
                var result = (from row in entities.BM_Meetings
                              join MP in entities.BM_MeetingParticipant on row.MeetingID equals MP.Meeting_ID
                              join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                              join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                              where row.Customer_Id == customerId && row.IsDeleted == false && MP.UserId == userId && row.MeetingDate == DateTime.Now
                              orderby row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn descending
                              select new MeetingVM
                              {
                                  MeetingID = row.MeetingID,
                                  Type = row.MeetingCircular.Trim(),
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = meeting.MeetingTypeName,
                                  Quarter_ = row.Quarter_,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  IsSeekAvailability = row.IsSeekAvailability,
                                  Availability_ShortDesc = row.Availability_ShortDesc,
                                  TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                  Entityt_Id = row.EntityId,
                                  EntityName = entity.CompanyName,
                                  FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Meeting Details
        public MeetingDetailsVM GetMeetingOtherDetails(long meetingId, int entityId)
        {
            var result = new MeetingDetailsVM() { MeetingsDetails_MeetingId = meetingId, MeetingsDetails_EntityId = entityId };
            try
            {
                result = (from row in entities.BM_MeetingsDetails
                          where row.MeetingId == meetingId && row.IsDeleted == false
                          select new MeetingDetailsVM
                          {
                              SigningAuthorityId = row.DirectorId,
                              MeetingsDetails_MeetingId = row.MeetingId,
                              MeetingsDetails_EntityId = entityId
                          }).FirstOrDefault();

                if (result == null)
                {
                    result = new MeetingDetailsVM() { MeetingsDetails_MeetingId = meetingId, MeetingsDetails_EntityId = entityId };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public MeetingDetailsVM SaveMeetingOtherDetails(MeetingDetailsVM obj, int userId)
        {
            try
            {
                if (obj.SigningAuthorityId > 0)
                {
                    obj.SigningAuthorityDesignation = (from meeting in entities.BM_Meetings
                                                       join row in entities.BM_Directors_DetailsOfInterest on meeting.EntityId equals row.EntityId
                                                       join director in entities.BM_DirectorMaster on row.Director_Id equals director.Id
                                                       join NOI in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals NOI.Id
                                                       join designation in entities.BM_Directors_Designation on row.IfDirector equals designation.Id
                                                       where meeting.MeetingID == obj.MeetingsDetails_MeetingId && NOI.IsDirector == true && director.Is_Deleted == false && row.IsDeleted == false
                                                       select designation.Name
                                                       ).FirstOrDefault();
                }
                var result = (from row in entities.BM_MeetingsDetails
                              where row.MeetingId == obj.MeetingsDetails_MeetingId && row.IsDeleted == false
                              select row).FirstOrDefault();
                if (result == null)
                {
                    result = new BM_MeetingsDetails();
                    result.MeetingId = obj.MeetingsDetails_MeetingId;
                    result.DirectorId = obj.SigningAuthorityId;
                    result.Designation = obj.SigningAuthorityDesignation;
                    result.CreatedBy = userId;
                    result.CreatedOn = DateTime.Now;
                    result.IsDeleted = false;
                    entities.BM_MeetingsDetails.Add(result);
                    entities.SaveChanges();

                    obj.MeetingsDetailsId = result.Id;
                    obj.Success = true;
                    obj.Message = "Saved Successfully.";
                }
                else
                {
                    result.MeetingId = obj.MeetingsDetails_MeetingId;
                    result.DirectorId = obj.SigningAuthorityId;
                    result.Designation = obj.SigningAuthorityDesignation;
                    result.UpdatedBy = userId;
                    result.UpdatedOn = DateTime.Now;
                    entities.Entry(result).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = "Updated Successfully.";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Check AGM Exists
        public bool CheckAGMExistsOrNotByFY(int entityId, int fyId)
        {
            var result = false;
            try
            {
                result = (from row in entities.BM_Meetings
                          where row.EntityId == entityId && row.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM && row.FY == fyId && row.IsDeleted == false
                          select row.MeetingID).Any();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public bool CheckAGMExistsOrNotBySrNo(int entityId, int SrNo)
        {
            var result = false;
            try
            {
                result = (from row in entities.BM_Meetings
                          where row.EntityId == entityId && row.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM && row.MeetingSrNo == SrNo && row.IsDeleted == false
                          select row.MeetingID).Any();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public int? GetNewAGMSrNo(int entityId)
        {
            int? result = 1;
            try
            {
                int? Meeting_SrNo = null;
                int? temp_SrNo = null;
                long? temp_FY = null;

                var config = (from row in entities.BM_ConfigurationMaster
                              where row.EntityID == entityId && row.MeetingType == SecretarialConst.MeetingTypeID.AGM && row.IsDeleted == false
                              select new { row.LastMeetingNo, row.MeetingNoType, row.MeetingFY }).FirstOrDefault();

                if (config != null)
                {
                    temp_SrNo = config.LastMeetingNo;
                    temp_FY = config.MeetingFY;
                }

                Meeting_SrNo = (from row in entities.BM_Meetings
                                where row.EntityId == entityId && row.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM && row.IsDeleted == false
                                select row.MeetingSrNo).Max();

                if (Meeting_SrNo == null && config != null)
                {
                    Meeting_SrNo = temp_SrNo;
                }

                result = Meeting_SrNo == null ? 1 : Meeting_SrNo + 1;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;

        }
        public int? GetNextMeetingSrNo(int entityId, int meetingTypeId, string meetingCircular, int fyId)
        {
            int? result = 1;
            try
            {
                int? Meeting_SrNo = null;
                int? temp_SrNo = null;
                long? temp_FY = null;
                var IsContinuosSrNo = true;

                var config = (from row in entities.BM_ConfigurationMaster
                              where row.MeetingType == meetingTypeId && row.EntityID == entityId && row.IsDeleted == false
                              select new { row.LastMeetingNo, row.LastCircularNo, row.MeetingNoType, row.CircularNoType, row.MeetingFY, row.CircularFY }).FirstOrDefault();

                if (config != null)
                {
                    if (config.MeetingNoType == "FY")
                    {
                        IsContinuosSrNo = false;
                    }

                    if (meetingCircular == SecretarialConst.MeetingCircular.MEETING)
                    {
                        temp_SrNo = config.LastMeetingNo;
                        temp_FY = config.MeetingFY;
                    }
                    else if (meetingCircular == SecretarialConst.MeetingCircular.CIRCULAR)
                    {
                        temp_SrNo = config.LastCircularNo;
                        temp_FY = config.CircularFY;
                    }
                }

                if (IsContinuosSrNo || meetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                {
                    //Continueous
                    Meeting_SrNo = (from row in entities.BM_Meetings
                                    where row.MeetingTypeId == meetingTypeId && row.EntityId == entityId && row.MeetingCircular == meetingCircular && row.IsDeleted == false
                                    select row.MeetingSrNo).Max();
                }
                else
                {
                    //Financial year-wise
                    Meeting_SrNo = (from row in entities.BM_Meetings
                                    where row.MeetingTypeId == meetingTypeId && row.EntityId == entityId && row.MeetingCircular == meetingCircular && row.IsDeleted == false &&
                                    row.FY == fyId
                                    select row.MeetingSrNo).Max();
                }

                if (Meeting_SrNo == null && config != null)
                {
                    Meeting_SrNo = IsContinuosSrNo ? temp_SrNo : fyId == temp_FY ? temp_SrNo : null;
                }

                result = Meeting_SrNo == null ? 1 : Meeting_SrNo + 1;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;

        }
        #endregion

        #region General meeting Notes
        public MeetingNotesVM SaveNotes(MeetingNotesVM obj, int userId)
        {
            try
            {
                var _obj = (from row in entities.BM_Meetings
                            where row.MeetingID == obj.NotesID
                            select row).FirstOrDefault();
                if (_obj != null)
                {
                    _obj.Notes = obj.Notes;
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = SecretarialConst.Messages.updateSuccess;
                }
                else
                {
                    obj.Error = true;
                    obj.Message = SecretarialConst.Messages.serverError;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Meeting Documents
        public Message UploadMeetingDocuments(VM_AgendaDocument obj, int userId, int customerId)
        {
            var result = new Message();
            try
            {
                var meeting = GetMeetingForDocumentName(customerId, obj.FileUpload_MeetingId);
                string path = "~/Areas/BM_Management/Documents/" + customerId + "/Meetings/" + obj.FileUpload_MeetingId;
                if (meeting != null)
                {
                    path = "~/Areas/BM_Management/Documents/" + customerId + "/" + meeting.Entityt_Id + "/Meeting/" + obj.FileUpload_MeetingId;
                }
                if (obj.files != null)
                {
                    foreach (var file in obj.files)
                    {
                        if (file.ContentLength > 0)
                        {
                            Stream fs = file.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            var objFileData = new FileDataVM();
                            objFileData.FileName = file.FileName;
                            objFileData.FilePath = path;
                            objFileData.FileData = bytes;

                            objFileData.MeetingId = obj.FileUpload_MeetingId;
                            objFileData.MeetingDoc = obj.MeetingDocs;

                            objIFileData_Service.SaveSecretarialDocs(objFileData, userId);
                        }
                    }

                    result.Success = true;
                    result.Message = SecretarialConst.Messages.saveSuccess;
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
            }
            return result;
        }


        #endregion

        #region Checkcustomer wise Video Confrencing
        public bool IsVideoConferencingApplicable_CustomerWise(int customerId)
        {
            bool success = false;
            try
            {
                success = (from x in entities.BM_CustomerConfiguration
                           where x.CustomerID == customerId
                           && x.IsActive == true
                           && x.IsVirtualMeeting != null
                           select x.IsVirtualMeeting).FirstOrDefault();
            }
            catch (Exception ex)
            {

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return success;
        }
        #endregion

        #region Agenda - Performer/Reviewer
        public AgendaReviewVM GetAgendaReviewDetails(long mappingId)
        {
            var result = new AgendaReviewVM();
            try
            {
                result = (from row in entities.BM_MeetingAgendaMapping
                          where row.MeetingAgendaMappingID == mappingId && row.IsActive == true && row.IsDeleted == false
                          select new AgendaReviewVM
                          {
                              MeetingAgendaMappingID = row.MeetingAgendaMappingID,
                              Meeting_Id = row.MeetingID,
                              AgendaFormatHeading = row.AgendaItemText,
                              AgendaFormat = row.AgendaFormat,
                              ResolutionFormat = row.ResolutionFormat
                          }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

		#region Set Agenda document generation flag
        public void MeetingAgendaSetGenerationFlag(long meetingId)
        {
            try
            {
                using (Compliance_SecretarialEntities context = new Compliance_SecretarialEntities())
                {
                    context.BM_SP_MeetingAgendaSetGenerationFlag(meetingId);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Draft Minutes viewer using Syncfusion
        public MinutesViewerVM GetDraftMinutesDetails(long meetingId)
        {
            var result = new MinutesViewerVM();
            try
            {
                result = (from row in entities.BM_Meetings
                          where row.MeetingID == meetingId && row.IsDeleted == false
                          select new MinutesViewerVM
                          {
                              MeetingMinutesId = row.MeetingID,
                              MeetingTypeId = row.MeetingTypeId
                          }).FirstOrDefault();
                if (result != null)
                {
                    result.MeetingMinutesDetails = GetMinutesDetails(meetingId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Copy Meetings
        public List<CopyMeetingSourceMeetingVM> GetSourceMeetings(int CustomerId)
        {
            var result = (from meeting in entities.BM_Meetings 
                          join committee in entities.BM_CommitteeComp on meeting.MeetingTypeId equals committee.Id 
                          join entity in entities.BM_EntityMaster on meeting.EntityId equals entity.Id
                          where meeting.Customer_Id == CustomerId 
                          && meeting.IsNoticeSent == false && meeting.IsDeleted == false && (meeting.IsCompleted == false || meeting.IsVirtualMeeting == true)
                          select new CopyMeetingSourceMeetingVM
                          {
                              MeetingID = meeting.MeetingID,
                              //MeetingTitle = entity.CompanyName + " "+ (meeting.MeetingTitle == null ? "Draft - " + committee.MeetingTypeName : meeting.MeetingTitle),
                              MeetingTitle = (meeting.MeetingTitle == null ? "Draft - " + committee.MeetingTypeName : meeting.MeetingTitle),
                              EntityName = entity.CompanyName,
                              EntityId = entity.Id,
                              EntityTypeId = entity.Entity_Type,
                              MeetingDate = meeting.MeetingDate,
                              MeetingTime = meeting.MeetingTime
                          }).ToList();
            return result;
        }

        public List<MeetingAgendaListForCopyMeeting_ResultVM> GetSourceMeetingsAgendaList(long meetingId, int customerId)
        {

            return (from row in entities.BM_SP_MeetingAgendaListForCopyMeeting(meetingId, customerId)
                    select new MeetingAgendaListForCopyMeeting_ResultVM
                    {
                        PartId = row.PartId,
                        SrNo = row.SrNo,
                        ItemNo = row.ItemNo,
                        MeetingAgendaMappingID = row.MeetingAgendaMappingID,
                        AgendaID = row.AgendaID,
                        AgendaItemText = row.AgendaItemText,
                        RefMasterId = row.RefMasterID
                    }).ToList();
        }

        public CopyMeetingVM SaveCopyMeetings(CopyMeetingVM obj, int customerID, int UserID)
        {
            try
            {
                var sourceMeeting = (from row in entities.BM_Meetings
                                    where row.MeetingID == obj.SourceMeetingID && row.IsDeleted == false
                                    select new
                                    {
                                        row.MeetingTypeId,
                                        row.MeetingCircular,
                                        row.IsShorter,
                                        row.FY,
                                        row.Quarter_,
                                        row.IsVirtualMeeting,
                                    }).FirstOrDefault();
                if(sourceMeeting != null)
                {
                    foreach (var entityId in obj.lstEntityId)
                    {
                        var meeting = new Meeting_NewVM()
                        {
                            Entityt_Id = entityId,
                            MeetingTypeId = sourceMeeting.MeetingTypeId,
                            Type = sourceMeeting.MeetingCircular,
                            IsShorter = sourceMeeting.IsShorter,
                            FYID = sourceMeeting.FY,
                            Quarter_ = sourceMeeting.Quarter_,
                            IsVirtualMeeting = sourceMeeting.IsVirtualMeeting,
                            CustomerId = customerID,
                            UserId = UserID
                        };
                        meeting = Create(meeting);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public CopyMeetingMessageVM CopyMeetingCheckComplianceAssigned(CopyMeetingCheckComplianceAssignedVM obj, int userId, int customerId)
        {
            var result = new CopyMeetingMessageVM() {  lstPendingAssignements = new List<CopyMeetingComplianceAssignementPendingVM>() };
            try
            {
                if(obj != null)
                {
                    var errorFlag = false;
                    DateTime? meetingDate = null;
                    string meetingTime = null;
                    if(obj.lstEntityId != null && obj.lstAgendaItems != null)
                    {
                        if(obj.lstEntityId.Count > 0 && obj.lstAgendaItems.Count > 0)
                        {
                            #region Check Compliance assignment for all entities
                            foreach (var entityId in obj.lstEntityId)
                            {
                                var entityName = (from row in entities.BM_EntityMaster
                                                  where row.Id == entityId
                                                  select row.CompanyName).FirstOrDefault();
                                var temp = new CopyMeetingComplianceAssignementPendingVM()
                                {
                                    EntityName = entityName,
                                    lstAgendaItems = new List<MeetingAgendaListForCopyMeeting_ResultVM>()
                                };

                                var pendingComplianceFlag = false;
                                foreach (var item in obj.lstAgendaItems)
                                {
                                    var pendingComplianceAssignment = (from row in entities.BM_SP_MeetingCheckComplianceAssign(item.AgendaID, null, customerId, entityId)
                                                 select row).ToList();

                                    if(pendingComplianceAssignment != null)
                                    {
                                        if(pendingComplianceAssignment.Count > 0)
                                        {
                                            foreach (var pendingCompliances in pendingComplianceAssignment)
                                            {
                                                temp.lstAgendaItems.Add(new MeetingAgendaListForCopyMeeting_ResultVM()
                                                {
                                                    AgendaID = pendingCompliances.AgendaId,
                                                    AgendaItemText = pendingCompliances.Agenda
                                                });
                                                pendingComplianceFlag = true;
                                            }
                                        }
                                    }
                                }

                                if(pendingComplianceFlag)
                                {
                                    result.lstPendingAssignements.Add(temp);
                                    result.Error = true;
                                    errorFlag = true;
                                }
                            }
                            #endregion

                            #region Create Meetings
                            if(errorFlag == false)
                            {
                                long? meetingCopyHeaderId = 0;
                                if(!string.IsNullOrEmpty(obj.MeetingDateStr) && !string.IsNullOrEmpty(obj.MeetingTimeStr))
                                {
                                    DateTime meetingDateTemp = new DateTime();
                                    if (!DateTime.TryParse(obj.MeetingDateStr, out meetingDateTemp))
                                    {
                                        meetingDate = null;
                                        meetingTime = null;
                                    }
                                    else
                                    {
                                        meetingDate = meetingDateTemp;
                                        meetingTime = obj.MeetingTimeStr;
                                    }
                                }

                                var sourceMeeting = (from row in entities.BM_Meetings
                                                     where row.MeetingID == obj.SourceMeetingID && row.IsDeleted == false
                                                     select new
                                                     {
                                                         row.MeetingTypeId,
                                                         row.MeetingCircular,
                                                         row.IsShorter,
                                                         row.FY,
                                                         row.Quarter_,
                                                         row.IsVirtualMeeting,
                                                     }).FirstOrDefault();
                                if (sourceMeeting != null)
                                {
                                    var meetingCopyHeader = (from row in entities.BM_MeetingCopyHeader
                                                             where row.SourceMeetingId == obj.SourceMeetingID && row.IsActive == true && row.IsDeleted == false
                                                             select row).FirstOrDefault();

                                    if (meetingCopyHeader == null)
                                    {
                                        meetingCopyHeader = new BM_MeetingCopyHeader()
                                        {
                                            SourceMeetingId = obj.SourceMeetingID,
                                            IsActive = true,
                                            IsDeleted = false,
                                            CreatedBy = userId,
                                            CreatedOn = DateTime.Now
                                        };
                                        entities.BM_MeetingCopyHeader.Add(meetingCopyHeader);
                                        entities.SaveChanges();
                                        meetingCopyHeaderId = meetingCopyHeader.Id;
                                    }
                                    else
                                    {
                                        meetingCopyHeaderId = meetingCopyHeader.Id;
                                        meetingCopyHeader.IsActive = true;
                                        meetingCopyHeader.IsDeleted = false;
                                        meetingCopyHeader.UpdatedBy = userId;
                                        meetingCopyHeader.UpdatedOn = DateTime.Now;
                                        entities.SaveChanges();
                                    }

                                    foreach (var entityId in obj.lstEntityId)
                                    {
                                        var meeting = new Meeting_NewVM()
                                        {
                                            Entityt_Id = entityId,
                                            MeetingTypeId = sourceMeeting.MeetingTypeId,
                                            Type = sourceMeeting.MeetingCircular,
                                            IsShorter = sourceMeeting.IsShorter,
                                            FYID = sourceMeeting.FY,
                                            Quarter_ = sourceMeeting.Quarter_,
                                            IsVirtualMeeting = sourceMeeting.IsVirtualMeeting,
                                            CustomerId = customerId,
                                            UserId = userId,
                                            MeetingDate = meetingDate,
                                            MeetingTime = meetingTime
                                        };
                                        meeting = Create(meeting);

                                        long meetingId = 0;
                                        if (meeting.Success)
                                        {
                                            meetingId = meeting.MeetingID;
                                        }
                                        else
                                        {
                                            if(meeting.Message.Contains("Draft"))
                                            {

                                                meetingId = (from row in entities.BM_Meetings
                                                            where row.EntityId == meeting.Entityt_Id && row.MeetingTypeId == meeting.MeetingTypeId && row.MeetingCircular == meeting.Type &&
                                                            row.Quarter_ == meeting.Quarter_ && row.IsNoticeSent == false && row.IsDeleted == false && row.IsCompleted == false
                                                            orderby row.MeetingID descending
                                                            select row.MeetingID).FirstOrDefault();
                                            }
                                        }

                                        if(meetingId > 0)
                                        {
                                            #region Copy Agenda
                                            foreach (var item in obj.lstAgendaItems)
                                            {
                                                var objAgendaItemSelect = new AgendaItemSelect()
                                                {
                                                    BM_AgendaMasterId = (long)item.AgendaID,
                                                    Meeting_Id = meetingId,
                                                    PartID = item.PartId,
                                                    CopyMeetingId = obj.SourceMeetingID
                                                };

                                                var agendaMapping = AddAgendaItem(objAgendaItemSelect, userId, customerId);
                                                if(agendaMapping.CopyAgendaMappingId > 0 && (item.RefMasterId == null || item.RefMasterId == 0))
                                                {
                                                    InsertTemplates(agendaMapping.CopyAgendaMappingId, item.MeetingAgendaMappingID, userId);
                                                    //Added on 19 Mar 2021
                                                    UpdateAgendaItemFormatedText(agendaMapping.CopyAgendaMappingId);
                                                    //End
                                                }
                                            }
                                            #endregion

                                            #region Create Pre-Committee Meetings
                                            var lstPendingPreCommittee = GetPendingPreCommittee(meetingId);
                                            if(lstPendingPreCommittee != null)
                                            {
                                                if(lstPendingPreCommittee.Count > 0)
                                                {
                                                    foreach (var PendingPreCommitteeMeetings in lstPendingPreCommittee)
                                                    {
                                                        var targetMeetingId = (from row in entities.BM_Meetings
                                                                               where row.EntityId == PendingPreCommitteeMeetings.EntityID && row.MeetingTypeId == PendingPreCommitteeMeetings.MeetingTypeID && row.MeetingCircular == meeting.Type &&
                                                                               row.Quarter_ == meeting.Quarter_ && row.IsNoticeSent == false && row.IsDeleted == false && row.IsCompleted == false
                                                                               orderby row.MeetingID descending
                                                                               select row.MeetingID).FirstOrDefault();


                                                        SavePreCommitteeMeetingID((int)PendingPreCommitteeMeetings.MeetingTypeID, meetingId, 0, 0, targetMeetingId, userId);
                                                    }
                                                }
                                            }
                                            #endregion

                                            #region Other meeting level code
                                            SetDefaultItemNumbers(meetingId);
                                            objIMeeting_Compliances.GenerateScheduleOn(meetingId, SecretarialConst.ComplianceMappingType.AGENDA, customerId, userId);

                                            #region Compliance Activation before Meeting
                                            objIComplianceTransaction_Service.CreateTransactionOnNoticeSend(meetingId, userId);
                                            #endregion
                                            //ReOpenVirtualMeeting(meetingId, userId);
                                            #endregion

                                            #region Copy meeting Log
                                            if(meetingCopyHeaderId > 0)
                                            {
                                                var meetingCopyDetails = (from row in entities.BM_MeetingCopyDetails
                                                                          where row.MeetingCopyHeaderId == meetingCopyHeaderId && row.TargetMeetingId == meetingId
                                                                          select row).FirstOrDefault();
                                                if(meetingCopyDetails == null)
                                                {
                                                    meetingCopyDetails = new BM_MeetingCopyDetails()
                                                    {
                                                        MeetingCopyHeaderId = meetingCopyHeaderId,
                                                        TargetMeetingId = meetingId,
                                                        IsActive = true,
                                                        IsDeleted = false,
                                                        CreatedBy = userId,
                                                        CreatedOn = DateTime.Now
                                                    };
                                                    entities.BM_MeetingCopyDetails.Add(meetingCopyDetails);
                                                    entities.SaveChanges();
                                                }
                                                else
                                                {
                                                    meetingCopyDetails.IsActive = true;
                                                    meetingCopyDetails.IsDeleted = false;
                                                    meetingCopyDetails.UpdatedBy = userId;
                                                    meetingCopyDetails.UpdatedOn = DateTime.Now;
                                                    entities.SaveChanges();
                                                }
                                            }
                                            #endregion
                                        }
                                    }

                                    result.Success = true;
                                    result.Message = SecretarialConst.Messages.saveSuccess;
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<CopyMeetingLogVM> GetCopyMeetingLog(long meetingId, int customerId)
        {
            var result = new List<CopyMeetingLogVM>();
            try
            {
                result = (from row in entities.BM_SP_MeetingCopyLog(meetingId, customerId)
                          select new CopyMeetingLogVM
                          {
                              TargetMeetingId = row.TargetMeetingId,
                              TargetMeetingTitle = row.MeetingTitle,
                              TargetMeetingEntityName = row.CompanyName,
                              Dated = row.Dated
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion
    }
}