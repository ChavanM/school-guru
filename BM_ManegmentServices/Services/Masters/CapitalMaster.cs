﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;
using System.Globalization;

namespace BM_ManegmentServices.Services.Masters
{
    public class CapitalMaster : ICapitalMaster
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        #region Public Privat Capital Master
        #region show list Capital Master
        public AuthorizedCapital GetAuthorisedCapital(int userID, int CustomerId, int entityId)
        {
            try
            {
                var AuthorizedCapital = (from row in entities.BM_CapitalMaster
                                         where row.CustomerId == CustomerId
                                         && row.Entity_Id == entityId
                                         select new AuthorizedCapital
                                         {
                                             AuthorizedCapval = row.AuthorizedCapital,
                                             AuthorisedId = row.Id,
                                             Entity_Id = row.Entity_Id
                                         }).FirstOrDefault();
                return AuthorizedCapital;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public VMDebentures GetDebenger(int userID, int CustomerId, int entityId, long authorisedId)
        {
            try
            {
                var _objDebenger = (from row in entities.BM_CapitalMaster
                                    join rows in entities.BM_Debentures
                                    on row.Id equals (rows.CapitalId)
                                    where row.Id == authorisedId
                                    select new VMDebentures
                                    {
                                        Id = row.Id,

                                        NumberofUnitsncd = rows.No_NonConDebUnits,
                                        NumberofUnitspcd = rows.No_PartiallyConDebUnit,
                                        NumberofUnitsfcd = rows.No_FullyConDebUnit,
                                        NominalValueperunitncd = rows.Nom_NonConDebperUnits,
                                        NominalValueperunitnpcd = rows.Nom_PartiallyConDebperUnit,
                                        NominalValueperunitnfcd = rows.Nom_FullyConDebperUnit,
                                        TotalValuencd = rows.tot_NonConDeb,
                                        TotalValuenpcd = rows.tot_PartiallyConDeb,
                                        TotalValuenfcd = rows.tot_FullyConDebper,
                                        TotalDebentures = rows.Total,
                                        IsDebentures = row.IsDebenger

                                    }).FirstOrDefault();
                return _objDebenger;
            }
            catch (Exception ex)
            {
                VMDebentures obj = new VMDebentures();
                obj.errorMessage = true;
                obj.SuccessErrorMsg = "Server error occurred";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public VMCapitalData GetEquityCapital(int userID, int v, int entityId, long authorisedId)
        {
            try
            {
                var _objEquiety = (from row in entities.BM_CapitalMaster

                                   where row.Id == authorisedId
                                   && row.Entity_Id == entityId
                                   select new VMCapitalData
                                   {
                                       Id = row.Id,
                                       EntityId = row.Entity_Id,
                                       TnAuthorisedCapital = row.TotNoAuthorizedCapita,
                                       TnIssuedCapital = row.TotNoIssuedCapital,
                                       TnSubscribedCapital = row.TotNoSubscribCapital,
                                       TnPaidupCapital = row.TotNoPaidupCapital,
                                       TaAuthorisedCapital = row.TotamtAuthorizedCapita,
                                       TaIssuedCapital = row.TotamtNoIssuedCapital,
                                       TaSubscribedCapital = row.TotamtNoSubscribCapital,
                                       TaPaidupCapital = row.TotamtNoPaidupCapital
                                   }).FirstOrDefault();
                return _objEquiety;
            }
            catch (Exception ex)
            {
                VMCapitalData obj = new VMCapitalData();
                obj.errorMessage = true;
                obj.SuccessErrorMsg = "Server error occurred";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public List<Shares> GetEquityCapital_subdtls(int userID, int CustomerId, long equityId, int EntityId)
        {
            try
            {
                var eqtsubdtls = (from row in entities.BM_Share
                                  where
    row.CapitalMasterId == equityId
    && row.Sharetype == "E"
    && row.IsActive == true
                                  select new Shares
                                  {
                                      Equ_sharesID = row.Id,
                                      AuthorisedCapital1 = row.No_AuhorisedCapital,
                                      AuthorisedCapital2 = row.Nominalper_AuhorisedCapital,
                                      AuthorisedCapital3 = row.totamt_AuhorisedCapital,
                                      IssuedCapital1 = row.No_IssuedCapital,
                                      IssuedCapital2 = row.Nominalper_IssuedCapital,
                                      IssuedCapital3 = row.totamt_IssuedCapital,
                                      PaidupCapital1 = row.No_Paid_upCapital,
                                      PaidupCapital2 = row.Nominalper_Paid_upCapital,
                                      PaidupCapital3 = row.totamt_upCapital,
                                      SubscribedCapital1 = row.No_SubscribedCapital,
                                      SubscribedCapital2 = row.Nominalper_SubscribedCapital,
                                      SubscribedCapital3 = row.totamt_SubscribedCapital,
                                      sharesID = (int)row.Shares_Class
                                  }).ToList();
                return eqtsubdtls;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMPrifrenscShare GetPrifrenceCapital(int userID, int CustomerId, int entityId, long capitalID)
        {
            try
            {
                var _objPreifrencedtls = (from row in entities.BM_CapitalMaster
                                          where row.Id == capitalID
                                          && row.IsActive == true
                                          select new VMPrifrenscShare
                                          {
                                              Id = row.Id,
                                              pTnAuthorisedCapital = row.Pri_TotNoAuthorizedCapita,
                                              pTnIssuedCapital = row.Pri_TotNoIssuedCapital,
                                              pTnSubscribedCapital = row.Pri_TotNoSubscribCapital,
                                              pTnPaidupCapital = row.Pri_TotNoPaidupCapital,
                                              pTaAuthorisedCapital = row.Pri_TotamtAuthorizedCapita,
                                              pTaIssuedCapital = row.Pri_TotamtNoIssuedCapital,

                                              pTaSubscribedCapital = row.Pri_TotamtNoSubscribCapital,
                                              pTaPaidupCapital = row.Pri_TotamtNoPaidupCapital,
                                              IsPrefrence = row.IsPrefrence,
                                              Coupen_Rate = row.Pri_CoupenRate,
                                              Preferance_capitalType = row.Pri_CapitalType,

                                          }).FirstOrDefault();
                return _objPreifrencedtls;
            }
            catch (Exception ex)
            {
                VMPrifrenscShare obj = new VMPrifrenscShare();
                obj.errorMessage = true;
                obj.SuccessErrorMsg = "Server error occurred";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public pShares GetPrifrenceCapitalList(int userID, int CustomerId, int entityId, long capitalID)
        {
            try
            {
                var prifrencedtls = (from row in entities.BM_Share
                                     where
       row.CapitalMasterId == capitalID
       && row.CustomerId == CustomerId
       && row.Sharetype == "P"
       && row.IsActive == true
                                     select new pShares
                                     {
                                         psharesID = row.Id,
                                         pAuthorisedCapital1 = row.No_AuhorisedCapital,
                                         pAuthorisedCapital2 = row.Nominalper_AuhorisedCapital,
                                         pAuthorisedCapital3 = row.totamt_AuhorisedCapital,
                                         pIssuedCapital1 = row.No_IssuedCapital,
                                         pIssuedCapital2 = row.Nominalper_IssuedCapital,
                                         pIssuedCapital3 = row.totamt_IssuedCapital,
                                         pPaidupCapital1 = row.No_Paid_upCapital,
                                         pPaidupCapital2 = row.Nominalper_Paid_upCapital,
                                         pPaidupCapital3 = row.totamt_upCapital,
                                         pSubscribedCapital1 = row.No_SubscribedCapital,
                                         pSubscribedCapital2 = row.Nominalper_SubscribedCapital,
                                         pSubscribedCapital3 = row.totamt_SubscribedCapital,



                                     }).FirstOrDefault();
                return prifrencedtls;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMUnclassified GetUnclassified(int userID, int CustomerId, int entityId, long authorisedId)
        {
            try
            {
                var _objunclassified = (from row in entities.BM_CapitalMaster
                                        where row.Id == authorisedId
                                        select new VMUnclassified
                                        {
                                            Id = row.Id,
                                            IsUnclassified = row.IsUnclassified,
                                            UnAuthorizedCapita = row.Unclassified_AuhorisedCapital
                                        }).FirstOrDefault();
                return _objunclassified;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }


        #endregion

        #region Save Capital Master
        public AuthorizedCapital SaveAuthorisedCapital(AuthorizedCapital objauhorizedCapital, int userID, int CustomerId)
        {
            try
            {
                AuthorizedCapital obj = new AuthorizedCapital();
                BM_CapitalMaster _objbm_capitalMaster = new BM_CapitalMaster();
                if (objauhorizedCapital.AuthorizedCapval >= 0)
                {
                    var _objCapitalMaster = (from row in entities.BM_CapitalMaster
                                             where row.Entity_Id == objauhorizedCapital.Entity_Id
                                             && row.IsActive == true
                                             && row.CustomerId == CustomerId
                                             select row).FirstOrDefault();
                    if (_objCapitalMaster == null)
                    {
                        _objbm_capitalMaster.AuthorizedCapital = objauhorizedCapital.AuthorizedCapval;
                        _objbm_capitalMaster.CustomerId = CustomerId;
                        _objbm_capitalMaster.Entity_Id = objauhorizedCapital.Entity_Id;
                        _objbm_capitalMaster.IsDebenger = false;
                        _objbm_capitalMaster.IsPrefrence = false;
                        _objbm_capitalMaster.IsUnclassified = false;
                        _objbm_capitalMaster.TotNoAuthorizedCapita = 0;
                        _objbm_capitalMaster.TotNoIssuedCapital = 0;
                        _objbm_capitalMaster.TotNoSubscribCapital = 0;
                        _objbm_capitalMaster.TotNoPaidupCapital = 0;
                        _objbm_capitalMaster.Pri_TotamtAuthorizedCapita = 0;
                        _objbm_capitalMaster.TotamtNoIssuedCapital = 0;
                        _objbm_capitalMaster.TotamtNoSubscribCapital = 0;
                        _objbm_capitalMaster.TotamtNoPaidupCapital = 0;
                        _objbm_capitalMaster.Pri_TotNoIssuedCapital = 0;
                        _objbm_capitalMaster.Pri_TotNoSubscribCapital = 0;
                        _objbm_capitalMaster.Pri_TotNoPaidupCapital = 0;
                        _objbm_capitalMaster.Pri_TotamtAuthorizedCapita = 0;
                        _objbm_capitalMaster.Pri_TotamtNoIssuedCapital = 0;
                        _objbm_capitalMaster.Pri_TotamtNoSubscribCapital = 0;
                        _objbm_capitalMaster.Pri_TotamtNoPaidupCapital = 0;
                        _objbm_capitalMaster.Unclassified_AuhorisedCapital = 0;
                        _objbm_capitalMaster.IsActive = true;
                        _objbm_capitalMaster.Createdby = userID;
                        _objbm_capitalMaster.CreatedOn = DateTime.Now;
                        entities.BM_CapitalMaster.Add(_objbm_capitalMaster);
                        entities.SaveChanges();
                        objauhorizedCapital.AuthorisedId = _objbm_capitalMaster.Id;
                        objauhorizedCapital.Message = true;
                        objauhorizedCapital.SuccessErrorMsg = "Saved successfully ";
                    }
                    else
                    {
                        var result = CheckValidCapital(objauhorizedCapital.AuthorizedCapval, "A", _objCapitalMaster.Id);
                        if (result == "true")
                        {
                            _objCapitalMaster.AuthorizedCapital = objauhorizedCapital.AuthorizedCapval;
                            _objCapitalMaster.CustomerId = CustomerId;
                            _objCapitalMaster.Entity_Id = objauhorizedCapital.Entity_Id;

                            entities.SaveChanges();
                            objauhorizedCapital.AuthorisedId = _objCapitalMaster.Id;
                            objauhorizedCapital.Message = true;
                            objauhorizedCapital.SuccessErrorMsg = "Updated  successfully";
                        }
                        else
                        {
                            objauhorizedCapital.SuccessErrorMsg = "Authorised Capital can't less then Equiety and Prefrence Capital.";
                            objauhorizedCapital.errorMessage = true;
                        }
                    }
                }
                else
                {
                    objauhorizedCapital.errorMessage = true;
                    objauhorizedCapital.SuccessErrorMsg = "somthing wents wrong ";
                }

            }
            catch (Exception ex)
            {
                objauhorizedCapital.errorMessage = true;
                objauhorizedCapital.SuccessErrorMsg = "Server error occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return objauhorizedCapital;
        }
        public VMDebentures SaveDebengerdtls(VMDebentures objDebenture, int userID, int CustomerId)
        {
            try
            {
                if (objDebenture != null)
                {
                    var debengrdtls = (from row in entities.BM_CapitalMaster
                                       where row.Id == objDebenture.Id
                                       && row.IsActive == true
                                       select row).FirstOrDefault();
                    if (debengrdtls != null)
                    {
                        debengrdtls.IsDebenger = objDebenture.IsDebentures;
                        entities.SaveChanges();
                        if (debengrdtls.Id > 0)
                        {
                            var isDebengerExist = (from row in entities.BM_Debentures
                                                   where row.CapitalId == debengrdtls.Id
                                                   && row.CustomerId == CustomerId
                                                   && row.IsActive == true
                                                   select row).FirstOrDefault();
                            if (debengrdtls.IsDebenger == true)
                            {
                                if (isDebengerExist == null)
                                {
                                    BM_Debentures _objBM_Debentures = new BM_Debentures();
                                    _objBM_Debentures.No_NonConDebUnits = objDebenture.NumberofUnitsncd;
                                    _objBM_Debentures.No_PartiallyConDebUnit = objDebenture.NumberofUnitspcd;
                                    _objBM_Debentures.No_FullyConDebUnit = objDebenture.NumberofUnitsfcd;
                                    _objBM_Debentures.Nom_NonConDebperUnits = objDebenture.NominalValueperunitncd;
                                    _objBM_Debentures.Nom_PartiallyConDebperUnit = objDebenture.NominalValueperunitnpcd;
                                    _objBM_Debentures.Nom_FullyConDebperUnit = objDebenture.NominalValueperunitnfcd;
                                    _objBM_Debentures.tot_NonConDeb = objDebenture.TotalValuencd;
                                    _objBM_Debentures.tot_PartiallyConDeb = objDebenture.TotalValuenpcd;
                                    _objBM_Debentures.tot_FullyConDebper = objDebenture.TotalValuenfcd;
                                    _objBM_Debentures.tot_NonConDeb = objDebenture.TotalValuencd;
                                    _objBM_Debentures.tot_PartiallyConDeb = objDebenture.TotalValuenpcd;
                                    _objBM_Debentures.tot_FullyConDebper = objDebenture.TotalValuenfcd;
                                    _objBM_Debentures.Total = objDebenture.TotalDebentures;
                                    _objBM_Debentures.CapitalId = (int)debengrdtls.Id;
                                    _objBM_Debentures.CustomerId = CustomerId;
                                    _objBM_Debentures.IsActive = true;
                                    entities.BM_Debentures.Add(_objBM_Debentures);
                                    entities.SaveChanges();
                                    objDebenture.Message = true;
                                    objDebenture.SuccessErrorMsg = "Saved successfully";
                                }
                                else
                                {
                                    isDebengerExist.No_NonConDebUnits = objDebenture.NumberofUnitsncd;
                                    isDebengerExist.No_PartiallyConDebUnit = objDebenture.NumberofUnitspcd;
                                    isDebengerExist.No_FullyConDebUnit = objDebenture.NumberofUnitsfcd;
                                    isDebengerExist.Nom_NonConDebperUnits = objDebenture.NominalValueperunitncd;
                                    isDebengerExist.Nom_PartiallyConDebperUnit = objDebenture.NominalValueperunitnpcd;
                                    isDebengerExist.Nom_FullyConDebperUnit = objDebenture.NominalValueperunitnfcd;
                                    isDebengerExist.tot_NonConDeb = objDebenture.TotalValuencd;
                                    isDebengerExist.tot_PartiallyConDeb = objDebenture.TotalValuenpcd;
                                    isDebengerExist.tot_FullyConDebper = objDebenture.TotalValuenfcd;
                                    isDebengerExist.tot_NonConDeb = objDebenture.TotalValuencd;
                                    isDebengerExist.tot_PartiallyConDeb = objDebenture.TotalValuenpcd;
                                    isDebengerExist.tot_FullyConDebper = objDebenture.TotalValuenfcd;
                                    isDebengerExist.Total = objDebenture.TotalDebentures;
                                    isDebengerExist.CapitalId = (int)debengrdtls.Id;
                                    isDebengerExist.CustomerId = CustomerId;
                                    isDebengerExist.IsActive = true;
                                    entities.SaveChanges();

                                    objDebenture.Message = true;
                                    objDebenture.SuccessErrorMsg = "Updated  successfully";
                                }
                            }
                            else
                            {
                                if (isDebengerExist != null)
                                {
                                    isDebengerExist.No_NonConDebUnits = 0;
                                    isDebengerExist.No_PartiallyConDebUnit = 0;
                                    isDebengerExist.No_FullyConDebUnit = 0;
                                    isDebengerExist.Nom_NonConDebperUnits = 0;
                                    isDebengerExist.Nom_PartiallyConDebperUnit = 0;
                                    isDebengerExist.Nom_FullyConDebperUnit = 0;
                                    isDebengerExist.tot_NonConDeb = 0;
                                    isDebengerExist.tot_PartiallyConDeb = 0;
                                    isDebengerExist.tot_FullyConDebper = 0;
                                    isDebengerExist.tot_NonConDeb = 0;
                                    isDebengerExist.tot_PartiallyConDeb = 0;
                                    isDebengerExist.tot_FullyConDebper = 0;
                                    isDebengerExist.Total = 0;
                                    isDebengerExist.CapitalId = (int)debengrdtls.Id;
                                    isDebengerExist.CustomerId = CustomerId;
                                    isDebengerExist.IsActive = true;
                                    entities.SaveChanges();

                                }
                                objDebenture.Message = true;
                                objDebenture.SuccessErrorMsg = "Updated  successfully";
                            }
                        }
                        else
                        {
                            objDebenture.errorMessage = true;
                            objDebenture.SuccessErrorMsg = "Somthing wents wrong";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objDebenture.errorMessage = true;
                objDebenture.SuccessErrorMsg = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return objDebenture;
        }


        public VMUnclassified SaveUnClassified(VMUnclassified objUnclassified, int userID, int CustomerId)
        {
            try
            {
                if (objUnclassified != null)
                {
                    #region Check Total Capital
                    decimal unclassified = 0;
                    if (objUnclassified.UnAuthorizedCapita > 0)
                    {
                        unclassified = (decimal)objUnclassified.UnAuthorizedCapita;
                    }

                    var result = CheckValidCapital(unclassified, "U", objUnclassified.Id);
                    if (result != "true")
                    {
                        if (result == "false")
                        {
                            objUnclassified.SuccessErrorMsg = "Please check Authorised Capital";
                            objUnclassified.errorMessage = true;
                        }
                        else
                        {
                            objUnclassified.SuccessErrorMsg = result;
                            objUnclassified.errorMessage = true;
                        }
                        return objUnclassified;
                    }
                    #endregion

                    var objUnclassifieddtls = (from row in entities.BM_CapitalMaster
                                               where row.Id == objUnclassified.Id
                                               && row.IsActive == true
                                               select row).FirstOrDefault();
                    if (objUnclassifieddtls != null)
                    {
                        if (objUnclassified.IsUnclassified == true)
                        {
                            objUnclassifieddtls.IsUnclassified = objUnclassified.IsUnclassified;
                            objUnclassifieddtls.Unclassified_AuhorisedCapital = objUnclassified.UnAuthorizedCapita;
                        }
                        else
                        {
                            objUnclassifieddtls.IsUnclassified = objUnclassified.IsUnclassified;
                            objUnclassifieddtls.Unclassified_AuhorisedCapital = 0;
                        }
                        entities.SaveChanges();
                        objUnclassified.Message = true;
                        objUnclassified.SuccessErrorMsg = "Saved successfully";
                    }
                    else
                    {
                        objUnclassified.errorMessage = true;
                        objUnclassified.SuccessErrorMsg = "Something wents wrong";
                        return objUnclassified;
                    }
                }
                else
                {
                    objUnclassified.errorMessage = true;
                    objUnclassified.SuccessErrorMsg = "Somthing wents wrong";
                    return objUnclassified;
                }

            }
            catch (Exception ex)
            {
                objUnclassified.errorMessage = true;
                objUnclassified.SuccessErrorMsg = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return objUnclassified;
        }

        public VMCapitalData AddEquietydtls(VMCapitalData _objequity, int userID, int CustomerId)
        {
            decimal TAOEquityAuth = 0;
            decimal TAOEquityIC = 0;
            decimal TAOEquitySC = 0;
            decimal TAOEquityPUC = 0;

            decimal TNOEquityAuth = 0;
            decimal TNOEquityIC = 0;
            decimal TNOEquitySC = 0;
            decimal TNOEquityPUC = 0;

            try
            {
                if (_objequity != null && _objequity.Id > 0)
                {
                    #region Check Total Capital
                    var result = CheckValidCapital(_objequity.TaAuthorisedCapital, "E", _objequity.Id);
                    if (result != "true")
                    {
                        if (result == "false")
                        {
                            _objequity.SuccessErrorMsg = "Please check Authorised Capital";
                            _objequity.errorMessage = true;
                        }
                        else
                        {
                            _objequity.SuccessErrorMsg = result;
                            _objequity.errorMessage = true;
                        }
                        return _objequity;
                    }
                    #endregion

                    var capitalDtls = (from row in entities.BM_CapitalMaster
                                       where row.Id == _objequity.Id
                                       && row.IsActive == true
                                       select row).FirstOrDefault();
                    if (capitalDtls != null)
                    {
                        capitalDtls.TotNoAuthorizedCapita = _objequity.TnAuthorisedCapital;
                        capitalDtls.TotNoIssuedCapital =    _objequity.TnIssuedCapital;
                        capitalDtls.TotNoSubscribCapital =  _objequity.TnSubscribedCapital;
                        capitalDtls.TotNoPaidupCapital =    _objequity.TnPaidupCapital;
                        capitalDtls.TotamtAuthorizedCapita = _objequity.TaAuthorisedCapital;
                        capitalDtls.TotamtNoIssuedCapital = _objequity.TaIssuedCapital;
                        capitalDtls.TotamtNoSubscribCapital = _objequity.TaSubscribedCapital;
                        capitalDtls.TotamtNoPaidupCapital = _objequity.TaPaidupCapital;
                        entities.SaveChanges();

                        if (capitalDtls.Id > 0)
                        {

                            var objdeleteshares = (from row in entities.BM_Share
                                                   where row.CapitalMasterId == capitalDtls.Id
                                                   && row.IsActive == true && row.Sharetype == "E"
                                                   select row).ToList();
                            if (objdeleteshares.Count > 0)
                            {
                                foreach (var items in objdeleteshares)
                                {
                                    items.IsActive = false;
                                    entities.SaveChanges();
                                }
                            }
                            foreach (var item in _objequity.shares)
                            {
                                var objshares = (from row in entities.BM_Share
                                                 where row.Id == item.Equ_sharesID
                                                 && row.CapitalMasterId == capitalDtls.Id
                                                 select row).FirstOrDefault();
                                if (objshares != null)
                                {
                                    objshares.Shares_Class = item.sharesID;
                                    objshares.No_AuhorisedCapital = item.AuthorisedCapital1;
                                    objshares.No_IssuedCapital = item.IssuedCapital1;
                                    objshares.No_Paid_upCapital = item.PaidupCapital1;
                                    objshares.No_SubscribedCapital = item.SubscribedCapital1;
                                    objshares.Nominalper_AuhorisedCapital = item.AuthorisedCapital2;
                                    objshares.Nominalper_IssuedCapital = item.IssuedCapital2;
                                    objshares.Nominalper_Paid_upCapital = item.PaidupCapital2;
                                    objshares.Nominalper_SubscribedCapital = item.SubscribedCapital2;
                                    objshares.totamt_AuhorisedCapital = item.AuthorisedCapital3;
                                    objshares.totamt_IssuedCapital = item.IssuedCapital3;
                                    objshares.totamt_upCapital = item.PaidupCapital3;
                                    objshares.totamt_SubscribedCapital = item.SubscribedCapital3;

                                    TNOEquityAuth = TNOEquityAuth + (item.AuthorisedCapital1);
                                    TNOEquityIC = TNOEquityIC + (item.IssuedCapital1);
                                    TNOEquitySC = TNOEquitySC + (item.SubscribedCapital1);
                                    TNOEquityPUC = TNOEquityPUC + (item.PaidupCapital1);

                                    TAOEquityAuth = TAOEquityAuth + item.AuthorisedCapital3;
                                    TAOEquityIC = TAOEquityIC + item.IssuedCapital3;
                                    TAOEquitySC = TAOEquitySC + item.SubscribedCapital3;
                                    TAOEquityPUC = TAOEquityPUC + item.PaidupCapital3;

                                    objshares.Sharetype = "E";
                                    objshares.IsActive = true;
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    BM_Share obj = new BM_Share();
                                    obj.Shares_Class = item.sharesID;
                                    obj.CapitalMasterId = (int)capitalDtls.Id;
                                    obj.No_AuhorisedCapital = item.AuthorisedCapital1;
                                    obj.No_IssuedCapital = item.IssuedCapital1;
                                    obj.No_Paid_upCapital = item.PaidupCapital1;
                                    obj.No_SubscribedCapital = item.SubscribedCapital1;
                                    obj.Nominalper_AuhorisedCapital = item.AuthorisedCapital2;
                                    obj.Nominalper_IssuedCapital = item.IssuedCapital2;
                                    obj.Nominalper_Paid_upCapital = item.PaidupCapital2;
                                    obj.Nominalper_SubscribedCapital = item.SubscribedCapital2;
                                    obj.totamt_AuhorisedCapital = item.AuthorisedCapital3;
                                    obj.totamt_IssuedCapital = item.IssuedCapital3;
                                    obj.totamt_upCapital = item.PaidupCapital3;
                                    obj.totamt_SubscribedCapital = item.SubscribedCapital3;

                                    TNOEquityAuth = TNOEquityAuth + (item.AuthorisedCapital1);
                                    TNOEquityIC = TNOEquityIC + (item.IssuedCapital1);
                                    TNOEquitySC = TNOEquitySC + (item.SubscribedCapital1);
                                    TNOEquityPUC = TNOEquityPUC + (item.PaidupCapital1);

                                    TAOEquityAuth = TAOEquityAuth + item.AuthorisedCapital3;
                                    TAOEquityIC = TAOEquityIC + item.IssuedCapital3;
                                    TAOEquitySC = TAOEquitySC + item.SubscribedCapital3;
                                    TAOEquityPUC = TAOEquityPUC + item.PaidupCapital3;

                                    obj.Sharetype = "E";
                                    obj.IsActive = true;
                                    obj.CustomerId = CustomerId;
                                    entities.BM_Share.Add(obj);
                                    entities.SaveChanges();
                                }

                            }
                            if (capitalDtls != null)
                            {
                                capitalDtls.TotNoAuthorizedCapita = TNOEquityAuth;
                                capitalDtls.TotNoIssuedCapital = TNOEquityIC;
                                capitalDtls.TotNoSubscribCapital = TNOEquitySC;
                                capitalDtls.TotNoPaidupCapital = TNOEquityPUC;

                                capitalDtls.TotamtAuthorizedCapita = TAOEquityAuth;
                                capitalDtls.TotamtNoIssuedCapital = TAOEquityIC;
                                capitalDtls.TotamtNoSubscribCapital = TAOEquitySC;
                                capitalDtls.TotamtNoPaidupCapital = TAOEquityPUC;
                                entities.SaveChanges();
                            }
                            _objequity.TnAuthorisedCapital = TNOEquityAuth;
                            _objequity.TnIssuedCapital = TNOEquityIC;
                            _objequity.TnSubscribedCapital = TNOEquitySC;
                            _objequity.TnPaidupCapital = TNOEquityPUC;
                            _objequity.TaAuthorisedCapital = TAOEquityAuth;
                            _objequity.TaIssuedCapital = TAOEquityIC;
                            _objequity.TaSubscribedCapital = TAOEquitySC;
                            _objequity.TaPaidupCapital = TAOEquityPUC;

                            _objequity.SuccessErrorMsg = "Saved successfully";
                            _objequity.Message = true;

                        }

                        else
                        {
                            _objequity.SuccessErrorMsg = "Something went wrong";
                            _objequity.errorMessage = true;

                        }
                    }
                    else
                    {
                        _objequity.SuccessErrorMsg = "Something went wrong";
                        _objequity.errorMessage = true;

                    }

                }
                else
                {
                    _objequity.SuccessErrorMsg = "Something went wrong";
                    _objequity.errorMessage = true;

                }

            }
            catch (Exception ex)
            {
                _objequity.SuccessErrorMsg = "Sarver error occured";
                _objequity.errorMessage = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objequity;
        }
        public VMPrifrenscShare AddPrefrence(VMPrifrenscShare objPrefrence, int userID, int CustomerId)
        {
            try
            {
                decimal TAOEquityAuth = 0;
                decimal TAOEquityIC = 0;
                decimal TAOEquitySC = 0;
                decimal TAOEquityPUC = 0;

                if (objPrefrence != null && objPrefrence.Id > 0)
                {
                    #region Check Total Capital
                    var result = CheckValidCapital(objPrefrence.pTaAuthorisedCapital, "P", objPrefrence.Id);
                    if (result != "true")
                    {
                        if (result == "false")
                        {
                            objPrefrence.SuccessErrorMsg = "Please check Authorised Capital";
                            objPrefrence.errorMessage = true;
                        }
                        else
                        {
                            objPrefrence.SuccessErrorMsg = result;
                            objPrefrence.errorMessage = true;
                        }
                        return objPrefrence;
                    }
                    #endregion

                    var prifrenceDtls = (from row in entities.BM_CapitalMaster
                                         where row.Id == objPrefrence.Id
                                         && row.IsActive == true
                                         select row).FirstOrDefault();
                    if (prifrenceDtls != null)
                    {
                        if (objPrefrence.IsPrefrence == true)
                        {
                            prifrenceDtls.Pri_TotNoAuthorizedCapita = objPrefrence.pTnAuthorisedCapital;
                            prifrenceDtls.Pri_TotNoIssuedCapital = objPrefrence.pTnIssuedCapital;
                            prifrenceDtls.Pri_TotNoSubscribCapital = objPrefrence.pTnSubscribedCapital;
                            prifrenceDtls.Pri_TotNoPaidupCapital = objPrefrence.pTnPaidupCapital;
                            prifrenceDtls.Pri_TotamtAuthorizedCapita = objPrefrence.pTaAuthorisedCapital;
                            prifrenceDtls.Pri_TotamtNoIssuedCapital = objPrefrence.pTaIssuedCapital;
                            prifrenceDtls.Pri_TotamtNoSubscribCapital = objPrefrence.pTaSubscribedCapital;
                            prifrenceDtls.Pri_TotamtNoPaidupCapital = objPrefrence.pTaPaidupCapital;
                            prifrenceDtls.IsPrefrence = objPrefrence.IsPrefrence;
                            prifrenceDtls.Pri_CapitalType = objPrefrence.Preferance_capitalType;
                            prifrenceDtls.Pri_CoupenRate = objPrefrence.Coupen_Rate;
                        }
                        else
                        {
                            prifrenceDtls.Pri_TotNoAuthorizedCapita = 0;
                            prifrenceDtls.Pri_TotNoIssuedCapital = 0;
                            prifrenceDtls.Pri_TotNoSubscribCapital = 0;
                            prifrenceDtls.Pri_TotNoPaidupCapital = 0;
                            prifrenceDtls.Pri_TotamtAuthorizedCapita = 0;
                            prifrenceDtls.Pri_TotamtNoIssuedCapital = 0;
                            prifrenceDtls.Pri_TotamtNoSubscribCapital = 0;
                            prifrenceDtls.Pri_TotamtNoPaidupCapital = 0;
                            prifrenceDtls.IsPrefrence = objPrefrence.IsPrefrence;
                            prifrenceDtls.Pri_CapitalType = 0;
                            prifrenceDtls.Pri_CoupenRate = 0;
                        }
                        entities.SaveChanges();
                        if (prifrenceDtls.Id > 0)
                        {

                            var objsdtls = (from row in entities.BM_Share
                                            where
                                            //row.Id == objPrefrence.pshares.psharesID
                                            //&&
                                            row.CapitalMasterId == prifrenceDtls.Id
                                            && row.CustomerId == CustomerId
                                            && row.Sharetype == "P"
                                            select row).FirstOrDefault();
                            if (objsdtls != null)
                            {
                                if (objPrefrence.IsPrefrence == true)
                                {
                                    objsdtls.No_AuhorisedCapital = objPrefrence.pshares.pAuthorisedCapital1;
                                    objsdtls.No_IssuedCapital = objPrefrence.pshares.pIssuedCapital1;
                                    objsdtls.No_Paid_upCapital = objPrefrence.pshares.pPaidupCapital1;
                                    objsdtls.No_SubscribedCapital = objPrefrence.pshares.pSubscribedCapital1;
                                    objsdtls.Nominalper_AuhorisedCapital = objPrefrence.pshares.pAuthorisedCapital2;
                                    objsdtls.Nominalper_IssuedCapital = objPrefrence.pshares.pIssuedCapital2;
                                    objsdtls.Nominalper_Paid_upCapital = objPrefrence.pshares.pPaidupCapital2;
                                    objsdtls.Nominalper_SubscribedCapital = objPrefrence.pshares.pSubscribedCapital2;
                                    objsdtls.totamt_AuhorisedCapital = objPrefrence.pshares.pAuthorisedCapital3;
                                    objsdtls.totamt_IssuedCapital = objPrefrence.pshares.pIssuedCapital3;
                                    objsdtls.totamt_upCapital = objPrefrence.pshares.pPaidupCapital3;
                                    objsdtls.totamt_SubscribedCapital = objPrefrence.pshares.pSubscribedCapital3;

                                    TAOEquityAuth = TAOEquityAuth + objPrefrence.pshares.pAuthorisedCapital3;
                                    TAOEquityIC = TAOEquityIC + objPrefrence.pshares.pIssuedCapital3;
                                    TAOEquitySC = TAOEquitySC + objPrefrence.pshares.pSubscribedCapital3;
                                    TAOEquityPUC = TAOEquityPUC + objPrefrence.pshares.pPaidupCapital3;

                                    objsdtls.Sharetype = "P";
                                    objsdtls.IsActive = true;
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    objsdtls.No_AuhorisedCapital = 0;
                                    objsdtls.No_IssuedCapital = 0;
                                    objsdtls.No_Paid_upCapital = 0;
                                    objsdtls.No_SubscribedCapital = 0;
                                    objsdtls.Nominalper_AuhorisedCapital = 0;
                                    objsdtls.Nominalper_IssuedCapital = 0;
                                    objsdtls.Nominalper_Paid_upCapital = 0;
                                    objsdtls.Nominalper_SubscribedCapital = 0;
                                    objsdtls.totamt_AuhorisedCapital = 0;
                                    objsdtls.totamt_IssuedCapital = 0;
                                    objsdtls.totamt_upCapital = 0;
                                    objsdtls.totamt_SubscribedCapital = 0;
                                    TAOEquityAuth = TAOEquityAuth + 0;
                                    TAOEquityIC = TAOEquityIC + 0;
                                    TAOEquitySC = TAOEquitySC + 0;
                                    TAOEquityPUC = TAOEquityPUC + 0;
                                    objsdtls.Sharetype = "P";
                                    objsdtls.IsActive = true;
                                    entities.SaveChanges();
                                }
                            }

                            else
                            {
                                BM_Share obj = new BM_Share();
                                obj.CapitalMasterId = (int)prifrenceDtls.Id;
                                obj.No_AuhorisedCapital = objPrefrence.pshares.pAuthorisedCapital1;
                                obj.No_IssuedCapital = objPrefrence.pshares.pIssuedCapital1;
                                obj.No_Paid_upCapital = objPrefrence.pshares.pPaidupCapital1;
                                obj.No_SubscribedCapital = objPrefrence.pshares.pSubscribedCapital1;
                                obj.Nominalper_AuhorisedCapital = objPrefrence.pshares.pAuthorisedCapital2;
                                obj.Nominalper_IssuedCapital = objPrefrence.pshares.pIssuedCapital2;
                                obj.Nominalper_Paid_upCapital = objPrefrence.pshares.pPaidupCapital2;
                                obj.Nominalper_SubscribedCapital = objPrefrence.pshares.pSubscribedCapital2;
                                obj.totamt_AuhorisedCapital = objPrefrence.pshares.pAuthorisedCapital3;
                                obj.totamt_IssuedCapital = objPrefrence.pshares.pIssuedCapital3;
                                obj.totamt_upCapital = objPrefrence.pshares.pPaidupCapital3;
                                obj.totamt_SubscribedCapital = objPrefrence.pshares.pSubscribedCapital3;

                                TAOEquityAuth = TAOEquityAuth + objPrefrence.pshares.pAuthorisedCapital3;
                                TAOEquityIC = TAOEquityIC + objPrefrence.pshares.pIssuedCapital3;
                                TAOEquitySC = TAOEquitySC + objPrefrence.pshares.pSubscribedCapital3;
                                TAOEquityPUC = TAOEquityPUC + objPrefrence.pshares.pPaidupCapital3;

                                obj.Sharetype = "P";
                                obj.IsActive = true;
                                obj.CustomerId = CustomerId;
                                entities.BM_Share.Add(obj);
                                entities.SaveChanges();
                            }
                            if (prifrenceDtls != null)
                            {
                                if (objPrefrence.IsPrefrence == true)
                                {
                                    prifrenceDtls.Pri_TotNoAuthorizedCapita = objPrefrence.pTnAuthorisedCapital;
                                    prifrenceDtls.Pri_TotNoIssuedCapital = objPrefrence.pTnIssuedCapital;
                                    prifrenceDtls.Pri_TotNoSubscribCapital = objPrefrence.pTnSubscribedCapital;
                                    prifrenceDtls.Pri_TotNoPaidupCapital = objPrefrence.pTnPaidupCapital;
                                    prifrenceDtls.Pri_TotamtAuthorizedCapita = TAOEquityAuth;
                                    prifrenceDtls.Pri_TotamtNoIssuedCapital = TAOEquityIC;
                                    prifrenceDtls.Pri_TotamtNoSubscribCapital = TAOEquitySC;
                                    prifrenceDtls.Pri_TotamtNoPaidupCapital = TAOEquityPUC;
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    prifrenceDtls.Pri_TotNoAuthorizedCapita = 0;
                                    prifrenceDtls.Pri_TotNoIssuedCapital = 0;
                                    prifrenceDtls.Pri_TotNoSubscribCapital = 0;
                                    prifrenceDtls.Pri_TotNoPaidupCapital = 0;
                                    prifrenceDtls.Pri_TotamtAuthorizedCapita = 0;
                                    prifrenceDtls.Pri_TotamtNoIssuedCapital = 0;
                                    prifrenceDtls.Pri_TotamtNoSubscribCapital = 0;
                                    prifrenceDtls.Pri_TotamtNoPaidupCapital = 0;
                                    entities.SaveChanges();
                                }
                            }
                            objPrefrence.pshares.pAuthorisedCapital3 = TAOEquityAuth;
                            objPrefrence.pshares.pIssuedCapital3 = TAOEquityIC;
                            objPrefrence.pshares.pSubscribedCapital3 = TAOEquitySC;
                            objPrefrence.pshares.pPaidupCapital3 = TAOEquityPUC;
                        }
                        objPrefrence.Message = true;
                        objPrefrence.SuccessErrorMsg = "Saved successfully";

                    }
                    else
                    {
                        objPrefrence.errorMessage = true;
                        objPrefrence.SuccessErrorMsg = "Somthing wents wrong";

                    }
                }

            }
            catch (Exception ex)
            {
                objPrefrence.errorMessage = false;
                objPrefrence.SuccessErrorMsg = "Server error Occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return objPrefrence;
        }
        #endregion

        #region Check -> Authorised + Prefenerance + Unclassified <= Total Authorised Capital
        public string CheckValidCapital(decimal? currentValue, string CapitalType, long CapitalId)
        {
            var result = "false";

            try
            {
                decimal? Authorised = 0, Equity = 0, Preference = 0, Unclasssified = 0;
                var Capital = (from row in entities.BM_CapitalMaster
                               where row.Id == CapitalId
                               select row).FirstOrDefault();

                if (Capital != null)
                {
                    Authorised = Capital.AuthorizedCapital;
                    if (CapitalType == "E")
                    {
                        Equity = currentValue;
                    }
                    else
                    {
                        Equity = Capital.TotamtAuthorizedCapita;
                    }

                    if (CapitalType == "P")
                    {
                        Preference = currentValue;
                    }
                    else
                    {
                        Preference = Capital.Pri_TotamtAuthorizedCapita;
                    }
                    if (CapitalType == "A")
                    {
                        Authorised = currentValue;
                    }
                    else
                    {
                        Authorised = Capital.AuthorizedCapital;

                    }
                    if (CapitalType == "U")
                    {
                        Unclasssified = currentValue;
                    }
                    else
                    {
                        Unclasssified = Capital.Unclassified_AuhorisedCapital;

                    }

                    //if (CapitalType != "U")
                    //    unclassified = Capital.Unclassified_AuhorisedCapital == 0 ? 0 : (decimal)Capital.Unclassified_AuhorisedCapital;
                    //+unclassified + currentValue
                    if (CapitalType == "E" || CapitalType == "P" || CapitalType == "A")
                    {
                        if (Authorised >= (Equity + Preference))
                        {
                            result = "true";
                        }
                        else
                        {
                            result = "false";
                        }
                    }
                    else if (CapitalType == "U")
                    {
                        if (Authorised >= Unclasssified)
                        {
                            result = "true";
                        }
                        else
                        {
                            result = "false";
                        }
                    }
                }
                else
                {
                    result = "Somthing wents wrong";
                }
            }
            catch (Exception ex)
            {
                result = "Server error Occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }
        #endregion

        #endregion Capital Master

        #region LLp Master

        public VMllpCapital EditCapitalLLP(int Id, int EntityId)
        {
            try
            {
                var _objgetllpdtls = (from row in entities.BM_LLpCapital_Master
                                      where row.Id == Id && row.EntityId == EntityId
                                      && row.IsActive == true
                                      select new VMllpCapital
                                      {
                                          Id = row.Id,
                                          DesignatedPartner_Id = (int)row.DesignatedPartnerId,
                                          Partner_Name = row.Partner_Name,
                                          Capital_Contribution = row.DePCapital_Contribution,
                                          Capital_ContributionforPartener = row.Capital_Contribution,
                                          DPIN_No = row.DPIN_No,
                                          IsDesiorPart = row.IsDPIN_No,
                                          PANNo = row.PAN
                                      }).FirstOrDefault();
                return _objgetllpdtls;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        //public List<VMllpCapital> GetCapitalLLPList(int EntityId)
        //{
        //    try
        //    {
        //        var _objgetllplist = (from row in entities.BM_LLpCapital_Master.
        //                              Where(x => x.IsActive == true && x.EntityId == EntityId)
        //                              from rows in entities.BM_DirectorMaster.Where(k => k.Id == row.DesignatedPartnerId).DefaultIfEmpty()
        //                              select new VMllpCapital
        //                              {
        //                                  Id = row.Id,
        //                                  DPIN_No = row.DPIN_No,
        //                                  Partner_Name = row.DesignatedPartnerId == 0 ? row.Partner_Name : rows.FirstName + " " + rows.LastName,
        //                                  Capital_Contribution = row.Capital_Contribution,
        //                                  IsDesiorPart = row.IsDPIN_No
        //                              }).ToList();
        //        return _objgetllplist;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        public List<VMllpCapital> GetCapitalLLPList(int EntityId)
        {
            try
            {
                var _objgetllplist = (from row in entities.BM_LLpCapital_Master.
                                      Where(x => x.IsActive == true && x.EntityId == EntityId)
                                      from rows in entities.BM_DirectorMaster.Where(k => k.Id == row.DesignatedPartnerId).DefaultIfEmpty()
                                      select new VMllpCapital
                                      {
                                          Id = row.Id,
                                          DPIN_No = row.DPIN_No,
                                          Partner_Name = row.DesignatedPartnerId == 0 ? row.Partner_Name : rows.FirstName + " " + rows.LastName,
                                          Capital_Contribution = row.DePCapital_Contribution,
                                          Capital_ContributionforPartener = row.Capital_Contribution,
                                          Capital = row.Capital_Contribution == 0 ? (decimal)row.DePCapital_Contribution : row.Capital_Contribution,
                                          PANNo = row.PAN,
                                          Address = row.Address,
                                          IsDesiorPart = row.IsDPIN_No
                                      }).ToList();
                return _objgetllplist;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMllpCapital AddLLpCapitalMaster(VMllpCapital _objllp, int customerId)
        {
            try
            {
                if (_objllp != null)
                {
                    if (_objllp.IsDesiorPart)
                    {
                        var _objcreatellpCapitalMasterpart = (from row in entities.BM_LLpCapital_Master
                                                              where row.Customer_Id == customerId
                                                              && row.IsActive == true
                                                              && row.Partner_Name == _objllp.Partner_Name
                                                              && row.PAN == _objllp.PANNo
                                                              select row).FirstOrDefault();
                        if (_objcreatellpCapitalMasterpart == null)
                        {
                            BM_LLpCapital_Master objllpcapital = new BM_LLpCapital_Master();
                            objllpcapital.DesignatedPartnerId = 0;
                            objllpcapital.DPIN_No = "0";
                            objllpcapital.Capital_Contribution = _objllp.Capital_ContributionforPartener;
                            objllpcapital.IsActive = true;
                            objllpcapital.EntityId = _objllp.Entity_Id;
                            objllpcapital.Customer_Id = customerId;
                            objllpcapital.PAN = _objllp.PANNo;
                            objllpcapital.Address = _objllp.Address;
                            objllpcapital.Partner_Name = _objllp.Partner_Name;
                            objllpcapital.IsDPIN_No = _objllp.IsDesiorPart;
                            objllpcapital.DePCapital_Contribution = 0;
                            entities.BM_LLpCapital_Master.Add(objllpcapital);
                            entities.SaveChanges();
                            _objllp.Message = true;
                            _objllp.SuccessErrorMsg = "Saved Successfully.";
                            return _objllp;
                        }
                        else
                        {
                            _objllp.errorMessage = true;
                            _objllp.SuccessErrorMsg = "Field Allredy Exist";
                            return _objllp;
                        }
                    }

                    else if (!_objllp.IsDesiorPart)
                    {
                        var _objcreatellpCapitalMaster = (from row in entities.BM_LLpCapital_Master
                                                          where row.Customer_Id == customerId
                                                          && row.IsActive == true
                                                          && row.DPIN_No == _objllp.DPIN_No
                                                          && row.DesignatedPartnerId == _objllp.DesignatedPartner_Id
                                                          select row).FirstOrDefault();
                        if (_objcreatellpCapitalMaster == null)
                        {
                            BM_LLpCapital_Master objllpcapital = new BM_LLpCapital_Master();
                            objllpcapital.DesignatedPartnerId = _objllp.DesignatedPartner_Id;
                            objllpcapital.DPIN_No = _objllp.DPIN_No;
                            objllpcapital.DePCapital_Contribution = _objllp.Capital_Contribution;
                            objllpcapital.Capital_Contribution = 0;
                            objllpcapital.IsActive = true;
                            objllpcapital.EntityId = _objllp.Entity_Id;
                            objllpcapital.Customer_Id = customerId;
                            //objllpcapital.PAN = "0";
                            //objllpcapital.Address = "0";
                            //objllpcapital.Partner_Name = "0";
                            entities.BM_LLpCapital_Master.Add(objllpcapital);
                            entities.SaveChanges();

                            _objllp.Message = true;
                            _objllp.SuccessErrorMsg = "Saved Successfully.";
                            return _objllp;

                        }
                        else
                        {
                            _objllp.errorMessage = true;
                            _objllp.SuccessErrorMsg = "Field Allredy Exist";
                            return _objllp;
                        }
                    }
                    else
                    {
                        _objllp.errorMessage = true;
                        _objllp.SuccessErrorMsg = "Field Allredy Exist";
                        return _objllp;
                    }

                }
                else
                {
                    _objllp.errorMessage = true;
                    _objllp.SuccessErrorMsg = "Somthing Wents Wrong";
                    return _objllp;

                }
            }
            catch (Exception ex)
            {
                _objllp.errorMessage = true;
                _objllp.SuccessErrorMsg = "Server error occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objllp;
            }
        }

        public VMllpCapital UpdateLLpCapitalMaster(VMllpCapital _objllp, int customerId)
        {
            try
            {
                if (_objllp != null)
                {
                    if (_objllp.IsDesiorPart)
                    {
                        var _objcreatellpCapitalMasterpart = (from row in entities.BM_LLpCapital_Master
                                                              where row.Customer_Id == customerId
                                                              && row.IsActive == true
                                                              && row.EntityId == _objllp.Entity_Id
                                                              && row.Id == _objllp.Id
                                                              //&& row.Partner_Name == _objllp.Partner_Name
                                                              //&& row.PAN == _objllp.PANNo
                                                              select row).FirstOrDefault();
                        if (_objcreatellpCapitalMasterpart != null)
                        {

                            _objcreatellpCapitalMasterpart.DesignatedPartnerId = 0;
                            _objcreatellpCapitalMasterpart.DPIN_No = "";
                            _objcreatellpCapitalMasterpart.Capital_Contribution = _objllp.Capital_ContributionforPartener;
                            _objcreatellpCapitalMasterpart.IsActive = true;
                            _objcreatellpCapitalMasterpart.EntityId = _objllp.Entity_Id;
                            _objcreatellpCapitalMasterpart.Customer_Id = customerId;
                            _objcreatellpCapitalMasterpart.PAN = _objllp.PANNo;
                            _objcreatellpCapitalMasterpart.Address = _objllp.Address;
                            _objcreatellpCapitalMasterpart.Partner_Name = _objllp.Partner_Name;
                            _objcreatellpCapitalMasterpart.IsDPIN_No = _objllp.IsDesiorPart;
                            _objcreatellpCapitalMasterpart.DePCapital_Contribution = 0;

                            entities.SaveChanges();
                            _objllp.Message = true;
                            _objllp.SuccessErrorMsg = "Updated  successfully";
                            return _objllp;
                        }
                        else
                        {
                            _objllp.errorMessage = true;
                            _objllp.SuccessErrorMsg = "Data not  Exist";
                            return _objllp;
                        }
                    }

                    else if (!_objllp.IsDesiorPart)
                    {
                        var _objcreatellpCapitalMaster = (from row in entities.BM_LLpCapital_Master
                                                          where row.Customer_Id == customerId
                                                          && row.IsActive == true
                                                          && row.EntityId == _objllp.Entity_Id
                                                          //&& row.DPIN_No == _objllp.DPIN_No
                                                          //&& row.DesignatedPartnerId == _objllp.DesignatedPartner_Id
                                                          && row.Id == _objllp.Id
                                                          select row).FirstOrDefault();
                        if (_objcreatellpCapitalMaster != null)
                        {

                            _objcreatellpCapitalMaster.DesignatedPartnerId = _objllp.DesignatedPartner_Id;
                            _objcreatellpCapitalMaster.DPIN_No = _objllp.DPIN_No;
                            _objcreatellpCapitalMaster.DePCapital_Contribution = _objllp.Capital_Contribution;
                            _objcreatellpCapitalMaster.Capital_Contribution = 0;
                            _objcreatellpCapitalMaster.IsActive = true;
                            _objcreatellpCapitalMaster.EntityId = _objllp.Entity_Id;
                            _objcreatellpCapitalMaster.Customer_Id = customerId;
                            //_objcreatellpCapitalMaster.PAN = "0";
                            // _objcreatellpCapitalMaster.Address = "0";
                            //  _objcreatellpCapitalMaster.Partner_Name = "0";

                            entities.SaveChanges();

                            _objllp.Message = true;
                            _objllp.SuccessErrorMsg = "Updated  successfully";
                            return _objllp;

                        }
                        else
                        {
                            _objllp.errorMessage = true;
                            _objllp.SuccessErrorMsg = "Data not found";
                            return _objllp;
                        }
                    }
                    else
                    {
                        _objllp.errorMessage = true;
                        _objllp.SuccessErrorMsg = "Somthing Wents Wrong";
                        return _objllp;
                    }

                }
                else
                {
                    _objllp.errorMessage = true;
                    _objllp.SuccessErrorMsg = "Somthing Wents Wrong";
                    return _objllp;

                }
            }
            catch (Exception ex)
            {
                _objllp.errorMessage = true;
                _objllp.SuccessErrorMsg = "Server error occure";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objllp;
            }
        }

        public IEnumerable<VMllpCapital> GetAllDINNumber(int customerId, int EntityId)
        {
            try
            {
                var ListofDin = (from row in entities.BM_Directors_DetailsOfInterest
                                 join rows in entities.BM_EntityMaster.Where(x => x.Entity_Type == 3)
                                 on row.EntityId equals rows.Id
                                 join rowss in entities.BM_DirectorMaster
                                 on row.Director_Id equals rowss.Id
                                 where rows.Is_Deleted == false && row.EntityId == EntityId

                                 select new VMllpCapital
                                 {
                                     Partner_Name = rowss.FirstName + " " + rowss.LastName,
                                     DPIN_No = rowss.DIN,
                                     Id = rowss.Id
                                 }).ToList();
                ListofDin.Insert(0, new VMllpCapital() { Id = 0, Partner_Name = "Select" });
                return ListofDin;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion LLp Master

        #region BodyCorporate
        public List<VMBodyCorporate> GetCapitalBodyCorporateList(int EntityId)
        {
            try
            {
                var _objbodyCorList = (from row in entities.BM_BodyCorporateCapital_Master
                                       where row.IsActive == true
                                       && row.EntityId == EntityId
                                       select new VMBodyCorporate
                                       {
                                           Id = row.Id,
                                           EntityId = row.EntityId,
                                           Security_Name = row.Security_Name,
                                           Securities_No = row.Security_No,
                                           Securities_Nomval = row.Security_NomValue,
                                           TotValSecurities = row.Security_totvalue,
                                           Currency = row.Currency,
                                           IslistedOnStockExchange = row.IsListed,

                                       }).ToList();
                return _objbodyCorList;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMBodyCorporate AddBodyCorporateCapitalMaster(VMBodyCorporate _objbodyCorporate, int customerId)
        {
            BM_BodyCorporateCapital_Master objbodycorporate = new BM_BodyCorporateCapital_Master();
            try
            {
                if (_objbodyCorporate.EntityId > 0)
                {
                    var _objCreateBodyCorporate = (from row in entities.BM_BodyCorporateCapital_Master
                                                   where row.Customer_Id == customerId

                                                   && row.Security_Name == _objbodyCorporate.Security_Name
                                                   && row.Security_NomValue == _objbodyCorporate.Securities_Nomval
                                                   && row.IsActive == true
                                                   select row).FirstOrDefault();
                    if (_objCreateBodyCorporate == null)
                    {

                        objbodycorporate.Security_No = _objbodyCorporate.Securities_No;
                        objbodycorporate.Security_Name = _objbodyCorporate.Security_Name;
                        objbodycorporate.Security_NomValue = _objbodyCorporate.Securities_Nomval;
                        objbodycorporate.Security_totvalue = _objbodyCorporate.TotValSecurities;
                        objbodycorporate.Currency = _objbodyCorporate.Currency;
                        objbodycorporate.IsListed = _objbodyCorporate.IslistedOnStockExchange;
                        objbodycorporate.EntityId = _objbodyCorporate.EntityId;
                        objbodycorporate.Customer_Id = customerId;

                        objbodycorporate.IsActive = true;
                        objbodycorporate.CreatedOn = DateTime.Now;
                        //objbodycorporate.Createdby =
                        entities.BM_BodyCorporateCapital_Master.Add(objbodycorporate);
                        entities.SaveChanges();
                        if (_objbodyCorporate.StockExchange != null)
                        {
                            BM_EntityStockMapping objMapping = new BM_EntityStockMapping();
                            if (objbodycorporate.Id > 0 && objbodycorporate.IsListed == true)
                            {
                                foreach (var item in _objbodyCorporate.StockExchange)
                                {
                                    objMapping.Cap_BodyCorporateId = objbodycorporate.Id;
                                    objMapping.StockExchangeId = item;
                                    objMapping.IsDeleted = false;
                                    entities.BM_EntityStockMapping.Add(objMapping);
                                    entities.SaveChanges();
                                }
                            }
                        }
                        _objbodyCorporate.Message = true;
                        _objbodyCorporate.SuccessErrorMsg = "Saved successfully";
                        return _objbodyCorporate;
                    }
                    else
                    {
                        _objbodyCorporate.errorMessage = true;
                        _objbodyCorporate.SuccessErrorMsg = "Data already Exist";
                        return _objbodyCorporate;
                    }

                }

                else
                {
                    _objbodyCorporate.errorMessage = true;
                    _objbodyCorporate.SuccessErrorMsg = "Somthing wents wrong";
                    return _objbodyCorporate;

                }
            }
            catch (Exception ex)
            {
                _objbodyCorporate.errorMessage = true;
                _objbodyCorporate.SuccessErrorMsg = "Server error occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objbodyCorporate;

            }
        }

        public string UpdatebodyCorporateCapitalMaster(VMBodyCorporate _objbodyCorporate, int customerId)
        {
            throw new NotImplementedException();
        }

        public VMBodyCorporate EditCapitalBodyCorporate(int id, int entityId)
        {
            try
            {
                var getbodycorporatedtls = (from row in entities.BM_BodyCorporateCapital_Master
                                            where row.Id == id
                                            && row.EntityId == entityId && row.IsActive == true
                                            select new VMBodyCorporate
                                            {
                                                Id = row.Id,
                                                EntityId = row.EntityId,
                                                Securities_No = row.Security_No,
                                                Security_Name = row.Security_Name,
                                                Securities_Nomval = row.Security_NomValue,
                                                TotValSecurities = row.Security_totvalue,
                                                IslistedOnStockExchange = row.IsListed,
                                                Currency = row.Currency,
                                                StockExchange = entities.BM_EntityStockMapping.Where(x => x.Cap_BodyCorporateId == id).Select(x => x.StockExchangeId).ToList()
                                            }).FirstOrDefault();
                return getbodycorporatedtls;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMBodyCorporate Update_BodyCorporateCapitalMaster(VMBodyCorporate _objbodyCorporate, int customerId)
        {

            BM_BodyCorporateCapital_Master objbodycorporate = new BM_BodyCorporateCapital_Master();
            try
            {
                if (_objbodyCorporate.EntityId > 0)
                {
                    var _objCreateBodyCorporate = (from row in entities.BM_BodyCorporateCapital_Master
                                                   where row.Customer_Id == customerId
                                                   && row.EntityId == _objbodyCorporate.EntityId
                                                  && row.Id == _objbodyCorporate.Id
                                                   && row.IsActive == true
                                                   select row).FirstOrDefault();
                    if (_objCreateBodyCorporate != null)
                    {

                        _objCreateBodyCorporate.Security_No = _objbodyCorporate.Securities_No;
                        _objCreateBodyCorporate.Security_Name = _objbodyCorporate.Security_Name;
                        _objCreateBodyCorporate.Security_NomValue = _objbodyCorporate.Securities_Nomval;
                        _objCreateBodyCorporate.Security_totvalue = _objbodyCorporate.TotValSecurities;
                        _objCreateBodyCorporate.Currency = _objbodyCorporate.Currency;
                        _objCreateBodyCorporate.IsListed = _objbodyCorporate.IslistedOnStockExchange;
                        _objCreateBodyCorporate.EntityId = _objbodyCorporate.EntityId;
                        _objCreateBodyCorporate.Customer_Id = customerId;
                        _objCreateBodyCorporate.IsActive = true;
                        _objCreateBodyCorporate.CreatedOn = DateTime.Now;
                        _objCreateBodyCorporate.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        //if (_objbodyCorporate.StockExchange != null)
                        //{
                        //    BM_EntityStockMapping objMapping = new BM_EntityStockMapping();


                        //    if (_objbodyCorporate.Id > 0 && _objbodyCorporate.IslistedOnStockExchange == true)
                        //    {
                        //        foreach (var item in _objbodyCorporate.StockExchange)
                        //        {
                        //            var objstockdata = (from row in entities.BM_EntityStockMapping
                        //                                where row.Cap_BodyCorporateId == _objCreateBodyCorporate.Id 

                        //                                && row.IsDeleted == false
                        //                                select row).FirstOrDefault();
                        //    foreach (var items in _objbodyCorporate.StockExchange)
                        //    {
                        //        if (objstockdata.StockExchangeId !=items)
                        //        {
                        //            objstockdata.IsDeleted = true;
                        //            entities.SaveChanges();
                        //        }
                        //        else
                        //        {
                        //            objstockdata.IsDeleted = false;
                        //            entities.SaveChanges();
                        //        }
                        //    }
                        //}


                        // }
                        // }
                        _objbodyCorporate.Message = true;
                        _objbodyCorporate.SuccessErrorMsg = "Updated  successfully";
                        return _objbodyCorporate;
                    }
                    else
                    {
                        _objbodyCorporate.errorMessage = true;
                        _objbodyCorporate.SuccessErrorMsg = "No data found";
                        return _objbodyCorporate;
                    }

                }

                else
                {
                    _objbodyCorporate.errorMessage = true;
                    _objbodyCorporate.SuccessErrorMsg = "Somthing wents wrong";
                    return _objbodyCorporate;

                }
            }
            catch (Exception ex)
            {
                _objbodyCorporate.errorMessage = true;
                _objbodyCorporate.SuccessErrorMsg = "Server error occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objbodyCorporate;

            }
        }

        public string getEntityName(int entityId)
        {
            try
            {
                var getentityName = (from row in entities.BM_EntityMaster
                                     where row.Id == entityId
                                     && row.Is_Deleted == false
                                     select row.CompanyName).FirstOrDefault();
                return getentityName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server error ocuured";
            }
        }

        #endregion

        //public List<EntityCpitalDetails> GetEntityShareCapitaldetails(int EntityId)
        //{
        //    List<EntityCpitalDetails> capitaldetails = new List<EntityCpitalDetails>();
        //    try
        //    {
        //        capitaldetails = (from row in entities.BM_CapitalMaster
        //                          join rows in entities.BM_Debentures
        //                          on row.Id equals rows.CapitalId
        //                          where row.Entity_Id == EntityId
        //                          select new EntityCpitalDetails
        //                          {
        //                              AuthorizedCapital = row.AuthorizedCapital,

        //                              PaidupCapital = row.Pri_TotamtNoPaidupCapital + row.TotamtNoPaidupCapital,
        //                              EquietyCapital = row.TotamtNoPaidupCapital,
        //                              PrefrenceCapital = row.Pri_TotamtNoPaidupCapital,
        //                              Othersecurities = row.Unclassified_AuhorisedCapital,
        //                              EquietyNominalVal = (from x in entities.BM_Share where x.CapitalMasterId == row.Id && x.Sharetype == "E" select x.Nominalper_AuhorisedCapital).FirstOrDefault(),
        //                              PrefrenceNominalVal = (from x in entities.BM_Share where x.CapitalMasterId == row.Id && x.Sharetype == "P" select x.Nominalper_AuhorisedCapital).FirstOrDefault(),
        //                              DebentureCapital = rows.Total,
        //                              DebencuteNominalVal = rows.Nom_FullyConDebperUnit,
        //                          }).ToList();

        //        foreach (var item in capitaldetails)
        //        {
        //            item.AuthorizedCapitall = item.AuthorizedCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
        //            item.PaidupCapitall = item.PaidupCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
        //            item.EquietyCapitall = item.EquietyCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
        //            item.PrefrenceCapitall = item.PrefrenceCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
        //            item.Debenture = item.DebentureCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

        //    }
        //    return capitaldetails;
        //}

        public List<EntityCpitalDetails> GetEntityShareCapitaldetails(int EntityId)
        {
            List<EntityCpitalDetails> capitaldetails = new List<EntityCpitalDetails>();
            try
            {
                capitaldetails = (from row in entities.BM_CapitalMaster
                                      //join rows in entities.BM_Debentures
                                      //on row.Id  equals rows.CapitalId
                                  where row.Entity_Id == EntityId
                                  select new EntityCpitalDetails
                                  {
                                      AuthorizedCapital = row.AuthorizedCapital,
                                      capitalId = row.Id,
                                      PaidupCapital = row.Pri_TotamtNoPaidupCapital + row.TotamtNoPaidupCapital,
                                      EquietyCapital = row.TotamtNoPaidupCapital,
                                      PrefrenceCapital = row.Pri_TotamtNoPaidupCapital,
                                      Othersecurities = row.Unclassified_AuhorisedCapital,
                                      EquietyNominalVal = (from x in entities.BM_Share where x.CapitalMasterId == row.Id && x.Sharetype == "E" select x.Nominalper_AuhorisedCapital).FirstOrDefault(),
                                      PrefrenceNominalVal = (from x in entities.BM_Share where x.CapitalMasterId == row.Id && x.Sharetype == "P" select x.Nominalper_AuhorisedCapital).FirstOrDefault(),
                                  }).ToList();

                if (capitaldetails.Count > 0)
                {
                    foreach (var item in capitaldetails)
                    {
                        item.DebentureCapital = (from y in entities.BM_Debentures where y.CapitalId == item.capitalId select y.Total).FirstOrDefault();
                        item.DebencuteNominalVal = (from y in entities.BM_Debentures where y.CapitalId == item.capitalId select y.Nom_FullyConDebperUnit).FirstOrDefault();
                        item.AuthorizedCapitall = item.AuthorizedCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
                        item.PaidupCapitall = item.PaidupCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
                        item.EquietyCapitall = item.EquietyCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
                        item.PrefrenceCapitall = item.PrefrenceCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
                        if (item.DebentureCapital > 0)
                        {
                            item.Debenture = Convert.ToDecimal(item.DebentureCapital).ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
                        }
                        else
                        {
                            item.Debenture = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return capitaldetails;
        }

        public decimal CalculatePaidupCapitalinAmount(long id)
        {
            long equitypaidup = 0;
            long paidupcaital = 0;
            long PrefrenceCapital = 0;
            try
            {
                var getcapitalDetails = (from row in entities.BM_CapitalMaster where row.Id == id select row).FirstOrDefault();
                if (getcapitalDetails != null)
                {
                    equitypaidup = (from x in entities.BM_Share where x.CapitalMasterId == id && x.Sharetype == "E" select x.No_Paid_upCapital).FirstOrDefault();
                    paidupcaital = (from x in entities.BM_Share where x.CapitalMasterId == id && x.Sharetype == "P" select x.No_Paid_upCapital).FirstOrDefault();
                    PrefrenceCapital = equitypaidup + paidupcaital;
                }
                else
                {
                    PrefrenceCapital = 0;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return PrefrenceCapital;
        }
    }
}