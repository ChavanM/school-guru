﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface ICommitteeComp
    {
        CommitteeCompVM GetCommitteeComp(int id);
        List<CommitteeCompVM> GetAllCommitteeCompList(int customerID);
        IEnumerable<CommitteeCompVM> GetAllCommitteeComp(int? customerID);
        IEnumerable<CommitteeCompVM> GetAllCommitteeCompForMaster(int? customerID);
        CommitteeCompVM CreateOrUpdate(CommitteeCompVM obj);
        CommitteeCompVM Update(CommitteeCompVM obj);
        bool Delete(int id);

        bool CheckAsPerStandard(int CommitteeId, int? CustomerId, int? EntityId);
        Composition GetComposition(int id, int CommitteeId);
        List<Composition> GetAllComposition(int CommitteeId, int EntityType, string RuleType, int? CustomerId, int? EntityId);
        Composition CreateRule(Composition obj);
        Composition UpdateRule(Composition obj);

        bool DeleteRule(int id, int CommitteeId, int UserId);
        MeetingDetails GetMeetingRule(int CommitteeId, int EntityType, int? CustomerId, int? EntityId);
        MeetingDetails CreateOrUpdate(MeetingDetails obj);

        bool CopyRules(int CommitteeId, int CustomerId, int EntityId, int UserId);

        IEnumerable<CommitteeCompVM> GetAllMeetingType();
        IEnumerable<CommitteeCompVM> GetAll_MeetingTypeByCustomer(int customerID, bool showAll);
        IEnumerable<CommitteeCompVM> GetAllMeetingType(int customerID, bool showAll, string role);

        IEnumerable<CommitteeCompVM> GetMeetingTypes(string Type, int customerID, int entityID, bool showGM);
        List<CommitteeCompVM> GetCommitteeCompEntitywise(long entityId, int customerId);
    }
}
