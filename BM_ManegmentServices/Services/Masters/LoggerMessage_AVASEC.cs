﻿using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.Services.Masters
{
    public class LoggerMessage_AVASEC
    {
        
        public static void InsertLog(Exception ex, string ClassName, string FunctionName)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_LogMessage obj = new BM_LogMessage();
                    obj.LogLevel = 0;
                    obj.ClassName = ClassName;
                    obj.FunctionName = FunctionName;
                    if(ex !=null)
                    {
                        obj.Message = ex.Message + "----\r\n" + ex.InnerException;
                        obj.StackTrace = ex.StackTrace;
                    }
                    else
                    {
                        obj.Message = ClassName;
                        obj.StackTrace = ClassName;
                    }
                    obj.CreatedOn = DateTime.Now;

                    entities.BM_LogMessage.Add(obj);
                    entities.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void InsertErrorMsg_DBLog(string errorMessage, string ClassName, string FunctionName)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_LogMessage msg = new BM_LogMessage()
                    {
                        LogLevel = 0,
                        ClassName = ClassName,
                        FunctionName = FunctionName,
                        Message = errorMessage,
                        CreatedOn = DateTime.Now,
                    };

                    entities.BM_LogMessage.Add(msg);
                    entities.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
