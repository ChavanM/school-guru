﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IConfiguration
    {
        VMConfiguration CreateMeeting(VMConfiguration _objmeeting);
        VMConfiguration UpdateMeeting(VMConfiguration _objmeeting);
        List<VMConfiguration> getMeetingConfiguration(int EntityId, int custID);
        List<VMConfiguration> getCircularConfiguration();
        VMConfiguration GetMeetingbyId(int Id);
       
        IEnumerable<CommitteeCompVM> GetAllMeetingType(int entityId);
        List<VMConfiguration> getConfiguration(int entityId);
        List<VMConfiguration> lastCircular(int entityId);
        List<YearMasterVM> GetAll_FYByEntityID(int entityID);

        bool? GetIsDefaultVirtualMeetingByEntityId(int EntityID);
        List<YearMasterVM> GetAll_FYByEntityIDForHistorical(int EntityID);
    }
}
