﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services
{
    public class SecretarialManagement
    {
        public static List<BM_SP_GetCustomerConfiguration_Result> GetCustomers_Secretarial()
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var customers = (from row in entities.BM_SP_GetCustomerConfiguration()                                 
                                 select row);

                return customers.ToList();
            }
        }
        public static List<object> GetAll_MyCustomers(int userID, int customerID, int serviceProviderID, int distributorID, string roleCode, bool showSelf)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    List<object> customers = new List<object>();
                    var lstCustomers = (from row in entities.BM_SP_GetMyCustomers(userID, roleCode, customerID, distributorID, serviceProviderID, 8)
                                        select row).ToList();

                    if (!showSelf)
                        lstCustomers = lstCustomers.Where(row => row.ID != distributorID).ToList();

                    if (lstCustomers.Count > 0)
                    {
                        foreach (var customerInfo in lstCustomers)
                        {
                            customers.Add(new
                            {
                                customerInfo.ID,
                                customerInfo.Name,
                                customerInfo.Address,
                                customerInfo.BuyerName,
                                customerInfo.BuyerContactNumber,
                                customerInfo.BuyerEmail,
                                customerInfo.CreatedOn,
                                customerInfo.IsDeleted,
                                customerInfo.StartDate,
                                customerInfo.EndDate,
                                customerInfo.DiskSpace,
                                customerInfo.LogoPath,
                                IsServiceProvider = customerInfo.IsServiceProvider != null ? ((Convert.ToBoolean(customerInfo.IsServiceProvider) == true) ? "Yes" : "No") : "No",
                                //DistributorOrCustomer = customerInfo.IsDistributor != null ? ((Convert.ToBoolean(customerInfo.IsDistributor) == true) ? "Distributor" : "Customer") : "Customer",
                                DistributorOrCustomer = customerInfo.IsDistributor != null ? ((Convert.ToBoolean(customerInfo.IsDistributor) == true) ? "Professional Firm" : "Customer") : "Customer",
                                ServiceProvider = customerInfo.ServiceProviderID != null ? ((Convert.ToInt32(customerInfo.ServiceProviderID) == 94) ? "TeamLease" : "Avantis") : "",
                                Status = customerInfo.Status != null ? ((Convert.ToInt32(customerInfo.Status) == 0) ? "InActive" : "Active") : "",
                            });
                        }
                    }

                    return customers;
                }
            }
            catch (Exception ex)            
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        public static List<BM_SP_GetMyCustomerEntities_Result> GetAll_MyCustomerEntities(int userID, int customerID, int serviceProviderID, int distributorID, string roleCode, bool showSelf)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var lstCustomers = (from row in entities.BM_SP_GetMyCustomerEntities(userID, roleCode, customerID, distributorID, serviceProviderID, 8)
                                        select row).ToList();

                    if (!showSelf)
                        lstCustomers = lstCustomers.Where(row => row.CustomerID != distributorID).ToList();

                    return lstCustomers;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<BM_SP_EntityConfigurationCustomerWise_Result> GetEntityConfigurationCustomerWise(int customerID)
        {
            List<BM_SP_EntityConfigurationCustomerWise_Result> lstConfigurations = new List<BM_SP_EntityConfigurationCustomerWise_Result>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    lstConfigurations = (from row in entities.BM_SP_EntityConfigurationCustomerWise(customerID)
                                         select row).ToList();

                    return lstConfigurations;
                }
            }
            catch (Exception ex)
            {
                return lstConfigurations;
            }
        }

        public static bool DeActive_EntityConfigurationCustomerWise(int customerID)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var prevSettings = entities.BM_EntityTypeCustomerWiseConfiguration.Where(o => o.CustomerID == customerID).ToList();

                    if (prevSettings.Count > 0)
                    {
                        prevSettings.ForEach(row => row.IsActive = false);
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool CreateUpdate_EntityTypeSetting(int custID, int entityTypeID, int value, int userID)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var result = entities.BM_EntityTypeCustomerWiseConfiguration.FirstOrDefault(o => o.CustomerID == custID
                                          && o.EntityTypeID == entityTypeID);

                    if (result == null)
                    {
                        BM_EntityTypeCustomerWiseConfiguration newMapping = new BM_EntityTypeCustomerWiseConfiguration();
                        newMapping.CustomerID = custID;
                        newMapping.EntityTypeID = entityTypeID;
                        newMapping.AllowedValue = value;

                        newMapping.IsActive = true;
                        newMapping.CreatedBy = userID;
                        newMapping.CreatedOn = DateTime.Now;

                        entities.BM_EntityTypeCustomerWiseConfiguration.Add(newMapping);
                    }
                    else
                    {
                        result.CustomerID = custID;
                        result.EntityTypeID = entityTypeID;
                        result.AllowedValue = value;


                        result.IsActive = true;
                        result.UpdatedBy = userID;
                        result.UpdatedOn = DateTime.Now;
                    }

                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool DeleteCustomer(int custID)
        {
            try
            {
                if (custID > 0)
                {
                    //CustomerManagement.Delete(custID);
                    //CustomerManagementRisk.Delete(custID);

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static List<BM_SP_GetAllUserCustomerMapping_Result> GetAll_UserCustomerMapping(int distID, int prodID)
        {
            List<BM_SP_GetAllUserCustomerMapping_Result> UserCustomerMappingList = new List<BM_SP_GetAllUserCustomerMapping_Result>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    UserCustomerMappingList = (entities.BM_SP_GetAllUserCustomerMapping(prodID, distID)).OrderBy(row => row.CustomerName).ToList();
                }

                return UserCustomerMappingList;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public static List<Customer> GetAll_MyCustomers(int customerID, int serviceProviderID, int distributorID, bool showSubDist, string filter = null)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var customers = (from row in entities.Customers
                                 where row.IsDeleted == false
                                 select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    customers = customers.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter));
                }

                if (serviceProviderID != -1)
                    customers = customers.Where(entry => entry.ServiceProviderID == serviceProviderID || entry.ID == serviceProviderID);

                if (distributorID != -1)
                {
                    if (!showSubDist)
                        customers = customers.Where(entry => (entry.ParentID == distributorID && entry.IsDistributor == false) || entry.ID == distributorID);
                    else
                        customers = customers.Where(entry => entry.ParentID == distributorID || entry.ID == distributorID);
                }

                if (customerID != -1)
                    customers = customers.Where(entry => entry.ID == customerID);

                if (customers.Count() > 0)
                    customers = customers.OrderBy(row => row.Name);

                return customers.ToList();
            }
        }

        public static List<BM_SP_GetAllUser_ServiceProviderDistributor_Result> GetAll_Users(int customerID, int serviceProviderID, int distributorID)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var lstRecords = (entities.BM_SP_GetAllUser_ServiceProviderDistributor(serviceProviderID, distributorID, customerID)).ToList();

                List<string> secRoleCodes = new List<string> { "HDCS", "CS", "CSMGR", "CEXCT", "DADMN" };

                if (lstRecords.Count > 0 && secRoleCodes.Count > 0)
                {
                    lstRecords = lstRecords.Where(row => secRoleCodes.Contains(row.RoleCode)).ToList();
                }

                return lstRecords;
            }
        }

        //public static bool Delete_UserCustomerMapping(int recordID)
        //{
        //    try
        //    {
        //        if (recordID > 0)
        //        {
        //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //            {
        //                var mappingRecord = entities.UserCustomerMappings.Where(row => row.ID == recordID).FirstOrDefault();

        //                if(mappingRecord!=null)
        //                {
        //                    mappingRecord.IsActive = false;
        //                    entities.SaveChanges();
        //                }
        //            }

        //            return true;
        //        }
        //        else
        //            return false;
        //    }
        //    catch (Exception ex)
        //    {

        //        return false;
        //    }
        //}

        public static List<int> GetAll_AssignedCustomerIDs(int userID, int customerID, int serviceProviderID, int distributorID, string roleCode, bool showSelf)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                List<int> customers = new List<int>();
                var lstCustomers = (from row in entities.BM_SP_GetMyCustomers(userID, roleCode, customerID, distributorID, serviceProviderID, 8)
                                    select row).ToList();

                if (lstCustomers.Count > 0)
                {
                    customers = lstCustomers.Select(row => row.ID).ToList();
                }

                return customers;
            }
        }

        public static bool SetCustomerConfiguration(int customerID, int userID, bool accesstoDirector, bool defaultVirtualMeeting, bool videoConferencingAppl)
        {
            try
            {

                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var custRecord = (from row in entities.BM_CustomerConfiguration
                                      where row.CustomerID == customerID
                                      select row).FirstOrDefault();

                    if (custRecord != null)
                    {
                        custRecord.SentMailtoDirector = accesstoDirector;
                        custRecord.DefaultVirtualMeeting = defaultVirtualMeeting;
                        custRecord.IsActive = false;
                        custRecord.VideoConferencingApplicable = videoConferencingAppl;

                        custRecord.UpdatedBy = userID;
                        custRecord.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                    }
                    else
                    {
                        var newRecord = new BM_CustomerConfiguration
                        {
                            CustomerID = customerID,
                            DefaultVirtualMeeting = defaultVirtualMeeting,
                            SentMailtoDirector = accesstoDirector,
                            VideoConferencingApplicable = videoConferencingAppl,
                            CreatedBy = userID,
                            CreatedOn = DateTime.Now,
                            IsActive = true,
                        };

                        entities.BM_CustomerConfiguration.Add(newRecord);
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateTransaction_Secretarial(long scheduleOnID, int statusID, DateTime dated, int userID)
        {
            bool saveSuccess = false;

            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var prevRecord = (from row in entities.BM_ComplianceScheduleOnNew
                                  where row.ScheduleOnID == scheduleOnID
                                  select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        long? tempMeetingId = null;
                        DateTime closedDate = dated;
                        if (!DateTime.TryParse(dated.ToString("dd/MMM/yyyy"), out closedDate))
                        {
                            closedDate = dated;
                        }

                        prevRecord.StatusId = statusID;

                        if (statusID == 4 || statusID == 5)
                        {
                            #region Set Open Agenda On Compliance Completion

                            var OnComplianceCompletion = (from row in entities.ComplianceScheduleOns
                                                          join instance in entities.ComplianceInstances on row.ComplianceInstanceID equals instance.ID
                                                          join mapping in entities.BM_ComplianceMapping on instance.ComplianceId equals mapping.ComplianceID
                                                          join agenda in entities.BM_AgendaMaster on mapping.AgendaMasterID equals agenda.BM_AgendaMasterId
                                                          where row.ID == scheduleOnID
                                                          && agenda.FrequencyId == 15
                                                          select new
                                                          {
                                                              //row.EntityId,
                                                              agenda.BM_AgendaMasterId,
                                                              agenda.EntityType,
                                                              agenda.MeetingTypeId
                                                          }).FirstOrDefault();

                            if (OnComplianceCompletion != null)
                            {
                                var openAgenda = new BM_MeetingOpenAgendaOnCompliance()
                                {
                                    Id = 0,
                                    ScheduleOnID = scheduleOnID,
                                    EntityId = prevRecord.EntityId != null ? (int)prevRecord.EntityId : 0,
                                    AgendaId = OnComplianceCompletion.BM_AgendaMasterId,
                                    MeetingTypeId = (int)OnComplianceCompletion.MeetingTypeId,
                                    IsNoted = false,
                                    IsDeleted = false,
                                    CreatedBy = userID,
                                    CreatedOn = DateTime.Now
                                };

                                entities.BM_MeetingOpenAgendaOnCompliance.Add(openAgenda);
                                entities.SaveChanges();
                            }
                            #endregion

                            prevRecord.ClosedDate = closedDate;

                            tempMeetingId = prevRecord.MeetingID;
                        }
                        entities.SaveChanges();

                        if (tempMeetingId > 0)
                        {
                            entities.BM_SP_ComplianceTransactionOnClosed(tempMeetingId, userID);
                            entities.SaveChanges();
                        }

                        saveSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                saveSuccess = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return saveSuccess;
        }        
    }
}
