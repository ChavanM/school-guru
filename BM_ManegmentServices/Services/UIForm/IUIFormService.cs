﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.UIForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.UIForm
{
    public interface IUIFormService
    {
        #region Appointment of CEO/CFO/CS/Manager 
        UIForm_DirectorMasterVM AppointmentOfManager(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterVM AppointmentOfCS(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterVM AppointmentOfCEO(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterVM AppointmentOfCFO(long meetingId, long agendaId, long mappingId);
        UIForm_KMPMasterVM SaveAppointmentOfCFO(UIForm_KMPMasterVM obj, int userId, int customerId);
        #endregion

        #region Cessation of CEO/CFO/CS/Manager
        UIForm_DirectorMasterCessationVM CessationOfManager(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterCessationVM CessationOfCS(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterCessationVM CessationOfCEO(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterCessationVM CessationOfCFO(long meetingId, long agendaId, long mappingId);
        UIForm_KMPMasterCessationVM SaveCessationOfKMP(UIForm_KMPMasterCessationVM obj, int userId, int customerId);
        #endregion

        #region Appointment of Director
        UIForm_DirectorMasterVM AppointmentOfMD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterVM AppointmentOfWTD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterVM AppointmentOfADDD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterVM AppointmentOfADDD_Independant(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterVM AppointmentOfCAVD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterVM AppointmentOfNOMD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterVM AppointmentOfALTD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterVM SaveAppointmentOfDirector(UIForm_DirectorMasterVM obj, int userId, int customerId);
        #endregion

        #region Cessation of Director
        UIForm_DirectorMasterCessationVM CessationOfMD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterCessationVM CessationOfWTD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterCessationVM CessationOfADDD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterCessationVM CessationOfCAVD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterCessationVM CessationOfNOMD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterCessationVM CessationOfALTD(long meetingId, long agendaId, long mappingId);
        UIForm_DirectorMasterCessationVM SaveCessationOfDirector(UIForm_DirectorMasterCessationVM obj, int userId, int customerId);
        #endregion
        #region Appointment of Auditor
        VMInternalAuditor AppointmentOFInternalAuditor(long meetingId, long agendaId, long mappingId);
        VMInternalAuditor SaveAppointmentOfInternalAuditor(VMInternalAuditor obj, int userId, int customerId);
        VM_SecreterialAuditor AppointmentOfSecretarialAuditor(long meetingId, long agendaId, long mappingId);
        VM_SecreterialAuditor SaveAppointmentOfSecretarialAuditor(VM_SecreterialAuditor obj, int userId, int customerId);
        #endregion

        #region Statutory Auditor
        VMStatutory_Auditor AppointmentOfFirstStatutoryAuditor(long meetingId, long agendaId, long mappingId, int customerId);
        VMStatutory_Auditor AppointmentOfStatutoryAuditorInCaseOfCasualVacancy(long meetingId, long agendaId, long mappingId, int customerId);
        VMStatutory_Auditor AppointmentOfStatutoryAuditor(long meetingId, long agendaId, long mappingId, int natureOfAppointment, int customerId);
        VMStatutory_Auditor SaveAppointmentOfStatutoryAuditor(VMStatutory_Auditor obj, int userId, int customerId);
        #endregion

        #region Cost Auditor
        VM_CostAuditor AppointmentOfOriginalCostAuditor(long meetingId, long agendaId, long mappingId);
        VM_CostAuditor SaveAppointmentOfCostAuditor(VM_CostAuditor obj, int userId, int customerId);

        VM_CostAuditorRatification RatificationCostAuditor(long meetingId, long agendaId, long mappingId);
        VM_CostAuditorRatification SaveRatificationCostAuditor(VM_CostAuditorRatification obj, int userId, int customerId);
        #endregion

        #region Resignation of Auditor
        Auditor_ResignationVM ResignationOfAuditor(long meetingId, long agendaId, long mappingId, long uiFormId);
        Auditor_ResignationVM SaveResignationOfAuditor(Auditor_ResignationVM obj, int userId, int customerId);

        UIForm_AuditorResignationVM ResignationOfAuditorNew(long meetingId, long agendaId, long mappingId, long uiFormId);
        UIForm_AuditorResignationVM SaveResignationDetailsOfAuditor(UIForm_AuditorResignationVM obj, int userId, int customerId);
        #endregion

        #region Financial Result
        UIForm_FinancialResult_ListedVM FinancialResultListed(long meetingId, long agendaId, long mappingId, int EntityTypeId);
        UIForm_FinancialResult_ListedVM SaveFinancialResultListed(UIForm_FinancialResult_ListedVM obj, int userId, int customerId);

        UIForm_FinancialResultYearEnded_PrivateVM FinancialResultForYearEnded_Private(long meetingId, long agendaId, long mappingId, int entityTypeId);
        UIForm_FinancialResultYearEnded_PrivateVM SaveFinancialResultForYearEnded_Private(UIForm_FinancialResultYearEnded_PrivateVM obj, int userId, int customerId);

        UIForm_FinancialResultYearEnded_ListedVM FinancialResultForYearEnded_Listed(long meetingId, long agendaId, long mappingId, int entityTypeId);
        UIForm_FinancialResultYearEnded_ListedVM SaveFinancialResultForYearEnded_Listed(UIForm_FinancialResultYearEnded_ListedVM obj, int userId, int customerId);
        #endregion

        #region Calling GM
        UIForm_Calling_GeneralMeetingVM CallingGeneralMeeting(long meetingId, long agendaId, long mappingId);
        UIForm_Calling_GeneralMeetingVM SaveCallingGeneralMeeting(UIForm_Calling_GeneralMeetingVM obj, int userId, int customerId);
        #endregion

        #region Books of Accoutns
        EntityMasterBooksOfAccountsVM BooksOfAccoutns(long meetingId, long agendaId, long mappingId);
        EntityMasterBooksOfAccountsVM SaveBooksOfAccoutns(EntityMasterBooksOfAccountsVM obj, int userId);
        #endregion
    }
}
