﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.DocumentManagenemt
{
    public interface IFileData_Service
    {
        int GetFileVersion(long scheduleOnID, string fileName);
        FileDataVM Save(FileDataVM obj, int createdBy);

        Message CreateFileDataMapping(long transactionID, long scheduleOnID, long fileID, Int16 fileType);

        FileDataVM GetFile(long fileID, int userId);

        FileDataVM SaveSecretarialDocs(FileDataVM obj, int userId);
        List<FileDataVM> GetAllSecretarialDocs(long meetingId, string docs);
        FileDataVM GetSecretarialDocs(long fileID);
        Message DeleteSecretarialDocs(long fileId, long meetingId, string docs, int userId);
        Message DeleteSecretarialDocs(long fileId, long meetingAgendaMappingId, long meetingId, int userId);
        string GetSecretarialFile(long fileID, int userId, int customerId);
        byte[] GetEULAFile(string filePath);
        VM.EULA.EULA_DetailsVM SaveEULAFile(VM.EULA.EULA_DetailsVM obj);
        byte[] ReadDocFiles(string filePath);
    }
}
