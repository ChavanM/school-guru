﻿using BM_ManegmentServices.VM.DocumentManagenemt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.DocumentManagenemt
{
    public interface IDocument_Service
    {
        byte[] GenerateAgendaDocument(long meetingId, long? taskAssignmentId, int userId, int customerId, bool generateNew);
        string GenerateAgendaDocument1(long meetingId, long? taskAssignmentId, int userId, int customerId, bool generateNew);

        string GetMinutesDocumentFile(long meetingId, long? taskAssignmentId, int userId, int customerId, bool forceGenerateNewDocument, bool generatePDF, string generateFor);
        byte[] GetMinutesDocumentData(long meetingId, long? taskAssignmentId, int userId, int customerId, bool forceGenerateNewDocument, bool generatePDF, string generateFor);
    }
}
