﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BM_ManegmentServices.Services.DocumentManagenemt
{
    public interface IFileStorage_Service
    {
        FileStorageDataVM UploadFile(FileStorageDataVM obj, long meetingId, int customerId);
        FileDataVM GetFileDetails(long fileId);
        List<FileDataVM> GetUploadedFilesByMeetingId(long meetingId);
    }
}
