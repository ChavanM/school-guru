﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.DocumentManagenemt
{
    public interface IDocumentSetting_Service
    {
        DocumentSettingVM GetDocumentSetting(int customerId, string DocumentType);

        List<PageHeaderFooterSettingListVM> GetPageHeaderFooterEntityList(int customerId);
        PageHeaderFooterSettingVM GetPageHeaderFooter(int entityId);
        PageHeaderFooterSettingVM SavePageHeaderFooter(PageHeaderFooterSettingVM obj, int userId);
    }
}
