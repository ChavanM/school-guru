﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using System.Reflection;
using Syncfusion.DocIO.DLS;

namespace BM_ManegmentServices.Services.DocumentManagenemt
{
    public class DocumentSetting_Service : IDocumentSetting_Service
    {
        public DocumentSettingVM GetDocumentSetting(int customerId, string documentType)
        {
            var result = new DocumentSettingVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result.PageSetting = (from row in entities.BM_SP_GetDocumentPageSetting(customerId, documentType)
                                          select new PageSettingVM
                                          {
                                              PageSize = row.PageSize,
                                              MarginTop = (float)row.MarginTop,
                                              MarginLeft = (float)row.MarginLeft,
                                              MarginRight = (float)row.MarginRight,
                                              MarginBottom = (float)row.MarginBottom,
                                          }).FirstOrDefault();

                    result.Styles = (from row in entities.BM_SP_GetDocumentFormatSetting(customerId, documentType)
                                     select new StyleVM
                                     {
                                         StyleName = row.StyleName,
                                         FontName = row.FontName,
                                         FontSize = row.FontSize,
                                         FontColor = row.FontColor,
                                         IsBold = (bool)row.IsBold,
                                         IsItalic = (bool)row.IsItalic,
                                         IsUnderline = (bool)row.IsUnderline
                                     }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<PageHeaderFooterSettingListVM> GetPageHeaderFooterEntityList(int customerId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    return (from row in entities.BM_EntityMaster
                            join t in entities.BM_TemplatePageHeaderFooter on row.Id equals t.EntityId
                            where row.Customer_Id == customerId && row.Is_Deleted == false &&
                              t.IsActive == true
                            select new PageHeaderFooterSettingListVM
                            {
                                PageHeaderEntityId = row.Id,
                                EntityName = row.CompanyName,
                                PageHeaderId = t.Id,
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new List<PageHeaderFooterSettingListVM>();
            }
        }

        public PageHeaderFooterSettingVM GetPageHeaderFooter(int entityId)
        {
            PageHeaderFooterSettingVM result = null;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_TemplatePageHeaderFooter
                              where row.EntityId == entityId && row.IsActive == true
                              select new PageHeaderFooterSettingVM
                              {
                                  PageHeaderId = row.Id,
                                  PageHeaderEntityId = row.EntityId,
                                  PageHeaderFormat = row.PageHeaderFormat,
                                  PageFooterFormat = row.PageFooterFormat,
                                  UsedInAgenda = row.UsedInAgenda,
                                  UsedInNotice = row.UsedInNotice,
                                  UsedInCTC = row.UsedInCTC
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            if (result == null)
            {
                result = new PageHeaderFooterSettingVM() { PageHeaderEntityId = entityId };
            }
            return result;
        }

        public PageHeaderFooterSettingVM SavePageHeaderFooter(PageHeaderFooterSettingVM obj, int userId)
        {
            try
            {
                if(!string.IsNullOrEmpty(obj.PageHeaderFormat))
                {
                    if (IsTemplateValid(obj.PageHeaderFormat) == false)
                    {
                        obj.Error = true;
                        obj.Message = "Header format not supported.";
                        return obj;
                    }
                }

                if (!string.IsNullOrEmpty(obj.PageFooterFormat))
                {
                    if (IsTemplateValid(obj.PageFooterFormat) == false)
                    {
                        obj.Error = true;
                        obj.Message = "Footer format not supported.";
                        return obj;
                    }
                }
                
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_TemplatePageHeaderFooter
                                where row.EntityId == obj.PageHeaderEntityId
                                select row).FirstOrDefault();
                    if (_obj == null)
                    {
                        _obj = new BM_TemplatePageHeaderFooter()
                        {
                            EntityId = obj.PageHeaderEntityId,
                            PageHeaderFormat = obj.PageHeaderFormat,
                            PageFooterFormat = obj.PageFooterFormat,
                            UsedInAgenda = obj.UsedInAgenda,
                            UsedInNotice = obj.UsedInNotice,
                            UsedInCTC = obj.UsedInCTC,
                            IsActive = true,
                            CreatedBy = userId,
                            CreatedOn = DateTime.Now
                        };
                        entities.BM_TemplatePageHeaderFooter.Add(_obj);
                        entities.SaveChanges();
                        obj.PageHeaderId = _obj.Id;
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        _obj.IsActive = true;
                        _obj.PageHeaderFormat = obj.PageHeaderFormat;
                        _obj.PageFooterFormat = obj.PageFooterFormat;
                        _obj.UsedInAgenda = obj.UsedInAgenda;
                        _obj.UsedInNotice = obj.UsedInNotice;
                        _obj.UsedInCTC = obj.UsedInCTC;

                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        obj.PageHeaderId = _obj.Id;
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.updateSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Success = false;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        private bool IsTemplateValid(string template)
        {
            var result = false;
            try
            {
                Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlFormatProvider htmlProvider = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlFormatProvider();
                Telerik.Windows.Documents.Flow.Model.RadFlowDocument htmlDocument = new Telerik.Windows.Documents.Flow.Model.RadFlowDocument();
                htmlDocument = htmlProvider.Import(template);
                Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings exportSettings = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings();

                string htmlstring = htmlProvider.Export(htmlDocument);

                using (WordDocument doc = new WordDocument())
                {
                    doc.EnsureMinimal();

                    WSection section = doc.LastSection;
                    var isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                    if (isValidHtml)
                    {
                        var newPara = section.AddParagraph() as WParagraph;
                        newPara.AppendHTML(htmlstring);

                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
    }
}