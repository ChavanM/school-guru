﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Annotation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Annotation
{
    public interface IAnnotationService
    {
        AgendaMinutesCommentsVM GetAnnotation(long meetingId, long meetingParticipantId, string docType, int userId, string role, int customerId);
        Message SaveAnnotation(AgendaMinutesCommentsVM obj, int userId);
        string GetAnnote(long id);
    }
}
