﻿using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IUser
    {
        List<UserVM> GetUsers(int customerID);
        //List<UserVM> GetUsersNew(int customerID);
        UserVM Create(UserVM obj, string SenderEmailAddress);
        UserVM Update(UserVM obj, string SenderEmailAddress, out string old_EmailId);
        string Delete(long id, int userId);
        List<RoleVM> GetComplianceRoles();
        List<RoleVM> GetSecretarialRoles();
        List<RoleVM> GetSecretarialRoles_ICSI();
        
        List<RoleVM> GetHRRoles();
        UserVM GetUser(int customerID, string Email_Id);
        int GetRoleIdFromCode(string Code);

        List<UserforDropdown> UserAssignment();
        List<UserforDropdown> UserAssignment(int customerId);
        UserPageVM GetProductByCustomerId(int customerId);
    }
}
