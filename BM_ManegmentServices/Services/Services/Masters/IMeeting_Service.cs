﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Dashboard;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IMeeting_Service
    {

        #region Meeting
        IEnumerable<MeetingVM> GetMeetings(long CustomerId, int UserId, string Role);
        Dashboard_MeetingVM GetMeetingsForDashboard(int UserID, int CustomerId, string Role);
        List<CompletedMeetingsVM> GetCompletedMeetings(int entityId, int customerId);
        IEnumerable<MeetingVM> GetMeetingsForParticipant(int UserID);
        MeetingVM GetMeeting(long CustomerId, long MeetingId);
        MeetingVM GetMeetingDetails(int CustomerId, long MeetingId);
        MeetingVM GetMeetingForDocumentName(long CustomerId, long MeetingId);
        Meeting_NewVM Create(Meeting_NewVM obj);
        MeetingVM Update(MeetingVM obj);
        #endregion

        #region Upcoming Meetings
        List<MeetingListVM> GetUpcomingMeetings(long CustomerId, int UserId, string Role);
        #endregion

        #region Meeting For Dashboard
        List<DraftMeetingListVM> GetDraftMeetingsForDirectorApproval(int customerId, int userID);
        List<DraftMeetingListVM> GetDraftMeetingsForDirectorApproval(int userID);
        List<DraftMeetingListVM> GetConcludedMeetingsForDirector1(int UserID);
        List<DraftMeetingListVM> GetConcludedMeetingsForDirector(int customerId, int userId);
        #endregion

        #region Delete Meeting
        DeleteMeetingVM GetDetailsMeetingCanDelete(long meetingId);
        DeleteMeetingVM DeleteMeeting(DeleteMeetingVM obj, int userId);
        #endregion

        #region Meeting Postponded
        Message Postpond(long MeetingId, int postpondedBy);
        #endregion

        #region Seek Availability
        IEnumerable<MeetingAvailabilityVM> GetAllSeekAvailability(long parentID);
        MeetingAvailabilityVM GetSeekAvailability(long id);
        MeetingAvailabilityVM Create(MeetingAvailabilityVM obj);
        MeetingAvailabilityVM Update(MeetingAvailabilityVM obj);
        bool UpdateAvailabilityDueDate(long MeetingId, DateTime date_, int UserId);
        MeetingVM ForceEditSeekAvailability(long CustomerId, long MeetingId);

        MeetingAvailabilityVM SelectSeekAvailability(MeetingAvailabilityVM obj, int UserId);
        #endregion

        #region Meeting Participants
        List<MeetingParticipants_ResultVM> GetMeetingParticipants(long Meeting_Id);
        bool SaveMeetingParticipants(int customerID, int entityId, int meetingTypeId, long meetingID, int userID);
        #endregion

        #region Pending Availability
        IEnumerable<MeetingVM> GetPedingAvailability(long UserId);
        IEnumerable<CommitteeCompVM> GetAllMeetingType(string type);
        #endregion
        
        #region Meeting Agenda

        IEnumerable<MeetingAgendaMappingVM> GetMeetingAgenda(long MeetingId, string Part,string Type);
        AgendaItemSelect AddAgendaItem(AgendaItemSelect obj, int createdBy, int customerID);
        MeetingAgendaMappingVM GetAgendaItem(long MeetingAgendaMappingID);
        MeetingAgendaMappingVM UpdateAgendaItem(MeetingAgendaMappingVM obj, int updatedBy);
        MeetingAgendaMappingVM SaveOrderAgendaItem(MeetingAgendaMappingVM obj, int SrNo);
        PreviewAgendaVM PreviewAgenda(long MeetingId, int CustomerId);
        void SetDefaultItemNumbers(long MeetingId);
        #endregion

        #region Move Any Other business Agenda Items to Part B
        void MoveAnyOtherAgendaItems(long MeetingId);
        #endregion

        #region Delete Agenda
        DeleteAgendaVM GetAllowAgendaDelete(long meetingId, long meetingAgendaMappingId);
        DeleteAgendaVM DeleteAgendaItem(DeleteAgendaVM obj);
        #endregion

        #region Meeting Pre Committee Agenda
        List<MeetingPendingPreCommitteeVM> GetPendingPreCommittee(long MeetingId);
        List<PreCommiitteeDraftMeetingVM> GetDraftCommitteeMeeting(long PreCommitteeId, int EntityId, int CustomerId);
        string SavePreCommitteeMeetingID(int MeetingTypeID, long SourceMeetingID, long SourceMeetingAgendaMappingID, long? PostAgendaId, long TargetMeetingID, int UserId);
        #endregion

        #region Meeting Availability Mail
        MeetingAvailabilityMailVM UpdateAvailabilityMail(MeetingAvailabilityMailVM obj);
        MeetingAvailabilityMailVM PreviewAvailabilityMail(MeetingAvailabilityMailVM obj);
        MeetingAvailabilityMailVM SendAvailabilityMail(MeetingAvailabilityMailVM obj, out string MailFormat);

        #endregion

        #region Seek Availability Response
        List<AvailabilityResponse_ResultVM> GetAvailabilityResponse(long MeetingId, int CustomerId, int? UserId);
        AvailabilityResponse_ResultVM UpdateAvailabilityResponse(AvailabilityResponse_ResultVM obj, int CreatedBy);
        List<AvailabilityResponseChartData_ResultVM> GetAvailabilityResponseChartData(long MeetingId, long MeetingAvailabilityId);
        List<AvailabilityResponseByAvailabilityId_ResultVM> GetAvailabilityResponseByAvailabilityId(long MeetingId, long MeetingAvailabilityId);
        #endregion

        #region Meeting Agenda Item Data Fields Template Generation
        MeetingAgendaTemplateVM GetAgendaItemTemplate(long MeetingAgendaMappingID);
        MeetingAgendaTemplateVM SaveUpdateAgendaItemTemplateList(MeetingAgendaTemplateVM obj, int userID);
        bool InsertTemplates(long? targetMappingId, long? sourceMappingId, int createdBy);
        #endregion

        #region Create New Agenda Item
        AgendaMasterVM CreateNewAgendaItem(long meetingId, int CustomerId);
        AgendaMasterVM CreateNewAgendaItem(AgendaMasterVM obj, int Customer_Id);
        AgendaMasterVM Update(AgendaMasterVM obj);
        #endregion

        #region Meeting Agenda change Items No
        Message CheckAgendaItemNumberIsValid(long meetingId);
        List<ChangeAgendaItemNumberVM> GetAgendaItemForChangeNumber(long meetingId);
        IEnumerable<ChangeAgendaItemNumberVM> SaveAgendaItemNumber(IEnumerable<ChangeAgendaItemNumberVM> obj, int updatedBy);
        #endregion

        #region Meeting Notice Mail
        MeetingNoticeMailVM UpdateNoticeMail(MeetingNoticeMailVM obj);
        MeetingNoticeMailVM PreviewNoticeMail(MeetingNoticeMailVM obj);
        MeetingNoticeMailVM SendNoticeMail(MeetingNoticeMailVM obj, out string MailFormat);
        long CreateNoticeLog(long meetingId, string templateType, string mailTemplate, long? fileId, int createdBy);
        bool CreateNoticeTransaction(long noticeLogId, long participantId, DateTime dated);
        List<MeetingNoticeLogResultVM> GetNoticeAgendaLog(long meetingId, int customerId);
        Message GetNoticeAgendaLogDetails(long id);
        MeetingNoticeMailVM NoticeAgendaTemplate(MeetingNoticeMailVM obj);
        string GetNoticeFormatByMeetingId(long meetingId);
        #endregion

        #region Invitee Mail
        MeetingInviteeMailVM UpdateInviteeMail(MeetingInviteeMailVM obj, int updatedBy);
        MeetingInviteeMailVM PreviewInviteeMail(MeetingInviteeMailVM obj);
        MeetingInviteeMailVM SendInviteeMail(MeetingInviteeMailVM obj, out string MailFormat);
        #endregion

        #region Circular Meeting Response
        List<Agenda_SummitResponse> GetAgendaResponse(long meetingId, int customerId, int? userId,int? RoleId);
        Agenda_SummitResponse UpdateAgendaResponse(Agenda_SummitResponse item, int userId);
        List<CircularAgendaData> GetAllAgendaDetails(long parentID);
		List<AvailabilityResponseChartData_ResultVM> GetAgendaResponseChartData(long MeetingId, long AgendaId, long MappingId, int UserId);
        #endregion

        #region Meeting Adjourned
        Meeting_AdjournVM Adjourned(long meetingId, int customerId);
        Message Adjourned(long meetingId, DateTime adjournedToDate, int adjournedBy, int customerId);
        #endregion

        #region Conclude meeting (Compliance activation, Open Agenda items)
        void ConculdeMeeting(long meetingId, int userId, int customerId);
        void ConculdeAGM(long meetingId, int userId, int customerId);
        #endregion

        #region Create AGM from Board
        void CreateAGMFromBoard(long meetingId, int entityId, int customerId, int createdBy);
        #endregion

        #region Agenda Documents
        bool AgendaFileUpload(VM_AgendaDocument _vmagendaDocument);
        bool agendaDocumentDownload(long id);
        List<VM_AgendaDocument> GetAgendaDocument(long agendaMappingID, int customerId);
        #endregion

        #region Minutes of Meeting
        PreviewAgendaVM PreviewDraftMinutes(long MeetingId, int CustomerId, long DraftCirculationID, int UserID);
        PreviewAgendaVM PreviewMOM(long MeetingId, int CustomerId);
        MeetingAgendaMappingMOMVM PreviewMOM(long MappingId);
        MeetingAgendaMappingMOMVM GetMOM(long MeetingId, long MappingId, int CustomerId);
        MeetingAgendaMappingMOMVM SaveMOM(MeetingAgendaMappingMOMVM obj, int updatedBy);
        #endregion

        #region Get Minutes Formates For Default Agenda Item (Quorum)
        string GetMinutesForDefaultAgendaItem(int usedFor, List<string> lstTemplateFieldData);
        #endregion

        #region Minutes Formates For Default Agenda Item (Quorum)
        bool SetMinutesForDefaultAgendaItem(long meetingId, int usedFor, List<string> lstLeaveOfAbsence, int userId);
        #endregion

        #region Minutes Details (date of entering etc)
        MeetingMinutesDetailsVM GetMinutesDetails(long meetingId);
        MeetingMinutesDetailsVM CirculateMinutes(MeetingMinutesDetailsVM obj, int userId);
        MeetingMinutesDetailsVM SaveMinutesDetails(MeetingMinutesDetailsVM obj, int userId);
        MeetingMinutesDetailsVM FinalizedMinutes(MeetingMinutesDetailsVM obj, int userId);
        MeetingMinutesDetailsVM FinalizedMinutesOfVirtualMeeting(MeetingMinutesDetailsVM obj, int userId);
        #endregion

        #region Meeting level compliance reopen for  Virtaul Meeting
        bool ReOpenMeetingLevelCompliance(long meetingId, string meetingEvent);
        #endregion

        #region Minutes Comments
        MeetingMinutesApproveVM ApproveDraftMinutes(MeetingMinutesApproveVM obj, int userId);
        bool IsDraftMinutesApproved(long draftCirculationID);
        #endregion

        #region Director UI
        IEnumerable<MeetingVM> GetTodaysMeetingDirectorwise(int customerId, int userId);
        #endregion
    }
}