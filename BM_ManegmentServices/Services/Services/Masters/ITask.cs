﻿using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
   public interface ITask
    {
        IEnumerable<BM_MeetingAgendaMapping> GetAgendaList(int MeetingId);
        IEnumerable<TaskMeeting>  GetMeetingList(long EntityId);
        VMTask UpdateTask(VMTask vmtask);
        VMTask CreateTask(VMTask vmtask);
        VMTask CreateTaskAssinment(VMTask vmtask);
        List<VMTask> ReadData(int TaskId);
        List<VMTask> GetTaskDetails(int customerId,int UserId,string Role);
        VMTask gettaskbyId(int id,int userId,int customerId,string Role);
        VMTask UpdateAssigTask(VMTask _objentity);
        bool DownloadTaskFile(int id);
        bool ViewTaskFile(int id);
        List<TaskRemarks> GetPerformerDetails(int customerId, int userId, int taskId);
        VMTask UpdateTaskPerformer(VMTask vmtask);
        VMTask CreateTaskPerformer(VMTask vmtask);
        bool ExportExcel(byte[] fileBytes);
        List<BM_Task_SP_Status_Result> GetPerformerstatus(int customerId, int userId, int taskId);
        VMTask GetTask(int taskId);
        List<VMTask> GetTaskType(int taskId,int UserId);
        TaskTypeVM getTaskTypeDetails(int taskId);
        bool DownloadTaskFile1(int id);
        List<string> GetTaskFile(long id);
   
        List<TaskFile> GetTaskFileDetails(long taskId, int userId, int customerId);
        bool DeleteTask(long fileId);
        string getAgendatitle(int? agendaId);
    }
}
