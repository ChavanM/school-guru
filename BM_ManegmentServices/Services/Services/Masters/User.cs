﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Configuration;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public class User : IUser
    {
        ComplianceDBEntities entities = new ComplianceDBEntities();
        AuditControlEntities entities_audit = new AuditControlEntities();
        Compliance_SecretarialEntities entities_Secretarial = new Compliance_SecretarialEntities();
        public UserVM Create(UserVM obj, string SenderEmailAddress)
        {
            try
            {
                obj.Message = new Response();
                bool emailExists;

                #region Audit Role
                string PrimaryRoleAudit = "";
                string Isauditheadormgr = "";
                int getproductAudit = 0;

                if (obj.AuditRoleId == 1)
                {
                    getproductAudit = 7;
                    PrimaryRoleAudit = "Executive";
                }
                else if (obj.AuditRoleId == 2)
                {
                    Isauditheadormgr = "AM";
                    getproductAudit = 7;
                    PrimaryRoleAudit = "Manager";
                }
                else if (obj.AuditRoleId == 4)
                {
                    getproductAudit = 2;
                    Isauditheadormgr = "AH";
                    PrimaryRoleAudit = "Partner";
                }
                else
                {
                    //getproductAudit = -1;
                    getproductAudit = 7;
                    //PrimaryRoleAudit = "Executive";
                }
                #endregion

                com.VirtuosoITech.ComplianceManagement.Business.Data.User _obj_ComplianceUser = new com.VirtuosoITech.ComplianceManagement.Business.Data.User();

                //set default 7
                if (obj.ComplianceRoleId == null)
                {
                    //obj.ComplianceRoleId = -1;
                    obj.ComplianceRoleId = 7;
                }

                _obj_ComplianceUser.ID = obj.UserID;
                _obj_ComplianceUser.Email = obj.Email;
                _obj_ComplianceUser.FirstName = obj.FirstName;
                _obj_ComplianceUser.LastName = obj.LastName;

                _obj_ComplianceUser.RoleID = Convert.ToInt32(obj.ComplianceRoleId);

                if (obj.SecretarialRoleId != null)
                    _obj_ComplianceUser.SecretarialRoleID = obj.SecretarialRoleId;

                //_obj_ComplianceUser.HRRoleID = Convert.ToInt32(obj.HRRoleId);
                _obj_ComplianceUser.IsAuditHeadOrMgr = Isauditheadormgr;
                //_obj_ComplianceUser.PrimaryRoleAudit = PrimaryRoleAudit;

                _obj_ComplianceUser.CustomerID = obj.CustomerID;
                _obj_ComplianceUser.ContactNumber = obj.ContactNumber;
                _obj_ComplianceUser.Address = obj.Address;
               
                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    ID = obj.UserID,
                    FirstName = obj.FirstName,
                    LastName = obj.LastName,
                    //Designation = tbxDesignation.Text.Trim(),
                    Email = obj.Email,

                    SecretarialRoleID = Convert.ToInt32(obj.SecretarialRoleId),

                    CustomerID = obj.CustomerID,
                    ContactNumber = obj.ContactNumber,
                    Address = obj.Address,
                    //PrimaryRoleAudit = PrimaryRoleAudit
                };

                #region Check User Exists or not in UserManagement, UserManagementRisk
                UserManagement.Exists(_obj_ComplianceUser, out emailExists);
                if (emailExists)
                {
                    obj.Message.Success = false;
                    obj.Message.Error = true;
                    obj.Message.Message = "User with Same Email already Exists.";
                    return obj;
                }

                UserManagementRisk.Exists(mstUser, out emailExists);
                if (emailExists)
                {
                    obj.Message.Success = false;
                    obj.Message.Error = true;
                    obj.Message.Message = "User with Same Email already Exists.";
                    return obj;
                }
                #endregion

                string passwordText = Util.CreateRandomPassword(10);
                obj.Password = passwordText;

                _obj_ComplianceUser.CreatedBy = obj.CreatedBy;
                _obj_ComplianceUser.CreatedByText = obj.CreatedByText;
                _obj_ComplianceUser.Password = Util.CalculateAESHash(passwordText);
                _obj_ComplianceUser.EnType = "A";
                _obj_ComplianceUser.RoleID = Convert.ToInt32(obj.ComplianceRoleId);
                _obj_ComplianceUser.SecretarialRoleID = obj.SecretarialRoleId;
                //_obj_ComplianceUser.HRRoleID = obj.HRRoleId;

                _obj_ComplianceUser.IsAuditHeadOrMgr = Isauditheadormgr;
                //_obj_ComplianceUser.PrimaryRoleAudit = PrimaryRoleAudit;

                _obj_ComplianceUser.IsActive = true;
                _obj_ComplianceUser.IsDeleted = false;
                _obj_ComplianceUser.WrongAttempt = 0;


                mstUser.CreatedBy = _obj_ComplianceUser.CreatedBy;
                mstUser.CreatedByText = _obj_ComplianceUser.CreatedByText;
                mstUser.Password = _obj_ComplianceUser.Password;
                mstUser.EnType = _obj_ComplianceUser.EnType;

                mstUser.RoleID = getproductAudit;
                mstUser.SecretarialRoleID = _obj_ComplianceUser.SecretarialRoleID;
                //mstUser.HRRoleID = _obj_ComplianceUser.HRRoleID;

                mstUser.IsAuditHeadOrMgr = _obj_ComplianceUser.IsAuditHeadOrMgr;                
                //mstUser.PrimaryRoleAudit = _obj_ComplianceUser.PrimaryRoleAudit;

                #region Create User

                var newUserID = UserManagement.CreateNew(_obj_ComplianceUser, new List<UserParameterValue>(), SenderEmailAddress, obj.EmailBody);
                if (newUserID > 0)
                {
                    var result = UserManagementRisk.Create(mstUser, new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>(), SenderEmailAddress, obj.EmailBody);
                    if (result == false)
                    {
                        UserManagement.deleteUser(newUserID);
                        obj.Message.Error = true;
                        obj.Message.Message = "Something wents wrong.";
                    }
                    else
                    {
                        obj.UserID = newUserID;
                        obj.Message.Success = true;
                        obj.Message.Message = "Saved Successfully";
                    }
                }
                else
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something wents wrong.";
                }

                #endregion
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public UserVM Update(UserVM obj, string SenderEmailAddress, out string old_EmailId)
        {
            old_EmailId = obj.Email;
            try
            {
                obj.Message = new Response();
                bool emailExists;
                com.VirtuosoITech.ComplianceManagement.Business.Data.User _obj_ComplianceUser = new com.VirtuosoITech.ComplianceManagement.Business.Data.User();

                _obj_ComplianceUser.ID = obj.UserID;
                _obj_ComplianceUser.Email = obj.Email;
                _obj_ComplianceUser.FirstName = obj.FirstName;
                _obj_ComplianceUser.LastName = obj.LastName;
                _obj_ComplianceUser.SecretarialRoleID = Convert.ToInt32(obj.SecretarialRoleId);
                //_obj.CustomerID = obj.CustomerID;
                _obj_ComplianceUser.ContactNumber = obj.ContactNumber;
                _obj_ComplianceUser.Address = obj.Address;

                if (obj.ComplianceRoleId == null)
                {
                    obj.ComplianceRoleId = -1;
                }

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    ID = obj.UserID,
                    FirstName = obj.FirstName,
                    LastName = obj.LastName,                   
                    Email = obj.Email,
                    
                    RoleID = Convert.ToInt32(obj.ComplianceRoleId),
                    SecretarialRoleID = Convert.ToInt32(obj.SecretarialRoleId),
                                        
                    ContactNumber = obj.ContactNumber,
                    Address = obj.Address
                };

                #region Audit Role
                string PrimaryRoleAudit = "";
                string Isauditheadormgr = "";
                int getproductAudit = 0;

                if (obj.AuditRoleId == 1)
                {
                    getproductAudit = 7;
                    PrimaryRoleAudit = "Executive";
                }
                else if (obj.AuditRoleId == 2)
                {
                    Isauditheadormgr = "AM";
                    getproductAudit = 7;
                    PrimaryRoleAudit = "Manager";
                }
                else if (obj.AuditRoleId == 4)
                {
                    getproductAudit = 2;
                    Isauditheadormgr = "AH";
                    PrimaryRoleAudit = "Partner";
                }
                else
                {
                    getproductAudit = -1;
                    //PrimaryRoleAudit = "Executive";
                }
                #endregion   

                var user = entities.Users.Where(k => k.ID == obj.UserID && k.IsDeleted == false).FirstOrDefault();
                bool checkexsistemail = false;
                bool checkexsistmobileno = false;
                if (user != null)
                {
                    checkexsistemail = entities.Users.Where(k => k.ID != obj.UserID && k.IsDeleted == false && k.Email == obj.Email).Any();
                    //checkexsistmobileno = entities.Users.Where(k => k.ID != obj.UserID && k.IsDeleted == false && k.ContactNumber == obj.ContactNumber).Any();
                    if (checkexsistemail == false) /*&& checkexsistmobileno == false*/
                    {
                        old_EmailId = user.Email;

                        user.Email = obj.Email;
                        user.FirstName = obj.FirstName;
                        user.LastName = obj.LastName;
                        user.ContactNumber = obj.ContactNumber;
                        user.Address = obj.Address;

                        user.RoleID = Convert.ToInt32(obj.ComplianceRoleId);
                        user.SecretarialRoleID = Convert.ToInt32(obj.SecretarialRoleId);
                        //user.HRRoleID = obj.HRRoleId;
                        user.IsAuditHeadOrMgr = Isauditheadormgr;
                        //.PrimaryRoleAudit = PrimaryRoleAudit;

                        entities.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();

                        var mst_user = entities_audit.mst_User.Where(k => k.ID == obj.UserID && k.IsDeleted == false).FirstOrDefault();
                        if (mst_user != null)
                        {
                            mst_user.Email = obj.Email;
                            mst_user.FirstName = obj.FirstName;
                            mst_user.LastName = obj.LastName;
                            mst_user.ContactNumber = obj.ContactNumber;
                            mst_user.Address = obj.Address;

                            mst_user.RoleID = getproductAudit; // Audit
                            mst_user.SecretarialRoleID = Convert.ToInt32(obj.SecretarialRoleId);
                            //mst_user.HRRoleID = obj.HRRoleId;
                            mst_user.IsAuditHeadOrMgr = Isauditheadormgr;
                            //mst_user.PrimaryRoleAudit = PrimaryRoleAudit;

                            entities_audit.Entry(mst_user).State = System.Data.Entity.EntityState.Modified;
                            entities_audit.SaveChanges();
                        }

                        obj.Message.Success = true;
                        obj.Message.Message = "Updated Successfully";
                    }
                    else
                    {
                        if (checkexsistemail)
                        {
                            obj.Message.Error = true;
                            obj.Message.Message = "Email already exist";
                        }
                        else if (checkexsistmobileno)
                        {
                            obj.Message.Error = true;
                            obj.Message.Message = "Contact Number already exist";
                        }
                    }
                }
                else
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something went wrong.";
                }

            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public string Delete(long id, int userId)
        {
            throw new NotImplementedException();
        }

        public List<RoleVM> GetComplianceRoles()
        {
            var roles = (from row in entities.Roles
                             //where row.IsForCompliance == true
                         where row.ID == 2 || row.ID == 7 || row.ID == 8
                         select new RoleVM
                         {
                             RoleID = row.ID,
                             RoleName = row.Name,
                             RoleCode = row.Code
                         }).ToList();
            return roles;
        }

        public List<RoleVM> GetSecretarialRoles()
        {
            var roles = (from row in entities.Roles
                         where row.IsForSecretarial == true
                         select new RoleVM
                         {
                             RoleID = row.ID,
                             RoleName = row.Name,
                             RoleCode = row.Code
                         }).ToList();
            return roles;
        }

        public List<RoleVM> GetSecretarialRoles_ICSI()
        {
            List<string> secRoleCodes = new List<string> { "DADMN", "CEXCT", "CSMGR" };

            var roles = (from row in entities.Roles
                         where row.IsForSecretarial == true
                         && secRoleCodes.Contains(row.Code)
                         select new RoleVM
                         {
                             RoleID = row.ID,
                             RoleName = row.Name,
                             RoleCode = row.Code
                         }).ToList();
            return roles;
        }

        public List<RoleVM> GetHRRoles()
        {
            List<string> hrRoleCodes = new List<string> { "DADMN", "HEXCT", "HMGR" };

            var roles = (from row in entities.Roles
                         where hrRoleCodes.Contains(row.Code)
                         select new RoleVM
                         {
                             RoleID = row.ID,
                             RoleName = row.Name,
                             RoleCode = row.Code
                         }).ToList();
            return roles;
        }

        //public List<UserVM> GetUsersNew(int customerID)
        //{
        //    var result = (from u in entities_Secretarial.BM_SP_GetUsersByCustomerId(customerID)
        //                  select new UserVM
        //                  {
        //                      UserID = u.UserID,
        //                      FirstName = u.FirstName,
        //                      LastName = u.LastName,
        //                      FullName = u.FirstName + (u.LastName == null ? "" : " " + u.LastName),
        //                      Email = u.Email,
        //                      SecretarialRoleId = u.SecretarialRoleId,
        //                      ComplianceRoleId = u.ComplianceRoleId,
        //                      HRRoleId = u.HRRoleId,
        //                      AuditRoleId = u.AuditRoleId,
        //                      RoleName = u.RoleName,
        //                      ContactNumber = u.ContactNumber,
        //                      Address = u.Address_
        //                  }).ToList();
        //    return result;
        //}

        public List<UserVM> GetUsers(int customerID)
        {
            List<UserVM> result = new List<UserVM>();
            int? parentID = entities.Customers.Where(k => k.ID == customerID).Select(row => row.ParentID).FirstOrDefault();

            if (parentID != null)
            {
                result = (from u in entities.Users
                          from r in entities.Roles.Where(k => k.ID == u.SecretarialRoleID && k.IsForSecretarial == true).DefaultIfEmpty()
                          where u.IsActive == true && u.IsDeleted == false && (u.CustomerID == customerID || u.CustomerID == parentID)
                          select new UserVM
                          {
                              UserID = u.ID,
                              FirstName = u.FirstName,
                              LastName = u.LastName,
                              FullName = u.FirstName + (u.LastName == null ? "" : " " + u.LastName),
                              Email = u.Email,
                              SecretarialRoleId = u.SecretarialRoleID,
                              ComplianceRoleId = u.RoleID,
                              //HRRoleId = u.HRRoleID,
                              RoleName = r.Name,
                              ContactNumber = u.ContactNumber,
                              Address = u.Address
                          }).OrderBy(row => row.FullName).ToList();
            }
            else
            {
                result = (from u in entities.Users
                          from r in entities.Roles.Where(k => k.ID == u.SecretarialRoleID && k.IsForSecretarial == true).DefaultIfEmpty()
                          where u.IsActive == true && u.IsDeleted == false
                          && u.CustomerID == customerID
                          select new UserVM
                          {
                              UserID = u.ID,
                              FirstName = u.FirstName,
                              LastName = u.LastName,
                              FullName = u.FirstName + (u.LastName == null ? "" : " " + u.LastName),
                              Email = u.Email,
                              SecretarialRoleId = u.SecretarialRoleID,
                              ComplianceRoleId = u.RoleID,
                              //HRRoleId = u.HRRoleID,
                              RoleName = r.Name,
                              ContactNumber = u.ContactNumber,
                              Address = u.Address
                          }).OrderBy(row => row.FullName).ToList();
            }

            return result;
        }

        public UserVM GetUser(int customerID, string Email_Id)
        {
            var result = (from u in entities.Users
                          from r in entities.Roles.Where(k => k.ID == u.SecretarialRoleID && k.IsForSecretarial == true).DefaultIfEmpty()
                          where u.IsActive == true && u.IsDeleted == false && u.CustomerID == customerID && u.Email == Email_Id
                          select new UserVM
                          {
                              UserID = u.ID,
                              FirstName = u.FirstName,
                              LastName = u.LastName,
                              Email = u.Email,
                              SecretarialRoleId = u.SecretarialRoleID,
                              RoleName = r.Name,
                              ContactNumber = u.ContactNumber,
                              Address = u.Address
                          }).FirstOrDefault();
            return result;
        }

        public int GetRoleIdFromCode(string Code)
        {
            var result = 0;

            try
            {
                var r = entities.Roles.Where(k => k.Code == Code && k.IsForSecretarial == true).FirstOrDefault();
                result = r == null ? 0 : r.ID;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<UserforDropdown> UserAssignment()
        {
            try
            {
                var getuserforAssignment = (from row in entities.Users
                                            where row.SecretarialRoleID == 20 || row.SecretarialRoleID == 20 || row.SecretarialRoleID == 22
                                            select new UserforDropdown
                                            {
                                                Name = row.FirstName + "  " + row.LastName,
                                                Id = row.ID
                                            }).ToList();
                return getuserforAssignment;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<UserforDropdown> UserAssignment(int customerId)
        {
            try
            {
                var getuserforAssignment = (from row in entities.Users
                                            where row.SecretarialRoleID == 19 || row.SecretarialRoleID == 20 || row.SecretarialRoleID == 22
                                            select new UserforDropdown

                                            {
                                                Name = row.FirstName + "  " + row.LastName,
                                                Id = row.ID
                                            }
                                            ).ToList();
                return getuserforAssignment;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public UserPageVM GetProductByCustomerId(int customerId)
        {
            var result = new UserPageVM();
            try
            {
                var ProductMappingDetails = UserManagement.GetByProductIDList(customerId);
                result.ShowComplianceRole = ProductMappingDetails.Contains(1);
                result.ShowAuditRole = ProductMappingDetails.Contains(4);
                result.ShowHRRole = ProductMappingDetails.Contains(9);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
    }
}