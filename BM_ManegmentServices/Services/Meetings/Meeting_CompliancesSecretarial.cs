﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using BM_ManegmentServices.Services.Masters;
using System.Reflection;
using BM_ManegmentServices.VM.Compliance;
using BM_ManegmentServices.Services.Compliance;

namespace BM_ManegmentServices.Services.Meetings
{
    public class Meeting_CompliancesSecretarial : IMeeting_Compliances
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        ICompliance_Service objICompliance_Service;
        public Meeting_CompliancesSecretarial(ICompliance_Service objCompliance_Service)
        {
            objICompliance_Service = objCompliance_Service;
        }
        public IEnumerable<MeetingComplianceScheduleDetails_ResultVM> GetCompliances(long agendaId, long meetingId)
        {

            //var result = from mapping in entities.BM_ComplianceMapping
            //             join schedule in entities.BM_ComplianceScheduleOn on mapping.MCM_ID equals schedule.MCM_ID
            //             from status in entities.ComplianceStatus.Where(k => k.ID == schedule.StatusId).DefaultIfEmpty()
            //             join com in entities.Compliances on mapping.ComplianceID equals com.ID
            //             where schedule.MeetingID == meetingId && mapping.AgendaMasterID == agendaId && mapping.MappingType == SecretarialConst.ComplianceMappingType.AGENDA && mapping.IsDeleted == false
            //             select new MeetingComplianceVM
            //             {
            //                 MeetingComplianceID = 0,
            //                 Meeting_ID = meetingId,

            //                 Agenda_ComplianceId = mapping.MCM_ID,
            //                 AgendaMasterId = (long)mapping.AgendaMasterID,
            //                 ComplianceId = mapping.ComplianceID,

            //                 ComplianceShortDesc = com.ShortDescription,
            //                 DateType = mapping.DateType,
            //                 DayType = mapping.DayType.Trim(),
            //                 BeforeAfter = mapping.BeforeAfter.Trim(),
            //                 Numbers = mapping.Numbers,
            //                 DaysOrHours = mapping.DaysOrHours.Trim(),
            //                 ScheduleOn = schedule.ScheduleOn,
            //                 StatusId = schedule.StatusId,
            //                 Status = status == null ? "" : status.Name
            //             };

            //return result.ToList();
            return null;
        }

        

        #region Get Compliance Id for generate Meeting Level schedule
        public Message GenerateScheduleOn(long meetingId, string mappingType, int customerId, int createdBy)
        {
            var result = new Message();
            try
            {
                var entityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId && k.Customer_Id == customerId && k.IsDeleted == false).Select(k => k.EntityId).FirstOrDefault();
                return GenerateScheduleOn(meetingId, mappingType, entityId, customerId, createdBy, false);
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Something went wrong.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public Message GenerateScheduleOn(long meetingId, string mappingType, int entityId, int customerId, int createdBy, bool eVoting)
        {
            var result = new Message();
            try
            {
                List<BM_ComplianceScheduleAssignment> lstUsers = null;

                var lstComplianceIdForScheduleOn = (from row in entities.BM_SP_GetComplianceIdForScheduleSecretarial(entityId, meetingId, mappingType, customerId)
                                                    select new ComplianceScheduleOnVM
                                                    {
                                                        ScheduleOnID = 0,
                                                        MCM_ID = row.MCM_ID,
                                                        AgendaMasterId = row.AgendaMasterID,
                                                        ComplianceId = row.ComplianceID,
                                                        MeetingAgendaMappingId = (long)row.MeetingAgendaMappingId
                                                    }).ToList();

                if (lstComplianceIdForScheduleOn != null)
                {
                    foreach (var item in lstComplianceIdForScheduleOn)
                    {
                        lstUsers = null;
                        var complianceScheduleOn = new BM_ComplianceScheduleOn
                        {
                            MeetingID = meetingId,
                            MappingType = mappingType,
                            MCM_ID = item.MCM_ID,
                            AgendaMasterID = item.AgendaMasterId,
                            ComplianceID = item.ComplianceId,
                            CreatedBy = createdBy,
                            CreatedOn = DateTime.Now,
                            IsActive = true,
                            IsDeleted = false,
                            EntityId = entityId,
                            Customer_Id = customerId
                        };

                        entities.BM_ComplianceScheduleOn.Add(complianceScheduleOn);
                        entities.SaveChanges();

                        if (complianceScheduleOn.ScheduleOnID > 0)
                        {
                            #region Get Performer/Reviewer list
                            var lstAssignment = objICompliance_Service.GetAssignment(entityId, complianceScheduleOn.ComplianceID);
                            if(lstAssignment !=null)
                            {
                                lstUsers = lstAssignment.Select(row => new BM_ComplianceScheduleAssignment
                                {
                                    ScheduleOnID = complianceScheduleOn.ScheduleOnID,
                                    RoleID = row.RoleID,
                                    UserID = row.UserID,
                                    IsDeleted = false,
                                    CreatedBy = createdBy,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                            }

                            #region Insert Performer / Reviewer Default
                            if (lstUsers == null)
                            {
                                lstUsers = new List<BM_ComplianceScheduleAssignment>();
                            }

                            if(lstUsers.Where(k=>k.RoleID == SecretarialConst.RoleID.PERFORMER).Any() == false)
                            {
                                lstUsers.Add(new BM_ComplianceScheduleAssignment()
                                {
                                    ScheduleOnID = complianceScheduleOn.ScheduleOnID,
                                    RoleID = SecretarialConst.RoleID.PERFORMER,
                                    IsDeleted = false,
                                    CreatedBy = createdBy,
                                    CreatedOn = DateTime.Now,
                                });
                            }

                            if (lstUsers.Where(k => k.RoleID == SecretarialConst.RoleID.REVIEWER).Any() == false)
                            {
                                lstUsers.Add(new BM_ComplianceScheduleAssignment()
                                {
                                    ScheduleOnID = complianceScheduleOn.ScheduleOnID,
                                    RoleID = SecretarialConst.RoleID.REVIEWER,
                                    IsDeleted = false,
                                    CreatedBy = createdBy,
                                    CreatedOn = DateTime.Now,
                                });
                            }
                            #endregion

                            #endregion

                            entities.BM_ComplianceScheduleAssignment.AddRange(lstUsers);
                            entities.SaveChanges();
                        }
                    }
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Something went wrong.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }
        #endregion

        #region Get Meeting -> Compliance Performer / Reviewer
        public List<MeetingComplianceScheduleDetails_ResultVM> GetMeetingComplianceScheduleAssignment(long meetingId, string MappingType)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetMeetingComplianceScheduleAssignment(meetingId)
                              select new MeetingComplianceScheduleDetails_ResultVM
                              {
                                  AssignmentID = row.AssignmentID,
                                  MeetingID = row.MeetingID,
                                  MappingType = row.MappingType,
                                  PartId = row.PartId,
                                  SrNo = row.SrNo,
                                  ScheduleOnID = row.ScheduleOnID,
                                  ScheduleOn = row.ScheduleOn,
                                  AgendaItemHeading = row.AgendaItemHeading,
                                  ShortDescription = row.ShortDescription,
                                  RoleID = row.RoleID,
                                  Role = row.Role,
                                  UserID = row.UserID,
                                  UserName = row.UserName,
                                  User = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.UserID,
                                      FullName = row.UserName
                                  }
                              });

                if (MappingType == SecretarialConst.ComplianceMappingType.MEETING || MappingType == SecretarialConst.ComplianceMappingType.AGENDA)
                {
                    result = result.Where(k => k.MappingType == MappingType);
                }
                return result.ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Meeting -> Compliance ReAssignment
        public List<MeetingComplianceScheduleDetails_ResultVM> MeetingComplianceScheduleReAssignment(List<MeetingComplianceScheduleDetails_ResultVM> lstUsers, int updatedBy)
        {
            try
            {
                foreach (var item in lstUsers)
                {
                    if (item.User != null)
                    {
                        if (item.User.UserID > 0)
                        {
                            var obj = entities.BM_ComplianceScheduleAssignment.Where(k => k.AssignmentID == item.AssignmentID).FirstOrDefault();
                            if (obj != null)
                            {
                                if (obj.UserID == 0)
                                {

                                }
                                obj.UserID = item.User.UserID;
                                obj.UpdatedBy = updatedBy;
                                obj.UpdatedOn = DateTime.Now;
                                entities.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return lstUsers;
        }
        #endregion

        #region Meeting -> Compliance schedule
        public List<MeetingComplianceScheduleDetails_ResultVM> GetMeetingComplianceSchedule(long meetingId)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetMeetingComplianceScheduleSecretarial(meetingId)
                              select new MeetingComplianceScheduleDetails_ResultVM
                              {
                                  MappingType = row.MappingType,
                                  ScheduleOnID = row.ScheduleOnID,
                                  ScheduleOn = row.ScheduleOn,
                                  ScheduleOnTime = row.ScheduleOnTime,
                                  ClosedDate = row.ClosedDate,
                                  ClosedDateTime = row.ClosedDateTime,
                                  StatusId = row.StatusId,
                                  Status = row.ComplianceStatus,
                                  AgendaItemHeading = row.AgendaItemHeading,
                                  ShortDescription = row.ShortDescription,
                                  Entity_ID = row.EntityId,
                                  MeetingID = row.MeetingID,
                                  MeetingAgendaMappingID = row.MeetingAgendaMappingId,
                                  ComplianceID = row.ComplianceId
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public List<MeetingComplianceScheduleDetails_ResultVM> GetMeetingComplianceSchedule(long meetingId, string mappingType, bool showEvent)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetMeetingComplianceScheduleSecretarial(meetingId)
                              select new MeetingComplianceScheduleDetails_ResultVM
                              {
                                  MappingType = row.MappingType,
                                  ScheduleOnID = row.ScheduleOnID,
                                  ScheduleOn = row.ScheduleOn,
                                  ScheduleOnTime = row.ScheduleOnTime,
                                  ClosedDate = row.ClosedDate,
                                  ClosedDateTime = row.ClosedDateTime,
                                  StatusId = row.StatusId,
                                  Status = row.ComplianceStatus,
                                  AgendaItemHeading = row.AgendaItemHeading,
                                  ShortDescription = row.ShortDescription,
                                  Entity_ID = row.EntityId,
                                  MeetingID = row.MeetingID,
                                  MeetingAgendaMappingID = row.MeetingAgendaMappingId,
                                  ComplianceID = row.ComplianceId
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public ComplianceScheduleDetailsVM GetScheduleDetails(long scheduleOnID, int entityID, int customerID)
        {
            try
            {
                var result = (from schedule in entities.BM_ComplianceScheduleOn
                              join mapping in entities.BM_ComplianceMapping on schedule.MCM_ID equals mapping.MCM_ID
                              join compliance in entities.Compliances on schedule.ComplianceID equals compliance.ID
                              where schedule.ScheduleOnID == scheduleOnID && schedule.IsActive == true
                              && schedule.EntityId == entityID && schedule.Customer_Id == customerID
                              select new ComplianceScheduleDetailsVM
                              {
                                  ScheduleOnID = schedule.ScheduleOnID,
                                  ScheduleOn = schedule.ScheduleOn,
                                  Meeting_ID = schedule.MeetingID,
                                  AgendaMasterId = schedule.AgendaMasterID,
                                  MappingType = schedule.MappingType,
                                  MCM_ID = schedule.MCM_ID,
                                  DaysOrHours = mapping.DaysOrHours,
                                  ComplianceId = schedule.ComplianceID,
                                  EntityId = entityID,

                                  Compliance = new ComplianceDetailsVM()
                                  {
                                      ActID = compliance.ActID,
                                      ComplianceId = compliance.ID,
                                      Description = compliance.ShortDescription,
                                      DetailedDescription = compliance.Description,
                                      RiskType = compliance.RiskType,
                                      SampleFormLink = compliance.SampleFormLink
                                  }
                              }).FirstOrDefault();

                if (result != null)
                {
                    result.Act = (from act in entities.Acts
                                  where act.ID == result.Compliance.ActID
                                  select new ActVM
                                  {
                                      ActId = act.ID,
                                      ActName = act.Name
                                  }).FirstOrDefault();

                    result.TransactionLog = (from log in entities.BM_ComplianceTransaction
                                             from status in entities.ComplianceStatus.Where(k => k.ID == log.StatusId).DefaultIfEmpty()
                                             where log.ComplianceScheduleOnID == result.ScheduleOnID
                                             orderby log.TransactionID descending
                                             select new ComplianceTransactionLogVM
                                             {
                                                 TransactionID = log.TransactionID,
                                                 StatusId = log.StatusId,
                                                 Status = status == null ? "" : status.ID == 1 ? result.ScheduleOn > DateTime.Now ? "Upcoming" : "Overdue" : status.Name,
                                                 Remarks = log.Remarks,
                                                 Dated = log.Dated,
                                                 CreatedBy = log.CreatedBy,
                                                 CreatedByText = log.CreatedByText,
                                                 StatusChangedOn = log.StatusChangedOn
                                             }).ToList();

                    result.MeetingTitle = (from row in entities.BM_Meetings
                                           where row.MeetingID == result.Meeting_ID
                                           select row.MeetingTitle).FirstOrDefault();
                    
                    if(result.AgendaMasterId != null)
                    {
                        result.AgendaItem = (from row in entities.BM_MeetingAgendaMapping
                                               where row.MeetingID == result.Meeting_ID && row.AgendaID == result.AgendaMasterId
                                               select row.AgendaItemTextNew).FirstOrDefault();
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public ComplianceScheduleDetailsVM GetScheduleDetailsNew(long scheduleOnID, int userID)
        {
            throw new NotImplementedException();
        }
        public List<FileDataDocumentVM> GetComplianceDocuments(long scheduleOnID)
        {
            try
            {
                var result = from row in entities.BM_FileDataMapping
                             join fileData in entities.BM_FileData on row.FileID equals fileData.Id
                             from u in entities.Users.Where(k => k.ID == fileData.UploadedBy).DefaultIfEmpty()
                             where row.ScheduledOnID == scheduleOnID
                             select new FileDataDocumentVM
                             {
                                 FileID = fileData.Id,
                                 FileName = fileData.FileName,
                                 Version = fileData.Version,
                                 UploadedBy = u == null ? "" : u.FirstName + " " + u.LastName
                             };
                return result.ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Meeting Compliance  Generate Schedule date
        public Message CalculateScheduleDate(long meetingID, int updatedBy)
        {
            var result = new Message();
            try
            {
                entities.BM_SP_CalculateScheduleOnForMeetingSecretarial(meetingID, updatedBy);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Server Error occured.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Director Schedule on
        public Message GenerateDirectorScheduleOn(long directorId, string generateScheduleFor, DateTime closedDate, int customerId, int createdBy, string userName)
        {
            throw new NotImplementedException();
        }
        #endregion

        public Message UpdateEventCalendarDate(MeetingComplianceScheduleDetails_ResultVM obj, int updatedBy)
        {
            throw new NotImplementedException();
        }

        public Message DeleteShceduleOnById(long id, int updatedBy)
        {
            throw new NotImplementedException();
        }
        public List<tbl_ChecklistRemark> GetAllRemark()
        {
            throw new NotImplementedException(); 
        }
    }
}