﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using System.Reflection;
using System.Text.RegularExpressions;

namespace BM_ManegmentServices.Services.Meetings
{
    public class AgendaMinutesReviewService : IAgendaMinutesReviewService
    {
        #region Agenda - Performer/Reviewer
        public AgendaReviewVM GetAgendaReviewDetails(long mappingId, long taskId, long assignmentId, long agendaReviewId)
        {
            var result = new AgendaReviewVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_SP_MeetingAgendaItemReview(mappingId, taskId, assignmentId, agendaReviewId)
                              select new AgendaReviewVM
                              {
                                  MeetingAgendaMappingID = row.MeetingAgendaMappingID,
                                  //Meeting_Id = row.Mee,
                                  AgendaFormatHeading = row.AgendaItemTextNew,
                                  AgendaFormat = row.AgendaFormat,
                                  ResolutionFormat = row.ResolutionFormat,
                                  AgendaReviewId = agendaReviewId,
                                  TaskId = taskId,
                                  TaskAssignmentId = assignmentId
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public AgendaReviewVM SaveAgendaReviewDeails(AgendaReviewVM obj, bool canAcceptChanges, int createdBy)
        {
            try
            {
                bool hasChanges = false, hasComments = false;
                var tempFormat = (string.IsNullOrEmpty(obj.AgendaFormat) ? "" : obj.AgendaFormat) + (string.IsNullOrEmpty(obj.ResolutionFormat) ? "" : obj.ResolutionFormat);
                var match = Regex.Match(tempFormat, "<ins", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    hasChanges = true;
                }
                else
                {
                    match = Regex.Match(tempFormat, "<del", RegexOptions.IgnoreCase);
                    if (match.Success)
                    {
                        hasChanges = true;
                    }
                }

                match = Regex.Match(tempFormat, "reComment", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    hasComments = true;
                }

                BM_MeetingAgendaMinutesReviewDetails _obj = null;
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (obj.AgendaReviewId > 0)
                    {
                        _obj = (from row in entities.BM_MeetingAgendaMinutesReviewDetails
                                where row.Id == obj.AgendaReviewId && row.IsDeleted == false && row.IsActive == true
                                select row).FirstOrDefault();
                    }
                    else
                    {
                        _obj = (from row in entities.BM_MeetingAgendaMinutesReviewDetails
                                where row.TaskAssignmentId == obj.TaskAssignmentId && row.MeetingAgendaMappingID == obj.MeetingAgendaMappingID && row.IsDeleted == false && row.IsActive == true
                                select row).FirstOrDefault();
                    }

                    if (_obj == null)
                    {
                        _obj = new BM_MeetingAgendaMinutesReviewDetails();
                        _obj.TaskInstanceId = obj.TaskId;
                        _obj.TaskAssignmentId = obj.TaskAssignmentId;
                        _obj.MeetingAgendaMappingID = obj.MeetingAgendaMappingID;
                        _obj.AgendaMinutesFormat = obj.AgendaFormat;
                        _obj.ResolutionFormat = obj.ResolutionFormat;
                        _obj.HasChanges = hasChanges;
                        _obj.HasComment = hasComments;
                        _obj.IsChangesDone = false;
                        _obj.IsActive = true;
                        _obj.IsDeleted = false;

                        _obj.CreatedBy = createdBy;
                        _obj.CreatedOn = DateTime.Now;

                        entities.BM_MeetingAgendaMinutesReviewDetails.Add(_obj);
                        entities.SaveChanges();

                        obj.AgendaReviewId = _obj.Id;
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;

                    }
                    else
                    {
                        _obj.AgendaMinutesFormat = obj.AgendaFormat;
                        _obj.ResolutionFormat = obj.ResolutionFormat;

                        if (canAcceptChanges && _obj.HasChanges && hasChanges == false)
                        {
                            _obj.IsChangesDone = true;
                        }

                        _obj.HasChanges = hasChanges;
                        _obj.HasComment = hasComments;
                        _obj.UpdatedBy = createdBy;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.updateSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public Message MergeChanges(long taskId, long taskAssignmentId, string remark, int mergedBy)
        {
            var result = new Message();
            try
            {
                long meetingId = 0;
                if(string.IsNullOrEmpty(remark))
                {
                    remark = "";
                }
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_MeetingAgendaMinutesReviewDetails
                                where row.TaskInstanceId == taskId && row.TaskAssignmentId == taskAssignmentId &&
                                row.IsDeleted == false && row.IsActive == true && row.IsChangesDone == true
                                select row).ToList();
                    if(_obj != null)
                    {
                        foreach (var item in _obj)
                        {
                            var _objMeetingAgendaMapping = (from row in entities.BM_MeetingAgendaMapping
                                                            where row.MeetingAgendaMappingID == item.MeetingAgendaMappingID && row.IsDeleted == false && row.IsActive == true
                                                            select row).FirstOrDefault();
                            if(_objMeetingAgendaMapping != null)
                            {
                                meetingId = _objMeetingAgendaMapping.MeetingID;
                                _objMeetingAgendaMapping.AgendaFormat = item.AgendaMinutesFormat;
                                _objMeetingAgendaMapping.ResolutionFormat = item.ResolutionFormat;
                                _objMeetingAgendaMapping.UpdatedBy = mergedBy;
                                _objMeetingAgendaMapping.UpdatedOn = DateTime.Now;
                                entities.SaveChanges();

                                item.IsChangesDone = false;
                                item.UpdatedBy = mergedBy;
                                item.UpdatedOn = DateTime.Now;
                                entities.SaveChanges();

                            }
                        }

                        #region Update task Status
                        var _objAssignment = (from row in entities.BM_TaskAssignment
                                    where row.ID == taskAssignmentId && row.IsActive == true
                                    select row).FirstOrDefault();
                        if (_obj != null)
                        {

                            _objAssignment.StatusId = SecretarialConst.TaskStaus.COMPLETED;
                            _objAssignment.Status = "Closed";
                            _objAssignment.Remark = remark;
                            _objAssignment.Updatedby = mergedBy;
                            _objAssignment.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();

                            var _objTask = (from row in entities.BM_Task
                                            where row.ID == taskId && row.IsActive == true
                                            select row).FirstOrDefault();
                            if (_objTask != null)
                            {
                                _objTask.StatusId = SecretarialConst.TaskStaus.COMPLETED;
                                _objTask.status = "Closed";
                                _objTask.Updatedby = mergedBy;
                                _objTask.UpdatedOn = DateTime.Now;
                                entities.SaveChanges();
                            }

                            #region task transaction
                            var _objTaskTransaction = new BM_TaskTransaction();
                            _objTaskTransaction.TaskID = taskId;
                            _objTaskTransaction.StatusId = SecretarialConst.TaskStaus.COMPLETED;
                            _objTaskTransaction.Status = "Closed";
                            _objTaskTransaction.Comment = remark;
                            _objTaskTransaction.UserID = mergedBy;
                            _objTaskTransaction.RoleID = SecretarialConst.RoleID.PERFORMER;
                            _objTaskTransaction.IsActive = true;
                            _objTaskTransaction.StatusChangeOn = DateTime.Now;
                            _objTaskTransaction.CreatedBy = mergedBy;
                            _objTaskTransaction.CreatedOn = DateTime.Now;
                            entities.BM_TaskTransaction.Add(_objTaskTransaction);
                            entities.SaveChanges();
                            #endregion

                            result.Success = true;
                            result.Message = SecretarialConst.Messages.updateSuccess;
                        }
                        else
                        {
                            result.Error = true;
                            result.Message = SecretarialConst.Messages.serverError;
                        }
                        #endregion

                        var agendaFlag = (from row in entities.BM_SP_MeetingAgendaSetGenerationFlag(meetingId)
                                      select row);

                        result.Success = true;
                        result.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    else
                    {
                        result.Error = true;
                        result.Message = SecretarialConst.Messages.serverError;
                    }
                }

            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public Message ReSendChangesForReview(long taskId, long taskAssignmentId, string remark, int updatedBy, out string mailFormat)
        {
            var result = new Message();
            mailFormat = "";
            long? meetingId = null;
            try
            {
                if (string.IsNullOrEmpty(remark))
                {
                    remark = "";
                }
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_TaskAssignment
                                where row.ID == taskAssignmentId && row.IsActive == true
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {

                        _obj.StatusId = SecretarialConst.TaskStaus.PENDING_FOR_REVIEW;
                        _obj.Status = "Pending For Review";
                        _obj.Remark = remark;
                        _obj.Updatedby = updatedBy;
                        _obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();

                        var _objTask = (from row in entities.BM_Task
                                        where row.ID == taskId && row.IsActive == true
                                        select row).FirstOrDefault();
                        if(_objTask != null)
                        {
                            _objTask.StatusId = SecretarialConst.TaskStaus.PENDING_FOR_REVIEW;
                            _objTask.status = "Pending For Review";
                            _objTask.Updatedby = updatedBy;
                            _objTask.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();

                            meetingId = _objTask.MeetingId;
                        }

                        #region task transaction
                        var _objTaskTransaction = new BM_TaskTransaction();
                        _objTaskTransaction.TaskID = taskId;
                        _objTaskTransaction.StatusId = SecretarialConst.TaskStaus.PENDING_FOR_REVIEW;
                        _objTaskTransaction.Status = "Pending For Review";
                        _objTaskTransaction.Comment = remark;
                        _objTaskTransaction.UserID = updatedBy;
                        _objTaskTransaction.RoleID = SecretarialConst.RoleID.PERFORMER;
                        _objTaskTransaction.IsActive = true;
                        _objTaskTransaction.StatusChangeOn = DateTime.Now;
                        _objTaskTransaction.CreatedBy = updatedBy;
                        _objTaskTransaction.CreatedOn = DateTime.Now;
                        entities.BM_TaskTransaction.Add(_objTaskTransaction);
                        entities.SaveChanges();
                        #endregion

                        #region Get Mail template
                        if (meetingId == null)
                        {
                            meetingId = (from row in entities.BM_Task
                                         where row.ID == taskId && row.IsActive == true
                                         select row.MeetingId).FirstOrDefault();
                        }
                        mailFormat = (from row in entities.BM_SP_GenerateAgendaMinuteReviewMail(meetingId, taskId, taskAssignmentId, SecretarialConst.MeetingTemplatType.AGENDA_REVIEW_ASSIGNED)
                                      select row).FirstOrDefault();
                        #endregion
                        result.Success = true;
                        result.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    else
                    {
                        result.Error = true;
                        result.Message = SecretarialConst.Messages.serverError;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        public PreviewAgendaVM PreviewAgenda(long MeetingId, int CustomerId, long? taskAssignmentId)
        {
            return Preview(MeetingId, CustomerId, false, taskAssignmentId);
        }
        public PreviewAgendaVM PreviewMOM(long MeetingId, int CustomerId)
        {
            return Preview(MeetingId, CustomerId, true, null);
        }
        public PreviewAgendaVM Preview(long MeetingId, int CustomerId, bool GenerateMinutes, long? taskAssignmentId)
        {
            var result = new PreviewAgendaVM();
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                result = (from row in entities.BM_Meetings
                          join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                          join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                          join fy in entities.BM_YearMaster on row.FY equals fy.FYID
                          from city in entities.Cities.Where(k => k.ID == entity.Regi_CityId).Select(k => k.Name).DefaultIfEmpty()
                          from state in entities.States.Where(k => k.ID == entity.Regi_StateId).Select(k => k.Name).DefaultIfEmpty()
                          where row.MeetingID == MeetingId && row.Customer_Id == CustomerId && row.IsDeleted == false
                          select new PreviewAgendaVM
                          {
                              MeetingID = row.MeetingID,
                              IsVirtual = row.IsVirtualMeeting,
                              IsEVoting = row.IsEVoting,
                              GenerateMinutes = GenerateMinutes,
                              MeetingSrNo = row.MeetingSrNo,
                              MeetingCircular = row.MeetingCircular,
                              MeetingTypeId = row.MeetingTypeId,
                              MeetingTypeName = meeting.MeetingTypeName,
                              Quarter_ = row.Quarter_,
                              MeetingTitle = row.MeetingTitle,
                              NoticeDate = row.SendDateNotice,
                              CircularDate = row.CircularDate,
                              MeetingDate = row.MeetingDate,
                              MeetingTime = row.MeetingTime,
                              MeetingAddressType = row.MeetingVenueType,
                              MeetingVenue = row.MeetingVenue,

                              MeetingStartDate = row.StartMeetingDate,
                              MeetingStartTime = row.StartTime,
                              MeetingEndDate = row.EndMeetingDate,
                              MeetingEndTime = row.EndMeetingTime,

                              GM_Notes = row.Notes,
                              TotalMemberPresentInAGM = row.TotalMemberPresentInAGM,
                              TotalProxyPresentInAGM = row.TotalProxyPresentInAGM,

                              IsAdjourned = row.IsAdjourned,

                              Entityt_Id = row.EntityId,
                              EntityCIN_LLPIN = entity.CIN_LLPIN,
                              EntityName = entity.CompanyName,
                              EntityAddressLine1 = entity.Regi_Address_Line1,
                              EntityAddressLine2 = entity.Regi_Address_Line2,
                              EntityCity = city == null ? "" : city,
                              EntityState = state == null ? "" : state,
                              FYText = fy.FYText
                          }).FirstOrDefault();

                if (result != null)
                {
                    result.lstAgendaItems = (from TemplateList in entities.BM_SP_MeetingAgendaTemplateList(MeetingId, null, GenerateMinutes, taskAssignmentId)
                                             select new MeetingAgendaMappingVM
                                             {
                                                 SrNo = (int)TemplateList.SrNo,
                                                 MeetingAgendaMappingID = (long)TemplateList.MeetingAgendaMappingID,
                                                 Meeting_Id = TemplateList.MeetingID,
                                                 AgendaID = TemplateList.BM_AgendaMasterId,
                                                 PartId = TemplateList.PartID,
                                                 AgendaItemHeading = TemplateList.AgendaItemHeading,
                                                 AgendaItemText = TemplateList.AgendaItemText,

                                                 AgendaFormat = TemplateList.AgendaFormat,
                                                 ExplanatoryStatement = TemplateList.ExplanatoryStatement,
                                                 IsOrdinaryBusiness = TemplateList.IsOrdinaryBusiness,
                                                 IsSpecialResolution = TemplateList.IsSpecialResolution
                                             }).ToList();

                    if (GenerateMinutes)
                    {
                        var minutesDetails = (from row in entities.BM_MeetingMinutesDetails
                                              where row.MeetingId == MeetingId && row.IsDeleted == false
                                              select new
                                              {
                                                  row.TotalMemberPresentInAGM,
                                                  row.TotalProxyPresentInAGM,
                                                  row.TotalMemberInAGM,

                                                  //row.TotalValidProxy,
                                                  //row.TotalMemberProxy,
                                                  //row.TotalShareHeldByProxy,
                                                  //row.FaceValueOfEnquityShare,
                                                  //row.PercentageOfIssuedCapital
                                              }).FirstOrDefault();

                        if (minutesDetails != null)
                        {
                            result.TotalMemberPresentInAGM = minutesDetails.TotalMemberPresentInAGM;
                            result.TotalProxyPresentInAGM = minutesDetails.TotalProxyPresentInAGM;
                            result.TotalMemberInAGM = minutesDetails.TotalMemberInAGM;

                            //result.TotalValidProxy = minutesDetails.TotalValidProxy;
                            //result.TotalMemberProxy = minutesDetails.TotalMemberProxy;
                            //result.TotalShareHeldByProxy = minutesDetails.TotalShareHeldByProxy;
                            //result.FaceValueOfEnquityShare = minutesDetails.FaceValueOfEnquityShare;
                            //result.PercentageOfIssuedCapital = minutesDetails.PercentageOfIssuedCapital;
                        }

                        if (result.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                        {
                            var hasConsolidatedFinancial = (from row in entities.BM_Meetings
                                                            join mapping in entities.BM_MeetingAgendaMapping on row.MeetingID equals mapping.MeetingID
                                                            join agenda in entities.BM_AgendaMaster on mapping.AgendaID equals agenda.BM_AgendaMasterId
                                                            where row.MeetingID == MeetingId && mapping.IsDeleted == false && mapping.IsActive == true &&
                                                            agenda.DefaultAgendaFor == SecretarialConst.DefaultAgenda.AUDITED_CONSOLIDATED_FINANCIAL_STATEMENTS
                                                            select row.MeetingID).Any();
                            result.HasConsolidatedFinancial = hasConsolidatedFinancial;
                        }
                    }
                    else if (result.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || result.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                    {
                        result.MeetingSigningAuthorityForAGM = (from row in entities.BM_SP_MeetingSigningAuthorityForAGM(result.MeetingID, result.Entityt_Id)
                                                                select new MeetingSigningAuthorityForAGM_VM
                                                                {
                                                                    AuthorityName = row.AuthorityName,
                                                                    Designation = row.Designation,
                                                                    DIN_PAN = row.DIN_PAN,
                                                                    MembershipNo = row.MembershipNo,
                                                                    IsCS = row.IsCS,
                                                                    IsChairman = row.IsChairman,
                                                                    IsAuthorisedSignotory = row.IsAuthorisedSignotory
                                                                }).FirstOrDefault();
                    }
                }
            }
            return result;
        }
        #region Agenda, Documents review
        public List<AgendaAndDocumentReviewVM> AgendaDocumentReview(long meetingId, long taskId, long assignmentId)
        {
            var result = new List<AgendaAndDocumentReviewVM>();
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                result = (from row in entities.BM_SP_MeetingAgendaReview(meetingId, taskId, assignmentId)
                          select new AgendaAndDocumentReviewVM
                          {
                              Meeting_Id = meetingId,
                              ID = row.Id,
                              AgendaReviewId = row.AgendaReviewId,
                              ParentID = row.ParentId,
                              Category = row.Category,
                              AgendaOrFileName = row.AgnedaOrFile,
                              HasParent = row.HasParent,
                              hasChildren = !row.HasParent,
                              ItemNo = row.ItemNo,
                              HasChanges = row.HasChanges,
                              HasComment = row.HasComment,
                              TaskId = row.TaskId,
                              TaskAssignmentId = row.TaskAssignmentId
                          }).ToList();
            }
            return result;
        }
        public Message SetDocumentOrder(List<FileSrNoVM> obj, long meetingId, int updatedBy)
        {
            var result = new Message();
            try
            {
                if (obj != null)
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        foreach (var item in obj)
                        {
                            var _obj = (from row in entities.BM_FileData
                                        where row.Id == item.FileID && row.IsDeleted == false
                                        select row).FirstOrDefault();
                            if (_obj != null)
                            {
                                _obj.FileSrNo = item.SrNo;
                                _obj.UploadedBy = updatedBy;
                                _obj.UploadedOn = DateTime.Now;
                                entities.SaveChanges();
                            }
                        }
                        //var agendaFlag = (from row in entities.BM_SP_MeetingAgendaSetGenerationFlag(meetingId)
                        //              select row);
                    }
                }

                result.Success = true;
                result.Message = SecretarialConst.Messages.saveSuccess;
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<VM_AgendaDocument> GetAgendaDocument(long agendaMappingID, int customerId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getAgendaDocument = (from row in entities.BM_FileData
                                             where row.AgendaMappingId == agendaMappingID
                                             && row.IsDeleted == false
                                             orderby row.FileSrNo
                                             select new VM_AgendaDocument
                                             {
                                                 ID = row.Id,
                                                 FileName = row.FileName
                                             }
                                   ).ToList();
                    return getAgendaDocument;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region AgendaMaster check format is valid or not
        public List<AgendaReviewVM> GetAgenda(int c)
        {
            var result = new List<AgendaReviewVM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_AgendaMaster
                              where row.IsDeleted == false && row.BM_AgendaMasterId > c
                              orderby row.BM_AgendaMasterId
                              select new AgendaReviewVM
                              {
                                  AgendaID = row.BM_AgendaMasterId,
                                  AgendaFormatHeading = row.AgendaItemHeading,
                                  AgendaFormat = row.AgendaFormat,
                                  ResolutionFormat = row.ResolutionFormat,
                                  MinutesApproveFormat = row.MinutesFormat,
                                  MinutesDeferFormat = row.MinutesDifferFormat,
                                  MinutesDisApproveFormat = row.MinutesDisApproveFormat
                              }
                            ).Take(100).ToList();
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        #endregion

        #region Attendance
        public List<MeetingAttendance_VM> GetPerticipenforAttendenceCS(long meetingID, int customerId, int? userId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getParticipant = (from row in entities.BM_SP_GetParticipantAttendance(meetingID)
                                          select new MeetingAttendance_VM
                                          {
                                              ParticipantId = row.MeetingParticipantId,
                                              MeetingParticipant_Name = row.Participant_Name,
                                              MeetingId = row.Meeting_ID,
                                              Director_Id = row.Director_Id,
                                              ImagePath = row.Photo_Doc,
                                              Designation = row.Designation,
                                              Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                                              Remark = row.Reason,
                                              Position = row.Position,
                                              RSVP = row.RSVP,
                                              RSVP_Reason = row.RSVP_Reason,
                                              ParticipantType = row.ParticipantType
                                          }).ToList();
                    return getParticipant;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public List<MeetingAttendance_VM> GetOtherParticipentAttendence(long meetingID)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getParticipant = (from row in entities.BM_SP_GetParticipantAttendanceAllType(meetingID)
                                          select new MeetingAttendance_VM
                                          {
                                              ParticipantId = row.MeetingParticipantId,
                                              MeetingParticipant_Name = row.ParticipantName,
                                              MeetingId = meetingID,
                                              Director_Id = row.MasterId,
                                              Designation = row.Designation,
                                              Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                                              Remark = row.Reason,
                                              ParticipantType = row.ParticipantType,
                                              Representative = row.Representative
                                          }).ToList();
                    return getParticipant;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Minutes Details
        public MeetingMinutesDetailsVM GetMinutesDetails(long meetingId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var result = (from row in entities.BM_MeetingMinutesDetails
                                  where row.MeetingId == meetingId
                                  select new MeetingMinutesDetailsVM
                                  {
                                      MeetingMinutesDetailsId = row.Id,
                                      MOM_MeetingId = row.MeetingId,
                                      Place = row.Place,
                                      DateOfEntering = row.DateOfEntering,
                                      DateOfSigning = row.SigningOfEntering,
                                      IsCirculate = row.IsCirculate,
                                      IsFinalized = row.IsFinalized,
                                      ConfirmMeetingId = row.ConfirmMeetingId
                                  }).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<AgendaAndDocumentReviewVM> DraftMinutesReview(long meetingId, long taskId, long assignmentId)
        {
            var result = new List<AgendaAndDocumentReviewVM>();
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                result = (from row in entities.BM_SP_MeetingAgendaTemplateList(meetingId, null, true, assignmentId)
                          select new AgendaAndDocumentReviewVM
                          {
                              Meeting_Id = meetingId,
                              ID = row.MeetingAgendaMappingID,
                              //AgendaReviewId = row.AgendaReviewId,
                              //ParentID = row.ParentId,
                              Category = "A",
                              AgendaOrFileName = row.AgendaItemText,
                              HasParent = false,
                              hasChildren = true,
                              ItemNo = row.SrNo,
                              HasChanges = false,
                              HasComment = false,
                              TaskId = 0,
                              TaskAssignmentId = assignmentId
                          }).ToList();
            }
            return result;
        }
        #endregion
    }
}