﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Services.Masters;
using System.Reflection;
using BM_ManegmentServices.Data;
using System.Globalization;
using System.Web.Mvc;

namespace BM_ManegmentServices.Services.AnnualMeetingCalendar
{
    public class AnnualCalender : IAnnualCalender
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public AnnualMeetingVM CreateAnnualMeeting(AnnualMeetingVM _objAnnualMeeting, int userId, int customerId)
        {
            if (_objAnnualMeeting.MeetingDate != null)
            {
                DateTime dTCurrent = Convert.ToDateTime(_objAnnualMeeting.MeetingDate);
                string date = dTCurrent.ToString("yyyy-MM-dd");
                var FDate = DateTime.ParseExact(date, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                var checkdate = (from x in entities.BM_AnnualMeetings
                                 where x.EntityId == _objAnnualMeeting.EntityId
                                 && x.Customer_Id == customerId
                                 && x.FY == _objAnnualMeeting.FYID
                                 && x.MeetingDate == _objAnnualMeeting.MeetingDate
                                 select x).FirstOrDefault();
            }

            try
            {
                var checkMeetingexist = (from x in entities.BM_AnnualMeetings
                                         where x.EntityId == _objAnnualMeeting.EntityId
                                         && x.Customer_Id == customerId
                                         && x.FY == _objAnnualMeeting.FYID
                                         && x.MeetingSrNo == _objAnnualMeeting.MeetingSRNo
                                         && x.MeetingDate == _objAnnualMeeting.MeetingDate
                                         select x).FirstOrDefault();

                if (checkMeetingexist == null)
                {
                    BM_AnnualMeetings _objAM = new BM_AnnualMeetings
                    {
                        EntityId = _objAnnualMeeting.EntityId,
                        MeetingTypeId = _objAnnualMeeting.MeetingTypeId,
                        FY = _objAnnualMeeting.FYID,
                        MeetingDate = _objAnnualMeeting.MeetingDate,
                        Customer_Id = customerId,
                        MeetingCircular = "M",
                        IsDeleted = false,
                        CreatedBy = userId,
                        CreatedOn = DateTime.Now,
                    };

                    entities.BM_AnnualMeetings.Add(_objAM);
                    entities.SaveChanges();

                    _objAnnualMeeting.Success = true;
                    _objAnnualMeeting.Message = SecretarialConst.Messages.saveSuccess;
                }
                else
                {
                    _objAnnualMeeting.Error = true;
                    _objAnnualMeeting.Message = "Same Record already exist";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objAnnualMeeting.Error = true;
                _objAnnualMeeting.Message = SecretarialConst.Messages.serverError;
            }
            return _objAnnualMeeting;
        }



        public virtual List<AnnualMeetingSheduler> GetAnnualMeeting(int CustomerId, int UserID, string Role)
        {
            List<AnnualMeetingSheduler> _objAnnualMeeting = new List<AnnualMeetingSheduler>();
            _objAnnualMeeting = (from x in entities.BM_SP_GetAnnualMeetings(CustomerId, UserID, Role)
                                 select new AnnualMeetingSheduler
                                 {
                                     ID = x.MeetingId,
                                     EntityName = x.EntityName,
                                     EntityId = x.EntityId,
                                     MeetingDate=x.MeetingDate,
                                     MeetingTypeId = Convert.ToInt32(x.MeetingTypeId),
                                     // Start = Convert.ToDateTime(x.MeetingDate),
                                     Start = Convert.ToDateTime(x.StartMeetingDate),
                                     End = Convert.ToDateTime(x.EndMeetingDate),
                                     MeetingStartTime=x.MeetingTime,
                                     MeetingEndTime=x.MeetingEndTime,
                                     FY = x.FYText,
                                     FYID = Convert.ToInt32(x.FYID),
                                     MeetingTypeName = x.MeetingTypeName,
                                     MeetingSRNo = x.MeetingSrNo
                                     //MeetingTitle = Convert.ToString(x.MeetingTypeName + "[" + x.MeetingSrNo != null ? x.MeetingSrNo : "#" + "]" +x.FYText);
                                 }).ToList();

            if (_objAnnualMeeting.Count > 0)
            {
                string srno = string.Empty;

                foreach (var item in _objAnnualMeeting)
                {
                    if (item.MeetingSRNo != null)
                    {
                        srno = Convert.ToString(item.MeetingSRNo);
                    }
                    else
                    {
                        srno = "#";
                    }

                    item.Title = item.EntityName + "-" + item.MeetingTypeName + "-" + "[ " + srno + " ]" + "-" + item.FY;
                }
            }
            return _objAnnualMeeting;
        }

        public List<EntityDetails> GetAllEntityDetails(int customerID, int userID, string role)
        {
            List<EntityDetails> result = new List<EntityDetails>();
            try
            {
                if (role == SecretarialConst.Roles.CS)
                {
                    result = (from row in entities.BM_SP_EntityListforTaskUser(userID, customerID)
                              orderby row.CompanyName ascending
                              select new EntityDetails
                              {
                                  EntityId = row.Id,

                                  EntityName = row.CompanyName,

                              }).ToList();
                }
                else if (role == SecretarialConst.Roles.DRCTR)
                {
                    result = (from row in entities.BM_SP_EntityListforBOD(userID, customerID)
                              orderby row.CompanyName ascending
                              select new EntityDetails
                              {
                                  EntityId = row.ID,

                                  EntityName = row.CompanyName,
                              }).ToList();
                }
                else if (role == SecretarialConst.Roles.HDCS)
                {
                    result = (from row in entities.BM_EntityMaster
                              where row.Customer_Id == customerID
                              select new EntityDetails
                              {
                                  EntityName = row.CompanyName,
                                  EntityId = row.Id
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }

        public List<FYearDetails> GetFYEntityWise(int EntityID)
        {
            var result = new List<FYearDetails>();
            try
            {
                DateTime dTCurrent = DateTime.Now.Date.AddDays(-365.50);
                string date = dTCurrent.ToString("yyyy-MM-dd");
                var FDate = DateTime.ParseExact(date, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (EntityID > 0)
                    {
                        result = (from BEM in entities.BM_EntityMaster
                                  join BYM in entities.BM_YearMaster
                                  on BEM.FY_CY equals BYM.Type
                                  where BYM.IsDeleted == false
                                  && BYM.FromDate >= FDate
                                  && BEM.Id == EntityID
                                  orderby BYM.FYText descending
                                  select new FYearDetails
                                  {
                                      FYID = (int)BYM.FYID,
                                      FYText = BYM.FYText,
                                      EntityId = EntityID
                                  }).ToList();
                    }
                    else
                    {
                        result = (from BEM in entities.BM_EntityMaster
                                  join BYM in entities.BM_YearMaster
                                  on BEM.FY_CY equals BYM.Type
                                  where BYM.IsDeleted == false
                                  && BYM.FromDate >= FDate
                                  // && BEM.Id == EntityID
                                  orderby BYM.FYText descending
                                  select new FYearDetails
                                  {
                                      FYID = (int)BYM.FYID,
                                      FYText = BYM.FYText,
                                      EntityId = EntityID

                                  }).Distinct().ToList();
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return result;
            }
        }

        public List<AnnualMVM> GetAllAnnualMeeting(int customerId)
        {
            List<AnnualMVM> obj = new List<AnnualMVM>();
            try
            {
                //    obj = (from x in entities.BM_SP_GetAnnualMeetings(customerId)
                //           select new AnnualMVM

                //           {
                //               AnnualMeetingId = x.MeetingId,
                //               EntityId = x.EntityId,
                //               MeetingDate = x.MeetingDate,
                //               FYID = Convert.ToInt32(x.FYID),
                //               Id = Convert.ToInt32(x.MeetingTypeId),
                //               MeetingSrNumber = Convert.ToInt32(x.MeetingSrNo)

                //           }
                //                   ).ToList();
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public AnnualMVM AddAnnualMeeting(AnnualMVM item, int customerId, int userId, ModelStateDictionary modelState)
        {
            bool success = true;
            try
            {
                var checkDate = (from row in entities.BM_AnnualMeetings
                                 where row.EntityId == item.EntityId
                                 && row.FY == item.FYID
                                 && row.MeetingTypeId == item.Id
                                 select row).FirstOrDefault();

                if (checkDate != null)
                {
                    #region Comment check Meeting Date
                    //TimeSpan ts = Convert.ToDateTime(checkDate.MeetingDate).Subtract( Convert.ToDateTime(item.MeetingDate));
                    //double NumberOfDays = Math.Abs(ts.TotalDays);
                    //if (NumberOfDays >= 120)
                    //{
                    //    success = false;
                    //}
                    //else
                    //{
                    //    success = true;
                    //}
                    #endregion

                    var EntityType = (from en in entities.BM_EntityMaster
                                      where en.Id == item.EntityId
                                      select en.Entity_Type).FirstOrDefault();

                    var MeetingGapInDays = (from row in entities.BM_Committee_MeetingRule
                                            where row.CommitteeId == item.Id
                                            && row.Entity_Type == EntityType
                                            && row.IsDeleted == false
                                            select row.MeetingGapInDays).FirstOrDefault();

                    if (MeetingGapInDays > 0)
                    {
                        DateTime? LastMeetingDate = null;
                        var meetingDate = (from row in entities.BM_AnnualMeetings
                                           where row.EntityId == item.EntityId
                                           && row.FY == item.FYID
                                           && row.MeetingTypeId == item.Id
                                           && row.IsDeleted == false
                                           select row);

                        LastMeetingDate = (from row in meetingDate
                                           select row.MeetingDate).Max();

                        if (LastMeetingDate != null)
                        {
                            var maxMeetingDate = Convert.ToDateTime(LastMeetingDate).AddDays((int)MeetingGapInDays);
                            if (maxMeetingDate <= Convert.ToDateTime(item.MeetingDate))
                            {
                                item.Message = item.Message + "<br>" + "Gap between two meeting is not more than " + ((int)MeetingGapInDays) + " days";
                                item.Error = true;
                                item.Success = false;
                                success = false;
                            }
                            else
                            {
                                success = true;
                            }
                        }
                    }
                }

                if (success)
                {
                    var checkmeeting = (from x in entities.BM_AnnualMeetings
                                        where x.IsDeleted == false
                                        && x.Customer_Id == customerId
                                        && x.EntityId == item.EntityId
                                        && x.FY == item.FYID
                                        && x.MeetingTypeId == item.Id
                                        && x.MeetingSrNo == item.MeetingSrNumber
                                        select x).FirstOrDefault();

                    if (checkmeeting == null)
                    {
                        BM_AnnualMeetings _objannualmeeting = new BM_AnnualMeetings
                        {
                            EntityId = item.EntityId,
                            FY = item.FYID,
                            MeetingDate = item.MeetingDate,
                            MeetingSrNo = item.MeetingSrNumber,
                            MeetingTypeId = item.Id,
                            MeetingTime = item.StartmeetingTime,
                            MeetingEndTime = item.EndmeetingTime,
                            IsDeleted = false,
                            Customer_Id = customerId,
                            CreatedOn = DateTime.Now,
                            CreatedBy = userId,
                            MeetingCircular = "M"
                        };

                        entities.BM_AnnualMeetings.Add(_objannualmeeting);
                        entities.SaveChanges();
                    }
                    else
                    {
                        // modelState.AddModelError("", "Record already exist");
                        item.Message = "Same Record Already Exists";
                    }
                }
                else
                {
                    //item.Message = "Gap between two meeting is not more than " + ((int)MeetingGapInDays) + " days.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                item.Message = SecretarialConst.Messages.serverError;
            }
            return item;
        }


        public AnnualMeetingSheduler UpdateAnnualMeeting(AnnualMeetingSheduler item, int userId, int customerId)
        {
            try
            {
                var checksrnumber = (from row in entities.BM_AnnualMeetings
                                     where
row.ID != item.ID
&& row.Customer_Id == customerId
&& row.EntityId == item.EntityId
&& row.MeetingTypeId == item.MeetingTypeId
&& row.MeetingSrNo == item.MeetingSRNo
                                     select row).FirstOrDefault();

                if (checksrnumber == null)
                {
                    var checkmeeting = (from x in entities.BM_AnnualMeetings
                                        where
    x.IsDeleted == false
    && x.Customer_Id == customerId
    && x.ID == item.ID
                                        select x).FirstOrDefault();

                    if (checkmeeting != null)
                    {
                        checkmeeting.EntityId = item.EntityId;
                        checkmeeting.FY = item.FYID;
                        checkmeeting.MeetingDate = item.MeetingDate;
                        checkmeeting.MeetingSrNo = item.MeetingSRNo;
                        checkmeeting.MeetingTypeId = item.MeetingTypeId;
                        checkmeeting.IsDeleted = false;
                        checkmeeting.Customer_Id = customerId;
                        checkmeeting.CreatedOn = DateTime.Now;
                        checkmeeting.CreatedBy = userId;
                        checkmeeting.MeetingCircular = "M";
                        checkmeeting.MeetingTime = item.MeetingStartTime;
                        checkmeeting.MeetingEndTime = item.MeetingEndTime;


                        entities.SaveChanges();

                        item.Success = true;
                        item.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    else
                    {
                        item.Error = true;
                        item.Message = "Record not found";
                    }
                }
                else
                {
                    item.Error = true;
                    item.Message = "Meeting with Same Sr.No. already exists";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return item;

        }

        public AnnualMeetingSheduler GetSchedulerEditvalue(int Id)
        {
            AnnualMeetingSheduler _objscheduler = new AnnualMeetingSheduler();
            try
            {
                _objscheduler = (from x in entities.BM_AnnualMeetings
                                 where x.ID == Id
                                 select new AnnualMeetingSheduler
                                 {
                                     ID = x.ID,
                                     EntityId = x.EntityId,
                                     MeetingTypeId = x.MeetingTypeId,
                                     MeetingDate = x.MeetingDate,
                                     MeetingSRNo = x.MeetingSrNo
                                 }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objscheduler;
        }

        public void DeleteAnnualCalenderMeeting(AnnualMeetingSheduler _objannualmeeting, ModelStateDictionary modelState, int UserId)
        {
            try
            {
                if (_objannualmeeting.ID > 0)
                {
                    var checkdata = (from x in entities.BM_AnnualMeetings
                                     where x.ID == _objannualmeeting.ID
                                     select x).FirstOrDefault();

                    if (checkdata != null)
                    {
                        checkdata.IsDeleted = true;
                        checkdata.UpdatedOn = DateTime.Now;
                        checkdata.UpdatedBy = UserId;
                        entities.SaveChanges();
                    }
                    else
                    {
                        modelState.AddModelError("", "Record not found");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                modelState.AddModelError("", SecretarialConst.Messages.serverError);
            }
        }

        public bool CheckDuplicateAnnualMeetingDate(IEnumerable<AnnualMVM> _objMeeting, int customerId)
        {
            bool success = false;
            try
            {
                foreach (var item in _objMeeting)
                {
                    var checksrno = (from x in entities.BM_AnnualMeetings
                                     where x.EntityId == item.EntityId
                                     && x.MeetingTypeId == item.Id
                                     && x.Customer_Id == customerId
                                     && x.FY == item.FYID
                                     && x.MeetingSrNo == item.MeetingSrNumber
                                     select x).FirstOrDefault();
                    if (checksrno != null)
                    {
                        success = false;
                        break;
                    }
                    else
                    {
                        success = true;
                    }
                    if (success)
                    {
                        var checkdate = (from x in entities.BM_AnnualMeetings
                                         where x.EntityId == item.EntityId
                                         && x.MeetingTypeId == item.Id
                                         && x.Customer_Id == customerId
                                         && x.FY == item.FYID
                                         && x.MeetingDate == item.MeetingDate
                                         select x).FirstOrDefault();
                        if (checkdate != null)
                        {
                            success = false;
                            break;
                        }
                        else
                        {
                            success = true;
                        }
                        if (success)
                        {
                            var EntityType = (from en in entities.BM_EntityMaster where en.Id == item.EntityId select en.Entity_Type).FirstOrDefault();
                            var MeetingGapInDays = (from row in entities.BM_Committee_MeetingRule
                                                    where row.CommitteeId == item.Id && row.Entity_Type == EntityType && row.IsDeleted == false
                                                    select row.MeetingGapInDays).FirstOrDefault();
                            if (MeetingGapInDays > 0)
                            {
                                DateTime? LastMeetingDate = null;
                                var meetingDate = (from row in entities.BM_AnnualMeetings
                                                   where row.EntityId == item.EntityId && row.FY == item.FYID && row.MeetingTypeId == item.Id && row.IsDeleted == false
                                                   select row);


                                LastMeetingDate = (from row in meetingDate

                                                   select row.MeetingDate).Max();

                                if (LastMeetingDate != null)
                                {
                                    var maxMeetingDate = Convert.ToDateTime(LastMeetingDate).AddDays((int)MeetingGapInDays);
                                    if (maxMeetingDate <= Convert.ToDateTime(item.MeetingDate))
                                    {
                                        item.Message = item.Message + "<br>" + "Gap between two meeting is not more than " + ((int)MeetingGapInDays) + " days.";
                                        item.Error = true;
                                        item.Success = false;
                                        success = false;
                                        break;
                                    }
                                    else
                                    {
                                        success = true;
                                    }
                                }
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }

        public string Checkvalidation(AnnualMVM _obj, int customerId)
        {
            bool success = false;
            string Message = string.Empty;
            //var checksrno = (from x in entities.BM_AnnualMeetings
            //                 where x.EntityId == _obj.EntityId
            //                 && x.MeetingTypeId == _obj.Id
            //                 && x.Customer_Id == customerId
            //                 && x.FY == _obj.FYID
            //                 && x.MeetingSrNo == _obj.MeetingSrNumber
            //                 select x).FirstOrDefault();
            //if (checksrno != null)
            //{
            //    Message = "Meeting sr.Number already exist";
            //    success = false;
            //}
            //else
            //{
            //    Message = "true";
            //    success = true;
            //}

            var checkdate = (from x in entities.BM_AnnualMeetings
                             where x.EntityId == _obj.EntityId
                             && x.MeetingTypeId == _obj.Id
                             && x.Customer_Id == customerId
                             && x.FY == _obj.FYID
                             && x.MeetingDate == _obj.MeetingDate
                             select x).FirstOrDefault();
            if (checkdate != null)
            {
                success = false;
                Message = "Meeting date already exist";
            }
            else
            {
                Message = "true";
                success = true;
            }
            if (success)
            {
                var EntityType = (from en in entities.BM_EntityMaster where en.Id == _obj.EntityId select en.Entity_Type).FirstOrDefault();
                var MeetingGapInDays = (from row in entities.BM_Committee_MeetingRule
                                        where row.CommitteeId == _obj.Id && row.Entity_Type == EntityType && row.IsDeleted == false
                                        select row.MeetingGapInDays).FirstOrDefault();
                if (MeetingGapInDays > 0)
                {
                    DateTime? LastMeetingDate = null;
                    var meetingDate = (from row in entities.BM_AnnualMeetings
                                       where row.EntityId == _obj.EntityId && row.FY == _obj.FYID && row.MeetingTypeId == _obj.Id && row.IsDeleted == false
                                       select row);


                    LastMeetingDate = (from row in meetingDate

                                       select row.MeetingDate).Max();

                    if (LastMeetingDate != null)
                    {
                        var maxMeetingDate = Convert.ToDateTime(LastMeetingDate).AddDays((int)MeetingGapInDays);
                        if (maxMeetingDate <= Convert.ToDateTime(_obj.MeetingDate))
                        {
                            Message = Message + "<br>" + "Gap between two meeting is not more than " + ((int)MeetingGapInDays) + " days.";

                            success = false;

                        }
                        else
                        {
                            Message = "true";
                            success = true;
                        }
                    }
                }

            }

            return Message;
        }

        public string CheckvalidationforSrnumber(AnnualMVM _obj, int customerId)
        {
            string Message = string.Empty;

            var checksrno = (from x in entities.BM_AnnualMeetings
                             where x.EntityId == _obj.EntityId
                             && x.MeetingTypeId == _obj.Id
                             && x.Customer_Id == customerId
                             && x.FY == _obj.FYID
                             && x.MeetingSrNo == _obj.MeetingSrNumber
                             select x).FirstOrDefault();

            if (checksrno != null)
            {
                Message = "Meeting with Same Sr.No. already exists";
            }
            else
            {
                Message = "true";
            }

            return Message;
        }
    }
}