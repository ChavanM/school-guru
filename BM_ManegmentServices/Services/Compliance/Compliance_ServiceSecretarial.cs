﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Compliance
{
    public class Compliance_ServiceSecretarial : ICompliance_Service //ICompliance_ServiceSecretarial
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public int GetCustomerBranchID(int entityId, int customerId)
        {
            var customerBranchID = entities.BM_EntityMaster.Where(k => k.Id == entityId && k.Customer_Id == customerId && k.Is_Deleted == false).Select(k => k.CustomerBranchId).FirstOrDefault();
            return customerBranchID.HasValue ? (int)customerBranchID : 0;
        }

        #region Get Instance Id
        public long? GetInstanceId(int entityId, long complianceId)
        {
            try
            {
                var result = (from row in entities.BM_ComplianceInstance
                              where row.EntityID == entityId && row.ComplianceId == complianceId && row.IsDeleted == false
                              select row.ComplianceInstanceID).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public long? GetInstanceIdForDirector(long directorId, long complianceId)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Assigned Compliances
        public IEnumerable<ComplianceAssignedListVM> GetAssignedCompliances(int entityID, int customerID, string mappingType)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetAssignedComplianceSecretarial(entityID, customerID, mappingType)
                              select new ComplianceAssignedListVM
                              {
                                  EntityID = row.EntityID,
                                  ComplianceId = row.ComplianceID,
                                  Description = row.Description,
                                  Performer = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.PerformerID,
                                      FullName = row.Performer
                                  },
                                  Reviewer = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.ReviewerID,
                                      FullName = row.Reviewer
                                  }
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Get Assignment for Schedule on
        public List<ComplianceScheduleAssignmentVM> GetAssignment(int entityId, long complianceID)
        {
            List<ComplianceScheduleAssignmentVM> result = null;
            try
            {
                var complianceInstanceID = (from instance in entities.BM_ComplianceInstance
                                            where instance.EntityID == entityId && instance.ComplianceId == complianceID && instance.IsDeleted == false
                                            select instance.ComplianceInstanceID).FirstOrDefault();

                if (complianceInstanceID > 0)
                {
                    var complianceAssignment = (from assignment in entities.BM_ComplianceAssignment
                                                where assignment.ComplianceInstanceID == complianceInstanceID
                                                select new { assignment.RoleID, assignment.UserID }).ToList();

                    if (complianceAssignment != null)
                    {
                        result = complianceAssignment.Select(r => new ComplianceScheduleAssignmentVM
                        {
                            RoleID = r.RoleID,
                            UserID = r.UserID,
                            IsDeleted = false,
                            CreatedOn = DateTime.Now,
                        }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Compliance Assign
        public IEnumerable<AgendaComplianceListVM> GetAgendaComplianceList(int entityID, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetAgendaComplianceSecretarialByEntityID(entityID, customerId)
                              select new AgendaComplianceListVM
                              {
                                  AgendaId = row.AgendaID,
                                  AgendaItem = row.AgendaItem,
                                  ComplianceId = row.ComplianceID,
                                  Description = row.ShortDescription,
                                  IsAgendaSelect = true,
                                  IsComplianceSelect = true,
                                  PerformerID = 0,
                                  ReviewerID = 0,
                                  EntityID = entityID,
                                  Performer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  Reviewer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = row.MeetingTypeName
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public AgendaComplianceAssingVM CreateComplianceInstance_Statutory(AgendaComplianceAssingVM obj)
        {
            throw new NotImplementedException();
        }
        public AgendaComplianceAssingVM CreateComplianceInstance(AgendaComplianceAssingVM obj)
        {
            try
            {
                if (obj.lst != null)
                {
                    DateTime ScheduleOnDate = new DateTime().Date;

                    DateTime.TryParse(obj.ScheduleOnDate, out ScheduleOnDate);
                    foreach (var item in obj.lst)
                    {
                        var isExists = entities.BM_ComplianceInstance
                                               .Where(k => k.ComplianceId == item.ComplianceId &&
                                                           k.EntityID == obj.EntityID &&
                                                           k.IsDeleted == false).Any();

                        if (isExists == false)
                        {
                            if (item.Performer.UserID > 0 && item.Reviewer.UserID > 0)
                            {
                                #region Generate Instance ID

                                var complianceInstance = new BM_ComplianceInstance()
                                {
                                    MappingType = obj.MappingType,
                                    ComplianceId = item.ComplianceId,
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false,
                                    GenerateSchedule = true,
                                    ScheduledOn = ScheduleOnDate,

                                    CustomerBranchID = 0,
                                    EntityID = obj.EntityID,
                                    CustomerID = obj.CustomerID
                                };

                                entities.BM_ComplianceInstance.Add(complianceInstance);
                                entities.SaveChanges();

                                #region Save Performer Reviewer
                                if (complianceInstance.ComplianceInstanceID > 0)
                                {
                                    var performer = new BM_ComplianceAssignment()
                                    {
                                        ComplianceInstanceID = complianceInstance.ComplianceInstanceID,
                                        RoleID = SecretarialConst.RoleID.PERFORMER,
                                        UserID = item.Performer.UserID,
                                        IsDeleted = false,
                                        CreatedBy = obj.UserID,
                                        CreatedOn = DateTime.Now
                                    };

                                    var reviewer = new BM_ComplianceAssignment()
                                    {
                                        ComplianceInstanceID = complianceInstance.ComplianceInstanceID,
                                        RoleID = SecretarialConst.RoleID.REVIEWER,
                                        UserID = item.Reviewer.UserID,
                                        IsDeleted = false,
                                        CreatedBy = obj.UserID,
                                        CreatedOn = DateTime.Now
                                    };

                                    entities.BM_ComplianceAssignment.Add(performer);
                                    entities.BM_ComplianceAssignment.Add(reviewer);
                                    entities.SaveChanges();
                                }
                                #endregion
                                #endregion
                            }
                        }
                    }

                    obj.Success = true;
                    obj.Message = "Save Successfully.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Meeting level Compliance
        public IEnumerable<AgendaComplianceListVM> GetMeetingComplianceList(int entityID, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetMeetingComplianceSecretarialByEntityID(entityID, customerId)
                              select new AgendaComplianceListVM
                              {
                                  ComplianceId = row.ComplianceID,
                                  Description = row.ShortDescription,
                                  IsComplianceSelect = true,
                                  PerformerID = 0,
                                  ReviewerID = 0,
                                  EntityID = entityID,
                                  Performer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  Reviewer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = row.MeetingTypeName
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Statutory Compliance
        public IEnumerable<StatutoryComplianceListVM> GetStatutoryComplianceList(int entityID, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_SP_ComplianceStatutoryByEntityID(entityID, customerId)
                              select new StatutoryComplianceListVM
                              {
                                  ActId = row.ActID,
                                  //ActName = row.ActName,

                                  ComplianceId = row.ID,
                                  ShortDescription = row.ShortDescription,
                                  //Description = row.Description,
                                  Description = row.ShortDescription,
                                  Section = row.Sections,
                                  Risk = row.Risk,
                                  Frequency = row.Frequency,

                                  IsComplianceSelect = true,
                                  PerformerID = 0,
                                  ReviewerID = 0,
                                  EntityID = entityID,
                                  Performer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  Reviewer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public IEnumerable<ActVM> GetStatutoryActList(int entityID, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_SP_ComplianceStatutoryActsByEntityID(entityID, customerId)
                              select new ActVM
                              {
                                  ActId = row.ActID,
                                  ActName = row.ActName,
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Compliances Re-Assignment
        public IEnumerable<ComplianceReAssignmentListVM> GetAssignedCompliancesDetails(int entityID, int customerID, string mappingType, int userID)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetAssignedComplianceDetailsSecretarial(entityID, customerID, mappingType)
                              where row.EntityID == entityID && row.UserID == userID
                              select new ComplianceReAssignmentListVM
                              {
                                  ComplianceAssignmentId = row.ComplianceAssignmentID,
                                  ComplianceId = row.ComplianceID,
                                  Description = row.ShortDescription,
                                  User = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.UserID,
                                      FullName = row.UserName
                                  },
                                  RoleID = row.RoleID,
                                  UserRole = row.UserRole,
                                  IsComplianceSelect = true
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ComplianceReAssingmentVM ComplianceReAssignment(ComplianceReAssingmentVM obj)
        {
            try
            {
                if (obj.lst != null)
                {
                    foreach (var item in obj.lst)
                    {
                        var objDetails = entities.BM_ComplianceAssignment.Where(k => k.ComplianceAssignmentID == item.ComplianceAssignmentId && k.IsDeleted == false).FirstOrDefault();
                        if (objDetails != null)
                        {
                            objDetails.UserID = item.User.UserID;

                            objDetails.UpdatedBy = obj.UpdatedBy;
                            objDetails.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }
                    }
                    obj.Success = true;
                    obj.Message = "Saved Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Compliance Upload
        public UploadComplianceVM CreateInstance(int customerId, int entityId, long complianceId, string performerEmail, string reviewerEmail, string scheduleOn, int createdBy)
        {
            var obj = new UploadComplianceVM() { EntityID = entityId, ComplianceId = complianceId, PerformerMail = performerEmail, ReviewerMail = reviewerEmail, ScheduleOn = scheduleOn, IsValid = true };

            try
            {
                var SrNo = 0;
                if (entityId == 0)
                {
                    obj.IsValid = false;
                    SrNo++;
                    obj.ValidationMessage += SrNo + ". Invalid Entity ID. <br/> ";
                }
                else
                {
                    #region check Entity
                    var entityName = (from row in entities.BM_EntityMaster
                                      where row.Id == entityId && row.Customer_Id == customerId && row.Is_Deleted == false
                                      select row.CompanyName).FirstOrDefault();

                    if (string.IsNullOrEmpty(entityName))
                    {
                        obj.IsValid = false;
                        SrNo++;
                        obj.ValidationMessage += SrNo + ". Entity name not found. <br/> ";
                    }
                    else
                    {
                        obj.EntityName = entityName;
                    }
                    #endregion
                }

                if (complianceId == 0)
                {
                    obj.IsValid = false;
                    SrNo++;
                    obj.ValidationMessage += SrNo + ". Invalid Compliance ID. <br/> ";
                }
                else
                {
                    #region Check Compliance

                    var shortDescription = (from row in entities.Compliances
                                            where row.ID == complianceId && row.IsDeleted == false
                                            select row.ShortDescription).FirstOrDefault();

                    if (string.IsNullOrEmpty(shortDescription))
                    {
                        obj.IsValid = false;
                        SrNo++;
                        obj.ValidationMessage += SrNo + ". Compliance not found. <br/> ";
                    }
                    else
                    {
                        obj.Description = shortDescription;

                        #region check Complaince Assigned or not

                        var isInstanceCreated = (from row in entities.BM_ComplianceInstance
                                                 where row.EntityID == entityId && row.ComplianceId == complianceId && row.IsDeleted == false
                                                 select row.EntityID).Any();

                        if (isInstanceCreated)
                        {
                            obj.IsValid = false;
                            SrNo++;
                            obj.ValidationMessage += SrNo + ". Compliance all ready assigned. <br/> ";
                        }

                        #endregion
                    }
                    #endregion
                }

                if (string.IsNullOrEmpty(performerEmail))
                {
                    obj.IsValid = false;
                    SrNo++;
                    obj.ValidationMessage += SrNo + ". Performer mail required. <br/> ";
                }
                else
                {
                    #region Check Performer Mail
                    var performer = (from row in entities.Users
                                     where row.CustomerID == customerId && row.Email == performerEmail && row.IsActive == true && row.IsDeleted == false
                                     select new PerformerReviewerViewModel
                                     {
                                         UserID = row.ID,
                                         FullName = row.FirstName + " " + row.LastName
                                     }).FirstOrDefault();

                    if (performer != null)
                    {
                        obj.Performer = performer;
                    }
                    else
                    {
                        obj.IsValid = false;
                        SrNo++;
                        obj.ValidationMessage += SrNo + ". Performer not found. <br/> ";
                    }
                    #endregion
                }

                if (string.IsNullOrEmpty(reviewerEmail))
                {
                    obj.IsValid = false;
                    SrNo++;
                    obj.ValidationMessage += SrNo + ". Reviewer mail required. <br/> ";
                }
                else
                {
                    #region Check Reviewer Mail
                    var reviewer = (from row in entities.Users
                                    where row.CustomerID == customerId && row.Email == reviewerEmail && row.IsActive == true && row.IsDeleted == false
                                    select new PerformerReviewerViewModel
                                    {
                                        UserID = row.ID,
                                        FullName = row.FirstName + " " + row.LastName
                                    }).FirstOrDefault();

                    if (reviewer != null)
                    {
                        obj.Reviewer = reviewer;
                    }
                    else
                    {
                        obj.IsValid = false;
                        SrNo++;
                        obj.ValidationMessage += SrNo + ". Reviewer not found. <br/> ";
                    }
                    #endregion
                }

                if (string.IsNullOrEmpty(scheduleOn))
                {
                    obj.IsValid = false;
                    SrNo++;
                    obj.ValidationMessage += SrNo + ". Start Date required.";
                }
                else
                {
                    DateTime scheduleDate;
                    if (DateTime.TryParseExact(scheduleOn, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out scheduleDate))
                    {
                        obj.ScheduleOnDate = scheduleDate;
                    }
                    else
                    {
                        obj.IsValid = false;
                        SrNo++;
                        obj.ValidationMessage += SrNo + ". " + scheduleOn + " is an invalid date format.";
                    }
                }
            }
            catch (Exception ex)
            {
                obj.IsValid = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public bool CreateInstance(List<UploadComplianceVM> lst, int customerId, int createdBy)
        {
            foreach (var obj in lst)
            {
                if (obj.IsValid)
                {
                    #region Generate Instance ID

                    var complianceInstance = new BM_ComplianceInstance()
                    {
                        ComplianceId = obj.ComplianceId,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                        GenerateSchedule = true,
                        ScheduledOn = obj.ScheduleOnDate,

                        CustomerBranchID = 0,
                        EntityID = obj.EntityID,
                        CustomerID = customerId,

                    };

                    entities.BM_ComplianceInstance.Add(complianceInstance);
                    entities.SaveChanges();

                    #region Save Performer Reviewer
                    if (complianceInstance.ComplianceInstanceID > 0)
                    {
                        var performer = new BM_ComplianceAssignment()
                        {
                            ComplianceInstanceID = complianceInstance.ComplianceInstanceID,
                            RoleID = SecretarialConst.RoleID.PERFORMER,
                            UserID = obj.Performer.UserID,
                            IsDeleted = false,
                            CreatedBy = createdBy,
                            CreatedOn = DateTime.Now
                        };

                        var reviewer = new BM_ComplianceAssignment()
                        {
                            ComplianceInstanceID = complianceInstance.ComplianceInstanceID,
                            RoleID = SecretarialConst.RoleID.REVIEWER,
                            UserID = obj.Reviewer.UserID,
                            IsDeleted = false,
                            CreatedBy = createdBy,
                            CreatedOn = DateTime.Now
                        };

                        entities.BM_ComplianceAssignment.Add(performer);
                        entities.BM_ComplianceAssignment.Add(reviewer);
                        entities.SaveChanges();
                    }
                    #endregion
                    #endregion
                }
            }

            return true;
        }
        #endregion

        #region Director Related Compliance
        public IEnumerable<StatutoryComplianceListVM> GetDirectorComplianceList(long directorId, int customerId)
        {
            throw new NotImplementedException();
        }
        public AgendaComplianceAssingVM CreateComplianceInstanceForDirector(AgendaComplianceAssingVM obj)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ComplianceAssignedListVM> GetAssignedCompliancesDirector(long directorId, int customerId)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region Compliances Re-Assignment (Director)
        public IEnumerable<ComplianceReAssignmentListVM> GetAssignedCompliancesDetailsForDirector(long directorId, int customerID, int userID)
        {
            throw new NotImplementedException();
        }
        public ComplianceReAssingmentVM ComplianceReAssignmentForDirector(ComplianceReAssingmentVM obj)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Get Compliance
        public List<VMCompliences> BindCompliancesNew(long? AgendaId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {

                var compliancesQuery = (from row in entities.BM_ComplianceListView

                                        select new VMCompliences
                                        {
                                            ID = row.ID
                                                ,
                                            ActID = row.ActID
                                                ,
                                            ActName = row.Name
                                                ,
                                            Description = row.Description
                                                ,
                                            Sections = row.Sections != null ? row.Sections.ToUpper() : row.Sections
                                                ,
                                            ComplianceType = row.ComplianceType
                                                ,
                                            UploadDocument = row.UploadDocument
                                                ,
                                            NatureOfCompliance = row.NatureOfCompliance
                                                ,
                                            ShortDescription = row.ShortDescription,
                                            RiskType = row.Risk,
                                            Frequency = row.Frequency,

                                            AgendaId = AgendaId
                                        }).ToList();

                return compliancesQuery;
            }
        }
        public IEnumerable<ActListViewVM> GetActName()
        {
            var acts = (from row in entities.BM_ActListView
                            ////where row.IsDeleted == false
                        orderby row.Name ascending
                        select new ActListViewVM
                        {
                            ID = row.ID,
                            Name = row.Name
                        }).OrderBy(entry => entry.Name).ToList();
            return acts;
        }
        #endregion

        #region My Compliances
        public IEnumerable<MyComplianceListVM> GetMyCompliances(MyCompliancesVM obj, int userId, int customerId)
        {
            try
            {
                return (from row in entities.BM_SP_Compliance_MyCompliancesSecretarial(obj.StatusId, userId, obj.RoleId, customerId)
                        select new MyComplianceListVM
                        {
                            EntityId = row.EntityId,
                            ScheduleOnID = row.ScheduleOnID,
                            RoleID = row.RoleID,
                            MappingType = row.MappingType,
                            CompanyName = row.CompanyName,
                            MeetingID = row.MeetingID,
                            MeetingTitle = row.MeetingTitle,
                            ShortDescription = row.ShortDescription,
                            Description = row.Description,
                            ScheduleOn = row.ScheduleOn,
                            ScheduleOnTime = row.ScheduleOnTime,
                            StatusName = row.Status_
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<MyComplianceListVM> GetMyCompliances(int userID, int customerID, int statusID, int roleID)
        {
            try
            {
                return (from row in entities.BM_SP_Compliance_MyCompliancesNew(statusID, userID, roleID, customerID)
                        select new MyComplianceListVM
                        {
                            MappingType = row.MappingType,
                            EntityId = row.EntityId,
                            CompanyName = row.CompanyName,
                            DirectorId = row.DirectorId,
                            MeetingID = row.MeetingID,
                            MeetingTitle = row.MeetingTitle,
                            //ComplianceID=row.
                            //ShortForm
                            ShortDescription = row.ShortDescription,
                            Description = row.Description,
                            RoleID = row.RoleID,
                            ScheduleOnID = row.ScheduleOnID,
                            ScheduleOn = row.ScheduleOn,
                            ScheduleOnTime = row.ScheduleOnTime,
                            StatusName = row.Status_
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_MyComplianceDocuments_Result> GetMyDocuments(int userID, int customerID, int? statusID, int showAll, string userRole)
        {
            try
            {
                return entities.BM_SP_MyComplianceDocuments(userID, customerID, statusID, showAll, userRole).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_DirectorComplianceDocuments_Result> GetMyDocuments_Director(int userID, int customerID, int statusID, int roleID)
        {
            try
            {
                return entities.BM_SP_DirectorComplianceDocuments(userID, customerID, statusID, roleID, 2).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_DirectorComplianceStatusReport_Result> GetComplianceStatusReport_Director(int userID, int customerID, int? statusID, int showAll)
        {
            try
            {
                return entities.BM_SP_DirectorComplianceStatusReport(userID, customerID, statusID, showAll).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_ComplianceStatusReport_Result> GetComplianceStatusReport(int userID, int customerID, int? statusID, int showAll, string userRole)
        {
            try
            {
                return entities.BM_SP_ComplianceStatusReport(userID, customerID, statusID, showAll, userRole).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_DirectorAttendanceReport_Result> GetAttendanceReport_Director(int userID, int customerID)
        {
            try
            {
                return entities.BM_SP_DirectorAttendanceReport(userID, customerID).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_AttendanceReport_Result> GetAttendanceReport(int userID, int customerID, string userRole)
        {
            try
            {
                return entities.BM_SP_AttendanceReport(userID, customerID, userRole).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_AttendanceReportCS_Result> GetAttendanceReportCS(int userID, int customerID, string userRole)
        {
            try
            {
                return entities.BM_SP_AttendanceReportCS(userID, customerID, userRole).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_AttendanceReportCSDetail_Result> GetAttendanceReportCSDetail(int userID, int customerID, string userRole, int Meeting_Id, int CompanyList, string FinYr)
        {
            try
            {
                return entities.BM_SP_AttendanceReportCSDetail(userID, customerID, userRole, Meeting_Id, CompanyList, FinYr).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion

        public List<SecretarialTag_VM> GetSecretarialTagList()
        {
            try
            {
                var result = (from row in entities.Compliance_SecretarialTagMaster
                              where row.IsActive == true
                              select new SecretarialTag_VM
                              {
                                  ID = row.ID,
                                  Tag = row.Tag,
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public long GetScheduleOnIDForMeetingEvent(long meetingID, string autoCompleteOn)
        {
            throw new NotImplementedException();
        }

        public List<FileDataVM> FileDataListByScheduleOnID(long schdeuleOnId)
        {
            throw new NotImplementedException();
        }

        public List<string> getComplianceRemark(int userId, int complianceID)
        {
            throw new NotImplementedException();
        }

        public Message ExcludeCompliance(IEnumerable<ComplianceAssignedListVM> obj, int customerId)
        {
            throw new NotImplementedException();
        }
    }
}