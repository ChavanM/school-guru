﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Compliance
{
    public class DirectorCompliances : IDirectorCompliances
    {
        ICompliance_Service objICompliance_Service;
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public DirectorCompliances(ICompliance_Service objCompliance_Service)
        {
            objICompliance_Service = objCompliance_Service;
        }
        public long? GetInstanceIdForDirector(long directorId, long complianceId)
        {
            try
            {
                var result = (from row in entities.ComplianceInstances
                              where row.DirectorId == directorId && row.ComplianceId == complianceId && row.CustomerBranchID == 0 && row.IsDeleted == false && row.IsSecretarial == true
                              select row.ID).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public long? GetInstanceIdForEntity(int entityId, long complianceId, long? directorId)
        {
            long? result = null;
            try
            {
                var customerBrachId = (from entity in entities.BM_EntityMaster
                                       where entity.Id == entityId && entity.Is_Deleted == false
                                       select entity.CustomerBranchId
                                       ).FirstOrDefault();

                if(customerBrachId >0)
                {
                    if(directorId > 0)
                    {
                        result = (from row in entities.ComplianceInstances
                                  where row.CustomerBranchID == customerBrachId && row.DirectorId == directorId && row.ComplianceId == complianceId && row.IsDeleted == false && row.IsSecretarial == true
                                  select row.ID).FirstOrDefault();
                    }
                    else
                    {
                        result = (from row in entities.ComplianceInstances
                                  where row.CustomerBranchID == customerBrachId && row.ComplianceId == complianceId && row.IsDeleted == false && row.IsSecretarial == true
                                  select row.ID).FirstOrDefault();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        #region Director Schedule on
        public Message GenerateDirectorScheduleOn(int customerID, long directorId, string generateScheduleFor, DateTime closedDate, int createdBy, string userName, int? fileId, int? assignTo)
        {
            return GenerateDirectorScheduleOn(customerID, directorId, generateScheduleFor, closedDate, createdBy, userName, fileId, assignTo, null);
        }
        public Message GenerateDirectorScheduleOn(int customerID, long directorId, string generateScheduleFor, DateTime closedDate, int createdBy, string userName, int? fileId, int? assignTo, DateTime? eventDateTime)
        {
            var result = new Message();
            try
            {
                var lstComplianceIdForScheduleOn = (from row in entities.BM_ComplianceMapping
                                                    join r in entities.ComplianceInstances.Where(k => k.DirectorId == directorId).Select(k => new { k.ComplianceId, k.ID }) on row.ComplianceID equals r.ComplianceId
                                                    where row.AutoCompleteOn == generateScheduleFor && row.MappingType == SecretarialConst.ComplianceMappingType.DIRECTOR
                                                    select new ComplianceScheduleOnVM
                                                    {
                                                        ScheduleOnID = 0,
                                                        MCM_ID = row.MCM_ID,
                                                        AgendaMasterId = 0,
                                                        ComplianceId = row.ComplianceID,
                                                        MeetingAgendaMappingId = 0,

                                                        DaysOrHours = row.DaysOrHours,
                                                        Numbers = row.Numbers,
                                                        BeforeAfter = row.BeforeAfter
                                                    }).ToList();

                if (lstComplianceIdForScheduleOn != null)
                {
                    foreach (var item in lstComplianceIdForScheduleOn)
                    {
                        var instanceId = GetInstanceIdForDirector(directorId, item.ComplianceId);

                        #region Compliance Instance if assignment not exists else re-assignment

                        if(instanceId == 0 && assignTo > 0)
                        {
                            var dirUserId = (from row in entities.BM_DirectorMaster
                                             where row.Id == directorId
                                             select row.UserID).FirstOrDefault();
                            if(dirUserId > 0)
                            {
                                var objComplianceInstance = new AgendaComplianceAssingVM()
                                {
                                    DirectorID = directorId,

                                    lst = new List<AgendaComplianceListVM>()
                                    {
                                        new AgendaComplianceListVM()
                                        {
                                            ComplianceId = item.ComplianceId,
                                            Performer = new PerformerReviewerViewModel() { UserID = (int) assignTo },
                                            Reviewer = new PerformerReviewerViewModel() { UserID = (long) dirUserId }
                                        }
                                    }
                                };

                                objICompliance_Service.CreateComplianceInstanceForDirector(objComplianceInstance);

                                instanceId = objComplianceInstance.lst.FirstOrDefault().ComplianceInstanceIdNew;
                            }
                        }
                        else if(instanceId > 0 && assignTo > 0)
                        {
                            var _tempAssignment = (from row in entities.ComplianceAssignments
                                                   where row.ComplianceInstanceID == instanceId && row.RoleID == SecretarialConst.RoleID.PERFORMER
                                                   select row
                                                  ).FirstOrDefault();
                            if(_tempAssignment != null)
                            {
                                _tempAssignment.UserID = (long) assignTo;
                                entities.Entry(_tempAssignment).State = System.Data.Entity.EntityState.Modified;
                                entities.SaveChanges();
                            }
                        }

                        #endregion

                        if (instanceId > 0)
                        {
                            DateTime scheduleOn = DateTime.Now;
                            if(eventDateTime.HasValue)
                            {
                                scheduleOn = Convert.ToDateTime(eventDateTime);

                                if(item.Numbers.HasValue && !string.IsNullOrEmpty(item.DaysOrHours))
                                {
                                    double num = Convert.ToDouble(item.Numbers);

                                    if(item.BeforeAfter == "B")
                                    {
                                        num = num * (-1);
                                    }

                                    switch (item.DaysOrHours.Trim())
                                    {
                                        case "D":
                                            scheduleOn = scheduleOn.AddDays(num);
                                            break;
                                        case "H":
                                            scheduleOn = scheduleOn.AddHours(num);
                                            break;
                                        case "M":
                                            scheduleOn = scheduleOn.AddMinutes(num);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                            var complianceScheduleOnOld = new ComplianceScheduleOn
                            {
                                ComplianceInstanceID = instanceId,
                                ForMonth = "",
                                ForPeriod = null,
                                IsActive = true,
                                IsUpcomingNotDeleted = true,
                                ScheduleOn = scheduleOn,
                            };

                            bool existSchedule = false;
                            if (complianceScheduleOnOld.ComplianceInstanceID != null)
                            {
                                long complianceScheduleOnID = 0;

                                complianceScheduleOnID = ExistsComplianceSchedule(Convert.ToInt64(complianceScheduleOnOld.ComplianceInstanceID), complianceScheduleOnOld.ScheduleOn);

                                if (complianceScheduleOnID != 0)
                                    existSchedule = true;
                            }

                            if (!existSchedule)
                            {
                                entities.ComplianceScheduleOns.Add(complianceScheduleOnOld);
                                entities.SaveChanges();
                            }

                            if (complianceScheduleOnOld.ID > 0)
                            {
                                var complianceScheduleOn = new BM_ComplianceScheduleOnNew
                                {
                                    ComplianceInstanceID = instanceId,
                                    ScheduleOnID = complianceScheduleOnOld.ID,
                                    ComplianceID = item.ComplianceId,
                                    MeetingID = 0,
                                    MappingType = SecretarialConst.ComplianceMappingType.DIRECTOR,
                                    MCM_ID = item.MCM_ID,
                                    AgendaMasterID = 0,
                                    CreatedBy = createdBy,
                                    CreatedOn = DateTime.Now,
                                    IsActive = true,
                                    IsDeleted = false,
                                    EntityId = 0,
                                    //Customer_Id = ,
                                    MeetingAgendaMappingId = 0,
                                    StatusId = 1,
                                    ScheduleOn = scheduleOn,
                                    Customer_Id = customerID,
                                };

                                if (eventDateTime.HasValue)
                                {
                                    complianceScheduleOn.ScheduleOn = scheduleOn;
                                }

                                if (complianceScheduleOn.ComplianceInstanceID != null)
                                {
                                    long recordID = 0;
                                    recordID = ExistsComplianceSchedule_Secretarial(Convert.ToInt64(complianceScheduleOn.ComplianceInstanceID), complianceScheduleOn.ScheduleOnID, complianceScheduleOn.ScheduleOn);
                                     
                                    if (recordID != 0)
                                        existSchedule = true;
                                }

                                if (!existSchedule)
                                {
                                    entities.BM_ComplianceScheduleOnNew.Add(complianceScheduleOn);
                                    entities.SaveChanges();
                                }

                                #region create transaction

                                ComplianceTransaction _obj = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = (long)instanceId,
                                    ComplianceScheduleOnID = complianceScheduleOnOld.ID,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned",
                                    //Dated = obj.Dated,
                                    Dated = closedDate,
                                    CreatedBy = createdBy,
                                    CreatedByText = userName,
                                    StatusChangedOn = DateTime.Now
                                };

                                entities.ComplianceTransactions.Add(_obj);
                                entities.SaveChanges();

                                #endregion

                                if (fileId > 0)
                                {
                                    var _objFileDataMapping = new FileDataMapping()
                                    {
                                        TransactionID = _obj.ID,
                                        FileID = (int) fileId,
                                        FileType = 1,
                                        ScheduledOnID = complianceScheduleOnOld.ID
                                    };

                                    entities.FileDataMappings.Add(_objFileDataMapping);
                                    entities.SaveChanges();
                                }
                                
                            }
                        }
                    }
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }

        public Message GenerateScheduleOnForEntityOnEvent(int customerId, int entityId, string generateScheduleFor, DateTime closedDate, int createdBy, string userName, int? fileId, int? performerId, int? reviewerId, DateTime? eventDateTime, long? directorId = 0, string refMaster = null, long? refMasterId = null)
        {
            var result = new Message();
            try
            {
                var lstComplianceIdForScheduleOn = (from row in entities.BM_ComplianceMapping
                                                    //join r in entities.ComplianceInstances.Where(k => k.DirectorId == directorId).Select(k => new { k.ComplianceId, k.ID }) on row.ComplianceID equals r.ComplianceId
                                                    where row.AutoCompleteOn == generateScheduleFor && row.IsDeleted == false && row.IsDeleted == false //&& row.MappingType == SecretarialConst.ComplianceMappingType.DIRECTOR
                                                    select new ComplianceScheduleOnVM
                                                    {
                                                        ScheduleOnID = 0,
                                                        MCM_ID = row.MCM_ID,
                                                        AgendaMasterId = 0,
                                                        ComplianceId = row.ComplianceID,
                                                        MeetingAgendaMappingId = 0,

                                                        DaysOrHours = row.DaysOrHours,
                                                        Numbers = row.Numbers,
                                                        BeforeAfter = row.BeforeAfter
                                                    }).ToList();

                if (lstComplianceIdForScheduleOn != null)
                {
                    foreach (var item in lstComplianceIdForScheduleOn)
                    {
                        var instanceId = GetInstanceIdForEntity(entityId, item.ComplianceId, directorId);

                        #region Compliance Instance if assignment not exists else re-assignment
                        if (instanceId == 0 && performerId > 0 && reviewerId > 0)
                        {
                            var objComplianceInstance = new AgendaComplianceAssingVM()
                            {
                                EntityID = entityId,
                                CustomerID = customerId,
                                DirectorID = directorId == null ? 0 : (long) directorId,
                                ScheduleOnDate = DateTime.Now.Date.ToString("dd/MMM/yyyy"),
                                lst = new List<AgendaComplianceListVM>()
                                {
                                    new AgendaComplianceListVM()
                                    {
                                        ComplianceId = item.ComplianceId,
                                        EntityID = entityId,
                                        Performer = new PerformerReviewerViewModel() { UserID = (int) performerId },
                                        Reviewer = new PerformerReviewerViewModel() { UserID = (int) reviewerId }
                                    }
                                }
                            };

                            objComplianceInstance = objICompliance_Service.CreateComplianceInstance(objComplianceInstance);

                            instanceId = objComplianceInstance.lst.FirstOrDefault().ComplianceInstanceIdNew;
                        }
                        else if (instanceId > 0 && performerId > 0 && reviewerId > 0)
                        {
                            var _tempAssignment = (from row in entities.ComplianceAssignments
                                                   where row.ComplianceInstanceID == instanceId && row.RoleID == SecretarialConst.RoleID.PERFORMER
                                                   select row
                                                  ).FirstOrDefault();
                            if (_tempAssignment != null)
                            {
                                _tempAssignment.UserID = (long)performerId;
                                entities.Entry(_tempAssignment).State = System.Data.Entity.EntityState.Modified;
                                entities.SaveChanges();
                            }

                            var _tempAssignmentReviewer = (from row in entities.ComplianceAssignments
                                                   where row.ComplianceInstanceID == instanceId && row.RoleID == SecretarialConst.RoleID.REVIEWER
                                                   select row
                                                  ).FirstOrDefault();
                            if (_tempAssignmentReviewer != null)
                            {
                                _tempAssignmentReviewer.UserID = (long)reviewerId;
                                entities.Entry(_tempAssignmentReviewer).State = System.Data.Entity.EntityState.Modified;
                                entities.SaveChanges();
                            }
                        }
                        #endregion

                        if (instanceId > 0)
                        {
                            DateTime scheduleOn = DateTime.Now;
                            if (eventDateTime.HasValue)
                            {
                                scheduleOn = Convert.ToDateTime(eventDateTime);

                                if (item.Numbers.HasValue && !string.IsNullOrEmpty(item.DaysOrHours))
                                {
                                    double num = Convert.ToDouble(item.Numbers);

                                    if (item.BeforeAfter == "B")
                                    {
                                        num = num * (-1);
                                    }

                                    switch (item.DaysOrHours.Trim())
                                    {
                                        case "D":
                                            scheduleOn = scheduleOn.AddDays(num);
                                            break;
                                        case "H":
                                            scheduleOn = scheduleOn.AddHours(num);
                                            break;
                                        case "M":
                                            scheduleOn = scheduleOn.AddMinutes(num);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                            var complianceScheduleOnOld = new ComplianceScheduleOn
                            {
                                ComplianceInstanceID = instanceId,
                                ForMonth = "",
                                ForPeriod = null,
                                IsActive = true,
                                IsUpcomingNotDeleted = true,
                                ScheduleOn = scheduleOn,
                            };

                            entities.ComplianceScheduleOns.Add(complianceScheduleOnOld);
                            entities.SaveChanges();

                            if (complianceScheduleOnOld.ID > 0)
                            {
                                var complianceScheduleOn = new BM_ComplianceScheduleOnNew
                                {
                                    ComplianceInstanceID = instanceId,
                                    ScheduleOnID = complianceScheduleOnOld.ID,
                                    ComplianceID = item.ComplianceId,
                                    MeetingID = 0,
                                    MappingType = SecretarialConst.ComplianceMappingType.STATUTORY,
                                    MCM_ID = item.MCM_ID,
                                    AgendaMasterID = 0,
                                    CreatedBy = createdBy,
                                    CreatedOn = DateTime.Now,
                                    IsActive = true,
                                    IsDeleted = false,
                                    EntityId = entityId,
                                    MeetingAgendaMappingId = 0,
                                    StatusId = 1
                                };

                                if(refMasterId > 0)
                                {
                                    complianceScheduleOn.RefMasterId = (long)refMasterId;
                                    complianceScheduleOn.RefMaster = refMaster;
                                }

                                if (eventDateTime.HasValue)
                                {
                                    complianceScheduleOn.ScheduleOn = scheduleOn;
                                }
                                entities.BM_ComplianceScheduleOnNew.Add(complianceScheduleOn);
                                entities.SaveChanges();

                                #region create transaction
                                ComplianceTransaction _obj = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = (long)instanceId,
                                    ComplianceScheduleOnID = complianceScheduleOnOld.ID,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned.",
                                    //Dated = obj.Dated,
                                    Dated = closedDate,
                                    CreatedBy = createdBy,
                                    CreatedByText = userName,
                                    StatusChangedOn = DateTime.Now
                                };

                                entities.ComplianceTransactions.Add(_obj);
                                entities.SaveChanges();

                                if (fileId > 0)
                                {
                                    var _objFileDataMapping = new FileDataMapping()
                                    {
                                        TransactionID = _obj.ID,
                                        FileID = (int)fileId,
                                        FileType = 1,
                                        ScheduledOnID = complianceScheduleOnOld.ID
                                    };

                                    entities.FileDataMappings.Add(_objFileDataMapping);
                                    entities.SaveChanges();
                                }
                                #endregion
                            }
                        }
                    }
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Something went wrong.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }
        #endregion

        #region Director Compliances performer user
        public long? GetPerformer(long directorId, string generateScheduleFor)
        {
            long? userId = null;
            try
            {
                userId = (from row in entities.BM_ComplianceMapping
                            join r in entities.ComplianceInstances.Where(k => k.DirectorId == directorId).Select(k => new { k.ComplianceId, k.ID }) on row.ComplianceID equals r.ComplianceId
                            join a in entities.ComplianceAssignments on r.ID equals a.ComplianceInstanceID
                            where row.AutoCompleteOn == generateScheduleFor && row.MappingType == SecretarialConst.ComplianceMappingType.DIRECTOR &&
                            a.RoleID == SecretarialConst.RoleID.PERFORMER
                            select a.UserID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return userId;
        }
        #endregion

        public long ExistsComplianceSchedule(long complianceInstanceID, DateTime scheduleON)
        {
            try
            {
                var existComplianceSchduleRecord = (from CSO in entities.ComplianceScheduleOns
                                                    where CSO.ComplianceInstanceID == complianceInstanceID
                                                    && CSO.IsActive == true
                                                    && CSO.IsUpcomingNotDeleted == true
                                                    && CSO.ScheduleOn == scheduleON
                                                    select CSO).FirstOrDefault();

                if (existComplianceSchduleRecord != null)
                {
                    var ComplianceScheduleOnID = existComplianceSchduleRecord.ID;

                    return existComplianceSchduleRecord.ID;
                }
                else
                    return 0;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public long ExistsComplianceSchedule_Secretarial(long complianceInstanceID, long scheduleOnID,  DateTime? scheduleON)
        {
            try
            {
                var existComplianceSchduleRecord = (from CSO in entities.BM_ComplianceScheduleOnNew
                                                    where CSO.ComplianceInstanceID == complianceInstanceID
                                                    && CSO.ScheduleOnID == scheduleOnID
                                                    && CSO.IsActive == true
                                                    && CSO.ScheduleOn == scheduleON
                                                    select CSO).FirstOrDefault();

                if (existComplianceSchduleRecord != null)
                {
                    return existComplianceSchduleRecord.Id;
                }
                else
                    return 0;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

    }
}