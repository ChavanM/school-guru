﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Compliance
{
    public interface ICompliance_ServiceSecretarial
    {
        #region Assigned Compliances
        IEnumerable<ComplianceAssignedListVM> GetAssignedCompliances(int entityID, int customerID, string mappingType);
        #endregion

        #region Get Assignment for Schedule on
        List<ComplianceScheduleAssignmentVM> GetAssignment(int entityId, long complianceID);
        #endregion

        #region Compliance Assign
        IEnumerable<AgendaComplianceListVM> GetAgendaComplianceList(int entityId, int customerId);
        AgendaComplianceAssingVM CreateComplianceInstance(AgendaComplianceAssingVM obj);
        #endregion

        #region Meeting level Compliance
        IEnumerable<AgendaComplianceListVM> GetMeetingComplianceList(int entityID, int customerId);
        #endregion

        #region Statutory Compliance
        IEnumerable<StatutoryComplianceListVM> GetStatutoryComplianceList(int entityID, int customerId);
        IEnumerable<ActVM> GetStatutoryActList(int entityID, int customerId);
        #endregion

        #region Compliances Re-Assignment
        IEnumerable<ComplianceReAssignmentListVM> GetAssignedCompliancesDetails(int entityID, int customerID, string mappingType, int userID);
        ComplianceReAssingmentVM ComplianceReAssignment(ComplianceReAssingmentVM obj);
        #endregion

        #region Compliance Upload
        UploadComplianceVM CreateInstance(int customerId,int entityId, long complianceId, string performerEmail, string reviewerEmail, string scheduleOn, int createdBy);
        bool CreateInstance(List<UploadComplianceVM> lst, int customerId, int createdBy);
        #endregion

        #region Get Compliance
        List<VMCompliences> BindCompliancesNew(long? AgendaId);
        IEnumerable<ActListViewVM> GetActName();
        #endregion

        #region My Compliances
        IEnumerable<MyComplianceListVM> GetMyCompliances(MyCompliancesVM obj, int userId, int customerId);
        #endregion
    }
}