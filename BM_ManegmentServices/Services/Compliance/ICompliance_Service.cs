﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Compliance
{
    public interface ICompliance_Service
    {
        int GetCustomerBranchID(int entityId, int customerId);

        #region Get Instance Id
        long? GetInstanceId(int entityId, long complianceId);
        #endregion

        #region Assigned Compliances
        IEnumerable<ComplianceAssignedListVM> GetAssignedCompliances(int entityID, int customerID, string mappingType);
        #endregion

        #region Get Assignment for Schedule on
        List<ComplianceScheduleAssignmentVM> GetAssignment(int entityId, long complianceID);
        #endregion

        #region Compliance Assign
        IEnumerable<AgendaComplianceListVM> GetAgendaComplianceList(int entityId, int customerId);
        AgendaComplianceAssingVM CreateComplianceInstance(AgendaComplianceAssingVM obj);
        AgendaComplianceAssingVM CreateComplianceInstance_Statutory(AgendaComplianceAssingVM obj);

        List<SecretarialTag_VM> GetSecretarialTagList();
        #endregion

        #region Meeting level Compliance
        IEnumerable<AgendaComplianceListVM> GetMeetingComplianceList(int entityID, int customerId);
        #endregion

        #region Statutory Compliance
        IEnumerable<StatutoryComplianceListVM> GetStatutoryComplianceList(int entityID, int customerId);
        IEnumerable<ActVM> GetStatutoryActList(int entityID, int customerId);
        #endregion

        #region Compliances Re-Assignment
        IEnumerable<ComplianceReAssignmentListVM> GetAssignedCompliancesDetails(int entityID, int customerID, string mappingType, int userID);
        ComplianceReAssingmentVM ComplianceReAssignment(ComplianceReAssingmentVM obj);
        #endregion

        #region Compliance Upload
        UploadComplianceVM CreateInstance(int customerId, int entityId, long complianceId, string performerEmail, string reviewerEmail, string scheduleOn, int createdBy);

        bool CreateInstance(List<UploadComplianceVM> lst, int customerId, int createdBy);
        #endregion

        #region Director Related Compliance
        IEnumerable<StatutoryComplianceListVM> GetDirectorComplianceList(long directorId, int customerId);
        AgendaComplianceAssingVM CreateComplianceInstanceForDirector(AgendaComplianceAssingVM obj);
        IEnumerable<ComplianceAssignedListVM> GetAssignedCompliancesDirector(long directorId, int customerId);
        #endregion

        #region Compliances Re-Assignment (Director)
        IEnumerable<ComplianceReAssignmentListVM> GetAssignedCompliancesDetailsForDirector(long directorId, int customerID, int userID);
        ComplianceReAssingmentVM ComplianceReAssignmentForDirector(ComplianceReAssingmentVM obj);
        #endregion

        #region Get Compliance
        List<VMCompliences> BindCompliancesNew(long? AgendaId);
        IEnumerable<ActListViewVM> GetActName();
        #endregion

        #region My Compliances
        IEnumerable<MyComplianceListVM> GetMyCompliances(MyCompliancesVM obj, int userId, int customerId);
        IEnumerable<MyComplianceListVM> GetMyCompliances(int userID, int customerID, int statusID, int roleID);
        #endregion

        #region My Documents
        List<BM_SP_DirectorComplianceDocuments_Result> GetMyDocuments_Director(int userID, int customerID, int statusID, int roleID);
        List<BM_SP_MyComplianceDocuments_Result> GetMyDocuments(int userID, int customerID, int? statusID, int showAll, string userRole);


        #endregion

        #region My Reports
        List<BM_SP_DirectorComplianceStatusReport_Result> GetComplianceStatusReport_Director(int userID, int customerID, int? statusID, int roleID);
        List<BM_SP_ComplianceStatusReport_Result> GetComplianceStatusReport(int userID, int customerID, int? statusID, int showAll, string userRole);
        List<BM_SP_DirectorAttendanceReport_Result> GetAttendanceReport_Director(int userID, int customerID);
        List<BM_SP_AttendanceReport_Result> GetAttendanceReport(int userID, int customerID, string userRole);
        List<BM_SP_AttendanceReportCS_Result> GetAttendanceReportCS(int userID, int customerID, string userRole);
        List<BM_SP_AttendanceReportCSDetail_Result> GetAttendanceReportCSDetail(int userID, int customerID, string userRole,int Meeting_Id, int CompanyList, string FinYr);
        #endregion

        #region ScheduleOnId for Meeting level Compliace
        long GetScheduleOnIDForMeetingEvent(long meetingID, string autoCompleteOn);
        #endregion

        #region Get file data list by scheduleOnID
        List<FileDataVM> FileDataListByScheduleOnID(long schdeuleOnId);
        #endregion

        #region Compliance Exclude
        Message ExcludeCompliance(IEnumerable<ComplianceAssignedListVM> obj, int customerId);
        #endregion
        List<string> getComplianceRemark(int userId, int complianceID);
    }
}