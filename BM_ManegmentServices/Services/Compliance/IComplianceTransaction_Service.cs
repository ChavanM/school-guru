﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Compliance
{
    public interface IComplianceTransaction_Service
    {
        ComplianceTransactionVM CreateTransaction(ComplianceTransactionVM obj);
        List<ComplianceTransactionVM> CreateTransactionOnMeetingEvent(long meetingID, string autoCompleteON);
        List<ComplianceTransactionVM> CreateTransactionOnMeetingEvent(long meetingID, string autoCompleteON, bool autoClose);
        List<ComplianceTransactionVM> CreateTransactionOnMeetingEvent(long meetingID, string autoCompleteON, bool autoClose, int statusId, DateTime dated, FileDataVM objFileData, int userID);
        bool CheckMeetingComplianceClosed(long meetingID, string autoCompleteON);
        bool CreateTransactionOnMeetingEnd(long meetingID, int userId);
        bool CreateTransactionOnNoticeSend(long meetingID, int userId);
    }
}
