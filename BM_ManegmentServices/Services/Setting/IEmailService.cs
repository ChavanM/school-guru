﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Setting
{
    public interface IEmailService
    {
        List<MailSettingVM> GetAll(int customerId, int userId, string role);
        TestMailVM Get(long id, int customerId);
        MailSettingVM GetMailSetting(long id, int customerId);
        MailSettingVM Save(MailSettingVM obj, int userId);
        //MailSettingVM GetMailSettingByEntityId(long entityId, int customerId);
    }
}
