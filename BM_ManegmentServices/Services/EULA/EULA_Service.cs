﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.EULA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.EULA
{
    public class EULA_Service : IEULA_Service
    {
        public List<EULA_DetailsVM> GetEULADetails(int userId)
        {
            List<EULA_DetailsVM> result = null;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_SP_GetEULAList(userId)
                              orderby row.UploadedOn descending, row.AcceptedOn descending
                              select new EULA_DetailsVM
                              {
                                  CustomerName = row.Customer,
                                  UserID = (int)row.UserID,
                                  UserName = row.UserName,
                                  MobileNo = row.ContactNumber,
                                  EmailID = row.Email,
                                  EULA_ID = row.EULA_ID,
                                  EULA = row.EULA,
                                  AcceptedOn = row.AcceptedOn,
                                  UploadedOn = row.UploadedOn,
                                  FilePath = row.FilePath,
                                  IsDocUploaded = row.FilePath == null ? false : true,
                                  StatusId = row.StatusId,
                                  Status = row.StatusName,
                                  //MembershipNumber = row.MembershipNumber,
                                  LastLoginTime = row.LastLoginTime,
                                  LastLoginBefore = row.LastLoginBefore,
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public EULA_DetailsVM GetEULADetailsById(int id, string userMode)
        {
            var result = new EULA_DetailsVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.EULADetails
                              join u in entities.Users on row.UserID equals u.ID
                              join c in entities.Customers on u.CustomerID equals c.ID
                              where row.ID == id && row.IsActive == true
                              select new EULA_DetailsVM
                              {
                                  CustomerID = c.ID,
                                  CustomerName = c.Name,
                                  UserID = (int)row.UserID,
                                  UserName = u.FirstName + " " + u.LastName,
                                  MobileNo = u.ContactNumber,
                                  EmailID = u.Email,
                                  EULA_ID = row.ID,
                                  EULA = row.EULA,
                                  AcceptedOn = row.AcceptedOn,
                                  UploadedOn = row.UploadedOn,
                                  FilePath = row.FilePath,
                                  StatusId = row.StatusId
                              }).FirstOrDefault();

                    if(result != null)
                    {
                        result.UserMode = userMode;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public EULA_DetailsVM GetEULADetailsByUserId(int userId)
        {
            var result = new EULA_DetailsVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.EULADetails
                              join u in entities.Users on row.UserID equals u.ID
                              join c in entities.Customers on u.CustomerID equals c.ID
                              where u.ID == userId && row.IsActive == true
                              select new EULA_DetailsVM
                              {
                                  CustomerID = c.ID,
                                  CustomerName = c.Name,
                                  UserID = (int)row.UserID,
                                  UserName = u.FirstName + " " + u.LastName,
                                  MobileNo = u.ContactNumber,
                                  EmailID = u.Email,
                                  EULA_ID = row.ID,
                                  EULA = row.EULA,
                                  AcceptedOn = row.AcceptedOn,
                                  UploadedOn = row.UploadedOn,
                                  FilePath = row.FilePath,
                                  StatusId = row.StatusId
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public EULA_DetailsVM SaveEULAStatus(EULA_DetailsVM obj, byte statusId, int userId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.EULADetails
                                where row.ID == obj.EULA_ID && row.IsActive == true
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {
                        _obj.StatusId = statusId;
                        _obj.StatusUpdatedBy = userId;
                        _obj.StatusUpdatedOn = DateTime.Now;
                        entities.SaveChanges();

                        obj.StatusId = statusId;
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = SecretarialConst.Messages.serverError;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public string GetEULADocById(int id)
        {
            var fileName = "";
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                fileName = (from row in entities.EULADetails
                            where row.ID == id && row.IsActive == true
                            select row.FilePath).FirstOrDefault();
            }
            return fileName;
        }
    }
}