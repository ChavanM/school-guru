﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.EULA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.EULA
{
    public interface IEULA_Service
    {
        List<EULA_DetailsVM> GetEULADetails(int userId);
        EULA_DetailsVM GetEULADetailsById(int id, string userMode);
        EULA_DetailsVM GetEULADetailsByUserId(int userId);
        EULA_DetailsVM SaveEULAStatus(EULA_DetailsVM obj, byte statusId, int userId);
        string GetEULADocById(int id);
    }
}