//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_MeetingSigningAuthorityForAGM_Result
    {
        public string AuthorityName { get; set; }
        public string Designation { get; set; }
        public string DIN_PAN { get; set; }
        public string MembershipNo { get; set; }
        public Nullable<bool> IsCS { get; set; }
        public Nullable<bool> IsChairman { get; set; }
        public Nullable<bool> IsAuthorisedSignotory { get; set; }
    }
}
