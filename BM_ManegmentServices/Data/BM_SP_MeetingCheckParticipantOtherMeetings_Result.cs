//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_MeetingCheckParticipantOtherMeetings_Result
    {
        public Nullable<long> Director_Id { get; set; }
        public string Director_Name { get; set; }
        public Nullable<long> MeetingID { get; set; }
        public string MeetingTime { get; set; }
        public string MeetingVenue { get; set; }
        public string MeetingTitle { get; set; }
        public Nullable<int> EntityId { get; set; }
        public string CompanyName { get; set; }
    }
}
