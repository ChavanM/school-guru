//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_AgendaMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BM_AgendaMaster()
        {
            this.BM_AgendaMaster1 = new HashSet<BM_AgendaMaster>();
        }
    
        public long BM_AgendaMasterId { get; set; }
        public string AgendaItemHeading { get; set; }
        public string Agenda { get; set; }
        public Nullable<int> EntityType { get; set; }
        public Nullable<int> MeetingTypeId { get; set; }
        public Nullable<bool> IsForAllEntityType { get; set; }
        public Nullable<int> FrequencyId { get; set; }
        public Nullable<int> PartId { get; set; }
        public Nullable<bool> IsMultiStageAgenda { get; set; }
        public Nullable<bool> CanPassedByCircularResolution { get; set; }
        public Nullable<bool> IsPostBallotRequired { get; set; }
        public Nullable<int> PreCommitteeMeetingTypeId { get; set; }
        public bool IsVoting { get; set; }
        public string AgendaFormat { get; set; }
        public string ResolutionFormat { get; set; }
        public string MinutesFormat { get; set; }
        public Nullable<bool> IsForAllMeetingType { get; set; }
        public bool IsNoting { get; set; }
        public Nullable<long> Parent_Id { get; set; }
        public Nullable<int> StageId { get; set; }
        public Nullable<int> SequenceNo { get; set; }
        public Nullable<bool> IsSubAgenda { get; set; }
        public Nullable<int> Customer_Id { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string MinutesFormatHeading { get; set; }
        public string ResolutionFormatHeading { get; set; }
        public string MinutesDisApproveFormat { get; set; }
        public string MinutesDifferFormat { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public Nullable<bool> HasTemplate { get; set; }
        public Nullable<bool> HasCompliance { get; set; }
        public string DefaultAgendaFor { get; set; }
        public string AgendaInfo { get; set; }
        public bool HasInfo { get; set; }
        public string InfoText { get; set; }
        public Nullable<long> UIFormID { get; set; }
        public bool CanMultiple { get; set; }
        public string SEBI_IntimationFormat { get; set; }
        public string SEBI_DisclosureFormat { get; set; }
        public bool IsOrdinaryBusiness { get; set; }
        public bool IsSpecialResolution { get; set; }
        public bool IsLive { get; set; }
        public string DocumentStatus { get; set; }
    
        public virtual BM_EntityType BM_EntityType { get; set; }
        public virtual BM_CommitteeComp BM_CommitteeComp { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BM_AgendaMaster> BM_AgendaMaster1 { get; set; }
        public virtual BM_AgendaMaster BM_AgendaMaster2 { get; set; }
    }
}
