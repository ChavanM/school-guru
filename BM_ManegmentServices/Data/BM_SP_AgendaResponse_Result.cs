//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_AgendaResponse_Result
    {
        public long BM_AgendaMasterId { get; set; }
        public long MeetingParticipantId { get; set; }
        public int Id { get; set; }
        public string MeetingTypeName { get; set; }
        public Nullable<long> AgendaID { get; set; }
        public string Agenda { get; set; }
        public long MeetingAgendaMappingID { get; set; }
        public string Designation { get; set; }
        public string CastingVote { get; set; }
        public Nullable<long> CircularResponceId { get; set; }
        public string ParticipantName { get; set; }
        public Nullable<long> UserId { get; set; }
        public string Response { get; set; }
        public long MeetingID { get; set; }
        public string Remark { get; set; }
        public string Due_Date { get; set; }
    }
}
