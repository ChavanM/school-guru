//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_MeetingNoticeAgendaLog_Result
    {
        public long MeetingID { get; set; }
        public long MeetingNoticeLogID { get; set; }
        public string ParticipantName { get; set; }
        public string LogType { get; set; }
        public Nullable<System.DateTime> SendOn { get; set; }
        public Nullable<System.DateTime> DeliveredOn { get; set; }
        public Nullable<System.DateTime> ReadOn { get; set; }
        public string LogTypeStr { get; set; }
    }
}
