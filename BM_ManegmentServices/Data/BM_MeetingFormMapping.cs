//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_MeetingFormMapping
    {
        public long MeetingFormMappingID { get; set; }
        public long MeetingId { get; set; }
        public long MeetingAgendaMappingId { get; set; }
        public Nullable<long> ComplianceId { get; set; }
        public Nullable<long> FileID { get; set; }
        public Nullable<long> FormID { get; set; }
        public string FormName { get; set; }
        public string Format_ { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
