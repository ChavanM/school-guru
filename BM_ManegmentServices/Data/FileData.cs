//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FileData
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FileData()
        {
            this.ComplianceTransactions = new HashSet<ComplianceTransaction>();
        }
    
        public int ID { get; set; }
        public string Name { get; set; }
        public byte[] Data { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
        public string Version { get; set; }
        public Nullable<System.DateTime> VersionDate { get; set; }
        public string VersionComment { get; set; }
        public bool IsDeleted { get; set; }
        public string EnType { get; set; }
        public Nullable<long> FileSize { get; set; }
        public bool ISLink { get; set; }
        public string PhysicalLocation { get; set; }
        public string FileNO { get; set; }
        public string Code_Esi_Epf_Lwf_Pt { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ComplianceTransaction> ComplianceTransactions { get; set; }
    }
}
