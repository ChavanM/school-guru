//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_DirectorMaster_Result
    {
        public long DirectorID { get; set; }
        public Nullable<long> UserID { get; set; }
        public string DIN { get; set; }
        public string DirectorName { get; set; }
        public System.DateTime DOB { get; set; }
        public string MobileNo { get; set; }
        public string EmailId_Personal { get; set; }
        public string EmailId_Official { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DSC_ExpiryDate { get; set; }
        public int Customer_Id { get; set; }
        public string UserName { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public string ContactNumber { get; set; }
        public Nullable<bool> MobileAccess { get; set; }
        public string IMEINumber { get; set; }
    }
}
