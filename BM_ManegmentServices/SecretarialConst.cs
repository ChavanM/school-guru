﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices
{
    public static class SecretarialConst
    {
        public static class Messages
        {
            public const string saveSuccess = "Record Save Successfully";
            public const string updateSuccess = "Record Updated Successfully";
            public const string deleteSuccess = "Record Deleted Successfully";
            public const string serverError = "Something wents wrong, Please try again !!";
            public const string validationError = "Validation Error Occurs, Please check the Input values and try again !!";
            public const string alreadyExist = "Same Record Already Exists !!";
            public const string noRecordFound = "No Record Found !!";
        }

        public static class RoleID
        {
            public const int PERFORMER = 3;
            public const int REVIEWER = 4;
        }
        public static class Roles
        {
            public const string HDCS = "HDCS"; //Head Company Secretary
            public const string CS = "CS";  //Company Secretary
            public const string DRCTR = "DRCTR";    //Director
            public const string CSIMP = "CSIMP";    //CS Implementation
            public const string CSCAD = "CSCAD";    //CS Company Admin

            public const string DADMN = "DADMN";    //Firm/Distributor Admin
            public const string CSMGR = "CSMGR";    //Manager
            public const string CEXCT = "CEXCT";    //Executive

            public const string SMNGT = "SMNGT";    //Management
        }

        public static class ComplianceMappingType
        {
            public const string MEETING = "M";
            public const string AGENDA = "A";
            public const string STATUTORY = "S";
            public const string DIRECTOR = "D";
        }
        public static class ComplianceDateType
        {
            public const string MEETINGDATE = "M";
            public const string NOTICEDATE = "N";
        }

        public static class ComplianceAutoCompleteOn
        {
            //Meeting related events
            public const string NOTICE_SEND = "Notice";
            public const string CIRCULATE_DRAFT = "CirculateDraft";
            public const string FINALIZED_MINUTES = "FinalizedMinutes";
            public const string E_VOTING = "eVoting";

            //Director related events
            public const string TYPE_OF_CHANGE_DIR_3 = "TypeOfChange-DIR-3";
            public const string TYPE_OF_CHANGE_DIR_6 = "TypeOfChange-DIR-6";
            public const string RESIGNATION_OF_DIRECTOR = "ResignationOfDirector";
        }

        public static class MeetingCircular
        {
            public const string MEETING = "M";
            public const string CIRCULAR = "C";
        }
        public static class MeetingTemplatType
        {
            public const string SEEK_AVAILABILITY = "SA";
            public const string AGENDA = "A";
            public const string AGENDA_PLUS_NOTICE = "AN";
            public const string NOTICE = "N"; 
            public const string CIRCULAR = "C";
            public const string DRAFT_MINUTES = "DM";
            public const string JOINMEETING = "JM";
            public const string TESTMAIL = "T";
            public const string INVITEE_MAIL = "I";
            public const string AGENDA_REVIEW_ASSIGNED = "ARA";
            public const string AGENDA_REVIEW_APPROVED = "ARAPR";
            public const string AGENDA_REVIEW_REJECTED = "ARRJC";
        }
        public static class MeetingStages
        {
            public const string DRAFT = "Draft";
            public const string COMPLETED = "Completed";
            public const string TODAYS = "Todays";
            public const string UPCOMING = "Upcoming";
            public const string PAST = "Past";
        }

        public static class AgendaResult
        {
            public const string APPROVED = "A";
            public const string DISAPPROVED = "D";
            public const string DIFFERED = "-";
            public const string CASTING = "C";
            public const string NOTED = "N";
        }
        public static class DefaultAgenda
        {
            public const string QUORUM = "Q";
            public const string CHAIRPERSON = "C";
            public const string CONFIRMATION_OF_MINUTES = "MI";
            public const string COMMITTEES_CONFIRMATION_OF_MINUTES = "CM";
            public const string NOTICE_FOR_CALLING_OF_AGM = "GM";
            public const string NOTICE_FOR_CALLING_OF_EGM = "EG";
            public const string TAKE_NOTE_ON_CIRCULAR = "CI";
            public const string AUDITED_CONSOLIDATED_FINANCIAL_STATEMENTS = "CF";
        }

        public static class DefaultAgendaUsedFor
        {
            public const int ALL_PRESENT = 1;
            public const int LEAVE_OF_ABSENCE = 2;
            public const int ADJOURNED = 3;
            public const int CANCELLED = 4;
        }
        public static class OpenAgendaRefMaster
        {
            public const string MEETING = "M";
        }

        public static class UIForms
        {
            public const long APPOINTMENT_OF_CFO = 1;
            public const long APPOINTMENT_OF_CS = 2;
            public const long APPOINTMENT_OF_CEO = 3;
            public const long APPOINTMENT_OF_MANAGER = 4;
            public const long APPOINTMENT_OF_DIRECTOR = 5;

            public const long RESIGNATION_OF_CFO = 6;
            public const long RESIGNATION_OF_CS = 7;
            public const long RESIGNATION_OF_CEO = 8;
            public const long RESIGNATION_OF_MANAGER = 9;
            public const long RESIGNATION_OF_DIRECTOR = 10;
            public const long RESIGNATION_OF_DIRECTOR_NONEXE = 28;
            public const long RESIGNATION_OF_DIRECTOR_NONEXE_INDEPENDANT = 29;

            public const long APPOINTMENT_OF_MD = 11;
            public const long APPOINTMENT_OF_WTD = 12;
            public const long APPOINTMENT_OF_ADDD = 13; // Additional Director
            public const long APPOINTMENT_OF_CAVD = 14; // Casual vacancy
            public const long APPOINTMENT_OF_NOMD = 15; // Appointment of Nominee director
            public const long APPOINTMENT_OF_ALTD = 16; // Appointment of Alternate director
            public const long APPOINTMENT_OF_ADDD_INDEPENDANT = 17; // Additional Director

            public const long RESIGNATION_OF_MD = 21;
            public const long RESIGNATION_OF_WTD = 22;
            public const long RESIGNATION_OF_ADDD = 23;
            public const long RESIGNATION_OF_CAVD = 24;
            public const long RESIGNATION_OF_NOMD = 25;
            public const long RESIGNATION_OF_ALTD = 26;
            public const long RESIGNATION_OF_ADDD_INDEPENDANT = 27;

            public const long APPOINTMENT_OF_INTERNAL_AUDITOR = 31;
            public const long APPOINTMENT_OF_SECRETARIAL_AUDITOR = 32;
            public const long APPOINTMENT_OF_FIRST_STATUTORY_AUDITOR = 33;
            public const long APPOINTMENT_OR_REAPPOINTMENT_OF_STATUTORY_AUDITOR_IN_AGM = 34;
            public const long APPOINTMENT_OF_COST_AUDITOR = 35;
            public const long APPOINTMENT_OR_REAPPOINTMENT_OF_COST_AUDITOR_IN_AGM = 36;


            public const long RESIGNATION_OF_INTERNAL_AUDITOR = 37;
            public const long RESIGNATION_OF_SECRETARIAL_AUDITOR = 38;
            public const long RESIGNATION_OF_STATUTORY_AUDITOR = 39;
            public const long RESIGNATION_OF_COST_AUDITOR = 40;

            public const long AUDITOR_RECOMMANDATION_IN_CASE_OF_CASUAL_VACANCY_IN_BOARD = 41;
            public const long AUDITOR_APPOINTED_IN_CASE_OF_CASUAL_VACANCY_RATIFICATION_IN_EGM = 42;
            public const long AUDITOR_APPOINTED_IN_CASE_OF_CASUAL_VACANCY_IN_AGM = 43;

            public const long AUDITOR_APPOINTED_IN_CASE_OF_CASUAL_VACANCY_IN_BOARD = 44;
            //public const long AUDITOR_APPOINTED_IN_CASE_OF_CASUAL_VACANCY_RATIFICATION_IN_EGM = 42;

            public const long FINANCIAL_RESULT_LISTED = 50;
            public const long FINANCIAL_RESULT_PUBLIC = 51;
            public const long FINANCIAL_RESULT_PRIVATE = 52;

            public const long CALLING_OF_AGM = 70;
            public const long CALLING_OF_EGM = 71;

            public const long KEEPING_OF_BOOKS_OF_ACCOUNTS = 100;
        }

        public static class EntityTypeID
        {
            public const int PRIVATE = 1;
            public const int PUBLIC = 2;
            public const int LLP = 3;
            public const int LISTED = 10;
        }

        public static class MeetingTypeID
        {
            public const int PARTNERS = 7;
            public const int DESIGNATED_PARTNERS = 8;
            public const int BOARD = 10;
            public const int AGM = 11;
            public const int EGM = 12;
        }

        public static class MeetingDocumentTypes
        {
            public const string NOTICE = "N";
            public const string AGENDA = "A";
            public const string MINUTES = "MI";
        }

        public static class MeetingParticipantsTypes
        {
            public const string DIRECTOR = "D";
            public const string MEMBER = "M";
            public const string KMP = "K";
            public const string AUDITOR = "A";
            public const string SCRUTINIZER = "S";
            public const string INVITEE = "I";
            public const string MANAGEMENT = "MT";
        }

        public static class MeetingFormatType
        {
            public const string GM_NOTES = "GM Notes";
            public const string EGM_NOTES = "EGM Notes";
            public const string EXPLANATORY_STATEMENTS = "Explanatory Statements";
            public const string NOTICE_MAIL = "Notice Mail";
            public const string SEEK_AVAILABILITY = "Seek Availability";
            public const string INVITEE_MAIL = "Invitee Mail";
        }

        public static class TaskStaus
        {
            public const int PENDING_FOR_REVIEW = 1;
            public const int COMPLETED = 4;
            public const int CLOSED = 5;
            public const int REJECTED = 6;
            public const int APPROVED = 7;
            
        }

        public static class TaskType
        {
            public const int DRAFT_AGENDA = 5;
            public const int DRAFT_MINUTES = 6;
        }

        public static class Providers
        {
            public const string Webex = "webex";
            public const string Zoom = "zoom";
        }
    }
}