﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.HelpVideo
{
    public class HelpVideoVM : IMessage
    {
        public string HelpMode { get; set; }
        public int HelpVideoId { get; set; }
        public int? ParentId { get; set; }
        public int? SrNo { get; set; }

        public string HelpName { get; set; }
        public string Description_ { get; set; }
        public int? FileType { get; set; }
        public string FileURL { get; set; }
        public bool hasChildren { get; set; }
        public bool IsLinkActive { get; set; }
        public HttpPostedFileBase Files { get; set; }
    }

    public class HelpVideoSRVM : IMessage
    {
        public int HelpVideoId { get; set; }
        public int? SrNoNew { get; set; }
    }
}