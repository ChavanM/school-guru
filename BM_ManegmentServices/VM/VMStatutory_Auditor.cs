﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMStatutory_Auditor
    {
        public long Id { get; set; }
        public int AuditorMappingId { get; set; }
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public int CustomerId { get; set; }
       // [Required]
        public bool? Class_of_Company_sec139 { get; set; }

        public int? Nature_of_Appointment { get; set; }

        //public List<VMAuditor> AuditorName { get; set; }
        public string AuditorName { get; set; }
        public string AddressOfAuditor { get; set; }
        public string AuditorNames { get; set; }
      //  [Required(ErrorMessage = "Please Select Auditor Name")]
        public int AuditorId { get; set; }
        public string AudtitorCriteria { get; set; }
        public bool? IsJoin_Auditor_Appointed { get; set; }

        public string jointAuditor { get; set; }
       // [Required(ErrorMessage = "Please enter Number of Auditor")]
        public int? Auditor_Number { get; set; }

        public bool Auditor_AGM { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date_AGM { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date_Appointment { get; set; }
        public string SRN { get; set; }
        public bool IsAuditor_Appointed_casualvacancy { get; set; }
        public string SRN_relevent_form { get; set; }
        public int? Person_vacated_office { get; set; }
        public string Membership_Number_or_Registrationfirm { get; set; }

        // [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date_vacancy { get; set; }
        public string Reasons_casual_vacancy { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? appointed_FromDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? appointed_ToDate { get; set; }
        public int? Number_of_appointedFY { get; set; }
        public bool IscompanyNoasappointedunder_141 { get; set; }
        public string tender_of_privious_Appointment { get; set; }
        public int? tender_FY { get; set; }
        public bool successMessage { get; set; }
        public bool errorMessage { get; set; }
        public string showMessage { get; set; }
        public int pcount { get; set; }
        public List<VMAuditorDetails> AuditDeatilsList { get; set; }

        public bool? IsMappingActive { get; set; }

        //Used in Template field
        public string RegistrationNo { get; set; }
        //public string DateOfAppointment { get; set; }
        //public string Appointed_FromDate { get; set; }
        //public string Appointed_DueDate { get; set; }
        public string AGMNo_TillAppointmentTerminate { get; set; }
        public long AgendaID { get; set; }
        public long MeetingAgengaMappingID { get; set; }

        public long? OriginalAuditorMappingId { get; set; }
        public string PreviousAuditorName { get; set; }
        public string PreviousRegistrationNo { get; set; }
        public string DateOfBoardMeeting { get; set; }
        public string DateOfAppointment { get; set; }
        public bool? IsTenureCompleted { get; set; }
    }
        public class VMAuditorDetails
        {
            public int Id { get; set; }
        
            public int Auditor_Id { get; set; }
            public int Auditor_CriteriaId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AppointedFromdate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dueDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AppointedFirstFYDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AppointedLastFYDate { get; set; }
            public int? Number_of_appointedFY { get; set; }
            

        public int? Statutory_AuditorId_ { get; set; }
    }
}