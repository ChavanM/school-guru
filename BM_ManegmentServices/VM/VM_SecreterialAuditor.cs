﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VM_SecreterialAuditor
    {
        public long Id { get; set; }
        public long AuditorMappingId { get; set; }
        public int EntityId { get; set; }
        public int CustomerId { get; set; }
        [Required (ErrorMessage = "Please Enter Secreterial Auditor")]
        public int AuditorID { get; set; }
        public string AuditorName { get; set; }
        public string AddressOfAuditor { get; set; }
        public string AuditorAppointedForFY { get; set; }
        public string CategoryName { get; set; }
        public int Category_of_SecreterialAuditor{get;set;}
        public string Certificate_of_Practice { get; set; }
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[Required(ErrorMessage = "Please select Appointment From Date")]
        public DateTime? Period_of_AppointmentFromDate { get; set; }
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[Required(ErrorMessage = "Please select Appointment To Date")]
        public DateTime? Period_of_AppointmentToDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please select  Date of Appointment")]
        public DateTime? Date_of_appointment { get; set; }
        public bool errorMessage { get; set; }
        public bool successMessage { get; set; }
        public string successerrorMessage { get; set; }

        public bool? IsMappingActive { get; set; }
        public bool IsTenureCompleted { get; set; }
        //Used in Template field
        public string RegistrationNo { get; set; }
        public string DateOfAppointment { get; set; }
        public string Appointed_FromDate { get; set; }
        public string Appointed_DueDate { get; set; }
        public long AgendaID { get; set; }
        public long MeetingAgengaMappingID { get; set; }
        public int? FYID { get; set; }
    }
}