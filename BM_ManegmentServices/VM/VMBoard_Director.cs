﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMBoard_Director
    {
        public int? Designation_dirId { get; set; }
        public int? Designation_ageId { get; set; }
        public int? Gender { get; set; }
        public int? Min { get; set; }
        public int? Max { get; set; }
        public int RuleId { get; set; }
        public string RoleType { get; set; }
        public long BDId { get; set; }
        public int EntityType { get; set; }
        public bool errorMessage { get; set; }
        public bool SuccessMessage { get; set; }
        public string Message { get; set; }
        public int DesignationDenominator { get; set; }
        public List<Director_Rule> Directors { get; set; }
        public List<Director_Age> DirectorAge { get; set; }
        public Response Messages { get; set; }
        public MeetingDetails MeetingDetails { get; set; }
        public Composition Quorum { get; set; }
    }
    public class Director_Rule
    {
        public int? gender { get; set; }
        public long Id { get; set; }
        public int CustomerId { get; set; }
        public int Entity_Id { get; set; }
        public int Entity_type { get; set; }
        public int? Min { get; set; }
        public int? Max { get; set; }
        public string Rules { get; set; }
        public int? Designation { get; set; }
        public string DesignatonName { get; set; }
        public char type { get; set; }
    }
    public class Director_Age
    {
        public long Id { get; set; }
        public int CustomerId { get; set; }
        public int Entity_Id { get; set; }
        public int Entity_type { get; set; }
        public int? DegignationId { get; set; }
        public int? Min_age { get; set; }
        public string DesignationName { get; set; }
        public int? Max_age { get; set; }
        public string Rules { get; set; }
        public string Description { get; set; }
        public char type { get; set; }
    }

}