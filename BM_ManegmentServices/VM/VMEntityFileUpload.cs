﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMEntityFileUpload
    {
        public Response Response { get; set; }
        public long EntityId { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public string FilePath { get; set; }
        [Required(ErrorMessage ="Please Select Form")]
        public HttpPostedFileBase File { get; set; }
        [Required(ErrorMessage ="Please select Form Type")]
        public string fromType { get; set; }
    }
    public class LLpPartnarVM
    {
        public string DPIN { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public string Pan { get; set; }
        public string Nationality { get; set; }
        public string FormContribution { get; set; }
        public decimal ObligationofContribution { get; set; }
        public decimal ContributionRecived { get; set; }
        public bool IsResidentinIndia { get; set; }
        public DateTime? DOB { get; set; }
        public string Occupation { get; set; }
        public string presentResidentAddressLine1 { get; set; }
        public string presentResidentAddressLine2 { get; set; }
        public int cityID { get; set; }
        public string Distric { get; set; }
        public int stateId { get; set; }
        public int PIN { get; set; }
        public int Country { get; set; }
        public string EmailId { get; set; }
        public string Gender { get; set; }

    }

    public class LLpBodyCorporateVM
    {
        public string CIN_FCIN_LLPIN { get; set; }
        public string ENTITYNAME { get; set; }

        public string Type { get; set; }
        public int BodyCType { get; set; }
       
        public bool IsResidentinIndia { get; set; }
       
        public string presentResidentAddressLine1 { get; set; }
        public string presentResidentAddressLine2 { get; set; }
        public int cityID { get; set; }
        public string Distric { get; set; }
        public int stateId { get; set; }
        public int PIN { get; set; }
        public int Country { get; set; }
        public string EmailId { get; set; }
        public string Gender { get; set; }

        public long NumberofShareheld { get; set; }
        public decimal PaidupShares { get; set; }

        public string FormofContribution { get; set; }
        public decimal MonetryofContribution { get; set; }
    }

    public class BodyCorporateNomineeVM
    {
        public string DPIN_DIN { get; set; }
        public string Name { get; set; }
        public bool IsResidentinIndia { get; set; }
        public int Gender { get; set; }
        public DateTime? DOB { get; set; }
        public int Nationality { get; set; }
        public string Occupation { get; set; }
        public string email { get; set; }
        public string DesignationandAuthrityinBC{ get; set; }
    }
}