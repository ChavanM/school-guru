﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{
    public class MeetingAttendance_VM : IMessage
    {
        public long AttendendenceId { get; set; }
        public long MeetingId { get; set; }
        public string Attendance { get; set; }
        public int? Position { get; set; }
        public string Designation { get; set; }
        public long ParticipantId { get; set; }
        public long? userId { get; set; }
        public string MeetingParticipant_Name { get; set; }
        public string Representative { get; set; }
        public string Remark { get; set; }
        public long? Director_Id { get; set; }
        public string ImagePath { get; set; }
        public int Role { get; set; }
        public int totalDirector { get; set; }
        public int totalDirectorPresent { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsAttendenceDone { get; set; }
        public bool ShowAttendenceMessage { get; set; }

        public int TotalMemberForAgm { get; set; }
        public int TotalMemberPresentForAgm { get; set; }
        public int TotalproxyPresentForAgm { get; set; }

        //public int TotalValidProxy { get; set; }
        //public int TotalMemberProxy { get; set; }
        //public int TotalShareHeldByProxy { get; set; }
        //public int FaceValueOfEnquityShare { get; set; }
        //public decimal PercentageOfIssuedCapital { get; set; }

        public int AuditorId { get; set; }
        public string AuditorName { get; set; }
        public string PartnerName { get; set; }
        public List<AttendenceDetails> AttendenceDetails { get; set; }
        public startstopeDate startstopeDateVM { get; set; }

        public string RSVP { get; set; }
        public string RSVP_Reason { get; set; }
        public string ParticipantType { get; set; }
        public bool CanVoteNote { get; set; }
    }

    public class ParticipantDetails
    {
        public long MeetingId { get; set; }
        public long MeetingParticipantId { get; set; }
        public string ParticipantType { get; set; }
        public bool CanVoteNote { get; set; }
    }
    public class AttendenceDetails
    {
        public long MeetingId { get; set; }
        public string Participant { get; set; }
        public string Attendence { get; set; }
        public string Reason { get; set; }
    }
    public class startstopeDate
    {
        public string Venu { get; set; }
        public string ChairmanoftheMeeting { get; set; }
        public int StartMeetingId { get; set; }
        public string MeetingType { get; set; }
        public long MeetingId { get; set; }
        public bool IsEVoting { get; set; }
        public bool? IsCompleted { get; set; }
        public bool IsAGMConclude { get; set; }
        public string Meetingno { get; set; }

        public int MeetingTypeId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }
        public string MeetingStartDate { get; set; }
        public string StartTime { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MeetingDueDate { get; set; }
        public string MeetingDueTime { get; set; }
        public string PauseTime { get; set; }
        public string ReasonforPause { get; set; }
        public string ResumeTime { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }
        public string EndTime { get; set; }
        public bool? IsPause { get; set; }
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public string CIN { get; set; }
        public int? MeetingSrNo { get; set; }
        public int? TotalMemberForAgm { get; set; }
        public int? TotalMemberPresentForAgm { get; set; }
        public int? TotalProxyPresentforAgm { get; set; }

        public int TotalValidProxy { get; set; }
        public int TotalMemberProxy { get; set; }
        public int TotalShareHeldByProxy { get; set; }
        public int FaceValueOfEnquityShare { get; set; }
        public decimal? PercentageOfIssuedCapital { get; set; }

        public bool IsVideoMeeting { get; set; }
        public string Startvideourl { get; set; }
        public string joinvideourl { get; set; }

        public string ProviderName { get; set; }

        public ChairpersonElection ChairpersonElectionVM { get; set; }

        public string FYText { get; set; }
    }
    public class ChairpersonElection : IMessage
    {
        public long MeetingParticipantId { get; set; }
        public long MeetingId { get; set; }
        public long DirectorId { get; set; }
        public int DesignationId { get; set; }
        public string DirectorName { get; set; }
        public string Designation { get; set; }
    }

    public class AgendaVoting : IMessage
    {
        public long VoingResponseId { get; set; }
        public long ParticipentId { get; set; }
        public string ParticipantName { get; set; }
        public long MeetingId { get; set; }
        public long AgendaId { get; set; }
        public long MeetingAgendaMappingId { get; set; }
        public string Agenda { get; set; }
        public string AgendaVotingResponse { get; set; }
        public string Remark { get; set; }
        public string ResultRemark { get; set; }
        public string Castingvote { get; set; }
        public bool? IsDeleted { get; set; }
        public MeetingAgendaData MeetingAgendaVM { get; set; }


    }
    public class MeetingAgendaData
    {
        public long Id { get; set; }
        public long? AgendaId { get; set; }
        public long AgendaMappingId { get; set; }
        public string AgendaName { get; set; }
        public long MeetingId { get; set; }
        public DateTime DueDate { get; set; }
        public MeetingAgendaResult MeetingAgendaResultVM { get; set; }

        public long? StartMeetingId { get; set; }
        public long? StartAgendaId { get; set; }
        public long? StartMappingId { get; set; }
        public int? SequenceNo { get; set; }
    }
    public class MeetingAgendaResult
    {
        public int Approved_count { get; set; }
        public int DisApprove_count { get; set; }
        public int Abstrain_count { get; set; }
        public int Differed_count { get; set; }
        public int Participant_Count { get; set; }
        public string Result { get; set; }
        public string ResultRemark { get; set; }
    }
    public class MainMeeting_VM : IMessage
    {
        public long? MeetingAgendaMappingID { get; set; }
        public long Id { get; set; }
        public long? SrNo { get; set; }
        public string AgendaSrNo { get; set; }
        
        public string Result { get; set; }
        public string ResultRemark { get; set; }
        public long? AgendaId { get; set; }
        public long MeetingId { get; set; }
        public string MeetingAgenda { get; set; }
        public int? EntityId { get; set; }
        public string EnttiyName { get; set; }
        public string Address { get; set; }
        public string CIN { get; set; }
        public int? MeetingSrNo { get; set; }
        public string MeetingType { get; set; }
        public string Meeting { get; set; }
        public string AgendaFormate { get; set; }
        public string DraftResolution { get; set; }
        public string DraftResolutionHeadine { get; set; }
        public string MinutesFormatHeading { get; set; }
        public string MinutesFormat { get; set; }
        public bool? IsVooting { get; set; }
        public string AgendaPart { get; set; }
        public bool? TemplateFieldEditable { get; set; }
        public AgendaVoting AgendaVoting_DtlsVM { get; set; }
        public List<PreCommitteeAgendaResultVM> lstPreCommitteeAgendaResult { get; set; }
    }
    public class PreCommitteeAgendaResultVM
    {
        public string PreCommittee { get; set; }
        public long MeetingAgendaMappingID { get; set; }
        public long MeetingId { get; set; }
        public long? AgendaId { get; set; }

        public string Result { get; set; }
        public string ResultRemark { get; set; }

        public string AgendaHeading { get; set; }
    }

    public class MeetingAgendaSummary
    {
        public long ParticipantId { get; set; }
        public long MeetingId { get; set; }
        public int MeetingTypeID { get; set; }
        public long? AgendaId { get; set; }
        public long? MeetingAgendaMappingId { get; set; }
        [AllowHtml]
        public string AgendaFormate { get; set; }
        public string AgendaResponse { get; set; }
        public string AgendaResponseResult { get; set; }
        public bool Isvooting { get; set; }
        public string Result { get; set; }
        public int userID { get; set; }
        public string MyResponses { get; set; }
        public bool hasDocuments { get; set; }
        
    }

    public class MeetingParticipants
    {
        public long ParticiapntId { get; set; }
        public long? DirectorId { get; set; }
        public long MeetingId { get; set; }
        public string DirectorName { get; set; }

    }

    public class MeetingResolution : IMessage
    {
        public long MeetingId { get; set; }
        public long AgendaId { get; set; }
        public long AgendaMappingId { get; set; }
        [AllowHtml]
        public string ResolutionFormatHeadingSub { get; set; }
        [AllowHtml]
        public string ResolutionFormatSub { get; set; }

    }

    public class MeetingCTC
    {
        public long MeetingId { get; set; }
        public long AgendaId { get; set; }
        public int? DirectorId { get; set; }
        public string DirectorName { get; set; }
        public string MeetingMinutesHeadingFormate { get; set; }
        public string MeetingMinutesFormate { get; set; }
        public string MeetingResolutionHeadingFormate { get; set; }
        public string MeetingResolutionFormate { get; set; }
        public string MeetingVenu { get; set; }
        public int? EntityId { get; set; }
        public string EntityName { get; set; }
        public int? MeetingNo { get; set; }
        public string MeetingDay { get; set; }
        public DateTime MeetingDate { get; set; }
        public string MeetingTime { get; set; }
        public string MeetingPlace { get; set; }
        public string NameofDirector { get; set; }
        public string DinNumberofDirector { get; set; }
        public string DesignationofDirector { get; set; }
    }
    public class AgendaListforCTC
    {
        public long MeetingId { get; set; }
        public long? AgendaId { get; set; }
        public long MeetingAgendaId { get; set; }
        public string AgendaName { get; set; }
        public int? DirectorId { get; set; }
    }

    public class AGMVoting
    {
        public long VotingResponseId { get; set; }
        public long MeetingID { get; set; }
        public AgendaTypes ListofAgendaType { get; set; }
        public long AgendaId { get; set; }
        public long? AgendaMappingID { get; set; }
        [AllowHtml]
        public string Agenda { get; set; }
        public string VotingResponse { get; set; }
        public bool IsVoting { get; set; }
        public bool? AllowUpdate { get; set; }
        public bool? HasPreCommittee { get; set; }
        [AllowHtml]
        public string PreCommitteResult { get; set; }

        public bool IsOrdinaryBusiness { get; set; }
        public bool IsSpecialResolution { get; set; }
        public string Business_ { get; set; }
        public string PoposedBy { get; set; }
        public string SecondedBy { get; set; }
        public int? Customer_Id { get; set; }
    }
    public class AgendaTypes
    {
        public string AgendaTypeId { get; set; }
        public string AgendaName { get; set; }
    }

}