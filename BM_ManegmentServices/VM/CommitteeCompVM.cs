﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class CommitteeCompVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Please enter name"),  MaxLength(50)]
        [RegularExpression(@"^[a-zA-Z\s]*$", ErrorMessage = "Please Enter valid name")]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description_ { get; set; }
        public int? RestrictedDesignation { get; set; }
        public int? RequiredDesignation { get; set; }
        //public int RequiredCount { get; set; }
        //public int Quorum { get; set; }
        public int? Customer_Id { get; set; }
        public bool AsPerStandard { get; set; }

        public bool? IsCommittee { get; set; }
        public bool? IsMeeting { get; set; }
        public bool? IsCircular { get; set; }
        public string CompositionType { get; set; }
        public string MeetingTypeName { get; set; }

        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public Response Response { get; set; }
    }

    public class CommitteeCompRulesForVM
    {
        public int CommitteeId { get; set; }
        public int Entity_Type { get; set; }
        public int? Customer_Id { get; set; }
        public bool AsPerStandard { get; set; }
        public Composition MinimumDirector { get; set; }
        public Composition Quorum { get; set; }
        public MeetingDetails Meeting { get; set; }
    }
    public class Composition
    {
        public long RuleId { get; set; }
        public int CommitteeId { get; set; }
        public int Entity_Type { get; set; }
        public string RuleType { get; set; }
        public string Rule { get; set; }
        public string Designation { get; set; }
        public int? DesignationId { get; set; }
        public int? DesignationIdQuorum { get; set; }

        [RegularExpression(@"(^[+]?([0-9]+)$)", ErrorMessage = "Please enter valid number")]
        public int? DesignationCount { get; set; }
        [RegularExpression(@"(^[+]?([0-9]+)$)", ErrorMessage = "Please enter valid number")]
        public int? DesignationNumerator { get; set; }
        [RegularExpression(@"(^[+]?([0-9]+)$)", ErrorMessage = "Please enter valid number")]
        public int? DesignationDenominator { get; set; }
        public int? Customer_Id { get; set; }

        public int? Entity_Id { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public Response Message { get; set; }

        public long? StdRefId { get; set; }
    }
    public class MeetingDetails
    {
        public long MeetingId { get; set; }
        public int CommitteeId { get; set; }
        public int Entity_Type { get; set; }

        [RegularExpression(@"(^[+]?([0-9]+)$)", ErrorMessage = "Please enter valid number")]
        public int? MinMeeting { get; set; }
        public int? MeetingGapInDays { get; set; }
        public bool VotingRequirement { get; set; }
        public bool DisclosureRequirement { get; set; }

        public bool IsMeetingSchedule { get; set; }

        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? Customer_Id { get; set; }
        public int? Entity_Id { get; set; }
        public long? StdRefId { get; set; }
        public List<MeetingSchedule> lstMeetingSchedule { get; set; }

        public Response Message { get; set; }
    }

    public class MeetingSchedule
    {
        public long MeetingScheduleId { get; set; }
        public int CommitteeId { get; set; }
        public int Entity_Type { get; set; }
        public string Details { get; set; }
        public string Quarter_ { get; set; }

        public int Day { get; set; }
        public int Month { get; set; }

        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public int? Customer_Id { get; set; }
        public int? Entity_Id { get; set; }

    }

    public class Days
    {
        public int Day_ { get; set; }
        public string DayName_ { get; set; }
    }

    public class CommittePageAuthorization
    {
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
    }

    public class CommitteeMatrixVM
    {
        public long Id { get; set; }
        public int CommitteeId { get; set; }
        public string CommitteeType { get; set; }
        public bool? IsStandard { get; set; }
        public string CommitteeName { get; set; }
        public int? DirectorId { get; set; }
        public int? UserId { get; set; }
        public string ParticipantType { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public int? Gender { get; set; }
        public string DIN_PAN { get; set; }
        public int? DesignationId { get; set; }
        public string Designation { get; set; }
        public int? DirectorShipIdForQuorum { get; set; }
        public string DirectorShip { get; set; }
        public string ParticipantDesignation { get; set; }
        public int PositionId { get; set; }
        public string Position { get; set; }
        public System.DateTime? DateOfAppointment { get; set; }
        public System.DateTime? DateOfCessation { get; set; }
        public string PhotoDoc { get; set; }
    }
}