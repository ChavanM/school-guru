﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class AnnualMeetingVM:IMessage
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please select Entity")]
        public int EntityId { get; set; }
        public int MeetingTypeId { get; set; }
        public string EntityName { get; set; }
        public string MeetingTypeName { get; set; }
        public string MeetingTitle { get; set; }
        public string Circular_Meeting { get; set; }
        public string Type { get; set; }
        public long FYID { get; set; }
        public string FY { get; set; }
        public string status { get; set; }
        public bool IsOther { get; set; }
        public string AgendaTag { get; set; }
        public int? MeetingSRNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MeetingDate { get; set; }
        public string MeetingStartTime { get; set; }
        public string MeetingEndTime { get; set; }
        public string Quarter_ { get; set; }

        public int MeetingNo { get; set; }
        public bool Isdefaultcheck { get; set; }

        public bool IsComcheck { get; set; }

        public string FileTags { get; set; }

        public IEnumerable<HttpPostedFileBase> files { get; set; }
    }

    public class AnnualMeetingSheduler : AnnualMeetingVM, ISchedulerEvent
    {
        
        public string Description { get; set; }
      
        public DateTime End { get; set; }

        public string EndTimezone { get; set; }
       

        public bool IsAllDay { get; set; }
      

        public string RecurrenceException { get; set; }
      

        public string RecurrenceRule { get; set; }
       

        public DateTime Start { get; set; }
     

        public string StartTimezone { get; set; }
        

        public string Title { get; set; }
        
    }

    public class AddAnnualMeetingCalenderVM
    {
    
        public IEnumerable<EntityDetails> Entity { get; set; }

      
        public IEnumerable<FYearDetails> FY { get; set; }

     
        public IEnumerable<CommitteeCompVM> MeetingType { get; set; }

     
    }

    public class EntityDetails
    {
        public int EntityId { get; set; }
        public string EntityName { get; set; }
      
    }
    public class FYearDetails
    {
        public int FYID { get; set; }
        public string FYText { get; set; }
        public int EntityId { get; set; }
    }

    public class AnnualMVM:IMessage
    {
      
        public long AnnualMeetingId { get; set; }

        [UIHint("Id")]
        [Required(ErrorMessage = "Type is required")]
        public int Id { get; set; }

        [UIHint("EntityId")]
        [Required(ErrorMessage = "Entity is required")]
        public int EntityId { get; set; }


        [UIHint("FYID")]
        [Required(ErrorMessage = "FY is required")]
        public int FYID { get; set; }
        public int MeetingSrNumber { get; set; }
        [Required(ErrorMessage = "Date is required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MeetingDate { get; set; }

        public string StartmeetingTime { get; set; }

        public string EndmeetingTime { get; set; }

    }


}