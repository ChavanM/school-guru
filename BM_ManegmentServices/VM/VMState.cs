﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMState
    {
        public int SIdPublic { get; set; }
        public string Name { get; set; }
        public int countryId { get; set; }
        public string Message { get; set; }
        public bool error { get; set; }
        public bool success { get; set; }
    }
}