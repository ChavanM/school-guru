﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{
    public class Template_MasterVM :IMessage
    {
        public long TemplateMasterID { get; set; }
        public string MeetingCircular { get; set; }
        public int MeetingTypeId { get; set; }
        public string TemplateType { get; set; }
        public string Quarter_ { get; set; }
        public bool? IsQuarter { get; set; }
        public bool IsShorter { get; set; }

        [AllowHtml]
        public string MailTemplate { get; set; }
        public string SMSTemplate { get; set; }
        public int Entityt_Id { get; set; }
        public bool IsDeleted { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

        public int userId { get; set; }

    }
}