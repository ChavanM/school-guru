﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMNotes:IMessage
    {
        public long Id { get; set; }
        public long MeetingId { get; set; }
        public long AgendaId { get; set; }
        public string AgendaName { get; set; }
        public string userName { get; set; }
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public List<long> DirectorId { get; set; }
        public string type { get; set; }
        [Required]
        public string NotesDetails { get; set; }
        public int FlagId { get; set; }
        [Required]
        public string Title { get; set; }
    }
}