﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.Compliance
{
    public class VM_MeetingCamplianceMapping: AgendaComplianceEditorVM
    {
        public long Id { get; set; }
        public string MeetingTypeName { get; set; }
        public bool IsMeeting_Circular { get; set; }
        public int CustomerId { get; set; }
        //public int UserId { get; set; }
        public int MeetingTypeId { get; set; }
        public long EntityID { get; set; }
        public int EntityType { get; set; }
    }
}