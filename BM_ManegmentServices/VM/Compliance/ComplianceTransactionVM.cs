﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.Compliance
{
    public class ComplianceStatusVM
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
    public class ComplianceTransactionVM : IMessage
    {
        public long ComplianceId { get; set; }
        public long TransactionID { get; set; }
        public long ComplianceScheduleOnID { get; set; }
        [Required(ErrorMessage = "Please Select Status")]
        public int StatusId { get; set; }
        public string Remarks { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please Enter Date")]
        public System.DateTime Dated { get; set; }
        public string ClosedTime { get; set; }
        public string DaysOrHours { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByText { get; set; }
        public System.DateTime? StatusChangedOn { get; set; }

        public long? MeetingID_ { get; set; }
        public int? EntityID_ { get; set; }

        public int? RoleID { get; set; }
        public bool IsPerformerReviewerSame { get; set; }
    }

    public class ComplianceTransactionLogVM
    {
        public long TransactionID { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public System.DateTime Dated { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByText { get; set; }
        public System.DateTime? StatusChangedOn { get; set; }
    }
}