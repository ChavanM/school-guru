﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class ReportVM
    {
        public long PolicesID { get; set; }
        public Response Response { get; set; }
        public long? EntityId { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public string FilePath { get; set; }
        [Required]
        public HttpPostedFileBase File { get; set; }
        public string ReportName { get; set; }
    }
}