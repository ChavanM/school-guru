﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{
    public class Equity
    {
        public string EntityName { get; set; }
        public int EntityId { get; set; }
        public VMCapitalData VMCapitalData { get; set; }
        public VMPrifrenscShare VMPrifrenscShare { get; set; }
        public VMDebentures VMDebentures { get; set; }
        public VMUnclassified VMUnclassified { get; set; }
        [Required]
        public AuthorizedCapital AuthorizedCapital { get; set; }
    }

    public class AuthorizedCapital
    {
        public long AuthorisedId { get; set; }
        [Required(ErrorMessage = "Please Enter Authorized Capital")]
      
        public decimal AuthorizedCapval { get; set; }

        public decimal? paidupCapita { get; set; }
       // public decimal PaidupCapitalval { get; set; }
        public int Entity_Id { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string SuccessErrorMsg { get; set; }
    }
    public class VMUnclassified
    {
        public long Id { get; set; }
        public bool IsUnclassified { get; set; }
        public decimal UnAuthorizedCapita { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string SuccessErrorMsg { get; set; }
    }
    public class VMDebentures
    {
        public long Id { get; set; }
        public bool IsDebentures { get; set; }
        public long NumberofUnitsncd { get; set; }
        public decimal NominalValueperunitncd { get; set; }
        public decimal TotalValuencd { get; set; }
        public long NumberofUnitspcd { get; set; }
        public decimal NominalValueperunitnpcd { get; set; }
        public decimal TotalValuenpcd { get; set; }
        public long NumberofUnitsfcd { get; set; }
        public decimal NominalValueperunitnfcd { get; set; }
        public decimal TotalValuenfcd { get; set; }
        public decimal TotalDebentures { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string SuccessErrorMsg { get; set; }

    }
    public class VMCapitalData
    {
        public int EntityId { get; set; }
        public long Id { get; set; }
        [Required]
      
        public decimal TnAuthorisedCapital { get; set; }
        [Required]
      
        public decimal TnIssuedCapital { get; set; }
        [Required]
      
        public decimal TnSubscribedCapital { get; set; }
        [Required]
      
        public decimal TnPaidupCapital { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal TaAuthorisedCapital { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal TaIssuedCapital { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal TaSubscribedCapital { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal TaPaidupCapital { get; set; }
        public int count { get; set; }
        public List<Shares> shares { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string SuccessErrorMsg { get; set; }
    }
    public class Shares
    {
        public long Equ_sharesID { get; set; }
        [Required]
        public int sharesID { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public long AuthorisedCapital1 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public long IssuedCapital1 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public long SubscribedCapital1 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public long PaidupCapital1 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal AuthorisedCapital2 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal IssuedCapital2 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal SubscribedCapital2 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal PaidupCapital2 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal AuthorisedCapital3 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal IssuedCapital3 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal SubscribedCapital3 { get; set; }
        [Required]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Value should be not -(ve) value")]
        public decimal PaidupCapital3 { get; set; }
        public bool Message { get; set; }
        public string SuccessErrorMsg { get; set; }
    }
    public class VMPrifrenscShare
    {
        public long Id { get; set; }
        //[Required(ErrorMessage = "Please select Preferance capital Type")]
        [Required, Range(0, int.MaxValue, ErrorMessage = "Please select Preference capital Type")]
        public int Preferance_capitalType { get; set; }
        [Required (ErrorMessage = "Please enter Coupon Rate")]
        public decimal Coupen_Rate { get; set; }
        public bool IsPrefrence { get; set; }
        public decimal pTnAuthorisedCapital { get; set; }
        public decimal pTnIssuedCapital { get; set; }
        public decimal pTnSubscribedCapital { get; set; }
        public decimal pTnPaidupCapital { get; set; }
        public decimal pTaAuthorisedCapital { get; set; }
        public decimal pTaIssuedCapital { get; set; }
        public decimal pTaSubscribedCapital { get; set; }
        public decimal pTaPaidupCapital { get; set; }
        public decimal pTaAuthorisedCapital1 { get; set; }
        public decimal pTaIssuedCapital1 { get; set; }
        public decimal pTaSubscribedCapital1 { get; set; }
        public decimal pTaPaidupCapital1 { get; set; }
        public string pClassofShares { get; set; }
        public int pcount { get; set; }
        public pShares pshares { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string SuccessErrorMsg { get; set; }
    }
    public class pShares
    {
        public long psharesID { get; set; }
        public long pAuthorisedCapital1 { get; set; }
        public long pIssuedCapital1 { get; set; }
        public long pSubscribedCapital1 { get; set; }
        public long pPaidupCapital1 { get; set; }
        public decimal pAuthorisedCapital2 { get; set; }
        public decimal pIssuedCapital2 { get; set; }
        public decimal pSubscribedCapital2 { get; set; }
        public decimal pPaidupCapital2 { get; set; }
        public decimal pAuthorisedCapital3 { get; set; }
        public decimal pIssuedCapital3 { get; set; }
        public decimal pSubscribedCapital3 { get; set; }
        public decimal pPaidupCapital3 { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
    }



    public class EntityCpitalDetails
    {
        public long capitalId { get; set; }
        public decimal AuthorizedCapital { get; set; }
        public string AuthorizedCapitall { get; set; }
        public decimal PaidupCapital { get; set; }
        public string PaidupCapitall { get; set; }
        public decimal EquietyCapital { get; set; }
        public string EquietyCapitall { get; set; }
        public decimal PrefrenceCapital { get; set; }
        public string PrefrenceCapitall { get; set; }
        public decimal? DebentureCapital { get; set; }
        public string Debenture { get; set; }
        public decimal Othersecurities { get; set; }

        public decimal EquietyNominalVal { get; set; }
        public decimal PrefrenceNominalVal { get; set; }
        public decimal? DebencuteNominalVal { get; set; }
    }

}