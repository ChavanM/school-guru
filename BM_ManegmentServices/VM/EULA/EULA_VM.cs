﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.EULA
{
    public class EULA_VM
    {
    }
    public class EULA_DetailsVM : IMessage
    {
        public int? EULA_ID { get; set; }
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public bool? EULA { get; set; }
        public DateTime? AcceptedOn { get; set; }
        public DateTime? UploadedOn { get; set; }
        public string FilePath { get; set; }
        public bool? IsDocUploaded { get; set; }
        public byte? StatusId { get; set; }
        public string Status { get; set; }
        public string UserMode { get; set; }
        public IEnumerable<HttpPostedFileBase> files { get; set; }

        public string MembershipNumber { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public Nullable<int> LastLoginBefore { get; set; }
    }
}