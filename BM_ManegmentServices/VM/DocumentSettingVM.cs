﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class DocumentSettingVM
    {
        public PageSettingVM PageSetting { get; set; }
        public List<StyleVM> Styles { get; set; }
    }

    public class PageSettingVM
    {
        public string PageSize { get; set; }
        public double MarginTop { get; set; }
        public double MarginLeft { get; set; }
        public double MarginRight { get; set; }
        public double MarginBottom { get; set; }
    }

    public class StyleVM
    {
        public string StyleName { get; set; }
        public string FontName { get; set; }
        public string FontSize { get; set; }
        public string FontColor { get; set; }
        public bool IsBold { get; set; }
        public bool IsItalic { get; set; }
        public bool IsUnderline { get; set; }
    }
}