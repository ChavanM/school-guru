﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BM_ManegmentServices.VM
{
    public class VMDesignation
    {
        public int ID { get; set; }
        [Required(ErrorMessage ="Please Enter Designation.")]
        public string Name { get; set; }

        public bool IsDeleted { get; set; }
        
        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
    }
}