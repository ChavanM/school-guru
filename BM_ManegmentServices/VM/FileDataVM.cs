﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class FileDataVM : IMessage
    {
        public long FileID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
        public byte[] FileData { get; set; }
        public string Version { get; set; }
        public DateTime? VersionDate { get; set; }
        public string VersionComment { get; set; }

        public string EnType { get; set; }

        public long MeetingId { get; set; }
        public string MeetingDoc { get; set; }
    }

    public class FileDataDocumentVM 
    {
        public long FileID { get; set; }
        public string FileName { get; set; }
        public string Version { get; set; }
        public DateTime? VersionDate { get; set; }
        public string UploadedBy { get; set; }
        public short? FileType { get; set; }
    }

    public class FileStorageDataVM : IMessage
    {
        public long MeetingId { get; set; }
        public HttpPostedFileBase Files { get; set; }
    }

    public class FileSrNoVM
    {
        public long FileID { get; set; }
        public int? SrNo { get; set; }
    }
}