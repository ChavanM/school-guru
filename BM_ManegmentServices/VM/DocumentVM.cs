﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.DocumentManagenemt
{
    public class DocumentVM
    {
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public bool IsFileGenerated { get; set; }
    }
}