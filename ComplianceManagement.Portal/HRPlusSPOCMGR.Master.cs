﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class HRPlusSPOCMGR : System.Web.UI.MasterPage
    {
        protected string LastLoginDate;
        protected string CustomerName;
        protected string userRole;
        protected List<Int32> roles;
        protected List<Int32> TaskRoles;
        protected static int customerid;
        protected static int userid;
        protected string Approveruser_Roles;
        protected int checkTaskapplicable = 0;

        protected bool showMyWorkspace = false;
        protected bool vendorAuditApplicable = false;

        protected IList<Cust> lstUserAssignedCustomers;
        public class Cust
        {
            public int custID { get; internal set; }
            public string custName { get; internal set; }
            public string logoPath { get; internal set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    userRole = AuthenticationHelper.Role;

                    if (Session["AssignedRoles"] != null)
                    {
                        roles = (Session["AssignedRoles"]) as List<int>; ;
                    }
                    else
                    {
                        roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                        Session["AssignedRoles"] = roles;
                    }

                    //BindUserAssignedCustomers();

                    if (!IsPostBack)
                    {
                        hdnProfileID.Value = AuthenticationHelper.ProfileID;
                        hdnAuthKey.Value = AuthenticationHelper.AuthKey;

                        customerid = Convert.ToInt32(AuthenticationHelper.CustomerID); //UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                        userid = AuthenticationHelper.UserID;
                        
                        //if (Session["vendorAuditApplicable"] != null)
                        //{
                        //    vendorAuditApplicable = Convert.ToBoolean(Session["vendorAuditApplicable"]);
                        //}
                        //else
                        //{
                        //    vendorAuditApplicable = RLCSManagement.CheckScopeApplicability(customerid, "SOW10");
                        //    Session["vendorAuditApplicable"] = vendorAuditApplicable;
                        //}

                        showMyWorkspace = true;
                        //if (Session["showMyWorkspace"] != null)
                        //{
                        //    showMyWorkspace = Convert.ToBoolean(Session["showMyWorkspace"]);
                        //}
                        //else
                        //{
                        //    showMyWorkspace = RLCS_Master_Management.CheckShowHideInputs(customerid, userid, 95);
                        //    Session["showMyWorkspace"] = showMyWorkspace;
                        //}

                        Page.Header.DataBind();

                        if (Session["LastLoginTime"] != null)
                        {
                            LastLoginDate = Session["LastLoginTime"].ToString();
                        }

                        if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
                        {
                            if (AuthenticationHelper.UserID != -1)
                            {
                                //var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                                //if (cname != null)
                                //{
                                //    CustomerName = cname;
                                //}
                            }
                        }

                        User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                        if (LoggedUser != null)
                        {
                            if (LoggedUser.ImagePath != null)
                            {
                                ProfilePicTop.Src = LoggedUser.ImagePath;
                            }
                            else
                            {
                                ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                            }
                        }
                        else
                        {
                            ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                        }

                        string pageName = this.ContentPlaceHolder1.Page.GetType().FullName;
                    }
                }
                else
                {                    
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        public void BindUserAssignedCustomers()
        {
            try
            {
                int loggedInUserID = Convert.ToInt32((AuthenticationHelper.UserID));

                //var custmerlist = UserCustomerMappingManagement.Get_UserCustomerMapping(loggedInUserID);
                var custmerlist = UserCustomerMappingManagement.Get_UserCustomerMapping(loggedInUserID, AuthenticationHelper.Role);

                if (custmerlist.Count > 1)
                {
                    lstUserAssignedCustomers = new List<Cust>();

                    foreach (var item in custmerlist)
                    {
                        string path = string.Empty;
                        if (item.LogoPath != null)
                            path = item.LogoPath.Replace("~", "");

                        lstUserAssignedCustomers.Add(new Cust()
                        {
                            custID = item.ID,
                            custName = item.Name,
                            logoPath = string.IsNullOrEmpty(path) ? null : path
                        });                       
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClick_Click(object sender, EventArgs e)
        {
            if (hfCustId.Value != "")
            {
                int customerID = Convert.ToInt32(hfCustId.Value);
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", "alert('" + customerID + "')", true);

                ProductMappingStructure _obj = new ProductMappingStructure();
                if (_obj.ReAuthenticate_User(customerID))
                    Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
            }
        }
    }
}