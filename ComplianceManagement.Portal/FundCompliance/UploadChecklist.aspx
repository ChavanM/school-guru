﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="UploadChecklist.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.FundCompliance.Common.UploadChecklist" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .aspNetDisabled {
            width: 100%;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            color: #8e8e93;
            background-color: #fff;
            border: 1px solid #c7c7cc;
            border-radius: 4px;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .modal-header-custom {
    display: block;
    float: left;
    font-size: 20px;
    color: #1fd9e1;
    /* width: 225px; */
    font-weight: 400;
}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftuploadmenu');
            fhead('Checklist Master');            
        });
        function fopenpopup() {
            $('#divOpenNewFolderPopup').modal('show');
        }
        function fclosepopup() {
            $('#divOpenNewFolderPopup').modal('hide');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upUploadUtility">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row Dashboard-white-widget">
    <asp:UpdatePanel ID="upUploadUtility" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">

                    <div class="col-lg-12 col-md-12" style="min-height: 0px; max-height: 250px; overflow-y: auto;">
                        <asp:Panel ID="vdpanel" runat="server" ScrollBars="Auto">
                            <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                            <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                        </asp:Panel>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-lg-12 col-md-12">
                        <div runat="server" id="divCase">

                            <div style="clear:both;height:20px;">&nbsp;</div>
                            <div class="col-md-12 colpadding0" style="border: 1px solid #ccc;height: 49px;border-radius: 9px;padding:9px;">
                                  <div class="col-md-3">
                                    <asp:FileUpload ID="MasterFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                    <asp:RequiredFieldValidator ID="rfvFileUpload" ErrorMessage="Please Select File" ControlToValidate="MasterFileUpload"
                                        runat="server" Display="None" ValidationGroup="uploadUtilityValidationGroup" />
                                </div>
                                <div class="col-md-1 colpadding0 text-right">
                                    <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="uploadUtilityValidationGroup"
                                        class="btn btn-primary" OnClick="btnUploadFile_Click" OnClientClick="showProgress()" />

                                    <asp:Button ID="btnAddNew" runat="server" Text="Add New"
                                        class="btn btn-primary" OnClientClick="fopenpopup()" Visible="false" />
                                </div>
                                <div class="col-md-2 colpadding0 text-right">
                                    <asp:LinkButton ID="lnkSampleFormat" class="newlink" Font-Underline="True" OnClick="lnkSampleFormat_Click"
                                        data-toggle="tooltip" data-placement="bottom" runat="server" Text="Sample Format(Excel)"
                                        ToolTip="Download Sample Excel Document Format for Checklist Upload"></asp:LinkButton>
                                </div>
                                   <div class="col-md-4 colpadding0 text-right">
                                   </div>
                                   <div class="col-md-2 colpadding0 text-left">
                                    <asp:DropDownListChosen runat="server" ID="ddlDocumentType"
                                        DataPlaceHolder="Select Type" Height="30px" Width="100%" class="form-control" OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="Fund Documents" Value="FD" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Side Letter" Value="SL"></asp:ListItem>
                                        </asp:DropDownListChosen>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ddlDocumentType" />
             <asp:PostBackTrigger ControlID="btnUploadFile" />
            <asp:PostBackTrigger ControlID="lnkSampleFormat" />
        </Triggers>
    </asp:UpdatePanel>


    <div class="col-md-12 colpadding0">
        <asp:UpdatePanel ID="upUploadAuditChecklist" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView runat="server" ID="grdChecklistDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    OnRowCommand="grdChecklistDetails_RowCommand"  PageSize="10" AllowPaging="true" OnRowDataBound="grdChecklistDetails_RowDataBound" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="2%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Relevant Clause" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Clause_ref_num") %>' ToolTip='<%# Eval("Clause_ref_num") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Compliance Question Relating to Agreed Term/Conditions" ItemStyle-Width="50%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Comp_Que") %>' ToolTip='<%# Eval("Comp_Que") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField> 
                          <asp:TemplateField HeaderText="Investor" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("InvestorName") %>' ToolTip='<%# Eval("InvestorName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>                       
                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                            <ItemTemplate>
                            <asp:LinkButton ID="lnkEditChecklist" runat="server" CommandName="Edit_Checklist" ToolTip="Edit Checklist" data-toggle="tooltip" data-placement="bottom"
                                    CommandArgument='<%# Eval("Id") %>'>
                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit Checklist" /></asp:LinkButton>

                                <asp:LinkButton ID="lnkDeleteChecklist" runat="server" CommandName="Delete_Checklist" ToolTip="Delete Checklist" data-toggle="tooltip" data-placement="bottom"
                                    CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('Are you sure, you want to delete this Checklist?');">
                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Checklist" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-10 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">                                
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width:100%; float: right; margin-right:6%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                    <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                </div>
            </div>
    <div class="col-md-12 colpadding0">
        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
    </div>
      <div class="modal fade" id="divOpenNewFolderPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Update Checklist Details</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <asp:UpdatePanel ID="upMailDocument" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:ValidationSummary ID="ValidationSummary4" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="vsFolderDocumentValidationGroup" />
                                    <asp:CustomValidator ID="cvCasePayment" runat="server" EnableClientScript="False"
                                        ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                </div>
                                <div class="form-group col-md-12 colpadding0">
                                                        <asp:UpdatePanel ID="upCasePayment" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:GridView runat="server" ID="grdStatus" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                                                                    OnRowCommand="grdStatus_RowCommand" OnRowDataBound="grdStatus_RowDataBound" OnRowEditing="grdStatus_RowEditing"                                                                    
                                                                    PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdStatus_PageIndexChanging">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="2%" FooterStyle-Width="2%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="30%" FooterStyle-Width="30%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLstatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:DropDownList runat="server" ID="ddlStatus" DataPlaceHolder="Lawyer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    Class="form-control" Width="100%">
                                                                                </asp:DropDownList>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="55%" FooterStyle-Width="55%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblRemark" runat="server" Text='<%# Eval("Remark") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxRemark" runat="server" class="form-control" PlaceHolder="Remark"></asp:TextBox>
                                                                                <asp:TextBox ID="tbxCheckliststatusID" runat="server" CssClass="form-control" Style="display: none"></asp:TextBox>

                                                                                <asp:RequiredFieldValidator ID="rfvPaymentRemark" ErrorMessage="Provide Remark."
                                                                                    ControlToValidate="tbxRemark" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left" ItemStyle-Width="100%"
                                                                            FooterStyle-Width="100%" FooterStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                                                                                          
                                                                                     <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                        AutoPostBack="true" CommandName="Edit_status"
                                                                                        ID="lnkBtnEditPayment" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" title="Edit" />
                                                                                    </asp:LinkButton>
                                                                                
                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                        AutoPostBack="true" CommandName="Delete_Status"
                                                                                        OnClientClick="return confirm('Are you sure!! You want to Delete this Status Detail?');"
                                                                                        ID="lnkBtnDeletePayment" runat="server">
                                                                                            <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" 
                                                                                                title="Delete" />
                                                                                    </asp:LinkButton>
                                                                                </div>
                                                                                  
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:Button CssClass="btn btn-primary" ID="btnStatusremarkSave" runat="server" Text="Save" CausesValidation="true" OnClick="btnStatusremarkSave_Click"></asp:Button><%--ValidationGroup="CasePopUpPaymentLogValidationGroup"--%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="grdStatus" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>

