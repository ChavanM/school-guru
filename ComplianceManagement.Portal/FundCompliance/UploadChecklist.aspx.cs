﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.ComplianceFund;

namespace com.VirtuosoITech.ComplianceManagement.Portal.FundCompliance.Common
{
    public partial class UploadChecklist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindGrid();
                    bindPageNumber();                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      

      
        public void BindGrid()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int Dtype = 1;
                    if (ddlDocumentType.SelectedValue == "FD")
                    {
                        Dtype = 1;
                    }
                    if (ddlDocumentType.SelectedValue == "SL")
                    {
                        Dtype = 0;
                    }

                    #region new 
                    var lstChecklistDetails = FundMasterManagement.GetChecklistNew(Dtype,Convert.ToInt32(AuthenticationHelper.CustomerID));
                          
                    Session["TotalRows"] = null;
                    if (lstChecklistDetails.Count > 0)
                    {
                        grdChecklistDetails.DataSource = lstChecklistDetails;
                        grdChecklistDetails.DataBind();
                        Session["TotalRows"] = lstChecklistDetails.Count;
                    }
                    else
                    {
                        grdChecklistDetails.DataSource = lstChecklistDetails;
                        grdChecklistDetails.DataBind();
                    }

                    lstChecklistDetails.Clear();
                    lstChecklistDetails = null;
                    #endregion                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

      

        #region Common Methods
        //public bool checkDuplicateMultipleDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, string ActIDText, string StateIDText, string NatureOfComplianceText)
        //{
        //    bool matchSuccess = false;
        //    try
        //    {
        //        if (xlWorksheet != null)
        //        {
        //            string TextToCompare = string.Empty;
        //            int lastRow = xlWorksheet.Dimension.End.Row;

        //            for (int i = 2; i <= lastRow; i++)
        //            {
        //                if (i != currentRowNumber)
        //                {
        //                    string TextToCompareActId = xlWorksheet.Cells[i, 1].Text.ToString().Trim();
        //                    if ((TextToCompareActId.Trim()).Equals(ActIDText.Trim()))
        //                    {
        //                        string TextToCompareStateId = xlWorksheet.Cells[i, 3].Text.ToString().Trim();
        //                        if ((TextToCompareStateId.Trim()).Equals(StateIDText.Trim()))
        //                        {
        //                            string TextToCompareNatureOfcompliance = xlWorksheet.Cells[i, 4].Text.ToString().Trim();
        //                            if ((TextToCompareNatureOfcompliance.Trim()).Equals(NatureOfComplianceText.Trim()))
        //                            {
        //                                matchSuccess = true;
        //                                i = lastRow; //exit from for Loop
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        return matchSuccess;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return matchSuccess;
        //    }
        //}

        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
            vsUploadUtility.CssClass = "alert alert-danger";
        }

        #endregion

        protected void lnkSampleFormat_Click(object sender, EventArgs e)
        {
            try
            {
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                string filePath = Server.MapPath("~/FundCompliance/SampleFormat/ComplianceChecklistDocument_Upload_Format.xlsx");

                if (!string.IsNullOrEmpty(filePath))
                {
                    response.Clear();
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.AddHeader("Content-Disposition", "attachment;filename=ComplianceChecklistDocument_Upload_Format.xlsx");
                    byte[] data = req.DownloadData(filePath);
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    response.BinaryWrite(data);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "No Format Available for Download, Please Contact Admin for more detail";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/FundCompliance/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/FundCompliance/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (ddlDocumentType.SelectedValue == "FD")
                            {
                                bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "Fund Document");
                                if (matchSuccess)
                                {
                                    ProcessChecklistData(xlWorkbook);
                                }
                                else
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'Fund Document'.";
                                }
                            }
                            else if (ddlDocumentType.SelectedValue == "SL")
                            {
                                bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "Side Letter");
                                if (matchSuccess)
                                {
                                    ProcessChecklistSideLetterData(xlWorkbook);
                                }
                                else
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'Side Letter'.";
                                }
                            }
                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }
        public class customerlist
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }
        private void ProcessChecklistSideLetterData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;

                int uploadedChecklistCount = 0;

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Side Letter"];

                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();

                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var objStatus = (from row in entities.CompFund_tbl_ComplianceStatus
                                         select row).ToList();

                        #region Validations

                        if (xlrow2 > 1)
                        {
                            for (int i = 2; i <= xlrow2; i++)
                            {
                                bool IsInvestorNameIsCorrect = false;
                                #region 1 Relevant Clause,Compliance Question,Status and remark
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                                {
                                    errorMessage.Add("Required Relevant Clause at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                                {
                                    errorMessage.Add("Required Compliance Question relating to Agreed Term/Condition at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                                {
                                    errorMessage.Add("Required Investor Name at row number" + i + ".");
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                                    {
                                        bool ChecklistInvestorExist = FundMasterManagement.ExistsInvestorName(xlWorksheet.Cells[i, 3].Text.Trim(), Convert.ToInt32(AuthenticationHelper.CustomerID));
                                        if (!ChecklistInvestorExist)
                                        {
                                            errorMessage.Add("Investor Name not exists at row number - " + i + "");
                                        }
                                    }
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim())
                                    && !String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim())
                                    && !String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                                {
                                    bool ChecklistExist = FundMasterManagement.ExistsSideLetterChecklistData(xlWorksheet.Cells[i, 1].Text.Trim(), xlWorksheet.Cells[i, 2].Text.Trim(), xlWorksheet.Cells[i, 3].Text.Trim(), false, Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (ChecklistExist)
                                    {
                                        errorMessage.Add("Checklist already exists at row number - " + i + "");
                                    }
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                                {
                                    string statusName = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                                    var chk = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                    if (chk != null)
                                    {
                                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                                        {
                                            errorMessage.Add("Required Remark1 at row number" + i + ".");
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Required Correct status1 as 'Not Applicable/Not Complied/Complied' at row number" + i + ".");
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                                    {
                                        errorMessage.Add("Required Status1 at row number - " + i + ".");
                                    }
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
                                {
                                    string statusName = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                    var chk = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                    if (chk != null)
                                    {
                                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                                        {
                                            errorMessage.Add("Required Remark2 at row number" + i + ".");
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Required Correct status2 as 'Not Applicable/Not Complied/Complied' at row number" + i + ".");
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                                    {
                                        errorMessage.Add("Required Status1 at row number - " + i + ".");
                                    }
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 8].Text).Trim()))
                                {
                                    string statusName = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                                    var chk = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                    if (chk != null)
                                    {
                                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 9].Text).Trim()))
                                        {
                                            errorMessage.Add("Required Remark3 at row number" + i + ".");
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Required Correct status3 as 'Not Applicable/Not Complied/Complied' at row number" + i + ".");
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 9].Text).Trim()))
                                    {
                                        errorMessage.Add("Required Status3 at row number - " + i + ".");
                                    }
                                }
                                #endregion
                            }
                        }
                        else
                            errorMessage.Add("Empty Sheet for Fund Document Upload.");
                        #endregion

                        int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                        int CustID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                        if (errorMessage.Count <= 0)
                        {
                            #region Save                        
                            for (int i = 2; i <= xlrow2; i++)
                            {
                                long InvestorID = FundMasterManagement.GetInvestorNameID(xlWorksheet.Cells[i, 3].Text.Trim(), Convert.ToInt32(AuthenticationHelper.CustomerID));
                                CompFund_tbl_Checklist objchklst = new CompFund_tbl_Checklist()
                                {
                                    Clause_ref_num = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim(),
                                    Comp_Que = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim(),
                                    CreatedOn = DateTime.Now,
                                    CreatedBy = UserID,
                                    CustomerId = CustID,
                                    IsDeleted = false,
                                    IsFundDocument = false,
                                    InvestorID = InvestorID
                                };
                                long ChecklistID = FundMasterManagement.saveCheckListDetail(objchklst);
                                if (ChecklistID > 0)
                                {
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                                    {
                                        string statusName = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                                        var statusdetails = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                        CompFund_tbl_Checklist_Status objchklststatus1 = new CompFund_tbl_Checklist_Status()
                                        {
                                            ChecklistID = ChecklistID,
                                            Status = statusdetails.ID.ToString(),
                                            Remark = xlWorksheet.Cells[i, 5].Text.Trim(),
                                            CreatedOn = DateTime.Now,
                                            CreatedBy = UserID,
                                            IsDeleted = false,
                                            CustomerId = CustID
                                        };
                                        if (!FundMasterManagement.ExistsChecklistStatusName(objchklststatus1))
                                            FundMasterManagement.saveCheckListStatusDetail(objchklststatus1);
                                    }
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
                                    {
                                        string statusName = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                        var statusdetails = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                        CompFund_tbl_Checklist_Status objchklststatus2 = new CompFund_tbl_Checklist_Status()
                                        {
                                            ChecklistID = ChecklistID,
                                            Status = statusdetails.ID.ToString(),
                                            Remark = xlWorksheet.Cells[i, 7].Text.Trim(),
                                            CreatedOn = DateTime.Now,
                                            CreatedBy = UserID,
                                            IsDeleted = false,
                                            CustomerId = CustID
                                        };
                                        if (!FundMasterManagement.ExistsChecklistStatusName(objchklststatus2))
                                            FundMasterManagement.saveCheckListStatusDetail(objchklststatus2);
                                    }
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 8].Text).Trim()))
                                    {
                                        string statusName = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                                        var statusdetails = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                        CompFund_tbl_Checklist_Status objchklststatus3 = new CompFund_tbl_Checklist_Status()
                                        {
                                            ChecklistID = ChecklistID,
                                            Status = statusdetails.ID.ToString(),
                                            Remark = xlWorksheet.Cells[i, 9].Text.Trim(),
                                            CreatedOn = DateTime.Now,
                                            CreatedBy = UserID,
                                            IsDeleted = false,
                                            CustomerId = CustID
                                        };
                                        if (!FundMasterManagement.ExistsChecklistStatusName(objchklststatus3))
                                            FundMasterManagement.saveCheckListStatusDetail(objchklststatus3);
                                    }
                                }
                                uploadedChecklistCount++;
                                saveSuccess = true;
                            }
                            #endregion
                        }
                        else
                        {
                            if (errorMessage.Count > 0)
                            {
                                ErrorMessages(errorMessage);
                            }
                        }

                        if (saveSuccess)
                        {
                            if (uploadedChecklistCount > 0)
                            {
                                BindGrid();
                                bindPageNumber();
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = uploadedChecklistCount + " Checklist Details Uploaded Successfully.";
                                //cvUploadUtilityPage.CssClass = "alert alert-success";
                                vsUploadUtility.CssClass = "alert alert-success";
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document";
                                vsUploadUtility.CssClass = "alert alert-danger";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
                vsUploadUtility.CssClass = "alert alert-danger";
            }
        }

        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;

                int uploadedChecklistCount = 0;

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Fund Document"];

                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();

                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var objStatus = (from row in entities.CompFund_tbl_ComplianceStatus
                                         select row).ToList();

                        #region Validations
                        if (xlrow2 > 1)
                        {
                            for (int i = 2; i <= xlrow2; i++)
                            {
                                #region 1 Relevant Clause,Compliance Question,Status and remark
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                                {
                                    errorMessage.Add("Required Relevant Clause at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                                {
                                    errorMessage.Add("Required Compliance Question relating to Agreed Term/Condition at row number" + i + ".");
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim())
                                    && !String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                                {
                                    bool ChecklistExist = FundMasterManagement.ExistsChecklistData(xlWorksheet.Cells[i, 1].Text.Trim(), xlWorksheet.Cells[i, 2].Text.Trim(), true, Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (ChecklistExist)
                                    {
                                        errorMessage.Add("Checklist already exists at row number - " + i + "");
                                    }
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                                {
                                    string statusName = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                                    var chk = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                    if (chk != null)
                                    {
                                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                                        {
                                            errorMessage.Add("Required Remark1 at row number" + i + ".");
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Required correct Status1 as 'Not Applicable/Not Complied/Complied' at row number" + i + ".");
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                                    {
                                        errorMessage.Add("Required Status1 at row number - " + i + ".");
                                    }
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                                {
                                    string statusName = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                                    var chk = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                    if (chk != null)
                                    {
                                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
                                        {
                                            errorMessage.Add("Required Remark2 at row number" + i + ".");
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Required Correct status2 as 'Not Applicable/Not Complied/Complied' at row number" + i + ".");
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
                                    {
                                        errorMessage.Add("Required Status2 at row number - " + i + ".");
                                    }
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                                {
                                    string statusName = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                                    var chk = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                    if (chk != null)
                                    {
                                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 8].Text).Trim()))
                                        {
                                            errorMessage.Add("Required Remark3 at row number" + i + ".");
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Required Correct status3 as 'Not Applicable/Not Complied/Complied' at row number" + i + ".");
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 8].Text).Trim()))
                                    {
                                        errorMessage.Add("Required Status3 at row number - " + i + ".");
                                    }
                                }
                                #endregion
                            }
                        }
                        else
                            errorMessage.Add("Empty Sheet for Fund Document Upload.");
                        #endregion

                        int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                        int CustID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                        if (errorMessage.Count <= 0)
                        {
                            #region Save                        
                            for (int i = 2; i <= xlrow2; i++)
                            {
                                CompFund_tbl_Checklist objchklst = new CompFund_tbl_Checklist()
                                {
                                    Clause_ref_num = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim(),
                                    Comp_Que = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim(),
                                    CreatedOn = DateTime.Now,
                                    CreatedBy = UserID,
                                    CustomerId = CustID,
                                    IsDeleted = false,
                                    IsFundDocument = true
                                };
                                long ChecklistID = FundMasterManagement.saveCheckListDetail(objchklst);
                                if (ChecklistID > 0)
                                {
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                                    {
                                        string statusName = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                                        var statusdetails = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                        CompFund_tbl_Checklist_Status objchklststatus1 = new CompFund_tbl_Checklist_Status()
                                        {
                                            ChecklistID = ChecklistID,
                                            Status = statusdetails.ID.ToString(),
                                            Remark = xlWorksheet.Cells[i, 4].Text.Trim(),
                                            CreatedOn = DateTime.Now,
                                            CreatedBy = UserID,
                                            IsDeleted = false,
                                            CustomerId = CustID
                                        };
                                        if (!FundMasterManagement.ExistsChecklistStatusName(objchklststatus1))
                                            FundMasterManagement.saveCheckListStatusDetail(objchklststatus1);
                                    }
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                                    {
                                        string statusName = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                                        var statusdetails = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                        CompFund_tbl_Checklist_Status objchklststatus2 = new CompFund_tbl_Checklist_Status()
                                        {
                                            ChecklistID = ChecklistID,
                                            Status = statusdetails.ID.ToString(),
                                            Remark = xlWorksheet.Cells[i, 6].Text.Trim(),
                                            CreatedOn = DateTime.Now,
                                            CreatedBy = UserID,
                                            IsDeleted = false,
                                            CustomerId = CustID
                                        };
                                        if (!FundMasterManagement.ExistsChecklistStatusName(objchklststatus2))
                                            FundMasterManagement.saveCheckListStatusDetail(objchklststatus2);
                                    }
                                    if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                                    {
                                        string statusName = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                                        var statusdetails = objStatus.Where(x => x.Name == statusName).FirstOrDefault();
                                        CompFund_tbl_Checklist_Status objchklststatus3 = new CompFund_tbl_Checklist_Status()
                                        {
                                            ChecklistID = ChecklistID,
                                            Status = statusdetails.ID.ToString(),
                                            Remark = xlWorksheet.Cells[i, 8].Text.Trim(),
                                            CreatedOn = DateTime.Now,
                                            CreatedBy = UserID,
                                            IsDeleted = false,
                                            CustomerId = CustID
                                        };
                                        if (!FundMasterManagement.ExistsChecklistStatusName(objchklststatus3))
                                            FundMasterManagement.saveCheckListStatusDetail(objchklststatus3);
                                    }
                                }
                                uploadedChecklistCount++;
                                saveSuccess = true;
                            }
                            #endregion
                        }
                        else
                        {
                            if (errorMessage.Count > 0)
                            {
                                ErrorMessages(errorMessage);
                            }
                        }

                        if (saveSuccess)
                        {
                            if (uploadedChecklistCount > 0)
                            {
                                BindGrid();
                                bindPageNumber();
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = uploadedChecklistCount + " Checklist Details Uploaded Successfully.";
                                vsUploadUtility.CssClass = "alert alert-success";
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document.";
                                vsUploadUtility.CssClass = "alert alert-danger";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
                vsUploadUtility.CssClass = "alert alert-danger";
            }
        }

        protected void grdChecklistDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit_Checklist"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int checklistID = Convert.ToInt32(commandArgs[0]);
                    ViewState["ChecklistInstanceID"] = checklistID;
                    BindCheckliststatus(checklistID);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup();", true);
                }
                if (e.CommandName.Equals("Delete_Checklist"))
                {
                    
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int checklistID = Convert.ToInt32(commandArgs[0]);
                    bool existcheck = FundMasterManagement.ExistsChecklistMapping(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(checklistID));
                    if (!existcheck)
                    {
                        FundMasterManagement.DeleteCheckListDetailByID(checklistID, Convert.ToInt32(AuthenticationHelper.UserID));
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Checklist Deleted Successfully.";
                        ValidationSummary4.CssClass = "alert alert-success";
                        BindGrid();
                        bindPageNumber();
                    }
                    else
                    {
                        //FundMasterManagement.DeleteCheckListDetailByID(checklistID, Convert.ToInt32(AuthenticationHelper.UserID));
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "The Checklist can not be deteleted, it has been assigned to user.";
                        ValidationSummary4.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindCheckliststatus(int ChecklistID)
        {
            try
            {
                //List<tbl_NoticeCasePayment> lstCasePayments = new List<tbl_NoticeCasePayment>();
                List<CompFund_SP_Checklist_Status_Result> lstStatus = new List<CompFund_SP_Checklist_Status_Result>();

                lstStatus = FundMasterManagement.GetStatusDetails(ChecklistID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                if (lstStatus != null && lstStatus.Count > 0)
                {
                    grdStatus.DataSource = lstStatus;
                    grdStatus.DataBind();
                }
                else
                {
                    CompFund_SP_Checklist_Status_Result obj = new CompFund_SP_Checklist_Status_Result(); //initialize empty class that may contain properties
                    lstStatus.Add(obj); //Add empty object to list

                    grdStatus.DataSource = lstStatus; /*Assign datasource to create one row with default values for the class you have*/
                    grdStatus.DataBind(); //Bind that empty source                    

                    //To Hide row
                    grdStatus.Rows[0].Visible = false;
                    grdStatus.Rows[0].Controls.Clear();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        protected void btnStatusremarkSave_Click(object sender, EventArgs e)
        {
            try
            {

                int checklistID = Convert.ToInt32(ViewState["ChecklistInstanceID"]);
                TextBox tbxRemark = (TextBox)grdStatus.FooterRow.FindControl("tbxRemark");
                DropDownList ddlStatus = (DropDownList)grdStatus.FooterRow.FindControl("ddlStatus");
                if (tbxRemark.Text != null && tbxRemark.Text != "" && ddlStatus.SelectedValue != null && ddlStatus.SelectedValue != "-1")
                {
                    if (checklistID > 0)
                    {
                        CompFund_tbl_Checklist_Status objchklststatus = new CompFund_tbl_Checklist_Status()
                        {
                            ChecklistID = checklistID,
                            Status = ddlStatus.SelectedValue.ToString(),
                            Remark = tbxRemark.Text.Trim(),
                            CreatedOn = DateTime.Now,
                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                            IsDeleted = false,
                            CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID)
                        };
                        if (!FundMasterManagement.ExistsChecklistStatusName(objchklststatus))
                        {
                            FundMasterManagement.saveCheckListStatusDetail(objchklststatus);
                            cvCasePayment.IsValid = false;
                            cvCasePayment.ErrorMessage = "Remark of the Status Saved Successfully.";
                            ValidationSummary4.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvCasePayment.IsValid = false;
                            cvCasePayment.ErrorMessage = "Remark for the status already exist.";
                            ValidationSummary4.CssClass = "alert alert-danger";
                        }
                    }
                    BindCheckliststatus(checklistID);
                    ViewState["ChecklistStatusID"] = "Add";
                }
                else {
                    if (tbxRemark.Text == null || tbxRemark.Text == "")
                    {
                        cvCasePayment.IsValid = false;
                        cvCasePayment.ErrorMessage = "Please Enter Remark.";
                        ValidationSummary4.CssClass = "alert alert-danger";
                    }
                    if (ddlStatus.SelectedValue == null || ddlStatus.SelectedValue != "-1")
                    {
                        cvCasePayment.IsValid = false;
                        cvCasePayment.ErrorMessage = "Please Select Status.";
                        ValidationSummary4.CssClass = "alert alert-danger";
                    }
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }

        protected void grdStatus_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit_status"))
                {
                    ViewState["ChecklistStatusID"] = "Edit";
                    int checklistID = Convert.ToInt32(ViewState["ChecklistInstanceID"]);
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int checkliststatusID = Convert.ToInt32(commandArgs[0]);
                    if (checkliststatusID > 0)
                    {
                        TextBox tbxRemark = (TextBox)grdStatus.FooterRow.FindControl("tbxRemark");
                        DropDownList ddlStatus = (DropDownList)grdStatus.FooterRow.FindControl("ddlStatus");
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            CompFund_tbl_Checklist_Status objStatus = (from row in entities.CompFund_tbl_Checklist_Status
                                                                       where row.ID == checkliststatusID
                                                                       && row.ChecklistID == checklistID
                                                                       select row).FirstOrDefault();
                            if (objStatus != null)
                            {                     
                                tbxRemark.Text = objStatus.Remark;
                                ddlStatus.SelectedValue = objStatus.Status;
                            }
                        }
                    }                    
                }
                if (e.CommandName.Equals("Delete_Status"))
                {
                    int checklistID = Convert.ToInt32(ViewState["ChecklistInstanceID"]);
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ChecklistStatusID = Convert.ToInt32(commandArgs[0]);
                    FundMasterManagement.DeleteCheckListStatusDetailByID(ChecklistStatusID, Convert.ToInt32(AuthenticationHelper.UserID));
                    cvCasePayment.IsValid = false;
                    cvCasePayment.ErrorMessage = "Remark of Status Deleted Successfully.";
                    ValidationSummary4.CssClass = "alert alert-success";
                    ViewState["ChecklistStatusID"] = "Add";
                    BindCheckliststatus(checklistID);
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }

        protected void grdStatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ChecklistInstanceID"] != null)
                {
                    grdStatus.PageIndex = e.NewPageIndex;
                    int chkid = Convert.ToInt32(ViewState["ChecklistInstanceID"]);                   
                    BindCheckliststatus(chkid);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    DropDownList ddlStatusType = (DropDownList)e.Row.FindControl("ddlStatus");

                    if (ddlStatusType != null)
                        BindStatusType(ddlStatusType);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }
        private void BindStatusType(DropDownList ddl)
        {
            try
            {
                var key = "StatusType-" + Convert.ToString(AuthenticationHelper.CustomerID);

                var StatusMasterList = (List<CompFund_tbl_ComplianceStatus>)HttpContext.Current.Cache[key];
                if (HttpContext.Current.Cache[key] == null || StatusMasterList.Count == 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        StatusMasterList = (from row in entities.CompFund_tbl_ComplianceStatus
                                            select row).ToList();

                        HttpContext.Current.Cache.Insert(key, StatusMasterList, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
                    }
                }

                ddl.DataValueField = "ID";
                ddl.DataTextField = "Name";

                ddl.DataSource = StatusMasterList;
                ddl.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);           
            }
        }

        protected void grdStatus_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        
        protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
        }

        protected void grdChecklistDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;
                int totalPages = 0;
                if (Session["TotalRows"] != null)
                {
                    TotalRows.Value = Session["TotalRows"].ToString();

                    totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                    // total page item to be displayed
                    int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                    // remaing no of pages
                    if (pageItemRemain > 0)// set total No of pages
                    {
                        totalPages = totalPages + 1;
                    }
                    else
                    {
                        totalPages = totalPages + 0;
                    }
                    // return totalPages;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdChecklistDetails.PageIndex = chkSelectedPage - 1;
            grdChecklistDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid();
            bindPageNumber();
            SetShowingRecords();
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdChecklistDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdChecklistDetails.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
    }
}