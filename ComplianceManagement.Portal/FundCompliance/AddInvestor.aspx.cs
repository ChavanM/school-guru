﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.ComplianceFund;

namespace com.VirtuosoITech.ComplianceManagement.Portal.FundCompliance
{
    public partial class AddInvestor : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;        
        protected void Page_Load(object sender, EventArgs e)
        {
            divsuccessmsgaCDoctype.Visible = false;
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["InvestorId"]))
                    {
                        long InvestorId = Convert.ToInt64(Request.QueryString["InvestorId"]);

                        CompFund_tbl_InvestorMaster _objInvestorRecord = FundMasterManagement.GetInvestorDetailsByID(InvestorId, Convert.ToInt32(CustomerID));

                        if (_objInvestorRecord != null)
                        {
                            btnSave.Text = "Update";
                            tbxInvestorName.Text = _objInvestorRecord.InvestorName;
                            ViewState["Mode"] = 1;
                            ViewState["InvestorID"] = InvestorId;
                        }
                    }
                    else
                    {
                        btnSave.Text = "Save";
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    divsuccessmsgaCDoctype.Visible = false;
                    cvDuplicateLocation.IsValid = false;
                    vsAddDocType.CssClass = "alert alert-danger";
                    cvDuplicateLocation.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxInvestorName.Text.Trim()))
                {
                    CompFund_tbl_InvestorMaster _objInvestor = new CompFund_tbl_InvestorMaster()
                    {
                        InvestorName = tbxInvestorName.Text,
                        CustomerID = (int)CustomerID,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (FundMasterManagement.ExistsInvestorName(_objInvestor))
                        {
                            divsuccessmsgaCDoctype.Visible = false;
                            cvDuplicateLocation.IsValid = false;
                            cvDuplicateLocation.ErrorMessage = "Investor already exists.";
                            vsAddDocType.CssClass = "alert alert-danger";
                        }
                        else
                        {
                            long newInvestorID = 0;

                            newInvestorID = FundMasterManagement.CreateInvestorName(_objInvestor);

                            if (newInvestorID > 0)
                            {
                                if (cvDuplicateLocation.IsValid)
                                {
                                    divsuccessmsgaCDoctype.Visible = true;
                                    successmsgaCDoctype.Text = "Investor Saved Successfully.";
                                    vsAddDocType.CssClass = "alert alert-success";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCDoctype.ClientID + "').style.display = 'none' },2000);", true);
                                }
                                tbxInvestorName.Text = string.Empty;
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        if (ViewState["InvestorID"] != null)
                        {
                            _objInvestor.ID = Convert.ToInt64(ViewState["InvestorID"]);
                            _objInvestor.UpdatedBy = AuthenticationHelper.UserID;
                            _objInvestor.UpdatedOn = DateTime.Now;

                            bool checkexist = FundMasterManagement.ExistsInvestorNameforupdate(_objInvestor.InvestorName, Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(_objInvestor.ID));
                            if (!checkexist)
                            {
                                bool saveSuccess = FundMasterManagement.UpdateInvestorDetails(_objInvestor);

                                if (saveSuccess)
                                {
                                    if (cvDuplicateLocation.IsValid)
                                    {
                                        divsuccessmsgaCDoctype.Visible = true;
                                        successmsgaCDoctype.Text = "Investor Updated Successfully.";
                                        vsAddDocType.CssClass = "alert alert-success";
                                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCDoctype.ClientID + "').style.display = 'none' },2000);", true);
                                    }
                                }
                                else
                                {
                                    cvDuplicateLocation.IsValid = false;
                                    divsuccessmsgaCDoctype.Visible = false;
                                    cvDuplicateLocation.ErrorMessage = "Something went wrong, Please try again";
                                    vsAddDocType.CssClass = "alert alert-danger";
                                }
                            }
                            else
                            {
                                divsuccessmsgaCDoctype.Visible = false;
                                cvDuplicateLocation.IsValid = false;
                                cvDuplicateLocation.ErrorMessage = "Investor already exists.";
                                vsAddDocType.CssClass = "alert alert-danger";
                            }
                        }
                    }
                }
                else
                {
                    divsuccessmsgaCDoctype.Visible = false;
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Please Enter Investor Name.";
                    vsAddDocType.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgaCDoctype.Visible = false;
                cvDuplicateLocation.IsValid = false;
                cvDuplicateLocation.ErrorMessage = "Server Error Occurred. Please try again.";
                vsAddDocType.CssClass = "alert alert-danger";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divsuccessmsgaCDoctype.Visible = false;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}