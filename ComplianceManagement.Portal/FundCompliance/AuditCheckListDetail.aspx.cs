﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Microsoft.IdentityModel.Protocols;

namespace com.VirtuosoITech.ComplianceManagement.Portal.FundCompliance
{
    public partial class AuditCheckListDetail : System.Web.UI.Page
    {
        protected static int customerid;
        protected static int UId;
        protected static int roleId;
        protected static string Path;
        protected static string Flag;
        protected static string Authorization;
        protected static int AuditChecklistScheduleID;
        protected static int AuditchecklistID;
        protected static int AuditStatusID;
        protected static string DType;
        protected static string AuditChecklistName;
        //DType string 
        //
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            roleId = Convert.ToInt32(AuthenticationHelper.UserID);
            if (!string.IsNullOrEmpty(Request.QueryString["DType"])
                    && !string.IsNullOrEmpty(Request.QueryString["AuditchecklistID"])
                       && !string.IsNullOrEmpty(Request.QueryString["AuditChecklistScheduleID"])
                       && !string.IsNullOrEmpty(Request.QueryString["AuditStatusID"]))
            {               
                DType = Request.QueryString["DType"].ToString();
                AuditchecklistID = Convert.ToInt32(Request.QueryString["AuditchecklistID"]);
                AuditChecklistScheduleID = Convert.ToInt32(Request.QueryString["AuditChecklistScheduleID"]);
                AuditStatusID = Convert.ToInt32(Request.QueryString["AuditStatusID"]);
                AuditChecklistName = string.Empty;
                GetAuditChecklistName(AuditchecklistID);
            }
        }
        private void GetAuditChecklistName(int AuditchecklistID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var AuditDetail = (from row in entities.CompFund_tbl_ChecklistAuditInstance
                                       join row1 in entities.CustomerBranches
                                       on row.CustomerBranchID equals row1.ID
                                       where row1.IsDeleted == false
                                       && row.ID == AuditchecklistID
                                       select new { row, row1 }).FirstOrDefault();
                    if (AuditDetail != null)
                    {
                        AuditChecklistName = AuditDetail.row1.Name + " > " + AuditDetail.row.Year + " > " + AuditDetail.row.Period;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}