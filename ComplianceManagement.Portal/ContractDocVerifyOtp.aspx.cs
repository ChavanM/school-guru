﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class ContractDocVerifyOtp : System.Web.UI.Page
    {
        static int WrongOTPCount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    divVerifyotp.Visible = false;
                    spangenarteotp.Visible = false;
                    Labelmsg.Text = "will be send";
                    string DecryptedEmail = string.Empty;

                    if (!string.IsNullOrEmpty(Request.QueryString["UID"])
                        && !string.IsNullOrEmpty(Request.QueryString["CustID"])
                        && !string.IsNullOrEmpty(Request.QueryString["ContractID"]))
                    {

                        int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                        int CustomerID = Convert.ToInt32(Request.QueryString["CustID"]);
                        int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractID"]);
                       
                        if (true)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var user = VendorDetails.GetVendorMasterDetails(UserID);
                                if (user != null)
                                {
                                    DecryptedEmail = user.Email;
                                    UserEmail.Text = user.Email;
                                    FirstNameUser.Text = user.VendorName.ToString();
                                    UserId.Text = UserID.ToString();
                                    Label1.Text = DecryptedEmail.Substring(0, 1).ToUpper();

                                    #region Email,Time,contact number display
                                    // For Email Id Logic Set.
                                    string[] spl = Convert.ToString(DecryptedEmail).Split('@');
                                    int lenthusername = (spl[0].Length / 2);
                                    int mod = spl[0].Length % 2;
                                    if (mod > 0)
                                    {
                                        lenthusername = lenthusername - mod;
                                    }
                                    string beforemanupilatedemail = "";
                                    if (lenthusername == 1)
                                    {
                                        beforemanupilatedemail = spl[0].Substring(lenthusername);
                                    }
                                    else
                                    {
                                        beforemanupilatedemail = spl[0].Substring(lenthusername + 1);
                                    }

                                    string appendstar = "";
                                    for (int i = 0; i < beforemanupilatedemail.Length; i++) /*lenthusername*/
                                    {
                                        appendstar += "*";
                                    }

                                    string manupilatedemail = spl[0].Replace(beforemanupilatedemail, appendstar);

                                    string[] spl1 = spl[1].Split('.');
                                    int lenthatname = (spl1[0].Length / 2);
                                    int modat = spl1[0].Length % 2;
                                    if (modat > 0)
                                    {
                                        lenthatname = lenthatname - modat;
                                    }

                                    string beforatemail = "";
                                    if (lenthatname == 1)
                                    {
                                        beforatemail = spl1[0].Substring(lenthatname);
                                    }
                                    else
                                    {
                                        beforatemail = spl1[0].Substring(lenthatname + 1);
                                    }
                                    string appendstar1 = "";
                                    for (int i = 0; i < beforatemail.Length; i++) /*lenthatname*/
                                    {
                                        appendstar1 += "*";
                                    }

                                    string manupilatedatemail = spl1[0].Replace(beforatemail, appendstar1);
                                    string emailid = manupilatedemail;

                                    DateTime NewTime = DateTime.Now.AddMinutes(30);
                                    Time.Text = NewTime.Hour + ":" + NewTime.Minute + NewTime.ToString(" tt");
                                    email.Text = Convert.ToString(DecryptedEmail).Replace(spl[0], manupilatedemail).Replace(spl1[0], manupilatedatemail);

                                    if (!string.IsNullOrEmpty(Convert.ToString(user.ContactNumber)))
                                    {
                                        // For Mobile Number Logic Set.
                                        if (Convert.ToString(user.ContactNumber).Length == 10 && (Convert.ToString(user.ContactNumber).StartsWith("9") || Convert.ToString(user.ContactNumber).StartsWith("8") || Convert.ToString(user.ContactNumber).StartsWith("7")))
                                        {
                                            string s = Convert.ToString(user.ContactNumber);
                                            int l = s.Length;
                                            s = s.Substring(l - 4);
                                            string r = new string('*', l - 4);
                                            //r = r + s;
                                            mobileno.Text = r + s;
                                        }
                                        else
                                        {
                                            mobileno.Text = "";
                                            //cvLogin.IsValid = false;
                                            //cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                                        }
                                    }
                                    else
                                    {
                                        mobileno.Text = "";
                                        //cvLogin.IsValid = false;
                                        //cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                                    }
                                    #endregion
                                }                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                Response.Redirect("Blank.aspx", false);

            }
        }
     
        protected void btnOTP_Click(object sender, EventArgs e)
        {

            try
            {
                int userID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["UID"]))
                {
                    userID = Convert.ToInt32(Request.QueryString["UID"]);
                }

                if (userID != null)
                {
                    if (VerifyOTPManagement.verifyOTP(Convert.ToInt32(txtOTP.Text), userID))
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            VerifyOTP OTPData = VerifyOTPManagement.GetByID(Convert.ToInt32(userID));
                            if (OTPData != null)
                            {
                                TimeSpan span = DateTime.Now.Subtract(OTPData.CreatedOn.Value);
                                if (0 <= span.Minutes && span.Minutes <= 30)
                                {
                                    VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(userID), Convert.ToInt32(txtOTP.Text));

                                    int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                                    int CustomerID = Convert.ToInt32(Request.QueryString["CustID"]);
                                    int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractID"]);

                                    Response.Redirect("ContractProduct/aspxPages/ContractDocByVendor.aspx?CID=" + CustomerID + "&UID=" + UserID, false);                                   
                                }
                                else
                                {
                                    cvLogin.ErrorMessage = "OTP Expired.";
                                    cvLogin.IsValid = false;
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (WrongOTPCount < 3)
                        {
                            cvLogin.ErrorMessage = "Invalid OTP, Please Enter Again.";
                            cvLogin.IsValid = false;
                            WrongOTPCount++;
                            return;
                        }
                        else
                        {
                            cvLogin.ErrorMessage = "Invalid OTP, Please Enter Again.";
                            cvLogin.IsValid = false;
                            return;
                        }
                    }
                }
                else
                {
                    cvLogin.ErrorMessage = "Please Enter Correct OTP.";
                    cvLogin.IsValid = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLogin.IsValid = false;
                cvLogin.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkResendOTP_Click(object sender, EventArgs e)
        {
            bool Success = false;
            int userID = -1;
            if (!string.IsNullOrEmpty(Request.QueryString["UID"]))
            {
                userID = Convert.ToInt32(Request.QueryString["UID"]);
            }
            if (userID != -1)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var user = VendorDetails.GetVendorMasterDetails(userID);
                    if (user != null)
                    {
                        string ipaddress = string.Empty;
                        try
                        {
                            ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                            if (ipaddress == "" || ipaddress == null)
                                ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }

                        Random random = new Random();
                        int value = random.Next(1000000);
                        long Contact;

                        VerifyOTP OTPData = new VerifyOTP()
                        {
                            UserId = Convert.ToInt32(user.ID),
                            EmailId = user.Email.ToString(),
                            OTP = Convert.ToInt32(value),
                            CreatedOn = DateTime.Now,
                            IsVerified = false,
                            IPAddress = ipaddress
                        };
                        bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                        if (OTPresult)
                        {
                            OTPData.MobileNo = Contact;
                        }
                        else
                        {
                            OTPData.MobileNo = 0;
                        }
                        VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                        try
                        {
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                            Success = true;
                        }
                        catch (Exception ex)
                        {
                            Success = false;
                        }
                        if (OTPresult && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                        {
                            try
                            {
                                //SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.");
                                Success = true;
                            }
                            catch (Exception ex)
                            {
                                Success = false;
                            }
                        }
                        if (Success)
                        {
                            cvLogin.ErrorMessage = "OTP Sent Successfully.";
                            cvLogin.IsValid = false;
                        }
                        else
                        {
                            //cvLogin.ErrorMessage = "OTP Sent Successfully.";
                            //cvLogin.IsValid = false;
                        }
                    }                  
                }
            }
        }

        protected void BtnGenerate_Click(object sender, EventArgs e)
        {
            divgenarteotp.Visible = false;
            divVerifyotp.Visible = true;
            spangenarteotp.Visible = true;
            Labelmsg.Text = "has been sent";

            bool Success = false;
            int userID = -1;
            if (!string.IsNullOrEmpty(Request.QueryString["UID"]))
            {
                userID = Convert.ToInt32(Request.QueryString["UID"]);
            }
            if (userID != -1)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var user = VendorDetails.GetVendorMasterDetails(userID);
                    if (user != null)
                    {
                        string ipaddress = string.Empty;
                        try
                        {
                            ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                            if (ipaddress == "" || ipaddress == null)
                                ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }

                        Random random = new Random();
                        int value = random.Next(1000000);
                        long Contact;

                        VerifyOTP OTPData = new VerifyOTP()
                        {
                            UserId = Convert.ToInt32(user.ID),
                            EmailId = user.Email.ToString(),
                            OTP = Convert.ToInt32(value),
                            CreatedOn = DateTime.Now,
                            IsVerified = false,
                            IPAddress = ipaddress
                        };
                        bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                        if (OTPresult)
                        {
                            OTPData.MobileNo = Contact;
                        }
                        else
                        {
                            OTPData.MobileNo = 0;
                        }
                        VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                        try
                        {
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                            Success = true;
                        }
                        catch (Exception ex)
                        {
                            Success = false;
                        }
                        if (OTPresult && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                        {
                            try
                            {
                                //SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.");
                                Success = true;
                            }
                            catch (Exception ex)
                            {
                                Success = false;
                            }
                        }
                        if (Success)
                        {
                            cvLogin.ErrorMessage = "OTP Sent Successfully.";
                            cvLogin.IsValid = false;
                        }
                    }
                }
            }
        }
    }
}