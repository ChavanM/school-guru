﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class MobileAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["device"]) && !string.IsNullOrEmpty(Request.QueryString["email"]))
                {
                    string deviceID = Request.QueryString["device"];
                    string email = Request.QueryString["email"];

                    Business.Data.User user = null;

                    if (UserManagement.IsValidUser(email.Trim(), out user))
                    {
                        if (user != null)
                        {
                            try
                            {
                                if (user.CustomerID != null)
                                {
                                    if (Convert.ToBoolean(user.SSOAccess))
                                    {
                                        var custRecord = SAMLManagement.CheckCustomerExists(Convert.ToInt32(user.CustomerID));

                                        if (custRecord != null)
                                        {
                                            if (!string.IsNullOrEmpty(custRecord.SAMLLoginPageURL) && custRecord.IdentityProvider == "Microsoft")
                                            {
                                                Response.Redirect(custRecord.SAMLLoginPageURL + "?ID=" + user.CustomerID, true);
                                            }
                                        }
                                        else
                                        {
                                            hdnStatus.Value = "error";
                                            hdnErrMessage.Value = "Customer for provided Email not configured to Login using SSO";
                                            LoggerMessage.InsertErrorMsg_DBLog(hdnErrMessage.Value, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "pushDetails();", true);
                                        }
                                    }
                                    else
                                    {
                                        hdnStatus.Value = "error";
                                        hdnErrMessage.Value = "You are not allowed to Login using SSO, Contact administrator for more details";
                                        LoggerMessage.InsertErrorMsg_DBLog(hdnErrMessage.Value, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "pushDetails();", true);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                hdnStatus.Value = "error";
                                hdnErrMessage.Value = "Something went wrong, please try again later";
                                LoggerMessage.InsertErrorMsg_DBLog(hdnErrMessage.Value, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "pushDetails();", true);
                            }
                        }
                        else
                        {
                            hdnStatus.Value = "error";
                            hdnErrMessage.Value = "No Registered User found with Email-" + email;
                            LoggerMessage.InsertErrorMsg_DBLog(hdnErrMessage.Value, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "pushDetails();", true);
                        }
                    }
                    else
                    {
                        hdnStatus.Value = "error";
                        hdnErrMessage.Value = "No Registered User found with Email-" + email;
                        LoggerMessage.InsertErrorMsg_DBLog(hdnErrMessage.Value, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "pushDetails();", true);
                    }
                }
            }
        }
    }
}