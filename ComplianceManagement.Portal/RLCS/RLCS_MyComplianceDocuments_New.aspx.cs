﻿using com.VirtuosoITech.ComplianceManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyComplianceDocuments_New : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int UId;
        protected static List<Int32> roles;
        protected static int RoleID;
        protected static int RoleFlag;
        protected static string Falg;
        protected static bool DisableFalg;
        protected static String SDate;
        protected static String LDate;
        protected static String IsMonthID;
        protected static string Path;
        protected static string avacomRLCSAPI_URL;
        protected static string ProfileID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                Path = ConfigurationManager.AppSettings["AVACOM_API_URL"];
                avacomRLCSAPI_URL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
                ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
                CId = Convert.ToString(AuthenticationHelper.CustomerID);
                UserId = Convert.ToString(AuthenticationHelper.UserID);
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
                RoleFlag = 0;
                IsMonthID = "All";

                if (AuthenticationHelper.Role == "MGMT")
                {
                    Falg = "MGMT";
                    RoleFlag = 1;
                    DisableFalg = false;
                }
                if (AuthenticationHelper.Role == "AUDT")
                {
                    IsMonthID = "AUD";
                    SDate = Convert.ToDateTime(Session["Auditstartdate"].ToString()).Date.ToString("dd-MM-yyyy");
                    LDate = Convert.ToDateTime(Session["Auditenddate"].ToString()).Date.ToString("dd-MM-yyyy");
                    Falg = "AUD";
                    DisableFalg = true;
                }
                else if (AuthenticationHelper.Role == "EXCT")
                {
                    DisableFalg = false;
                    Falg = "PRA";
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                    if (roles.Contains(4))
                    {
                        RoleFlag = 1;
                    }
                }

                int CustomerBranchID = !string.IsNullOrEmpty(Request.QueryString["Customer_BranchID"]) ? Convert.ToInt32(Request.QueryString["Customer_BranchID"].ToString()) : 0;
                string EstablishmentType = Convert.ToString(Request.QueryString["EstablishmentType"]);
                string State = Convert.ToString(Request.QueryString["State"]);
                string Month = Convert.ToString(Request.QueryString["Month"]);
                string Year = Convert.ToString(Request.QueryString["Year"]);

                string challanType = Request.QueryString["Type"];
                int FileTypeId = !string.IsNullOrEmpty(Request.QueryString["FileTypeId"]) ? Convert.ToInt32(Request.QueryString["FileTypeId"]) : 0;
                string DocumentType = Convert.ToString(Request.QueryString["DocumentType"]);


                if (DocumentType == "Register")
                {
                    if (CustomerBranchID > 0 && EstablishmentType != null && State != null && Month != null && Year != null)
                    {
                        bool _objRegData = getDocumentRegDtls(CustomerBranchID, EstablishmentType, State, Month, Year);

                        if (!_objRegData)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document to Download')", true);
                        }
                    }
                }
                else if (DocumentType == "Return")
                {
                    if (CustomerBranchID > 0 && EstablishmentType != null && State != null && Month != null && Year != null)
                    {
                        bool _objRegData = getDocumentReturnDtls(CustomerBranchID, EstablishmentType, State, Month, Year);

                        if (!_objRegData)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document to Download')", true);
                        }
                    }
                }
                else if (DocumentType == "Challan")
                {
                    if (challanType != null)
                    {
                        if (!string.IsNullOrEmpty(challanType))
                        {
                            bool _objChlDownload = GetChallanDocuments(State, Month, Year, challanType, FileTypeId, CustomerBranchID);
                            if (!_objChlDownload)
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document Available to Download')", true);
                            }
                        }
                    }
                }
            }
        }

        private bool getDocumentRegDtls(int customerBranchID, string establishmentType, string state, string month, string year)
        {
            try
            {
                List<long> lstComplianceIDs = RLCSManagement.GetAll_Registers_Compliance_StateWise(state, establishmentType);

                if (lstComplianceIDs.Count > 0 && lstComplianceIDs != null)
                {
                    List<long> lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, customerBranchID, year, month);

                    if (lstScheduleOnIDs.Count > 0)
                    {
                        bool _objDocument_RegDownloadval = RLCS_DocumentManagement.GetRLCSDocuments("Register", lstScheduleOnIDs);
                        if (!_objDocument_RegDownloadval)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private bool getDocumentReturnDtls(int customerBranchID, string establishmentType, string state, string month, string year)
        {
            try
            {
                List<long> lstComplianceIDs = RLCSManagement.GetAll_ReturnsRelatedCompliance_SectionWise(state);

                if (lstComplianceIDs.Count > 0 && lstComplianceIDs != null)
                {
                    List<long> lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, customerBranchID, year, month);

                    if (lstScheduleOnIDs.Count > 0)
                    {
                        bool _objDocument_RegDownloadval = RLCS_DocumentManagement.GetRLCSDocuments("Return", lstScheduleOnIDs);
                        if (!_objDocument_RegDownloadval)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private bool GetChallanDocuments(string state, string month, string year, string challanType, int FileTypeId, int CustomerBranchID)
        {
            try
            {
                if (challanType.Trim().ToUpper().Equals("EPF") || challanType.Trim().ToUpper().Equals("PF"))
                    challanType = "EPF";
                else if (challanType.Trim().ToUpper().Equals("ESI") || challanType.Trim().ToUpper().Equals("LWF") || challanType.Trim().ToUpper().Equals("PT"))
                    challanType = challanType.Trim().ToUpper();

                List<long> lstComplianceIDs = new List<long>();
                List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();

                if (challanType.Trim().ToUpper().Equals("EPF") || challanType.Trim().ToUpper().Equals("ESI"))
                {
                    lstComplianceIDs = RLCSManagement.GetAll_ChallanRelatedCompliance(challanType);

                    lstCustomerBranches = RLCSManagement.GetRLCSBranchDetailsByID(CustomerBranchID, "B");
                }
                else if (challanType.Trim().Trim().ToUpper().Equals("PT"))
                {
                    lstCustomerBranches = RLCSManagement.GetRLCSBranchDetailsByID(CustomerBranchID, "B");

                    lstComplianceIDs = RLCSManagement.GetAll_ChallanRelatedCompliance(state, challanType);
                }

                if (lstComplianceIDs != null && lstCustomerBranches != null)
                {
                    if (lstComplianceIDs.Count > 0 && lstCustomerBranches.Count > 0)
                    {
                        bool getSchdules = false;
                        foreach (var eachCustBranch in lstCustomerBranches)
                        {
                            if (!getSchdules)
                            {
                                List<long> lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, Convert.ToInt32(eachCustBranch.AVACOM_BranchID), year, month);

                                if (lstScheduleOnIDs.Count > 0)
                                {
                                    bool _objDocument_ChlDownloadval = RLCS_DocumentManagement.GetRLCSDocumentsByFileTypeId("Challan", lstScheduleOnIDs, FileTypeId);

                                    if (_objDocument_ChlDownloadval)
                                    {
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }

                        return true;
                    }

                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        [WebMethod]
        public static string GetActType(string ActID)
        {
            string ACTType = "";
            try
            {
                int Act_ID = Convert.ToInt32(ActID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstData = (from CIM in entities.ComplianceInputMappings
                                   join C in entities.Compliances on (long)CIM.ComplianceID equals C.ID
                                   where C.ActID == Act_ID && CIM.IsActive == true
                                   select CIM.AVACOM_ActGroup).Distinct().FirstOrDefault();
                    if (lstData != "")
                    {
                        ACTType = Convert.ToString(lstData);
                    }
                }
                return ACTType;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ACTType;

            }
        }
    }
}