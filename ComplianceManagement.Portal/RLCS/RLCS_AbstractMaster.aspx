﻿<%@ Page Title="Abstracts-Displays :: My Documents" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_AbstractMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_AbstractMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />
    <title></title>

    <style>
        .k-grid-content {
            min-height: 50px;
        }
    </style>

    <script type="text/jscript">
        $(document).ready(function () {
            setactivemenu('leftnoticesmenu');
            fhead('My Documents/ Abstracts-Displays');
            ApiTrack_Activity("Registration", "pageView", null);
        });

        $(document).on("click", "#grid tbody tr .ob-download", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewpup(item.AvacomRegionalDocPath);
            return true;
        });

        $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpup(item.AvacomRegionalDocPath);
            return true;
        });

          $(document).on("click", "#grid tbody tr .ob-downloadEng", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewpup(item.AvacomEnglishDocPath);
            return true;
        });

        $(document).on("click", "#grid tbody tr .ob-overviewEng", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpup(item.AvacomEnglishDocPath);
            return true;
        });

        function OpenDownloadOverviewpup(DownloadPath) {   
            alert(DownloadPath);
            $('#DownloadViews').attr('src', "RLCS_AbstractMaster.aspx?RLCSDownloadPath=" + DownloadPath);
        }
        function OpenDocumentOverviewpup(DownloadPath) {
            if (DownloadPath != undefined && DownloadPath != "") {
                $('#DocumentReviewPopUp1').modal('show');
                $('#ContentPlaceHolder1_CaseDocViewFrame').attr('src', "/docviewerPath.aspx?docurl=" + DownloadPath + "");
            }
            else {
                alert('No Document to View');
            }
        }
        function CloseClearDV() {
            $('#DownloadViews').attr('src', "../Common/blank.html");
        }
        function bindState() {
            $("#dropdownStateActivity").kendoDropDownList({
                dataTextField: "SM_Name",
                dataValueField: "SM_Code",
                optionLabel: "All",
                change: function (e) {
                    BindActTree();
                    setCommonAllfilterMain();                    
                },
                dataSource: {
                    transport: {
                        read: {
                            url: "<% =Path%>GetStateMaster",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    }
                }
            });
        }
        function BindActTree() {
            $("#dropdownACTtree").kendoDropDownList({
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select ACT",
                dataTextField: "AM_ActName",
                dataValueField: "AM_ActID",
                optionLabel: "All",
                change: function (e) {
                    setCommonAllfilterMain();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: "<% =Path%>GetACTMaster?StateId=" + $("#dropdownStateActivity").val(),
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    }
                }
            });
        }

        function bindDetail()
        {
            var record = 0;
            var customerID =<% =CustId%>;
            $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: "<% =Path%>GetAbstractDetail?customerID=-1&StateId=&ActId=",
                            type: 'GET',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },

                            dataType: 'json'
                        }
                    },
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        total: function (response) {
                            return response.Result.length;
                        }
                    }
                },
                //height: 380,
                sortable: true,
                filterable: true,
                columnMenu: true,
                //pageable: {
                //    refresh: true
                //},
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,

                pageable: {
                    refresh: true,
                    //pageSizes: true,
                    buttonCount: 3
                },

                columns: [
                    {
                        title: "Sr.No",
                        field: "rowNumber",
                        template: "<span class='row-number'></span>",
                        width: 70,
                        attributes: {
                            style: 'white-space: nowrap;'

                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "SM_Name",
                        title: "State",
                        width: 100,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "AM_ActName",
                        title: "Act",
                        width: 300,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "AvacomRegionalDocPath",
                        title: "Regional Document",
                        width: 75,
                        attributes: {
                            style: 'white-space: nowrap;'

                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "AvacomEnglishDocPath",
                        title: "English Document",
                        width: 75,
                        attributes: {
                            style: 'white-space: nowrap;'

                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            {
                                name: "editLable1", text: "No Document",
                                visible: function (dataItem) {
                                    if (dataItem.AvacomRegionalDocPath != "" && dataItem.AvacomRegionalDocPath != undefined) {
                                        return false;
                                    }
                                    else {
                                        return true;
                                    }
                                }
                            },
                            {
                                name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download",
                                visible: function (dataItem) {
                                    if (dataItem.AvacomRegionalDocPath != "" && dataItem.AvacomRegionalDocPath != undefined) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                },
                                width: 70
                            },
                            {
                                name: "edit2", text: "", iconClass: "k-icon k-i-eye",
                                visible: function (dataItem) {
                                    if (dataItem.AvacomRegionalDocPath != "" && dataItem.AvacomRegionalDocPath != undefined) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }, className: "ob-overview"
                            }

                        ], title: "Regional Document", lock: true, width: 150,
                    },
                    {
                        command: [
                            {
                                name: "editLable1", text: "No Document",
                                visible: function (dataItem) {
                                    if (dataItem.AvacomEnglishDocPath != "" && dataItem.AvacomEnglishDocPath != undefined) {
                                        return false;
                                    }
                                    else {
                                        return true;
                                    }
                                }
                            },
                            {
                                name: "edit1", text: "", iconClass: "k-icon k-i-download",
                                visible: function (dataItem) {
                                    if (dataItem.AvacomEnglishDocPath != "" && dataItem.AvacomEnglishDocPath != undefined) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }, className: "ob-downloadEng", width: 70
                            },
                            {
                                name: "edit2", text: "", iconClass: "k-icon k-i-eye",
                                visible: function (dataItem) {
                                    if (dataItem.AvacomEnglishDocPath != "" && dataItem.AvacomEnglishDocPath != undefined) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }, className: "ob-overviewEng"
                            }

                        ], title: "English Document", lock: true, width: 150,

                    },

                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "View";
                }
            });
        }
        

        $(document).ready(function () {
            bindState();
            BindActTree();
            bindDetail();                   
        });

        function setCommonAllfilterMain() {

            if ($("#dropdownACTtree").val() != '' && $("#dropdownACTtree").val() != undefined
                && $("#dropdownStateActivity").val() != '' && $("#dropdownStateActivity").val() != undefined) {

                //location details             
                var locationsdetails = [];
                locationsdetails.push({
                    field: "ActID", operator: "eq", value: $("#dropdownACTtree").val()
                });

                //Activities Details
                var Activitiedetails = [];
                Activitiedetails.push({
                    field: "stateID", operator: "eq", value: $("#dropdownStateActivity").val()
                });


                var dataSource = $("#grid").data("kendoGrid").dataSource;

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Activitiedetails
                        },
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });

            }


            else if ($("#dropdownStateActivity").val() != '' && $("#dropdownStateActivity").val() != undefined) {

                //Activities Details
                var Activitiedetails = [];
                Activitiedetails.push({
                    field: "stateID", operator: "eq", value: $("#dropdownStateActivity").val()
                });

                var dataSource = $("#grid").data("kendoGrid").dataSource;

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Activitiedetails
                        }
                    ]
                });
            } else if ($("#dropdownACTtree").val() != '' && $("#dropdownACTtree").val() != undefined) {

                var locationsdetails = [];
                locationsdetails.push({
                    field: "ActID", operator: "eq", value: $("#dropdownACTtree").val()
                });

                var dataSource = $("#grid").data("kendoGrid").dataSource;

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="col-md-4 colpadding0">
                <label for="dropdownStateActivity" style="color: #515967;">State</label>
                <input id="dropdownStateActivity" style="width: 98%" />
            </div>
            <div class="col-md-4 colpadding0">
                <label for="dropdownStateActivity" style="color: #515967;">Act</label>
                <input id="dropdownACTtree" style="width: 98%;" />
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-2 colpadding0">
                <button id="ClearfilterMain" style="float: right; margin-right: 1%; display: none" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
            </div>
        </div>
    </div>

    <div id="grid" style="border: none; margin-top: 5px">
        <div class="k-header k-grid-toolbar">
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filtersstoryboard">&nbsp;</div>
        </div>
    </div>
    <%--DownLoad ModalPOPup--%>
    <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 360px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="DownloadViews" src="about:blank" width="300px" height="150px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div class="col-md-12">
                                <fieldset style="height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="OverViews" width="100%" height="100%" frameborder="0"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--Document Viewer--%>
    <div>
        <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                            <fieldset style="border-style: solid; border-width: 0px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="CaseDocViewFrame" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

