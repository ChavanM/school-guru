﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class InputOutputPopup : System.Web.UI.Page
    {
        protected static int customerid;
        protected static string ComplianceTypeFlag;
        protected static bool IsApprover = false;
        protected static string avacomAPIUrl;
        public static List<long> Branchlist = new List<long>();
        public string ComplianceID_;
        public string ComplianceType_;
        public string ReturnRegisterChallanID_;
        public string MonthID_;
        public int LoginUserID;
        public int Year_;
        public string InputID_;
        public string ddlFileType_;
        public string BranchID_;
        public int JSONID;
        public static List<int> locationList = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);

                         avacomAPIUrl = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();

                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                                BindLocationFilter(assignedbranchIDs);
                                // upDocGenPopup_Load(sender, e);
                            }
                        }
                    }

                    if (Request.QueryString["ComplianceType"] != null && Request.QueryString["JSONID"] != null)  
                    {
                        //ComplianceID_ = Convert.ToString(Request.QueryString["ComplianceID"]);
                        ComplianceType_ = Convert.ToString(Request.QueryString["ComplianceType"]);
                        //ReturnRegisterChallanID_ = Convert.ToString(Request.QueryString["ReturnRegisterChallanID"]);
                        MonthID_ = Convert.ToString(Request.QueryString["Month"]);
                        Year_ = Convert.ToInt32(Request.QueryString["Year"]);
                        InputID_ = Convert.ToString(Request.QueryString["InputID"]);
                        ddlFileType_ = Convert.ToString(Request.QueryString["ddlFileType"]);
                        //BranchID_ = Convert.ToString(Request.QueryString["BranchID"]);
                        JSONID = Convert.ToInt32(Request.QueryString["JSONID"]);

                        //hdnComplianceID.Value = Convert.ToString(ComplianceID_);
                        hdnComplianceType.Value = Convert.ToString(ComplianceType_);
                        //hdnReturnRegisterChallanID.Value = Convert.ToString(ReturnRegisterChallanID_);
                        hdnMonth.Value = Convert.ToString(MonthID_);
                        hdnYear.Value = Convert.ToString(Year_);
                        hdnInputId.Value = Convert.ToString(InputID_);
                        hdnFileType.Value = Convert.ToString(ddlFileType_);
                        //hdnBranchId.Value = Convert.ToString(BranchID_);
                        hdnIOJSON.Value = Convert.ToString(JSONID);
                        ddlFileType_SelectedIndexChanged(null, null);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "bindTreeListGrid1('" + customerid + "','" + ComplianceType_ + "','" + MonthID_ + "','" + Year_ + "','" + LoginUserID + "','" + JSONID + "');", true);
                        // Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "bindTreeListGrid1('" + customerid + "',0,'', 'ALL', '', '0', '0', '', '01,02,03,07', 2019, '');", true);
                    }
                    //('802', '16754', '', 'ALL', '', '0', '0', '', '01', 2019, '')", true);


                    ScriptManager.RegisterStartupScript(this, this.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                }
            }
            catch (Exception)
            {
            }

        }
        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var branches = RLCS_ClientsManagement.GetAllHierarchy(customerID);
                    tvFilterLocation.Nodes.Clear();
                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);
                    foreach (var item in branches)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }
                    tvFilterLocation.CollapseAll();
                    // tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        //{
        //    tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        //}
        protected void upDocGenPopup_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUIDeptDDL('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnFileSelectnOther_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hdnInputId.Value))
            {
                string Frequency = "";
                int InputId = Convert.ToInt32(hdnInputId.Value);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Frequency_ID = (from PP in entities.page_parameter
                                        where PP.id == InputId
                                        select PP.Frequency).Distinct().FirstOrDefault();
                    if (Frequency_ID != null)
                    {
                        List<Tuple<int, string>> FreqTList = new List<Tuple<int, string>>();
                        FreqTList.Add(new Tuple<int, string>(1, "Quarterly"));
                        FreqTList.Add(new Tuple<int, string>(2, "Half Yearly"));
                        FreqTList.Add(new Tuple<int, string>(4, "Four Monthly"));
                        FreqTList.Add(new Tuple<int, string>(5, "BiAnnual"));
                        var lst = FreqTList.FindAll(m => m.Item1 == Frequency_ID);
                        if (lst.Count > 0)
                        {


                            BindPeriod_Return(Convert.ToString(lst[0].Item2));


                        }

                    }

                }
            }

        }
        private void BindPeriod_Return(string selectedFreq)
        {
            try
            {
                if (!string.IsNullOrEmpty(selectedFreq))
                {
                    List<Tuple<string, string, string>> tupleList = new List<Tuple<string, string, string>>();

                    tupleList.Add(new Tuple<string, string, string>("Annual", "Annual", "Annual"));
                    tupleList.Add(new Tuple<string, string, string>("BiAnnual", "BiAnnual", "BiAnnual"));

                    tupleList.Add(new Tuple<string, string, string>("Half Yearly", "First Half", "HY1"));
                    tupleList.Add(new Tuple<string, string, string>("Half Yearly", "Second Half", "HY2"));

                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q1", "Q1"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q2", "Q2"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q3", "Q3"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q4", "Q4"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "January", "01"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "February", "02"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "March", "03"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "April", "04"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "May", "05"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "June", "06"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "July", "07"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "August", "08"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "September", "09"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "Octomber", "10"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "November", "11"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "December", "12"));

                    DataTable dtReturnPeriods = new DataTable();
                    dtReturnPeriods.Columns.Add("ID");
                    dtReturnPeriods.Columns.Add("Name");

                    var lst = tupleList.FindAll(m => m.Item1 == selectedFreq);
                    if (lst.Count > 0)
                    {
                        foreach (var item in lst)
                        {
                            DataRow drReturnPeriods = dtReturnPeriods.NewRow();
                            drReturnPeriods["ID"] = item.Item3;
                            drReturnPeriods["Name"] = item.Item2;
                            dtReturnPeriods.Rows.Add(drReturnPeriods);
                        }

                        rptPeriodList.DataSource = dtReturnPeriods;
                        rptPeriodList.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnFileInput_Click(object sender, EventArgs e)
        {

            try
            {
                List<int> lstInputId = new List<int>();
                if (ddlFileType.SelectedValue == "Other")
                {
                    int InputId = Convert.ToInt32(hdnInputId.Value);
                    lstInputId.Add(InputId);
                }
                else
                {
                    foreach (RepeaterItem Item in rptFileInput.Items)
                    {
                        CheckBox chkFileInput = (CheckBox)Item.FindControl("chkFileInput");

                        if (chkFileInput != null && chkFileInput.Checked)
                        {
                            Label lblFileInputID = (Label)Item.FindControl("lblFileInputID");
                            Label lblFileInputName = (Label)Item.FindControl("lblFileInputName");

                            int TestID = (Convert.ToInt32(Item.ID));
                            lstInputId.Add(Convert.ToInt32(lblFileInputID.Text));

                            txtFileInputList.Text += lblFileInputName.Text + ",";
                        }

                    }
                }
                txtFileInputList.Text.TrimEnd(',');
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var DocumentType = (from CIM in entities.ComplianceInputMappings
                                        where lstInputId.Contains((int)CIM.InputID)
                                        select new
                                        {
                                            Id = CIM.DocumentType,
                                            Name = CIM.DocumentType
                                        }).Distinct().ToList();

                    ddlComplianceType.DataValueField = "Id";
                    ddlComplianceType.DataTextField = "Name";

                    ddlComplianceType.DataSource = DocumentType;
                    ddlComplianceType.DataBind();
                    ddlComplianceType.Items.Insert(0, "All");
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void ddlFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Clear();
            DataTable dtFileInput = new DataTable();
            dtFileInput.Columns.Add("ID");
            dtFileInput.Columns.Add("Name");
            if (hdnFileType.Value != "")
            {
                ddlFileType.SelectedValue = hdnFileType.Value;
                ddlFileType.Enabled = false;
                ddlYear.SelectedValue = hdnYear.Value;
                ddlYear.Enabled = false;

                txtPeriodList.Enabled = false;
            }
            int ddlvalue = -1;
            List<int> lstFrequency = new List<int>();
            if (ddlFileType.SelectedValue != "")
            {
                if (ddlFileType.SelectedValue == "Monthly")   //File type Monthly
                {
                    ddlvalue = 0;
                    lstFrequency.Add(ddlvalue);
                }
                else if (ddlFileType.SelectedValue == "Annual") //File type Yearly
                {
                    ddlvalue = 3;
                    lstFrequency.Add(ddlvalue);
                }
                else if (ddlFileType.SelectedValue == "Other")
                {
                    foreach (RepeaterItem item1 in rptFileInput.Items)
                    {
                        CheckBox chkFileInput = (System.Web.UI.WebControls.CheckBox)item1.FindControl("chkFileInput");
                    }
                    ////   1=Quaterly//2=Half//4 =4Monthly//5 = BiAnnual   /////
                    //ddlvalue = 1;
                    lstFrequency.Add(ddlvalue);
                    int[] FreqArr = new int[4] { 1, 2, 4, 5 };
                    for (int i = 0; i < FreqArr.Length; i++)
                    {
                        lstFrequency.Add(FreqArr[i]);
                    }

                }
                #region MyRegion InputOutput Page File
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var listFileInputOutput = (from PP in entities.page_parameter
                                               join CIM in entities.ComplianceInputMappings on PP.id equals CIM.InputID
                                               //where PP.Frequency == ddlvalue
                                               where lstFrequency.Contains((int)PP.Frequency)
                                               && PP.page_name != ""
                                               select new
                                               {
                                                   Id = PP.id,
                                                   Name = PP.page_name
                                               }).Distinct().ToList();

                    if (listFileInputOutput.Count > 0)
                    {
                        foreach (var item in listFileInputOutput)
                        {
                            DataRow drFileInput = dtFileInput.NewRow();
                            drFileInput["ID"] = item.Id;
                            drFileInput["Name"] = item.Name;
                            dtFileInput.Rows.Add(drFileInput);
                        }

                        rptFileInput.DataSource = dtFileInput;
                        rptFileInput.DataBind();

                        List<int> lstInputId = new List<int>();
                        if (hdnInputId.Value != "")
                        {
                            lstInputId = (hdnInputId.Value).Split(',').Select(int.Parse).ToList();
                        }
                        if (ddlFileType.SelectedValue == "Other")
                        {

                            foreach (RepeaterItem item1 in rptFileInput.Items)
                            {
                                CheckBox chkFileInput = (CheckBox)item1.FindControl("chkFileInput");

                                chkFileInput.Visible = false;
                            }
                        }
                        else
                        {
                            foreach (RepeaterItem item1 in rptFileInput.Items)
                            {
                                CheckBox chkFileInput = (CheckBox)item1.FindControl("chkFileInput");
                                Label lblFileInputID = (Label)item1.FindControl("lblFileInputID");

                                if (lstInputId.Contains(Convert.ToInt32(lblFileInputID.Text)))
                                {
                                    chkFileInput.Checked = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        rptFileInput.DataSource = dtFileInput;
                        rptFileInput.DataBind();
                    }
                }
                #endregion

                BindPeriod_Return(ddlFileType.SelectedValue);
                btnFileInput_Click(sender, e);
                if (hdnComplianceType.Value != "")
                {
                    ddlComplianceType.SelectedItem.Text = hdnComplianceType.Value;
                    ddlComplianceType.Enabled = false;

                }

            }


            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ddlFT", "ddlFT();", true);
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                hdnCustID.Value = Convert.ToString(AuthenticationHelper.CustomerID);

                string BranchID = "";
                hdnBranchId.Value = "";
                locationList.Clear();

                //for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                //{
                //    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                //}
                //if (locationList.Count > 0)
                //{
                //    foreach (var item in locationList)
                //    {
                //        BranchID += Convert.ToString(item) + ",";
                //    }
                //}
                //if (!string.IsNullOrEmpty(BranchID))
                //{
                //    BranchID = BranchID.Remove(BranchID.Length - 1);
                //    hdnBranchId.Value = Convert.ToString(BranchID);
                //}


                string InputId = "";

                if (ddlFileType.SelectedValue == "Other")
                {
                    InputId = string.IsNullOrEmpty(Convert.ToString(hdnInputId.Value)) ? "" : Convert.ToString(hdnInputId.Value);
                }
                else
                {
                    foreach (RepeaterItem Item in rptFileInput.Items)
                    {
                        CheckBox chkFileInput = (CheckBox)Item.FindControl("chkFileInput");

                        if (chkFileInput != null && chkFileInput.Checked)
                        {
                            Label lblFileInputID = (Label)Item.FindControl("lblFileInputID");

                            InputId += (lblFileInputID.Text) + ",";

                        }
                    }
                    if (InputId.Length > 0)
                    {
                        InputId = InputId.Remove(InputId.Length - 1);
                    }
                }
                if (InputId != "")
                {
                    hdnInputId.Value = Convert.ToString(InputId);
                }
                else
                {
                    InputId = hdnInputId.Value;

                }
                string MonthId = "";
                if (hdnMonth.Value != "")
                {
                    MonthId = hdnMonth.Value;
                }
                string ComplianceType = "";
                ComplianceType = string.IsNullOrEmpty(Convert.ToString(hdnComplianceType.Value)) ? "All" : Convert.ToString(hdnComplianceType.Value);

                //string ActGroup = "";
                //hdnActGroup.Value = ActGroup;
                //string ComplianceID = "";
                //ComplianceID = Convert.ToString(hdnComplianceID.Value);
                //string ReturnRegisterChallanID = "";
                //ReturnRegisterChallanID = Convert.ToString(hdnReturnRegisterChallanID.Value);
                //string DocumentType = "";
                //hdnDocumentType.Value = string.IsNullOrEmpty(Convert.ToString(DocumentType)) ? "" : Convert.ToString(DocumentType);
                //string Period = "";
                //hdnPeriod.Value = string.IsNullOrEmpty(Convert.ToString(Period)) ? "" : Convert.ToString(Period);
                int Year = 0;
                if (hdnYear.Value != "0")
                {
                    Year = Convert.ToInt32(hdnYear.Value);
                }
                hdnSubmitbtn.Value = "Submit";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "bindTreeListGrid1('" + customerid + "','" + ComplianceType_ + "','" + MonthID_ + "','" + Year_ + "','" + LoginUserID + "','" + JSONID + "');", true);

                //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "bindTreeListGrid1('" + CustomerID + "','" + BranchID + "','" + InputId + "','" + ComplianceType + "','" + ActGroup + "','" + ComplianceID + "','" + ReturnRegisterChallanID + "','" + DocumentType + "','" + MonthId + "','" + Year + "','" + Period + "','"+LoginUserID+"');", true);
            }
            catch (Exception ex)
            {
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }
        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                if (node.Checked)
                {
                    if (!locationList.Contains(Convert.ToInt32(node.Value)))
                    {
                        locationList.Add(Convert.ToInt32(node.Value));
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                            {
                                locationList.Add(Convert.ToInt32(tn.Value));
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }
        protected void btnlocation1_Click(object sender, EventArgs e)
        {

        }
    }
}