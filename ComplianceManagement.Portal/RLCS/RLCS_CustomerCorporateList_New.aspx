﻿<%@ Page Title="Customer :: Setup/Onboarding" Language="C#" AutoEventWireup="true" MasterPageFile="~/HRPlusSPOCMGR.Master" Culture="en-GB"
    CodeBehind="RLCS_CustomerCorporateList_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_CustomerCorporateList_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>

    <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <%--<link href="../NewCSS/kendo.bootstrap.min.css" rel="stylesheet" />--%>

    <%--  <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>--%>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss_V1.css" rel="stylesheet" />

    
       <style type="text/css">

           .k-textbox>input{
         text-align:left;
     }
            .k-tooltip{
            margin-top:5px;
        }
             .k-dropdown-wrap-hover,.k-state-default-hover, .k-state-hover, .k-state-hover:hover {
    color: #2e2e2e;
    background-color: #d8d6d6 !important;
}
           .k-i-plus-outline{
               margin-top:-4px;
           }
           .k-grid, .k-listview {
               margin-top: 10px;
           }
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }
        
              .k-tooltip-content{
         width: max-content;
        
         }
        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0px -3px ;
            line-height: normal;
        }
        .k-calendar-container {
    background-color: white;
    width: 217px;
}
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top:-7px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: auto !important;
        }
        
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:2px;
            margin-bottom:0px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
             min-width: 19px;
              min-height: 25px;
              border-radius: 25px;
              background-color: transparent;
              border-color: transparent;
              border-color: transparent;
              margin-left:0px;
              padding-right:0px;
              padding-left:0px;
        }
         .k-button.k-state-active, .k-button:active{
            color:black;
        }
        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
            padding-right:18px;
        }

        div.k-window-content{
            overflow:hidden;
        }
        .k-grid-pager {
           
             margin-top: 0px;
            border-width:0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height:34px;
        }
        .k-multiselect-wrap, .k-floatwrap{
            height:34px;
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

           .btn {
         height:36px;
         font-weight:400;
         font-size:14px;
         }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
           .k-filter-row th, .k-grid-header th[data-index='7'] {
            text-align:center;
            color:#535b6a;
        }  
           .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
             .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
               .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }
         .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }
           .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
           .k-dropdown-wrap .k-input, .k-numeric-wrap .k-input, .k-picker-wrap .k-input {
    padding-top: 5px;
    border-radius: 3px 0 0 3px;
}
             label.k-label:hover{
              color:#1fd9e1;
          }
    </style>
    <%-- <style type="text/css">
          .k-filter-row th, .k-grid-header th[data-index='8'] {
            text-align:center;
        }  
           .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
             .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
               .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }
         .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }
           .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
      .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top:-1px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: auto !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
            margin-top:-4px;
            padding-top:0px;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:2px;
            margin-bottom:0px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
              min-width: 16px;
              min-height: 25px;
              border-radius: 25px;
              background-color: transparent;
              border-color: transparent;
              border-color: transparent;
              margin-left:0px;
              padding-right:0px;
       }   
        

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            
            padding-top:0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

      .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
            padding-right:25px;
        }

        .k-grid-pager {
            margin-top: 0px;
            border-width:0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 1px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }
         .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            width:max-content;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: -5px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            background-color: white;
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-grid-footer-wrap, .k-grid-header-wrap {
   border-width: 0 0px 0 0;
}
        .k-grid, .k-listview{
            margin-top:8px;
        }
         .k-autocomplete, .k-dropdown-wrap, .k-multiselect-wrap, .k-numeric-wrap, .k-picker-wrap, .k-textbox {
    border-width: 1px;
    border-style: solid;
    height:33px;
}
         .k-state-default>.k-select {
    margin-top: 0px;
    border-color: #c5c5c5;
}
         .k-dropdown-wrap .k-input, .k-numeric-wrap .k-input, .k-picker-wrap .k-input{
             padding-top:5px;
             padding-bottom:5px;
         }
             .k-grid-header .k-header>.k-link, .k-header, .k-treemap-title {
    color: rgba(0,0,0,0.5);
}
              .k-tooltip-content{
         width: max-content;
        
         }
     .k-multiselect-wrap .k-input{
                  padding-top:5px;
              }
        .k-state-focused:hover {
            text-decoration: none;
            background-color: transparent;
            border-color: transparent;
        }
    </style>--%>
    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
            <div> #:value ? value : "N/A" #</div>
        </div>
    </script>
      var record=0;
    <script type="text/javascript">


        function ClearAllFilterMain(e) {

            e.preventDefault();

            $("#ddlServiceProvider").data("kendoDropDownList").value("");
            $('#ClearfilterMain').css('display', 'none');

            $("#grid").data("kendoGrid").dataSource.data([]);
            $("#txtSearch").val('');
        
        };

        function Bind_ServiceProvider() {
            $("#ddlServiceProvider").kendoDropDownList({
                placeholder: "Select",
                autoWidth: true,                
                dataTextField: "Name",
                dataValueField: "ID",
                index: 0,
                change: function (e) {
                    BindGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetSPDist?IsSPDist=' +<%=IsSPDist%> +'&SPID='+<%=spID%>+'&distID='+<%=distID%>+'&custID=' + <%=custID%>,
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {                           
                            return response.Result;
                        }
                    }
                }
            });
        }

        $(document).ready(function () {
            fhead('Masters/Customer');
            Bind_ServiceProvider();
        });

        function checkCustomerActive(value) {
            if (value == 1) {
                return "Active";
            } else if (value == 0) {
                return "InActive";
            }
        }

        function BindGrid() {                       
            var gridview = $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetHRCustomers?userID=<%=userID%>&custID=<%=custID%>&SPID=<%=spID%>&distID=<%=distID%>&roleCode=<%=RoleCode%>&prodType=2',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {                            
                            if (response != null || response != undefined)
                                return response.Result;
                        },
                        total: function (response) {                            
                            if (response != null || response != undefined)
                                return response.Result.length;
                        }
                    }
                },
                // editable: "inline",
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh:false,
                    pageSizes:true,
                    pageSize:10,
                    buttonCount: 3,
                    change: function (e) {
                    }
                },
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page()-1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ID", title: "ID",menu:false },
                    //{
                    //    title: "Sr.No.",
                    //    field: "rowNumber",
                    //    template: "#= ++record #",
                    //    //template: "<span class='row-number'></span>",
                    //    width: "9.6%;",
                    //    attributes: {
                    //        //border-width: 0px 1px 1px 1px;
                    //        style: 'white-space: nowrap;text-align:left;'
                    //    },
                    //    filterable:false,


                    //},
                    {
                        title: "Type",
                        field: "DistributorOrCustomer",
                        width: "11.9%;",
                        
                        attributes: {
                                //border-width: 0px 1px 1px 0px
                            style: 'white-space: nowrap;text-align:left;'
                        },filterable: { multi: true, search: true },

                    },
                    {
                        field: "CorporateID", title: 'Corporate ID',
                        width: "16%;",
                        attributes: {
                            //border-width: 0px 1px 1px 0px;
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            multi: true, 
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Name", title: 'Name',
                        width: "13.9%",
                        attributes: {
                            //border-width: 0px 1px 1px 0px;
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    //{
                    //    field: "BuyerName", title: 'Buyer Name',
                    //    width: "15%",
                    //    filterable: {
                    //        extra: false,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                    {
                        field: "BuyerContactNumber", title: 'Buyer Contact',
                        width: "15.9%",
                        filterable: {
                            extra: false,
                            multi: true, 
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        field: "BuyerEmail", title: 'Buyer Email',
                        width: "16.9%",
                        attributes: {
                        //border-width: 0px 1px 1px 0px;'
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false, 
                            multi: true, 
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Status", title: 'Status',
                        width: "12.9%",
                        attributes: {
                            //border-width: 0px 1px 1px 0px;
                            style: 'white-space: nowrap;'
                        },
                        //template: '#:checkCustomerActive(Status)#',
                        filterable: {
                            extra: false,
                            multi: true, 
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },                   
                    {
                        command:
                            [
                          { name: "edit", text: "", iconClass: ".k-icon k-i-pencil", className: "k-grid-edit", visible:"showHideEditDeleteButton" },
                          { name: "delete", text: "", iconClass: ".k-icon k-i-delete", className: "k-grid-delete", visible:"showHideEditDeleteButton" }, 
                          { name: "checkApplicability", text: "", iconClass: "k-icon k-i-zoom", className: "k-grid-zoom", }, 
                                //visible:"showHideApplicabilityButton"
                          { name: "proceed", text: "", iconClass: ".k-icon .k-i-arrow-right", className: "k-grid-arrow-right" },
                            ],
                        title: "Action",
                        
                        lock: true, width: "12.5%;",
                        attributes: {
                              //border-width: 0px 1px 1px 0px
                            style: 'white-space: nowrap;'
                        },
                    }
                ],               
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });

                    $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                    $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                    $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                }
            });
            }

            function OpenCheckApplicabilityPopup(ID) {
                $('#divCheckApplicabilityDialog').show();

                var myWindowAdv = $("#divCheckApplicabilityDialog");
            
                myWindowAdv.kendoWindow({
                    width: "95%",
                    height: '95%',
                    title: "Applicability Assessment",
                    visible: false,
                    actions: ["Close"],
                    close: onClose
                });

                $('#iframeCheck').attr('src', 'about:blank');
                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeCheck').attr('src', "/RLCS/HRPlus_ApplicabilityDetails.aspx?ID=" + ID+"&Source=Master");
                return false;
            }

            function showHideApplicabilityButton(data) {                
                var showHide = true;               
                if (data.DistributorOrCustomer != null && data.DistributorOrCustomer != undefined) {
                    if(data.DistributorOrCustomer==="Customer")
                        showHide=true;
                    else
                        showHide=false;
                }

                return showHide;
            }

            function showHideEditDeleteButton(data) {                
                var myCustID=<%=loggedInUserCustID%>;
                var showHide = true; 

                if(myCustID!=null && myCustID!=undefined){
                    if (data.ID != null && data.ID != undefined) {
                        if(data.ID===myCustID){
                            showHide=false;
                        }
                    }
                }

                return showHide;
            }

            $(document).on("click", "#grid tbody tr .k-grid-zoom", function (e) {                
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                var custID = item.ID;
            
                OpenCheckApplicabilityPopup(custID);
            });

            $(document).on("click", "#grid tbody tr .k-grid-arrow-right", function (e) {                
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                var CustID = item.ID;

                window.location.href="RLCS_EntityLocation_Master_New.aspx?CustID="+CustID;
            });

            //function Edit(e) {
            $(document).on("click", "#grid tbody tr .k-grid-edit", function (e) {                
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                
                var CustID = item.ID;
                
                OpenAddCustomerPopup(CustID);

                return false;
            });

            $(document).on("click", "#grid tbody tr .k-grid-delete", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                if(item!=null && item!=undefined){
                    var cust = item.ID;
                    if(cust!=null && cust!=undefined){
                        window.kendoCustomConfirm("This will also delete all the sub-entities/branch associated with customer. Are you certain you want to delete this customer?","Confirm").then(function () {
                            
                            $.ajax({
                                type: 'POST',
                                url: "<%=avacomRLCSAPIURL%>DeleteCustomer?CustomerID=" + cust,
                                dataType: "json",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    xhr.setRequestHeader('Content-Type', 'application/json');
                                },
                                success: function (result) {
                                    $("#grid").data("kendoGrid").dataSource.read();
                                    var popupNotification = $("#popupNotification").kendoNotification().data("kendoNotification");
                                    popupNotification.show("Deleted Successfully", "error");
                                },
                                error: function (result) {
                   
                                }
                            });
                        }, function () {
                            // For Cancel
                        });  
                    }
                }

                return false;
            });

            function SetGridTooltip(){
                $("#grid").kendoTooltip({
                    filter: ".k-grid-edit",
                    content: function(e){
                        return "View/Edit";
                    }
                });

                $("#grid").kendoTooltip({
                    filter: ".k-grid-delete",
                    content: function(e){
                        return "Delete";
                    }
                });

                $("#grid").kendoTooltip({
                    filter: ".k-grid-arrow-right",
                    content: function(e){
                        return "View Entity/Branch";
                    }
                });

                $("#grid").kendoTooltip({
                    filter: ".k-grid-zoom",
                    content: function(e){
                        return "Applicability Assessment";
                    }
                });
                $("#grid").kendoTooltip({
                    filter: "td:nth-child(2)",
                    position: "bottom",
                    content: function (e) {
                        var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                        var content = dataItem.DistributorOrCustomer;
                        return content;
                    }
                }).data("kendoTooltip");
                $("#grid").kendoTooltip({
                    filter: "td:nth-child(3)",
                    position: "bottom",
                    content: function (e) {
                        var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                        var content = dataItem.CorporateID;
                        return content;
                    }
                }).data("kendoTooltip");
                $("#grid").kendoTooltip({
                    filter: "td:nth-child(4)",
                    position: "bottom",
                    content: function (e) {
                        var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                        var content = dataItem.Name;
                        return content;
                    }
                }).data("kendoTooltip");
                $("#grid").kendoTooltip({
                    filter: "td:nth-child(5)",
                    position: "bottom",
                    content: function (e) {
                        var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                        var content = dataItem.BuyerContactNumber;
                        return content;
                    }
                }).data("kendoTooltip");
                $("#grid").kendoTooltip({
                    filter: "td:nth-child(6)",
                    position: "bottom",
                    content: function (e) {
                        var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                        var content = dataItem.BuyerEmail;
                        return content;
                    }
                }).data("kendoTooltip");
                $("#grid").kendoTooltip({
                    filter: "td:nth-child(7)",
                    position: "bottom",
                    content: function (e) {
                        var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                        var content = dataItem.Status;
                        return content;
                    }
                }).data("kendoTooltip");
               
                $("#btnAdd").kendoTooltip({
                    //filter: ".k-grid-zoom",
                    content: function(e){
                        return "Add New User";
                    }
                });
            }
           

            $(document).ready(function () {  
                
                $('#txtSearch').on('keypress', function(e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) { 
                        e.preventDefault();
                        return false;
                    }
                });
                $('#divAddEditCustomerDialog').hide();
                $('#divCheckApplicabilityDialog').hide();

                BindGrid();
                
                SetGridTooltip();

                $("#btnSearch").click(function (e) {

                    e.preventDefault();
                    
                    var filter = [];
                    $x = $("#txtSearch").val();
                    if ($x) {
                        var gridview = $("#grid").data("kendoGrid");
                        gridview.dataSource.query({
                            page: 1,
                            pageSize: 50,
                            filter: {
                                logic: "or",
                                filters: [
                                  { field: "Name", operator: "contains", value: $x },
                                ]
                            }
                        });

                        return false;
                    }
                    else {
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter({});
                        //BindGrid();
                    }
                    // return false;
                });
            });
    </script>

    <script>
        function OpenAddCustomerPopup(CustID) {
            $('#divAddEditCustomerDialog').show();
            var myWindowAdv = $("#divAddEditCustomerDialog");

            myWindowAdv.kendoWindow({
                width: "60%",
                height: '90%',                
                iframe: true,
                title: "Customer Details",
                visible: false,
                actions: ["Close"],
                close: onClose
            });

            $('#iframeAdd').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeAdd').attr('src', "/RLCS/RLCS_CustomerDetails.aspx?CustomerID=" + CustID);
            return false;
        }

        function onClose() {
            $('#grid').data('kendoGrid').dataSource.read();
        }
        
        $(document).ready(function () {

            $('#ddlServiceProvider').change(function () {
                  
                if($(this).val()){
                    $('#ClearfilterMain').show();
                }
                else{
                    $('#ClearfilterMain').hide();
                }
            });
           

            
                $('#txtSearch').keyup(function () {
                  
                    if($(this).val()){
                        $('#ClearfilterMain').show();
                    }
                    else{
                        $('#ClearfilterMain').hide();
                    }
                });
           

           // $('#ClearfilterMain').css('display', 'none');
            // function BindLocationFilter() {
         
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <span id="popupNotification"></span>
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton>
                        <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>

        <div class="col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
            <div class="col-md-4" style="padding-left: 0px;">
                <input id="ddlServiceProvider" style="width: 100%" />
            </div>
            <div class="col-md-3 colpadding0">
                <input class="k-textbox" type="text" id="txtSearch" style="width: 100%;height:36px"placeholder="Type to search" autocomplete="off" />
                </div>
                <div class="col-md-1" style="margin-top: 0px;"><button id="btnSearch" class="btn btn-primary"style="height:36px;font-weight:400;">Search</button>
            </div>
            <div class="col-md-3">
            </div>

    <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role != null)
        { %>
    <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role != "CADMN")
        { %>
    <div class="col-md-1 colpadding0" style="text-align: right;margin-left: 0px;height:36px;margin-top:-1px;margin-right:1px; width:210px;">
        <button id="btnAdd" class="btn btn-primary"onclick="return OpenAddCustomerPopup(0)"style="height:36px;font-weight:400;float:left;"><span class="k-icon k-i-plus-outline"></span>Add New</button>
    
     <button id="ClearfilterMain" class="btn btn-primary" style="margin-left: 10px;float: none;margin-top:0px;display:none;height:36px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span><font size="2" style="zoom:1.1">Clear Filter</font></button>
              
    </div>
    <% } %>
    <% } %>

        </div>
    </div>

    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: black;" id="filtersstoryboard">&nbsp;</div>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="grid" <%--style="border: none;"--%>>
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>

    <div id="divAddEditCustomerDialog">
        <iframe id="iframeAdd" style="width: 100%; height: 100%; border: none"></iframe>
    </div>

    <div id="divCheckApplicabilityDialog">
        <iframe id="iframeCheck" style="width: 100%; height: 100%; border: none"></iframe>
    </div>
</asp:Content>
