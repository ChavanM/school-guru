﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static com.VirtuosoITech.ComplianceManagement.Business.RLCS.RLCSAPIClasses;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_DocumentOverview : System.Web.UI.Page
    {
        public static string RLCSDocPath = "";
        public static string DocPath = "";
        public int ShowRepeater = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (!string.IsNullOrEmpty(Request.QueryString["RecordID"]) && !string.IsNullOrEmpty(Request.QueryString["DocType"]))
                {
                    ShowRepeater = 1;
                    long RecordID = Convert.ToInt64(Request.QueryString["RecordID"].ToString());
                    string Doctype = Request.QueryString["DocType"].ToString();
                    ViewState["DocType"] = Doctype;
                    ViewState["RecordID"] = RecordID;
                    if (Doctype != "undefined" && RecordID > 0)
                    {
                        getFileViewDtls(Doctype, RecordID);
                    }
                }
                //for Register-Document
                else if (!string.IsNullOrEmpty(Request.QueryString["Customer_BranchID"]) && !string.IsNullOrEmpty(Request.QueryString["EstablishmentType"]) && !string.IsNullOrEmpty(Request.QueryString["State"]) && !string.IsNullOrEmpty(Request.QueryString["Month"]) && !string.IsNullOrEmpty(Request.QueryString["Year"]))
                {
                    ShowRepeater = 2;
                    int CustomerBranchID = Convert.ToInt32(Request.QueryString["Customer_BranchID"].ToString());
                    string EstablishmentType = Request.QueryString["EstablishmentType"];
                    string State = Request.QueryString["State"];
                    string Month = Request.QueryString["Month"];
                    string Year = Request.QueryString["Year"];
                    List<long> lstComplianceIDs = RLCSManagement.GetAll_Registers_Compliance_StateWise(State, EstablishmentType);
                    if (lstComplianceIDs.Count > 0 && lstComplianceIDs != null)
                    {
                        List<long> lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, CustomerBranchID, Year, Month);
                        bool flag = false;
                        if (lstScheduleOnIDs.Count > 0)
                        {
                            List<int> lstFileIDs = RLCSManagement.GetlstFileIDs(lstScheduleOnIDs);
                            List<FileData> lstfilesData = RLCSManagement.GetFileData(lstFileIDs);

                            List<FileData> lstfileDataToView = new List<FileData>();

                            if (lstfilesData.Count > 0)
                            {
                                lstfilesData.ForEach(eachFile =>
                                {
                                    if (!lstfileDataToView.Any(row => row.Name == eachFile.Name))
                                    {
                                        lstfileDataToView.Add(eachFile);
                                    }
                                });
                            }

                            Repeater1.DataSource = lstfileDataToView.OrderBy(entry => entry.ID);
                            Repeater1.DataBind();


                            foreach (var items in lstfilesData)
                            {
                                if (!flag)
                                {
                                    flag = getDataforReg(items.ID);
                                }
                            }

                            //foreach (var item in lstScheduleOnIDs)
                            //{
                            //    if (!flag)
                            //    {
                            //        int ScheduledOnID = Convert.ToInt32(item);
                            //        List<int> objfile = RLCSManagement.GetFileIDs(ScheduledOnID);
                            //        List<FileData> fileData = RLCSManagement.GetFileData(objfile);

                            //        foreach (var items in fileData)
                            //        {
                            //            flag = getDataforReg(items.ID);                                       
                            //        }
                            //    }
                            //}

                            if (!flag)
                            {
                                lblDocuments.Text = "Sorry, No document available to preview.";
                                DocumentViewer1.Visible = false;
                            }
                        }
                        else
                        {
                            lblDocuments.Text = "Sorry, No document available to preview.";
                            DocumentViewer1.Visible = false;
                        }
                    }
                }
                //for Challan-Document
                else if (!string.IsNullOrEmpty(Request.QueryString["RecordID"]) && !string.IsNullOrEmpty(Request.QueryString["ScopeID"]) && !string.IsNullOrEmpty(Request.QueryString["State"]) && !string.IsNullOrEmpty(Request.QueryString["Month"]) && !string.IsNullOrEmpty(Request.QueryString["Year"]) && !string.IsNullOrEmpty(Request.QueryString["Type"]) && !string.IsNullOrEmpty(Request.QueryString["FiletypeID"]) && !string.IsNullOrEmpty(Request.QueryString["ClientID"]))
                {
                    ShowRepeater = 3;
                    long recordID = Convert.ToInt64(Request.QueryString["RecordID"].ToString());
                    int FiletypeID = Convert.ToInt32(Request.QueryString["FiletypeID"].ToString());

                    string ClientID = Request.QueryString["ClientID"];                    
                    string ScopeID = Request.QueryString["ScopeID"];
                    string State = Request.QueryString["State"];
                    string Month = Request.QueryString["Month"];
                    string Year = Request.QueryString["Year"];
                    string challanType = Request.QueryString["Type"];

                    List<long> lstComplianceIDs = new List<long>();

                    bool flagchallan = false;

                    if (challanType.Trim().ToUpper().Equals("EPF") || challanType.Trim().ToUpper().Equals("PF"))
                        challanType = "EPF";
                    else if (challanType.Trim().ToUpper().Equals("ESI") || challanType.Trim().ToUpper().Equals("LWF") || challanType.Trim().ToUpper().Equals("PT"))
                        challanType = challanType.Trim().ToUpper();                    

                    List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
                                       
                    if (challanType.Trim().ToUpper().Equals("EPF") || challanType.Trim().ToUpper().Equals("ESI"))
                    {
                        lstCustomerBranches = RLCSManagement.GetCustomerBranchesByRecordID(recordID, "B");
                        //lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPFOrESICCode(ClientID, challanType, State, "B");
                        lstComplianceIDs = RLCSManagement.GetAll_ChallanRelatedCompliance(challanType);
                    }
                    else if (challanType.Trim().Trim().ToUpper().Equals("PT"))
                    {
                        lstCustomerBranches = RLCSManagement.GetCustomerBranchesByRecordID(recordID, "B");
                        //lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPTState(Convert.ToInt32(AuthenticationHelper.CustomerID), ClientID, State, "B");
                        lstComplianceIDs = RLCSManagement.GetAll_ChallanRelatedCompliance(State, challanType);
                    }                    

                    if (lstComplianceIDs != null)
                    {
                        if (lstComplianceIDs.Count > 0)
                        {
                            //var lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPFOrESICCode(challanType, State, "B");                          
                                                        
                            if (lstCustomerBranches != null)
                            {
                                if (lstCustomerBranches.Count > 0)
                                {
                                    bool getSchdules = false;
                                    foreach (var eachCustBranch in lstCustomerBranches)
                                    {
                                        if (!getSchdules)
                                        {
                                            List<long> lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, Convert.ToInt32(eachCustBranch.AVACOM_BranchID), Year, Month);

                                            if (lstScheduleOnIDs.Count > 0)
                                            {
                                                List<int> lstFileIDs = RLCSManagement.GetlstFileIDs(lstScheduleOnIDs);
                                                List<FileData> lstfilesData = RLCSManagement.GetFileData(lstFileIDs);

                                                Repeater2.DataSource = lstfilesData.OrderBy(entry => entry.ID);
                                                Repeater2.DataBind();

                                                foreach (var item in lstfilesData)
                                                {
                                                    if (!flagchallan)
                                                    {
                                                        flagchallan = getDataforRegChallan(item.ID);
                                                    }
                                                }
                                                //foreach (var Item in lstScheduleOnIDs)
                                                //{
                                                //    //int ScheduledOnID = Convert.ToInt32(Item);
                                                //    ////List<int> _objFileIds = RLCSManagement.GetFileIDs(ScheduledOnID);
                                                //    //List<int> _objFileIds = RLCSManagement.GetFileIDByFileTypeId(ScheduledOnID, FiletypeID);                                                    
                                                //    //if (_objFileIds.Count > 0)
                                                //    //{
                                                //    //    List<FileData> fileData = RLCSManagement.GetFileData(_objFileIds);
                                                //    //    foreach (var item in fileData)
                                                //    //    {
                                                //    //        getDataforRegChallan(item.ID);
                                                //    //        flagchallan = 1;
                                                //    //    }
                                                //    //}
                                                //}                                               
                                            }                                          
                                        }
                                    }//End ForEach
                                }
                            }
                        }
                    }
                    if (!flagchallan)
                    {
                        DocumentViewer2.Visible = false;
                        lblDocuments2.Text = "Sorry, No document available to preview.";
                    }
                }
                //for Return-Document
                else if (!string.IsNullOrEmpty(Request.QueryString["State"]) && !string.IsNullOrEmpty(Request.QueryString["Location"]) && !string.IsNullOrEmpty(Request.QueryString["BranchID"]) && !string.IsNullOrEmpty(Request.QueryString["Month"]) && !string.IsNullOrEmpty(Request.QueryString["Year"]))
                {
                    string State = Request.QueryString["State"];
                    string taskID = Request.QueryString["TaskID"];
                    string Month = Request.QueryString["Month"];
                    string Year = Request.QueryString["Year"];
                    if (State != null && taskID != null && Month != null && Year != null)
                    {
                        GetMyDocumentReturnDocuments(State, taskID, Month, Year);
                    }
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["ScheduleList"]))
                {
                    ShowRepeater = 4;
                    bool flag = false;

                    string ScheduleList = Request.QueryString["ScheduleList"];
                    if (!String.IsNullOrEmpty(ScheduleList))
                    {
                        List<long> lstScheduleOnIDs = new List<long>();
                        lstScheduleOnIDs = ScheduleList.Split(',').Select(long.Parse).ToList();
                        RLCS_DocumentManagement.GetRLCSDocuments("Return", lstScheduleOnIDs);
                        if (lstScheduleOnIDs.Count > 0)
                        {
                            List<int> lstFileIDs = RLCSManagement.GetlstFileIDs(lstScheduleOnIDs);
                            List<FileData> lstfilesData = RLCSManagement.GetFileData(lstFileIDs);
                            List<string> FileNames = new List<string>();
                            List<FileData> lstfileDataToView = new List<FileData>();

                            if (lstfilesData.Count > 0)
                            {
                                lstfilesData.ForEach(eachFile =>
                                {
                                    if (!lstfileDataToView.Any(row => row.Name == eachFile.Name))
                                    {
                                        lstfileDataToView.Add(eachFile);
                                    }
                                });
                            }

                            var ds = lstfileDataToView.OrderBy(entry => entry.ID);

                            Repeater3.DataSource = ds;
                            Repeater3.DataBind();

                            foreach (var file in lstfilesData)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles/RLCS";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension != ".zip")
                                    {
                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        RLCSDocPath = FileName;
                                        FileNames.Add(RLCSDocPath);
                                        if (FileNames.Count > 0)
                                        {
                                            foreach (var filename in FileNames)
                                            {
                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                {

                                                    DocumentViewer3.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(filename);
                                                }
                                                else
                                                {
                                                    DocumentViewer3.Document = Server.MapPath(Convert.ToString(filename));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            lblDocument3.Text = "Sorry, No document available to preview.";
                                            DocumentViewer3.Visible = false;
                                        }

                                    }
                                    else
                                    {
                                        lblDocument3.Text = "Zip file cannot be viewed here. Please use the download option.";
                                        DocumentViewer3.Visible = false;
                                    }

                                }
                                else
                                {
                                    lblDocument3.Text = "Sorry, No document available to preview.";
                                    DocumentViewer3.Visible = false;
                                }
                            }
                        }
                    }
                }
            }
        }


        private void GetMyDocumentReturnDocuments(string state, string taskID, string month, string year)
        {
            List<long> lstScheduleOnIDs = new List<long>();
            List<long> lstScheduleOnIDsToDownload = new List<long>();
            List<long> lstComplianceIDs = new List<long>();

            var lstReturn_Compliances = RLCSManagement.GetAll_ReturnsRelatedCompliance_SectionWise(state, taskID);

            if (lstReturn_Compliances != null)
            {
                if (lstReturn_Compliances.Count > 0)
                {
                    lstComplianceIDs = lstReturn_Compliances.Select(row => (long)row.AVACOM_ComplianceID).ToList();

                    List<MappedCompliance> lstComplianceToProcess = new List<MappedCompliance>();

                    lstReturn_Compliances.ForEach(eachReturnMappedCompliance =>
                    {
                        //lstComplianceToProcess.Clear();
                        //lstComplianceToProcess.Add(eachReturnMappedCompliance); //Needs to find ActCode of Each Compliance that

                        if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("EPF") || eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("ESI"))
                        {
                            //Map Compliance to Code 
                            //Get All CustomerBranch With Code and Map compliance to it

                            string type = string.Empty;
                            if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("EPF"))
                                type = "EPF";
                            else if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("ESI"))
                                type = "ESI";

                            var lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPFOrESICCode(type, state, "B"); //LM_State--CodeValue                                            

                            if (lstCustomerBranches != null)
                            {
                                bool getSchdules = false;

                                if (lstCustomerBranches.Count > 0)
                                {
                                    lstCustomerBranches.ForEach(eachCustomerBranch =>
                                    {
                                        if (eachCustomerBranch != null)
                                        {
                                            if (eachCustomerBranch.AVACOM_BranchID != null)
                                            {
                                                if (!getSchdules)
                                                {
                                                    lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, Convert.ToInt32(eachCustomerBranch.AVACOM_BranchID), year, month);

                                                    if (lstScheduleOnIDs.Count > 0)
                                                    {
                                                        lstScheduleOnIDsToDownload.Union(lstScheduleOnIDs);
                                                        getSchdules = true;
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    lblDocuments.Text = "Sorry, No document available to preview.";
                                }
                            }
                            else
                            {
                                lblDocuments.Text = "Sorry, No document available to preview.";
                            }
                        }
                        else if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("PT") || eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("LWF"))
                        {
                            //Map Compliance to State                                               
                            var customerBranchDetails = RLCSManagement.GetCustomerBranchByStateCode(Convert.ToInt32(AuthenticationHelper.CustomerID), state, "S");

                            if (customerBranchDetails != null)
                            {
                                if (customerBranchDetails.AVACOM_BranchID != null)
                                {
                                    lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID), year, month);

                                    if (lstScheduleOnIDs.Count > 0)
                                    {
                                        lstScheduleOnIDsToDownload.Union(lstScheduleOnIDs);
                                    }
                                    else
                                    {
                                        lblDocuments.Text = "Sorry, No document available to preview.";
                                    }
                                }
                            }
                        }
                        else
                        {

                        }
                    });

                    if (lstScheduleOnIDsToDownload.Count > 0)
                    {
                        RLCS_DocumentManagement.GetRLCSDocuments("Return", lstScheduleOnIDsToDownload);
                    }
                    else
                    {
                        lblDocuments.Text = "Sorry, No document available to preview.";
                    }
                }
            }
        }
        private bool getDataforRegChallan(int iD)
        {
            try
            {
                bool getresultchallan = false;
                var files = GetFile1(iD);
                List<string> FileNames = new List<string>();
                if (files.Count > 0)
                {
                    //Repeater2.DataSource = files.OrderBy(entry => entry.ID);
                    //Repeater2.DataBind();
                    foreach (var file in files)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles/RLCS";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            if (extension != ".zip")
                            {
                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                getresultchallan = true;
                                RLCSDocPath = FileName;
                                FileNames.Add(RLCSDocPath);
                                if (FileNames.Count > 0)
                                {
                                    foreach (var filename in FileNames)
                                    {
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {

                                            DocumentViewer2.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(filename);
                                        }
                                        else
                                        {
                                            DocumentViewer2.Document = Server.MapPath(Convert.ToString(filename));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                lblDocuments2.Text = "Zip file cannot be viewed here. Please use the download option.";
                                DocumentViewer2.Visible = false;
                            }
                        }
                        else
                        {
                            lblDocuments2.Text = "Sorry,No document available to preview.";
                            DocumentViewer2.Visible = false;
                        }

                    }
                }
                else
                {
                    lblDocuments2.Text = "Sorry,No document available to preview.";
                    DocumentViewer2.Visible = false;
                }
                return getresultchallan;
            }

            catch (Exception ex)
            {
                lblDocuments2.Text = "Server Error occurs";
                return false;
            }
        }


        private bool getDataforReg(int iD)
        {
            try
            {
                bool getFileSuccess = false;
                var files = GetFile1(iD);
                List<string> FileNames = new List<string>();
                if (files.Count > 0)
                {
                    //Repeater1.DataSource = files.OrderBy(entry => entry.ID);
                    //Repeater1.DataBind();
                    foreach (var file in files)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {                            
                            string Folder = "~/TempFiles/RLCS";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            if (extension != ".zip")
                            {
                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();

                                RLCSDocPath = FileName;
                                FileNames.Add(RLCSDocPath);
                                getFileSuccess = true;
                            }
                        }
                        else
                        {
                            lblDocuments.Text = "Zip file cannot be viewed here. Please use the download option.";
                            DocumentViewer1.Visible = false;
                        }
                    }
                    if (FileNames.Count > 0)
                    {
                        foreach (var filename in FileNames)
                        {
                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                            {

                                DocumentViewer1.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(filename);
                            }
                            else
                            {
                                DocumentViewer1.Document = Server.MapPath(Convert.ToString(filename));
                            }
                        }
                    }
                    else
                    {
                        lblDocuments.Text = "Sorry, No document available to preview.";
                        DocumentViewer1.Visible = false;
                    }

                }
                else
                {
                    lblDocuments.Text = "Sorry, No document available to preview.";
                    DocumentViewer1.Visible = false;
                }

                return getFileSuccess;
            }
            catch (Exception ex)
            {
                lblDocuments.Text = "Server Error occurs";
                return false;
            }
        }
        private List<FileData> GetFile1(int iD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ojdata = (from row in entities.FileDatas
                              where row.ID == iD
                              select row).ToList();
                return ojdata;
            }
        }
        private void getFileViewDtls(string doctype, long recordID)
        {
            try
            {

                var files = GetFile(recordID, doctype);
                List<string> FileNames = new List<string>();
                if (files.Count > 0)
                {
                    DocViewReapeter.DataSource = files.OrderBy(entry => entry.ID);
                    DocViewReapeter.DataBind();
                    foreach (var file in files)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles/RLCS";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            if (extension != ".zip")
                            {
                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();

                                RLCSDocPath = FileName;
                                FileNames.Add(RLCSDocPath);
                                if (FileNames.Count > 0)
                                {
                                    foreach (var filename in FileNames)
                                    {
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {

                                            doccontrol.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(filename);
                                        }
                                        else
                                        {
                                            doccontrol.Document = Server.MapPath(Convert.ToString(filename));
                                        }
                                    }
                                }
                                else
                                {
                                    errormsg.Text = "Sorry, No document available to preview.";
                                    doccontrol.Visible = false;
                                }

                            }
                            else
                            {                                
                                errormsg.Text = "Zip file cannot be viewed here. Please use the download option.";
                                doccontrol.Visible = false;
                            }

                        }
                        else
                        {
                            errormsg.Text = "Sorry, No document available to preview.";
                            doccontrol.Visible = false;
                        }
                    }
                }
                else
                {
                    errormsg.Text = "Sorry, No document available to preview.";
                    doccontrol.Visible = false;
                }

            }
            catch (Exception ex)
            {

            }
        }
        private List<RLCS_HistoricalDocument_FileData> GetFile(long recordID, string docType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var file = (from row in entities.RLCS_HistoricalDocument_FileData
                            where row.RecordID == recordID && row.DocType == docType
                            select row).ToList();

                return file;
            }
        }

        protected void DocViewReapeter_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            ShowRepeater = 1;
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

            List<RLCS_HistoricalDocument_FileData> DocumentFileData = new List<RLCS_HistoricalDocument_FileData>();
            List<RLCS_HistoricalDocument_FileData> _objDocumentFileData = new List<RLCS_HistoricalDocument_FileData>();
            DocumentFileData = getFileDataforView(Convert.ToInt32(commandArgs[0]), commandArgs[1], Convert.ToInt32(commandArgs[2]));

            if (e.CommandName.Equals("View"))
            {
                if (DocumentFileData.Count > 0)
                {
                    int i = 0;
                    foreach (var file in DocumentFileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);
                            //if (extension == ".zip")
                            //{

                            //    errormsg.Text = "Zip File Can't View! please Download it";
                            //}
                            //else
                            //{
                            //Directory.CreateDirectory(Server.MapPath(DateFolder));

                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }

                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            if (file.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();
                            i++;
                            RLCSDocPath = FileName;
                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                            {

                                doccontrol.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(RLCSDocPath);
                            }
                            else
                            {
                                doccontrol.Document = Server.MapPath(Convert.ToString(RLCSDocPath));
                            }
                            //}
                        }
                    }
                }

            }
        }
        private List<RLCS_HistoricalDocument_FileData> getFileDataforView(int v1, string v2, int v3)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objFileData = (from row in entities.RLCS_HistoricalDocument_FileData
                                    where row.ID == v1 && row.RecordID == v3
                                    select row).ToList();
                return _objFileData;
            }
        }
        protected void DocViewReapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //var scriptManager = ScriptManager.GetCurrent(this.Page);
                //LinkButton lblIDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                //scriptManager.RegisterAsyncPostBackControl(lblIDocumentVersionView);

            }
        }
        protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            ShowRepeater = 3;
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

            List<FileData> DocumentFileData = new List<FileData>();
            List<FileData> _objDocumentFileData = new List<FileData>();
            DocumentFileData = getDocumentRegistrationFileDataforView(Convert.ToInt32(commandArgs[0]), commandArgs[1]);

            if (e.CommandName.Equals("View"))
            {
                if (DocumentFileData.Count > 0)
                {
                    int i = 0;
                    foreach (var file in DocumentFileData)
                    {
                        DocumentViewer2.Visible = true;
                        lblDocuments2.Text = "";

                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);
                            if (extension != ".zip")
                            {
                                //Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;
                                RLCSDocPath = FileName;
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {

                                    DocumentViewer2.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(RLCSDocPath);
                                }
                                else
                                {
                                    DocumentViewer2.Document = Server.MapPath(Convert.ToString(RLCSDocPath));
                                }
                            }
                            else {
                                DocumentViewer2.Visible = false;
                                lblDocuments2.Text = "Zip file cannot be viewed here. Please use the download option.";
                            }
                        }
                    }
                }
            }

        }
        private List<FileData> getDocumentRegistrationFileDataforView(int FileID, string FileName)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var _objFileData = (from row in entities.FileDatas
                                        where row.ID == FileID && row.Name == FileName
                                        select row).ToList();
                    return _objFileData;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }
        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            ShowRepeater = 2;
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

            List<FileData> DocumentFileData = new List<FileData>();
            List<FileData> _objDocumentFileData = new List<FileData>();
            DocumentFileData = getDocumentRegistrationFileDataforView(Convert.ToInt32(commandArgs[0]), commandArgs[1]);

            if (e.CommandName.Equals("View"))
            {

                lblDocuments.Text = "";
                DocumentViewer1.Visible = true;
                if (DocumentFileData.Count > 0)
                {
                    int i = 0;
                    foreach (var file in DocumentFileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);
                            if (extension != ".zip")
                            {
                                //Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;
                                RLCSDocPath = FileName;
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {

                                    DocumentViewer1.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(RLCSDocPath);
                                }
                                else
                                {
                                    DocumentViewer1.Document = Server.MapPath(Convert.ToString(RLCSDocPath));
                                }
                            }
                            else
                            {
                                lblDocuments.Text = "Zip file cannot be viewed here. Please use the download option.";
                                DocumentViewer1.Visible = false;
                            }
                        }
                    }
                }
            }
        }
        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }
    }
}
