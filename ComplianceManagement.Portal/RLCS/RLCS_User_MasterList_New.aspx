﻿<%@ Page Title="User :: Setup HR+" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_User_MasterList_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_User_MasterList_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="~/Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="~/Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="~/Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss_V1.css" rel="stylesheet" />

    <title></title>

   
     <style type="text/css">
           input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0px -3px ;
            line-height: normal;
        }

         .k-textbox>input{
         text-align:left;
     }
         .k-grid-header th.k-with-icon .k-link{
             color:#535b6a;
         }


          .k-tooltip{
            margin-top:5px;
        }
          label.k-label:hover{
              color:#1fd9e1;
          }
         .k-filter-row th, .k-grid-header th[data-index='7'] {
             text-align:center;
             color:#535b6a;
         }
          .k-menu .k-menu-group .k-separator, .k-menu-scroll-wrapper.vertical .k-menu-group .k-separator, .k-menu-scroll-wrapper.vertical .k-separator, .k-popups-wrapper.vertical .k-menu-group .k-separator, .k-popups-wrapper.vertical .k-separator, ul.k-menu-vertical .k-separator {
            color:#535b6a;
            border-color: #ceced2 !important;
      } 
        .k-item,.k-in{
    /* width: 90px; */
             border-color: transparent !important;
             box-sizing: border-box;
             box-shadow: none;
        }
           .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
             .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
               .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }
         .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }
           .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
      .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top:-1px;
        }
       .k-button.k-state-active, .k-button:active{
            color:black;
        }
        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: auto !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
            margin-top:-4px;
            padding-top:0px;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:2.7px;
            margin-bottom:0px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 20px;
            min-height: 25px;
            background-color:transparent;
            border: none;
            padding-left:0px;
            padding-right:0px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            
            padding-top:0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

      .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            /*border-width: 0 0px 1px 0px;*/
            text-align: center;
            padding-left: 0px;
            padding-right: 10px;
        }

        .k-grid-pager {
            margin-top: 0px;
            border-width:0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 1px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: #535b6a;
            height: 20px;
            vertical-align: middle;
        }
         .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            width:max-content;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: -5px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            background-color: white;
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-grid-footer-wrap, .k-grid-header-wrap {
   border-width: 0 0px 0 0;
}
        .k-grid, .k-listview{
            margin-top:8px;
        }
         .k-autocomplete, .k-dropdown-wrap, .k-multiselect-wrap, .k-numeric-wrap, .k-picker-wrap, .k-textbox {
    border-width: 1px;
    border-style: solid;
    height:33px;
}
         .k-state-default>.k-select {
    margin-top: 0px;
    border-color: #c5c5c5;
}
         .k-dropdown-wrap .k-input, .k-numeric-wrap .k-input, .k-picker-wrap .k-input{
             padding-top:5px;
             padding-bottom:5px;
         }
             .k-grid-header .k-header>.k-link, .k-header, .k-treemap-title {
    color: rgba(0,0,0,0.5);
}
              .k-tooltip-content{
         width: max-content;
        
         }
     .k-multiselect-wrap .k-input{
                  padding-top:5px;
              }
        .k-state-focused:hover {
            text-decoration: none;
            background-color: transparent;
            border-color: transparent;
        }
         .btn {
         height:36px;
         font-weight:400;
         font-size:14px;
         }
    </style>
    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
        <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script type="text/javascript">

        function ClearAllFilterMain(e) {

            e.preventDefault();

            $("#ddlCustomer").data("kendoDropDownList").value("");
            $('#ClearfilterMain').css('display', 'none');

            $("#grid").data("kendoGrid").dataSource.data([]);
            $("#txtSearch").val('');
        
        };


        function Bind_Customers() {
            $("#ddlCustomer").kendoDropDownList({
                placeholder: "Select",
                autoWidth: true,                
                dataTextField: "Name",
                dataValueField: "ID",
                index: 0,
                change: function (e) {
                    $('#btnAddUser').show();
                    BindUserGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            <%--url: '<%=avacomRLCSAPIURL%>GetCustomers?custID=' + <%=custID%> +'&SPID='+<%=spID%>+'&distID='+<%=distID%>,--%>
                             url: '<%=avacomRLCSAPIURL%>GetHRCustomers?userID=<%=userID%>&custID=<%=custID%>&SPID=<%=spID%>&distID=<%=distID%>&roleCode=<%=RoleCode%>&prodType=2',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            return response.Result;
                        }
                    }
                }
            });
        }
                
        function checkUserActive(value) {                        
            if (value) {
                return "Active";
            } else if (!value) {
                return "In-Active";
            }
        }

        $(document).on("click", "#grid tbody tr .k-grid-edit", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            var userID = item.ID;
            OpenAddEditUserPopup(userID);
        });

        $(document).on("click", "#grid tbody tr .k-grid-delete", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            if(item!=null && item!=undefined){
                if(!item.IsActive){
                    var selectedUserID = item.ID;
                  
                    if(selectedUserID!=null && selectedUserID!=undefined){
                        window.kendoCustomConfirm("Are you sure! You want to delete this User?","Confirm").then(function () {
                        
                            $.ajax({
                                type: 'POST',
                                url: '<%=avacomRLCSAPIURL%>DeleteUser?UserID=' + selectedUserID,
                                dataType: "json",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    xhr.setRequestHeader('Content-Type', 'application/json');
                                },
                                success: function (result) {
                                    if(result.Result=="NOTDELETE")
                                        window.kendoCustomAlert("Compliance(s) assigned to this user, hence can not be delete","Message");
                                    else
                                        window.kendoCustomAlert("User Deleted Successfully","Message");                               
                                    
                                    $("#grid").data("kendoGrid").dataSource.read();
                                    //var popupNotification = $("#popupNotification").kendoNotification().data("kendoNotification");
                                    //popupNotification.show("Deleted Successfully", "error");
                                },
                                error: function (result) {
                                    // notify the data source that the request failed
                                    console.log(result);
                                }
                            });
                           
                        }, function () {
                            // For Cancel
                        });  
                    }
                }
                else{
                    window.kendoCustomAlert("Active User can not Deleted","Message");  
                }
            }

            return false;
        });
        
        $(document).on("click", "#grid tbody tr .k-grid-chngpassword", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            if(item!=null && item!=undefined){
                var selectedUserID = item.ID;
                if(selectedUserID!=null && selectedUserID!=undefined){
                   
                    window.kendoCustomConfirm("Do you want to Reset Password of this User?","Confirm").then(function () {
                        debugger;
                                                
                        kendo.ui.progress($("#grid").data("kendoGrid").element,true);
                      
                        $.ajax({
                            type: 'POST',
                            url: "<%= avacomRLCSAPIURL%>ResetPasswordUser?UserID=" + selectedUserID+"&Role=<%= userRole%>",
                            dataType: "json",
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                xhr.setRequestHeader('Content-Type', 'application/json');
                            },
                            success: function (result) {
                                kendo.ui.progress($("#grid").data("kendoGrid").element,false);

                                window.kendoCustomAlert("User Password Reset Successfully","Message");                                
                            },
                            error: function (result) {
                                // notify the data source that the request failed
                                kendo.ui.progress($("#grid").data("kendoGrid").element,false);                                
                                console.log(result);
                            }
                        });
                           
                    }, function () {
                        // For Cancel
                    });
                }
            }
        });

        function BindUserGrid() {
            
            var customerID=-1;

            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                customerID=$("#ddlCustomer").val();
            } else {
                customerID=<% =loggedInUserCustID%>;
            }
            
            var gridview = $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetAll_UserMaster?customerID='+customerID,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            if (response != undefined)
                                return response.Result;
                        },
                        total: function (response) {
                            if (response != undefined)
                                return response.Result.length;
                        }
                    }
                },                
                sortable: true,
                filterable: true,
                columnMenu: true,
               
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ID", title: "UserID",menu:false, },
                    {
                        field: "FirstName", title: 'First Name',
                        width: "18%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: true,
                            search: true,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "LastName", title: 'Last Name',
                        width: "18%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Email", title: 'Email',
                        width: "18%",
                        filterable: {
                            extra: false,
                            search: true,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ContactNumber", title: 'Contact No',
                        width: "14%",
                        attributes: {
                            style: 'white-space: nowrap;text-align:left; '
                        },
                        filterable: {
                            extra: false,
                            search: true,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "HR_Role", title: 'Role',
                        width: "12%",
                        attributes: {
                            style: 'white-space: nowrap;text-align:left;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "IsActive", title: 'Status',
                        width: "10%",
                        template: '#:checkUserActive(IsActive)#',
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            feild: '#:checkUserActive(IsActive)#',
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: 
                            [
                                { name: "edit", text: "", imageClass: "k-i-edit", iconClass: "k-icon k-i-edit", className: "ob-download" },
                                { name: "delete", text: "", iconClass: "k-icon k-i-delete", className: "ob-delete" },
                                { name: "chngpassword", text: "", iconClass: "k-icon k-i-reset", className: "ob-changepassword" },
                            ], title: "Action", lock: true, width: "10%;"
                    }
                ],
                pageable: {
                    refresh: false,
                    pageSize:10,
                    pageSizes:true,
                    buttonCount: 3,
                  
                    change: function (e) {
                        //$('#chkAll').removeAttr('checked');
                        //$('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                },
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                }
            });
        }

        function SetGridTooltip(){
            $("#grid").kendoTooltip({
                filter: "td:nth-child(2)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.FirstName;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.LastName;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Email;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.ContactNumber;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(6)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Role;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(7)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = checkUserActive(dataItem.IsActive);
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function(e){
                    return "View/Edit";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-delete",
                content: function(e){
                    return "Delete";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-chngpassword",
                content: function(e){
                    return "Reset Password";
                }
            });
        }

        $(document).ready(function () { 

            $('#ddlCustomer').change(function () {
                  
                if($(this).val()){
                    $('#ClearfilterMain').show();
                }
                else{
                    $('#ClearfilterMain').hide();
                }
            });
           


            $('#txtSearch').keyup(function () {
                  
                if($(this).val()){
                    $('#ClearfilterMain').show();
                }
                else{
                    $('#ClearfilterMain').hide();
                }
            });
           
           

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            fhead('Masters/User');
            $('#divRLCSUserAddDialog').hide();
            $("#btnAddUser").kendoTooltip({ content: "Add New-User" });
            $("#UserCustomerMapping").kendoTooltip({ content: "User-Customer-Mapping" });
            
            Bind_Customers();

            BindUserGrid();

            SetGridTooltip();

            $("#btnSearch").click(function (e) {
                e.preventDefault();

                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#grid").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 10,
                        filter: {
                            logic: "or",
                            filters: [
                                { field: "FirstName", operator: "contains", value: $x },
                              { field: "LastName", operator: "contains", value: $x },                              
                              { field: "Email", operator: "contains", value: $x },
                              { field: "ContactNumber", operator: "contains", value: $x },
                              { field: "Role", operator: "contains", value: $x }
                            ]
                        }
                    });

                    return false;
                }
                else {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    //BindUserGrid();
                }
                // return false;
            });
        });

    </script>

    <script>
        function OpenAddEditUserPopup(UserID){
            
            $('#divRLCSUserAddDialog').show();            
            var myWindowAdv = $("#divRLCSUserAddDialog");

            var ddlcustomer = $("#ddlCustomer").val();

            myWindowAdv.kendoWindow({
                width: "40%",
                height: '80%',
                content: "../RLCS/RLCS_UserDetails.aspx?UserID=" + UserID + '&CustomerID=' + ddlcustomer,
                iframe: true,
                title: "User Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose,
                open:onOpen
            });

            myWindowAdv.data("kendoWindow").center().open();
            return false;
        }

        function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }

        function onClose() {
            $('#grid').data('kendoGrid').dataSource.read();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton>
                        <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>
    </div>

    <div class="row colpadding0" style="margin-top: 1%">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="col-md-4" style="padding-left: 0px;">
                <input id="ddlCustomer" style="width: 100%; height:32px" />
            </div>
            <div class="col-md-4 colpadding0" style="width: 320px;">
                <input class="k-textbox" type="text" id="txtSearch" style="width: 70%;height:36px;margin-left:5px;" placeholder="Type to Search" />
                <button id="btnSearch" class="btn btn-primary" style="margin-left:3px;height:36px">Search</button>
            </div>

             <div class="col-md-4" style="text-align: right; height:36px; width:330px; float:left;margin-left: -46px;">
               <div id="UserCustomerMapping" style="float:left;margin-left:24px">  <asp:LinkButton ID="lnkbtnUserCustomerMapping" class="btn btn-primary" runat="server" PostBackUrl="~/RLCS/HRPlus_UserCustomerMapping_New.aspx" >User-Customer-Mapping</asp:LinkButton>
               </div>
                   
                    <%--<button id="btnUserCustomerMapping" class="btn btn-primary"><span class="k-icon k-i-plus-outline"></span>User-Customer-Mapping</button> class="k-button btnAdd"--%>
                 <button id="btnAddUser" class="btn btn-primary" onclick="return OpenAddEditUserPopup(0)"><span class="k-icon k-i-plus-outline" ></span>Add New</button> <%--class="k-button btnAdd"--%>
            </div>
            <button id="ClearfilterMain" class="btn btn-primary" style="margin-left: 0px;float: left;display:none;height:36px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span><font size="3" style="zoom:0.9">Clear Filter</font></button>
        
                        
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="grid"<%-- style="border: none;"--%>>
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>

    <div id="divRLCSUserAddDialog">
        <iframe id="iframeAdd" style="width: 100%; height: 100%; border: none"></iframe>
    </div>
</asp:Content>
