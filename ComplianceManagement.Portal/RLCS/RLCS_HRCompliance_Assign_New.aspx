﻿<%@ Page Title="Compliance-Assignment :: Setup HR+" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_HRCompliance_Assign_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_HRCompliance_Assign_New" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
         });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        //function UncheckHeader() {
        //    var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
        //    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
        //    var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
        //    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        //        rowCheckBoxHeader[0].checked = true;
        //    } else {

        //        rowCheckBoxHeader[0].checked = false;
        //    }
        //}

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            fhead('Masters/Compliance Assignment');

            $('#divReAssignCompliance').hide();

            $("#divFilterLocation").hide();
        });

        function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }

        function OpenReAssignPopup() {
            
            //Loader to Show
            var selectedCustID = $('#<%= ddlCustomer.ClientID %>').val();

            if (selectedCustID != undefined && selectedCustID != null && selectedCustID != -1 && selectedCustID != "") {
                $('#divReAssignCompliance').show();

                var myWindowAdv = $("#divReAssignCompliance");

                myWindowAdv.kendoWindow({
                    width: "85%",
                    height: '90%',
                    //maxHeight: '90%',
                    //minHeight:'50%',
                    title: "Re-Assign Compliance",
                    visible: false,
                    actions: ["Close"],
                    open: onOpen,                    
                });

                $('#iframeReAssign').attr('src', 'about:blank');
                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeReAssign').attr('src', "/RLCS/ClientSetup/RLCS_ComplianceReAssign.aspx?CustID=" + selectedCustID);

                var dialog = $("#divReAssignCompliance").data("kendoWindow");
                dialog.title("Re-Assign Compliance");

                if ($('#divReAssignCompliance_wnd_title').text() == 'Re-Assign Compliance') {
                    $('.k-window').css('width', '85%');
                    $('.k-window').css('height', '90%');
                    $('.k-window').css('position', 'fixed');
                    $('.k-window').css('top', '2%');
                    $('.k-window').css('left', '8%');

                }
            }
            return false;
        }

        function OpenUploadAssignPopup() {
            
            //Loader to Show
            var selectedCustID = $('#<%= ddlCustomer.ClientID %>').val();

            if (selectedCustID != undefined && selectedCustID != null && selectedCustID != -1 && selectedCustID != "") {
                $('#divReAssignCompliance').show();

                var myWindowAdv = $("#divReAssignCompliance");

                myWindowAdv.kendoWindow({
                    width: "50%",
                    height: '50%',
                    //maxHeight: '90%',
                    //minHeight:'50%',
                    title: "Upload Compliance Assignment",
                    visible: false,
                    actions: ["Close"],
                    open: onOpen,                    
                });

                $('#iframeReAssign').attr('src', 'about:blank');
                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeReAssign').attr('src', "/RLCS/HRPlus_ComplianceAssignmentUpload.aspx?CustID=" + selectedCustID);

                var dialog = $("#divReAssignCompliance").data("kendoWindow");
                dialog.title("Upload Compliance Assignment");

                if ($('#divReAssignCompliance_wnd_title').text() == 'Upload Compliance Assignment') {
                    $('.k-window').css('width', '50%');
                    $('.k-window').css('height', '50%');
                    $('.k-window').css('position', 'fixed');
                    $('.k-window').css('top', '20%');
                    $('.k-window').css('left', '27%');
                }

            }
            return false;
        }
    </script>

    
    <style>
        .panel-heading .nav > li > a {
            font-size: 17px;
        }

        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .btn{
            font-size:14px;
            font-weight:400;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .aspNetDisabled {
            cursor: not-allowed;
        }

        .btn-primary[disabled] {
            background-color: #d7d7d7;
            border-color: #d7d7d7;
        }

        .k-window div.k-window-content {
            overflow: hidden;
        }

        .form-control{
            height: 32px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0" style="margin-bottom: 10px">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li> 
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton> <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>
    </div>

    <div class="clearfix"></div>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="vsHRCompAssign" runat="server" class="alert alert-block alert-danger fade in"
                    ValidationGroup="ComplianceInstanceValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
            </div>

            <div class="clearfix"></div>

            <div class="row col-md-12 colpadding0" style="margin-bottom: 10px">
                <div class="col-md-4 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlCustomer" class="form-control" AutoPostBack="true" AllowSingleDeselect="false"
                        DataPlaceHolder="Select Customer" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" Width="95%">
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-4 colpadding0">
                    <asp:TextBox runat="server" ID="tbxFilterLocation" autocomplete="off" CssClass="form-control" Width="95%" />
                    <div style="position: absolute;z-index: 50;overflow-y: auto;background: white;border: 1px solid rgb(195, 185, 185);height: 200px;width: 100%;display: block;" id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                            Style="overflow: auto; margin-top: -20px; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                        <asp:CompareValidator ControlToValidate="tbxFilterLocation" ErrorMessage="Select Entity/Branch"
                            runat="server" ValueToCompare="< Select >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                            Display="None" />
                    </div>
                </div>

                <div class="col-md-4 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlScopeType" class="form-control" AllowSingleDeselect="false"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlScopeType_SelectedIndexChanged" Width="95%">
                        <asp:ListItem Text="Register" Value="SOW03"></asp:ListItem>
                        <asp:ListItem Text="PF Challan" Value="SOW13"></asp:ListItem>
                        <asp:ListItem Text="ESI Challan" Value="SOW14"></asp:ListItem>
                        <asp:ListItem Text="PT Challan" Value="SOW17"></asp:ListItem>
                        <asp:ListItem Text="Return" Value="SOW05"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row col-md-12 colpadding0">
                <div class="col-md-4 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlState" DataPlaceHolder="Select State" class="form-control" AllowSingleDeselect="false"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" Width="95%">
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlFilterPerformer" AllowSingleDeselect="false" DataPlaceHolder="Select Performer"
                        class="form-control" Width="95%">
                    </asp:DropDownListChosen>
                    <asp:CompareValidator ErrorMessage="Select Performer" ControlToValidate="ddlFilterPerformer" runat="server"
                        ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                </div>

                <div class="col-md-3 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlFilterReviewer" AllowSingleDeselect="false" DataPlaceHolder="Select Reviewer"
                        class="form-control" Width="90%">
                    </asp:DropDownListChosen>
                    <asp:CompareValidator ErrorMessage="Select Reviewer" ControlToValidate="ddlFilterReviewer" runat="server"
                        ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                </div>

                 <div class="col-md-2 text-right" style="margin-left:-29px;width:191px; float:left;">
                    <asp:Button ID="btnReAssign" runat="server"  Style="float:left;" CssClass="btn btn-primary" OnClientClick="OpenReAssignPopup();" Text="Re-Assign"  />
                     <asp:Button ID="btnUploadAssignment" runat="server"  Style="float:left;margin-left:10px;"  CssClass ="btn btn-primary" OnClientClick="OpenUploadAssignPopup();" Text="Upload"  />
                </div>
            </div>

            <div class="row col-md-12 colpadding0" style="display: none;">
                <div class="col-md-4 colpadding0">
                    <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Compliance Category
                    </label>
                    <asp:DropDownList runat="server" ID="ddlComplianceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceCatagory_SelectedIndexChanged">
                    </asp:DropDownList>
                    <%--<asp:CompareValidator ErrorMessage="Please select Compliance Catagory." ControlToValidate="ddlComplianceCatagory"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" Display="None" ValidationGroup="ComplianceInstanceValidationGroup" /> --%>
                </div>

                <div class="col-md-4 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlFilterApprover" Style="padding: 0px; margin: 0px; min-height: 22px; min-width: 50px;">
                    </asp:DropDownList>
                </div>

                <div class="col-md-4 colpadding0">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Act:
                    </label>
                    <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="txtbox" />
                    <div style="margin-left: 0px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvActList">
                        <asp:Repeater ID="rptActList" runat="server">
                            <HeaderTemplate>
                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                    <tr>
                                        <td style="width: 100px;">
                                            <asp:CheckBox ID="actSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                        <td style="width: 282px;">
                                            <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 20px;">
                                        <asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" /></td>
                                    <td style="width: 200px;">
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                            <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                            <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>

                <div class="col-md-4 colpadding0">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Filter:
                    </label>

                    <asp:TextBox runat="server" ID="tbxFilter" Style="height: 16px; width: 390px;" MaxLength="50" AutoPostBack="true"
                        OnTextChanged="tbxFilter_TextChanged" />
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row col-md-12 colpadding0">
                <asp:Panel ID="Panel1" Width="100%" Style="min-height: 10px; overflow-y: auto; margin-bottom: 10px;" runat="server">
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                    <div class="tab-content">
                        <div runat="server" id="performerdocuments" class="tab-pane active">
                            <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" GridLines="None" CssClass="table"
                                AllowSorting="true" OnSorting="grdComplianceRoleMatrix_Sorting" OnRowCreated="grdComplianceRoleMatrix_RowCreated"
                                CellPadding="4" AllowPaging="True" PageSize="50" Width="100%" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound"
                                Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging" ShowHeaderWhenEmpty="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="SR.No" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAssignSelectAll" Text="" runat="server" AutoPostBack="true"
                                                Checked="true" OnCheckedChanged="chkAssignSelectAll_CheckedChanged" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkAssign" runat="server" Checked="true" />
                                            <%--AutoPostBack="true" OnCheckedChanged="chkAssign_CheckedChanged"--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ComplianceID" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Act">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label ID="lblAct" runat="server" Text='<%# Eval("ActName")%>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:Label ID="lblState" runat="server" Text='<%# Eval("SM_Name")%>' ToolTip='<%# Eval("SM_Name") %>'></asp:Label>
                                            <asp:Label ID="lblStateCode" runat="server" Text='<%# Eval("StateID")%>' ToolTip='<%# Eval("StateID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Compliance">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label runat="server" Text='<%# Eval("ShortForm")%>' ToolTip='<%# Eval("ShortForm") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                <asp:Label ID="lblShortDesc" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Form">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblReqForm" runat="server" Text='<%# Eval("RequiredForms") %>' ToolTip='<%# Eval("RequiredForms") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" />
                                <%-- <RowStyle CssClass="clsROWgrid" />--%>
                                <%--<HeaderStyle CssClass="clsheadergrid" />--%>
                                <PagerTemplate>
                                    <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No compliance to display. Either all have been assigned or all compliances are filtered out.                                       
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                <div runat="server" id="DivRecordsScrum">
                                    <p style="padding-right: 0px !Important; color: #666; text-align: -webkit-left;">
                                        <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" runat="server" id="DivnextScrum" style="margin-bottom: 20px">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1" style="padding-left: 0px;">
                        <label for="ddlPageSize" class="hidden-label">Show</label>
                        <asp:DropDownList runat="server" Visible="false" ID="ddlPageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                            class="form-control">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10"  />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" Selected="True" />
                        </asp:DropDownList>
                    </div>
                </asp:Panel>
            </div>

            <div class="row col-md-12 colpadding0" style="text-align: center">
                <asp:Button Text="Save" runat="server" ID="btnSaveAssignment" OnClick="btnSave_Click" class="btn btn-primary"
                    ValidationGroup="ComplianceInstanceValidationGroup" CausesValidation="true" Visible="false" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divReAssignCompliance">
        <iframe id="iframeReAssign" style="width: 100%; height: 100%; border: none;"></iframe>
    </div>
</asp:Content>

