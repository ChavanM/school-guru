﻿<%@ Page Title="MY DASHBOARD" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_DFMSPOCDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_DFMSPOCDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="/avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="/avantischarts/highcharts/js/modules/drilldown.js"></script>
    <script type="text/javascript" src="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>

    <link href="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />

    <link href="/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>

    <script src="/Newjs/bootstrap-datepicker.min.js"></script>
    <link href="/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />

    <script>
        function fchangeheight(obj, cal) {
            alert('Called');
            $('iframe#showdetails').removeAttr('height');
            $('iframe#showdetails').attr('height', cal)
        }

        $(document).ready(function () {
            fhead('My Dashboard');

            initializeJQueryUIDeptDDL();
        });

        function GotoComplianceInputOutputPage(complianceType, returnRegisterChallanID, actID, complianceID, branchID, month, year) {
            window.location.href = "/RLCS/ComplianceInputOutput.aspx?ComplianceType=" + complianceType
                                                            + "&ReturnRegisterChallanID=" + returnRegisterChallanID
                                                            + "&ActID=" + actID
                                                            + "&ComplianceID=" + complianceID
                                                            + "&BranchID=" + branchID
                                                            + "&Month=" + month
                                                            + "&Year=" + year;
        }
    </script>

    <script type="text/javascript">
        function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }
        
        function ShowMyCompliances(comType, filterType, customerid, userid, selectedFreq, selectedPeriod, selectedYear, startPeriod, endPeriod) {
            debugger;
            $('#divMyCompliances').show();

            var myWindowAdv = $("#divMyCompliances");

            function onClose() {
                $(this.element).empty();
            }

            var isnotEmpty = (filterType.trim().length !== 0);

            var strTitle = '';

            if (isnotEmpty) {
                if (filterType.trim() === 'U') {
                    strTitle = 'Upcoming Compliances'
                } else if (filterType.trim() === 'O') {
                    strTitle = 'Overdue Compliances'
                } else if (filterType.trim() === 'PR') {
                    strTitle = 'Pending for Review Compliances'
                }
            }

            myWindowAdv.kendoWindow({
                width: "90%",
                height: "80%",
                title: strTitle,
                content: "../RLCS/HRPlus_MyCompliances.aspx?customerid=" + customerid + "&filterType=" + filterType + "&comType=" + comType + "&userID=" + userid
                            + "&Freq=" + selectedFreq + "&period=" + selectedPeriod + "&year=" + selectedYear + "&startPeriod=" + startPeriod + "&endPeriod=" + endPeriod,
                iframe: true,
                visible: false,
                actions: [
                    "Pin",
                    "Close"
                ],
                open: onOpen,
                close: onClose
            });

            var dialog = myWindowAdv.data("kendoWindow");
            dialog.title(strTitle);
            
            myWindowAdv.data("kendoWindow").center().open();

            return false;
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            //exapand
            $(".panel-wrap").on("click", "span.k-i-sort-desc-sm", function (e) {
                var contentElement = $(e.target).closest(".widget").find(">div");
                $(e.target)
                    .removeClass("k-i-sort-desc-sm")
                    .addClass("k-i-sort-asc-sm");

                kendo.fx(contentElement).expand("vertical").stop().play();
            });

            //collapse
            $(".panel-wrap").on("click", "span.k-i-sort-asc-sm", function (e) {
                var contentElement = $(e.target).closest(".widget").find(">div");
                $(e.target)
                    .removeClass("k-i-sort-asc-sm")
                    .addClass("k-i-sort-desc-sm");

                kendo.fx(contentElement).expand("vertical").stop().reverse();
            });

            $("#filterPanel").kendoPanelBar({
                expandMode: "full"
            });

            $('[id*=lstBoxCustomers]').multiselect({
                enableFiltering: false,
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                nSelectedText: ' - Customer(s) selected',
            });

            $(".closeButton").click(function () {

                id = $(this).attr("id");
                if ($('#ContentPlaceHolder1_divContainerCustomer_' + id).hasClass("customerDetails"))
                    $('#ContentPlaceHolder1_divContainerCustomer_' + id).removeClass('customerDetails').slideUp();
                else
                    $('#ContentPlaceHolder1_divContainerCustomer_' + id).addClass('customerDetails').slideDown();

            });


        });
    </script>

    <script type="text/javascript">
        $(document).kendoTooltip({
            filter: ".k-icon k-i-window-maximize",
            content: function (e) {
                return "Maximize";
            }
        });
    </script>

    <style type="text/css">
        .bg-aqua {
            background-color: #00c0ef !important;
            color: #fff !important;
        }

        .bg-green {
            background-color: #00a65a !important;
        }

        .bg-red, .bg-yellow, .bg-aqua, .bg-blue, .bg-light-blue, .bg-green, .bg-navy, .bg-teal, .bg-olive, .bg-lime, .bg-orange, .bg-fuchsia, .bg-purple, .bg-maroon, .bg-black, .bg-red-active, .bg-yellow-active, .bg-aqua-active, .bg-blue-active, .bg-light-blue-active, .bg-green-active, .bg-navy-active, .bg-teal-active, .bg-olive-active, .bg-lime-active, .bg-orange-active, .bg-fuchsia-active, .bg-purple-active, .bg-maroon-active, .bg-black-active {
            color: #fff !important;
        }

        .bg-yellow {
            background-color: #f39c12 !important;
        }

        :after, :before {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        :after, :before {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .icon {
            -webkit-transition: all .3s linear;
            -o-transition: all .3s linear;
            transition: all .3s linear;
            position: absolute;
            top: -10px;
            right: 10px;
            z-index: 0;
            font-size: 96px;
            color: rgba(0,0,0,0.15);
        }

        .divCount:hover {
            /*color: #FF7473;*/
            font-weight: 500;
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
        }

        .entity-bg {
            color: #fff;
            background: #2b80b7;
        }

        .compliances-bg {
            color: #fff;
            background: #64B973;
        }

        .dueToday-bg {
            color: #fff;
            background: #8F45AD;
        }

        .upcoming-bg {
            color: #fff;
            background: #004586;
        }

        .overdue-bg {
            color: #fff;
            background: #FF7473;
            /*#FF0000;*/
        }

        @media (min-width: 768px) {
            .five-cols .col-md-1,
            .five-cols .col-sm-1,
            .five-cols .col-lg-1 {
                width: 100%;
            }
        }

        @media (min-width: 992px) {
            .five-cols .col-md-1,
            .five-cols .col-sm-1,
            .five-cols .col-lg-1 {
                width: 20%;
            }
        }

        @media (min-width: 1200px) {
            .five-cols .col-md-1,
            .five-cols .col-sm-1,
            .five-cols .col-lg-1 {
                width: 20%;
            }
        }

        #max:hover {
            z-index: 10;
        }

        .closeButton:hover, .maxButton:hover {
            -moz-box-shadow: 0 0 0 15px #f4f4f4;
            -webkit-box-shadow: 0 0 0 15px #f4f4f4;
            box-shadow: 0 0 0 15px #f4f4f4;
            -moz-transition: -moz-box-shadow .2s;
            -webkit-transition: -webkit-box-shadow .2s;
            transition: box-shadow .2s;
        }
    </style>

    <style>
        .mr10 {
            margin-left: 15px;
        }

        .ml5 {
            margin-left: 10px;
        }

        .panel-wrap {
            display: table;
            /* width: 968px;
                  background-color: #f5f5f5;
                    border: 1px solid #e5e5e5;*/
        }

        .widget.placeholder {
            opacity: 0.4;
            border: 1px dashed #a6a6a6;
        }

        /* WIDGETS */
        .widget {
            margin-left: 15px;
            margin-bottom: 15px;
            /*margin: 0 0 20px;*/
            padding: 0;
            background-color: #ffffff;
            /*border: 1px solid #e7e7e7;*/
            border-radius: 3px;
            cursor: pointer;
        }

            /*.widget:hover {
            background-color: #fcfcfc;
            border-color: #cccccc;
        }*/

            .widget div {
                /*padding: 10px;
                min-height: 50px;*/
            }

            .widget h3 {
                margin-top: 0px;
                margin-bottom: 0px;
                font-size: 17px;
                padding: 10px 10px;
                /*text-transform: uppercase;*/
                /*border-bottom: 1px solid #e7e7e7;*/
            }

                .widget h3 span {
                    float: right;
                }

                    .widget h3 span:hover {
                        cursor: pointer;
                        background-color: #e7e7e7;
                        border-radius: 20px;
                    }

        /* PROFILE */
        .profile-photo {
            /*width: 80px;
                    height: 80px;*/
            /*margin: 10px auto;*/
            border-radius: 100px;
            border: 1px solid #e7e7e7;
            background: url('../content/web/Customers/ISLAT.jpg') no-repeat 50% 50%;
        }

        .hint {
            width: 250px;
            height: 100px;
            overflow: hidden;
        }

            .hint > h3 {
                padding-left: 20px;
            }

        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            cursor: not-allowed;
            background-color: #ffffff;
            color: #000000;
        }

        .select_Date {
            width: 100%;
            margin-right: 0;
        }

        .icongcalender {
            color: #666;
        }

        .m10 {
            margin-left: 10px;
        }

        .k-icon-15 {
            font-size: 15px; /* Sets icon size to 32px */
        }

        imgCentered {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
    </style>

    <style type="text/css">
        b, strong {
            font-weight: 400;
        }

        div.k-window {
            z-index: 27;
        }

        .tile_count {
        }

        *, :after, :before {
            box-sizing: border-box;
        }

        .tile_count .tile_stats_count {
            padding: 0px 0px 0px 20px;
            position: relative;
        }

        .tile_count .tile_stats_count, ul.quick-list li {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .body {
            color: #eee;
        }

        *, :after, :before {
            box-sizing: border-box;
        }

        #dailyupdates .bx-viewport {
            height: auto !important;
        }

        .function-selector-radio-label {
            font-size: 12px;
        }

        .colorPickerWidget {
            padding: 10px;
            margin: 10px;
            text-align: center;
            width: 360px;
            border-radius: 5px;
            background: #fafafa;
            border: 2px solid #ddd;
        }

        .TMB {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 20% !important;
        }

        .TMBImg {
            padding-left: 0px !important;
            padding-right: 0px !important;
            text-align: center;
            /*margin-left: -7%;
                margin-right: -3%;*/
        }

        .clscircle {
            margin-right: 7px !important;
        }

        .fixwidth {
            width: 20% !important;
        }

        .badge {
            font-size: 10px !important;
            font-weight: 200 !important;
        }

        .days > div.day {
            margin: 1px;
            background: #eee;
        }

        .overdue ~ div > a {
            background: red;
        }

        .info-box {
            min-height: 65px !important;
            /*margin-left: 10px;*/
            border: #dee2e6 solid 1px;
            margin-bottom: 10px;
        }

            .info-box .titleMD {
                font-size: 18px;
            }

            .info-box .countMD {
                font-size: 18px;
            }

        .top-box:hover {
            font-weight: 500;
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            z-index: 5;
        }

        .div-location {
            min-height: 52px;
            padding-top: 0px;
        }

        .Dashboard-white-widget {
            padding: 5px 10px 0px !important;
        }

        .TMBImg > img {
            width: 37px;
        }

        div.panel {
            margin-bottom: 12px;
        }

        .panel .panel-heading .panel-actions {
            height: 25px !important;
        }

            .panel .panel-heading .panel-actions a {
                border: none;
            }

        hr {
            margin-bottom: 8px;
        }

        .panel .panel-heading h2 {
            font-size: 18px;
        }

        span.input-group-addon {
            padding: 0px;
        }

        td > label {
            padding: 3px 4px 0 4px;
            margin-top: -1%;
            padding: 6px;
        }

        .nav-tabs > li > a {
            color: #333 !important;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color: #1fd9e1 !important;
        }
    </style>

    <style type="text/css">
        .m-b-n-2 {
            /*/ margin-bottom: -13px;*/
        }

        .m-r {
            margin-right: 15px;
        }

        .m-l {
            margin-left: 10px;
        }

        .b-info {
            border-color: #23b7e5;
        }

        .b-l {
            border-left: 1px solid #23b7e5;
        }

        .m-10 {
            margin-top: 10px;
            margin-left: 10px;
        }

        .count-number {
            font-size: 36px;
            font-weight: bold;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .count:hover {
            cursor: pointer;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('#divFloaterChange').parent().toggleClass('floater-active');
            $('#divFloaterChange').click(function () {
                $('#divThemeChange').parent().removeClass('theme-active');
                $('#divFloaterChange').parent().toggleClass('floater-active');
            });
        });

        $(document).ready(function () {
            $('.fromMonthPicker').datepicker({
                autoclose: true,
                minViewMode: 1,
                format: 'M-yyyy'
            }).on('changeDate', function (selected) {
                startDate = new Date(selected.date.valueOf());
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                $('.toMonthPicker').datepicker('setStartDate', startDate);
            });

            $('.toMonthPicker').datepicker({
                autoclose: true,
                minViewMode: 1,
                format: 'M-yyyy'
            }).on('changeDate', function (selected) {
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                $('.fromMonthPicker').datepicker('setEndDate', FromEndDate);
            });
        });

        function checkAllPeriods(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkPeriod") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckPeriodHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkPeriod']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkPeriod']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='PeriodSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {
                rowCheckBoxHeader[0].checked = false;
            }
            //$('#BodyContent_udcInputForm_txtReturnList').val(rowCheckBoxSelected.length + ' ' + 'item selected');
        }

        function initializeJQueryUIDeptDDL() {
            debugger;

            $("#<%= txtPeriodList.ClientID %>").unbind('click');
            $("#<%= txtPeriodList.ClientID %>").click(function () {
                $("#dvPeriod").toggle("blind", null, 100, function () { });
            });
        }

    </script>

    <style>
        .dropdown-menu.extended {
            max-width: 700px !important;
            min-width: 700px !important;
        }

        div > p {
            color: black;
        }

        .clear, .text-ellipsis {
            display: block;
            overflow: hidden;
        }

        .chosen-container-single .chosen-single {
            box-shadow: none !important;
        }

        .chosen-results {
            max-height: 100px !important;
        }
    </style>

    <style>
        div > p {
            color: black;
        }

        .clear, .text-ellipsis {
            display: block;
            overflow: hidden;
        }

        .marq {
            margin-left: 15px;
            margin-right: 30px;
        }

        div.k-window-content {
            overflow: hidden;
        }

        .overlay {
            opacity: 0;
            filter: alpha(opacity = 0);
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            display: block;
            z-index: 2;
            background: transparent;
        }

        .table > tbody > tr > th, .table > tbody > tr > th > a {
            vertical-align: bottom;
        }

        .panel-heading .nav .active {
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- container section start -->
    <section id="container-fluid" class="">
        <section class="">
            <div class="clear"></div>

            <!-- Top Count start -->
            <div id="divTabs" runat="server" class="row col-md-12 tile_count">
                <div id="entitycountDivBox" runat="server" class="five-cols col-md-1 TMB">
                    <div class="info-box top-box bluenew-bg">
                       <div class="div-location">
                            <div class="col-md-2 TMBImg">
                                <img src="../Images/Location-new.png" />
                            </div>
                            <div class="col-md-10 colpadding0">
                                <div class="titleMD">Customer</div>
                                <div id="divCustomerCount" runat="server" class="countMD">0</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="compliancecountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                    <div class="info-box top-box compliances-bg">
                        <div class="div-location" onclick="fCompliances(<%=customerID%>)">
                            <div class="col-md-2 TMBImg">
                                <img src="../Images/Compliances-new.png" />
                            </div>
                            <div class="col-md-10 colpadding0" style="margin-left: -4%;">
                                <div class="titleMD">Location</div>
                                <div id="divLocationCount" runat="server" class="countMD">0</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="duetodayCountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                    <div class="info-box top-box dueToday-bg">
                        <div class="div-location" style="cursor: pointer" onclick="ShowMyCompliances('All','U','-1','-1','','','','','')">
                            <div class="col-md-2 TMBImg">
                                <img src="../Images/Due Today.png" />
                            </div>
                            <div class="col-md-10 colpadding0">
                                <div class="titleMD">Upcoming</div>
                                <div id="divUpcomingCount" runat="server" class="countMD">0</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="upcomingCountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                    <div class="info-box top-box overdue-bg">
                        <div class="div-location" style="cursor: pointer" onclick="ShowMyCompliances('All','O','-1','-1','','','','','')">
                            <div class="col-md-2 TMBImg">
                                <img src="../Images/Upcoming-new.png" />
                            </div>
                            <div class="col-md-10 colpadding0">
                                <div class="titleMD">Overdue</div>
                                <div id="divOverdueCount" runat="server" class="countMD">0</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="OverdueCountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                    <div class="info-box top-box upcoming-bg">
                        <div class="div-location" style="cursor: pointer" onclick="ShowMyCompliances('All','PR','-1','-1','','','','','')">
                            <div class="col-md-2 TMBImg">
                                <img src="../Images/Overdue-new.png" />
                            </div>
                            <div class="col-md-10 colpadding0">
                                <div class="titleMD">Pending Review</div>
                                <div id="divPendingForReviewCount" runat="server" class="countMD">0</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Top Count End -->

            <header class="panel-heading tab-bg-primary" style="background: transparent;">
                <ul id="rblDashboard" class="nav nav-tabs">
                    <li class="" id="liCustomerWise" runat="server" style="padding-left: 5px; padding-right: 6px; margin-right: 6px!important;">
                        <asp:LinkButton ID="lnkShowCustomerWiseDashboard" runat="server" Style="font-size: 16px; line-height: 1.0;" OnClick="lnkShowCustomerWiseDashboard_Click">
                            <i class="fa fa-users" style="padding-left: 0.5em; padding-right: 0.5em;"></i>Customer Wise</asp:LinkButton>
                    </li>
                    <li class="active" id="liSPOCWise" runat="server" style="padding-left: 5px; padding-right: 6px;">
                        <asp:LinkButton ID="lnkSPOCWiseDashboard" runat="server" Style="font-size: 16px; line-height: 1.0;" OnClick="lnkSPOCWiseDashboard_Click">
                            <i class="fa fa-sitemap" style="padding-left: 0.5em; padding-right: 0.5em;"></i>SPOC Wise</asp:LinkButton>
                    </li>
                </ul>
            </header>

            <div class="clearfix"></div>

            <!-- Filter start -->
            <div class="row widget Dashboard-white-widget" style="width: 98%">
                <div class="panel panel-default">
                    <div class="panel-heading" style="margin-bottom: 5px;" data-toggle="collapse" data-parent="#accordion" href="#collapseDivFilters">
                        <a>
                            <h2><b>Filters</b></h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivFilters">
                                <i class="fa fa-chevron-down"></i></a>
                        </div>
                    </div>

                    <div id="collapseDivFilters" class="panel-collapse collapse in">
                        <div class="col-md-12 colpadding0">
                            <div id="divFilterCustomer" runat="server" class="col-md-1" style="padding-left: 0px; margin-bottom: 15px; width: 20%">
                                <asp:DropDownListChosen ID="ddlCustomers" CssClass="form-control" runat="server" Width="100%" AllowSingleDeselect="false"></asp:DropDownListChosen>
                            </div>

                            <div id="divFilterSPOC" runat="server" class="col-md-1" style="padding-left: 0px; margin-bottom: 15px; width: 20%">
                                <asp:DropDownListChosen ID="ddlSPOCs" CssClass="form-control" runat="server" Width="100%" AllowSingleDeselect="false"></asp:DropDownListChosen>
                            </div>

                            <div class="col-md-1 colpadding0" style="width: 25%">
                                <div class="col-md-6 input-group date" style="padding-left: 0px;">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" style="padding: 6px !important; color: black;"></span>
                                    </span>
                                    <asp:TextBox runat="server" Height="32px" placeholder="Start Month" Style="padding-left: 5px;" class="form-control text-center fromMonthPicker" ID="txtStartPeriod" autocomplete="off" />
                                </div>
                                <div class="col-md-6 input-group date" style="padding-left: 0px;">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" style="padding: 6px !important; color: black;"></span>
                                    </span>
                                    <asp:TextBox runat="server" Height="32px" placeholder="End Month" Style="padding-left: 5px; margin-left: 0px;" class="form-control text-center toMonthPicker" ID="txtEndPeriod" autocomplete="off" />
                                </div>
                            </div>

                            <asp:UpdatePanel ID="upFreqFilter" runat="server" OnLoad="upFreqFilter_Load">
                                <ContentTemplate>
                                    <div class="col-md-1 col-sm-1" style="padding-left: 0px; width: 15%">
                                        <asp:DropDownListChosen ID="ddlFrequency" runat="server" Width="100%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged">
                                            <asp:ListItem Text="Frequency" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Monthly" Value="Monthly"></asp:ListItem>
                                            <asp:ListItem Text="Quarterly" Value="Quarterly"></asp:ListItem>
                                            <asp:ListItem Text="Half Yearly" Value="Half Yearly"></asp:ListItem>
                                            <asp:ListItem Text="Annual" Value="Annual"></asp:ListItem>
                                            <asp:ListItem Text="BiAnnual" Value="BiAnnual"></asp:ListItem>
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-1 col-sm-1" style="padding-left: 0px; width: 10%">
                                        <asp:TextBox runat="server" ID="txtPeriodList" placeholder="Period" Style="padding: 10px; margin: 0px; height: 32px; width: 100%;" CssClass="form-control" />
                                        <div style="display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width: auto;" id="dvPeriod" class="dvDeptHideshow form-control">
                                            <asp:Repeater ID="rptPeriodList" runat="server">
                                                <HeaderTemplate>
                                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                        <tr>
                                                            <td style="width: 100px;" colspan="2">
                                                                <asp:CheckBox ID="PeriodSelectAll" Text="Select All" runat="server" onclick="checkAllPeriods(this)" />
                                                            </td>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 20px;">
                                                            <asp:CheckBox ID="chkPeriod" runat="server" onclick="UncheckPeriodHeader();" /></td>
                                                        <td style="width: 200px;">
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 118px; padding-bottom: 5px;">
                                                                <asp:Label ID="lblPeriodID" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                <asp:Label ID="lblPeriodName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                           
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>

                                    <div class="col-md-1 col-sm-1" style="padding-left: 0px; width: 10%">
                                        <asp:DropDownListChosen ID="ddlYear" runat="server" Width="100%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select"
                                            AutoPostBack="true">
                                        </asp:DropDownListChosen>
                                    </div>
                                </ContentTemplate>                               
                            </asp:UpdatePanel>

                            <div class="col-md-1 colpadding0 text-right" style="width: 5%">
                                <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-primary" font-size="14px" style="font-weight:400" runat ="server" Text="Apply" OnClick="btnSearch_Click" />
                            </div>

                            <div class="col-md-1 col-sm-1 colpadding0 text-center" style="width: 5%; margin-bottom: 5px; margin-left:5px;">
                                <asp:LinkButton runat="server" CausesValidation="false" class="btn btn-primary" style="height:33px;margin-left:13px;"  OnClick="btnClear_Click" ><span aria-hidden="true" class="k-icon k-i-filter-clear"> </span> <font size="2" style="zoom:1.1"><b> Clear Filter</b></font></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Filter End -->

            <div class="clearfix"></div>

            <asp:MultiView ID="MainView" runat="server">
                <asp:View ID="CustomerWiseView" runat="server">
                    <div id="divContainer_CustomerWiseSummary" class="col-md-12 colpadding0" runat="server"></div>
                </asp:View>
                <asp:View ID="SPOCWiseView" runat="server">
                    <div id="divContainer_SPOCWiseSummary" class="col-md-12 colpadding0" runat="server"></div>
                </asp:View>
            </asp:MultiView>

            <div id="divMyCompliances" style="height: 95%"></div>
           
            <div id="RLCS_RCAnchoreList" title="" style="display: block; height: 410px; max-height: 510px">
                <iframe id="iFrameDialog" src="about:blank" width="98%" height="100%" style="border: none;margin-left:16px;"></iframe>
            </div>
            
        </section>
    </section>

    <asp:HiddenField ID="hidposition" runat="server" Value="" />

    <script type="text/javascript">
        function fposition() {
            $('#<%=hidposition.ClientID%>').val(document.body.scrollTop);
        }

        function fsetscroll() {
            document.body.scrollTop = $('#<%=hidposition.ClientID%>').val();
        }
    </script>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                <%=CustomerWiseComplianceStatusChart%>
            });

            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                <%=SPOCWiseComplianceStatusChart%>
            });
        });
    </script>
    
     <script type="text/javascript">
         ////New Add RLCS List
         $(document).ready(function () {
             var RLCS_WEB_URL='<% =RLCS_WEB_URL%>';
            $('#RLCS_RCAnchoreList').hide();
            debugger;
            if (<% =serviceproviderid%>==94) {
                $('#RLCS_RCAnchoreList').show();
                setTimeout(
                    $('#iFrameDialog').attr('src', RLCS_WEB_URL+"Home/Index?AvacomUserId=" + '<% =Encrypted_userID%>')
                    , 20000);
            }
        });
    </script>

</asp:Content>

