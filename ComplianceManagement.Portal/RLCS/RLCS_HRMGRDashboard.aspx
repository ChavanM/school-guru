﻿<%@ Page Title="My Dashboard" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_HRMGRDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_HRMGRDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="AVANTIS - Development Team" />

    <title>My Dashboard :: AVANTIS - Products that simplify</title>

    <!-- Offline-->
    <script type="text/javascript" src="../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/drilldown.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/exporting.js"></script>
    <script type="text/javascript" src="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <link href="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../newcss/spectrum.css" rel="stylesheet" />
    <script type="text/javascript" src="../newjs/spectrum.js"></script>

    <script type="text/javascript" src="../newjs/ammap.js"></script>
    <script type="text/javascript" src="../newjs/indiaLow.js"></script>

    <script type="text/javascript" src="https://www.amcharts.com/lib/3/ammap.js"></script>
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/maps/js/indiaLow.js"></script>
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>

    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

    <link href="/NewCSS/margin-padding.css" rel="stylesheet" />

    <style type="text/css">
        .m-b-n-2 {
            margin-bottom: -13px;
        }

        .m-r {
            margin-right: 15px;
        }

        .m-l {
            margin-left: 15px;
        }

        .b-info {
            border-color: #1fd9e1;
        }

        .b-l {
            border-left: 1px solid #dee5e7;
        }

        .text-sm {
            font-size: 13px;
        }

        .streamline {
            position: relative;
            border-color: transparent;
        }

            .streamline .sl-item:after, .streamline:after {
                position: absolute;
                bottom: 0;
                left: 0;
                width: 10px;
                height: 10px;
                margin-left: -5px;
                background-color: #fff;
                border-color: inherit;
                border-style: solid;
                border-width: 1px;
                border-radius: 10px;
                content: '';
            }

        .i-switch, .sl-item, .tl-content, .tl-date, .tl-wrap:before {
            position: relative;
        }

        .sl-item {
            padding-bottom: 10px;
        }

            .sl-item:after, .sl-item:before {
                display: table;
                content: " ";
            }

            .sl-item:after {
                clear: both;
                top: 6px;
                bottom: auto;
            }

            .sl-item.b-l {
                margin-left: 10px;
            }
    </style>
    
    <style type="text/css">
        #dailyupdates .bx-viewport {
            height: 190px !important;
        }

        .info-box:hover {
            color: #FF7473;
            font-weight: 500;
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            z-index: 5;
        }

        .function-selector-radio-label {
            font-size: 12px;
        }

        .colorPickerWidget {
            padding: 10px;
            margin: 10px;
            text-align: center;
            width: 360px;
            border-radius: 5px;
            background: #fafafa;
            border: 2px solid #ddd;
        }

        #ContentPlaceHolder1_grdGradingRepportSummary.table tr td {
            border: 1px solid white;
        }

        .TMB {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 20% !important;
        }

        .TMB1 {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 19.5% !important;
        }

        .TMBImg {
            padding-left: 0px !important;
            padding-right: 0px !important;
            margin-left: -7%;
            margin-right: -3%;
        }

        .clscircle {
            margin-right: 7px !important;
        }

        .fixwidth {
            width: 20% !important;
        }

        .badge {
            font-size: 10px !important;
            font-weight: 200 !important;
        }

        .responsive-calendar .day {
            width: 13.7% !important;
            height: 45px;
        }

            .responsive-calendar .day.cal-header {
                border-bottom: none !important;
                width: 13.9% !important;
                font-size: 17px;
                height: 25px;
            }

        #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        .bx-viewport {
            height: 285px !important;
        }

        #dailyupdates .bx-viewport {
            height: 190px !important;
        }

        .graphcmp {
            margin-left: 36%;
            font-size: 16px;
            margin-top: -3%;
            color: #666666;
            font-family: 'Roboto';
        }

        .days > div.day {
            margin: 1px;
            background: #eee;
        }

        .overdue ~ div > a {
            background: red;
        }

        .info-box {
            min-height: 95px !important;
        }

        .Dashboard-white-widget {
            padding: 5px 10px 0px !important;
        }

        .dashboardProgressbar {
            display: none;
        }

        .TMBImg > img {
            width: 47px;
        }

        #reviewersummary {
            height: 150px;
        }

        #performersummary {
            height: 150px;
        }

        #eventownersummary {
            height: 150px;
        }

        #performersummarytask {
            height: 150px;
        }

        #reviewersummarytask {
            height: 150px;
        }

        div.panel {
            margin-bottom: 12px;
        }

        .panel .panel-heading .panel-actions {
            height: 25px !important;
        }

        hr {
            margin-bottom: 8px;
        }

        .panel .panel-heading h2 {
            font-size: 18px;
        }

        td > label {
            padding: 6px;
        }

        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 20px;
        }

        span.input-group-addon {
            padding: 0px;
        }

        td > label {
            padding: 3px 4px 0 4px;
            margin-top: -1%;
        }

        .nav-tabs > li > a {
            color: #333 !important;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color: #1fd9e1 !important;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');

            binddailyupdatedata();
            bindnewsdata();

            BindNewNotifications(); 
            BindApplicableActs();
            //ShowComplianceMapChart(<%= data_areas%>);            

            $("#divApplicableActs").scroll(function(){
                $("#divApplicableActs").getNiceScroll().resize();
            });
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

    </script>

    <script type="text/javascript">
        var dailyupadatepageno = 1;
        var Newslettersrowpageno = 1;
        function fdailyclick() { dailyupadatepageno += 1; binddailyupdatedata(); }
        function fnewsclick() { Newslettersrowpageno += 1; bindnewsdata(); }
        var dailyupdateJson;
        var NewsLetterJson;

        function bindnewsdata() {
            var k = 0;
            $.ajax({
                async: true,
                type: 'Get',
                url: 'https://login.avantis.co.in/apadr/Data/GetNewsletterData?page=' + Newslettersrowpageno,
                content: 'application/json;charset=utf-8',
                processData: true,
                headers: {
                    "Authorization": "Bearer"
                },
                success: function (result) {
                    NewsLetterJson = result;
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.length; step++) {
                        var objDate = new Date(result[step].NewsDate);
                        str += '<li><img style="width:250px;height:166px" src="../NewsLetterImages/' + result[step].FileName + '" /><h3>' + result[step].Title + '</h3><p><a class="view-pdf" data-toggle="modal" style="cursor:pointer" onclick="viewpdf(this)" data-title="' + result[step].Title + '"  data-href="../NewsLetterImages/' + result[step].DocFileName + '"> Issue ' + getmonths(objDate) + ' ' + objDate.getFullYear() + '</a></p></li>'
                    }
                    $("#Newslettersrow").html(str);
                    var ker = $('.bxnews').bxSlider({
                        minSlides: 3,
                        maxSlides: 3,
                        slideWidth: 250,
                        slideMargin: 50,
                        moveSlides: 1,
                        auto: false,
                        nextCss: 'newsletternext',
                        nextclickFunction: 'fnewsclick()',
                    });
                },
                error: function (e, t) { }
            })
        }

        function binddailyupdatedata() {
            var k = 0;
            $.ajax({
                async: true,
                type: 'Get',
                url: "https://login.avantis.co.in/apadr/Data/GetDailyupdateData?page=" + dailyupadatepageno + "&searchKey=",
                content: 'application/json;charset=utf-8',
                processData: true,
                headers: {
                    "Authorization": "Bearer"
                },
                success: function (result) {
                    dailyupdateJson = result;
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.length; step++) {
                        var titledaily = '';
                        var Descriptiondaily = '';
                        if (result[step].Title.length > 150) {
                            titledaily = result[step].Title.substring(0, 150) + '...'
                        } else {
                            titledaily = result[step].Title;
                        }
                        if (result[step].Description.length > 150) {
                            if (result[step].Description.indexOf('<div') == -1 || result[step].Description.indexOf('<span') == -1) {
                                Descriptiondaily = result[step].Description.substring(0, result[step].Description) + '...'
                            }
                        } else {
                            if (result[step].Description.indexOf('<div') == -1 || result[step].Description.indexOf('<span') == -1) {
                                Descriptiondaily = result[step].Description;
                            }
                        }
                        str += '<li><h3 style="height: 70px;"> ' + titledaily + ' </h3> <p><br />   <a data-toggle="modal" onclick="Binddailyupdatepopup(' + result[step].ID + ');" href="#NewsModal">Read More</a></p></li>'
                    }
                    $("#DailyUpdatesrow").append(str);
                    var ite = $('.bxdaily').bxSlider({
                        minSlides: 3,
                        maxSlides: 3,
                        slideWidth: 250,
                        slideMargin: 50,
                        moveSlides: 1,
                        auto: false,
                        nextCss: 'dailyupdatenext',
                        nextclickFunction: 'fdailyclick()',
                    });

                },
                error: function (e, t) { }
            })
        }

        function timeconvert(ds) {
            var D, dtime, T, tz, off,
            dobj = ds.match(/(\d+)|([+-])|(\d{4})/g);
            T = parseInt(dobj[0]);
            tz = dobj[1];
            off = dobj[2];
            if (off) {
                off = (parseInt(off.substring(0, 2), 10) * 3600000) +
                 (parseInt(off.substring(2), 10) * 60000);
                if (tz == '-') off *= -1;
            }
            else off = 0;
            return new Date(T += off).toUTCString();
        }

        function getmonths(mon) {
            try {
                return mon.toString().split(' ')[1];
            } catch (e) { }
        }

        function Binddailyupdatepopup(DID) {
            for (var i = 0; i < dailyupdateJson.length; i++) {
                if (DID == dailyupdateJson[i].ID) {
                    var objDate = new Date(timeconvert(dailyupdateJson[i].CreatedDate));
                    if (timeconvert(dailyupdateJson[i].CreatedDate) == "Invalid Date") {
                        objDate = new Date(dailyupdateJson[i].CreatedDate);
                    }
                    var mon_I = getmonths(objDate);
                    $("#dailyupdatedate").html('Daily Updates:' + mon_I + ' ' + objDate.getDate() + ', ' + objDate.getFullYear());
                    $("#dailyupdatedateInner").html('Daily Updates:' + mon_I + ' ' + objDate.getDate());
                    $("#contents").html(dailyupdateJson[i].Description);
                    $("#dailytitle").html(dailyupdateJson[i].Title);
                    break;
                }
            }

        }

        function BindNewsLetterpopup(DID) {
            for (var i = 0; i < NewsLetterJson.length; i++) {
                if (DID == NewsLetterJson[i].ID) {

                    $("#newsImg").attr('src', '../Images/' + NewsLetterJson[i].FileName);
                    $("#newsTitle").html(NewsLetterJson[i].Title);
                    $("#newsDesc").html(NewsLetterJson[i].Description);

                    break;
                }
            }
        }

        function viewpdf(obj) {
            var pdf_link = $(obj).attr('data-href');
            var pdf_title = $(obj).attr('data-title');
            var iframe = '<object type="application/pdf" data="' + pdf_link + '"#toolbar=1&navpanes=0&statusbar=0&messages=0 width="100%" height="500">No Support</object>'
            $.createModal({
                title: pdf_title,
                message: iframe,
                closeButton: true,
                scrollable: false
            });
            return false;
        }

        function BindNewNotifications() {
            var k = 0;
            $.ajax({
                async: true,
                type: 'Get',
                url: '/dailyupdateservice.svc/BindNotifications?userid=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>',
                content: 'application/json;charset=utf-8',
                processData: true,
                success: function (result) {
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.length; step++) {

                        str += '<li><p><a href="../DailyUpdates/Notification.aspx">' + result[step] + '</a></p></li>'
                    }
                    $("#divNotificationData").html(str);

                },
                error: function (e, t) { }
            });
        }

         function BindApplicableActs() {                                      
             var divApplicableActs = document.getElementById("divApplicableActs");
             divApplicableActs
             if(divApplicableActs!=null &&  divApplicableActs!=undefined){
                 $.ajax({
                     type: "POST",
                     url: "/RLCS/RLCS_HRMGRDashboard.aspx/GetApplicableActs",
                     data: '{}',
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (response) {                     
                         $.each(response, function () {
                             var hml="";
                             for (var i = 0; i < response.d.length; i++) {
                                 debugger;
                                 hml+="<div class='sl-item b-info b-l'> <div class='m-l text-sm'>"+ response.d[i].ActName +"</div></div>";                             
                             }
                             $(divApplicableActs).html(hml);
                         });                     
                     },
                     failure: function (response) {
                     },
                     error: function (response) {
                     }
                 });
             }           
        }
    </script>

    <script type="text/javascript">
        
        function closeDiv(id) {
            document.getElementById(id + 'Checkbox').className = 'menucheckbox-notchecked';
            document.getElementById(id).style.display = 'none';
            // do something
        }

        function showDiv(id) {
            if (document.getElementById(id).style.display != 'none') {
                document.getElementById(id + 'Checkbox').className = 'menucheckbox-notchecked';
                document.getElementById(id).style.display = 'none';
            }
            else {
                //if (document.getElementById(id + 'Checkbox').className == 'menucheckbox-checked') {
                document.getElementById(id).style.display = 'block';
                document.getElementById(id + 'Checkbox').className = 'menucheckbox-checked';

                // }
            }
        }
    </script>

    <script type="text/javascript">
        //<!-- Graph-Monthly Compliance-Column Chart -->
        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });
                
                var DeptWiseColumnChart = Highcharts.chart('divMonthlyComplianceColumnChart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },

                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },

                    yAxis: {
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false,
                        //enabled: true,
                        //itemDistance: 0,
                        //itemStyle: {
                        //    textshadow: false,
                        //},
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y}',
                                style: {
                                    textShadow: false,
                                    // color:'red',
                                }
                            },
                        },
                    },
                    legend: {
                        itemDistance: 2,
                    },

                    tooltip: {
                        //headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                    },

                    series: [ <%=seriesData_GraphMonthlyComplianceColumn%>]
                });
            });
        });
        //<!-- Graph-Monthly Compliance-Column Chart END-->
    </script>

    <script type="text/javascript">
        //<!-- Graph-Monthly Compliance-Pie Chart 
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divMonthlyCompliancePieChart',
                        type: 'pie',
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: '<%=complianceSummaryMonth%>' ,                   
                        style: {
                            fontWeight: '300',
                            fontSize:'15px'
                        }
                    },
                    plotOptions: {
                        pie: {
                            //innerSize: '40%',
                            dataLabels: { enabled: true, format: '{y}', distance: 2 }
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=seriesData_GraphMonthlyCompliancePie%>
                        ],
                        size: '100%',
                        showInLegend: true,
                    }]
                },

                function (chart) { // on complete
                    var textX = chart.plotLeft + (chart.plotWidth * 0.5);
                    var textY = chart.plotTop + (chart.plotHeight * 0.5);

                    var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
                    span += '<span style="font-size: 20px;font-family: sans-serif;"> </span><br>';
                    span += '<span style="font-size: 16px">1000</span>';
                    span += '</span>';

                    $("#addText").append(span);
                    span = $('#pieChartInfoText');
                    span.css('left', textX + (span.width() * -0.5));
                    span.css('top', textY + (span.height() * -0.5));
                });
            });
        });
        //Graph-Monthly Compliance-Pie Chart-END 
    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            $(function () {        
                debugger;
                //India  Map 
                AmCharts.makeChart("divMapPanIndiaCompliance", {
                    "type": "map",
                    "addClassNames": true,
                    "fontSize": 11,
                    "color": "#000000",
                    "hideCredits":true,

                    "projection": "miller",
                    "backgroundAlpha": 1,
                    "backgroundColor": "transparent",
                    "dataProvider": {
                        "map": "indiaLow",
                        "getAreasFromMap": true,
                        "areas": <%=data_areas%> 
                        },
                    "balloon": {
                        "horizontalPadding": 5,
                        "borderAlpha": 0,
                        "borderThickness": 1,
                        "verticalPadding": 5
                    },
                    "areasSettings": {
                        "color": "#cfd6d8",
                        "outlineColor": "rgba(255,255,255,1)",
                        "rollOverOutlineColor": "rgba(255,255,255,1)",
                        "rollOverBrightness": 20,
                        "selectedBrightness": 20,
                        "selectable": true,
                        "unlistedAreasAlpha": 0,
                        "unlistedAreasOutlineAlpha": 0
                    },
                    "imagesSettings": {
                        "alpha": 1,
                        "color": "rgba(163,194,232,1)",
                        "outlineAlpha": 0,
                        "rollOverOutlineAlpha": 0,
                        "outlineColor": "rgba(255,255,255,1)",
                        "rollOverBrightness": 20,
                        "selectedBrightness": 20,
                        "selectable": true
                    },
                    "linesSettings": {
                        "color": "rgba(163,194,232,1)",
                        "selectable": true,
                        "rollOverBrightness": 20,
                        "selectedBrightness": 20
                    },
                    "zoomControl": {
                        "zoomControlEnabled": true,
                        "homeButtonEnabled": false,
                        "panControlEnabled": false,
                        "right": 5,
                        "bottom": 20,
                        "minZoomLevel": 0.25,
                        "gridHeight": 100,
                        "gridAlpha": 0.1,
                        "gridBackgroundAlpha": 0,
                        "gridColor": "#19a9d5",
                        "draggerAlpha": 1,
                        "buttonCornerRadius": 2
                    }
                });
            });
        });
    </script>

     <%-- <script type="text/javascript">        
          function ShowComplianceMapChart(data_areas) {        
                debugger;
                //India  Map 
                AmCharts.makeChart("divMapPanIndiaCompliance", {
                    "type": "map",
                    "addClassNames": true,
                    "fontSize": 11,
                    "color": "#000000",

                    "projection": "miller",
                    "backgroundAlpha": 1,
                    "backgroundColor": "transparent",
                    "dataProvider": {
                        "map": "indiaLow",
                        "getAreasFromMap": true,
                        "areas": data_areas
                        },
                    "balloon": {
                        "horizontalPadding": 5,
                        "borderAlpha": 0,
                        "borderThickness": 1,
                        "verticalPadding": 5
                    },
                    "areasSettings": {
                        "color": "#cfd6d8",
                        "outlineColor": "rgba(255,255,255,1)",
                        "rollOverOutlineColor": "rgba(255,255,255,1)",
                        "rollOverBrightness": 20,
                        "selectedBrightness": 20,
                        "selectable": true,
                        "unlistedAreasAlpha": 0,
                        "unlistedAreasOutlineAlpha": 0
                    },
                    "imagesSettings": {
                        "alpha": 1,
                        "color": "rgba(163,194,232,1)",
                        "outlineAlpha": 0,
                        "rollOverOutlineAlpha": 0,
                        "outlineColor": "rgba(255,255,255,1)",
                        "rollOverBrightness": 20,
                        "selectedBrightness": 20,
                        "selectable": true
                    },
                    "linesSettings": {
                        "color": "rgba(163,194,232,1)",
                        "selectable": true,
                        "rollOverBrightness": 20,
                        "selectedBrightness": 20
                    },
                    "zoomControl": {
                        "zoomControlEnabled": true,
                        "homeButtonEnabled": false,
                        "panControlEnabled": false,
                        "right": 5,
                        "bottom": 20,
                        "minZoomLevel": 0.25,
                        "gridHeight": 100,
                        "gridAlpha": 0.1,
                        "gridBackgroundAlpha": 0,
                        "gridColor": "#19a9d5",
                        "draggerAlpha": 1,
                        "buttonCornerRadius": 2
                    }
                });
            }
    </script>--%>

    <script type="text/javascript">
        function ShowGraphDetails(gridName, Status) {
            debugger;
            var modalHeight = screen.height - 200;

            if (modalHeight < 0)
                modalHeight = screen.height;

            $('#divGraphDetails').modal('show');
            //$('.modal-dialog').css('width', '90%');
            $('#showChartDetails').attr('src', 'about: blank');
            $('#showChartDetails').attr('width', '100%');            
            $('#showChartDetails').attr('height', modalHeight + "px");
            $('#showChartDetails').attr('src', "/RLCS/RLCS_GraphDetails.aspx?Grid="+gridName+"&Status="+ Status);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="dashboard">
        <div class="col-md-12 colpadding0">
            <asp:ValidationSummary ID="vsContractDashboard" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                ValidationGroup="ContractDashboardValidationGroup" />
            <asp:CustomValidator ID="cvContractDashboard" runat="server" EnableClientScript="False"
                ValidationGroup="ContractDashboardValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
        </div>

        <!-- Top Count start -->
        <div id="divTabs" runat="server" class="row">
            <div id="entitycount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                <div class="info-box indigo-bg">
                    <div class="div-location" onclick="fEntity('<%=IsSatutoryInternal%>','<%=IsApprover%>')" style="cursor: pointer;">
                        <div class="col-md-5 TMBImg">
                            <img src="../img/manager-ico-location.png" alt="" />
                        </div>
                        <div class="col-md-7 colpadding0">
                            <div class="titleMD" style="text-align: left">Entities</div>
                            <div id="divEntitesCount" runat="server" class="countMD" style="text-align: left">0</div>
                        </div>
                    </div>
                </div>
                <!--/.info-box-->
            </div>           

            <div id="locationcount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                <div class="info-box seablue-bg">
                    <div class="div-location" style="cursor: pointer" onclick="PopulateGraphdata('<%=IsApprover%>')">
                        <div class="col-md-5 TMBImg">
                            <img src="../img/manager-ico-location.png" alt="" />
                        </div>
                        <div class="col-md-7 colpadding0">
                            <div class="titleMD" style="text-align: left">Locations</div>
                            <div id="divLocationCount" runat="server" class="countMD" style="text-align: left">0</div>
                        </div>
                    </div>
                </div>
                <!--/.info-box-->
            </div>            

            <div id="employeeCount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                <div class="info-box bluenew-bg">
                    <div class="div-location" style="cursor: pointer" onclick="fFunctions(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')">
                        <div class="col-md-5 TMBImg">
                            <img src="../img/manager-ico-functions.png" alt="" />
                        </div>
                        <div class="col-md-7 colpadding0">
                            <div class="titleMD" style="text-align: left">Employees</div>
                            <div id="divEmployeeCount" runat="server" class="countMD" style="text-align: left">0</div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="compliancecount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                <div class="info-box rednew-bg">
                    <div class="div-location" style="cursor: pointer" onclick="fCompliances(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')">
                        <div class="col-md-5 TMBImg">
                            <img src="../img/manager-ico-compliances.png" alt="" />
                        </div>
                        <div class="col-md-7 colpadding0">
                            <div class="titleMD" style="text-align: left; font-size: 17px;">Compliances</div>
                            <div id="divCompliancesCount" runat="server" class="countMD" style="text-align: left">0</div>
                        </div>
                    </div>
                </div>
                <!--/.info-box-->
            </div>           

            <div id="usercount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                <div class="info-box greennew-bg">
                    <div class="div-location" onclick="fusers(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')" style="cursor: pointer;">
                        <div class="col-md-5 TMBImg">
                            <img src="../img/manager-ico-users.png" />
                        </div>
                        <div class="col-md-7 colpadding0">
                            <div class="titleMD" style="text-align: left">Users</div>
                            <div id="divUsersCount" runat="server" class="countMD" style="text-align: left">0</div>
                        </div>
                    </div>
                </div>
                <!--/.info-box-->
            </div>            
        </div>
        <!-- Top Count End -->

        <div class="clearfix"></div>

        <!--Activity Status Summary-->
        <div id="activityStatusSummary" class="col-lg-12 col-md-12 colpadding0 row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Activity Status Summary</h2>
                    <div class="panel-actions">
                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseActivityStatusSummary"><i class="fa fa-chevron-up"></i></a>
                        <a href="javascript:closeDiv('activityStatusSummary')" class="btn-close"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>

            <div id="collapseActivityStatusSummary" class="panel-collapse collapse in">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                    <div class="info-box white-bg" onclick="ShowGraphDetails('AS','DT')">
                        <div class="title">Due (Today)</div>
                        <div class="col-md-12">
                            <div class="count" runat="server" id="divDueTodayCount">0</div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                    <div class="info-box white-bg" onclick="ShowGraphDetails('AS','UC')">
                        <div class="title">Upcoming</div>
                        <div class="col-md-12">
                            <div class="count" runat="server" id="divUpcomingCount">0</div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                    <div class="info-box white-bg" onclick="ShowGraphDetails('AS','OD')">
                        <div class="title">Overdue</div>
                        <div class="col-md-12">
                            <div class="count" runat="server" id="divOverdueCount">0</div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                    <div class="info-box white-bg" onclick="ShowGraphDetails('AS','HR')">
                        <div class="title">High Risk</div>
                        <div class="col-md-12">
                            <div class="count" runat="server" id="divHighRiskCount">0</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Activity Status Summary END-->

        <!-- Monthwise Compliance Summary start -->
        <div class="row">
            <div class="col-md-4 pl0">
                <div id="monthSummary" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12" style="padding-left: 5px; padding-right: 5px;">
                            <div class="panel panel-default">
                                <div class="panel-heading">  <%--style="margin-left: 10px;"--%>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseMonthSummary">
                                        <h2>Compliance Summary </h2>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseMonthSummary"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('collapseMonthSummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                                <div id="collapseMonthSummary" class="panel-collapse collapse in">
                                    <div class="panel-body plr0 mb0">
                                        <div class="col-md-12 plr0" id="divMonthlyCompliancePieChart" style="height: 300px;">                                                                             
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8 pl0">
                <div id="monthlySummary" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12" style="padding-left: 5px; padding-right: 5px;">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <%--style="margin-left: 10px;"--%>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseMonthlySummary">
                                        <h2>Month Wise Compliance Summary</h2>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseMonthlySummary"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('collapseMonthlySummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                                <div id="collapseMonthlySummary" class="panel-collapse collapse in">
                                    <div class="panel-body plr0 mb0">
                                        <div class="col-md-12 plr0" id="divMonthlyComplianceColumnChart" style="height: 300px;"> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 </div>
        </div>
        <!-- Monthwise Compliance Summary End -->

        <!-- PAN INDIA Compliance MAP, Invoice, Applicable Acts Start -->
        <div class="row">
            <div class="col-md-4 pl0">
                <div id="mapPanIndiaCompliance" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">  <%--style="margin-left: 10px;"--%>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseMapPanIndiaCompliance">
                                        <h2>Pan India Compliance Summary </h2>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseMapPanIndiaCompliance"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('mapPanIndiaCompliance')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                                <div id="collapseMapPanIndiaCompliance" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div id="divMapPanIndiaCompliance" style="max-height: 240px; width: auto; overflow: auto; min-height: 240px; color: #666;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 pl0">
                <div id="invoiceSummary" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <%--style="margin-left: 10px;"--%>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseInvoice">
                                        <h2>Invoices</h2>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseInvoice"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('mapPanIndiaCompliance')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                                <div id="collapseInvoice" class="panel-collapse collapse in">
                                    <div class="panel-body plr0">
                                        <div class="col-md-12 colpadding0">

                                            <div id="divInvoices" style="width: auto; overflow: auto; color: #666;">
                                                <div class="text-center text-info text-5x mb10 mt10">
                                                    <i class="fa fa-file fa-5x"></i>
                                                </div>
                                                <ul class="list-group no-radius">
                                                    <li class="list-group-item" onclick="GetInvoiceDetail('C')">
                                                        <span id="currentMonthInvoiceCount" runat="server" class="pull-right">0</span>
                                                        <span class="text-sm">Current Month Invoice Count</span>
                                                    </li>

                                                    <li class="list-group-item" onclick="GetInvoiceDetail('O')">
                                                        <span id="outstandingInvoiceCount" runat="server" class="pull-right">0</span>
                                                        <span class="text-sm ">Outstanding Invoice Count</span>
                                                    </li>

                                                    <li class="list-group-item" onclick="GetInvoiceDetail('T')">
                                                        <span id="totalInvoiceCount" runat="server" class="pull-right">0</span>
                                                        <span class="text-sm ">Total Invoice Count</span>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 pl0">
                <div id="applicableAct" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">  <%--style="margin-left: 10px;"--%>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseApplicableAct">
                                        <h2>Applicable Statutes</h2>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseApplicableAct"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('applicableAct')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                                <div id="collapseApplicableAct" class="panel-collapse collapse in">
                                    <div class="panel-body plr0">
                                        <div class="col-md-12 colpadding0">

                                            <div id="divApplicableActs" class="streamline b-l m-b-n-2 plr0"
                                                style="max-height: 240px; width: auto; overflow: auto; min-height: 240px; color: #666;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- PAN INDIA Compliance MAP, Invoice, Applicable Acts END -->

        <div class="clearfix"></div>

        <!-- Daily Updates - START -->
        <div class="row Dashboard-white-widget" id="dailyupdates">
            <div class="dashboard">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseDailyUpdates">
                                <h2>Daily Updates</h2>
                            </a>
                            <div class="panel-actions">
                                <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseDailyUpdates"><i class="fa fa-chevron-up"></i></a>
                                <a href="javascript:closeDiv('dailyupdates')" class="btn-close"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                    </div>
                    <div id="collapseDailyUpdates" class="panel-collapse collapse in">
                        <div class="row dailyupdates">
                            <ul id="DailyUpdatesrow" class="bxslider bxdaily" style="width: 100%; max-width: none;">
                            </ul>
                        </div>
                        <!--/.row-->
                        <div class="clearfix" style="height: 10px"></div>
                    </div>
                    <div class="modal fade" id="NewsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 900px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                </div>
                                <div class="modal-body" id="dailyupdatecontent">
                                    <h2 id="dailyupdatedate" style="margin-top: 1px;">Daily Updates: November 3, 2016</h2>
                                    <h3 id="dailyupdatedateInner">Daily Updates: November 3,</h3>
                                    <p id="dailytitle"></p>
                                    <div id="contents" style="color: #666666;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <a class="btn btn-search" style="float: right;" href="../Users/DailyUpdateList.aspx" title="View">View All</a>
                        <div class="clearfix" style="height: 50px"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Daily Updates - End -->

        <div class="clearfix"></div>

        <!-- News Letter - START -->
        <div class="row Dashboard-white-widget" id="newsletter">
            <div class="dashboard">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseNewsletter">
                                <h2>News Letter</h2>
                            </a>
                            <div class="panel-actions">
                                <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseNewsletter"><i class="fa fa-chevron-up"></i></a>
                                <a href="javascript:closeDiv('newsletter')" class="btn-close"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                    </div>
                    <div id="collapseNewsletter" class="panel-collapse collapse in">
                        <div class="row dailyupdates">
                            <ul id="Newslettersrow" class="bxslider bxnews" style="width: 100%; max-width: none;">
                            </ul>
                        </div>
                        <!--/.row-->
                        <div class="modal fade" id="Newslettermodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" style="width: 550px;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <img id="newsImg" src="../Images/xyz.png" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; width: 100%" />
                                        <h2 id="newsTitle"></h2>
                                        <div class="clearfix" style="height: 10px;"></div>
                                        <div id="newsDesc"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <a class="btn btn-search" style="float: right;" href="../Users/NewsLetterList.aspx" title="View">View All</a>
                            <div class="clearfix" style="height: 50px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- News Letter - End -->

        <div class="modal fade" id="divGraphDetails" role="dialog" aria-labelledby="myModal" aria-hidden="true">
            <div class="modal-dialog" style="width: 90%;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body" style="width: 100%; height: 635px">
                        <iframe id="showChartDetails" src="about:blank" scrolling="auto" frameborder="0"></iframe>
                        <%--onload="" --%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
