﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyComplianceDocuments : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int UId;
        protected static List<Int32> roles;
        //protected static int RoleDropdDisplay;
        protected static int RoleID;
        protected static int RoleFlag;
        protected static string Falg;
        protected static bool DisableFalg;
        protected static String SDate;
        protected static String LDate;
        protected static String IsMonthID;
        protected static string Path;
        protected static string avacomRLCSAPI_URL;
        protected static string ProfileID;
        protected static int? ServiceProviderID;

        protected void Page_Load(object sender, EventArgs e)
        { if (HttpContext.Current.Request.IsAuthenticated)
{
                ServiceProviderID = Business.CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (ServiceProviderID == 94)
                {
                    Response.Redirect("~/RLCS/RLCS_MyComplianceDocuments_New.aspx");
                }

            Path = ConfigurationManager.AppSettings["AVACOM_API_URL"];
            avacomRLCSAPI_URL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
            CId = Convert.ToString(AuthenticationHelper.CustomerID);
            UserId = Convert.ToString(AuthenticationHelper.UserID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
            RoleFlag = 0;
            IsMonthID = "All";
            if (AuthenticationHelper.Role == "MGMT")
            {
                Falg = "MGMT";
                RoleFlag = 1;
                DisableFalg = false;
            }
            if (AuthenticationHelper.Role == "AUDT")
            {
                IsMonthID = "AUD";
                SDate = Convert.ToDateTime(Session["Auditstartdate"].ToString()).Date.ToString("dd-MM-yyyy");
                LDate = Convert.ToDateTime(Session["Auditenddate"].ToString()).Date.ToString("dd-MM-yyyy");
                Falg = "AUD";
                DisableFalg = true;
            }
            else if (AuthenticationHelper.Role == "EXCT")
            {
                DisableFalg = false;
                Falg = "PRA";
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
}
            }
        }

        [WebMethod]
        public static string GetActType(string ActID)
        {
            string ACTType = "";
            try
            {
                int Act_ID = Convert.ToInt32(ActID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstData = (from CIM in entities.ComplianceInputMappings
                                   join C in entities.Compliances on (long)CIM.ComplianceID equals C.ID
                                   where C.ActID == Act_ID && CIM.IsActive == true
                                   select CIM.AVACOM_ActGroup).Distinct().FirstOrDefault();
                    if (lstData != "")
                    {
                        ACTType = Convert.ToString(lstData);
                    }
                }
                return ACTType;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ACTType;

            }
        }
    }
}