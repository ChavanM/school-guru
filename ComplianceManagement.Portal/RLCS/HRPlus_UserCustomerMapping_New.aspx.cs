﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_UserCustomerMapping_New : System.Web.UI.Page
    {
        public static List<int> branchList = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                divFilterUsers.Visible = true;
                lnkbtnAddNew.Visible = true;

                BindCustomers();

                BindUsers();

                //BindUsers(ddlUserPopup);
                //BindUsers(ddlMgrPage, "HMGR");
                //BindUsers(ddlMgrPopup, "HMGR");               

                BindUserCustomerMapping();

                GetPageDisplaySummary();

                if (Session["TotalCustomMapping"] != null)
                    TotalRows.Value = Session["TotalCustomMapping"].ToString();
            }
        }

        private void BindUserCustomerMapping()
        {
            try
            {
                int serviceProviderID = -1;
                int distributorID = -1;
                int customerID = -1;

                int selectedCustID = -1;

                if (!string.IsNullOrEmpty(ddlCustomerPage.SelectedValue))
                {
                    selectedCustID = string.IsNullOrEmpty(ddlCustomerPage.SelectedValue) ? -1 : Convert.ToInt32(ddlCustomerPage.SelectedValue);
                }

                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    selectedCustID = customerID;
                }

                int selectedUserID = string.IsNullOrEmpty(ddlUserPage.SelectedValue) ? -1 : Convert.ToInt32(ddlUserPage.SelectedValue);
                int selectedMgrID = string.IsNullOrEmpty(ddlMgrPage.SelectedValue) ? -1 : Convert.ToInt32(ddlMgrPage.SelectedValue);
                
                var lstUserCustomerMapping = UserCustomerMappingManagement.GetUserCustomerMappingList(selectedUserID, selectedCustID, serviceProviderID, distributorID, selectedMgrID);

                if (lstUserCustomerMapping.Count > 0)
                {
                    //if (custID != -1)
                    //    lstUserCustomerMapping = lstUserCustomerMapping.Where(row => row.CustomerID == custID).ToList();

                    //if (userID != -1)
                    //    lstUserCustomerMapping = lstUserCustomerMapping.Where(row => row.UserID == userID).ToList();

                    grdUserCustomerMapping.DataSource = lstUserCustomerMapping;
                    Session["TotalCustomMapping"] = 0;
                    Session["TotalCustomMapping"] = lstUserCustomerMapping.Count();
                    grdUserCustomerMapping.DataBind();

                    upUserCustomerList.Update();
                }
                else
                {
                    grdUserCustomerMapping.DataSource = null;
                    Session["TotalCustomMapping"] = 0;
                    Session["TotalCustomMapping"] = lstUserCustomerMapping.Count();
                    grdUserCustomerMapping.DataBind();
                }
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPage.IsValid = false;
                cvUCMPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upMapUser_Load(object sender, EventArgs e)
        {
            try
            {
                //   ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPage.IsValid = false;
                cvUCMPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindCustomers()
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                var customerList = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2, false);
                // var customerList = CustomerManagement.GetAll_HRComplianceCustomers_IncludesServiceProvider(customerID, serviceProviderID, 2);
                //var customerList = CustomerManagement.GetAll_HRComplianceCustomers(customerID, serviceProviderID, 2);

                #region Page Drop Down

                ddlCustomerPage.DataTextField = "Name";
                ddlCustomerPage.DataValueField = "ID";

                ddlCustomerPage.DataSource = customerList;
                ddlCustomerPage.DataBind();

                ddlCustomerPage.Items.Insert(0, new ListItem("Select Customer", "-1"));

                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPage.IsValid = false;
                cvUCMPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        //private void BindCustomersPopUp()
        //{
        //    try
        //    {
        //        lblCustomerPopUP.DataTextField = "Name";
        //        lblCustomerPopUP.DataValueField = "ID";

        //        int customerID = -1;
        //        if (AuthenticationHelper.Role == "CADMN")
        //        {   
        //            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        }

        //        int serviceProviderID = 95;
        //        if (AuthenticationHelper.Role == "SPADM")
        //        {
        //            serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        }

        //        lblCustomerPopUP.DataSource = CustomerManagement.GetAll_HRComplianceCustomers(customerID, serviceProviderID, 2);                
        //        lblCustomerPopUP.DataBind();

        //        lblCustomerPopUP.Items.Insert(0, new ListItem("< Select Customer >", "-1"));                
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}

        protected void ddlCustomerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlUserPage.ClearSelection();
                ddlMgrPage.ClearSelection();

                BindUserCustomerMapping();

                //BindUsers();

                //BindUsers(ddlUserPopup);
                //BindUsers(ddlUserPage);

                if (ddlCustomerPage.SelectedIndex != -1)
                    lnkbtnAddNew.Visible = true;
                else
                    lnkbtnAddNew.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPage.IsValid = false;
                cvUCMPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        private void BindUsers()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = -1;
                }

                int distID = -1;
                if (AuthenticationHelper.Role == "DADMN")
                {
                    distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int serviceProviderID = -1;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                var lstUsers = UserCustomerMappingManagement.GetAll_Users(customerID, serviceProviderID, distID);

                BindUserToDropDown(lstUsers, ddlUserPage);
                BindUserToDropDown(lstUsers, ddlMgrPage, "HMGR");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPage.IsValid = false;
                cvUCMPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindUserToDropDown(List<SP_RLCS_GetAllUser_ServiceProviderDistributor_Result> lstUsers, DropDownList ddltoBind, string roleCode = "")
        {
            try
            {
                if (lstUsers.Count > 0 && !string.IsNullOrEmpty(roleCode))
                    lstUsers = lstUsers.Where(row => row.RoleCode == roleCode).ToList();

                List<object> users = new List<object>();
                users = (from row in lstUsers
                         select new { ID = row.ID, Name = row.FirstName + " " + row.LastName, RoleCode = row.RoleCode }).Distinct().OrderBy(row => row.Name).ToList<object>();

                ddltoBind.Items.Clear();

                ddltoBind.DataTextField = "Name";
                ddltoBind.DataValueField = "ID";

                users.Insert(0, new { ID = -1, Name = "Select" });

                ddltoBind.DataSource = users;
                ddltoBind.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPage.IsValid = false;
                cvUCMPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlUserPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomerPage.ClearSelection();
            ddlMgrPage.ClearSelection();

            BindUserCustomerMapping();
        }

        protected void ddlMgrPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomerPage.ClearSelection();
            ddlUserPage.ClearSelection();

            BindUserCustomerMapping();
        }



        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        bool saveSuccess = false;

        //        if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue) && ddlCustomerPopup.SelectedValue != "-1")
        //        {
        //            if (!string.IsNullOrEmpty(ddlUserPopup.SelectedValue) && ddlUserPopup.SelectedValue != "-1")
        //            {
        //                int userID = Convert.ToInt32(ddlUserPopup.SelectedValue);

        //                List<UserCustomerMapping> lstUserCustmerMapping = new List<UserCustomerMapping>();

        //                foreach (ListItem eachCustomer in ddlCustomerPopup.Items)
        //                {
        //                    if (eachCustomer.Selected)
        //                    {
        //                        UserCustomerMapping objUserCustMapping = new UserCustomerMapping()
        //                        {
        //                            UserID = userID,
        //                            CustomerID = Convert.ToInt32(eachCustomer.Value),
        //                            IsActive = true
        //                        };

        //                        lstUserCustmerMapping.Add(objUserCustMapping);
        //                    }
        //                }

        //                if (lstUserCustmerMapping.Count > 0)
        //                {
        //                    saveSuccess = UserCustomerMappingManagement.CreateUpdate_UserCustomerMapping_Multiple(lstUserCustmerMapping);

        //                    if (saveSuccess)
        //                    {
        //                        BindUserCustomerMapping();

        //                        ddlUserPopup.ClearSelection();
        //                        ddlCustomerPopup.ClearSelection();
        //                    }
        //                    else
        //                    {
        //                        cvUserCustomerMappingPopup.IsValid = false;
        //                        cvUserCustomerMappingPopup.ErrorMessage = "Something went wrong, please try again";
        //                    }
        //                }
        //                else
        //                {
        //                    cvUserCustomerMappingPopup.IsValid = false;
        //                    cvUserCustomerMappingPopup.ErrorMessage = "Select Customer/User to Assign";
        //                }
        //            }
        //            else
        //            {
        //                cvUserCustomerMappingPopup.IsValid = false;
        //                cvUserCustomerMappingPopup.ErrorMessage = "Select User to Assign";
        //            }
        //        }
        //        else
        //        {
        //            cvUserCustomerMappingPopup.IsValid = false;
        //            cvUserCustomerMappingPopup.ErrorMessage = "Select Customer to Assign";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvUserCustomerMappingPopup.IsValid = false;
        //        cvUserCustomerMappingPopup.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN" || AuthenticationHelper.Role == "SPADM")
                {
                    if (!string.IsNullOrEmpty(ddlCustomerPage.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomerPage.SelectedValue);
                    }
                }

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignUserCustomerDialog\").dialog('open');", true);
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPage.IsValid = false;
                cvUCMPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdUserCustomerMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdUserCustomerMapping.PageIndex = e.NewPageIndex;
            BindUserCustomerMapping();
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdUserCustomerMapping_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int serviceProviderID = -1;
                int distributorID = -1;
                int customerID = -1;

                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int selectedUserID = string.IsNullOrEmpty(ddlUserPage.SelectedValue) ? -1 : Convert.ToInt32(ddlUserPage.SelectedValue);
                int selectedMgrID = string.IsNullOrEmpty(ddlMgrPage.SelectedValue) ? -1 : Convert.ToInt32(ddlMgrPage.SelectedValue);

                int selectedCustID = -1;

                if (!string.IsNullOrEmpty(ddlCustomerPage.SelectedValue))
                {
                    selectedCustID = string.IsNullOrEmpty(ddlCustomerPage.SelectedValue) ? -1 : Convert.ToInt32(ddlCustomerPage.SelectedValue);
                }

                var lstUserCustomerMapping = UserCustomerMappingManagement.GetUserCustomerMappingList(selectedUserID, selectedCustID, serviceProviderID, distributorID, selectedMgrID);

                if (lstUserCustomerMapping.Count > 0)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        lstUserCustomerMapping = lstUserCustomerMapping.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        lstUserCustomerMapping = lstUserCustomerMapping.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Ascending;
                    }

                    foreach (DataControlField field in grdUserCustomerMapping.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["SortIndex"] = grdUserCustomerMapping.Columns.IndexOf(field);
                        }
                    }

                    grdUserCustomerMapping.DataSource = lstUserCustomerMapping;
                    grdUserCustomerMapping.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPage.IsValid = false;
                cvUCMPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdUserCustomerMapping_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdUserCustomerMapping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    long recordID = Convert.ToInt64(e.CommandArgument);

                    if (e.CommandName.Equals("DELETE_USER"))
                    {
                        if (UserCustomerMappingManagement.Delete_UserCustomerMapping(recordID))
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('User Customer Mapping Deleted Successfully.')", true);
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('User Customer Mapping Not Deleted.')", true);
                        }

                        BindUserCustomerMapping();
                    }
                    else if (e.CommandName.Equals("EDIT_USER"))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "OpenPopupUCM("+ recordID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPage.IsValid = false;
                cvUCMPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;
                DivnextScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalCustomMapping"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalCustomMapping"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalCustomMapping"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                    DivnextScrum.Visible = false;
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                if (Session["TotalCustomMapping"] != null)
                    TotalRows.Value = Session["TotalCustomMapping"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalCustomMapping"]))
                    EndRecord = Convert.ToInt32(Session["TotalCustomMapping"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdUserCustomerMapping.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdUserCustomerMapping.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindUserCustomerMapping();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalCustomMapping"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalCustomMapping"]))
                    EndRecord = Convert.ToInt32(Session["TotalCustomMapping"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdUserCustomerMapping.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdUserCustomerMapping.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindUserCustomerMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdUserCustomerMapping.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdUserCustomerMapping.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindUserCustomerMapping();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindUserCustomerMapping();
            //BindCustomers();
            // ddlSPList_SelectedIndexChanged(sender, e);
        }
    }
}