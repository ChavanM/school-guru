﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_CustomerDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_CustomerDetails" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <link href="~/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="/Newjs/jquery.js"></script>

    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <style>
        .m-10 {
            /*margin-left: 10px;*/
            padding-left: 24px;
        }

        .pl0{
            padding-left:0px;
        }
    </style>
    <script>
        $(document).ready(function ()
        {
            debugger;
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
            
            $(window.parent.document).find("#BodyContent_updateProgress").hide();
                  
            $('#btnSave').click(function () {
                $(window.parent.document).find("#BodyContent_updateProgress").show();
            });

            BindControls();
        });

        function BindControls() {
            $(function () {
                $('[id*=lstBoxProduct]').multiselect({
                    includeSelectAllOption: false,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: false,
                    //filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - Product(s) selected',
                });
            });
        }

        //added by tejashri for close popup window after save record..
        function ClosePopupWindowAfterSave() {
            setTimeout(function () {
                if (window.parent.$("#divAddEditCustomerDialog") != null && window.parent.$("#divAddEditCustomerDialog") != undefined)
                    window.parent.$("#divAddEditCustomerDialog").data("kendoWindow").close();
            }, 3000);
        }
        //added by tejashri for close popup window after save record..

    </script>
</head>
<body style="background: white;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="upCustomers" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="row">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ValidationGroup="CustomerValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="CustomerValidationGroup" Display="None" />
                    <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                </div>

                <div style="margin-bottom: 7px; display: none">
                    <asp:Label Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">&nbsp;</asp:Label>
                    <asp:Label Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                       Is Service Provider:</asp:Label>
                    <asp:CheckBox ID="chkSp" runat="server" />
                </div>

                <div class="row" style="margin-top: 7px;">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 colpadding0">
                            <asp:Label ID="lblCustDistComp" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                            <asp:Label ID="lblCustDist" CssClass="control-label" runat="server">
                        Select</asp:Label>
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-9 colpadding0">
                            <asp:RadioButtonList ID="rblCustomerDistributor" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblCustomerDistributor_CheckedChanged" AutoPostBack="true">
                                <asp:ListItem Text="Customer" Value="C" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Professional Firm" Value="D"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                    </div>
                </div>

                <div class="row" style="margin-top: 7px;">
                    <div class="col-xs-6 col-sm-6 col-md-6" id="divSubDist" runat="server">
                        <div class="col-xs-9 col-sm-9 col-md-9 colpadding0">
                            <asp:Label ID="Label1" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                            <asp:Label ID="Label2" CssClass="control-label" runat="server">Can Create Sub-Distributor(s)?</asp:Label>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 colpadding0">
                            <asp:RadioButtonList ID="rblSubDistributor" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-6" id="divPayment" runat="server">
                        <div class="col-xs-9 col-sm-9 col-md-9 colpadding0">
                            <asp:Label ID="Label5" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                            <asp:Label ID="Label6" CssClass="control-label" runat="server">Fixed Price</asp:Label>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 colpadding0">
                            <asp:RadioButtonList ID="rblPayment" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>

                <div class="row" style="display: none">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <asp:Label ID="lblast" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                        <asp:Label CssClass="control-label" ID="lblsp" runat="server">Service Provider</asp:Label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:DropDownList runat="server" ID="ddlSPName" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="row" style="display: none" runat="server" id="divDistributor">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <asp:Label ID="Label3" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                        <asp:Label CssClass="control-label" ID="Label4" runat="server">Distributor:</asp:Label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:DropDownList runat="server" ID="ddlDistributor" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">Name</label>
                        <asp:TextBox runat="server" ID="tbxName" CssClass="form-control" autocomplete="off" MaxLength="2000" OnTextChanged="tbxName_TextChanged" AutoPostBack="true" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty."
                            ControlToValidate="tbxName" runat="server" ValidationGroup="CustomerValidationGroup" Display="None" />
                      <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                            ErrorMessage="Please enter a valid name" ControlToValidate="tbxName" ValidationExpression="^[_A-z0-9]*((-|\s)*[_A-z0-9])*$"></asp:RegularExpressionValidator>--%>

                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                            ErrorMessage="Please enter a valid name" ControlToValidate="tbxName" ValidationExpression="[ a-zA-Z0-9-&()]*$"></asp:RegularExpressionValidator>

                    </div>
                    <div id="divCorpID" runat="server" class="col-xs-6 col-sm-6 col-md-6">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label" style="">CorporateID</label>
                        <div class="col-xs-10 col-sm-10 col-md-10 pl0">
                            <asp:TextBox runat="server" ID="txtCorpID" CssClass="form-control" MaxLength="10" autoComplete="off" />
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 colpadding0">
                            <asp:Button Text="Check" runat="server" ID="btnCheckCorpID" CssClass="btn btn-primary"
                                OnClick="btnCheckCorpID_Click" />
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-xs-12 col-sm-12 col-md-12 colpadding0">
                            <asp:Label runat="server" ID="lblCheckCorporate" ForeColor="red" CssClass="control-label"
                                Text="Please click on Check button to verify CorporateID"></asp:Label>
                        </div>
                    </div>
                </div>

                <div class="row" style="display: none;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label class="control-label">Location Type:</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:DropDownList runat="server" ID="ddlLocationType" CssClass="form-control" Width="94%" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">Buyer Name</label>
                        <asp:TextBox runat="server" ID="tbxBuyerName" CssClass="form-control" autocomplete="off"
                            MaxLength="50" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Buyer Name can not be empty."
                            ControlToValidate="tbxBuyerName" runat="server" ValidationGroup="CustomerValidationGroup"
                            Display="None" />
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">Buyer Contact No</label>
                        <asp:TextBox runat="server" ID="tbxBuyerContactNo" CssClass="form-control" autocomplete="off"
                            MaxLength="15" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Buyer Contact Number can not be empty"
                            ControlToValidate="tbxBuyerContactNo" runat="server" ValidationGroup="CustomerValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                            ErrorMessage="Please enter a valid contact number." ControlToValidate="tbxBuyerContactNo"
                            ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                            ValidationGroup="CustomerValidationGroup" ErrorMessage="Buyer Contact No can be of 10 digits"
                            ControlToValidate="tbxBuyerContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Buyer Email</label>
                        <asp:TextBox runat="server" ID="tbxBuyerEmail" CssClass="form-control" autocomplete="off"
                            MaxLength="200" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Buyer Email can not be empty."
                            ControlToValidate="tbxBuyerEmail" runat="server" ValidationGroup="CustomerValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                            ErrorMessage="Please enter a valid email." ControlToValidate="tbxBuyerEmail"
                            ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">Status</label>
                        <asp:DropDownListChosen runat="server" ID="ddlCustomerStatus" CssClass="form-control" Width="100%" AllowSingleDeselect="false" />
                    </div>
                </div>

                <div style="display: none" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Label runat="server" ID="lblstartdate" CssClass="control-label">
                        Start Date</asp:Label>
                        <asp:TextBox runat="server" ID="txtStartDate" CssClass="form-control" ReadOnly="true" autocomplete="off"
                            MaxLength="200" />
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Label ID="lblenddate" runat="server" CssClass="control-label">
                        End Date</asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtEndDate" ReadOnly="true" autocomplete="off"
                            MaxLength="200" />
                    </div>
                </div>

                <div style="margin-bottom: 7px; display: none;" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">
                            Disk Space</label>
                        <asp:TextBox runat="server" ID="txtDiskSpace" CssClass="form-control" autocomplete="off"
                            MaxLength="200" />
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">
                            Internal Compliance Applicable</label>
                        <asp:DropDownListChosen runat="server" ID="ddlComplianceApplicable" CssClass="form-control" Width="100%">
                            <asp:ListItem Text="No" Value="0" Selected="True" />
                            <asp:ListItem Text="Yes" Value="1" />
                        </asp:DropDownListChosen>
                    </div>
                </div>

                <div style="margin-bottom: 7px; display: none;" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">
                            Task Management Applicable</label>
                        <asp:DropDownListChosen runat="server" ID="ddlTaskApplicable" CssClass="form-control" Width="100%">
                            <asp:ListItem Text="No" Value="0" Selected="True" />
                            <asp:ListItem Text="Yes" Value="1" />
                        </asp:DropDownListChosen>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">
                            Compliance Product Type</label>
                        
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6" id="divPAN" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">
                            PAN</label>
                        <asp:TextBox runat="server" ID="txtPAN" CssClass="form-control" MaxLength="10" autoComplete="off" />
                        <asp:RegularExpressionValidator ID="revPAN" ControlToValidate="txtPAN" runat="server" ErrorMessage="PAN No can be of 10 digits"
                            ValidationGroup="CustomerValidationGroup" ValidationExpression="^[\w]{5}[\d]{4}[\w]$" Display="None"></asp:RegularExpressionValidator>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6" id="divTAN" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">TAN</label>
                        <asp:TextBox runat="server" ID="txtTAN" CssClass="form-control" autoComplete="off" MaxLength="10" />
                        <asp:RegularExpressionValidator ID="revTAN" ControlToValidate="txtTAN" runat="server" ErrorMessage="TAN No can be of 10 digits"
                            ValidationGroup="CustomerValidationGroup" ValidationExpression="^[\w]{4}[\d]{5}[\w]$" Display="None"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">Address</label>
                        <asp:TextBox runat="server" ID="tbxAddress" CssClass="form-control" MaxLength="500"
                            TextMode="MultiLine" autocomplete="off" />
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3" id="divCountry" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">Country</label>
                        <asp:DropDownListChosen runat="server" ID="ddlCountry" CssClass="form-control" DataPlaceHolder="Select" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 pl0" id="divState" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">State</label>
                        <asp:DropDownListChosen runat="server" ID="ddlState" CssClass="form-control" Width="100%" DataPlaceHolder="Select" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="ddlState_SelectedIndexChanged" AutoPostBack="true" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 pl0" id="divCity" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">City</label>
                        <asp:DropDownListChosen runat="server" ID="ddlCity" CssClass="form-control" Width="100%" DataPlaceHolder="Select" AllowSingleDeselect="false" />
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 pl0">
                        <label class="control-label">Assign Product(s)</label>
                        <asp:ListBox ID="lstBoxProduct" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 pl0" id="div1" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">Compliance Product Type</label>
                        <asp:DropDownListChosen runat="server" ID="ddlComplianceProductType" CssClass="form-control"
                            OnSelectedIndexChanged="ddlComplianceProductType_SelectedIndexChanged" AutoPostBack="true" Width="100%">
                            <asp:ListItem Text="HRProduct Only" Value="2" Selected="True" />
                            <asp:ListItem Text="Avacom + TL" Value="3" />
                            <asp:ListItem Text="Avacom + HRProduct" Value="4" />
                        </asp:DropDownListChosen>
                    </div>
                </div>

                <table class="row col-md-12" style="width: 100%">
                    <tr>
                        <td>
                            <div style="margin-bottom: 7px" runat="server" class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label class="control-label">
                                        Logo</label>
                                </div>
                                <div class="col-xs-9 col-sm-9 col-md-9">
                                    <asp:FileUpload ID="fuCustLogoUpload" runat="server" />

                                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                                        ErrorMessage="Unsupported Logo File. Supported files are (*.gif,*.jpeg,*.jpeg,*.png)" ControlToValidate="fuCustLogoUpload"
                                        ValidationExpression="^([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.gif)$"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </td>
                        <td><span visible="false" id="imgDiv" runat="server">
                            <asp:Image ID="ImgLogo" runat="server" Style="max-height: 100px; max-width: 100px; height: auto; width: auto;" /></span>
                        </td>
                    </tr>
                </table>

                <div style="text-align: center; margin-top: 10px;">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" CausesValidation="true"
                        ValidationGroup="CustomerValidationGroup" />
                </div>

                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnsave" />
            </Triggers>
        </asp:UpdatePanel>

    </form>
</body>
</html>
