﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using paytm;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_ApplicabilityDetails : System.Web.UI.Page
    {
        protected static string AVACOM_RLCS_API_URL;
        protected static string avacomAPIUrl;
        protected static string TLConnectAPIUrl;
        protected static int customerID;
        protected static int BranchID;
        protected static string authKey;
        protected static string userProfileID_Encrypted;
        protected static int loggedInUserID;

        protected void Page_Load(object sender, EventArgs e)
        {
            AVACOM_RLCS_API_URL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            loggedInUserID = AuthenticationHelper.UserID;
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            //hdnCustID.Value = Convert.ToString(AuthenticationHelper.CustomerID);
            VSPopup.CssClass = "alert alert-danger";
            vsPricingTab.CssClass = "alert alert-danger";

            if (!IsPostBack)
            {
                processTxnResponse_PayTM();

                txtPaymentMobile.Attributes.Add("readonly", "readonly");
                txtPaymentEmail.Attributes.Add("readonly", "readonly");
                txtPaymentDate.Attributes.Add("readonly", "readonly");
                txtPaymentAmount.Attributes.Add("readonly", "readonly");

                try
                {
                    avacomAPIUrl = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
                    TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                    authKey = ConfigurationManager.AppSettings["AVACOM_RLCS_API_authKey"];
                    userProfileID_Encrypted = ConfigurationManager.AppSettings["userProfileID_Encrypted"];

                    if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                    {
                        var custID = Request.QueryString["ID"];

                        if (custID != "")
                        {
                            Session["CustomerID"] = custID;

                            if (Convert.ToInt64(custID) == 0)
                            {
                                ViewState["Mode"] = 0;
                                ClearControls(); //Add New Detail
                                divClientDetails.Visible = false;
                            }
                            else
                            {
                                hdnCustID.Value = custID;

                                ViewState["Mode"] = 1;
                                ShowCustomerDetails(Convert.ToInt32(custID));//Edit Detail 
                                //BindGrid_Clients(Convert.ToInt32(custID));
                                //divClientDetails.Visible = true;
                                divClientDetails.Visible = false;
                            }

                            if (!string.IsNullOrEmpty(Request.QueryString["Source"]))
                            {
                                if (Request.QueryString["Source"].Equals("Master"))
                                    EnableDisableSaveButtons(false);
                                else
                                    EnableDisableSaveButtons(true);
                            }
                        }
                    }

                    lnkBtnDetails_Click(sender, e);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        public void EnableDisableSaveButtons(bool flag)
        {
            try
            {
                btnSave.Enabled = flag;
                Button1.Enabled = flag; //Questionnaire Grid Page
                btnContinueAsCustomer.Enabled = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void ShowCustomerDetails(int customerID)
        {
            try
            {
                //var record = RLCS_ApplicabilityCheck.GetDetails(recordID);
                var customerRecord = CustomerManagement.GetByID(customerID);

                if (customerRecord != null)
                {
                    txtCustName.Text = customerRecord.Name;
                    txtBuyerName.Text = customerRecord.BuyerName;
                    txtContactNumber.Text = customerRecord.BuyerContactNumber;

                    txtEmail.Text = customerRecord.BuyerEmail;
                    txtAddress.Text = customerRecord.Address;

                    hdnCustName.Value = customerRecord.Name;
                    hdnEmail.Value = customerRecord.BuyerEmail;
                    hdnMobile.Value = customerRecord.BuyerContactNumber;

                    ViewState["Status"] = customerRecord.Status;
                    ViewState["IsDist"] = customerRecord.IsDistributor;
                    //BindGrid_Clients(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindGrid_Clients(int custID)
        {
            try
            {
                long parentID = -1;

                grdEntityClient.DataSource = CustomerBranchManagement.GetAll(Convert.ToInt32(custID), parentID, string.Empty);
                grdEntityClient.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdEntityClient_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    int clientEntityID = Convert.ToInt32(e.CommandArgument);
                    if (clientEntityID != 0)
                    {
                        if (e.CommandName.Equals("Proceed"))
                        {
                            //hdnRecordID.Value = clientEntityID.ToString();
                            lnkQuestionnaire_Click(null, null);
                        }
                        else if (e.CommandName.Equals("EditClient"))
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "EditEntiyClient", "OpenAddEntityClientPopup(" + clientEntityID + ")", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvClient.IsValid = false;
                cvClient.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void lnkBtnDetails_Click(object sender, EventArgs e)
        {
            MainView.ActiveViewIndex = 0;

            liDetails.Attributes.Add("class", "active");
            liQuestionnaire.Attributes.Add("class", "");
            liApplicability.Attributes.Add("class", "");
            liPricing.Attributes.Add("class", "");
            liPayment.Attributes.Add("class", "");
        }

        protected void lnkQuestionnaire_Click(object sender, EventArgs e)
        {
            if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
            {
                MainView.ActiveViewIndex = 1;

                liDetails.Attributes.Add("class", "");
                liQuestionnaire.Attributes.Add("class", "active");
                liApplicability.Attributes.Add("class", "");
                liPricing.Attributes.Add("class", "");
                liPayment.Attributes.Add("class", "");
                SetInitialRow();
                //BindData(Convert.ToInt32(hdnCustID.Value));

                Page.ClientScript.RegisterStartupScript(Page.GetType(), "CallMyFunction", "bindQuestionnaireGrid();", true);
                divtreelist.Style.Add("display", "block");
                Questionnaire.Style.Add("display", "none");
                addQuestionnaire.Value = "Add New";
            }
            else
            {
                if (MainView.ActiveViewIndex == 0)
                {
                    cvPopUp.IsValid = false;
                    cvPopUp.ErrorMessage = "Please Save the details first before proceed further";
                }
            }
        }

        protected void lnkBtnApplicability_Click(object sender, EventArgs e)
        {
            if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
            {
                MainView.ActiveViewIndex = 2;

                liDetails.Attributes.Add("class", "");
                liApplicability.Attributes.Add("class", "active");
                liQuestionnaire.Attributes.Add("class", "");
                liPricing.Attributes.Add("class", "");
                liPayment.Attributes.Add("class", "");

                Page.ClientScript.RegisterStartupScript(this.GetType(), "showApplicabiltyTab", "LoadControls_ApplicabilityTab()", true);
            }
            else
            {
                if (MainView.ActiveViewIndex == 0)
                {
                    cvPopUp.IsValid = false;
                    cvPopUp.ErrorMessage = "Please Save the details first before proceed further";
                }
            }
        }

        protected void lnkBtnPricing_Click(object sender, EventArgs e)
        {
            if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
            {
                MainView.ActiveViewIndex = 3;

                liDetails.Attributes.Add("class", "");
                liQuestionnaire.Attributes.Add("class", "");
                liApplicability.Attributes.Add("class", "");
                liPricing.Attributes.Add("class", "active");
                liPayment.Attributes.Add("class", "");

                Page.ClientScript.RegisterStartupScript(this.GetType(), "showTabPrice", "ShowPricingTab();", true);
            }
            else
            {
                if (MainView.ActiveViewIndex == 0)
                {
                    cvPopUp.IsValid = false;
                    cvPopUp.ErrorMessage = "Please Save the details first before proceed further";
                }
            }
        }

        protected void lnkBtnPayment_Click(object sender, EventArgs e)
        {
            if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
            {
                MainView.ActiveViewIndex = 4;

                liDetails.Attributes.Add("class", "");
                liQuestionnaire.Attributes.Add("class", "");
                liApplicability.Attributes.Add("class", "");
                liPricing.Attributes.Add("class", "");
                liPayment.Attributes.Add("class", "active");

                Page.ClientScript.RegisterStartupScript(this.GetType(), "showPayTab", "ShowPaymentTab()", true);
                txtOrderID.Text = PaymentManagement.Get_UniqueOrderID(Convert.ToInt32(hdnCustID.Value), "Order");
            }
            else
            {
                if (MainView.ActiveViewIndex == 0)
                {
                    cvPopUp.IsValid = false;
                    cvPopUp.ErrorMessage = "Please Save the details first before proceed further";
                }
            }
        }

        protected void btnNextPricing_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnPayment_Click(sender, e);

                //if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
                //{
                //    //Page.ClientScript.RegisterStartupScript(this.GetType(), "setTotValue", "setTotalPrice();", true);

                //    MainView.ActiveViewIndex = 4;

                //    liDetails.Attributes.Add("class", "");
                //    liQuestionnaire.Attributes.Add("class", "");
                //    liApplicability.Attributes.Add("class", "");
                //    liPricing.Attributes.Add("class", "");
                //    liPayment.Attributes.Add("class", "active");

                //    Page.ClientScript.RegisterStartupScript(this.GetType(), "showPayTab", "ShowPaymentTab();", true);

                //    txtOrderID.Text = PaymentManagement.Get_UniqueOrderID(Convert.ToInt32(hdnCustID.Value), "Order");
                //}
                //else
                //{
                //    if (MainView.ActiveViewIndex == 0)
                //    {
                //        cvPopUp.IsValid = false;
                //        cvPopUp.ErrorMessage = "Please Save the details first before proceed further";
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnProceedPayment_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
                {
                    if (!string.IsNullOrEmpty(txtOrderID.Text))
                    {
                        if (!string.IsNullOrEmpty(txtPaymentAmount.Text))
                        {
                            //CustID, PGID, OrderID, Amount, Phone, Email
                            Session["PaymentDetails"] = hdnCustID.Value + "," + 2 + "," + txtOrderID.Text.Trim() + "," + txtPaymentAmount.Text.Trim() + "," + txtPaymentMobile.Text.Trim() + "," + txtPaymentEmail.Text.Trim();

                            PaymentManagement.PaywithPayTM(Convert.ToInt32(hdnCustID.Value), 1, txtOrderID.Text.Trim(), txtPaymentAmount.Text.Trim(), txtPaymentMobile.Text, txtPaymentEmail.Text, AuthenticationHelper.UserID);
                        }
                        else
                        {
                            cvPayment.IsValid = false;
                            cvPayment.ErrorMessage = "Amount can not be empty, Please try again later or refresh the page";
                        }
                    }
                    else
                    {
                        cvPayment.IsValid = false;
                        cvPayment.ErrorMessage = "OrderID can not be empty, Please try again later or refresh the page";
                    }
                }
                else
                {
                    if (MainView.ActiveViewIndex == 0)
                    {
                        cvPopUp.IsValid = false;
                        cvPopUp.ErrorMessage = "Please Save the details first before proceed further";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;

                Customer ComplianceCustomerRecord = new Customer()
                {
                    Name = txtCustName.Text.Trim(),
                    Address = txtAddress.Text.Trim(),
                    BuyerName = txtBuyerName.Text.Trim(),
                    BuyerContactNumber = txtContactNumber.Text.Trim(),
                    BuyerEmail = txtEmail.Text.Trim(),

                    IsDeleted = false,

                    CreatedOn = DateTime.Now,
                    IComplianceApplicable = 1,
                    TaskApplicable = 1,
                    IsServiceProvider = false,
                    ServiceProviderID = 95,
                    ComplianceProductType = 2,
                    Status = 0,
                    IsDistributor = false,
                };

                if (AuthenticationHelper.CustomerID != null && AuthenticationHelper.CustomerID != 0)
                    ComplianceCustomerRecord.ParentID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                mst_Customer auditCustomerRecord = new mst_Customer()
                {
                    Name = ComplianceCustomerRecord.Name,
                    Address = ComplianceCustomerRecord.Address,
                    BuyerName = ComplianceCustomerRecord.BuyerName,
                    BuyerContactNumber = ComplianceCustomerRecord.BuyerContactNumber,
                    BuyerEmail = ComplianceCustomerRecord.BuyerEmail,
                    IsDeleted = ComplianceCustomerRecord.IsDeleted,
                    CreatedOn = ComplianceCustomerRecord.CreatedOn,
                    IComplianceApplicable = ComplianceCustomerRecord.IComplianceApplicable,
                    TaskApplicable = ComplianceCustomerRecord.TaskApplicable,
                    IsServiceProvider = ComplianceCustomerRecord.IsServiceProvider,
                    ServiceProviderID = ComplianceCustomerRecord.ServiceProviderID,
                    ComplianceProductType = ComplianceCustomerRecord.ComplianceProductType,

                    Status = ComplianceCustomerRecord.Status,
                    IsDistributor = ComplianceCustomerRecord.IsDistributor,
                    ParentID = ComplianceCustomerRecord.ParentID
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    if (ViewState["Status"] != null)
                    {
                        ComplianceCustomerRecord.Status = Convert.ToInt32(ViewState["Status"]);
                        auditCustomerRecord.Status = Convert.ToInt32(ViewState["Status"]);
                    }

                    if (ViewState["IsDist"] != null)
                    {
                        ComplianceCustomerRecord.IsDistributor = Convert.ToBoolean(ViewState["IsDist"]);
                        auditCustomerRecord.IsDistributor = Convert.ToBoolean(ViewState["IsDist"]);
                    }

                    ComplianceCustomerRecord.ID = Convert.ToInt32(Session["CustomerID"]);
                    auditCustomerRecord.ID = Convert.ToInt32(Session["CustomerID"]);
                }

                saveSuccess = CustomerManagement.Exists(ComplianceCustomerRecord);

                if (!saveSuccess) //No Customer With Same Name Exists
                {
                    int avacom_CustomerID = 0;

                    if ((int)ViewState["Mode"] == 0)
                    {
                        avacom_CustomerID = CustomerManagement.Create(ComplianceCustomerRecord);
                        if (avacom_CustomerID > 0)
                        {
                            saveSuccess = CustomerManagementRisk.Create(auditCustomerRecord);
                            if (saveSuccess == false)
                            {
                                CustomerManagement.deleteCustReset(avacom_CustomerID);
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        avacom_CustomerID = ComplianceCustomerRecord.ID;

                        CustomerManagement.Update(ComplianceCustomerRecord);
                        CustomerManagementRisk.Update(auditCustomerRecord);
                        saveSuccess = true;
                    }

                    //Create or Update Customer/Corporate in RLCS DB
                    if (saveSuccess && avacom_CustomerID != 0 && (int)ViewState["Mode"] == 0)
                    {
                        RLCS_Customer_Corporate_Mapping newRecord = new RLCS_Customer_Corporate_Mapping()
                        {
                            AVACOM_CustomerID = avacom_CustomerID,
                            CO_Name = ComplianceCustomerRecord.Name,

                            CO_Version = 1,
                        };

                        string corpID = GenerateCorpID(ComplianceCustomerRecord.Name.Trim());

                        if (!string.IsNullOrEmpty(corpID))
                        {
                            newRecord.CO_CorporateID = corpID;

                            if (ComplianceCustomerRecord.Status != null)
                            {
                                if (ComplianceCustomerRecord.Status == 0)
                                    newRecord.CO_Status = "I";
                                else if (ComplianceCustomerRecord.Status == 1)
                                    newRecord.CO_Status = "A";
                            }

                            saveSuccess = RLCS_Master_Management.CreateUpdate_Customer_CorporateClient_Mapping(newRecord);

                            if (saveSuccess)
                            {
                                Model_CorporateClientMaster corpRecord = new Model_CorporateClientMaster()
                                {
                                    CorporateId = newRecord.CO_CorporateID,
                                    CorporateName = newRecord.CO_Name,
                                    CO_ModifiedBy = "1",
                                };

                                string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                                rlcsAPIURL += "AventisIntegration/CreateUpdateCorporateMaster";

                                string responseData = WebAPIUtility.POST_Corporate(rlcsAPIURL, corpRecord);

                                if (!string.IsNullOrEmpty(responseData))
                                {
                                    var apiResponse = JsonConvert.DeserializeObject<RLCS_API_Response>(responseData);

                                    if (apiResponse != null)
                                    {
                                        saveSuccess = Convert.ToBoolean(apiResponse.StatusCode);

                                        if (saveSuccess)
                                        {
                                            RLCS_ClientsManagement.Update_ProcessedStatus_Corporate(avacom_CustomerID, corpRecord.CorporateId, true);
                                        }
                                    }
                                }

                                com.VirtuosoITech.ComplianceManagement.Business.Data.ProductMapping productmapping = new com.VirtuosoITech.ComplianceManagement.Business.Data.ProductMapping()
                                {
                                    CustomerID = avacom_CustomerID,
                                    ProductID = 1, //Compliance
                                    IsActive = false,
                                    CreatedBy = 1,
                                    CreatedOn = DateTime.Now
                                };

                                ProductMapping_Risk productmappingrisk = new ProductMapping_Risk()
                                {
                                    CustomerID = avacom_CustomerID,
                                    ProductID = 1, //Compliance
                                    IsActive = false,
                                    CreatedBy = 1,
                                    CreatedOn = DateTime.Now
                                };

                                RLCS_Master_Management.CreateUpdate_ProductMapping_ComplianceDB(productmapping);
                                RLCS_Master_Management.CreateUpdate_ProductMapping_AuditDB(productmappingrisk);
                            }
                        }
                        else
                        {
                            LoggerMessage.InsertErrorMsg_DBLog("CorpID-Blank-CustID-" + avacom_CustomerID, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }

                    if (saveSuccess)
                    {
                        hdnCustID.Value = avacom_CustomerID.ToString();

                        hdnCustName.Value = ComplianceCustomerRecord.Name;
                        hdnEmail.Value = ComplianceCustomerRecord.BuyerEmail;
                        hdnMobile.Value = ComplianceCustomerRecord.BuyerContactNumber;

                        cvPopUp.IsValid = false;
                        cvPopUp.ErrorMessage = "Details Save Successfully";
                        VSPopup.CssClass = "alert alert-success";

                        //divClientDetails.Visible = true;
                        ViewState["Mode"] = 1;

                        lnkQuestionnaire_Click(sender, e);
                    }
                    else
                    {
                        cvPopUp.IsValid = false;
                        cvPopUp.ErrorMessage = "Something went wrong, Please try again";
                    }
                }
                else
                {
                    cvPopUp.IsValid = false;
                    cvPopUp.ErrorMessage = "Customer with Same Name already exists, Please provide another";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
                {
                    lnkQuestionnaire_Click(sender, e);
                }
                else
                {
                    if (MainView.ActiveViewIndex == 0)
                    {
                        cvPopUp.IsValid = false;
                        cvPopUp.ErrorMessage = "Please Save the details first before proceed further";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnNextToApplicability_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
                {
                    lnkBtnApplicability_Click(sender, e);
                }
                else
                {
                    if (MainView.ActiveViewIndex == 0)
                    {
                        cvPopUp.IsValid = false;
                        cvPopUp.ErrorMessage = "Please Save the details first before proceed further";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefreshEntityClient_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnCustID.Value != null)
                {
                    BindGrid_Clients(Convert.ToInt32(hdnCustID.Value));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                ViewState["Mode"] = 0;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void btnSaveApplicability_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string strMappedCompliances=string.Empty;

        //        if (!string.IsNullOrEmpty(strMappedCompliances) && hdnRecordID.Value != null)
        //        {
        //            int recordID = Convert.ToInt32(hdnRecordID.Value);
        //            var arrMappedCompliances = strMappedCompliances.Split(',');

        //            if (arrMappedCompliances.Length > 0)
        //            {



        //            }
        //        }

        //        TempCustomer customerRecord = new TempCustomer()
        //        {
        //            Name = txtCustName.Text.Trim(),
        //            Address = txtAddress.Text.Trim(),
        //            BuyerName = txtBuyerName.Text.Trim(),
        //            BuyerContactNumber = txtContactNumber.Text.Trim(),
        //            BuyerEmail = txtEmail.Text.Trim(),
        //            IsDeleted = false,
        //            CreatedBy = AuthenticationHelper.UserID,
        //            UpdatedBy = AuthenticationHelper.UserID
        //        };

        //        if (AuthenticationHelper.CustomerID != null && AuthenticationHelper.CustomerID != 0)
        //            customerRecord.DistributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);

        //        if ((int)ViewState["Mode"] == 1)
        //        {
        //            customerRecord.ID = Convert.ToInt32(Session["RecordID"]);
        //        }

        //        saveSuccess = RLCS_ApplicabilityCheck.Exists_Customer_TempCustomer(customerRecord);

        //        if (!saveSuccess) //No Customer With Same Name Exists
        //        {
        //            saveSuccess = RLCS_ApplicabilityCheck.CreateUpdate_TempCustomer(customerRecord);

        //            if (saveSuccess)
        //            {
        //                cvPopUp.IsValid = false;
        //                cvPopUp.ErrorMessage = "Details Save Successfully";
        //            }
        //            else
        //            {
        //                cvPopUp.IsValid = false;
        //                cvPopUp.ErrorMessage = "Something went wrong, Please try again";
        //            }
        //        }
        //        else
        //        {
        //            cvPopUp.IsValid = false;
        //            cvPopUp.ErrorMessage = "Company with Same Name already exists, Please provide another";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void btnContinueAsCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
                {
                    int customerID = Convert.ToInt32(hdnCustID.Value);
                    var customerRecord = CustomerManagement.GetByID(customerID);
                    string corpID = RLCS_Master_Management.GetCorporateIDByCustID(customerID);

                    if (customerRecord != null && !string.IsNullOrEmpty(corpID))
                    {
                        var entityBranchRecords = RLCS_Master_Management.Get_CustomerAssessmentDetails(customerID);

                        if (entityBranchRecords != null)
                        {
                            if (entityBranchRecords.Count > 0)
                            {
                                var entityRecords = entityBranchRecords.Where(row => (row.ParentBranchID == null || row.ParentBranchID == 0) && row.BranchType == "E").ToList();

                                if (entityRecords != null)
                                {
                                    if (entityRecords.Count > 0)
                                    {
                                        bool entityBranchSuccess = false;
                                        int parentID = 0;

                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            entityRecords.ForEach(eachEntity =>
                                            {
                                                parentID = 0;
                                                string clientID = string.Empty;

                                                if (eachEntity.AVACOM_BranchID != null)
                                                    clientID = RLCS_Master_Management.GetClientIDByBranchID(Convert.ToInt32(eachEntity.AVACOM_BranchID));
                                                else
                                                    clientID = GenerateClientID(eachEntity.Location.Trim());

                                                if (!string.IsNullOrEmpty(clientID))
                                                {
                                                    #region Entity

                                                    CustomerBranch newEntity_Compliance = new CustomerBranch()
                                                    {
                                                        Status = 0,
                                                        IsDeleted = false,
                                                        ParentID = null,
                                                        Type = 1,
                                                        LegalRelationShipOrStatus = 0, //TBD
                                                        ComType = 0,
                                                        Industry = -1,
                                                    };

                                                    //CustomerID
                                                    newEntity_Compliance.CustomerID = customerID;

                                                    //NAME
                                                    if (!string.IsNullOrEmpty(eachEntity.Location))
                                                        newEntity_Compliance.Name = eachEntity.Location.Trim();

                                                    //Address
                                                    if (!string.IsNullOrEmpty(customerRecord.Address))
                                                        newEntity_Compliance.AddressLine1 = customerRecord.Address;

                                                    //Contact Person
                                                    newEntity_Compliance.ContactPerson = customerRecord.BuyerName;

                                                    //Landline                                            
                                                    newEntity_Compliance.Landline = customerRecord.BuyerContactNumber;

                                                    //EmailID
                                                    newEntity_Compliance.EmailID = customerRecord.BuyerEmail;

                                                    //StateID
                                                    newEntity_Compliance.StateID = -1;

                                                    //CityID
                                                    newEntity_Compliance.CityID = -1;

                                                    mst_CustomerBranch newEntity_Audit = new mst_CustomerBranch()
                                                    {
                                                        CustomerID = newEntity_Compliance.CustomerID,

                                                        Status = newEntity_Compliance.Status,
                                                        IsDeleted = newEntity_Compliance.IsDeleted,
                                                        ParentID = newEntity_Compliance.ParentID,
                                                        Type = newEntity_Compliance.Type,
                                                        LegalRelationShipOrStatus = newEntity_Compliance.LegalRelationShipOrStatus,
                                                        ComType = newEntity_Compliance.ComType,
                                                        Industry = newEntity_Compliance.Industry,

                                                        Name = newEntity_Compliance.Name,
                                                        AddressLine1 = newEntity_Compliance.AddressLine1,
                                                        ContactPerson = newEntity_Compliance.ContactPerson,
                                                        Landline = newEntity_Compliance.Landline,
                                                        EmailID = newEntity_Compliance.EmailID,
                                                        StateID = newEntity_Compliance.StateID,
                                                        CityID = newEntity_Compliance.CityID,
                                                    };

                                                    if (newEntity_Compliance.CustomerID != 0)
                                                    {
                                                        int newCustomerBranchID_Compliance = 0;
                                                        int newCustomerBranchID_audit = 0;

                                                        newCustomerBranchID_Compliance = RLCS_Master_Management.Create_CustomerBranch_Compliance(newEntity_Compliance);

                                                        if (newCustomerBranchID_Compliance != 0)
                                                        {
                                                            newCustomerBranchID_audit = RLCS_Master_Management.Create_CustomerBranch_Audit(newEntity_Audit);

                                                            if (newCustomerBranchID_audit != 0)
                                                            {
                                                                entityBranchSuccess = true;
                                                                parentID = newCustomerBranchID_Compliance;

                                                                RLCS_Master_Management.Update_CustomerAssessmentDetails(customerID, eachEntity.BranchID, newCustomerBranchID_Compliance);

                                                                #region Create-Update RLCS Client

                                                                RLCS_CustomerBranch_ClientsLocation_Mapping rlcsEntityRecord = new RLCS_CustomerBranch_ClientsLocation_Mapping()
                                                                {
                                                                    BranchType = "E",

                                                                    CO_CorporateID = corpID,
                                                                    CM_ClientID = clientID,

                                                                    AVACOM_CustomerID = customerID,
                                                                    AVACOM_BranchID = newCustomerBranchID_Compliance,

                                                                    AVACOM_BranchName = newEntity_Compliance.Name,
                                                                    CM_ClientName = newEntity_Compliance.Name,

                                                                    CM_Address = newEntity_Compliance.AddressLine1,
                                                                    CM_State = eachEntity.State
                                                                };

                                                                if (newEntity_Compliance.Status == 1)
                                                                    rlcsEntityRecord.CM_Status = "A";
                                                                else
                                                                    rlcsEntityRecord.CM_Status = "I";

                                                                RLCSManagement.CreateUpdate_Partial_CustomerBranch_ClientsOrLocation_Mapping(rlcsEntityRecord);

                                                                #endregion
                                                            }
                                                            else
                                                            {
                                                                RLCS_Master_Management.DeleteCustomerBranch_Compliance(newCustomerBranchID_Compliance);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            LoggerMessage.InsertErrorMsg_DBLog("Error-EntityCreate-" + eachEntity.Location.Trim() + "-CustomerID-" + customerID, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                        }
                                                    }

                                                    #endregion

                                                    #region Branch

                                                    if (parentID != 0 && entityBranchSuccess)
                                                    {
                                                        var branchRecords = entityBranchRecords.Where(row => row.ParentBranchID == eachEntity.BranchID).ToList();

                                                        if (branchRecords.Count > 0)
                                                        {
                                                            branchRecords.ForEach(eachBranch =>
                                                            {
                                                                CustomerBranch newCustomerBranch_Compliance = new CustomerBranch()
                                                                {
                                                                    Type = 2,
                                                                    ComType = 0,

                                                                    Status = 0,
                                                                    IsDeleted = false,
                                                                    LegalRelationShipOrStatus = 0,

                                                                    Name = eachBranch.Location.Trim(),
                                                                    ContactPerson = newEntity_Compliance.ContactPerson,
                                                                    Landline = newEntity_Compliance.Landline,
                                                                    Mobile = newEntity_Compliance.Mobile,
                                                                    EmailID = newEntity_Compliance.EmailID,
                                                                    CustomerID = customerID,
                                                                };

                                                                newCustomerBranch_Compliance.AddressLine1 = " ";
                                                                newCustomerBranch_Compliance.CityID = -1;

                                                                var matchedState = entities.RLCS_State_Mapping.Where(row => row.SM_Code.Trim().ToUpper().Equals(eachBranch.State.Trim().ToUpper())).FirstOrDefault();

                                                                if (matchedState != null)
                                                                    newCustomerBranch_Compliance.StateID = matchedState.AVACOM_StateID;
                                                                else
                                                                    newCustomerBranch_Compliance.StateID = -1;

                                                                newCustomerBranch_Compliance.ParentID = parentID;

                                                                if (newCustomerBranch_Compliance.CustomerID != 0)
                                                                {
                                                                    mst_CustomerBranch newCustBranch_Audit = new mst_CustomerBranch()
                                                                    {
                                                                        Name = newCustomerBranch_Compliance.Name,

                                                                        Type = newCustomerBranch_Compliance.Type,
                                                                        ComType = newCustomerBranch_Compliance.ComType,
                                                                        AddressLine1 = newCustomerBranch_Compliance.AddressLine1,
                                                                        AddressLine2 = newCustomerBranch_Compliance.AddressLine2,
                                                                        StateID = newCustomerBranch_Compliance.StateID,
                                                                        CityID = newCustomerBranch_Compliance.CityID,

                                                                        ContactPerson = newCustomerBranch_Compliance.ContactPerson,
                                                                        Landline = newCustomerBranch_Compliance.Landline,
                                                                        Mobile = newCustomerBranch_Compliance.Mobile,
                                                                        EmailID = newCustomerBranch_Compliance.EmailID,
                                                                        CustomerID = newCustomerBranch_Compliance.CustomerID,
                                                                        ParentID = newCustomerBranch_Compliance.ParentID,
                                                                        Status = newCustomerBranch_Compliance.Status,

                                                                        IsDeleted = newCustomerBranch_Compliance.IsDeleted,
                                                                        LegalRelationShipOrStatus = newCustomerBranch_Compliance.LegalRelationShipOrStatus,
                                                                    };

                                                                    int newCustomerBranchID_Compliance = 0;
                                                                    int newCustomerBranchID_audit = 0;

                                                                    newCustomerBranchID_Compliance = RLCS_Master_Management.Create_CustomerBranch_Compliance(newCustomerBranch_Compliance);

                                                                    if (newCustomerBranchID_Compliance != 0)
                                                                    {
                                                                        newCustomerBranchID_audit = RLCS_Master_Management.Create_CustomerBranch_Audit(newCustBranch_Audit);

                                                                        if (newCustomerBranchID_audit != 0)
                                                                        {
                                                                            entityBranchSuccess = true;
                                                                            RLCS_Master_Management.Update_CustomerAssessmentDetails(customerID, eachBranch.BranchID, newCustomerBranchID_Compliance);

                                                                            RLCS_CustomerBranch_ClientsLocation_Mapping rlcsBranchRecord = new RLCS_CustomerBranch_ClientsLocation_Mapping()
                                                                            {
                                                                                CO_CorporateID = corpID,
                                                                                CM_ClientID = clientID,

                                                                                BranchType = "B",

                                                                                AVACOM_CustomerID = customerID,
                                                                                AVACOM_BranchID = newCustomerBranchID_Compliance,

                                                                                AVACOM_BranchName = newCustomerBranch_Compliance.Name,
                                                                                CM_ClientName = newCustomerBranch_Compliance.Name,

                                                                                CM_EstablishmentType = eachBranch.LocationType,
                                                                                CM_State = eachBranch.State,
                                                                                CL_PT_State = eachBranch.State,
                                                                            };

                                                                            if (!string.IsNullOrEmpty(eachBranch.LocationType))
                                                                            {
                                                                                if (eachBranch.LocationType.Trim().ToUpper().Equals("SEA"))
                                                                                    rlcsBranchRecord.CL_OfficeType = "Branch";
                                                                                if (eachBranch.LocationType.Trim().ToUpper().Equals("FACT"))
                                                                                    rlcsBranchRecord.CL_OfficeType = "Factory";
                                                                            }

                                                                            if (newCustomerBranch_Compliance.Status == 1)
                                                                                rlcsBranchRecord.CM_Status = "A";
                                                                            else
                                                                                rlcsBranchRecord.CM_Status = "I";

                                                                            RLCSManagement.CreateUpdate_Partial_CustomerBranch_ClientsOrLocation_Mapping(rlcsBranchRecord);
                                                                        }
                                                                        else
                                                                        {
                                                                            RLCS_Master_Management.DeleteCustomerBranch_Compliance(newCustomerBranchID_Compliance);
                                                                        }
                                                                    }

                                                                }
                                                            });
                                                        }
                                                    }

                                                    #endregion
                                                }
                                                else
                                                {
                                                    LoggerMessage.InsertErrorMsg_DBLog("GenerateClientID-Blank-" + eachEntity.Location.Trim() + "-CustomerID-" + customerID, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                }
                                            });
                                        }

                                        if (entityBranchSuccess)
                                        {
                                            CustomerManagement.UpdateCustomerStatus(customerID, 1);
                                            CustomerManagementRisk.UpdateCustomerStatus(customerID, 1);

                                            Page.ClientScript.RegisterStartupScript(this.GetType(), "redirecttoMaster", "RedirectToEntityBranch("+ customerID + ");", true);

                                            //vsPricingTab.CssClass = "alert alert-success";
                                            //cvPricingTab.IsValid = false;
                                            //cvPricingTab.ErrorMessage = "Customer Details Save Successfully, Check in Master-> Customers and fill the additional required details";

                                            //Page.ClientScript.RegisterStartupScript(this.GetType(), "showPricing", "ShowPricingTab();", true);
                                        }
                                    }
                                    else
                                    {
                                        LoggerMessage.InsertErrorMsg_DBLog("NoEntityClientFound-CustomerID-" + customerID, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                else
                                {
                                    LoggerMessage.InsertErrorMsg_DBLog("NoEntityClientFound-CustomerID-" + customerID, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                            }
                            else
                            {
                                LoggerMessage.InsertErrorMsg_DBLog("NoClientBranchFound-CustomerID-" + customerID, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }
                        else
                        {
                            LoggerMessage.InsertErrorMsg_DBLog("NoClientBranchFound-CustomerID-" + customerID, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    else
                        LoggerMessage.InsertErrorMsg_DBLog("NoCustomerFound-" + customerID + " -CorpID-Blank", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    LoggerMessage.InsertErrorMsg_DBLog("Blank-hdnCustID", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void ClearControls()
        {
            try
            {
                txtCustName.Text = string.Empty;
                txtBuyerName.Text = string.Empty;
                txtContactNumber.Text = string.Empty;

                txtEmail.Text = string.Empty;
                txtAddress.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string GenerateCorpID(string corpName)
        {
            string corpID = string.Empty;
            bool generateCorpIDSuccess = false;
            try
            {
                int i = 0;
                string rlcsAPIURL = string.Empty;

                do
                {
                    corpID = GetRandomString(corpName, 5);

                    rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                    rlcsAPIURL += "AventisIntegration/CheckCorporateIdExists?CorporateId=" + corpID.Trim();

                    string responseData = WebAPIUtility.Invoke("GET", rlcsAPIURL, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        bool corpExists = Convert.ToBoolean(responseData.Trim());

                        if (corpExists)
                        {
                            generateCorpIDSuccess = false;
                        }
                        else
                        {
                            corpExists = RLCS_Master_Management.Exists_CorporateID(corpID);
                            if (corpExists)
                            {
                                generateCorpIDSuccess = false;
                            }
                            else
                            {
                                generateCorpIDSuccess = true;
                            }
                        }
                    }

                    i++;
                } while (generateCorpIDSuccess == false && i < 10);

                if (generateCorpIDSuccess)
                    return corpID;
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public string GenerateClientID(string clientName)
        {
            string clientID = string.Empty;
            bool generateClientIDSuccess = false;
            try
            {
                int i = 0;
                string rlcsAPIURL = string.Empty;
                bool clientExists = false;

                do
                {
                    clientExists = false;
                    clientID = GetRandomString(clientName, 7);

                    rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                    rlcsAPIURL += "AventisIntegration/CheckClientIdExists?ClientId=" + clientID.Trim();

                    string responseData = RLCSAPIClasses.Invoke("GET", rlcsAPIURL, "");

                    if (responseData != null)
                    {
                        string data = responseData;

                        if (!string.IsNullOrWhiteSpace(data))
                            clientExists = Convert.ToBoolean(data);
                        else
                            clientExists = false;

                        if (clientExists)
                        {
                            generateClientIDSuccess = false;
                        }
                        else
                        {
                            clientExists = RLCS_Master_Management.Exists_ClientID(clientID.Trim());

                            if (clientExists)
                                generateClientIDSuccess = false;
                            else
                                generateClientIDSuccess = true;
                        }
                    }

                    i++;
                } while (generateClientIDSuccess == false && i < 10);

                if (generateClientIDSuccess)
                    return clientID;
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public static String GetRandomString(String text, int length)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;

            do
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));

                if (text.ToUpper().Contains(ch))
                    builder.Append(ch);
            } while (builder.Length < length);

            return "AVA" + builder.ToString();
        }

        #region

        private void SetInitialRow()
        {
            DataTable dt;
            for (int i = 0; i < 3; i++)
            {
                dt = new DataTable(Convert.ToString(i));
                DataRow dr = null;
                dt.Columns.Add(new DataColumn("SrNo", typeof(string)));
                dt.Columns.Add(new DataColumn("Type", typeof(string)));
                dt.Columns.Add(new DataColumn("Location", typeof(string)));
                dt.Columns.Add(new DataColumn("State", typeof(string)));
                dt.Columns.Add(new DataColumn("EmpCount", typeof(string)));
                dt.Columns.Add(new DataColumn("MinBasic", typeof(string)));
                dt.Columns.Add(new DataColumn("MinGross", typeof(string)));
                dt.Columns.Add(new DataColumn("MaxGross", typeof(string)));
                dt.Columns.Add(new DataColumn("BranchID", typeof(string)));

                dr = dt.NewRow();
                dr["SrNo"] = 1;
                dr["Type"] = "0";
                dr["Location"] = string.Empty;
                dr["State"] = "0";
                dr["EmpCount"] = string.Empty;
                dr["MinBasic"] = string.Empty;
                dr["MinGross"] = string.Empty;
                dr["MaxGross"] = string.Empty;
                dr["BranchID"] = string.Empty;
                dt.Rows.Add(dr);

                ViewState["CurrentTable"] = dt;
                Gridview1.DataSource = dt;
                Gridview1.DataBind();
            }

            ImageButton lb = (ImageButton)Gridview1.Rows[0].FindControl("lnkBtnDelete");
            lb.Enabled = false;
        }

        public void AddNewRowToGrid()
        {

        }

        private void SetPreviousData()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows.Count == 1)
                    {
                        ImageButton lb = (ImageButton)Gridview1.Rows[0].FindControl("lnkBtnDelete");
                        lb.Enabled = false;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("TextBox1");
                        DropDownList ddlType = (DropDownList)Gridview1.Rows[rowIndex].FindControl("ddlType");
                        TextBox txtBranchLocation = (TextBox)Gridview1.Rows[rowIndex].FindControl("txtBranchLocation");
                        DropDownList ddlState = (DropDownList)Gridview1.Rows[rowIndex].FindControl("ddlState");

                        TextBox txtEmpCount = (TextBox)Gridview1.Rows[rowIndex].FindControl("txtEmpCount");
                        TextBox txtMinBasic = (TextBox)Gridview1.Rows[rowIndex].FindControl("txtMinBasic");
                        TextBox txtMinGross = (TextBox)Gridview1.Rows[rowIndex].FindControl("txtMinGross");
                        TextBox txtMaxGross = (TextBox)Gridview1.Rows[rowIndex].FindControl("txtMaxGross");
                        HiddenField hdnBranchId = (HiddenField)Gridview1.Rows[rowIndex].FindControl("hdnGridBranchId");
                        //box1.Text = dt.Rows[i]["Column1"].ToString();
                        ddlType.SelectedValue = dt.Rows[i]["Type"].ToString();
                        txtBranchLocation.Text = dt.Rows[i]["Location"].ToString();
                        ddlState.SelectedValue = dt.Rows[i]["State"].ToString();
                        txtEmpCount.Text = dt.Rows[i]["EmpCount"].ToString();
                        txtMinBasic.Text = dt.Rows[i]["MinBasic"].ToString();
                        txtMinGross.Text = dt.Rows[i]["MinGross"].ToString();
                        txtMaxGross.Text = dt.Rows[i]["MaxGross"].ToString();
                        hdnBranchId.Value = dt.Rows[i]["BranchID"].ToString();

                        rowIndex++;
                    }
                }
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                string a = "";
            }

            Questionnaire.Style.Add("display", "block");
            divtreelist.Style.Add("display", "none");
            addQuestionnaire.Value = "Cancel";
            int rowIndex = 0;
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        DropDownList ddlType = (DropDownList)Gridview1.Rows[i - 1].FindControl("ddlType");
                        DropDownList ddlState = (DropDownList)Gridview1.Rows[i - 1].FindControl("ddlState");
                        TextBox txtBranchLocation = (TextBox)Gridview1.Rows[i - 1].FindControl("txtBranchLocation");
                        TextBox txtEmpCount = (TextBox)Gridview1.Rows[i - 1].FindControl("txtEmpCount");
                        TextBox txtMinBasic = (TextBox)Gridview1.Rows[i - 1].FindControl("txtMinBasic");
                        TextBox txtMinGross = (TextBox)Gridview1.Rows[i - 1].FindControl("txtMinGross");
                        TextBox txtMaxGross = (TextBox)Gridview1.Rows[i - 1].FindControl("txtMaxGross");
                        HiddenField hdnBranchId = (HiddenField)Gridview1.Rows[i - 1].FindControl("hdnGridBranchId");


                        dtCurrentTable.Rows[i - 1]["Type"] = ddlType.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["Location"] = string.IsNullOrEmpty(Convert.ToString(txtBranchLocation.Text)) ? "" : Convert.ToString(txtBranchLocation.Text);
                        dtCurrentTable.Rows[i - 1]["State"] = ddlState.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["EmpCount"] = string.IsNullOrEmpty(Convert.ToString(txtEmpCount.Text)) ? "" : Convert.ToString(txtEmpCount.Text);
                        dtCurrentTable.Rows[i - 1]["MinBasic"] = string.IsNullOrEmpty(Convert.ToString(txtMinBasic.Text)) ? "0" : Convert.ToString(txtMinBasic.Text);
                        dtCurrentTable.Rows[i - 1]["MinGross"] = string.IsNullOrEmpty(Convert.ToString(txtMinGross.Text)) ? "0" : Convert.ToString(txtMinGross.Text);
                        dtCurrentTable.Rows[i - 1]["MaxGross"] = string.IsNullOrEmpty(Convert.ToString(txtMaxGross.Text)) ? "0" : Convert.ToString(txtMaxGross.Text);
                        dtCurrentTable.Rows[i - 1]["BranchID"] = string.IsNullOrEmpty(Convert.ToString(hdnBranchId.Value)) ? "0" : Convert.ToString(hdnBranchId.Value);

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    // drCurrentRow["SrNo"] = i + 1;

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    Gridview1.DataSource = dtCurrentTable;
                    Gridview1.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {

                //Val
                addQuestionnaire.Value = "Cancel";
                ImageButton btn = (ImageButton)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                string hdnbranchid = (gvr.FindControl("hdnGridBranchId") as HiddenField).Value;
                int branchid = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(hdnbranchid)) ? 0 : Convert.ToInt32(hdnbranchid));
                if (branchid > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {


                        IQueryable<RLCS_AssesmentVM> childbranchs = (from row in entities.RLCS_CustomerAssessmentDetails
                                                                     where row.BranchID == branchid
                                                                     select new RLCS_AssesmentVM
                                                                     {
                                                                         BranchID = row.BranchID,
                                                                         CustomerID = row.CustomerID,
                                                                         IsActive = row.IsActive
                                                                     });


                        var branchs = childbranchs.ToList();
                        foreach (var branch in branchs)
                        {
                            RLCS_CustomerAssessmentDetails customerassessmentdetail = new RLCS_CustomerAssessmentDetails();
                            customerassessmentdetail = (from row in entities.RLCS_CustomerAssessmentDetails
                                                        where row.CustomerID == branch.CustomerID
                                                        && row.BranchID == branch.BranchID
                                                        select row).FirstOrDefault();

                            customerassessmentdetail.IsActive = false;
                            customerassessmentdetail.ModifiedBy = customerID;
                            customerassessmentdetail.ModifiedDate = DateTime.Now;

                            entities.SaveChanges();
                        }
                    }

                }

                //End
                Questionnaire.Style.Add("display", "block");
                divtreelist.Style.Add("display", "none");

                ImageButton lb = (ImageButton)sender;
                GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
                int rowID = gvRow.RowIndex;
                if (ViewState["CurrentTable"] != null)
                {
                    int rowIndex = 0;
                    DataTable dt = (DataTable)ViewState["CurrentTable"];

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 1; i <= dt.Rows.Count; i++)
                        {

                            DropDownList ddlType = (DropDownList)Gridview1.Rows[i - 1].FindControl("ddlType");
                            DropDownList ddlState = (DropDownList)Gridview1.Rows[i - 1].FindControl("ddlState");
                            TextBox txtBranchLocation = (TextBox)Gridview1.Rows[i - 1].FindControl("txtBranchLocation");
                            TextBox txtEmpCount = (TextBox)Gridview1.Rows[i - 1].FindControl("txtEmpCount");
                            TextBox txtMinBasic = (TextBox)Gridview1.Rows[i - 1].FindControl("txtMinBasic");
                            TextBox txtMinGross = (TextBox)Gridview1.Rows[i - 1].FindControl("txtMinGross");
                            TextBox txtMaxGross = (TextBox)Gridview1.Rows[i - 1].FindControl("txtMaxGross");
                            HiddenField hdnBranchId = (HiddenField)Gridview1.Rows[i - 1].FindControl("hdnGridBranchId");


                            dt.Rows[i - 1]["Type"] = ddlType.SelectedValue;
                            dt.Rows[i - 1]["Location"] = string.IsNullOrEmpty(Convert.ToString(txtBranchLocation.Text)) ? "" : Convert.ToString(txtBranchLocation.Text);
                            dt.Rows[i - 1]["State"] = ddlState.SelectedValue;
                            dt.Rows[i - 1]["EmpCount"] = string.IsNullOrEmpty(Convert.ToString(txtEmpCount.Text)) ? "" : Convert.ToString(txtEmpCount.Text);
                            dt.Rows[i - 1]["MinBasic"] = string.IsNullOrEmpty(Convert.ToString(txtMinBasic.Text)) ? "0" : Convert.ToString(txtMinBasic.Text);
                            dt.Rows[i - 1]["MinGross"] = string.IsNullOrEmpty(Convert.ToString(txtMinGross.Text)) ? "0" : Convert.ToString(txtMinGross.Text);
                            dt.Rows[i - 1]["MaxGross"] = string.IsNullOrEmpty(Convert.ToString(txtMaxGross.Text)) ? "0" : Convert.ToString(txtMaxGross.Text);
                            dt.Rows[i - 1]["BranchID"] = string.IsNullOrEmpty(Convert.ToString(hdnBranchId.Value)) ? "0" : Convert.ToString(hdnBranchId.Value);
                            rowIndex++;
                        }
                    }
                    if (dt.Rows.Count > 1)
                    {
                        if (gvRow.RowIndex <= dt.Rows.Count - 1)
                        {
                            //Remove the Selected Row data and reset row number  
                            dt.Rows.Remove(dt.Rows[rowID]);
                            ResetRowID(dt);
                        }
                    }

                    //Store the current data in ViewState for future reference  
                    ViewState["CurrentTable"] = dt;

                    //Re bind the GridView for the updated data  
                    Gridview1.DataSource = dt;
                    Gridview1.DataBind();
                }

                ////Set Previous Data on Postbacks  
                //SetPreviousData();
            }
        }

        private void ResetRowID(DataTable dt)
        {
            int rowNumber = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row[0] = rowNumber;
                    rowNumber++;
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Button1.Enabled = false;
            if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
            {
                customerID = Convert.ToInt32(hdnCustID.Value);

                int rowIndex = 0;
                StringCollection sc = new StringCollection();
                List<RLCS_CustomerAssessmentDetails> customerdetails = new List<RLCS_CustomerAssessmentDetails>();

                if (ViewState["CurrentTable"] != null)
                {
                    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        RLCS_CustomerAssessmentDetails customerdetail = new RLCS_CustomerAssessmentDetails();
                        customerdetail.CustomerID = customerID;
                        customerdetail.Location = TextBox1.Text;
                        customerdetail.BranchType = "E";
                        customerdetail.BranchID = string.IsNullOrEmpty(Convert.ToString(hdnparentBranchId.Value)) ? 0 : Convert.ToInt32(hdnparentBranchId.Value);
                        if (customerdetail.BranchID == 0)
                        {
                            customerdetail.BranchID = string.IsNullOrEmpty(Convert.ToString(hdnBranchId.Value)) ? 0 : Convert.ToInt32(hdnBranchId.Value);
                        }
                        customerdetails.Add(customerdetail);

                        for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                        {
                            customerdetail = new RLCS_CustomerAssessmentDetails();

                            //extract the TextBox values  
                            DropDownList ddlType = (DropDownList)Gridview1.Rows[rowIndex].FindControl("ddlType");
                            TextBox txtBranchLocation = (TextBox)Gridview1.Rows[rowIndex].FindControl("txtBranchLocation");
                            DropDownList ddlState = (DropDownList)Gridview1.Rows[rowIndex].FindControl("ddlState");
                            TextBox txtEmpCount = (TextBox)Gridview1.Rows[rowIndex].FindControl("txtEmpCount");
                            TextBox txtMinBasic = (TextBox)Gridview1.Rows[rowIndex].FindControl("txtMinBasic");
                            TextBox txtMinGross = (TextBox)Gridview1.Rows[rowIndex].FindControl("txtMinGross");
                            TextBox txtMaxGross = (TextBox)Gridview1.Rows[rowIndex].FindControl("txtMaxGross");
                            HiddenField hdnBranchId = (HiddenField)Gridview1.Rows[rowIndex].FindControl("hdnGridBranchId");

                            //get the values from TextBox and DropDownList  
                            //then add it to the collections with a comma "," as the delimited values  

                            customerdetail.BranchType = "B";
                            customerdetail.LocationType = ddlType.SelectedValue;
                            customerdetail.Location = txtBranchLocation.Text;
                            customerdetail.State = ddlState.SelectedValue;

                            if (!String.IsNullOrEmpty(txtEmpCount.Text))
                                customerdetail.EmployeeCount = Convert.ToInt32(txtEmpCount.Text);

                            if (!String.IsNullOrEmpty(txtMinBasic.Text))
                                customerdetail.MinimumBasicSalary = Convert.ToDecimal(txtMinBasic.Text);

                            if (!String.IsNullOrEmpty(txtMinGross.Text))
                                customerdetail.MinimumGrossSalary = Convert.ToDecimal(txtMinGross.Text);

                            if (!String.IsNullOrEmpty(txtMaxGross.Text))
                                customerdetail.MaximumGrossSalary = Convert.ToDecimal(txtMaxGross.Text);

                            customerdetail.CustomerID = customerID;
                            customerdetail.BranchID = string.IsNullOrEmpty(Convert.ToString(hdnBranchId.Value)) ? 0 : Convert.ToInt32(hdnBranchId.Value);
                            customerdetail.ParentBranchID = string.IsNullOrEmpty(Convert.ToString(hdnparentBranchId.Value)) ? 0 : Convert.ToInt32(hdnparentBranchId.Value);

                            customerdetail.CreatedBy = AuthenticationHelper.UserID;
                            customerdetail.ModifiedBy = AuthenticationHelper.UserID;

                            customerdetails.Add(customerdetail);
                            rowIndex++;
                        }

                        //Call the method for executing inserts  
                        InsertRecords(customerdetails);
                    }

                }

                //DataTable dts = (DataTable)ViewState["CurrentTable"];
            }
            if (Convert.ToString(Session["Save"]) == "Save")
            {
                addQuestionnaire.Value = "Add New";
                ClearControl();
                Session["Save"] = "";
                string JsMethodName = "MSGAlert('7')";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", JsMethodName, true);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Entity Save Successfully";
                cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";

            }
            else if (Convert.ToString(Session["Save"]) == "Update")
            {
                addQuestionnaire.Value = "Add New";
                ClearControl();
                Session["Save"] = "";
                string JsMethodName = "MSGAlert('7')";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", JsMethodName, true);

                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Entity Updated Successfully";
                cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";

            }
        }

        private void ClearControl()
        {
            Button1.Enabled = true;
            Gridview1.DataSource = null;
            Gridview1.DataBind();
            ViewState["CurrentTable"] = null;
            SetInitialRow();

            hdnBranchId.Value = "";
            hdnparentBranchId.Value = "";
            hdnQuestionarycustomerId.Value = "";
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            //Panel p = new Panel();
            int numOfGroups = 0;
            //p.ID = "GridView" + (pnlResult.Controls.Count + 1);
            //pnlResult.Controls.Add(p);

            if (numOfGroups > 0)
            {

                // DataTable dts = (DataTable)ViewState["CurrentTable"];
                for (int i = 0; i < numOfGroups; i++)
                {
                    Panel p = new Panel();
                    GridView grdvc1 = new GridView();
                    TextBox t = new TextBox();
                    DropDownList d = new DropDownList();
                    DataTable dts = (DataTable)ViewState["CurrentTable"];

                    DataTable dt = new DataTable();
                    TableCell cell = new TableCell();
                    //grdvc1.Rows[0].Cells[0].Controls.Add(t);
                    DataRow dr = null;
                    dt.Columns.Add(new DataColumn("SrNo", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column1", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column2", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column3", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column4", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column5", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column6", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column7", typeof(string)));
                    dr = dt.NewRow();
                    dr["SrNo"] = 1;
                    dr["Column1"] = "Name";
                    dr["Column2"] = "Desig";
                    dr["Column3"] = "";
                    dr["Column4"] = string.Empty;
                    dr["Column5"] = string.Empty;
                    dr["Column6"] = string.Empty;
                    dr["Column7"] = string.Empty;

                    dt.Rows.Add(dr);
                    grdvc1.DataSource = dt;
                    grdvc1.DataBind();
                    grdvc1.Rows[0].Cells[1].Controls.Add(t);
                    grdvc1.Rows[0].Cells[2].Controls.Add(d);
                    grdvc1.Rows[0].Cells[3].Controls.Add(t);
                    grdvc1.Rows[0].Cells[4].Controls.Add(d);
                    grdvc1.Rows[0].Cells[5].Controls.Add(t);
                    grdvc1.Rows[0].Cells[6].Controls.Add(t);
                    grdvc1.Rows[0].Cells[7].Controls.Add(t);

                    Panel1.Controls.Add(grdvc1);
                    //  this.Controls.Add(p);
                }

            }

        }

        private bool LocationValidation(List<RLCS_CustomerAssessmentDetails> customerdetails)
        {
            bool PLocation = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var x in customerdetails)
                {
                    PLocation = (from row in entities.RLCS_CustomerAssessmentDetails
                                 where row.BranchID != x.BranchID
                                 && row.CustomerID == x.CustomerID
                                 && row.Location == x.Location
                                 && row.BranchType == x.BranchType
                                 && row.IsActive == true
                                 select row.Location).Any();
                    if (PLocation == true)
                    {
                        Button1.Enabled = true;
                        Session["Save"] = "";
                        hdnLocationVal.Value = x.Location;
                        string JsMethodName = "MSGAlert('Location')";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", JsMethodName, true);
                        break;
                    }

                }
                return PLocation;
            }
        }

        private void InsertRecords(List<RLCS_CustomerAssessmentDetails> customerdetails)
        {
            try
            {
                StringBuilder sb = new StringBuilder(string.Empty);
                int parentBranchID = 0;
                if (LocationValidation(customerdetails) == false)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        foreach (var x in customerdetails)
                        {
                            List<RLCS_CustomerAssessmentDetails> customers = new List<RLCS_CustomerAssessmentDetails>();
                            customers = (from row in entities.RLCS_CustomerAssessmentDetails
                                         where customerID == x.CustomerID
                                         && row.BranchType == x.BranchType
                                         && row.Location == x.Location
                                         && row.BranchID != x.BranchID
                                         && row.ParentBranchID == x.ParentBranchID
                                         && row.IsActive == true
                                         select row).ToList();

                            if (customers.Count == 0)
                            {

                                RLCS_CustomerAssessmentDetails customerassessmentdetail = new RLCS_CustomerAssessmentDetails();
                                if (x.BranchID > 0)
                                {
                                    customerassessmentdetail = (from row in entities.RLCS_CustomerAssessmentDetails
                                                                where customerID == x.CustomerID
                                                                && row.BranchID == x.BranchID
                                                                select row).FirstOrDefault();
                                }
                                else
                                {
                                    customerassessmentdetail = new RLCS_CustomerAssessmentDetails();
                                }
                                if (parentBranchID > 0)
                                {
                                    customerassessmentdetail.ParentBranchID = Convert.ToInt32(parentBranchID);
                                }
                                else
                                {
                                    customerassessmentdetail.ParentBranchID = string.IsNullOrEmpty(Convert.ToString(x.ParentBranchID)) ? 0 : Convert.ToInt32(x.ParentBranchID);//;
                                }
                                //customerassessmentdetail.ParentBranchID = string.IsNullOrEmpty(Convert.ToString(x.ParentBranchID)) ? 0 : Convert.ToInt32(x.ParentBranchID);//;
                                customerassessmentdetail.BranchID = x.BranchID;
                                customerassessmentdetail.CustomerID = customerID;
                                customerassessmentdetail.BranchType = x.BranchType;
                                customerassessmentdetail.Location = x.Location;
                                customerassessmentdetail.LocationType = x.LocationType;
                                customerassessmentdetail.State = x.State;
                                customerassessmentdetail.EmployeeCount = x.EmployeeCount;
                                customerassessmentdetail.MinimumBasicSalary = x.MinimumBasicSalary;
                                customerassessmentdetail.MinimumGrossSalary = x.MinimumGrossSalary;
                                customerassessmentdetail.MaximumGrossSalary = x.MaximumGrossSalary;
                                customerassessmentdetail.IsActive = true;
                                customerassessmentdetail.ModifiedBy = customerID;
                                customerassessmentdetail.ModifiedDate = DateTime.Now;
                                Session["Save"] = "Update";
                                if (x.BranchID == 0)
                                {
                                    Session["Save"] = "Save";
                                    customerassessmentdetail.CreatedBy = customerID;
                                    customerassessmentdetail.CreatedDate = DateTime.Now;

                                    entities.RLCS_CustomerAssessmentDetails.Add(customerassessmentdetail);
                                }

                                entities.SaveChanges();

                                if (customerassessmentdetail.BranchType == "E")
                                {
                                    parentBranchID = customerassessmentdetail.BranchID;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Session["Save"] = "";
            }

        }

        public void BindData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_CustomerAssessmentDetails customer = new RLCS_CustomerAssessmentDetails();
                GridView2.DataSource = entities.RLCS_CustomerAssessmentDetails.ToList();
                GridView2.DataBind();
            }
        }

        protected void Gridview1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList State = (e.Row.FindControl("ddlState") as DropDownList);
                    string statevalue = (string)DataBinder.Eval(e.Row.DataItem, "State");
                    State.SelectedIndex = State.Items.IndexOf(State.Items.FindByValue(statevalue));
                    // State.SelectedValue = statevalue;
                    DropDownList Type = (e.Row.FindControl("ddlType") as DropDownList);
                    string Typevalue = (string)DataBinder.Eval(e.Row.DataItem, "Type");
                    Type.SelectedIndex = Type.Items.IndexOf(Type.Items.FindByValue(Typevalue));


                }

            }
            catch (Exception)
            {


            }
        }

        protected void btnhidden_Click(object sender, EventArgs e)
        {
            btnNextToApplicability.Visible = false;

            addQuestionnaire.Value = "Cancel";

            int customerid = string.IsNullOrEmpty(Convert.ToString(hdnQuestionarycustomerId.Value)) ? 0 : Convert.ToInt32(hdnQuestionarycustomerId.Value);
            int branchid = string.IsNullOrEmpty(Convert.ToString(hdnBranchId.Value)) ? 0 : Convert.ToInt32(hdnBranchId.Value);
            int parentbranchid = string.IsNullOrEmpty(Convert.ToString(hdnparentBranchId.Value)) ? 0 : Convert.ToInt32(hdnparentBranchId.Value);

            var AssesData = RLCS_Assesment.RLCSAssesmentGridBind(customerid, parentbranchid, branchid);
            if (AssesData.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(AssesData[0].parentLocation)))
                {
                    TextBox1.Text = AssesData[0].parentLocation;
                }
                if (parentbranchid == 0)
                {
                    hdnparentBranchId.Value = Convert.ToString(branchid);
                }
                Gridview1.DataSource = AssesData;
                Gridview1.DataBind();
                ViewState["CurrentTable"] = null;
                DataTable DT1 = new DataTable();
                DT1 = AssesData.ToDataTable();
                DT1.Columns.Add(new DataColumn("SrNo", typeof(string)));
                ViewState["CurrentTable"] = DT1;
            }
            else
            {
                SetInitialRow();
                var parentLocation = RLCS_Assesment.RLCSAssesmentParentName(customerid, parentbranchid, branchid);
                TextBox1.Text = Convert.ToString(parentLocation);
            }
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CallMyFunction", "ShowInputs(8);", true);

        }

        protected void btnDeleteQuestionier_Click(object sender, EventArgs e)
        {

            int branchid = string.IsNullOrEmpty(Convert.ToString(hdnBranchId.Value)) ? 0 : Convert.ToInt32(hdnBranchId.Value);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                IQueryable<RLCS_AssesmentVM> masterbranch = (from row in entities.RLCS_CustomerAssessmentDetails
                                                             where row.BranchID == branchid
                                                             select new RLCS_AssesmentVM
                                                             {
                                                                 BranchID = row.BranchID,
                                                                 CustomerID = row.CustomerID,
                                                                 IsActive = row.IsActive
                                                             });

                IQueryable<RLCS_AssesmentVM> childbranchs = (from row in entities.RLCS_CustomerAssessmentDetails
                                                             where row.ParentBranchID == branchid
                                                             select new RLCS_AssesmentVM
                                                             {
                                                                 BranchID = row.BranchID,
                                                                 CustomerID = row.CustomerID,
                                                                 IsActive = row.IsActive
                                                             });


                var branchs = masterbranch.Union(childbranchs).ToList();
                foreach (var branch in branchs)
                {
                    RLCS_CustomerAssessmentDetails customerassessmentdetail = new RLCS_CustomerAssessmentDetails();
                    customerassessmentdetail = (from row in entities.RLCS_CustomerAssessmentDetails
                                                where row.CustomerID == branch.CustomerID
                                                && row.BranchID == branch.BranchID
                                                select row).FirstOrDefault();

                    customerassessmentdetail.IsActive = false;
                    customerassessmentdetail.ModifiedBy = customerID;
                    customerassessmentdetail.ModifiedDate = DateTime.Now;

                    entities.SaveChanges();
                }
            }

            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "ShowInputs(9);", true);

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = " Entity Deleted Successfully";
            cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";

        }

        #endregion

        #region Payment

        public bool processTxnResponse_PayTM()
        {
            try
            {
                bool txnSuccess = false;

                if (Session["PaymentDetails"] != null)
                {
                    int loggedInUserID = AuthenticationHelper.UserID;

                    string merchantKey = ConfigurationManager.AppSettings["PayTM_merchantKey"];  //Replace the with the Merchant Key provided by Paytm at the time of registration.
                    string paytmChecksum = string.Empty;

                    //CustID, PGID, OrderID, Amount, Phone, Email
                    string[] txnDetails = Session["PaymentDetails"].ToString().Split(',');

                    Dictionary<string, string> paytmParams = new Dictionary<string, string>();

                    foreach (string key in Request.Form.Keys)
                    {
                        paytmParams.Add(key.Trim(), Request.Form[key].Trim());
                    }

                    if (paytmParams.ContainsKey("CHECKSUMHASH"))
                    {
                        paytmChecksum = paytmParams["CHECKSUMHASH"];
                        paytmParams.Remove("CHECKSUMHASH");
                    }

                    int customerID = Convert.ToInt32(txnDetails[0]);
                    string orderID = txnDetails[2]; //Generated By Avantis
                    string txnID = string.Empty; //Generated By PaymentGateway PayTM

                    bool saveSuccess = false;

                    if (CheckSum.verifyCheckSum(merchantKey, paytmParams, paytmChecksum))
                    {
                        if (Request.Form["STATUS"].ToString().ToUpper().Contains("SUCCESS")) //TXN_SUCCESS 
                        {
                            txnSuccess = true;
                        }
                        else  //TXN_FAILURE PENDING OPEN
                        {
                            txnSuccess = false;
                        }
                    }
                    else
                        txnSuccess = false;

                    saveSuccess = saveTxnInfo_PayTM(customerID);

                    //Re-Check Transaction Status
                    PaymentManagement.GetTxnStatus_Paytm(customerID, orderID, loggedInUserID);

                    var paymentDetails = PaymentManagement.GetPaymentOrderDetailsByOrderID(orderID);

                    if (paymentDetails != null)
                    {
                        if (txnDetails.Length > 1)
                        {
                            //ViewBag.orderID = paymentDetails.OrderID;
                            //ViewBag.Amount = paymentDetails.Amount.ToString();

                            //if (paymentDetails.StatusID == 0)
                            //    ViewBag.Status = "Failed";
                            //else if (paymentDetails.StatusID == 1)
                            //    ViewBag.Status = "Success";
                            //else if (paymentDetails.StatusID == 2)
                            //    ViewBag.Status = "Failed";
                            //else if (paymentDetails.StatusID == 3)
                            //    ViewBag.Status = "Pending";
                            //else
                            //    ViewBag.Status = "Failed";

                            //ViewBag.Reason = paymentDetails.Resp_MSG;

                            if (paymentDetails.StatusID != 1)
                                txnSuccess = false;
                        }
                    }
                }

                return txnSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public bool saveTxnInfo_PayTM(int customerID)
        {
            try
            {
                bool saveSuccess = false;

                PaymentTransactionLog newPaymentTxn = new PaymentTransactionLog();

                newPaymentTxn.CustomerID = customerID;
                newPaymentTxn.PGID = 2; //2--PayTM

                //if (!string.IsNullOrEmpty(Request.Form["udf1"]))
                //    newPaymentTxn.UserID = Convert.ToInt32(Request.Form["udf1"]);

                //if (!string.IsNullOrEmpty(Request.Form["udf2"]))
                //    newPaymentTxn.TaxDetailRecordID = Convert.ToInt32(Request.Form["udf2"]);

                if (!string.IsNullOrEmpty(Request.Form["TXNAMOUNT"]))
                    newPaymentTxn.Amount = Convert.ToDecimal(Request.Form["TXNAMOUNT"]);

                if (!string.IsNullOrEmpty(Request.Form["ORDERID"]))
                    newPaymentTxn.OrderID = Convert.ToString(Request.Form["ORDERID"]);

                if (!string.IsNullOrEmpty(Request.Form["TXNID"]))
                    newPaymentTxn.TxnID = Convert.ToString(Request.Form["TXNID"]);

                if (!string.IsNullOrEmpty(Request.Form["BANKTXNID"]))
                    newPaymentTxn.BankTxnID = Convert.ToString(Request.Form["BANKTXNID"]);

                if (!string.IsNullOrEmpty(Request.Form["RESPCODE"]))
                    newPaymentTxn.Resp_Code = Convert.ToString(Request.Form["RESPCODE"]);

                if (!string.IsNullOrEmpty(Request.Form["RESPMSG"]))
                    newPaymentTxn.Resp_MSG = Convert.ToString(Request.Form["RESPMSG"]);

                if (!string.IsNullOrEmpty(Request.Form["STATUS"]))
                    newPaymentTxn.Status = Convert.ToString(Request.Form["STATUS"]);

                if (!string.IsNullOrEmpty(Request.Form["TXNDATE"]))
                    newPaymentTxn.TxnDate = Convert.ToDateTime(Request.Form["TXNDATE"]);

                if (!string.IsNullOrEmpty(Request.Form["GATEWAYNAME"]))
                    newPaymentTxn.PG_Type = Convert.ToString(Request.Form["GATEWAYNAME"]);

                if (!string.IsNullOrEmpty(Request.Form["PAYMENTMODE"]))
                    newPaymentTxn.Payment_Mode = Convert.ToString(Request.Form["PAYMENTMODE"]);

                if (newPaymentTxn.Status != null)
                {
                    if (newPaymentTxn.Status.ToUpper().Contains("SUCCESS"))
                        newPaymentTxn.StatusID = 1;
                    else if (newPaymentTxn.Status.ToUpper().Contains("FAILURE"))
                        newPaymentTxn.StatusID = 2;
                    else if (newPaymentTxn.Status.ToUpper().Contains("PENDING"))
                        newPaymentTxn.StatusID = 3;
                    else if (newPaymentTxn.Status.ToUpper().Contains("OPEN"))
                        newPaymentTxn.StatusID = 4;
                }

                newPaymentTxn.IsDeleted = false;

                int statusID = 0;

                if (newPaymentTxn.StatusID != null)
                    statusID = Convert.ToInt32(newPaymentTxn.StatusID);

                saveSuccess = PaymentManagement.CreatePaymentLog(customerID, 2, 1, newPaymentTxn.OrderID, newPaymentTxn.Amount.ToString(), statusID, AuthenticationHelper.UserID); //PGID--2--PayTM, TxnType--1--PaymentRequest
                saveSuccess = PaymentManagement.CreateUpdate_PaymentTransactionLog(newPaymentTxn);

                return saveSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion
    }
}