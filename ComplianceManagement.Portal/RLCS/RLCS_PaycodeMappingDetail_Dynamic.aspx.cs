﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nelibur.ObjectMapper;
using System.Collections;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.IO;
using OfficeOpenXml;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Web.Script.Serialization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_PaycodeMappingDetail_Dynamic : System.Web.UI.Page
    {

        List<SP_Get_ClientPaycodeMappingDetails_Result> listOfPaycode = null;
        private static bool btnAddClick = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
            lblPaycodeSequence.Visible = false;
            lblPaycodeHeader.Visible = false;

            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Edit"]) || !string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    if (Request.QueryString["Edit"] == "YES")
                    {
                        divBtnUploadHeader.Visible = false;
                    }
                    SetEditRow();
                }
                else
                {
                    divBtnUploadHeader.Visible = true;
                    SetInitialRow();
                }
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = "Name";
                    if (!string.IsNullOrEmpty(Request.QueryString["ClientID"]))
                    {
                        var ClientID = Convert.ToString(Request.QueryString["ClientID"]);
                        BindClients(ClientID);
                    }
                    Session["CurrentRole"] = AuthenticationHelper.Role;
                    Session["CurrentUserId"] = AuthenticationHelper.UserID;
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }

        private void BindClients(string ClientID)
        {
            try
            {
                var list = RLCS_ClientsManagement.GetAll_client(ClientID);
                ddlClientList.DataSource = list;
                ddlClientList.DataTextField = "CM_ClientName";
                ddlClientList.DataValueField = "CM_ClientID";
                ddlClientList.DataBind();

                if (list.Count > 0)
                {
                    ddlClientList.SelectedIndex = 1;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void FillDropDownList(DropDownList ddl)
        {
            ArrayList arr = new ArrayList();

            //if (btnAddClick == true)
            //{
            //    arr.Add(new ListItem("Earning", "E"));
            //    arr.Add(new ListItem("Deduction", "D"));
            //}
            //else
            //{
            arr.Add(new ListItem("Earning", "E"));
            arr.Add(new ListItem("Deduction", "D"));
            arr.Add(new ListItem("Standard", "S"));
            //}

            foreach (ListItem item in arr)
            {
                ddl.Items.Add(item);

            }
        }

        private void SetEditRow()
        {
            ViewState["Sorder"] = null;
            ViewState["HeaderTable"] = null;
            int id = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                id = Convert.ToInt32(Request.QueryString["ID"]);
            }
            RLCS_Client_Paycode_Mapping_Details paycodeDetails = RLCS_ClientsManagement.GetMappedPaycodeDetails(id);
            if (paycodeDetails != null)
            {
                DataTable dt = new DataTable();
                DataRow dr = null;

                dt.Columns.Add(new DataColumn("ID", typeof(string)));
                dt.Columns.Add(new DataColumn("HeaderName", typeof(string)));
                dt.Columns.Add(new DataColumn("PaycodeType", typeof(string)));//for DropDownList selected item
                dt.Columns.Add(new DataColumn("Paycode", typeof(string)));  //for DropDownList selected item 
                dt.Columns.Add(new DataColumn("SquanceOrder", typeof(string)));   //for TextBox value   
                dt.Columns.Add(new DataColumn("ESI", typeof(string)));//for checkbox  
                dt.Columns.Add(new DataColumn("PF", typeof(string)));  //for checkbox  
                dt.Columns.Add(new DataColumn("PT", typeof(string)));//for checkbox  
                dt.Columns.Add(new DataColumn("LWF", typeof(string)));  //for checkbox 
                int Sorder = 0;


                dr = dt.NewRow();

                dr["ID"] = paycodeDetails.ID;
                dr["HeaderName"] = paycodeDetails.CPMD_Header;
                if (paycodeDetails.CPMD_Deduction_Type == "S") { dr["PaycodeType"] = "Standard"; }
                else if (paycodeDetails.CPMD_Deduction_Type == "E") { dr["PaycodeType"] = "Earning"; }
                else if (paycodeDetails.CPMD_Deduction_Type == "D") { dr["PaycodeType"] = "Deduction"; }

                if (paycodeDetails.CPMD_Deduction_Type == "S")
                {
                    dr["Paycode"] = paycodeDetails.CPMD_Standard_Column;
                }
                else if (paycodeDetails.CPMD_Deduction_Type == "E" || paycodeDetails.CPMD_Deduction_Type == "D")
                {
                    dr["Paycode"] = paycodeDetails.CPMD_PayCode;
                }
                dr["SquanceOrder"] = paycodeDetails.CPMD_Sequence_Order;

                dr["ESI"] = paycodeDetails.CPMD_appl_ESI;
                dr["PF"] = paycodeDetails.CPMD_appl_PF;
                dr["PT"] = paycodeDetails.CPMD_Appl_PT;
                dr["LWF"] = paycodeDetails.CPMD_Appl_LWF;

                dt.Rows.Add(dr);

                ViewState["Sorder"] = Sorder;
                ViewState["HeaderTable"] = dt;

                gvPayCode.DataSource = dt;
                gvPayCode.DataBind();

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    TextBox txtHeader = (TextBox)gvPayCode.Rows[i].Cells[0].FindControl("txtHeader");
                    txtHeader.Text = dt.Rows[i]["HeaderName"].ToString();
                    DropDownList PaycodeType = (DropDownList)gvPayCode.Rows[i].Cells[1].FindControl("ddlPaycodeType");
                    FillDropDownList(PaycodeType);
                    PaycodeType.ClearSelection();
                    if (dt.Rows[i]["PaycodeType"].ToString() != "" && dt.Rows[i]["PaycodeType"].ToString() != "Select")
                    {
                        PaycodeType.Items.FindByText(dt.Rows[i]["PaycodeType"].ToString()).Selected = true;
                    }
                    DropDownList Paycode = (DropDownList)gvPayCode.Rows[i].Cells[2].FindControl("ddlPaycode");
                    if (PaycodeType.SelectedValue == "S")
                    {
                        List<StandardColumnModel> lst1 = new List<StandardColumnModel>();
                        string[] arrStandard1 = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "ArrearPFGROSS", "PTGROSS", "ArrearPTGROSS", "ESIGROSS", "ArrearESIGROSS", "ReceiptNo_BankTransactionID", "DateofPayment", "SalaryConfirmation", "ArrESIGross", "ArrPFGross", "ArrPTGROSS", "EffectiveDate" };
                        foreach (var item in arrStandard1)
                        {
                            StandardColumnModel objColumn = new StandardColumnModel();
                            objColumn.CPMD_Standard_Column = item;
                            lst1.Add(objColumn);
                        }

                        Paycode.DataSource = lst1;
                        Paycode.DataTextField = "CPMD_Standard_Column";
                        Paycode.DataValueField = "CPMD_Standard_Column";
                        Paycode.DataBind();
                        Paycode.ClearSelection();
                        if (dt.Rows[i]["Paycode"].ToString() != "" && dt.Rows[i]["Paycode"].ToString() != "Select")
                        {
                            if (Paycode.Items.FindByText(dt.Rows[i]["Paycode"].ToString()) != null)
                                Paycode.Items.FindByText(dt.Rows[i]["Paycode"].ToString()).Selected = true;
                        }
                        else
                        {
                            Paycode.ClearSelection();
                            Paycode.Items.Insert(0, new ListItem("Select", "-1"));
                        }
                    }

                    else if (PaycodeType.SelectedValue == "E" || PaycodeType.SelectedValue == "D")
                    {
                        string paycode = PaycodeType.SelectedValue;
                        if (paycode != null)
                        {
                            List<RLCS_PayCode_Master> PaycodeMasterModellist = RLCS_ClientsManagement.GetPaycodeByType(paycode);
                            Paycode.DataSource = PaycodeMasterModellist;
                            Paycode.DataTextField = "PEM_Pay_Code_Description";
                            Paycode.DataValueField = "PEM_Pay_Code";
                            Paycode.DataBind();
                            Paycode.ClearSelection();
                            if (dt.Rows[i]["Paycode"].ToString() != "" && dt.Rows[i]["Paycode"].ToString() != "Select")
                            {

                                var PayCode = PaycodeMasterModellist.Where(t => t.PEM_Pay_Code == dt.Rows[i]["Paycode"].ToString()).FirstOrDefault();
                                Paycode.Items.FindByText(PayCode.PEM_Pay_Code_Description).Selected = true;
                            }
                            else
                            {
                                Paycode.ClearSelection();
                                Paycode.Items.Insert(0, new ListItem("Select", "-1"));
                            }
                        }
                    }
                    TextBox SquanceOrder = (TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder");
                    SquanceOrder.Text = dt.Rows[i]["SquanceOrder"].ToString();
                    CheckBox chkESI = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxESI");
                    CheckBox chkPF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxPF");
                    CheckBox chkPT = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxPT");
                    CheckBox chkLWF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxLWF");

                    if (PaycodeType.SelectedValue == "S")
                    {
                        PaycodeType.Enabled = false;
                        Paycode.Enabled = false;
                        chkESI.Enabled = false;
                        chkPF.Enabled = false;
                        chkPT.Enabled = false;
                        chkLWF.Enabled = false;
                    }
                    else
                    {
                        if (dt.Rows[i]["ESI"].ToString() == "Y") { chkESI.Checked = true; }
                        if (dt.Rows[i]["PF"].ToString() == "Y") { chkPF.Checked = true; }
                        if (dt.Rows[i]["PT"].ToString() == "Y") { chkPT.Checked = true; }
                        if (dt.Rows[i]["LWF"].ToString() == "Y") { chkLWF.Checked = true; }

                    }
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                }
            }
        }
        private void SetInitialRow()
        {


            if (Request.QueryString["ClientID"] != null)
            {
                listOfPaycode = RLCS_ClientsManagement.GetAllPaycodeDetailsList(Convert.ToString(Request.QueryString["ClientID"]), ""); //ClientIDnew = Convert.ToString(Request.QueryString["ClientID"]);
            }


            //Session["ClientIDNew"] = null;

            if (listOfPaycode.Count == 0)
            {
                DataTable dt = new DataTable();
                DataRow dr = null;

                dt.Columns.Add(new DataColumn("ID", typeof(string)));
                dt.Columns.Add(new DataColumn("HeaderName", typeof(string)));
                dt.Columns.Add(new DataColumn("PaycodeType", typeof(string)));//for DropDownList selected item
                dt.Columns.Add(new DataColumn("Paycode", typeof(string)));  //for DropDownList selected item 
                dt.Columns.Add(new DataColumn("SquanceOrder", typeof(string)));   //for TextBox value   
                dt.Columns.Add(new DataColumn("ESI", typeof(string)));//for checkbox  
                dt.Columns.Add(new DataColumn("PF", typeof(string)));  //for checkbox  
                dt.Columns.Add(new DataColumn("PT", typeof(string)));//for checkbox  
                dt.Columns.Add(new DataColumn("LWF", typeof(string)));  //for checkbox 
                int Sorder = 0;
                List<StandardColumnModel> lst = new List<StandardColumnModel>();
                string[] arrStandard = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS" };
                foreach (string item in arrStandard)
                {
                    StandardColumnModel objColumn = new StandardColumnModel();
                    objColumn.CPMD_Standard_Column = item.ToString();
                    lst.Add(objColumn);
                    dr = dt.NewRow();
                    Sorder = Sorder + 1;
                    dr["ID"] = Sorder;
                    dr["HeaderName"] = item;
                    dr["PaycodeType"] = "Standard";
                    dr["Paycode"] = item;
                    dr["SquanceOrder"] = Sorder;
                    dt.Rows.Add(dr);
                }

                ViewState["Sorder"] = Sorder;
                ViewState["HeaderTable"] = dt;

                gvPayCode.DataSource = dt;
                gvPayCode.DataBind();

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    TextBox txtHeader = (TextBox)gvPayCode.Rows[i].Cells[0].FindControl("txtHeader");
                    txtHeader.Text = dt.Rows[i]["HeaderName"].ToString();
                    DropDownList PaycodeType = (DropDownList)gvPayCode.Rows[i].Cells[1].FindControl("ddlPaycodeType");
                    FillDropDownList(PaycodeType);
                    PaycodeType.ClearSelection();
                    if (dt.Rows[i]["PaycodeType"].ToString() != "" && dt.Rows[i]["PaycodeType"].ToString() != "Select")
                    {
                        PaycodeType.Items.FindByText(dt.Rows[i]["PaycodeType"].ToString()).Selected = true;
                    }
                    DropDownList Paycode = (DropDownList)gvPayCode.Rows[i].Cells[2].FindControl("ddlPaycode");
                    if (PaycodeType.SelectedValue == "S")
                    {
                        List<StandardColumnModel> lst1 = new List<StandardColumnModel>();

                        string[] arrStandard1 = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "ArrearPFGROSS", "PTGROSS", "ArrearPTGROSS", "ESIGROSS", "ArrearESIGROSS", "ReceiptNo_BankTransactionID", "DateofPayment", "SalaryConfirmation", "ArrESIGross", "ArrPFGross", "ArrPTGROSS", "EffectiveDate" };
                        foreach (string item in arrStandard1)
                        {
                            StandardColumnModel objColumn = new StandardColumnModel();
                            objColumn.CPMD_Standard_Column = item;
                            lst1.Add(objColumn);
                        }
                        Paycode.DataSource = lst1;
                        Paycode.DataTextField = "CPMD_Standard_Column";
                        Paycode.DataValueField = "CPMD_Standard_Column";
                        Paycode.DataBind();
                        Paycode.ClearSelection();
                        if (dt.Rows[i]["Paycode"].ToString() != "" && dt.Rows[i]["Paycode"].ToString() != "Select")
                        {
                            Paycode.Items.FindByText(dt.Rows[i]["Paycode"].ToString()).Selected = true;
                        }
                        else
                        {
                            Paycode.ClearSelection();
                            Paycode.Items.Insert(0, new ListItem("Select", "-1"));
                        }
                    }
                    CheckBox chkESI = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxESI");
                    CheckBox chkPF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxPF");
                    CheckBox chkPT = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxPT");
                    CheckBox chkLWF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxLWF");
                    PaycodeType.Enabled = false;
                    Paycode.Enabled = false;
                    chkESI.Enabled = false;
                    chkPF.Enabled = false;
                    chkPT.Enabled = false;
                    chkLWF.Enabled = false;
                    TextBox SquanceOrder = (TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder");
                    SquanceOrder.Text = dt.Rows[i]["SquanceOrder"].ToString();
                }
            }
            else
            {
                DataTable dt = new DataTable();
                DataRow dr = null;

                dt.Columns.Add(new DataColumn("ID", typeof(string)));
                dt.Columns.Add(new DataColumn("HeaderName", typeof(string)));
                dt.Columns.Add(new DataColumn("PaycodeType", typeof(string)));//for DropDownList selected item
                dt.Columns.Add(new DataColumn("Paycode", typeof(string)));  //for DropDownList selected item 
                dt.Columns.Add(new DataColumn("SquanceOrder", typeof(string)));   //for TextBox value   
                dt.Columns.Add(new DataColumn("ESI", typeof(string)));//for checkbox  
                dt.Columns.Add(new DataColumn("PF", typeof(string)));  //for checkbox  
                dt.Columns.Add(new DataColumn("PT", typeof(string)));//for checkbox  
                dt.Columns.Add(new DataColumn("LWF", typeof(string)));  //for checkbox 
                int Sorder = 0;
                int sequenceVal = 0;
                List<StandardColumnModel> lst = new List<StandardColumnModel>();
                List<SP_Get_ClientPaycodeMappingDetails_Result> headerList = listOfPaycode as List<SP_Get_ClientPaycodeMappingDetails_Result>;
                int countOfHeader = headerList.Count();

                for (int i = 0; i < countOfHeader; i++)
                {
                    foreach (string item in headerList[i].CPMD_Header.Split(new[] { '\n' }))
                    {
                        StandardColumnModel objColumn = new StandardColumnModel();
                        objColumn.CPMD_Standard_Column = item.ToString();
                        lst.Add(objColumn);
                        dr = dt.NewRow();
                        dr["HeaderName"] = item;
                        dr["PaycodeType"] = headerList[i].CPMD_Deduction_Type;
                        dr["Paycode"] = headerList[i].CPMD_PayCode;
                        dr["ESI"] = headerList[i].CPMD_appl_ESI;
                        dr["PF"] = headerList[i].CPMD_appl_PF;
                        dr["PT"] = headerList[i].CPMD_Appl_PT;
                        dr["LWF"] = headerList[i].CPMD_Appl_LWF;
                        Sorder = headerList[i].CPMD_Sequence_Order;
                        dr["SquanceOrder"] = Sorder;
                        dr["ID"] = headerList[i].ID;
                        sequenceVal = Sorder >= sequenceVal ? Sorder : sequenceVal;

                        dt.Rows.Add(dr);

                    }
                }

                ViewState["Sorder"] = sequenceVal;
                ViewState["HeaderTable"] = dt;

                gvPayCode.DataSource = dt;
                gvPayCode.DataBind();

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    TextBox txtHeader = (TextBox)gvPayCode.Rows[i].Cells[0].FindControl("txtHeader");
                    txtHeader.Text = dt.Rows[i]["HeaderName"].ToString();
                    DropDownList PaycodeType = (DropDownList)gvPayCode.Rows[i].Cells[1].FindControl("ddlPaycodeType");
                    FillDropDownList(PaycodeType);
                    PaycodeType.ClearSelection();
                    if (dt.Rows[i]["PaycodeType"].ToString() != "" && dt.Rows[i]["PaycodeType"].ToString() != "Select")
                    {
                        PaycodeType.Items.FindByText(dt.Rows[i]["PaycodeType"].ToString()).Selected = true;
                    }
                    DropDownList Paycode = (DropDownList)gvPayCode.Rows[i].Cells[2].FindControl("ddlPaycode");
                    if (PaycodeType.SelectedValue == "S")
                    {
                        List<StandardColumnModel> lst1 = new List<StandardColumnModel>();
                        string[] arrStandard1 = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "ArrearPFGROSS", "PTGROSS", "ArrearPTGROSS", "ESIGROSS", "ArrearESIGROSS", "ReceiptNo_BankTransactionID", "DateofPayment", "SalaryConfirmation", "ArrESIGross", "ArrPFGross", "ArrPTGROSS", "EffectiveDate" };
                        List<SP_Get_ClientPaycodeMappingDetails_Result> headerListNew = listOfPaycode as List<SP_Get_ClientPaycodeMappingDetails_Result>;

                        //for (int j = 0; j < countOfHeader; j++)
                        //{
                        //foreach (var item in headerList[i].CPMD_Header.Split(new[] { '\n' }))
                        foreach (var item in arrStandard1)
                        {
                            StandardColumnModel objColumn = new StandardColumnModel();
                            objColumn.CPMD_Standard_Column = item;
                            lst1.Add(objColumn);
                        }
                        //}

                        Paycode.DataSource = lst1;
                        Paycode.DataTextField = "CPMD_Standard_Column";
                        Paycode.DataValueField = "CPMD_Standard_Column";
                        Paycode.DataBind();
                        Paycode.ClearSelection();
                        if (dt.Rows[i]["Paycode"].ToString() != "" && dt.Rows[i]["Paycode"].ToString() != "Select")
                        {
                            if (Paycode.Items.FindByText(dt.Rows[i]["Paycode"].ToString()) != null)
                                Paycode.Items.FindByText(dt.Rows[i]["Paycode"].ToString()).Selected = true;
                        }
                        else
                        {
                            Paycode.ClearSelection();
                            Paycode.Items.Insert(0, new ListItem("Select", "-1"));
                        }

                    }
                    else if (PaycodeType.SelectedValue == "E" || PaycodeType.SelectedValue == "D")
                    {
                        string paycodeNew = PaycodeType.SelectedValue;
                        if (paycodeNew != null)
                        {
                            List<RLCS_PayCode_Master> PaycodeMasterModellistNew = RLCS_ClientsManagement.GetPaycodeByType(paycodeNew);
                            Paycode.DataSource = PaycodeMasterModellistNew;
                            Paycode.DataTextField = "PEM_Pay_Code_Description";
                            Paycode.DataValueField = "PEM_Pay_Code";
                            Paycode.DataBind();
                            Paycode.ClearSelection();
                            if (dt.Rows[i]["Paycode"].ToString() != "" && dt.Rows[i]["Paycode"].ToString() != "Select")
                            {
                                var PayCodeNew = PaycodeMasterModellistNew.Where(t => t.PEM_Pay_Code == dt.Rows[i]["Paycode"].ToString()).FirstOrDefault();
                                if (PayCodeNew != null)
                                    Paycode.Items.FindByText(PayCodeNew.PEM_Pay_Code_Description).Selected = true;
                            }
                            else
                            {
                                Paycode.ClearSelection();
                                Paycode.Items.Insert(0, new ListItem("Select", "-1"));
                            }
                        }
                    }
                    CheckBox chkESI = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxESI");
                    CheckBox chkPF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxPF");
                    CheckBox chkPT = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxPT");
                    CheckBox chkLWF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxLWF");

                    if (PaycodeType.SelectedValue == "S")
                    {
                        PaycodeType.Enabled = false;
                        Paycode.Enabled = false;
                        chkESI.Enabled = false;
                        chkPF.Enabled = false;
                        chkPT.Enabled = false;
                        chkLWF.Enabled = false;
                    }
                    else
                    {
                        if (dt.Rows[i]["ESI"].ToString() == "YES") { chkESI.Checked = true; }
                        if (dt.Rows[i]["PF"].ToString() == "YES") { chkPF.Checked = true; }
                        if (dt.Rows[i]["PT"].ToString() == "YES") { chkPT.Checked = true; }
                        if (dt.Rows[i]["LWF"].ToString() == "YES") { chkLWF.Checked = true; }

                    }
                    TextBox SquanceOrder = (TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder");
                    SquanceOrder.Text = dt.Rows[i]["SquanceOrder"].ToString();
                }
            }

        }

        private void AddNewRowToGrid()
        {
            if (ViewState["HeaderTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["HeaderTable"];
                int rowID = 0;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    ViewState["HeaderTable"] = dtCurrentTable;
                    bool ifHeaderExist = false;
                    for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                    {
                        List<RLCS_Client_Paycode_Mapping_Details> LstPaycodedetails = new List<RLCS_Client_Paycode_Mapping_Details>();
                        RLCS_Client_Paycode_Mapping_Details paycodedetails = new RLCS_Client_Paycode_Mapping_Details();
                        TextBox txtHeader = (TextBox)gvPayCode.Rows[i].Cells[0].FindControl("txtHeader");
                        ////if (txtHeader.Text != "")
                        ////{
                        ////    var id = "0";
                        ////    if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                        ////    {
                        ////        id = Convert.ToString(Request.QueryString["ID"]);
                        ////    }
                        ////    ifHeaderExist = RLCS_ClientsManagement.CheckHeader(txtHeader.Text, Request.QueryString["ClientID"], id);
                        ////    if (ifHeaderExist)
                        ////    {
                        ////        lblPaycodeHeader.Visible = true;
                        ////        lblPaycodeHeader.Text = "Paycode Header Already Exist : " + txtHeader.Text;
                        ////        lblPaycodeHeader.CssClass = "alert alert-danger";
                        ////        btnSave.Enabled = false;
                        ////        break;
                        ////    }
                        ////    else
                        ////{
                        lblPaycodeHeader.Visible = false;
                        btnSave.Enabled = true;
                        DropDownList PaycodeType = (DropDownList)gvPayCode.Rows[i].Cells[1].FindControl("ddlPaycodeType");
                        DropDownList Paycode = (DropDownList)gvPayCode.Rows[i].Cells[2].FindControl("ddlPaycode");
                        TextBox SquanceOrder = (TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder");
                        CheckBox chkESI = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxESI");
                        CheckBox chkPF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxPF");
                        CheckBox chkPT = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxPT");
                        CheckBox chkLWF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxLWF");


                        rowID = Convert.ToInt32((dtCurrentTable.Rows[i]).ItemArray[0]);
                        paycodedetails.CPMD_Header = txtHeader.Text;
                        dtCurrentTable.Rows[i]["HeaderName"] = txtHeader.Text;
                        dtCurrentTable.Rows[i]["PaycodeType"] = PaycodeType.SelectedItem.Text;
                        dtCurrentTable.Rows[i]["Paycode"] = Paycode.SelectedItem.Text;
                        dtCurrentTable.Rows[i]["SquanceOrder"] = SquanceOrder.Text;
                        if (PaycodeType.SelectedValue != "-1")
                        {
                            paycodedetails.CPMD_PayGroup = "paycodes";
                            if (PaycodeType.SelectedValue == "S")
                            {
                                paycodedetails.CPMD_Standard_Column = Convert.ToString(Paycode.SelectedValue);
                            }
                            else
                            {
                                paycodedetails.CPMD_PayCode = Convert.ToString(Paycode.SelectedValue);
                            }


                            if (SquanceOrder.Text != "")
                            {
                                paycodedetails.CPMD_Sequence_Order = Convert.ToInt32(SquanceOrder.Text);
                            }

                            paycodedetails.CPMD_Deduction_Type = Convert.ToString(PaycodeType.SelectedValue);
                            if (PaycodeType.SelectedValue == "E" || PaycodeType.SelectedValue == "D")
                            {
                                if (chkESI.Checked)
                                {
                                    dtCurrentTable.Rows[i]["ESI"] = chkESI.Checked;
                                    paycodedetails.CPMD_appl_ESI = "Y";
                                }
                                else
                                {
                                    paycodedetails.CPMD_appl_ESI = "N";
                                }
                                if (chkPF.Checked)
                                {
                                    dtCurrentTable.Rows[i]["PF"] = chkPF.Checked;
                                    paycodedetails.CPMD_appl_PF = "Y";
                                }
                                else
                                {
                                    paycodedetails.CPMD_appl_PF = "N";
                                }
                                if (chkPT.Checked)
                                {
                                    dtCurrentTable.Rows[i]["PT"] = chkPT.Checked;
                                    paycodedetails.CPMD_Appl_PT = "Y";
                                }
                                else
                                {
                                    paycodedetails.CPMD_Appl_PT = "N";
                                }
                                if (chkLWF.Checked)
                                {
                                    dtCurrentTable.Rows[i]["LWF"] = chkLWF.Checked;
                                    paycodedetails.CPMD_Appl_LWF = "Y";
                                }
                                else
                                {
                                    paycodedetails.CPMD_Appl_LWF = "N";
                                }

                            }
                            else
                            {
                                paycodedetails.CPMD_appl_ESI = "NULL";
                                paycodedetails.CPMD_appl_PF = "NULL";
                                paycodedetails.CPMD_Appl_PT = "NULL";
                                paycodedetails.CPMD_Appl_LWF = "NULL";
                            }
                            // Save the data in DataBase
                            try
                            {
                                int Squance_Order = 0;
                                if (SquanceOrder.Text != "")
                                {
                                    Squance_Order = Convert.ToInt32(SquanceOrder.Text);
                                }
                                int CustID = 0;
                                if (!string.IsNullOrEmpty(Request.QueryString["ClientID"]))
                                {
                                    CustID = RLCS_ClientsManagement.GetCustomerIDByClientID(Request.QueryString["ClientID"]);
                                }
                                string CPMD_appl_ESI = "", CPMD_appl_PF = "", CPMD_Appl_PT = "", CPMD_Appl_LWF = "";
                                if (PaycodeType.SelectedValue == "E" || PaycodeType.SelectedValue == "D")
                                {
                                    if (chkESI.Checked) { CPMD_appl_ESI = "Y"; } else { CPMD_appl_ESI = "N"; }
                                    if (chkPF.Checked) { CPMD_appl_PF = "Y"; } else { CPMD_appl_PF = "N"; }
                                    if (chkPT.Checked) { CPMD_Appl_PT = "Y"; } else { CPMD_Appl_PT = "N"; }
                                    if (chkLWF.Checked) { CPMD_Appl_LWF = "Y"; } else { CPMD_Appl_LWF = "N"; }
                                }
                                else
                                {
                                    CPMD_appl_ESI = ""; CPMD_appl_PF = ""; CPMD_Appl_PT = ""; CPMD_Appl_LWF = "";
                                }
                                string CPMD_PayCode = "", CPMD_Standard_Column = "";
                                if (PaycodeType.SelectedValue == "S")
                                {
                                    CPMD_Standard_Column = Convert.ToString(Paycode.SelectedValue);
                                }
                                else
                                {
                                    CPMD_PayCode = Convert.ToString(Paycode.SelectedValue);
                                }
                                PaycodeMappingModel PaycodeMappingModel = new PaycodeMappingModel()
                                {
                                    AVACOM_CustomerID = CustID,
                                    CPMD_ClientID = Request.QueryString["ClientID"],
                                    CPMD_PayCode = CPMD_PayCode,
                                    CPMD_Standard_Column = CPMD_Standard_Column,
                                    CPMD_Header = txtHeader.Text,
                                    CPMD_Sequence_Order = Squance_Order,
                                    CPMD_Deduction_Type = Convert.ToString(PaycodeType.SelectedValue),
                                    CPMD_appl_ESI = CPMD_appl_ESI,
                                    CPMD_appl_PF = CPMD_appl_PF,
                                    CPMD_Appl_PT = CPMD_Appl_PT,
                                    CPMD_Appl_LWF = CPMD_Appl_LWF

                                };
                                List<RLCS_Client_Paycode_Mapping> PaycodeList = new List<RLCS_Client_Paycode_Mapping>();
                                RLCS_Client_Paycode_Mapping newRecord = new RLCS_Client_Paycode_Mapping()
                                {
                                    AVACOM_CustomerID = CustID,
                                    CPM_ClientID = Request.QueryString["ClientID"],
                                    CPM_PayGroup = "Default",
                                    CPM_CreatedBy = "Avantis",
                                    CPM_Status = "A"
                                };
                                PaycodeList.Add(newRecord);

                                TinyMapper.Bind<PaycodeMappingModel, RLCS_Client_Paycode_Mapping_Details>();
                                paycodedetails = TinyMapper.Map<RLCS_Client_Paycode_Mapping_Details>(PaycodeMappingModel);
                                paycodedetails.CPMD_Standard_Column = paycodedetails.CPMD_Standard_Column ?? "";
                                paycodedetails.CPMD_PayGroup = PaycodeType.SelectedValue == "S" ? "standard" : "paycodes";

                                if (paycodedetails.CPMD_PayCode == null)
                                    paycodedetails.CPMD_PayCode = "";

                                paycodedetails.AVACOM_CustomerID = CustID;
                                LstPaycodedetails.Add(paycodedetails);
                                int ID = 0;
                                bool sucess = false;
                                if (!string.IsNullOrEmpty(Request.QueryString["ID"]) || rowID != 0)
                                {
                                    ID = (string.IsNullOrEmpty(Request.QueryString["ID"])) ? Convert.ToInt32(rowID) : Convert.ToInt32(Request.QueryString["ID"]);
                                    sucess = RLCS_ClientsManagement.CreateUpdate_ClientPaycodeMapping(PaycodeList, LstPaycodedetails, ID);
                                }
                                else
                                {
                                    sucess = RLCS_ClientsManagement.CreateUpdate_ClientPaycodeMapping(PaycodeList, LstPaycodedetails);
                                }

                                if (sucess)
                                {
                                    PaycodeMappingModel.CPMD_PayGroup = "";
                                    PaycodeMappingModel.CPMD_Header = "";
                                    PaycodeMappingModel.CPMD_Sequence_Order = 0;
                                    PaycodeMappingModel.Message = true;
                                    ModelState.Clear();
                                    lblMessage.Visible = true;
                                    if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                                    {
                                        lblMessage.Text = "PayCode Mapping update Successfully";
                                    }
                                    else
                                    {
                                        lblMessage.Text = "PayCode Mapping save Successfully";
                                    }

                                    lblMessage.CssClass = "alert alert-success";
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                                }
                                var Clients = RLCS_ClientsManagement.GetAll_client();
                                if (Clients != null && Clients.Count > 0)
                                {
                                    TinyMapper.Bind<List<RLCS_CustomerBranch_ClientsLocation_Mapping>, List<ClientModel>>();
                                    PaycodeMappingModel.Clients = TinyMapper.Map<List<ClientModel>>(Clients);

                                }
                            }
                            catch (Exception ex)
                            {
                                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                //PaycodeMappingModel.Exception = true;
                            }

                        }
                        ////}
                        ////}
                    }
                    //Rebind the Grid with the current data to reflect changes   
                    gvPayCode.DataSource = dtCurrentTable;
                    gvPayCode.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");

            }
            SetPreviousData();
        }
        private void SetPreviousData()
        {
            int rowIndex = 0;
            string ClientIDnew = "0";
            if (Session["ClientIDNew"] != null)
            {
                ClientIDnew = Convert.ToString(Session["ClientIDNew"]);
            }

            listOfPaycode = RLCS_ClientsManagement.GetAllPaycodeDetailsList(ClientIDnew, "");
            //Session["ClientIDNew"] = null;
            if (ViewState["HeaderTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["HeaderTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox txtHeader = (TextBox)gvPayCode.Rows[i].Cells[0].FindControl("txtHeader");
                        DropDownList PaycodeType = (DropDownList)gvPayCode.Rows[rowIndex].Cells[1].FindControl("ddlPaycodeType");
                        DropDownList Paycode = (DropDownList)gvPayCode.Rows[rowIndex].Cells[2].FindControl("ddlPaycode");
                        TextBox SquanceOrder = (TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder");
                        CheckBox chkESI = (CheckBox)gvPayCode.Rows[rowIndex].Cells[4].FindControl("CheckboxESI");
                        CheckBox chkPF = (CheckBox)gvPayCode.Rows[rowIndex].Cells[5].FindControl("CheckboxPF");
                        CheckBox chkPT = (CheckBox)gvPayCode.Rows[rowIndex].Cells[4].FindControl("CheckboxPT");
                        CheckBox chkLWF = (CheckBox)gvPayCode.Rows[rowIndex].Cells[5].FindControl("CheckboxLWF");
                        FillDropDownList(PaycodeType);
                        if (i < dt.Rows.Count)
                        {
                            txtHeader.Text = dt.Rows[i]["HeaderName"].ToString();
                            PaycodeType.ClearSelection();
                            if (dt.Rows[i]["PaycodeType"].ToString() != "" && dt.Rows[i]["PaycodeType"].ToString() != "Select")
                            {
                                if (PaycodeType.Items.FindByText(dt.Rows[i]["PaycodeType"].ToString()) != null)
                                    PaycodeType.Items.FindByText(dt.Rows[i]["PaycodeType"].ToString()).Selected = true;
                            }
                            if (PaycodeType.SelectedValue == "E" || PaycodeType.SelectedValue == "D")
                            {
                                string paycode = PaycodeType.SelectedValue;
                                if (paycode != null)
                                {
                                    List<RLCS_PayCode_Master> PaycodeMasterModellist = RLCS_ClientsManagement.GetPaycodeByType(paycode);
                                    Paycode.DataSource = PaycodeMasterModellist;
                                    Paycode.DataTextField = "PEM_Pay_Code_Description";
                                    Paycode.DataValueField = "PEM_Pay_Code";
                                    Paycode.DataBind();
                                    Paycode.ClearSelection();
                                    if (dt.Rows[i]["Paycode"].ToString() != "" && dt.Rows[i]["Paycode"].ToString() != "Select")
                                    {
                                        string payCode = dt.Rows[i]["Paycode"].ToString();
                                        string s = "/";
                                        if (payCode.Contains(s))
                                        {
                                            Paycode.Items.FindByText(dt.Rows[i]["Paycode"].ToString()).Selected = true;

                                        }
                                        else
                                        {
                                            var PayCode = PaycodeMasterModellist.Where(t => t.PEM_Pay_Code == dt.Rows[i]["Paycode"].ToString()).FirstOrDefault();
                                            Paycode.Items.FindByText(PayCode.PEM_Pay_Code_Description).Selected = true;
                                        }
                                    }
                                    else
                                    {
                                        Paycode.ClearSelection();
                                        Paycode.Items.Insert(0, new ListItem("Select", "-1"));
                                    }
                                }
                            }
                            else if (PaycodeType.SelectedValue == "S")
                            {
                                List<StandardColumnModel> lst = new List<StandardColumnModel>();
                                string[] arrStandard = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "ArrearPFGROSS", "PTGROSS", "ArrearPTGROSS", "ESIGROSS", "ArrearESIGROSS", "ReceiptNo_BankTransactionID", "DateofPayment", "SalaryConfirmation", "ArrESIGross", "ArrPFGross", "ArrPTGROSS", "EffectiveDate" };
                                //List<SP_Get_ClientPaycodeMappingDetails_Result> headerList = listOfPaycode as List<SP_Get_ClientPaycodeMappingDetails_Result>;
                                //{
                                //    foreach (var item in arrStandard)
                                //int countOfHeader = headerList.Count();
                                //for (int chkHeaderCount = 0; chkHeaderCount < countOfHeader; chkHeaderCount++)
                                //{
                                //foreach (string item in headerList[chkHeaderCount].CPMD_Header.Split(new[] { '\n' }))

                                foreach (string item in arrStandard)
                                {
                                    StandardColumnModel objColumn = new StandardColumnModel();
                                    objColumn.CPMD_Standard_Column = item;
                                    lst.Add(objColumn);
                                }
                                //}

                                Paycode.DataSource = lst;
                                Paycode.DataTextField = "CPMD_Standard_Column";
                                Paycode.DataValueField = "CPMD_Standard_Column";
                                Paycode.DataBind();
                                Paycode.ClearSelection();
                                if (dt.Rows[i]["Paycode"].ToString() != "" && dt.Rows[i]["Paycode"].ToString() != "Select")
                                {
                                    if (Paycode.Items.FindByText(dt.Rows[i]["Paycode"].ToString()) != null)
                                        Paycode.Items.FindByText(dt.Rows[i]["Paycode"].ToString()).Selected = true;
                                }
                                else
                                {
                                    Paycode.ClearSelection();
                                    Paycode.Items.Insert(0, new ListItem("Select", "-1"));
                                }
                            }
                            SquanceOrder.Text = dt.Rows[i]["SquanceOrder"].ToString();
                            if (dt.Rows[i]["ESI"].ToString() == "True") { chkESI.Checked = true; }
                            if (dt.Rows[i]["PF"].ToString() == "True") { chkPF.Checked = true; }
                            if (dt.Rows[i]["PT"].ToString() == "True") { chkPT.Checked = true; }
                            if (dt.Rows[i]["LWF"].ToString() == "True") { chkLWF.Checked = true; }



                        }
                        rowIndex++;

                        if (PaycodeType.SelectedItem.Text == "Standard")
                        {
                            PaycodeType.Enabled = false;
                            Paycode.Enabled = false;
                            chkESI.Enabled = false;
                            chkPF.Enabled = false;
                            chkPT.Enabled = false;
                            chkLWF.Enabled = false;
                        }
                    }
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

            DataTable dtCurrentTable = (DataTable)ViewState["HeaderTable"];
            bool flag = false;
            DataTable dt = (DataTable)ViewState["HeaderTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox txtHeader = (TextBox)gvPayCode.Rows[i].Cells[0].FindControl("txtHeader");
                    DropDownList ddl1 = (DropDownList)gvPayCode.Rows[i].Cells[1].FindControl("ddlPaycodeType");
                    DropDownList Paycode = (DropDownList)gvPayCode.Rows[i].Cells[2].FindControl("ddlPaycode");
                    TextBox SquanceOrder = (TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder");
                    CheckBox chkESI = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxESI");
                    CheckBox chkPF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxPF");
                    CheckBox chkPT = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxPT");
                    CheckBox chkLWF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxLWF");

                    dtCurrentTable.Rows[i]["ID"] = (dtCurrentTable.Rows[i]).ItemArray[0];
                    dtCurrentTable.Rows[i]["HeaderName"] = txtHeader.Text;
                    dtCurrentTable.Rows[i]["PaycodeType"] = ddl1.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Paycode"] = Paycode.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["SquanceOrder"] = SquanceOrder.Text;
                    if (chkESI.Checked) { dtCurrentTable.Rows[i]["ESI"] = chkESI.Checked; }
                    if (chkPF.Checked) { dtCurrentTable.Rows[i]["PF"] = chkPF.Checked; }
                    if (chkPT.Checked) { dtCurrentTable.Rows[i]["PT"] = chkPT.Checked; }
                    if (chkLWF.Checked) { dtCurrentTable.Rows[i]["LWF"] = chkLWF.Checked; }
                }
            }
            ViewState["HeaderTable"] = dtCurrentTable;
            try
            {
                if (lblPaycodeAndType.Text == "" && lblPaycodeType.Text == "" && lblPaycodeHeader.Text == "" && lblPaycodeSequence.Text == "")
                {
                    for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                    {
                        TextBox txtHeader = (TextBox)gvPayCode.Rows[i].Cells[0].FindControl("txtHeader");
                        if (string.IsNullOrEmpty(txtHeader.Text))
                        {
                            lblPaycodeAndType.Visible = true;
                            lblPaycodeAndType.Text = "Please Enter Header.";
                            lblPaycodeAndType.CssClass = "alert alert-danger";
                            btnSave.Enabled = false;
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }
                    }
                    if (flag == false)
                        AddNewRowToGrid();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void gvPayCode_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["HeaderTable"];
                LinkButton lb = (LinkButton)e.Row.FindControl("LinkButton1");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {

                if (Request.QueryString["Edit"] == "YES")
                {
                    ImageButton btn = (ImageButton)e.Row.FindControl("btnAdd");
                    btn.Visible = false;
                }
                else
                {
                    ImageButton btn = (ImageButton)e.Row.FindControl("btnAdd");
                    btn.Visible = true;
                }

            }

        }
        protected void ddlPaycodeType_SelectedIndexChanged(object sender, EventArgs e)
        {

            btnSave.Enabled = true;
            DropDownList ddlPaycodeType = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddlPaycodeType.NamingContainer;
            DropDownList PaycodeType = (DropDownList)row.FindControl("ddlPaycodeType");

            if (PaycodeType.SelectedValue == "E" || PaycodeType.SelectedValue == "D")
            {
                string paycode = PaycodeType.SelectedValue;
                if (paycode != null)
                {
                    List<RLCS_PayCode_Master> PaycodeMasterModellist = RLCS_ClientsManagement.GetPaycodeByType(paycode);
                    DropDownList ddlPaycode = (DropDownList)sender;
                    GridViewRow Prow = (GridViewRow)ddlPaycode.NamingContainer;
                    DropDownList PayCode = (DropDownList)Prow.FindControl("ddlPaycode");
                    PayCode.DataSource = PaycodeMasterModellist;
                    PayCode.DataTextField = "PEM_Pay_Code_Description";
                    PayCode.DataValueField = "PEM_Pay_Code";
                    PayCode.DataBind();
                    PayCode.Items.Insert(0, new ListItem("Select", "-1"));
                }
            }
            else
            {
                List<StandardColumnModel> lst = new List<StandardColumnModel>();
                string[] arrStandard = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "ArrearPFGROSS", "PTGROSS", "ArrearPTGROSS", "ESIGROSS", "ArrearESIGROSS", "ReceiptNo_BankTransactionID", "DateofPayment", "SalaryConfirmation", "ArrESIGross", "ArrPFGross", "ArrPTGROSS", "EffectiveDate" };
                foreach (var item in arrStandard)
                {
                    StandardColumnModel objColumn = new StandardColumnModel();
                    objColumn.CPMD_Standard_Column = item;
                    lst.Add(objColumn);
                }
                DropDownList ddlPaycode = (DropDownList)sender;
                GridViewRow Prow = (GridViewRow)ddlPaycode.NamingContainer;
                DropDownList PayCode = (DropDownList)Prow.FindControl("ddlPaycode");
                PayCode.DataSource = lst;
                PayCode.DataTextField = "CPMD_Standard_Column";
                PayCode.DataValueField = "CPMD_Standard_Column";
                PayCode.DataBind();
                PayCode.Items.Insert(0, new ListItem("Select", "-1"));
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            else
            {
                btnSave.Visible = true;
                btnUpdate.Visible = false;
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            btnAddClick = true;

            if (ViewState["HeaderTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["HeaderTable"];
                DataRow drCurrentRow = null;
                int Sorder = Convert.ToInt32(ViewState["Sorder"]);
                if (dtCurrentTable.Rows.Count > 0)
                {
                    drCurrentRow = dtCurrentTable.NewRow();
                    if (ViewState["Sorder"] != null)
                    {
                        Sorder = Sorder + 1;
                        drCurrentRow["SquanceOrder"] = Sorder;
                        ViewState["Sorder"] = Sorder;
                    }
                    drCurrentRow["ID"] = 0;
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["HeaderTable"] = dtCurrentTable;
                    for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                    {
                        TextBox txtHeader = (TextBox)gvPayCode.Rows[i].Cells[0].FindControl("txtHeader");
                        DropDownList ddl1 = (DropDownList)gvPayCode.Rows[i].Cells[1].FindControl("ddlPaycodeType");
                        DropDownList Paycode = (DropDownList)gvPayCode.Rows[i].Cells[2].FindControl("ddlPaycode");
                        TextBox SquanceOrder = (TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder");
                        CheckBox chkESI = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxESI");
                        CheckBox chkPF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxPF");
                        CheckBox chkPT = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxPT");
                        CheckBox chkLWF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxLWF");


                        dtCurrentTable.Rows[i]["HeaderName"] = txtHeader.Text;
                        dtCurrentTable.Rows[i]["PaycodeType"] = ddl1.SelectedItem.Text;
                        dtCurrentTable.Rows[i]["Paycode"] = Paycode.SelectedItem.Text;
                        dtCurrentTable.Rows[i]["SquanceOrder"] = SquanceOrder.Text;
                        if (chkESI.Checked) { dtCurrentTable.Rows[i]["ESI"] = chkESI.Checked; }
                        if (chkPF.Checked) { dtCurrentTable.Rows[i]["PF"] = chkPF.Checked; }
                        if (chkPT.Checked) { dtCurrentTable.Rows[i]["PT"] = chkPT.Checked; }
                        if (chkLWF.Checked) { dtCurrentTable.Rows[i]["LWF"] = chkLWF.Checked; }
                    }

                    gvPayCode.DataSource = dtCurrentTable;
                    ViewState["HeaderTable"] = dtCurrentTable;
                    gvPayCode.DataBind();
                }
            }
            SetPreviousData();
            //btnAddClick = false;
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            Button lb = (Button)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["HeaderTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["HeaderTable"];
                int Sorder = Convert.ToInt32(ViewState["Sorder"]);
                if (dt.Rows.Count > 0)
                {
                    if (gvRow.RowIndex < dt.Rows.Count)
                    {
                        dt.Rows.Remove(dt.Rows[rowID]);
                        Sorder = Sorder - 1;
                        ViewState["Sorder"] = Sorder;
                    }
                }
                ViewState["HeaderTable"] = dt;
                gvPayCode.DataSource = dt;
                gvPayCode.DataBind();
            }
            SetPreviousData();

            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            else
            {
                btnSave.Visible = true;
                btnUpdate.Visible = false;
            }
        }
        protected void txtSquanceOrder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox txtSqnceOrd = sender as TextBox;
                bool ifSequenceExist = false;
                DataTable dtcurrentTable = (DataTable)ViewState["HeaderTable"];
                DataTable filterTable = new DataTable();
                if (dtcurrentTable.Rows.Count == 1)
                {
                    for (int i = dtcurrentTable.Rows.Count - 1; i >= 0; i--)
                    {
                        TextBox SquanceOrder = (TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder");
                        if (SquanceOrder.Text != "")
                        {
                            int id = 0;
                            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                            {
                                id = Convert.ToInt32(Request.QueryString["ID"]);
                                btnUpdate.Visible = true;
                                btnSave.Visible = false;
                            }
                            else
                            {
                                btnSave.Visible = true;
                                btnUpdate.Visible = false;
                            }

                            //sequenceValue.Add(Convert.ToInt32((TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder")));

                            ifSequenceExist = RLCS_ClientsManagement.CheckSequence(Convert.ToInt32(txtSqnceOrd.Text), Request.QueryString["ClientID"], id);
                            if (ifSequenceExist)
                            {
                                lblPaycodeSequence.Visible = true;
                                lblPaycodeSequence.Text = SquanceOrder.Text + " Paycode Sequence Already Exist!";
                                lblPaycodeSequence.CssClass = "alert alert-danger";
                                btnSave.Enabled = false;
                                break;
                            }
                            else if (Convert.ToInt32(txtSqnceOrd.Text) == 0)
                            {
                                lblPaycodeSequence.Visible = true;
                                lblPaycodeSequence.Text = "Please Enter Valid Paycode Sequence";
                                lblPaycodeSequence.CssClass = "alert alert-danger";
                                btnSave.Enabled = false;
                                break;
                            }
                            else
                            {
                                btnSave.Enabled = true;
                                lblPaycodeSequence.Visible = false;
                                lblPaycodeSequence.Visible = false;
                                lblPaycodeSequence.Text = "";
                                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                                {
                                    SquanceOrder.Text = SquanceOrder.Text;
                                }
                                else
                                {
                                    if (RLCS_ClientsManagement.CheckSequence(Convert.ToInt32(SquanceOrder.Text), Request.QueryString["ClientID"], 0))
                                        SquanceOrder.Text = Convert.ToString(RLCS_ClientsManagement.GetSequence(Request.QueryString["ClientID"]));
                                }
                            }
                        }
                    }
                }
                else if (dtcurrentTable.Rows.Count > 1)
                {
                    if (dtcurrentTable != null)
                    {
                        DataView dv = new DataView(dtcurrentTable);
                        dv.RowFilter = "SquanceOrder = " + txtSqnceOrd.Text;
                        int countValue = dv.ToTable().Rows.Count;
                        if (countValue > 0 || Convert.ToInt32(txtSqnceOrd.Text) == 0)
                        {
                            if (Convert.ToInt32(txtSqnceOrd.Text) == 0)
                            {
                                lblPaycodeSequence.Visible = true;
                                lblPaycodeSequence.Text = "Please Enter valid Paycode Sequence.";
                                lblPaycodeSequence.CssClass = "alert alert-danger";
                                btnSave.Enabled = false;
                            }
                            else if (!String.IsNullOrEmpty(dv.ToTable().Rows[0]["Paycode"].ToString()))
                            {
                                lblPaycodeSequence.Visible = true;
                                lblPaycodeSequence.Text = txtSqnceOrd.Text + " Paycode Sequence Already Exist!";
                                lblPaycodeSequence.CssClass = "alert alert-danger";
                                btnSave.Enabled = false;
                            }
                            else
                            {
                                btnSave.Enabled = true;
                                lblPaycodeSequence.Visible = false;
                                lblPaycodeSequence.Text = "";

                            }
                        }
                        else
                        {
                            btnSave.Enabled = true;
                            lblPaycodeSequence.Visible = false;
                            lblPaycodeSequence.Text = "";

                        }
                    }
                }
                else
                {
                    cvDuplicateEntry.ErrorMessage = "Data table is blank.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }

            //bool ifSequenceExist = false;

            //List<int> sequenceValue = new List<int>();
            //if (ViewState["HeaderTable"] != null)
            //{
            //    DataTable dtCurrentTable = (DataTable)ViewState["HeaderTable"];
            //    for (int i = dtCurrentTable.Rows.Count - 1; i >= 0; i--)
            //    {
            //        TextBox SquanceOrder = (TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder");
            //        if (SquanceOrder.Text != "")
            //        {
            //            int id = 0;
            //            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            //            {
            //                id = Convert.ToInt32(Request.QueryString["ID"]);
            //                btnUpdate.Visible = true;
            //                btnSave.Visible = false;
            //            }
            //            else
            //            {
            //                btnSave.Visible = true;
            //                btnUpdate.Visible = false;
            //            }

            //            //sequenceValue.Add(Convert.ToInt32((TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder")));

            //            ifSequenceExist = RLCS_ClientsManagement.CheckSequence(Convert.ToInt32(txtSqnceOrd.Text), Request.QueryString["ClientID"], id);
            //            if (ifSequenceExist)
            //            {
            //                lblPaycodeSequence.Visible = true;
            //                lblPaycodeSequence.Text = SquanceOrder.Text + " Paycode Sequence Already Exist!";
            //                lblPaycodeSequence.CssClass = "alert alert-danger";
            //                btnSave.Enabled = false;
            //                break;
            //            }
            //            else
            //            {
            //                btnSave.Enabled = true;
            //                lblPaycodeSequence.Visible = false;
            //                lblPaycodeSequence.Visible = false;
            //                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            //                {
            //                 //   SquanceOrder.Text = SquanceOrder.Text;
            //                }
            //                else
            //                {
            //                   // if (RLCS_ClientsManagement.CheckSequence(Convert.ToInt32(SquanceOrder.Text), Request.QueryString["ClientID"], 0))
            //                   //     SquanceOrder.Text = Convert.ToString(RLCS_ClientsManagement.GetSequence(Request.QueryString["ClientID"]));
            //                }
            //            }
            //        }
            //    }
            //}
        }
        protected void txtHeader_TextChanged(object sender, EventArgs e)
        {

            bool ifHeaderExist = false;
            if (ViewState["HeaderTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["HeaderTable"];
                ////for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                ////{
                TextBox txtHeader = (TextBox)gvPayCode.Rows[dtCurrentTable.Rows.Count - 1].Cells[0].FindControl("txtHeader");
                DropDownList ddlPaycodeType = (DropDownList)gvPayCode.Rows[dtCurrentTable.Rows.Count - 1].Cells[1].FindControl("ddlPaycodeType");
                DropDownList ddlPaycode = (DropDownList)gvPayCode.Rows[dtCurrentTable.Rows.Count - 1].Cells[2].FindControl("ddlPaycode");
                TextBox squanceOrder = (TextBox)gvPayCode.Rows[dtCurrentTable.Rows.Count - 1].Cells[3].FindControl("txtSquanceOrder"); 
                if (txtHeader.Text != "")
                {
                    string id = "0";
                    if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                    {
                        id = Convert.ToString(Request.QueryString["ID"]);
                        btnUpdate.Visible = true;
                        btnSave.Visible = false;
                    }
                    else
                    {
                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                    }
                    ifHeaderExist = RLCS_ClientsManagement.CheckHeader(txtHeader.Text, Request.QueryString["ClientID"], id);
                    if (ifHeaderExist)
                    {
                        lblPaycodeHeader.Visible = true;
                        ddlPaycodeType.Enabled = false;
                        ddlPaycode.Enabled = false;
                        squanceOrder.Enabled = false;
                        lblPaycodeHeader.Text = "Paycode Header Already Exist : " + txtHeader.Text;
                        lblPaycodeHeader.CssClass = "alert alert-danger";
                        btnSave.Enabled = false;
                    }
                    else
                    {
                        ddlPaycodeType.Enabled = true;
                        ddlPaycode.Enabled = true;
                        squanceOrder.Enabled = true;
                        lblPaycodeHeader.Visible = false;
                        lblPaycodeHeader.Text = "";
                        btnSave.Enabled = true;
                    }
                }
                ////}
            }

            lblPaycodeAndType.Visible = false;
            lblPaycodeAndType.Text = "";
            lblPaycodeAndType.CssClass = "";
        }
        protected void btnExcel_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["HeaderTable"];
            DataRow dr = null;
            if (Session["HeaderList"] != null)
            {
                List<StandardColumnModel> lst = new List<StandardColumnModel>();
                lst = Session["HeaderList"] as List<StandardColumnModel>;
                int Sorder = Convert.ToInt32(ViewState["Sorder"]);

                foreach (var item in lst)
                {
                    dr = dt.NewRow();
                    dr["ID"] = 0;
                    dr["HeaderName"] = item.CPMD_Standard_Column;
                    if (ViewState["Sorder"] != null)
                    {
                        Sorder = Sorder + 1;
                        dr["SquanceOrder"] = Sorder;
                        ViewState["Sorder"] = Sorder;
                    }
                    dt.Rows.Add(dr);
                }
            }
            Session["HeaderList"] = null;
            ViewState["HeaderTable"] = dt;
            gvPayCode.DataSource = dt;
            gvPayCode.DataBind();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TextBox SquanceOrder = (TextBox)gvPayCode.Rows[i].Cells[3].FindControl("txtSquanceOrder");
                SquanceOrder.Text = SquanceOrder.Text = dt.Rows[i]["SquanceOrder"].ToString();
                CheckBox chkESI = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxESI");
                CheckBox chkPF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxPF");
                CheckBox chkPT = (CheckBox)gvPayCode.Rows[i].Cells[4].FindControl("CheckboxPT");
                CheckBox chkLWF = (CheckBox)gvPayCode.Rows[i].Cells[5].FindControl("CheckboxLWF");
                if (dt.Rows[i]["ESI"].ToString() == "YES") { chkESI.Checked = true; }
                if (dt.Rows[i]["PF"].ToString() == "YES") { chkPF.Checked = true; }
                if (dt.Rows[i]["PT"].ToString() == "YES") { chkPT.Checked = true; }
                if (dt.Rows[i]["LWF"].ToString() == "YES") { chkLWF.Checked = true; }
            }
            SetPreviousData();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lblPaycodeAndType.Text == "" && lblPaycodeType.Text == "" && lblPaycodeHeader.Text == "" && lblPaycodeSequence.Text == "")
                AddNewRowToGrid();
        }

        protected void ddlPaycode_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            string[] arrStandard1 = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS" };
            DropDownList ddlPaycodeType = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddlPaycodeType.NamingContainer;
            DropDownList PaycodeType = (DropDownList)row.FindControl("ddlPaycodeType");
            DropDownList Paycode = (DropDownList)row.FindControl("ddlPaycode");

            if (btnAddClick == true && PaycodeType.SelectedValue == "S" && arrStandard1.Contains(Paycode.SelectedValue))
            {
                lblPaycodeType.Visible = true;
                lblPaycodeType.Text = "You can not select " + Paycode.SelectedValue + " PayCode Type!";
                lblPaycodeType.CssClass = "alert alert-danger";
                btnSave.Enabled = false;
            }
            else
            {
                lblPaycodeType.Visible = false;
                lblPaycodeType.Text = "";
                btnSave.Enabled = true;

            }
        }

        protected void btnHidden_Click(object sender, EventArgs e)
        {
            Session["HeaderName"] = ViewState["HeaderTable"];
        }
    } 
}