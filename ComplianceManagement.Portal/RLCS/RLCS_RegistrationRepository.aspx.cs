﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Configuration;
using System.Reflection;
using System.Web.UI;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_RegistrationRepository : System.Web.UI.Page
    {
        protected int CustId;
        protected int UserId;
        protected static string Path;
        protected static string ProfileID;
        public static string RLCSRegistrationDocPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
            if (!string.IsNullOrEmpty(Request.QueryString["RLCSRecordID"]) && !string.IsNullOrEmpty(Request.QueryString["RLCSDockType"]))
            {
                long RecordID = Convert.ToInt64(Request.QueryString["RLCSRecordID"].ToString());
                string DocType = Request.QueryString["RLCSDockType"].ToString();
                string Event = Request.QueryString["Event"].ToString();

                if (Event == "Download")
                {
                    bool _objDownloadval = RLCS_DocumentDownload(RecordID, DocType);
                    if (!_objDownloadval)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document to Download')", true);
                    }
                }
            }
        }

        private bool RLCS_DocumentDownload(long recordID, string docType)
        {
            try
            {
                string fileName = string.Empty;

                if (!string.IsNullOrEmpty(docType))
                {
                    if (docType.Trim().Equals("SOW03"))
                        fileName = "Register";
                    else if (docType.Trim().Equals("SOW04"))
                        fileName = "Challan";
                    else if (docType.Trim().Equals("SOW05"))
                        fileName = "Return";
                    else if (docType.Trim().Equals("REN"))
                        fileName = "Registration";
                    else if (docType.Trim().Equals("SOW05"))
                        fileName = "Registration";
                }

                if (recordID > 0 && docType != null)
                {
                    bool getDocumentDownloadval = RLCS_DocumentManagement.GetRLCSDocuments_Historical(recordID, docType, fileName);
                    if (!getDocumentDownloadval)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }

            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        //private bool RLCS_RegistrationfileDownload(long RecordID, string DocType)
        //{
        //    try
        //    {
        //        if (RecordID > 0 && DocType != null)
        //        {
        //            bool getDocumentDownloadval = RLCS_DocumentManagment.GetRLCSDocuments_Historical(RecordID, DocType, "Registration");

        //            if (!getDocumentDownloadval)
        //            {
        //                return false;
        //            }
        //            else
        //            {
        //                return true;
        //            }
        //        }
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}
    }
}