﻿using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_CustomerSelection : System.Web.UI.Page
    {
        protected IList<Cust> lstUserAssignedCustomers;
        public class Cust
        {
            public int custID { get; internal set; }
            public string custName { get; internal set; }
            public string logoPath { get; internal set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindUserAssignedCustomers();
            //if (!IsPostBack)
            //{
            //    BindUserAssignedCustomers();
            //}
        }

        public void BindUserAssignedCustomers()
        {
            try
            {
                int loggedInUserID = Convert.ToInt32((AuthenticationHelper.UserID));

                var custmerlist = UserCustomerMappingManagement.Get_UserCustomerMapping(loggedInUserID);

                lstUserAssignedCustomers = new List<Cust>();

                if (custmerlist.Count > 1)
                {
                    foreach (var item in custmerlist)
                    {
                        string path = string.Empty;
                        if (item.LogoPath != null)
                            path = item.LogoPath.Replace("~", "");

                        lstUserAssignedCustomers.Add(new Cust()
                        {
                            custID = item.ID,
                            custName = item.Name,
                            logoPath = string.IsNullOrEmpty(path) ? null : path
                        });
                    }
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClick_Click(object sender, EventArgs e)
        {
            int customerID = Convert.ToInt32(hfCustId.Value);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", "alert('" + customerID + "')", true);

            if (customerID != 0)
            {
                string[] userDetails = HttpContext.Current.User.Identity.Name.Split(';');

                if (userDetails.Length == 9)
                {
                    //profileID = userDetails[7];
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}",
                      userDetails[0], userDetails[1], userDetails[2], userDetails[3], userDetails[4], customerID, userDetails[6], userDetails[7]
                      , userDetails[8], userDetails[9], userDetails[10], userDetails[11]), false);

                    Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                }
            }
        }
    }
}