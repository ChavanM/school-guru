﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlus_PaymentResponse.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_PaymentResponse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    <link href="/NewCSS/contract_custom_style.css" rel="stylesheet" />

    <script>
        $(document).ready(function () {
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
           
            if ($(window.parent.document).find("#divProceedPayment").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#divProceedPayment").children().first().hide();
        });
    </script>
</head>
<body style="background: none;">
    <form id="form1" runat="server">
    <div>
     <asp:HiddenField ID="hdnCustID" runat="server" />
    <div class="row col-md-12 colpadding0 mb10">
        <div class="row">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                ValidationGroup="PaymentValidationGroup" />
            <asp:CustomValidator ID="cvPayment" runat="server" EnableClientScript="False"
                ValidationGroup="PaymentValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
        </div>

        <div class="col-md-12 text-center mt-1">           
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="form-group row">
                    <div class="col-md-12 text-center">
                        <%if (txnStatus == 1)
                        { %>
                        <div class="service-header">
                            <div class="text-center">
                                <img src="/Images/success-icon.png" height="75" />
                            </div>
                            <div class="text-center">
                                <h1 style="font-size: 20px!important; font-weight: bold; margin: 0px!important">Payment Success</h1>
                            </div>
                        </div>
                        <% } %>
                        <%if (txnStatus != 1)
                        { %>
                        <div class="service-header">
                            <div class="text-center">
                                <img src="/Images/failure-icon.png" height="75" />
                            </div>
                            <div class="text-center">
                                <h1 style="font-size: 20px!important; font-weight: bold; margin: 0px!important">Payment Failure</h1>
                            </div>
                        </div>
                        <% } %>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                        <label class="control-label">OrderID</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <asp:TextBox ID="txtOrderID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ErrorMessage="Required OrderID" ControlToValidate="txtOrderID"
                            runat="server" ValidationGroup="PaymentValidationGroup" Display="None" />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                        <label class="control-label">Mobile</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <asp:TextBox ID="txtPaymentMobile" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ErrorMessage="Required Mobile Number" ControlToValidate="txtPaymentMobile"
                            runat="server" ValidationGroup="PaymentValidationGroup" Display="None" />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                        <label class="control-label">Email</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <asp:TextBox ID="txtPaymentEmail" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ErrorMessage="Required Email" ControlToValidate="txtPaymentEmail"
                            runat="server" ValidationGroup="PaymentValidationGroup" Display="None" />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                        <label class="control-label">Transaction Date</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ErrorMessage="Required Transaction Date" ControlToValidate="txtPaymentDate"
                            runat="server" ValidationGroup="PaymentValidationGroup" Display="None" />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                        <label class="control-label">Amount</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <asp:TextBox ID="txtPaymentAmount" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ErrorMessage="Required Amount" ControlToValidate="txtPaymentAmount"
                            runat="server" ValidationGroup="PaymentValidationGroup" Display="None" />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                        <label class="control-label">Remark</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>

        <div class="clearfix"></div>

        <%if (txnStatus != 1)
            { %>
        <div class="col-md-12 text-center mb-1">
            <asp:Button ID="btnProceedPayment" runat="server" CssClass="btn btn-primary" Text="Try Again" ValidationGroup="PaymentValidationGroup"
                OnClick="btnProceedPayment_Click" CausesValidation="true"></asp:Button>
        </div>
        <% } %>
        <%if (txnStatus == 1)
            { %>
        <div class="col-md-12 text-center mb-1">
            <asp:LinkButton ID="Button1" runat="server" CssClass="btn btn-primary" Text="Back to Payment History" PostBackUrl="~/RLCS/HRPlus_MyPayments.aspx">
            </asp:LinkButton>
        </div>
        <% } %>
    </div>
    </div>
    </form>
</body>
</html>
