﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_DocumentOverview.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_DocumentOverview" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<!DOCTYPE html>
<script>
 
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>

    <%--<script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
        });
    </script>--%>
</head>
<body>
    <form id="form1" runat="server">
         <% if (ShowRepeater == 0)
        { %>
        <div></div>
         <% } 
        else if (ShowRepeater == 1)
        { %>
        <div class="row colpadding0">
            <div class="col-md-1 colpadding0">
                <div class="col-md-12">
               
                </div>
                <table width="100%">
                    <thead>
                        <tr>
                            <td valign="top">
                                <asp:Repeater runat="server" ID="DocViewReapeter" OnItemCommand="DocViewReapeter_ItemCommand" OnItemDataBound="DocViewReapeter_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <%--<td><%# Container.ItemIndex + 1 %></td>--%>
                                            <td>
                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("FileName")+ ","+ Eval("RecordID")%>'
                                                    ID="lblDocumentVersionView" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName")%>'
                                                    Text='<%# Eval("FileName").ToString().Substring(0,6) +"..." %>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>

                    </thead>
                </table>
            </div>
            <div class="col-md-11">
                 <tr> <asp:Label ID="errormsg" style="color:red" runat="server"></asp:Label></tr>
                <GleamTech:DocumentViewer runat="server" Width="100%" ID="doccontrol" FullViewport="false" searchControl="false" SidePaneVisible="false" Height="500px"
                    DownloadAsPdfEnabled="false" DisableHeaderIncludes="false" />
            </div>
        </div>
        <%}
       else if (ShowRepeater == 2)
        { %>
        <div class="row colpadding0">
            <div class="col-md-1 colpadding0">
                <table width="100%">
                    <thead>
                        <tr>
                            <td valign="top">
                                <asp:Repeater runat="server" ID="Repeater1" OnItemCommand="Repeater1_ItemCommand" OnItemDataBound="Repeater1_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <%--<td><%# Container.ItemIndex + 1 %></td>--%>
                                            <td>
                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("Name")%>'
                                                    ID="lblDocumentVersionView" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Name")%>'
                                                    Text='<%# Eval("Name").ToString().Substring(0,6) +"..." %>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>

                    </thead>
                </table>
            </div>
            <div class="col-md-11">
                <tr> <asp:Label ID="lblDocuments" style="color:red" runat="server"></asp:Label></tr>
                <GleamTech:DocumentViewer runat="server" Width="100%" ID="DocumentViewer1" FullViewport="false" searchControl="false" SidePaneVisible="false" Height="500px"
                    DownloadAsPdfEnabled="false" DisableHeaderIncludes="false" />

            </div>
        </div>
        <%} 
        else if (ShowRepeater == 3)
       { %>
        <div class="row colpadding0">
            <div class="col-md-1 colpadding0">
                <table width="100%">
                    <thead>
                        <tr>
                            <td valign="top">
                                <asp:Repeater runat="server" ID="Repeater2" OnItemCommand="Repeater2_ItemCommand" OnItemDataBound="Repeater2_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <%--<td><%# Container.ItemIndex + 1 %></td>--%>
                                            <td>
                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("Name")%>'
                                                    ID="lblDocumentVersionView" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Name")%>'
                                                    Text='<%# Eval("Name").ToString().Substring(0,6) +"..." %>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>

                    </thead>
                </table>
            </div>
            <div class="col-md-11">
                <tr> <asp:Label ID="lblDocuments2" style="color:red" runat="server"></asp:Label></tr>
                <GleamTech:DocumentViewer runat="server" Width="100%" ID="DocumentViewer2" FullViewport="false" searchControl="false" SidePaneVisible="false" Height="500px"
                    DownloadAsPdfEnabled="false" DisableHeaderIncludes="false" />

            </div>
        </div>
        <%}
        else if (ShowRepeater == 4)
        { %>
        <div class="row colpadding0">
            <div class="col-md-1 colpadding0">
                <div class="col-md-12">
               
                </div>
                <table width="100%">
                    <thead>
                        <tr>
                            <td valign="top">
                                <asp:Repeater runat="server" ID="Repeater3" OnItemCommand="DocViewReapeter_ItemCommand" OnItemDataBound="DocViewReapeter_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <%--<td><%# Container.ItemIndex + 1 %></td>--%>
                                            <td>
                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("Name") %>'
                                                    ID="lblDocumentVersionView" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Name")%>'
                                                    Text='<%# Eval("Name").ToString() +"..." %>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>

                    </thead>
                </table>
            </div>
            <div class="col-md-11">
                 <tr> <asp:Label ID="lblDocument3" style="color:red" runat="server"></asp:Label></tr>
                <GleamTech:DocumentViewer runat="server" Width="100%" ID="DocumentViewer3" FullViewport="false" searchControl="false" SidePaneVisible="false" Height="500px"
                    DownloadAsPdfEnabled="false" DisableHeaderIncludes="false" />
            </div>
        </div>
        <%} %>
    </form>
</body>
</html>
