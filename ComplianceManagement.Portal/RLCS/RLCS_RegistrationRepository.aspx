﻿<%@ Page Title="Repository :: My Registrations" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_RegistrationRepository.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_RegistrationRepository" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <%--<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>--%>
    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />
    <title></title>

    <style>
        .k-grid-content { min-height:50px; }
    </style>

    <script type="text/jscript">
        $(document).ready(function () {
            setactivemenu('leftnoticesmenu');
            fhead('My Registrations/ Repository');
                ApiTrack_Activity("Registration", "pageView",null);
        });

        $(document).on("click", "#grid tbody tr .ob-download", function (e) {           
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewpup(item.RecordID,"REG","Download");     
            analytics("Registration","Download",$('#CName').val(),1);
                ApiTrack_Activity('Registration','Download',recordid);
            return true;
        });

        $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpup(item.RecordID,"REG","View");
            analytics("Registration","Overview",$('#CName').val(),1);
                ApiTrack_Activity('Registration','Overview',recordid);
            return true;
        });

        function OpenDownloadOverviewpup(RecordID,DocType,Event) {
           
            $('#DownloadViews').attr('src', "RLCS_RegistrationRepository.aspx?RLCSRecordID=" + RecordID + "&RLCSDockType=" + DocType+"&Event="+Event);
        }
        function OpenDocumentOverviewpup(RecordID,DocType,Event) {
           
            $('#divViewDocument').modal('show');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "RLCS_DocumentOverview.aspx?RecordID=" + RecordID + "&DocType="+DocType+"&Event="+Event)
        }
        function CloseClearDV() {
            $('#DownloadViews').attr('src', "../Common/blank.html");           
        }
        function fcloseStory(obj) {
           
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);            
            $(upperli).remove();
        };
        $(document).ready(function () {
            $("#dropdowntree").kendoDropDownTree({ 
                placeholder: "Entity/State/Location/Branch",
                checkboxes: {
                    checkChildren: true
                },
                //checkboxes: true,
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {

                 
                    if ($("#dropdownActivity").val() != '' && $("#dropdownActivity").val() != undefined) {
                        setCommonAllfilterMain();
                         $('input[id=chkAllMain]').prop('checked', false);
                        $('#dvbtndownloadDocumentMain').css('display', 'none');
                        fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                    }
                    else {
                        var filter = { logic: "or", filters: [] };
                        //  values is an array containing values to be searched
                        var values = this.value();
                        $.each(values, function (i, v) {
                            filter.filters.push({
                                field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                            });
                        });
                        fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        $('input[id=chkAllMain]').prop('checked', false);
                        $('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: "<% =Path%>GetAssignedEntitiesLocationsList?customerId=<% =CustId%>&userId=<% =UserId%>&profileID=<% =ProfileID%>",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }                  
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

        });
        $(document).ready(function () 
        {
         
            var record = 0;
            var customerID =<% =CustId%>;
            $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: "<% =Path%>GetAll_RegistrationRepository?customerID=<% =CustId%>&userID=<% =UserId%>&profileID=<% =ProfileID%>",
                            type: 'GET',
                            //data: function () {
                            //    return {
                            //        customerID: customerID
                            //    };
                            //},
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },

                            dataType: 'json'
                        }
                        //read: "<% =Path%>GetAll_Registrations"
                    },
                    pageSize:10,
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        total: function (response) {
                            return response.Result.length;
                        }
                    }
                },
                //height: 380,
                sortable: true,
                filterable: true,
                columnMenu: true,
                //pageable: {
                //    refresh: true
                //},
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,

                pageable: {
                    refresh: true,
                    //pageSizes: true,
                    buttonCount: 3
                },
              
                columns: [
                     {
                         title: "Sr.No",
                         field: "rowNumber",
                         template: "<span class='row-number'></span>",
                         width: 85,
                         attributes: {
                             style: 'white-space: nowrap;'

                         },
                         filterable: {
                             extra: false,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     }, 
                      {
                          field: "EntityClientName",
                          title: "Entity",
                          width: 100,
                          attributes: {
                              style: 'white-space: nowrap;'
                          },
                          filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                    },
                      {
                            hidden:true,
                            title: "RR_ClientID",
                            field: "RR_ClientID",
                            //template: "<span class='row-number'></span>",
                            width: 70,                         
                        },
                        {
                            hidden:true,
                            title: "AVACOM_BranchID",
                            field: "AVACOM_BranchID",
                            //template: "<span class='row-number'></span>",
                            width: 70,
                         
                        },
                          {
                              hidden:true,
                              title: "RR Doc ID",
                              field: "RR_DocID",
                              //template: "<span class='row-number'></span>",
                              width: 70,
                          }, 
                      {
                          field: "CM_ActivityName",
                          title: "Document Type",
                          width: 150,
                          attributes: {
                              style: 'white-space: nowrap;'

                          },
                          filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                      {
                          field: "SM_Name",
                          title: "State",
                          width: 100,
                          attributes: {
                              style: 'white-space: nowrap;'

                          },
                          filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                      {
                          field: "LM_Name",
                          title: "Location",
                          width: 130,
                          attributes: {
                              style: 'white-space: nowrap;'

                          },
                          filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                    {
                        field: "AVACOM_BranchName",
                        title: "Branch",
                        width: 130,
                        attributes: {
                            style: 'white-space: nowrap;'

                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    //{
                    //    //field: "RR_ExpiryDate",
                    //    title: "Establisment Address",
                    //    width: 140,
                    //    type: "date",
                    //    format: "{0:dd-MMM-yyyy}",
                    //    attributes: {
                    //        style: 'white-space: nowrap;'

                    //    },
                    //    filterable: {
                    //        extra: false,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                    {
                        field: "RR_ExpiryDate",
                        title: "Expiry Date",
                        width: 150,
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'

                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                      {// template: kendo.template('<span class="file-icon img-file"></span>').html()
                          command: [
                           { name:"editLable", text: "No Document" },
                              { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download",width:70},
                               { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                          
                          ], title: "Action", lock: true,width: 150,

                      }

                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1 
                        + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });


                        //Selects all delete buttons
                    $("#grid tbody tr .k-grid-edit1").each(function () {
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Not Available') {
                            $(this).remove();
                        }                    
                    });
                    $("#grid tbody tr .k-grid-edit2").each(function () {
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Not Available') {
                            $(this).remove();
                        }                    
                    });


                      $("#grid tbody tr .k-grid-editLable").each(function () {
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Available') {
                            $(this).remove();
                        }                    
                    });
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",                  
                content: function (e) {         
                    return "Download";              
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",                  
                content: function (e) {         
                    return "View";              
                }
            });
        });
               
        $(document).ready(function() {
            $(document).ready(function() {
                $("#dropdownActivity").kendoDropDownList({
                    dataTextField: "CM_ActivityName",
                    dataValueField: "CM_ID",
                    optionLabel: "All Activities",
                    change: function (e) {
                        debugger;
                        if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {
                            setCommonAllfilterMain();
                        }
                        else {
                            var values = this.value();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                var values = this.value();
                                filter.filters.push({
                                    field: "RR_DocID", operator: "eq", value: values
                                });
                                var dataSource = $("#grid").data("kendoGrid").dataSource;

                                dataSource.filter(filter);
                            }
                            else {
                                $("#grid").data("kendoGrid").dataSource.filter({});
                            }
                        }
                    },
                    dataSource: {
                        transport: {
                            read: {
                                url: "<% =Path%>GetAll_OneTimeActivityMaster",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                                dataType: 'json',
                            }                           
                        },
                        schema: {
                            data: function (response) {                      
                                return response.Result;
                            }
                        }
                    }
                });
            });
        });


        function setCommonAllfilterMain() {

            if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                && $("#dropdownActivity").val() != '' && $("#dropdownActivity").val() != undefined) {

                //location details
                var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list, function (i, v) {
                    locationsdetails.push({
                        field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                    });
                });

                //Activities Details
                var Activitiedetails = [];
                Activitiedetails.push({
                    field: "RR_DocID", operator: "eq", value: $("#dropdownActivity").val()
                });


                var dataSource = $("#grid").data("kendoGrid").dataSource;

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Activitiedetails
                        },
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });

            }


            else  if ($("#dropdownActivity").val() != '' && $("#dropdownActivity").val() != undefined) {
                               
                //Activities Details
                var Activitiedetails = [];
                Activitiedetails.push({
                    field: "RR_DocID", operator: "eq", value: $("#dropdownActivity").val()
                });

                var dataSource = $("#grid").data("kendoGrid").dataSource;

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Activitiedetails
                        }
                    ]
                });

            }

              else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {

                //location details
                var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list, function (i, v) {
                    locationsdetails.push({
                        field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                    });
                });

             
                var dataSource = $("#grid").data("kendoGrid").dataSource;

                dataSource.filter({
                    logic: "and",
                    filters: [                       
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });

            }


        }


        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
           
            $('#ClearfilterMain').css('display', 'none');            
        }
        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#ddlMonth').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#ddlYear').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }
        function fCreateStoryBoard(Id, div, filtername) {
            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
       
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            $('#Clearfilter').css('display', 'none');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtertype') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                //  $('#' + div).append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterpstData1') {
                $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompType') {
                $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtertype1') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');                
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterFY') {
                $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }                
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;white-space: nowrap;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }



            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                //
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="col-md-4 colpadding0">
                <input id="dropdowntree" data-placeholder="Entity/State/Location/Branch" style="width: 98%;" />
            </div>
            <div class="col-md-4 colpadding0">
                <input id="dropdownActivity" style="width: 98%" />
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-2 colpadding0">
                 <button id="ClearfilterMain" style="float: right; margin-right: 1%; display:none" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
            </div>
        </div>
    </div>

    <div id="grid" style="border: none; margin-top: 5px">
        <div class="k-header k-grid-toolbar">
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filtersstoryboard">&nbsp;</div>
        </div>
    </div>
    <%--DownLoad ModalPOPup--%>
    <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 360px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="DownloadViews" src="about:blank" width="300px" height="150px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%--View Modalpopup--%>
    <%-- <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 1150px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="OverViews" src="about:blank" width="100%" height="100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>--%>

    <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <%-- <div style="float: left; width: 12%">
                                <table width="100%" style="text-align: left; margin-left: 25%;">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="upLitigationDetails" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptLitigationVersionView" runat="server" OnItemCommand="rptLitigationVersionView_ItemCommand"
                                                            OnItemDataBound="rptLitigationVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="RLCS_HistoricalDocument_FileData">
                                                                    <thead>
                                                                        <th><b>File Name</b></th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("RecordID") + ","+ Eval("FileName")%>'
                                                                            ID="lblDocumentVersionView" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName")%>'
                                                                            Text='<%# Eval("FileName").ToString() +"..." %>'></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptLitigationVersionView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>--%>
                            <div class="col-md-12">
                                <%--<asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>--%>
                                <fieldset style="height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="OverViews" width="100%" height="100%" frameborder="0"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
