﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web.Security;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_DFMDashboard : System.Web.UI.Page
    {
        protected static DateTime FromFinancialYearSummery;
        protected static DateTime ToFinancialYearSummery;
        protected string Performername;
        protected string Reviewername;
        protected string InternalPerformername;
        public static string CalendarDate;
        public static DateTime CalendarTodayOrNextDate;
        public static string date = "";
        protected static string CalenderDateString;
        protected static string perFunctionChart;
        protected static string perFunctionPieChart;
        protected static string perPenaltyStatusPieChart;
        protected static string perRiskChart;
        protected static string perFunctionHIGHPenalty;
        protected static string perFunctionMEDIUMPenalty;
        protected static string perFunctionLOWPenalty;
        protected static int customerID;
        protected static int BID;
        protected static string IsSatutoryInternal;
        protected static string PieMonth;
        protected static bool IsApprover = false;
        protected static int IsStatues = 0;
        public static List<long> Branchlist = new List<long>();

        public List<KeyValuePair<int, string>> LstApplicableStatues = new List<KeyValuePair<int, string>>();
        bool IsPenaltyVisible = true;

        protected static string data_areas;

        protected static string tlConnect_url = String.Empty;
        protected static string rlcs_API_url;

        protected static string seriesData_GraphMonthlyCompliancePie;
        protected static string complianceSummaryMonth;
        
        protected static string userProfileID;
        protected static string userProfileID_Encrypted;
        protected static string authKey;
        protected static string profileID;
        protected static string RegulatoryAuthKey;
        protected static string UserAuthKey;
        protected static string AvacomRLCSApiURL;

        protected static string userID;
        //ChartPieMonthly_Category
        List<int> assignedbranchIDs = new List<int>();

        protected bool initialLoad;

        protected void Page_Load(object sender, EventArgs e)
        {
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            //custID = Convert.ToString(AuthenticationHelper.CustomerID);
            userID = Convert.ToString(AuthenticationHelper.UserID);

            rlcs_API_url = ConfigurationManager.AppSettings["RLCSAPIURL"].ToString();
            RegulatoryAuthKey = ConfigurationManager.AppSettings["RegulatoryAuth"].ToString();
            UserAuthKey = ConfigurationManager.AppSettings["X-User-Id-1"].ToString();
            AvacomRLCSApiURL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"].ToString();

            if (!IsPostBack)
            {
                try
                {
                    #region IsPostBack
                    Session["masterpage"] = "HRPlusCompliance";

                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            initialLoad = true;
                            IsApprover = false;

                            HiddenField home = (HiddenField)Master.FindControl("Ishome");
                            home.Value = "true";

                            entities.Database.CommandTimeout = 180;
                            var userAssignedBranchList = (entities.SP_GetManagerAssignedBranch(AuthenticationHelper.UserID)).ToList();

                            if (userAssignedBranchList != null)
                            {
                                tlConnect_url = ConfigurationManager.AppSettings["TLConnect_API_URL"].ToString();

                                if (userAssignedBranchList.Count > 0)
                                {
                                    assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                                }

                                var Ecnt = (entities.sp_Entitycount(Convert.ToInt32(AuthenticationHelper.UserID), "SAT")).ToList();
                                if (Ecnt.Count > 0)
                                {
                                    divEntitesCount.InnerText = Convert.ToString(Ecnt.Count);
                                }
                                else
                                {
                                    divEntitesCount.InnerText = "0";
                                }

                                Branchlist.Clear();
                                BindLocationFilter(assignedbranchIDs);

                                //setCompliancesSummaryCounts(assignedbranchIDs);

                                LoadDashboard(customerID);

                                fldsCalender.Visible = true;

                                string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];

                                if (!string.IsNullOrEmpty(TLConnectKey))
                                {
                                    userProfileID = string.Empty;
                                    userProfileID = AuthenticationHelper.ProfileID;
                                    if (!string.IsNullOrEmpty(userProfileID))
                                    {
                                        userProfileID_Encrypted = CryptographyHandler.encrypt(userProfileID.Trim(), TLConnectKey);
                                    }
                                    authKey = AuthenticationHelper.AuthKey;
                                }

                                BindStates();



                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                                divTabs.Visible = true;
                                ComplianceCalender.Visible = true;
                                DivOverDueCompliance.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        protected void clear_click(object sender,EventArgs e)
        {
            if (DateTime.Now.Month > 9)
            {
                ddlMonthReg.SelectedValue = Convert.ToString(DateTime.Now.Month);
                ddlYearReg.SelectedValue = Convert.ToString(DateTime.Now.Year);
            }
            else
            {
                ddlMonthReg.SelectedValue = "0" + Convert.ToString(DateTime.Now.Month);
                ddlYearReg.SelectedValue = Convert.ToString(DateTime.Now.Year);
            }
            ddlState.SelectedValue = "-1";
            ddlAct.SelectedValue = "-1";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptbinddailyupdatedata", "binddailyupdatedata();", true);

        }

        protected void setCompliancesSummaryCounts(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;

                    var MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)
                      .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.RoleID == 3)).ToList();

                    if (MastertransactionsQuery.Count > 0)
                    {
                        if (assignedBranchList.Count > 0)
                            MastertransactionsQuery = MastertransactionsQuery.Where(row => assignedBranchList.Contains(row.CustomerBranchID)).ToList();

                        //Upcoming                                     
                        divUpcomingCount.InnerText = Convert.ToString(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "Upcoming").Count());

                        //DueToday                                     
                        //divDueToday.InnerText = Convert.ToString(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "DueToday").Count());

                        //Pending Review                                     
                        divPendingReviewCount.InnerText = Convert.ToString(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "Pending").Count());

                        //Overdue                                     
                        divOverdueCount.InnerText = Convert.ToString(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "Overdue").Count());

                        //HighRisk                                     
                        //divhighRiskCount.InnerText = Convert.ToString(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "HighRisk").Count());
                    }
                    else
                    {
                        divUpcomingCount.InnerText = "0";
                        //divDueToday.InnerText = "0";                        
                        divOverdueCount.InnerText = "0";
                        divPendingReviewCount.InnerText = "0";

                        //showPerfRiskMapCalendarChart = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> GetFilteredData(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery)
        {
            bool filterApplied = false;

            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
            {
                BID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                filterApplied = true;
            }
            else
            {
                BID = 0;
            }

            if (BID > 0)
                MastertransactionsQuery = MastertransactionsQuery.Where(entry => (entry.CustomerBranchID == BID)).ToList();

            #region Date Period Filter
            try
            {
                DateTime? startDate = null;
                DateTime? endDate = null;

                if (!string.IsNullOrEmpty(txtStartPeriod.Text) && !string.IsNullOrEmpty(txtEndPeriod.Text))
                {
                    string[] startPeriod = txtStartPeriod.Text.Split('-');
                    if (startPeriod.Length > 1)
                    {
                        string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(startPeriod[0]);

                        if (!string.IsNullOrEmpty(monthValue))
                        {
                            int month = Convert.ToInt32(monthValue);
                            int year = Convert.ToInt32(startPeriod[1]);

                            startDate = new DateTime(year, month, 1).Date;
                        }
                    }

                    string[] endPeriod = txtEndPeriod.Text.Split('-');
                    if (endPeriod.Length > 1)
                    {
                        string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(endPeriod[0]);

                        if (!string.IsNullOrEmpty(monthValue))
                        {
                            int month = Convert.ToInt32(monthValue);
                            int year = Convert.ToInt32(endPeriod[1]);

                            endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(txtStartPeriod.Text) && string.IsNullOrEmpty(txtEndPeriod.Text))
                {
                    string[] startPeriod = txtStartPeriod.Text.Split('-');
                    if (startPeriod.Length > 1)
                    {
                        string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(startPeriod[0]);

                        if (!string.IsNullOrEmpty(monthValue))
                        {
                            int month = Convert.ToInt32(monthValue);
                            int year = Convert.ToInt32(startPeriod[1]);

                            startDate = new DateTime(year, month, 1).Date;
                            endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
                        }
                    }
                }
                else if (string.IsNullOrEmpty(txtStartPeriod.Text) && !string.IsNullOrEmpty(txtEndPeriod.Text))
                {
                    string[] startPeriod = txtStartPeriod.Text.Split('-');
                    if (startPeriod.Length > 1)
                    {
                        string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(startPeriod[0]);

                        if (!string.IsNullOrEmpty(monthValue))
                        {
                            int month = Convert.ToInt32(monthValue);
                            int year = Convert.ToInt32(startPeriod[1]);

                            startDate = new DateTime(year, month, 1).Date;
                            endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
                        }
                    }
                }

                if (startDate != null && endDate != null)
                {
                    filterApplied = true;
                    MastertransactionsQuery = MastertransactionsQuery.Where(row => row.PerformerScheduledOn.Date >= startDate && row.PerformerScheduledOn.Date <= endDate).ToList();
                }
            }
            catch (Exception ex)
            {

            }

            #endregion

            #region Period Filter

            if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
            {
                List<string> lstPeriod = new List<string>();
                lstPeriod = GetSelectedPeriod();

                string frequencySelected = string.Empty;
                frequencySelected = ddlFrequency.SelectedValue;

                if (frequencySelected.Equals("BiAnnual"))
                    frequencySelected = "TwoYearly";

                if (ddlFrequency.SelectedValue != "-1" && lstPeriod.Count > 0)
                {
                    MastertransactionsQuery = MastertransactionsQuery.Where(row => row.Frequency.Equals(frequencySelected) && lstPeriod.Contains(row.RLCS_PayrollMonth)).ToList();
                    filterApplied = true;
                }
                else if (ddlFrequency.SelectedValue != "-1")
                {
                    MastertransactionsQuery = MastertransactionsQuery.Where(row => row.Frequency.Equals(frequencySelected)).ToList();
                    filterApplied = true;
                }
            }

            if (!string.IsNullOrEmpty(ddlYear.SelectedValue))
            {
                if (ddlYear.SelectedValue != "-1")
                {
                    MastertransactionsQuery = MastertransactionsQuery.Where(row => row.RLCS_PayrollYear.Equals(ddlYear.SelectedValue)).ToList();
                    filterApplied = true;
                }
            }

            #endregion

            if (filterApplied)
                btnClear.Visible = true;
            else
                btnClear.Visible = false;

            return MastertransactionsQuery;
        }

        public List<string> GetSelectedPeriod()
        {
            List<string> lstPeriod = new List<string>();
            try
            {
                foreach (RepeaterItem aItem in rptPeriodList.Items)
                {
                    try
                    {
                        CheckBox chkPeriod = (CheckBox)aItem.FindControl("chkPeriod");
                        if (chkPeriod != null && chkPeriod.Checked)
                        {
                            Label lblPeriodName = (Label)aItem.FindControl("lblPeriodName");
                            if (lblPeriodName != null && !string.IsNullOrEmpty(lblPeriodName.Text))
                            {
                                if (!string.IsNullOrEmpty(lblPeriodName.Text.Trim()))
                                {
                                    Label lblPeriodID = (Label)aItem.FindControl("lblPeriodID");
                                    if (lblPeriodID != null && !string.IsNullOrEmpty(lblPeriodID.Text))
                                    {
                                        if (ddlFrequency.SelectedValue == "Monthly")
                                            lstPeriod.Add(lblPeriodID.Text);
                                        else if (ddlFrequency.SelectedValue == "Quarterly")
                                        {
                                            if (lblPeriodID.Text == "Q1")
                                            {
                                                lstPeriod.Add("03");//CY
                                                lstPeriod.Add("06");//FY
                                            }
                                            else if (lblPeriodID.Text == "Q2")
                                            {
                                                lstPeriod.Add("06");//CY
                                                lstPeriod.Add("09");//FY
                                            }
                                            else if (lblPeriodID.Text == "Q3")
                                            {
                                                lstPeriod.Add("09");//CY
                                                lstPeriod.Add("12");//FY
                                            }
                                            else if (lblPeriodID.Text == "Q4")
                                            {
                                                lstPeriod.Add("12");//CY
                                                lstPeriod.Add("03");//FY
                                            }
                                        }
                                        else if (ddlFrequency.SelectedValue == "HalfYearly")
                                        {
                                            if (lblPeriodID.Text == "HY1")
                                            {
                                                lstPeriod.Add("06");//CY
                                                lstPeriod.Add("09");//FY
                                            }
                                            else if (lblPeriodID.Text == "HY2")
                                            {
                                                lstPeriod.Add("12");//CY
                                                lstPeriod.Add("03");//FY
                                            }
                                        }
                                        else if (ddlFrequency.SelectedValue == "Annual")
                                        {
                                            lstPeriod.Add("12");//CY
                                            lstPeriod.Add("03");//FY
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {

                    }
                }

                return lstPeriod;
            }
            catch (Exception ex)
            {
                return lstPeriod;
            }
        }

        protected void BindYears(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQueryList)
        {
            try
            {
                var lstYears = MastertransactionsQueryList.Where(row => row.RLCS_PayrollYear != null).Select(row => row.RLCS_PayrollYear).Distinct().OrderByDescending(row => row).ToList();

                ddlYear.Items.Clear();

                ddlYear.DataSource = lstYears;
                ddlYear.DataBind();

                ddlYear.Items.Insert(0, new ListItem("Year", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void LoadDashboard(int custID)
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                var userRecord = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));

                if (userRecord != null)
                {
                    if (userRecord.CustomerID != null)
                    {
                        distributorID = Convert.ToInt32(userRecord.CustomerID);
                    }
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, customerID, distributorID, serviceProviderID, AuthenticationHelper.Role)
                                            .Where(entry => entry.IsActive == true
                                                && entry.IsUpcomingNotDeleted == true
                                                && entry.CustomerID == AuthenticationHelper.CustomerID)).ToList();

                    if (MastertransactionsQuery.Count > 0)
                    {
                        //Compliance Count
                        if (MastertransactionsQuery != null)
                        {
                            if (MastertransactionsQuery.Count > 0)
                            {
                                var complianceCount = MastertransactionsQuery.Select(entry => entry.ComplianceID).Distinct().Count();
                                divCompliancesCount.InnerText = complianceCount.ToString();
                            }
                            else
                            {
                                divCompliancesCount.InnerText = "0";
                            }
                        }

                        if (initialLoad)
                            BindYears(MastertransactionsQuery);

                        SetTopBoxesCounts(MastertransactionsQuery);

                        MastertransactionsQuery = GetFilteredData(MastertransactionsQuery);

                        BindOverDueComplianceList(MastertransactionsQuery);
                        GetApplicableStatues(MastertransactionsQuery);
                        GetComplianceStatusCount_CustomerWise(MastertransactionsQuery);
                        BindPerfRiskSummary(custID, MastertransactionsQuery);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void SetTopBoxesCounts(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQueryList) /*UserAllCustomer*/
        {
            try
            {
                if (MastertransactionsQueryList.Count > 0)
                {
                    divUpcomingCount.InnerText = Convert.ToString(DashboardData_ComplianceDisplayCount(MastertransactionsQueryList, "Upcoming").Count());
                    divOverdueCount.InnerText = Convert.ToString(DashboardData_ComplianceDisplayCount(MastertransactionsQueryList, "Overdue").Count());
                    divPendingReviewCount.InnerText = Convert.ToString(DashboardData_ComplianceDisplayCount(MastertransactionsQueryList, "PendingForReview").Count());
                }
                else
                {
                    divUpcomingCount.InnerText = "0";
                    divOverdueCount.InnerText = "0";
                    divPendingReviewCount.InnerText = "0";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardData_ComplianceDisplayCount(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery, string filter)
        {
            List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result>();

            if (!string.IsNullOrEmpty(filter))
            {
                DateTime now = DateTime.Now.Date;
                DateTime nextOneMonth = DateTime.Now.AddMonths(1);
                DateTime nextTenDays = DateTime.Now.AddDays(10);

                switch (filter)
                {
                    case "Upcoming":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "Overdue":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                              && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case "PendingForReview":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case "DueToday":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.PerformerScheduledOn == now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "HighRisk":
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.PerformerScheduledOn < nextTenDays
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
            }

            return transactionsQuery.ToList();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                //GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";


                GridMonthlyPie.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                GridMonthlyPie.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;


                //Reload the Grid
                //BindCompliancePieChart(assignedbranchIDs);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                GridMonthlyPie.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                GridMonthlyPie.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                //BindCompliancePieChart(assignedbranchIDs);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                // DivRecordsScrum.Visible = true;
                if (Convert.ToString(Session["TotalRows"]) != null && Convert.ToString(Session["TotalRows"]) != "")
                {
                    lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
                    lTotalCount.Text = GetTotalPagesCount().ToString();

                    if (lTotalCount.Text != "0")
                    {
                        if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                        {
                            SelectedPageNo.Text = "1";
                            lblStartRecord.Text = "1";

                            if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                                lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                            else
                                lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                        }
                    }
                    else if (lTotalCount.Text == "0")
                    {
                        SelectedPageNo.Text = "0";
                        // DivRecordsScrum.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        #region Common Function   

        private void BindOverDueComplianceList(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQueryList)
        {
            try
            {
                IsSatutoryInternal = "Statutory";
                DateTime now = DateTime.Now.Date;
                MastertransactionsQueryList = MastertransactionsQueryList.Where(row => (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10) && row.ScheduledOn < now).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).Take(4).ToList();

                grdSummaryDetails.Visible = true;
                grdSummaryDetails.DataSource = MastertransactionsQueryList;
                grdSummaryDetails.DataBind();

                if (MastertransactionsQueryList.Count > 0)
                    lnkOverdueCompliance.Visible = true;
                else
                    lnkOverdueCompliance.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected string GetPerformer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        
        private void BindPerfRiskSummary(int customerID, List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQueryList)
        {
            try
            {
                GetManagementCompliancesSummary(MastertransactionsQueryList, customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue),
                    "T", FromFinancialYearSummery, ToFinancialYearSummery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), AuthenticationHelper.UserID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        public void GetApplicableStatues(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQueryList)
        {
            string str1 = string.Empty;
            IsStatues = 0;



            var assignedActs = MastertransactionsQueryList.GroupBy(entity => entity.ActID).Select(entity => entity.FirstOrDefault()).ToList();

            if (assignedActs.Count > 0)
            {
                assignedActs = assignedActs.OrderBy(row => row.ActName).ToList();
                foreach (var i in assignedActs)
                {
                    LstApplicableStatues.Add(new KeyValuePair<int, string>(i.ActID, i.ActName));
                    str1 += " ['" + i.ActID + "', '" + i.ActName + "'],";
                }
            }

            if (LstApplicableStatues.Count > 0)
            {
                StatuesCount.InnerText = Convert.ToString(LstApplicableStatues.Count);
                IsStatues = 1;
            }
        }

        public void GetApplicableStatues(int custID, int userID)
        {
            string str1 = "";
            IsStatues = 0;
            bool IsAvantisFlag = true;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statues = (entities.SP_RLCS_GetApplicableActs(custID, userID, AuthenticationHelper.ProfileID, IsAvantisFlag)).ToList();

                statues = statues.GroupBy(entity => entity.ACTID).Select(entity => entity.FirstOrDefault()).ToList();
                if (tvFilterLocation.SelectedValue == "")
                    BID = 0;
                else
                    BID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                if (BID > 0)
                    statues = statues.Where(entry => (entry.BranchID == BID)).ToList();

                if (statues.Count > 0)
                {
                    statues = statues.OrderBy(row => row.ACTNAME).ToList();
                    foreach (var i in statues)
                    {
                        LstApplicableStatues.Add(new KeyValuePair<int, string>(i.ACTID, i.ACTNAME));
                        str1 += " ['" + i.ACTID + "', '" + i.ACTNAME + "'],";
                    }
                }

                if (LstApplicableStatues.Count > 0)
                {
                    StatuesCount.InnerText = Convert.ToString(LstApplicableStatues.Count);
                    IsStatues = 1;
                }
            }
        }

        #region Top DropDown Selected Index Change

        private void BindUserColors()
        {
            try
            {
                var Cmd = Business.ComplianceManagement.Getcolor(AuthenticationHelper.UserID);
                if (Cmd != null)
                {
                    highcolor.Value = Cmd.High;
                    mediumcolor.Value = Cmd.Medium;
                    lowcolor.Value = Cmd.Low;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                LoadDashboard(customerID);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTreeViewSelection(tvFilterLocation);
                TreeViewSelectedNode(tvFilterLocation, "-1");
                tvFilterLocation_SelectedNodeChanged(sender, e);

                txtStartPeriod.Text = "";
                txtEndPeriod.Text = "";

                ddlFrequency.ClearSelection();
                ddlFrequency_SelectedIndexChanged(sender, e);

                ddlYear.ClearSelection();

                LoadDashboard(customerID);
            }
            catch (Exception ex) { LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name); }
        }

        #endregion

        #region  Function Report

        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocation.Nodes.Clear();



                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "Entity/Branch";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime? GetDate(string date)
        {
            if (date != null && date != "")
            {
                string date1 = "";
                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }
                return Convert.ToDateTime(date1);
            }
            else
            {
                return null;
            }
        }

        public void BindEntityCount(string tlConnectAPIUrl, string profileID, string encryptedProfileID, string authenticationKey)
        {
            try
            {
                string requestUrl = string.Empty;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    requestUrl = tlConnectAPIUrl + "Dashboard/GetProfile_BasicInformationByProfileID?profileID=" + profileID;

                    string responseData = RLCSAPIClasses.InvokeWithAuthKey("GET", requestUrl, authenticationKey, encryptedProfileID, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var _objProfileInfo = JsonConvert.DeserializeObject<Profile_BasicInfo>(responseData);

                        if (_objProfileInfo != null)
                        {
                            if (_objProfileInfo.lstBasicInfrmation != null)
                            {
                                divEntitesCount.InnerText = _objProfileInfo.lstBasicInfrmation.Count.ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetManagementCompliancesSummary(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MasterManagementCompliancesSummaryQuery, int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;

                perFunctionChart = string.Empty;
                perFunctionPieChart = string.Empty;
                perRiskChart = string.Empty;

                string selectedFreq = ddlFrequency.SelectedValue;
                string selectedPeriod = string.Join(",", GetSelectedPeriod());
                string selectedYear = ddlYear.SelectedValue;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (MasterManagementCompliancesSummaryQuery != null)
                    {
                        if (MasterManagementCompliancesSummaryQuery.Count > 0)
                        {
                            if (Branchlist.Count > 0)
                            {
                                MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                            }

                            DateTime EndDate = DateTime.Today.Date;
                            DateTime? PassEndValue = EndDate;

                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                            //PieChart
                            long totalPieCompletedcount = 0;
                            long totalPieNotCompletedCount = 0;
                            long totalPieAfterDueDatecount = 0;

                            long totalPieCompletedHIGH = 0;
                            long totalPieCompletedMEDIUM = 0;
                            long totalPieCompletedLOW = 0;

                            long totalPieAfterDueDateHIGH = 0;
                            long totalPieAfterDueDateMEDIUM = 0;
                            long totalPieAfterDueDateLOW = 0;

                            long totalPieNotCompletedHIGH = 0;
                            long totalPieNotCompletedMEDIUM = 0;
                            long totalPieNotCompletedLOW = 0;

                            long highcount;
                            long mediumCount;
                            long lowcount;
                            long totalcount;

                            DataTable table = new DataTable();
                            table.Columns.Add("ID", typeof(int));
                            table.Columns.Add("Catagory", typeof(string));
                            table.Columns.Add("High", typeof(long));
                            table.Columns.Add("Medium", typeof(long));
                            table.Columns.Add("Low", typeof(long));

                            string listCategoryId = "";
                            List<sp_ComplianceAssignedCategory_Result> CatagoryList = new List<sp_ComplianceAssignedCategory_Result>();

                            CatagoryList = RLCSManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist);

                            string perFunctionHIGH = "";
                            string perFunctionMEDIUM = "";
                            string perFunctionLOW = "";

                            string perFunctiondrilldown = "";

                            perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: '#FF0000' , data: ["; /*perFunctionChartColorScheme.high*/
                            perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: '#FFD320', data: ["; /*perFunctionChartColorScheme.medium*/
                            perFunctionLOW = "{name: 'Low',id: 'Low',color: '#64B973', data: ["; /*perFunctionChartColorScheme.low*/

                            perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";

                            string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                            foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                            {
                                listCategoryId += ',' + cc.Id.ToString();
                                highcount = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                                mediumCount = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                                lowcount = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                                totalcount = highcount + mediumCount + lowcount;

                                if (totalcount != 0)
                                {
                                    //High
                                    long AfterDueDatecountHigh;
                                    long CompletedCountHigh;
                                    long NotCompletedcountHigh;

                                    if (ColoumnNames[2] == "High")
                                    {
                                        perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                                    }

                                    AfterDueDatecountHigh = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                                    CompletedCountHigh = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                                    NotCompletedcountHigh = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 0
                                    && (entry.ComplianceStatusID == 1
                                    || entry.ComplianceStatusID == 2
                                    || entry.ComplianceStatusID == 3
                                    || entry.ComplianceStatusID == 6
                                    || entry.ComplianceStatusID == 8
                                    || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                                    perFunctiondrilldown += "{" +
                                        " id: 'high' + '" + cc.Name + "'," +
                                        " name: 'High'," +
                                        " color: '#FF0000'," + /*perFunctionChartColorScheme.high*/
                                        " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                        " ['After due date', " + AfterDueDatecountHigh + "]," +
                                        " ['Not completed', " + NotCompletedcountHigh + "],]," +
                                        " events: {" +
                                        " click: function(e){" +
                                        // String.Format("CallMyCompliances('All',e.point.name,'-1','High')") + "}" +
                                        " CallMyCompliances('All',e.point.name,'-1','High')}" +
                                        //" fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "')}" +
                                        " }},";

                                    //per Status pie Chart
                                    totalPieCompletedcount += CompletedCountHigh;
                                    totalPieNotCompletedCount += NotCompletedcountHigh;
                                    totalPieAfterDueDatecount += AfterDueDatecountHigh;

                                    //pie drill down
                                    totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                                    totalPieCompletedHIGH += CompletedCountHigh;
                                    totalPieNotCompletedHIGH += NotCompletedcountHigh;

                                    //Medium
                                    long AfterDueDatecountMedium;
                                    long CompletedCountMedium;
                                    long NotCompletedcountMedium;

                                    if (ColoumnNames[3] == "Medium")
                                    {
                                        perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                                    }
                                    AfterDueDatecountMedium = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                                    CompletedCountMedium = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                                    NotCompletedcountMedium = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                                    perFunctiondrilldown += "{ " +
                                        " id: 'mid' + '" + cc.Name + "'," +
                                        " name: 'Medium'," +
                                        " color: '#FFD320'," + /*perFunctionChartColorScheme.medium*/
                                        " data: [['In Time', " + CompletedCountMedium + "]," +
                                        " ['After due date', " + AfterDueDatecountMedium + "]," +
                                        " ['Not completed', " + NotCompletedcountMedium + "],], " +
                                        " events: {" +
                                        " click: function(e){ " +
                                        " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "')}" +
                                        " }},";

                                    //per Status pie Chart
                                    totalPieCompletedcount += CompletedCountMedium;
                                    totalPieNotCompletedCount += NotCompletedcountMedium;
                                    totalPieAfterDueDatecount += AfterDueDatecountMedium;

                                    //pie drill down 
                                    totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                                    totalPieCompletedMEDIUM += CompletedCountMedium;
                                    totalPieNotCompletedMEDIUM += NotCompletedcountMedium;

                                    //Low
                                    long AfterDueDatecountLow;
                                    long CompletedCountLow;
                                    long NotCompletedcountLow;

                                    if (ColoumnNames[4] == "Low")
                                    {
                                        perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                                    }

                                    AfterDueDatecountLow = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                                    CompletedCountLow = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                                    NotCompletedcountLow = MasterManagementCompliancesSummaryQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                                    perFunctiondrilldown += "{" +
                                        " id: 'low' + '" + cc.Name + "', " +
                                        " name: 'Low', " +
                                        " color: '#64B973', " + /*perFunctionChartColorScheme.low*/
                                        " data: [['In Time', " + CompletedCountLow + "], " +
                                        " ['After due date'," + AfterDueDatecountLow + "], " +
                                        " ['Not completed'," + NotCompletedcountLow + "],], " +
                                        " events: {" +
                                        " click: function(e){ " +
                                        " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "')}" +
                                        " }},";

                                    //per Status pie Chart
                                    totalPieCompletedcount += CompletedCountLow;
                                    totalPieNotCompletedCount += NotCompletedcountLow;
                                    totalPieAfterDueDatecount += AfterDueDatecountLow;

                                    //pie drill down
                                    totalPieAfterDueDateLOW += AfterDueDatecountLow;
                                    totalPieCompletedLOW += CompletedCountLow;
                                    totalPieNotCompletedLOW += NotCompletedcountLow;
                                }
                            }

                            perFunctionHIGH += "],},";
                            perFunctionMEDIUM += "],},";
                            perFunctionLOW += "],},],";
                            perFunctiondrilldown += "],},";
                            perFunctionChart = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + perFunctiondrilldown;

                            #region Upcoming Overdue In Time After Due Date
                            //perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Overdue',y: " + totalPieOverduecount + ",color: perStatusChartColorScheme.high,drilldown: 'overdue',},{ name: 'Upcoming',y: " + totalPieUpcomingcount + ",color:'#1d86c8',drilldown: 'upcoming',},{ name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',},{name: 'In Time',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',}],}],";

                            //perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                            //                   " series: [" +

                            //                    " {id: 'inTime',name: 'In Time',cursor: 'pointer',data: [{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                            //                    // " // In time - High                   
                            //                    " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                            //                    // " // In time - Medium                                
                            //                    " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                            //                    // " // In time - Low                               
                            //                    " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            //                    " },},}],}," +
                            //                    " {id: 'afterDueDate',name: 'After Due Date',cursor: 'pointer',data: [{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                            //                    // " // After Due Date - High
                            //                    " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                            //                    //  " // After Due Date - Medium
                            //                    " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                            //                    // " // After Due Date - Low
                            //                    " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            //                    " },},}],}," +
                            //                    " {id: 'overdue',name: 'Overdue',cursor: 'pointer',data: [{name: 'High',y: " + totalPieOverdueHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                            //                    // " // Not Completed - High
                            //                    " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Medium',y: " + totalPieOverdueMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                            //                    // " // Not Completed - Medium
                            //                    " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Low',y: " + totalPieOverdueLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                            //                    // " // Not Completed - Low
                            //                    " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            //                    " },},}],}," +

                            //                    " {id: 'upcoming',name: 'Upcoming',cursor: 'pointer',data: [{name: 'High',y: " + totalPieUpcomingHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                            //                    // " // Not Completed - High
                            //                    " fpopulateddata(e.point.name,'Upcoming'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Medium',y: " + totalPieUpcomingMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                            //                    // " // Not Completed - Medium
                            //                    " fpopulateddata(e.point.name,'Upcoming'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Low',y: " + totalPieUpcomingLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                            //                    // " // Not Completed - Low
                            //                    " fpopulateddata(e.point.name,'Upcoming'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            //                     " },},}],}],},";

                            #endregion

                            #region Previous Working
                            perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: '#FF0000',drilldown: 'notCompleted',},{name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: '#FFD320',drilldown: 'afterDueDate',},{name: 'In Time',y: " + totalPieCompletedcount + ",color: '#64B973',drilldown: 'inTime',}],}],";

                            perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                               " series: [" +

                                                " {id: 'inTime',name: 'In Time',cursor: 'pointer',data: [{name: 'High',y: " + totalPieCompletedHIGH + ",color: '#FF0000',events:{click: function(e) { " +
                                                // " // In time - High 
                                                " CallMyCompliances('All','CT','-1','High')" +
                                                //" fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: '#FFD320',events:{click: function(e) { " +
                                                // " // In time - Medium                              
                                                " CallMyCompliances('All','CT','-1','Medium')" +
                                                //" fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: '#64B973',events:{click: function(e) { " +
                                                // " // In time - Low                               
                                                " CallMyCompliances('All','CT','-1','Low')" +
                                                //" fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                                                " },},}],}," +
                                                " {id: 'afterDueDate',name: 'After Due Date',cursor: 'pointer',data: [{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: '#FF0000',events:{click: function(e) { " +
                                                // " // After Due Date - High
                                                " CallMyCompliances('All','CD','-1','High')" +
                                                //" fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: '#FFD320',events:{click: function(e) { " +
                                                //  " // After Due Date - Medium
                                                " CallMyCompliances('All','CD','-1','Medium')" +
                                                //" fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: '#64B973',events:{click: function(e) { " +
                                                // " // After Due Date - Low
                                                " CallMyCompliances('All','CD','-1','Low')" +
                                                //" fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                                                " },},}],}," +
                                                " {id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: '#FF0000',events:{click: function(e) { " +
                                                // " // Not Completed - High
                                                " CallMyCompliances('All','NC','-1','High')" +
                                                //" fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: '#FFD320',events:{click: function(e) { " +
                                                // " // Not Completed - Medium
                                                " CallMyCompliances('All','NC','-1','Medium')" +
                                                //" fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: '#64B973',events:{click: function(e) { " +
                                                // " // Not Completed - Low
                                                " CallMyCompliances('All','NC','-1','Low')" +
                                                //" fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                                                " },},}],}],},";
                            #endregion

                            perRiskChart = "series: [{name: 'Not Completed', color: '#FF0000',data: [{" + /*perRiskStackedColumnChartColorScheme.high*/
                                                                                                          // Not Completed - High
                            "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                            " CallMyCompliances('All','NC','-1','High')" +
                             //" fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{" +

                            // Not Completed - Medium
                            " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                            " CallMyCompliances('All','NC','-1','Medium')" +
                            //" fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{  " +

                            // Not Completed - Low
                            "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                            " CallMyCompliances('All','NC','-1','Low')" +
                              //" fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}}]},{name: 'After Due Date',color:'#FFD320',data: [{" + /*color: perRiskStackedColumnChartColorScheme.medium*/

                            // After Due Date - High
                            "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                            " CallMyCompliances('All','CD','-1','High')" +
                            //" fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{   " +

                            // After Due Date - Medium
                            "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                            " CallMyCompliances('All','CD','-1','Medium')" +
                            //" fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{   " +
                            // After Due Date - Low
                            "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                            " CallMyCompliances('All','CD','-1','Low')" +
                            //" fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}}]},{name: 'In Time',color:'#64B973',data: [{" + /*perRiskStackedColumnChartColorScheme.low*/
                                                                                // In Time - High
                            "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                            " CallMyCompliances('All','CT','-1','High')" +
                            //" fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{  " +
                            // In Time - Medium
                            "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                            " CallMyCompliances('All','CT','-1','Medium')" +
                            //" fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{  " +
                            // In Time - Low
                            "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                            " CallMyCompliances('All','CT','-1','Low')" +
                            //" fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}}]}]";

                            if (1 == 2)
                            {
                                //if graph wise filter is required make 1==1
                                if (clickflag == "F")
                                {
                                    perRiskChart = tempperRiskChart;
                                }
                                else if (clickflag == "R")
                                {
                                    perFunctionChart = tempperFunctionChart;
                                    perFunctionPieChart = tempperFunctionPieChart;
                                }
                            }
                        }
                        else
                        {

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                //Top
                DateTime TopStartdate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TopStartdate))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopStartdate", string.Format("initializeDatePickerTopStartdate(new Date({0}, {1}, {2}));", TopStartdate.Year, TopStartdate.Month - 1, TopStartdate.Day), true);
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopStartdate", "initializeDatePickerTopStartdate(null);", true);
                }

                DateTime TopEnddate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TopEnddate))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopEnddate", string.Format("initializeDatePickerTopEnddate(new Date({0}, {1}, {2}));", TopEnddate.Year, TopEnddate.Month - 1, TopEnddate.Day), true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopEnddate", "initializeDatePickerTopEnddate(null);", true);
                }

                //Function

                DateTime FunctionStartDate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FunctionStartDate))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionStartDate", string.Format("initializeDatePickerFunctionStartDate(new Date({0}, {1}, {2}));", FunctionStartDate.Year, FunctionStartDate.Month - 1, FunctionStartDate.Day), true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionStartDate", "initializeDatePickerFunctionStartDate(null);", true);
                }

                DateTime FunctionEndDate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FunctionEndDate))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionEndDate", string.Format("initializeDatePickerFunctionEndDate(new Date({0}, {1}, {2}));", FunctionEndDate.Year, FunctionEndDate.Month - 1, FunctionEndDate.Day), true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionEndDate", "initializeDatePickerFunctionEndDate(null);", true);
                }


                //Risk
                DateTime RiskStartDate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out RiskStartDate))
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerRiskStartDate", string.Format("initializeDatePickerRiskStartDate(new Date({0}, {1}, {2}));", RiskStartDate.Year, RiskStartDate.Month - 1, RiskStartDate.Day), true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerRiskStartDate", "initializeDatePickerRiskStartDate(null);", true);
                }

                DateTime RiskEndDate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out RiskEndDate))
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerRiskEndDate", string.Format("initializeDatePickerRiskEndDate(new Date({0}, {1}, {2}));", RiskEndDate.Year, RiskEndDate.Month - 1, RiskEndDate.Day), true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerRiskEndDate", "initializeDatePickerRiskEndDate(null);", true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected string GetPerformerInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 3);
                InternalPerformername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }

        public void GetCalender()
        {
            DateTime dt1 = DateTime.Now.Date;
            string month = dt1.Month.ToString();

            if (Convert.ToInt32(month) < 10)
            {
                month = "0" + month;
            }

            string year = dt1.Year.ToString();

            CalendarDate = year + "-" + month;

            //var roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userid = AuthenticationHelper.UserID;

            DataTable dt = fetchComplianceInstanceTransactions(customerid, userid);

            var list = (from r in dt.AsEnumerable()
                        select r["ScheduledOn"]).Distinct().ToList();

            var datetodayornext = (from r in dt.AsEnumerable()
                                   orderby r.Field<DateTime>("ScheduledOn")
                                   where r.Field<DateTime>("ScheduledOn") >= DateTime.Now.AddDays(-1)
                                   select r["ScheduledOn"]).Distinct().FirstOrDefault();

            CalendarTodayOrNextDate = Convert.ToDateTime(datetodayornext);

            CalenderDateString = "";

            for (int i = 0; i < list.Count; i++)
            {
                Compliancecalendar _Event = new Compliancecalendar();
                string clor = "";
                int assignedCount = 0;
                DateTime date = Convert.ToDateTime(list[i]);

                //if (date < DateTime.Now)
                //{

                var Assigned = (from dts in dt.AsEnumerable()
                                where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                // && dts.Field<int>("RoleID") == 3
                                && (dts.Field<int>("ComplianceStatusID") == 1
                                || dts.Field<int>("ComplianceStatusID") == 10
                                || dts.Field<int>("ComplianceStatusID") == 2
                                || dts.Field<int>("ComplianceStatusID") == 3
                                || dts.Field<int>("ComplianceStatusID") == 4
                                || dts.Field<int>("ComplianceStatusID") == 6
                                || dts.Field<int>("ComplianceStatusID") == 5)
                                select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                var Completed = (from dts in dt.AsEnumerable()
                                 where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                 && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                 && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                 //&& dts.Field<int>("RoleID") == 4
                                 && (dts.Field<int>("ComplianceStatusID") == 4
                                 || dts.Field<int>("ComplianceStatusID") == 5)
                                 select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                assignedCount = Assigned.Count;

                if (date > DateTime.Now)
                {
                    clor = "";

                    if (Assigned.Count > Completed.Count)
                    {
                        clor = "upcoming";
                    }
                    else if ((Assigned.Count) == (Completed.Count))
                    {
                        clor = "complete";
                    }
                }
                else
                {
                    if (Assigned.Count > Completed.Count)
                    {
                        clor = "overdue";
                    }
                    else if ((Assigned.Count) == (Completed.Count))
                    {
                        clor = "complete";
                    }
                }
                //}
                //else
                //{
                //    clor = "upcoming";
                //}


                DateTime dtdate = Convert.ToDateTime(list[i]);

                var Date = dtdate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                CalenderDateString += "\"" + Date.ToString() + "\": { \"number\" :" + assignedCount + ", \"badgeClass\": \"badge-warning " + clor + "\",\"url\": \"javascript: fcal('" + Date.ToString() + "');\"},";

            }

            CalenderDateString = CalenderDateString.Trim(',');
        }

        public DataTable fetchComplianceInstanceTransactions(int customerid, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //statutory and statutory Checklist
                var Query = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerid, userid, AuthenticationHelper.ProfileID)
                             select new
                             {
                                 ScheduledOn = row.PerformerScheduledOn,
                                 ComplianceStatusID = row.ComplianceStatusID,
                                 RoleID = row.RoleID,
                                 ScheduledOnID = row.ScheduledOnID,
                                 CustomerBranchID = row.CustomerBranchID,
                             }).Distinct().ToList();


                DataTable table = new DataTable();
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));

                if (Query.Count > 0)
                {
                    foreach (var item in Query)
                    {
                        DataRow dr = table.NewRow();
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        table.Rows.Add(dr);
                    }
                }

                return table;
            }
        }

        [WebMethod]
        public static List<ActDetail> GetApplicableActs()
        {
            List<ActDetail> lstActDetail = new List<ActDetail>();

            string requestUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"].ToString();
            requestUrl += "Dashboard/GetRLCSActNameByProfileID?profileID=HVZCE4TNUGIZFYX";

            string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

            if (!string.IsNullOrEmpty(responseData))
            {
                var lstActDetail_Response = JsonConvert.DeserializeObject<List<ActDetail>>(responseData);

                if (lstActDetail_Response != null)
                {
                    if (lstActDetail_Response.Count > 0)
                    {
                        lstActDetail = lstActDetail_Response;
                    }
                }
            }

            return lstActDetail;
        }

        public static string GetMonthName(int monthNumber)
        {
            string monthName = string.Empty;

            if (monthNumber != 0)
            {
                if (monthNumber == 1)
                    monthName = "Jan";
                else if (monthNumber == 2)
                    monthName = "Feb";
                else if (monthNumber == 3)
                    monthName = "Mar";
                else if (monthNumber == 4)
                    monthName = "Apr";
                else if (monthNumber == 5)
                    monthName = "May";
                else if (monthNumber == 6)
                    monthName = "Jun";
                else if (monthNumber == 7)
                    monthName = "Jul";
                else if (monthNumber == 8)
                    monthName = "Aug";
                else if (monthNumber == 9)
                    monthName = "Sep";
                else if (monthNumber == 10)
                    monthName = "Oct";
                else if (monthNumber == 11)
                    monthName = "Nov";
                else if (monthNumber == 12)
                    monthName = "Dec";
            }

            return monthName;
        }

        public void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";
                ddlState.Items.Clear();

                ddlState.DataSource = RLCS_ClientsManagement.GetStates();
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("--Select State--", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public void BindActs()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.Items.Clear();

                ddlAct.DataSource = RLCS_ClientsManagement.GetAllActs(Convert.ToInt32(ddlState.SelectedItem.Value));
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("--Select Act--", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public void BindMonthYear()
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(DateTime.Now.Month)))
                    ddlMonthReg.SelectedItem.Value = Convert.ToString(DateTime.Now.Month);
                if (!string.IsNullOrEmpty(Convert.ToString(DateTime.Now.Year)))
                    ddlYearReg.SelectedItem.Value = Convert.ToString(DateTime.Now.Year);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }

        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            // ddlAct.ClearSelection();
            BindActs();
        }

        public void GetComplianceStatusCount_CustomerWise(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> customerWiseRecords)
        {
            try
            {
                List<Tuple<String, String>> GraphName = new List<Tuple<String, String>>();
                GraphName.Add(new Tuple<String, String>("REG", "Registers"));
                GraphName.Add(new Tuple<String, String>("RET", "Returns"));
                GraphName.Add(new Tuple<String, String>("CHA", "Challans"));

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var registerComplianceIDs = (from row in entities.RLCS_Register_Compliance_Mapping
                                                 where row.AVACOM_ComplianceID != null
                                                 select (long)row.AVACOM_ComplianceID).ToList();

                    var returnComplianceIDs = (from row in entities.RLCS_Returns_Compliance_Mapping
                                               where row.AVACOM_ComplianceID != null
                                               select (long)row.AVACOM_ComplianceID).ToList();

                    var challanComplianceIDs = (from row in entities.RLCS_Challan_Compliance_Mapping
                                                where row.AVACOM_ComplianceID != null
                                                select (long)row.AVACOM_ComplianceID).ToList();

                    long UpcomingCount = 0;
                    long OverdueCount = 0;
                    long PendingForReviewCount = 0;

                    long totalcount = 0;
                    DateTime now = DateTime.Now.Date;
                    DateTime nextOneMonth = DateTime.Now.AddMonths(1);

                    UpcomingCount = 0;
                    OverdueCount = 0;
                    PendingForReviewCount = 0;

                    UpcomingCount = (from row in customerWiseRecords
                                     where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                    OverdueCount = (from row in customerWiseRecords
                                    where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                     && row.PerformerScheduledOn < now
                                    select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                    PendingForReviewCount = (from row in customerWiseRecords
                                             where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                    totalcount = UpcomingCount + OverdueCount + PendingForReviewCount;

                    if (totalcount != 0)
                    {
                        List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> filteredRecords = new List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result>();

                        GraphName.ForEach(eachType =>
                        {
                            if (eachType.Item1 == "REG")
                            {
                                filteredRecords = (from row in customerWiseRecords
                                                   where registerComplianceIDs.Contains(row.ComplianceID)
                                                   select row).ToList();
                            }
                            else if (eachType.Item1 == "RET")
                            {
                                filteredRecords = (from row in customerWiseRecords
                                                   where returnComplianceIDs.Contains(row.ComplianceID)
                                                   select row).ToList();
                            }
                            else if (eachType.Item1 == "CHA")
                            {
                                filteredRecords = (from row in customerWiseRecords
                                                   where challanComplianceIDs.Contains(row.ComplianceID)
                                                   select row).ToList();
                            }

                            UpcomingCount = 0;
                            OverdueCount = 0;
                            PendingForReviewCount = 0;

                            UpcomingCount = (from row in filteredRecords
                                             where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                            OverdueCount = (from row in filteredRecords
                                            where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                             && row.PerformerScheduledOn < now
                                            select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                            PendingForReviewCount = (from row in filteredRecords
                                                     where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                            if (eachType.Item1 == "REG")
                            {
                                divRegisterUpcomingCount.InnerText = UpcomingCount.ToString();
                                if(divRegisterUpcomingCount.InnerText=="0")
                                {
                                    divRegisterUpcomingCount.Style["cursor"] = "default";
                                }
                                divRegisterOverdueCount.InnerText = OverdueCount.ToString();
                                if (divRegisterOverdueCount.InnerText == "0")
                                {
                                    divRegisterOverdueCount.Style["cursor"] = "default";
                                }
                                divRegisterPendingReviewCount.InnerText = PendingForReviewCount.ToString();
                                if (divRegisterPendingReviewCount.InnerText == "0")
                                {
                                    divRegisterPendingReviewCount.Style["cursor"] = "default";
                                }
                            }
                            else if (eachType.Item1 == "RET")
                            {
                                divReturnUpcomingCount.InnerText = UpcomingCount.ToString();
                                if (divReturnUpcomingCount.InnerText == "0")
                                {
                                    divReturnUpcomingCount.Style["cursor"] = "default";
                                }
                                divReturnOverdueCount.InnerText = OverdueCount.ToString();
                                if (divReturnOverdueCount.InnerText == "0")
                                {
                                    divReturnOverdueCount.Style["cursor"] = "default";
                                }
                                divReturnPendingReviewCount.InnerText = PendingForReviewCount.ToString();
                                if (divReturnPendingReviewCount.InnerText == "0")
                                {
                                    divReturnPendingReviewCount.Style["cursor"] = "default";
                                }
                            }
                            else if (eachType.Item1 == "CHA")
                            {
                                divChallanUpcomingCount.InnerText = UpcomingCount.ToString();
                                if (divChallanUpcomingCount.InnerText == "0")
                                {
                                    divChallanUpcomingCount.Style["cursor"] = "default";
                                }
                                divChallanOverdueCount.InnerText = OverdueCount.ToString();
                                if (divChallanOverdueCount.InnerText == "0")
                                {
                                    divChallanOverdueCount.Style["cursor"] = "default";
                                }
                                divChallanPendingReviewCount.InnerText = PendingForReviewCount.ToString();
                                if (divChallanPendingReviewCount.InnerText == "0")
                                {
                                    divChallanPendingReviewCount.Style["cursor"] = "default";
                                }
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindPeriod(string selectedFreq)
        {
            try
            {
                if (!string.IsNullOrEmpty(selectedFreq))
                {
                    List<Tuple<string, string, string>> tupleList = new List<Tuple<string, string, string>>();

                    tupleList.Add(new Tuple<string, string, string>("Annual", "Annual", "Annual"));
                    //tupleList.Add(new Tuple<string, string, string>("Biennial", "Biennial", "Biennial"));
                    tupleList.Add(new Tuple<string, string, string>("BiAnnual", "BiAnnual", "BiAnnual"));

                    tupleList.Add(new Tuple<string, string, string>("Half Yearly", "First Half", "HY1"));
                    tupleList.Add(new Tuple<string, string, string>("Half Yearly", "Second Half", "HY2"));

                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q1", "Q1"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q2", "Q2"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q3", "Q3"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q4", "Q4"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "January", "01"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "February", "02"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "March", "03"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "April", "04"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "May", "05"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "June", "06"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "July", "07"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "August", "08"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "September", "09"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "Octomber", "10"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "November", "11"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "December", "12"));

                    DataTable dtReturnPeriods = new DataTable();
                    dtReturnPeriods.Columns.Add("ID");
                    dtReturnPeriods.Columns.Add("Name");

                    var lst = tupleList.FindAll(m => m.Item1 == selectedFreq);
                    if (lst.Count > 0)
                    {
                        foreach (var item in lst)
                        {
                            DataRow drReturnPeriods = dtReturnPeriods.NewRow();
                            drReturnPeriods["ID"] = item.Item3;
                            drReturnPeriods["Name"] = item.Item2;
                            dtReturnPeriods.Rows.Add(drReturnPeriods);
                        }

                        rptPeriodList.DataSource = dtReturnPeriods;
                        rptPeriodList.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
                {
                    BindPeriod(ddlFrequency.SelectedValue);
                    ddlYear.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upFreqFilter_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }

        protected void TreeViewSelectedNode(TreeView tree, string value)
        {
            foreach (TreeNode node in tree.Nodes)
            {
                if (node.ChildNodes.Count > 0)
                {
                    foreach (TreeNode child in node.ChildNodes)
                    {
                        if (child.Value == value)
                        {
                            child.Selected = true;
                        }
                    }
                }
                else if (node.Value == value)
                {
                    node.Selected = true;
                }
            }
        }
    }
}