﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_PaycodeMappingDetails_Updated : System.Web.UI.Page
    {
        protected static int CustID;
        public string ClientID;
        protected static string avacomRLCSAPIURL;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = "Name";
                    if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                    {
                        var CustomerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                        BindClients(CustomerID);
                    }

                    //BindPaycodeDetails();


                    Session["CurrentRole"] = AuthenticationHelper.Role;
                    Session["CurrentUserId"] = AuthenticationHelper.UserID;
                    avacomRLCSAPIURL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }

        }

        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                        ViewState["EntityClientID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void BindClients(int CustomerID)
        {
            try
            {
                ddlClientList.DataTextField = "Name";
                ddlClientList.DataValueField = "ID";
                var list = RLCS_ClientsManagement.GetAll_Entities(CustomerID);
                ddlClientList.DataSource = list;
                ddlClientList.DataBind();
                if (list.Count > 0)
                {
                    ddlClientList.SelectedIndex = 1;
                    BindPaycodeDetails();
                }
                else
                {
                    Session["ClientIDNew"] = null;
                }
               
              //  ddlClientList.Items.Insert(0, new ListItem("Select", "-1"));
                //GG ADD 27JAN2020
                long parentID = -1;
                dlBreadcrumb.DataSource = CustomerBranchManagement.GetHierarchy(CustomerID, parentID);
                dlBreadcrumb.DataBind();
                //END

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<Business.Data.SP_Get_ClientPaycodeMappingDetails_Result> BindPaycodeDetailsWeb(string ClientID)
        {
            List<Business.Data.SP_Get_ClientPaycodeMappingDetails_Result> result = null;
            try
            {
                if (!string.IsNullOrEmpty(ClientID))
                {
                    result = RLCS_ClientsManagement.GetAllPaycodeDetailsList(ClientID, "");
                }
            }
            catch(Exception ex)
            {
                result = null;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static void DeletePaycodeDetailsWeb(long recordID, string clientID)
        {
            try
            {
                if((recordID != null) && (!string.IsNullOrEmpty(clientID)))
                RLCS_ClientsManagement.DeletePaycodeDetails(recordID, clientID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static void UpdatePaycodeDetailsWeb(string clientID)
        {
            try
            {
                if (!string.IsNullOrEmpty(clientID))
                {
                    RLCS_ClientsManagement.UpdateIsProcessedPaycodeMaster(clientID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindPaycodeDetails()
        {
            try
            {
                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (ddlClientList.SelectedValue != "-1")
                {
                    var client = ddlClientList.SelectedItem.Text;
                    // var entity = client.Split('/');
                    // ClientID = entity[0].Trim();
                    ClientID = Convert.ToString(ddlClientList.SelectedValue);
                    Session["ClientIDNew"] = ClientID;
                    hdnClientID.Value = Convert.ToString(ddlClientList.SelectedValue);
                    var list = RLCS_ClientsManagement.GetAllPaycodeDetailsList(ClientID, tbxFilter.Text);
                    if (list.Count()>0)
                    {
                        //grdPaycodeMappingDetails.DataSource = list;
                        Session["TotalPaycode"] = 0;
                        Session["TotalPaycode"] = list.Count();
                        //grdPaycodeMappingDetails.DataBind();
                       
                    }
                    else
                    {
                        Session["TotalPaycode"] = 0;
                        //grdPaycodeMappingDetails.DataSource = null;
                        //grdPaycodeMappingDetails.DataBind();
                       
                    }
                    btnAddEmployee.Visible = true;
                    btnUploadEmployee.Visible = true;

                }
                else
                {
                   
                    btnAddEmployee.Visible = false;
                    btnUploadEmployee.Visible = false;
                    Session["TotalPaycode"] = 0;
                    Session["ClientIDNew"] = null;
                    //grdPaycodeMappingDetails.DataSource = null;
                    //grdPaycodeMappingDetails.DataBind();
                }
                upEmpMasterCADMN.Update();
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                {
                    if (ddlClientList.SelectedItem.Text == "< Select Customer >")
                    {
                        btnAddEmployee.Visible = false;
                        btnUploadEmployee.Visible = false;
                    }
                    else
                    {
                        btnAddEmployee.Visible = true;
                        btnUploadEmployee.Visible = true;
                    }
                }

                if (!String.IsNullOrEmpty(ddlClientList.SelectedValue) && ddlClientList.SelectedItem.Text != "< Select Customer >")
                {
                    //  CustID = Convert.ToInt32(ddlClientList.SelectedValue);
                    //  Session["CustID"] = CustID;

                    btnAddEmployee.Visible = true;
                    btnUploadEmployee.Visible = true;
                    ClientID = ddlClientList.SelectedValue;
                }
                else
                {
                    btnAddEmployee.Visible = false;
                    btnUploadEmployee.Visible = false;
                }

                BindPaycodeDetails();
                //GetPageDisplaySummary();
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdPaycodeMappingDetails_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                if (ddlClientList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlClientList.SelectedValue);
                }

                var employees = RLCS_Master_Management.GetAll_EmployeeMaster(customerID);

                if (direction == SortDirection.Ascending)
                {
                    employees = employees.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    employees = employees.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                //foreach (DataControlField field in grdPaycodeMappingDetails.Columns)
                //{
                //    if (field.SortExpression == e.SortExpression)
                //    {
                //        ViewState["MatrixSortIndex"] = grdPaycodeMappingDetails.Columns.IndexOf(field);
                //    }
                //}

                //grdPaycodeMappingDetails.DataSource = employees;
                //grdPaycodeMappingDetails.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //grdPaycodeMappingDetails.PageIndex = 0;
                BindPaycodeDetails();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }


        protected void grdPaycodeMappingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //grdPaycodeMappingDetails.PageIndex = e.NewPageIndex;
                //grdPaycodeMappingDetails.DataBind();

                BindPaycodeDetails();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

  

        protected void grdPaycodeMappingDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                bool deleteSuccess = false;
                string headerCode = "";
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("Delete_Employee"))
                    {
                        GridViewRow gvrow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                        //Get Grid Data
                        HiddenField CPMD_ClientID = (HiddenField)gvrow.FindControl("CPMD_ClientID");
                        CPMD_ClientID.Value = Convert.ToString(CPMD_ClientID.Value);
                        HiddenField CPMD_Status = (HiddenField)gvrow.FindControl("hdnStatus");
                        HiddenField CPMD_Header = (HiddenField)gvrow.FindControl("Header");
                        CPMD_Header.Value = Convert.ToString(CPMD_Header.Value);
                        long recordID = Convert.ToInt64(e.CommandArgument);
                        HiddenField CPMD_PayCode = (HiddenField)gvrow.FindControl("hdnPayCode");
                        HiddenField CPMD_Standard_Column = (HiddenField)gvrow.FindControl("hdnStandard_Column");

                        //string recordID = Convert.ToString(e.CommandArgument);
                        //if (!string.IsNullOrEmpty(recordID) && (!string.IsNullOrEmpty(CPMD_ClientID.Value)))
                        //    deleteSuccess = RLCS_ClientsManagement.DeletePaycodeDetails(recordID, CPMD_ClientID.Value);
                        //else
                        //{
                        //    HiddenField CPMD_Header = (HiddenField)gvrow.FindControl("Header");
                        //    CPMD_Header.Value = Convert.ToString(CPMD_Header.Value);

                        //    deleteSuccess = RLCS_ClientsManagement.DeletePaycodeDetails(CPMD_Header.Value, CPMD_ClientID.Value);
                        //}
                       
                        bool Res = false;
                        if (Convert.ToString(CPMD_Status.Value) == "I")
                        {
                            Res = RLCS_ClientsManagement.ActivePaycodeCheck(CPMD_ClientID.Value, headerCode);
                            if (Res == true)
                            {
                                cvDuplicate.IsValid = false;
                                cvDuplicate.ErrorMessage = "Same Paycode Already Active..Please Check....!";
                            }
                        }
                        if (Res == false)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(recordID)) && (!string.IsNullOrEmpty(CPMD_ClientID.Value)))
                                deleteSuccess = RLCS_ClientsManagement.DeletePaycodeDetails(recordID, CPMD_ClientID.Value);

                            if (deleteSuccess)
                            {
                                deleteSuccess = RLCS_ClientsManagement.UpdateIsProcessedPaycodeMaster(CPMD_ClientID.Value);
                                if (deleteSuccess)
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Paycode Deleted Successfully";

                                    BindPaycodeDetails();
                                }

                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }


        

        protected void upEmpMasterCADMN_Load(object sender, EventArgs e)
        {
            try
            {
                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdPaycodeMappingDetails_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        //DivRecordsScrum.Attributes.Remove("disabled");
        //        DivRecordsScrum.Visible = true;
        //        DivnextScrum.Visible = true;

        //        lblTotalRecord.Text = " " + Session["TotalPaycode"].ToString();

        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
        //            {
        //                SelectedPageNo.Text = "1";
        //                lblStartRecord.Text = "1";

        //                if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalPaycode"].ToString())))
        //                    lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
        //                else
        //                    lblEndRecord.Text = Session["TotalPaycode"].ToString() + " ";
        //            }
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //            DivRecordsScrum.Visible = false;
        //            DivnextScrum.Visible = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //SelectedPageNo.Text = "1";

                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                //if (currentPageNo < GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //}

                //grdPaycodeMappingDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //grdPaycodeMappingDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindPaycodeDetails();
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //private int GetTotalPagesCount()
        //{
        //    try
        //    {
        //        if (Session["TotalPaycode"] != null)
        //            TotalRows.Value = Session["TotalPaycode"].ToString();

        //        int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
        //        // total page item to be displyed
        //        int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
        //        // remaing no of pages
        //        if (pageItemRemain > 0)// set total No of pages
        //        {
        //            totalPages = totalPages + 1;
        //        }
        //        else
        //        {
        //            totalPages = totalPages + 0;
        //        }
        //        return totalPages;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return 0;
        //    }
        //}
        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                //int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                //int EndRecord = 0;

                //if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                //{
                //    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                //}

                //StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                //if (StartRecord < 1)
                //    StartRecord = 1;

                //EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                //if (EndRecord > Convert.ToInt32(Session["TotalPaycode"]))
                //    EndRecord = Convert.ToInt32(Session["TotalPaycode"]);

                //lblStartRecord.Text = StartRecord.ToString();
                //lblEndRecord.Text = EndRecord.ToString() + " ";

                //grdPaycodeMappingDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //grdPaycodeMappingDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindPaycodeDetails();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                //int EndRecord = 0;

                //if (currentPageNo < GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                //}
                //else
                //{

                //}

                //if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalPaycode"])))
                //    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                //EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                //if (EndRecord > Convert.ToInt32(Session["TotalPaycode"]))
                //    EndRecord = Convert.ToInt32(Session["TotalPaycode"]);

                //lblStartRecord.Text = StartRecord.ToString();
                //lblEndRecord.Text = EndRecord.ToString() + " ";

                //grdPaycodeMappingDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //grdPaycodeMappingDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindPaycodeDetails();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdPaycodeMappingDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                ClientID = Convert.ToString(ddlClientList.SelectedValue);
                hdnClientID.Value = Convert.ToString(ddlClientList.SelectedValue);
                var list = RLCS_ClientsManagement.GetAllPaycodeDetailsList(ClientID, tbxFilter.Text);
                if (list.Count() > 0)
                {
                    //grdPaycodeMappingDetails.DataSource = list;
                    Session["TotalPaycode"] = 0;
                    Session["TotalPaycode"] = list.Count();
                    //grdPaycodeMappingDetails.DataBind();
                    //DivRecordsScrum.Visible = true;
                    //DivnextScrum.Visible = true;
                    upEmpMasterCADMN.Update();
                    //GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }   
        }
    }
}