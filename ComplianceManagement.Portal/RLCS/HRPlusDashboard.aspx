﻿<%@ Page Title="MY DASHBOARD" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="HRPlusDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlusDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="/NewCSS/bootstrap.min.css" rel="stylesheet" />
        <script type="text/javascript" src="/avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
        <script type="text/javascript" src="/avantischarts/highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="/avantischarts/highcharts/js/modules/drilldown.js"></script>
        <script type="text/javascript" src="/avantischarts/highcharts/js/modules/exporting.js"></script>
        <script type="text/javascript" src="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
        <link href="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
        <link href="/newcss/spectrum.css" rel="stylesheet" />

        <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
        <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
        <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
        <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

        <%--<script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>--%>
        <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>
        <%--<script type="text/javascript" src="../Newjs/jszip.min.js"></script>--%>

        <script type="text/javascript" src="/newjs/spectrum.js"></script>
        <script src="/Newjs/jquery.cookie.js"></script>

    <script>

        
        function LoadMarqueeSectionData() {
            
               // var obj = GlobalDatas();
            //    var api = obj.api;
               // var EncrProfileId = <%= userProfileID_Encrypted%>;
                // var AuthKey =<%= authKey%>;
                var uri ='<%= tlConnect_url%>Dashboard/GetMarqueeSectionDueDate?Type=GetSectionDueDate'
                var type = "GetSectionDueDate";                
                $.ajax({
                    url: uri,
                    type: 'GET',
                  //  dataType:'jsonp',
                   // contentType: "application/json;charset=utf-8",
                    beforeSend: function(xhr) {
                        //xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
                        //xhr.setRequestHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
                        //xhr.setRequestHeader('Access-Control-Request-Headers','GET,PUT,POST,DELETE,PATCH,OPTIONS');
                        //xhr.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
                        xhr.setRequestHeader('Authorization','B2B5B33B-2544-4195-B772-E90278');
                        xhr.setRequestHeader('X-User-Id-1','E43SC3fopmDfizK5kv2chQ=');
                       
                    },
                    success: function (data) {
                        if (data.StatusCode == 0) {
                            try{ AuthenticationFailed(e);}catch(e){}
                           
                        } else {
                            if (data.DueDate.length > 0) {
                                $("#MarqueeSectionData").html("");
                                $("#MarqueeSectionData").html("<span><b>Activities Due For Current Month -</b></span>" + data.DueDate);
                            }
                        }
                    },
                    error: function (result) {
                    }
                });
            }

        $(document).ready(function(){
            LoadMarqueeSectionData();
        })
    </script>
        <script>
            $(document).ready(function () {
                fhead('My Dashboard');                
            });
        </script>

        <script type="text/javascript">
            function fComplianceOverview(obj) {
                OpenOverViewpup($(obj).attr('scheduledonid'), $(obj).attr('instanceid'), $(obj).attr('Ctype'));
            }

            function OpenOverViewpup(scheduledonid, instanceid) {
                $('#divOverView').modal('show');
                $('#OverViewvs').attr('width', '1150px');
                $('#OverViews').attr('height', '500px');
                $('.modal-dialog').css('width', '1200px');
                $('#OverViews').attr('src', "../RLCS/RLCS_ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
        </script>

        <script type="text/javascript">
            var dataset2 = [];

            var locations=[];
            
            function createMap() {
                if(locations!=null||locations!=undefined){
                    
                    if (locations.length > 0) {
                        var geocoder = new google.maps.Geocoder();
                        $(".map").kendoMap({
                            center: [0,0],
                            zoom: 5,
                            layers: [{
                                type: "tile",
                                urlTemplate: "http://a.tile.openstreetmap.org/#= zoom #/#= x #/#= y #.png",
                                attribution: "&copy; OpenStreetMap"
                            }],
                            markerClick: OnMarkerClick,                      
                        });

                        function OnMarkerClick(e){
                            fpopulatestatewisedata();                        
                        }
                        //var map = $("#Windowmap").data("kendoMap");
                        var windowmap= $("#divMapPanIndiaCompliance").data("kendoMap");                        
                        //  var loc = map.eventToLocation(e);
                        for (i = 0; i < locations.length; i++) {
                            // geocodeAddress(locations, i);
                           
                            try {
                                var addressName=locations[i][1];                                

                                geocoder.geocode({ "address": locations[i][1]}, function (results, status) {
                                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                                        var locName=addressName;
                                      
                                        var location = results[0].geometry.location,                        
                                            lat = location.lat(),
                                            lng = location.lng();
                                                                             
                                        windowmap.markers.add({ location: [18.520430,73.856743],shape: "pin", tooltip: {content: results[0].address_components[0].short_name }});
                                        //windowmap.markers.add({ location: [lat,lng] });
                                        windowmap.center([lat,lng]);
                                    }
                                    //   else {
                                    // console.log('Geocode was not successful for the following reason: ' + status);
                                    //}
                                });
                              
                            } catch (t) {
                                return ''
                            }
                        }
                        
                        $("#divMapPanIndiaCompliance").unbind("mousewheel");
                        $("#divMapPanIndiaCompliance").unbind("DOMMouseScroll");
                        // $('#iframepan').attr('src', "../RLCS/StateGraphDetails.aspx?&customerid=" + <%= customerID%> + "&branchid=" + <%= BID%> +"&Internalsatutory=Statutory&IsApprover="+'<%= IsApprover%>');
                                
                        //graph('Graphpan','Detailspan','liGraphpan','liDetailspan');('Graphpan','Detailspan','liGraphpan','liDetailspan');
                    }
                }

                return false;
            }
                       
            $(document).ready(function(){               
                createMap();
                // monthWiseChart();
                $('#window').hide();
                $('#windowfunctionchart').hide();
                $('#windowgradingchart').hide();
                $('#compliancecalender').hide();
                $("#windowApplicableActs").hide();               
                $("#windowriskchart").hide();
                $("#windowpiechart").hide();                
                $('#windowduetoday').hide();
                $('#divreportOVerdue').hide();
                $("#windowfunctionchart").hide();
                $("#ContentPlaceHolder1_ddlStatus").kendoDropDownList();
                $("#ContentPlaceHolder1_ddlYearNew").kendoDropDownList();
                $("#ContentPlaceHolder1_txtAdvStartDate").kendoDatePicker();
                $("#ContentPlaceHolder1_txtAdvEndDate").kendoDatePicker();
                $("#ContentPlaceHolder1_ddlmonthsGrading").kendoDropDownList();
                $("#ContentPlaceHolder1_ddlYearGrading").kendoDropDownList();
                $("#ContentPlaceHolder1_ddlPeriodGrading").kendoDropDownList();
            });
         
        </script>

        <script>
            var statuesflag=0;
            statuesflag=<%= IsStatues%>;
              
            function ApplicationStatusOnLoad() {
                
                var uri="";
                uri='<%= tlConnect_url%>Dashboard/GetRLCSActNameByProfileID?profileID=<%= userProfileID%>';
                var JoinData = "";
                $("#ApplicationStatus").empty();
                $.ajax({
                    url:uri,
                    type: 'GET',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization','<%= authKey%>');
                        xhr.setRequestHeader('X-User-Id-1','<%= userProfileID_Encrypted%>');
                    },
                    success: function (data) {
                        if (data.StatusCode == 0) {
                            try{ AuthenticationFailed(e);}catch(e){}
                        } else {
                            if (data.lstActsDetails.length > 0) {
                                $.each(data.lstActsDetails, function (i, item) {
                                    JoinData += '<div class="sl-item b-info b-l"><div class="m-l text-sm">' + item.ActNameDetail + '<p>&nbsp;</p></div></div>';
                                });                                      
                            }
                            $('#divApplicableActs').append(JoinData);
                            document.getElementById("ContentPlaceHolder1_StatuesCount").textContent=data.lstActsDetails.length;
                        }
                        
                    },
                    error: function (result) {
                    }
                });
            }
            
            function BindApplicableActs() {                                      
                var divApplicableActs = document.getElementById("divApplicableActs");
               
                if(divApplicableActs!=null &&  divApplicableActs!=undefined){
                    $.ajax({
                        type: "POST",
                        url: "/RLCS/RLCS_HRMDashboardNew.aspx/GetApplicableActs",
                        data: '{}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {                     
                            $.each(response, function () {
                                var hml="";
                                for (var i = 0; i < response.d.length; i++) {
                                  
                                    hml+="<div class='sl-item b-info b-l'> <div class='m-l text-sm'>"+ response.d[i].ActName +"</div></div>";                             
                                }
                                $(divApplicableActs).html(hml);
                            });                     
                        },
                        failure: function (response) {
                        },
                        error: function (response) {
                        }
                    });
                }           
            }
        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                                           
                //exapand
                $(".panel-wrap").on("click", "span.k-i-sort-desc-sm", function(e) {
                    var contentElement = $(e.target).closest(".widget").find(">div");
                    $(e.target)
                        .removeClass("k-i-sort-desc-sm")
                        .addClass("k-i-sort-asc-sm");

                    kendo.fx(contentElement).expand("vertical").stop().play();
                });

                //collapse
                $(".panel-wrap").on("click", "span.k-i-sort-asc-sm", function(e) {
                    var contentElement = $(e.target).closest(".widget").find(">div");
                    $(e.target)
                        .removeClass("k-i-sort-asc-sm")
                        .addClass("k-i-sort-desc-sm");

                    kendo.fx(contentElement).expand("vertical").stop().reverse();
                });
            });
        </script>

        <script type="text/javascript">          
            function OpenActClick(ActID,CustID,BID,App){
                var IsAvantisFlag=true;
                $('#iframeActs').attr('src', "../RLCS/ComplianceDetails.aspx?customerid="+CustID+"&ActID="+ActID+"&Internalsatutory=Statutory&IsApprover="+App+"&IsAvantisFlag="+IsAvantisFlag);                
                return false;
            }

            function OpenFunctionchart(){                
                $('#windowfunctioncolumn').show();
               
                var myWindowAdv = $("#windowfunctionchart");
                
                function onClose() {

                }

                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Completion",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();

                // kendo.ui.progress($(".chart-loading"), true);

                windowFunctioncoulmnChart();

                return false;
            }

            function MaximizePerfSummaryPieChart() {  
                //$('#Detailsperformance').hide();
                $('#windowfunctionPie').show();
                var myWindowAdv = $("#windowpiechart");

                function onClose() {
                    $(this.element).empty();
                }
                function onOpen(e) {
                    kendo.ui.progress(e.sender.element, true);
                }  

                myWindowAdv.kendoWindow({
                    width: "95%",
                    height: "93%",
                    title: "Performance Summary",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose,
                    open: onOpen,
                    content: {
                     
                        url: "../RLCS/SatutoryManagement.aspx?&pointname=High&customerid=" + <%= customerID%> + "&branchid=" + <%= BID%>+"&Filter=Function&ChartName=FunctionPiechart&Internalsatutory=Statutory&IsApprover="+'<%= IsApprover%>',
                    dataType: "json",

                        iframe: true,
                    } 
                });
                //kendo.ui.progress($(".chart-loading"), true);
                // $('#idloader').show();
                myWindowAdv.data("kendoWindow").center().open();

                WindowStatusPieChart();
                //kendo.ui.progress($(".chart-loading"), true);
                //$("#KendoLoader").show(); 
                //$('#iframePerformance').attr('src', "../RLCS/SatutoryManagement.aspx?&pointname=High&customerid=" + <%= customerID%> + "&branchid=" + <%= BID%>+"&Filter=Function&ChartName=FunctionPiechart&Internalsatutory=Statutory&IsApprover="+'<%= IsApprover%>');
                //graph('Graphperformance','Detailsperformance','liGraphperformance','liDetailsPer');

                //var viewportHeight = $("#windowpiechart").height();
                
                //$("#Detailsperformance").height(viewportHeight);

                return false;
            }

            function OpenLocations(){
                var IsAvantisFlag=true;
                $('#Detailspan').hide();
                $('#Graphpan').show();
                $('#window').show();
                var myWindowAdv = $("#window");

                function onClose() {
                    $(this.element).empty();
                }

                function onOpen(e) {
                    kendo.ui.progress(e.sender.element, true);
                }

                myWindowAdv.kendoWindow({
                    width: "90%",
                    height: "85%",
                    title: "My Assigned Locations",
                    content:"../RLCS/StateGraphDetails.aspx?&customerid=" + <%= customerID%> + "&branchid=" +<%= BID%> +"&Internalsatutory=Statutory&IsApprover="+'<%= IsApprover%>'+"&IsAvantisFlag="+IsAvantisFlag,
                    iframe:true,
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose,
                    open: onOpen
                });

                myWindowAdv.data("kendoWindow").center().open();
               
                if (locations.length > 1) {
                    $("#Mapwindow").kendoMap({
                        center: [0,0],
                        zoom: 5,
                        layers: [{
                            type: "tile",
                            urlTemplate: "http://a.tile.openstreetmap.org/#= zoom #/#= x #/#= y #.png",
                            attribution: "&copy; OpenStreetMap"
                        }],
                        markerClick: OnMarkerClick
                    });

                    function OnMarkerClick(e){ 
                        $('#KendoLoaderLocation').show();
                       // $('#iframepan').attr('src', "../RLCS/StateGraphDetails.aspx?&customerid=" + <%= customerID%> + "&branchid=" +<%= BID%> +"&Internalsatutory=Statutory&IsApprover="+'<%= IsApprover%>');
                                
                        Detail('Graphpan','Detailspan','liGraphpan','liDetailspan');
                    }

                    var map = $("#Mapwindow").data("kendoMap");
                       
                    var geocoder = new google.maps.Geocoder();

                    for (i = 0; i < locations.length; i++) {
                        var s2 =locations[i][2];
                       
                        geocoder.geocode({ "address": locations[i][1] }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK && results.length > 0) {

                                var location = results[0].geometry.location,
                                    lat = location.lat(),
                                    lng = location.lng();
                              
                                map.center([lat,lng]);
                                map.markers.add({ location: [lat,lng],shape: "pin", tooltip: {content: results[0].address_components[0].short_name },valueField: {val:s2}});
                                //map.markers.add({tooltip:results[0].address_components[0].short_name});
                            }                            
                        });
                    }

                    $("#Mapwindow").unbind("mousewheel");
                    $("#Mapwindow").unbind("DOMMouseScroll");
                    //Detail('Graphpan','Detailspan','liGraphpan','liDetailspan');
                    graph('Graphpan','Detailspan','liGraphpan','liDetailspan');
                  //  $('#iframepan').attr('src', "../RLCS/StateGraphDetails.aspx?&customerid=" + <%= customerID%> + "&branchid=" +<%= BID%> +"&Internalsatutory=Statutory&IsApprover="+'<%= IsApprover%>');
                 }    
                 return false;
             }  

            function fpopulatestatewisedata(){
                var IsAvantisFlag=true;
                 $('#Detailspan').show();
                 $('#Graphpan').hide();
                 $('#window').show();
                 var myWindowAdv = $("#window");
                 function onClose() {}
                 myWindowAdv.kendoWindow({
                     width: "90%",
                     height: "94%",
                     content:"../RLCS/StateGraphDetails.aspx?&customerid=" + <%= customerID%> + "&branchid=" +<%= BID%> +"&Internalsatutory=Statutory&IsApprover="+'<%= IsApprover%>'+"&IsAvantisFlag="+IsAvantisFlag,
                     iframe:true,
                     title: "My Assigned Locations",
                     visible: false,
                     actions: [
                         "Pin",
                         "Close"
                     ],
                     close: onClose
                 });

                 myWindowAdv.data("kendoWindow").center().open();
               
                 if (locations.length > 1) {
                     var geocoder = new google.maps.Geocoder();
                     $("#Mapwindow").kendoMap({
                         center: [0,0],
                         zoom: 5,
                         layers: [{
                             type: "tile",
                             urlTemplate: "http://a.tile.openstreetmap.org/#= zoom #/#= x #/#= y #.png",
                             attribution: "&copy; OpenStreetMap"
                         }],
                         markerClick: OnMarkerClick
                     });

                     var map = $("#Mapwindow").data("kendoMap");

                     function OnMarkerClick(e){ 
                         $('#KendoLoaderLocation').show();                        
                        // $('#iframepan').attr('src', "../RLCS/StateGraphDetails.aspx?&customerid=" + <%= customerID%> + "&branchid=" +<%= BID%> +"&Internalsatutory=Statutory&IsApprover="+'<%= IsApprover%>');                                
                         Detail('Graphpan','Detailspan','liGraphpan','liDetailspan');
                     }
                  
                     //  var loc = map.eventToLocation(e);
                     for (i = 0; i < locations.length; i++) {                               
                         geocoder.geocode({ "address": locations[i][1] }, function (results, status) {
                             if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                                 var location = results[0].geometry.location,
                                     lat = location.lat(),
                                     lng = location.lng();                              
                                 map.center([lat,lng]);
                                 map.markers.add({ location: [lat,lng] });
                                 map.markers.add({tooltip:results[0].address_components[i]});
                             }
                         });
                     }

                   // $('#iframepan').attr('src', "../RLCS/StateGraphDetails.aspx?&customerid=" + <%= customerID%> + "&branchid=" +<%= BID%> +"&Internalsatutory=Statutory&IsApprover="+'<%= IsApprover%>');
                        
                    $("#Mapwindow").unbind("mousewheel");
                    $("#Mapwindow").unbind("DOMMouseScroll");
                }    
                 //$("#idloader6").show();
                Detail('Graphpan','Detailspan','liGraphpan','liDetailspan');
                               
                return false;
            }

            function OpenCalenderWindow(){                
                $('#compliancecalender').show();
                var myWindowAdv = $("#windowcalender");
                function onOpen(e) {
                    kendo.ui.progress(e.sender.element, true);
                }  
                function onClose() {

                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "My Compliance Calendar",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    open: onOpen,
                    close: onClose
                });
                myWindowAdv.data("kendoWindow").center().open();
                kendo.ui.progress(e.sender.element, true);
                return false;
            }

            function ApplicableActs(customerID) {
                var IsAvantisFlag=true;
                $('#idloader8').show();
                $('#iframeActs').attr('src', "../RLCS/ComplianceDetails.aspx?customerid="+<%= customerID %>+"&Internalsatutory=Statutory&IsAvantisFlag="+IsAvantisFlag);
                
                function onClose() {

                }
                var myWindowAdv = $("#windowApplicableActs");
                myWindowAdv.kendoWindow({width: "95%",
                    height: "79%",
                    title: "Applicable Statutes",
                    actions: ["Pin", "Close"],
                    close: onClose,
                    content: {                        
                        url: "../RLCS/ComplianceDetails.aspx?customerid="+<%= customerID %>+"&Internalsatutory=Statutory&IsAvantisFlag="+IsAvantisFlag,
                        // aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+0);
                        dataType: "json",
                        iframe: true,
                        //template: "#= data#"
                    }  
                });
             
                myWindowAdv.data("kendoWindow").center().open();
                $("#windowApplicableActs").show();

                resizeGrid();

                return false;
            }

            var resizeGrid = function() {
                var viewportHeight = $("#windowApplicableActs").height();
                $("#windowActs").height(viewportHeight);
                $("#iframeActs").height(viewportHeight);
            };

            function OpenActs(ActID,CID,BID){
                var IsAvantisFlag=true;
                function onClose() {
                    $(this.element).empty();
                }

                function onOpen(e) {
                    kendo.ui.progress(e.sender.element, true);
                }

                var myWindowAdv = $("#openlink");
                myWindowAdv.kendoWindow({width: "90%",
                    height: "79%",
                    title: "Applicable Statutes",
                    actions: [
                  "Pin",
                  "Close"
                    ],
                    close: onClose,
                    open: onOpen,
                    content: {
                      
                        url: "../RLCS/ComplianceDetails.aspx?customerid="+<%= customerID %>+"&ActID="+ActID+"&Internalsatutory=Statutory&IsAvantisFlag="+IsAvantisFlag,
                        // aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+0);
                        dataType: "json",
                       
                        iframe: true,
                       
                        //template: "#= data#"
                    }  
                });
               
                myWindowAdv.data("kendoWindow").center().open();

                return false;
            }
            
            function graph(Graph,Details,liGraph,liDetails){
                $('#' +Graph).show();
                $('#' +Details).hide();
                
                $('#' +liDetails).removeClass('active');
                $('#'+liGraph).addClass('active');
              
                return false;
            }

            function Detail(Graph,Details,liGraph,liDetails){                         
                $('#' +Graph).hide();
                $('#' +Details).show();
                $('#'+liGraph).removeClass('active');
                $('#' +liDetails).addClass('active');
                return false;
            }

            function OpenRiskchart() {    
                
                //$('#Details').hide();
                                 
                $('#windowriskchart').show();

                var myWindowAdv = $("#windowriskchart");
                function onClose() {
                    $(this.element).empty();
                }
                function onOpen(e) {
                    kendo.ui.progress(e.sender.element, true);
                }  
                myWindowAdv.kendoWindow({
                    width: "95%",
                    height: "92%",
                    title: "Risk Summary",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose,
                    open: onOpen,
                    content: {
                    url: "../RLCS/SatutoryManagement.aspx?&pointname=High&customerid=" + <%= customerID%> + "&branchid=" + <%= BID%>+"&ChartName=RiskBARchart&Internalsatutory=Statutory&IsApprover="+'<%= IsApprover%>',
                    dataType: "json",
                      
                        iframe: true,
                      
                    } 
                });
                
                //
                //Kendo.ui.progress($(".chart-loading"), true);
                
                myWindowAdv.data("kendoWindow").center().open();

                WindowRiskColumnChart();

                //if($("#idloader7")!=null||$("#idloader7")!=undefined)
                //$("#idloader7").show();
                //$("#KendoLoaderRisk").show(); 
               // $('#iframerisk').attr('src', ");
               
                graph('Graph','Details','liGraph','liDetails');
                return false;
            }

            function fpopulateddata(type,attribute,customerid,branchid,fromdate,enddate,filter,functionid,internalsatutory,chartname,listcategoryid,userid,isapprover)
            {   
                if(chartname=="RiskBARchart")
                {
                    $('#windowriskchart').show();

                    var myWindowAdv = $("#windowriskchart");
                    function onClose() {

                    }
                    myWindowAdv.kendoWindow({
                        width: "93%",
                        height: "81%",
                        title: "Completion Status - Overall",
                        visible: false,
                        actions: [
                            "Pin",
                            "Close"
                        ],
                        close: onClose
                    });
                    myWindowAdv.data("kendoWindow").center().open();

                    WindowRiskColumnChart();
                    //graph('Graph','Details','liGraph','liDetails');
                    Detail('Graph','Details','liGraph','liDetails');
                    // Kendo.ui.progress($(".chart-loading"), true);
                    if (internalsatutory == "Statutory") {      
                        $("#KendoLoaderRisk").show(); 
                        $('#iframerisk').attr('src', "../RLCS/SatutoryManagement.aspx?pointname=" + type + "&attrubute=" + attribute + "&customerid=" + customerid + "&branchid=" + branchid + "&FromDate=" + fromdate + "&Enddate=" + enddate + "&Filter=" + filter + "&functionid=" + functionid + "&ChartName=" + chartname + "&Internalsatutory=" + internalsatutory+"&listcategoryid="+listcategoryid+"&Userid="+userid+"&IsApprover="+isapprover);
                    }
                    if (internalsatutory == "Internal") {        
                        $("#KendoLoaderRisk").show(); 
                        $('#iframerisk').attr('src', "../RLCS/InternalManagement.aspx?pointname=" + type + "&attrubute=" + attribute + "&customerid=" + customerid + "&branchid=" + branchid + "&FromDate=" + fromdate + "&Enddate=" + enddate + "&Filter=" + filter + "&functionid=" + functionid + "&ChartName=" + chartname + "&Internalsatutory=" + internalsatutory+"&listcategoryid="+listcategoryid+"&Userid="+userid+"&IsApprover="+isapprover);
                    } 
                }

                if(chartname=="FunctionPiechart")
                {
                    $('#windowfunctionPie').show();
                    var myWindowAdv = $("#windowpiechart");
                    function onClose() {

                    }
                    myWindowAdv.kendoWindow({
                        width: "95%",
                        height: "81%",
                        title: "Performance Summary",
                        visible: false,
                        actions: [
                            "Pin",
                            "Close"
                        ],
                        close: onClose
                    });

                    kendo.ui.progress($(".chart-loading"), true);

                    myWindowAdv.data("kendoWindow").center().open();

                    WindowStatusPieChart();
                    //graph('Graphperformance','Detailsperformance','liGraphperformance','liDetailsPer');
                    Detail('Graphperformance','Detailsperformance','liGraphperformance','liDetailsPer');
                    //Detail('GraphPie','DetailsPie','liGraphMonthlypie','liDetailsMonthlypie')

                    if (internalsatutory == "Statutory") {     
                        $('#KendoLoader').show();
                        $('#iframePerformance').attr('src', "../RLCS/SatutoryManagement.aspx?pointname=" + type + "&attrubute=" + attribute + "&customerid=" + customerid + "&branchid=" + branchid + "&FromDate=" + fromdate + "&Enddate=" + enddate + "&Filter=" + filter + "&functionid=" + functionid + "&ChartName=" + chartname + "&Internalsatutory=" + internalsatutory+"&listcategoryid="+listcategoryid+"&Userid="+userid+"&IsApprover="+isapprover);
                    }
                    if (internalsatutory == "Internal") {    
                        $('#KendoLoader').show();
                        $('#iframePerformance').attr('src', "../RLCS/InternalManagement.aspx?pointname=" + type + "&attrubute=" + attribute + "&customerid=" + customerid + "&branchid=" + branchid + "&FromDate=" + fromdate + "&Enddate=" + enddate + "&Filter=" + filter + "&functionid=" + functionid + "&ChartName=" + chartname + "&Internalsatutory=" + internalsatutory+"&listcategoryid="+listcategoryid+"&Userid="+userid+"&IsApprover="+isapprover);
                    } 
                }     
            }
             
            function windowfilter(){
                  $('#windowfilter').show();

                  var myWindowAdv = $("#windowfilter");
                  function onClose() {
                  }
                  myWindowAdv.kendoWindow({
                      width: "90%",
                      height: "30%",
                      title: "Filters",
                      visible: false,
                      actions: [
                          "Pin",
                          "Close"
                      ],
                      close: onClose
                  });
                  kendo.ui.progress($(".chart-loading"), true);
                  myWindowAdv.data("kendoWindow").center().open();
                  // WindowmonthWiseChart();
                  return false;
                
              }


            
              function ClosePanels(Id){
                  $('#' +Id).css("display","none");
               

                  var cls=  $('#' +Id+'Checkbox').attr('class');
                  $('#' +Id+'Checkbox').removeClass(cls);
                  $('#' +Id+'Checkbox').addClass('menucheckbox-notchecked');
                  //CheckCookies(Id+'Checkbox');

                  return false;
              }
              function ClosePanels12(Id){
                  $('#' +Id).css("display","none");
                  $('#' +obj).removeClass('widget col-md-12 plr0');
                  $('#' +obj).addClass('widget col-md-12 plr0');
                  $('#' +obj).css('width','97.2%');
                  //CheckCookies(Id+'Checkbox');
                  return false;
              }

              function ShowPanels(Id){
                  $('#' +Id).css("display","");
              }

              $(document).kendoTooltip({
                  filter: ".k-icon k-i-window-maximize",
                  content: function (e) {
                      return "Maximize";
                  }
              });
        </script>

        <style type="text/css">
            .dailyupdates p a {
                color: #666;
                text-decoration: none;
            }

            .bx-wrapper .bx-controls-direction a {
                position: absolute;
                top: 20%;
                outline: 0;
                width: 20px;
                height: 35px;
                text-indent: -9999px;
                z-index: 999;
            }

            .bx-wrapper .bx-prev {
                left: -4.5%;
            }

            .bx-wrapper .bx-next {
                right: -4.5%;
            }

            .downloadinage {
                background: url(../Images/regulatorydownload.png) no-repeat 6px center;
                width: 100px;
                height: 100px;
                display: block;
            }

            #UpDetailView {
                margin-top: 25px;
                margin-left: 20px;
            }

            .windowgrading {
                min-height: 150px;
                max-height: 200px;
            }

            /*.tile_stats_count {
                transition: color 1s ease-out;
                color: navy;
            }*/

            .tile_stats_count:hover {
                /*font-size:22px;
                   margin-bottom:23px;*/
                /*color: white;*/
                /*background-color:grey;*/
            }

            .bg-aqua {
                background-color: #00c0ef !important;
                color: #fff !important;
            }

            .bg-green {
                background-color: #00a65a !important;
            }

            .bg-red, .bg-yellow, .bg-aqua, .bg-blue, .bg-light-blue, .bg-green, .bg-navy, .bg-teal, .bg-olive, .bg-lime, .bg-orange, .bg-fuchsia, .bg-purple, .bg-maroon, .bg-black, .bg-red-active, .bg-yellow-active, .bg-aqua-active, .bg-blue-active, .bg-light-blue-active, .bg-green-active, .bg-navy-active, .bg-teal-active, .bg-olive-active, .bg-lime-active, .bg-orange-active, .bg-fuchsia-active, .bg-purple-active, .bg-maroon-active, .bg-black-active {
                color: #fff !important;
            }

            .bg-yellow {
                background-color: #f39c12 !important;
            }

            :after, :before {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            :after, :before {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            .small-box {
                border-radius: 2px;
                position: relative;
                display: block;
                margin-bottom: 20px;
                box-shadow: 0 1px 1px rgba(0,0,0,0.1);
            }

                .small-box:hover {
                    text-decoration: none;
                    color: #f9f9f9;
                    /* transition: all .3s linear; */
                    /*height:123px;
    width:155px;*/
                }

                .small-box > .inner {
                    padding: 10px;
                }

            .icon {
                -webkit-transition: all .3s linear;
                -o-transition: all .3s linear;
                transition: all .3s linear;
                position: absolute;
                top: -10px;
                right: 10px;
                z-index: 0;
                font-size: 96px;
                color: rgba(0,0,0,0.15);
            }

            .small-box > .small-box-footer {
                position: relative;
                text-align: center;
                padding: 3px 0;
                color: #fff;
                color: rgba(255,255,255,0.8);
                display: block;
                z-index: 10;
                background: rgba(0,0,0,0.1);
                text-decoration: none;
            }

            .divCount:hover {
                /*color: #FF7473;*/
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
            }

            .entity-bg {
                color: #fff;
                background: #2b80b7;
            }

            .compliances-bg {
                color: #fff;
                background: #64B973;
            }

            .dueToday-bg {
                color: #fff;
                background: #8F45AD;
            }

            .upcoming-bg {
                color: #fff;
                background: #004586;
            }

            .overdue-bg {
                color: #fff;
                background: #FF7473;
                /*#FF0000;*/
            }

            @media (min-width: 768px) {
                .five-cols .col-md-1,
                .five-cols .col-sm-1,
                .five-cols .col-lg-1 {
                    width: 100%;
                }
            }

            @media (min-width: 992px) {
                .five-cols .col-md-1,
                .five-cols .col-sm-1,
                .five-cols .col-lg-1 {
                    width: 20%;
                }
            }

            @media (min-width: 1200px) {
                .five-cols .col-md-1,
                .five-cols .col-sm-1,
                .five-cols .col-lg-1 {
                    width: 20%;
                }
            }

            #max:hover {
                z-index: 10;
            }

            .closeButton:hover, .maxButton:hover {
                -moz-box-shadow: 0 0 0 15px #f4f4f4;
                -webkit-box-shadow: 0 0 0 15px #f4f4f4;
                box-shadow: 0 0 0 15px #f4f4f4;
                -moz-transition: -moz-box-shadow .2s;
                -webkit-transition: -webkit-box-shadow .2s;
                transition: box-shadow .2s;
            }
        </style>

        <style>
            .mr10 {
                margin-left: 15px;
            }

            .ml5 {
                margin-left: 10px;
            }

            .map {
            }

            .columnchart {
            }

            #example, #example1 {
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            .dash-head {
                width: 970px;
                height: 80px;
                /*background: url('../content/web/sortable/dashboard-head.png') no-repeat 50% 50% #222222;*/
            }

            .panel-wrap {
                display: table;
                /* width: 968px;
                  background-color: #f5f5f5;
                    border: 1px solid #e5e5e5;*/
            }

            #sidebar2 #sidebar3 {
                display: table-cell;
                /*padding: 20px 0 20px 20px;
                    width: 220px;*/
                vertical-align: top;
            }

            .widget.placeholder {
                opacity: 0.4;
                border: 1px dashed #a6a6a6;
            }

            /* WIDGETS */
            .widget {
                /*margin: 0 0 20px;*/
                padding: 0;
                background-color: #ffffff;
                /*border: 1px solid #e7e7e7;*/
                border-radius: 3px;
                cursor: move;
            }

                .widget:hover {
                    background-color: #fcfcfc;
                    border-color: #cccccc;
                }

                .widget div {
                    /*padding: 10px;
                    min-height: 50px;*/
                }

                .widget h3 {
                    margin-top: 0px;
                    margin-bottom: 0px;
                    font-size: 17px;
                    padding: 10px 10px;
                    /*text-transform: uppercase;*/
                    /*border-bottom: 1px solid #e7e7e7;*/
                }

                    .widget h3 span {
                        float: right;
                    }

                        .widget h3 span:hover {
                            cursor: pointer;
                            background-color: #e7e7e7;
                            border-radius: 20px;
                        }

            /* PROFILE */
            .profile-photo {
                /*width: 80px;
                    height: 80px;*/
                /*margin: 10px auto;*/
                border-radius: 100px;
                border: 1px solid #e7e7e7;
                background: url('../content/web/Customers/ISLAT.jpg') no-repeat 50% 50%;
            }

            #profile div {
                /*text-align: center;*/
            }

            #profile #profile1 h4 {
                /*width: auto;
                    margin: 0 0 5px;
                    font-size: 1.2em;*/
                color: #1f97f7;
            }

            #profile p {
                /*margin: 0 0 10px;*/
            }

            /* BLOGS & NEWS */
            /*#blogs,
            #news,#profile1,#teammates1,#teammates2,#teammates3,#teammates4,#teammates5 {
                margin-bottom:15px;
              margin-right:15px;
            }*/


            #teammates teammates1 teammates2 h4,
            #blogs h4,
            #news h4 {
                width: auto;
                font-size: 1.4em;
                color: #1f97f7;
                font-weight: normal;
            }

            .blog-info {
                font-size: .9em;
                color: #787878;
            }

            #sidebar2 #sidebar3 #blogs h4 {
                font-size: 1em;
            }

            #sidebar2 #sidebar3 #blogs p {
                display: none;
            }

            #sidebar2 #blogs .blog-info {
                display: block;
            }

            #sidebar4 {
                /*margin-left:15px;*/
            }

            #mainsummery #news h4 {
                font-size: 1.2em;
                line-height: 1.4em;
                height: 40px;
            }

                #mainsummery #news h4 span {
                    display: block;
                    float: left;
                    width: 100px;
                    height: 40px;
                    color: #000;
                }

            #teammates6, #teammates7, #teammates, #teammates8, #profile, #news, #blogs, #profile1, #teammates1, #teammates5, #teammates2, #ContentPlaceHolder1_ComplianceCalender, #teammates4 {
                margin-left: 15px;
                margin-bottom: 15px;
            }
            /* TEAMMATES */
            .team-mate:after {
                content: ".";
                display: block;
                height: 0;
                line-height: 0;
                clear: both;
                visibility: hidden;
            }
            /*#teammates1, #teammates2,  #teammates5{
                 margin-right:15px;
                 margin-bottom:15px;
             }*/
            #teammates #teammates1 #teammates2 #teammates3 #teammates4 #teammates5 .team-mate h4 {
                font-size: 1.4em;
                font-weight: normal;
                /*margin-top: 12px;*/
            }

            .team-mate p {
            }

            .team-mate img {
                float: left;
                margin: 0 15px 0 0;
                border: 1px solid #e7e7e7;
                border-radius: 60px;
            }

            .hint {
                width: 250px;
                height: 100px;
                overflow: hidden;
            }

                .hint > h3 {
                    padding-left: 20px;
                }

            .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
                cursor: not-allowed;
                background-color: #ffffff;
                color: #000000;
            }

            .select_Date {
                width: 100%;
                margin-right: 0;
            }

            .icongcalender {
                color: #666;
            }

            .m10 {
                margin-left: 10px;
            }

            .k-icon-15 {
                font-size: 15px; /* Sets icon size to 32px */
            }

            imgCentered {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
        </style>

        <style type="text/css">
            b, strong {
                font-weight: 400;
            }

            div.k-window {
                z-index: 27;
            }

            .tile_count {
                /*margin-bottom: 15px;*/
                /*margin-top: 15px;*/
                /*margin-left: 15px;*/
                /*color: #666;*/
                /*border-bottom: 1px solid darkgrey;*/
            }

            /*.tile_count .tile_stats_count:before {
                    content: "";
                    position: absolute;
                    left: 0;
                    height: 100%;
                    border-left: 1px solid #1fd9e1;
                }*/

            *, :after, :before {
                box-sizing: border-box;
            }

            .tile_count .tile_stats_count {
                /*border-bottom: 1px solid darkgrey;*/
                padding: 0px 0px 0px 20px;
                position: relative;
                /*color: navy;*/
                /*margin-bottom: 10px;*/
                /*margin-bottom: 20px;*/
            }

            .tile_count .tile_stats_count, ul.quick-list li {
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .body {
                color: #eee;
            }

            *, :after, :before {
                box-sizing: border-box;
            }

            #dailyupdates .bx-viewport {
                height: auto !important;
            }

            .info-box:hover {
                /*color: #FF7473;*/
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
                z-index: 5;
            }

            .function-selector-radio-label {
                font-size: 12px;
            }

            .colorPickerWidget {
                padding: 10px;
                margin: 10px;
                text-align: center;
                width: 360px;
                border-radius: 5px;
                background: #fafafa;
                border: 2px solid #ddd;
            }

            #ContentPlaceHolder1_grdGradingRepportSummary.table tr td, #ContentPlaceHolder1_GridGraddingWindow.table tr td {
                border: 1px solid white;
            }

            .TMB {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 20% !important;
            }

            .TMB1 {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 19.5% !important;
            }

            .TMBImg {
                padding-left: 0px !important;
                padding-right: 0px !important;
                text-align: center;
                /*margin-left: -7%;
                margin-right: -3%;*/
            }

            .clscircle {
                margin-right: 7px !important;
            }

            .fixwidth {
                width: 20% !important;
            }

            .badge {
                font-size: 10px !important;
                font-weight: 200 !important;
            }

            .responsive-calendar .day {
                width: 13.7% !important;
                height: 45px;
            }

                .responsive-calendar .day.cal-header {
                    border-bottom: none !important;
                    width: 13.9% !important;
                    font-size: 17px;
                    height: 25px;
                }

            #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
                left: 0%;
            }

            #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
                left: 0%;
            }

            .bx-viewport {
                height: 270px !important;
            }
            /*li >h3{
                 border-bottom:0px ;
             }*/
            .graphcmp {
                margin-left: 36%;
                font-size: 16px;
                margin-top: -3%;
                color: #666666;
                font-family: 'Roboto';
            }

            .days > div.day {
                margin: 1px;
                background: #eee;
            }

            .overdue ~ div > a {
                background: red;
            }

            /*.info-box {
                min-height: 95px !important;
            }*/

            .info-box {
                min-height: 65px !important;
                width: 90%;
                margin-left: 10px;
            }

                .info-box .titleMD {
                    font-size: 18px;
                }

                .info-box .countMD {
                    font-size: 18px;
                }

            .div-location {
                min-height: 52px;
                padding-top: 0px;
            }

            .Dashboard-white-widget {
                padding: 5px 10px 0px !important;
            }

            .dashboardProgressbar {
                display: none;
            }

            .TMBImg > img {
                width: 37px;
            }

            #reviewersummary {
                height: 150px;
            }

            #performersummary {
                height: 150px;
            }

            #eventownersummary {
                height: 150px;
            }

            #performersummarytask {
                height: 150px;
            }

            #reviewersummarytask {
                height: 150px;
            }

            div.panel {
                margin-bottom: 12px;
            }

            .panel .panel-heading .panel-actions {
                height: 25px !important;
            }

            hr {
                margin-bottom: 8px;
            }

            .panel .panel-heading h2 {
                font-size: 18px;
            }

            td > label {
                padding: 6px;
            }

            .radioboxlist radioboxlistStyle {
                font-size: x-large;
                padding-right: 20px;
            }

            span.input-group-addon {
                padding: 0px;
            }

            td > label {
                padding: 3px 4px 0 4px;
                margin-top: -1%;
            }

            .nav-tabs > li > a {
                color: #333 !important;
            }

            .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
                color: #1fd9e1 !important;
            }
        </style>

        <style type="text/css">
            .streamline {
                position: relative;
                border-color: #dee5e7;
            }

                .streamline .sl-item:after, .streamline:after {
                    position: absolute;
                    bottom: 0;
                    left: 0;
                    width: 9px;
                    height: 9px;
                    margin-left: -5px;
                    background-color: #fff;
                    border-color: inherit;
                    border-style: solid;
                    border-width: 1px;
                    border-radius: 10px;
                    content: '';
                }

            .i-switch, .sl-item, .tl-content, .tl-date, .tl-wrap:before {
                position: relative;
            }

            .sl-item, .tl-wrap {
                border-color: #dee5e7;
            }

            .sl-item {
                padding-bottom: 1px;
            }

                .sl-item:after, .sl-item:before {
                    display: table;
                    content: " ";
                }

                .sl-item:after {
                    clear: both;
                    top: 6px;
                    bottom: auto;
                }

                .sl-item.b-l {
                    margin-left: 5px;
                }

            .m-b-n-2 {
                /*/ margin-bottom: -13px;*/
            }

            .m-r {
                margin-right: 15px;
            }

            .m-l {
                margin-left: 10px;
            }

            .b-info {
                border-color: #23b7e5;
            }

            .b-l {
                border-left: 1px solid #23b7e5;
            }

            .top-list li {
                display: block;
                border: 1px solid #edf1f2;
                padding: 26px 15px;
                margin: -1px 0 0;
                color: #58666e;
                -webkit-transition: all .2s ease;
                -moz-transition: all .2s ease;
                -o-transition: all .2s ease;
                transition: all .2s ease;
            }

                .top-list li:hover {
                    background: #8e8e8e;
                    color: #e8e8e8;
                    border: 1px solid #6d6d6d;
                }

                .top-list li a {
                    font-weight: 600;
                }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

            .table tr th {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

            .clsheadergrid {
                padding: 8px !important;
                line-height: 1.428571429 !important;
                vertical-align: bottom !important;
                border-bottom: 2px solid #dddddd !important;
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
                text-align: left;
            }

            .clsROWgrid {
                padding: 8px !important;
                line-height: 1.428571429 !important;
                vertical-align: top !important;
                border-top: 1px solid #dddddd !important;
                color: #666666 !important;
                font-size: 14px !important;
                font-family: 'Roboto', sans-serif !important;
            }

            .m-10 {
                margin-top: 10px;
                margin-left: 10px;
            }

            .btn-search {
                background: #1fd9e1;
                float: right;
            }

            .title-count {
                font-weight: bold;
                font-size: 16px;
                text-align: center;
            }


            .count-number {
                font-size: 36px;
                font-weight: bold;
                display: flex;
                align-items: center;
                justify-content: center;
            }
        </style>

        <style>
            .k-map .k-i-marker-custom-marker {
                background-image: url('https://demos.telerik.com/kendo-ui/content/shared/images/we-are-here.png');
                background-size: 50px;
                width: 50px;
                height: 50px;
            }
        </style>

        <script>
            $(document).ready(function(){            
                $('#divFloaterChange').parent().toggleClass('floater-active');
                $('#divFloaterChange').click(function () {
                    $('#divThemeChange').parent().removeClass('theme-active');
                    $('#divFloaterChange').parent().toggleClass('floater-active');                  
                });
            });   

        </script>
       
        <style>
            .dropdown-menu.extended {
                max-width: 700px !important;
                min-width: 700px !important;
            }

            .floater-panel {
                position: fixed;
                height: 100px;
                right: -50%;
                /*top: 62px;*/
                top: 100px;
                z-index: 1020;
                background: #fff;
                border-bottom-left-radius: 9px;
                padding: 15px;
                -webkit-box-shadow: 0 1px 17px rgba(0,0,0,.175);
                box-shadow: 0 1px 17px rgba(0,0,0,.175);
                width: 50%;
                -webkit-transition: right .2s linear;
                -moz-transition: right .2s linear;
                transition: right .2s linear;
            }

                .floater-panel .floater-collapse-btn {
                    position: absolute;
                    left: -30px;
                    top: 17px;
                    margin-top: -17px;
                    width: 30px;
                    height: 33px;
                    line-height: 31px;
                    font-size: 14px;
                    text-align: center;
                    color: #939393;
                    background-color: #fff;
                    -webkit-background-clip: padding-box;
                    background-clip: padding-box;
                    border: 1px solid rgba(158, 151, 151, 0.15);
                    border-top-left-radius: 4px;
                    border-bottom-left-radius: 4px;
                    border-right: 0;
                    -webkit-box-shadow: 0 1px 17px rgba(0,0,0,.175);
                    box-shadow: 0 1px 17px rgba(0,0,0,.175);
                }


            .floater-active {
                /*right: 0;*/
                right: 0px;
            }

            .floater-btn {
                padding: 1px 3px;
                border-radius: 2px;
                background: #ffffff;
                border: 1px solid #d2cfcf !important;
                color: #908c8c;
                font-size: 11px;
            }

            .floater-panel .floater-list {
                margin: 0;
                padding: 0;
            }

            .stylesectionname {
                color: #558ed5;
            }

            .styleduedate {
                color: #00b050;
            }
            div > p {
       color: black; 
}
            .clear, .text-ellipsis {
     display: block; 
     overflow: hidden; 
}
           .marq{
                   margin-left: 15px;
    margin-right: 30px;
           }
        </style>
    
    <style>
          div > p {
       color: black; 
}
            .clear, .text-ellipsis {
     display: block; 
     overflow: hidden; 
}
           .marq{
                   margin-left: 15px;
    margin-right: 30px;
           }
            h4, span {
            font-weight: 500;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- container section start -->
    <section id="container-fluid" class="">
        <section class="">
            <!-- Top Count start -->            
             <div class="clear">
                <marquee onmouseover="this.stop();" class="marq" onmouseout="this.start();" behavior="scroll" direction="left" scrollamount="7">
        <div>
            <p id="MarqueeSectionData"></p>
        </div>     
        </marquee>
            </div>
            <div id="divTabs" runat="server" class="row col-md-12 tile_count">
                <div id="entitycountDivBox" runat="server" class="five-cols col-md-1 TMB">
                    <div class="info-box bluenew-bg">
                        <div class="div-location" style="cursor: pointer;"> <%--onclick="showEntityDetails();"--%>
                            <div class="col-md-5 TMBImg">
                                <img src="../Images/Location-new.png" />
                            </div>
                            <div class="col-md-7 colpadding0">
                                <div class="titleMD" style="text-align: left">Entities</div>
                                <div id="divEntitesCount" runat="server" class="countMD" style="text-align: left">0</div>
                            </div>
                        </div>
                    </div>                    
                </div>

                <div id="compliancecountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                    <div class="info-box compliances-bg">
                        <div class="div-location" style="cursor: pointer" onclick="fCompliances(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')">
                            <div class="col-md-5 TMBImg">
                                <img src="../Images/Compliances-new.png" />
                            </div>
                            <div class="col-md-7 colpadding0" style="margin-left: -4%;">
                                <div class="titleMD"style="text-align: left;">Compliances</div>
                                <div id="divCompliancesCount" runat="server" class="countMD" style="text-align: left">0</div>
                            </div>
                        </div>
                    </div>                    
                </div>

                <div id="duetodayCountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                    <div class="info-box dueToday-bg">
                        <div class="div-location" style="cursor: pointer" onclick="fDueTodayCompliances(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>','Due Today Compliances')">
                            <div class="col-md-5 TMBImg">
                                <img src="../Images/Due Today.png" />
                            </div>
                            <div class="col-md-7 colpadding0">
                                <div class="titleMD" style="text-align: left">Due Today</div>
                                <div id="divDueToday" runat="server" class="countMD" style="text-align: left">0</div>
                            </div>
                        </div>
                    </div>
                    <!--/.info-box-->
                </div>

                <div id="upcomingCountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                    <div class="info-box upcoming-bg">
                        <div class="div-location" style="cursor: pointer" onclick="fDueTodayCompliances(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>','Upcoming Compliances')">
                            <div class="col-md-5 TMBImg">
                                <img src="../Images/Upcoming-new.png" />
                            </div>
                            <div class="col-md-7 colpadding0">
                                <div class="titleMD" style="text-align: left">Upcoming</div>
                                <div id="divUpcomingCount" runat="server" class="countMD" style="text-align: left;">0</div>
                            </div>
                        </div>
                    </div>                    
                </div>

                <div id="OverdueCountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
                    <div class="info-box overdue-bg">
                        <div class="div-location" style="cursor: pointer" onclick="fDueTodayCompliances(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>','Overdue Compliances')">
                            <div class="col-md-5 TMBImg">
                                <img src="../Images/Overdue-new.png" />
                            </div>
                            <div class="col-md-7 colpadding0">
                                <div class="titleMD" style="text-align: left">Overdue</div>
                                <div id="divOverdueCount" runat="server" class="countMD" style="text-align: left;">0</div>
                            </div>
                        </div>
                    </div>                   
                </div>
            </div>
            <!-- Top Count End -->

            <!-- Monthwise Compliance Summary start -->
            <div class="row">
                <div id="example" class="">
                    <div class="panel-wrap hidden-on-narrow col-md-12 pl0 colpadding0" style="padding: 0">
                        <div id="sidebar2">
                          
                            <div id="profile1" class="widget col-md-4 colpadding0" style="width: 48%">
                                <h3><b>Performance Summary </b>&nbsp;(<%= MonthlyYear%>)
                                    <span class="k-icon k-i-close k-icon-15 closeButton" onclick="return ClosePanels('profile1')"></span>
                                    <span class="k-icon k-i-window-maximize k-icon-15 maxButton" onclick="return MaximizePerfSummaryPieChart()"></span>
                                </h3>
                                 <div class="col-md-12 colpadding0">
                                    <div id="perStatusPieChartDiv" class="col-md-12 colpadding0" style="height: 280px;">
                                        <%--width: 353px--%>
                                    </div>
                                </div>
                            </div>

                               <div id="teammates1" class="widget col-md-4 colpadding0" style="width: 48%">
                                <h3>
                                    <b>Risk Summary</b>&nbsp;(<%= MonthlyYear%>)
                                    <span class="k-icon k-i-close k-icon-15 closeButton" onclick="return ClosePanels('teammates1')"></span>
                                    <span class="k-icon k-i-window-maximize k-icon-15 maxButton" onclick="return OpenRiskchart()"></span>
                                </h3>

                                <div class="col-md-12 colpadding0">                                    
                                    <div id="perRiskStackedColumnChartDiv" class="col-md-12 colpadding0" style="height: 280px;">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="sidebar4">
                            <div id="profile" class="widget col-md-4 colpadding0" style="width: 48%">
                                <h3>
                                    <b>Applicable Statutes &nbsp;</b>(<span id="StatuesCount" runat="server" style="float:none;"></span>)
                                    <span class="k-icon k-i-close k-icon-15 closeButton" onclick="return ClosePanels('profile')"></span>
                                    <span class="k-icon k-i-window-maximize k-icon-15 maxButton" onclick="return ApplicableActs(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')"></span>
                                </h3>

                                <div id="divApplicableActs" class="streamline m-b-n-2 m-l m-r plr0"
                                    style="width: 95%; cursor: pointer; overflow: auto; min-height: 280px; max-height: 280px; color: #666;">
                                    <% foreach (var i in LstApplicableStatues)
                                        {%>
                                    <div class="sl-item b-info b-l" style="max-height: 65px !important">
                                        <div class="m-l text-sm" style="margin-top: 5px;" id="<%=i.Key %>" onclick="return OpenActs(<%=i.Key %>,<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')">
                                            <%= i.Value %><p>&nbsp;</p>
                                        </div>
                                    </div>
                                    <%}%>
                                </div>
                            </div>

                            <div id="teammates" class="widget col-md-4 colpadding0" style="width: 48%">
                                <h3>
                                    <b>Assigned Locations</b>
                                    <span class="k-icon k-i-close k-icon-15 closeButton" onclick="return ClosePanels('teammates')"></span>
                                    <span class="k-icon k-i-window-maximize k-icon-15 maxButton" onclick="return OpenLocations()"></span>
                                </h3>
                               
                                        <div class="col-md-12 colpadding0">
                                        <div class="col-md-12 colpadding0 map" style="height: 280px; padding: 0; margin-bottom: 0px" id="divMapPanIndiaCompliance">
                                        </div>
                                    </div>
                               
                            </div>

                            <%--<div style="clear: both; height: 1px;"></div>--%>

                            <div id="news" class="widget col-md-12 plr0" style="width: 97.2%;clear:both">
                                <h3>
                                    <b>Summary of Overdue Compliances</b>
                                    <span class="k-icon k-i-close k-icon-15 closeButton" onclick="return ClosePanels('news')"></span>
                                    <span class="k-icon k-i-window-maximize k-icon-15 maxButton" onclick="return OpenOverdueComplainceList()"></span>
                                </h3>
                                <div class="row">
                                    <div id="DivOverDueCompliance" runat="server" class="col-md-12">

                                        <asp:GridView runat="server" ID="grdSummaryDetails" AutoGenerateColumns="false" GridLines="None" AllowSorting="false"
                                            CellPadding="4" CssClass="table" AllowPaging="false" ShowHeaderWhenEmpty="true" DataKeyNames="ComplianceInstanceID">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Compliance items">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                                            <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortForm") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Responsibility" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                            <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# GetPerformer((long)Eval("ComplianceInstanceID")) %>' ToolTip='<%#Performername%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Due Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>' ToolTip='<%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Period">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblForMonth" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="RiskCategory" HeaderText="Risk" Visible="false" />

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                                            <ContentTemplate>
                                                                <asp:ImageButton ID="lblOverView" runat="server"
                                                                    CssClass="k-button k-button-icontext" Visible="false"
                                                                    ScheduledOnID='<%# Eval("ScheduledOnID")%>' instanceId='<%#Eval("ComplianceInstanceID") %>' ctype='Statutory' src ="../Images/Eye.png" 
                                                                    OnClientClick='fComplianceOverview(this)' ToolTip="Click to OverView"></asp:ImageButton>
                                                                 <input type="image" name="grdCompliancePerformer$ctl02$lblOverView" id="grdCompliancePerformer_lblOverView_0" title="Click to OverView" scheduledonid='<%# Eval("ScheduledOnID")%>' instanceid='<%#Eval("ComplianceInstanceID") %>' ctype='Statutory' src ="../Images/Eye.png" 
                                                                   onclick="fComplianceOverview(this);" style="cursor:pointer" />
                                                               <%-- <a role="button" class="k-button k-button-icontext ob-overview k-grid-edit2"  scheduledonid='<%# Eval("ScheduledOnID")%>' instanceid='<%#Eval("ComplianceInstanceID") %>' ctype='Statutory'
                                                                    onclick='fComplianceOverview(this)' tooltip="Click to OverView" style="border-radius: 44px; width: 36px; height: 34px;"></a>--%>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Records Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>

                                        <asp:GridView runat="server" ID="grdInternalSummaryDetails" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                                            CellPadding="4" AllowPaging="false"
                                            CssClass="table" DataKeyNames="InternalComplianceInstanceID">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Compliance items">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                                            <asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Responsibility">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                            <asp:Label ID="lblInternalPerformer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# GetPerformerInternal((long)Eval("InternalComplianceInstanceID")) %>' ToolTip='<%#InternalPerformername%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Due Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="Label3" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("InternalScheduledOn")!= null?((DateTime)Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy"):""%>' ToolTip='<%# Eval("InternalScheduledOn")!= null?((DateTime)Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Period">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                            <asp:Label ID="Label4" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RiskCategory" HeaderText="Risk" />
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:ImageButton ID="lblOverView1" runat="server" ImageUrl="~/Images/eye.png" ScheduledOnID='<%# Eval("InternalScheduledOnID")%>' instanceId='<%#Eval("InternalComplianceInstanceID") %>' ctype='Internal'
                                                                    OnClientClick='fComplianceOverview(this)' ToolTip="Overview"></asp:ImageButton>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Records Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>

                                        <div class="col-md-1" style="float: right;">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="lnkOverdueCompliance" Style="margin-left: -5px; color: blue; font-style: italic;" OnClientClick="OpenOverdueComplainceList()" runat="server">Show All</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkInternalOverdueCompliance" Style="margin-left: -5px; color: blue; font-style: italic;" OnClientClick="OpenInternalOverdueComplainceList()" Visible="false" runat="server">Show All</asp:LinkButton>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 colpadding0" id="ComplianceCalender" runat="server" style="width: 97.2%;clear:both">
                                <div id="teammates3" class="widget col-md-12">
                                    <h3>
                                        <b>My Compliance Calendar</b>
                                        <span class="k-icon k-i-close k-icon-15 closeButton" onclick="return ClosePanels('ContentPlaceHolder1_ComplianceCalender')"></span>
                                        <span class="k-icon k-i-window-maximize k-icon-15 maxButton" onclick="return OpenCalenderWindow()"></span>
                                    </h3>

                                    <div class="row col-md-12 colpadding0">
                                        <div class="col-md-5 colpadding0" style="background-color: #ffffff; height: 409px">
                                            <div>
                                                <fieldset runat="server" id="fldsCalender">
                                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                        <ContentTemplate>
                                                            <div class="radiobuttoncalendercontainer ml5">
                                                                <asp:RadioButtonList ID="rdbcalender" runat="server"
                                                                    RepeatDirection="Horizontal" Visible="false">
                                                                    <asp:ListItem Text="Statutory + Internal " Value="0" />
                                                                    <asp:ListItem Text="All(Including Checklist)" Value="1" Selected="True" />
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="rdbcalender" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </fieldset>
                                            </div>

                                            <!-- Responsive calendar - START -->
                                            <div class="responsive-calendar mr10">
                                                <div class="chart-loading" id="idloadercal2"></div>
                                                <%--//<img src="../images/processing.gif" style="position: absolute;">--%>
                                            </div>
                                            <!-- Responsive calendar - END -->
                                        </div>

                                        <div class="col-md-7" style="background-color: #ffffff;">
                                            <div style="">
                                                <span id="clsdatel" style="color: #515967;"></span>
                                                <br />
                                                <i style="color: #515967;">Select a date from calendar to view details</i>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="datacal">
                                                <div class="chart-loading" id="idloadercal"></div>
                                                <%--<img src="../images/processing.gif" id="imgcaldate" style="position: absolute;">--%>
                                                <iframe id="calframe" src="about:blank" scrolling="no" frameborder="0" width="100%" height="363px"></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                        
                            <div class="col-md-12 colpadding0" id="newsletter" style="width: 97.2%; display: none;clear:both">
                                <div id="teammates5" class="widget col-md-12">
                                    <h3>
                                        <b>News Letter</b>
                                        <span class="k-icon k-i-close k-icon-15 closeButton" onclick="return ClosePanels('newsletter')"></span>
                                    </h3>
                                    <div>
                                        <div class="">
                                            <div class="row dailyupdates">
                                                <ul id="Newslettersrow" class="bxslider bxnews" style="width: 100%; max-width: none;">
                                                </ul>
                                            </div>



                                            <div class="col-md-12">
                                                <a class="btn btn-search" style="float: right;" href="../RLCS/RLCS_NewsLetterList.aspx" title="View">View All</a>
                                                <div class="clearfix" style="height: 50px"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                           <%-- <div style="clear: both; height: 1px;"></div>--%>
                        </div>
                    </div>
                </div>
            </div>

             <div class="col-md-12 colpadding0" id="dailyupdates" style="width: 97.2%; padding-left: 10px; clear: both; display:block">
                <div id="teammates4111" class="widget col-md-12">
                    <h3>
                        <b>Regulatory Updates</b>

                    </h3>
                    <div class="row col-md-12 colpadding0" style="padding-top: 20px;">
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                <div class="row">

                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="ddlMonthReg" Width="180px" CssClass="form-control">
                                            <asp:ListItem Text="--Select Month--" Value="-1" />
                                            <asp:ListItem Text="January" Value="01" />
                                            <asp:ListItem Text="February" Value="02" />
                                            <asp:ListItem Text="March" Value="03" />
                                            <asp:ListItem Text="April" Value="04" />
                                            <asp:ListItem Text="May" Value="05" />
                                            <asp:ListItem Text="June" Value="06" />
                                            <asp:ListItem Text="July" Value="07" />
                                            <asp:ListItem Text="August" Value="08" />
                                            <asp:ListItem Text="September" Value="09" />
                                            <asp:ListItem Text="October" Value="10" />
                                            <asp:ListItem Text="November" Value="11" />
                                            <asp:ListItem Text="December" Value="12" />
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-2">
                                        <asp:DropDownList runat="server" ID="ddlYearReg" AutoPostBack="true" Width="180px" class="form-control">
                                            <asp:ListItem Text="--Select Year--" Value="-1" />
                                            <asp:ListItem Text="2020" Value="2020" />
                                            <asp:ListItem Text="2019" Value="2019" />
                                            <asp:ListItem Text="2018" Value="2018" />
                                            <asp:ListItem Text="2017" Value="2017" />
                                            <asp:ListItem Text="2016" Value="2016" />
                                            <asp:ListItem Text="2015" Value="2015" />
                                        </asp:DropDownList>
                                       
                                    </div>
                                    <div class="col-md-2">
                                         <asp:DropDownList runat="server" ID="ddlState" Width="180px" 
                                            class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:DropDownList runat="server" ID="ddlAct" Width="250px" 
                                            class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-1 colpadding0">
                                        <button class="btn btn-primary" id="view" onclick="return binddailyupdatedata()">View</button>
                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div>
                        <div class="row" style="padding-top: 20px;">
                            <div class="row dailyupdates">
                                <ul id="DailyUpdatesrow" onclick="return trackUseractivity()" class="bxslider bxdaily" style="width: 100%; max-width: none;">
                                </ul>
                            </div>

                            <div style="clear: both; height: 1px;"></div>

                            <div class="col-md-12">
                                <a class="btn btn-search" style="float: right; display: none;" href="../RLCS/RLCS_DailyUpdateList.aspx" title="View">View All</a>
                                <div class="clearfix" style="height: 0px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- PAN INDIA Compliance MAP, Invoice, Applicable Acts END -->

            <div class="clearfix"></div>

            <div class="floater-panel floater-active" id="floaterpnl">
                <button type="button" data-click="floater-panel-expand" id="divFloaterChange" class="floater-collapse-btn"><span class="k-icon k-i-filter"></span></button>

                <div class="floater-panel-content clear" id="floaterpanel">
                    <div class="floater-list dropdown-menu extended logout" id="floatermenu">
                        <h4>
                            <b style="margin-left: 10px">Filters</b>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-2 colpadding0" style="padding-left: 23px; display: none;">
                                    <asp:DropDownList runat="server" ID="ddlStatus" Width="350px" class="form-control">
                                        <asp:ListItem Text="Statutory" Value="0" Selected="True" />
                                    </asp:DropDownList>
                                </div>

                                <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                                    <ContentTemplate>
                                        <div class="col-md-6 colpadding0" style="background-color: #ffffff;">
                                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" CssClass="txtbox" autocomplete="off" />
                                            <div style="margin-left: 1px; display: none; position: absolute; z-index: 10; overflow-y: auto; max-height: 200px; width: 100%;" id="divFilterLocation">
                                                <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="128%" NodeStyle-ForeColor="#8e8e93"
                                                    Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                </asp:TreeView>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <div class="col-md-6">
                                    <div class="col-md-6 colpadding0">
                                        <%-- <fieldset runat="server" id="fldrbFinancialYearFunctionSummery">--%>
                                        <asp:UpdatePanel ID="uprdo" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList runat="server" ID="ddlYearNew" Width="150px" OnSelectedIndexChanged="ddlYearNew_SelectedIndexChanged"
                                                    class="form-control">
                                                    <asp:ListItem Text="Select Year" Value="-1" />
                                                    <asp:ListItem Text="2020" Value="0" Selected="True" />
                                                    <asp:ListItem Text="2019" Value="1" />
                                                    <asp:ListItem Text="2018" Value="2" />
                                                    <asp:ListItem Text="2017" Value="3" />
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <%--</fieldset>--%>
                                    </div>
                                    <div class="col-md-6 colpadding0 text-right">
                                        <asp:Button ID="btnTopSearch" class="btn btn-primary" runat="server" Text="Apply" OnClick="btnTopSearch_Click"></asp:Button>                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="row" style="display:none;">
                            <div class="col-md-6 pl0 colpadding0">
                                 <asp:TextBox ID="txtAdvStartDate" runat="server" placeholder="Start Date" />
                            </div>

                            <div class="col-md-6">
                                <div class="col-md-6 colpadding0">
                                    <asp:TextBox ID="txtAdvEndDate" runat="server" placeholder="End Date" />
                                </div>
                                <div class="col-md-6 colpadding0"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <div class="theme-panel theme-active" id="Settingfilter">
                    <button type="button" data-click="theme-panel-expand" id="divThemeChange" class="theme-collapse-btn" style="display:none;"><span class="fa fa-cog"></span></button>

                    <div class="theme-panel-content clear" id="thempanel" style="display:none">
                        <ul class="theme-list dropdown-menu extended logout" id="settingsmenu">
                            <li class="eborder-top" onclick="return fred(this)" data-ref="../RLCS/TlChangepassword.aspx" style="cursor: pointer; display:none;">
                                <a>Change Password</a>
                            </li>

                            <li onclick="return fred(this)" data-ref="../SecurityQuestion/ResetSecurityQuestions.aspx" style="cursor: pointer; display:none;">
                                <a>Change Security Questions</a>
                            </li>

                            <li class="rev_dr">
                                <a href="javascript:showDiv('teammates6')"  onclick="return CheckCookies('teammates6Checkbox')">
                                    <div id="teammates6Checkbox" class="menucheckbox-checked" ></div>
                                    Month Wise Summary</a>
                            </li>

                            <li class="rev_dr">
                                <a href="javascript:showDiv('teammates7')" onclick="return CheckCookies('teammates7Checkbox')">
                                    <div id="teammates7Checkbox" class="menucheckbox-checked"></div>
                                    Compliance Summary</a>
                            </li>

                            <li class="rev_dr">
                                <a href="javascript:showDiv('profile1')"  onclick="return CheckCookies('profile1Checkbox')">
                                    <div id="profile1Checkbox" class="menucheckbox-checked" ></div>
                                    Performance Summary</a>
                            </li>

                            <li class="rev_dr">
                                <a href="javascript:showDiv('profile')"  onclick="return CheckCookies('profileCheckbox')">
                                    <div id="profileCheckbox" class="menucheckbox-checked"></div>
                                    Applicable Statutes</a>
                            </li>

                            <li class="rev_dr">
                                <a href="javascript:showDiv('teammates')" onclick="return CheckCookies('teammatesCheckbox')">
                                    <div id="teammatesCheckbox" class="menucheckbox-checked"></div>
                                    Assigned Locations</a>
                            </li>

                            <li class="rev_dr">
                                <a href="javascript:showDiv('teammates1')" onclick="return CheckCookies('teammates1Checkbox')">
                                    <div id="teammates1Checkbox" class="menucheckbox-checked"></div>
                                    Risk Summary</a>
                            </li>

                            <li class="rev_dr">
                                <a href="javascript:showDiv('news')" onclick="return CheckCookies('newsCheckbox')">
                                    <div id="newsCheckbox" class="menucheckbox-checked"></div>
                                    Summary of Overdue Compliances</a>
                            </li>

                           <%-- <li class="rev_dr">
                                <a href="javascript:showDiv('compliancesummary')" onclick="return CheckCookies('compliancesummaryCheckbox')">
                                    <div id="compliancesummaryCheckbox" class="menucheckbox-checked"></div>
                                    Grading Report</a>
                            </li>--%>

                            <li class="rev_dr">
                                <a href="javascript:showDiv('ContentPlaceHolder1_ComplianceCalender')" onclick="return CheckCookies('ContentPlaceHolder1_ComplianceCalenderCheckbox')">
                                    <div id="ContentPlaceHolder1_ComplianceCalenderCheckbox" class="menucheckbox-checked"></div>
                                    My Compliance Calendar</a>
                            </li>

                            <li class="rev_dr">
                                <a href="javascript:showDiv('dailyupdates')" onclick="return CheckCookies('dailyupdatesCheckbox')">
                                    <div id="dailyupdatesCheckbox" class="menucheckbox-checked"></div>
                                    Regulatory Updates</a>
                            </li>

                            <li class="rev_dr" style="display:none;">
                                <a href="javascript:showDiv('newsletter')" onclick="return CheckCookies('newsletterCheckbox')">
                                    <div id="newsletterCheckbox" class="menucheckbox-checked"></div>
                                    News Letters</a>
                            </li>

                            <li onclick="return fred(this)" data-ref="../TlLogout.aspx" style="cursor: pointer; display:none;">
                                <a id="lbtLogout1">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>

            <div id="openlink" style="height: 98%; margin-top: 5px; margin-left: 20px; margin-right: 20px;">
            </div>

            <div id="windowduetoday" style="height:93%"> 
                <div class="col-md-10" style="height:93%">
                <div class="chart-loading" id="KendoLoaderDue"></div>
                <%-- <div class="k-loading-mask" id="loaderdue" style="width:100%;height:100%"><span class="k-loading-text">Loading...</span>
                        <div class="k-loading-image"><div class="k-loading-color"></div></div></div> --%>
                <iframe id="iframeduemycompliances" style="width: 1200px; height: 800px;"></iframe>
                </div>
            </div>

            <div id="windowApplicableActs">
                <div>
                    <div class="col-md-5 form-group pl0 colpadding0" style="padding: 0; margin-bottom: 0px; width: 30%;">
                        <div id="windowActs" class="streamline m-b-n-2 m-l plr0"
                            style="width: auto; cursor: pointer; overflow: auto; min-height: 150px; color: #666; margin-top: 10px;">

                            <% foreach (var i in LstApplicableStatues)
                                {%>

                            <div class="sl-item b-info b-l">
                                <div class="m-l text-sm" onclick="return OpenActClick(<%= i.Key %>,<%= customerID %>,<%=BID %>,'<%=IsApprover%>')">
                                    <%= i.Value %><p>&nbsp;</p>
                                </div>
                            </div>
                            <%}%>
                        </div>
                    </div>

                    <div class="col-md-7 pl0 colpadding0" id="">
                        <div class="chart-loading" id="idloader8"></div>
                        <iframe id="iframeActs" class="col-md-12  pl0 colpadding0" style="height: 480px; width: 870px; border: 0px"></iframe>
                    </div>
                </div>
            </div>


               <div id="windowpiechart">
                <div class="panel-heading">
                    <ul id="rblRole2" class="nav nav-tabs">

                        <li class="active" id="liGraphperformance">
                            <asp:LinkButton ID="LinkButton2" OnClientClick="return graph('Graphperformance','Detailsperformance','liGraphperformance','liDetailsPer')" runat="server">Graph</asp:LinkButton>
                        </li>

                        <li class="" id="liDetailsPer">
                            <asp:LinkButton ID="LinkButton3" OnClientClick="return Detail('Graphperformance','Detailsperformance','liGraphperformance','liDetailsPer')" runat="server">Details</asp:LinkButton>
                        </li>
                    </ul>
                    <div class="clearfix" style="clear: both; height: 5px;"></div>
                    <div id="Graphperformance" class=" m-t" style="margin-top: 29px;">

                        <div class="col-md-12 form-group pl0" style="height: 90%; width: 92%; padding: 0; margin-bottom: 0px" id="windowfunctionPie">
                        </div>

                    </div>
                    <div id="Detailsperformance" class=" m-t">
                        <div class="chart-loading" id="KendoLoader"></div>
                        <iframe id="iframePerformance" class="col-md-12  pl0 colpadding0" style="width: 100%; height: 461px; border: 0px"></iframe>
                    </div>
                </div>
            </div>

            <div id="window">
                <div class="panel-heading">
                    <ul id="rblRolepan" class="nav nav-tabs">

                        <li class="active" id="liGraphpan">
                            <asp:LinkButton ID="LinkButton6" OnClientClick="return graph('Graphpan','Detailspan','liGraphpan','liDetailspan')" runat="server">Locations</asp:LinkButton>
                        </li>

                        <li class="" id="liDetailspan">
                            <asp:LinkButton ID="LinkButton7" OnClientClick="return Detail('Graphpan','Detailspan','liGraphpan','liDetailspan')" runat="server">Details</asp:LinkButton>
                        </li>
                    </ul>
                    <div id="Graphpan" class=" m-t" style="margin-top: 29px; height: 95%">

                        <div class="col-md-12 form-group pl0" style="padding: 0; width: 1170px; margin-bottom: 0px" id="Mapwindow">
                        </div>

                    </div>
                    <div id="Detailspan" class=" m-t" style="margin-top: 20px;">
                        <div class="chart-loading" id="idloader6"></div>
                        <iframe id="iframepan" src="about:blank" class="col-md-12 pl0 colpadding0" style="width: 100%; height: 520px; border: 0px"></iframe>
                    </div>


                </div>
            </div>

            <div id="windowfunctionchart">
                <div class="panel-heading">
                    <ul id="rblRolefunction" class="nav nav-tabs">

                        <li class="active" id="liGraphMonthlypie">
                            <asp:LinkButton ID="LinkButton8" OnClientClick="return graph('GraphPie','DetailsPie','liGraphMonthlypie','liDetailsMonthlypie')" runat="server">Graph</asp:LinkButton>
                        </li>

                        <li class="" id="liDetailsMonthlypie">
                            <asp:LinkButton ID="LinkButton9" OnClientClick="return Detail('GraphPie','DetailsPie','liGraphMonthlypie','liDetailsMonthlypie')" runat="server">Details</asp:LinkButton>
                        </li>
                    </ul>

                    <div id="GraphPie" class=" m-t">
                        <div class="col-md-12 form-group pl0" style="height: 70%; width: 92%; padding: 0; margin-bottom: 0px" id="windowMonthlyPie">
                        </div>
                    </div>

                    <div id="DetailsPie" class="col-md-12 m-t" style="margin-top: 29px;">
                        <div class="chart-loading" id="idloader3"></div>

                        <div class="row" style="display: none;">
                            <asp:DropDownListChosen runat="server" ID="ddlPageSize" class="" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                <asp:ListItem Text="10" Selected="True" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownListChosen>

                            <div runat="server" id="DivRecordsScrum" style="float: right;">
                                <p style="padding-right: 0px !Important;">
                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="chart-loading" id="idloader2"></div>
                        <asp:UpdatePanel ID="upGridMonthlyComplianceWindow" runat="server">
                            <ContentTemplate>
                                <asp:GridView runat="server" ID="GridMonthlyPie" AutoGenerateColumns="false" GridLines="none" AllowSorting="true"
                                    CssClass="table" Width="100%" PageSize="10">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Entity">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblSections" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Entity") %>' ToolTip='<%# Eval("Entity") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="State">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("SM_Name") %>' ToolTip='<%# Eval("SM_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Branch">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LM_Name") %>' ToolTip='<%# Eval("LM_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Month">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                    <asp:Label ID="lblActName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("MonthName") %>' ToolTip='<%# Eval("MonthName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Year">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                    <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Year") %>' ToolTip='<%# Eval("Year") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Complied(%)" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                    Text='<%# Eval("CompliedNo") %>' ToolTip='<%# Eval("CompliedNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Not Complied(%)" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                    Text='<%# Eval("NotCompliedNo") %>' ToolTip='<%# Eval("NotCompliedNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />

                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>

                                <div class="col-md-12 colpadding0" style="display: none;">
                                    <div class="col-md-6 colpadding0" style="float: right;">
                                        <div class="table-paging" style="margin-bottom: 20px;">
                                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                            <div class="table-paging-text">
                                                <p>
                                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>

                                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>

            <div id="windowriskchart">
                <div class="panel-heading">
                    <ul id="rblRole1" class="nav nav-tabs">
                        <li class="active" id="liGraph">
                            <asp:LinkButton ID="lnkGraph" OnClientClick="return graph('Graph','Details','liGraph','liDetails')" runat="server">Graph</asp:LinkButton>
                        </li>
                        <li class="" id="liDetails">
                            <asp:LinkButton ID="LinkButton1" OnClientClick="return Detail('Graph','Details','liGraph','liDetails')" runat="server">Details</asp:LinkButton>
                        </li>
                    </ul>

                    <div id="Graph" class="m-t">
                        <div class="col-md-12 form-group pl0" style="height: 80%; width: 80%; padding: 0; margin-bottom: 0px" id="windowriskcolumn">
                        </div>
                    </div>

                    <div id="Details" class=" m-t">
                        <div class="chart-loading" id="KendoLoaderRisk"></div>
                        <iframe id="iframerisk" class="col-md-12 pl0 colpadding0" style="width: 100%; height: 466px; border: 0px"></iframe>
                    </div>
                </div>
            </div>

            <div id="windowcalender">
                <div class="col-md-12 form-group pl0" style="height: 95%; padding: 0; margin-bottom: 0px" id="compliancecalender">
                    <div>
                        <div class="col-md-5 colpadding0" style="background-color: #ffffff;">
                            <div>
                                <fieldset runat="server" id="Fieldset1">
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <div class="radiobuttoncalendercontainer ml5">
                                                <asp:RadioButtonList ID="rbcalendar1" runat="server" Visible="false"
                                                    RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Statutory + Internal " Value="0" />
                                                    <asp:ListItem Text="All(Including Checklist)" Value="1" Selected="True" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="rdbcalender" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </fieldset>
                            </div>

                            <!-- Responsive calendar - START -->
                            <div class="responsive-calendar mr10">
                                <img src="../images/processing.gif">
                            </div>
                            <!-- Responsive calendar - END -->
                        </div>
                        <div class="col-md-6" style="background-color: #ffffff;">
                            <div style="">
                                <span id="clsdatel1"></span>
                                <br />
                                <i style="">Select a date from calendar to view details</i>
                            </div>
                            <div class="clearfix"></div>
                            <div id="datacal1">
                                <div class="chart-loading" id="idloadercal1"></div>
                                <%--<img src="../images/processing.gif" id="imgcaldate1">--%>
                                <iframe id="calframe1" src="about:blank" scrolling="no" frameborder="0" width="100%" height="363px"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:HiddenField ID="highcolor" runat="server" Value="#FF7473" />
            <asp:HiddenField ID="mediumcolor" runat="server" Value="#FFC952" />
            <asp:HiddenField ID="lowcolor" runat="server" Value="#1FD9E1" />

            <div class="modal fade" id="Newslettermodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 550px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <img id="newsImg" src="../Images/xyz.png" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; width: 100%" />
                            <h2 id="newsTitle"></h2>
                            <div class="clearfix" style="height: 10px;"></div>
                            <div id="newsDesc"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="NoFileFoundModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 330px; height: 200px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <h1 id="filenotfoundmessage">No File Found</h1>
                        </div>
                    </div>
                </div>
            </div>

            <iframe src="about:blank" id="dwnfile" style="display: none;"></iframe>
             
            <div id="divreportOVerdue">
                <iframe id="showOverdueDetails" src="blank.html" width="1177px" height="670px" frameborder="0"></iframe>
            </div>

            <%--<div class="modal fade" id="divreports" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 95%;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="showdetails" src="blank.html" width="1100px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>--%>

            <div id="divreports" style="height: 98%; margin-top: 5px; margin-left: 20px; margin-right: 20px;">
            </div>

            <div class="modal fade" id="divNotification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h3 style="text-align: center">Notifications</h3>
                            <br />
                            <div id="divNotificationData"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1150px;">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none; height: 20px;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body">

                                <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </section>

    <asp:HiddenField ID="hidposition" runat="server" Value="" />

    <script type="text/javascript" src="/Newjs/responsive-calendar.min.js"></script>

    <script type="text/javascript">
        //var userpid='<%= userProfileID %>';
        //function getLocalItems()
        //{
        //    localStorage.clear();
        //    var content =   $("#sidebar2").getKendoSortable().element.context.innerHTML  //var content = $("#sidebar2").getKendoSortable().element.innerHTML;
        //    localStorage.setItem(userpid+"sideBar2", content);
        //    var content1  = $("#sidebar4").getKendoSortable().element.context.innerHTML;
        //    localStorage.setItem(userpid+"sideBar4", content1);
        //    //else{
        //    //    var content =   $("#sidebar2").getKendoSortable().element.context.innerHTML  //var content = $("#sidebar2").getKendoSortable().element.innerHTML;
        //    //    localStorage.setItem("sideBar2", content);
        //    //    var content1  = $("#sidebar4").getKendoSortable().element.context.innerHTML;// $("#sidebar4").getKendoSortable().element.context.innerHTML  //
        //    //    localStorage.setItem("sideBar4", content1);
        //    //}
        //}

        $(document).ready(function() {
                
            $("#sidebar2").kendoSortable({
                filter: ">div",
                cursor: "move",
                connectWith: "#sidebar2,#sidebar4",
                placeholder: placeholder,
                hint: hint,
               // change: SideBar2ContentChange
            });
               
            $("#sidebar4").kendoSortable({
                filter: ">div",
                cursor: "move",
                connectWith: "#sidebar4,#sidebar2",
                placeholder: placeholder,
                hint: hint,
                //change: SideBar4ContentChange
            }); 
        });

        function placeholder(element) {
            return element.clone().addClass("placeholder");
        }

        function hint(element) {
            return element.clone().addClass("hint")
                        .height(element.height())
                        .width(element.width());
        }
       
    </script>

    <script type="text/javascript">           
        function loaderhide(){
            $('#idloader').hide();
            $('#idloader2').hide();
            $('#idloader3').hide();
            $('#idloader4').hide();
            $('#idloader5').hide();
            $('#idloader7').hide();
            $('#idloader6').hide();
            $('#idloader8').hide();
            $('#idloader9').hide();
            $('#idloadercal').hide();
            $('#idloadercal1').hide();
            $('#idloadercal4').hide();
            $('#KendoLoader').hide();
            $('#KendoLoaderRisk').hide();
            $('#KendoLoaderLocation').hide();
        }

        function CheckCookies(Id){
            
            var cls= $('#'+Id).attr('class');

            var user= '<%= userProfileID%>';
                var c1= user+Id;
                $('#dailyupdates .bx-viewport').css('width','998px');
                if(cls=="menucheckbox-checked")
                {
                   
                    $('#'+Id).removeClass(cls);
                    $('#'+Id).addClass("menucheckbox-notchecked");
               
                    var s= Id.replace("Checkbox", "");
                    $('#' +s).css("display","none");
                    
                    if($.cookie(c1)!=undefined)
                    {
                        $.removeCookie(c1);
                        $.cookie(c1, 'menucheckbox-notchecked', { expires: 1000});
                        //getLocalItems();
                    }
                    else
                    {
                        $.cookie(c1, 'menucheckbox-notchecked', { expires: 1000});
                        //getLocalItems();
                    }
                    var ite = $('.bxdaily').bxSlider({
                        minSlides: 4,
                        maxSlides: 4,
                        slideWidth: 227,
                        slideMargin: 30,
                        moveSlides: 1,
                        auto: false,
                        nextCss: 'dailyupdatenext',
                        nextclickFunction: 'fdailyclick()',
                    });
             
                }
                else{
                    
                    $('#'+Id).removeClass(cls);
                    $('#'+Id).addClass("menucheckbox-checked");
                    var s= Id.replace("Checkbox", "");
                    $('#' +s).css("display","block");
                    if($.cookie(c1)!=undefined)
                    {
                        $.removeCookie(c1);
                        $.cookie(c1, 'menucheckbox-checked', { expires: 1000});
                        //getLocalItems();
                    }
                    else
                    {
                        $.cookie(c1, 'menucheckbox-checked', { expires: 1000});
                        //getLocalItems();
                    }
                    //$.cookie(Id, 'menucheckbox-checked', { expires: 1000});
                    var ite = $('.bxdaily').bxSlider({
                        minSlides: 4,
                        maxSlides: 4,
                        slideWidth: 227,
                        slideMargin: 30,
                        moveSlides: 1,
                        auto: false,
                        nextCss: 'dailyupdatenext',
                        nextclickFunction: 'fdailyclick()',
                    });
                }
                var highchartWidth = $("#highcharts-0").width();
                
                $("#highcharts-2").width(highchartWidth);
                $("#highcharts-6").width(highchartWidth);
                $('#highcharts-4').width(highchartWidth);
                
                if(Id.includes("profile1Checkbox")){                   
                    StatusPieChart();
                }
                else if(Id.includes("teammates1")){                  
                    RiskColumnChart();
                }
                else if(Id.includes("teammates7Checkbox")){                  
                    MonthlyCompliancePieChart();
                }

                return false;
            }
                      
            $(document).ready(function() {
                var userid='<%= userProfileID%>';
                $(function () { 
                    $("body").click(function (e) {                      
                        if ((e.target.id == "Settingfilter" || $(e.target).parents("#Settingfilter").length)
                            || (e.target.id == "floaterpnl" || $(e.target).parents("#floaterpnl").length)
                            || (e.target.id == "ContentPlaceHolder1_ddlYearNew_listbox" || $(e.target).parents("#ContentPlaceHolder1_ddlYearNew_listbox").length)
                            || (e.target.id == "ContentPlaceHolder1_txtAdvStartDate_dateview" || $(e.target).parents("#ContentPlaceHolder1_txtAdvStartDate_dateview").length)
                            || (e.target.id == "ContentPlaceHolder1_txtAdvEndDate_dateview" || $(e.target).parents("#ContentPlaceHolder1_txtAdvEndDate_dateview").length)) {
                             
                        } else {

                            $('#divThemeChange').parent().removeClass('theme-active');
                            $('#divFloaterChange').parent().removeClass('floater-active');
                        }
                    });
                });

                kendo.ui.progress($(".chart-loading"), true);

                $('#divThemeChange').on('click',function(){
                    $('#divFloaterChange').parent().removeClass('floater-active');
                });
                
                $('.maxButton').kendoTooltip({ content: "Maximize",
                    position: "top",
                });

                $('.closeButton').kendoTooltip({ content: "Close",
                    position: "top",
                }); 
                         
                if($.cookie(userid+'teammates7Checkbox')!=undefined){

                    if($.cookie(userid+'teammates7Checkbox') =="menucheckbox-notchecked")
                    {
                        //$('#teammates7Checkbox').css("display","none");
                        $('#teammates7Checkbox').removeClass("menucheckbox-checked");
                        $('#teammates7Checkbox').addClass($.cookie(userid+'teammates7Checkbox'));
                        $('#teammates7').css('display','none');
                    }
                    else{
                    
                        $('#teammates7Checkbox').removeClass("menucheckbox-notchecked");
                        $('#teammates7Checkbox').addClass($.cookie(userid+'teammates7Checkbox'));
                        $('#teammates7').css('display','block');
                    }                   
                }

                if($.cookie(userid+'teammates6Checkbox')!=undefined){
                    if($.cookie(userid+'teammates6Checkbox') =="menucheckbox-notchecked"){                   
                        $('#teammates6Checkbox').removeClass("menucheckbox-checked");
                        $('#teammates6Checkbox').addClass($.cookie(userid+'teammates6Checkbox'));
                        $('#teammates6').css('display','none');
                    }else{
                        $('#teammates6Checkbox').removeClass("menucheckbox-notchecked");
                        $('#teammates6Checkbox').addClass($.cookie(userid+'teammates6Checkbox'));
                        $('#teammates6').css('display','block');
                    } 
                }

                if($.cookie(userid+'profile1Checkbox')!=undefined){
                    if($.cookie(userid+'profile1Checkbox') =="menucheckbox-notchecked"){                   
                        $('#profile1Checkbox').removeClass("menucheckbox-checked");
                        $('#profile1Checkbox').addClass($.cookie(userid+'profile1Checkbox'));
                        $('#profile1').css('display','none');
                    }else{
                        $('#profile1Checkbox').removeClass("menucheckbox-notchecked");
                        $('#profile1Checkbox').addClass($.cookie(userid+'profile1Checkbox'));
                        $('#profile1').css('display','block');
                    } 
                }

                if($.cookie(userid+'profileCheckbox')!=undefined){
                    if($.cookie(userid+'profileCheckbox') =="menucheckbox-notchecked"){                   
                        $('#profileCheckbox').removeClass("menucheckbox-checked");
                        $('#profileCheckbox').addClass($.cookie(userid+'profileCheckbox'));
                        $('#profile').css('display','none');
                    }
                    else{
                        $('#profileCheckbox').removeClass("menucheckbox-notchecked");
                        $('#profileCheckbox').addClass($.cookie(userid+'profileCheckbox'));
                        $('#profile').css('display','block');
                    } 
                }

                if($.cookie(userid+'teammatesCheckbox')!=undefined){
                    if($.cookie(userid+'teammatesCheckbox') =="menucheckbox-notchecked"){                  
                        $('#teammatesCheckbox').removeClass("menucheckbox-checked");
                        $('#teammatesCheckbox').addClass($.cookie(userid+'teammatesCheckbox'));
                        $('#teammates').css('display','none');
                    }else{
                        $('#teammatesCheckbox').removeClass("menucheckbox-notchecked");
                        $('#teammatesCheckbox').addClass($.cookie(userid+'teammatesCheckbox'));
                        $('#teammates').css('display','block');
                    } 
                }

                if($.cookie(userid+'teammates1Checkbox')!=undefined){
                    if($.cookie(userid+'teammates1Checkbox') =="menucheckbox-notchecked"){                   
                        $('#teammates1Checkbox').removeClass("menucheckbox-checked");
                        $('#teammates1Checkbox').addClass($.cookie(userid+'teammates1Checkbox'));
                        $('#teammates1').css('display','none');
                    }else{
                        $('#teammates1Checkbox').removeClass("menucheckbox-notchecked");
                        $('#teammates1Checkbox').addClass($.cookie(userid+'teammates1Checkbox'));
                        $('#teammates1').css('display','block');
                    } 
                }

                if($.cookie(userid+'newsCheckbox')!=undefined){
                    if($.cookie(userid+'newsCheckbox') =="menucheckbox-notchecked"){                   
                        $('#newsCheckbox').removeClass("menucheckbox-checked");
                        $('#newsCheckbox').addClass($.cookie(userid+'newsCheckbox'));
                        $('#news').css('display','none');
                    }else{
                        $('#newsCheckbox').removeClass("menucheckbox-notchecked");
                        $('#newsCheckbox').addClass($.cookie(userid+'newsCheckbox'));
                        $('#news').css('display','block');
                    } 
                }
           
                if($.cookie(userid+'compliancesummaryCheckbox')!=undefined){
                    if($.cookie(userid+'compliancesummaryCheckbox') =="menucheckbox-notchecked"){                   
                        $('#compliancesummaryCheckbox').removeClass("menucheckbox-checked");
                        $('#compliancesummaryCheckbox').addClass($.cookie(userid+'compliancesummaryCheckbox'));
                        $('#compliancesummary').css('display','none');
                    }else{
                        $('#compliancesummaryCheckbox').removeClass("menucheckbox-notchecked");
                        $('#compliancesummaryCheckbox').addClass($.cookie(userid+'compliancesummaryCheckbox'));
                        $('#compliancesummary').css('display','block');
                    } 
                }

                if($.cookie(userid+'ContentPlaceHolder1_ComplianceCalenderCheckbox')!=undefined){
                    if($.cookie(userid+'ContentPlaceHolder1_ComplianceCalenderCheckbox') =="menucheckbox-notchecked"){                   
                        $('#ContentPlaceHolder1_ComplianceCalenderCheckbox').removeClass("menucheckbox-checked");
                        $('#ContentPlaceHolder1_ComplianceCalenderCheckbox').addClass($.cookie(userid+'ContentPlaceHolder1_ComplianceCalenderCheckbox'));
                        $('#ContentPlaceHolder1_ComplianceCalender').css('display','none');
                    }else{
                        $('#ContentPlaceHolder1_ComplianceCalenderCheckbox').removeClass("menucheckbox-notchecked");
                        $('#ContentPlaceHolder1_ComplianceCalenderCheckbox').addClass($.cookie(userid+'ContentPlaceHolder1_ComplianceCalenderCheckbox'));
                        $('#ContentPlaceHolder1_ComplianceCalender').css('display','block');
                    } 
                }

                if($.cookie(userid+'dailyupdatesCheckbox')!=undefined )
                {
                    if($.cookie(userid+'dailyupdatesCheckbox') =="menucheckbox-notchecked")
                    {
                   
                        $('#dailyupdatesCheckbox').removeClass("menucheckbox-checked");
                        $('#dailyupdatesCheckbox').addClass($.cookie(userid+'dailyupdatesCheckbox'));
                        $('#dailyupdates').css('display','none');
                    }
                    else{
                        $('#dailyupdatesCheckbox').removeClass("menucheckbox-notchecked");
                        $('#dailyupdatesCheckbox').addClass($.cookie(userid+'dailyupdatesCheckbox'));
                        $('#dailyupdates').css('display','block');
                    } 
                }
                var highchartWidth = $("#teammates6").width();
               
                $("#highcharts-2").css('width','');
                $("#highcharts-6").css('width','');
                $('#highcharts-4').css('width','');
               // $('#highcharts-2').css('width','');
                $('#dailyupdates .bx-viewport').css('width','998px');
            });
    </script>

    <script type="text/javascript">
        function fposition() {
            $('#<%=hidposition.ClientID%>').val(document.body.scrollTop);
        }

        function fsetscroll(){            
            document.body.scrollTop=$('#<%=hidposition.ClientID%>').val();
        }

        //carousel
        $(document).ready(function () {          
            $('.bxslider-map').bxSlider({                
                minSlides: 1,
                maxSlides: 1,
                slideWidth: 250,
                slideMargin: 50,
                moveSlides: 1,
                auto: false,

            });
        });

        //custom select box
        $(function () {
            $('select.styled').customSelect();
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $(".knob").knob({
                draw: function () {
                    // "tron" case
                    if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = 1;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                            && (sat = eat - 0.3)
                            && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                            ea = this.startAngle + this.angle(this.v);
                            this.o.cursor
                                && (sa = ea - 0.3)
                                && (ea = ea + 0.3);
                            this.g.beginPath();
                            this.g.strokeStyle = this.pColor;
                            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                            this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();
                        return false;
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        // replace/populate colors from user saved profile        
        var perFunctionChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium:"<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>"
        };
        var perRiskStackedColumnChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium:"<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>"
        };
        var perStatusChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium:"<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>"
        };

     
        // Chart Global options
        Highcharts.setOptions({
            credits: {
                text: '',
                href: 'https://www.avantis.co.in',
            },
            lang: {
                drillUpText: "< Back",
            },
        });       

        function  windowFunctioncoulmnChart()
        {
            //perFunctionChart
            var perFunctionChart = Highcharts.chart('windowfunctioncolumn', {
                chart: {
                    type: 'column',
                   
                },
                title: {
                    text: 'Per Function',
                    style: {
                        display: 'none'
                    },
                },
                subtitle: {
                    text: 'Completion Status - Overall Functions',
                    style: {
                        "font-family": 'Roboto',
                        fontWeight: '300',
                        fontSize:'15px'
                    }
                },
                xAxis: {
                    type: 'category',
                },
                yAxis: {
                    title: {
                        text: 'Number of Compliances'
                    },
                    //labels: {
                    //    enabled:false,
                    //},
                },
                plotOptions: {
                   
                    series: {
                        cursor: 'pointer',
                        events: {
                            click: function (event) {
                                    
                                Detail('GraphMonthly','DetailsMonthly','liGraphMonthlky','liDetailsMonthly');

                            }
                        },
                        dataLabels: {
                            enabled: true,
                            style: {
                                color: 'gray',
                            }
                        },
                    }
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    headerFormat: '<b>{point.key}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },              
                    <%=perFunctionChart%>
            });            
            return false;

        }

        function StatusPieChart(){
            var perStatusPieChart = Highcharts.chart('perStatusPieChartDiv', {
                chart: {
                    type: 'pie',
                    events: {
                        load: function(event) {                            
                            $(document).resize(); 
                        },
                        drilldown: function (e) {
                            this.setTitle({ text: e.point.name });
                            // this.subtitle.update({ text: 'Click on graph to view documents' });
                            //this.subtitle.update({ text: 'Completion Status - Overall Risk' });
                        },
                        drillup: function () {
                            this.setTitle({ text: 'Per Status' });
                            //this.subtitle.update({ text: 'Click on graph to drilldown' });
                            //this.subtitle.update({ text: 'Completion Status - Overall' });
                        }
                    },
                },
                subtitle: {
                    text: 'Click on graph to view details',
                    //  text: '',
                    style: {
                        // "font-family": 'Helvetica',
                    }
                },
                title: {
                    text: 'Per Status',
                    style: {
                        display: 'none'
                    },
                },
                xAxis: {
                    type: 'category',
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        events: {
                            click: function (event) {
                                    
                                Detail('GraphMonthly','DetailsMonthly','liGraphMonthlky','liDetailsMonthly');

                            }
                        },
                            
                        dataLabels: {
                            enabled: true,
                            //format: '{y} <br>{point.name}',
                            format: '{y}',
                            distance: 5,
                        },
                        showInLegend: true,
                    },
                  
                },
                legend: {
                    itemDistance: 2,
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    formatter: function () {
                        if (this.series.name == 'Status')
                            return 'Click to Drilldown';	// text before drilldown
                        else
                            return 'Click to View Documents';		// text after drilldown
                    }
                },
                <%=perFunctionPieChart%>                
            });
            return false;

        }

        function  WindowStatusPieChart()
        {
            var perStatusPieChart = Highcharts.chart('windowfunctionPie', {
                chart: {
                    type: 'pie',
                  
                },
                title: {
                    text: 'Per Status',
                    style: {
                        display: 'none'
                    },
                },
               
                xAxis: {
                    type: 'category',
                },
                plotOptions: {
                    series: {                        
                        dataLabels: {
                            enabled: true,
                            //format: '{y} <br>{point.name}',
                            format: '{y}',
                            distance: 5,
                        },
                        showInLegend: true,
                    },                  
                },
                legend: {
                    itemDistance: 2,
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    formatter: function () {
                        if (this.series.name == 'Status')
                            return 'Click to Drilldown';	// text before drilldown
                        else
                            return 'Click to View Documents';		// text after drilldown
                    }
                },
                <%=perFunctionPieChart%>                
            });
            return false;

        }

        function RiskColumnChart(){
            var perRiskStackedColumnChart = Highcharts.chart('perRiskStackedColumnChartDiv', {
                chart: {
                    type: 'column',
                    events: {
                        load: function(event) {
                            
                            $(document).resize(); 
                        }
                    }
                },
                title: {
                    text: 'Per Risk',
                    style: {
                        // "font-family": 'Helvetica',
                        display: 'none'
                    },
                },
                subtitle: {
                    text: 'Click on graph to view details',
                    //  text: '',
                    style: {
                        // "font-family": 'Helvetica',
                    }
                },
                xAxis: {
                    categories: ['High', 'Medium', 'Low']
                },
                yAxis: {
                    title: {
                        text: 'Number of Compliances',                     
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: 'gray'
                        }
                    }
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: 'white',
                            style:{
                              
                                textShadow:false,
                            }
                        },
                        cursor: 'pointer'
                    },
                },
                <%=perRiskChart%>
                });

                return false;
            }

            function WindowRiskColumnChart()
            {
                var perRiskStackedColumnChart = Highcharts.chart('windowriskcolumn', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Per Risk',
                        style: {
                            // "font-family": 'Helvetica',
                            display: 'none'
                        },
                    },
                    subtitle: {
                        //text: 'Click on graph to view documents',
                        text: '',
                        style: {
                            // "font-family": 'Helvetica',
                        }
                    },
                    xAxis: {
                        categories: ['High', 'Medium', 'Low']
                    },
                    yAxis: {
                        title: {
                            text: 'Number of Compliances',                     
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: 'gray'
                            }
                        }
                    },
                    tooltip: {
                        hideDelay: 0,
                        backgroundColor: 'rgba(247,247,247,1)',
                        headerFormat: '<b>{point.x}</b><br/>',
                        pointFormat: '{series.name}: {point.y}'
                    },
                    plotOptions: {
                   
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                color: 'white',
                                style:{
                              
                                    textShadow:false,
                                }
                            },
                            cursor: 'pointer'
                        },
                    },
                    drilldown: {
                        series: [{}],
                    },
                <%=perRiskChart%>
                 
             });

             return false;
         }

         function GradingChart()
         {
             var perRiskStackedColumnChart = Highcharts.chart('windowgradingcolumn', {
                 chart: {
                     type: 'column'
                 },
                 title: {
                     text: 'Per Risk',
                     style: {
                         // "font-family": 'Helvetica',
                         display: 'none'
                     },
                 },
                 subtitle: {
                     //text: 'Click on graph to view documents',
                     text: '',
                     style: {
                         // "font-family": 'Helvetica',
                     }
                 },
                 xAxis: {
                     categories: ['High', 'Medium', 'Low']
                 },
                 yAxis: {
                     title: {
                         text: 'Number of Compliances',                     
                     },
                     stackLabels: {
                         enabled: true,
                         style: {
                             fontWeight: 'bold',
                             color: 'gray'
                         }
                     }
                 },
                 tooltip: {
                     hideDelay: 0,
                     backgroundColor: 'rgba(247,247,247,1)',
                     headerFormat: '<b>{point.x}</b><br/>',
                     pointFormat: '{series.name}: {point.y}'
                 },
                 plotOptions: {
                     column: {
                         stacking: 'normal',
                         dataLabels: {
                             enabled: true,
                             color: 'white',
                             style:{
                              
                                 textShadow:false,
                             }
                         },
                         cursor: 'pointer'
                     },
                 },
                <%=perRiskChart%>
            });

            return false;
        }


        $(document).ready(function(){
            //FunctioncoulmnChart();
            StatusPieChart();
            RiskColumnChart();

            // change color in color picker according to chart selected
            $('input[name=radioButton]').change(function () {
                // destroy all color pickers
                $('#highColorPicker').simplecolorpicker('destroy');
                $('#mediumColorPicker').simplecolorpicker('destroy');
                $('#lowColorPicker').simplecolorpicker('destroy');

                // setting the colors for risks according to chart selected
                var chart = $('input[name=radioButton]:checked').val();
                switch (chart) {
                    case "perFunction":
                        $('#highColorPicker').val(perFunctionChartColorScheme.high);
                        $('#mediumColorPicker').val(perFunctionChartColorScheme.medium);
                        $('#lowColorPicker').val(perFunctionChartColorScheme.low);
                        break;
                    case "perRisk":
                        $('#highColorPicker').val(perRiskStackedColumnChartColorScheme.high);
                        $('#mediumColorPicker').val(perRiskStackedColumnChartColorScheme.medium);
                        $('#lowColorPicker').val(perRiskStackedColumnChartColorScheme.low);
                        break;
                    case "perStatus":
                        $('#highColorPicker').val(perStatusChartColorScheme.high);
                        $('#mediumColorPicker').val(perStatusChartColorScheme.medium);
                        $('#lowColorPicker').val(perStatusChartColorScheme.low);
                        break;
                    default:
                        $('#highColorPicker').val('#7CB5EC');
                        $('#mediumColorPicker').val('#434348');
                        $('#lowColorPicker').val('#90ED7D');
                }

                // initialise the color piskers again
                $('#highColorPicker').simplecolorpicker({ picker: true });
                $('#mediumColorPicker').simplecolorpicker({ picker: true });
                $('#lowColorPicker').simplecolorpicker({ picker: true });
            });

            // chart type selector radio buttons
            try	{
                $("#radioButtonGroup").buttonset();
                $(".chart-selector-radio-buttons").checkboxradio({
                    icon: false
                });
            }catch(e){}
            //apply color scheme to all charts [Apply to All] click event handler
            $('#applyToAllButton').click(function () {
                // value of colors selected at the time in color picker
                // $('#highColorPicker').val();
                // $('#mediumColorPicker').val();
                // $('#lowColorPicker').val();
            });

            $('#showModal').click(function () {
                $('#modalDiv').modal();
            });

        });   //	main documentReady function END

        var btnclosemgt=$('.modal-header').find('button');
        $(btnclosemgt).click(function () { 
            $('#showdetails').attr('src','blank.html');
        });
                
        function showEntityDetails()
        {          
            $('#divreports').modal('show');  
            $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','500px'); 
            $('.modal-dialog').css('width','95%');   
            $('#showdetails').attr('src', "../RLCS/EntitiesDetails.aspx");
        }

        function fCompliances(customerid,branchid,IsSatutoryInternal,isapprover){
            var IsAvantisFlag=true;
            function onClose() {
                $(this.element).empty();
            }

            function onOpen(e) {
                kendo.ui.progress(e.sender.element, true);
            }

            var myWindowAdv = $("#divreports");
            myWindowAdv.kendoWindow({width: "90%",
                height: "80%",
                title: "Compliances",
                actions: [
              "Pin",
              "Close"
                ],
                close: onClose,
                open: onOpen,
                content: {
                    url: "../RLCS/ComplianceDetails.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+isapprover+"&IsAvantisFlag="+IsAvantisFlag,
                    // aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+0);
                    dataType: "json",
                       
                    iframe: true,
                       
                    //template: "#= data#"
                }  
            });
               
            myWindowAdv.data("kendoWindow").center().open();

            return false;
        }

        function fDueTodayCompliances(customerid,branchid,IsSatutoryInternal,isapprover,filterType){
            $('#windowduetoday').show();
                
            var myWindowAdv = $("#windowduetoday");
            function onClose() {
				$(this.element).empty();
            }

            myWindowAdv.kendoWindow({
                width: "90%",
                height: "80%",
                title: filterType,  
				content: "../RLCS/RLCS_MyDueUpcomingCompliance.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&filterType="+ filterType + "&IsApprover="+isapprover,
                iframe: true,
                visible: false,
                actions: [
                    "Pin",
                    "Close"
                ],
                open :onOpen,
                close: onClose
            });
            function onOpen(e) {
                kendo.ui.progress(e.sender.element, true);
            }
            //$("#windowduetoday").kendoWindow();
            //var dialog =myWindowAdv.data("kendoWindow").title(filterType);
            //dialog.title(filterType);
            kendo.ui.progress($(".chart-loading"), true);
            myWindowAdv.data("kendoWindow").title(filterType).center().open();
            //myWindowAdv.data("kendoWindow").title(filterType);
          //  $('#iframeduemycompliances').attr('src', "../RLCS/RLCS_MyDueUpcomingCompliance.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&filterType="+ filterType + "&IsApprover="+isapprover);
                       
            return false;
        }        
    </script>    

    <script type="text/javascript">       
        var dailyupadatepageno = 1;
        var Newslettersrowpageno = 1;
        function fdailyclick() { dailyupadatepageno += 1; binddailyupdatedata(); }
        //function fnewsclick() { Newslettersrowpageno += 1; bindnewsdata(); }
        var dailyupdateJson;
        var NewsLetterJson;
        function bindnewsdata() {
            
            var k = 0;
            $.ajax({
                async: true,
                type: 'Get',                
                url: 'https://login.avantis.co.in/apadr/Data/GetNewsletterData?page=' + Newslettersrowpageno,
                content: 'application/json;charset=utf-8',
                processData: true,
                headers: {
                    "Authorization": "Bearer"
                },
                success: function (result) {
                    NewsLetterJson = result;
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.length; step++) {
                        var objDate = new Date(result[step].NewsDate);
                        str += '<li><img style="width:250px;height:166px" src="../../NewsLetterImages/' + result[step].FileName + '" /><h3>' + result[step].Title + '</h3><p><a class="view-pdf" data-toggle="modal" style="cursor:pointer" onclick="viewpdf(this)" data-title="' + result[step].Title + '"  data-href="../NewsLetterImages/' + result[step].DocFileName + '"> Issue ' + getmonths(objDate) + ' ' + objDate.getFullYear() + '</a></p></li>'
                    }
                    $("#Newslettersrow").html(str);
                    var ker = $('.bxnews').bxSlider({
                        minSlides: 3,
                        maxSlides: 3,
                        slideWidth: 250,
                        // slideHeight: 250,
                        slideMargin: 50,
                        moveSlides: 1,
                        auto: false,
                        nextCss: 'newsletternext',
                        nextclickFunction:'fnewsclick()',
                    });
                },
                error: function (e, t) { }
            })
        }
        
        var date = new Date();
        if($('#ContentPlaceHolder1_ddlMonthReg').val()!=null && $('#ContentPlaceHolder1_ddlMonthReg').val()!='-1')
        {
            m = date.getMonth();
        }
        else
            m = date.getMonth()+1;
        y = date.getFullYear();
        $(document).ready(function(){
            document.getElementById('ContentPlaceHolder1_ddlMonthReg').value = '0'+m.toString();
            document.getElementById('ContentPlaceHolder1_ddlYearReg').value = y;
        });
        function binddailyupdatedata() {
            var k = 0;
           
            if($('#ContentPlaceHolder1_ddlMonthReg').val()!=null && $('#ContentPlaceHolder1_ddlMonthReg').val()!='-1' )
            {
                m = $('#ContentPlaceHolder1_ddlMonthReg').val();
                var firstDay=y.toString()+m.toString()+'01';
                var lastDay =y.toString()+m.toString()+'28';
            }
            else
            {
                m= '0'+m;
                var firstDay=y.toString()+'0'+m.toString()+'01';
                var lastDay =y.toString()+'0'+m.toString()+'28';
            }
               

            if($('#ContentPlaceHolder1_ddlState').val()!='-1')
                var state =$('#ContentPlaceHolder1_ddlState option:selected').text();
            else
                var state="";
          
       
            if($('#ContentPlaceHolder1_ddlAct').val()!= null && $('#ContentPlaceHolder1_ddlAct').val()!=undefined)
                var act =$('#ContentPlaceHolder1_ddlAct').val();
            debugger;
            
            $.ajax({
                type: 'POST',
                url:'<%=AvacomRLCSApiURL%>' + "getregulatory_update",
                crossDomain:true,
                //dataType: "jsonp",
                data:{StateName:state,ActId:act,Start:firstDay, End:lastDay},
                headers: {
                    "Authorization":'<%= RegulatoryAuthKey%>',
                    "X-User-Id-1":'<%= UserAuthKey%>',
                },
                success: function (result) {
                    debugger;
                    if(result.Result != null && result.Result !=undefined ){
                        var abc= Object.keys(result.Result.data).length;
                        var step = 0;
                        var str = '';
                        info=[];
                        if(abc == 0)
                            $('.dailyupdates').hide();
                        for (step = 0; step < abc; step++) {
                         
                            $('.dailyupdates').show();
                            var id=''; 
                            //str += '<li><p><a data-toggle="modal" onclick="return BindRegulatoryupdatepopup( \'' + result.data[step].title + '\');" href="#NewsModal">'+ result.data[step].title +'</a></p></li>'
                            str += '<li><p><a data-toggle="modal" onclick="return BindRegulatoryupdatepopup(\'' + step+ '\');" href="#NewsModal">'+ result.Result.data[step].title +'</a></p></li>'
                            info.push(result.Result.data[step]);
                          
                        }
                      
                        var slider =  $('.bxdaily').bxSlider();
                      
                        if(slider.length!=0)
                        {
                            $('.bx-wrapper').remove();
                            $('.bx-viewport').remove();
                            var daily= $('#teammates4111 .row > .dailyupdates').append('<ul id="DailyUpdatesrow" onclick="return trackUseractivity()" class="bxslider bxdaily" style="width: 100%; max-width: none;"> </ul>');
                           
                            $("#DailyUpdatesrow").append(str);
                            
                            var ite = $('.bxdaily').bxSlider({
                                mode:'horizontal',
                                minSlides: 4,
                                maxSlides: 4,
                                slideWidth: 227,
                                slideMargin: 30,
                                moveSlides: 1,
                                auto: false,
                                nextCss: 'dailyupdatenext',
                                nextclickFunction: 'fdailyclick()',
                            });
                           
                            $('.bx-wrapper').css('max-width','999px');
                        }
                        else{
                            $("#DailyUpdatesrow").append(str);
                            var ite = $('.bxdaily').bxSlider({
                                mode:'horizontal',
                                minSlides: 4,
                                maxSlides: 4,
                                slideWidth: 227,
                                slideMargin: 30,
                                moveSlides: 1,
                                auto: false,
                                nextCss: 'dailyupdatenext',
                                nextclickFunction: 'fdailyclick()',
                            });
                        }

                       
                    }
                },
                error: function (e, t) { }
            })
        }       

        $(document).ready(function () {           
            binddailyupdatedata();
            //bindnewsdata();
        });
       
        function timeconvert(ds) {
            var D, dtime, T, tz, off,
            dobj = ds.match(/(\d+)|([+-])|(\d{4})/g);
            T = parseInt(dobj[0]);
            tz = dobj[1];
            off = dobj[2];
            if (off) {
                off = (parseInt(off.substring(0, 2), 10) * 3600000) +
                 (parseInt(off.substring(2), 10) * 60000);
                if (tz == '-') off *= -1;
            }
            else off = 0;
            return new Date(T += off).toUTCString();
        }

        function getmonths(mon) {
            try {
                return mon.toString().split(' ')[1];
            } catch (e) { }
        }
       
        function BindRegulatoryupdatepopup(UpdateID) {
            var uri ='<%= tlConnect_url%>Dashboard/GetRegulatoryTable?UpdateId="'+UpdateID;
            $.ajax({
                async: true,
                type: 'Get',               
                //url: "https://tlconnectapi.teamlease.com/api/Dashboard/GetRegulatoryTable?UpdateId="+UpdateID,
                url: uri,
                content: 'application/json;charset=utf-8',
                processData: true,
                headers: {
                    "Authorization":'<% =authKey %>',
                    "X-User-Id-1":'<% =userProfileID_Encrypted %>',
                },
                success: function (result) {                    
                    $("#lblSubject").html('');
                    $("#lblDesc").html('');
                    $("#lblMonth").html('');
                    $("#lblYear").html('');
                    $("#lblEffectiveDate").html('');
                    $("#LinkDocument").html('');
                    if (result != null) {
                        $("#lblSubject").html(result[0]._Subject);
                        $("#lblDesc").html(result[0]._Description);
                        $("#lblMonth").html(result[0]._Month).toString();
                        $("#lblYear").html(result[0]._Year).toString();
                        $("#lblEffectiveDate").html(result[0]._EffectiveDate).toString();                        
                        if (result[0]._DocumentPath != "") {
                           
                            var receivedpath = '<%=rlcs_API_url%>'+'Masters/Generate?FilePath=' + result[0]._DocumentPath;                            
                            var newPath = receivedpath.replace('192.168.0.2','tlstack');

                            //alert(receivedpath);
                            //alert(newPath);

                            $('[id$=lblUpdateDocumentPath]').click(function() {
                                downloadRegulatoryUpdateDocument(newPath);
                            });
                          
                           // $('[id$=lblUpdateDocumentPath]').attr('href', newPath);
                            $("#trDocumentPath").show();
                        } else {
                            $("#trDocumentPath").hide();
                        }                      
                    }
                },
                error: function (e, t) {}
            });
            return false;
        }

        function downloadRegulatoryUpdateDocument(path) {            
            //$('#dwnfile').attr('src', path);
            $("#loaderUpdates").show();
            //$('.loadingfile').show();
            $.ajax({
                url: path,
                dataType: "jsonp",
                timeout: 5000,

                success: function (response) {
                    $("#loaderUpdates").hide();
                    //$('.loadingfile').hide();
                },
                error: function (parsedjson) {
                    if (parsedjson != null && parsedjson.statusText != "success") {
                        $("#loaderUpdates").hide();
                        //$('.loadingfile').hide();
                        $('#NoFileFoundModal').modal('show');
                        setTimeout(function () { $('#NoFileFoundModal').modal('hide'); }, 3000);
                    } else {
                        $('#dwnfile').attr('src', path);
                        $("#loaderUpdates").hide();
                        //$('.loadingfile').hide();
                    }
                }
            });
        }

        function BindNewsLetterpopup(DID) {
            for (var i = 0; i < NewsLetterJson.length; i++) {
                if (DID == NewsLetterJson[i].ID) {

                    $("#newsImg").attr('src', '../Images/' + NewsLetterJson[i].FileName);
                    $("#newsTitle").html(NewsLetterJson[i].Title);
                    $("#newsDesc").html(NewsLetterJson[i].Description);

                    break;
                }
            }
        }

        function forchild(hgt){
            $('#showdetails').attr('height',75 * (hgt));
        }
       
        $("#High").spectrum({
            color:"<%=highcolor.Value %>",
            showInput: true,
            className: "full-spectrum",
            showInitial: true,
            showPalette: true,
            showSelectionPalette: true,
            maxSelectionSize: 10,
            preferredFormat: "hex",
            containerClassName: "highcls",
            move: function (color) {

            },
            show: function () {

            },
            beforeShow: function () {

            },
            hide: function () {

            },
            change: function () {
                 
            },
            palette: [
                ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        });

            $("#medium").spectrum({
                color: "<%=mediumcolor.Value %>",
                showInput: true,
                className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                maxSelectionSize: 10,
                preferredFormat: "hex",
                containerClassName: "mediumcls",
                move: function (color) {

                },
                show: function () {

                },
                beforeShow: function () {

                },
                hide: function () {

                },
                change: function () {
            
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });

            var lowchange = false;
            $("#low").spectrum({
                color:"<%=lowcolor.Value %>",
                showInput: true,
                className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                maxSelectionSize: 10,
                preferredFormat: "hex",
                containerClassName:"lowcls",
                move: function (color) {
              
                },
                show: function () {
            
                },
                beforeShow: function () {
              
                },
                hide: function () {
            
                },
                change: function () {
               
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });

            var mediumbtn = $(".mediumcls").find('button');
            $(mediumbtn).click(function () {            
                var inputclr = $(".mediumcls").find('input.sp-input');
                if( $('#ContentPlaceHolder1_mediumcolor').val()!=$(inputclr).val()){
                    upcolor();
                    $('#ContentPlaceHolder1_mediumcolor').val($(inputclr).val());
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }
            });

            var highbtn = $(".highcls").find('button');
            $(highbtn).click(function () {
                var inputclr1 = $(".highcls").find('input.sp-input');
          
         
                if( $('#ContentPlaceHolder1_highcolor').val()!=$(inputclr1).val()){
                    $('#ContentPlaceHolder1_highcolor').val($(inputclr1).val());
                    upcolor();
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }    
            });

            var lowbtn=$(".lowcls").find('button');
            $(lowbtn).click(function () {         
                var inputclr2 = $(".lowcls").find('input.sp-input');
        
                if($('#ContentPlaceHolder1_lowcolor').val()!=$(inputclr2).val())
                {   
                    $('#ContentPlaceHolder1_lowcolor').val($(inputclr2).val());
                    upcolor();
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }           
            });

            function upcolor() {            
                var k = 0;
                $.ajax({
                    async: true,
                    type: 'Post',
                    url: '/dailyupdateservice.svc/upcolor',                       
                    data: JSON.stringify({"high": $('#ContentPlaceHolder1_highcolor').val(),"sender": <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>, "medium":  $('#ContentPlaceHolder1_mediumcolor').val(), "low":  $('#ContentPlaceHolder1_lowcolor').val()}),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function (result) {
                    
                    },
                    error: function (e, t) { }
                });
            }
    
            function initializeJQueryUI(textBoxID, divID) {
                $("#" + textBoxID).unbind('click');

                $("#" + textBoxID).click(function () {
                    $("#" + divID).toggle("blind", null, 500, function () { });
                });
            }         
 
            (function(a){a.createModal=function(b){defaults={title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false};var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";html='<div class="modal fade" id="myModal">';html+='<div class="modal-dialog"  style="width:1000px;">';html+='<div class="modal-content" style="width:1100px;">';html+='<div class="modal-header">';html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';if(b.title.length>0){html+='<h4 class="modal-title">'+b.title+"</h4>"}html+="</div>";html+='<div class="modal-body" '+c+">";html+=b.message;html+="</div>";html+='<div class="modal-footer">';if(b.closeButton===true){html+='<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>'}html+="</div>";html+="</div>";html+="</div>";html+="</div>";a("body").prepend(html);a("#myModal").modal().on("hidden.bs.modal",function(){a(this).remove()})}})(jQuery);
    
            /*
            * Here is how you use it
            */
 
            function viewpdf(obj)  {         
                var pdf_link = $(obj).attr('data-href');
                var pdf_title = $(obj).attr('data-title');          
                var iframe = '<object type="application/pdf" data="' + pdf_link + '"#toolbar=1&navpanes=0&statusbar=0&messages=0 width="100%" height="500">No Support</object>'
                $.createModal({
                    title: pdf_title,
                    message: iframe,
                    closeButton:true,
                    scrollable: false
                });
                return false;        
            } 

            function openNotificationModal() { 
                if (sessionStorage.getItem("Notify") == null) {
                    $('#divNotification').modal('show');
                    SetNotifyKey();
                    return true;
                }            
            }       

            function BindNewNotifications() {
                var k = 0;
                $.ajax({
                    async: true,
                    type: 'Get',
                    url: '/dailyupdateservice.svc/BindNotifications?userid=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>',
                    content: 'application/json;charset=utf-8',
                    processData: true,
                    success: function (result) {                    
                        var step = 0;
                        var str = '';
                        for (step = 0; step < result.length; step++) {
                        
                            str += '<li><p><a href="../DailyUpdates/Notification.aspx">' + result[step] + '</a></p></li>'
                        }
                        $("#divNotificationData").html(str);
                   
                    },
                    error: function (e, t) { }               
                });    
                
            }

            $(document).ready(function () {            
                $.get("../RLCS/MyComplianceCalendar.aspx?m=8&type=0", function(data){
                    $(".responsive-calendar").html(data);
                });        
                setInterval(setcolor, 1000);
            });

            var strop=0;

            $('#ContentPlaceHolder1_rdbcalender').change(function () {
                //$(this).click(function () {
                var val=  $('#ContentPlaceHolder1_rdbcalender input:checked').val()
                if(strop==0)                        
                    fcallcal(val);
                //});
            });

            function fcallcal(val){                
                strop=1;
                $(".responsive-calendar").html('<div class="chart-loading" id="idloadercal4"></div>');
                //$(".responsive-calendar").html('<img src="../images/processing.gif">');
                $.get("../RLCS/MyComplianceCalendar.aspx?m=8&type="+val, function(data){
                    $(".responsive-calendar").html(data);
                    strop=0;
                });     
            }

            function setcolor() {
                $('.overdue').closest('div').find('a').css('background-color', '#FF0000');
                $('.complete').closest('div').find('a').css('background-color', '#006500');
                $('.pending').closest('div').find('a').css('background-color', '#00008d');
                $('.delayed').closest('div').find('a').css('background-color', '#ffcd70');                
            }

            $(document).ready(function (ClassName) {
                $("a").addClass(ClassName);
            });

            function formatDate(date) {
                var monthNames = [
                  "January", "February", "March",
                  "April", "May", "June", "July",
                  "August", "September", "October",
                  "November", "December"
                ];
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();
                return day + ' ' + monthNames[monthIndex] + ' ' + year;
            }

            function fclosepopcal(dt) {
                $('#divreports').modal('hide');
                fcal(dt)
            }

            function hideloader() {
                $('#imgcaldate').hide();
                $('#imgcaldate1').hide();
            }

            function fcal(dt) {
                try {                
                    var ddata = new Date(dt);
                    $('#clsdatel').html('Compliance items for date ' + formatDate(ddata));
                    $('#clsdatel1').html('Compliance items for date ' + formatDate(ddata));
                } catch (e) { }

                //$('#imgcaldate').show();
                $('#idloadercal').show();
                var valtype=  $('#ContentPlaceHolder1_rdbcalender input:checked').val()
                $('#calframe').attr('src', '../RLCS/RLCS_MyCalendarData.aspx?m=8&date=' + dt+'&type='+valtype)

                //$('#imgcaldate1').show();
                $('#idloadercal1').show();
                var valtype=  $('#rbcalendar1 input:checked').val()
                $('#calframe1').attr('src', '../RLCS/RLCS_MyCalendarData.aspx?m=8&date=' + dt+'&type='+valtype)

                return;               
            }

            function OpenOverViewpup(scheduledonid, instanceid, CType) {
                if (CType == 'Statutory' || CType == 'Statutory CheckList' || CType == 'Event Based') {
                    $('#divOverView').modal('show');
                    $('#OverViews').attr('width', '1150px');
                    $('#OverViews').attr('height', '500px');
                    $('.modal-dialog').css('width', '1200px');
                    $('#OverViews').attr('src', "../RLCS/RLCS_ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }
                else 
                {
                    $('#divOverView').modal('show');
                    $('#OverViews').attr('width', '1050px');
                    $('#OverViews').attr('height', '600px');
                    $('.modal-dialog').css('width', '1100px');
                    $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }
            }

            function OpenPerrevpopup(scheduledonid, instanceid) {
                $('#divreports').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '650px');
                $('.modal-dialog').css('width', '85%');
                $('#showdetails').attr('src', "/RLCS/RLCS_ComplianceStatusTransaction.aspx?instanceID=" + instanceid + "&scheduleOnID=" + scheduledonid);                
            }

            function OpenOverdueComplainceList(customerid,UserID,isapprover){                                   
                var myWindowAdv = $("#divreportOVerdue");
                var IsAvantisFlag=true;

                function onClose() {
                    $(this.element).empty();
                }

                function onOpen(e) {
                    kendo.ui.progress(e.sender.element, true);
                }

                myWindowAdv.kendoWindow({
                    width: "90%",
                    height: "75%",
                    title: "Summary of Overdue Compliances",
                    content:"../RLCS/RLCS_OverdueCompliances.aspx?IsAvantisFlag="+IsAvantisFlag,
                    iframe:"true",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose,
                    open: onOpen,
                });

                myWindowAdv.data("kendoWindow").center().open();
                //$('#idloader9').show();
               // $('#showOverdueDetails').attr('src', "../RLCS/RLCS_OverdueCompliances.aspx");
            }
            
            function OpenInternalOverdueComplainceList(customerid,UserID,isapprover){
                $('#divreports').modal('show');    
                $('#showdetails').attr('width','1150px'); 
                $('#showdetails').attr('height','580px'); 
                $('.modal-dialog').css('width','1200px');    
                $('#showdetails').attr('src', "../Management/InternalOverdueCompliance.aspx");
            }

            function SetNotifyKey() {
                sessionStorage.setItem("Notify", "1");
            }

            //BindNewNotifications();

            $("#DivFilters").click(function (event) { 
                if (event.target.id == "") {
                    var idvid = $(event.target).closest('div');
                    if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                        $("#divFilterLocation").show();
                    } else {
                        $("#divFilterLocation").hide();
                    }
                }
                else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                    $("#divFilterLocation").hide();
                } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                    $("#divFilterLocation").show();
                } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
                    $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

                    $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                        $("#divFilterLocation").toggle("blind", null, 500, function () { });
                    });
                }
            }); 
    </script>

</asp:Content>
