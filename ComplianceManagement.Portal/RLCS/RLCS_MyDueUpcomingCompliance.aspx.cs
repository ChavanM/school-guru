﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.IO;
using System.Globalization;
using OfficeOpenXml.Style;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSLocation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyDueUpcomingCompliance : System.Web.UI.Page
    {
        protected static int customerid;
        protected static bool IsApprover = false;
        protected static int BID;
        protected static string filterType;
        public static List<long> SelectedBranchList = new List<long>();
        protected string month;
        protected int year;
        public RLCS_LocationDetails rlcslocationdetails = new RLCS_LocationDetails();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerid, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
                        userAssignedBranchList = rlcslocationdetails.GetuserAssignedBranchList(userAssignedBranchList);
                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                //List<int> assignedbranchIDs = new List<int>();
                                //assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                                //BindLocationFilter(assignedbranchIDs);
                                BindActList(SelectedBranchList);
                                if (!string.IsNullOrEmpty(Request.QueryString["filterType"]))
                                {
                                    filterType = Request.QueryString["filterType"];
                                    var ftype = string.Empty;
                                    if (filterType == "Upcoming Compliances")
                                    {
                                        ftype = "Upcoming";
                                    }
                                    else if (filterType == "Overdue Compliances")
                                    {
                                        ftype = "Overdue";
                                    }
                                    else if (filterType == "Due Today Compliances")
                                    {
                                        ftype = "DueToday";
                                    }
                                    else if (filterType == "Pending Review Compliances")
                                    {
                                        ftype = "Pending";
                                    }

                                    GetCompliancesSummaryCounts(ftype);
                                }
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    SelectedBranchList.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false 
                                                && row.CustomerID == customerid
                                                && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                SelectedBranchList.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        private void BindActList(List<long> SelectedBranchList)
        {
            try
            {
                int CategoryID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                var ActList = ActManagement.GetAllAssignedActs_RLCS(customerid, AuthenticationHelper.UserID, CategoryID, AuthenticationHelper.ProfileID, SelectedBranchList,false);//0
                ddlAct.Items.Clear();
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                //var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                var bracnhes = RLCSManagement.GetAll_BranchHierarchy(customerID);
                tvFilterLocation.Nodes.Clear();
                TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void GetCompliancesSummaryCounts(string filterType)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)
                      .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.RoleID == 3)).ToList();

                   // MastertransactionsQuery = MastertransactionsQuery.Where(row => row.GenerateSchedule == false && row.IsAvantis == false).ToList();

                    var lstRecords = RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, filterType);
                    lstRecords = rlcslocationdetails.GetuserAssignedBranchList(lstRecords);
                    List<int> assignedbranchIDs = new List<int>();
                    assignedbranchIDs = lstRecords.Select(row => row.CustomerBranchID).Distinct().ToList();
                    BindLocationFilter(assignedbranchIDs);
                    GridStatutory.DataSource = lstRecords;
                    GridStatutory.DataBind();
                    Session["TotalRows"] = lstRecords.Count;
                    UpDetailView.Update();
                    GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindDetailView()
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;

                if (tvFilterLocation.SelectedValue != "-1")
                {
                    //ddlAct.ClearSelection();
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    SelectedBranchList.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    SelectedBranchList.ToList();
                }
                else
                {
                    SelectedBranchList.Clear();
                }

                //if (ddlAct.SelectedValue == "-1" || ddlAct.SelectedValue == "")
                //{
                //    BindActList(Branchlist);
                //}                

                int CustomerBranchId = -1;
                int ActId = -1;
                int CategoryID = -1;
                int UserID = -1;

                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UserID"]))
                {
                    UserID = Convert.ToInt32(Request.QueryString["UserID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ActID"]))
                {
                    ActId = Convert.ToInt32(Request.QueryString["ActID"]);

                    if (ActId != 0)
                        ddlAct.SelectedValue = Request.QueryString["ActID"];//Convert.ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Branchid"]))
                {
                    CustomerBranchId = Convert.ToInt32(Request.QueryString["Branchid"]);
                    //tvFilterLocation_SelectedNodeChanged(null, null);
                    foreach (TreeNode node in tvFilterLocation.Nodes)
                    {
                        if (node.ChildNodes.Count > 0)
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                if (child.Value == Convert.ToString(CustomerBranchId))
                                {
                                    child.Selected = true;
                                }
                            }
                        }
                        else if (node.Value == Convert.ToString(CustomerBranchId))
                        {
                            node.Selected = true;
                        }
                    }
                }
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    int CustomerBranchlength = tvFilterLocation.SelectedNode.Text.Length;
                    CustomerBranchId = Convert.ToInt32(tvFilterLocation.SelectedValue);

                    if (CustomerBranchlength >= 20)
                        lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text;
                }
                if (ddlAct.SelectedValue != "-1" && ddlAct.SelectedValue != "")
                {
                    int actlength = ddlAct.SelectedItem.Text.Length;

                    if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                        ActId = Convert.ToInt32(ddlAct.SelectedValue);

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (actlength >= 100)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 50) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (actlength >= 100)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 50) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }
                if (!string.IsNullOrEmpty(ddlmonth.SelectedValue) && ddlmonth.SelectedValue != "-1")
                {
                    month = ddlmonth.SelectedValue;
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Month: </b>" + ddlmonth.SelectedItem.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Month: </b>" + ddlmonth.SelectedItem.Text;
                    }
                }
                if (!string.IsNullOrEmpty(ddlyear.SelectedValue) && ddlyear.SelectedValue != "-1")
                {
                    year = Convert.ToInt32(ddlyear.SelectedValue);
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Year: </b>" + ddlyear.SelectedItem.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Year: </b>" + ddlyear.SelectedItem.Text;
                    }
                }
                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                }
                else
                {
                    divAdvSearch.Visible = false;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    string role = AuthenticationHelper.Role;
                    ddlAct.Visible = true;

                    //if ((AuthenticationHelper.Role == "HMGMT") || (AuthenticationHelper.Role == "LSPOC") ||
                    //        (AuthenticationHelper.Role == "HMGR") || (AuthenticationHelper.Role == "HAPPR") || (AuthenticationHelper.Role == "HEXCT"))
                    //{
                    role = "RLCS";
                    var MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)
                   .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.RoleID == 3)).ToList();
                    MastertransactionsQuery = rlcslocationdetails.GetuserAssignedBranchList(MastertransactionsQuery);
                    var ftype = string.Empty;
                    if (filterType == "Upcoming Compliances")
                    {
                        ftype = "Upcoming";
                    }
                    else if (filterType == "Overdue Compliances")
                    {
                        ftype = "Overdue";
                    }
                    else if (filterType == "Due Today Compliances")
                    {
                        ftype = "DueToday";
                    }
                    if (ActId != -1)
                    {
                        MastertransactionsQuery = MastertransactionsQuery.Where(x => x.ActID == ActId).ToList();
                    }
                    var detailsview = RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, ftype);
                    detailsview = rlcslocationdetails.GetuserAssignedBranchList(detailsview);
                    if (detailsview.Count > 0)
                    {
                        if (SelectedBranchList.Count > 0)
                        {
                            detailsview = detailsview.Where(x => SelectedBranchList.Contains(x.CustomerBranchID)).ToList();
                        }
                        else
                        {
                            if (CustomerBranchId > 0)
                            {
                                detailsview = detailsview.Where(x => x.CustomerBranchID.Equals(CustomerBranchId)).ToList();
                            }
                        }
                        if (month != null && month != "-1")
                        {
                            detailsview = detailsview.Where(x => x.RLCS_PayrollMonth == month).ToList();
                        }
                        if (year != 0 && year != -1)
                        {
                            detailsview = detailsview.Where(x => x.RLCS_PayrollYear == Convert.ToString(year)).ToList();
                        }
                    }
                    Session["TotalRows"] = detailsview.Count;

                    GridStatutory.DataSource = detailsview;
                    GridStatutory.DataBind();
                    GridStatutory.Visible = true;
                    //}
                }
                UpDetailView.Update();
                GetPageDisplaySummary();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "Script1", "HideLoaderDiv();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "Script1", "HideLoaderDiv();", true);
            }
        }
        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode node = tvFilterLocation.Nodes[0];
                node.Selected = true;
                ddlAct.ClearSelection();
                ddlmonth.ClearSelection();
                ddlyear.ClearSelection();
                lblAdvanceSearchScrum.Text = String.Empty;
                divAdvSearch.Visible = false;
                tvFilterLocation_SelectedNodeChanged(sender, e);
                var nvc = HttpUtility.ParseQueryString(Request.Url.Query);
                nvc.Remove("customerid");
                nvc.Remove("Category");
                nvc.Remove("ActID");
                nvc.Remove("Branchid");
                nvc.Remove("UserID");
                nvc.Remove("Internalsatutory");
                string url = Request.Url.AbsolutePath + "?" + nvc.ToString();
                Response.Redirect("~/RLCS/RLCS_MyDueUpcomingCompliance.aspx" + "?" + nvc.ToString(), false);
                Context.ApplicationInstance.CompleteRequest();

                //Rebind Grid
                //BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                lblAdvanceSearchScrum.Text = String.Empty;
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }
                GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //Reload the Grid
                BindDetailView();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;
                if (Convert.ToString(Session["TotalRows"]) != null && Convert.ToString(Session["TotalRows"]) != "")
                {
                    lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
                    lTotalCount.Text = GetTotalPagesCount().ToString();

                    if (lTotalCount.Text != "0")
                    {
                        if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                        {
                            SelectedPageNo.Text = "1";
                            lblStartRecord.Text = "1";

                            if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                                lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                            else
                                lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                        }
                    }
                    else if (lTotalCount.Text == "0")
                    {
                        SelectedPageNo.Text = "0";
                        DivRecordsScrum.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";
                GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";
                GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;

                if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                {
                    ddlAct.ClearSelection();
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    SelectedBranchList.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    SelectedBranchList.ToList();
                }
                else
                {
                    SelectedBranchList.Clear();
                }

                if (ddlAct.SelectedValue == "-1" || ddlAct.SelectedValue == "")
                {
                    BindActList(SelectedBranchList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    lblAdvanceSearchScrum.Text = String.Empty;
                    if (tvFilterLocation.SelectedValue != "-1")
                    {
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        SelectedBranchList.Clear();
                        GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        SelectedBranchList.ToList();
                    }
                    else
                    {
                        SelectedBranchList.Clear();
                    }
                    int CustomerBranchId = -1;
                    int ActId = -1;
                    int UserID = -1;
                    if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                    {
                        customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["UserID"]))
                    {
                        UserID = Convert.ToInt32(Request.QueryString["UserID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["ActID"]))
                    {
                        ActId = Convert.ToInt32(Request.QueryString["ActID"]);

                        if (ActId != 0)
                            ddlAct.SelectedValue = Request.QueryString["ActID"];//Convert.ToString();
                    }
                    if (ddlAct.SelectedValue != "-1" && ddlAct.SelectedValue != "")
                    {
                        ActId = Convert.ToInt32(ddlAct.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                    {
                        IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                    }

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        entities.Database.CommandTimeout = 180;
                        string role = AuthenticationHelper.Role;
                        ddlAct.Visible = true;


                        role = "RLCS";
                        var MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)
                       .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.RoleID == 3)).ToList();
                        MastertransactionsQuery = rlcslocationdetails.GetuserAssignedBranchList(MastertransactionsQuery);
                        MastertransactionsQuery = MastertransactionsQuery.Where(row => row.GenerateSchedule == false).ToList();

                        var ftype = string.Empty;
                        if (filterType == "Upcoming Compliances")
                        {
                            ftype = "Upcoming";
                        }
                        else if (filterType == "Overdue Compliances")
                        {
                            ftype = "Overdue";
                        }
                        else if (filterType == "Due Today Compliances")
                        {
                            ftype = "DueToday";
                        }
                        else if (filterType == "Pending Review Compliances")
                        {
                            ftype = "Pending";
                        }

                        if (ActId != -1)
                        {
                            MastertransactionsQuery = MastertransactionsQuery.Where(x => x.ActID == ActId).ToList();
                        }
                        var detailsview = RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, ftype);

                        if (detailsview.Count > 0)
                        {

                            if (SelectedBranchList.Count > 0)
                            {
                                detailsview = detailsview.Where(x => SelectedBranchList.Contains(x.CustomerBranchID)).ToList();
                            }
                            else
                            {
                                if (CustomerBranchId > 0)
                                {
                                    detailsview = detailsview.Where(x => x.CustomerBranchID.Equals(CustomerBranchId)).ToList();
                                }
                            }
                        }


                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add(ftype);
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView(detailsview.ToDataTable());
                        ExcelData = view.ToTable("Selected", false, "CustomerBranchName", "ShortForm", "RequiredForms", "Frequency", "ScheduledOn");

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Name:";
                        exWorkSheet.Cells["B2:C2"].Merge = true;
                        exWorkSheet.Cells["B2"].Value = "Report of " + ftype + " Compliances";

                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report Generated On:";
                        exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);

                        exWorkSheet.Cells["A5"].Value = "Entity / Branch";
                        exWorkSheet.Cells["B5"].Value = "Compliance";
                        exWorkSheet.Cells["C5"].Value = "Form";
                        exWorkSheet.Cells["D5"].Value = "Frequency";
                        exWorkSheet.Cells["E5"].Value = "Due Date";


                        //exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        //exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                        //exWorkSheet.Cells["B1:C1"].Merge = true;
                        //exWorkSheet.Cells["B1"].Value = LocationName;

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Name:";
                        exWorkSheet.Cells["B2:C2"].Merge = true;


                        exWorkSheet.Cells["B2"].Value = "Report of Compliances";
                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report Generated On:";
                        exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);


                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A5"].Value = "Entity / Branch";
                        exWorkSheet.Cells["A5"].AutoFitColumns(20);

                        exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B5"].Value = "Compliance";
                        exWorkSheet.Cells["B5"].AutoFitColumns(60);

                        exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C5"].Value = "Form";
                        exWorkSheet.Cells["C5"].AutoFitColumns(15);

                        exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D5"].Value = "Frequency";
                        exWorkSheet.Cells["D5"].AutoFitColumns(10);

                        exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E5"].Value = "Due Date";
                        exWorkSheet.Cells["E5"].AutoFitColumns(12);

                        using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.Numberformat.Format = "dd/MMM/yyyy";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.AutoFitColumns();

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        string filename = ftype + "Report.xlsx";
                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                    


                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}