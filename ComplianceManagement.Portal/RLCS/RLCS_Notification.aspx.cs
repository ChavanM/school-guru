﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_Notification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindNotifications();
                btnMarkasRead.Visible = false;
            }
        }

        protected void btnMarkasRead_Click(object sender, EventArgs e)
        {
            try
            {
                CheckBoxValueSaved();

                if (ViewState["checkedNotifications"] != null)
                {
                    List<long> NotificationsDetails = (List<long>)ViewState["checkedNotifications"];

                    if (NotificationsDetails != null)
                    {
                        Business.ComplianceManagement.SetReadToNotification(NotificationsDetails,AuthenticationHelper.UserID);
                        ViewState["checkedNotifications"] = null;
                    }
                }

                BindNotifications();
                ShowSelectedRecords(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            { 
                if (ViewState["NotificationID"] != null)
                {
                    List<long> NotificationsDetails = new List<long>();

                    NotificationsDetails.Add(Convert.ToInt32(ViewState["NotificationID"]));

                    if (NotificationsDetails != null)
                    {
                        Business.ComplianceManagement.SetReadToNotification(NotificationsDetails, AuthenticationHelper.UserID);
                        ViewState["NotificationID"] = null;
                    }
                }

                BindNotifications();
                ShowSelectedRecords(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        private void BindNotifications()
        {
            try
            {
                List<Business.Data.SP_GetUserNotificationsByUserID_Result> Notifications = new List<Business.Data.SP_GetUserNotificationsByUserID_Result>();

                if (ddlNotificationType.SelectedValue == "-1")
                    Notifications = Business.ComplianceManagement.GetAllUserNotification(AuthenticationHelper.UserID).OrderBy(Entry => Entry.IsRead).ThenByDescending(entry => entry.UpdatedOn).ToList();
                else
                    Notifications = Business.ComplianceManagement.GetAllUserNotification(AuthenticationHelper.UserID).Where(Entry => Entry.IsRead == Convert.ToBoolean(Convert.ToInt32(ddlNotificationType.SelectedValue))).OrderByDescending(entry => entry.UpdatedOn).ToList();

                Session["TotalRows"] = Notifications.Count;
                GridNotifications.DataSource = Notifications;
                GridNotifications.DataBind();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void CheckBoxValueSaved()
        {
            List<long> NotificationList = new List<long>();

            int index = -1;
            foreach (GridViewRow gvrow in GridNotifications.Rows)
            {
                index = Convert.ToInt32(GridNotifications.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;
                                
                if (ViewState["checkedNotifications"] != null)
                    NotificationList = (List<long>)ViewState["checkedNotifications"];

                if (result)
                {
                    if (!NotificationList.Contains(index))
                        NotificationList.Add(index);
                }
                else
                    NotificationList.Remove(index);
            }
            if (NotificationList != null && NotificationList.Count > 0)
                ViewState["checkedNotifications"] = NotificationList;
        }

        private void AssignCheckBoxexValue()
        {
            List<long> NotificationsDetails = (List<long>)ViewState["checkedNotifications"];

            if (NotificationsDetails != null && NotificationsDetails.Count > 0)
            {
                foreach (GridViewRow gvrow in GridNotifications.Rows)
                {
                    int index = Convert.ToInt32(GridNotifications.DataKeys[gvrow.RowIndex].Value);

                    if (NotificationsDetails.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                        myCheckBox.Checked = true;
                    }
                }

                if (NotificationsDetails.Count > 0)
                {
                    lblTotalSelected.Text = NotificationsDetails.Count + " Selected";
                    btnMarkasRead.Visible = true;
                }
                else if (NotificationsDetails.Count == 0)
                {
                    lblTotalSelected.Text = "";
                    btnMarkasRead.Visible = false;
                }
            }

            else
            {
                lblTotalSelected.Text = "";
                btnMarkasRead.Visible = false;
            }
        }

        protected void chkCompliancesHeader_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox)GridNotifications.HeaderRow.FindControl("chkCompliancesHeader");

            foreach (GridViewRow row in GridNotifications.Rows)
            {
                CheckBox chkCompliances = (CheckBox)row.FindControl("chkCompliances");

                if (ChkBoxHeader.Checked)
                    chkCompliances.Checked = true;
                else
                    chkCompliances.Checked = false;
            }

            ShowSelectedRecords(sender, e);
        }

        protected void chkCompliances_CheckedChanged(object sender, EventArgs e)
        {
            int Count = 0;

            CheckBox ChkBoxHeader = (CheckBox)GridNotifications.HeaderRow.FindControl("chkCompliancesHeader");

            foreach (GridViewRow row in GridNotifications.Rows)
            {
                CheckBox chkCompliances = (CheckBox)row.FindControl("chkCompliances");

                if (chkCompliances.Checked)
                    Count++;
            }

            if (Count == GridNotifications.Rows.Count)
                ChkBoxHeader.Checked = true;
            else
                ChkBoxHeader.Checked = false;

            ShowSelectedRecords(sender, e);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }
               
                    GridNotifications.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridNotifications.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;               

                //Reload the Grid
                BindNotifications();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        protected void GridNotifications_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                String NotificationType = row.Cells[2].Text;

                if (e.CommandName == "ShowDetails")
                {
                    int NotificationID = Convert.ToInt32(e.CommandArgument);

                    lblNotificationMessage.Text = string.Empty;

                    ViewState["NotificationID"] = NotificationID;

                    var NotificationDetails = Business.NotificationManagement.GetNotificationRemarkByNotificationID(NotificationID);

                    if (NotificationDetails != null)
                    {
                        if (NotificationType != "ticket" && NotificationType != "Custom")
                        {
                            var ComplianceDetails = Business.ComplianceManagement.GetByID(Convert.ToInt32(NotificationDetails.ComplianceID));

                            if (ComplianceDetails != null)
                            {
                                lblComplianceID.Text = Convert.ToString(ComplianceDetails.ID);
                                lblComplianceDiscription.Text = ComplianceDetails.ShortDescription;
                                lblDetailedDiscription.Text = ComplianceDetails.Description;

                                lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(ComplianceDetails.Frequency != null ? (int)ComplianceDetails.Frequency : -1));

                                string risk = Business.ComplianceManagement.GetRiskType(ComplianceDetails,-1);
                                lblRiskType.Text = Business.ComplianceManagement.GetRisk(ComplianceDetails,-1);
                                lblRisk.Text = risk;

                                string Penalty = Business.ComplianceManagement.GetPanalty(ComplianceDetails);
                                lblPenalty.Text = Penalty;

                                var ActInfo = Business.ActManagement.GetByID(ComplianceDetails.ActID);

                                if (ActInfo != null)
                                {
                                    lblActName.Text = ActInfo.Name;
                                }

                                divRiskType.Attributes.Remove("style");

                                if (risk == "HIGH")
                                {
                                    divRiskType.Attributes.Add("style", "background-color:red;");
                                }
                                else if (risk == "MEDIUM")
                                {
                                    divRiskType.Attributes.Add("style", "background-color:yellow;");
                                }
                                else if (risk == "LOW")
                                {
                                    divRiskType.Attributes.Add("style", "background-color:green;");
                                }

                                lblRule.Text = ComplianceDetails.Sections;
                                lblNotificationMessage.Text = NotificationDetails.Remark;

                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                                upNotification.Update();
                            }
                        }

                        else
                        {
                            lblTicketNotificationMessage.Text = NotificationDetails.Remark;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openTicketModal();", true);

                            upTicketNotification.Update();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //protected void GridNotifications_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "ShowDetails")
        //        {
        //            int NotificationID = Convert.ToInt32(e.CommandArgument);

        //            lblNotificationMessage.Text = string.Empty; 

        //            ViewState["NotificationID"] = NotificationID;

        //             var NotificationDetails=Business.NotificationManagement.GetNotificationRemarkByNotificationID(NotificationID);

        //            if (NotificationDetails != null)
        //            {
        //                var ComplianceDetails = Business.ComplianceManagement.GetByID(Convert.ToInt32(NotificationDetails.ComplianceID));

        //                lblNotificationMessage.Text = NotificationDetails.Remark;

        //                if (ComplianceDetails != null)
        //                    lblShorDescription.Text = ComplianceDetails.ShortDescription;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
        //    }
        //}

        protected String GetComplianceName(long CompID)
        {
            Business.Data.Compliance ComplianceDetails = Business.ComplianceManagement.GetCompliance(CompID);

            if(ComplianceDetails!=null)
            {
                return ComplianceDetails.ShortDescription;
            }
            return String.Empty;
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }

                CheckBoxValueSaved();

                GridNotifications.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                GridNotifications.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindNotifications();

                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                CheckBoxValueSaved();

                GridNotifications.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                GridNotifications.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                BindNotifications();

                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void ShowSelectedRecords(object sender, EventArgs e)
        {
            lblTotalSelected.Text = "";

            CheckBoxValueSaved();
            AssignCheckBoxexValue();
        }

        protected void lnkShowDetails_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)(sender);

                int NotificationID = Convert.ToInt32(btn.CommandArgument);

                lblNotificationMessage.Text = string.Empty;

                ViewState["NotificationID"] = NotificationID;

                //lblNotificationMessage.Text = Business.NotificationManagement.GetNotificationRemarkByNotificationID(NotificationID);               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }
    }
}