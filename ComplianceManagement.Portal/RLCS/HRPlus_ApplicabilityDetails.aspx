﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlus_ApplicabilityDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_ApplicabilityDetails" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background:white">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <style type="text/css">

        .error_pink {
        background: #f8d7da;
        }

        .validation-error
{
 /*  background: #f8d7da;*/
    border-radius: 5px;
    font-size: 12px;
    padding: 5px;
    margin-top: 5px;
    font-weight: 300;

}
         th.narrow
{
    color: rgba(0, 0, 0, 0.5);
    font-weight: 600 !important;
    font-size: 15px; 
    background: #E9EAEA; 
}

        #VSPopup ul li{
            margin-left:12px;
        }
       .k-check-all{
              zoom:1.12;
              font-size:13px;
          }

     .container{
         background:white;
     }
       .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a{
           background:white !important;
       }

       .clearfix{
           background:white;
       }

       .col-md-12{
           background:white;
       }
            .k-label input[type="checkbox"] {
    zoom: 1.2;
    margin: 0px 5px 0 !important;
}
    </style>
    <style type="text/css">
          /*.k-popup.k-calendar-container, .k-popup.k-list-container {
    background-color: white;
    width: 159px;
}*/ 
          .k-treeview .k-item {
    display: block;
    border-width: 0;
    margin: 0;
    padding: 0 0 0 5px;
}
          .k-treeview .k-content, .k-treeview .k-item>.k-group, .k-treeview>.k-group {
    margin: 0px;
    padding: 0;
    background: 0 0;
    list-style-type: none;
    position: relative;
    margin-top: -5px;
}
          .k-list-container.k-popup-dropdowntree .k-check-all {
    margin: 10px 12.5px -4px;
}
          .k-treeview .k-i-collapse, .k-treeview .k-i-expand, .k-treeview .k-i-minus, .k-treeview .k-i-plus {
    margin-left: -13px;
    cursor: pointer;
}
           .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
    cursor: pointer;
    background-color: transparent;
    border-color: transparent;
    color:#1fd9e1;
     }
    .k-checkbox-label:hover{
            color:#1fd9e1;
     }
          label.k-label:hover{
              color:#1fd9e1;
          }
             .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            
            /*box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);*/
            box-shadow:none;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0px -3px;
            margin-top: 1px;
            line-height: normal;
        }
        .k-calendar-container {
    background-color: white;
    width: 217px;
  }
      
        .k-picker-wrap{
            height:34px;
        }
        .k-autocomplete .k-input, .k-dropdown-wrap .k-input, .k-multiselect-wrap .k-input, .k-numeric-wrap .k-input, .k-picker-wrap .k-input, .k-selectbox .k-input, .k-textbox > input{
            padding-bottom:6px;
            padding-top:5px;
            text-align:left;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }
        .k-calendar td.k-state-selected .k-link, .k-calendar td.k-today.k-state-selected.k-state-hover .k-link {
            color: #515967;
        }
       .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: auto !important;
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            margin-left:0px;
            
        }
          .k-autocomplete > .k-i-close, .k-dropdown-wrap > .k-i-close {
              top: 30%;
          }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
         .k-tooltip-content{
         width: max-content;
        
       }
          .k-tooltip{
            margin-top:2px;
             background-color:#e9e9e9 !important;
        }
          
        .k-checkbox-label {
            display: inline;
            padding-left:23.2px;
           
        }

        .table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th{
            border:0.5px solid lightgray;
        }

       
        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 1px 1px 0px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 19px !important;
            min-height: 20px;
            background-color:transparent;
           border-color:transparent;
             margin-left: 3px;
            margin-right: 10px;
            padding-left: 0px;
            padding-right: 0px;
            color:black;
            border-radius:35px;

        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }
      .k-list-container {
    background-color: white;
    width: auto !important;
}
          .k-list-filter > .k-icon {
              top: 32%;
          }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
            color:rgba(0,0,0,0.5);
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }
         .k-calendar .k-alt, .k-calendar th, .k-dropzone-hovered, .k-footer-template td, .k-grid-footer, .k-group, .k-group-footer td, .k-grouping-header, .k-pager-wrap, .k-popup, .k-toolbar, .k-widget .k-status
        {
            width:auto;
        }
        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 1px 0px 0px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: 0px;
            border-width:0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height:34px;
        }
        .k-multiselect-wrap, .k-floatwrap{
            height:34px;
            
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
            overflow:auto;
        }
        .k-list-container.k-popup-dropdowntree .k-treeview{
            overflow:initial;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }
       
        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.1%;
        }
          .k-icon, .k-tool-icon{
              margin-top:5px;
          }
          .k-i-excel {
             margin-top: -3px;
          }
          .k-i-more-vertical{
              margin-top:0px;
          }
           .k-dropdown .k-state-focused, .k-filebrowser-dropzone, .k-list > .k-state-hover, .k-mobile-list .k-edit-label.k-check:active, .k-mobile-list .k-item > .k-label:active, .k-mobile-list .k-item > .k-link:active, .k-mobile-list .k-recur-view .k-check:active, .k-pager-wrap .k-link:hover, .k-scheduler .k-scheduler-toolbar ul li.k-state-hover, .k-splitbar-horizontal-hover:hover, .k-splitbar-vertical-hover:hover, .k-state-hover, .k-state-hover:hover{
            color: #2e2e2e;
            background-color: #d8d6d6;
        }
           .btn{
              font-weight:400;
              font-size:14px;
          }
           .k-menu .k-item, .k-menu.k-header {
    border-color: #ceced2;
    background-color:#fff;
}
           /*.k-widget {
    background-color: #fff !important;
}*/
           div.k-grid-footer, div.k-grid-header {
    padding-right: 17px;
    border-bottom-style: solid;
    border-bottom-width: 0px;
    zoom: 1;
}
    #gridPricing > .k-grid-header, #treelist > .k-grid-header {
            padding-right: 0px !important;
        }

        #gridPricing > .k-grid-footer {
            padding-right: 0px !important;
        }
       #gridPricing > .k-grid-pager {
    margin-top: 0px;
    border-width: 1px 0px 0px 0px;
}
      #gridApplicableCompliance > .k-grid-pager{
          border-width:1px 0px 0px 0px;
      }
    /*.k-checkbox-label:before {
    font: 14px WebComponentsIcons,monospace;
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    border-width: 1px;
    border-style: solid;
    width: 16px;
    height: 16px;
    font-size: 14px;
    line-height: 14px;
    text-align: center;
}*/
    </style>

    <script type="text/javascript">
        function RedirectToEntityBranch(custID) {
            debugger;
            //alert("called");
            window.parent.GotoEntityBranchPage(custID);
        }

        function kendoCustomAlert(content, title) {
            $("<div></div>").kendoAlert({
                title: title,
                content: content
            }).data("kendoAlert").open();
        }

        function hideAlert() {
            $(".alert").alert('close');
        }

        kendo.data.DataSource.prototype.dataFiltered = function () {
            // Gets the filter from the dataSource
            var filters = this.filter();

            // Gets the full set of data from the data source
            var allData = this.data();

            // Applies the filter to the data
            var query = new kendo.data.Query(allData);

            // Returns the filtered data
            return query.filter(filters).data;
        }
    </script>

    <script>
        $(document).ready(function () {
            $("#location1").keyup(function () {

                if ($('#location11').text().length > 0) {

                    $("#location11").addClass("error_pink");

                }
            });
            $("#no_of_emp1").keyup(function () {

                if ($('#no_of_emp11').text().length > 0) {

                    $("#no_of_emp11").addClass("error_pink");

                }
            });

            $("#minbas1").keyup(function () {

                if ($('#minbas11').text().length > 0) {

                    $("#minbas11").addClass("error_pink");

                }
            });

            $("#mingross1").keyup(function () {

                if ($('#mingross11').text().length > 0) {

                    $("#mingross11").addClass("error_pink");

                }
            });

            $("#maxgross1").keyup(function () {
              
                if ($('#maxgross11').text().length > 0) {  

                    $("#maxgross11").addClass("error_pink");

                }
            });
            

             $("#btnClearFilter").hide();
            $("body").tooltip({ selector: '[data-toggle=tooltip]' });

            if ($(window.parent.document).find("#divShowApplicability") != null && $(window.parent.document).find("#divShowApplicability") != undefined)
                if ($(window.parent.document).find("#divShowApplicability").children().first().hasClass("k-loading-mask"))
                    $(window.parent.document).find("#divShowApplicability").children().first().hide();

            $('input[type="submit"]').click(function () { $('.k-loading-mask').find().show() });
        });

        function SetCount(selectedComplianceIDs) {
            if (selectedComplianceIDs != null && selectedComplianceIDs != undefined) {
                if (selectedComplianceIDs.length > 0) {
                    $("#btnSaveCompliance").show();
                    $("#lblSelectedComplianceCount").show();
                    $("#lblSelectedComplianceCount").text(selectedComplianceIDs.length + "-Compliance(s) Selected");
                } else {
                    $("#btnSaveCompliance").hide();
                    $("#lblSelectedComplianceCount").hide();
                    $("#lblSelectedComplianceCount").text('');
                }
            }
        }

        function onChange(arg) {
            alert(this.selectedKeyNames().join(", "));
            //  debugger;
            var selectedComplianceIDs = this.selectedKeyNames();
            SetCount(selectedComplianceIDs);
        }

        function LoadControls_ApplicabilityTab() {
            BindGrid_ApplicableCompliance();

            BindLocationFilter();

            Bind_ComplianceType();
            //Bind_Act();
            Bind_State();

            $("#exportGrid_ApplicableCompliance").click(function (e) {
                ExportToExcel_Applicability(e);
            });            
        }

        function ShowApplicabilityTab() {
            //LoadControls_ApplicabilityTab();
            document.getElementById('<%= lnkBtnApplicability.ClientID %>').click();
        }

        function ShowPricingTab() {
            //LoadControls_ApplicabilityTab();
            //document.getElementById('<%= lnkBtnPricing.ClientID %>').click();
            debugger;
            OnClientClick('butSubmit', 'HiddenField1', '3');

            //BindLocationFilter_Pricing();
            BindGrid_Pricing();

            $("#exportPricingGrid").click(function (e) {
                ExportToExcel_Pricing(e);
            });

            return true;
        }

        function ShowPaymentTab() {
            debugger;
            //$("#CustName").val($('#hdnCustName').val());
            //$("#CustMobile").val($('#hdnMobile').val());
            //$("#CustEmail").val($('#hdnEmail').val());
            //$("#txtAmount").val($('#hdnTotalPrice').val());
            //$("#TxnDate").val(new Date());

            $("#<%= txtPaymentMobile.ClientID %>").val($('#hdnMobile').val());
            $("#<%= txtPaymentEmail.ClientID %>").val($('#hdnEmail').val());
            $("#<%= txtPaymentAmount.ClientID %>").val($('#hdnTotalPrice').val());

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            var today = dd + '/' + mm + '/' + yyyy;

            $("#<%= txtPaymentDate.ClientID %>").val(today);
            //$("#TxnDate").val(today);
        }

        function ExportToExcel_Pricing(e) {
            debugger;
            e.preventDefault();
            if ($("#gridPricing").data("kendoGrid") != null && $("#gridPricing").data("kendoGrid") != undefined)
                kendo.ui.progress($("#gridPricing").data("kendoGrid").element, true);

            // trigger export of the products grid
            $("#gridPricing").data("kendoGrid").saveAsExcel();

            if ($("#gridPricing").data("kendoGrid") != null && $("#gridPricing").data("kendoGrid") != undefined)
                kendo.ui.progress($("#gridPricing").data("kendoGrid").element, false);
            return false;
        }

        function ExportToExcel_Applicability(e) {
            debugger;
            e.preventDefault();
            if ($("#gridApplicableCompliance").data("kendoGrid") != null && $("#gridApplicableCompliance").data("kendoGrid") != undefined)
                kendo.ui.progress($("#gridApplicableCompliance").data("kendoGrid").element, true);

            // trigger export of the products grid
            $("#gridApplicableCompliance").data("kendoGrid").saveAsExcel();

            if ($("#gridApplicableCompliance").data("kendoGrid") != null && $("#gridApplicableCompliance").data("kendoGrid") != undefined)
                kendo.ui.progress($("#gridApplicableCompliance").data("kendoGrid").element, false);
                       
            return false;
        }

        function setTotalPrice() {
            debugger;
            var total = $("#gridPricing").data().kendoGrid.dataSource.aggregates().Price;
            if (total != undefined && total != null) {
                $('input[name=hdnTotalPrice]').val(total.sum);
            }
        }

        function ApplyFilter_Pricing() {
            //Branch
            var selectedBranchIDs = $("#ddlEntityBranch_Pricing").data("kendoDropDownTree")._values;

            if (selectedBranchIDs.length > 0) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if (selectedBranchIDs.length > 0) {

                    var branchFilter = { logic: "or", filters: [] };

                    $.each(selectedBranchIDs, function (i, v) {
                        branchFilter.filters.push({
                            field: "BranchID", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(branchFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#gridPricing").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }

                if (GetGridRowCount_Pricing() > 0) {
                    $("#btnSavePricing").show();
                } else {
                    $("#btnSavePricing").hide();
                }
            }
        }

        function ApplyFilter() {
            //debugger;

            //Branch
            var selectedBranchIDs = $("#ddlEntityBranch").data("kendoDropDownTree")._values;

            //DocType           
            var selecteddocTypes = $("#ddlDocType").data("kendoDropDownTree")._values;

            //State
            var selectedStates = $("#ddlState").data("kendoDropDownTree")._values;

            //Act            
            //var selectedActs = $("#ddlAct").data("kendoDropDownTree")._values;

            //if (selectedBranchIDs.length > 0 || selecteddocTypes.length > 0 || selectedStates.length > 0 || selectedActs.length > 0) {
            if (selectedBranchIDs.length > 0 || selecteddocTypes.length > 0 || selectedStates.length > 0) {

                var finalSelectedfilter = { logic: "and", filters: [] };

                if (selectedBranchIDs.length > 0) {

                    var branchFilter = { logic: "or", filters: [] };

                    $.each(selectedBranchIDs, function (i, v) {
                        branchFilter.filters.push({
                            field: "BranchID", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(branchFilter);
                }

                if (selecteddocTypes.length > 0) {

                    var docTypeFilter = { logic: "or", filters: [] };

                    $.each(selecteddocTypes, function (i, v) {
                        docTypeFilter.filters.push({
                            //field: "ScopeID", operator: "eq", value: v
                            field: "ScopeName", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(docTypeFilter);
                }

                if (selectedStates.length > 0) {

                    var stateFilter = { logic: "or", filters: [] };

                    $.each(selectedStates, function (i, v) {
                        stateFilter.filters.push({
                            field: "StateID", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(stateFilter);
                }

                //if (selectedActs.length > 0) {

                //    var actFilter = { logic: "or", filters: [] };

                //    $.each(selectedActs, function (i, v) {
                //        actFilter.filters.push({
                //            field: "ActID", operator: "eq", value: v
                //        });
                //    });

                //    finalSelectedfilter.filters.push(actFilter);
                //}

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#gridApplicableCompliance").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                    $("#btnClearFilter").show();
                }
            } else {
                var dataSource = $("#gridApplicableCompliance").data("kendoGrid").dataSource;
                dataSource.filter({});
                $("#btnClearFilter").hide();
            }

            if (GetGridRowCount() > 0) {
                $("#btnSaveCompliance").show();
            } else {
                $("#btnSaveCompliance").hide();
            }
        }

        function ClearFilter(e) {
            e.preventDefault();
            //e.stopPropogation();
            //$("#ddlCustomers").data("kendoDropDownTree").value([]);
            $("#ddlEntityBranch").data("kendoDropDownTree").value([]);
            $("#ddlDocType").data("kendoDropDownTree").value([]);
            $("#ddlState").data("kendoDropDownTree").value([]);
           
           
            $("#btnClearFilter").hide();
           // $('#btnClearFilter').css('display', 'none');

            return false;
        }
        function GetGridRowCount() {
            //debugger;
            var rowCount = 0;

            var dataFiltered = $("#gridApplicableCompliance").data("kendoGrid").dataSource.dataFiltered();
            rowCount = dataFiltered.length;

            return rowCount;

            //if ($("#gridApplicableCompliance").data("kendoGrid") != null && $("#gridApplicableCompliance").data("kendoGrid") != undefined) {
            //    var grid = $("#gridApplicableCompliance").data("kendoGrid");
            //    if (grid.dataSource != null && grid.dataSource != undefined) {
            //        var dataSource = grid.dataSource;

            //        // Gets the filter from the dataSource
            //        var filters = dataSource.filter();

            //        // Gets the full set of data from the data source
            //        var allData = dataSource.data();

            //        // Applies the filter to the data
            //        var query = new kendo.data.Query(allData);
            //        var filteredData = query.filter(filters).data;

            //        if (filteredData != null && filteredData != undefined) {
            //            rowCount = filteredData.length;
            //            alert(rowCount);
            //        }
            //    }
            //}
            //return rowCount;
        }

        function GetGridRowCount_Pricing() {
            //debugger;
            var rowCount = 0;

            var dataFiltered = $("#gridPricing").data("kendoGrid").dataSource.dataFiltered();
            rowCount = dataFiltered.length;

            return rowCount;
        }

        function GetGridRowCount_Questionary() {
            //debugger;
            var rowCount = 0;

            var dataFiltered = $("#treelist").data("kendoTreeList").dataSource.dataFiltered();
            rowCount = dataFiltered.length;

            return rowCount;
        }

        function BindGrid_ApplicableCompliance() {
            var initialLoad = true;

            var fileName = '';
            var exportExcelName = fileName.concat('<%=txtCustName.Text%>', "-Applicability-", '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");

            $("#gridApplicableCompliance").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<%=AVACOM_RLCS_API_URL%>GetApplicableCompliances?custID=' + $('#hdnCustID').val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    schema: {
                        model: {
                            //id: "ID"
                            id: "MasterID",
                            id: "BranchID",
                        },
                        data: function (response) {
                            if (response != null && response != undefined) {
                                return response.Result;
                            }
                        },
                        total: function (response) {
                            if (response.Result != null && response.Result != undefined) {
                                if (response.Result.length > 0) {

                                }
                                return response.Result.length;
                            }
                        }
                    },
                    requestEnd: function () {
                    },
                    group: {
                        field: "ScopeName",
                        aggregates: [{ field: "ScopeName", aggregate: "count" }]
                    },
                    sort: { field: "BranchID", dir: "asc" },
                },
                dataBound: function (e) {
                    debugger;
                    //if (initialLoad) {
                    //    SetEntityBranchSelection();
                    //    initialLoad = false;
                    //}

                    //var dataSource = e.sender.dataSource;
                    //total = dataSource.total(); 
                    //dataSource.pageSize(total);

                    if (GetGridRowCount() > 0) {
                        $("#btnSaveCompliance").show();
                    } else {
                        $("#btnSaveCompliance").hide();
                    }
                },
                excelExport: function (e) {
                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });
                },
                //toolbar: ["excel"],
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                },
                pageable: {
                    pageSize: 10,
                    refresh: false,
                    buttonCount:3,
                    pageSizes: ["All",5, 10,20],
                },
                //pageable: true,
                scrollable: true,
                persistSelection: true,
                sortable: true,
                filterable: true,
                columnMenu:true,
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                change: onChange,
                columns: [
                    {
                        selectable: true,
                        width: "5%",
                        hidden: true, menu: false,
                    },
                    {
                        field: "BranchID",
                        sortable: {
                            initialDirection: "asc"
                        },
                        hidden: true,menu:false,
                    },
                    {
                        field: "BranchName", title: 'Entity/Branch', width: "20%;", filterable: { search: true, multi: true }
                        //sortable: {
                        //    initialDirection: "asc"
                        //}
                    },
                    { field: "ActID", hidden: true, menu: false, },
                    { field: "ActName", title: 'Act', width: "35%;", hidden: true, menu: false, },
                    //{ field: "ShortDescription", title: 'Compliance', width: "30%;", },
                    { field: "ShortForm", title: 'Compliance', width: "25%;",filterable:{search:true,multi:true} },
                    { field: "StateID", title: 'State', hidden: true, menu: false, },
                    { field: "SM_Name", title: 'State', width: "10%;", filterable: { search: true, multi: true } },
                    { field: "Register_Name", title: 'Form', width: "10%", filterable: { search: true, multi: true } },
                    {
                        field: "ScopeName",
                        groupHeaderTemplate: "Type: #= value # Total: #= count # ",
                        aggregates: ["count"],
                        hidden: true, menu: false,
                    }
                ]
            });
            $("#gridApplicableCompliance").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "bottom",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if ($.trim(content) != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
            $("#exportGrid_ApplicableCompliance").kendoTooltip({
                //filter: ".my-edit",
                content: function (e) {
                    return "Export to Excel";
                }
            });

        }

        function SetEntityBranchSelection() {
            var dataFiltered = $("#gridApplicableCompliance").data("kendoGrid").dataSource;

            if (dataFiltered != null && dataFiltered != undefined) {
                if (dataFiltered._data != null && dataFiltered._data != undefined) {
                    var arrayBranchIDs = [];

                    for (i = 0; i < dataFiltered._data.length; i++) {
                        if (jQuery.inArray(dataFiltered._data[i].BranchID, arrayBranchIDs) !== -1) {
                            arrayBranchIDs.push(dataFiltered._data[i].BranchID);
                        }
                    }

                    if (arrayBranchIDs.length > 0) {
                        var ddlEntityBranch = $("#ddlEntityBranch").data("kendoDropDownTree");
                        ddlEntityBranch.value(arrayBranchIDs);

                        //ddlEntityBranch.value(["1", "2"]);
                    }
                }
            }
        }

        function BindLocationFilter_Pricing() {
            $("#ddlEntityBranch_Pricing").kendoDropDownTree({
                placeholder: "Entity/Branch",
                checkboxes: true,
                checkAll: true,
                tagMode: "single",
                checkboxes: {
                    checkChildren: true
                },
                autoWidth: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: ApplyFilter_Pricing,
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: "<% =AVACOM_RLCS_API_URL%>GetBranchListAssessment?customerId=" + $('#hdnCustID').val(),
                            type: 'Get',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json'
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
    }

    function BindGrid_Pricing() {

        var fileName = '';
        var exportExcelName = fileName.concat('<%=txtCustName.Text%>', "-", '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");
        //alert(exportExcelName);

        $("#gridPricing").kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: '<%=AVACOM_RLCS_API_URL%>GetPricing?custID=' + $('#hdnCustID').val() + '&branchIDs="1,2"',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                    }
                },
                schema: {
                    model: {
                        //id: "ID"
                        id: "MasterID",
                        id: "BranchID",
                    },
                    data: function (response) {
                        if (response != null && response != undefined) {
                            return response.Result;
                        }
                    },
                    total: function (response) {
                        if (response.Result != null && response.Result != undefined) {
                            if (response.Result.length > 0) {

                            }
                            return response.Result.length;
                        }
                    }
                },
                pageSize: 10,                
            },
            dataBound: function (e) {
                debugger;

                //var dataSource = e.sender.dataSource;
                //total = dataSource.total();
                //dataSource.pageSize(total);

                if (GetGridRowCount_Pricing() > 0) {
                    $("#btnSavePricing").show();
                } else {
                    $("#btnSavePricing").hide();
                }

                setTotalPrice();
            },
            excelExport: function (e) {
                var columns = e.workbook.sheets[0].columns;
                columns.forEach(function (column) {
                    delete column.width;
                    column.autoWidth = true;
                });
            },            
            excel: {
                fileName: exportExcelName,
                allPages: true,
            },
            pageable: {
                pageSizes: ["All", 5, 10, 20],
                buttonCount: 3,
                pageSize: 10,
                refresh:false,
            },
            //pageable: true,
            filterable:true,
            scrollable: true,
            persistSelection: true,
            sortable: true,
            columnMenu:true,
            change: onChange,
            columns: [                
                //{ field: "CustomerID", hidden: true, },                
                { field: "CustomerName", title: 'Customer', width: "21.5%;", filterable: {multi:true,search:true} },
                {
                    field: "BranchCount", title: 'No. of Location', width: "15%;",
                    headerAttributes: { style: "text-align:left" },
                    attributes: { style: "text-align:left;" },
                    filterable: { multi: true, search: true }
                },
                {
                    field: "EmployeeCount", title: 'No. of Employee(s)', width: "17%;",
                    headerAttributes: { style: "text-align:left" },
                    attributes: { style: "text-align:left;" },
                    filterable: { multi: true, search: true }
                },
                {
                    field: "Price", title: 'Price', width: "10%;",
                    template: "#= currencyConverter(data)#",
                    headerAttributes: { style: "text-align:left" },
                    attributes: { style: "text-align:right;" }, filterable: { multi: true, search: true }
                },
                {
                    field: "Discount", title: 'Discount', width: "11%;",                    
                    headerAttributes: { style: "text-align:left" },
                    attributes: { style: "text-align:right;" }, filterable: { multi: true, search: true }
                },
                {
                    field: "DiscountedPrice", title: 'Discounted Price', width: "15.5%;",
                    template: "#= currencyConverter_Discount(data)#",
                    headerAttributes: { style: "text-align:left" },
                    attributes: { style: "text-align:right;" }, filterable: { multi: true, search: true }
                },
                {
                    title: "Action", lock: true, width: "10%;",
                    attributes: { style: 'text-align: center;' },
                    headerAttributes: { style: "text-align:center;color:rgba(0,0,0,0.6)" },
                    command:
                        [
                           { name: "view", text: "", iconClass: "k-icon k-i-info", className: "btnInfo", click: ViewPricingPlan },
                        ],
                }
            ],            
        });

        $("#gridPricing").kendoTooltip({
            filter: "td", //this filter selects the second column's cells
            position: "bottom",
            content: function (e) {
                var content = e.target.context.textContent;
                if ($.trim(content) != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                }
                else
                    e.preventDefault();
            }
        }).data("kendoTooltip");
        $("#gridPricing").kendoTooltip({
            filter: ".btnInfo",
            content: function (e) {
                return "View Pricing Plan";
            }
        }); 
        $("#exportPricingGrid").kendoTooltip({
            //filter: ".btnInfo",
            content: function (e) {
                return "Export to Excel";
            }
        });
    }

        function ViewPricingPlan(e) {
            var d = e.data;
            var grid = $("#gridPricing").data("kendoGrid");

            var tr = $(e.target).closest("tr");    // get the current table row (tr)
            var item = this.dataItem(tr);
            var recordID = item.ID;

            $('#divShowPricingPlan').show();
            var myWindowAdv = $("#divShowPricingPlan");

            myWindowAdv.kendoWindow({
                width: "60%",
                height: '60%',
                //content: '/RLCS/HRPlus_ApplicabilityDetails.aspx?ID=' + recordID,
                title: "Pricing Plan",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],                
                //open: onOpen
            });

            $('#iframePricingPlan').attr('src', 'about:blank');
            var windowWidget = $("#divShowPricingPlan").data("kendoWindow");

            myWindowAdv.data("kendoWindow").center().open();

            $('#iframePricingPlan').attr('src', "/RLCS/HRPlus_PricingPlan.aspx");
        }

     <%--function BindGrid_Pricing() {

         var fileName = '';
         var exportExcelName = fileName.concat('<%=txtCustName.Text%>', "-", '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");
        //alert(exportExcelName);

        $("#gridPricing").kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: '<%=AVACOM_RLCS_API_URL%>GetPricing?custID=' + $('#hdnCustID').val() + '&branchIDs="1,2"',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                    }
                },
                schema: {
                    model: {
                        //id: "ID"
                        id: "MasterID",
                        id: "BranchID",
                    },
                    data: function (response) {
                        if (response != null && response != undefined) {
                            return response.Result;
                        }
                    },
                    total: function (response) {
                        if (response.Result != null && response.Result != undefined) {
                            if (response.Result.length > 0) {

                            }
                            return response.Result.length;
                        }
                    }
                },
                pageSize: 100,
                aggregate: [{ field: "Price", aggregate: "sum" }],
                sort: { field: "BranchID", dir: "asc" },
            },
            dataBound: function (e) {
                debugger;

                //var dataSource = e.sender.dataSource;
                //total = dataSource.total();
                //dataSource.pageSize(total);

                if (GetGridRowCount_Pricing() > 0) {
                    $("#btnSavePricing").show();
                } else {
                    $("#btnSavePricing").hide();
                }

                setTotalPrice();
            },
            excelExport: function (e) {
                var columns = e.workbook.sheets[0].columns;
                columns.forEach(function (column) {
                    delete column.width;
                    column.autoWidth = true;
                });
            },
            toolbar: ["excel"],
            excel: {
                fileName: exportExcelName,
                allPages: true,
            },
            pageable: {
                pageSizes: [50, 100],
            },
            //pageable: true,
            scrollable: true,
            persistSelection: true,
            sortable: true,
            change: onChange,
            columns: [
                {
                    selectable: true,
                    width: "5%",
                    hidden: true,
                },
                { field: "CustomerID", hidden: true, },
                { field: "ParentBranchID", hidden: true, },
                { field: "BranchID", hidden: true, },
                { field: "BranchType", hidden: true, },
                { field: "Location", title: 'Entity/Branch', width: "50%;", },
                {
                    field: "EmployeeCount", title: 'No. of Employee(s)', width: "25%;",
                    headerAttributes: { style: "text-align:right" },
                    attributes: {
                        style: "text-align:right;"
                    }
                },
                {
                    field: "Price",
                    title: 'Price',
                    width: "25%;",
                    aggregates: ["sum"],
                    template: "#= currencyConverter(data)#",
                    footerTemplate: "Total : #=numberToCurrencyConverter(sum)#",

                    headerAttributes: { style: "text-align:right" },
                    attributes: {
                        style: "text-align:right;"
                    },
                    footerAttributes: { style: "text-align:right" },
                },
            ]
        });
    }--%>

        function currencyConverter(data) {
            debugger;
            var strPrice = '';
            kendo.culture("en-IN");
            if (data.Price != null && data.Price != undefined) {
                strPrice = kendo.toString(data.Price, "n")
            }
            return strPrice;
        }

        function currencyConverter_Discount(data) {
            debugger;
            var strPrice = '';
            kendo.culture("en-IN");
            if (data.DiscountedPrice != null && data.DiscountedPrice != undefined) {
                strPrice = kendo.toString(data.DiscountedPrice, "n")
            }
            return strPrice;
        }

        function numberToCurrencyConverter(data) {
            debugger;
            var strPrice = '';
            kendo.culture("en-IN");
            if (data != null && data != undefined && data != '') {
                strPrice = kendo.toString(data, "n")
            }
            return strPrice;
        }

        function Bind_ComplianceType() {
            $("#ddlDocType").kendoDropDownTree({
                placeholder: "Select Type",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter,
                dataSource: [
                    { text: "Register", value: "Register" },
                    { text: "Return", value: "Return" },
                    { text: "Challan", value: "Challan" },
                ]
            });
        }

        function Bind_Act() {
            $("#ddlAct").kendoDropDownTree({
                placeholder: "Select Act",
                filter: "contains",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "ActName",
                dataValueField: "ActID",
                change: ApplyFilter,
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =AVACOM_RLCS_API_URL%>GetAllHRActs',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    },
                },
            });
        }

        function Bind_State() {
            $("#ddlState").kendoDropDownTree({
                placeholder: "Select State",
                filter: "startswith",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "StateName",
                dataValueField: "StateID",
                change: ApplyFilter,
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =AVACOM_RLCS_API_URL%>GetAll_StatesByDocType?docType=' + $("#ddlDocType").val(),
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    }
                },
            });
        }

        function BindLocationFilter() {
            $("#ddlEntityBranch").kendoDropDownTree({
                placeholder: "Entity/Branch",
                checkboxes: true,
                checkAll: true,
                tagMode: "single",
                //checkboxes: {
                //    checkChildren: true
                //},
                autoWidth: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: ApplyFilter,
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: "<% =AVACOM_RLCS_API_URL%>GetBranchListAssessment?customerId=" + $('#hdnCustID').val(),
                            type: 'Get',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json'
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }
    </script>

    <script>
        function Save_ApplicableCompliance(e) {
            debugger;
            e.preventDefault();
            kendo.ui.progress($('.chart-loading'), true);
            var saveSuccess = false;

            if ($('#hdnCustID').val() != null && $('#hdnCustID').val() != undefined) {
                var dataFiltered = $("#gridApplicableCompliance").data("kendoGrid").dataSource.dataFiltered();
                if (dataFiltered.length > 0) {
                    var objJson = [];

                    $.each(dataFiltered, function (index, item) {
                        _obj = {}
                        _obj["CustID"] = $('#hdnCustID').val();
                        _obj["BranchID"] = item.BranchID;
                        _obj["MasterID"] = item.MasterID;
                        _obj["LoggedInUserID"] = "<%= loggedInUserID %>";

                        objJson.push(_obj);
                    });

                    if (objJson.length > 0) {
                        var uri = '<%= avacomAPIUrl %>SaveApplicableCompliances'
                        $.ajax({
                            //async: true,
                            type: 'Post',
                            url: uri,
                            data: JSON.stringify(objJson),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            processData: true,
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                xhr.setRequestHeader('Content-Type', 'application/json');
                                <%--xhr.setRequestHeader('Authorization', '<%= authKey%>');
                                xhr.setRequestHeader('X-User-Id-1', '<%=userProfileID_Encrypted %>');--%>
                            },
                            success: function (result) {
                                debugger;
                                saveSuccess = true;
                                kendoCustomAlert("Details Save Successfully","");
                                //kendo.alert("Details Save Successfully");
                                //alert(JSON.stringify('Data Save Successfully'));
                                //$("#divAlert").html("Data save successfully"); 
                                kendo.ui.progress($('.chart-loading'), false);
                                ShowPricingTab();
                            },
                            error: function (e, t) {
                                kendo.ui.progress($('.chart-loading'), false);
                                alert("Error-" + e.error.toString());
                            }
                        });
                    }

                    if (saveSuccess) {
                        kendo.ui.progress($('.chart-loading'), false);
                    }
                }
            }

            return true;
        }

        function Save_Pricing(e) {
            debugger;

            e.preventDefault();

            var saveSuccess = false;

            if ($('#hdnCustID').val() != null && $('#hdnCustID').val() != undefined) {
                var dataFiltered = $("#gridPricing").data("kendoGrid").dataSource.dataFiltered();
                if (dataFiltered.length > 0) {
                    var objJson = [];

                    $.each(dataFiltered, function (index, item) {
                        _obj = {}
                        _obj["CustID"] = $('#hdnCustID').val();
                        _obj["BranchCount"] = item.BranchCount;
                        _obj["EmployeeCount"] = item.EmployeeCount;
                        _obj["Price"] = item.Price;
                        _obj["Discount"] = item.Discount;
                        _obj["DiscountedPrice"] = item.DiscountedPrice;
                        _obj["LoggedInUserID"] = "<%= loggedInUserID %>";

                        objJson.push(_obj);
                    });

                    if (objJson.length > 0) {
                        var uri = '<%= avacomAPIUrl %>SavePricing'
                        $.ajax({
                            //async: true,
                            type: 'Post',
                            url: uri,
                            data: JSON.stringify(objJson),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            processData: true,
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                xhr.setRequestHeader('Content-Type', 'application/json');
                                <%--xhr.setRequestHeader('Authorization', '<%= authKey%>');
                                xhr.setRequestHeader('X-User-Id-1', '<%=userProfileID_Encrypted %>');--%>
                            },
                            success: function (result) {
                                debugger;
                                saveSuccess = true;
                                kendoCustomAlert("Pricing Details Save Successfully", "");
                                //kendo.alert("Pricing Details Save Successfully");
                                //alert(JSON.stringify('Data Save Successfully'));
                                //$("#divAlert").html("Data save successfully");
                            },
                            error: function (e, t) {
                                alert("Error" + e.error.toString())
                            }
                        });
                    }
                }
            }

            return false;
        }

        <%--function Save_ApplicableCompliance() {
            debugger;

            if ($('#hdnCustID').val() != null && $('#hdnCustID').val() != undefined) {

                var selectedComplianceIDs = $('#gridApplicableCompliance').data("kendoGrid").selectedKeyNames();

                var dataFiltered = $("#gridApplicableCompliance").data("kendoGrid").dataSource.dataFiltered();

                //var selectedComplianceIDs = $('#gridApplicableCompliance').data("kendoGrid").selectedKeyNames();

                if (dataFiltered.length > 0) {

                    $.each(filteredData, function (index, item) {
                        $('#FilterResults').append('<li>' + item.Site + ' : ' + item.Visitors + '</li>')
                    });


                    var objJson = [];

                    for (iCounter = 0; iCounter < selectedComplianceIDs.length - 1; iCounter++) {
                        item = {}
                        item["CustID"] = $('#hdnCustID').val();
                        item["MasterID"] = selectedComplianceIDs[iCounter];
                        item["LoggedInUserID"] = "<%= loggedInUserID %>";

                        objJson.push(item);
                    }

                    if (objJson.length > 0) {
                        var uri = '<%= avacomAPIUrl %>SaveApplicableCompliances'
                        $.ajax({
                            async: true,
                            type: 'Post',
                            url: uri,
                            data: JSON.stringify(objJson),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            processData: true,
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('Authorization', '<%= authKey%>');
                                xhr.setRequestHeader('X-User-Id-1', '<%=userProfileID_Encrypted %>');
                            },
                            success: function (result) {
                                debugger;
                                alert(JSON.stringify('Data save successfully.'));
                                //$("#divAlert").html("Data save successfully");                    
                            },
                            error: function (e, t) { }
                        });
                    }

                    //$('#DownloadViews').attr('src', "HRPlus_ApplicabilityDetails.aspx?Event=Save&CompIDs=" + selectedComplianceIDs.join(","));
                    //console.log(selectedComplianceIDs.join(","));
                }
            }

            return false;
        }--%>

        function OpenAddEntityClientPopup(ID) {
            debugger;
            if ($('#hdnCustID').val() != null && $('#hdnCustID').val() != undefined) {

                var myWindowAdv = $("#divAddEntityClientDialog");

                function onClose() {
                    document.getElementById('<%= lnkRefresh.ClientID %>').click();
                }

                myWindowAdv.kendoWindow({
                    width: "60%",
                    height: '90%',
                    content: "../RLCS/ClientSetup/RLCS_EntityBranchDetails.aspx?CustID=" + $('#hdnCustID').val() + "&ClientID=" + ID,
                    iframe: true,
                    title: "Entity/Client Details",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
            }

            return false;
        }
    </script>

    <script>

        $(document).ready(function () {

            $("#btnSaveCompliance").click(function (e) {
                Save_ApplicableCompliance(e);
            });

            $("#btnSavePricing").click(function (e) {
                Save_Pricing(e);
            });

            //used to sync the exports
            var promises = [
              $.Deferred(),
              $.Deferred()
            ];


            var exportExcelName = '<%= txtCustName.Text %>' + "-" + '<%= DateTime.Now.ToString("ddMMyyyy") %> +".xlsx"';

            $("#exportPricingGrid").click(function (e) {
                
                debugger;
                // trigger export of the products grid
                $("#gridApplicableCompliance").data("kendoGrid").saveAsExcel();
                // trigger export of the orders grid
                $("#gridPricing").data("kendoGrid").saveAsExcel();
                // wait for both exports to finish

                $.when.apply(null, promises)
                 .then(function (gridApplicableComplianceWorkbook, gridPricingWorkbook) {

                     var columns = gridApplicableComplianceWorkbook.sheets[0].columns;
                     columns.forEach(function (column) {
                         delete column.width;
                         column.autoWidth = true;
                     });

                     columns = gridPricingWorkbook.sheets[0].columns;
                     columns.forEach(function (column) {
                         delete column.width;
                         column.autoWidth = true;
                     });

                     // create a new workbook using the sheets of the products and orders workbooks
                     var sheets = [
                         gridPricingWorkbook.sheets[0],
                       gridApplicableComplianceWorkbook.sheets[0],
                     ];

                     sheets[0].title = "Pricing";
                     sheets[1].title = "Applicable Compliances";

                     var workbook = new kendo.ooxml.Workbook({
                         sheets: sheets
                     });

                     // save the new workbook,b
                     kendo.saveAs({
                         dataURI: workbook.toDataURL(),
                         fileName: exportExcelName
                     });
                 });
            });

            $(document).on("click", "button.k-button.k-button-icontext.my-del", function (e) {

                var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));

                DeleteQuestionnaireDetails(item);
            });
        });
    </script>

    <script>
        function OnClientClick(ServerControID, IndexControlID, Index) {
            debugger;
            var objDemo = document.getElementById(ServerControID);
            if (objDemo) {
                document.getElementById(IndexControlID).value = Index;
                objDemo.click();
            }
        }
    </script>
    <script runat="server">
        protected void butSubmit_Click(object sender, EventArgs e)
        {
            lnkBtnPricing_Click(sender, e);
            //MainView.ActiveViewIndex = int.Parse(HiddenField1.Value);
        }
    </script>

    <script type="text/javascript">

        function FillQuestionnaireDetails(input) {
            debugger;

            $('#hdnparentBranchId').val(input.ParentBranchID);

            $('#hdnBranchId').val(input.BranchID);
            $('#hdnQuestionarycustomerId').val(input.CustomerID);

            $('#btnhidden').click();

            $("#btnNextToApplicability").hide();
        }

        function DeleteQuestionnaireDetails(item) {

            var r = confirm("Are you sure, do you want to delete ?");
            if (r == true) {
                $('#hdnBranchId').val(item.BranchID);
                $('#btnDeleteQuestionier').click();
                return true;
            }
            return false;
        }

        function MSGAlert(Msg) {
            debugger;

            if (Msg != "Location") {

                document.getElementById("divtreelist").style.display = "block";
                document.getElementById("Questionnaire").style.display = "none";

                bindQuestionnaireGrid();
            }
            else {
                bindQuestionnaireGrid();
                document.getElementById("divtreelist").style.display = "none";
                document.getElementById("Questionnaire").style.display = "block";

            }
            if (Msg == "7") {
                //kendo.alert("Entity Save Successfully.");
            }
            else if (Msg == "Location") {
                var valLocation = $('#hdnLocationVal').val();
                kendoCustomAlert(valLocation + " - Location Already Exist..!", "");
                //kendo.alert(valLocation + " - Location Already Exist..!");
            }

        }

        function SetInitialValueshdn(id) {
            debugger;
            if (id == 1) {

                $('#hdnBranchId').val(0);
                $('#hdnparentBranchId').val(0);
                $('#TextBox1').val("");


               <%-- var totalRows = $("#<%=Gridview1.ClientID %> tr").length;
                totalRows = totalRows - 2;
                var RowCount = 0
                for (var i = 0; i < totalRows; i++) {

                    $('#Gridview1_ddlType_' + i).val(0);
                    $('#Gridview1_hdnGridBranchId_' + i).val(0);
                    $('#Gridview1_txtBranchLocation_' + i).val("");
                    $('#Gridview1_ddlState_' + i).val(0);
                    $('#Gridview1_txtEmpCount_' + i).val("");
                    $('#Gridview1_txtMinBasic_' + i).val("");
                    $('#Gridview1_txtMinGross_' + i).val("");
                    $('#Gridview1_txtMaxGross_' + i).val("");
                }
              --%>
                var btnadd = $('#addQuestionnaire').val();
                if (btnadd == "Add New") {
                    $('#btnhidden').click();
                    $("#addQuestionnaire").prop('value', 'Cancel');
                }
                else {
                    $("#addQuestionnaire").prop('value', 'Add New');
                }
            }
        }

        function UserDeleteConfirmation() {
            var totalRows = $("#<%=Gridview1.ClientID %> tr").length;
            if (totalRows > 3) {

                return confirm("Are you sure you want to delete this Location/Branch?");
            }

        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //bindQuestionnaireGrid();

        });

        function bindQuestionnaireGrid() {
            debugger;
            var crudServiceBaseUrl = "<%= avacomAPIUrl %>GetQuestionary?CustomerID=" + $('#hdnCustID').val();

            var dataSource = new kendo.data.TreeListDataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl,
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                    }
                },
                schema: {
                    data: function (response) {
                        debugger;
                        if (response.Result != null)
                            return response.Result;
                    },
                    model: {
                        id: "BranchID",
                        parentId: "ParentBranchID",
                        fields: {
                            BranchID: { type: "number", nullable: false },
                            ParentBranchID: { type: "number", nullable: false }
                        }
                    }
                }
            });

            $("#treelist").kendoTreeList({
                dataSource: dataSource,
                dataBound: function (e) {
                    debugger;


                    if (GetGridRowCount_Questionary() > 0) {
                        $("#btnNextToApplicability").show();
                    } else {
                        $("#btnNextToApplicability").hide();
                    }
                },
                filterable: true,
                multi: true,
                search: true,
                sortable: true,
                columnMenu:true,
                columns: [
                    { field: "BranchID", expandable: true, title: "BranchID", hidden: true,menu:false },
                    { field: "ParentBranchID", title: "ParentBranchID", hidden: true, menu: false },
                    { field: "BranchType", hidden: true, menu: false },
                    {
                        field: "Location", title: "Location", width: "50%", filterable:{multi: true,search: true},
                    },
                    { field: "State", title: "State", width: "35%", filterable: { multi: true, search: true }, },
                    { field: "LocationType", title: "LocationType", hidden: true, menu: false },
                    { field: "EmployeeCount", title: "EmployeeCount", hidden: true, menu: false },
                    { field: "MinimumBasicSalary", title: "MinimumBasicSalary", hidden: true, menu: false },
                    { field: "MaxGrossSalary", title: "MaxGrossSalary", hidden: true, menu: false },
                    {
                        title: "Action",
                        filterable: { multi: true, search: true },
                        headerAttributes: {
                            style: 'text-align: center;'
                        },
                        attributes: {
                            style: 'text-align: center;'
                        },
                        width: "15%",
                        command: [
                                { name: "x", text: " ", imageClass: "k-i-edit", iconClass: ".k-icon k-i-eye", className: "my-edit" },
                                { name: "del", text: " ", iconClass: ".k-icon k-i-eye", imageClass: "k-i-delete", className: "my-del" }
                        ], lock: true,// width: 150,            
                    }
                ]
            });

            $(document).on("click", "#treelist tbody tr .my-edit", function (e) {
                // debugger;

                var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));

                FillQuestionnaireDetails(item)
                // $("#treelist").toggle();
                // $("#Questionnaire").toggle();
                // return false;
            });
            $("#treelist").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "bottom",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if ($.trim(content) != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
          $("#treelist").kendoTooltip({
                filter: ".my-edit",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#treelist").kendoTooltip({
                filter: ".my-del",
                content: function (e) {
                    return "Delete";
                }
            });
           
        }

        function ShowInputs(id) {
            debugger;
            if (id == 9) {
                // kendo.alert("Entity Save Successfully.");
                document.getElementById("divtreelist").style.display = "block";
                document.getElementById("Questionnaire").style.display = "none";
            }

            else if (id == 8) {
                document.getElementById("divtreelist").style.display = "none";
                document.getElementById("Questionnaire").style.display = "block";
            }
            else {
                $("#divtreelist").toggle();
                $("#Questionnaire").toggle();

            }

            if ($('#treelist').data('kendoTreeList') == undefined) {
                bindQuestionnaireGrid();
            }
            else {
                $('#treelist').data('kendoTreeList').dataSource.read();
            }

            SetInitialValueshdn(id);
        }


    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smComplianceApplicability" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <div class="chart-loading"></div>
        <div>
            <asp:HiddenField ID="hdnCustID" runat="server" />
            <asp:HiddenField ID="hdnCustName" runat="server" />
            <asp:HiddenField ID="hdnEmail" runat="server" />
            <asp:HiddenField ID="hdnMobile" runat="server" />
            <asp:HiddenField ID="hdnparentBranchId" runat="server" />
            <asp:HiddenField ID="hdnBranchId" runat="server" />
            <asp:HiddenField ID="hdnQuestionarycustomerId" runat="server" />
            <asp:Button Text="" runat="server" ID="btnDeleteQuestionier" OnClick="btnDeleteQuestionier_Click" Style="display: none;" />
            <asp:Button Text="" runat="server" ID="btnhidden" OnClick="btnhidden_Click" Style="display: none;" />
            <asp:Button Text="" runat="server" ID="btnaddnewrow" Style="display: none;" />

            <asp:HiddenField ID="hdnTotalPrice" runat="server" />

            <asp:HiddenField ID="hdnSaveApplicability" runat="server" />

            <div class="mainDiv" style="background-color: #f7f7f7;">
                <div class="col-md-12">
                    <header class="panel-heading tab-bg-primary" style="background: white">
                        <ul class="nav nav-tabs">
                            <li class="active" id="liDetails" runat="server">
                                <asp:LinkButton ID="lnkBtnDetails" runat="server" CssClass="bgColor-gray" OnClick="lnkBtnDetails_Click">1. Details</asp:LinkButton>
                            </li>

                            <li class="" id="liQuestionnaire" runat="server">
                                <asp:LinkButton ID="lnkBtnQuestionnaire" runat="server" CssClass="bgColor-gray" OnClick="lnkQuestionnaire_Click">2. Questionnaire</asp:LinkButton>
                            </li>

                            <li class="" id="liApplicability" runat="server">
                                <asp:LinkButton ID="lnkBtnApplicability" runat="server" CssClass="bgColor-gray" OnClick="lnkBtnApplicability_Click">3. Applicability</asp:LinkButton>
                            </li>

                            <li class="" id="liPricing" runat="server">
                                <asp:LinkButton ID="lnkBtnPricing" runat="server" CssClass="bgColor-gray" OnClick="lnkBtnPricing_Click">4. Pricing</asp:LinkButton>
                            </li>

                            <li class="" id="liPayment" runat="server" visible="false">
                                <asp:LinkButton ID="lnkBtnPayment" runat="server" CssClass="bgColor-gray" OnClick="lnkBtnPayment_Click">5. Payment</asp:LinkButton>
                            </li>
                        </ul>
                    </header>
                </div>

                <div class="clearfix"></div>

                <div id="divMainView">
                    <asp:MultiView ID="MainView" runat="server">
                        <asp:View ID="detailsView" runat="server">
                            <div class="container">
                                <div id="divDetails" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div id="collapseDivCustomerDetails" class="panel-collapse collapse in">
                                            <div class="row">
                                                <asp:ValidationSummary ID="VSPopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                    ValidationGroup="PopUpValidationGroup" />
                                                <asp:CustomValidator ID="cvPopUp" runat="server" EnableClientScript="False"
                                                    ValidationGroup="PopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                            </div>

                                            <div class="panel-body">
                                                <div class="container plr0">
                                                    <div class="row">
                                                        <div class="form-group required col-md-4">
                                                            <label for="txtCustName" class="control-label">Company</label>
                                                            <asp:TextBox runat="server" ID="txtCustName" CssClass="form-control" autocomplete="off" />
                                                            <asp:RequiredFieldValidator ID="rfvCustName" ErrorMessage="Required Company Name"
                                                                ControlToValidate="txtCustName" runat="server" ValidationGroup="PopUpValidationGroup" Display="None" />
                                                        </div>

                                                        <div class="form-group required col-md-4">
                                                            <label for="txtBuyerName" class="control-label">Contact Person</label>
                                                            <asp:TextBox runat="server" ID="txtBuyerName" CssClass="form-control" autocomplete="off" />
                                                            <asp:RequiredFieldValidator ID="rfvBuyerName" ErrorMessage="Required Contact Person Name"
                                                                ControlToValidate="txtBuyerName" runat="server" ValidationGroup="PopUpValidationGroup" Display="None" />
                                                        </div>

                                                        <div class="form-group required col-md-4">
                                                            <label for="txtEmail" class="control-label">Email</label>
                                                            <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" autocomplete="off" />
                                                            <asp:RequiredFieldValidator ID="rfvEmail" ErrorMessage="Required Email"
                                                                ControlToValidate="txtEmail" runat="server" ValidationGroup="PopUpValidationGroup"
                                                                Display="None" />
                                                            <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="PopUpValidationGroup"
                                                                ErrorMessage="Invalid Email" ControlToValidate="txtEmail"
                                                                ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group required col-md-4">
                                                            <label for="txtContactNumber" class="control-label">Contact Number</label>
                                                            <asp:TextBox runat="server" ID="txtContactNumber" CssClass="form-control" autocomplete="off" MaxLength="10" />
                                                            <asp:RequiredFieldValidator ID="rfvContactNumber" ErrorMessage="Required Contact Number"
                                                                ControlToValidate="txtContactNumber" runat="server" ValidationGroup="PopUpValidationGroup"
                                                                Display="None" />
                                                            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ValidationGroup="PopUpValidationGroup"
                                                                ControlToValidate="txtContactNumber" ErrorMessage="Contact Number must be number" Display="None" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                                                ValidationGroup="PopUpValidationGroup" ErrorMessage="Contact Number should be of 10 Digits"
                                                                ControlToValidate="txtContactNumber" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                                        </div>

                                                        <div class="form-group required col-md-8">
                                                            <label for="txtAddress" class="control-label">Address</label>
                                                            <asp:TextBox runat="server" ID="txtAddress" TextMode="MultiLine" CssClass="form-control" autocomplete="off" />
                                                            <asp:RequiredFieldValidator ID="rfvAddress" ErrorMessage="Required Address"
                                                                ControlToValidate="txtAddress" runat="server" ValidationGroup="PopUpValidationGroup"
                                                                Display="None" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-12 text-right">
                                                    <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                                        ValidationGroup="PopUpValidationGroup"></asp:Button>
                                                    <asp:Button Text="Next" runat="server" ID="btnNext" CssClass="btn btn-primary" OnClick="btnNext_Click"
                                                        ValidationGroup="PopUpValidationGroup"></asp:Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="divClientDetails" class="row Dashboard-white-widget" runat="server">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div id="collapseDivClientDetails" class="panel-collapse collapse in">
                                                <div class="row">
                                                    <asp:ValidationSummary ID="vsSummary_Client" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                        ValidationGroup="ClientPopUpValidationGroup" />
                                                    <asp:CustomValidator ID="cvClient" runat="server" EnableClientScript="False"
                                                        ValidationGroup="ClientPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                </div>

                                                <div class="panel-body">
                                                    <div class="container plr0">
                                                        <div class="col-md-12 plr0 text-right">
                                                            <asp:LinkButton ID="lnkAddNewClient" runat="server" CssClass="btn btn-primary"
                                                                ToolTip="Add New Entity/Company" data-toggle="tooltip" data-placement="bottom"
                                                                OnClientClick="return OpenAddEntityClientPopup(0);">
                                                                    <i class='fa fa-plus'></i>Add Entity</asp:LinkButton>

                                                            <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="btn btn-primary" Style="display: none;"
                                                                ToolTip="Add New Entity/Company" data-toggle="tooltip" data-placement="bottom"
                                                                OnClick="btnRefreshEntityClient_Click"></asp:LinkButton>
                                                        </div>

                                                        <div class="row">
                                                            <asp:GridView runat="server" ID="grdEntityClient" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                GridLines="None" PageSize="10" AllowPaging="false" AutoPostBack="true" CssClass="table" Width="100%" DataKeyNames="ID"
                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdEntityClient_RowCommand">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <%#Container.DataItemIndex+1 %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                                                                    <asp:BoundField DataField="Name" HeaderText="Entity/Client" />
                                                                    <asp:BoundField DataField="ContactPerson" HeaderText="Contact Person" />
                                                                    <asp:BoundField DataField="EmailID" HeaderText="Email" />
                                                                    <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkProceed" runat="server" CommandName="Proceed" CommandArgument='<%# Eval("ID") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Applicability Assessment">
                                                                                <img src='<%# ResolveUrl("~/Images/view-doc.png")%>' alt="Proceed" />
                                                                            </asp:LinkButton>
                                                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="EditClient" CommandArgument='<%# Eval("ID") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="View/Edit">
                                                                                <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Proceed" />
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <EmptyDataTemplate>
                                                                    No Records Found
                                                               
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>

                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:View>

                        <asp:View ID="QuestionnaireView" runat="server">
                            <div class="container">
                                <div id="divQuestionnaire" runat="server" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="clearfix"></div>
                                            <div class="parent col-md-12 colpadding0">
                                                <div class="repeatable">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <div class="row">
                                                                <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-success fade in" ValidationGroup="DocGenValidationGroup" />

                                                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False" ValidationGroup="DocGenValidationGroup" Display="None" />

                                                            </div>
                                                            <div style="margin-top: 5px; text-align: right">
                                                                <%--<asp:Button ID="btnAddNew" OnClick="btnAddNew_Click"  runat="server" Text="Add New" CssClass="btn btn-info"/>--%>
                                                                <input type="button" id="addQuestionnaire" class="btn btn-primary" value="Add New" onclick="return ShowInputs(1);" runat="server" />
                                                            </div>

                                                            <div id="divtreelist" runat="server">
                                                                <div id="treelist" runat="server" style=" margin: 5px 0px 5px 0px;">
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12 text-right colpadding0">
                                                                <asp:Button Text="Next" runat="server" ID="btnNextToApplicability" CssClass="btn btn-primary" OnClientClick="ShowApplicabilityTab();"></asp:Button>
                                                                <%--OnClick="btnNextToApplicability_Click"--%>
                                                            </div>

                                                            <div id="Questionnaire" style="padding: 10px 0px 10px 0px; margin: 10px 0px 10px 0px; display: none" runat="server">
                                                                <div class="col-md-12 colpadding0" style="margin-bottom: 50px;    margin-left: -16px;">
                                                                    <div class="col-md-1 text-label" style="margin-top: 6px;">
                                                                        <label class="control-label">Entity/Client</label>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <asp:TextBox ID="TextBox1" placeholder=" Entity Name" Width="100%" Class="form-control" runat="server"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ForeColor="Red" ValidationGroup="validate" ControlToValidate="TextBox1" ErrorMessage="Please Enter Entity Name"></asp:RequiredFieldValidator>
                                                                        <asp:HiddenField ID="hdnLocationVal" Value="" runat="server" />
                                                                    </div>
                                                                </div>

                                                                <div class="clearfix"></div>

                                                                <div class="col-md-12 colpadding0">
                                                                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Style="max-height: 450px;">
                                                                        <asp:GridView ID="Gridview1" runat="server" ShowFooter="true" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="Gridview1_RowDataBound"
                                                                            GridLines="None" PageSize="10" AllowPaging="false" CssClass="table">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Type" HeaderStyle-Width="10%" ItemStyle-Width="10%" ItemStyle-CssClass="narrow" HeaderStyle-CssClass="narrow">
                                                                                    <ItemTemplate>
                                                                                        <div id="type1" style="margin-top:10px;">
                                                                                        <asp:DropDownList ID="ddlType" Class="form-control" DataTextField="--Select--" AllowSingleDeselect="false" runat="server">
                                                                                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                                                            <asp:ListItem Text="Office" Value="SEA"></asp:ListItem>
                                                                                            <asp:ListItem Text="Factory" Value="FACT"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                            </div>
                                                                                          <div id="type11" class="validation-error">
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" InitialValue="0" Display="Dynamic" ControlToValidate="ddlType" ForeColor="Red" ValidationGroup="validate"
                                                                                            ErrorMessage="Select Type"></asp:RequiredFieldValidator>
                                                                                       </div>
                                                                                               <asp:HiddenField ID="hdnGridBranchId" runat="server" Value='<%# Eval("BranchID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Location/Branch" HeaderStyle-Width="20%" ItemStyle-Width="20%"  ItemStyle-CssClass="narrow" HeaderStyle-CssClass="narrow">
                                                                                    <ItemTemplate>
                                                                                        <div id="location1" style="margin-top:10px;">
                                                                                        <asp:TextBox ID="txtBranchLocation" Text='<%# Eval("Location") %>' placeholder=" Location" Class="form-control" runat="server"></asp:TextBox>
                                                                                        </div>
                                                                                              
                                                                                      <div id="location11" class="validation-error">
                                                                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ControlToValidate="txtBranchLocation" ValidationGroup="validate" ForeColor="Red" ErrorMessage="Please Enter Location"></asp:RequiredFieldValidator>
                                                                                     </div>
                                                                                          </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="State" HeaderStyle-Width="15%" ItemStyle-Width="15%"  ItemStyle-CssClass="narrow" HeaderStyle-CssClass="narrow">
                                                                                    <ItemTemplate>
                                                                                        <div id="state1" style="margin-top:10px;">
                                                                                        <asp:DropDownList ID="ddlState" CssClass="form-control state1test" DataTextField="Select State" AllowSingleDeselect="false" runat="server">
                                                                                            <asp:ListItem Text="Select State" Value="0"></asp:ListItem>
                                                                                            <asp:ListItem Text="Andaman and Nicobar Islands" Value="ANI"></asp:ListItem>
                                                                                            <asp:ListItem Text="Andhra Pradesh" Value="ANP"></asp:ListItem>
                                                                                            <asp:ListItem Text="Arunachal Pradesh" Value="ARP"></asp:ListItem>
                                                                                            <asp:ListItem Text="Assam" Value="ASM"></asp:ListItem>
                                                                                            <asp:ListItem Text="Bihar" Value="BHR"></asp:ListItem>
                                                                                            <asp:ListItem Text="Chhattisgarh" Value="CHG"></asp:ListItem>
                                                                                            <asp:ListItem Text="Chandigarh" Value="CHN"></asp:ListItem>
                                                                                            <asp:ListItem Text="Daman and Diu" Value="DAD"></asp:ListItem>
                                                                                            <asp:ListItem Text="Delhi" Value="DLH"></asp:ListItem>
                                                                                            <asp:ListItem Text="Dadra and Nagar haveli" Value="DNH"></asp:ListItem>
                                                                                            <asp:ListItem Text="Goa" Value="GOA"></asp:ListItem>
                                                                                            <asp:ListItem Text="Gujarat" Value="GUJ"></asp:ListItem>
                                                                                            <asp:ListItem Text="Haryana" Value="HAR"></asp:ListItem>
                                                                                            <asp:ListItem Text="Himachal Pradesh" Value="HMP"></asp:ListItem>
                                                                                            <asp:ListItem Text="Jharkhand" Value="JAK"></asp:ListItem>
                                                                                            <asp:ListItem Text="Jammu Kashmir" Value="JMK"></asp:ListItem>
                                                                                            <asp:ListItem Text="Karnataka" Value="KAR"></asp:ListItem>
                                                                                            <asp:ListItem Text="Kerala" Value="KRL"></asp:ListItem>
                                                                                            <asp:ListItem Text="Lakshadweep" Value="LKD"></asp:ListItem>
                                                                                            <asp:ListItem Text="Maharashtra" Value="MAH"></asp:ListItem>
                                                                                            <asp:ListItem Text="Madhya Pradesh" Value="MAP"></asp:ListItem>
                                                                                            <asp:ListItem Text="Mizoram" Value="MIZ"></asp:ListItem>
                                                                                            <asp:ListItem Text="Manipur" Value="MAN"></asp:ListItem>
                                                                                            <asp:ListItem Text="Orissa" Value="ORS"></asp:ListItem>
                                                                                            <asp:ListItem Text="Punjab" Value="PNJ"></asp:ListItem>
                                                                                            <asp:ListItem Text="Pondicherry" Value="PND"></asp:ListItem>
                                                                                            <asp:ListItem Text="Rajasthan" Value="RAJ"></asp:ListItem>
                                                                                            <asp:ListItem Text="Sikkim" Value="SKM"></asp:ListItem>
                                                                                            <asp:ListItem Text="Telangana" Value="TLG"></asp:ListItem>
                                                                                            <asp:ListItem Text="Tamil Nadu" Value="TMN"></asp:ListItem>
                                                                                            <asp:ListItem Text="Tripura" Value="TRP"></asp:ListItem>
                                                                                            <asp:ListItem Text="Uttar Pradesh" Value="UTP"></asp:ListItem>
                                                                                            <asp:ListItem Text="Uttarakhand" Value="UTR"></asp:ListItem>
                                                                                            <asp:ListItem Text="West Bengal" Value="WEB"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                            </div>
                                                                                          <div id="state11" class="validation-error">
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="Dynamic" ValidationGroup="validate" ControlToValidate="ddlState" ForeColor="Red"
                                                                                            ErrorMessage="Select State" InitialValue="0"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                              </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                
                                                                                <asp:TemplateField HeaderText="No. of Employee(s)" HeaderStyle-Width="10%" ItemStyle-Width="10%"  ItemStyle-CssClass="narrow" HeaderStyle-CssClass="narrow"> 
                                                                                    <ItemTemplate>
                                                                                        <div id="no_of_emp1" style="margin-top:10px;"> 
                                                                                        <asp:TextBox   ID="txtEmpCount" placeholder=" Employee Count" Class="form-control" Text='<%# Eval("EmpCount") %>' runat="server" MaxLength="18"></asp:TextBox>
                                                                                         </div>
                                                                                            <div id="no_of_emp11" class="validation-error">
                                                                                         <asp:RequiredFieldValidator  CssClass="validation-error" ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmpCount" Display="Dynamic" ForeColor="Red" ValidationGroup="validate" ErrorMessage="Please Enter Employee Count"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator  CssClass="validation-error" ID="RegularExpressionValidator4" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic" ControlToValidate="txtEmpCount" ForeColor="Red" ErrorMessage="Enter only Numbers"></asp:RegularExpressionValidator>
                                                                                    </div>
                                                                                             </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Min. Basic Salary" HeaderStyle-Width="10%" ItemStyle-Width="10%"  ItemStyle-CssClass="narrow" HeaderStyle-CssClass="narrow">
                                                                                    <ItemTemplate>
                                                                                           <div id="minbas1" style="margin-top:10px;"> 
                                                                                        <asp:TextBox  ID="txtMinBasic" Class="form-control" placeholder=" Min Basic Salary" Text='<%# Eval("MinBasic") %>' runat="server" MaxLength="18"></asp:TextBox>
                                                                                          </div>
                                                                                        <div id="minbas11"  class="validation-error">
                                                                                        <asp:RequiredFieldValidator   CssClass="validation-error" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMinBasic" Display="Dynamic" ForeColor="Red" ValidationGroup="validate" ErrorMessage="Please Enter Min Basic Salary"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator  CssClass="validation-error" ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[0-9]*\.?[0-9]+$" Display="Dynamic" ControlToValidate="txtMinBasic" ForeColor="Red" ErrorMessage="Only numbers are allowded"></asp:RegularExpressionValidator>
                                                                                    </div>
                                                                                              </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Min. Gross Salary" HeaderStyle-Width="10%" ItemStyle-Width="10%"  ItemStyle-CssClass="narrow" HeaderStyle-CssClass="narrow">
                                                                                    <ItemTemplate>
                                                                                        <div id="mingross1" style="margin-top:10px;"> 
                                                                                         <asp:TextBox   ID="txtMinGross" Class="form-control" placeholder=" Min Gross Salary" Text='<%# Eval("MinGross") %>' runat="server" MaxLength="18"></asp:TextBox>
                                                                                       </div>
                                                                                         <div id="mingross11" class="validation-error">
                                                                                             <asp:RequiredFieldValidator  CssClass="validation-error" ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMinGross" Display="Dynamic" ForeColor="Red" ValidationGroup="validate" ErrorMessage="Please Enter Min Gross Salary"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator  CssClass="validation-error" ID="RegularExpressionValidator2" runat="server" ValidationExpression="^[0-9]*\.?[0-9]+$" Display="Dynamic" ControlToValidate="txtMinGross" ForeColor="Red" ErrorMessage="Only numbers are allowded"></asp:RegularExpressionValidator>
                                                                                       <asp:CompareValidator ID="cm1"
                                                                                             CssClass="validation-error"
                                                                                            ControlToValidate="txtMinGross"
                                                                                            ControlToCompare="txtMinBasic"
                                                                                            Display="Dynamic"
                                                                                            Text="Min Gross must be greater than Min Basic!"
                                                                                            Operator="GreaterThan"
                                                                                            Type="Double"
                                                                                            ForeColor="Red"
                                                                                            runat="Server" ValidationGroup="validate" />
                                                                                             </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Max. Gross Salary" HeaderStyle-Width="10%" ItemStyle-Width="10%"  ItemStyle-CssClass="narrow" HeaderStyle-CssClass="narrow">
                                                                                    <ItemTemplate>
                                                                                       <div id="maxgross1" style="margin-top:10px;">
                                                                                        <asp:TextBox   ID="txtMaxGross" Class="form-control" placeholder=" Max Gross Salary" Text='<%# Eval("MaxGross") %>' runat="server" MaxLength="18"></asp:TextBox>
                                                                                     </div>
                                                                                             <div  id="maxgross11" class="validation-error">
                                                                                         <asp:RequiredFieldValidator  ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMaxGross" Display="Dynamic" ForeColor="Red" ValidationGroup="validate" ErrorMessage="Please Enter Max Gross Salary"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator   ID="RegularExpressionValidator3" runat="server" ValidationExpression="^[0-9]*\.?[0-9]+$" Display="Dynamic" ControlToValidate="txtMaxGross" ForeColor="Red" ValidationGroup="validate" ErrorMessage="Only numbers are allowded"></asp:RegularExpressionValidator>
                                                                                        <asp:CompareValidator
                                                                                            ID="cm2"
                                                                                            ControlToValidate="txtMaxGross"
                                                                                            ControlToCompare="txtMinGross"
                                                                                            Display="Dynamic"
                                                                                            ForeColor="Red"
                                                                                            Text="Max Gross must be greater than Min Gross!"
                                                                                            Operator="GreaterThan"
                                                                                            Type="Double"
                                                                                            runat="Server" ValidationGroup="validate" />
                                                                                    </div></ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center narrow" ItemStyle-CssClass="text-center narrow">
                                                                                    <ItemTemplate>
                                                                                         <div id="actionpane" style="margin-top:10px;">
                                                                                        <asp:ImageButton ID="ButtonAdd" OnClick="ButtonAdd_Click" Height="100%" CssClass="rowrepeat" ValidationGroup="validate" ImageUrl="~/img/Add.png" runat="server" ToolTip="Add New Branch" data-toggle="tooltip" data-placement="bottom" />
                                                                                        <asp:ImageButton ID="lnkBtnDelete" OnClick="LinkButton1_Click" Height="100%" ValidationGroup="delete" ImageUrl="~/img/Delete_Icon.png" runat="server" OnClientClick="if ( ! UserDeleteConfirmation()) return false;" meta:resourcekey="BtnUserDeleteResource1" ToolTip="Delete" data-toggle="tooltip" data-placement="bottom" />
                                                                                     </div>
                                                                                             </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </div>

                                                                <div class="col-md-12 colpadding0 text-right">
                                                                    <asp:Button ID="Button1" class="btn btn-primary" ValidationGroup="validate" OnClick="Button1_Click" runat="server" Text="Save" />
                                                                </div>
                                                            </div>

                                                        </ContentTemplate>
                                                        <Triggers>
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>

                                            <div class="col-md-12 colpadding0">
                                                <asp:GridView ID="GridView2" CssClass="table" runat="server" Visible="false">
                                                </asp:GridView>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:View>

                        <asp:View ID="ApplicabilityView" runat="server">
                            <div class="container">
                                <div id="divApplicability" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="container plr0">
                                                    <div class="row col-md-12 colpadding0 mb10">
                                                        <div class="col-md-3 pl0">
                                                            <%--<label for="ddlEntityBranch" class="control-label">Entity/Branch</label>--%>
                                                            <input id="ddlEntityBranch" data-placeholder="type" style="width: 100%;" />
                                                        </div>
                                                        <div class="col-md-2 pl0">
                                                            <%--<label for="ddlDocType" class="control-label">Type</label>--%>
                                                            <input id="ddlDocType" data-placeholder="type" style="width: 100%;" />
                                                        </div>
                                                        <div class="col-md-3 pl0">
                                                            <%--<label for="ddlDocType" class="control-label">State</label>--%>
                                                            <input id="ddlState" data-placeholder="State" style="width: 100%;" />
                                                        </div>
                                                        <div class="col-md-2 pull-right" style="margin-right:-50px;">
                                                            <%--<label for="ddlDocType" class="control-label">Act</label>--%>
                                                              <button id="btnClearFilter" class="btn btn-primary" onclick="ClearFilter(event)"style="height:36px;margin-left:-5px;display:none;font-weight:400"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"style="margin-top:-3px;"></span>Clear Filter</button>
        
                                                            <button id="exportGrid_ApplicableCompliance" style="margin-right: 5px; float:right; font-weight:400" class="btn btn-primary"><span class="k-icon k-i-excel"></span>Export</button>
                                                            <input id="ddlAct" data-placeholder="Act" style="width: 100%; display:none" />
                                                        </div>
                                                    </div>

                                                    <div id="gridApplicableCompliance" <%--style="border: none;"--%>></div>

                                                    <div class="row col-md-12 colpadding0 mt10 text-right">
                                                        <button id="btnSaveCompliance" class="btn btn-primary" style="display: none;">Save & Next</button>
                                                    </div>

                                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                                    <input id="btnShow1" type="button" value="Show View 1" onclick="OnClientClick('butSubmit', 'HiddenField1', '3')" style="display: none" />
                                                    <input id="btnShow2" type="button" value="Show View 2" onclick="OnClientClick('butSubmit', 'HiddenField1', '1')" style="display: none" />
                                                    <div style="display: none">
                                                        <asp:Button ID="butSubmit" runat="server" OnClick="butSubmit_Click" Text="Submit" />
                                                    </div>
                                                    <%--<div class="col-md-1">
                                                            
                                                        </div>--%>
                                                    <%--<div class="col-md-11" >
                                                            <label id="lblSelectedComplianceCount" class="control-label"></label>
                                                        </div>--%>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:View>

                        <asp:View ID="PricingView" runat="server">
                            <div class="container">
                                <div id="divPricing" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="container plr0">
                                                    <div class="row col-md-12 colpadding0 mb10">
                                                        <asp:ValidationSummary ID="vsPricingTab" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                            ValidationGroup="PricingTab" />
                                                        <asp:CustomValidator ID="cvPricingTab" runat="server" EnableClientScript="False"
                                                            ValidationGroup="PricingTab" Display="none" class="alert alert-block alert-danger fade in" />
                                                    </div>

                                                    <div class="row col-md-12 colpadding0 mb10">
                                                        <div class="col-md-4 pl0">
                                                            <%--<label for="ddlEntityBranch_Pricing" class="control-label">Entity/Branch</label>--%>
                                                            <input id="ddlEntityBranch_Pricing" data-placeholder="type" style="width: 100%; display:none;" />
                                                        </div>

                                                        <div class="col-md-8 colpadding0 text-right">                                                            
                                                            <button id="exportPricingGrid" class="btn btn-primary"><span class="k-icon k-i-excel"></span>Export</button>
                                                        </div>
                                                    </div>

                                                    <div id="gridPricing" ></div>

                                                    <div class="row col-md-12 colpadding0 mt10 text-right">
                                                        <button id="btnSavePricing" class="btn btn-primary">Save</button>
                                                        <asp:Button Text="Next" runat="server" ID="btnNextPricing" CssClass="btn btn-primary" OnClick="btnNextPricing_Click" Visible="false"></asp:Button>
                                                        <asp:Button Text="Onboard as Customer" runat="server" ID="btnContinueAsCustomer" CssClass="btn btn-primary" OnClick="btnContinueAsCustomer_Click"></asp:Button>
                                                        <%--<button id="btnPayNow" class="btn btn-primary">Next</button>--%>
                                                    </div>

                                                    <div id="divShowPricingPlan">
                                                        <iframe id="iframePricingPlan" style="width: 100%; height: 98%; border: none;"></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:View>

                        <asp:View ID="PaymentView" runat="server">
                            <div class="container">
                                <div id="divPayment" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="container plr0">
                                                    <div class="row">
                                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                            ValidationGroup="PopUpValidationGroup" />
                                                        <asp:CustomValidator ID="cvPayment" runat="server" EnableClientScript="False"
                                                            ValidationGroup="PopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                    </div>

                                                    <div class="row col-md-12 colpadding0 mb10">
                                                        <div class="col-md-12 text-center mt-1">
                                                            <div class="col-md-4">
                                                                <div class="col-md-12">
                                                                    <h5 class="control-label">Payment Method</h5>
                                                                </div>

                                                                <div class="checkbox text-left">
                                                                    <label>
                                                                        <input type="radio" class="m10" checked="checked" />
                                                                        <img src="/images/paytm.png" width="100px" alt="PayTM" title="PayTM" class="m5all" /><br>
                                                                    </label>
                                                                </div>

                                                                <div class="checkbox text-left">
                                                                    <label>
                                                                        <input type="radio" class="m10" />
                                                                        <i class="fa fa-forwardslash fa-2x"></i>
                                                                        <img src="/images/netbanking.png" width="50px" alt="NetBanking" title="PayUMoney" class="m5all" /><br>
                                                                    </label>
                                                                </div>

                                                                <div class="checkbox text-left">
                                                                    <label>
                                                                        <input type="radio" class="m10" />
                                                                        <img src="/images/mobikwik.png" width="100px" alt="MobiKwik" title="MobiKwik" class="m5all" /><br>
                                                                    </label>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-8 callout">
                                                                <div class="form-group row">
                                                                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                                                        <label class="control-label">Order ID</label>
                                                                    </div>
                                                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                                                        <asp:TextBox ID="txtOrderID" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                                                        <label class="control-label">Mobile</label>
                                                                    </div>
                                                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                                                        <asp:TextBox ID="txtPaymentMobile" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <%--<input type="text" id="CustMobile" name="userMobile" class="form-control" readonly />--%>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                                                        <label class="control-label">Email</label>
                                                                    </div>
                                                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                                                        <asp:TextBox ID="txtPaymentEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <%--<input type="text" id="CustEmail" name="userEmail" class="form-control" readonly />--%>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                                                        <label class="control-label">Date</label>
                                                                    </div>
                                                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                                                        <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <%--<input type="text" id="TxnDate" name="TxnDate" class="form-control" readonly />--%>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                                                        <label class="control-label">Amount</label>
                                                                    </div>
                                                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                                                        <asp:TextBox ID="txtPaymentAmount" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <%--<input type="text" id="txtAmount" name="Amount" class="form-control" readonly />--%>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12 text-center mb-1">
                                                                <asp:Button ID="btnProceedPayment" runat="server" CssClass="btn btn-primary" Text="Proceed to Payment"
                                                                    OnClick="btnProceedPayment_Click" CausesValidation="false"></asp:Button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
