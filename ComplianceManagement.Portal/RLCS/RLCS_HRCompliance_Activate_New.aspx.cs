﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_HRCompliance_Activate_New : System.Web.UI.Page
    {
        public static List<int> branchList = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            ValidationSummary1.CssClass = "alert alert-danger";
            if (!IsPostBack)
            {
                record1.Visible = false;

                BindCustomers();
                BindLocationFilter();

                if (Session["TotalComplianceActivate"] != null)
                    TotalRows.Value = Session["TotalComplianceActivate"].ToString();

                tbxFilterLocation.Text = "Entity/Branch";
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string masterpage = Convert.ToString(Session["masterpage"]);
            if (masterpage == "HRPlusSPOCMGR")
            {
                this.Page.MasterPageFile = "~/HRPlusSPOCMGR.Master";
            }
            else if (masterpage == "HRPlusCompliance")
            {
                this.Page.MasterPageFile = "~/HRPlusCompliance.Master";
            }
        }

        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                
                //ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2, false);
                ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomers(AuthenticationHelper.UserID, customerID, serviceProviderID, distributorID, AuthenticationHelper.Role, false);                
                ddlCustomer.DataBind();

                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomer.Items.FindByValue(customerID.ToString()) != null)
                    {
                        ddlCustomer.ClearSelection();
                        ddlCustomer.Items.FindByValue(customerID.ToString()).Selected = true;

                        if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                            btnReAssign.Enabled = true;
                        else
                            btnReAssign.Enabled = false;
                    }                        
                }

                ddlCustomer.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    btnReAssign.Visible = true;
                else
                    btnReAssign.Visible = false;

                BindLocationFilter();
                tvFilterLocation_SelectedNodeChanged(sender, e);

                BindGrid(-1);
                GetPageDisplaySummary();

                ForceCloseFilterBranchesTreeView();

                upComplianceTypeList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else 
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}

                if (customerID != -1)
                {
                    //var bracnhes = RLCS_Master_Management.GetAll_Entities(customerID);
                    var bracnhes = RLCS_ClientsManagement.GetAllHierarchy(customerID);

                    tvFilterLocation.Nodes.Clear();
                    TreeNode node = new TreeNode("Entity/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();

                    tvFilterLocation.CollapseAll();
                    //tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                //record1.Visible = true;
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                int nCustomerBranchID = -1;
                nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                BindGrid(nCustomerBranchID);
                GetPageDisplaySummary();
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                //    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
                //    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void tbxStartDate_TextChanged(object sender, EventArgs e)
        {

            setDateToGridView();
            ForceCloseFilterBranchesTreeView();
        }

        private void BindGrid(int CustomerBranchID)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}
                
                if (customerID != -1)
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                    branchList.Clear();
                    GetAll_Branches(customerID, CustomerBranchID);
                    branchList.ToList();

                    var dataSource = Business.ComplianceManagement.GetTempAssignedDetails(branchList);

                    if (!string.IsNullOrEmpty(ddlScopeType.SelectedValue))
                    {
                        if (ddlScopeType.SelectedValue != "-1")
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var complianceIDs = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance()
                                                     where row.IsDeleted == false
                                                     && row.ScopeID == ddlScopeType.SelectedValue
                                                     //&& (row.ComplianceType == 0 || row.ComplianceType == 2)
                                                     && row.EventFlag == null && row.Status == null
                                                     select row.ID).Distinct().ToList();

                                if (complianceIDs.Count > 0 && dataSource.Count > 0)
                                    dataSource = dataSource.Where(row => complianceIDs.Contains(row.ComplianceID)).ToList();
                            }
                        }
                    }

                    if (dataSource.Count > 0)
                    {
                        btnSave.Visible = true;
                        grdComplianceRoleMatrix.Visible = true;
                        grdComplianceRoleMatrix.DataSource = dataSource;
                        Session["TotalComplianceActivate"] = 0;
                        Session["TotalComplianceActivate"] = dataSource.Count();
                        grdComplianceRoleMatrix.DataBind();

                        grdToDeactivate.DataSource = null;
                        grdToDeactivate.Visible = false;
                        grdToDeactivate.DataBind();
                    }

                    else
                    {
                        btnSave.Visible = false;
                        grdComplianceRoleMatrix.DataSource = null;
                        Session["TotalComplianceActivate"] = 0;
                        Session["TotalComplianceActivate"] = dataSource.Count();
                        grdComplianceRoleMatrix.DataBind();
                    }

                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }

                record1.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindDeactivateGrid(int CustomerBranchID)
        {
            try
            {
                var dataSource = Business.ComplianceManagement.GetDetailsToDeactive(CustomerBranchID);

                grdToDeactivate.Visible = true;
                grdToDeactivate.DataSource = dataSource;
                grdToDeactivate.DataBind();

                grdComplianceRoleMatrix.DataSource = null;
                grdComplianceRoleMatrix.Visible = false;
                grdComplianceRoleMatrix.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void chkActivateSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                CheckBox chkActivateSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkActivateSelectAll");
                foreach (GridViewRow row in grdComplianceRoleMatrix.Rows)
                {
                    CheckBox chkActivate = (CheckBox)row.FindControl("chkActivate");
                    if (chkActivateSelectAll.Checked == true)
                    {
                        chkActivate.Checked = true;
                    }
                    else
                    {
                        chkActivate.Checked = false;
                    }
                }
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }

        }

        protected void chkActivate_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chkActivateSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkActivateSelectAll");
                int countCheckedCheckbox = 0;
                for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
                {
                    GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                    if (((CheckBox)row.FindControl("chkActivate")).Checked)
                    {

                        countCheckedCheckbox = countCheckedCheckbox + 1;

                    }
                }
                if (countCheckedCheckbox == grdComplianceRoleMatrix.Rows.Count)
                {
                    chkActivateSelectAll.Checked = true;
                }
                else
                {
                    chkActivateSelectAll.Checked = false;
                }
                ForceCloseFilterBranchesTreeView();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }

        }

        protected void ddlScopeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlScopeType.SelectedValue) && !string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
            {
                int nCustomerBranchID = -1;
                nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                BindGrid(nCustomerBranchID);
                GetPageDisplaySummary();
                ForceCloseFilterBranchesTreeView();
            }
        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (tbxStartDate.Text != "")
                {
                    SaveCheckedValues();

                    int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    BindGrid(nCustomerBranchID);

                    grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                    grdComplianceRoleMatrix.DataBind();
                    if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && tbxStartDate.Text != null)
                    {
                        setDateToGridView();
                    }
                    PopulateCheckedValues();
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select start date";
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<TempComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<TempComplianceAsignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        //int index = Convert.ToInt32(((Label)gvrow.FindControl("lblComplianceID")).Text);
                        TempComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ID == index).FirstOrDefault();
                        if (rmdata != null)
                        {
                            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkActivate");
                            chkPerformer.Checked = rmdata.Performer;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //    int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                //    var ComplianceRoleMatrixList = Business.ComplianceManagement.GetTempAssignedDetails(nCustomerBranchID);

                //    if (direction == SortDirection.Ascending)
                //    {
                //        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //        direction = SortDirection.Descending;
                //    }
                //    else
                //    {
                //        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //        direction = SortDirection.Ascending;
                //    }


                //    foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
                //    {
                //        if (field.SortExpression == e.SortExpression)
                //        {
                //            ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
                //        }
                //    }


                //    grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
                //    grdComplianceRoleMatrix.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        //public SortDirection direction
        //{
        //    get
        //    {
        //        if (ViewState["dirState"] == null)
        //        {
        //            ViewState["dirState"] = SortDirection.Ascending;
        //        }
        //        return (SortDirection)ViewState["dirState"];
        //    }
        //    set
        //    {
        //        ViewState["dirState"] = value;
        //    }
        //}
        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddMatrixSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        private void SaveCheckedValues()
        {
            try
            {
                List<TempComplianceAsignmentProperties> complianceList = new List<TempComplianceAsignmentProperties>();

                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    TempComplianceAsignmentProperties complianceProperties = new TempComplianceAsignmentProperties();
                    complianceProperties.ID = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkActivate")).Checked;
                    complianceProperties.ComplianceId = Convert.ToInt32(((Label)gvrow.FindControl("lblComplianceID")).Text);
                    complianceProperties.Role = ((Label)gvrow.FindControl("lblRole")).Text;
                    complianceProperties.UserID = Convert.ToInt32(((Label)gvrow.FindControl("lblUserID")).Text);
                    complianceProperties.StartDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString(); // ((CheckBox)gvrow.FindControl("chkActivate")).Checked;
                    complianceProperties.CustomerBranchID = Convert.ToInt32(((Label)gvrow.FindControl("lblBranchID")).Text);

                    // Check in the Session
                    if (ViewState["CHECKED_ITEMS"] != null)
                        complianceList = ViewState["CHECKED_ITEMS"] as List<TempComplianceAsignmentProperties>;

                    if (complianceProperties.Performer)
                    {
                        TempComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ID == complianceProperties.ID).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        TempComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ID == complianceProperties.ID).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }
                }

                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        //private void SaveCheckedValues()
        //{
        //    try
        //    {

        //        List<ComplianceAsignmentProperties> complianceList = new List<ComplianceAsignmentProperties>();
        //        foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
        //        {
        //            ComplianceAsignmentProperties complianceProperties = new ComplianceAsignmentProperties();
        //            complianceProperties.ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
        //            // complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkPerformer")).Checked;
        //            //complianceProperties.Reviewer1 = ((CheckBox)gvrow.FindControl("chkReviewer1")).Checked;
        //            //complianceProperties.Reviewer2 = ((CheckBox)gvrow.FindControl("chkReviewer2")).Checked;
        //            //complianceProperties.StartDate = Request[((TextBox)gvrow.FindControl("txtStartDate")).UniqueID].ToString();
        //            // Check in the Session
        //            if (ViewState["CHECKED_ITEMS"] != null)
        //                complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

        //            if (complianceProperties.Performer || complianceProperties.Reviewer1)
        //            {
        //                ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
        //                if (rmdata != null)
        //                {
        //                    complianceList.Remove(rmdata);
        //                    complianceList.Add(complianceProperties);
        //                }
        //                else
        //                {
        //                    complianceList.Add(complianceProperties);
        //                }
        //            }
        //            else
        //            {
        //                ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
        //                if (rmdata != null)
        //                    complianceList.Remove(rmdata);
        //            }

        //        }
        //        if (complianceList != null && complianceList.Count > 0)
        //            ViewState["CHECKED_ITEMS"] = complianceList;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}

        //private void PopulateCheckedValues()
        //{
        //    try
        //    {
        //        List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

        //        if (complianceList != null && complianceList.Count > 0)
        //        {
        //            foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
        //            {
        //                int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
        //                ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == index).FirstOrDefault();
        //                if (rmdata != null)
        //                {
        //                    CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkPerformer");
        //                    CheckBox chkReviewer1 = (CheckBox)gvrow.FindControl("chkReviewer1");
        //                    //CheckBox chkReviewer2 = (CheckBox)gvrow.FindControl("chkReviewer2");
        //                    TextBox txtStartDate = (TextBox)gvrow.FindControl("txtStartDate");
        //                    chkPerformer.Checked = rmdata.Performer;
        //                    chkReviewer1.Checked = rmdata.Reviewer1;
        //                    //chkReviewer2.Checked = rmdata.Reviewer2;
        //                    txtStartDate.Text = rmdata.StartDate;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}
                
                if (customerID != -1)
                {
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                    {
                        if (!string.IsNullOrEmpty(tbxStartDate.Text.Trim()))
                        {
                            //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                            var complianceList = new List<TempComplianceAsignmentProperties>();
                            SaveCheckedValues();
                            complianceList = ViewState["CHECKED_ITEMS"] as List<TempComplianceAsignmentProperties>;

                            if (complianceList != null)
                            {
                                if (complianceList.Count > 0)
                                {
                                    int selectedbranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                                    //int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;

                                    if (chkToActiveInstance.Checked)
                                    {
                                        List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();

                                        //Entity Assignment
                                        List<EntitiesAssignment> assignmentEntities = new List<EntitiesAssignment>();

                                        List<int> lstComplianceCategory = new List<int>();
                                        lstComplianceCategory.Add(2);
                                        lstComplianceCategory.Add(5);

                                        bool recordActive = true;
                                        List<int> selectedBranchList = new List<int>();

                                        for (int i = 0; i < complianceList.Count; i++)
                                        {
                                            assignments.Clear();
                                            int TempAssignmentID = complianceList[i].ID;
                                            int complianceID = complianceList[i].ComplianceId;

                                            assignmentEntities.Clear();
                                            selectedBranchList.Clear();

                                            if (!string.IsNullOrEmpty(complianceList[i].StartDate))
                                            {
                                                DateTime dtStartDate = Convert.ToDateTime(complianceList[i].StartDate);
                                                string StartDate = Convert.ToDateTime(dtStartDate).ToString("dd-MM-yyyy");
                                                int userID = complianceList[i].UserID;
                                                int branchID = complianceList[i].CustomerBranchID;

                                                selectedBranchList.Add(branchID);

                                                ComplianceInstance instance = new ComplianceInstance();
                                                instance.ComplianceId = complianceID;
                                                instance.CustomerBranchID = branchID;
                                                instance.ScheduledOn = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                                if (complianceList[i].Role == "Performer")
                                                {
                                                    ComplianceAssignment assignment = new ComplianceAssignment();
                                                    assignment.UserID = userID;
                                                    assignment.RoleID = 3; //RoleManagement.GetByCode("PERF").ID;
                                                    assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment));

                                                    lstComplianceCategory.ForEach(eachCategory =>
                                                    {
                                                        var data = AssignEntityManagement.SelectEntity(branchID, userID, eachCategory); //GG Add validation condition
                                                        if (data == null)
                                                        {
                                                            EntitiesAssignment objEntitiesAssignment = new EntitiesAssignment();
                                                            objEntitiesAssignment.UserID = userID;
                                                            objEntitiesAssignment.BranchID = branchID;
                                                            objEntitiesAssignment.ComplianceCatagoryID = eachCategory;
                                                            objEntitiesAssignment.CreatedOn = DateTime.Now;

                                                            assignmentEntities.Add(objEntitiesAssignment);

                                                            RLCS_Master_Management.CreateUpdate_RLCS_EntityAssignment(userID, selectedBranchList, recordActive, lstComplianceCategory);
                                                        }
                                                    });
                                                }

                                                if (complianceList[i].Role == "Reviewer")
                                                {
                                                    ComplianceAssignment assignment1 = new ComplianceAssignment();
                                                    assignment1.UserID = userID;
                                                    assignment1.RoleID = 4; // RoleManagement.GetByCode("RVW1").ID;
                                                    assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));

                                                    lstComplianceCategory.ForEach(eachCategory =>
                                                    {
                                                        var data = AssignEntityManagement.SelectEntity(branchID, userID, eachCategory); //GG Add validation condition
                                                        if (data == null)
                                                        {
                                                            EntitiesAssignment objEntitiesAssignment = new EntitiesAssignment();
                                                            objEntitiesAssignment.UserID = userID;
                                                            objEntitiesAssignment.BranchID = branchID;
                                                            objEntitiesAssignment.ComplianceCatagoryID = eachCategory;
                                                            objEntitiesAssignment.CreatedOn = DateTime.Now;

                                                            assignmentEntities.Add(objEntitiesAssignment);

                                                            RLCS_Master_Management.CreateUpdate_RLCS_EntityAssignment(userID, selectedBranchList, recordActive, lstComplianceCategory);
                                                        }
                                                    });
                                                }

                                                if (complianceList[i].Role == "Approver")
                                                {
                                                    ComplianceAssignment assignment1 = new ComplianceAssignment();
                                                    assignment1.UserID = userID;
                                                    assignment1.RoleID = 6; //RoleManagement.GetByCode("APPR").ID;
                                                    assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));

                                                    lstComplianceCategory.ForEach(eachCategory =>
                                                    {
                                                        var data = AssignEntityManagement.SelectEntity(branchID, userID, eachCategory); //GG Add validation condition
                                                        if (data == null)
                                                        {
                                                            EntitiesAssignment objEntitiesAssignment = new EntitiesAssignment();
                                                            objEntitiesAssignment.UserID = userID;
                                                            objEntitiesAssignment.BranchID = branchID;
                                                            objEntitiesAssignment.ComplianceCatagoryID = eachCategory;
                                                            objEntitiesAssignment.CreatedOn = DateTime.Now;

                                                            assignmentEntities.Add(objEntitiesAssignment);

                                                            RLCS_Master_Management.CreateUpdate_RLCS_EntityAssignment(userID, selectedBranchList, recordActive, lstComplianceCategory);
                                                        }
                                                    });
                                                }

                                                if (assignments.Count != 0)     //GG 05NOV2019
                                                {
                                                    var instanceSuccess = Business.ComplianceManagement.CreateInstances(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User, customerID);
                                                    if (instanceSuccess)
                                                    {
                                                        Business.ComplianceManagement.DeactiveTempAssignmentDetails(TempAssignmentID);
                                                        if (assignmentEntities.Count > 0)
                                                            AssignEntityManagement.Create(assignmentEntities);
                                                    }
                                                }
                                            }
                                        }

                                        //if (assignments.Count != 0)
                                        //{
                                        //    Business.ComplianceManagement.CreateInstances(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User, customerID);

                                        //    if (assignmentEntities.Count > 0)
                                        //        AssignEntityManagement.Create(assignmentEntities);
                                        //}
                                        //else
                                        //{
                                        //    cvDuplicateEntry.IsValid = false;
                                        //    cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
                                        //    setDateToGridView();
                                        //}

                                        BindGrid(selectedbranchID);
                                        GetPageDisplaySummary();
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Compliance Activated successfully.";
                                        ValidationSummary1.CssClass = "alert alert-success";
                                    }
                                    else
                                    {
                                        for (int i = 0; i < grdToDeactivate.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdToDeactivate.Rows[i];
                                            int TempAssignmentID = Convert.ToInt32(grdToDeactivate.DataKeys[row.RowIndex]["ID"]);
                                            Label lblComplianceInstanceID = (Label)row.FindControl("lblComplianceInstanceID");
                                            CheckBox chkDeActivate = (CheckBox)row.FindControl("chkDeActivate");

                                            if (chkDeActivate.Checked)
                                            {
                                                int dComplianceInstanceID = Convert.ToInt32(lblComplianceInstanceID.Text);
                                                Business.ComplianceManagement.ActivateTempAssignmentDetails(TempAssignmentID, dComplianceInstanceID);
                                            }
                                        }

                                        BindDeactivateGrid(selectedbranchID);
                                    }

                                    tbxStartDate.Text = "";
                                    ViewState["CHECKED_ITEMS"] = null;
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Select at least one compliance to assign";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Select at least one compliance to assign";
                            }
                        }
                    }
                }
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {


        //        int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim()).ID;
        //        if (chkToActiveInstance.Checked == true)
        //        {
        //            List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();
        //            //int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim()).ID;
        //            for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
        //            {

        //                //List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();

        //                GridViewRow row = grdComplianceRoleMatrix.Rows[i];
        //                int TempAssignmentID = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[row.RowIndex]["ID"]);
        //                Label lblComplianceID = (Label)row.FindControl("lblComplianceID");
        //                int complianceID = Convert.ToInt32(lblComplianceID.Text);
        //                //int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim()).ID;
        //                TextBox txtStartDate = (TextBox)row.FindControl("txtStartDate");
        //                //DateTime dtStartDate = Convert.ToDateTime(txtStartDate.Text);
        //                CheckBox chkActivate = (CheckBox)row.FindControl("chkActivate");
        //                Label lblRole = (Label)row.FindControl("lblRole");
        //                Label lblUserID = (Label)row.FindControl("lblUserID");
        //                int userID = Convert.ToInt32(lblUserID.Text);
        //                if (chkActivate.Checked)
        //                {

        //                    ComplianceInstance instance = new ComplianceInstance();
        //                    instance.ComplianceId = complianceID;
        //                    instance.CustomerBranchID = branchID;
        //                    instance.ScheduledOn = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                    //if (complianceList[i].Performer)
        //                    if (lblRole.Text == "Performer")
        //                    {

        //                        ComplianceAssignment assignment = new ComplianceAssignment();
        //                        assignment.UserID = userID;
        //                        assignment.RoleID = RoleManagement.GetByCode("PERF").ID;
        //                        assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment));
        //                    }
        //                    if (lblRole.Text == "Reviewer")
        //                    {

        //                        ComplianceAssignment assignment1 = new ComplianceAssignment();
        //                        assignment1.UserID = userID;
        //                        assignment1.RoleID = RoleManagement.GetByCode("RVW1").ID;
        //                        assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
        //                    }
        //                    if (lblRole.Text == "Approver")
        //                    {
        //                        ComplianceAssignment assignment1 = new ComplianceAssignment();
        //                        assignment1.UserID = userID;
        //                        assignment1.RoleID = RoleManagement.GetByCode("APPR").ID;
        //                        assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
        //                    }
        //                    Business.ComplianceManagement.DeactiveTempAssignmentDetails(TempAssignmentID);
        //                }
        //            }
        //            if (assignments.Count != 0)
        //            {
        //                Business.ComplianceManagement.CreateInstances(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User);

        //            }
        //            else
        //            {
        //                cvDuplicateEntry.IsValid = false;
        //                cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
        //                setDateToGridView();
        //            }


        //            BindGrid(branchID);

        //        }
        //        else
        //        {
        //            for (int i = 0; i < grdToDeactivate.Rows.Count; i++)
        //            {

        //                GridViewRow row = grdToDeactivate.Rows[i];
        //                int TempAssignmentID = Convert.ToInt32(grdToDeactivate.DataKeys[row.RowIndex]["ID"]);
        //                Label lblComplianceInstanceID = (Label)row.FindControl("lblComplianceInstanceID");
        //                CheckBox chkDeActivate = (CheckBox)row.FindControl("chkDeActivate");

        //                if (chkDeActivate.Checked)
        //                {
        //                    int dComplianceInstanceID = Convert.ToInt32(lblComplianceInstanceID.Text);
        //                    Business.ComplianceManagement.ActivateTempAssignmentDetails(TempAssignmentID, dComplianceInstanceID);
        //                }
        //            }
        //            BindDeactivateGrid(branchID);

        //        }

        //        tbxStartDate.Text = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
        //    }


        //}

        private void setDateToGridView()
        {
            try
            {
                for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
                {
                    TextBox txt = (TextBox)grdComplianceRoleMatrix.Rows[i].FindControl("txtStartDate");
                    txt.Text = tbxStartDate.Text;
                }

                DateTime date = DateTime.Now;
                if (!string.IsNullOrEmpty(tbxStartDate.Text.Trim()))
                {
                    date = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void chkToActiveInstance_CheckedChanged(object sender, EventArgs e)
        {
            if (chkToActiveInstance.Checked == true)
            {
                if (tvFilterLocation.SelectedValue != "")
                {
                    int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    BindGrid(nCustomerBranchID);
                    GetPageDisplaySummary();
                }

                chkToDeActiveInstance.Checked = false;
            }
            else
            {
                grdComplianceRoleMatrix.Visible = false;
                grdComplianceRoleMatrix.DataSource = null;
                grdComplianceRoleMatrix.DataBind();
            }
        }

        protected void chkToDeActiveInstance_CheckedChanged(object sender, EventArgs e)
        {
            if (chkToDeActiveInstance.Checked == true)
            {
                if (chkToDeActiveInstance.Checked == true)
                {
                    if (tvFilterLocation.SelectedValue != "")
                    {
                        int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        BindDeactivateGrid(nCustomerBranchID);
                    }
                    chkToActiveInstance.Checked = false;
                }
            }
            else
            {
                grdToDeactivate.Visible = false;
                grdToDeactivate.DataSource = null;
                grdToDeactivate.DataBind();
            }
        }

        protected void chkDeActivateSelectAll_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox chkDeActivateSelectAll = (CheckBox)grdToDeactivate.HeaderRow.FindControl("chkDeActivateSelectAll");
            foreach (GridViewRow row in grdToDeactivate.Rows)
            {
                CheckBox chkDeActivate = (CheckBox)row.FindControl("chkDeActivate");
                if (chkDeActivateSelectAll.Checked == true)
                {
                    chkDeActivate.Checked = true;
                }
                else
                {
                    chkDeActivate.Checked = false;
                }
            }

        }

        protected void chkDeActivate_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkDeActivateSelectAll = (CheckBox)grdToDeactivate.HeaderRow.FindControl("chkDeActivateSelectAll");
            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdToDeactivate.Rows.Count; i++)
            {
                GridViewRow row = grdToDeactivate.Rows[i];
                if (((CheckBox)row.FindControl("chkDeActivate")).Checked)
                {

                    countCheckedCheckbox = countCheckedCheckbox + 1;

                }

            }
            if (countCheckedCheckbox == grdToDeactivate.Rows.Count)
            {
                chkDeActivateSelectAll.Checked = true;
            }
            else
            {
                chkDeActivateSelectAll.Checked = false;
            }


        }

        protected void grdToDeactivate__RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdToDeactivate_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //    int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                //    var ComplianceRoleMatrixList = Business.ComplianceManagement.GetTempAssignedDetails(nCustomerBranchID);

                //    if (direction == SortDirection.Ascending)
                //    {
                //        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //        direction = SortDirection.Descending;
                //    }
                //    else
                //    {
                //        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //        direction = SortDirection.Ascending;
                //    }


                //    foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
                //    {
                //        if (field.SortExpression == e.SortExpression)
                //        {
                //            ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
                //        }
                //    }


                //    grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
                //    grdComplianceRoleMatrix.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        //public SortDirection direction
        //{
        //    get
        //    {
        //        if (ViewState["dirState"] == null)
        //        {
        //            ViewState["dirState"] = SortDirection.Ascending;
        //        }
        //        return (SortDirection)ViewState["dirState"];
        //    }
        //    set
        //    {
        //        ViewState["dirState"] = value;
        //    }
        //}

        protected void grdToDeactivate_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddMatrixSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }

        protected void grdToDeactivate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                BindDeactivateGrid(nCustomerBranchID);

                grdToDeactivate.PageIndex = e.NewPageIndex;
                grdToDeactivate.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        #region To Find Branches not Entities and Sub-Entities

        public static List<NameValueHierarchy> GetAll_Branches(int customerID, int selectedEntityID)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID == selectedEntityID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    branchList.Add(item.ID);
                    LoadChildBranches(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadChildBranches(int customerid, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.CustomerID == customerid
                                                && row.ParentID == nvp.ID
                                                //&& row.Type != 1
                                                select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                branchList.Add(item.ID);
                LoadChildBranches(customerid, item, false, entities);
            }
        }

        #endregion

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;
                DivnextScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalComplianceActivate"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalComplianceActivate"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalComplianceActivate"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                    DivnextScrum.Visible = false;
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                if (Session["TotalComplianceActivate"] != null)
                    TotalRows.Value = Session["TotalComplianceActivate"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalComplianceActivate"]))
                    EndRecord = Convert.ToInt32(Session["TotalComplianceActivate"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                tvFilterLocation_SelectedNodeChanged(null, null);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalComplianceActivate"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalComplianceActivate"]))
                    EndRecord = Convert.ToInt32(Session["TotalComplianceActivate"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                tvFilterLocation_SelectedNodeChanged(null, null);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnReAssign_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "openPopup", "OpenReAssignPopup(" + ddlCustomer.SelectedValue + ");", true);
            }
            else
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Select Customer";
            }
        }
    }
}