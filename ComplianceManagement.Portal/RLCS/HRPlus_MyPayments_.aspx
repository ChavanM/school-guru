﻿<%@ Page Title="My Payments" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_MyPayments.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_MyPayments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/Kendouicss_V1.css" rel="stylesheet" />
    <script src="../Newjs/moment.min.js"></script>
   <style type="text/css">
       .k-list-container.k-popup-dropdowntree .k-check-all {
    margin: 10px 8.5px 0px;
    zoom: 1.13;
}
          .btn{
              font-weight:400;
              font-size:14px;
          }
       .k-treeview .k-content, .k-treeview .k-item > .k-group, .k-treeview > .k-group {
           margin: 0px;
           margin-top: -10px;
       }
       .k-textbox>input{
         text-align:left;
     }

       .k-list-container.k-popup-dropdowntree .k-treeview{
           margin-left:-10px;
       }
       .k-checkbox-label{
margin-left:-1px;
       }
       .k-treeview .k-checkbox {
    margin-top: .2em;
    margin-left: -9px;
}
     .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
    cursor: pointer;
    background-color: transparent !important;
    border-color: transparent;
    color:#1fd9e1;
     }
    .k-checkbox-label:hover{
            color:#1fd9e1;
     }
     .k-dropdown-wrap-hover,.k-state-default-hover, .k-state-hover, .k-state-hover:hover {
    color: #2e2e2e;
    background-color: #d8d6d6 !important;
}
      .k-calendar .k-today .k-link {
             color:#515967;
             font-weight: 700;
        }
     .k-primary:focus:active:not(.k-state-disabled):not([disabled]) {
          box-shadow: none;
        }
      .k-calendar td.k-state-selected .k-link, .k-calendar td.k-today.k-state-selected.k-state-hover .k-link {
        color: #515967;
     }
      .k-calendar td.k-state-selected.k-state-hover {
    background-color:#1984c8;
    background-image: none;
    border-color:#1a87cd;
}
      .k-calendar .k-today {
    -webkit-box-shadow:none;
    box-shadow: none;
}
        .k-grid-content {
            min-height: auto !important;
        }
       .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
    color: #515967;
    padding-top: 5px;
}
        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            background-color: transparent;
            border: none;
            margin-left: 0px;
            margin-right: -7px;
            padding-left: 0px;
            padding-right: 15px;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
             .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
               .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }
         .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }
           .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
           .k-grid, .k-listview {
               margin-top: 10px;
           }
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }
        
              .k-tooltip-content{
         width: max-content !important;
        padding-right:0px !important;
        padding-left:0px !important;
        margin-left:0px !important;
        margin-right:0px !important;
         }
        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px;
            margin-left: 7px;
            line-height: normal;
        }
        .k-calendar-container {
    background-color: white;
    width: 217px;
}
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top:0px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

       
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
            font-size: 13px;
            padding-left: 23px;
        }

        .k-pager-numbers .k-link, .k-treeview .k-in{
            border-color: transparent;
    margin-left: 5px;
        }
        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }
        .k-pager-wrap td:nth-child(3) {
        border-width: 1px;
    }
        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:2px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }


        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-list-container.k-popup-dropdowntree .k-treeview{
            overflow:initial;
        }

        .k-grid-pager {
            margin-top: -1px;
            border-width:0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 7px;
            margin-bottom:0px;
        }
        .k-multiselect-wrap .k-input{
            padding-top:6px;
        }
        .k-popup .k-calendar {
    width: 217px;
    border: 0;
}
        .col-md-2 {
            width: 20%;
        }

        .k-grid-header th.k-with-icon .k-link{
            color:#535b6a;
        }
        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: #535b6a;
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height:34px;
        }
        .k-multiselect-wrap, .k-floatwrap{
            height:34px;
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }
        .tab-bg-primary {
    background: white;
    border-bottom: none;
}
        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-grid-footer {
             border-style: solid;
             border-width: 0px 0 0;
        }
        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
             border-left-width: 0px;
        }
        .k-checkbox:focus+.k-checkbox-label:before, .k-checkbox:focus+.k-checkbox-label:hover:before {
             border-color: #acacac;
             -webkit-box-shadow: 0 0 2px 0 #acacac;
             box-shadow: none;
       }
        .k-grid-header .k-header>.k-link, .k-header, .k-treemap-title {
            color:rgba(0,0,0,0.5);
       }
        .k-button.k-button-icon .k-icon, .k-grid-filter .k-icon, .k-header .k-icon {
              text-indent: -99999px;
              overflow: hidden;
              margin-bottom: -6px;
     }
        .k-list-container.k-popup-dropdowntree {
    padding: 0;
    overflow:auto;
    width: 233px !important;
    background-color: white;
}
 #gridConsolidatedBusiness .k-grid-pager {
    margin-top: 1px;
    border-width: 0px 0px 0px 0px;
}
 .k-multicheck-wrap {
    padding-right: 10px;
}
 .k-active-filter, .k-state-active, .k-state-active:hover {
    background-color: #E9EAEA;
    border-color:#E9EAEA;
}
   label.k-label:hover{
              color:#1fd9e1;
          }
   .k-i-more-vertical{
       margin-top:-9px;
   }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Payments');            

           <%-- if("<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role%>"!=="SPADM"){                
                var grid = $("#gridPaymentHistory").data("kendoGrid");
                if(grid!=null&&grid!=undefined){
                    if(grid.columns!=null && grid.columns!=undefined)        
                        grid.hideColumn(grid.columns.length-1);
                }
            }--%>

            $('#PaymentDetails').hide();

            $('#divProceedPayment').hide();

            $("#btnExcel").click(function (e) {
                ExportToExcel(e);
            });

            $("#btnExcel_Deductions").click(function (e) {
                ExportToExcel_Deductions(e);
            });

            $("#btnExcel_Business").click(function (e) {
                ExportToExcel_ConsolidatedBusiness(e);
            });
        });

        function ClearFilterPayment(e) {
            e.preventDefault();
            //e.stopPropogation();
            $("#ddlMode").data("kendoDropDownTree").value([]);

            var txnDateFrom=$("#txnDateFrom").data("kendoDatePicker");

            if(txnDateFrom!=null && txnDateFrom!=undefined){
                txnDateFrom.value(null);
                txnDateFrom.trigger("change");
            }

            var txnDateTo=$("#txnDateTo").data("kendoDatePicker");

            if(txnDateTo!=null && txnDateTo!=undefined){
                txnDateTo.value(null);
                txnDateTo.trigger("change");
            }
           
            $('#btnClearFilterPayment').css('display', 'none');   
            
            return false;
        }

        function ClearFilterDeduction(e) {
            e.preventDefault();
            //e.stopPropogation();
            $("#ddlCustomers").data("kendoDropDownTree").value([]);

            var FromMonth_Deduction=$("#FromMonth_Deduction").data("kendoDatePicker");

            if(FromMonth_Deduction!=null && FromMonth_Deduction!=undefined){
                FromMonth_Deduction.value(null);
                FromMonth_Deduction.trigger("change");
            }

            var ToMonth_Deduction=$("#ToMonth_Deduction").data("kendoDatePicker");

            if(ToMonth_Deduction!=null && ToMonth_Deduction!=undefined){
                ToMonth_Deduction.value(null);
                ToMonth_Deduction.trigger("change");
            }
           
            $('#btnClearFilterDeduction').css('display', 'none');   
            
            return false;
        }

        function ClearFilterBusiness(e) {
            e.preventDefault();
            //e.stopPropogation();
            $("#ddlDistributors").data("kendoDropDownTree").value([]);

            var FromMonth_Business=$("#FromMonth_Business").data("kendoDatePicker");

            if(FromMonth_Business!=null && FromMonth_Business!=undefined){
                FromMonth_Business.value(null);
                FromMonth_Business.trigger("change");
            }

            var ToMonth_Business=$("#ToMonth_Business").data("kendoDatePicker");

            if(ToMonth_Business!=null && ToMonth_Business!=undefined){
                ToMonth_Business.value(null);
                ToMonth_Business.trigger("change");
            }
           
            $('#btnClearFilterBusiness').css('display', 'none');   
            
            return false;
        }

        function ExportToExcel(e) {            
            e.preventDefault();
            kendo.ui.progress($("#gridPaymentHistory").data("kendoGrid").element, true);

            // trigger export of the products grid
            $("#gridPaymentHistory").data("kendoGrid").saveAsExcel();

            kendo.ui.progress($("#gridPaymentHistory").data("kendoGrid").element, false);
            return false;
        }

        function ExportToExcel_Deductions(e) {            
            e.preventDefault();
            kendo.ui.progress($("#gridPaymentDeductionReport").data("kendoGrid").element, true);

            // trigger export of the products grid
            $("#gridPaymentDeductionReport").data("kendoGrid").saveAsExcel();

            kendo.ui.progress($("#gridPaymentDeductionReport").data("kendoGrid").element, false);
            return false;
        }

        function ExportToExcel_ConsolidatedBusiness(e) {            
            e.preventDefault();
            kendo.ui.progress($("#gridConsolidatedBusiness").data("kendoGrid").element, true);

            // trigger export of the products grid
            $("#gridConsolidatedBusiness").data("kendoGrid").saveAsExcel();

            kendo.ui.progress($("#gridConsolidatedBusiness").data("kendoGrid").element, false);
            return false;
        }

        function BindGrid() {     
            var fileName = '';
            var exportExcelName = fileName.concat('PaymentReport-', '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");
            var custName= '<%=custName%>';
            var gridview = $("#gridPaymentHistory").kendoGrid({
                dataSource: {                   
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetPaymentHistory?custID=' + <%=custID%>,                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },   
                    pageSize: 10,
                    schema: {
                        data: function (response) {                              
                            if (response != null && response != undefined)
                                return response.Result;
                        },
                        total: function (response) {                            
                            if (response != null && response != undefined)
                                if (response.Result != null && response.Result != undefined)
                                    return response.Result.length;
                        },                        
                    }
                },

                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh:false,
                    pageSize:10,
                    pageSizes:true,
                    buttonCount: 3,
                    change: function (e) {

                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "PaymentID",menu:false, },
                    { hidden: true, field: "Mode",menu:false, },                                        
                    //{
                    //    field: "TxnDate", 
                    //    type: "date",
                    //    //hidden: true,
                    //    //format: "{0:DD/MM/YYYY}"
                    //    //template: "#= kendo.toString(kendo.parseDate(TxnDate, 'yyyy-MM-dd')) #",
                    //},
                    { hidden: true, field: "FilterDate",type: "date",menu:false, },  
                    {
                        field: "TxnDate", title: 'Transaction Date',
                        type: "date",
                        format:"{0:dd-MMM-yyyy}",
                        width: "20%",
                        filterable: {
                            multi:true,
                            search:true,
                            ui: 'datepicker',
                        },
                     attributes: {
                         style: 'white-space: nowrap;text-align:left;'
                         //border-width: 0px 0px 1px 1px
                        },
                        headerAttributes: { style: "text-align:left" },
                        template: "#= dateConverter(data)#", 
                        //format: "{0:dd-MMM-yyyy HH:mm tt}",                     
                    },
                    {
                        field: "Mode", title: 'Mode',
                        template: "#= showModeText(data)#",
                        width: "11%",
                        filterable: { 
                           
                            text:"#= showModeText(data)#",
                            multi:true,
                            search:true,
                           
                           },
                        attributes: {
                            style: 'text-align:left;'
                        },
                        headerAttributes: { style: "text-align:left" },                        
                    },
                    {
                        field: "OrderID", title: 'OrderID',
                        width: "21%;", 
                        filterable: {
                            multi:true,
                            search:true,
                        },
                        attributes: {
                            style: 'white-space: nowrap; text-align:left;'
                        },
                        headerAttributes: { style: "text-align:left" },                       
                    },
                    {
                        field: "StatusName", title: 'Status',
                        width: "13%",
                        filterable: {
                            multi:true,
                            search:true,
                        },
                        attributes: {
                            style: 'white-space: nowrap; text-align:left;'
                        },
                        headerAttributes: { style: "text-align:left" },                        
                    },
                    {
                        field: "Amount", title: 'Amount',
                        template: "#= currencyConverter(data)#",
                        width: "10%",
                        filterable: {
                            multi:true,
                            search:true,
                        },
                        attributes: {
                            style: 'text-align:right;'
                            //border-width: 0px 0px 1px 1px
                        },
                        headerAttributes: { style: "text-align:left" }, 
                        footerAttributes: { style: "text-align:right" }, 
                        footerTemplate: "Total Credit: #=GetTotalCredit()#",                        
                    },
                    {
                        title: "Action", lock: true, width: "11%;",
                        attributes: {
                            style: 'text-align: center;'
                        },
                        headerAttributes: { style: "text-align:center" },
                        command:
                            [
                               { name: "edit", text: "", iconClass: ".k-icon k-i-pencil", visible: showHideButton, click:openPaymentDetails },                               
                               { name: "TxnStatusCheck", text: "", iconClass: ".k-icon .k-i-refresh", className: "TxnStatusCheck", visible: showHideTxnStatusButton, click:CallCheckTxnStatus },
                            ],
                    }
                ],
                dataBound: function () {
                    $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                    $(".TxnStatusCheck").find("span").addClass("k-icon k-i-refresh");
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];   
                    
                    sheet.frozenRows = 1;
                    sheet.mergedCells = ["A1:B1"];
                    sheet.name = "Payments";

                    var myHeaders = [{
                        value: custName,                        
                    }];

                    sheet.rows.splice(0, 0, { cells: myHeaders, type: "header"});

                    for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {                        
                        var row = sheet.rows[rowIndex];
                        var cellIndex = 0;
                        row.cells[cellIndex].format = "dd-MMM-yyyy hh:mm AM/PM";  
                        row.cells[cellIndex].autoWidth = true;
                    }

                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });                    
                },
                //toolbar: ["excel"],
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                },
            });
            $("#gridPaymentHistory").kendoTooltip({
                filter: "td", 
                position: "bottom",
                content: function (e) {
                    
                    var content = e.target.context.textContent;
                    if ($.trim(content) != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }   

        function GetTotalCredit() {
            //debugger;
            if($('#gridPaymentHistory').data('kendoGrid')!=null && $('#gridPaymentHistory').data('kendoGrid')!=undefined){
                var dataSource = $('#gridPaymentHistory').data('kendoGrid').dataSource;
                if(dataSource!=null && dataSource!=undefined){
                    var data = dataSource.data();
                    
                    var item, sum = 0;
                    for (var idx = 0; idx < data.length; idx++) {
                        item = data[idx];
                        if (((item.PGID===2 && item.StatusID===1)||(item.PGID===0 && item.StatusID===2)) && item.Amount) {
                            sum += item.Amount;
                        }
                    }
                }
            }
            return ConvertToCurrency(sum);
        }
        
        function BindGrid_PaymentDeduction() {  
            var fileName = '';
            var exportExcelName = fileName.concat('PaymentDeductionReport-', '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");
            var custName= '<%=custName%>';
            var gridview = $("#gridPaymentDeductionReport").kendoGrid({
                dataSource: {                   
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetPaymentDeductionReport?distID=' + <%=custID%>,                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },   
                    pageSize: 10,
                    aggregate: [{ field: "Amount", aggregate: "sum" }],
                    schema: {
                        data: function (response) {                              
                            if (response != null && response != undefined)
                                return response.Result;
                        },
                        total: function (response) {                            
                            if (response != null && response != undefined)
                                if (response.Result != null && response.Result != undefined)
                                    return response.Result.length;
                        },                        
                    },                   
                },

                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh:false,
                    pageSize:10,
                    pageSizes:true,
                    buttonCount: 3,
                    change: function (e) {

                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "CustomerID",menu:false,},
                    { hidden: true, field: "BranchID",menu:false, },
                    { hidden: true, field: "FilterDate", type: "date",menu:false, },  
                    {
                        field: "TxnDate", title: 'Transaction Date',
                        width: "16.5%",
                        type:"date",
                        format:"{0:dd-MMM-yyyy}",
                        filterable:{
                            multi:true,
                            search:true,
                            ui: 'datepicker',
                            operators: {
                               string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                        attributes: {
                            style: 'white-space: nowrap;text-align:left;'
                        },
                        headerAttributes: { style: "text-align:left" },
                       template: "#= dateConverter(data)#", 
                        //format: "{0:dd-MMM-yyyy HH:mm tt}",
                    },                    
                    {
                        field: "CustomerName", title: 'Customer',
                        width: "12.5%;",
                        filterable:{
                            multi:true,
                            search:true,
                        },
                        attributes: {
                            style: 'white-space: nowrap;'
                        },                        
                    },  
                    {
                        field: "BranchCount", title: 'No. of Branch',
                        width: "14.4%;",
                        filterable:{
                            multi:true,
                            search:true,
                        },
                        attributes: { style: 'white-space: nowrap;text-align:left;'},      
                        headerAttributes: { style: "text-align:left" },                        
                    },
                    {
                        field: "EmployeeCount", title: 'No. of Employee',
                        width: "16.3%;",
                        filterable:{
                            multi:true,
                            search:true,
                        },
                        attributes: { style: 'white-space: nowrap;text-align:left;'},      
                        headerAttributes: { style: "text-align:left" },                         
                    },
                    {
                        field: "PeriodName", title: 'Period',
                        width: "9.9%;",
                        filterable:{
                            multi:true,
                            search:true,
                        },
                        attributes: {
                            style: 'white-space: nowrap;text-align:left;'
                        },      
                        headerAttributes: { style: "text-align:left" },
                    }, 
                     {
                         field: "Year", title: 'Year',
                         width: "8.8%;", 
                         filterable:{
                             multi:true,
                             search:true,
                         },
                         attributes: {
                             style: 'white-space: nowrap;text-align:left;'
                         },                        
                         headerAttributes: { style: "text-align:left" },
                     },
                     {
                         field: "CreditDebit", title: 'Credit/Debit',                        
                         width: "13.3%",
                         filterable:{
                             multi:true,
                             search:true,
                         },
                         attributes: {
                             style: 'text-align:left;'
                         },
                         headerAttributes: { style: "text-align:left" },                        
                     },
                    {
                        field: "Amount", title: 'Amount',
                        template: "#= currencyConverter(data)#",
                        width: "10.9%",
                        filterable:{
                            multi:true,
                            search:true,
                        },
                        attributes: {
                            style: 'text-align:right;'
                            //border-width:0px 1px 0px 1px
                        },
                        //aggregates: ["sum"], footerTemplate: "Total : #=sum#",
                        headerAttributes: { style: "text-align:left;" },   
                        aggregates: ["sum"],
                        footerTemplate: "Total Debit: #=ConvertToCurrency(sum)#",
                        footerAttributes: { style: "text-align:left" }, 
                    },                                     
                ],
                dataBound: function () {                    
                    
                },     
                
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0]; 

                    sheet.frozenRows = 1;
                    sheet.mergedCells = ["A1:B1"];
                    sheet.name = "Deductions";

                    var myHeaders = [{
                        value: custName,                        
                    }];

                    sheet.rows.splice(0, 0, { cells: myHeaders, type: "header"});

                    for (var rowIndex = 2; rowIndex < sheet.rows.length; rowIndex++) {                        
                        var row = sheet.rows[rowIndex];
                        var cellIndex = 0;
                        var value = row.cells[cellIndex].value;
                        var newValue = DateFormatConverter(value,"dd-MMM-yyyy hh:mm tt");

                        row.cells[cellIndex].value = newValue;                        
                        row.cells[cellIndex].autoWidth = true;

                        //Mode
                        //cellIndex = 1;
                        //var modeValue = row.cells[cellIndex].value;
                        //var modeNewValue = GetMode(modeValue);

                        //row.cells[cellIndex].value = modeNewValue;                        
                        //row.cells[cellIndex].autoWidth = true;                        
                    }
                                        
                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });
                },
                //toolbar: ["excel"],
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                },
            });

            $("#gridPaymentDeductionReport").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "bottom",
                content: function (e) {
                    
                    var content = e.target.context.textContent;
                    if ($.trim(content) != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                         return e.preventDefault();
                }
            }).data("kendoTooltip");
            $("#btnExcel_Deductions").kendoTooltip({
                //filter: ".k-grid-edit3",
                content: function (e) {
                    return "Export to Excel";
                }
            });
        }


        function GetMode(providedData) {

            var strDate = '';
            kendo.culture("en-IN");

            if (providedData != null && providedData != undefined) {
                if(providedData===1)
                    modeText="Online";
                else if(providedData===2)
                    modeText="Offline";
            }

            return strDate;
        }

        function DateFormatConverter(providedDate, format) {

            var strDate = '';
            kendo.culture("en-IN");
            if (providedDate != null && providedDate != undefined) {
                strDate = kendo.toString(kendo.parseDate(providedDate), format)
            }
            return strDate;
        }

        function Bind_Customer() {
            $("#ddlCustomers").kendoDropDownTree({
                placeholder: "Select Customer",               
                //autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "CustomerName",
                dataValueField: "CustomerID",
                change: ApplyFilter_Deductions,
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =avacomRLCSAPIURL%>GetMyAssignedCustomers?userID=<% =loggedInUserID%>&distID=<% =distID%>&roleCode=<% =loggedUserRole%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            
                            return response.Result;
                        }
                    },
                },
            });
        }

        function BindGrid_ConsolidatedBusiness() {   
            var fileName = '';
            var exportExcelName = fileName.concat('ConsolidatedBusinessReport-', '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");
             var custName= '<%=custName%>';
            var gridview = $("#gridConsolidatedBusiness").kendoGrid({
                dataSource: {                   
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetPaymentConsolidatedReport?distID=' + <%=custID%>,                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },   
                    pageSize: 10,
                    schema: {
                        data: function (response) {                              
                            if (response != null && response != undefined)
                                return response.Result;
                        },
                        total: function (response) {                            
                            if (response != null && response != undefined)
                                if (response.Result != null && response.Result != undefined)
                                    return response.Result.length;
                        },                        
                    },
                    //
                    group: {
                        field: "ParentName",  
                        aggregates: [{ field: "TotalDebit", aggregate: "sum" }]
                    },                    
                },

                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh:false,
                    pageSize:10,
                    pageSizes:true,
                    buttonCount: 3,
                    change: function (e) {

                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ParentID",menu:false, },
                    { hidden: true, field: "CustID",menu:false, },   
                    { hidden: true, field: "FilterDate", type:"date",menu:false,}, 
                    {
                        field: "ParentName", title: 'Customer',
                        width: "20%;", 
                        filterable:{
                            multi:true,
                            search:true,
                        },
                        attributes: { style: 'white-space: nowrap;'
                        //border-bottom-width:1px;
                },                        
                    },
                    {
                        field: "PeriodName", title: 'Period',
                        width: "10%;",   
                        filterable:{
                            multi:true,
                            search:true,
                        },
                        attributes: {style: 'white-space: nowrap; text-align:left;'},     
                        //border-bottom-width:1px;
                        headerAttributes: { style: "text-align:left" },
                    }, 
                    {
                        field: "Year", title: 'Year',
                        width: "10%;",
                        filterable:{
                            multi:true,
                            search:true,
                        },
                        attributes: {style: 'white-space: nowrap; text-align:left;'},     
                        //border-bottom-width:1px;
                        headerAttributes: { style: "text-align:left" },
                    },                     
                    {
                        field: "TotalDebit", title: 'Total',
                        template: "#= currencyCommaSeperatedConverter(data)#",
                        width: "10%",
                        filterable:{
                            multi:true,
                            search:true,
                        },
                        attributes: {style: 'text-align:right;border-right-width:1px;'},
                        //border-bottom-width:1px
                        aggregates: ["sum"], 
                        groupHeaderTemplate: "Total: #=sum#",
                        //groupFooterTemplate: " : #=sum#",
                        headerAttributes: { style: "text-align:left;" },            
                        //border-width:1px 1px 0px 0px;
                    },                                     
                ],
                dataBound: function () {                    
                    
                },
                excelExport: function (e) {

                    var sheet = e.workbook.sheets[0];   
                    
                    sheet.frozenRows = 1;
                    sheet.mergedCells = ["A1:B1"];                    

                    var myHeaders = [{
                        value: custName,                        
                    }];

                    sheet.rows.splice(0, 0, { cells: myHeaders, type: "header"});

                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });
                },
                //toolbar: ["excel"],
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                },
            });
            $("#gridConsolidatedBusiness").kendoTooltip({
                filter: "td:nth-child(6)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridConsolidatedBusiness").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.PeriodName;
                    return content;
                }
            }).data("kendoTooltip");
            $("#gridConsolidatedBusiness").kendoTooltip({
                filter: "td:nth-child(5)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridConsolidatedBusiness").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.ParentName;
                    return content;
                }
            }).data("kendoTooltip");
            $("#gridConsolidatedBusiness").kendoTooltip({
                filter: "td:nth-child(7)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridConsolidatedBusiness").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Year;
                    return content;
                }
            }).data("kendoTooltip");
            $("#gridConsolidatedBusiness").kendoTooltip({
                filter: "td:nth-child(8)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridConsolidatedBusiness").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.TotalDebit;
                    return content;
                }
            }).data("kendoTooltip");
            $("#btnExcel_Business").kendoTooltip({
                //filter: ".k-grid-edit3",
                content: function (e) {
                    return "Export to Excel";
                }
            });
        }

        function Bind_MyDistributors() {
            $("#ddlDistributors").kendoDropDownTree({
                placeholder: "Select Customer",               
                //autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: ApplyFilter_ConsolidatedBusiness,
                dataSource: {
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetMyDistributors?parentDistID=<%=distID%>&uptoSubDist=true',                            
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            
                            return response.Result;
                        }
                    },
                },
            });
        }

        function dateConverter(data) {
            
            var strDate = '';
            kendo.culture("en-IN");
            if (data.TxnDate != null && data.TxnDate != undefined) {
                strDate = kendo.toString(kendo.parseDate(data.TxnDate), "dd-MMM-yyyy hh:mm tt")
            }
            return strDate;
        }

        function onRowBound(e) {
            $(".k-grid-edit").find("span").addClass("k-icon k-edit");
            $(".k-grid-delete").find("span").addClass("k-icon k-delete");
            $(".k-grid-add").find("span").addClass("k-i-plus-outline");
        }

        function showHideButton(data) {            
            var showHide = false; 
            
            if("<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role%>"!=="SPADM"){
                showHide=false;
            }else{
                showHide=true;
            }
            
            return showHide;
        }

        function showHideTxnStatusButton(data) {  
            
            var showHide = false; 
            
            if (data.Mode != null && data.Mode != undefined) {
                if(data.Mode===1){
                    if(data.StatusID===0){
                        showHide=true;
                    }else{
                        showHide=false;
                    }
                }                   
            }            
            
            //if (data.Mode != null && data.Mode != undefined) {
            //    if(data.Mode===2)
            //        showHide=true;
            //}
            return showHide;
        }        
        
        function showModeText(data) {            
            var modeText = '';
            kendo.culture("en-IN");
            if (data.Mode != null && data.Mode != undefined) {
                if(data.Mode==1)
                    modeText="Online";
                else if(data.Mode==2)
                    modeText="Offline";
            }
            return modeText;
        }

        function currencyConverter(data) {            
            var strPrice = '';
            kendo.culture("en-IN");
            if (data.Amount != null && data.Amount != undefined) {
                strPrice = kendo.toString(data.Amount, "n")
            }
            return strPrice;
        }

        function ConvertToCurrency(amount) {            
            var strPrice = '';
            kendo.culture("en-IN");
            if (amount != null && amount != undefined) {
                strPrice = kendo.toString(amount, "n")
            }
            return strPrice;
        }

        function currencyCommaSeperatedConverter(data) {            
            var strPrice = '';
            kendo.culture("en-IN");
            if (data.TotalDebit != null && data.TotalDebit != undefined) {
                strPrice = kendo.toString(data.TotalDebit, "n")
            }
            return strPrice;
        }

        function openPaymentDetails(e){
            e.preventDefault();
            
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            <%--$("#<%= hdnPaymentID.ClientID %>").val(dataItem.PaymentID);--%>

            if(dataItem!=null && dataItem!=undefined){
                if(dataItem.PaymentID!=null && dataItem.PaymentID!=undefined){

                    $('#PaymentDetails').show();
                    var myWindowAdv = $("#PaymentDetails");

                    myWindowAdv.kendoWindow({
                        width: "60%",
                        height: '90%',                
                        iframe: true,
                        title: "Payment Details",
                        visible: false,
                        actions: ["Close"],
                        close: onClose,
                        open: onOpen
                    });

                    $('#iframePayment').attr('src', 'about:blank');
                    myWindowAdv.data("kendoWindow").center().open();
                    $('#iframePayment').attr('src', "/RLCS/HRPlus_MyPaymentDetails.aspx?PID=" + dataItem.PaymentID);
                }
            }

            return false;
        }    
        
        function OpenPaymentSummaryPopup() {  
            
            var custID=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;

            $('#divProceedPayment').show();

            var myWindowAdv = $("#divProceedPayment");
            
            myWindowAdv.kendoWindow({
                width: "90%",
                height: '90%',
                title: "Order Summary",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose,
                open: onOpen
            });
            
            $('#iframeProceedPayment').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeProceedPayment').attr('src', "/RLCS/HRPlus_PaymentCheckout.aspx?CustID="+custID);
            return false;
        }

        function CallCheckTxnStatus(e){
            e.preventDefault();
            kendo.ui.progress($("#gridPaymentHistory").data("kendoGrid").element, true);
            
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            
            if(dataItem!=null && dataItem!=undefined){
                if(dataItem.OrderID!=null && dataItem.OrderID!=undefined && dataItem.CustomerID!=null && dataItem.CustomerID!=undefined){
                    $("#<%= hdnOrderID.ClientID %>").val(dataItem.OrderID);
                    $("#<%= hdnCustomerID.ClientID %>").val(dataItem.CustomerID);   
                    
                    document.getElementById("<%=btnCheckTxnStatus.ClientID %>").click();
                }
            }

            kendo.ui.progress($("#gridPaymentHistory").data("kendoGrid").element, false);
        }

        function showWindow(){
            
            $("#PaymentDetails").show();

            var myWindow = $("#PaymentDetails");

            myWindow.kendoWindow({
                width: "45%",
                height: "60%", 
                top:"0px",
                visible:false,
                modal: true,
                title: "Payment Details",
                actions: [ "Close" ],                
                close: onClose,
                open: onOpen
            });

            myWindow.data("kendoWindow").center().open();          
        }

        function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }

        function onClose() {
            $("#PaymentDetails").hide();
            $('#gridPaymentHistory').data('kendoGrid').dataSource.read();

            //$(this.element).empty();
        }

        function Bind_PaymentMode() {
            $("#ddlMode").kendoDropDownTree({
                placeholder: "Select Mode",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter_Payments,
                dataSource: [
                    { text: "Online", value: "1" },
                    { text: "Offline", value: "2" },                    
                ]
            });
        }

        function Bind_DeductionMode() {
            $("#ddlDeductionMode").kendoDropDownTree({
                placeholder: "Select Type",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter_Deductions,
                dataSource: [
                    { text: "Credit", value: "Credit" },
                    { text: "Debit", value: "Debit" },                    
                ]
            });
        }

        function ApplyFilter_Payments() {
                        
            //Branch
            var selectedPaymentModes = $("#ddlMode").data("kendoDropDownTree")._values;
                        
            var finalSelectedfilter = { logic: "and", filters: [] };

            if (selectedPaymentModes.length > 0) {
                var modeFilter = { logic: "or", filters: [] };

                $.each(selectedPaymentModes, function (i, v) {
                    modeFilter.filters.push({
                        field: "Mode", operator: "eq", value: v
                    });
                });

                finalSelectedfilter.filters.push(modeFilter);
            }                       
            
            //datefilter                       
            if ($("#txnDateFrom").data("kendoDatePicker").value() != null && $("#txnDateFrom").data("kendoDatePicker").value() != "") {
                var startFilterDate = moment($("#txnDateFrom").data("kendoDatePicker").value());
                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "gte", value: startFilterDate });
            }
            
            if ($("#txnDateTo").data("kendoDatePicker").value() != null && $("#txnDateTo").data("kendoDatePicker").value() != "") {
                //var endFilterDate = moment($("#txnDateTo").data("kendoDatePicker").value());
                var endFilterDate = new Date($("#txnDateTo").data("kendoDatePicker").value().getFullYear(), 
                                             $("#txnDateTo").data("kendoDatePicker").value().getMonth(), 
                                             $("#txnDateTo").data("kendoDatePicker").value().getDate(),
                                             23, 59, 59);

                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "lte", value: endFilterDate });
            }

            if (finalSelectedfilter.filters.length > 0) {
                var dataSource = $("#gridPaymentHistory").data("kendoGrid").dataSource;
                dataSource.filter(finalSelectedfilter);

                $('#btnClearFilterPayment').css('display', 'block');
            } else {
                var dataSource = $("#gridPaymentHistory").data("kendoGrid").dataSource;
                dataSource.filter({});

                $('#btnClearFilterPayment').css('display', 'none');
            }
        }
                
        function ShowPaymentTab(){

            $('#btnClearFilterPayment').css('display', 'none');   

            BindGrid();

            $("#gridPaymentHistory").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "View Payment Details";
                }
            });

            $("#gridPaymentHistory").kendoTooltip({
                filter: ".k-grid-TxnStatusCheck",
                content: function (e) {
                    return "Check Current Status";
                }
            });
          
            $("#btnExcel").kendoTooltip({
                //   filter: ".k-grid-edit3",
                content: function (e) {
                    return "Export to Excel";
                }
            });
            $("#btnAddAmount").kendoTooltip({
               // filter: ".k-grid-edit3",
                content: function (e) {
                    return "Add Payments";
                }
            });
        
      
            
            Bind_PaymentMode();
                        
            function startChange() {
                
                var startDate = start.value(),
                endDate = end.value();

                if (startDate) {
                    startDate = new Date(startDate);
                    startDate.setDate(startDate.getDate());
                    end.min(startDate);
                } else if (endDate) {
                    start.max(new Date(endDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_Payments();
            }

            function endChange() {
                var endDate = end.value(),
                startDate = start.value();

                if (endDate) {
                    endDate = new Date(endDate);
                    endDate.setDate(endDate.getDate());
                    start.max(endDate);
                } else if (startDate) {
                    end.min(new Date(startDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_Payments();
            }

            var start = $("#txnDateFrom").kendoDatePicker({
                change: startChange,                   
                format: "dd-MMM-yyyy",                
                dateInput: false               
            }).data("kendoDatePicker");

            var end = $("#txnDateTo").kendoDatePicker({
                change: endChange,                                     
                format: "dd-MMM-yyyy",                
                dateInput: false
            }).data("kendoDatePicker");

            start.max(end.value());
            end.min(start.value());            
        }

        function ApplyFilter_Deductions() {
                        
            //Customer
            var selectedCustomers = $("#ddlCustomers").data("kendoDropDownTree")._values;
            //var selectedDeductionModes = $("#ddlDeductionMode").data("kendoDropDownTree")._values;
                        
            var finalSelectedfilter = { logic: "and", filters: [] };

            if (selectedCustomers.length > 0) {
                var modeFilter = { logic: "or", filters: [] };

                $.each(selectedCustomers, function (i, v) {
                    modeFilter.filters.push({
                        field: "CustomerID", operator: "eq", value: v
                    });
                });

                finalSelectedfilter.filters.push(modeFilter);
            }

            //datefilter                       
            if ($("#FromMonth_Deduction").data("kendoDatePicker").value() != null && $("#FromMonth_Deduction").data("kendoDatePicker").value() != "") {
                //var startFilterDate = new Date($("#FromMonth_Deduction").data("kendoDatePicker").value());
                var startFilterDate = new Date($("#FromMonth_Deduction").data("kendoDatePicker").value().getFullYear(), 
                                               $("#FromMonth_Deduction").data("kendoDatePicker").value().getMonth(), 
                                               1);                
                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "gte", value: startFilterDate });
            }
            
            if ($("#ToMonth_Deduction").data("kendoDatePicker").value() != null && $("#ToMonth_Deduction").data("kendoDatePicker").value() != "") {
                //var endFilterDate = moment($("#txnDateTo").data("kendoDatePicker").value());

                var year=$("#ToMonth_Deduction").data("kendoDatePicker").value().getFullYear();
                var month=$("#ToMonth_Deduction").data("kendoDatePicker").value().getMonth();

                if(month==12){
                    year=year+1;
                    month=1;
                }else{
                    month=month+1;
                }

                var endFilterDate = new Date(year, month,0, 23, 59, 59);                
                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "lte", value: endFilterDate });
            }

            if (finalSelectedfilter.filters.length > 0) {
                var dataSource = $("#gridPaymentDeductionReport").data("kendoGrid").dataSource;
                dataSource.filter(finalSelectedfilter);

                $('#btnClearFilterDeduction').css('display', 'block');   
            } else {
                var dataSource = $("#gridPaymentDeductionReport").data("kendoGrid").dataSource;
                dataSource.filter({});

                $('#btnClearFilterDeduction').css('display', 'none');   
            }
        }

        function ShowDeductionsTab(){
            $('#btnClearFilterDeduction').css('display', 'none');   
            BindGrid_PaymentDeduction();
            Bind_Customer();
            //Bind_DeductionMode();

            function startChange() {
                
                var startDate = start.value(),
                endDate = end.value();

                if (startDate) {
                    startDate = new Date(startDate);
                    startDate.setDate(startDate.getDate());
                    end.min(startDate);
                } else if (endDate) {
                    start.max(new Date(endDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_Deductions();
            }

            function endChange() {
                var endDate = end.value(),
                startDate = start.value();

                if (endDate) {
                    endDate = new Date(endDate);
                    endDate.setDate(endDate.getDate());
                    start.max(endDate);
                } else if (startDate) {
                    end.min(new Date(startDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_Deductions();
            }
                        
            var start = $("#FromMonth_Deduction").kendoDatePicker({
                change: startChange,
                // defines the start view
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMM-yyyy",
                // specifies that DateInput is used for masking the input element
                //dateInput: true
                parseFormats: ["MM-yyyy"]
            }).data("kendoDatePicker");

            var end = $("#ToMonth_Deduction").kendoDatePicker({                
                change: endChange,
                // defines the start view
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMM-yyyy",
                // specifies that DateInput is used for masking the input element
                //dateInput: true
            }).data("kendoDatePicker");

            start.max(end.value());
            end.min(start.value());
        }

        function ApplyFilter_ConsolidatedBusiness() {
            var finalSelectedfilter = { logic: "and", filters: [] };
            
            //Customer
            var selectedDistributors = $("#ddlDistributors").data("kendoDropDownTree")._values;

            if (selectedDistributors.length > 0) {
                var modeFilter = { logic: "or", filters: [] };

                $.each(selectedDistributors, function (i, v) {
                    modeFilter.filters.push({
                        field: "ParentID", operator: "eq", value: v
                    });
                });

                finalSelectedfilter.filters.push(modeFilter);
            }
                
            //datefilter                       
            if ($("#FromMonth_Business").data("kendoDatePicker").value() != null && $("#FromMonth_Business").data("kendoDatePicker").value() != "") {
                //var startFilterDate = new Date($("#FromMonth_Deduction").data("kendoDatePicker").value());
                var startFilterDate = new Date($("#FromMonth_Business").data("kendoDatePicker").value().getFullYear(), 
                                               $("#FromMonth_Business").data("kendoDatePicker").value().getMonth(), 
                                               1);                
                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "gte", value: startFilterDate });
            }
            
            if ($("#ToMonth_Business").data("kendoDatePicker").value() != null && $("#ToMonth_Business").data("kendoDatePicker").value() != "") {
                //var endFilterDate = moment($("#txnDateTo").data("kendoDatePicker").value());

                var year=$("#ToMonth_Business").data("kendoDatePicker").value().getFullYear();
                var month=$("#ToMonth_Business").data("kendoDatePicker").value().getMonth();

                if(month==12){
                    year=year+1;
                    month=1;
                }else{
                    month=month+1;
                }

                var endFilterDate = new Date(year, month,0, 23, 59, 59);                
                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "lte", value: endFilterDate });
            }

            if (finalSelectedfilter.filters.length > 0) {
                var dataSource = $("#gridConsolidatedBusiness").data("kendoGrid").dataSource;
                dataSource.filter(finalSelectedfilter);
                $('#btnClearFilterBusiness').css('display', 'block');
            } else {
                var dataSource = $("#gridConsolidatedBusiness").data("kendoGrid").dataSource;
                dataSource.filter({});
                $('#btnClearFilterBusiness').css('display', 'none');
            }
        }

        function ShowBusinessTab(){

            Bind_MyDistributors();

            BindGrid_ConsolidatedBusiness();

            function startChange() {
                
                var startDate = start.value(),
                endDate = end.value();

                if (startDate) {
                    startDate = new Date(startDate);
                    startDate.setDate(startDate.getDate());
                    end.min(startDate);
                } else if (endDate) {
                    start.max(new Date(endDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_ConsolidatedBusiness();
            }

            function endChange() {
                var endDate = end.value(),
                startDate = start.value();

                if (endDate) {
                    endDate = new Date(endDate);
                    endDate.setDate(endDate.getDate());
                    start.max(endDate);
                } else if (startDate) {
                    end.min(new Date(startDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_ConsolidatedBusiness();
            }
                        
            var start = $("#FromMonth_Business").kendoDatePicker({
                change: startChange,
                // defines the start view
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMM-yyyy",
                // specifies that DateInput is used for masking the input element
                //dateInput: true
            }).data("kendoDatePicker");

            var end = $("#ToMonth_Business").kendoDatePicker({                
                change: endChange,
                // defines the start view
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMM-yyyy",
                // specifies that DateInput is used for masking the input element
                //dateInput: true
            }).data("kendoDatePicker");

            start.max(end.value());
            end.min(start.value());

            $('#btnClearFilterBusiness').css('display', 'none');
        }
    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdnPaymentID" runat="server" />
    <asp:HiddenField ID="hdnCustomerID" runat="server" />
    <asp:HiddenField ID="hdnOrderID" runat="server" />

    <div style="display: none">
        <asp:Button ID="btnShowPayment" runat="server" />
    </div>

    <div class="col-md-12 colpadding0">
        <header class="panel-heading tab-bg-primary colpadding0">
            <ul class="nav nav-tabs">
                <li class="active" id="liPayment" runat="server">
                    <asp:LinkButton ID="lnkPayments" CausesValidation="false" runat="server" OnClick="lnkPayments_Click">Payments</asp:LinkButton>
                </li>
                <li class="" id="liDeduction" runat="server">
                    <asp:LinkButton ID="lnkDeductions" CausesValidation="false" runat="server" OnClick="lnkDeductions_Click">Deductions</asp:LinkButton>
                </li>
                <li class="" id="liBusiness" runat="server">
                    <asp:LinkButton ID="lnkConsolidatedBusiness" CausesValidation="false" runat="server" OnClick="lnkConsolidatedBusiness_Click">Consolidated Business</asp:LinkButton>
                </li>
                <li class="" id="liAvailableBalance" runat="server" style="float: right">
                    <asp:LinkButton ID="LinkButton1" CausesValidation="false" runat="server" OnClientClick="return false;">
                        Available Balance:
                        <asp:Label ID="lblBalance" runat="server">0.00</asp:Label>
                    </asp:LinkButton>
                </li>
            </ul>
        </header>
    </div>

    <div class="clearfix"></div>

    <asp:MultiView ID="MainView" runat="server">
        <asp:View ID="PaymentsView" runat="server">
            <div class="row">
                <asp:ValidationSummary ID="vsPayments" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="PaymentsValidationGroup" />
                <asp:CustomValidator ID="cvPayments" runat="server" EnableClientScript="False"
                    ValidationGroup="PaymentsValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div id="divContainerPayments" class="mt10 col-md-12 colpadding0" style="margin-top:12px;">
                <div class="row mb10 colpadding0">
                    <div class="col-md-12 colpadding0 text-right">
                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="txnDateFrom" placeholder="From Date" style="width: 100%;height:36px;" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="txnDateTo" placeholder="To Date" style="width: 100%;height:36px;" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="ddlMode" data-placeholder="type" style="width: 100%;" />
                        </div>
                      
                    

                    <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role != null)
                        { %>
                    <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "SPADM")
                        { %>
                    <div class="col-md-1 colpadding0 text-right">
                        <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="lnkBtnBack" data-toggle="tooltip" data-placement="bottom" ToolTip="Back to My Distributors"
                            PostBackUrl="~/RLCS/HRPlus_MyDistributors.aspx">Back</asp:LinkButton>
                    </div>
                    <% } %>
                    <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "DADMN")
                        { %>
                         <div class="col-md-1" style="width: 20%; padding-left:94px">
                            <button id="btnClearFilterPayment" class="btn btn-primary" onclick="ClearFilterPayment(event)" style="height:36px;padding-top:5px;"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                        </div> 
                    <div class="col-md-1 text-right">
                        <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnAddAmount"  data-placement="bottom" Style="margin-left:-12px;height:36px;padding-top:5px;"
                            OnClientClick="return OpenPaymentSummaryPopup();"><span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;Add New
                        </asp:LinkButton>
                        </div>
                        <%--OnClick="btnAddAmount_Click"--%>
                        <% } %>
                    
                    <% } %>

                   <div class="col-md-1 text-right">
                        <asp:Button CssClass="btn btn-primary" runat="server" ID="btnCheckTxnStatus" OnClick="btnCheckTxnStatus_Click" Style="display: none"></asp:Button>
                        <button id="btnExcel" class="btn btn-primary" <%--data-toggle="tooltip"--%> style="margin-left:10px;height:36px;padding-top:5px;" data-placement="bottom" title=""><span class="k-icon k-i-excel"></span>Export</button>                        
                   </div>
                        
                        </div>
                </div>

                <div class="clearfix"></div>

                <div class="row" style="display: flex; /* align-items: center; *//* border: 0px; *//* justify-content: center; */">
                    <div id="gridPaymentHistory"<%-- style="border: none;"--%>>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="DeductionsView" runat="server">
            <div id="divContainerDeductions" class="mt10 col-md-12 colpadding0" style="margin-top:12px;">
                <div class="row mb12 colpadding0">
                    <div class="col-md-12 colpadding0 text-right">
                        <div class="col-md-1" style="width: 30%; padding-left: 0px">
                            <input id="ddlCustomers" style="width: 100%" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="FromMonth_Deduction" placeholder="From (Month-Year)" style="width: 100%;height:35px;" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="ToMonth_Deduction" placeholder="To (Month-Year)" style="width: 100%;height:35px;" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="ddlDeductionMode" data-placeholder="Mode" style="width: 100%; display: none;" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <button id="btnClearFilterDeduction" class="btn btn-primary" style="margin-left: 127px;margin-top: 1px; height:35px;"onclick="ClearFilterDeduction(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                        </div>
                   <%--</div>

                    <div class="col-md-2 colpadding0 text-right">--%>
                        <button id="btnExcel_Deductions" class="btn btn-primary " <%--data-toggle="tooltip"--%> data-placement="bottom" title="" style="height:35px;margin-top: 2px;"><span class="k-icon k-i-excel"></span>Export</button>                        
                    </div>
                </div>
</div>
                <div class="clearfix"></div>

                <div class="row" style="display: flex; /* align-items: center; *//* border: 0px; *//* justify-content: center; */">
                    <div id="gridPaymentDeductionReport" <%--style="border: none;"--%>>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="BusinessView" runat="server">
            <div id="divContainerConsolidatedBusiness" class="mt10 col-md-12 colpadding0" style="margin-top:10px;">
                <div class="row mb10 colpadding0">
                    <div class="col-md-12 colpadding0 text-right">
                         <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="ddlDistributors" placeholder="To (Month-Year)" style="width: 100%" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="FromMonth_Business" placeholder="From (Month-Year)" style="width: 100%;height:35px;" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="ToMonth_Business" placeholder="To (Month-Year)" style="width: 100%;height:35px;" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <button id="btnClearFilterBusiness" class="btn btn-primary"   onclick="ClearFilterBusiness(event)" style="margin-left:237px;height: 36px;"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                        </div>
                   <%--</div>

                    <div class="col-md-2 colpadding0 text-right">--%>
                        <button id="btnExcel_Business" class="btn btn-primary" <%--data-toggle="tooltip"--%> style="height: 36px;" data-placement="bottom" title="" ><span class="k-icon k-i-excel"></span>Export</button>                        
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row" style="display: flex; /* align-items: center; *//* border: 0px; *//* justify-content: center; */">
                    <div id="gridConsolidatedBusiness" <%--style="border: none;"--%>>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>

    <div id="PaymentDetails">
        <iframe id="iframePayment" style="width: 100%; height: 100%; border: none"></iframe>
    </div>

    <div id="divProceedPayment">
        <iframe id="iframeProceedPayment" style="width: 100%; height: 98%; border: none;"></iframe>
    </div>
</asp:Content>
