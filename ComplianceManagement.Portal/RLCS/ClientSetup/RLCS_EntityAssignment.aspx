﻿<%@ Page Title="Entity/Branch Assignment :: Setup HR+" Language="C#" EnableEventValidation="false" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCS_EntityAssignment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_EntityAssignment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/NewCSS/arrow-Progressbar.css" rel="stylesheet" />
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .custom-combobox-input {
            width: 225px;
        }
        .validation-summary {
        color: #270;
        background-color: #b4f1c4;
        height:28px;
        padding-top: 1px; 
        padding-bottom: 15px;
    }
        .alert-success {	
        color: #5c9566;	
        background-color: #dff8e3;	
        border-color: #d6e9c6;	
        }	
    .alert {	
        /*padding: 15px;	
        margin-bottom: 20px;*/	
        border: 1px solid transparent;	
        border-radius: 4px;	
    }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/RLCS/ClientSetup/RLCS_CustomerCorporateList.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity-Branch"></asp:LinkButton>
                    <%--PostBackUrl="~/RLCS/Setup/RLCS_EntityBranchList.aspx"--%>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserMaster.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/RLCS/ClientSetup/RLCS_EmployeeMaster.aspx" runat="server" Text="Employee"></asp:LinkButton>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityAssignment.aspx" runat="server" Text="Entity-Branch Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Assign.aspx" runat="server" Text="Compliance Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAct" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Activate.aspx" runat="server" Text="Compliance Activation"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div style="width: 100%">
                <div runat="server" id="divCustomer" style="width: 30%; float: left; margin-top: 5px;">
                    <div style="width: 30%; float: left;">
                        <label style="width: 100%; display: block; text-align: right; font-size: 13px; color: #333; margin-top: 4px; font-weight: bold;">
                            Customer:</label>
                    </div>
                    <div style="width: 70%; float: right;">
                        <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Please select customer."
                            ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="UserValidationGroup" Display="None" />
                    </div>
                </div>

                <div id="FilterLocationdiv" runat="server" style="width: 30%; float: left; margin-top: 5px;">
                    <div style="width: 30%; float: left;">
                        <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                            Entity/Location:</label>
                        <%-- <label style="width: 100%; display: block; text-align: right; font-size: 13px; color: #333; margin-top: 4px; font-weight: bold;">
                            Entity/Location:</label>--%>
                    </div>
                    <div style="width: 70%; float: right;">
                        <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 100%;"
                            CssClass="txtbox" />
                        <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                            <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Style="min-height: 50px; max-height: 200px; overflow: auto; width: 300px;"
                                ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                    </div>
                </div>

                <div runat="server" id="divFilterUsers" style="width: 30%; float: left; margin-top: 5px;">
                    <div style="width: 30%; float: left;">
                        <label style="width: 100%; display: block; text-align: right; font-size: 13px; color: #333; margin-top: 4px; font-weight: bold;">
                            User:
                        </label>
                    </div>

                    <div style="width: 70%; float: right;">
                        <asp:DropDownList runat="server" ID="ddlFilterUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterUsers_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="width: 10%; float: right; margin-top: 10px;">
                    <asp:LinkButton Text="Add New" runat="server" ID="btnAddNew" OnClick="btnAddNew_Click" CssClass="button" />
                </div>
            </div>

            <div style="width: 100%; margin-left:15px;">
                <asp:GridView runat="server" ID="grdAssignEntities" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdAssignEntities_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="96%" OnSorting="grdAssignEntities_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdAssignEntities_PageIndexChanging" ShowHeaderWhenEmpty="true">
                    <Columns>
                         <asp:TemplateField HeaderText="Sr.No." SortExpression="Sr.No.">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                              <HeaderStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location/Branch" SortExpression="Branch">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                             <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category" SortExpression="Category" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Category") %>' ToolTip='<%# Eval("Category") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="UserName" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="UserName" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No entity/branch assignment record(s) found or all records are filtered out
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divAssignEntitiesDialog">
        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
            <ContentTemplate>
                <div class="col-md-12 colpadding0" >
                                        
                                        <asp:ValidationSummary ID="vsSaveMessage" runat="server" CssClass="validation-summary" ValidationGroup="saveValidationGroup"/>
                                        <asp:CustomValidator ID="cvSaveMessage" runat="server" EnableClientScript="False" ValidationGroup="saveValidationGroup" Display="None" />
                                        
                </div>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            User</label>
                        <asp:DropDownList ID="ddlUsers" runat="server" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select User." ControlToValidate="ddlUsers"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                            Display="None" />
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Location</label>
                                <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 265px;"
                                    CssClass="txtbox" />
                                <div style="margin-left: 160px; position: absolute; z-index: 10" id="divBranches">
                                    <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="265px"
                                        Style="overflow: auto; max-height: 105px; min-height: 75px;" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                                <asp:CompareValidator ID="CompareValidator2" ControlToValidate="tbxBranch" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px;display:none;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Compliance Category</label>
                                <asp:DropDownList runat="server" ID="ddlComplianceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                    CssClass="txtbox" AutoPostBack="false">
                                    <asp:ListItem Text="Human Resource" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Finance" Value="5"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Compliance Category." ControlToValidate="ddlComplianceCatagory"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; text-align: center; margin-top: 10px; clear: both">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="button" OnClientClick="$('#divAssignEntitiesDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 64px; width: 260px;">
                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divAssignEntitiesDialog').dialog({
                //height: auto,
                width: 500,
                autoOpen: false,
                draggable: true,
                title: "Assign Entities/Location/Branch to User",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            initializeCombobox();
        });

        function initializeCombobox() {
            $("#<%= ddlCustomer.ClientID %>").combobox();
            $("#<%= ddlFilterUsers.ClientID %>").combobox();
            $("#<%= ddlUsers.ClientID %>").combobox();
            $("#<%= ddlComplianceCatagory.ClientID %>").combobox();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 500, function () { });
        }
    </script>
</asp:Content>
