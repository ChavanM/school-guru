﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup
{

    public partial class RLCS_HRCompliance_Assign : System.Web.UI.Page
    {
        public static List<int> branchList = new List<int>();
        public static List<string> lstStatesToFilter = new List<string>();
        public static List<string> lstactgroups = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                if (!IsPostBack)
                {
                    BindCustomers();
                    BindLocationFilter();

                    List<string> lstStates = new List<string>();
                    BindStates();
                    //BindComplianceCategories();

                    BindUsers(ddlFilterPerformer);
                    BindUsers(ddlFilterReviewer);
                    //BindUsers(ddlFilterApprover);
                    BindActList();

                    //AddFilter();
                    tbxFilterLocation.Text = "< Select >";
                    txtactList.Text = "< Select >";

                    RefreshScopeDropDown();

                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                    {
                        btnUploadAssignment.Visible = true;

                        if (AuthenticationHelper.ComplianceProductType == 3)
                        {
                            FilterScopeDropDown();
                        }
                    }
                    else
                    {
                        btnUploadAssignment.Visible = false;
                    }
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void AddFilter(int pageIndex = 0)
        {
            try
            {
                ViewState["pagefilter"] = Convert.ToString(Request.QueryString["Param"]);
                if (ViewState["pagefilter"] != null)
                {
                    if (Convert.ToString(ViewState["pagefilter"]).Equals("location"))
                    {
                        //divFilterUsers.Visible = false;
                        //FilterLocationdiv.Visible = true;
                    }
                    else if (Convert.ToString(ViewState["pagefilter"]).Equals("user"))
                    {
                        //divFilterUsers.Visible = true;
                        //FilterLocationdiv.Visible = false;
                    }
                    else
                    {
                        //divFilterUsers.Visible = false;
                        //FilterLocationdiv.Visible = false;
                        //if (AuthenticationHelper.Role == "EXCT")
                        //{
                        //    btnAddComplianceType.Visible = false;
                        //}

                        //BindComplianceInstances();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeStateFilter", string.Format("initializeJQueryUI('{0}', 'dvState');", txtStateList.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideState", "$(\"#dvState\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int serviceProviderID = -1;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomers(customerID, serviceProviderID, 2);
                ddlCustomer.DataBind();

                if (AuthenticationHelper.Role != "CADMN")
                    ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindLocationFilter();
                tbxFilterLocation.Text = "< Select >";
                BindUsers(ddlFilterPerformer);
                BindUsers(ddlFilterReviewer);

                RefreshScopeDropDown();

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue!="-1")
                {
                    btnUploadAssignment.Visible = true;

                    int complianceProductType = AuthenticationHelper.ComplianceProductType;

                    if (AuthenticationHelper.Role == "IMPT")
                        complianceProductType = RLCS_Master_Management.GetComplianceProductType(Convert.ToInt32(ddlCustomer.SelectedValue));

                    if (complianceProductType == 3)
                    {
                        FilterScopeDropDown();
                    }
                }
                else
                {
                    btnUploadAssignment.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {
                    tvFilterLocation.Nodes.Clear();

                    //var bracnhes = RLCS_Master_Management.GetAll_Entities(customerID);
                    var bracnhes = RLCS_ClientsManagement.GetAllHierarchy(customerID);

                    //TreeNode node = new TreeNode("< All >", "-1");
                    //node.Selected = true;
                    //tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    //tvFilterLocation_SelectedNodeChanged(null, null);       
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindStates()
        {
            try
            {
                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int customerID = -1;
                int customerBranchID = 0;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {
                    //ddlState.DataTextField = "SM_Name";
                    //ddlState.DataValueField = "SM_Code";

                    if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue) && branchList.Count == 0)
                    {
                        customerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                        branchList.Clear();
                        //GetAll_Branches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        //GetAll_SubBranches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        branchList.Add(Convert.ToInt32(tvFilterLocation.SelectedValue));
                        branchList.ToList();
                    }

                    if (ddlScopeType.SelectedValue.Trim().Equals("SOW13"))
                    {
                        int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                        var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(branchList);

                        if (custBranchDetails != null)
                        {
                            //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);
                            string clientID = custBranchDetails[0].CM_ClientID;
                            //string ptState = custBranchDetails.CL_PT_State;

                            if (!string.IsNullOrEmpty(clientID))
                            {
                                lstStatesToFilter = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "EPF");
                            }
                        }
                    }
                    else if (ddlScopeType.SelectedValue.Trim().Equals("SOW14"))
                    {
                        int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                        var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(branchList);

                        if (custBranchDetails != null)
                        {
                            //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);
                            string clientID = custBranchDetails[0].CM_ClientID;
                            //string ptState = custBranchDetails.CL_PT_State;

                            if (!string.IsNullOrEmpty(clientID))
                            {
                                lstStatesToFilter = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "ESI");
                            }
                        }
                    }
                    else if (ddlScopeType.SelectedValue.Trim().Equals("SOW17"))
                    {
                        int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        //var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(custBranchID);
                        var custBranchDetails = RLCS_Master_Management.GetEntityOrBranchDetails(branchList);

                        if (custBranchDetails != null)
                        {
                            //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);
                            string clientID = custBranchDetails[0].CM_ClientID;
                            //string ptState = custBranchDetails.CL_PT_State;

                            if (!string.IsNullOrEmpty(clientID))
                            {
                                //lstStatesToFilter = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "PT");
                                lstStatesToFilter = RLCS_Master_Management.GetPTStates(custBranchID, clientID, "PT");
                            }
                        }
                    }
                    else
                    {
                        lstStatesToFilter = RLCS_Master_Management.GetBranchStateDetails(branchList);
                    }

                    var lstStates = RLCS_ClientsManagement.GetAllStates();

                    if (lstStatesToFilter.Count > 0 && lstStates.Count > 0)
                    {
                        lstStates = lstStates.Where(row => lstStatesToFilter.Contains(row.SM_Code)).ToList();
                    }

                    //ddlState.DataSource = lstStates;
                    //ddlState.DataBind();

                    rptState.DataSource = lstStates;
                    rptState.DataBind();

                    //ddlState.Items.Insert(0, new ListItem("< Select >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            FilterLocationNodeChange();
        }

        protected void tvFilterLocation_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            FilterLocationNodeChange();
        }

        private void FilterLocationNodeChange()
        {
            //if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue))
            //{
            //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            int customerID = -1;
            if (AuthenticationHelper.Role == "CADMN")
            {
                //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }

            if (customerID != -1)
            {
                branchList.Clear();
                ////GetAll_Branches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                ////GetAll_SubBranches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                //branchList.Add(Convert.ToInt32(tvFilterLocation.SelectedValue));
                //branchList.ToList();

                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }

            }
            //}

            if (branchList.Count > 0)
            {
                //tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                BindStates();
                BindGrid();
            }
            else
            {
                //tvFilterLocation.SelectedNode.Selected = false;

                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "No Branch available for Selected Entity, Please Create Location/Branch before Compliance Assignment.";
            }
        }

        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                if (node.Checked)
                {
                    if (!branchList.Contains(Convert.ToInt32(node.Value)))
                    {
                        branchList.Add(Convert.ToInt32(node.Value));
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (!branchList.Contains(Convert.ToInt32(tn.Value)))
                            {
                                branchList.Add(Convert.ToInt32(tn.Value));
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {

                    ddlUserList.DataTextField = "Name";
                    ddlUserList.DataValueField = "ID";
                    ddlUserList.Items.Clear();

                    var users = UserManagement.HRGetAllNVP(customerID, ids: ids, Flags: true);

                    ddlUserList.DataSource = users;
                    ddlUserList.DataBind();

                    ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        private void BindComplianceCategories()
        {
            try
            {
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";
                ddlComplianceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplianceCatagory.DataBind();
                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlComplianceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindActList();
            grdComplianceRoleMatrix.PageIndex = 0;
            BindGrid();
        }
        
        private void BindGrid()
        {
            //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int customerID = -1;

            if (AuthenticationHelper.Role == "CADMN")
            {
                //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }

            string scopeType = Convert.ToString(ddlScopeType.SelectedValue.Trim());
            string stateCode = Convert.ToString(ddlState.SelectedValue);
            lstStatesToFilter.Clear();

            foreach (RepeaterItem Item in rptState.Items)
            {
                CheckBox chkState = (CheckBox)Item.FindControl("chkState");
                if (chkState != null && chkState.Checked)
                {
                    Label lblStateID = (Label)Item.FindControl("lblStateID");
                    Label lblStateName = (Label)Item.FindControl("lblStateName");

                    lstStatesToFilter.Add(lblStateID.Text);
                }
            }

            lstactgroups.Clear();
            foreach (RepeaterItem Item in rptActList.Items)
            {
                CheckBox chkState = (CheckBox)Item.FindControl("chkAct");
                if (chkState != null && chkState.Checked)
                {
                    Label lblActID = (Label)Item.FindControl("lblActID");

                    lstactgroups.Add(lblActID.Text);
                }
            }

            string establishmentType = string.Empty;
            string branchState = string.Empty;

            if (scopeType.Trim().ToUpper().Equals("SOW03"))
            {
                //if (branchList.Count == 1)
                //{
                //    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(branchList);

                //    if (custBranchDetails != null)
                //    {
                //        if (!string.IsNullOrEmpty(custBranchDetails.CM_EstablishmentType) && !string.IsNullOrEmpty(custBranchDetails.CM_State))
                //        {
                //            establishmentType = custBranchDetails.CM_EstablishmentType;
                //            branchState = custBranchDetails.CM_State;
                //        }

                //    }
                //}

                var compliancelist = RLCS_ComplianceManagement.GetHRComplianceList_Assignment_New(customerID, scopeType, branchList, lstStatesToFilter, stateCode, establishmentType, branchState, false, lstactgroups);

                if (compliancelist != null)
                {
                    grdComplianceRoleMatrix.DataSource = compliancelist;
                    grdComplianceRoleMatrix.DataBind();
                }
                else
                {
                    grdComplianceRoleMatrix.DataSource = null;
                    grdComplianceRoleMatrix.DataBind();

                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please specify Establishment Type and State, prior to Register Compliance Assignment";
                }
            }
            else if (scopeType.Trim().ToUpper().Equals("SOW17"))
            {
                if (lstStatesToFilter.Count > 0)
                {
                    var list = RLCS_ComplianceManagement.GetHRComplianceList_Assignment_New(customerID, scopeType, branchList, lstStatesToFilter, stateCode, establishmentType, branchState, false, lstactgroups);
                    grdComplianceRoleMatrix.DataSource = list;
                    Session["TotalComplianceAssign"] = 0;
                    Session["TotalComplianceAssign"] = list.Count();
                    grdComplianceRoleMatrix.DataBind();
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please specify PT State for Employee(s) prior to PT Compliance Assignment";
                }
            }
            else
            {
                if (branchList.Count > 0)
                {
                    //var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(branchList[0]);
                    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(branchList);

                    lstStatesToFilter.Clear();
                    foreach (var custBranchDetail in custBranchDetails)
                    {
                        lstStatesToFilter.Add(custBranchDetail.CM_State);
                    }

                    ////if (custBranchDetails != null)
                    ////{
                    ////    if (!string.IsNullOrEmpty(custBranchDetails.CM_State))
                    ////        branchState = custBranchDetails.CM_State;
                    ////    lstStatesToFilter.Clear();
                    ////    lstStatesToFilter.Add(branchState);
                    ////}
                }
                grdComplianceRoleMatrix.DataSource = RLCS_ComplianceManagement.GetHRComplianceList_Assignment_New(customerID,scopeType, branchList, lstStatesToFilter, stateCode, establishmentType, branchState, false, lstactgroups);
                grdComplianceRoleMatrix.DataBind();
            }

            if (grdComplianceRoleMatrix.Rows.Count > 0)
                btnSaveAssignment.Visible = true;
            else
                btnSaveAssignment.Visible = false;
        }

        private void BindActList()
        {
            try
            {
                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                ////int customerID = -1;

                ////if (AuthenticationHelper.Role == "CADMN")
                ////{
                ////    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                ////    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                ////}
                ////else if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                ////{
                ////    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                ////    {
                ////        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                ////    }
                ////}

                ////int complianceTypeID = Convert.ToInt32(ddlScopeType.SelectedValue);
                ////int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);

                List<RLCS_ActGroup_Mapping> ActList = ActManagement.GetRLCSActGroupAll();
                ////if (complianceTypeID != -1)
                ////{
                ////    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                ////    {
                ////        //int branchID = tvBranches.SelectedNode != null ? Convert.ToInt32(tvBranches.SelectedNode.Value) : -1;
                ////        int branchID = -1;
                ////        if (!(tbxFilterLocation.Text.Trim().Equals("< Select >")))
                ////        {
                ////            branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                ////        }
                ////        if (branchID != -1)
                ////        {
                ////            int StateId = CustomerBranchManagement.GetByID(branchID).StateID;
                ////            ActList = ActList.Where(entry => entry.StateID == StateId).ToList();
                ////        }
                ////    }
                ////}

                rptActList.DataSource = ActList;
                rptActList.DataBind();

                ////if (complianceCatagoryID != -1 || complianceTypeID != -1)
                ////{

                ////    foreach (RepeaterItem aItem in rptActList.Items)
                ////    {
                ////        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");

                ////        if (!chkAct.Checked)
                ////        {
                ////            chkAct.Checked = true;
                ////        }
                ////    }
                ////    CheckBox actSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("actSelectAll");
                ////    actSelectAll.Checked = true;
                ////}

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int complianceTypeID = Convert.ToInt32(ddlScopeType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                
                
                SaveCheckedValues();
                
                PopulateCheckedValues();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();

                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();

                BindGrid();

                PopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlScopeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlScopeType.SelectedValue))
            {
                ViewState["CHECKED_ITEMS"] = null;
                if (ddlScopeType.SelectedValue.Trim().Equals("SOW03") || ddlScopeType.SelectedValue.Trim().Equals("SOW17"))
                {
                    //ddlState.ClearSelection();
                    //ddlState.Visible = true;

                    lblState.Visible = true;
                    txtStateList.Visible = true;
                    //dvState.Visible = true;
                    BindStates();
                }
                else
                {
                    //ddlState.ClearSelection();
                    lblState.Visible = false;
                    //ddlState.Visible = false;
                    txtStateList.Visible = false;
                    //dvState.Visible = false;
                }

                BindGrid();
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlScopeType.SelectedValue.Trim().Equals("SOW03") || ddlScopeType.SelectedValue.Trim().Equals("SOW17"))
                {
                    //ddlState.ClearSelection();
                    lblState.Visible = true;
                    ddlState.Visible = true;
                }
                else
                {
                    ddlState.ClearSelection();
                    lblState.Visible = false;
                    ddlState.Visible = false;
                }

                BindGrid();
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            //BindComplianceMatrix("N", "N");

        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {

        }

        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void chkAssignSelectAll_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            foreach (GridViewRow row in grdComplianceRoleMatrix.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkAssign");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }

        }

        protected void chkAssign_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                if (((CheckBox)row.FindControl("chkAssign")).Checked)
                {

                    countCheckedCheckbox = countCheckedCheckbox + 1;

                }

            }
            if (countCheckedCheckbox == grdComplianceRoleMatrix.Rows.Count)
            {
                chkAssignSelectAll.Checked = true;
            }
            else
            {
                chkAssignSelectAll.Checked = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool assignSuccess = false;
                List<string> lstCodes = new List<string>();
                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {
                    ////if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                    ////{
                    if (!String.IsNullOrEmpty(ddlFilterPerformer.SelectedValue) && ddlFilterPerformer.SelectedValue != "-1")
                    {
                        //    //string scopeType = Convert.ToString(ddlScopeType.SelectedValue.Trim());

                        ////var complianceList = new List<ComplianceAsignmentProperties>();
                        ////SaveCheckedValues();
                        ////complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                        List<TempAssignmentTable> Tempassignments = new List<TempAssignmentTable>();
                        foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                        {
                            int ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                            bool Performer = ((CheckBox)gvrow.FindControl("chkAssign")).Checked;
                            string StateCode = ((Label)gvrow.FindControl("lblStateCode")).Text;
                            string scopeType = ((HiddenField)gvrow.FindControl("hndScopeID")).Value;
                            int branchID = Convert.ToInt32(((HiddenField)gvrow.FindControl("hdnAVACOM_BranchID")).Value);

                            if (scopeType.Trim().ToUpper().Equals("SOW03"))
                            {
                                RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails = null;

                                ////branchList.Clear();
                                //////GetAll_Branches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                                ////branchList.Add(Convert.ToInt32(branchID));
                                ////branchList.ToList();

                                if (branchID > 0)
                                {
                                    ////branchList.ForEach(eachBranch =>
                                    ////{
                                    //Get CustomerBranchID, StartDate from RLCS_CustomerBranch_ClientsLocation_Mapping
                                    customerBranchDetails = RLCS_Master_Management.GetClientLocationDetails(branchID);

                                    if (customerBranchDetails != null && customerBranchDetails.CM_EstablishmentType != null)
                                    {
                                        ////complianceList.ForEach(eachSelectedCompliance =>
                                        ////{
                                        if (Performer) //Performer - means Compliance Selected or Not
                                        {
                                            var mapCompliance = RLCS_ComplianceManagement.Check_Register_Compliance_Map(ComplianceId,
                                                customerBranchDetails.CM_State, customerBranchDetails.CM_EstablishmentType.Trim().ToUpper());

                                            if (mapCompliance)
                                            {
                                                //Performer
                                                TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                {
                                                    ComplianceId = ComplianceId,
                                                    CustomerBranchID = branchID,
                                                    RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                    UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                    IsActive = true,
                                                    CreatedOn = DateTime.Now
                                                };

                                                Tempassignments.Add(TempAssP);

                                                //Reviewer
                                                TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                {
                                                    ComplianceId = ComplianceId,
                                                    CustomerBranchID = branchID,
                                                    RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                    UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue),
                                                    IsActive = true,
                                                    CreatedOn = DateTime.Now
                                                };

                                                Tempassignments.Add(TempAssR);
                                            }
                                        }
                                        ////});
                                    }
                                    else
                                        LoggerMessage.InsertErrorMsg_DBLog("customerBranchDetails=null",
                                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    ////});
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Branch Available for Selected Entity/Client";
                                }
                            }
                            else if (scopeType.Trim().ToUpper().Equals("SOW13")) //PF Challan
                            {
                                ////int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                                var custBranchDetails = RLCS_Master_Management.GetEntityDetails(branchID);
                                //var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(custBranchID);

                                if (custBranchDetails != null)
                                {
                                    string clientID = custBranchDetails.CM_ClientID;

                                    if (!string.IsNullOrEmpty(clientID))
                                    {
                                        lstCodes = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "EPF");

                                        if (lstCodes.Count > 0)
                                        {
                                            lstCodes.ForEach(eachpfCode =>
                                            {
                                                //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);

                                                if (!string.IsNullOrEmpty(eachpfCode))
                                                {
                                                    var lstBranches = RLCSManagement.GetCustomerBranchesByPFOrESICOrPTOrLWFCode(clientID, "EPF", eachpfCode, "E");

                                                    if (lstBranches.Count > 0)
                                                    {
                                                        lstBranches.ForEach(eachCustBranch =>
                                                        {
                                                            ////complianceList.ForEach(eachSelectedCompliance =>
                                                            ////{
                                                            if (eachCustBranch.AVACOM_BranchID != null)
                                                            {
                                                                if (Performer) //Performer - means Compliance Selected or Not
                                                                {
                                                                    //Performer
                                                                    TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                                    {
                                                                        ComplianceId = ComplianceId,
                                                                        CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                        RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                                        UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                                        IsActive = true,
                                                                        CreatedOn = DateTime.Now
                                                                    };

                                                                    Tempassignments.Add(TempAssP);

                                                                    //Reviewer
                                                                    TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                                    {
                                                                        ComplianceId = ComplianceId,
                                                                        CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                        RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                                        UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue),
                                                                        IsActive = true,
                                                                        CreatedOn = DateTime.Now
                                                                    };

                                                                    Tempassignments.Add(TempAssR);
                                                                }
                                                            }
                                                            ////});
                                                        });
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "NO Entity/Client Assigned with EPF Code-" + eachpfCode;
                                                    }
                                                }
                                            });
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "No PF Code Assigned for Selected Entity/Client";
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        //cvDuplicateEntry.ErrorMessage = "Branch Details not found, Please try again";
                                        cvDuplicateEntry.ErrorMessage = "Entity/Branch Details not found, or Select Entity for PF Compliance Assignment";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Entity/Branch Details not found, or Select Entity for PF Compliance Assignment";
                                }
                            }
                            else if (scopeType.Trim().ToUpper().Equals("SOW14"))
                            {
                                ////int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                                var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(branchID);

                                if (custBranchDetails != null)
                                {
                                    string clientID = custBranchDetails.CM_ClientID;
                                    //string esiCode = custBranchDetails.CL_PF_Code;

                                    //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);

                                    if (!string.IsNullOrEmpty(clientID))
                                    {
                                        lstCodes = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "ESI");

                                        if (lstCodes.Count > 0)
                                        {
                                            lstCodes.ForEach(eachesiCode =>
                                            {
                                                var lstBranches = RLCSManagement.GetCustomerBranchesByPFOrESICOrPTOrLWFCode(clientID, "ESI", eachesiCode, "B");

                                                if (lstBranches.Count > 0)
                                                {
                                                    lstBranches.ForEach(eachCustBranch =>
                                                    {
                                                            ////complianceList.ForEach(eachSelectedCompliance =>
                                                            ////{
                                                            if (eachCustBranch.AVACOM_BranchID != null)
                                                        {
                                                            if (Performer) //Performer - means Compliance Selected or Not
                                                                {
                                                                    //Performer
                                                                    TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                                {
                                                                    ComplianceId = ComplianceId,
                                                                    CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                    RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                                        UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                                    IsActive = true,
                                                                    CreatedOn = DateTime.Now
                                                                };

                                                                Tempassignments.Add(TempAssP);

                                                                    //Reviewer
                                                                    TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                                {
                                                                    ComplianceId = ComplianceId,
                                                                    CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                    RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                                        UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue),
                                                                    IsActive = true,
                                                                    CreatedOn = DateTime.Now
                                                                };

                                                                Tempassignments.Add(TempAssR);
                                                            }
                                                        }
                                                            ////});
                                                        });
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "NO Employee Assigned with ESI Code-" + eachesiCode;
                                                }
                                            });
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "No ESI Code Assigned to Employee";
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Branch Details not found, Please try again";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Branch Details not found, Please try again";
                                }
                            }
                            else if (scopeType.Trim().ToUpper().Equals("SOW15")) //LWF Challan
                            {


                            }
                            else if (scopeType.Trim().ToUpper().Equals("SOW17")) //PT Challan
                            {
                                ////int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                                var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(branchID);

                                if (custBranchDetails != null)
                                {
                                    //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);
                                    string clientID = custBranchDetails.CM_ClientID;
                                    //string ptState = custBranchDetails.CL_PT_State;

                                    if (!string.IsNullOrEmpty(clientID))
                                    {
                                        lstCodes = RLCS_Master_Management.GetPTStates(branchID, clientID, "PT");
                                        //lstCodes = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "PT");                                            

                                        if (lstCodes.Count > 0)
                                        {
                                            lstCodes.ForEach(eachptState =>
                                            {
                                                var lstBranches = RLCSManagement.GetCustomerBranchesByPFOrESICOrPTOrLWFCode(clientID, "PT", eachptState, "B");
                                                    //var lstBranches = RLCSManagement.GetCustomerBranchesByPTState(customerID, clientID, eachptState, "B");
                                                    if (lstBranches.Count > 0)
                                                {
                                                        ////complianceList.ForEach(eachSelectedCompliance =>
                                                        ////{
                                                        if (Performer) //Performer - means Compliance Selected or Not
                                                        {
                                                        if (!string.IsNullOrEmpty(StateCode))
                                                        {
                                                                //var lstBranches = RLCSManagement.GetCustomerBranchesByPTState(customerID, clientID, eachSelectedCompliance.StateCode, "B");

                                                                lstBranches.ForEach(eachCustBranch =>
                                                                {
                                                                        //Performer
                                                                        TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                                    {
                                                                        ComplianceId = ComplianceId,
                                                                        CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                        RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                                            UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                                        IsActive = true,
                                                                        CreatedOn = DateTime.Now
                                                                    };

                                                                    Tempassignments.Add(TempAssP);

                                                                        //Reviewer
                                                                        TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                                    {
                                                                        ComplianceId = ComplianceId,
                                                                        CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                        RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                                            UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue),
                                                                        IsActive = true,
                                                                        CreatedOn = DateTime.Now
                                                                    };

                                                                    Tempassignments.Add(TempAssR);
                                                                });
                                                        }
                                                    }
                                                        ////});
                                                    }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "No PT State mapped to Selected Branch";
                                                }
                                            });
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "No PT State Assigned for Selected Entity/Client";
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Branch Details not found, Please try again";
                                    }
                                }
                            }
                            else if (scopeType.Trim().ToUpper().Equals("SOW05")) //Return
                            {
                                ////int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                                if (branchID != -1)
                                {
                                    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(branchID);

                                    if (custBranchDetails != null)
                                    {
                                        ////complianceList.ForEach(eachSelectedCompliance =>
                                        ////{
                                        if (Performer) //Performer - means Compliance Selected or Not
                                        {
                                            //Performer
                                            TempAssignmentTable TempAssP = new TempAssignmentTable()
                                            {
                                                ComplianceId = ComplianceId,
                                                CustomerBranchID = branchID,
                                                RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                IsActive = true,
                                                CreatedOn = DateTime.Now
                                            };

                                            Tempassignments.Add(TempAssP);

                                            //Reviewer
                                            TempAssignmentTable TempAssR = new TempAssignmentTable()
                                            {
                                                ComplianceId = ComplianceId,
                                                CustomerBranchID = branchID,
                                                RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue),
                                                IsActive = true,
                                                CreatedOn = DateTime.Now
                                            };

                                            Tempassignments.Add(TempAssR);
                                        }
                                        ////});
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Select Branch";
                                    }
                                }
                            }

                        }

                        if (Tempassignments.Count != 0)
                        {
                            RLCS_ComplianceManagement.SaveTempAssignments(Tempassignments);
                            ClearSelection();
                            assignSuccess = true;
                        }

                        if (assignSuccess)
                        {
                            tvFilterLocation_SelectedNodeChanged(null, null);
                            //BindGrid();
                            ViewState["CHECKED_ITEMS"] = null;

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Selected Details Save Successfully";
                            vsHRCompAssign.CssClass = "alert alert-success";

                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select User to Assign";
                    }
                    //}
                    //else
                    //{
                    //    cvDuplicateEntry.IsValid = false;
                    //    cvDuplicateEntry.ErrorMessage = "Please select at least one Entity/Location/Branch";
                    //}
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select Customer.";

                }
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ClearSelection()
        {
            tbxFilterLocation.Text = "< Select >";
            ddlFilterPerformer.SelectedValue = "-1";
            ddlFilterReviewer.SelectedValue = "-1";
            ddlFilterApprover.SelectedValue = "-1";
            ddlComplianceCatagory.SelectedValue = "-1";
            //ddlScopeType.SelectedValue = "-1";
        }

        private void SaveCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = new List<ComplianceAsignmentProperties>();
                // Check in the Session
                if (ViewState["CHECKED_ITEMS"] != null)
                    complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    ComplianceAsignmentProperties complianceProperties = new ComplianceAsignmentProperties();
                    complianceProperties.ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkAssign")).Checked;
                    complianceProperties.StateCode = ((Label)gvrow.FindControl("lblStateCode")).Text;

                    if (complianceProperties.Performer)
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }
                }

                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == index).FirstOrDefault();
                        if (rmdata != null)
                        {
                            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkAssign");
                            chkPerformer.Checked = rmdata.Performer;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region To Find Branches not Entities and Sub-Entities

        public static List<NameValueHierarchy> GetAll_Branches(int customerID, int selectedEntityID)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID == selectedEntityID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    LoadChildBranches(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadChildBranches(int customerid, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.CustomerID == customerid
                                                && row.ParentID == nvp.ID
                                                && row.Type != 1
                                                select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                branchList.Add(item.ID);
                LoadChildBranches(customerid, item, false, entities);
            }
        }

        #endregion

        #region To Find Child Branches

        public static List<NameValueHierarchy> GetAll_SubBranches(int customerID, int selectedBranchID)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID == selectedBranchID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    branchList.Add(item.ID);
                    LoadSubBranches(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubBranches(int customerid, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.CustomerID == customerid
                                                && row.ParentID == nvp.ID
                                                select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                branchList.Add(item.ID);
                LoadSubBranches(customerid, item, false, entities);
            }
        }

        #endregion

        private void FilterScopeDropDown()
        {
            try
            {
                List<string> lstTLMappedScope = RLCS_Master_Management.GetScopeofWork(Convert.ToInt32(ddlCustomer.SelectedValue));

                if (lstTLMappedScope != null)
                {
                    if (lstTLMappedScope.Count > 0)
                    {
                        lstTLMappedScope.ForEach(eachScope =>
                        {
                            if (ddlScopeType.Items.FindByValue(eachScope) != null)
                            {
                                ddlScopeType.Items.Remove(ddlScopeType.Items.FindByValue(eachScope));
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void RefreshScopeDropDown()
        {
            try
            {
                ddlScopeType.Items.Clear();

                ListItemCollection lstScopeType = new ListItemCollection();
                lstScopeType.Add(new ListItem("Register", "SOW03"));
                lstScopeType.Add(new ListItem("PF Challan", "SOW13"));
                lstScopeType.Add(new ListItem("ESI Challan", "SOW14"));
                lstScopeType.Add(new ListItem("PT Challan", "SOW17"));
                lstScopeType.Add(new ListItem("Return", "SOW05"));

                ddlScopeType.DataTextField = "Text";
                ddlScopeType.DataValueField = "Value";
                ddlScopeType.DataSource = lstScopeType;
                ddlScopeType.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        private void StateSelectedChanged()
        {
            if (ddlScopeType.SelectedValue.Trim().Equals("SOW03") || ddlScopeType.SelectedValue.Trim().Equals("SOW17"))
            {
                //ddlState.ClearSelection();
                lblState.Visible = true;
                //ddlState.Visible = true;

                txtStateList.Visible = true;
                //dvState.Visible = true;
            }
            else
            {
                //ddlState.ClearSelection();
                lblState.Visible = false;
                //ddlState.Visible = false;
                txtStateList.Visible = false;
                //dvState.Visible = false;
            }

            BindGrid();
        }

        protected void StateSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            StateSelectedChanged();
        }

        protected void chkState_CheckedChanged(object sender, EventArgs e)
        {
            StateSelectedChanged();
        }

        protected void actSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void chkAct_CheckedChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}