﻿<%@ Page Title="HRCompliance-Assignment::Setup HR+" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCS_HRCompliance_Assign.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_HRCompliance_Assign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/NewCSS/arrow-Progressbar.css" rel="stylesheet" />

    <script type="text/javascript">
        $(function () {
            initializeCombobox();
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeCombobox() {
            $("#<%= ddlCustomer.ClientID %>").combobox();
            $("#<%= ddlFilterPerformer.ClientID %>").combobox();
            $("#<%= ddlFilterReviewer.ClientID %>").combobox();
            $("#<%= ddlFilterApprover.ClientID %>").combobox();
            $("#<%= ddlScopeType.ClientID %>").combobox();
            $("#<%= ddlComplianceCatagory.ClientID %>").combobox();
            $("#<%= ddlState.ClientID %>").combobox();
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        //function UncheckHeader() {
        //    var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
        //    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
        //    var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
        //    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        //        rowCheckBoxHeader[0].checked = true;
        //    } else {

        //        rowCheckBoxHeader[0].checked = false;
        //    }
        //}

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>

    <style type="text/css">
        .td1 {
            width: 10%;
        }

        .td2 {
            width: 23%;
        }

        .td3 {
            width: 10%;
        }

        .td4 {
            width: 23%;
        }

        .td5 {
            width: 10%;
        }

        .td6 {
            width: 23%;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/RLCS/ClientSetup/RLCS_CustomerCorporateList.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity/ Location/ Branch"></asp:LinkButton>
                    <%--PostBackUrl="~/RLCS/Setup/RLCS_EntityBranchList.aspx"--%>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserMaster.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityAssignment.aspx" runat="server" Text="Entity Assignment"></asp:LinkButton>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnComAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Assign.aspx" runat="server" Text="Compliance Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAct" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Activate.aspx" runat="server" Text="Compliance Activation"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <center>
                <table runat="server" width="100%">
                    <tr>
                        <td style="width: 100%;">
                            <asp:ValidationSummary ID="vsHRCompAssign" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table runat="server" width="100%">
                    <tr>
                        <td class="td1">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Customer</label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />

                        </td>
                        <td class="td3"></td>
                        <td class="td4"></td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Entity</label>
                        </td>

                        <td class="td2">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Style="min-height: 50px; max-height: 200px; overflow: auto; width: 390px;"
                                    ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                                <asp:CompareValidator ControlToValidate="tbxFilterLocation" ErrorMessage="Please Select Entity"
                                    runat="server" ValueToCompare="< Select >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>
                        </td>

                        <td class="td3">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                User
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlFilterPerformer" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please Select User" ControlToValidate="ddlFilterPerformer" runat="server"
                                ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Compliance Type
                            </label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlScopeType" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlScopeType_SelectedIndexChanged">
                                <asp:ListItem Text="Register" Value="SOW03"></asp:ListItem>
                                <asp:ListItem Text="PF Challan" Value="SOW13"></asp:ListItem>
                                <asp:ListItem Text="ESI Challan" Value="SOW14"></asp:ListItem>
                                <asp:ListItem Text="PT Challan" Value="SOW17"></asp:ListItem>
                                <asp:ListItem Text="Return" Value="SOW05"></asp:ListItem>
                            </asp:DropDownList>
                        </td>

                        <td class="td3">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;"
                                runat="server" id="lblState">
                                State
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlScopeType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr style="display: none;">
                        <td class="td1">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Reviewer:
                            </label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlFilterReviewer" Style="padding: 0px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>
                            <%--<asp:CompareValidator ErrorMessage="Please select Reviewer." ControlToValidate="ddlFilterReviewer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />--%>
                        </td>
                        <td class="td3">
                            <label style="width: 105px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Approver:
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlFilterApprover" Style="padding: 0px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr style="display: none;">
                        <td class="td1">
                            <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Compliance Category
                            </label>
                        </td>
                        <td class="td2" style="display: none;">
                            <asp:DropDownList runat="server" ID="ddlComplianceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceCatagory_SelectedIndexChanged">
                            </asp:DropDownList>
                            <%--<asp:CompareValidator ErrorMessage="Please select Compliance Catagory." ControlToValidate="ddlComplianceCatagory"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" Display="None" ValidationGroup="ComplianceInstanceValidationGroup" /> --%>
                        </td>
                    </tr>

                    <tr style="display: none;">
                        <td class="td1">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Act:
                            </label>
                        </td>
                        <td class="td2">
                            <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvActList">
                                <asp:Repeater ID="rptActList" runat="server">
                                    <HeaderTemplate>
                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                            <tr>
                                                <td style="width: 100px;">
                                                    <asp:CheckBox ID="actSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                                <td style="width: 282px;">
                                                    <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 20px;">
                                                <asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" /></td>
                                            <td style="width: 200px;">
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                    <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                        <td class="td3"></td>
                        <td class="td4"></td>
                    </tr>

                    <tr style="display: none;">
                        <td class="td1">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Filter:
                            </label>
                        </td>
                        <td class="td2">
                            <asp:TextBox runat="server" ID="tbxFilter" Style="height: 16px; width: 390px;" MaxLength="50" AutoPostBack="true"
                                OnTextChanged="tbxFilter_TextChanged" />
                        </td>
                        <td class="td3">
                            <%-- <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Event Based:
                            </label>--%>
                        </td>
                        <td class="td4"></td>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">
                            <asp:Panel ID="Panel1" Width="100%" Style="min-height: 10px; overflow-y: auto; margin-bottom: 10px;" runat="server">
                                <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdComplianceRoleMatrix_RowCreated"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdComplianceRoleMatrix_Sorting"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50" Width="100%" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound"
                                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging" ShowHeaderWhenEmpty="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Compliance ID" SortExpression="ComplianceID" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ID")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Act" SortExpression="ActName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="lblAct" runat="server" Text='<%# Eval("ActName")%>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="State" SortExpression="SM_Name">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblState" runat="server" Text='<%# Eval("SM_Name")%>' ToolTip='<%# Eval("SM_Name") %>'></asp:Label>
                                                    <asp:Label ID="lblStateCode" runat="server" Text='<%# Eval("StateID")%>' ToolTip='<%# Eval("StateID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Sections" SortExpression="Sections">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label runat="server" Text='<%# Eval("Sections")%>' ToolTip='<%# Eval("Sections") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Short Description" SortExpression="ShortDescription">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label ID="lblShortDesc" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Required Form" SortExpression="RequiredForms">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblReqForm" runat="server" Text='<%# Eval("RequiredForms") %>' ToolTip='<%# Eval("RequiredForms") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAssignSelectAll" Text="Assign" runat="server" AutoPostBack="true"
                                                    Checked="true" Enabled="false" OnCheckedChanged="chkAssignSelectAll_CheckedChanged" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkAssign" runat="server" Checked="true" Enabled="false" />
                                                <%--AutoPostBack="true" OnCheckedChanged="chkAssign_CheckedChanged"--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                            <asp:Button Text="Save" runat="server" ID="btnSaveAssignment" OnClick="btnSave_Click" CssClass="button"
                                ValidationGroup="ComplianceInstanceValidationGroup" CausesValidation="true" Visible="false" />
                        </td>
                    </tr>
                </table>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

