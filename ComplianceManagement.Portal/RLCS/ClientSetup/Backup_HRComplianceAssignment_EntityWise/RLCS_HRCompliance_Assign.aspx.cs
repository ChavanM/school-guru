﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup
{
    
    public partial class RLCS_HRCompliance_Assign : System.Web.UI.Page
    {
        public static List<int> branchList = new List<int>();
        public static List<string> lstStatesToFilter = new List<string>();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomers();
                BindLocationFilter();

                List<string> lstStates = new List<string>();
                BindStates();
                //BindComplianceCategories();
               
                BindUsers(ddlFilterPerformer);
                //BindUsers(ddlFilterReviewer);
                //BindUsers(ddlFilterApprover);
                //BindActList();

                //AddFilter();
                tbxFilterLocation.Text = "< Select >";
                txtactList.Text = "< Select >";
            }
        }

        protected void AddFilter(int pageIndex = 0)
        {
            try
            {
                ViewState["pagefilter"] = Convert.ToString(Request.QueryString["Param"]);
                if (ViewState["pagefilter"] != null)
                {
                    if (Convert.ToString(ViewState["pagefilter"]).Equals("location"))
                    {
                        //divFilterUsers.Visible = false;
                        //FilterLocationdiv.Visible = true;
                    }
                    else if (Convert.ToString(ViewState["pagefilter"]).Equals("user"))
                    {
                        //divFilterUsers.Visible = true;
                        //FilterLocationdiv.Visible = false;
                    }
                    else
                    {
                        //divFilterUsers.Visible = false;
                        //FilterLocationdiv.Visible = false;
                        //if (AuthenticationHelper.Role == "EXCT")
                        //{
                        //    btnAddComplianceType.Visible = false;
                        //}

                        //BindComplianceInstances();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                
                ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomers(customerID, 95);
                ddlCustomer.DataBind();

                //if (AuthenticationHelper.Role != "CADMN")
                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                BindLocationFilter();
                BindUsers(ddlFilterPerformer);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {
                    tvFilterLocation.Nodes.Clear();

                    var bracnhes = RLCS_Master_Management.GetAll_Entities(customerID);

                    //TreeNode node = new TreeNode("< All >", "-1");
                    //node.Selected = true;
                    //tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    //tvFilterLocation_SelectedNodeChanged(null, null);       
                }         
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStates()
        {
            try
            {
                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {
                    ddlState.DataTextField = "SM_Name";
                    ddlState.DataValueField = "SM_Code";

                    if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                    {
                        branchList.Clear();
                        GetAll_Branches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        branchList.ToList();
                    }

                    lstStatesToFilter = RLCS_Master_Management.GetBranchStateDetails(branchList);

                    //if (lstStatesToFilter.Count > 0)
                    //    BindStates(lstStatesToFilter);

                    var lstStates = RLCS_ClientsManagement.GetAllStates();

                    if (lstStatesToFilter.Count > 0 && lstStates.Count > 0)
                    {
                        lstStates = lstStates.Where(row => lstStatesToFilter.Contains(row.SM_Code)).ToList();
                    }

                    ddlState.DataSource = lstStates;
                    ddlState.DataBind();

                    ddlState.Items.Insert(0, new ListItem("< Select >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue))
            {
                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {
                    branchList.Clear();
                    GetAll_Branches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    branchList.ToList();
                }
            }

            if (branchList.Count > 0)
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                BindStates();
                BindGrid();
            }
            else
            {
                tvFilterLocation.SelectedNode.Selected = false;

                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "No Branch available for Selected Entity, Please Create Location/Branch before Compliance Assignment.";
            }
        }
        
        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {

                    ddlUserList.DataTextField = "Name";
                    ddlUserList.DataValueField = "ID";
                    ddlUserList.Items.Clear();

                    var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);

                    ddlUserList.DataSource = users;
                    ddlUserList.DataBind();

                    ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }
        
        private void BindComplianceCategories()
        {
            try
            {
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";
                ddlComplianceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplianceCatagory.DataBind();
                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlComplianceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindActList();
            grdComplianceRoleMatrix.PageIndex = 0;
            BindGrid();
        }

        private void BindComplianceMatrix(string flag, string IFCheckedEvent)
        {
            try
            {
                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                
                int complianceTypeID = Convert.ToInt32(ddlScopeType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                              
                int branchID = -1;
                if (!(tbxFilterLocation.Text.Trim().Equals("< Select >")))
                {
                    branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                }
                List<int> actIds = new List<int>();
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked)
                    {
                        actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                    }
                }
                //TempGetByType
                if (complianceTypeID != -1 || complianceCatagoryID != -1 || actIds.Count != 0)
                {

                    if (flag == "N" && IFCheckedEvent == "N")
                    {
                        //grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.GetByType(complianceTypeID, complianceCatagoryID, cbApprover.Checked, branchID, actIds);
                        grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.TempGetByType(complianceTypeID, complianceCatagoryID, false, branchID, actIds);
                    }
                    else if (flag == "N" && IFCheckedEvent == "Y")
                    {
                        //grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.GetByTypeFilterEvent(complianceTypeID, complianceCatagoryID, "Y", cbApprover.Checked, branchID, actIds);
                        grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.TempGetByTypeFilterEvent(complianceTypeID, complianceCatagoryID, "Y", false, branchID, actIds);
                    }
                    else if (flag == "Y" && IFCheckedEvent == "N")
                    {
                        //GetByTypeFilterEvent
                        //grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.GetByType(complianceTypeID, complianceCatagoryID, cbApprover.Checked, branchID, actIds, tbxFilter.Text.Trim());
                        grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.TempGetByType(complianceTypeID, complianceCatagoryID, false, branchID, actIds, tbxFilter.Text.Trim());
                    }
                    else if (flag == "Y" && IFCheckedEvent == "Y")
                    {
                        //grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.GetByTypeFilterEvent(complianceTypeID, complianceCatagoryID, "Y", cbApprover.Checked, branchID, actIds, tbxFilter.Text.Trim());
                        grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.TempGetByTypeFilterEvent(complianceTypeID, complianceCatagoryID, "Y", false, branchID, actIds, tbxFilter.Text.Trim());
                    }
                    grdComplianceRoleMatrix.DataBind();

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindGrid()
        {
            //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int customerID = -1;

            if (AuthenticationHelper.Role == "CADMN")
            {
                //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "IMPT")
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }

            string scopeType = Convert.ToString(ddlScopeType.SelectedValue.Trim());
            string stateCode = Convert.ToString(ddlState.SelectedValue);
            
            grdComplianceRoleMatrix.DataSource = RLCS_ComplianceManagement.GetHRComplianceList_Assignment(scopeType, branchList, lstStatesToFilter, stateCode, false);
            grdComplianceRoleMatrix.DataBind();

            if (grdComplianceRoleMatrix.Rows.Count > 0)
                btnSaveAssignment.Visible = true;
            else
                btnSaveAssignment.Visible = false;
        }

        private void BindActList()
        {
            try
            {
                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                int complianceTypeID = Convert.ToInt32(ddlScopeType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);

                List<ActView> ActList = ActManagement.GetAll(complianceCatagoryID, complianceTypeID);
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        //int branchID = tvBranches.SelectedNode != null ? Convert.ToInt32(tvBranches.SelectedNode.Value) : -1;
                        int branchID = -1;
                        if (!(tbxFilterLocation.Text.Trim().Equals("< Select >")))
                        {
                            branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                        }
                        if (branchID != -1)
                        {
                            int StateId = CustomerBranchManagement.GetByID(branchID).StateID;
                            ActList = ActList.Where(entry => entry.StateID == StateId).ToList();
                        }
                    }
                }

                rptActList.DataSource = ActList;
                rptActList.DataBind();

                if (complianceCatagoryID != -1 || complianceTypeID != -1)
                {

                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");

                        if (!chkAct.Checked)
                        {
                            chkAct.Checked = true;
                        }
                    }
                    CheckBox actSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("actSelectAll");
                    actSelectAll.Checked = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int complianceTypeID = Convert.ToInt32(ddlScopeType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                var ComplianceRoleMatrixList = Business.ComplianceManagement.TempGetByType(complianceTypeID, complianceCatagoryID);
                if (direction == SortDirection.Ascending)
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
                    }
                }
                SaveCheckedValues();
                grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
                grdComplianceRoleMatrix.DataBind();
                PopulateCheckedValues();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();
                                
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();

                BindGrid();

                PopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlScopeType_SelectedIndexChanged(object sender, EventArgs e)
        {            
            if (!string.IsNullOrEmpty(ddlScopeType.SelectedValue))
            {
                ViewState["CHECKED_ITEMS"] = null;
                if (ddlScopeType.SelectedValue.Trim().Equals("SOW03"))
                {
                    //ddlState.ClearSelection();
                    lblState.Visible = true;
                    ddlState.Visible = true;                    
                }
                else
                {
                    ddlState.ClearSelection();
                    lblState.Visible = false;
                    ddlState.Visible = false;
                }

                BindGrid();
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlScopeType.SelectedValue.Trim().Equals("SOW03"))
                {
                    //ddlState.ClearSelection();
                    lblState.Visible = true;
                    ddlState.Visible = true;
                }
                else
                {
                    ddlState.ClearSelection();
                    lblState.Visible = false;
                    ddlState.Visible = false;
                }

                BindGrid();
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            //BindComplianceMatrix("N", "N");
            
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
           
        }

        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        protected void chkAssignSelectAll_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            foreach (GridViewRow row in grdComplianceRoleMatrix.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkAssign");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }

        }

        protected void chkAssign_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                if (((CheckBox)row.FindControl("chkAssign")).Checked)
                {

                    countCheckedCheckbox = countCheckedCheckbox + 1;

                }

            }
            if (countCheckedCheckbox == grdComplianceRoleMatrix.Rows.Count)
            {
                chkAssignSelectAll.Checked = true;
            }
            else
            {
                chkAssignSelectAll.Checked = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool assignSuccess = false;

                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {
                    if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                    {
                        string scopeType = Convert.ToString(ddlScopeType.SelectedValue.Trim());

                        var complianceList = new List<ComplianceAsignmentProperties>();
                        SaveCheckedValues();
                        complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                        if (complianceList.Count > 0)
                        {
                            List<TempAssignmentTable> Tempassignments = new List<TempAssignmentTable>();

                            if (scopeType.Trim().ToUpper().Equals("SOW03"))
                            {
                                RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails = null;

                                branchList.Clear();
                                GetAll_Branches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                                branchList.ToList();

                                if (branchList.Count > 0)
                                {
                                    branchList.ForEach(eachBranch =>
                                    {
                                    //Get CustomerBranchID, StartDate from RLCS_CustomerBranch_ClientsLocation_Mapping
                                    customerBranchDetails = RLCS_Master_Management.GetClientLocationDetails(eachBranch);

                                        if (customerBranchDetails != null && customerBranchDetails.CM_EstablishmentType != null)
                                        {
                                            complianceList.ForEach(eachSelectedCompliance =>
                                            {
                                                if (eachSelectedCompliance.Performer) //Performer - means Compliance Selected or Not
                                            {
                                                    var mapCompliance = RLCS_ComplianceManagement.Check_Register_Compliance_Map(eachSelectedCompliance.ComplianceId,
                                                        customerBranchDetails.CM_State, customerBranchDetails.CM_EstablishmentType.Trim().ToUpper());

                                                    if (mapCompliance)
                                                    {
                                                    //Performer
                                                    TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                        {
                                                            ComplianceId = eachSelectedCompliance.ComplianceId,
                                                            CustomerBranchID = eachBranch,
                                                            RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                        UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                            IsActive = true,
                                                            CreatedOn = DateTime.Now
                                                        };

                                                        Tempassignments.Add(TempAssP);

                                                    //Reviewer
                                                    TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                        {
                                                            ComplianceId = eachSelectedCompliance.ComplianceId,
                                                            CustomerBranchID = eachBranch,
                                                            RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                        UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                            IsActive = true,
                                                            CreatedOn = DateTime.Now
                                                        };

                                                        Tempassignments.Add(TempAssR);
                                                    }
                                                }
                                            });
                                        }
                                        else
                                            LoggerMessage.InsertErrorMsg_DBLog("customerBranchDetails=null",
                                                MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    });
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Branch Available for Selected Entity.";
                                }
                            }
                            else if (scopeType.Trim().ToUpper().Equals("SOW13"))
                            {
                                int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                                var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);

                                if (!string.IsNullOrEmpty(clientID))
                                {
                                    var lstBranches = RLCSManagement.GetCustomerBranchesWithPFOrESICCode(clientID, "EPF", "", "B");

                                    if (lstBranches.Count > 0)
                                    {
                                        lstBranches.ForEach(eachCustBranch =>
                                        {
                                            complianceList.ForEach(eachSelectedCompliance =>
                                            {
                                                if (eachCustBranch.AVACOM_BranchID != null)
                                                {
                                                    if (eachSelectedCompliance.Performer) //Performer - means Compliance Selected or Not
                                                {
                                                    //Performer
                                                    TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                        {
                                                            ComplianceId = eachSelectedCompliance.ComplianceId,
                                                            CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                            RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                        UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                            IsActive = true,
                                                            CreatedOn = DateTime.Now
                                                        };

                                                        Tempassignments.Add(TempAssP);

                                                    //Reviewer
                                                    TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                        {
                                                            ComplianceId = eachSelectedCompliance.ComplianceId,
                                                            CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                            RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                        UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                            IsActive = true,
                                                            CreatedOn = DateTime.Now
                                                        };

                                                        Tempassignments.Add(TempAssR);

                                                    }
                                                }
                                            });
                                        });
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "NO Branch Assigned with EPF Code.";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Select Entity.";
                                }
                            }
                            else if (scopeType.Trim().ToUpper().Equals("SOW14"))
                            {
                                int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                                var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);

                                if (!string.IsNullOrEmpty(clientID))
                                {
                                    var lstBranches = RLCSManagement.GetCustomerBranchesWithPFOrESICCode(clientID, "ESI", "", "B");

                                    if (lstBranches.Count > 0)
                                    {
                                        lstBranches.ForEach(eachCustBranch =>
                                        {
                                            complianceList.ForEach(eachSelectedCompliance =>
                                            {
                                                if (eachCustBranch.AVACOM_BranchID != null)
                                                {
                                                    if (eachSelectedCompliance.Performer) //Performer - means Compliance Selected or Not
                                                {
                                                    //Performer
                                                    TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                        {
                                                            ComplianceId = eachSelectedCompliance.ComplianceId,
                                                            CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                            RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                        UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                            IsActive = true,
                                                            CreatedOn = DateTime.Now
                                                        };

                                                        Tempassignments.Add(TempAssP);

                                                    //Reviewer
                                                    TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                        {
                                                            ComplianceId = eachSelectedCompliance.ComplianceId,
                                                            CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                            RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                        UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                            IsActive = true,
                                                            CreatedOn = DateTime.Now
                                                        };

                                                        Tempassignments.Add(TempAssR);
                                                    }
                                                }
                                            });
                                        });
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "NO Branch Assigned with ESI Code.";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Select Entity.";
                                }
                            }
                            else if (scopeType.Trim().ToUpper().Equals("SOW15")) //LWF Challan
                            {

                            }
                            else if (scopeType.Trim().ToUpper().Equals("SOW17")) //PT Challan
                            {
                                int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                                var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);

                                complianceList.ForEach(eachSelectedCompliance =>
                                {
                                    if (eachSelectedCompliance.Performer) //Performer - means Compliance Selected or Not
                                {
                                        if (!string.IsNullOrEmpty(eachSelectedCompliance.StateCode))
                                        {
                                            var lstBranches = RLCSManagement.GetCustomerBranchesByPTState(customerID, clientID, eachSelectedCompliance.StateCode, "B");

                                            if (lstBranches.Count > 0)
                                            {
                                                lstBranches.ForEach(eachCustBranch =>
                                                {
                                                //Performer
                                                TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                    {
                                                        ComplianceId = eachSelectedCompliance.ComplianceId,
                                                        CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                        RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                    UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                        IsActive = true,
                                                        CreatedOn = DateTime.Now
                                                    };

                                                    Tempassignments.Add(TempAssP);

                                                //Reviewer
                                                TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                    {
                                                        ComplianceId = eachSelectedCompliance.ComplianceId,
                                                        CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                        RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                    UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                        IsActive = true,
                                                        CreatedOn = DateTime.Now
                                                    };

                                                    Tempassignments.Add(TempAssR);
                                                });
                                            }
                                        }
                                    }
                                });
                            }

                            if (Tempassignments.Count != 0)
                            {
                                Business.ComplianceManagement.AddDetailsTempAssignmentTable(Tempassignments);
                                ClearSelection();
                                assignSuccess = true;
                            }

                            if (assignSuccess)
                            {
                                BindGrid();
                                ViewState["CHECKED_ITEMS"] = null;
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Select one or more compliance to assign";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select Entity.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select Customer.";
                }
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This kind of assignment is not supported in current version.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ClearSelection()
        {
            tbxFilterLocation.Text = "< Select >";
            ddlFilterPerformer.SelectedValue = "-1";
            ddlFilterReviewer.SelectedValue = "-1";
            ddlFilterApprover.SelectedValue = "-1";
            ddlComplianceCatagory.SelectedValue = "-1";
            ddlScopeType.SelectedValue = "-1";
        }

        private void SaveCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = new List<ComplianceAsignmentProperties>();
                // Check in the Session
                if (ViewState["CHECKED_ITEMS"] != null)
                    complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    ComplianceAsignmentProperties complianceProperties = new ComplianceAsignmentProperties();
                    complianceProperties.ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkAssign")).Checked;
                    complianceProperties.StateCode = ((Label)gvrow.FindControl("lblStateCode")).Text;

                    if (complianceProperties.Performer)
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }
                }

                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == index).FirstOrDefault();
                        if (rmdata != null)
                        {
                            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkAssign");
                            chkPerformer.Checked = rmdata.Performer;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region To Find Branches not Entities and Sub-Entities
          
        public static List<NameValueHierarchy> GetAll_Branches(int customerID, int selectedEntityID)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID == selectedEntityID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    LoadChildBranches(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadChildBranches(int customerid, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.CustomerID == customerid
                                                && row.ParentID == nvp.ID
                                                && row.Type != 1
                                                select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                branchList.Add(item.ID);
                LoadChildBranches(customerid, item, false, entities);
            }
        }

        #endregion
    }
}