﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup
{
    public partial class RLCS_CustomerCorporateList : System.Web.UI.Page
    {
        public bool MGM_KEy;
        protected static int customerid;
        protected void Page_Load(object sender, EventArgs e)
        {
            bool ISCADMN = false;
            if (AuthenticationHelper.Role == "CADMN")
            {
                ISCADMN = true;
            }
            bool ISIMPL = false;
            if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "RREV")
            {
                ISIMPL = true;
            }
            if (AuthenticationHelper.Role == "MGMT")
            {
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            MGM_KEy = CaseManagement.CheckForClient(customerid, "MGMT_CompanyAdmin");
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                if (!IsPostBack)
                {
                    bool ISMGMT = false;
                    if (MGM_KEy && AuthenticationHelper.Role != "EXCT")
                    {
                        ISMGMT = true;
                    }
                    else
                    {
                        ISMGMT = false;
                    }

                    if (HttpContext.Current.Request.IsAuthenticated && (ISMGMT || ISCADMN || ISIMPL))
                    {
                        Session["CustomerID"] = null;

                        if (!(AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT")
                            || AuthenticationHelper.Role.Equals("SPADM") || AuthenticationHelper.Role.Equals("DADMN")))
                        {
                            btnAddCustomer.Visible = false;
                        }

                        BindServiceProvider();
                        //BindCustomers();
                        ddlSPList_SelectedIndexChanged(sender, e);
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }

            udcInputForm.OnSaved += (inputForm, args) => { BindCustomers(); };

            ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeCmbox", "initializeCombobox();", true);
        }

        public void BindServiceProvider()
        {
            try
            {
                int serviceProviderID = -1;
                int distributorID = -1;
                int IsSPDist = 2;

                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    IsSPDist = 0;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    IsSPDist = 1;
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    int? serProID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));

                    if (serProID != null)
                        serviceProviderID = Convert.ToInt32(serProID);
                    
                    IsSPDist = 0;
                }

                ddlSPList.DataTextField = "Name";
                ddlSPList.DataValueField = "ID";

                ddlSPList.Items.Clear();

                //ddlSPList.DataSource = CustomerManagement.FillServiceProvider(serviceProviderID);
                ddlSPList.DataSource = CustomerManagement.Fill_ServiceProviders_Distributors(IsSPDist, serviceProviderID, distributorID);
                ddlSPList.DataBind();
                
                if (ddlSPList.Items.FindByValue(serviceProviderID.ToString()) != null)
                    ddlSPList.Items.FindByValue(serviceProviderID.ToString()).Selected = true;
                else if (ddlSPList.Items.FindByValue(distributorID.ToString()) != null)
                    ddlSPList.Items.FindByValue(distributorID.ToString()).Selected = true;
                else
                    ddlSPList.Items.Insert(0, new ListItem("< Select Service Provider >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindCustomers()
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlSPList.SelectedValue) && ddlSPList.SelectedValue != "-1")
                {
                    int customerID = -1;
                    if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    }
                    else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "IMPT")
                    {
                        customerID = -1;
                    }
                    else
                    {
                        var aa = UserManagement.GetByID(AuthenticationHelper.UserID).IsAuditHeadOrMgr ?? null;
                        if (aa == "AM" || aa == "AH")
                        {
                            customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                        }
                    }

                    var customerData = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, -1, Convert.ToInt32(ddlSPList.SelectedValue), 2, true, tbxFilter.Text);
                    //var customerData = CustomerManagement.GetAll_HRComplianceCustomers(customerID, Convert.ToInt32(ddlSPList.SelectedValue), 2, tbxFilter.Text);
                    
                    List<object> dataSource = new List<object>();

                    foreach (var customerInfo in customerData)
                    {
                        dataSource.Add(new
                        {
                            customerInfo.ID,
                            customerInfo.ParentID,
                            customerInfo.Name,
                            customerInfo.Address,
                            customerInfo.Industry,
                            customerInfo.BuyerName,
                            customerInfo.BuyerContactNumber,
                            customerInfo.BuyerEmail,
                            customerInfo.CreatedOn,
                            customerInfo.IsDeleted,
                            customerInfo.StartDate,
                            customerInfo.EndDate,
                            customerInfo.DiskSpace,
                            IsServiceProvider = customerInfo.IsServiceProvider != null ? ((Convert.ToBoolean(customerInfo.IsServiceProvider) == true) ? "Yes" : "No") : "No",
                            DistributorOrCustomer = customerInfo.IsDistributor != null ? ((Convert.ToBoolean(customerInfo.IsDistributor) == true) ? "Professional Firm" : "Customer") : "Customer",
                            //DistributorOrCustomer = customerInfo.IsDistributor != null ? ((Convert.ToBoolean(customerInfo.IsDistributor) == true) ? "Distributor" : "Customer") : "Customer",
                            ServiceProvider = customerInfo.ServiceProviderID != null ? ((Convert.ToInt32(customerInfo.ServiceProviderID) == 94) ? "TeamLease" : "Avantis") : "",
                            CorporateID = RLCS_Master_Management.GetCorporateIDByCustID(customerInfo.ID),
                            Status = Enumerations.GetEnumByID<CustomerStatus>(Convert.ToInt32(customerInfo.Status != null ? (int)customerInfo.Status : -1)),
                        });
                    }

                    grdCustomer.DataSource = dataSource;
                    grdCustomer.DataBind();

                    upCustomerList.Update();
                }
                else
                { 
                    //Empty DataTable to execute the “else-condition”  
                    DataTable dt = new DataTable();
                    grdCustomer.DataSource = dt;
                    grdCustomer.DataBind();                   

                    upCustomerList.Update();

                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Service Provider";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null && !string.IsNullOrEmpty(e.CommandName))
                {
                    int customerID = Convert.ToInt32(e.CommandArgument);

                    Session["CustomerID"] = customerID;

                    if (e.CommandName.Equals("EDIT_CUSTOMER"))
                    {
                        udcInputForm.EditCustomerInformation(customerID);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                    }
                    else if (e.CommandName.Equals("DELETE_CUSTOMER"))
                    {
                        if (CustomerManagement.CustomerDelete(customerID))
                        {
                            CustomerManagement.Delete(customerID);
                            CustomerManagementRisk.Delete(customerID);

                            BindCustomers();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('Customer is associated with assigned compliance, can not be deleted')", true);
                        }
                    }
                    else if (e.CommandName.Equals("VIEW_COMPANIES"))
                    {
                        Session["CustomerID"] = customerID;
                        Session["ParentID"] = null;
                        Response.Redirect("~/RLCS/ClientSetup/RLCS_EntityBranchList.aspx", false);
                        //Response.Redirect("~/Customers/CustomerBranchList.aspx", false);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlSPList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCustomers();

                if (!string.IsNullOrEmpty(ddlSPList.SelectedValue))
                {
                    if (ddlSPList.SelectedValue != "-1")
                        if (!AuthenticationHelper.Role.Equals("CADMN"))
                            btnAddCustomer.Visible = true;
                        else
                            btnAddCustomer.Visible = false;
                }
                else
                    btnAddCustomer.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindServiceProvider();
            //BindCustomers();
            ddlSPList_SelectedIndexChanged(sender, e);
        }
        protected void btnRefresh1_Click(object sender, EventArgs e)
        {
           
            BindCustomers();
            
        }
        protected void grdCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CheckBoxValueSaved();
                grdCustomer.PageIndex = e.NewPageIndex;
                BindCustomers();
                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnAddCustomer_Click(object sender, EventArgs e)
        {
            // TBD
            udcInputForm.AddCustomer();
            BindCustomers();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);

            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = 0;
                BindCustomers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdCustomer_OnPreRender(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role.Equals("CADMN"))
                {
                    foreach (DataControlField column in grdCustomer.Columns)
                    {
                        if (column.HeaderText == "")
                            column.Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomer_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "IMPT")
                {
                    customerID = -1;
                }
                
                var customerlist = CustomerManagement.GetAll_HRComplianceCustomers(customerID, Convert.ToInt32(ddlSPList.SelectedValue), 2, tbxFilter.Text);

                //var customerlist = CustomerManagement.GetAll(customerID, tbxFilter.Text);
                //var customerData = CustomerManagement.GetAll(customerID, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var customerInfo in customerlist)
                {
                    dataSource.Add(new
                    {
                        customerInfo.ID,
                        customerInfo.ParentID,
                        customerInfo.Name,
                        customerInfo.Address,
                        customerInfo.Industry,
                        customerInfo.BuyerName,
                        customerInfo.BuyerContactNumber,
                        customerInfo.BuyerEmail,
                        customerInfo.CreatedOn,
                        customerInfo.IsDeleted,
                        customerInfo.StartDate,
                        customerInfo.EndDate,
                        customerInfo.DiskSpace,
                        IsServiceProvider = customerInfo.IsServiceProvider != null ? ((Convert.ToBoolean(customerInfo.IsServiceProvider) == true) ? "Yes" : "No") : "No",
                        DistributorOrCustomer = customerInfo.IsDistributor != null ? ((Convert.ToBoolean(customerInfo.IsDistributor) == true) ? "Professional Firm" : "Customer") : "Customer",
                        //DistributorOrCustomer = customerInfo.IsDistributor != null ? ((Convert.ToBoolean(customerInfo.IsDistributor) == true) ? "Distributor" : "Customer") : "Customer",
                        ServiceProvider = customerInfo.ServiceProviderID != null ? ((Convert.ToInt32(customerInfo.ServiceProviderID) == 94) ? "TeamLease" : "Avantis") : "",
                        CorporateID = RLCS_Master_Management.GetCorporateIDByCustID(customerInfo.ID),
                        Status = Enumerations.GetEnumByID<CustomerStatus>(Convert.ToInt32(customerInfo.Status != null ? (int)customerInfo.Status : -1)),
                    });
                }

                if (direction == SortDirection.Ascending)
                {
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdCustomer.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomer.Columns.IndexOf(field);
                    }
                }

                grdCustomer.DataSource = dataSource;
                grdCustomer.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdCustomer_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl= "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        //added by sudarshan for sending mail for server down

        private void CheckBoxValueSaved()
        {
            List<int> chkList = new List<int>();
            int index = -1;
            foreach (GridViewRow gvrow in grdCustomer.Rows)
            {
                index = Convert.ToInt32(grdCustomer.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkCustomer")).Checked;

                if (ViewState["CustomerID"] != null)
                    chkList = (List<int>)ViewState["CustomerID"];

                if (result)
                {
                    if (!chkList.Contains(index))
                        chkList.Add(index);
                }
                else
                    chkList.Remove(index);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["CustomerID"] = chkList;
        }

        private void AssignCheckBoxexValue()
        {
            List<int> chkList = (List<int>)ViewState["CustomerID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdCustomer.Rows)
                {
                    int index = Convert.ToInt32(grdCustomer.DataKeys[gvrow.RowIndex].Value);
                    if (chkList.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCustomer");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        protected void btnSendNotification_Click(object sender, EventArgs e)
        {
            try
            {
                upServerDownTimeDetails.Update();
                ddlStartTime.Items.Clear();
                ddlEndTime.Items.Clear();
                SetTime(ddlStartTime);
                SetTime(ddlEndTime);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenNotificationTime", "$(\"#divNotificationTime\").dialog('open');", true);
                
                //CheckBoxValueSaved();
                //List<int> chkList = (List<int>)ViewState["CustomerID"];
                //if (chkList != null)
                //{
                //    List<User> userList = UserManagement.GetAll(-1).Where(entry => entry.CustomerID != null).ToList();
                //    userList = userList.Where(entry => chkList.Contains((int)entry.CustomerID)).ToList();
                //    string ReplyEmailAddressName = "Avantis";

                //    foreach (User user in userList)
                //    {
                //        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                //        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                                .Replace("@User", username)
                //                                .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                                .Replace("@Password", "")
                //                                .Replace("@From", ReplyEmailAddressName);

                //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM account created.", message);
                //    }

                //}

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upServerDownTimeDetails_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;

                if (DateTime.TryParseExact(txtDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDowntimeDatePicker", string.Format("initializeDowntimeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDowntimeDatePicker", "initializeDowntimeDatePicker(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upCustomer_Load(object sender, EventArgs e)
        {
            try
            {  
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void SetTime(DropDownList DDl)
        {
            DDl.Items.Add(new ListItem("Select", "-1"));
            for (int i = 0; i <= 12; i++)
            {
                if (i != 12)
                    DDl.Items.Add(new ListItem(i.ToString("00") + " AM", i + " AM"));
                else
                    DDl.Items.Add(new ListItem(i.ToString("00") + " PM", i + " PM"));
            }

            for (int i = 1; i <= 11; i++)
            {
                DDl.Items.Add(new ListItem(i.ToString("00") + " PM", i + " PM"));
            }
        }

        protected void btnSend_click(object sender, EventArgs e)
        {
            new Thread(() => { SendNotification(); }).Start();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenNotificationTime", "$(\"#divNotificationTime\").dialog('close');", true);
        }

        private void SendNotification()
        {
            try
            {

                string dated = txtDate.Text;
                string starttime = ddlStartTime.SelectedValue;
                string endtime = ddlEndTime.SelectedValue;

                CheckBoxValueSaved();
                List<int> chkList = (List<int>)ViewState["CustomerID"];
                if (chkList != null)
                {
                    List<User> userList = UserManagement.GetAll(-1).Where(entry => entry.CustomerID != null).ToList();
                    userList = userList.Where(entry => chkList.Contains((int)entry.CustomerID)).ToList();
                    string ReplyEmailAddressName = "Avantis";

                    foreach (User user in userList)
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ServerDown
                                                .Replace("@User", username)
                                                .Replace("@Dated", dated)
                                                .Replace("@StartTime", starttime)
                                                .Replace("@EndTime", endtime)
                                                .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                .Replace("@From", ReplyEmailAddressName);

                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM server down.", message);
                    }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

    }
}