﻿<%@ Page Title="HR+ Compliance-Assignment::Setup HR+" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCS_HRCompliance_Assign.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_HRCompliance_Assign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/NewCSS/arrow-Progressbar.css" rel="stylesheet" />
    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>

    <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.default.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('#divReAssignCompliance').hide();
        });

        $(function () {
            initializeCombobox();
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeCombobox() {
            $("#<%= ddlCustomer.ClientID %>").combobox();
            $("#<%= ddlFilterPerformer.ClientID %>").combobox();
            $("#<%= ddlFilterReviewer.ClientID %>").combobox();
            $("#<%= ddlFilterApprover.ClientID %>").combobox();
            $("#<%= ddlScopeType.ClientID %>").combobox();
            $("#<%= ddlComplianceCatagory.ClientID %>").combobox();
            $("#<%= ddlState.ClientID %>").combobox();
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }

        }

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        function OpenUploadAssignPopup() {
            
            //Loader to Show
            var selectedCustID = $('#<%= ddlCustomer.ClientID %>').val();

            if (selectedCustID != undefined && selectedCustID != null && selectedCustID != -1 && selectedCustID != "") {
                $('#divReAssignCompliance').show();

                var myWindowAdv = $("#divReAssignCompliance");

                myWindowAdv.kendoWindow({
                    width: "50%",
                    height: '50%',
                    //maxHeight: '90%',
                    //minHeight:'50%',
                    title: "Upload Compliance Assignment",
                    visible: false,
                    actions: ["Close"],                                   
                });

                $('#iframeReAssign').attr('src', 'about:blank');
                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeReAssign').attr('src', "/RLCS/HRPlus_ComplianceAssignmentUpload.aspx?CustID=" + selectedCustID);

                var dialog = $("#divReAssignCompliance").data("kendoWindow");
                dialog.title("Upload Compliance Assignment");
            }
            return false;
        }

        function OnTreeClick(evt) {

            debugger;

            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);

                __doPostBack('', '');
            }
        }
        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }
        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

        function checkAllState(cb) {

            cb.checked = true;
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkState") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckStateHeader() {
            var rowCheckBox = $("#RepeaterTable1 input[id*='chkState']");
            var rowCheckBoxSelected = $("#RepeaterTable1 input[id*='chkState']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable1 input[id*='StateSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
            $("#RepeaterTable1 input[id*='btnGetSelectedLbl']").click();
            //$("#RepeaterTable1 input[id*='StateSelectAll']").click();

        }

    </script>

    <style type="text/css">
        .td1 {
            width: 15%;
        }

        .td2 {
            width: 30%;
        }

        .td3 {
            width: 15%;
        }

        .td4 {
            width: 30%;
        }

        .td5 {
            width: 10%;
        }

        .alert-success {
            color: #5c9566;
            background-color: #dff8e3;
            border-color: #d6e9c6;
        }

        .alert {
            /*padding: 15px;	
            margin-bottom: 20px;*/
            border: 1px solid transparent;
            border-radius: 4px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/RLCS/ClientSetup/RLCS_CustomerCorporateList.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity-Branch"></asp:LinkButton>
                    <%--PostBackUrl="~/RLCS/Setup/RLCS_EntityBranchList.aspx"--%>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserMaster.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                 <div class="step done">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/RLCS/ClientSetup/RLCS_EmployeeMaster.aspx" runat="server" Text="Employee"></asp:LinkButton>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityAssignment.aspx" runat="server" Text="Entity-Branch Assignment"></asp:LinkButton>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnComAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Assign.aspx" runat="server" Text="Compliance Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAct" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Activate.aspx" runat="server" Text="Compliance Activation"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <center>
                <table runat="server" width="100%">
                    <tr>
                        <td style="width: 100%;">
                            <asp:ValidationSummary ID="vsHRCompAssign" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table runat="server" width="100%">
                    <tr>
                        <td class="td1">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Customer</label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />
                        </td>
                        <td class="td3">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Compliance Type: 
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlScopeType" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlScopeType_SelectedIndexChanged">
                                <asp:ListItem Text="Register" Value="SOW03"></asp:ListItem>
                                <asp:ListItem Text="PF Challan" Value="SOW13"></asp:ListItem>
                                <asp:ListItem Text="ESI Challan" Value="SOW14"></asp:ListItem>
                                <asp:ListItem Text="PT Challan" Value="SOW17"></asp:ListItem>
                                <asp:ListItem Text="Return" Value="SOW05"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="td5"></td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Entity/Branch: 
                            </label>
                        </td>

                        <td class="td2">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black" ShowCheckBoxes="All" onclick="OnTreeClick(event)"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Style="min-height: 50px; max-height: 200px; overflow: auto; width: 390px;"
                                    ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged" OnTreeNodeCheckChanged="tvFilterLocation_TreeNodeCheckChanged">
                                </asp:TreeView>
                              <%--  <asp:CompareValidator ControlToValidate="tbxFilterLocation" ErrorMessage="Please Select Entity"
                                    runat="server" ValueToCompare="< Select >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />--%>
                            </div>
                        </td>

                        <td class="td3">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;"
                                runat="server" id="lblState">
                                State: 
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; height: 22px; width: 50px; display:none;"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" Visible="false">
                            </asp:DropDownList>
                            <asp:TextBox runat="server" ID="txtStateList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" CssClass="txtbox" AutoComplete="off" placeholder="All States"/>
                            <div style="margin-left: 0px; position: absolute; z-index: 10; background: white; border: 1px solid; width: 390px;" id="dvState">
                                <asp:Repeater ID="rptState" runat="server">
                                    <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit record_table" id="RepeaterTable1">
                                        <tr>
                                            <td style="width: 100px;" colspan="2">
                                                <div id="divSelectAll">
                                                    <asp:CheckBox ID="StateSelectAll" Text="Select All" runat="server" AutoPostBack="true" onclick="checkAllState(this);" OnCheckedChanged="StateSelectAll_CheckedChanged"/>
                                                </div>
                                            </td> 
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td id="tdChkFiles" runat="server" style="width:100%;">                                                                       
                                                <div id="chkDivision"   onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666'">
                                                    <asp:CheckBox ID="chkState" Text='<%# Eval("SM_Name")%>' runat="server"  onclick="UncheckStateHeader();" CssClass="setChkSpace" OnCheckedChanged="chkState_CheckedChanged" AutoPostBack="true"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%; padding-bottom: 5px;">
                                                    <asp:Label ID="lblStateID" runat="server" Style="display: none" Text='<%# Eval("SM_Code")%>' ToolTip='<%# Eval("SM_Code") %>'></asp:Label>
                                                    <asp:Label ID="lblStateName" runat="server" Style="display: none" Text='<%# Eval("SM_Name")%>'  onclick='<%# string.Format("DivClick(this,\"{0}\");",Eval("SM_Code")) %>' onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666'"  ToolTip='<%# Eval("SM_Name") %>'></asp:Label>
                                                    <div id="divState" style="margin-top: 3px;cursor: pointer;" onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666';" onclick="return DivClick(this,<%# Eval("SM_Code")%>)"></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>

                            </div>
                        </td>
                        <td class="td5"></td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Performer: 
                            </label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlFilterPerformer" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please Select Performer" ControlToValidate="ddlFilterPerformer" runat="server"
                                ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        </td>
                        <td class="td3">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Reviewer: 
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlFilterReviewer" Style="padding: 0px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please select Reviewer" ControlToValidate="ddlFilterReviewer" runat="server"
                                ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />                            
                        </td>
                        <td class="td5">
                            <asp:Button ID="btnUploadAssignment" runat="server" CssClass="btn btn-primary" OnClientClick="OpenUploadAssignPopup();" Text="Upload" /></td>
                    </tr>

                    <tr style="display: none;">
                        <td class="td3">
                            <label style="width: 105px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Approver: 
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlFilterApprover" Style="padding: 0px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr style="display: none;">
                        <td class="td1">
                            <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Compliance Category: 
                            </label>
                        </td>
                        <td class="td2" style="display: none;">
                            <asp:DropDownList runat="server" ID="ddlComplianceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceCatagory_SelectedIndexChanged">
                            </asp:DropDownList>
                            <%--<asp:CompareValidator ErrorMessage="Please select Compliance Catagory." ControlToValidate="ddlComplianceCatagory"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" Display="None" ValidationGroup="ComplianceInstanceValidationGroup" /> --%>
                        </td>
                    </tr>

                    <tr>
                        <td class="td1">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight:bold; margin-top: 4px;">
                                Act Group: 
                            </label>
                        </td>
                        <td class="td2">
                            <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px; width: 390px;" id="dvActList">
                                <asp:Repeater ID="rptActList" runat="server">
                                    <HeaderTemplate>
                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 12px;" colspan="2">
                                                <asp:CheckBox ID="actSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" AutoPostBack="true" OnCheckedChanged="actSelectAll_CheckedChanged" /></td>
                                           <%-- <td style="width: 100px;">
                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" Visible="false" />
                                            </td>--%>
                                        </tr>  
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 20px;">
                                                <asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" AutoPostBack="true" OnCheckedChanged="chkAct_CheckedChanged" /></td>
                                            <td style="width: 200px;">
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                    <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("RS_ActGroup")%>' ToolTip='<%# Eval("RS_ActGroup") %>'></asp:Label>
                                                    <asp:Label ID="lblActName" runat="server" Text='<%# Eval("RS_ActGroup")%>' ToolTip='<%# Eval("RS_ActGroup") %>'></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                        <td class="td3"></td>
                        <td class="td4"></td>
                        <td class="td5"></td>
                    </tr>

                    <tr style="display: none;">
                        <td class="td1">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Filter:
                            </label>
                        </td>
                        <td class="td2">
                            <asp:TextBox runat="server" ID="tbxFilter" Style="height: 16px; width: 390px;" MaxLength="50" AutoPostBack="true"
                                OnTextChanged="tbxFilter_TextChanged" />
                        </td>
                        <td class="td3">
                            <%-- <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Event Based:
                            </label>--%>
                        </td>
                        <td class="td4"></td>
                        <td class="td5"></td>
                    </tr>

                    <tr>
                        <td colspan="6" align="center">
                            <asp:Panel ID="Panel1" Width="100%" Style="min-height: 10px; overflow-y: auto; margin-bottom: 10px;" runat="server">
                                <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdComplianceRoleMatrix_RowCreated"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdComplianceRoleMatrix_Sorting"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50" Width="100%" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound"
                                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging" ShowHeaderWhenEmpty="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                                <asp:HiddenField ID="hndScopeID" runat="server" Value='<%# Eval("ScopeID") %>' />
                                                <asp:HiddenField ID="hdnAVACOM_BranchID" runat="server" Value='<%# Eval("AVACOM_BranchID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Branch" SortExpression="AVACOM_BranchName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblbranchName" runat="server" Text='<%# Eval("AVACOM_BranchName")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Compliance ID" SortExpression="ComplianceID" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ID")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Act" SortExpression="ActName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="lblAct" runat="server" Text='<%# Eval("ActName")%>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="State" SortExpression="SM_Name">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblState" runat="server" Text='<%# Eval("SM_Name")%>' ToolTip='<%# Eval("SM_Name") %>'></asp:Label>
                                                    <asp:Label ID="lblStateCode" runat="server" Text='<%# Eval("StateID")%>' ToolTip='<%# Eval("StateID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Sections" SortExpression="Sections">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label runat="server" Text='<%# Eval("Sections")%>' ToolTip='<%# Eval("Sections") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Short Description" SortExpression="ShortDescription">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label ID="lblShortDesc" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Compliance Type" SortExpression="Compliance Type"  ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                    <asp:Label id="lblScopeName" runat="server" Text='<%# Eval("ScopeName")%>' ToolTip='<%# Eval("ScopeName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Required Form" SortExpression="RequiredForms">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblReqForm" runat="server" Text='<%# Eval("RequiredForms") %>' ToolTip='<%# Eval("RequiredForms") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAssignSelectAll" Text="Assign" runat="server" AutoPostBack="true"
                                                    Checked="true" OnCheckedChanged="chkAssignSelectAll_CheckedChanged" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkAssign" runat="server" Checked="true" />
                                                <%--AutoPostBack="true" OnCheckedChanged="chkAssign_CheckedChanged"--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No compliance to display. Either all have been assigned or all compliances are filtered out.                                       
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                            <asp:Button Text="Save" runat="server" ID="btnSaveAssignment" OnClick="btnSave_Click" CssClass="button"
                                ValidationGroup="ComplianceInstanceValidationGroup" CausesValidation="true" Visible="false" />
                        </td>
                    </tr>
                </table>
            </center>

            <div id="divReAssignCompliance">
        <iframe id="iframeReAssign" style="width: 100%; height: 100%; border: none;"></iframe>
    </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

