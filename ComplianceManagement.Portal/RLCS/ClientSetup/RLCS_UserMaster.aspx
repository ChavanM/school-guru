﻿<%@ Page Title="User Master :: Setup HR+" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCS_UserMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_UserMaster" %>

<%@ Register Src="~/RLCS/ClientSetup/RLCS_UserDetails.ascx" TagName="UserDetailsControl" TagPrefix="vit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <link href="/NewCSS/arrow-Progressbar.css" rel="stylesheet" />

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#divAddEditUserDialog').hide();            
        });

        function initializeDatePickerFunctionEndDate(FunctionEndDate) {
            var startDate = new Date();
            $('#BodyContent_udcInputForm_txtStartDate').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }

        function initializeDatePickerFunctionEndDate1(FunctionEndDate) {
            var startDate = new Date();
            $('#BodyContent_udcInputForm_txtEndDate').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }

        function initializeDatePickerFunctionEndDate2(FunctionEndDate) {
            var startDate = new Date();
            $('#BodyContent_udcInputForm_txtperiodStartDate').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }

        function initializeDatePickerFunctionEndDate3(FunctionEndDate) {
            var startDate = new Date();
            $('#BodyContent_udcInputForm_txtperiodEndDate').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }
    </script>

    
    <script type="text/javascript">
        function OpenAddUserPopup() {
          
            var distID =-1;

            if ($("#<%= ddlCustomerList.ClientID %>").val() != null && $("#<%= ddlCustomerList.ClientID %>") != undefined) {
                distID = $("#<%= ddlCustomerList.ClientID %>").val();
            }else if('<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role %>'==='CADMN'){
                 distID = <%= customerID %>;
            }

            if(distID!=-1){
                $('#divAddEditUserDialog').show();
                var myWindowAdv = $("#divAddEditUserDialog");

                myWindowAdv.kendoWindow({
                    width: "60%",
                    height: '90%',
                    iframe: true,
                    title: "User Details",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                $('#iframeAdd').attr('src', 'about:blank');
                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeAdd').attr('src', "/RLCS/RLCS_UserDetails.aspx?UserID=0&CustomerID=" + distID);
            }

            return false;
        }

        function onClose() {
            debugger;
            document.getElementById('<%= btnRefresh.ClientID %>').click();
        }

        function OpenEditUserPopup(obj) {
            debugger;
            var userCustID = $(obj).attr('data-clId');

            if (userCustID != null && userCustID != undefined) {
                var splitArr=userCustID.split("-");
                if(splitArr.length>0){
                    $('#divAddEditUserDialog').show();
                    var myWindowAdv = $("#divAddEditUserDialog");

                    myWindowAdv.kendoWindow({
                        width: "60%",
                        height: '90%',
                        iframe: true,
                        title: "User Details",
                        visible: false,
                        actions: [
                            //"Pin",
                            "Close"
                        ],
                        close: onClose
                    });

                    $('#iframeAdd').attr('src', 'about:blank');
                    myWindowAdv.data("kendoWindow").center().open();
                    $('#iframeAdd').attr('src', "/RLCS/RLCS_UserDetails.aspx?UserID=" + splitArr[0]+"&CustomerID="+splitArr[1]);
                }
            }

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/RLCS/ClientSetup/RLCS_CustomerCorporateList.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity-Branch"></asp:LinkButton>
                    <%--PostBackUrl="~/RLCS/Setup/RLCS_EntityBranchList.aspx"--%>
                </div>
                 <div class="step current">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserMaster.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/RLCS/ClientSetup/RLCS_EmployeeMaster.aspx" runat="server" Text="Employee"></asp:LinkButton>
                </div>                              
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityAssignment.aspx" runat="server" Text="Entity-Branch Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Assign.aspx" runat="server" Text="Compliance Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAct" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Activate.aspx" runat="server" Text="Compliance Activation"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 100%; margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td style="width: 50%;">
                        <div id="divCustomerfilter" runat="server">
                            <div style="width: 25%; float: left;">
                                <label style="display: block; float: right; font-size: 13px; color: #333; font-weight: bold; margin-top: 5px;">
                                    Customer :</label>
                            </div>
                            <div style="width: 75%; float: left;">
                                <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" />
                            </div>
                        </div>
                    </td>
                    <td style="width: 30%;">
                        <div style="width: 30%; float: left;">
                            <label style="display: block; float: right; font-size: 13px; color: #333; font-weight: bold; margin-top: 5px;">Filter :</label>
                        </div>
                        <div style="width: 70%; float: left;">
                            <asp:TextBox runat="server" ID="tbxFilter" Width="100%" MaxLength="50" AutoPostBack="true"
                                OnTextChanged="tbxFilter_TextChanged" />
                        </div>
                    </td>
                    <td align="right" style="width: 5%;">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddUser" OnClientClick = "return OpenAddUserPopup()"  CssClass="button" /> <%--OnClick="btnAddUser_Click"--%>
                    </td>
                    <td align="right" style="width: 15%;">
                          <asp:LinkButton Text="User-Customer Mapping" runat="server" ID="btnUserCustomerMapping" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserCustomerMapping.aspx" CssClass="k-hidden" />
                    </td>
                     <td align="right" style="width: 10%; display: none">
                        <asp:LinkButton Text="Refresh" runat="server" ID="btnRefresh" CssClass="button" OnClick="btnRefresh_Click" />
                        <%--OnClick="btnAddCustomer_Click"--%>                        
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel2" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdUser" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCreated="grdUser_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdUser_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdUser_RowCommand" OnPageIndexChanging="grdUser_PageIndexChanging"
                    OnRowDataBound="grdUser_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="UserID" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="FirstName" HeaderText="First Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="FirstName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="ContactNumber" HeaderText="Contact No." ItemStyle-HorizontalAlign="Center" SortExpression="ContactNumber" />
                        <asp:BoundField DataField="HR_Role" HeaderText="Role" SortExpression="HR_Role" />
                        <asp:TemplateField ItemStyle-Width="60px" HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ID") %>' ID="lbtnChangeStatus"
                                    ToolTip="Click to toggle the status..." OnClientClick="return confirm('Are you certain you want to change the status of this User?');"><%# (Convert.ToBoolean(Eval("IsActive"))) ? "Active" : "Disabled" %></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="150px" HeaderText="Assignments" ItemStyle-HorizontalAlign="Center" Visible="false">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnModifyAssignment" runat="server" CommandName="MODIFY_ASSIGNMENT" CommandArgument='<%# Eval("ID") %>'
                                    ToolTip="Modify assignments for this user...">Modify Assignments</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="130px" HeaderText="Assignments" ItemStyle-HorizontalAlign="Center" Visible="false">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEventAssignment" runat="server" CommandName="MODIFY_EVENT_ASSIGNMENT" CommandArgument='<%# Eval("ID") %>'
                                    ToolTip="Modify event assignments for this user...">Modify Events</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="110px" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" data-clId='<%# Eval("ID")+"-"+Eval("CustomerID")%>' OnClientClick="OpenEditUserPopup(this)">
                                    <img src="../../Images/edit_icon.png" alt="Edit" title="Edit User" /></asp:LinkButton> <%--CommandName="EDIT_USER" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'--%>
                                <asp:LinkButton runat="server" CommandName="DELETE_USER" CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete"
                                    OnClientClick="return confirm('Are you certain you want to delete this User?');"><img src="../../Images/delete_icon.png" alt="Delete" title="Delete User" /></asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="RESET_PASSWORD" ID="lbtnReset" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to reset password for this user?');"><img src="../../Images/reset_password.png" alt="Reset" title="Reset Password" /></asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="UNLOCK_USER" CommandArgument='<%# Eval("ID") %>'
                                    Visible='<%# IsLocked((string)Eval("Email")) %>'><img src="../../Images/permissions_icon.png" alt="Unlock" title="Unblock User" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <vit:UserDetailsControl runat="server" ID="udcInputForm" />

    <div id="divAddEditUserDialog">
        <iframe id="iframeAdd" style="width: 100%; height: 100%; border: none"></iframe>
    </div>
  
    <script type="text/javascript">
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }       
    </script>
</asp:Content>
