﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_CustomerBranchDetails_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_CustomerBranchDetails_New" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    <style>
        .form-control {
            margin-bottom: 8px;
            height: 31px;
            font-family: sans-serif;
        }

        .m-10 {
            /*margin-left: 10px;*/
            padding-left: 24px;
        }

        #loaderImage {
            position: fixed;
            margin-left: 40%;
            margin-top: 25%;
            width: 50px;
            height: 50px;
            background-image: url("../NewCSS/Default/loading-image.gif");
        }

        .anchorcss {
            margin-left: 15px;
            width: 32.0%;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("body").tooltip({ selector: '[data-toggle=tooltip]' });
            initializeDatePicker();
            ddltype();
            $("#txtBonusP").change(function (e) {
                var InitialValue = $(this).attr("initialvalue");
                var checkForPercentage = /^\d{1,2}\.\d{1,2}$|^\d{1,3}$/;
                if (!checkForPercentage.test($(this).val())) {
                    alert("Enter Bonus Percentage only in Decimal or Integer");
                    $(this).val(InitialValue);
                }
            });

            $(window.parent.document).find("#BodyContent_updateProgress").hide();

            $('#btnSave').click(function () {
                debugger;
               // $(window.parent.document).find("#BodyContent_updateProgress").show();
                //alert("saving record");
            });

        });

        function chkDecimal() {
            var val = $("#txtBonusP").val();
            var checkForPercentage = /^\d{1,2}\.\d{1,2}$|^\d{1,3}$/;
            if (!checkForPercentage.test(val)) {
                alert("Enter Bonus Percentage only in Decimal or Integer");
                $("#txtBonusP").val("");
            }
        }

        function myShow()
        {
            $(window.parent.document).find("#BodyContent_updateProgress").show();
        }

       

        function initializeDatePicker() {
            $('#<%= txtServicedate.ClientID %>').datepicker({
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            });

            $('#<%= txtcommencementdt.ClientID %>').datepicker({
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0",
            });
        }

    </script>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function Hideshow()
        {
            $('#btnSave').addClass('disabled');
            window.parent.$("#divRLCSCustomerBranchesDialog").val($('#hdnsave').val());
            setTimeout(function () {
                if (window.parent.$("#divRLCSCustomerBranchesDialog") != null && window.parent.$("#divRLCSCustomerBranchesDialog") != undefined)
                {
                    $(window.parent.document).find("#BodyContent_updateProgress").hide();
                    window.parent.$("#divRLCSCustomerBranchesDialog").data("kendoWindow").close();
                    //window.parent.$("#divRLCSCustomerBranchesDialog").val($('#hdnsave').val());
                }
                   
            }, 3000);
           
            setTimeout(function () {
               // $("#loader").hide();
               // $('#btnSave').removeClass('disabled');
               // $(window.parent.document).find("#BodyContent_updateProgress").hide();
            }, 2700);
           
        }

        function DisableBtn(e) {
            var ValidationError = $('#vsEntityBranchPage ul').text();

            if (ValidationError.length == 0) {
                $("#loader").show();
                $('#btnSave').addClass('disabled');
            }
        }

        function hideLoader() {
            debugger;
            $("#loader").hide();
            $('#btnSave').removeClass('disabled');
        }

        function showLoader() {
                        
            $("#loader").show();
            $('#btnSave').addClass('disabled');
        }
        function ddltype() {
            //debugger;
            var ddltype = $("#ddlPFType").val();
            //alert("VAL:" + ddltype);
            if (ddltype == "C") {
                $("#SpanPF").show();
                $("#txtPfCode").removeAttr('disabled');
            }
            else {
                $("#SpanPF").hide();
                $("#txtPfCode").val("");
                $("#txtPfCode").attr("disabled", "disabled");
                
            }
        }
        
    </script>
</head>
<body style="background: white;">
    <form id="form1" runat="server">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="loader" style="display: none;">
            <div id="loaderImage">
            </div>
        </div>
        <asp:UpdatePanel ID="upCustomerBranches" runat="server" UpdateMode="Conditional" OnLoad="upCustomerBranches_Load">
            <ContentTemplate>
                <asp:HiddenField  id="hdnsave" runat="server" Value="" />
                <div class="row form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <asp:ValidationSummary runat="server" ID="vsEntityBranchPage" CssClass="alert alert-block alert-danger alert-dismissable"
                            ValidationGroup="CustomerBranchValidationGroup" />
                        <asp:CustomValidator ID="cvEntityBranchPage" runat="server" EnableClientScript="False"
                            ValidationGroup="CustomerBranchValidationGroup" Display="None" CssClass="alert alert-block alert-danger fade in" />
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">
                            Customer</label>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 control-label">
                        <asp:Literal ID="litCustomer" runat="server" />
                        <span id="customerName" runat="server" class="control-label"></span>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4"></div>
                </div>

                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>  Name</label>
                        <asp:TextBox runat="server" ID="tbxName" MaxLength="50" autoComplete="nope" CssClass="form-control"
                            OnTextChanged="tbxName_TextChanged" AutoPostBack="true" />
                        <asp:RequiredFieldValidator ErrorMessage="Required Entity/Client Name" ControlToValidate="tbxName"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="[a-zA-Z0-9\s()+\/@&#.,-_]*$">
                        </asp:RegularExpressionValidator>
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4" runat="server" id="divClientID">
                        <div class="col-xs-10 col-sm-10 col-md-10 pl0">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label class="control-label">ClientID</label>
                            <asp:TextBox runat="server" ID="txtClientID" MaxLength="10" CssClass="form-control"  OnTextChanged="txtClientID_TextChanged" AutoPostBack="true"/>
                            <asp:RequiredFieldValidator ErrorMessage="Required ClientID" ControlToValidate="txtClientID"
                                runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 colpadding0">
                            <label class="control-label">&nbsp;</label>
                            <asp:Button runat="server" ID="btnCheck" Text="Check" OnClick="btnCheck_Click" CssClass="btn btn-primary" data-toggle="tooltip" ToolTip="Check ClientID is already exists or not" />
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 colpadding0">
                            <asp:Label runat="server" ID="lblCheckClient" ForeColor="red" Text="Please click on check button to verify ClientID"></asp:Label>
                        </div>
                    </div>
                     <div class="col-xs-1 col-sm-1 col-md-1">
                         <label class="control-label">
                         <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                         PF Type</label>
                         <asp:DropDownListChosen runat="server" ID="ddlPFType" Width="100%" CssClass="form-control" onchange="return ddltype();">
                             <asp:ListItem Value="C" Text="C" ></asp:ListItem> 
                             <asp:ListItem Value="B" Text="B"></asp:ListItem>
                         </asp:DropDownListChosen>
                     </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label class="control-label">
                            <span id="SpanPF" style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            PF Code</label>
                        <asp:TextBox runat="server" ID="txtPfCode" CssClass="form-control" autoComplete="off" />
                        <%--<asp:RequiredFieldValidator ErrorMessage="Required PF Code" ControlToValidate="txtPfCode"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />--%>
                        <%--<asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            ErrorMessage="Please enter a valid pf code." ControlToValidate="txtPfCode" ValidationExpression="[a-zA-Z0-9]*$"></asp:RegularExpressionValidator>--%>
                         
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            ErrorMessage="Please enter a valid pf code. Only alphanumeric characters,/,\ and - are allowed" ControlToValidate="txtPfCode" ValidationExpression="[a-zA-Z0-9-\\/\\\\]*$"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div style="margin-bottom: 3px; display: none;" class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label class="control-label">
                                Sub Entity Type</label>
                            <asp:DropDownList runat="server" ID="ddlType" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
                            <asp:CompareValidator ErrorMessage="Please select Sub Entity Type." ControlToValidate="ddlType"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                Display="None" />
                        </div>
                        <div class="col-md-6">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                Industry
                            </label>
                            <asp:TextBox runat="server" ID="txtIndustry" CssClass="form-control" />
                        </div>
                    </div>
                </div>

                <div style="margin-left: 508px; position: absolute; z-index: 50; width: 45%; overflow-y: auto; background: white; border: 1px solid gray; height: 200px; display: none" id="dvIndustry">
                    <asp:Repeater ID="rptIndustry" runat="server">
                        <HeaderTemplate>
                            <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                <tr>
                                    <td style="width: 100px;">
                                        <asp:CheckBox ID="IndustrySelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                    <td style="width: 282px;">
                                        <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" /></td>
                                    <%--OnClick="btnRefresh_Click" --%>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="width: 20px;">
                                    <asp:CheckBox ID="chkIndustry" runat="server" onclick="UncheckHeader();" /></td>
                                <td style="width: 200px;">
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                        <asp:Label ID="lblIndustryID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                        <asp:Label ID="lblIndustryName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>

                <div class="row form-group" style="display: none">
                    <div class="col-xs-4 col-sm-4 col-md-4" runat="server" id="divLegalEntityType">
                        <label id="lblLegalEntityType" class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Legal Entity Type</label>
                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntityType" CssClass="form-control" Width="100%" AllowSingleDeselect="false" />
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4" runat="server" id="divLegalRelationship">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Legal Relationship</label>
                        <asp:DropDownListChosen runat="server" ID="ddlLegalRelationShip" CssClass="form-control" Width="100%" AllowSingleDeselect="false" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Select Legal Relationship"
                            ControlToValidate="ddlLegalRelationShip" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>
                </div>

                <div runat="server" id="divCompanyType" visible="false" class="row">
                    <div class="col-md-12">
                       
                        <div class="col-md-6">
                            <label class="control-label"><span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span> Person Responsible Applicable</label>
                            <asp:DropDownListChosen runat="server" ID="ddlPersonResponsibleApplicable" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Address</label>
                        <asp:TextBox runat="server" ID="tbxAddressLine1" CssClass="form-control" autocomplete="off"
                            MaxLength="150" />
                        <asp:RequiredFieldValidator ErrorMessage="Required Address" ControlToValidate="tbxAddressLine1"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 d-none">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Address Line 2</label>
                        <asp:TextBox runat="server" ID="tbxAddressLine2" CssClass="form-control" autoComplete="off"
                            MaxLength="150" />
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4" runat="server" id="divState">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            State</label>
                        <asp:DropDownListChosen runat="server" ID="ddlState" Width="100%" AllowSingleDeselect="false"
                            CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" />
                        <asp:CompareValidator ErrorMessage="Please select State." ControlToValidate="ddlState"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            City</label>
                        <asp:DropDownListChosen runat="server" ID="ddlCity" Width="100%" AllowSingleDeselect="false"
                            CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged" />
                        <asp:CompareValidator ErrorMessage="Please select City." ControlToValidate="ddlCity"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>
                </div>

                <div class="row form-group" style="display: none">
                    <div class="col-xs-6 col-sm-6 col-md-6" runat="server" id="divOther">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Others</label>
                        <asp:TextBox runat="server" ID="tbxOther" CssClass="form-control" MaxLength="50" autoComplete="off" />
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label class="control-label">
                            Pin Code</label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="tbxPinCode" MaxLength="6" />
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Contact Person</label>
                        <asp:TextBox runat="server" ID="tbxContactPerson" autoComplete="off" CssClass="form-control"
                            MaxLength="200" />
                        <asp:RequiredFieldValidator ErrorMessage="Required Contact Person"
                            ControlToValidate="tbxContactPerson" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                            ValidationGroup="CustomerBranchValidationGroup" ErrorMessage="Please enter a valid contact person name (i.e. it should only contains alphabets)"
                            ControlToValidate="tbxContactPerson" ValidationExpression="^[a-zA-Z_ .-]*$"></asp:RegularExpressionValidator>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Contact Number</label>
                        <asp:TextBox runat="server" ID="tbxLandline" CssClass="form-control" autoComplete="off" MaxLength="10" />
                         <asp:RequiredFieldValidator ErrorMessage="Please Contact Number"
                            ControlToValidate="tbxLandline" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="None"
                            ValidationGroup="CustomerBranchValidationGroup" ErrorMessage="Please enter a valid contact number"
                            ControlToValidate="tbxLandline" ValidationExpression="(\+\d{1,3}[- ]?)?\d{10}$"></asp:RegularExpressionValidator>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 d-none">
                        <label class="control-label">
                            Mobile No.</label>
                        <asp:TextBox runat="server" ID="tbxMobile" CssClass="form-control" MaxLength="15" autoComplete="off" />

                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Email</label>
                        <asp:TextBox runat="server" ID="tbxEmail" CssClass="form-control" MaxLength="200" autoComplete="off" />
                        <asp:RequiredFieldValidator ErrorMessage="Email can not be empty." ControlToValidate="tbxEmail"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            ErrorMessage="Please enter a valid email." ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Commencement Date</label>
                        <asp:TextBox runat="server" ID="txtcommencementdt" CssClass="form-control" autoComplete="off" />
                        <asp:RequiredFieldValidator ErrorMessage="Required Commencement Date" ControlToValidate="txtcommencementdt"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Status</label>
                        <asp:DropDownListChosen runat="server" ID="ddlCustomerStatus" CssClass="form-control" Width="100%" AllowSingleDeselect="false" />
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 d-none">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Service Start Date</label>
                        <asp:TextBox runat="server" ID="txtServicedate" CssClass="form-control" autoComplete="off" />
                        <asp:RequiredFieldValidator ErrorMessage="Required Service Start Date" ControlToValidate="txtServicedate"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Establishment Type</label>
                        <asp:DropDownListChosen runat="server" ID="ddlestablishmentType" CssClass="form-control" Width="100%">
                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Shop & Establishment" Value="SEA"></asp:ListItem>
                            <asp:ListItem Text="Factory" Value="FACT"></asp:ListItem>
                            <asp:ListItem Text="Both" Value="SF"></asp:ListItem>
                        </asp:DropDownListChosen>
                        <asp:CompareValidator ErrorMessage="Select Establishment Type" ControlToValidate="ddlestablishmentType"
                            runat="server" ValueToCompare="0" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Wage Period (From)</label>
                        <asp:DropDownListChosen runat="server" ID="ddlWagePFrom" AutoPostBack="true" CssClass="form-control" Width="100%" AllowSingleDeselect="false" />
                         <asp:CompareValidator ErrorMessage="Please select Wage Period (From)" ControlToValidate="ddlWagePFrom"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />

                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Wage Period (To)</label>
                        <asp:DropDownListChosen runat="server" ID="ddlWagePTo" CssClass="form-control" Width="100%" AllowSingleDeselect="false" />
                        <asp:CompareValidator ErrorMessage="Please select Wage Period (To)" ControlToValidate="ddlWagePTo"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />

                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Payment Day
                        </label>
                        <asp:DropDownListChosen runat="server" ID="ddlPaymentDay" CssClass="form-control" Width="100%" AllowSingleDeselect="false" />
                        <asp:CompareValidator ErrorMessage="Please select Payment Day" ControlToValidate="ddlPaymentDay"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Bonus Percentage
                        </label>
                        <asp:TextBox runat="server" ID="txtBonusP" CssClass="form-control" autoComplete="off" MaxLength="5" onblur="chkDecimal();" />
                        <asp:RequiredFieldValidator ErrorMessage="Required Bonus Percentage" ControlToValidate="txtBonusP"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <%-- <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>--%>
                            Act Applicability</label>
                        <asp:DropDownListChosen runat="server" ID="ddlActApplicability" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Central" Value="CEN"></asp:ListItem>
                            <asp:ListItem Text="State" Value="STATE"></asp:ListItem>
                        </asp:DropDownListChosen>
                        <%-- <asp:CompareValidator ErrorMessage="Select Act Applicability" ControlToValidate="ddlActApplicability"
                            runat="server" ValueToCompare="0" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />--%>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <%--<span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>--%>
                            Service Tax Exempted</label>
                        <asp:DropDownListChosen runat="server" ID="ddlServiceTax" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                            <asp:ListItem Text="No" Value="N"></asp:ListItem>
                        </asp:DropDownListChosen>
                        <%--  <asp:CompareValidator ErrorMessage="Service Tax Exempted" ControlToValidate="ddlServiceTax"
                            runat="server" ValueToCompare="0" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />--%>
                    </div>

                </div>


                <div class="row form-group">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            Bonus Exempted
                        </label>
                        <asp:DropDownListChosen runat="server" ID="ddlBonusExempted" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                            <asp:ListItem Text="No" Value="N"></asp:ListItem>
                        </asp:DropDownListChosen>
                         <asp:CompareValidator ErrorMessage="Please select is Bonus Exempted." ControlToValidate="ddlBonusExempted"
                            runat="server" ValueToCompare="0" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>
                     <div class="col-xs-4 col-sm-4 col-md-4" runat="server" id="divIndustry">
                            <label class="control-label">
                                <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                Type</label>
                            <asp:DropDownListChosen runat="server" ID="ddlCompanyType" Width="100%" CssClass="form-control" />
                        </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                         <label class="control-label">
                         <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                         EDLI Excemption Type</label>
                         <asp:DropDownListChosen runat="server" ID="ddlEDLIExcemptionType" Width="100%" CssClass="form-control">
                             <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                             <asp:ListItem Value="C" Text="C" ></asp:ListItem>
                             <asp:ListItem Value="B" Text="B"></asp:ListItem>
                         </asp:DropDownListChosen>
                        <asp:CompareValidator ErrorMessage="Please select is EDLI Excemption Type." ControlToValidate="ddlEDLIExcemptionType"
                            runat="server" ValueToCompare="0" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                     </div>
                      <div class="col-xs-4 col-sm-4 col-md-4">
                        <label class="control-label">
                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                            CLRA Applicable
                        </label>
                        <asp:DropDownListChosen runat="server" ID="ddlCLRAApplicable" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                            <asp:ListItem Text="No" Value="N"></asp:ListItem>
                        </asp:DropDownListChosen>
                         <asp:CompareValidator ErrorMessage="Please select CLRA Applicable." ControlToValidate="ddlCLRAApplicable"
                            runat="server" ValueToCompare="0" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>

                </div>
                <div class="row" id="RLCSFields" runat="server">
                    <div class="col-sm-12 col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border bg-info">
                                <h4 class="box-title">Anchor Details</h4>
                            </div>
                            <div class="box-body form-horizontal">
                                <div class="form-group">
                                    <div class="row form-group">

                                        <div class="col-xs-4 col-sm-4 col-md-4 anchorcss">
                                            <label class="control-label">
                                                <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                                BD Anchor
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="txtContactEP1"
                                                        ErrorMessage="Please enter valid EP1 Contact Number" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlBDAnchor" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 anchorcss">
                                            <label class="control-label">
                                                <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                                RAM Anchor</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlRAMAnchor" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 anchorcss">
                                            <label class="control-label">
                                                <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                                Register Anchor
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlRegisterAnchor" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            </asp:DropDownListChosen>

                                        </div>

                                        <div class="col-xs-4 col-sm-4 col-md-4 anchorcss">
                                            <label class="control-label">
                                                <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                                Challan Anchor
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlChallanAnchor" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            </asp:DropDownListChosen>

                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 anchorcss">
                                            <label class="control-label">
                                                <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                                Return Anchor
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlReturnAnchor" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            </asp:DropDownListChosen>

                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 anchorcss">
                                            <label class="control-label">
                                                <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                                Location Anchor
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlLocAnchor" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            </asp:DropDownListChosen>

                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 anchorcss">
                                            <label class="control-label">
                                                <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                                CR Anchor
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlCRAnchor" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 anchorcss">
                                            <label class="control-label">
                                                <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                                Audit Anchor
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlAuditAnchor" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            </asp:DropDownListChosen>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border bg-info">
                                <h4 class="box-title">Spoc Details</h4>
                            </div>
                            <div class="box-body form-horizontal">
                                <div class="form-group">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                            Salutation
                                        </label>
                                        <asp:DropDownListChosen runat="server" ID="ddlSalutationSpoc" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            <asp:ListItem Text="Select Salutation" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Mr" Value="Mr"></asp:ListItem>
                                            <asp:ListItem Text="Ms" Value="Ms"></asp:ListItem>
                                        </asp:DropDownListChosen>

                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                            First Name
                                            <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtFirstNameSpoc"
                                                ErrorMessage="Please enter valid Spoc First Name" ValidationGroup="CustomerBranchValidationGroup" ValidationExpression="^(?=.{1,40}$)[a-zA-Z]+(?:[-'\s][a-zA-Z]+)*$"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtFirstNameSpoc" CssClass="form-control" autoComplete="off" />


                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;"></span>
                                            Last Name
                                            <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator11" runat="server" ControlToValidate="txtLastNameSpoc"
                                                ErrorMessage="Please enter valid Spoc Last Name" ValidationGroup="CustomerBranchValidationGroup" ValidationExpression="^(?=.{1,40}$)[a-zA-Z]+(?:[-'\s][a-zA-Z]+)*$"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtLastNameSpoc" CssClass="form-control" autoComplete="off" />
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                            Contact No
                                            <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtContactSpoc"
                                                ErrorMessage="Please enter valid Spoc Contact Number" ValidationGroup="CustomerBranchValidationGroup" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtContactSpoc" CssClass="form-control" MaxLength="10" autoComplete="off" />


                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                            Email
                                            <asp:RegularExpressionValidator ForeColor="Red" Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                                ErrorMessage="Please enter a valid Spoc email." ControlToValidate="txtEmailSpoc" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtEmailSpoc" CssClass="form-control" autoComplete="off" />
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                            Designation
                                        </label>
                                        <asp:TextBox runat="server" ID="txtDesignationSpoc" CssClass="form-control" autoComplete="off" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border bg-info">
                                <h4 class="box-title">Escalation Point1 Details</h4>
                            </div>
                            <div class="box-body form-horizontal">
                                <div class="form-group">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                            Salutation
                                        </label>
                                        <asp:DropDownListChosen runat="server" ID="ddlSalutationEP1" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            <asp:ListItem Text="Select Salutation" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Mr" Value="Mr"></asp:ListItem>
                                            <asp:ListItem Text="Ms" Value="Ms"></asp:ListItem>
                                        </asp:DropDownListChosen>

                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                            First Name<asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtFirstNameEP1"
                                                ErrorMessage="Please enter valid EP1 First Name" ValidationGroup="CustomerBranchValidationGroup" ValidationExpression="^(?=.{1,40}$)[a-zA-Z]+(?:[-'\s][a-zA-Z]+)*$"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtFirstNameEP1" CssClass="form-control" autoComplete="off" />
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;"></span>
                                            Last Name
                                            <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtLastNameEP1"
                                                ErrorMessage="Please enter valid EP1 Last Name" ValidationGroup="CustomerBranchValidationGroup" ValidationExpression="^(?=.{1,40}$)[a-zA-Z]+(?:[-'\s][a-zA-Z]+)*$"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtLastNameEP1" CssClass="form-control" autoComplete="off" />

                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                            Contact No 
                                            <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtContactEP1"
                                                ErrorMessage="Please enter valid EP1 Contact Number" ValidationGroup="CustomerBranchValidationGroup" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtContactEP1" CssClass="form-control" MaxLength="10" autoComplete="off" />
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                            Email 
                                            <asp:RegularExpressionValidator ForeColor="Red" Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                                ErrorMessage="Please enter a valid EP1 email." ControlToValidate="txtEmailEP1" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtEmailEP1" CssClass="form-control" autoComplete="off" />
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                            Designation
                                        </label>
                                        <asp:TextBox runat="server" ID="txtDesignationEP1" CssClass="form-control" autoComplete="off" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border bg-info">
                                <h4 class="box-title">Escalation Point2 Details</h4>
                            </div>
                            <div class="box-body form-horizontal">
                                <div class="form-group">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;"></span>
                                            Salutation
                                        </label>
                                        <asp:DropDownListChosen runat="server" ID="ddlSalutationEP2" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                            <asp:ListItem Text="Select Salutation" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Mr" Value="Mr"></asp:ListItem>
                                            <asp:ListItem Text="Ms" Value="Ms"></asp:ListItem>
                                        </asp:DropDownListChosen>

                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;"></span>
                                            First Name
                                            <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtFirstNameEP2"
                                                ErrorMessage="Please enter valid EP2 First Name" ValidationGroup="CustomerBranchValidationGroup" ValidationExpression="^(?=.{1,40}$)[a-zA-Z]+(?:[-'\s][a-zA-Z]+)*$"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtFirstNameEP2" CssClass="form-control" autoComplete="off" />


                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;"></span>
                                            Last Name
                                            <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtLastNameEP2"
                                                ErrorMessage="Please enter valid EP2 Last Name" ValidationGroup="CustomerBranchValidationGroup" ValidationExpression="^(?=.{1,40}$)[a-zA-Z]+(?:[-'\s][a-zA-Z]+)*$"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtLastNameEP2" CssClass="form-control" autoComplete="off" />
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;"></span>
                                            Contact No
                                            <asp:RegularExpressionValidator ForeColor="Red" ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtContactEP2"
                                                ErrorMessage="Please enter valid EP2 Contact Number" ValidationGroup="CustomerBranchValidationGroup" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtContactEP2" CssClass="form-control" MaxLength="10" autoComplete="off" />

                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;"></span>
                                            Email
                                            <asp:RegularExpressionValidator ForeColor="Red" Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                                ErrorMessage="Please enter a valid EP2 email." ControlToValidate="txtEmailEP2" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox runat="server" ID="txtEmailEP2" CssClass="form-control" autoComplete="off" />
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label class="control-label">
                                            <span style="color: red; margin-right: 7px; float: left; font-size: 13px;"></span>
                                            Designation
                                        </label>
                                        <asp:TextBox runat="server" ID="txtDesignationEP2" CssClass="form-control" autoComplete="off" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border bg-info">
                                <h4 class="box-title">Setting</h4>
                            </div>
                            <div class="box-body form-horizontal">
                                <div class="col-xs-4 col-sm-4 col-md-4" style="margin-left:-15px">
                                    <label class="control-label">
                                        <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                        PO Applicable</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlPoApplicable" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 " style="margin-left:10px">
                                    <label class="control-label">
                                        <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                        AgreementID
                                    </label>
                                    <asp:TextBox runat="server" ID="txtAgreementID" Width="100%" CssClass="form-control" autoComplete="off" />
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4" >
                                    <label class="control-label">
                                        <span style="color: red; margin-right: 7px; float: left; font-size: 13px;">*</span>
                                        Mandate
                                    </label>
                                    <asp:DropDownListChosen runat="server" ID="ddlContractType" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="One time" Value="O"></asp:ListItem>
                                        <asp:ListItem Text="Recurring" Value="R"></asp:ListItem>
                                        <asp:ListItem Text="Both(OneTime & Recurring)" Value="B"></asp:ListItem>
                                        <asp:ListItem Text="PayRoll" Value="P"></asp:ListItem>
                                    </asp:DropDownListChosen>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div class="row form-group" style="display: none">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">
                            EDLI Excemption
                        </label>
                        <asp:DropDownList runat="server" ID="ddlEdliExcemption" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                            <asp:ListItem Text="N" Value="N"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">
                            Excemption</label>
                        <asp:CheckBox ID="chkexcemption" runat="server" />
                    </div>
                </div>

                <div class="row col-xs-12 col-sm-12 col-md-12 colpadding0 text-center">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                        ValidationGroup="CustomerBranchValidationGroup" />
                    <%--OnClientClick="return DisableBtn(this);"--%>
                </div>

                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
