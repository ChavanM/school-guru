﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSLocation
{
    public class RLCS_LocationDetails
    {
        public bool IsLocationWise = false;
        public RLCS_LocationDetails()
        {
            IsLocationWise = true;
        }

        public class TempPanIndiaClass
        {
            public int AVACOM_BranchID { get; set; }
            public string locationCityName { get; set; }

            public string stateName { get; set; }
        }

        public class ListAssignedBranches
        {
            public int BranchId { get; set; }
        }
        public List<ListAssignedBranches> LstAssignedBranchIds()
        {
            List<ListAssignedBranches> LstBranches = new List<ListAssignedBranches>();
            try
            {
                string ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
                var Lst = RLCSManagement.GetAllAssigenedLocationsBasedOnProfileId(ProfileID);
                if (Lst != null)
                {
                    if (Lst.Count > 0)
                    {
                        foreach (var item in Lst)
                            LstBranches.Add(new ListAssignedBranches { BranchId = item });
                    }
                }

            }
            catch (Exception Ex)
            {
                LstBranches = new List<ListAssignedBranches>();
            }
            return LstBranches;
        }

        public List<SP_RLCS_GetAssignedLocationBranches_Result> GetuserAssignedBranchList(List<SP_RLCS_GetAssignedLocationBranches_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.ID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }

        public List<SP_RLCS_ComplianceInstanceTransactionCount_Result> GetuserAssignedBranchList(List<SP_RLCS_ComplianceInstanceTransactionCount_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.CustomerBranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }

        //TempPanIndiaClass
        public List<SP_RLCS_GetAssignedEntities_Result> GetuserAssignedBranchList(List<SP_RLCS_GetAssignedEntities_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.AVACOM_BranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }

        public List<TempPanIndiaClass> GetuserAssignedBranchList(List<TempPanIndiaClass> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.AVACOM_BranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }
        public List<SP_RLCS_GetMonthlyComplianceSummary_Result> GetuserAssignedBranchList(List<SP_RLCS_GetMonthlyComplianceSummary_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.AVACOM_BranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }
        public List<SP_RLCS_GetApplicableActs_Result> GetuserAssignedBranchList(List<SP_RLCS_GetApplicableActs_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.BranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }

        //SP_RLCS_GetManagementCompliancesSummary_Result
        public List<SP_RLCS_GetManagementCompliancesSummary_Result> GetuserAssignedBranchList(List<SP_RLCS_GetManagementCompliancesSummary_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.CustomerBranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }
        //sp_InternalComplianceInstanceAssignmentViewDetails_Result
        public List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> GetuserAssignedBranchList(List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.CustomerBranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }

        public List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result> GetuserAssignedBranchList(List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.CustomerBranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }


        public List<SP_RLCS_GetOverDueHighCompliance_Result> GetuserAssignedBranchList(List<SP_RLCS_GetOverDueHighCompliance_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.CustomerBranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }


        public List<SP_GetCannedReportCompliancesSummary_Result> GetuserAssignedBranchList(List<SP_GetCannedReportCompliancesSummary_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.CustomerBranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }

        //SP_GetOnBoardingHealthCheckSummary_Result
        public List<SP_GetOnBoardingHealthCheckSummary_Result> GetuserAssignedBranchList(List<SP_GetOnBoardingHealthCheckSummary_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.AVACOM_BranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }
        public List<NameValueHierarchy> GetuserAssignedBranchList(List<NameValueHierarchy> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.ID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }

        //SP_LitigationNoticeDocumentfor_Tag_Result
        public List<SP_LitigationNoticeDocumentfor_Tag_Result> GetuserAssignedBranchList(List<SP_LitigationNoticeDocumentfor_Tag_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.CustomerBranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }

        //DataTable
        public List<SP_RLCS_ComplianceInstanceTransactionViewCustomerWiseManagement_Result> GetuserAssignedBranchList(List<SP_RLCS_ComplianceInstanceTransactionViewCustomerWiseManagement_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.CustomerBranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }

        public List<SP_RLCS_NoticeAssignedInstance_Result> GetuserAssignedBranchList(List<SP_RLCS_NoticeAssignedInstance_Result> Lst)
        {
            if (IsLocationWise)
            {
                List<ListAssignedBranches> ListBranches = LstAssignedBranchIds();
                if (ListBranches != null)
                {
                    if (ListBranches.Count > 0)
                    {
                        Lst = (from L in Lst join LB in ListBranches on L.CustomerBranchID equals LB.BranchId select L).ToList();
                    }
                }
            }
            return Lst;
        }
    }
}