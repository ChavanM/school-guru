﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_Designation_Master : System.Web.UI.Page
    {
        protected static int CustId;
        protected int UserId;
        protected static string Path;
        protected void Page_Load(object sender, EventArgs e)
        {
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];


        }

        [WebMethod]
        public static string changeStatus(string IOJson)
        {
            int recordID = Convert.ToInt32(IOJson);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                com.VirtuosoITech.ComplianceManagement.Business.Data.RLCS_Designation_Master
                record = entities.RLCS_Designation_Master.SingleOrDefault(a => a.ID == recordID);
                record.Status = "I";
                record.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
            return "0";
        }
    }
}