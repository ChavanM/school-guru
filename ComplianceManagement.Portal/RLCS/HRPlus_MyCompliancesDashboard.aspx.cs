﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.IO;
using System.Globalization;
using OfficeOpenXml.Style;
using System.Data;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_MyCompliancesDashboard : System.Web.UI.Page
    {
        public static List<long> SelectedBranchList = new List<long>();
        protected bool initialLoad;

        protected static List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> mstRecords;

        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                try
                {
                    initialLoad = true;

                    string filterType = string.Empty;
                    string comType = string.Empty;
                    string strCustID = string.Empty;
                    string strUserID = string.Empty;
                    string strStartPeriod = string.Empty;
                    string strEndPeriod = string.Empty;
                    
                    if (!string.IsNullOrEmpty(Request.QueryString["filterType"]))
                    {
                        filterType = Request.QueryString["filterType"];
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["comType"]))
                    {
                        comType = Request.QueryString["comType"];
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                    {
                        strCustID = Request.QueryString["customerid"];
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["userID"]))
                    {
                        strUserID = Request.QueryString["userID"];
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["startPeriod"]))
                    {
                        strStartPeriod = Request.QueryString["startPeriod"];
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["endPeriod"]))
                    {
                        strEndPeriod = Request.QueryString["endPeriod"];
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["risk"]))
                    {
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["period"]))
                    {
                        string strPeriods = Request.QueryString["period"];                        
                    }
                    BindGrid();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
       
        private void BindGrid()
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                string roleCode = string.Empty;
                roleCode = AuthenticationHelper.Role;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_AssignedCustomers(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role);

                    var masterRecords = (entities.SP_RLCS_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, customerID, distributorID, serviceProviderID, roleCode)
                                       .Where(entry => entry.IsActive == true
                                           && entry.IsUpcomingNotDeleted == true
                                           && lstMyAssignedCustomers.Select(row => row.CustomerID).Contains(entry.CustomerID))).Take(2).ToList();

                    if (masterRecords.Count > 0)
                    {
                        mstRecords = masterRecords;
                    }

                    Session["TotalRows"] = masterRecords.Count;
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        

        [System.Web.Services.WebMethod]
        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> GetHRMyCompliances()
        {
            List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> record = null;
            record = mstRecords;
            return record;
        }
    }
}