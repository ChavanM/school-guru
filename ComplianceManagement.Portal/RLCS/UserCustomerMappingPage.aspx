﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserCustomerMappingPage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.UserCustomerMappingPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <link href="~/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
        
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <style>
        .m-10 {
            /*margin-left: 10px;*/
            padding-left: 24px;
        }
    </style>

    <script>
        $(document).ready(function () {
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
            // window.parent.forchild($('#grdSummaryDetails').find('tr').length);
                        
            if ($(window.parent.document).find("#divUserCustomerMapping").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#divUserCustomerMapping").children().first().hide();
        });
    </script>
</head>
<body style="background: white;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="modal-body" style="width: 100%;">
            <%--<asp:UpdatePanel ID="upMapUser" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>--%>

            <div class="col-xs-12 col-sm-12 col-md-12 colpadding0" style="margin-bottom: 8px;">
                <asp:ValidationSummary ID="vsUserCustomerMapping" runat="server" CssClass="alert alert-danger" ValidationGroup="UCMPopupValidationGroup" />
                <asp:CustomValidator ID="cvUCMPopup" runat="server" EnableClientScript="False"
                    ValidationGroup="UCMPopupValidationGroup" Display="None" />
                <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 colpadding0">
                <div class="col-xs-3 col-sm-3 col-md-3 float-left">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label for="ddlCustomerPopup" class="control-label">Customer</label>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 float-left" style="margin-bottom: 8px;">
                    <asp:DropDownListChosen ID="ddlCustomerPopup" AllowSingleDeselect="false" runat="server" CssClass="form-control"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerPopup_SelectedIndexChanged" Width="100%">
                    </asp:DropDownListChosen>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-xs-12 col-sm-12 col-md-12 colpadding0">
                <div class="col-xs-3 col-sm-3 col-md-3 float-left">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label for="ddlUserPopup" class="control-label">User</label>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 float-left" style="margin-bottom: 8px;">
                    <asp:DropDownListChosen ID="ddlUserPopup" AllowSingleDeselect="false" runat="server" CssClass="form-control" Width="100%">
                    </asp:DropDownListChosen>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-xs-12 col-sm-12 col-md-12 colpadding0">
                <div class="col-xs-3 col-sm-3 col-md-3 float-left">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label for="ddlMgrPopup" class="control-label">Manager</label>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 float-left" style="margin-bottom: 8px;">
                    <asp:DropDownListChosen ID="ddlMgrPopup" AllowSingleDeselect="false" runat="server" CssClass="form-control" Width="100%">
                    </asp:DropDownListChosen>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-xs-12 col-sm-12 col-md-12 colpadding0" style="margin-bottom: 8px; text-align: center;">
                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" class="btn btn-primary"
                    ValidationGroup="UCMPopupValidationGroup" />
            </div>

            <div style="margin-left: 10px; margin-top: 10px;">
                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
            </div>
            <%-- </ContentTemplate>

                    </asp:UpdatePanel>--%>
        </div>
    </form>
</body>
</html>
