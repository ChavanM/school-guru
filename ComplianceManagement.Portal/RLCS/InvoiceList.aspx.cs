﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class InvoiceList : System.Web.UI.Page
    {
        protected static string CId;
        protected static string UId;
        protected void Page_Load(object sender, EventArgs e)
        {
            CId = Convert.ToString(AuthenticationHelper.CustomerID);
            UId = Convert.ToString(AuthenticationHelper.UserID);
        }
    }
}