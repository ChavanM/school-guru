﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.Reflection;
using System.Text;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_DesignationBulkUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            spanErrors.Visible = false;
        }

        protected void btnUploadExcel_Click(object sender, EventArgs e)
        {
            hdFileName.Value = "";
            if (DesignationUpload.HasFile && Path.GetExtension(DesignationUpload.FileName).ToLower() == ".xlsx")
            {
                try
                {
                    if (!Directory.Exists(Server.MapPath("~/Uploaded/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Uploaded/"));
                    }

                    string filename = Path.GetFileName(DesignationUpload.FileName);
                    DesignationUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {

                            bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "DesignationBulkUplod");
                            if (matchSuccess)
                            {
                                if (ISAdd1.Checked)
                                {
                                    ProcessChecklistData(xlWorkbook);
                                }
                                else
                                {
                                    ProcessChecklistUpdateData(xlWorkbook);
                                }
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'DesignationBulkUplod'.";
                            }

                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
            else
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Please upload excel file (.xlsx)";
            }
        }

        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["DesignationBulkUplod"];
                using (ComplianceDBEntities db = new ComplianceDBEntities())
                {
                    if (xlWorksheet != null)
                    {
                        List<ErrorMessage> lstErrorMessage = new List<ErrorMessage>();
                        int xlrow2 = xlWorksheet.Dimension.End.Row;
                        string ErrorFileName = "ErrorFile_DesignationUpload_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";

                        hdFileName.Value = ErrorFileName;

                        #region Validations

                        ErrorMessage objError = null;
                        string clientid = hdClientId.Value;
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            string designation = xlWorksheet.Cells[i, 1].Text.Trim();
                            string Status = xlWorksheet.Cells[i, 2].Text.Trim();
                            string status = "";
                            if (Status == "A" || Status == "Active")
                            {
                                status = "A";
                            }
                            else if(Status == "I" || Status == "In-Active")
                            {
                                status = "I";
                            }
                           
                            if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                            {
                                objError = new ErrorMessage();
                                objError.ErrorDescription = "Required Long_designation  at Row ";
                                objError.RowNum = i.ToString();
                                lstErrorMessage.Add(objError);
                            }
                            else
                            {
                               
                                var existingDesignation = db.RLCS_Designation_Master.Where(t => t.Long_designation == designation).FirstOrDefault();
                                if (existingDesignation != null)
                                {
                                  
                                    if (existingDesignation.Long_designation == designation)
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Long_designation " + designation + " already exists   at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }
                            }

                            if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                            {
                                objError = new ErrorMessage();
                                objError.ErrorDescription = "Required Status  at Row ";
                                objError.RowNum = i.ToString();
                                lstErrorMessage.Add(objError);
                            }
                            else
                            {
                               if (Status == "A" || Status == "Active")
                                {
                                    status = "A";
                                }
                                else if (Status == "I" || Status == "In-Active")
                                {
                                    status = "I";
                                }
                                else
                                {
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Status must be A or Active and  I or In-Active  at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }
                            }
                        }

                        if (lstErrorMessage.Count == 0)
                        {
                            for (int i = 2; i <= xlrow2; i++)
                            {
                                string designation = xlWorksheet.Cells[i, 1].Text.Trim();
                                string Status = xlWorksheet.Cells[i, 2].Text.Trim();
                                string status = "";
                                if(Status=="A" || Status== "Active")
                                {
                                    status = "A";
                                }
                                else if (Status == "I" || Status == "In-Active")
                                {
                                    status = "I";
                                }
                                com.VirtuosoITech.ComplianceManagement.Business.Data.RLCS_Designation_Master objDesignation = new com.VirtuosoITech.ComplianceManagement.Business.Data.RLCS_Designation_Master()
                                {
                                    Long_designation = designation,
                                    Status = status,
                                    CreatedOn = DateTime.Now
                                };
                                db.RLCS_Designation_Master.Add(objDesignation);
                                db.SaveChanges();
                            }

                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = " Details Uploaded Successfully";
                            vsUploadUtility.CssClass = "alert alert-success";
                        }
                        else
                            GenerateDesignationUploadErrorCSV(lstErrorMessage, ErrorFileName);

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void ProcessChecklistUpdateData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["DesignationBulkUplod"];
                using (ComplianceDBEntities db = new ComplianceDBEntities())
                {
                    if (xlWorksheet != null)
                    {
                        List<ErrorMessage> lstErrorMessage = new List<ErrorMessage>();
                        int xlrow2 = xlWorksheet.Dimension.End.Row;
                        string ErrorFileName = "ErrorFile_DesignationUpload_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";

                        hdFileName.Value = ErrorFileName;

                        #region Validations

                        ErrorMessage objError = null;
                        string clientid = hdClientId.Value;
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                            {
                                objError = new ErrorMessage();
                                objError.ErrorDescription = "Required ID  at Row ";
                                objError.RowNum = i.ToString();
                                lstErrorMessage.Add(objError);
                            }
                            int id =Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                            string designation = xlWorksheet.Cells[i, 2].Text.Trim();
                            string Status = xlWorksheet.Cells[i, 3].Text.Trim();

                            string status = "";
                            if (Status == "A" || Status == "Active")
                            {
                                status = "A";
                            }
                            else if (Status == "I" || Status == "In-Active")
                            {
                                status = "I";
                            }
                            
                            if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                            {
                                objError = new ErrorMessage();
                                objError.ErrorDescription = "Required Long_designation  at Row ";
                                objError.RowNum = i.ToString();
                                lstErrorMessage.Add(objError);
                            }
                            else
                            {
                                var existingDesignation = db.RLCS_Designation_Master.Where(t => t.Long_designation == designation && t.Status== status).FirstOrDefault();
                                int DuplicateDesignationCount = db.RLCS_Designation_Master.Where(t => t.Long_designation == designation).Count();
                                if (existingDesignation != null)
                                {
                                    
                                    if (existingDesignation.Long_designation == designation && existingDesignation.Status== status && existingDesignation.ID != id && DuplicateDesignationCount != 0)
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Long_designation " + designation + " already exists   at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                   
                                }
                            }

                            if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                            {
                                objError = new ErrorMessage();
                                objError.ErrorDescription = "Required Status  at Row ";
                                objError.RowNum = i.ToString();
                                lstErrorMessage.Add(objError);
                            }
                            else
                            {
                                if (Status == "A" || Status == "Active")
                                {
                                    status = "A";
                                }
                                else if (Status == "I" || Status == "In-Active")
                                {
                                    status = "I";
                                }
                                else
                                {
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Status must be A or Active and  I or In-Active  at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }
                            }

                        }

                        if (lstErrorMessage.Count == 0)
                        {
                            for (int i = 2; i <= xlrow2; i++)
                            {
                                int id = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                                string designation = xlWorksheet.Cells[i, 2].Text.Trim();
                                string Status = xlWorksheet.Cells[i, 3].Text.Trim();
                                string status = "";
                                if (Status == "A" || Status == "Active")
                                {
                                    status = "A";
                                }
                                else if (Status == "I" || Status == "In-Active")
                                {
                                    status = "I";
                                }
                                com.VirtuosoITech.ComplianceManagement.Business.Data.RLCS_Designation_Master updateDetails = (from row in db.RLCS_Designation_Master where row.ID == id select row).FirstOrDefault();
                                if (updateDetails != null)
                                {
                                    updateDetails.Long_designation = designation;
                                    updateDetails.Status = status;
                                    db.SaveChanges();
                                }
                            }

                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = " Details Uploaded Successfully";
                            vsUploadUtility.CssClass = "alert alert-success";
                        }
                        else
                            GenerateDesignationUploadErrorCSV(lstErrorMessage, ErrorFileName);

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void GenerateDesignationUploadErrorCSV(List<ErrorMessage> detailView, string FileName)
        {
            try
            {
                spanErrors.Visible = true;

                if (!Directory.Exists(Server.MapPath("~/TempDesignnationError/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/TempDesignnationError/"));
                }

                string FilePath = Server.MapPath("~/TempDesignnationError/") + FileName;

                using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                {
                    string delimiter = "";
                    StringBuilder sb = new StringBuilder();
                    List<string> CsvRow = new List<string>();

                    if (detailView.Count > 0)
                    {
                        string content = "Error Description,Row No";
                        CsvRow.Add(content + Environment.NewLine);

                        foreach (var data in detailView)
                        {
                            if (data != null)
                            {
                                content = data.ErrorDescription + "," + data.RowNum;
                                CsvRow.Add(content + Environment.NewLine);
                            }
                        }
                    }

                    sb.AppendLine(string.Join(delimiter, CsvRow));
                    swOutputFile.WriteLine(sb.ToString());
                }
            }
            catch (Exception e)
            {

            }

        }
        protected void btnDownloadError_Click(object sender, EventArgs e)
        {
            string filePath = Server.MapPath("~/TempDesignnationError/" + hdFileName.Value);
            DownloadFile(filePath);
        }
        private void DownloadFile(string Path)
        {
            try
            {
                FileInfo file = new FileInfo(Path);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "text/plain";
                    Response.Flush();
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public class ErrorMessage
        {
            public string ErrorDescription { get; set; }
            public string RowNum { get; set; }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(Server.MapPath("~/RLCSDocuments/")))
            {
                Directory.CreateDirectory(Server.MapPath("~/RLCSDocuments/"));
            }
            string filePath = Server.MapPath("~/RLCSDocuments/DesignationSample.xlsx");
            DownloadFile(filePath);
        }

        protected void ISUpdate1_CheckedChanged(object sender, EventArgs e)
        {
            if(ISUpdate1.Checked)
            {
                ISAdd1.Checked = false;
            }
        }

        protected void ISAdd1_CheckedChanged(object sender, EventArgs e)
        {
            if(ISAdd1.Checked)
            {
                ISUpdate1.Checked = false;
            }
        }
    }
}