﻿<%@ Page Title="COMPLIANCE::InputOutput" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="ComplianceInputOutput.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ComplianceInputOutput" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo_V1.silver.min1.2.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
  <link rel="dns-prefetch" href="https://login.avantis.co.in/AVACOM_API/api/GetAllFileInputOutput" />
<link rel="preconnect" href="https://login.avantis.co.in/AVACOM_API/api/GetAllFileInputOutput" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script>
        $(document).ready(function () {
            fhead('My Workspace/Compliance Input Output');
            //New Code 
            var winPop1 = false;
            $("#btnViewDocument").click(function () {
            debugger;
            if (winPop1 && !winPop1.closed) {  //checks to see if window is open
                winPop1.close();
                winPop1 = false;
            }
            winPop1 = false;
            if (winPop1 == false) {
                var ScheduleIds = $("#lblScheduleIds").text();

                winPop1 = window.open("../RLCS/RLCS_MyComplianceDocumentsPopup.aspx?scid=" + ScheduleIds, "winPop1", 'width=1250px,height=570,status=no,scrollbars=yes,toolbar=0,menubar=no,resizable=yes,top=90,left=10');
            }
            });
            //END

            //$("#btnViewDocument").click(function () {
            //    $("#GenrateDocumentPageModal").modal("show");
            //});


            //$('#GenrateDocumentPageModal').on('shown.bs.modal', function () {
            //    ScheduleIds = $("#lblScheduleIds").text();
            //    $('#iframePopupPage').attr('src', 'about:blank');
            //    $('#iframePopupPage').attr('src', "RLCS_MyComplianceDocumentsPopup.aspx?scid=" + ScheduleIds);
            //});

            //New Start Function To check The window 
            //END Function      
            $(document).click(function (evt) {
                if (evt.target.id != "ContentPlaceHolder1_txtFileInputList") {
                    if (evt.target.type == "checkbox" && $(evt.target).closest("table").attr("id") == "RepeaterTable1") {

                        $("#dvFileInput").show();
                    }
                    else {
                        $("#dvFileInput").hide();
                    }
                }
                if (evt.target.id != "ContentPlaceHolder1_txtPeriodList") {
                    if (evt.target.type == "checkbox" && $(evt.target).closest("table").attr("id") == "RepeaterTable") {
                        $("#dvPeriod").show();
                    }
                    else {
                        $("#dvPeriod").hide();
                    }
                }
            });


            $(document).on("click", "#grid tbody tr .my-edit", function (e) {
                // 

                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                CompliancePopup(item, "I");
            });

            var ComplianceId = '<% =this.ComplianceID_%>';
            var ComplianceType = '<% =this.ComplianceType_%>';
            var MonthId = '<% =this.MonthID_%>';
            var Year = '<% =this.Year_%>';
            if (ComplianceId != '' && ComplianceType != '' && MonthId != '' && Year != '') {
                ShowHideTab();
                // $('#btnProceedOut').show();

            }
            $('#divFilterLocation').addClass("hidden");
            $("#ContentPlaceHolder1_tbxFilterLocation").click(function (e) {
                $('#divFilterLocation').removeClass("hidden");
            });

            $('#divFilterLocationOutput').hide();//hide after arriving from dashboard
        });
        $(document).on("click", "#gridOutput tbody tr .my-edit1", function (e) {
            // 
            var item = $("#gridOutput").data("kendoGrid").dataItem($(this).closest("tr"));
            CompliancePopup(item, "O");
        });
        function hideDivBranch() {
            $("#divFilterLocation").hide();
            $('#divFilterLocation').hide("blind", null, 0, function () { });
            $('#divFilterLocationOutput').hide("blind", null, 0, function () { });
        }
        function ShowHideTab() {

            $('#Output').addClass('active');
            $('#Input').removeClass('active');
            $('#tabInput').removeClass('active');
            $('#tabOutput').addClass('active');
        }
        function Test() {
            var ComplianceId = '<% =this.ComplianceID_%>';
            if (ComplianceId == '') {
                $('#ContentPlaceHolder1_btnhdnClear').click();

                bindKendoGridOut(0, "", "", "", 0, 0,"",0);
                
            }
        }
        function Test1() {
            $('#ContentPlaceHolder1_btnhdnClear1').click();
            bindTreeListGrid1(0, "", "", "", 0, 0,"",0);
        }
        $(document).on("click", function (event) {


            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tvFilterLocation.ClientID %>') {
                $('<%= tvFilterLocation.ClientID %>').unbind('click');

                $('<%= tvFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }

            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvOutputBranch.ClientID %>') > -1) {
                    $("#divFilterLocationOutput").show();
                } else {
                    $("#divFilterLocationOutput").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvOutputBranch.ClientID %>') > -1) {
                $("#divFilterLocationOutput").show();
            } else if (event.target.id != '<%= tbxOutputBranch.ClientID %>') {
                $("#divFilterLocationOutput").hide();
            } else if (event.target.id == '<%= tbxOutputBranch.ClientID %>') {
                $("#divFilterLocationOutput").show();
            } else if (event.target.id == '<%= tvOutputBranch.ClientID %>') {
                $('<%= tvOutputBranch.ClientID %>').unbind('click');

                $('<%= tvOutputBranch.ClientID %>').click(function () {
                    $("#divFilterLocationOutput").toggle("blind", null, 500, function () { });
                });
            }

        });

function ProceedClick() {
    var JsonYear = $('#ContentPlaceHolder1_hdnYear').val();
    var gridMapping = $('#grid').data().kendoGrid;
    var gridData = gridMapping.dataSource.data();
    var strComplianceID = "";
    var arrComplianceID = [];
    var strReturnRegisterChallanID = "";
    var BranchIDList = "";
    var strBranchIDList = "";
    var InputID = "";
    var arrInputID = [];
    var Month = "";
    var arrBranchIDList = [];
    var strBranchIDList1 = "";
    var IOJson = [];
    var IOJsonNew = {};
    var ScheduleOnID = "";
    var JSONID = "";
    var arrScheduleID = [];
    var InsertarrBreakFlag = "false";
    var IOScheduleID = "";
    var arrReturnRegisterChallanID = [];
    
    for (var i = 0; i < gridData.length; i++) {
       
        //if (gridData[i].IsSelected == true) {
        //   // strComplianceID += ComplianceID + ",";
        //    strReturnRegisterChallanID += ReturnRegisterChallanID + ",";
        //    strBranchIDList1 += gridData[i].BranchIDList + ",";
        //    ScheduleOnID = gridData[i].ScheduleOnIDList;
        //    IOJsonNew = {
        //        ScheduleOnID: ScheduleOnID,
        //        ReturnRegisterChallanID: ReturnRegisterChallanID

        //    };
        //    arrComplianceID.push(ComplianceID);
        //    IOJson.push(IOJsonNew)
        //}
        //ADD NEW
        if (gridData[i].IsSelected == true) {
//            strReturnRegisterChallanID += gridData[i].ReturnRegisterChallanID + ",";
            strBranchIDList1 += gridData[i].BranchIDList + ",";

            ReturnRegisterChallanIDNEW = gridData[i].ReturnRegisterChallanID;
            ScheduleOnID = gridData[i].ScheduleOnIDList;
          
            var elepushRRC = 'false';
            if (arrReturnRegisterChallanID.length > 0) {
                for (var j = 0; j < arrReturnRegisterChallanID.length; j++) {
                    if (arrReturnRegisterChallanID[j] == ReturnRegisterChallanIDNEW) {
                        elepushRRC = 'true';
                        break;
                    }
                };
            }
            if (elepushRRC == 'false') {
                if (arrReturnRegisterChallanID.length > 0) {
                    InsertarrBreakFlag = 'true';
                }
                arrReturnRegisterChallanID.push(ReturnRegisterChallanIDNEW);
            }
            if (InsertarrBreakFlag == 'true') {
                var ReturnID = "";
                var klength = arrReturnRegisterChallanID.length - 2;
                for (var k = 0; k < arrReturnRegisterChallanID.length; k++) {
                    ReturnID = arrReturnRegisterChallanID[klength];
                    break;
                }
                var lastChar3 = IOScheduleID.slice(-1);
                if (lastChar3 == ',') {
                    IOScheduleID = IOScheduleID.slice(0, -1);
                }
                IOJsonNew = {
                    ScheduleOnID: IOScheduleID,
                    ReturnRegisterChallanID: ReturnID
                };

                IOJson.push(IOJsonNew);
                IOScheduleID = "";
                arrScheduleID = [];
                InsertarrBreakFlag = 'false';
            }
            var elepush = 'false';
            if (arrScheduleID.length > 0) {
                for (var j = 0; j < arrScheduleID.length; j++) {
                    if (arrScheduleID[j] == ScheduleOnID) {
                        elepush = 'true';
                        break;
                    }
                };
            }
            if (elepush == 'false') {
                arrScheduleID.push(ScheduleOnID);
                IOScheduleID += ScheduleOnID + ",";
            }
        }
        //END
    }

  IOJsonNew = {
        ScheduleOnID: ScheduleOnID,
        ReturnRegisterChallanID: ReturnRegisterChallanIDNEW
    };
    IOJson.push(IOJsonNew);

    var IO = JSON.stringify(IOJson);
    if (strBranchIDList1.length > 0) {
        var lastChar2 = strBranchIDList1.slice(-1);
        if (lastChar2 == ',') {
            strBranchIDList1 = strBranchIDList1.slice(0, -1);
        }

        arrBranchIDList = strBranchIDList1.split(",");
        arrBranchIDList = arrBranchIDList.map(function (el) {
            return el.trim();
        });

        strBranchIDList = arrBranchIDList.filter(function (itm, i, arrBranchIDList) {
            return i == arrBranchIDList.indexOf(itm);
        });
    }
    if (IO.length > 0) {
        //var lastChar = strComplianceID.slice(-1);
        //if (lastChar == ',') {
        //    strComplianceID = strComplianceID.slice(0, -1);
        //}
        //var lastChar1 = strReturnRegisterChallanID.slice(-1);
        //if (lastChar1 == ',') {
        //    strReturnRegisterChallanID = strReturnRegisterChallanID.slice(0, -1);
        //}
        strComplianceID = arrComplianceID.filter(function (itm, i, arrComplianceID) {
            return i == arrComplianceID.indexOf(itm);
        });
        strComplianceID = $.map(strComplianceID, function (val, index) {
            var str =  val;
            return str;
        }).join(", ");

        var CustomerID = $('#ContentPlaceHolder1_hdnCustID').val();
        var ComplianceID = strComplianceID;
        var ReturnRegisterChallanID = strReturnRegisterChallanID;
        var InputID = $('#ContentPlaceHolder1_hdnInputId').val();
        var ComplianceType = $('#ContentPlaceHolder1_hdnComplianceType').val();
        var MonthId = $('#ContentPlaceHolder1_hdnMonth').val();
        var Year = $('#ContentPlaceHolder1_hdnYear').val();
        var BranchID = $.trim(strBranchIDList);

    }
    //alert(IO);
    if (IO.length > 0) {

        InputOutputAjaxCall(ComplianceID, ComplianceType, ReturnRegisterChallanID, MonthId, Year, InputID, BranchID, IO);
    }
    else {
        alert("Please Select Input File");
    }
}
var winPop = false;
function InputOutputAjaxCall(ComplianceID, ComplianceType, ReturnRegisterChallanID, MonthId, Year, InputID, BranchID, IO) {
    var ParameterJson = [];
    var ParameterJsonNew = {};
    ParameterJsonNew = {
        InputID: InputID,
        BranchID: BranchID
    };

    ParameterJson.push(ParameterJsonNew);
    var ParameterID = JSON.stringify(ParameterJson);

    $.ajax({
        type: "POST",
        url: "../RLCS/ComplianceInputOutput.aspx/JSONInsert",
        data: "{ 'IOJson': '" + IO + "','ParameterID':'" + ParameterID + "' }",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            debugger;
            if (data.d != 0) {

                $('#ContentPlaceHolder1_hdnJSONID').val(data.d);
              if (winPop && !winPop.closed) {  //checks to see if window is open
                  winPop.close();
                  winPop = false;
              }
              winPop = false;
              if (winPop == false) {
                  //winPop = window.open("../RLCS/RLCS_MyInputs.aspx?ComplianceID=" + ComplianceID + "&ComplianceType=" + ComplianceType + "&ReturnRegisterChallanID=" + ReturnRegisterChallanID + "&MonthId=" + MonthId + "&Year=" + Year + "&InputID=" + InputID + "&BranchID=" + BranchID + "&JSONID=" + data.d);
                  winPop = window.open("../RLCS/RLCS_MyInputs.aspx?ComplianceType=" + ComplianceType + "&MonthId=" + MonthId + "&Year=" + Year + "&JSONID=" + data.d);
              }
                
            }
        },
        failure: function (response) {
            // alert("Error");
        }
    });
}
function pad2(number) {

    return (number < 10 ? '0' : '') + number

}
function GenerateOutclick() {
    var CustomerID = $('#ContentPlaceHolder1_hdnOutCustID').val();
    var ComplianceID = $('#ContentPlaceHolder1_hdnOutComplianceID').val();

    var arrReturnRegisterChallanID = [];
    var ReturnRegisterChallanID = "";
    var IOJsonScheduleID = [];
    var arrScheduleID = [];
    var IOJson = [];
    var IOScheduleID = "";
    var IOJsonNew = {};
    var ScheduleOnID = "";
    var JSONID = "";
    var ReturnRegisterChallanIDNEW = "";
    var InsertarrBreakFlag = 'false';

    var gridMapping = $('#gridOutput').data().kendoGrid;
    var gridData = gridMapping.dataSource.data();

    for (var i = 0; i < gridData.length; i++) {
        if (gridData[i].IsSelected == true) {
            ReturnRegisterChallanIDNEW = gridData[i].ReturnRegisterChallanID;
            ScheduleOnID = gridData[i].ScheduleOnIDList;
            debugger;
            var elepushRRC = 'false';
            if (arrReturnRegisterChallanID.length > 0) {
                for (var j = 0; j < arrReturnRegisterChallanID.length; j++) {
                    if (arrReturnRegisterChallanID[j] == ReturnRegisterChallanIDNEW) {
                        elepushRRC = 'true';
                        break;
                    }
                };
            }
            if (elepushRRC == 'false') {
                if (arrReturnRegisterChallanID.length > 0) {
                    InsertarrBreakFlag = 'true';
                }
                arrReturnRegisterChallanID.push(ReturnRegisterChallanIDNEW);
            }
            if (InsertarrBreakFlag == 'true') {
                var ReturnID = "";
                var klength = arrReturnRegisterChallanID.length - 2;
                for (var k = 0; k < arrReturnRegisterChallanID.length; k++) {
                    ReturnID = arrReturnRegisterChallanID[klength];
                    break;
                }
                var lastChar3 = IOScheduleID.slice(-1);
                if (lastChar3 == ',') {
                    IOScheduleID = IOScheduleID.slice(0, -1);
                }
                IOJsonNew = {
                    ScheduleOnID: IOScheduleID,
                    ReturnRegisterChallanID: ReturnID
                };

                IOJson.push(IOJsonNew);
                IOScheduleID = "";
                arrScheduleID = [];
                InsertarrBreakFlag = 'false';
            }
            var elepush = 'false';
            if (arrScheduleID.length > 0) {
                for (var j = 0; j < arrScheduleID.length; j++) {
                    if (arrScheduleID[j] == ScheduleOnID) {
                        elepush = 'true';
                        break;
                    }
                };
            }
            if (elepush == 'false') {
                arrScheduleID.push(ScheduleOnID);
                IOScheduleID += ScheduleOnID + ",";
            }
        }
    }
    var lastChar3 = ScheduleOnID.slice(-1);
    if (lastChar3 == ',') {
        ScheduleOnID = ScheduleOnID.slice(0, -1);
    }
    IOJsonNew = {
        ScheduleOnID: ScheduleOnID,
        ReturnRegisterChallanID: ReturnRegisterChallanIDNEW
    };
    IOJson.push(IOJsonNew);
    var IO = JSON.stringify(IOJson);
    //ADD NEW
    var ParameterJson = [];
    var ParameterJsonNew = {};
    ParameterJsonNew = {
    };
    ParameterJson.push(ParameterJsonNew);
    var ParameterID = JSON.stringify(ParameterJson);
    $.ajax({
        type: "POST",
        url: "../RLCS/ComplianceInputOutput.aspx/JSONInsert",
        data: "{ 'IOJson': '" + IO + "','ParameterID':'" + ParameterID + "' }",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            debugger;
            if (data.d != 0) {
                $('#ContentPlaceHolder1_hdnJSONID').val(data.d);
                    $.ajax({
                        type: "POST",
                        url: '<% =avacomAPIUrl%>GetDIYDocumentDetailsForGeneration?customerid=<% =customerID%>&userid=<% =LoginUserID%>&complianceids=' + ComplianceID + '&JSONID=' + data.d,
                        //data: IO,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processData: true,
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                        success: function (data) {
                            if (data.Result.StatusCode = 200) {
                                ScheduleIds = data.Result.Result;
                                $("#lblScheduleIds").text("");
                                $("#lblScheduleIds").text(ScheduleIds);
                                $("#lbldocumentMessageAlert").text(data.Result.Message);
                                $("#AlertModal").modal("show");
                            }
                        },
                        failure: function (data) {
                        }
                    });
                

            }
        },
        failure: function (response) {
           
        }
    });
    //END
  }

    function ProceedOutclick() {

        var arrInputID = [];
        var arrMonth = [];
        var arrReturnRegisterChallanID = [];
        var gridMapping = $('#gridOutput').data().kendoGrid;
        var gridData = gridMapping.dataSource.data();
        var strMonth = "";
        var strInputID = "";
        var strReturnRegisterChallanID = "";
        var strBranchIDList = "";
        var arrBranchIDList = [];
        var strBranchIDList1 = "";
        var IOJsonScheduleID = [];
        var arrScheduleID = [];
        var IOJson = [];
        var IOScheduleID = "";
        var IOJsonNew = {};
        var ScheduleOnID = "";
        var JSONID = "";
        var ReturnRegisterChallanIDNEW = "";

        var InsertarrBreakFlag = 'false';

        for (var i = 0; i < gridData.length; i++) {
            ReturnRegisterChallanIDNEW = gridData[i].ReturnRegisterChallanID;

            if (gridData[i].IsSelected == true) {

                arrInputID.push(gridData[i].InputID);
                arrMonth.push(pad2(gridData[i].Month));
                strBranchIDList1 += gridData[i].BranchIDList + ",";

                ScheduleOnID = gridData[i].ScheduleOnIDList;

                debugger;
                var elepushRRC = 'false';
                if (arrReturnRegisterChallanID.length > 0) {
                    for (var j = 0; j < arrReturnRegisterChallanID.length; j++) {
                        if (arrReturnRegisterChallanID[j] == ReturnRegisterChallanIDNEW) {
                            elepushRRC = 'true';
                            break;
                        }
                    };
                }
                if (elepushRRC == 'false') {
                    if (arrReturnRegisterChallanID.length > 0) {
                        InsertarrBreakFlag = 'true';
                    }
                    arrReturnRegisterChallanID.push(ReturnRegisterChallanIDNEW);

                }
                if (InsertarrBreakFlag == 'true') {
                    var arrReturnID = "";
                    var klength = arrReturnRegisterChallanID.length - 2;
                    for (var k = 0; k < arrReturnRegisterChallanID.length; k++) {
                        arrReturnID = arrReturnRegisterChallanID[klength];
                        break;
                    }
                    var lastChar3 = IOScheduleID.slice(-1);
                    if (lastChar3 == ',') {
                        IOScheduleID = IOScheduleID.slice(0, -1);
                    }
                    IOJsonNew = {
                        ScheduleOnID: IOScheduleID,
                        ReturnRegisterChallanID: arrReturnID
                    };

                    IOJson.push(IOJsonNew);
                    IOScheduleID = "";
                    arrScheduleID = [];
                    InsertarrBreakFlag = 'false';
                }
                var elepush = 'false';
                if (arrScheduleID.length > 0) {
                    for (var j = 0; j < arrScheduleID.length; j++) {
                        if (arrScheduleID[j] == ScheduleOnID) {
                            elepush = 'true';
                            break;
                        }
                    };
                }
                if (elepush == 'false') {
                    arrScheduleID.push(ScheduleOnID);
                    IOScheduleID += ScheduleOnID + ",";
                }
            }
        }

        IOJsonNew = {
            ScheduleOnID: IOScheduleID,
            ReturnRegisterChallanID: ReturnRegisterChallanIDNEW
        };

        IOJson.push(IOJsonNew);
        if (strBranchIDList1.length > 0) {

            var lastChar3 = IOScheduleID.slice(-1);
            if (lastChar3 == ',') {
                IOScheduleID = IOScheduleID.slice(0, -1);
            }

            var lastChar2 = strBranchIDList1.slice(-1);
            if (lastChar2 == ',') {
                strBranchIDList1 = strBranchIDList1.slice(0, -1);
            }
            arrBranchIDList = strBranchIDList1.split(",");
            arrBranchIDList = arrBranchIDList.map(function (el) {
                return el.trim();
            });
            strBranchIDList = arrBranchIDList.filter(function (itm, i, arrBranchIDList) {
                return i == arrBranchIDList.indexOf(itm);
            });
        }
        if (arrMonth.length > 0) {

            strMonth = arrMonth.filter(function (itm, i, arrMonth) {
                return i == arrMonth.indexOf(itm);
            });
            strInputID = arrInputID.filter(function (itm, i, arrInputID) {
                return i == arrInputID.indexOf(itm);
            });
            //New ADD
            var strInput = "";
            $.each(strInputID, function (idx2, val2) {
                strInput += val2 + ",";
            });
            var lastChar1 = strInput.slice(-1);
            if (lastChar1 == ',') {
                strInputID = strInput.slice(0, -1);
            }
            //END
            strReturnRegisterChallanID = arrReturnRegisterChallanID.filter(function (itm, i, arrReturnRegisterChallanID) {
                return i == arrReturnRegisterChallanID.indexOf(itm);
            });

            var CustomerID = $('#ContentPlaceHolder1_hdnOutCustID').val();
            var ComplianceID = $('#ContentPlaceHolder1_hdnOutComplianceID').val();
            var InputID = strInputID;
            var ComplianceType = $('#ContentPlaceHolder1_hdnOutComplianceType').val();
            var MonthId = strMonth;
            var Year = $('#ContentPlaceHolder1_hdnOutYear').val();
            var Year1 = $('#ContentPlaceHolder1_ddlOutYear').val();
            var ReturnRegisterChallanID = ReturnRegisterChallanIDNEW;
            var BranchID = $.trim(strBranchIDList);
            var ScheduleIDstr = IOScheduleID;



            var IO = JSON.stringify(IOJson);

            if (IOJson.length > 0) {
                //alert(IO);
                InputOutputAjaxCall(ComplianceID, ComplianceType, ReturnRegisterChallanID, MonthId, Year, InputID, BranchID, IO);
            }
            else {
                alert("Please Select Input File");
            }

        }


    }
    function checkAllPeriods(cb) {

        var ctrls = document.getElementsByTagName('input');
        for (var i = 0; i < ctrls.length; i++) {
            var cbox = ctrls[i];
            if (cbox.type == "checkbox" && cbox.id.indexOf("chkPeriod") > -1) {
                cbox.checked = cb.checked;
            }
        }
        //============================================For Placeholder property======================================
        var rowCheckBoxHeaderForPlaceholder = $("#RepeaterTable input[id*='PeriodSelectAll']");
        if (rowCheckBoxHeaderForPlaceholder.prop('checked') == true) {
            $("#ContentPlaceHolder1_txtPeriodList").attr("placeholder", "Selected multiple Periods");
        }
        else if (rowCheckBoxHeaderForPlaceholder.prop('checked') == false) {

            $("#ContentPlaceHolder1_txtPeriodList").attr("placeholder", "All Periods");
        }
        //============================================For Placeholder property======================================
    }
    function checkAllFileInput(cb) {
        var ctrls = document.getElementsByTagName('input');
        for (var i = 0; i < ctrls.length; i++) {
            var cbox = ctrls[i];
            if (cbox.type == "checkbox" && cbox.id.indexOf("chkFileInput") > -1) {
                cbox.checked = cb.checked;

            }
        }


        //============================================For Placeholder property======================================
        var rowCheckBoxHeaderForPlaceholder = $("#RepeaterTable1 input[id*='FileInputSelectAll']");
        if (rowCheckBoxHeaderForPlaceholder.prop('checked') == true) {
            $("#ContentPlaceHolder1_txtFileInputList").attr("placeholder", "Selected multiple Files");
            $('#ContentPlaceHolder1_btnFileInput').click();
        }
        else if (rowCheckBoxHeaderForPlaceholder.prop('checked') == false) {

            $("#ContentPlaceHolder1_txtFileInputList").attr("placeholder", "All Files");
            $('#ContentPlaceHolder1_btnFileInput').click();
        }
        //============================================For Placeholder property======================================


    }
    function UncheckFileInputHeader() {


        var rowCheckBox = $("#RepeaterTable1 input[id*='chkFileInput']");
        var rowCheckBoxSelected = $("#RepeaterTable1 input[id*='chkFileInput']:checked");
        var rowCheckBoxHeader = $("#RepeaterTable1 input[id*='FileInputSelectAll']");
        if (rowCheckBox.length == rowCheckBoxSelected.length) {
            rowCheckBoxHeader[0].checked = true;
            $('#ContentPlaceHolder1_btnFileInput').click();
        } else {

            rowCheckBoxHeader[0].checked = false;
            $('#ContentPlaceHolder1_btnFileInput').click();
        }
        $("#RepeaterTable1 input[id*='btnGetSelectedLbl']").click();
    }
    function checkAllcompliance(cb) {
        var ctrls = document.getElementsByTagName('input');
        for (var i = 0; i < ctrls.length; i++) {
            var cbox = ctrls[i];
            if (cbox.type == "checkbox" && cbox.id.indexOf("chkcompliance") > -1) {
                cbox.checked = cb.checked;

            }
        }
        //============================================For Placeholder property======================================
        var rowCheckBoxHeaderForPlaceholder = $("#RepeaterTableC input[id*='ComplianceSelectAll']");
        if (rowCheckBoxHeaderForPlaceholder.prop('checked') == true) {
            $("#ContentPlaceHolder1_txtcompliance").attr("placeholder", "Selected multiple Files");
        }
        else if (rowCheckBoxHeaderForPlaceholder.prop('checked') == false) {
            $("#ContentPlaceHolder1_txtcompliance").attr("placeholder", "Compliance");
        }
        //============================================For Placeholder property======================================
    }
    function UncheckcomplianceHeader() {


        var rowCheckBox = $("#RepeaterTableC input[id*='chkcompliance']");
        var rowCheckBoxSelected = $("#RepeaterTableC input[id*='chkcompliance']:checked");
        var rowCheckBoxHeader = $("#RepeaterTableC input[id*='complianceSelectAll']");
        if (rowCheckBox.length == rowCheckBoxSelected.length) {
            rowCheckBoxHeader[0].checked = true;
        } else {

            rowCheckBoxHeader[0].checked = false;
        }
        //$("#RepeaterTableC input[id*='btnGetSelectedLbl']").click();
    }
    function UncheckPeriodHeader() {

        var rowCheckBox = $("#RepeaterTable input[id*='chkPeriod']");
        var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkPeriod']:checked");
        var rowCheckBoxHeader = $("#RepeaterTable input[id*='PeriodSelectAll']");
        if (rowCheckBox.length == rowCheckBoxSelected.length) {
            rowCheckBoxHeader[0].checked = true;
        } else {
            rowCheckBoxHeader[0].checked = false;
        }
    }


    function iteratePages(object) {
        var str = "";
        if (object !== null && object.length > 0) {
            for (var x = 0; x < object.length; x++) {
                str += "<ul style='margin-top: 0px;margin-left: -35px;'>" + object[x].PageName + "</ul>";
            }
        }
        return str;
    }
    function iteratePages1(object) {
        var str = "";
        if (object !== null && object.length > 0) {
            for (var x = 0; x < object.length; x++) {
                str += "<ul style='margin-top: 0px;margin-left: -35px;'>" + object[x].InputID + "</ul>";
            }
        }
        return str;
    }
    function iteratePages2(object) {
        var str = "";
        if (object !== null && object.length > 0) {
            for (var x = 0; x < object.length; x++) {
                str += "<ul style='margin-top: 0px;margin-left: -35px;'>" + object[x].FileType + "</ul>";
            }
        }
        return str;
    }

    function initializeJQueryUIDeptDDL() {
        //
        $("#<%= txtPeriodList.ClientID %>").unbind('click');
    $("#<%= txtPeriodList.ClientID %>").click(function () {
        $("#dvPeriod").toggle("blind", null, 100, function () { });
    });
    $("#<%= txtFileInputList.ClientID %>").unbind('click');
    $("#<%= txtFileInputList.ClientID %>").click(function () {
        $("#dvFileInput").toggle("blind", null, 100, function () { });
    });

    $("#<%= txtPeriod1List.ClientID %>").unbind('click');
    $("#<%= txtPeriod1List.ClientID %>").click(function () {
        $("#dvPeriod1").toggle("blind", null, 100, function () { });
    });
    $("#<%= txtActNameList.ClientID %>").unbind('click');
    $("#<%= txtActNameList.ClientID %>").click(function () {
        $("#dvActName").toggle("blind", null, 100, function () { });
    });
    $("#<%= txtcompliance.ClientID %>").unbind('click');
    $("#<%= txtcompliance.ClientID %>").click(function () {
        $("#dvcomplianceName").toggle("blind", null, 100, function () { });
    });
}

//bindTreeListGrid1(CustomerID, BranchId, InputId, ComplianceType, ActGroup, ComplianceID, ReturnRegisterChallanID, DocumentType, MonthId, Year, Period) {
        function bindTreeListGrid1(CustomerID, ComplianceType, ComplianceID, MonthId, Year, LoginUserID, ClientID,JSONID) {

    // var crudServiceBaseUrl = "<%= avacomAPIUrl %>GetAllFileInputOutput?CustomerID=" + CustomerID + "BranchID=" + BranchId + "InputId=" + InputId + "ComplianceType=" + ComplianceType + "ActGroup=" + ActGroup + "ComplianceID=" + ComplianceID + "ReturnRegisterChallanID=" + ReturnRegisterChallanID + "DocumentType=" + DocumentType + "Month=" + Month + "Year=" + Year + "Period=" + Period;
            var crudServiceBaseUrl = "<%= avacomAPIUrl %>GetAllFileInputOutput?CustomerID=" + CustomerID + "&ComplianceType=" + ComplianceType + "&ComplianceID=" + ComplianceID + "&MonthId=" + MonthId + "&Year=" + Year + "&UserID=" + LoginUserID + "&ClientID=" + ClientID + "&JSONID=" + JSONID;


    var initialLoad = true;
    $("#grid").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: crudServiceBaseUrl,
                  
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                        request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                        request.setRequestHeader('Content-Type', 'application/json');
                    },
		  dataType: "json",
                },

            },

            schema: {
                model: {

                    id: "ComplianceID",
                    id: "ReturnRegisterChallanID",

                },
                data: function (response) {
                    if (response != null && response != undefined) {

                        return response.Result;
                    }
                },
                total: function (response) {
                    if (response.Result != null && response.Result != undefined) {
                        if (response.Result.length > 0) {

                        }
                        return response.Result.length;
                    }
                }
            }

        },

        columns: [
           {
               field: "IsSelected", title: " ", template: "<input type='checkbox' #= IsSelected ? checked='checked':'' # class='chkbxComplianceAssign' />", width: "40px;"
               , headerTemplate: "<input type='checkbox' id='header_chbReAssignment' >", 
           },
            { field: "ComplianceID", hidden: true, },
            {
                field: "ShortDescription", title: 'Compliance Name', width: "25%;", attributes: { style: "vertical-align: top;" }//, attributes: { "class": "table-cell", style: "vertical-align: top;" }
            },
            { field: "ReturnRegisterChallanID", title: 'ReturnRegisterChallanID', hidden: true },
            { field: "BranchIDList", title: 'BranchIDList', hidden: true },
            { field: "Month", title: 'Month', hidden: true },
            { field: "lstPages", title: "InputID", template: "#= iteratePages1(data.lstPages)#", hidden: true },
            { field: "ScheduleOnIDList", title: 'ScheduleOnIDList', hidden: true },
            { field: "lstPages", title: "Input File ", template: "#= iteratePages(data.lstPages)#", width: "28%;", attributes: { style: "font-color: black;text-align: left;vertical-align: top;" } },
            {
                title: 'Branch Count',

                command: [
                       { name: "x", text: " ", className: "my-edit", attributes: { style: " cursor: pointer;" } }

                ], lock: true

                , headerTemplate: { style: "text-align: center;" }, width: "12%;", attributes: { style: "text-align: center;" }
            },
            { field: "RequiredForms", title: 'Output Form', width: "32%;", attributes: { style: "vertical-align: top;" } }
            ,
            { field: "Frequency", title: 'Frequency', width: "10%;", attributes: { style: "vertical-align: top;" } }


        ],
        noRecords: {
            template: "No data found."
        },
        dataBound: onDataBound
    });

    $('#grid').on('click', '.chkbxComplianceAssign', function () {

        var checked = $(this).is(':checked');
        var grid = $('#grid').data().kendoGrid;
        var dataItem = grid.dataItem($(this).closest('tr'));
        // this.dirty = true;
        if (dataItem != undefined) {

            dataItem.set('IsSelected', checked);
        }
        if (checked == false) {

            $('#header_chbReAssignment').prop('checked', false);
        }
        //dataItem.set('dirty', true);
        // grid.refresh();

    });

    $('#header_chbReAssignment').change(function () {

        checkAll(this);
    });

    function onDataBound(e) {

        var data = this.dataSource.view();
        for (var i = 0; i < data.length; i++) {
            var uid = data[i].uid;
            var row = this.table.find("tr[data-uid='" + uid + "']");
            if (data[i].BranchCount) {
                row.find(".k-command-cell").contents().last()[0].textContent = data[i].BranchCount;
            }
            else {
                row.find(".k-command-cell").contents().hide();
            }
        }
    }

    function checkAll(ele) {
        var chk = $(ele).is(':checked');
        var grid = $('#grid').data().kendoGrid;

        $.each(grid.dataSource.view(), function () {
            if (this['IsSelected'] != chk)
                // this.dirty = true;
                this['IsSelected'] = chk;
        });
        grid.refresh();
    }

}
//Out Grid
function bindKendoGridOut(CustomerID, ComplianceType, ComplianceID,MonthId, Year, LoginUserID,ClientID,JSONID) {
    if (JSONID == "undefined" || JSONID == undefined) {
        $("#ContentPlaceHolder1_btnOutSubmit").click();
    }
    else {
        // var crudServiceBaseUrl = "<%= avacomAPIUrl %>GetAllFileInputOutput?CustomerID=" + CustomerID + "BranchID=" + BranchId + "InputId=" + InputId + "ComplianceType=" + ComplianceType + "ActGroup=" + ActGroup + "ComplianceID=" + ComplianceID + "ReturnRegisterChallanID=" + ReturnRegisterChallanID + "DocumentType=" + DocumentType + "Month=" + Month + "Year=" + Year + "Period=" + Period;
        //var crudServiceBaseUrl = "<%= avacomAPIUrl %>GetAllFileInputOutput?CustomerID=" + CustomerID + "&ComplianceType=" + ComplianceType + "&ComplianceID=" + ComplianceID + "&MonthId=" + MonthId + "&Year=" + Year + "&UserID=" + LoginUserID + "&ClientID=" + ClientID + "&JSONID=" + JSONID;

    // var crudServiceBaseUrl = "<%= avacomAPIUrl %>GetAllFileInputOutput?CustomerID=" + CustomerID + "BranchID=" + BranchId + "InputId=" + InputId + "ComplianceType=" + ComplianceType + "ActGroup=" + ActGroup + "ComplianceID=" + ComplianceID + "ReturnRegisterChallanID=" + ReturnRegisterChallanID + "DocumentType=" + DocumentType + "Month=" + Month + "Year=" + Year + "Period=" + Period;
    var crudServiceBaseUrl = "<%= avacomAPIUrl %>GetAllFileInputOutput?CustomerID=" + CustomerID + "&ComplianceType=" + ComplianceType + "&ComplianceID=" + ComplianceID + "&MonthId=" + MonthId + "&Year=" + Year + "&UserID=" + LoginUserID + "&ClientID=" + ClientID + "&JSONID=" + JSONID;


    var initialLoad = true;
    $("#gridOutput").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: crudServiceBaseUrl,
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                        request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                        request.setRequestHeader('Content-Type', 'application/json');
                    },
                },

            },

            schema: {
                model: {

                    id: "ComplianceID",
                    id: "ReturnRegisterChallanID"
                },
                data: function (response) {
                    if (response != null && response != undefined) {
                        if (response.Result.length > 0) {
                           <%-- var ComplianceId = '<% =this.ComplianceID_%>';
                            if (ComplianceId != '') {--%>
                            $("#header_chbReAssignment1").prop('checked', true);
                            for (var i = 0; i < response.Result.length; i++) {
                                response.Result[i].IsSelected = true;
                            }
                            // }
                        }
                        return response.Result;

                    }
                },
                total: function (response) {
                    if (response.Result != null && response.Result != undefined) {

                        return response.Result.length;
                    }
                }
            }

        },

        columns: [
          {
              field: "IsSelected", title: " ", template: "<input type='checkbox' #= IsSelected ? checked='checked':'' # class='chkbxComplianceAssign1' disabled />",width:"40px",
              headerTemplate: "<input type='checkbox' id='header_chbReAssignment1'disabled>",
              attributes: { style: "vertical-align: top;border-width:0px 0px 1px 1px;" },
              headerAttributes: {
        style: "border-width: 1px 0px 0px 1px;"
              }
          },
           { field: "ComplianceID", hidden: true, },
           {
               field: "ShortDescription", title: 'Compliance Name', width: "25%;", attributes: { style: "vertical-align: top;border-width:0px 0px 1px 1px;" }//, attributes: { "class": "table-cell", style: "vertical-align: top;" }
           },
           { field: "ReturnRegisterChallanID", title: 'ReturnRegisterChallanID', hidden: true },
           { field: "BranchIDList", title: 'BranchIDList', hidden: true },
           { field: "Month", title: 'Month', hidden: true },
           { field: "lstPages", title: "InputID", template: "#= iteratePages1(data.lstPages)#", hidden: true },
           { field: "ScheduleOnIDList", title: 'ScheduleOnIDList', hidden: true },
           { field: "lstPages", title: "Input File ", template: "#= iteratePages(data.lstPages)#", width: "25%;", attributes: { style: "font-color: black;text-align: left;vertical-align: top;border-width:0px 0px 1px 1px;" } },
           { field: "TextPeriod", title: 'Period', width: "13%;", attributes: { style: "vertical-align: top;border-width:0px 0px 1px 1px;" } },
           { field: "lstPages", title: "FileType", template: "#= iteratePages2(data.lstPages)#", hidden: true },
           {
               title: 'Branch Count',

               command: [
                      { name: "x", text: " ", className: "my-edit1", attributes: { style: " cursor: pointer;" } }

               ], lock: true

               , headerTemplate: { style: "text-align: center;" }, width: "12%;", attributes: { style: "text-align: center;" }
           },
           { field: "RequiredForms", title: 'Output Form', width: "24%;", attributes: { style: "vertical-align: top;" } }
           ,
           { field: "Frequency", title: 'Frequency', width: "10%;", attributes: { style: "vertical-align: top;border-width:0px 1px 1px 1px;" } }
           


        ],
        noRecords: {
            template: "No data found."
        },
        dataBound: onDataBound1
    });




    $('#gridOutput').on('click', '.chkbxComplianceAssign1', function () {

        var checked = $(this).is(':checked');
        var grid = $('#gridOutput').data().kendoGrid;
        var dataItem = grid.dataItem($(this).closest('tr'));
        if (dataItem != undefined) {
            dataItem.set('IsSelected', checked);
        }

        if (checked == false) {
            $('#header_chbReAssignment1').prop('checked', false);
        }
        //dataItem.set('dirty', true);
        //grid.refresh();

    });

    $('#header_chbReAssignment1').change(function () {

        checkAll1(this);
    });
    function onDataBound1(e) {
        var btnflage = false;
        var data = this.dataSource.view();
        for (var i = 0; i < data.length; i++) {
            var uid = data[i].uid;
            var row = this.table.find("tr[data-uid='" + uid + "']");
            if (data[i].BranchCount) {
                row.find(".k-command-cell").contents().last()[0].textContent = data[i].BranchCount;

            }
            else {
                row.find(".k-command-cell").contents().hide();
            }

            debugger;
            for (var j = 0; j < data[i].lstPages.length; j++) {
                var FileType = data[i].lstPages[j].FileType;
                if (FileType == "1" && btnflage == false) {
                    btnflage = true;
                    $('#btnProceedOut').show();
                    $('#btnGenerateOut').hide();
                    break;
                }
                if (FileType == "2" && btnflage == false) {
                    $('#btnProceedOut').hide();
                    $('#btnGenerateOut').show();
                }
            }


        }
    }

    function checkAll1(ele) {
        var chk = $(ele).is(':checked');
        var grid = $('#gridOutput').data().kendoGrid;
        $.each(grid.dataSource.view(), function () {
            if (this['IsSelected'] != chk)
                this.dirty = true;
            this['IsSelected'] = chk;
        });
        grid.refresh();
        }
    }

}
function DivClick(e, id) {

    var ddlFileType = $('#ContentPlaceHolder1_ddlFileType').val();
    if (ddlFileType == "Other") {
        var a = e.innerText;
        $('#ContentPlaceHolder1_hdnInputId').val(id);
        $('#ContentPlaceHolder1_txtFileInputList').val(a);
        $('#ContentPlaceHolder1_btnFileSelectnOther').click();
        //alert(id);

    }

}

function CompliancePopup(item, Type) {
    var IOJsonNew = {};
    var IOJson = [];
    var ScheduleOnID = item.ScheduleOnIDList;
    var ReturnRegisterChallanID = item.ReturnRegisterChallanID;
    IOJsonNew = {
        ScheduleOnID: ScheduleOnID,
        ReturnRegisterChallanID: ReturnRegisterChallanID
    };
    IOJson.push(IOJsonNew);
    // alert(IO);
    var ComplianceID;
    var InputID;
    var Month;
    var Year;
    var ComplianceType;
    var ddlFileType;
    var BranchID;
    if (Type == "I") {


        ComplianceID = item.ComplianceID;
        InputID = $('#ContentPlaceHolder1_hdnInputId').val();
        Month = $('#ContentPlaceHolder1_hdnMonth').val();
        Year = $('#ContentPlaceHolder1_hdnYear').val();
        ComplianceType = $('#ContentPlaceHolder1_hdnComplianceType').val();
        ddlFileType = $('#ContentPlaceHolder1_ddlFileType').val();
        BranchID = $('#ContentPlaceHolder1_hdnBranchId').val();
    }
    else {
        ComplianceID = item.ComplianceID;
        InputID = $('#ContentPlaceHolder1_hdnOutInputId').val();
        Month = $('#ContentPlaceHolder1_hdnOutMonth').val();
        Year = $('#ContentPlaceHolder1_hdnOutYear').val();
        ComplianceType = $('#ContentPlaceHolder1_hdnOutComplianceType').val();
        ddlFileType = "";
        BranchID = $('#ContentPlaceHolder1_hdnOutBranchId').val();
    }
    if (BranchID == "") {
        BranchID = item.BranchIDList;
    }
    var ParameterJson = [];
    var ParameterJsonNew = {};
    ParameterJsonNew = {
        InputID: InputID,
        BranchID: BranchID
    };

    ParameterJson.push(ParameterJsonNew);
    var ParameterID = JSON.stringify(ParameterJson);
    var IO = JSON.stringify(IOJson);
    $.ajax({
        type: "POST",
        url: "../RLCS/ComplianceInputOutput.aspx/JSONInsert",
        data: "{ 'IOJson': '" + IO + "','ParameterID':'" + ParameterID + "' }",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            debugger;
            if (data.d != 0) {

                var myWindowAdv = $("#CompliancePopup");
                var Popupuri = "../RLCS/InputOutputPopup.aspx?InputID=" + InputID + "&Month=" + Month + "&Year=" + Year + "&ComplianceType=" + ComplianceType + "&ddlFileType=" + ddlFileType + "&JSONID=" +data.d ;
                $(".ui-widget-overlay").attr('style', 'background-color: #000; opacity:1; z-index:1000;');

                myWindowAdv.kendoWindow({
                    width: "96%",
                    height: '85%',
                    position: {
                        top: "40px",
                        left: "2%"
                    },

                    content: Popupuri,
                    iframe: true,
                    title: "Input Status",
                    visible: false,
                    modal: true



                });
                myWindowAdv.data("kendoWindow").open();

                return false;
            }
        },
        failure: function (response) {
            
        }
    });
    
}

////////////////////////////////////////////////////////////////////////////////
$(document).ready(function () {

    ////Panel 
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {


        //    // re-bind your jQuery events here
        $("#ContentPlaceHolder1_ddlYear").change(function () {
            $('#ContentPlaceHolder1_btnFileInput').click();
        });
        //ADD
        $("#ContentPlaceHolder1_txtcompliance").click(function () {
            $("#dvActName").hide();
        });
        $("#ContentPlaceHolder1_txtPeriod1List").click(function () {
            $("#dvcomplianceName").hide();
        });
        //END
        //Out
        $("#ContentPlaceHolder1_ddlOutYear").change(function () {
            $("#dvPeriod1").hide();
        });
        //IN


        var ddlFileType = $('#ContentPlaceHolder1_ddlFileType').val();
        if (ddlFileType == "Other") {
            $('#divSelectAll').hide();
        }

        var btnSubmitVal = $('#ContentPlaceHolder1_hdnSubmitbtn').val();
        // var LoginUserID = '<% =this.LoginUserID%>';
        var LoginUserID = $('#ContentPlaceHolder1_hdnLoginUserID').val();
        if (btnSubmitVal == "Submit") {
            var ValidationError = $('#ContentPlaceHolder1_vsDocGen ul').text();
            if (ValidationError == "") {
                $('#btnProceed').show();

                var CustomerID = $('#ContentPlaceHolder1_hdnCustID').val();
                var BranchId = $('#ContentPlaceHolder1_hdnBranchId').val();
                var InputId = $('#ContentPlaceHolder1_hdnInputId').val();
                var ComplianceType = $('#ContentPlaceHolder1_hdnComplianceType').val();
                //var ActGroup = $('#ContentPlaceHolder1_hdnActGroup').val();
                var ComplianceID = $('#ContentPlaceHolder1_hdnComplianceID').val();
                //var ReturnRegisterChallanID = $('#ContentPlaceHolder1_hdnReturnRegisterChallanID').val();
                //var DocumentType = $('#ContentPlaceHolder1_hdnDocumentType').val();
                var Month = $('#ContentPlaceHolder1_hdnMonth').val();
                var Year = $('#ContentPlaceHolder1_hdnYear').val();
                //var Period = $('#ContentPlaceHolder1_hdnPeriod').val();
                var ClientID = "";
                if (ComplianceType != '') {
                    var IOJson = [];
                    var ParameterJson = [];
                    var ParameterJsonNew = {};
                    ParameterJsonNew = {
                        InputID: InputId,
                        BranchID: BranchId
                    };
                    ParameterJson.push(ParameterJsonNew);
                    var ParameterID = JSON.stringify(ParameterJson);
                    var IO = JSON.stringify(IOJson);
                    $.ajax({
                        type: "POST",
                        url: "../RLCS/ComplianceInputOutput.aspx/JSONInsert",
                        data: "{ 'IOJson': '" + IO + "','ParameterID':'" + ParameterID + "' }",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            debugger;
                            if (data.d != 0) {
                                bindTreeListGrid1(CustomerID, ComplianceType, ComplianceID, Month, Year, LoginUserID, ClientID, data.d);
                            }
                        },
                        failure: function (response) {

                        }
                    });
                    
                }
            }
            else {
                $('#ContentPlaceHolder1_hdnSubmitbtn').val('');
            }
        }
        else if (btnSubmitVal == "OutSubmit") {
            var ValidationError = $('#ContentPlaceHolder1_vsDocGenOut ul').text();
            if (ValidationError == "") {
                // $('#btnProceedOut').show();

                var CustomerID = $('#ContentPlaceHolder1_hdnOutCustID').val();
                var BranchId = $('#ContentPlaceHolder1_hdnOutBranchId').val();
                var InputId = $('#ContentPlaceHolder1_hdnOutInputId').val();
                var ComplianceType = $('#ContentPlaceHolder1_hdnOutComplianceType').val();
                //var ActGroup = $('#ContentPlaceHolder1_hdnOutActGroup').val();
                var ComplianceID = $('#ContentPlaceHolder1_hdnOutComplianceID').val();
                //var ReturnRegisterChallanID = $('#ContentPlaceHolder1_hdnOutReturnRegisterChallanID').val();
                //var DocumentType = $('#ContentPlaceHolder1_hdnOutDocumentType').val();
                var Month = $('#ContentPlaceHolder1_hdnOutMonth').val();
                var Year = $('#ContentPlaceHolder1_hdnOutYear').val();
                //var Period = $('#ContentPlaceHolder1_hdnOutPeriod').val();
                var ClientID = "";
                if (ComplianceType != '') {
                    var IOJson = [];
                    var ParameterJson = [];
                    var ParameterJsonNew = {};
                    ParameterJsonNew = {
                        InputID: InputId,
                        BranchID: BranchId
                    };
                    ParameterJson.push(ParameterJsonNew);
                    var ParameterID = JSON.stringify(ParameterJson);
                    var IO = JSON.stringify(IOJson);
                    $.ajax({
                        type: "POST",
                        url: "../RLCS/ComplianceInputOutput.aspx/JSONInsert",
                        data: "{ 'IOJson': '" + IO + "','ParameterID':'" + ParameterID + "' }",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            debugger;
                            if (data.d != 0) {
                                bindKendoGridOut(CustomerID, ComplianceType, ComplianceID, Month, Year, LoginUserID, ClientID, data.d);
                            }
                        },
                        failure: function (response) {

                        }
                    });
                    
                }
            }
            else {
                $('#ContentPlaceHolder1_hdnSubmitbtn').val('');
            }
        }

    });
});

/////////////////////////////////Output///////////////////
function checkAllAct(cb) {
    var ctrls = document.getElementsByTagName('input');
    for (var i = 0; i < ctrls.length; i++) {
        var cbox = ctrls[i];
        if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
            cbox.checked = cb.checked;

        }
    }
}
function UncheckActHeader() {


    var rowCheckBox = $("#RepeaterTableAct input[id*='chkAct']");
    var rowCheckBoxSelected = $("#RepeaterTableAct input[id*='chkAct']:checked");
    var rowCheckBoxHeader = $("#RepeaterTableAct input[id*='ActSelectAll']");
    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        rowCheckBoxHeader[0].checked = true;
    } else {

        rowCheckBoxHeader[0].checked = false;
    }
}
function checkAllPeriods1(cb) {

    var ctrls = document.getElementsByTagName('input');
    for (var i = 0; i < ctrls.length; i++) {
        var cbox = ctrls[i];
        if (cbox.type == "checkbox" && cbox.id.indexOf("chkPeriod1") > -1) {
            cbox.checked = cb.checked;
        }
    }
}
function UncheckPeriodHeader1() {

    var rowCheckBox = $("#RepeaterTableP input[id*='chkPeriod1']");
    var rowCheckBoxSelected = $("#RepeaterTableP input[id*='chkPeriod1']:checked");
    var rowCheckBoxHeader = $("#RepeaterTableP input[id*='Period1SelectAll']");
    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        rowCheckBoxHeader[0].checked = true;
    } else {
        rowCheckBoxHeader[0].checked = false;
    }
}
function DivClick1(e, id) {

    //var a = e.innerText;
    //$('#ContentPlaceHolder1_hdnActID').val(id);
    //$('#ContentPlaceHolder1_txtActNameList').val(a);
    //$('#ContentPlaceHolder1_btnComplianceGet').click();

}
function CallbtnCompliance() {
    $('#ContentPlaceHolder1_btnComplianceGet').click();
}
function CallbtnGetFrequency() {
    $('#ContentPlaceHolder1_btnFrequencyGet').click();
}

function InputBlur() {

    $("#dvFileInput").hide(); // 21Nov
}

    </script>
    <style>
        .h1, h2, h3, h4, h5, label {
            font-weight: 400;
        }
        .hoverCircle:hover {
            -moz-box-shadow: 0 0 0 12px #E9EAEA;
            -webkit-box-shadow: 0 0 0 12px #E9EAEA;
            box-shadow: 0 0 0 12px #E9EAEA;
            -moz-transition: -moz-box-shadow .3s;
            -webkit-transition: -webkit-box-shadow .3s;
            transition: box-shadow .3s;
            border-radius: 50%;
            background-color: #E9EAEA;
        }

        .setChkSpace input[type="checkbox"] {
            margin-right: 10px;
        }
          .setChkSpace1 input[type="checkbox"] {
            margin-left: 10px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 7px;
        }


        .chosen-results {
            max-height: 200px !important;
            margin-top: 0px !important;
        }

        .chosen-container .chosen-drop {
            width: 175% !important;
        }

        .chosen-container-single .chosen-default {
            color: #666 !important;
        }

        .ContentPlaceHolder1_tvFilterLocation_1 {
            color: #666;
        }

        .ContentPlaceHolder1_tvOutputBranch_1 {
            color: #666;
        }

        .body {
            /*color: #353333;*/
            color: #666;
            border: 1px thin;
        }

        .form-control {
            color: #666;
            padding:6px 9px;
        }
        .chosen-container-single .chosen-single div{
            top: 2px;
        }
        .k-grid-footer-wrap, .k-grid-header-wrap {
   border-width: 0 0px 0 0;
}
        .btn{
              font-weight:400;
          }
    
        .panel-body {
            padding: 1px;
            margin-top: 10px;
        }
        /*html .k-grid tr:hover {
            background: transparent;
        }

        html .k-grid tr.k-alt:hover {
            background: #f1f1f1;
        }*/
        #ContentPlaceHolder1_rptFileInput_FileInputSelectAll {
            margin-right: 7px;
        }

        #ContentPlaceHolder1_rptPeriodList_PeriodSelectAll {
            margin-right: 3px;
        }

        label[for="ContentPlaceHolder1_rptFileInput_FileInputSelectAll"], label[for="ContentPlaceHolder1_rptPeriodList_PeriodSelectAll"], label[for="ContentPlaceHolder1_rptAct_ActSelectAll"], label[for="ContentPlaceHolder1_rptPeriod1_Period1SelectAll"] {
            font-weight: 400;
            font-family: 'Roboto',sans-serif;
            padding-left: 3px;
        }
         input[type=file]:focus, input[type=checkbox]:focus, input[type=radio]:focus{
            outline:none;
        }

        #ContentPlaceHolder1_rptAct_ActSelectAll, #ContentPlaceHolder1_rptPeriod1_Period1SelectAll {
            margin-right: 4px;
        }
    </style>
    <style type="text/css">
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        /*.k-widget k-window {
           top:0px;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
            border-width:0px 0px 1px 1px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            border-radius: 35px;
            background-color:transparent;
            border-color:transparent;
        }
        .k-header{
            background:black;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-grid-content {
            min-height: 32px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }
        .k-filter-row th, .k-grid-header th[data-index='13'] {
            border-width:1px 1px 0px 0px;
        }

        /*.k-grid,
        .k-grid-header-wrap,
        .k-grid-header th,
        .k-grid-content > table > tbody > tr > td {
            border: none;
        }*/
        .k-grid-header th {
            height: 15px;
        }

        .Dashboard-white-widget {
            padding: 2px 10px 0px;
            margin-bottom: 0px;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        td.k-command-cell {
            border-width: 0 0px 1px 1px;
        }

        .k-grid-pager {
            border-width: 1px 0px 0px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        /*.k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 0px;
        }*/

        #AdavanceSearch {
            border: .5px solid #dcdcdf;
            background-color: #f5f5f6;
        }

        .clearfix {
            height: 9px !important;
        }

        .container {
            max-width:100%;
            max-height:550px;
        }
         label:hover{
            color:#1fd9e1;
     }
         a:focus, a:hover {
         color:#1fd9e1;
         }
        /*.k-grid-content{
            height:395px !important;
        }*/
        .sidebar-menu {
            margin-top: 53px !important;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color:#1fd9e1;
            background-color: #fff;
            border: 1px solid #ddd;
            border-bottom-color: transparent;
            cursor: default;
            font-size: 17px;
        }

        .nav-tabs > li > a {
            color: #8e8e93;
            margin-right: 2px;
            line-height: 1.428571429;
            border: 1px solid transparent;
            border-radius: 4px 4px 0 0;
            font-size: 17px;
        }

        .k-dirty {
            display: none;
        }
        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 1px 0px 0px 0px;
            height: 30px;
            border-color:#dedee0;
            vertical-align: middle;
            padding-top: 2px;
            padding-bottom: 3px;
            /*font-family: 'Roboto',sans-serif;*/
            font-weight: bold;
            font-size: 15px;
            color:#535b6a;
        
        }
        .k-grid, .k-widget, .k-display-block{
            border-width:0px 1px 0px 1px;
        }
        .k-filter-row th, .k-grid-header th[data-index='13'] {
            border-width: 1px 1px 0px 0px;
        }
       .k-grid, .k-widget, .k-display-block {
    border-width: 0px 1px 0px 1px;
    border-color:#dedee0;
}
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="container">
        <div class="row">
            <div class="col-md-12 colpadding0">
                <div id="Tabs" role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="Mytab">
                        <li class="active" id="tabInput" onclick="return Test();"><a href="#Input" aria-controls="Input" role="tab" data-toggle="tab">Input</a>  </li>
                        <li id="tabOutput" onclick="return Test1();"><a href="#Output" aria-controls="Output" role="tab" data-toggle="tab">Output</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="Input">
                            <asp:UpdatePanel ID="upInputOutput" runat="server" OnLoad="upDocGenPopup_Load">
                                <ContentTemplate>
                                    <div class="col-md-12 colpadding0">
                                        <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="DocGenValidationGroup" />
                                        <asp:ValidationSummary ID="vsDocGen1" runat="server" class="alert alert-block alert-success fade in" ValidationGroup="success" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="DocGenValidationGroup" Display="None" />
                                        <asp:CustomValidator ID="cvDuplicateEntrysucess" runat="server" EnableClientScript="False"
                                            ValidationGroup="success" Display="None" />
                                    </div>

                                    <asp:Button ID="btnFileInput" runat="server" Text="" OnClick="btnFileInput_Click" CssClass="hidden" />
                                    <asp:Button ID="btnFileSelectnOther" runat="server" Text="" OnClick="btnFileSelectnOther_Click" CssClass="hidden" />
                                    <asp:Button ID="btnhdnClear1" runat="server" Text="" OnClick="btnhdnClear1_Click" CssClass="hidden" />
                                    <asp:HiddenField ID="hdnSubmitbtn" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnCustID" runat="server" />
                                    <asp:HiddenField ID="hdnBranchId" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnInputId" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnComplianceType" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnActGroup" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnComplianceID" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnReturnRegisterChallanID" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnDocumentType" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnMonth" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnYear" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdnPeriod" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnLoginUserID" runat="server" Value="" />
                                      <asp:HiddenField ID="hdnJSONID" runat="server" Value="" />
                                    <div class="row">

                                        <div class="col-md-12 colpadding0">
                                            <div class="panel-body body ">

                                                <div class="col-md-1 colpadding0" style="width: 13%">
                                                    <asp:Label Text="Input File Type" runat="server" CssClass="hidden form-control" />

                                                    <asp:DropDownList ID="ddlFileType" runat="server" Width="90%" CssClass="form-control" Style="" OnSelectedIndexChanged="ddlFileType_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Text="All File Type" Value="" />
                                                        <asp:ListItem Text="Monthly" Value="Monthly" />
                                                        <asp:ListItem Text="Annual" Value="Annual" />
                                                        <asp:ListItem Text="Other" Value="Other" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-2 colpadding0" style="width: 18%">
                                                    <asp:Label Text=" Input File Selection" runat="server" CssClass="hidden" />

                                                    <asp:TextBox runat="server" ID="txtFileInputList" Width="93%" Style="padding: 5px; margin: 0px;" CssClass="txtbox form-control" AutoComplete="off" placeholder="All Files"/>
                                                    <div style="display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width: 150%;" id="dvFileInput" class="dvDeptHideshow form-control">
                                                        <asp:Repeater ID="rptFileInput" runat="server">
                                                            <HeaderTemplate>
                                                                <table class="detailstable FadeOutOnEdit record_table" id="RepeaterTable1">
                                                                    <tr>
                                                                        <td style="width: 100px;" colspan="2">
                                                                            <div id="divSelectAll">
                                                                                <asp:CheckBox ID="FileInputSelectAll" Text="Select All" runat="server" onclick="checkAllFileInput(this)" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                               
                                                                    <tr>
                                                                    <td id="tdChkFiles" runat="server" style="width:100%;">                                                                       
                                                                        <div id="chkDivision"   onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666'">
                                                                            <asp:CheckBox ID="chkFileInput" Text='<%# Eval("Name")%>' runat="server"  onclick=" UncheckFileInputHeader();" CssClass="setChkSpace" OnCheckedChanged="btnFileInput_Click"  />
                                                                         </div>
                                                                    </td>
                                                                    <td>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%; padding-bottom: 5px;">
                                                                            <asp:Label ID="lblFileInputID" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                            <asp:Label ID="lblFileInputName" runat="server"  Text='<%# Eval("Name")%>'  onclick='<%# string.Format("DivClick(this,\"{0}\");",Eval("ID")) %>' onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666'"  ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                            <asp:Button ID="btnGetSelectedLbl" runat="server"  CssClass="hidden" OnClick="btnGetSelectedLbl_Click"/>
                                                                            <div id="divfileinput" style="margin-top: 3px;cursor: pointer;" onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666';" onclick="return DivClick(this,<%# Eval("ID")%>)"></div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>

                                                    </div>
                                                </div>

                                                <div class="col-md-1 colpadding0" style="width: 13%">
                                                    <asp:Label Text="Period" runat="server" CssClass="hidden" />

                                                    <asp:TextBox runat="server" ID="txtPeriodList" Width="92%" Style="padding:5px; margin: 0px;" autocomplete="off" CssClass="txtbox form-control" onclick="return InputBlur();" placeholder="Periods"/>
                                                    <div style="margin-top: 0px;cursor: pointer; display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width: 107%;" id="dvPeriod" class="dvDeptHideshow form-control"  onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666';">
                                                        <asp:Repeater ID="rptPeriodList" runat="server">
                                                            <HeaderTemplate>
                                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                                    <tr>
                                                                        <td style="width: 100px;  padding-left:3px;" colspan="2">
                                                                            <asp:CheckBox ID="PeriodSelectAll" Text="&nbsp;Select All" runat="server" onclick="checkAllPeriods(this)"  />
                                                                        </td>
                                                                    </tr>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <%-- Added by teju for checkbox padding left alignment --%>
                                                                    <td style="width: 100%;  padding-left:3px;">
                                                                        <div id="chkDivPeriod"  onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666'">
                                                                            <asp:CheckBox ID="chkPeriod" runat="server" Text='<%# Eval("Name")%>' onclick="UncheckPeriodHeader();" CssClass="setChkSpace" /></td>
                                                                        </div>
                                                                    <td>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 92%; padding-bottom: 5px; padding-left:5px; margin-top: 3px;" onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666';">
                                                                            <asp:Label ID="lblPeriodID" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                            <asp:Label ID="lblPeriodName" runat="server" Style="display: none" ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>

                                                <div class="col-md-1 colpadding0" style="width: 12%;">
                                                    <asp:Label Text="Year" runat="server" CssClass="hidden" />

                                                    <asp:DropDownList runat="server" ID="ddlYear" Width="92%" class="form-control" Style="" placeholder="Year">
                                                        <asp:ListItem Text="Year" Value="0" />
                                                        <asp:ListItem Text="2020" Value="2020" />
                                                        <asp:ListItem Text="2019" Value="2019" />
                                                        <asp:ListItem Text="2018" Value="2018" />
                                                        <asp:ListItem Text="2017" Value="2017" />
                                                        <asp:ListItem Text="2016" Value="2016" />
                                                        <asp:ListItem Text="2015" Value="2015" />
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="col-md-2 colpadding0" style="width: 15%">
                                                    <asp:Label Text="Compliance Type" runat="server" CssClass="hidden" />

                                                    <asp:DropDownList ID="ddlComplianceType" Width="92%" runat="server" CssClass="form-control">
                                                       
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="col-md-3 colpadding0" style="width: 22%">
                                                    <asp:Label Text="Entity" runat="server" CssClass="hidden" />

                                                    <asp:TextBox runat="server" Width="92%" AutoComplete="off" ID="tbxFilterLocation"
                                                        Style="padding: 0px; padding-left: 10px; margin: 0px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" placeholder="Entity/State/Location/Branch"
                                                        CssClass="txtbox form-control" />
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10; width: 88%; margin-top: -20px;" id="divFilterLocation">
                                                        <asp:TreeView runat="server" ID="tvFilterLocation" onmouseover="this.style.color='#1fd9e1'" SelectedNodeStyle-Font-Bold="true" Width="110%" NodeStyle-ForeColor="#8e8e93" ShowCheckBoxes="All" onclick="OnTreeClick(event)"
                                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important; max-height: 300px;width:236px;" ShowLines="true">
                                                        </asp:TreeView>
                                                        <div id="bindelement1" style="background: white; height: 292px; display: none; width: 390px; border: 1px solid; overflow: auto;"></div>
                                                        <asp:Button ID="btnlocation1" runat="server" OnClick="btnlocation1_Click" Text="select" CssClass="hidden" />
                                                        <asp:Button ID="btnClear1" Visible="true" runat="server" OnClick="btnClear1_Click" Text="Clear" CssClass="hidden" />
                                                    </div>

                                                </div>

                                                <div class="col-md-1 colpadding0" style="width: 7%"; margin-top: 0px;">

                                                    <asp:Button ID="btnsubmit" Text="Apply" runat="server" CssClass="btn btn-primary pull-right" style="width: 81%;margin-top: 1px;height: 32px;padding-top: 3px;font-size: 15px;" OnClick="btnsubmit_Click" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 colpadding0 panel-body" style="max-height: 350px; overflow-y: auto;" id="divgrid">
                                                <div id="grid"></div>
                                            </div>
                                        </div>
                                        <div class="row colpadding0">
                                            <div class="col-md-12 colpadding0">
                                                <div id="CompliancePopup">
                                                </div>
                                            </div>
                                        </div>

                                        <input id="btnProceed" type="button" value="Proceed" onclick="return ProceedClick();" style="display: none; margin-top: 10px; margin-bottom: 10px;" class="btn btn-primary pull-right" />
                                        <br />
                                        
                                        <%-- ShowHideTab();--%>
                                    </div>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="Output">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" OnLoad="upDocGenPopup_Load">
                                <ContentTemplate>
                                    <div class="col-md-12 colpadding0">
                                        <asp:ValidationSummary ID="vsDocGenOut" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="DocGenValidationGroup1" />
                                        <asp:ValidationSummary ID="vsDocGen1Out" runat="server" class="alert alert-block alert-success fade in" ValidationGroup="success" />
                                        <asp:CustomValidator ID="cvDuplicateEntryOut" runat="server" EnableClientScript="False"
                                            ValidationGroup="DocGenValidationGroup1" Display="None" />
                                        <asp:CustomValidator ID="cvDuplicateEntrysucessOut" runat="server" EnableClientScript="False"
                                            ValidationGroup="success" Display="None" />
                                    </div>
                                    <asp:Button ID="btnComplianceGet" runat="server" Text="" OnClick="btnComplianceGet_Click" CssClass="hidden" />
                                    <asp:Button ID="btnFrequencyGet" runat="server" Text="" OnClick="btnFrequencyGet_Click" CssClass="hidden" />
                                    <asp:Button ID="btnhdnClear" runat="server" Text="" OnClick="btnhdnClear_Click" CssClass="hidden" />
                                    <asp:HiddenField ID="hdnOutBranchId" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutInputId" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutActID" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutComplianceType" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutActGroup" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutComplianceID" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutReturnRegisterChallanID" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutDocumentType" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutMonth" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutYear" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutPeriod" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnOutCustID" runat="server" Value="" />

                                    <div class="row">
                                        <div class="col-md-12 colpadding0">
                                            <div class="panel-body body ">
                                                <div class="col-md-2 colpadding0" style="width: 15%">
                                                    <asp:Label Text="Compliance Type" runat="server" CssClass="hidden" />

                                                    <asp:DropDownList ID="ddlOutputComplianceType" Width="100%"  runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlOutputComplianceType_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Text="Compliance Type"></asp:ListItem>
                                                        <asp:ListItem Text="Register"></asp:ListItem>
                                                        <asp:ListItem Text="Challan"></asp:ListItem>
                                                        <asp:ListItem Text="Return"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-2 colpadding0" style="width: 14%">
                                                    <asp:Label Text="Act Name" runat="server" CssClass="hidden" />

                                                    <asp:TextBox runat="server" ID="txtActNameList" Width="85%" Style="padding: 5px; margin: 0px;margin-left:13px;" CssClass="txtbox form-control" AutoComplete="off" placeholder="Act Name" />
                                                    <div style="display: none; position: absolute; z-index: 50; overflow-y: auto;overflow-x: scroll; background: white; border: 1px solid #c3b9b9; height: 200px; margin-left:12px;width: 421%;" id="dvActName" class="dvDeptHideshow form-control">
                                                        <asp:Repeater ID="rptAct" runat="server">
                                                            <HeaderTemplate>
                                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTableAct">
                                                                    <tr>
                                                                        <td style="width: 100px;" colspan="2">
                                                                            <div id="divSelectAll">
                                                                                <asp:CheckBox ID="ActSelectAll" Text="&nbsp;Select All" runat="server" onclick="checkAllAct(this);CallbtnCompliance();" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="width: 100%;">
                                                                         <div id="chkDivAct" style="white-space: nowrap;"  onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666'">
                                                                                <asp:CheckBox ID="chkAct" runat="server" Text='<%# Eval("Name")%>' onclick=" UncheckActHeader();CallbtnCompliance();" CssClass="setChkSpace" /></td>
                                                                         </div>
                                                                     <td>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 95%; padding-bottom: 5px;">
                                                                            <asp:Label ID="lblActID" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                            <asp:Label ID="lblActName" runat="server" Style="display: none" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                            <div id="divAct" style="margin-top: 3px;" onclick="return DivClick1(this,<%# Eval("ID")%>)"></div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>

                                                    </div>
                                                </div>
                                                <div class="col-md-2 colpadding0" style="width: 19%">
                                                    <div id="complianceReturn"  runat="server">
                                                        <asp:Label Text="Compliance Name" runat="server" CssClass="hidden" />
                                                        <asp:DropDownListChosen ID="ddlComplianceName" Width="93%"  runat="server" DataPlaceHolder="Compliance" AllowSingleDeselect="false" CssClass="form-control" OnSelectedIndexChanged="ddlComplianceName_SelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownListChosen>
                                                    </div>
                                                      <div id="complianceRegisterChallan" runat="server" visible="false">
                                                         <asp:TextBox runat="server" ID="txtcompliance" Width="93%" Style="padding: 5px; margin: 0px;" CssClass="txtbox form-control" AutoComplete="off" placeholder="Compliances"/>
                                                    <div style="display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width: 258%;" id="dvcomplianceName" class="dvDeptHideshow form-control">
                                                        <asp:Repeater ID="rptCompliance" runat="server">
                                                            <HeaderTemplate>
                                                                <table class="detailstable FadeOutOnEdit record_table" id="RepeaterTableC">
                                                                    <tr>
                                                                        <td style="width: 100px;" colspan="2">
                                                                            <div id="divSelectAll">
                                                                                <asp:CheckBox ID="complianceSelectAll" Text="&nbsp;&nbsp;All Compliances" runat="server" onclick="checkAllcompliance(this);CallbtnGetFrequency();" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                               
                                                                    <tr>
                                                                    <td id="tdChkFiles" runat="server" style="width:100%;">                                                                       
                                                                        <div id="chkDivision"   onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666'">
                                                                            <asp:CheckBox ID="chkcompliance" Text='<%# Eval("Name")%>' runat="server"  onclick=" UncheckcomplianceHeader();CallbtnGetFrequency();" CssClass="setChkSpace"  />
                                                                         </div>
                                                                    </td>
                                                                    <td>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%; padding-bottom: 5px;">
                                                                            <asp:Label ID="lblcomplianceID" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                            <asp:Label ID="lblcomplianceName" runat="server" Style="display: none" Text='<%# Eval("Name")%>'  onclick='<%# string.Format("DivClick(this,\"{0}\");",Eval("ID")) %>' onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666'"  ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                           
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>

                                                    </div>
                                                      </div>
                                                </div>
                                                <div class="col-md-1 colpadding0" style="width: 13%">
                                                    <asp:Label Text="Period" runat="server" CssClass="hidden" />
                                                    <asp:TextBox runat="server" ID="txtPeriod1List" Width="91%" Style="padding: 5px; margin: 0px;" autocomplete="off" CssClass="txtbox form-control" placeholder="Periods"/>
                                                    <div style="display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width: 101%;" id="dvPeriod1" class="dvDeptHideshow form-control">
                                                        <asp:Repeater ID="rptPeriod1" runat="server">
                                                            <HeaderTemplate>
                                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTableP">
                                                                    <tr>
                                                                        <td style="width: 100px;" colspan="2">
                                                                            <asp:CheckBox ID="Period1SelectAll" Text="&nbsp;Select All" runat="server" onclick="checkAllPeriods1(this)" />
                                                                        </td>
                                                                    </tr>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="width: 100%;">
                                                                        <div id="chkDivPeriodOut"   onmouseover="this.style.color='#1fd9e1'" onmouseout="this.style.color='#666'">
                                                                            <asp:CheckBox ID="chkPeriod1" runat="server" Text='<%# Eval("Name")%>' onclick="UncheckPeriodHeader1();" CssClass="setChkSpace" /></td>
                                                                        </div>
                                                                    <td>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 92%; padding-bottom: 5px;">
                                                                            <asp:Label ID="lblPeriodID1" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                            <asp:Label ID="lblPeriodName1" runat="server"  ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <div class="col-md-1 colpadding0" style="width: 10%">
                                                    <asp:Label Text="Year" runat="server" CssClass="hidden" />

                                                    <asp:DropDownList runat="server" ID="ddlOutYear" Width="89%" class="form-control">
                                                        <asp:ListItem Text="Year" Value="0" />
                                                        <asp:ListItem Text="2020" Value="2020" />
                                                        <asp:ListItem Text="2019" Value="2019" />
                                                        <asp:ListItem Text="2018" Value="2018" />
                                                        <asp:ListItem Text="2017" Value="2017" />
                                                        <asp:ListItem Text="2016" Value="2016" />
                                                        <asp:ListItem Text="2015" Value="2015" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-3 colpadding0" style="width: 22%">
                                                    <asp:Label Text="Entity" runat="server" CssClass="hidden" />

                                                    <asp:TextBox runat="server" Width="92%" AutoComplete="off" ID="tbxOutputBranch"
                                                        Style="padding: 0px; padding-left: 5px; margin: 0px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" placeholder="Entity/State/Location/Branch"
                                                        CssClass="txtbox form-control" />
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10; width: 88%; margin-top: -20px;" id="divFilterLocationOutput">
                                                        <asp:TreeView runat="server" ID="tvOutputBranch" SelectedNodeStyle-Font-Bold="true" Width="112%" NodeStyle-ForeColor="#8e8e93" ShowCheckBoxes="All" onclick="OnTreeClick(event)"
                                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important; max-height: 300px;" ShowLines="true">
                                                        </asp:TreeView>
                                                        <div id="bindelement" style="background: white; height: 292px; display: none; width: 390px; border: 1px solid; overflow: auto;"></div>
                                                        <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation1_Click" Text="select" CssClass="hidden" />
                                                        <asp:Button ID="btnClear" Visible="true" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="hidden" />
                                                    </div>

                                                </div>

                                                <div class="col-md-1 colpadding0" style="width: 7%; margin-top: 0px;">

                                                    <asp:Button ID="btnOutSubmit" Text="Apply" runat="server" CssClass="btn btn-primary pull-right " style="width:81%;height: 32px;margin-top: 2px;padding-top: 2px;font-size: 15px;" OnClick="btnOutSubmit_Click" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 colpadding0 panel-body" style="max-height: 360px; overflow-y: auto;" id="divOutgrid">
                                                <div id="gridOutput">
                                                </div>
                                            </div>
                                        </div>

                                       
                                        <input id="btnGenerateOut" type="button" value="Generate" onclick="return GenerateOutclick();" style="display: none; margin-top: 10px; margin-bottom: 10px;" class="btn btn-primary pull-right" />
                                        <input id="btnProceedOut" type="button" value="Proceed" onclick="return ProceedOutclick();" style="display: none; margin-top: 10px; margin-bottom: 10px;" class="btn btn-primary pull-right" />
                                        <br />
                                        
                                       


                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </div>
                </div>

                    <!-- Modal -->
    <div class="modal fade" id="AlertModal" role="dialog" style="margin-top: 181px;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <label id="lbldocumentMessageAlert" style="color: black; font-size: large;"></label>
                            <label id="lblScheduleIds" class="hidden"></label>

                        </div>
                    </div>
                    <div class="row Top-M15">
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <button id="btnViewDocument" type="button" class="btn btn-primary">View Document</button>
                        </div>
                    </div>
                </div>
                <%-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>--%>
            </div>

        </div>
    </div>

    <div class="modal  modal-fullscreen" id="GenrateDocumentPageModal" role="dialog">
        <div class="modal-dialog modal-full" style="width: 100%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-primary close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <iframe id="iframePopupPage" class="col-md-12" style="height: 572px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

            </div>
        </div>
    </div>
    <script>
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function fCheckTree(obj) {
            var id = $(obj).attr('data-attr');
            var elm = $("#" + id);
            $(elm).trigger('click');
        }
        function FnSearch1() {

            var tree = document.getElementById('BodyContent_tvFilterLocation');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tvFilterLocation').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement1').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement1').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement1').show();
            } else {
                $('#bindelement1').html('');
                $('#bindelement1').hide();
                $(tree).show();
            }

        }
        function FnSearch() {

            var tree = document.getElementById('BodyContent_tvOutputBranch');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tvOutputBranch').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');
                            // var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  name="' + getId + 'CheckBox" id="' + getId + 'CheckBox"><a id="' + getId + '" href="' + $(anch).attr('href') + '" onclick="' + $(anch).attr('onclick') + '" >' + anch.html() + '</a></br>';
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement').show();
            } else {
                $('#bindelement').html('');
                $('#bindelement').hide();
                $(tree).show();
            }

        }
        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }
        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }
        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }
        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }
    </script>
    <style>
     #gridOutput
      {
        margin: 0;
        padding: 0;
        border-width:0;
        height:100%; /* DO NOT USE !important for setting the Grid height! */
      }
    </style>
</asp:Content>
