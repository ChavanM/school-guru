﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web.Security;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSLocation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyReports : System.Web.UI.Page
    {
        protected static int customerID;
        protected static int userID;

        protected static string showReportType;

        protected static string seriesData_GraphMonthlyCompliancePie;
        protected static string seriesData_GraphMonthlyComplianceColumn;
        protected static string seriesData_GraphMonthlyComplianceCategory;

        protected static string CompliedCountList = string.Empty;
        protected static string NotCompliedCountList = string.Empty;
        protected static string seriesData_GraphBoardingCompliancePie;
        protected static string seriesData_GraphBoardingComplianceColumn;
        protected static string seriesData_GraphBoardingComplianceCategory;

        protected static string Flag;
        protected static string[] cateboarding = new string[20];
        protected static int BID;

        public static List<long> Branchlist = new List<long>();
        protected List<KeyValuePair<int, string>> lstcategory = new List<KeyValuePair<int, string>>();
        public List<KeyValuePair<int, int>> lstBoardingSummary = new List<KeyValuePair<int, int>>();
        int Selectedbranch = 0;
        int Month = 0;
        int Year = 0;
        public RLCS_LocationDetails rlcslocationdetails = new RLCS_LocationDetails();

        protected static string Path;
        protected void Page_Load(object sender, EventArgs e)
        {
            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            userID = Convert.ToInt32(AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
                    userAssignedBranchList = rlcslocationdetails.GetuserAssignedBranchList(userAssignedBranchList);
                    if (userAssignedBranchList != null)
                    {
                        if (userAssignedBranchList.Count > 0)
                        {
                            List<int> assignedbranchIDs = new List<int>();
                            assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                            BindLocationFilter(assignedbranchIDs);
                            //BindLocationFilterMonthly(assignedbranchIDs);
                        }
                    }
                }
               
                ddlMonth.SelectedItem.Text = (ddlMonth.SelectedItem.Text);
                ddlYear.SelectedItem.Text = (ddlYear.SelectedItem.Text);
                showReportType = Request.QueryString["ReportType"];
                tvFilterLocation1_SelectedNodeChanged(null, null);
            }


            //if (!string.IsNullOrEmpty(Request.QueryString["Month"]) && !string.IsNullOrEmpty(Request.QueryString["Month"]))
            //{
            //    Month = Convert.ToInt32(Request.QueryString["Month"].ToString());
            //    Year = Convert.ToInt32(Request.QueryString["Year"].ToString());
            //    showReportType = "M";
            //}
           

            //BindLocationFilter();



            //BindYear();
            

            //tvFilterLocationMonthly_SelectedNodeChanged(null, null);
        }


        // public void BindYear()
        // {
        //     using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //     {
        //         ddlYear.SelectedIndex = -1;
        //         ListItem item = new ListItem("Select Year","-1");
        //         ddlYear.Items.Add(item);
        //         var years = entities.RLCS_Monthly_Compliance_Summary_Mapping.Where(x => x.AVACOM_CustomerID == customerID).Select(x => x.Year).Distinct().ToList();
        //         ddlYear.DataSource = years;
        //         ddlYear.DataBind();
        //    }
        //   //  var years 
        //}
        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(tvFilterLocation1.SelectedValue))
            {
                Selectedbranch = Convert.ToInt32(tvFilterLocation1.SelectedValue);
            }

            BindGraph();

            //if (!string.IsNullOrEmpty(showReportType) && showReportType == "B")
            //{
            //    Boarding.Visible = true;
            //    Compliances.Visible = false;
            //    GetBoardingComplianceSummary(Selectedbranch);
            //}
            //else
            //{
            //    Boarding.Visible = false;
            //    Compliances.Visible = true;
            //    Year = Convert.ToInt32(ddlYear.SelectedItem.Text);
            //    GetMonthlyComplianceSummary(Selectedbranch, Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedItem.Text));
            //}
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(tvFilterLocation1.SelectedValue))
            {
                Selectedbranch = Convert.ToInt32(tvFilterLocation1.SelectedValue);
            }

            BindGraph();

            //if (!string.IsNullOrEmpty(showReportType) && showReportType == "B")
            //{
            //    Boarding.Visible = true;
            //    Compliances.Visible = false;
            //    GetBoardingComplianceSummary(Selectedbranch);
            //}
            //else
            //{
            //    Boarding.Visible = false;
            //    Compliances.Visible = true;
            //    Year = Convert.ToInt32(ddlYear.SelectedItem.Text);
            //    GetMonthlyComplianceSummary(Selectedbranch, Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedItem.Text));
            //}
        }

        public void BindGraph()
        {
            if (!String.IsNullOrWhiteSpace(tvFilterLocation1.SelectedValue))
            {
                Selectedbranch = Convert.ToInt32(tvFilterLocation1.SelectedValue);
            }

            if (!string.IsNullOrEmpty(showReportType) && showReportType == "B")
            {
                Boarding.Visible = true;
                Compliances.Visible = false;

                GetBoardingComplianceSummary(Selectedbranch);
            }
            else
            {


                if (ddlYear.SelectedItem.Text != "--Select--" && ddlMonth.SelectedItem.Text != "--Select--")
                {
                    Boarding.Visible = false;
                    Compliances.Visible = true;
                    Year = Convert.ToInt32(ddlYear.SelectedItem.Text);
                    GetMonthlyComplianceSummary(Selectedbranch, Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
                }

                else
                {
                    seriesData_GraphMonthlyComplianceColumn = string.Empty;
                    seriesData_GraphBoardingCompliancePie = string.Empty;
                    seriesData_GraphBoardingComplianceCategory = string.Empty;
                    Compliances.Visible = true;
                    GetMonthlyComplianceSummary(Selectedbranch, Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script1", "clearMonthlySummary();", true);
                }

            }            
        }
        public void GetBoardingComplianceSummary(int selectedbranchID)
        {
            seriesData_GraphBoardingCompliancePie = string.Empty;
            seriesData_GraphBoardingComplianceColumn = string.Empty;
            seriesData_GraphMonthlyComplianceCategory = string.Empty;
            seriesData_GraphMonthlyComplianceColumn = string.Empty;
            //DivOnBoardingPie.Visible = true;
            //DivOnBoardingBar.Visible = true;
            string CompliedCountList = string.Empty;
            string NotCompliedCountList = string.Empty;
            string downloadFilePath = string.Empty;

            string rlcsAPIURL = ConfigurationSettings.AppSettings["RLCSAPIURL"] + "Masters/";
            string folderPath = ConfigurationSettings.AppSettings["RLCS_FileStorage_Path"];

            string commonPath = rlcsAPIURL + "GenerateZip?FilePath=" + folderPath + "\\RLCSDocuments\\HealthCheck\\";

            seriesData_GraphBoardingComplianceCategory = "xAxis: { categories: [";

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Pie = (entities.SP_GetOnBoardingHealthCheckSummary(customerID)).ToList();
                Pie = rlcslocationdetails.GetuserAssignedBranchList(Pie);
                if (Pie != null)
                {
                    Branchlist.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(selectedbranchID));
                    Branchlist.ToList();

                    if (Branchlist.Count > 0)
                        Pie = Pie.Where(row => Branchlist.Contains((long)row.AVACOM_BranchID) && row.AVACOM_BranchID != null).ToList();

                    List<int> lstComplied = new List<int>();
                    List<int> lstNotComplied = new List<int>();

                    List<string> lstCompliedData = new List<string>();
                    List<string> lstNotCompliedData = new List<string>();
                    // var  = Pie.Compliance[1];
                    int k = 0;
                    List<double> lstNotCompliedToProcess = new List<double>();
                    foreach (var i in Pie)
                    {
                        string name = i.SM_Name + '/' + i.Branch;
                        lstComplied.Add((int)i.CompliedNo);
                        lstNotComplied.Add((int)i.NotCompliedNo);

                        lstcategory.Add(new KeyValuePair<int, string>(0, name));
                        //cateboarding[k++] = name;
                        seriesData_GraphBoardingComplianceCategory += "'" + name + "',";

                        downloadFilePath = commonPath + i.ClientID + "\\" + i.SM_Name + "\\" + i.LocationName + "_" + i.Branch;

                        string CompliedData = "{ y:" + (int)i.CompliedNo + ",events: { click: function(e) { downloadMIS('" + downloadFilePath.Replace(@"\", "%5C") + "') }} },";
                        lstCompliedData.Add(CompliedData);
                        string NotCompliedData = "{ y:" + (int)i.NotCompliedNo + ",events: { click: function(e) { downloadMIS('" + downloadFilePath.Replace(@"\", "%5C") + "') }} },";
                        lstNotCompliedData.Add(NotCompliedData);
                    }

                    //seriesData_GraphMonthlyComplianceColumn += "{name:'Complied', color:'#64B973',  data:[" + lstCompliedData.ToString() + "]},";
                    //seriesData_GraphMonthlyComplianceColumn += "{name:'Not Complied', color:'#FF0000', data:[" + lstNotCompliedData.ToString() + "]},";

                    //seriesData_GraphBoardingComplianceColumn += "{name:'Complied', color:'#64B973',  data:[" + lstCompliedData.ToString() + "]},";
                    //seriesData_GraphBoardingComplianceColumn += "{name:'Not Complied', color:'#FF0000', data:[" + lstNotCompliedData.ToString() + "]},";

                    seriesData_GraphMonthlyComplianceColumn += "{name:'Complied', color:'#64B973',  data:[" + string.Join("", lstCompliedData) + "]},";
                    seriesData_GraphMonthlyComplianceColumn += "{name:'Not Complied', color:'#FF0000', data:[" + string.Join("", lstNotCompliedData) + "]},";

                    seriesData_GraphBoardingComplianceColumn += "{name:'Complied', color:'#64B973',  data:[" + string.Join("", lstCompliedData) + "]},";
                    seriesData_GraphBoardingComplianceColumn += "{name:'Not Complied', color:'#FF0000', data:[" + string.Join("", lstNotCompliedData) + "]},";

                    seriesData_GraphBoardingComplianceCategory = seriesData_GraphBoardingComplianceCategory.Trim(',') + "], labels: { rotation: -45, style: {fontSize: '10px' } }  } ,";
                    seriesData_GraphBoardingCompliancePie += "{name:'Complied', color:'#64B973',  y:" + lstComplied.Sum() + " },";
                    seriesData_GraphBoardingCompliancePie += "{name:'Not Complied', color:'#FF0000', y:" + lstNotComplied.Sum() + " },";

                    if (!string.IsNullOrEmpty(seriesData_GraphBoardingCompliancePie))
                    {
                        seriesData_GraphBoardingCompliancePie = seriesData_GraphBoardingCompliancePie.Trim(',');
                        // seriesData_GraphMonthlyComplianceCategory = seriesData_GraphMonthlyComplianceCategory.Trim(',');
                        seriesData_GraphMonthlyComplianceColumn = seriesData_GraphMonthlyComplianceColumn.Trim(',');
                    }
                    if (Pie.Count <= 0)
                    {
                        cvReportvalidation.IsValid = false;
                        cvReportvalidation.ErrorMessage = "No data available for current selection.";
                        seriesData_GraphMonthlyComplianceColumn = string.Empty;
                        seriesData_GraphBoardingCompliancePie = string.Empty;
                        seriesData_GraphBoardingComplianceCategory = string.Empty;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script1", "ClearOboradingSummary();", true);
                        //DivOnBoardingPie.Visible = false;
                        //DivOnBoardingBar.Visible = false;
                    }
                }
                else
                {
                    cvReportvalidation.IsValid = false;
                    cvReportvalidation.ErrorMessage = "No data available for current selection.";
                    seriesData_GraphMonthlyComplianceColumn = string.Empty;
                    seriesData_GraphBoardingCompliancePie = string.Empty;
                    seriesData_GraphBoardingComplianceCategory = string.Empty;
                    //DivOnBoardingPie.Visible = false;
                    //DivOnBoardingBar.Visible = false;
                }
            }
        }

        public void GetMonthlyComplianceSummary(int branchID, int month, int year)
        {
            seriesData_GraphMonthlyCompliancePie = string.Empty;
            seriesData_GraphMonthlyComplianceColumn = string.Empty;
            seriesData_GraphMonthlyComplianceCategory = string.Empty;

            string CompliedCountList = string.Empty;
            string NotCompliedCountList = string.Empty;
            string downloadFilePath = string.Empty;

            string rlcsAPIURL = ConfigurationSettings.AppSettings["RLCSAPIURL"] + "Masters/";
            //string folderPath = ConfigurationSettings.AppSettings["RLCS_FileStorage_Path"]; 
            //string commonPath = rlcsAPIURL + "GenerateZip?FilePath=" + folderPath + "\\RLCSDocuments\\MIS\\";

            string folderPath = ConfigurationSettings.AppSettings["RLCS_MIS_Path"];
            string commonPath = rlcsAPIURL + "GenerateZip?FilePath=" + folderPath + "\\MIS\\";

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Pie = (entities.SP_RLCS_GetMonthlyComplianceSummary(customerID)).Distinct().ToList();
                if(Pie != null)
                {
                    if(Pie.Count > 0)
                    {
                        Pie = rlcslocationdetails.GetuserAssignedBranchList(Pie);
                    }

                }
                if (Pie != null && month != 0 && year != 0)
                {
                    
                        Boarding.Visible = false;
                        Compliances.Visible = true;
                        if (Pie.Count > 0)
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
                        userAssignedBranchList = rlcslocationdetails.GetuserAssignedBranchList(userAssignedBranchList);
                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                                if (assignedbranchIDs.Count > 0)
                                    Pie = Pie.Where(row => row.AVACOM_BranchID != null && assignedbranchIDs.Contains((int)row.AVACOM_BranchID)).Distinct().ToList();

                                seriesData_GraphMonthlyComplianceCategory = "xAxis: { categories: [";

                                Branchlist.Clear();
                                GetAllHierarchy(customerID, Convert.ToInt32(branchID));
                                Branchlist.ToList();

                                // \RLCSDocuments\MIS\ClientID\Month_Year\State\Location_Branch

                                Pie = Pie.Where(x => x.Year == Convert.ToString(Year)).Where(row => row.Month == Convert.ToString(month)).Distinct().ToList();

                                if (Branchlist.Count > 0)
                                    Pie = Pie.Where(row => row.AVACOM_BranchID != null && Branchlist.Contains((long)row.AVACOM_BranchID)).Distinct().ToList();

                                bool flagNewPath = true;
                                if (year >= 2019)
                                    if (month >= 2)
                                        flagNewPath = false;

                                Pie.ForEach(eachRecord =>
                                {
                                    downloadFilePath = string.Empty;

                                    string strMonth = string.Empty;

                                    if (month < 10)
                                        strMonth = "0" + month;
                                    else if (month >= 10)
                                        strMonth = "" + month;

                                    downloadFilePath = commonPath + eachRecord.ClientID + "\\" + strMonth + "_" + year + "\\" + eachRecord.SM_Name + "\\" + eachRecord.LM_Name + "_" + eachRecord.Branch;

                                    //if (flagNewPath)                                    
                                    //    downloadFilePath = commonPath + eachRecord.ClientID + "\\" + strMonth + "_" + year + "\\" + eachRecord.SM_Name + "\\" + eachRecord.LM_Name + "_" + eachRecord.Branch;
                                    //else
                                    //    downloadFilePath = commonPath + eachRecord.ClientID + "\\" + strMonth + "_" + year + "\\BranchWise\\" + eachRecord.State + "\\" + eachRecord.Location;

                                    string name = eachRecord.SM_Name + '/' + eachRecord.Branch;
                                    seriesData_GraphMonthlyComplianceCategory += "'" + name + "',";

                                    var compliedCount = eachRecord.CompliedNo;
                                    var notCompliedCount = eachRecord.NotCompliedNo;
                                    //if (highCount > 0)                                                                                               
                                    CompliedCountList = CompliedCountList + "{ y:" + compliedCount + ",events: { click: function(e) { downloadMIS('" + downloadFilePath.Replace(@"\", "%5C") + "') }} },";
                                    //if (mediumCount > 0)
                                    NotCompliedCountList = NotCompliedCountList + "{ y:" + notCompliedCount + ",events: { click: function(e) { downloadMIS('" + downloadFilePath.Replace(@"\", "%5C") + "') }} },";
                                });

                                //CompliedCountList = "{ name: 'Complied', color: '#FF7473', data: [" + CompliedCountList.Trim(',') + "] },";
                                //NotCompliedCountList = "{ name: 'Not Complied', color: '#FFC952', data: [" + NotCompliedCountList.Trim(',') + "] },";
                                CompliedCountList = "{ name: 'Complied', color: '#64B973', data: [" + CompliedCountList.Trim(',') + "] },";
                                NotCompliedCountList = "{ name: 'Not Complied', color: '#FF0000', data: [" + NotCompliedCountList.Trim(',') + "] },";

                                seriesData_GraphMonthlyComplianceColumn = CompliedCountList + NotCompliedCountList.Trim(',');

                                seriesData_GraphMonthlyComplianceCategory = seriesData_GraphMonthlyComplianceCategory.Trim(',') + "], labels: { rotation: -45, style: {fontSize: '10px' } }  } ,";

                                //Pie
                                List<int> lstComplied = new List<int>();
                                List<int> lstNotComplied = new List<int>();
                                foreach (var i in Pie)
                                {
                                    lstComplied.Add((int)i.CompliedNo);
                                    lstNotComplied.Add((int)i.NotCompliedNo);
                                }

                                seriesData_GraphMonthlyCompliancePie += "{name:'Complied', color:'#64B973',  y:" + lstComplied.Sum() + "},";
                                seriesData_GraphMonthlyCompliancePie += "{name:'Not Complied', color:'#FF0000', y:" + lstNotComplied.Sum() + "},";

                                //seriesData_GraphMonthlyCompliancePie += "{name:'Complied', color:'#64B973',  y:" + lstComplied.Sum() + ",events: { click: function(e) { downloadMIS('" + downloadFilePath.Replace(@"\", "%5C") + "') }} },";
                                //seriesData_GraphMonthlyCompliancePie += "{name:'Not Complied', color:'#FF0000', y:" + lstNotComplied.Sum() + ", events: { click: function(e) { downloadMIS('" + downloadFilePath.Replace(@"\", "%5C") + "') }} },";

                                if (!string.IsNullOrEmpty(seriesData_GraphMonthlyCompliancePie))
                                {
                                    seriesData_GraphMonthlyCompliancePie = seriesData_GraphMonthlyCompliancePie.Trim(',');
                                    // seriesData_GraphMonthlyComplianceCategory = seriesData_GraphMonthlyComplianceCategory.Trim(',');
                                }
                                if (Pie.Count <= 0)
                                {
                                    cvReportvalidation.IsValid = false;
                                    cvReportvalidation.ErrorMessage = "No data available for current selection.";
                                    seriesData_GraphMonthlyCompliancePie = string.Empty;
                                    seriesData_GraphMonthlyComplianceColumn = string.Empty;
                                    seriesData_GraphMonthlyComplianceCategory = string.Empty;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script1", "clearMonthlySummary();", true);
                                    //DivMonthlyPia.Visible = false;
                                    //DivMonthlyBar.Visible = false;
                                }
                            }
                        }
                    }
                
                else
                {
                        //cvReportvalidation.IsValid = false;
                        //cvReportvalidation.ErrorMessage = "No data available for current selection.";
                        seriesData_GraphMonthlyCompliancePie = string.Empty;
                        seriesData_GraphMonthlyComplianceColumn = string.Empty;
                        seriesData_GraphMonthlyComplianceCategory = string.Empty;
                        Compliances.Visible = true;
                       ScriptManager.RegisterStartupScript(this, this.GetType(), "script1", "clearMonthlySummary();", true);

                }
              }
            }
        }

        //public void GetMonthlyComplianceSummary(int branchID, int month, int year)
        //{
        //    seriesData_GraphMonthlyCompliancePie = string.Empty;
        //    seriesData_GraphMonthlyComplianceColumn = string.Empty;
        //    seriesData_GraphMonthlyComplianceCategory = string.Empty;


        //    string downloadFilePath = string.Empty;

        //    string rlcsAPIURL = ConfigurationSettings.AppSettings["RLCSAPIURL"];
        //    string folderPath = ConfigurationSettings.AppSettings["RLCS_FileStorage_Path"];

        //    //  string commonPath = rlcsAPIURL + "Generatezip?FilePath=" + folderPath + "\\RLCSDocuments\\MIS\\";
        //    string commonPath = @"http://complianceapi.teamlease.com/api/Masters/GenerateZip?FilePath=\\tlstack\RLCSWEB\RLCSFiles\RLCSDocuments\MIS";
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var Pie = (entities.SP_RLCS_GetMonthlyComplianceSummary(customerID)).Distinct().ToList();

        //        if (Pie != null)
        //        {
        //            if (Pie.Count > 0)
        //            {
        //                seriesData_GraphMonthlyComplianceCategory = "xAxis: { categories: [";

        //                Branchlist.Clear();
        //                GetAllHierarchy(customerID, Convert.ToInt32(branchID));
        //                Branchlist.ToList();

        //                // \RLCSDocuments\MIS\ClientID\Month_Year\State\Location_Branch

        //                Pie = Pie.Where(x => x.Year==Convert.ToString(Year)).Where(row => row.Month==Convert.ToString(month)).Distinct().ToList();

        //                if (Branchlist.Count > 0)
        //                    Pie = Pie.Where(row => row.AVACOM_BranchID != null && Branchlist.Contains((long)row.AVACOM_BranchID)).Distinct().ToList();

        //                Pie.ForEach(eachRecord =>
        //                {
        //                    downloadFilePath = string.Empty;

        //                    downloadFilePath = @"http://complianceapi.teamlease.com/api/Masters/GenerateZip?FilePath=\\tlstack\RLCSWEB\RLCSFiles\RLCSDocuments\MIS\A4DRG\04_2018\Maharashtra\Mumbai_Uran"; //commonPath + eachRecord.ClientID + "\\" + month + "_" + year + "\\" + eachRecord.SM_Name + "\\" + eachRecord.LM_Name + "_" + eachRecord.Branch;

        //                    string name = eachRecord.SM_Name + '/' + eachRecord.Branch;
        //                    seriesData_GraphMonthlyComplianceCategory += "'" + name + "',";

        //                    var compliedCount = eachRecord.CompliedNo;
        //                    var notCompliedCount = eachRecord.NotCompliedNo;
        //                    //if (highCount > 0)                                                                                               
        //                    CompliedCountList = CompliedCountList + "{ y:" + compliedCount + ",events: { click: function(e) { downloadMIS('" + downloadFilePath + "') }} },";
        //                    //if (mediumCount > 0)
        //                    NotCompliedCountList = NotCompliedCountList + "{ y:" + notCompliedCount + ",events: { click: function(e) { downloadMIS('" + downloadFilePath + "') }} },";
        //                });

        //                 CompliedCountList = "{ name: 'Complied', color: '#64B973', data: [" + CompliedCountList.Trim(',') + "],events: { click: function(e) { downloadMIS('" + downloadFilePath + "') }} },";
        //                CompliedCountList += "{ name: 'Not Complied', color: '#FF0000', data: [" + NotCompliedCountList.Trim(',') + "],events: { click: function(e) { downloadMIS('" + downloadFilePath + "') }} },";

        //                seriesData_GraphMonthlyComplianceColumn = CompliedCountList + NotCompliedCountList.Trim(',');

        //                seriesData_GraphMonthlyComplianceCategory = seriesData_GraphMonthlyComplianceCategory.Trim(',') + "], labels: { rotation: -45, style: {fontSize: '10px' } }  } ,";

        //                //Pie
        //                List<int> lstComplied = new List<int>();
        //                List<int> lstNotComplied = new List<int>();
        //                foreach (var i in Pie)
        //                {
        //                    lstComplied.Add((int)i.CompliedNo);
        //                    lstNotComplied.Add((int)i.NotCompliedNo);
        //                }

        //                seriesData_GraphMonthlyCompliancePie = "{name:'Complied', color:'#64B973',  y:" + lstComplied.Sum() + " ,events: { click: function(e) { downloadMIS('" + downloadFilePath + "'); }}},";
        //                seriesData_GraphMonthlyCompliancePie += "{name:'Not Complied', color:'#FF0000', y:" + lstNotComplied.Sum() + " ,events: { click: function(e) { downloadMIS('" + downloadFilePath + "'); }}}";

        //                if (!string.IsNullOrEmpty(seriesData_GraphMonthlyCompliancePie))
        //                {
        //                    //seriesData_GraphMonthlyCompliancePie = seriesData_GraphMonthlyCompliancePie.Trim(',');
        //                   //seriesData_GraphMonthlyComplianceCategory = seriesData_GraphMonthlyComplianceCategory.Trim(',');
        //                }
        //            }
        //        }
        //    }
        //}

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocation1.Nodes.Clear();

                    string isstatutoryinternal = "";
                    //if (ddlStatus.SelectedValue == "Statutory")
                    //{
                    //if (ddlStatus.SelectedValue == "0")
                    //{
                    //    isstatutoryinternal = "S";
                    //}
                    //else if (ddlStatus.SelectedValue == "1")
                    //{
                    //    isstatutoryinternal = "I";
                    //}

                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation1.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation1.Nodes.Add(node);
                    }

                    tvFilterLocation1.CollapseAll();
                    //tvFilterLocation1_SelectedNodeChanged(null, null);
                    upDivLocation_Load(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilterMonthly(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocation1.Nodes.Clear();

                    string isstatutoryinternal = "";
                    //if (ddlStatus.SelectedValue == "Statutory")
                    //{
                    //if (ddlStatus.SelectedValue == "0")
                    //{
                    //isstatutoryinternal = "S";
                    //}
                    //else if (ddlStatus.SelectedValue == "1")
                    //{
                    //    isstatutoryinternal = "I";
                    //}
                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    //tvFilterLocationMonthly.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation1.Nodes.Add(node);
                        //tvFilterLocationMonthly.Nodes.Add(node);

                    }

                    //tvFilterLocationMonthly.CollapseAll();

                    //Comment By Rahul on 19 April 2018 Because remove filter from graphs
                    //BindLocationFilterFunction(bracnhes, LocationList); 
                    // BindLocationFilterPenalty(bracnhes, LocationList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void tvFilterLocation1_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                if (tvFilterLocation1.SelectedNode != null)
                {
                    tbxFilterLocation1.Text = tvFilterLocation1.SelectedNode.Text;
                    BindGraph();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {

        }
    }
}