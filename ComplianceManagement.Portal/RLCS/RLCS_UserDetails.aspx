﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_UserDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_UserDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <link href="~/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
                        
            if ($(window.parent.document).find("#divRLCSUserAddDialog").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#divRLCSUserAddDialog").children().first().hide();
        });

        function initializeJQueryUI() {
            $("#<%= tbxBranch.ClientID %>").unbind('click');

            $("#<%= tbxBranch.ClientID %>").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        }
        function ClosePopupWindowAfterSave() {
            setTimeout(function () {
                if (window.parent.$("#divAddEditUserDialog") != null && window.parent.$("#divAddEditUserDialog") != undefined)
                    window.parent.$("#divAddEditUserDialog").data("kendoWindow").close();
            }, 3000);
            setTimeout(function () {
                if (window.parent.$("#divRLCSUserAddDialog") != null && window.parent.$("#divRLCSUserAddDialog") != undefined)
                    window.parent.$("#divRLCSUserAddDialog").data("kendoWindow").close();
            }, 3000);
        }
      
    </script>
    <script type="text/javascript">
        function ValidateFilestatus() {
            var InvalidvalidFilesTypes = ["exe", "bat", "dll", "docx", "xlsx", "html", "css", "js", "txt", "doc", "gif", "jsp",
                "php5", "pht", "phtml", "shtml", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "zip", "rar", "php", "reg", "rdp"];
            var isValidFile = true;
            var lblerror = document.getElementById("<%=lblRErrormsg.ClientID%>");
            if (lblerror != null || lblerror != undefined) {
                var fuSampleFile = $("#<%=UserImageUpload.ClientID%>").get(0).files;
                for (var i = 0; i < fuSampleFile.length; i++) {
                    var fileExtension = fuSampleFile[i].name.split('.').pop();
                    if (InvalidvalidFilesTypes.indexOf(fileExtension) != -1) {
                        isValidFile = false;
                        break;
                    }
                }
                if (!isValidFile) {
                    lblerror.style.color = "red";
                    lblerror.innerHTML = "Invalid file extension. format not supported.";
                }
            }
            return isValidFile;
        }
    </script>
    <style>
        .form-control {
            margin-bottom: 8px;
            height: 31px;
            font-family: sans-serif;
        }

        .m-10 {
            /*margin-left: 10px;*/
            padding-left: 24px;
        }
    </style>
</head>
<body style="background: white;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="upUsers" runat="server" UpdateMode="Conditional" OnLoad="upUsers_Load">
            <ContentTemplate>
                <div class="row col-xs-12 col-sm-12 col-md-12">
                    <asp:ValidationSummary ID="vsUserDetails" runat="server" CssClass="alert alert-danger" ValidationGroup="UserValidationGroup" />
                    <asp:CustomValidator ID="cvEmailError" runat="server" EnableClientScript="False"
                        ErrorMessage="Email already exists." ValidationGroup="UserValidationGroup" Display="None" />
                </div>

                <div class="row" id="divCustomer" runat="server" style="margin-bottom: 5px; margin-top: 5px">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Customer</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:DropDownListChosen runat="server" ID="ddlCustomer" Width="100%"
                            CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Select Customer" ControlToValidate="ddlCustomer" 
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserValidationGroup" Display="None" />
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            First Name</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:TextBox runat="server" ID="tbxFirstName" CssClass="form-control"
                            MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Required First Name"
                            ControlToValidate="tbxFirstName" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid first name."
                            ControlToValidate="tbxFirstName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Last Name</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:TextBox runat="server" ID="tbxLastName" CssClass="form-control"
                            MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Required Last Name"
                            ControlToValidate="tbxLastName" runat="server" ValidationGroup="UserValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid last name."
                            ControlToValidate="tbxLastName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Designation</label>
                    </div>

                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:TextBox runat="server" ID="tbxDesignation" CssClass="form-control"
                            MaxLength="50" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Required Designation"
                            ControlToValidate="tbxDesignation" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid designation."
                            ControlToValidate="tbxDesignation" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Email</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:TextBox runat="server" ID="tbxEmail" CssClass="form-control" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Required Email"
                            ControlToValidate="tbxEmail" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid email."
                            ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Contact No</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:TextBox runat="server" ID="tbxContactNo" CssClass="form-control" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Required Contact Number"
                            ControlToValidate="tbxContactNo" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid contact number"
                            ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                        <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter only 10 digits"
                            ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Status</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:DropDownListChosen runat="server" ID="ddlStatus" CssClass="form-control" Width="100%" AllowSingleDeselect="false" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="Active" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="In-Active" Value="0"></asp:ListItem>
                        </asp:DropDownListChosen>                        
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row" runat="server" visible="false">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Department</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:DropDownListChosen runat="server" ID="ddlDepartment" CssClass="form-control" />
                        <asp:CheckBox runat="server" ID="chkHead" Visible="false" Style="padding: 0px; margin: 0px; height: 22px; width: 10px;" Text="Is Department Head" />
                    </div>
                </div>

                <div style="margin-bottom: 7px; display: none;" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            &nbsp;</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:RadioButtonList runat="server" ID="rblAuditRole" RepeatDirection="Horizontal"
                            RepeatLayout="Flow" OnSelectedIndexChanged="rblAuditRole_SelectedIndexChanged">
                            <asp:ListItem Text="Is Audit Head" Value="IAH" />
                            <asp:ListItem Text="Is Audit Manager" Value="IAM" />
                        </asp:RadioButtonList>
                    </div>
                </div>

                <div runat="server" id="divComplianceRole" style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Role</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:DropDownListChosen runat="server" ID="ddlRole" CssClass="form-control" Width="100%"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Select Role" ControlToValidate="ddlRole"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserValidationGroup"
                            Display="None" />
                    </div>
                </div>

                <div runat="server" id="divRiskRole" style="margin-bottom: 7px" class="row" visible="false">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Risk Role</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:DropDownListChosen runat="server" ID="ddlRiskRole" Width="100%"
                            CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlRiskRole_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Select Role" ControlToValidate="ddlRiskRole"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserValidationGroup"
                            Display="None" />
                    </div>
                </div>

                <div runat="server" id="Auditor1" style="margin-bottom: 7px" visible="false" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Auditor Login Start Date:
                        </label>
                    </div>

                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:TextBox runat="server" ID="txtStartDate" />
                    </div>

                    <label style="display: block; font-size: 13px; color: #333;">
                        End Date:</label>
                                <asp:TextBox runat="server" ID="txtEndDate" />
                    <asp:RequiredFieldValidator ID="reqAudit1" Visible="false" ErrorMessage="Auditor login start date can not be empty."
                        ControlToValidate="txtStartDate" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                    <asp:RequiredFieldValidator ID="reqAudit2" Visible="false" ErrorMessage="Auditor login end date can not be empty."
                        ControlToValidate="txtEndDate" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                </div>

                <div runat="server" id="Auditor2" style="margin-bottom: 7px" visible="false">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="display: block; float: left; font-size: 13px; color: #333;">
                        Auditor Period Start Date:
                                <asp:TextBox runat="server" ID="txtperiodStartDate" CssClass="form-control" Style="height: 16px; width: 180px;" />
                    </label>

                    <label style="display: block; font-size: 13px; color: #333;">
                        End Date:
                                <asp:TextBox runat="server" ID="txtperiodEndDate" Style="height: 16px; width: 180px;" /></label>
                    <asp:RequiredFieldValidator ID="reqAudit3" Visible="false" ErrorMessage="Auditor Period start date can not be empty."
                        ControlToValidate="txtperiodStartDate" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                    <asp:RequiredFieldValidator ID="reqAudit4" Visible="false" ErrorMessage="Auditor Period end date can not be empty."
                        ControlToValidate="txtperiodEndDate" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                </div>

                <div runat="server" id="divCustomerBranch" style="margin-bottom: 7px" class="row" visible="false">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Location</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:TextBox runat="server" ID="tbxBranch" CssClass="form-control" />
                        <div style="margin-left: 150px; position: absolute; z-index: 10" id="divBranches">
                            <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                                Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please select Location."
                            ControlToValidate="tbxBranch" runat="server" ValidationGroup="UserValidationGroup" InitialValue="< Select Location >"
                            Display="None" />
                    </div>
                </div>

                <div runat="server" id="divReportingTo" style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Reporting to</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:DropDownListChosen runat="server" ID="ddlReportingTo"  Width="100%" CssClass="form-control" />
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="control-label">Address</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:TextBox runat="server" ID="tbxAddress" CssClass="form-control" TextMode="MultiLine" />
                    </div>
                </div>

                <%--  <asp:UpdatePanel ID="upImage" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>--%>
                <div style="margin-bottom: 7px; display: none;" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Profile Picture</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:FileUpload ID="UserImageUpload" runat="server" />

                        <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="Upload" Visible="false" />
                    </div>
                </div>

                <div style="margin-bottom: 7px" class="row">
                    <asp:Label ID="lblRErrormsg" class="alert alert-block alert-danger fade in" runat="server" Visible="false"></asp:Label>
                </div>

                <div style="margin-bottom: 7px" runat="server" visible="false">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            &nbsp;
                        </label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:Image ID="ImageShow" runat="server" Height="100" Width="100" ImageUrl="~/UserPhotos/DefaultImage.png" Visible="false" />
                    </div>
                </div>

                <asp:Repeater runat="server" ID="repParameters">
                    <ItemTemplate>
                        <div style="margin-bottom: 7px">
                            <asp:Label runat="server" ID="lblName" Style="width: 150px; display: block; float: left; font-size: 13px; color: #333;"
                                Text='<%# Eval("Name")  + ":"%>' />
                            <asp:TextBox runat="server" ID="tbxValue" Style="height: 20px; width: 390px;" Text='<%# Eval("Value") %>'
                                MaxLength='<%# Eval ("Length") %>' />
                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ValueID") %>' />
                            <asp:HiddenField runat="server" ID="hdnEntityParameterID" Value='<%# Eval("ParameterID") %>' />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

                <div class="row col-md-12 colpadding0 text-center">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                        CausesValidation="true" ValidationGroup="UserValidationGroup" />
                </div>

                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                </div>
            </ContentTemplate>
            <Triggers>
                <%--<asp:PostBackTrigger ControlID="btnSave" />--%>
                <%--<asp:AsyncPostBackTrigger ControlID="btnUpload" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
