﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class ComplianceOverdueSummary : System.Web.UI.Page
    {
        protected static List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> mstRecords;
        public static string CompDocReviewPath = "";
        protected string CId;
        protected string UserId;
        protected int CustId;
        protected int UId;
        protected List<Int32> roles;
        protected int RoleID;
        protected int UserRoleID;
        protected int StatusFlagID;
        protected int RoleFlag;
        protected string Flag;
        protected bool DisableFalg;
        protected string Path;
        protected int isapprover;
        protected string CustomerName;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (AuthenticationHelper.Role.Equals("HMGR") )
                {
                    isapprover = 0;
                    Flag = "HMGR";
                }
               
                if (AuthenticationHelper.Role == "HMGR" )
                {
                    Path = ConfigurationManager.AppSettings["KendoPathApp"];
                    CId = Convert.ToString(AuthenticationHelper.CustomerID);
                    UserId = Convert.ToString(AuthenticationHelper.UserID);
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
                    RoleFlag = 0;
                    CustomerName = CustomerManagement.CustomerGetByIDName(CustId);
                }
            }
            BindGrid();
        }

        private void BindGrid()
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                string roleCode = string.Empty;
                roleCode = AuthenticationHelper.Role;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_AssignedCustomers(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role);

                    var masterRecords = (entities.SP_RLCS_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, customerID, distributorID, serviceProviderID, roleCode)
                                       .Where(entry => entry.IsActive == true
                                           && entry.IsUpcomingNotDeleted == true
                                           && lstMyAssignedCustomers.Select(row => row.CustomerID).Contains(entry.CustomerID))).Take(2).ToList();

                    if (masterRecords.Count > 0)
                    {
                        mstRecords = masterRecords;
                    }

                    Session["TotalRows"] = masterRecords.Count;
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        [System.Web.Services.WebMethod]
        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> GetHRMyCompliances()
        {
            List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> record = null;
            record = mstRecords;
            return record;
        }
    }
    }
