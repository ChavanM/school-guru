﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Web.Services;
using System.Net.Http;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_HRMGRDashboard : System.Web.UI.Page
    {
        protected static string IsSatutoryInternal;
        protected static bool IsApprover = false;
        protected static int customerID;
        protected static int BID;

        static string tlConnect_url = ConfigurationManager.AppSettings["TLConnect_API_URL"].ToString();
        static string tlConnect_url_Live = ConfigurationManager.AppSettings["TLConnect_API_URL_Live"].ToString();

        protected static string seriesData_GraphMonthlyComplianceColumn;
        protected static string seriesData_GraphMonthlyCompliancePie;
        protected static string complianceSummaryMonth;

        protected static string data_areas;

        protected static string profileID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                home.Value = "true";
                                              
                bool apiAccessible = RLCSAPIClasses.TestWebAPIConnection(tlConnect_url + "Dashboard/GetMarqueeSectionDueDate?Type=GetSectionDueDate");

                profileID = "8IADVYI5JWQL0IC";

                //if (apiAccessible)
                //{
                    GetTabCount(tlConnect_url);

                    GetActivityStatusCount(tlConnect_url);

                    GetMonthlyCompliance_ColumnChart(tlConnect_url_Live);

                    GetMonthlyCompliance_PieChart(tlConnect_url_Live);

                    GetPanIndiaCompliance_MAP(tlConnect_url);

                    GetInvoiceCount(tlConnect_url);

                    //GetTopMorqueeCount_HttpClient(tlConnect_url);
                //}
            }
        }

        public void GetTabCount(string requestUrl)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //Users
                    var usercount = (from row in entities.Users
                                     where row.CustomerID == AuthenticationHelper.CustomerID
                                     && row.IsActive == true
                                     && row.IsDeleted == false
                                     select row.ID).ToList();

                    if (usercount.Count > 0)
                    {
                        divUsersCount.InnerText = Convert.ToString(usercount.Count);
                    }

                    //Entities
                    var entityCount = (from row in entities.CustomerBranches
                                       where row.CustomerID == AuthenticationHelper.CustomerID
                                       && row.ParentID == null
                                       select row).Distinct().Count();

                    divEntitesCount.InnerText = Convert.ToString(entityCount);

                    //
                    requestUrl += "Dashboard/GetProfile_BasicInformationByProfileID?profileID=" + profileID;

                    string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var _objProfileInfo = JsonConvert.DeserializeObject<Profile_BasicInfo>(responseData);

                        if (_objProfileInfo != null)
                        {
                            if (_objProfileInfo.lstBasicInfrmation[0] != null)
                            {
                                divEmployeeCount.InnerText = Convert.ToString(_objProfileInfo.lstBasicInfrmation[0].Employees);
                                divLocationCount.InnerText = Convert.ToString(_objProfileInfo.lstBasicInfrmation[0].Branches);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetActivityStatusCount(string requestUrl)
        {
            try
            {
                requestUrl += "Dashboard/GetClientDashboardDetails?ClientId=DUMGD0RG&profileID=" + profileID;

                string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

                if (!string.IsNullOrEmpty(responseData))
                {
                    var _objActivityStatusCount = JsonConvert.DeserializeObject<List<DashboardCount>>(responseData);

                    if (_objActivityStatusCount != null)
                    {
                        if (_objActivityStatusCount[0] != null)
                        {
                            divDueTodayCount.InnerText = Convert.ToString(_objActivityStatusCount[0].DueTodayCount);
                            divUpcomingCount.InnerText = Convert.ToString(_objActivityStatusCount[0].UpcomingCount);

                            divOverdueCount.InnerText = Convert.ToString(_objActivityStatusCount[0].OverDueCount);
                            divHighRiskCount.InnerText = Convert.ToString(_objActivityStatusCount[0].HighRiskCount);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetMonthlyCompliance_ColumnChart(string requestUrl)
        {
            seriesData_GraphMonthlyComplianceColumn = string.Empty;

            requestUrl += "Dashboard/GetMonthWiseComplianceByProfileID?profileID=" + profileID + "&Year=2018";

            string responseData = RLCSAPIClasses.Invoke_Live("GET", requestUrl, "");
            //string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

            if (!string.IsNullOrEmpty(responseData))
            {
                var _objMonthlyComplianceStatus = JsonConvert.DeserializeObject<MonthlyComplianceColumnChart>(responseData);

                if (_objMonthlyComplianceStatus != null)
                {
                    var lstComplied = _objMonthlyComplianceStatus.Compliance[0];
                    var lstNotComplied = _objMonthlyComplianceStatus.Compliance[1];

                    List<double> lstCompliedToProcess = new List<double>();
                    List<double> lstNotCompliedToProcess = new List<double>();

                    if (lstComplied.Count == lstNotComplied.Count)
                    {
                        for (int i = 0; i < lstComplied.Count; i++)
                        {
                            if (lstComplied[i] != 0 || lstNotComplied[i] != 0)
                            {
                                lstCompliedToProcess.Add(lstComplied[i]);
                                lstNotCompliedToProcess.Add(lstNotComplied[i]);
                            }
                        }
                    }

                    seriesData_GraphMonthlyComplianceColumn += "{name:'Complied', color:'#FFE26B',  data:[" + string.Join(",", lstCompliedToProcess) + "]},";
                    seriesData_GraphMonthlyComplianceColumn += "{name:'Not Complied', color:'#FF7473', data:[" + string.Join(",", lstNotCompliedToProcess) + "]},";
                }

                if (!string.IsNullOrEmpty(seriesData_GraphMonthlyComplianceColumn))
                {
                    seriesData_GraphMonthlyComplianceColumn = seriesData_GraphMonthlyComplianceColumn.Trim(',');
                }
            }
        }

        public void GetMonthlyCompliance_PieChart(string requestUrl)
        {
            seriesData_GraphMonthlyCompliancePie = string.Empty;

            profileID = "HVZCE4TNUGIZFYX";

            requestUrl += "Dashboard/GetClientComplianceByProfileID?ClientId=DUMGD0RG&profileID=" + profileID;

            string responseData = RLCSAPIClasses.Invoke_Live("GET", requestUrl, "");

            if (!string.IsNullOrEmpty(responseData))
            {
                var _objMonthlyComplianceStatus = JsonConvert.DeserializeObject<MonthlyCompliancePieChart>(responseData);

                if (_objMonthlyComplianceStatus != null)
                {
                    var lstComplianceStatus = _objMonthlyComplianceStatus.ComplianceStatus;

                    complianceSummaryMonth = _objMonthlyComplianceStatus.FooterLabel;

                    seriesData_GraphMonthlyCompliancePie += "{name:'Complied', color:'#FFE26B',  y:" + lstComplianceStatus[0] + ", events: { click: function(e) { ShowGraphDetails('LCS',''); } } },";
                    seriesData_GraphMonthlyCompliancePie += "{name:'Not Complied', color:'#FF7473', y:" + lstComplianceStatus[1] + ", events: { click: function(e) { ShowGraphDetails('LCS',''); } } },";
                }

                if (!string.IsNullOrEmpty(seriesData_GraphMonthlyCompliancePie))
                {
                    seriesData_GraphMonthlyCompliancePie = seriesData_GraphMonthlyCompliancePie.Trim(',');
                }
            }
        }

        public void GetPanIndiaCompliance_MAP(string requestUrl)
        {
            data_areas = string.Empty;

            requestUrl += "Dashboard/GetPanIndiaIdByProfileID?profileID=" + profileID;

            string responseData = RLCSAPIClasses.Invoke_Live("GET", requestUrl, "");
            //string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

            if (!string.IsNullOrEmpty(responseData))
            {

                var lstPanIndiaStateDetails = JsonConvert.DeserializeObject<List<PanIndiaState>>(responseData);

                if (lstPanIndiaStateDetails != null)
                {
                    if (lstPanIndiaStateDetails.Count > 0)
                    {
                        foreach (var eachState in lstPanIndiaStateDetails)
                        {
                            //{ "id": $(this).attr('id'), "color": "#FC4C4C" }
                            data_areas += "{id:'" + eachState.id + "', color:'#FF7473'},";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(data_areas))
                {
                    data_areas = "[" + data_areas.Trim(',') + "]";
                }
            }
        }

        [WebMethod]
        public static List<ActDetail> GetApplicableActs()
        {
            List<ActDetail> lstActDetail = new List<ActDetail>();

            string requestUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"].ToString();
            requestUrl += "Dashboard/GetRLCSActNameByProfileID?profileID=HVZCE4TNUGIZFYX";

            string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

            if (!string.IsNullOrEmpty(responseData))
            {
                var lstActDetail_Response = JsonConvert.DeserializeObject<List<ActDetail>>(responseData);

                if (lstActDetail_Response != null)
                {
                    if (lstActDetail_Response.Count > 0)
                    {
                        lstActDetail = lstActDetail_Response;
                    }
                }
            }

            return lstActDetail;
        }

        public void GetInvoiceCount(string requestUrl)
        {
            try
            {
                requestUrl += "Dashboard/GetInvoiceDetails?profileID=" + profileID;

                string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

                if (!string.IsNullOrEmpty(responseData))
                {

                    var lstInvoices = JsonConvert.DeserializeObject<List<InvoiceDetail>>(responseData);

                    if (lstInvoices != null)
                    {
                        if (lstInvoices[0] != null)
                        {
                            currentMonthInvoiceCount.InnerText = Convert.ToString(lstInvoices[0].Current_Month_Invoice);
                            outstandingInvoiceCount.InnerText = Convert.ToString(lstInvoices[0].OutStanding_Invoice);
                            totalInvoiceCount.InnerText = Convert.ToString(lstInvoices[0].Total_Invoice);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetTopMorqueeCount_HttpClient(string requestUrl)
        {
            try
            {
                HttpClient client = new HttpClient();

                requestUrl = "http://tlconnectuatapi.teamlease.com/api/Dashboard/GetMarqueeSectionDueDate?Type=GetSectionDueDate";

                string responseData = string.Empty;

                //HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(requestUrl);

                //http.Headers.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
                //http.Headers.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
                client.DefaultRequestHeaders.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");

                HttpResponseMessage response;

                //Execute the REST API call.
                response = client.GetAsync(requestUrl).Result;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    responseData = response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetTopMorqueeCount(string requestUrl)
        {
            try
            {
                requestUrl = "http://tlconnectuatapi.teamlease.com/api/Dashboard/GetMarqueeSectionDueDate?Type=GetSectionDueDate";

                string responseData = string.Empty;

                HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(requestUrl);

                http.Headers.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
                http.Headers.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");

                HttpWebResponse response = (HttpWebResponse)http.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader responseReader = new StreamReader(response.GetResponseStream()))
                    {
                        responseData = responseReader.ReadToEnd();


                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}