
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS

    
{
    public partial class RLCS_Designation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
        }

        protected void upDesignation_Load(object sender, EventArgs e)
        {
           
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            var designation = tbxDesignation.Text;
            var status="I";
            if (ddlStatus.Text == "1") status = "A";  

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                com.VirtuosoITech.ComplianceManagement.Business.Data.RLCS_Designation_Master entity
                    = new com.VirtuosoITech.ComplianceManagement.Business.Data.RLCS_Designation_Master();
                entity.Long_designation = designation;
                entity.Status = status;
                entity.CreatedOn = DateTime.Now;

                var existingDesignationCount = entities.RLCS_Designation_Master.Count(a => a.Long_designation == designation);
                
                if (existingDesignationCount == 0)
                {
                    submit.IsValid = false;
                    btn_Save.Enabled = false;
                    entities.RLCS_Designation_Master.Add(entity);
                    entities.SaveChanges();
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);
                } else
                {
                    designation_Exist.IsValid = false;
                }
            }
            
        }

        protected void btn_Cancle_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);
        }

    
    }
}