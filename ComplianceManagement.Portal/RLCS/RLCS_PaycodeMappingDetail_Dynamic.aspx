﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_PaycodeMappingDetail_Dynamic.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_PaycodeMappingDetail_Dynamic" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>
    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
        function numeric(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode == 8 || unicode == 9 || (unicode >= 48 && unicode <= 57)) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <script>

   
        $(document).ready(function () {
            $('#divRLCSHeaderUploadDialogBulk').hide();
        });
        function OpenHeaderPopup() {
            $("#hiddenData").click();
                $('#divRLCSHeaderUploadDialogBulk').show();
                var myWindowAdv = $("#divRLCSHeaderUploadDialogBulk");
                myWindowAdv.kendoWindow({
                    width: "70%",
                    height: '50%',

                    content: "../RLCS/RLCS_PayCodeHeaderBulkUpload.aspx",
                    iframe: true,
                    title: "Upload",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose,
                    open: onOpen,
                    scrollable: false
                });

                myWindowAdv.data("kendoWindow").center().open();
                return false;
        }


        function onOpen(e) {
            kendo.ui.progress(e.sender.element, false);
           
        }

        function onClose() {
            debugger;
            var btnExcel = $('#btnExcel');
            btnExcel.click();
            //$('#gvPayCode').data('kendoGrid').dataSource.read();

        }

        $(document).on("click", "#btnRemove", function (e) {
            //debugger
            //var item = $("#gridPaycode").data("kendoGrid").dataItem($(this).closest("tr"));
            //var recordID = item.ID;
            //var clientID = item.CPMD_ClientID;
            //if (item != null && item != undefined) {

            //    window.kendoCustomConfirm("Are you certain you want to delete this Paycode?", "Confirm").then(function () {
            //        debugger
            //        DeletePaycodeDetails(item)
            //    }, function () {
            //        // For Cancel
            //    });
            //}
        }
       );

    </script>
    <style>
        .form-control {
            margin-bottom: 8px;
            height: 31px;
            font-family: sans-serif;
        }

        .button, input, select, textarea {
            border:none;
        }

        .m-10 {
            padding-left: 24px;
        }

        html, body {
            overflow-x: hidden;
        }
        
    </style>


</head>
    
<body style="background-color: white; ">

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div class="col-xs-12 col-sm-12 col-md-12 colpadding0">
            <div class="col-md-4">
                <asp:DropDownList runat="server" ID="ddlClientList" Style="padding: 0px; margin: 0px;"
                    CssClass="form-control" NoResultsText="No results match." />
            </div>
             <div  class="col-md-4"></div>
             <div class="col-md-2">
                 <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_Click" Style="display: none" />
              </div>
                <div class="col-md-2" id="divBtnUploadHeader" runat="server">
                <button id="btnUploadHeader" class="btn btn-primary"  onclick="return OpenHeaderPopup();"><span class="k-icon k-i-upload"></span>Upload</button>
                 </div>


            <%--<table>
                <tr>
                    <th style="width:100px; margin:10px" >
                       Header Name
                    </th>

                    <th>
                       Paycode Type	
                    </th>
                    <th>
                       Paycode
                    </th>
                    <th>
                       Squance<br /> Order
                    </th>
                    <th>
                       ESI<br /> Applicable	
                    </th>
                    <th>
                       PF<br /> Applicable	
                    </th>
                    <th>
                       PT<br /> Applicable	
                    </th>
                    <th>
                       LWF <br />Applicable
                    </th>
                </tr>
            </table>--%>
            </div>
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="form-group clearfix"></div>

                <div style="width: 100%; margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                  <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="DocGenValidationGroup" />
                                       
                                        <asp:CustomValidator ID="cvDuplicate" runat="server" EnableClientScript="False"
                                            ValidationGroup="DocGenValidationGroup" Display="None" />
                </div>


                <div class="row" style="width: 90%;  margin-left:30px; position:fixed">
                    <div class="col-lg-12 col-md-12 col-sm-12" style="margin: -35px;">
                        <asp:Label ID="lblMessage" Font-Size="15px" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblPaycodeHeader" Font-Size="15px" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblPaycodeSequence" Font-Size="15px" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblPaycodeType" Font-Size="15px" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblPaycodeAndType" Font-Size="15px" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblHeader" Font-Size="15px" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>
                <div class="form-group clearfix"></div>
                <asp:GridView Width="85%" Height="70%" ID="gvPayCode" HorizontalAlign="Center" runat="server" ShowFooter="true" AutoGenerateColumns="false" OnRowCreated="gvPayCode_RowCreated">
                    <Columns>
                        <asp:TemplateField HeaderText="Header Name" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:RequiredFieldValidator ID="ddlHeaderRequired" runat="server" ControlToValidate="txtHeader" ForeColor="Red" ValidationGroup="valid1" InitialValue="" ErrorMessage="Select Header.">
                                </asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtHeader" CssClass="form-control" Width="130px" OnTextChanged="txtHeader_TextChanged" AutoPostBack="true" runat="server"></asp:TextBox>
                                
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="130px" />
                            <ItemStyle HorizontalAlign="Center" Width="130px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Paycode Type" >
                            <ItemTemplate>
                                <asp:RequiredFieldValidator ID="ddlPaycodeRequired" runat="server" ControlToValidate="ddlPaycodeType" ForeColor="Red" ValidationGroup="valid1" InitialValue="-1" ErrorMessage="Select Paycode type.">
                                </asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddlPaycodeType" CssClass="form-control" runat="server" Width="140px" 
                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddlPaycodeType_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="-1">Select</asp:ListItem>
                                </asp:DropDownList>
                                
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="90px" />
                            <ItemStyle HorizontalAlign="Center" Width="90px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Paycode">
                            <ItemTemplate>
                                <asp:RequiredFieldValidator ID="ddlPaycodeRequired1" runat="server" ControlToValidate="ddlPaycode" ValidationGroup="valid1" ForeColor="Red" InitialValue="-1" ErrorMessage="Please select Paycode.">
                                </asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddlPaycode" OnSelectedIndexChanged="ddlPaycode_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server" Width="140px">
                                    <asp:ListItem Value="-1">Select</asp:ListItem>
                                </asp:DropDownList>
                                
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="140px" />
                            <ItemStyle HorizontalAlign="Center" Width="140px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Squance Order">

                            <ItemTemplate>
                                <asp:RequiredFieldValidator ID="txtSquanceOrderRequired" runat="server" ControlToValidate="txtSquanceOrder" ValidationGroup="valid1" ForeColor="Red" InitialValue="" ErrorMessage="*Required">
                                </asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtSquanceOrder" CssClass="form-control" Width="50px" runat="server" MaxLength="5" onkeypress="return numeric(event);" OnTextChanged="txtSquanceOrder_TextChanged" AutoPostBack="true"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ESI Applicable">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckboxESI" runat="server" AppendDataBoundItems="true" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PF Applicable">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckboxPF" runat="server" AppendDataBoundItems="true"/>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PT Applicable">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckboxPT" runat="server" AppendDataBoundItems="true"/>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="LWF Applicable">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckboxLWF" runat="server" AppendDataBoundItems="true" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />

                            <FooterStyle HorizontalAlign="Center" Width="50px" />
                            <FooterTemplate>
                                <asp:ImageButton ID="btnAdd" class="btn btn-primary" ValidationGroup="valid1" runat="server" ImageUrl="~/img/Add.png" BackColor="White" ToolTip="Add New" OnClick="btnAdd_Click" Style="width:40px;" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnRemove" class="btn btn-primary" runat="server" Text="-" OnClick="btnRemove_Click" Style="width:40px;" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>--%>
                    </Columns>
                </asp:GridView>
                 <div class="col-xs-12 col-sm-12 col-md-12 colpadding0" style="margin-left: 918px; width:50px; margin-top: 10px; align-items:flex-end">
                     
                    <asp:Button ID="btnSave" ValidationGroup="valid1" runat="server" class="btn btn-primary"  Text="Save" OnClick="btnSave_Click"/>
                     <asp:Button ID="btnUpdate" runat="server" class="btn btn-primary" Text="Save" OnClick="btnUpdate_Click" Visible="false"/>
                    
                  </div>
                </div>
                <asp:Button type="button" class="btn btn-primary" id="hiddenData" style="display: none" onclick="btnHidden_Click" runat="server"></asp:Button>
            </ContentTemplate>
            <Triggers>
                <%--<asp:PostBackTrigger ControlID="btnSave" />--%>
                <asp:PostBackTrigger ControlID="btnExcel" />
                <asp:AsyncPostBackTrigger ControlID="hiddenData" EventName="Click"/>
            </Triggers>
        </asp:UpdatePanel>
        <div id="divRLCSHeaderUploadDialogBulk">
            <iframe id="iframeUploadBulk" style="width: 878px; height: 500px; border: none"></iframe>
        </div>
    </form>
</body>
   
</html>
