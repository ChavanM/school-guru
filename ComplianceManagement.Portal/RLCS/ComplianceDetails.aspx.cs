﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class ComplianceDetails : System.Web.UI.Page
    {
        protected static int customerid;
        protected static string ComplianceTypeFlag;
        protected static bool IsApprover = false;
        protected static int BID;
        int ActId = -1;
        public static List<long> Branchlist = new List<long>();
        public RLCS_LocationDetails rlcslocationdetails = new RLCS_LocationDetails();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerid, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
                        userAssignedBranchList = rlcslocationdetails.GetuserAssignedBranchList(userAssignedBranchList);
                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                                {
                                    BID = Convert.ToInt32(Request.QueryString["branchid"]);

                                }
                                BindLocationFilter(assignedbranchIDs);
                                BindActList(Branchlist);
                                BindDetailView();
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindActList(List<long> blist)
        {
            try
            {
                int CategoryID = -1;
                bool IsAvantisFlag = false;
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (ddlAct.SelectedValue != "-1" && ddlAct.SelectedValue != "")
                {
                    ActId = Convert.ToInt32(ddlAct.SelectedValue);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsAvantisFlag"]))
                {
                    IsAvantisFlag = Convert.ToBoolean(Request.QueryString["IsAvantisFlag"]);
                }
                var ActList = ActManagement.GetAllAssignedActs_RLCS(customerid, AuthenticationHelper.UserID, CategoryID, AuthenticationHelper.ProfileID, blist, IsAvantisFlag);
                ddlAct.Items.Clear();
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                tvFilterLocation.Nodes.Clear();
                string isstatutoryinternal = "";

                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ComplianceTypeFlag == "Internal")
                {
                    isstatutoryinternal = "I";
                }

                //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    //BindBranchesHierarchy(node, item);
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindDetailView()
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;

                int RoleID = 3;
                int CustomerBranchId = -1;

                int CategoryID = -1;
                int UserID = -1;
                bool IsAvantisFlag = false;

                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UserID"]))
                {
                    UserID = Convert.ToInt32(Request.QueryString["UserID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsAvantisFlag"]))
                {
                    IsAvantisFlag = Convert.ToBoolean(Request.QueryString["IsAvantisFlag"]);
                }
                if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                {
                    //ddlAct.ClearSelection();
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    Branchlist.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    Branchlist.ToList();
                }
                else
                {
                    Branchlist.Clear();
                }
                //if (ddlAct.SelectedValue == "-1" || ddlAct.SelectedValue == "")
                //{
                //    BindActList(Branchlist);
                //}
                if (!string.IsNullOrEmpty(Request.QueryString["ActID"]))
                {
                    ActId = Convert.ToInt32(Request.QueryString["ActID"]);

                    if (ActId != 0)
                        // ddlAct.Items.FindByValue(Request.QueryString["ActID"]).Selected = true;
                        ddlAct.SelectedValue = Request.QueryString["ActID"];//Convert.ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Branchid"]))
                {
                    CustomerBranchId = Convert.ToInt32(Request.QueryString["Branchid"]);
                    // var nodeValue = "BITA Consulting Pune";
                    tvFilterLocation_SelectedNodeChanged(null, null);

                    foreach (TreeNode node in tvFilterLocation.Nodes)
                    {
                        if (node.ChildNodes.Count > 0)
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                if (child.Value == Convert.ToString(CustomerBranchId))
                                {
                                    child.Selected = true;
                                }
                            }
                        }
                        else if (node.Value == Convert.ToString(CustomerBranchId))
                        {
                            node.Selected = true;
                        }
                    }
                }

                if (tvFilterLocation.SelectedValue != "-1")
                {
                    int CustomerBranchlength = tvFilterLocation.SelectedNode.Text.Length;
                    CustomerBranchId = Convert.ToInt32(tvFilterLocation.SelectedValue);

                    if (CustomerBranchlength >= 20)
                        lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text;
                }
                if (ddlAct.SelectedValue != "-1" && ddlAct.SelectedValue != "")
                {
                    int actlength = ddlAct.SelectedItem.Text.Length;

                    if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                        ActId = Convert.ToInt32(ddlAct.SelectedValue);

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (actlength >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (actlength >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                }
                else
                {
                    divAdvSearch.Visible = false;
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    string role = AuthenticationHelper.Role;
                    ddlAct.Visible = true;

                    //if ((AuthenticationHelper.Role == "HMGMT") || (AuthenticationHelper.Role == "LSPOC") ||
                    //        (AuthenticationHelper.Role == "HMGR") || (AuthenticationHelper.Role == "HAPPR") || (AuthenticationHelper.Role == "HEXCT") || (AuthenticationHelper.Role == "DADMN"))
                    //{
                    role = "RLCS";
                    var detailsview = RLCSManagement.GetComplianceDetails(customerid, Branchlist, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, IsAvantisFlag);
                    detailsview = rlcslocationdetails.GetuserAssignedBranchList(detailsview);
                    if (UserID != -1)
                        detailsview = detailsview.Where(Entry => Entry.UserID == UserID).ToList();

                    //if (IsAvantisFlag)
                    //    detailsview = detailsview.Where(Entry => Entry.IsAvantis == IsAvantisFlag).ToList(); //1                    
                    //else
                    //    detailsview = detailsview.Where(Entry => Entry.IsAvantis == IsAvantisFlag).ToList(); //0 

                    Session["TotalRows"] = detailsview.Count;

                    GridStatutory.DataSource = detailsview;
                    GridStatutory.DataBind();

                    GridStatutory.Visible = true;
                    GridInternalCompliance.Visible = false;
                    //}
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    ddlAct.ClearSelection();
                    ddlAct.Visible = false;
                    var detailsview = Business.InternalComplianceManagement.GetInternalComplianceDetailsDashboard(customerid, Branchlist, RoleID, CustomerBranchId, CategoryID, AuthenticationHelper.UserID, IsApprover);
                    detailsview = rlcslocationdetails.GetuserAssignedBranchList(detailsview);
                    if (UserID != -1)
                        detailsview = detailsview.Where(Entry => Entry.UserID == UserID).ToList();

                    Session["TotalRows"] = detailsview.Count;
                    GridInternalCompliance.DataSource = detailsview;
                    GridInternalCompliance.DataBind();
                    GridStatutory.Visible = false;
                    GridInternalCompliance.Visible = true;
                }

                UpDetailView.Update();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode node = tvFilterLocation.Nodes[0];
                node.Selected = true;
                ddlAct.ClearSelection();
                lblAdvanceSearchScrum.Text = String.Empty;
                divAdvSearch.Visible = false;
                tvFilterLocation_SelectedNodeChanged(sender, e);
                var nvc = HttpUtility.ParseQueryString(Request.Url.Query);
                nvc.Remove("customerid");
                nvc.Remove("Category");
                nvc.Remove("ActID");
                nvc.Remove("Branchid");
                nvc.Remove("UserID");
                nvc.Remove("Internalsatutory");
                string url = Request.Url.AbsolutePath + "?" + nvc.ToString();
                Response.Redirect("~/RLCS/ComplianceDetails.aspx", false);
                Context.ApplicationInstance.CompleteRequest();

                //Rebind Grid
                //BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                if (ComplianceTypeFlag == "Statutory")
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                else if (ComplianceTypeFlag == "Internal")
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                lblAdvanceSearchScrum.Text = String.Empty;

                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;
                if (Convert.ToString(Session["TotalRows"]) != null && Convert.ToString(Session["TotalRows"]) != "")
                {
                    lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
                    lTotalCount.Text = GetTotalPagesCount().ToString();

                    if (lTotalCount.Text != "0")
                    {
                        if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                        {
                            SelectedPageNo.Text = "1";
                            lblStartRecord.Text = "1";

                            if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                                lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                            else
                                lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                        }
                    }
                    else if (lTotalCount.Text == "0")
                    {
                        SelectedPageNo.Text = "0";
                        DivRecordsScrum.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                //BindCategories();

                if (tvFilterLocation.SelectedValue != "-1")
                {
                    ddlAct.ClearSelection();
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    Branchlist.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    Branchlist.ToList();
                }
                else
                {
                    Branchlist.Clear();
                }
                if (ddlAct.SelectedValue == "-1" || ddlAct.SelectedValue == "")
                {
                    BindActList(Branchlist);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }


        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {

                    lblAdvanceSearchScrum.Text = String.Empty;

                    int RoleID = 3;
                    int CustomerBranchId = -1;

                    int CategoryID = -1;
                    int UserID = -1;
                    bool IsAvantisFlag = false;

                    if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                    {
                        customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["UserID"]))
                    {
                        UserID = Convert.ToInt32(Request.QueryString["UserID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["IsAvantisFlag"]))
                    {
                        IsAvantisFlag = Convert.ToBoolean(Request.QueryString["IsAvantisFlag"]);
                    }
                    if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                    {
                        //ddlAct.ClearSelection();
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        Branchlist.Clear();
                        GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        Branchlist.ToList();
                    }
                    else
                    {
                        Branchlist.Clear();
                    }
                    //if (ddlAct.SelectedValue == "-1" || ddlAct.SelectedValue == "")
                    //{
                    //    BindActList(Branchlist);
                    //}
                    if (!string.IsNullOrEmpty(Request.QueryString["ActID"]))
                    {
                        ActId = Convert.ToInt32(Request.QueryString["ActID"]);

                        if (ActId != 0)
                            // ddlAct.Items.FindByValue(Request.QueryString["ActID"]).Selected = true;
                            ddlAct.SelectedValue = Request.QueryString["ActID"];//Convert.ToString();
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                    {
                        IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Branchid"]))
                    {
                        CustomerBranchId = Convert.ToInt32(Request.QueryString["Branchid"]);
                        // var nodeValue = "BITA Consulting Pune";
                        tvFilterLocation_SelectedNodeChanged(null, null);

                        foreach (TreeNode node in tvFilterLocation.Nodes)
                        {
                            if (node.ChildNodes.Count > 0)
                            {
                                foreach (TreeNode child in node.ChildNodes)
                                {
                                    if (child.Value == Convert.ToString(CustomerBranchId))
                                    {
                                        child.Selected = true;
                                    }
                                }
                            }
                            else if (node.Value == Convert.ToString(CustomerBranchId))
                            {
                                node.Selected = true;
                            }
                        }
                    }

                    if (tvFilterLocation.SelectedValue != "-1")
                    {
                        int CustomerBranchlength = tvFilterLocation.SelectedNode.Text.Length;
                        CustomerBranchId = Convert.ToInt32(tvFilterLocation.SelectedValue);

                        if (CustomerBranchlength >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text;
                    }
                    if (ddlAct.SelectedValue != "-1" && ddlAct.SelectedValue != "")
                    {
                        int actlength = ddlAct.SelectedItem.Text.Length;

                        if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                            ActId = Convert.ToInt32(ddlAct.SelectedValue);

                        if (lblAdvanceSearchScrum.Text != "")
                        {
                            if (actlength >= 30)
                                lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                        }
                        else
                        {
                            if (actlength >= 30)
                                lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                        }
                    }

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        divAdvSearch.Visible = true;
                    }
                    else
                    {
                        divAdvSearch.Visible = false;
                    }

                    if (ComplianceTypeFlag == "Statutory")
                    {
                        string role = AuthenticationHelper.Role;
                        ddlAct.Visible = true;

                        //if ((AuthenticationHelper.Role == "HMGMT") || (AuthenticationHelper.Role == "LSPOC") ||
                        //        (AuthenticationHelper.Role == "HMGR") || (AuthenticationHelper.Role == "HAPPR") || (AuthenticationHelper.Role == "HEXCT") || (AuthenticationHelper.Role == "DADMN"))
                        //{
                        role = "RLCS";
                        var detailsview = RLCSManagement.GetComplianceDetails(customerid, Branchlist, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, IsAvantisFlag);
                        detailsview = rlcslocationdetails.GetuserAssignedBranchList(detailsview);
                        if (UserID != -1)
                            detailsview = detailsview.Where(Entry => Entry.UserID == UserID).ToList();

                        //if (IsAvantisFlag)
                        //    detailsview = detailsview.Where(Entry => Entry.IsAvantis == IsAvantisFlag).ToList(); //1                    
                        //else
                        //    detailsview = detailsview.Where(Entry => Entry.IsAvantis == IsAvantisFlag).ToList(); //0 

                        Session["TotalRows"] = detailsview.Count;
                    

                        if (detailsview.Count > 0)
                        {
                            ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Compliances");
                            DataTable ExcelData = null;
                            DataView view = new System.Data.DataView(detailsview.ToDataTable());
                            ExcelData = view.ToTable("Selected", false, "Branch", "Name", "Section", "ShortForm", "RequiredForms", "Frequency");//

                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Value = "Report Name:";
                            exWorkSheet.Cells["B2:C2"].Merge = true;
                            exWorkSheet.Cells["B2"].Value = "Report of Compliances";

                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Value = "Report Generated On:";
                            exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                            exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);

                            exWorkSheet.Cells["A5"].Value = "Branch";
                            exWorkSheet.Cells["B5"].Value = "Act";
                            exWorkSheet.Cells["C5"].Value = "Sections";
                            exWorkSheet.Cells["D5"].Value = "Compliance";
                            exWorkSheet.Cells["E5"].Value = "Form";
                            exWorkSheet.Cells["F5"].Value = "Frequency";

                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Value = "Report Name:";
                            exWorkSheet.Cells["B2:C2"].Merge = true;


                            exWorkSheet.Cells["B2"].Value = "Report of Compliances";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Value = "Report Generated On:";
                            exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "Branch";
                            exWorkSheet.Cells["A5"].AutoFitColumns(20);

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Name";
                            exWorkSheet.Cells["B5"].AutoFitColumns(60);

                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "Section";
                            exWorkSheet.Cells["C5"].AutoFitColumns(50);

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "ShortForm";
                            exWorkSheet.Cells["D5"].AutoFitColumns(70);

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "RequiredForms";
                            exWorkSheet.Cells["E5"].AutoFitColumns(15);

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Frequency";
                            exWorkSheet.Cells["F5"].AutoFitColumns(15);

                            string filename = "";

                            filename = "Compliances_Report.xlsx";


                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                    
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            TreeNode node = tvFilterLocation.Nodes[0];
            node.Selected = true;
            ddlAct.SelectedIndex = -1;
            ddlPageSize.SelectedIndex = 0;
            BindDetailView();
          
        }
        

    }
}