﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_MyPaymentDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    hdnPaymentID.Value = Request.QueryString["PID"];
                    GetPaymentDetails(Convert.ToInt32(Request.QueryString["PID"]));
                }
            }
        }

        protected void rbMode_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(rbMode.SelectedValue))
                {
                    if (rbMode.SelectedValue == "2") //Offline
                    {
                        divUTR.Visible = true;
                        divBank.Visible = true;
                        divOfflineType.Visible = true;

                        txtPaymentDate.Enabled = true;
                        txtPaymentDate.Text = string.Empty;

                        btnSubmit.Visible = true;

                        rfvPaymentDate.ValidationGroup = "OfflinePayment";
                        rfvOrderID.ValidationGroup = "OfflinePayment";
                        rfvPaymentAmount.ValidationGroup = "OfflinePayment";
                        rfvPaymentMobile.ValidationGroup = "OfflinePayment";
                        rfvPaymentEmail.ValidationGroup = "OfflinePayment";

                        //cvAmount1.ValidationGroup = "OfflinePayment";
                        cvAmount2.ValidationGroup = "OfflinePayment";
                    }
                    else if (rbMode.SelectedValue == "1") //Online
                    {
                        divUTR.Visible = false;
                        divBank.Visible = false;
                        divOfflineType.Visible = false;

                        txtPaymentDate.Enabled = false;
                        txtPaymentDate.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                        btnSubmit.Visible = false;

                        rfvPaymentDate.ValidationGroup = "OnlinePayment";
                        rfvOrderID.ValidationGroup = "OnlinePayment";
                        rfvPaymentAmount.ValidationGroup = "OnlinePayment";
                        rfvPaymentMobile.ValidationGroup = "OnlinePayment";
                        rfvPaymentEmail.ValidationGroup = "OnlinePayment";

                        //cvAmount1.ValidationGroup = "OnlinePayment";
                        cvAmount2.ValidationGroup = "OnlinePayment";
                    }

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "selClass", "setRadioButtonClass();", true);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (rbMode.SelectedValue == "2") //Offline
                {
                    if (!string.IsNullOrEmpty(rbOfflineType.SelectedValue))
                    {
                        
                        if (!string.IsNullOrEmpty(txtTxnNo.Text) && !string.IsNullOrEmpty(txtOrderID.Text) && !string.IsNullOrEmpty(txtPaymentDate.Text))
                        {
                            int loggedInUserID = AuthenticationHelper.UserID;

                            int customerID = -1;

                            if (hdnCustomerID.Value != null)
                                customerID = Convert.ToInt32(hdnCustomerID.Value);
                            else
                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                            int statusID = Convert.ToInt32(rbStatus.SelectedValue);

                            bool saveSuccess = PaymentManagement.CreatePaymentLog(customerID, 0, 1, txtOrderID.Text.Trim(), txtPaymentAmount.Text.Trim(), statusID, loggedInUserID); //PGID--2--OFFLINE, TxnType--1--PaymentRequest, StatusID--0--Initiated

                            if (saveSuccess)
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    PaymentTransactionLog newPaymentLog = new PaymentTransactionLog();
                                    newPaymentLog.MODE = Convert.ToInt32(rbMode.SelectedValue);
                                    newPaymentLog.PGID = 0;
                                    newPaymentLog.OrderID = txtOrderID.Text.Trim();
                                    newPaymentLog.CustomerID = customerID;
                                    newPaymentLog.Phone = txtPaymentMobile.Text.Trim();
                                    newPaymentLog.Email = txtPaymentEmail.Text.Trim();
                                    newPaymentLog.StatusID = statusID;
                                    newPaymentLog.Amount = Convert.ToDecimal(txtPaymentAmount.Text.Trim());
                                    newPaymentLog.TxnDate = DateTime.Now;

                                    newPaymentLog.BankTxnID = txtTxnNo.Text.Trim(); //UTR-Cheque No
                                    newPaymentLog.TxnID = txtBank.Text.Trim(); //Bank

                                    if(!string.IsNullOrEmpty(rbOfflineType.SelectedValue))
                                    {
                                        newPaymentLog.Payment_Mode = rbOfflineType.SelectedValue;
                                    }

                                    newPaymentLog.IsDeleted = false;
                                    newPaymentLog.CreatedOn = DateTime.Now;
                                    newPaymentLog.CreatedBy = loggedInUserID;

                                    saveSuccess = PaymentManagement.CreateUpdate_PaymentTransactionLog(newPaymentLog);
                                }
                            }
                        }
                        else
                        {
                            cvPayment.IsValid = false;
                            cvPayment.ErrorMessage = "Required UTR/Cheque No., OrderID, Transaction Date";
                        }
                    
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetPaymentDetails(int paymentID)
        {
            var paymentDetails = PaymentManagement.GetPaymentOrderDetailsByPaymentID(paymentID);
            if (paymentDetails != null)
            {
                if (paymentDetails.MODE != null)
                {
                    if (rbMode.Items.FindByValue(paymentDetails.MODE.ToString()) != null)
                    {
                        rbMode.ClearSelection();
                        rbMode.Items.FindByValue(paymentDetails.MODE.ToString()).Selected = true;
                    }
                }

                rbMode_CheckedChanged(null, null);

                if (paymentDetails.Payment_Mode != null)
                {
                    if (rbOfflineType.Items.FindByValue(paymentDetails.Payment_Mode.ToString()) != null)
                    {
                        rbOfflineType.ClearSelection();
                        rbOfflineType.Items.FindByValue(paymentDetails.Payment_Mode.ToString()).Selected = true;
                    }
                }

                if (paymentDetails.BankTxnID != null)
                    txtTxnNo.Text = paymentDetails.BankTxnID;
                else
                    txtTxnNo.Text = string.Empty;

                if (paymentDetails.TxnID != null)
                    txtBank.Text = paymentDetails.TxnID;
                else
                    txtBank.Text = string.Empty;

                if (paymentDetails.TxnDate != null)
                    txtPaymentDate.Text = Convert.ToDateTime(paymentDetails.TxnDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                else
                    txtPaymentDate.Text = string.Empty;

                txtOrderID.Text = paymentDetails.OrderID;
                txtPaymentEmail.Text = paymentDetails.Email;
                txtPaymentMobile.Text = paymentDetails.Phone;

                if (paymentDetails.Amount != null)
                    txtPaymentAmount.Text = Convert.ToString(Convert.ToInt32(paymentDetails.Amount));
                else
                    txtPaymentAmount.Text = string.Empty;

                rbStatus.ClearSelection();
                if (paymentDetails.StatusID != null)
                {
                    if (rbStatus.Items.FindByValue(paymentDetails.StatusID.ToString()) != null)
                    {
                        rbStatus.ClearSelection();
                        rbStatus.Items.FindByValue(paymentDetails.StatusID.ToString()).Selected = true;
                    }
                }
                                
                hdnCustomerID.Value = paymentDetails.CustomerID.ToString();
            }
        }
    }
}