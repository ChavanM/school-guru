﻿<%@ Page Title="Employee :: Setup/Onboarding" Language="C#" AutoEventWireup="true" MasterPageFile="~/HRPlusSPOCMGR.Master" CodeBehind="RLCS_EmployeeMaster_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_EmployeeMaster_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>


    <title></title>
      <style type="text/css">
               .k-textbox>input{
         text-align:left;
     }

             .k-check-all{
              zoom:1.1;
              font-size:13px;
          }
            .k-label input[type="checkbox"] {
    zoom: 1.2;
        }

            .k-treeview .k-i-collapse, .k-treeview .k-i-expand, .k-treeview .k-i-minus, .k-treeview .k-i-plus{
                
    margin-left: -19px;
    cursor: pointer;
            }

            .k-list-container.k-popup-dropdowntree .k-check-all{
                margin: 10px 8px 0;
            }
           .k-treeview .k-checkbox {
    margin-top: .2em;
    margin-left: 2px;
}
            .k-list-container.k-popup-dropdowntree .k-treeview {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0px 5px 7px 0px;
}
            .k-checkbox-label:hover{
            color:#1fd9e1;
     }
             .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
    cursor: pointer;
    background-color: transparent !important;
    border-color: transparent;
    color:#1fd9e1;
     }
           .btn {
         height:33px;
         font-weight:400;
         font-size:14px;
         }
             .k-dropdown-wrap-hover,.k-state-default-hover, .k-state-hover, .k-state-hover:hover {
    color: #2e2e2e;
    background-color: #d8d6d6 !important;
}
             .k-grid-content {
            min-height: auto !important;
        }
           .k-multiselect-wrap .k-input {
               padding-top:6px;
               display: inherit !important;
           }
        .k-grid tbody .k-button {
            min-width: 19px;
            min-height: 25px;
            background-color: transparent;
            border: none;
            margin-left: 0px;
            margin-right: 0px;
            padding-left: 2px;
            padding-right: 2px;
        }
        .k-button.k-state-active, .k-button:active{
            color:black;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
             .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
               .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }
         .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }
           .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
           .k-grid, .k-listview {
               margin-top: 0px;
           }
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }
        
              .k-tooltip-content{
         width: max-content;
        
         }
        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0px -3px;
            line-height: normal;
        }
        .k-calendar-container {
    background-color: white;
    width: 217px;
}
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top:0px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

       
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }


        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: #535b6a;
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height:34px;
        }
        .k-multiselect-wrap, .k-floatwrap{
            height:34px;
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
    color: #515967;
    padding-top: 5px;
}
        .k-filter-row th, .k-grid-header th[data-index='10'] {
            text-align:center;
            }
        .k-i-filter-clear{
            margin-top:-3px;
        }
        .k-button{
            margin-top:2.5px;
        }
         label.k-label:hover{
              color:#1fd9e1;
          }
          .k-tooltip{
            margin-top:5px;
        }

    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>
    var record=0;
    <script type="text/javascript">

        
        function ClearAllFilterMain(e) {

            e.preventDefault();
            
            $("#ddlCustomer").data("kendoDropDownList").value("");
            $("#dropdowntree").data("kendoDropDownTree").value("");
           
            $('#ddlStatus').data("kendoDropDownList").select(0);
       
            $("#txtSearch").val('');
            $("#grid").data("kendoGrid").dataSource.data([]);
            $('#ClearfilterMain').css('display', 'none');
           
           
        }

        $(document).ready(function() {

            
            $("#btnSearch").click(function () {
                debugger;

                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#grid").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 50,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "EM_Branch", operator: "contains", value: $x },
                              { field: "EM_EmpName", operator: "contains", value: $x },
                              { field: "EM_EmpID", operator: "contains", value: $x },
                          //    { field: "EM_DOJ", operator: "contains", value: $x },
                              { field: "EM_Location", operator: "contains", value: $x },
                              { field: "EM_PT_State", operator: "contains", value: $x }                          
                            ]
                        }
                    });

                    return false;
                }
                else {
                    //BindGrid_Employee();
                    $('#grid').data('kendoGrid').dataSource.read();
                    return false;
                }
           
            });

            $('#ddlCustomer').change(function () {
                  
                if($(this).val()){
                    $('#ClearfilterMain').show();
                }
                else{
                    $('#ClearfilterMain').hide();
                }
            });

            $('#dropdowntree').change(function () {
                  
                if($(this).val() ){
                    $('#ClearfilterMain').show();
                }
                else{
                    $('#ClearfilterMain').hide();
                }
            });
           
            $('#ddlStatus').change(function () {
                  
                if($(this).val() ){
                    $('#ClearfilterMain').show();
                }
                else{
                    $('#ClearfilterMain').hide();
                }
            });



            $('#txtSearch').keyup(function () {
                  
                if($(this).val() ){
                   
                    $('#ClearfilterMain').show();
                    
                }
                else{
                    if(!$('#ddlCustomer').select() ||  !$('#dropdowntree').select()){
                        $('#ClearfilterMain').hide();
                    }
                }
            });


            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#btnUploadCtc").hide(); 

             var serviceProviderID ='<% =serviceproviderid%>';
            if(serviceProviderID == "94")
                $("#btnUploadCtc").show(); //if serviceprovider is 94 then only show    
            else
                $("#btnUploadCtc").hide();

        });
        function Bind_Customers() {
            $("#ddlCustomer").kendoDropDownList({
               // placeholder: "Select",
               // optionLabel: "Select",
                autoWidth: true,                
                dataTextField: "Name",
                dataValueField: "ID",
                index: 0,
                change: function (e) {
                    debugger;                    
                    checkServiceProvider();
                    Bind_CustomerBranches();
                    BindGrid_Employee();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {     
                            url: '<%=avacomRLCSAPIURL%>GetHRCustomers?userID=<%=userID%>&custID=<%=custID%>&SPID=<%=spID%>&distID=<%=distID%>&roleCode=<%=userRole%>&prodType=2&showSubDist=false',
                            //url: '<%=avacomRLCSAPIURL%>GetCustomers?custID=' + <%=custID%> +'&SPID='+<%=spID%>+'&distID='+<%=distID%>,
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            return response.Result;
                        }
                    }
                }
            });
        }

       

        function checkServiceProvider() 
        {        
            var cust= $("#ddlCustomer").val();

            if((cust!="")&&(cust!=null))
            {
                var ID = cust;  // make sure you are passing the integer value
                var obj = { ID: ID };
                var param = JSON.stringify(obj);  // stringify the parameter

                $.ajax({
                    url: '/RLCS/RLCS_EmployeeMaster_New.aspx/GetServiceProvider', // remove the ..
                    method: 'post',
                    contentType: 'application/json; charset=utf-8',
                    data:  param,
                    dataType: 'json',
                    async: true,
                    cache: false,
                    success: function (data) 
                    {
                        if(data.d == "94")
                            $("#btnUploadCtc").show(); //if serviceprovider is 94 then only show          
                        else
                            $("#btnUploadCtc").hide();
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);  // to see the error message
                    }
                }); 
            }
                                 
        } 

        function Bind_CustomerBranches(){
            debugger;
            var customerID=-1;

            if ($('#dropdowntree').data('kendoDropDownTree')) {
                $('#dropdowntree').data('kendoDropDownTree').destroy();
                $('#divBranches').empty();
                $('#divBranches').append('<input id="dropdowntree" data-placeholder="Entity/Branch" style="width: 98%;" />')
            }

            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                customerID=$("#ddlCustomer").val();
            }else{
                customerID=<% =custID%>;
            }

            $("#dropdowntree").kendoDropDownTree({
                
                placeholder: "Entity/Branch",
                checkboxes: {
                    checkChildren: true
                },
                //checkboxes: true,
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                //dataTextField: "AVACOM_BranchName",
                //dataValueField: "AVACOM_BranchID",
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
               
                change: function (e) { 
                    debugger;
                    if ($("#ddlStatus").data("kendoDropDownList").text()!="Select" )                   
                    {
                        setCommonAllfilter();
                    }
                    else{
                        var filter = { logic: "or", filters: [] };
                        var values = this.value();

                        $.each(values, function (i, v) {
                            filter.filters.push({
                                field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)    
                            });
                        });

                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                   
                },
                dataSource: {                    
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetAllEntitiesLocationsList?customerID='+customerID,
                            //url: '<%=avacomRLCSAPIURL%>GetAssignedEntities?customerId='+customerID+'&userId=<% =userID%>&profileID=<% =ProfileID%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {                            
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }

        $(document).ready(function () {
            fhead('Masters/Employee');
            Bind_Customers(); 
            setTimeout(function () {
                BindGrid_Employee();
            }, 5000);
           // Bind_CustomerBranches();            
        });

        function checkEmployeeActive(value) {
            if (value == "A") {
                return "Active";
            } else if (value == "I") {
                return "InActive";
            }
        }

        function BindGrid_Employee() {
            debugger;

            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined ||<% =custID%>!=-1||<% =distID%>!=-1){
                customerID=$("#ddlCustomer").val();

                if(customerID==""){
                    customerID=<% =custID%>;
                        var a=$("#ddlCustomer").data("kendoDropDownList");a.value(customerID);
                }
                Bind_CustomerBranches(); 
                $("#grid").empty(); 
                var gridview = $("#grid").kendoGrid({
                    dataSource: {
                        serverPaging: false,
                        pageSize: 10,
                        transport: {
                            read: {
                                url: '<% =avacomRLCSAPIURL%>GetAll_EmployeeMaster?CustomerID='+customerID,
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                            }
                        },
                        batch: true,
                        pageSize: 10,
                        schema: {
                            data: function (response) {                            
                                if (response != null && response != undefined)                                
                                    return response.Result;
                            },
                            total: function (response) {                            
                                if (response != null && response != undefined)
                                    if (response.Result != null && response.Result != undefined)
                                        return response.Result.length;
                            }
                        }
                    },

                    sortable: true,
                    filterable: true,
                    columnMenu: true,
                    pageable: {
                        refresh:false,
                        pageSize:10,
                        pageSizes:true,
                        buttonCount: 3,
                    },
                    dataBinding: function () {
                        record = 0;
                        var total = this.dataSource._pristineTotal;
                        if (this.dataSource.pageSize() == undefined) {
                            this.dataSource.pageSize(total);
                            record = (this.dataSource.page()-1) * this.dataSource.pageSize();
                        }
                        else {
                            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                        }
                    },
                    reorderable: true,
                    resizable: true,
                    multi: true,
                    selectable: true,
                    columns: [

                        { hidden: true, field: "AVACOM_CustomerID", title: "CustID",menu:false, },
                        { hidden: true, field: "AVACOM_BranchID", title: "BranchID",menu:false, },
                       // { hidden: true, field: "EM_Branch", title: "Branch",menu:false, },
                        //{
                        //    title: "Sr.No.",lock:true,
                        //    field: "rowNumber",
                        //    template: "#= ++record #",
                        //    //template: "<span class='row-number'></span>",
                        //    width: "10%;",
                        //    attributes: {
                        //        //border-width: 0px 0px 1px 1px;
                        //        style: 'white-space: nowrap;text-align:left;'
                        //    },
                        //    filterable:false,
                            
                        //},
                        {
                            field: "EM_EmpID", title: 'EmpID',
                            width: "11.5%;",
                            attributes: {
                                    //border-width: 0px 1px 1px 1px
                                style: 'white-space: nowrap;'

                            }, filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "EM_EmpName", title: 'Name',
                            width: "13%",
                            attributes: {
                                //border-width: 0px 1px 1px 0px;
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        }, 
                        {
                            field: "EM_Branch", title: 'Branch',
                            width: "13%",
                            attributes: {
                                //border-width: 0px 1px 1px 0px;
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "EM_Location", title: 'Location',
                            width: "14%",
                            attributes: {
                                //border-width: 0px 1px 1px 0px;
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "EM_DOJ", 
                            title: 'DOJ',
                            type:'date',
                            format: "{0:dd-MMM-yyyy}",
                            //template: "#= kendo.toString(kendo.parseDate(EM_DOJ, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
                            width: "11%",
                            attributes: {
                                    //border-width:0px 1px 1px 0px
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                               // format: "{0:dd/MM/yyyy}",
                            
                                //ui: 'datepicker',
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        type:'date',
                                        format: "{0:dd-MMM-yyyy}",
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "EM_DOL",
                            title: 'DOL',
                            type:'date',
                            format: "{0:dd-MMM-yyyy}",
                           // template: "#= dateConverter(data)#",
                            //template: "#= kendo.toString(kendo.parseDate(EM_DOL, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
                            width: "10.4%",
                            attributes: {
                                //border-width: 0px 1px 1px 0px;
                                style: 'white-space: nowrap;'
                       
                            },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        type:'date',
                                        format: "{0:dd-MMM-yyyy}",
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                         {
                             field: "EM_PT_State", title: 'PT State',
                             width: "12.8%",
                             attributes: {
                                 //border-width: 0px 1px 1px 0px;
                                 style: 'white-space: nowrap;'
                             },
                             filterable: {
                                 multi: true,
                                 extra: false,
                                 search: true,
                                 operators: {
                                     string: {
                                         eq: "Is equal to",
                                         neq: "Is not equal to",
                                         contains: "Contains"
                                     }
                                 }
                             }
                         },
                        {
                            field: "EM_Status",
                            title: 'Status',
                            template: '#:checkEmployeeActive(EM_Status)#',
                            width: "10.3%",
                            attributes: {
                                //border-width: 0px 1px 1px 0px; 
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            command: 
                                [
                                    { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-download" },
                                    { name: "delete", text: "", iconClass: "k-icon k-i-delete", className: "my-delete" },
                                ], title: "Action", lock: true, width: "8%;",
                                attributes: {
                                    //border-width: 0px 1px 1px 0px;
                                    style: 'white-space: nowrap;text-align:center;'
                                },
                        }
                    ],
                    dataBound: function () {
                        var rows = this.items();
                        $(rows).each(function () {
                            var index = $(this).index() + 1
                                + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                            var rowLabel = $(this).find(".row-number");
                            $(rowLabel).html(index);
                        });
                    }
                });
             }
            else
            { $("#grid").empty();}
            //gridview.Custom("edit").Click("Edit");    
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit";
                }
            });

            $("#ClearfilterMain").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Clear Filter";
                }
            });
            

            $("#grid").kendoTooltip({
                filter: ".k-grid-delete",
                content: function (e) {
                    return "Delete";
                }
            });

            $("#btnUploadEmployeeNew").kendoTooltip({               
                content: function (e) {
                    return "Upload Bulk";
                }
            });

            $("#btnUploadEmployeeSelectedCloumn").kendoTooltip({               
                content: function (e) {
                    return "Upload Selected Column";
                }
            });

            $("#btnUploadCtc").kendoTooltip({               
                content: function (e) {
                    return "Upload CTC";
                }
            });
            $("#btnAdd").kendoTooltip({
                //filter: ".k-grid-zoom",
                content: function(e){
                    return "Add New Employee";
                }
            });
            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "bottom",
                content: function (e) {
                    debugger;
                    var content = e.target.context.textContent;
                    if (content != "" && content != record) {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                    {
                        e.preventDefault();
                    }
                }
            }).data("kendoTooltip");
        }

        function dateConverter(data) {
            //debugger;
            var strDate = '';
            kendo.culture("en-IN");
            if (data.EM_DOL != null && data.EM_DOL != undefined) {
                strDate = kendo.toString(kendo.parseDate(data.EM_DOL), "dd-MMM-yyyy")
            }
            return strDate;
        }
       
        $(document).on("click", "#grid tbody tr .k-grid-edit", function (e) {
            debugger;
            var d = e.data;
            
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

            var EMPID = item.EM_EmpID;
            var Client = item.EM_ClientID;
            var CustID = item.AVACOM_CustomerID;
       
            $('#divRLCSEmployeeAddDialog').show();
            //$("#idloader1").show();
            var myWindowAdv = $("#divRLCSEmployeeAddDialog");

            myWindowAdv.kendoWindow({
                width: "85%",
                height: '90%',
                content: "../Setup/CreateEmplodyeeMaster?EmpID=" + EMPID + "&CustID=" + CustID + "&Client=" + Client,
                iframe: true,
                title: "Employee Details-HR Compliance",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            //$('#iframeAdd').attr('src', "../Setup/CreateEmplodyeeMaster?EmpID=" + EMPID + "&CustID=" + CustID + "&Client=" + Client);

            // $('#grid').data('kendoGrid').refresh();
            // return false;
        }
        );
        function onClose() {
            $('#grid').data('kendoGrid').dataSource.read();
        }

        $(document).on("click", "#grid tbody tr .k-grid-delete", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            var cust = item.AVACOM_CustomerID;
            var EmpID = item.EM_EmpID;
            var $tr = $(this).closest("tr");

            $.ajax({
                type: 'POST',
                url: '<% =avacomRLCSAPIURL%>DeleteEmployee?EmpID=' + EmpID + '&CustID=' + cust,
                dataType: "json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                    xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                    xhr.setRequestHeader('Content-Type', 'application/json');
                },
                success: function (result) {
                    // notify the data source that the request succeeded
                    onClose();
                    $("#CheckCustomer").text("Employee InActivated Successfully.");
                    $('#btnModal').click();  
                    return false;
                    //grid = $("#grid").data("kendoGrid");
                    //grid.removeRow($tr);
                },
                error: function (result) {
                    // notify the data source that the request failed
                    console.log(result);
                }
            });
        });

        $(document).ready(function () {
            $('#divRLCSEmployeeUploadDialog').hide();
            $('#divRLCSEmployeeUploadDialogBulk').hide();
            $('#divRLCSEmployeeAddDialog').hide();
            $('#divRLCSEmployeeUploadCtc').hide();
           // BindGrid_Employee();            
        });
    </script>

    <script type="text/javascript">
        function fcloseStory(obj) {
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();
            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            
            CheckFilterClearorNotMain();
        };

        //Filter 16JAN2020
        function setCommonAllfilter() {
            if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0      
                && $("#ddlStatus").data("kendoDropDownList").text()  != "Select")
            {

                //location details
                var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list, function (i, v) {
                    locationsdetails.push({
                        field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                    });
                });

                //Status details
                var StatusVal = $("#ddlStatus").data("kendoDropDownList").value();
                var Statusdetails = [];
               //$.each(list1, function (i, v) {
                    Statusdetails.push({
                        field: "EM_Status", operator: "eq", value: StatusVal
                    });
              // });


                var dataSource = $("#grid").data("kendoGrid").dataSource;

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Statusdetails
                        },
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });

            }
            else  if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                   && $("#ddlStatus").data("kendoDropDownList").text()=="Select" ) 
                   {

                //location details
                var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list, function (i, v) {
                    locationsdetails.push({
                        field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                    });
                });


                var dataSource = $("#grid").data("kendoGrid").dataSource;
                   
                dataSource.filter({
                    logic: "or",
                    filters: [  
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });
    
            }
            else  if ( $("#ddlStatus").data("kendoDropDownList").value()=="A" ) 
            {
                //Status details
                var StatusVal = $("#ddlStatus").data("kendoDropDownList").value();
                var Statusdetails = [];
                Statusdetails.push({
                    field: "EM_Status", operator: "eq", value: StatusVal
                });
              


                var dataSource = $("#grid").data("kendoGrid").dataSource;
                   
                dataSource.filter({
                    logic: "or",
                    filters: [  
                        {
                            logic: "or",
                            filters: Statusdetails
                        }
                    ]
                });
    
            }
        }
        function fCreateStoryBoard(Id, div, filtername) {
          
            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>&nbsp;');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }


            CheckFilterClearorNot();

            CheckFilterClearorNotMain();
        }
        function CheckFilterClearorNot() {
            if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) ) {
                $('#Clearfilter').css('display', 'none');
            }
        }
        //END
     
        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }        
    </script>

    <script>
        function OpenUploadWindow() {

            $('#divRLCSEmployeeUploadDialog').show();
            kendo.ui.progress($(".chart-loading"), true);
            $("#idloader3").show();
            //$('#iframeUpload').attr('src', "../Setup/UploadEmployeeFiles");
            var myWindowAdv = $("#divRLCSEmployeeUploadDialog");
            var customerID = $("#ddlCustomer").val();
            if (customerID!='') {
            myWindowAdv.kendoWindow({
                width: "60%",
                height: "50%",
                content: "../Setup/UploadEmployeeFiles?CustID=" + customerID,
                iframe: true,
                title: "Upload",
                visible: false,
                actions: [
                    "Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            
            return false;
            }
            else {
                $("#CheckCustomer").text("Please Select Customer..!");
                $('#btnModal').click();  
                return false;
            }
        }

function OpenUploadWindowSelectedBulk() {
            var UserID=<% =userID%>;
            $('#divRLCSEmployeeUploadDialogBulk').show();
            kendo.ui.progress($(".chart-loading"), true);
            $("#idloader3").show();
            //$('#iframeUpload').attr('src', "../Setup/UploadEmployeeFiles");
            var myWindowAdv = $("#divRLCSEmployeeUploadDialogBulk");
            var customerID = $("#ddlCustomer").val();
            if (customerID!='') {
            myWindowAdv.kendoWindow({
                width: "85%",
                height: "80%",
                content: "../Setup/UploadEmployeeUpdateSelected?CustID=" + customerID+"&UserID="+UserID ,
                iframe: true,
                title: "Upload Employee Details",
                visible: false,
                actions: [
                    "Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            
            return false;
            }
            else {
                $("#CheckCustomer").text("Please Select Customer..!");
                $('#btnModal').click();  
                return false;
            }
        }


        function OpenUploadCTCWindow() 
        {
            var UserID=<% =userID%>;
            $('#divRLCSEmployeeUploadCtc').show();
            kendo.ui.progress($(".chart-loading"), true);
            $("#idloader3").show();
            //$('#iframeUpload').attr('src', "../Setup/UploadEmployeeFiles");
            var myWindowAdv = $("#divRLCSEmployeeUploadCtc");
            var customerID = $("#ddlCustomer").val();
            if (customerID!='') 
            {
                myWindowAdv.kendoWindow({
                    width: "60%",
                    height: "60%",
                    //content: "../Setup/UploadEmployeeUpdateSelected?CustID=" + customerID+"&UserID="+UserID ,
                    content: "../Setup/uploadEmpCTCfiles?CustID=" + customerID,            
                    iframe: true,
                    title: "Upload Employee CTC",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();            
                return false;
            }
            else 
            {
                $("#CheckCustomer").text("Please Select Customer..!");
                $('#btnModal').click();  
                return false;
            }
        }

        function OpenUploadWindowNew() {
            var UserID=<% =userID%>;
            $('#divRLCSEmployeeUploadDialog').show();
            kendo.ui.progress($(".chart-loading"), true);
            $("#idloader3").show();
            //$('#iframeUpload').attr('src', "../Setup/UploadEmployeeFiles");
            var myWindowAdv = $("#divRLCSEmployeeUploadDialog");
            var customerID = $("#ddlCustomer").val();
            if (customerID!='') {
                myWindowAdv.kendoWindow({
                    width: "60%",
                    height: "50%",
                    content: "../Setup/UploadEmployeeFilesNew?CustID=" + customerID +"&UserID="+UserID ,
                    iframe: true,
                    title: "Upload",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
            
                return false;
            }
            else {
                $("#CheckCustomer").text("Please Select Customer..!");
                $('#btnModal').click();  
                return false;
            }
        }
        function OpenAddEmployeePopup(ID) {
            debugger;
            $('#divRLCSEmployeeAddDialog').show();
            //$("#idloader1").show();
            var myWindowAdv = $("#divRLCSEmployeeAddDialog");
            var ddlCust= $('#ddlCustomer').val();
            if (ddlCust!='') {
    
           
                function onClose() {
                    BindGrid_Employee();
                }

                myWindowAdv.kendoWindow({
                    width: "86%",
                    height: '90%',
                    content: "../Setup/CreateEmplodyeeMaster?EmpID=" + ID+"&CustID="+ddlCust,
                    iframe: true,
                    title: "Employee Details-HR Compliance",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
                //$('#iframeAdd').attr('src', "../Setup/CreateEmplodyeeMaster?Id=" + ID);

                // $('#grid').data('kendoGrid').refresh();
                BindGrid_Employee();
                return false;
            }
            else {
                $("#CheckCustomer").text("Please Select Customer..!");
                $('#btnModal').click();  
                return false;
            }
        }


        $(document).ready(function () {
            // function BindLocationFilter() {

             function ClearAllFilterMain(e) {

                if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                    customerID=$("#ddlCustomer").val();
                }else{
                    customerID=<% =custID%>;
            }

                 myWindowAdv.kendoWindow({
                     width: "85%",
                     height: '90%',
                     content: "../Setup/CreateEmplodyeeMaster?EmpID=" + ID + "&CustID=" + customerID + "&Client=" + Client,
                     iframe: true,
                     title: "Employee Details-HR Compliance",
                     visible: false,
                     actions: [
                         //"Pin",
                         "Close"
                     ],
                     close: onClose
                 });
            
            myWindowAdv.data("kendoWindow").center().open();
           
            return false;
        }
        });
    $(document).ready(function () {
            


        $("#ddlStatus").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "EM_Status",
            dataSource: 
                [
                    { text: "Select", EM_Status: "S" },
                    { text: "Active", EM_Status: "A" },
                    { text: "InActive", EM_Status: "I" }
                ],
            change: function () {
                debugger;
                var value = this.value();

                var gridview = $("#grid").data("kendoGrid");

                if (value) {
                    if ($("#dropdowntree").data("kendoDropDownTree")._values.length == 0)
                    {
                        gridview.dataSource.filter({ field: "EM_Status", operator: "eq", value: value });
                    }
                    else {
                        setCommonAllfilter();
                      //  $("#grid").data("kendoGrid").dataSource.filter({ field: "EM_Status", operator: "eq", value: value });
                        fCreateStoryBoard('ddlStatus', 'filterstatus1', 'loc');
                    }
                } else {
                    grid.dataSource.filter({});
                }
            }
        });
       
    });


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- Trigger the modal with a button -->
<button id ="btnModal" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="display:none;">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Message</h3>
      </div>
      <div class="modal-body">
        <h4> <span id="CheckCustomer" ></span></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server" PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx">Entity-Branch</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>
        </div>

       <div style="margin: 0.4% 0.5% 0.5%;">
                <div id="divBranches1" style="width: 19%;float: left;"><input id="ddlCustomer" style="width: 94%; margin-right: 0.2%;" /></div>
                <div id="divBranches" style="width: 19%;float: left;"><input id="dropdowntree"  style="width: 94%; margin-right: 0.2%;" /></div>
                <input id="ddlStatus" style="width: 15%; margin-right: 0.2%;" />
                <input class="k-textbox" type="text" id="txtSearch" style="width: 120px;margin-right: 0.2%;height:34px;" placeholder="Type to Search" />
                <button id="btnSearch" class="btn btn-primary" style="height:33px;" onclick="return ApplyFilter();">Search</button>
             
                 <button id="btnUploadCtc" class="btn btn-primary" onclick="return OpenUploadCTCWindow();"style="height: 33px;padding-top:0px;"><span class="k-icon k-i-upload"></span></button>
             <button id="btnUploadEmployeeNew" class="btn btn-primary" onclick="return OpenUploadWindowNew()"style="height: 33px;padding-top:0px;"><span class="k-icon k-i-upload"></span></button>
                <button id="btnUploadEmployeeSelectedCloumn" class="btn btn-primary" onclick="return OpenUploadWindowSelectedBulk()"style="height:33px;;padding-top:0px"><span class="k-icon k-i-upload"></span></button>
                      <button id="btnAdd" class="btn btn-primary" onclick="return OpenAddEmployeePopup()"style="height: 33px;"><span class="k-icon k-i-plus-outline"></span>Add New</button>
               <button id="ClearfilterMain" class="btn btn-primary" style="margin-left: -0.3%;margin-top:2px;float: right;display:none;height:33px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span><font size="2" style="zoom:1.1">Clear Filter</font></button>
              </div>
   
        
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar" style="margin-top:4px;">
            <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: black;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterstatus1">&nbsp;</div>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">

            <div id="grid" <%--style="border: none;"--%>>
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>
        <div id="divRLCSEmployeeUploadDialogBulk">
        <iframe id="iframeUploadBulk" style="width: 1165px; height: 677px; border: none"></iframe>
    </div>
    <div id="divRLCSEmployeeUploadDialog">
        <iframe id="iframeUpload" style="width: 1165px; height: 677px; border: none"></iframe>
    </div>
    <div id="divRLCSEmployeeAddDialog">
        <iframe id="iframeAdd" style="width: 1165px; height: 677px; border: none"></iframe>
    </div>
    <div id="divRLCSEmployeeUploadCtc">
        <iframe id="iframeUploadCtc" style="width: 1165px; height: 677px; border: none"></iframe>
    </div>

</asp:Content>

