﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EntitiesDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.EntitiesDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
        
    <%--<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>--%>

    <style type="text/css">
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #fff;
            background-color: #1fd9e1;
            border-color: #1fd9e1;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            border-color: #ceced2;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }
        /*.k-grid{
            position:;
        }*/
        /*.k-grid,
        .k-grid-header-wrap,
        .k-grid-header th,
        .k-grid-content > table > tbody > tr > td {
            border: none;
        }*/
        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 1px 1px 1px 1px;
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        td.k-command-cell {
            border-width: 1px 1px 1px 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-grid, .k-listview {
            /*position: relative;*/
            zoom: 1;
        }
    </style>
</head>
    
<script type="text/javascript">
    $(document).ready(function () {
        var entity = "Locations";
        var name = "";
        var client = "";
        var clnt = "";
        $('#brd').hide();
        $('#export').hide();
        name = $('#grdEntityDetails').find("td:first").next().next().text();

        $('#grdEntityDetails').click(function () {
            $('.breadcrumb').css({ '-webkit-box-shadow': '', 'box-shadow': '' });
            name = $(this).find("td:first").next().next().text();
            client = $('#grdEntityDetails').find("td:first").next().text().trim();
            //alert(name);
            //alert(client);
            //$('#liEntity').html(name);
        });

        $('#liEntity').click(function () {            
            $('#grdEntityDetails').show();
            $('#brd').hide();
            $('#export').hide();
            $('#btnback').hide();
            $('#gridEmployees').hide();
            $('#gridContrators').hide();
            $('#gridPrevJoinees').hide();
            $('#gridPrevResignee').hide();
            $('#gridLocation').hide();
        });
    });

    function NoOfEmployees(obj) {   

         var array = $(obj).attr('data-clId');
        var temp = new Array();
        if (array != "" && array != undefined) {
            temp = array.split(",");
        }
        //alert(eiddata);
        var client = temp[0];
        entitymain = "";
        entitymain = temp[1];
          
        entity = "";
        entity = "Employees";
        $('#liEntityVal').html(entity);
        $('#liEntity').html(entitymain);
        
            $('#brd').show();
            $('#export').show();
            //entity = "";
            
            //$('#liEntityVal').html(entity);
            //client = $('#grdEntityDetails').find("td:first").next().text().trim();
            var authkey = $('#hdnAuthKey').val();
            var ProfileID = $('#hdnProfileID').val();
            $('#grdEntityDetails').hide();
            $('#gridLocations').hide();
            $('#gridContrators').hide();
            $('#gridPrevJoinees').hide();
            $('#gridPrevResignee').hide();
            $('#gridEmployees').show();
            name = $('#grdEntityDetails').find("td:first").next().next().text();
            var grid = $("#gridEmployees").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =tlConnectAPIUrl%>ClientDetail/NoOfEmployees?id=' + client,
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('Authorization', authkey);
                                xhr.setRequestHeader('X-User-Id-1', ProfileID);
                            },
                            dataType: "json"
                        }
                    },
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    }
                },

                height: 513,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    {
                        field: "EM_EmpID", title: 'Employee ID',

                        // width: 143,

                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EM_EmpName", title: 'Employee Name', filterable: {
                            extra: false,
                            width: 115,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "SM_Name", title: 'State',
                        // width: 148,
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "LM_Name", title: 'Location',
                        //  width: 149,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EM_Branch", title: 'Branch',
                        width: 132,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "EM_Gender", title: 'Gender',
                        //width: 180,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "EM_DOB", title: 'Date of Birth',
                        //  width: 132,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                     {
                         field: "EM_DOJ", title: 'Date of Joining',
                         // width: 180,
                         attributes: {
                             style: 'white-space: nowrap '
                         },
                         filterable: {
                             extra: false,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                    {
                        field: "EM_Designation", title: 'Designation',
                        // width: 180,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                ]
            });

            $('.k-grid-header').css({ 'background-color': 'white' });
            return false;
        }

    function NoOfLocations(obj) {
        debugger;
        var array = $(obj).attr('data-clId');
        var temp = new Array();
        if (array != "" && array != undefined) {
            temp = array.split(",");
        }
        //alert(eiddata);
        var eid = temp[0];
        entitymain = "";
        entitymain = temp[1];
            $('#brd').show();
            $('#export').show();
            entity = "";
            entity = "Locations";
        $('#liEntityVal').html(entity);
        $('#liEntity').html(entitymain);
            //client = $('#grdEntityDetails').find("td:first").next().text().trim();
            var authkey = $('#hdnAuthKey').val();
            var ProfileID = $('#hdnProfileID').val();
            $('#grdEntityDetails').hide();
            $('#gridLocations').show();
            $('#gridContrators').hide();
            $('#gridPrevJoinees').hide();
            $('#gridPrevResignee').hide();
            $('#gridEmployees').hide();
            $("#gridLocation").css("display", "block");


            var grid = $("#gridLocation").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            //url: '<% =tlConnectAPIUrl%>ClientDetail/NoOfLocations?id=A9DRG&status=A',
                            url: '<% =tlConnectAPIUrl%>ClientDetail/NoOfLocations?id=' + eid + '&status=A',
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('Authorization', authkey);
                                xhr.setRequestHeader('X-User-Id-1', ProfileID);
                            },
                            dataType: "json"
                        }
                    },
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    }
                },
                height: 513,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [

                {
                    field: "SM_Name", title: 'State',
                    width: 148,
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "LM_Name", title: 'Location',
                    width: 149,
                    attributes: {
                        style: 'white-space: nowrap;'
                    },
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "CL_BranchName", title: 'Branch',

                    width: 143,
                    format: "{0:dd-MMM-yyyy}",
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "CL_BranchAddress", title: 'Branch Address', filterable: {
                        extra: false,
                        width: 115,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },

                {
                    field: "CL_OfficeType", title: 'Office Type',
                    width: 132,
                    attributes: {
                        style: 'white-space: nowrap '
                    },
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                 {
                     field: "CL_EstablishmentType", title: 'Establishment Type',
                     width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
                ]
            });

            $('.k-grid-header').css({ 'background-color': 'white' });
            return false;
        }
    
       
    function NoOfLocation(entityid) {          
        entity = "";
        entity = "Locations";
        $('#liEntityVal').html(entity);
        $('#brd').show();
        $('#export').show();
        client = $('#grdEntityDetails').find("td:first").next().text().trim();      
        $('#grdEntityDetails').hide();
        $('#gridLocation').show();
        $('#gridEmployees').hide();
        $('#gridContrators').hide();
        $('#gridPrevJoinees').hide();
        $('#gridPrevResignee').hide();
        var authkey = $('#hdnAuthKey').val();
        var ProfileID = $('#hdnProfileID').val();

        $("#gridLocation").css("display", "block");
       
        var grid = $("#gridLocation").kendoGrid({
            dataSource: {              
                transport: {
                   
                    read: {
                        url: '<% =tlConnectAPIUrl%>ClientDetail/NoOfLocations?id=' + entityid + '&status=A',
                        beforeSend: function (xhr) {
                           xhr.setRequestHeader('Authorization', authkey);
                                xhr.setRequestHeader('X-User-Id-1', ProfileID);
                        },
                        dataType: "json"
                    }
                },
                pageSize: 10,
                schema: {
                    data: function (response) {                       
                        return response;
                    },
                    total: function (response) {
                        return response.length;
                    }
                }
            },
          
            sortable: true,
            filterable: true,
            columnMenu: true,
            pageable: true,
            reorderable: true,
            resizable: true,
            multi: true,
            selectable: true,
            columns: [

                {
                    field: "SM_Name", title: 'State',
                    width: 148,
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "LM_Name", title: 'Location',
                    width: 149,
                    attributes: {
                        style: 'white-space: nowrap;'
                    },
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "CL_BranchName", title: 'Branch',

                    width: 143,
                    format: "{0:dd-MMM-yyyy}",
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "CL_BranchAddress", title: 'Branch Address', filterable: {
                        extra: false,
                        width: 115,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },

                {
                    field: "CL_OfficeType", title: 'Office Type',
                    width: 132,
                    attributes: {
                        style: 'white-space: nowrap '
                    },
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                 {
                     field: "CL_EstablishmentType", title: 'Establishment Type',
                     width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },                
            ]
        });

        $('.k-grid-header').css({ 'background-color': 'white' });
        $('.k-grid').css({'position':''});
        return false;
    }
    
    function NoOfContrators(obj) {
        var array = $(obj).attr('data-clId');
        var temp = new Array();
        if (array != "" && array != undefined) {
            temp = array.split(",");
        }
        //alert(eiddata);
        var eid = temp[0];
        entitymain = "";
        entitymain = temp[1];
        
        entity = "";
        entity = "Contrators";
        $('#liEntityVal').html(entity);
        $('#liEntity').html(entitymain);
       

        $('#brd').show();
        $('#export').show();
      
        client = $('#grdEntityDetails').find("td:first").next().text().trim();
        $('#grdEntityDetails').hide();
        $('#gridContrators').show();
        $('#gridEmployees').hide();
        $('#gridLocations').hide();
        $('#gridPrevJoinees').hide();
        $('#gridPrevResignee').hide();
        var authkey = $('#hdnAuthKey').val();
        var ProfileID = $('#hdnProfileID').val();       
        var grid = $("#gridContrators").kendoGrid({
            dataSource: {                
                transport: {                   
                    read: {
                        url: '<% =tlConnectAPIUrl%>ClientDetail/NoOfContractors?id=' + eid,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', authkey);
                                xhr.setRequestHeader('X-User-Id-1', ProfileID);
                        },
                        dataType: "json"
                    }
                },
                pageSize: 10,
                schema: {
                    data: function (response) {                        
                        return response;
                    },
                    total: function (response) {
                        return response.length;
                    }                    
                }
            },

            sortable: true,
            filterable: true,
            columnMenu: true,
            pageable: true,
            reorderable: true,
            resizable: true,
            multi: true,
            selectable: true,
            columns:
                [

                {
                    field: "CC_ContracterID", title: 'Contractor ID',
                   // width: 148,
                    attributes: {
                        style: 'white-space: nowrap;'
                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "CC_ContractorName", title: 'Contractor Name',
                   // width: 149,
                    attributes: {
                        style: 'white-space: nowrap;'
                    },
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "CC_ContractorAddress", title: 'Contractor Address',

                    //width: 143,
                    format: "{0:dd-MMM-yyyy}",
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "SM_Name", title: 'State', filterable: {
                        extra: false,
                       // width: 115,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },

                {
                    field: "CC_BranchID", title: 'Branch',
                   // width: 132,
                    attributes: {
                        style: 'white-space: nowrap '
                    },
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                 {
                     field: "CC_ContractFrom", title: 'Contract From Date',
                     //width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
                 {
                     field: "CC_ContractTO", title: 'Contract To Date',
                    // width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
                 {
                     field: "CC_NoOfJoinees", title: 'No of Joinees',
                    // width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
                 {
                     field: "CC_NatureOfBusiness", title: 'Nature Of Business',
                    // width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },               
            ]
        });
        return false;
    }
   

    function NoOfPrevJoinees(obj) {
        var array = $(obj).attr('data-clId');
        var temp = new Array();
        if (array != "" && array != undefined) {
            temp = array.split(",");
        }
        //alert(eiddata);
        var eid = temp[0];
        entitymain = "";
        entitymain = temp[1];

        entity = "";
        entity = "Previous Month Joinees";
        $('#liEntityVal').html(entity);
        $('#liEntity').html(entitymain);
        $('#brd').show();
        $('#export').show();
        client = $('#grdEntityDetails').find("td:first").next().text().trim();
        $('#grdEntityDetails').hide();
        $('#gridPrevJoinees').show();
        $('#gridEmployees').hide();
        $('#gridLocations').hide();
        $('#gridPrevResignee').hide();
        $('#gridContrators').hide();
       
        if(<%= DateTime.Now.Month%> == 1)
        { 
            var year = <%= DateTime.Now.Year%>-1;
              var month = "12";
        }
        else
        {
            var year = '<%= DateTime.Now.Year%>';
              var month = <%= DateTime.Now.Month%>-1;
        }
      
        var authkey = $('#hdnAuthKey').val();
        var ProfileID = $('#hdnProfileID').val();
        var grid = $("#gridPrevJoinees").kendoGrid({
            dataSource: {               
                transport: {                   
                    read: {
                        url: '<% =tlConnectAPIUrl%>ClientDetail/PrevMonthJoinees?id=' + eid + '&Month='+month+'&Year='+year,
                        beforeSend: function (xhr) {
                             xhr.setRequestHeader('Authorization', authkey);
                                xhr.setRequestHeader('X-User-Id-1', ProfileID);
                        },
                        dataType: "json"
                    }
                },
                pageSize: 10,
                schema: {
                    data: function (response) {                      
                        return response;
                    }
                }
            },

            sortable: true,
            filterable: true,
            columnMenu: true,
            pageable: true,
            reorderable: true,
            resizable: true,
            multi: true,
            selectable: true,
            columns: [

                {
                    field: "EM_EmpID", title: 'Employee ID',
                    // width: 148,
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "EM_EmpName", title: 'Employee Name',
                    // width: 149,
                    attributes: {
                        style: 'white-space: nowrap;'
                    },
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "SM_Name", title: 'State',

                    // width: 143,
                    format: "{0:dd-MMM-yyyy}",
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "LM_Name", title: 'Location', filterable: {
                        extra: false,
                        // width: 115,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },

                {
                    field: "EM_Branch", title: 'Branch',
                    // width: 132,
                    attributes: {
                        style: 'white-space: nowrap '
                    },
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                 {
                     field: "EM_Gender", title: 'Gender',
                     // width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
                 {
                     field: "EM_DOB", title: 'Date of Birth',
                     // width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
                 {
                     field: "EM_DOJ", title: 'Date Of Joining',
                     // width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
                 {
                     field: "EM_Designation", title: 'Designation',
                     // width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
               
            ]
        });
        $('.k-grid-header').css({ 'background-color': 'white' });
        return false;
    }
   

    function NoOfPrevResignee(obj) {
        var array = $(obj).attr('data-clId');
        var temp = new Array();
        if (array != "" && array != undefined) {
            temp = array.split(",");
        }
        if(<%= DateTime.Now.Month%> == 1)
        { 
            var year = <%= DateTime.Now.Year%>-1;
              var month = "12";
        }
        else
        {
            var year = '<%= DateTime.Now.Year%>';
              var month = <%= DateTime.Now.Month%>-1;
        }
        //alert(eiddata);
        var eid = temp[0];
        entitymain = "";
        entitymain = temp[1];

        entity = "";
        entity = "Previous Month Resignee";
        $('#liEntityVal').html(entity);
        $('#liEntity').html(entitymain);
        $('#brd').show();
        $('#export').show();
        client = $('#grdEntityDetails').find("td:first").next().text().trim();
        $('#grdEntityDetails').hide();
        $('#gridPrevResignee').show();
        $('#gridEmployees').hide();
        $('#gridLocations').hide();
        $('#gridPrevJoinees').hide();
        $('#gridContrators').hide();
        var authkey = $('#hdnAuthKey').val();
        var ProfileID = $('#hdnProfileID').val();
        var grid = $("#gridPrevResignee").kendoGrid({
            dataSource: {               
                transport: {              
                    read: {
                        url: '<% =tlConnectAPIUrl%>ClientDetail/PrevMonthResignees?id=' + eid + '&Month='+month+'&Year='+year,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', authkey);
                                xhr.setRequestHeader('X-User-Id-1', ProfileID);
                        },
                        dataType: "json"
                    }
                },
                pageSize: 10,
                schema: {
                    data: function (response) {                      
                        return response;
                    },
                    total: function (response) {
                        return response.length;
                    }
                }
            },
          
            sortable: true,
            filterable: true,
            columnMenu: true,
            pageable: true,
            reorderable: true,
            resizable: true,
            multi: true,
            selectable: true,
            columns: [

                {
                    field: "EM_EmpID", title: 'Employee ID',
                    width: 148,
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }


                },
                {
                    field: "EM_EmpName", title: 'Employee Name',
                    width: 149,
                    attributes: {
                        style: 'white-space: nowrap;'
                    },
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "SM_Name", title: 'State',

                    width: 143,
                    format: "{0:dd-MMM-yyyy}",
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "LM_Name", title: 'Location', filterable: {
                        extra: false,
                        width: 115,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },

                {
                    field: "EM_Branch", title: 'Branch',
                    width: 132,
                    attributes: {
                        style: 'white-space: nowrap '
                    },
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                 {
                     field: "EM_Gender", title: 'Gender',
                     width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
                 {
                     field: "EM_DOB", title: 'Date of Birth',
                     width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
                 {
                     field: "EM_DOJ", title: 'Date of Joining',
                     width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },
                 {
                     field: "EM_Designation", title: 'Designation',
                     width: 180,
                     attributes: {
                         style: 'white-space: nowrap '
                     },
                     filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }
                 },               
            ]
        });
        $('.k-grid-header').css({ 'background-color': 'white' });
        return false;
    }
    
    function exportReport() {
        if (entity == "Locations")
            $('#gridLocation').getKendoGrid().saveAsExcel();
        else if (entity == "Employees")
            $('#gridEmployees').getKendoGrid().saveAsExcel();
        else if (entity == "Contrators")
            $('#gridContrators').getKendoGrid().saveAsExcel();
        else if (entity == "Previous Month Resignee")
            $('#gridPrevResignee').getKendoGrid().saveAsExcel();
        else if (entity == "Previous Month Joinees")
            $('#gridPrevJoinees').getKendoGrid().saveAsExcel();
        return false;
    };
</script>
<body class="bgColor-white">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smDashboardGraphDetail" runat="server"></asp:ScriptManager>

        <div>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="col-md-12">
                <asp:ValidationSummary ID="vsGraphDetailPage" runat="server" class="alert alert-block alert-danger fade in"
                    ValidationGroup="GraphDetailPageValidationGroup" />
                <asp:CustomValidator ID="cvGraphDetail" runat="server" EnableClientScript="False"
                    ValidationGroup="GraphDetailPageValidationGroup" Display="None" />
            </div>

             <div class="clearfix"></div>

            <div class="col-md-11">
                <ul class="breadcrumb" id="brd">
                    <li><a href="#" id="liEntity"></a></li>
                    <li><a href="#" id="liEntityVal"></a></li>
                </ul>
            </div>
            <div class="col-md-1">
                <button id="export" onclick="exportReport()" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width: 35px; height: 30px; background-color: white; border: none;"></button>
                <%--  <button type="button" id="btnback" class="btn btn-primary" style="margin-bottom:30px;">Back</button>--%>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 colpadding0">
                <asp:HiddenField ID="hdnAuthKey" runat="server" />
                  <asp:HiddenField ID="hdnProfileID" runat="server" />
                <asp:GridView runat="server" ID="grdEntityDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="8" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Entity ID">
                            <ItemTemplate>
                                <asp:Label ID="clientID" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ClientID") %>' ToolTip='<%# Eval("ClientID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Entity">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ClientName") %>' ToolTip='<%# Eval("ClientName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="No of Locations" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <%--<asp:LinkButton runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branches") %>' ToolTip='<%# Eval("Branches") %>' OnClientClick="javascript:return NoOfLocation();"></asp:LinkButton>
                                        
                                    <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branches") %>' ToolTip='<%# Eval("Branches") %>' OnClick='<%# "NoOfLocation(\""+ Eval("ClientID", "{0}") + "\")" %>'></asp:LinkButton>--%>

                                    <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="bottom" data-clId='<%# Eval("ClientID") +","+ Eval("ClientName")%>' Text='<%# Eval("Branches") %>' ToolTip='<%# Eval("Branches") %>' OnClientClick='javascript: return NoOfLocations(this);'></asp:LinkButton> 
                                           
                                    <%--<asp:LinkButton runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branches") %>' ToolTip='<%# Eval("Branches") %>' OnClientClick='<%# "NoOfLocation(\""+ Eval("ClientID", "{0}") + "\")" %>'></asp:LinkButton>--%>

                                     
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="No of Employees" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:LinkButton runat="server" ID="btnContractors" data-toggle="tooltip" data-clId='<%# Eval("ClientID") +","+ Eval("ClientName")%>' data-placement="bottom" Text='<%# Eval("Employees") %>' ToolTip='<%# Eval("Employees") %>' OnClientClick='javascript: return NoOfEmployees(this);'></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="No Of Contractors" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="false">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="bottom" data-clId='<%# Eval("ClientID") +","+ Eval("ClientName")%>'  Text='<%# Eval("Contractors") %>' ToolTip='<%# Eval("Contractors") %>' OnClientClick='javascript: return NoOfContrators(this);'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Previous Month Joinees" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="bottom" data-clId='<%# Eval("ClientID") +","+ Eval("ClientName")%>' Text='<%# Eval("Joinees") %>' ToolTip='<%# Eval("Joinees") %>' OnClientClick='javascript: return NoOfPrevJoinees(this);'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Previous Month Resignees" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="bottom" data-clId='<%# Eval("ClientID") +","+ Eval("ClientName")%>' Text='<%# Eval("Resignees") %>' ToolTip='<%# Eval("Resignees") %>' OnClientClick='javascript: return NoOfPrevResignee(this);'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
          
            <div class="clearfix"></div>

            <div id="gridLocation" style="border: none; margin-top: 5px"></div>
            <div id="gridEmployees" style="border: none; margin-top: 5px"></div>
            <div id="gridContrators" style="border: none; margin-top: 5px"></div>
            <div id="gridPrevJoinees" style="border: none; margin-top: 5px"></div>
            <div id="gridPrevResignee" style="border: none; margin-top: 5px"></div>

        </div>

        <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                        <button type="button" class="close" onclick="javascript:reloadTaskList();" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #f7f7f7;">
                        <iframe id="showdetails" src="about:blank" width="95%" height="75%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>