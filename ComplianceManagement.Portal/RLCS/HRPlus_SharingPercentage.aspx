﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlus_SharingPercentage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_SharingPercentage" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
</head>
<body style="background-color: white;">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.Add-text').datepicker({
                dateFormat: 'dd-mm-yy'
            });
        });
    </script>

    <form id="frmBooks" runat="server">
        <div class="row col-xs-12 col-sm-12 col-md-12">
            <asp:ValidationSummary ID="vsPopup" runat="server" DisplayMode="BulletList" class="alert alert-block alert-danger fade in" ValidationGroup="PopUpValidationGroup" />
            <asp:CustomValidator ID="cvCustomError" runat="server" EnableClientScript="False" ValidationGroup="PopUpValidationGroup" Display="None" />
        </div>

        <div class="row col-xs-12 col-sm-12 col-md-12">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <asp:DropDownListChosen ID="ddlCustomer" Width="100%" CssClass="form-control" runat="server"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged">
                </asp:DropDownListChosen>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6"></div>
        </div>

        <div class="clearfix"></div>

        <div class="row col-xs-12 col-sm-12 col-md-12">
            <asp:GridView ID="grd_details" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" DataKeyNames="ID" OnRowEditing="grd_details_RowEditing" OnRowUpdating="grd_details_RowUpdating"
                GridLines="None" PageSize="50" AllowPaging="false" AutoPostBack="true" CssClass="table" Width="100%" OnRowCancelingEdit="grd_details_RowCancelingEdit" OnRowDeleting="grd_details_RowDeleting"
                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Date (From)" SortExpression="FromDate" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblfdate" runat="server" DataFormatString="{0:dd/MM/yyyy}" Text='<%# Eval("FromDate")!=null? Convert.ToDateTime(Eval("FromDate")).ToShortDateString():""  %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtfdate" CssClass="form-control Add-text text-center" runat="server" Text='<%# Eval("FromDate")!=null? Convert.ToDateTime(Eval("FromDate")).ToShortDateString():"" %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="edit" runat="server" Display="Dynamic" ForeColor="Red" ControlToValidate="txtfdate" ErrorMessage="Please Enter Date"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txt_fdate" CssClass="form-control Add-text text-center" placeholder="DD/MM/YYYY" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="save" runat="server" Display="Dynamic" ForeColor="Red" ControlToValidate="txt_fdate" ErrorMessage="Please Enter Date"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Date (To)" SortExpression="ToDate" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbltdate" runat="server" Text='<%# Eval("ToDate")!=null? Convert.ToDateTime(Eval("ToDate")).ToShortDateString():"" %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txttdate" CssClass="form-control Add-text text-center" TextMode="DateTime" runat="server" Text='<%# Eval("ToDate")!=null? Convert.ToDateTime(Eval("ToDate")).ToShortDateString():"" %>'></asp:TextBox>                            
                            <asp:CompareValidator ID="CompareValidator2" runat="server" Operator="GreaterThanEqual" ValidationGroup="edit" ControlToValidate="txttdate" ControlToCompare="txtfdate" Display="Dynamic" ForeColor="Red" ErrorMessage="Date(To) should be Greater than Date(From)"></asp:CompareValidator>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txt_tdate" CssClass="form-control Add-text text-center" placeholder="DD/MM/YYYY" runat="server"></asp:TextBox>                            
                            <asp:CompareValidator ID="CompareValidator1" runat="server" Operator="GreaterThanEqual" ValidationGroup="save" ControlToValidate="txt_tdate" ControlToCompare="txt_fdate" Display="Dynamic" ForeColor="Red" ErrorMessage="Date(To) should be Greater than Date(From)"></asp:CompareValidator>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Discount (%)" SortExpression="DiscountPercentage" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblDiscountPercentage" runat="server" Text='<%# Bind("DiscountPercentage") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDiscountPercentage" CssClass="form-control text-right" TextMode="Number" runat="server" Text='<%# Bind("SharingPercentage") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDiscPer" ValidationGroup="edit" runat="server" Display="Dynamic" ForeColor="Red" ControlToValidate="txtDiscountPercentage" ErrorMessage="Enter Discount"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txt_DiscountPercentage" placeholder="Enter" CssClass="form-control text-right" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvFooterDiscPer" ValidationGroup="save" runat="server" Display="Dynamic" ForeColor="Red" ControlToValidate="txt_DiscountPercentage" ErrorMessage="Enter Discount"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Sharing (%)" SortExpression="SharingPercentage" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblSharingPercentage" runat="server" Text='<%# Bind("SharingPercentage") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSharingPercentage" CssClass="form-control text-right" TextMode="Number" runat="server" Text='<%# Bind("SharingPercentage") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="edit" runat="server" Display="Dynamic" ForeColor="Red" ControlToValidate="txtSharingPercentage" ErrorMessage="Enter Sharing(%)"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txt_SharingPercentage" placeholder="Enter" CssClass="form-control text-right" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="save" runat="server" Display="Dynamic" ForeColor="Red" ControlToValidate="txt_SharingPercentage" ErrorMessage="Enter Sharing(%)"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnedit" ImageUrl="../Images/edit_icon_new.png" CommandName="Edit" runat="server"></asp:ImageButton>
                            <asp:ImageButton ID="imgbtndel" ImageUrl="../Images/delete_icon_new.png" CommandName="Delete" runat="server"></asp:ImageButton>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btn_Update" CssClass="btn btn-primary" ValidationGroup="edit" runat="server" Text="Update" CommandName="Update" />
                            <asp:Button ID="btn_Cancel" CssClass="btn btn-primary" runat="server" Text="Cancel" CommandName="Cancel" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary" ValidationGroup="save" CausesValidation="True" CommandName="Save" OnClick="btnsave_Click" Text="Save" />
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <tr>
                        <th class="text-center">Date (From)</th>
                        <th class="text-center">Date (To)</th>
                        <th class="text-center">Discount (%)</th>
                        <th class="text-center">Sharing (%)</th>
                        <th class="text-center">Action</th>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_fdate" CssClass="form-control Add-text text-center" runat="server" /></td>                        
                        <td>
                            <asp:TextBox ID="txt_tdate" CssClass="form-control Add-text text-center" runat="server" /></td>                        
                        <td>
                            <asp:TextBox ID="txt_DiscountPercentage" CssClass="form-control text-right" runat="server" />                            
                        </td>                        
                        <td>
                            <asp:TextBox ID="txt_SharingPercentage" CssClass="form-control text-right" runat="server" />                            
                        </td>
                        <td class="text-center">
                            <asp:Button ID="btInsert" Text="Save" CssClass=" btn btn-primary" OnClick="btnsave_Click" CommandName="Save" runat="server" />
                        </td>
                    </tr>
                </EmptyDataTemplate>                
            </asp:GridView>
        </div>
    </form>
</body>
</html>
