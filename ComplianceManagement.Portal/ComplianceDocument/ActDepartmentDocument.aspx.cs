﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ComplianceDocument
{
    public partial class ActDepartmentDocument : System.Web.UI.Page
    {
        protected static string Path;
        protected static string Falg;
        protected static String SDate;
        protected static String LDate;
        protected static string UserId;
        protected static int UId;
        protected static int RoleID;
        protected static int CustId;
        protected static string Role;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                var UserDetails = UserManagement.GetByID(AuthenticationHelper.UserID);                
                Role = "PRA";
                if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
                {
                    if ((bool)UserDetails.IsHead)
                    {
                        Role = "DEPT";
                    }                    
                }
                //RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
        }
    }
}