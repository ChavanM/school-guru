﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GradingDisplay.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.GradingDisplay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" src="../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/drilldown.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/exporting.js"></script>
    <script type="text/javascript" src="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <link href="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../newcss/spectrum.css" rel="stylesheet" />
    <script type="text/javascript" src="../newjs/spectrum.js"></script>

    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .dashboard h1 {
            color: #555;
            padding: 0px 0 0px;
            margin: 0 0 0px;
        }

        .Dashboard-white-widget {
            padding: 0px 10px 10px;
        }
        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .modal-header, .panel-heading 
        {
              background: #fff;
              color: #688a7e;
              margin-inline-end: -1676px;
              height: 27px;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
            if (($('.table > tbody > tr').length) > 7) {
                var cal = ($('.table > tbody > tr').length * 45) + 200;
                window.parent.fchangeheight('', cal + "px");
            }
        });
    </script>
</head>
<body>

    <form id="form1" runat="server" style="margin-left: -14px;">


        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div class="row Dashboard-white-widget">
            <div class="dashboard">
                <div class="col-lg-12 col-md-12 ">
                    <section class="panel">
                        <header class="panel-heading tab-bg-primary ">
                            <div class="col-lg-5 col-md-5 colpadding0">
                                <h1 id="display" runat="server" style="height: 30px;background-color: #f8f8f8;height: 30px;background-color: #f8f8f8;margin-top: 0px;font-weight: 100;font-size: 21px;color: #666;margin-bottom: 12px;padding-left: 10px;padding-top: 3px;margin-inline-end: -32px;">Grading Reports</h1>
                            </div> 
 <div class="clearfix" style="clear: both; height: 10px;"></div>                             
                            <ul id="rblRole1" class="nav nav-tabs">

                                <li class="active" id="liDetails" runat="server">
                                    <asp:LinkButton ID="lnkDetails" OnClick="ShowDetails" runat="server">Details</asp:LinkButton>
                                </li>
                                <li class="" id="liGraph" runat="server">
                                    <asp:LinkButton ID="lnkGraph" OnClick="ShowGraph" runat="server">Graph</asp:LinkButton>
                                </li>
                            </ul>
                        </header>

                        <div class="clearfix" style="clear: both; height: 10px;"></div>
                        <asp:HiddenField ID="highcolor" runat="server" Value="#FF7473" />
                        <asp:HiddenField ID="mediumcolor" runat="server" Value="#FFC952" />
                        <asp:HiddenField ID="lowcolor" runat="server" Value="#1FD9E1" />
                        <asp:HiddenField ID="criticalcolor" runat="server" Value="#f1c232" />

                        

                        <div id="Details" runat="server">
                            <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="updateProgress" runat="server">
                                        <ProgressTemplate>
                                            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                                                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="col-md-2 colpadding0 entrycount">
                                        <div class="col-md-3 colpadding0">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="10" Selected="True" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin-bottom: 4px;border-style: solid;border-width: thin;border-color: lightgray;">
                                        <asp:GridView runat="server" ID="GridStatutory" AutoGenerateColumns="false" AllowSorting="true" GridLines="None"
                                            CssClass="table" AllowPaging="true" PageSize="10" Width="100%" OnRowDataBound="GridStatutory_RowDataBound">
                                            <Columns>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLocation" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Compliance">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                            <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Performer">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("User") %>' ToolTip='<%# Eval("User") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Due Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>' ToolTip='<%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="For Month">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                            <asp:Label ID="lblForMonth" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                            <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />

                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>

                                            <EmptyDataTemplate>
                                                No Records Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div style="margin-bottom: 4px">
                                        <asp:GridView runat="server" ID="GridInternalCompliance" AutoGenerateColumns="false" GridLines="none" AllowSorting="true"
                                            CssClass="table" AllowPaging="true" PageSize="10" Width="100%" OnRowDataBound="GridInternalCompliance_RowDataBound">
                                            <%--OnPageIndexChanging="grdSummaryDetails_PageIndexChanging" DataKeyNames="ID"--%>
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLocation" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Compliance">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                            <asp:Label ID="lblSections" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Performer">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("User") %>' ToolTip='<%# Eval("User") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Due Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("InternalScheduledOn")!= null?((DateTime)Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy"):""%>' ToolTip='<%# Eval("InternalScheduledOn")!= null?((DateTime)Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="For Month">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                            <asp:Label ID="lblForMonth" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                            <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>

                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />

                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>

                                            <EmptyDataTemplate>
                                                No Records Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0" style="margin-top: -6px;">
                                        <div class="col-md-6 colpadding0" style="float: right;">
                                            <div class="table-paging" style="margin-bottom: 20px;">
                                                <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                                <div class="table-paging-text">
                                                    <p>
                                                        <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                                    </p>
                                                </div>

                                                <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div id="Graph" runat="server" style="display: none;">
                            <div style="margin-bottom: 4px">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-2"></div>
                                        <div id="perRiskStackedColumnChartDiv" class="col-md-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <script>
            // replace/populate colors from user saved profile

            var perRiskStackedColumnChartColorScheme = {
                high: "<%=highcolor.Value %>",
                medium: "<%=mediumcolor.Value %>",
                low: "<%=lowcolor.Value %>",
                critical: "<%=criticalcolor.Value %>"
              
            };

            // function executes when page is ready
            // main documentReady function starts
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                var perRiskStackedColumnChart = Highcharts.chart('perRiskStackedColumnChartDiv', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Per Risk',
                        style: {
                            // "font-family": 'Helvetica',
                            display: 'none'
                        },
                    },
                    subtitle: {
                        //text: 'Click on graph to view documents',
                        text: '',
                        style: {
                            // "font-family": 'Helvetica',
                        }
                    },
                    xAxis: {
                        categories: ['Critical' ,'High', 'Medium', 'Low' ]
                    },
                    yAxis: {
                        title: {
                            text: 'Number of Compliances',
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: 'gray'
                            }
                        }
                    },
                    tooltip: {
                        hideDelay: 0,
                        backgroundColor: 'rgba(247,247,247,1)',
                        headerFormat: '<b>{point.x}</b><br/>',
                        pointFormat: '{series.name}: {point.y}'
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                color: 'white',
                                style: {

                                    textShadow: false,
                                }
                            },
                            cursor: 'pointer'
                        },
                    },
              <%=perGradingRiskChart%>
                });


                // change color in color picker according to chart selected
                $('input[name=radioButton]').change(function () {
                    // destroy all color pickers
                    $('#highColorPicker').simplecolorpicker('destroy');
                    $('#mediumColorPicker').simplecolorpicker('destroy');
                    $('#lowColorPicker').simplecolorpicker('destroy');
                    $('#criticalColorPicker').simplecolorpicker('destroy');


                    // setting the colors for risks according to chart selected
                    var chart = $('input[name=radioButton]:checked').val();
                    switch (chart) {
                        case "perFunction":
                            $('#highColorPicker').val(perFunctionChartColorScheme.high);
                            $('#mediumColorPicker').val(perFunctionChartColorScheme.medium);
                            $('#lowColorPicker').val(perFunctionChartColorScheme.low);
                            $('#criticalColorPicker').val(perFunctionChartColorScheme.critical);
                            break;
                        case "perRisk":
                            $('#highColorPicker').val(perRiskStackedColumnChartColorScheme.high);
                            $('#mediumColorPicker').val(perRiskStackedColumnChartColorScheme.medium);
                            $('#lowColorPicker').val(perRiskStackedColumnChartColorScheme.low);
                            $('#criticalColorPicker').val(perFunctionChartColorScheme.critical);
                            break;
                        case "perStatus":
                            $('#highColorPicker').val(perStatusChartColorScheme.high);
                            $('#mediumColorPicker').val(perStatusChartColorScheme.medium);
                            $('#lowColorPicker').val(perStatusChartColorScheme.low);
                            $('#criticalColorPicker').val(perStatusChartColorScheme.critical);
                            break;
                        default:
                            $('#highColorPicker').val('#7CB5EC');
                            $('#mediumColorPicker').val('#434348');
                            $('#lowColorPicker').val('#90ED7D');
                            $('#criticalColorPicker').val('#f1c232');
                    }

                    // initialise the color piskers again
                    $('#highColorPicker').simplecolorpicker({ picker: true });
                    $('#mediumColorPicker').simplecolorpicker({ picker: true });
                    $('#lowColorPicker').simplecolorpicker({ picker: true });
                    $('#criticalColorPicker').simplecolorpicker({ picker: true });
                });


                //apply color scheme to all charts [Apply to All] click event handler
                $('#applyToAllButton').click(function () {
                });

                $('#showModal').click(function () {
                    $('#modalDiv').modal();
                });

            });   //	main documentReady function END
        </script>

    </form>
</body>
</html>
