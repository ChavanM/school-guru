﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class KendoGridNew : System.Web.UI.Page
    {
        protected static DateTime sdate;
        protected static DateTime edate;
        protected static int Customerbranchid;
        protected static int UId;
        protected static int CustId;
        protected static string CId;
        protected static int customerid;
        protected static string Path;
        protected static string ComplianceTypeFlag;
        protected static string Type;
        protected List<Int32> roles;
        protected static bool IsApprover = false;
        protected bool DeptHead = false;
        protected static string perGradingRiskChart;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    Path = ConfigurationManager.AppSettings["KendoPathApp"];           
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    CId = Convert.ToString(AuthenticationHelper.CustomerID);
                    if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                    {
                        customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["type"]))
                    {
                        Type = Request.QueryString["type"];
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["StartDate"]))
                    {
                        sdate = Convert.ToDateTime(Request.QueryString["StartDate"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["EndDate"]))
                    {
                        edate = Convert.ToDateTime(Request.QueryString["EndDate"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Customerbanchid"]))
                    {
                        Customerbranchid = Convert.ToInt32(Request.QueryString["Customerbanchid"]);
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
    }
    }


             