﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewAssignmentReportKendo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.NewAssignmentReportKendo" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <link href="../NewCSS/basickendo.css" rel="stylesheet" />
   
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
     <style type="text/css">
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: inset 0 0 1px 1px #14699f;
        }
        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px #d7dae0;
            box-shadow: inset 0 0 1px 1px white;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
            overflow: hidden;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px;
            color: inherit;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }
              input[type=checkbox], input[type=radio] {
    margin: 4px 4px 0;
    line-height: normal;
}
        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: -4px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }
        .k-multiselect-wrap > .k-i-close {
    top: 8px;
    margin-right: 10px;
        }

        .k-multiselect-wrap .k-input {
               /*padding-top:6px;*/
               display: inherit !important;
           }
    </style>

        <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

  <script type="text/javascript">

        $(document).ready(function () {
           
        
            BindBranch();
            BindTypeNew();
            BindCategory();
            BindFrequency();
            BindAct();
            BindGrid();
        });

        function BindGrid() {

            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetAssignmentReportDataMGMTTotal?Customerid=<%=CustId%>&type=<%=Internalsatutory%>&roles=<%=roles%>&Uid=<% =UId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    },
                    pageSize: 12
                },
                excel: {
                    allPages: true,
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    {
                        field: "ComplianceID", title: "Compliance ID", width: "10%",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }, autoWidth: true,
                        }
                    },
                    {
                        field: "Branch", title: 'Branch',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Performer", title: 'Performer',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap; width:100px;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Reviewer", title: 'Reviewer',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap; width:100px;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }

                    },
                    {
                        hidden: true,
                        field: "Approver", title: 'Approver',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap; width:100px;'
                        }, filterable: {
                            extra: false, multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }

                    },
                    {
                        hidden: true,
                        field: "StartDate", title: 'StartDate',
                        type: "date",
                        width: "10%",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            search: true,
                            extra: false,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "ComplianceType", title: 'ComplianceType',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap; width:100px;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }

                    },
                    {
                        hidden: true,
                        field: "State", title: 'State',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        hidden: true,
                        field: "ComplianceCategoryName", title: 'Compliance Category',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },


                ]
            });
            $("#grid").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        return e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        return e.preventDefault();

                }
            }).data("kendoTooltip");

        }

        function BindBranch() {
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkAll: true,                
                autoWidth: true,
                autoClose: false,
                //checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filterLocation', 'Location');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }

        function BindAct() {

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: true,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "Name",
                optionLabel: "Act",
                change: function (e) {
                    FilterAll();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =roles%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                },
                dataBound: function (e) {
                    e.sender.list.width("400");
                }
            });
        }

        function BindCategory() {
            $("#dropdownCategory").kendoDropDownTree({
                placeholder: "Category",
                checkboxes: true,
                autoClose: false,
                autoWidth: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "Id",
                filter: "contains",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdownCategory', 'filterCategory', 'Category');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetAssignmentCategoryDataReport?Customerid=<%=CustId%>&type=All&roles=<%=roles%>&Uid=<% =UId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });
        }

        function BindTypeNew() {
            $("#dropdownTypeNew").kendoDropDownTree({
                placeholder: "Type",
                checkboxes: true,
                autoClose: false,
                autoWidth: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    FilterAll();
                    fCreateStoryBoard('dropdownTypeNew', 'filterTypeNew', 'Type');
                },
                dataSource: [
                    { text: "Statutory", value: "Statutory" },
                    { text: "Statutory CheckList", value: "StatutoryCheckList" },
                    { text: "EventBased", value: "Event" },
                    //{ text: "Internal", value: "Internalstatutory" },
                    //{ text: "Internal CheckList", value: "InternalCheckList" },

                ]
            });

        }

        function BindFrequency() {

            $("#dropdownFrequency").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Frequency",
                change: function (e) {
                    FilterAll();
                },
                dataSource: [

                    { text: "Monthly", value: "Monthly" },
                    { text: "Quarterly", value: "Quarterly" },
                    { text: "HalfYearly", value: "HalfYearly" },
                    { text: "Annual", value: "Annual" },
                    { text: "FourMonthly", value: "FourMonthly" },
                    { text: "TwoYearly", value: "TwoYearly" },
                    { text: "SevenYearly", value: "SevenYearly" },
                    { text: "Daily", value: "Daily" },
                    { text: "Weekly", value: "Weekly" },
                ]
            });
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoard('dropdowntree', 'filterLocation', 'Location');
            fCreateStoryBoard('dropdownTypeNew', 'filterTypeNew', 'Type');
            fCreateStoryBoard('dropdownCategory', 'filterCategory', 'Category');
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filterLocation') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterTypeNew') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                //if (buttontest.length > 10) {
                //    buttontest = buttontest.substring(0, 10).concat("...");
                //}
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:1px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function FilterAll() {

            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;

            var TypeList = $("#dropdownTypeNew").data("kendoDropDownTree")._values;

            var CategoryList = $("#dropdownCategory").data("kendoDropDownTree")._values;

            if (locationlist.length > 0 ||
                TypeList.length > 0 ||
                ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) ||
                CategoryList.length>0 ||
                ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != undefined)) {

                var finalSelectedfilter = { logic: "and", filters: [] };

                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "BranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }

                if (TypeList.length > 0) {

                    var TypeFilter = { logic: "or", filters: [] };

                    $.each(TypeList, function (i, v) {
                        TypeFilter.filters.push({
                            field: "Type", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(TypeFilter);
                }

                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };
                    ActFilter.filters.push({
                        field: "ActName", operator: "eq", value: $("#dropdownACT").val()
                    });
                    finalSelectedfilter.filters.push(ActFilter);
                }

                if ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != undefined) {
                    var FrequencyFilter = { logic: "or", filters: [] };
                    FrequencyFilter.filters.push({
                        field: "Frequency", operator: "Contains", value: $("#dropdownFrequency").val()
                    });
                    finalSelectedfilter.filters.push(FrequencyFilter);
                }
                if (CategoryList.length > 0) {

                    var catFilter = { logic: "or", filters: [] };

                    $.each(CategoryList, function (i, v) {
                        catFilter.filters.push({
                            field: "ComplianceCategoryId", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(catFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

        function ClearAllFilterMain(e) {

            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownTypeNew").data("kendoDropDownTree").value([]);
            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#dropdownCategory").data("kendoDropDownTree").value([]);
            $("#dropdownFrequency").data("kendoDropDownList").select(0);
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function exportReportAdvanced(e) {

            var ReportName = "";
            var ReportName1 = "";
            var FileName = "";

            var ReportName1 = $("#dropdownTypeNew").data("kendoDropDownTree").value();

            if (ReportName1.length != 0) {
                for (var i = 0; i < ReportName1.length; i++) {
                    if (ReportName1[i] == "Internalstatutory") {
                        ReportName1[i] = "Internal";
                    }
                    if (ReportName1[i] == "Event") {
                        ReportName1[i] = "EventBased";
                    }
                    ReportName = ReportName + ReportName1[i] + " and ";
                    FileName = FileName + ReportName1[i] + "&";
                }
                ReportName = ReportName.substr(0, ReportName.length - 4) + "Report";
                FileName = FileName.substr(0, FileName.length - 1) + "Report";
            }

            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values == "Statutory,StatutoryCheckList,Event,Internalstatutory,InternalCheckList") {
                ReportName = "All Compliance Report";
                FileName = "AllComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values == "") {
                ReportName = "All Compliance Report";
                FileName = "AllComplianceReport";
            }

            var todayDate = moment().format('DD-MMM-YYYY');

            var grid = $("#grid").getKendoGrid();


            var rows = [
                {
                    cells: [
                        { value: "Customer Name:", bold: true },
                        { value: '<% =CustomerName%>' }
                    ]
                },
                {
                    cells: [
                        { value: "Report Name:", bold: true },
                        { value: ReportName }
                    ]
                },
                {
                    cells: [
                        { value: "Report Generated On:", bold: true },
                        { value: todayDate }
                    ]
                },
                {
                    cells: [
                        { value: "" }
                    ]
                },
                {
                    cells: [
                        { value: "S.No.", bold: true },
                        { value: "Compliance ID", bold: true },
                        { value: "Branch", bold: true },
                        { value: "Act Name", bold: true },
                        { value: "Short Description", bold: true },
                        { value: "Description", bold: true },
                        { value: "Short Form", bold: true },
                        { value: "Section", bold: true },
                        { value: "Risk", bold: true },
                        { value: "Frequency", bold: true },
                        { value: "Compliance Type", bold: true },
                        { value: "Performer", bold: true },
                        { value: "Reviewer", bold: true },
                        { value: "Approver", bold: true },
                        { value: "State", bold: true },
                        { value: "Compliance Category", bold: true },
                        { value: "Compliance Type Name", bold: true },
                        { value: "Start Date", bold: true },
                        { value: "Label", bold: true },
                        { value: "Department", bold: true },

                    ]
                }
            ];

            var trs = grid.dataSource;
            var filteredDataSource = new kendo.data.DataSource({
                data: trs.data(),
                filter: trs.filter()
            });

            filteredDataSource.read();
            var data = filteredDataSource.view();
            for (var i = 0; i < data.length; i++) {
                var dataItem = data[i];
                var rowHeightDescr = 0;
                var rowHeightAct = 0;
                if (dataItem.ActName != null && dataItem.ActName != "") {
                    rowHeightAct = dataItem.ActName.length > 27 ? Math.ceil(dataItem.ActName.length / 27) * 20 : 20;
                }
                if (dataItem.Description != null && dataItem.Description != "") {
                    rowHeightDescr = dataItem.Description.length > 27 ? Math.ceil(dataItem.Description.length / 27) * 20 : 20;
                }
                var rowHeight = 20;
                if (rowHeightAct > rowHeightDescr) {
                    rowHeight = rowHeightAct;
                }
                else {
                    rowHeight = rowHeightDescr;
                }
                rows.push({
                    cells: [ // dataItem."Whatever Your Attributes Are"
                        { value: '' },
                        { value: dataItem.ComplianceID },
                        { value: dataItem.Branch },
                        { value: dataItem.ActName },
                        { value: dataItem.ShortDescription },
                        { value: dataItem.Description },
                        { value: dataItem.ShortForm },
                        { value: dataItem.Section },
                        { value: dataItem.Risk },
                        { value: dataItem.Frequency },
                        { value: dataItem.ComplianceType },
                        { value: dataItem.Performer },
                        { value: dataItem.Reviewer },
                        { value: dataItem.Approver },
                        { value: dataItem.State },
                        { value: dataItem.ComplianceCategoryName },
                        { value: dataItem.ComplianceTypeName },
                        { value: dataItem.StartDate, format: "dd-MMM-yyyy" },
                        { value: dataItem.SequenceID },
                        { value: dataItem.Department }
                    ],
                    height: rowHeight
                });

            }
            for (var i = 4; i < rows.length; i++) {
                for (var j = 0; j < 20; j++) {
                    rows[i].cells[j].borderBottom = "#000000";
                    rows[i].cells[j].borderLeft = "#000000";
                    rows[i].cells[j].borderRight = "#000000";
                    rows[i].cells[j].borderTop = "#000000";
                    rows[i].cells[j].hAlign = "left";
                    rows[i].cells[j].vAlign = "top";
                    rows[i].cells[j].wrap = true;

                    if (i != 4) {
                        rows[i].cells[0].value = i - 4;
                    }
                    if (i == 4) {
                        rows[4].cells[j].background = "#A9A9A9";
                    }
                }
            }

            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        columns: [
                            { autoWidth: true },
                            { autoWidth: true },
                            { width: 200 },
                            { width: 300 },
                            { width: 250 },
                            { width: 400 },
                            { width: 300 },
                            { width: 200 },

                            { width: 120 },
                            { width: 120 },
                            { width: 120 },
                            { width: 120 },
                            { width: 120 },
                            { width: 120 },
                            { width: 120 },
                            { width: 150 },
                            { width: 100 },
                            { width: 100 },
                            { width: 140 }
                        ],
                        title: FileName,
                        rows: rows
                    },
                ]
            });

            var nameOfPage = FileName;
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + ".xlsx" });

            e.preventDefault();
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
  <div id="example">
        <div style="margin: 0.4% 0% 0.5%;">
            <input id="dropdownTypeNew" data-placeholder="Status" style="width: 10%;margin-right:0.8%" />
            <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 20%;margin-right:0.8%" />
            <input id="dropdownCategory" data-placeholder="Category" style="width: 15%;margin-right:0.8%" />
            <input id="dropdownFrequency" data-placeholder="Frequency" style="width: 15%;margin-right:0.8%" />
             <input id="dropdownACT" data-placeholder="Act" style="width: 18.7%;margin-right:0.8%" />
            <button id="exportAdvanced" onclick="exportReportAdvanced(event)" style="margin-right:10px;height:30px" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
            <button id="ClearfilterMain" onclick="ClearAllFilterMain(event)" style="height:30px"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
      </div>
  
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterLocation">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterTypeNew">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterCategory">&nbsp;</div>
        <div id="grid"></div>
    </div>
    </form>
</body>
</html>
