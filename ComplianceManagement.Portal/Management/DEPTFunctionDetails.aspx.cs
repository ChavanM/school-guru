﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class DEPTFunctionDetails : System.Web.UI.Page
    {
        protected static int customerid;
        protected static int BID;
        protected static string satutoryinternal;
        protected static bool IsApprover = false;
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                    {
                        customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        satutoryinternal = Request.QueryString["Internalsatutory"];
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                    {
                        IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                    {
                        BID = Convert.ToInt32(Request.QueryString["branchid"]);
                        Branchlist.Clear();
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        GetAllHierarchy(customerID, Convert.ToInt32(BID));
                        Branchlist.ToList();
                    }
                    BindLocationFilter();
                    BindDepartment();
                    BindDetailView();
                    Branchlist.Clear();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindDetailView()
        {
            try
            {
                string DeptHead = null;
                int CustBranchID = -1;
                int departmentid = -1;
                if (tvFilterLocation.SelectedValue != "" && tvFilterLocation.SelectedValue != "-1")
                {
                    CustBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (ddlDepartment.SelectedValue != "" && ddlDepartment.SelectedValue != "-1")
                {
                    departmentid = Convert.ToInt32(ddlDepartment.SelectedValue);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["IsDeptHead"])))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }

                if (satutoryinternal == "Statutory")
                {
                    if (DeptHead != null)
                    {                        
                        var detailsview = DepartmentHeadManagement.GetFunctionDetailsStatutory_DeptHead(AuthenticationHelper.UserID, CustBranchID, Branchlist,"S", IsApprover);

                        if (departmentid != -1)
                            detailsview = detailsview.Where(Entry => Entry.Id == departmentid).ToList();
                        grdSummaryDetails.DataSource = detailsview;
                        grdSummaryDetails.DataBind();
                    }                  
                }
                else if (satutoryinternal == "Internal")
                {                    
                    var detailsview = DepartmentHeadManagement.GetFunctionDetailsStatutory_DeptHead(AuthenticationHelper.UserID, CustBranchID, Branchlist, "I", IsApprover);

                    if (departmentid != -1)
                        detailsview = detailsview.Where(Entry => Entry.Id == departmentid).ToList();

                    grdSummaryDetails.DataSource = detailsview;
                    grdSummaryDetails.DataBind();
                }
                Branchlist.Clear();
                UpDetailView.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindDepartment()
        {
            try
            {
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";
                int CustBranchID = -1;
                if (tvFilterLocation.SelectedValue != null && tvFilterLocation.SelectedValue != "")
                {
                    CustBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                List<sp_ComplianceAssignedDepartment_Result> data = new List<sp_ComplianceAssignedDepartment_Result>();
                if (satutoryinternal == "Statutory")
                {
                    data = CompDeptManagement.GetNewAll(AuthenticationHelper.UserID, CustBranchID, Branchlist, "S");
                }
                else if (satutoryinternal == "Internal")
                {
                    data = CompDeptManagement.GetNewAll(AuthenticationHelper.UserID, CustBranchID, Branchlist, "I");
                }
                ddlDepartment.DataSource = data;
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("Select Department", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

     
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                tvFilterLocation.Nodes.Clear();
                string isstatutoryinternal = "";

                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    satutoryinternal = Request.QueryString["Internalsatutory"];
                }
                if (satutoryinternal == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (satutoryinternal == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;                    
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode node = tvFilterLocation.Nodes[0];
                node.Selected = true;

                ddlDepartment.SelectedValue = "-1";

                lblAdvanceSearchScrum.Text = String.Empty;
                divAdvSearch.Visible = false;

                tvFilterLocation_SelectedNodeChanged(sender, e);
               
                string DeptHead = null;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["IsDeptHead"])))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }                
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    satutoryinternal = Request.QueryString["Internalsatutory"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    BID = Convert.ToInt32(Request.QueryString["branchid"]);                   
                }                
                if (DeptHead == "1")
                {                    
                    Response.Redirect("~/Management/DEPTFunctionDetails.aspx?customerid=" + customerid + "&branchid="+ BID + "&Internalsatutory=" + satutoryinternal + "&IsApprover=" + IsApprover + "&IsDeptHead=1", false);
                }
                else
                {
                    Response.Redirect("~/Management/DEPTFunctionDetails.aspx?customerid=" + customerid + "&branchid=" + BID + "&Internalsatutory=" + satutoryinternal + "&IsApprover=" + IsApprover + "", false);                    
                }
                

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;
                int location = 0;
                int Category = 0;

                if (tvFilterLocation.SelectedValue != "-1")
                {
                    location = tvFilterLocation.SelectedNode.Text.Length;

                    Branchlist.Clear();
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    Branchlist.ToList();
                    if (location >= 30)
                        lblAdvanceSearchScrum.Text = "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text.Substring(0, 30) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text;
                }

                if (ddlDepartment.SelectedValue != "-1")
                {
                    Category = ddlDepartment.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Department: </b>" + ddlDepartment.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Department: </b>" + ddlDepartment.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Department: </b>" + ddlDepartment.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Department: </b>" + ddlDepartment.SelectedItem.Text;
                    }
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                    BindDetailView();
                }
                else
                {
                    divAdvSearch.Visible = false;
                    BindDetailView();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected int GetCount(int DepartmentID, String CountType)
        {
            try
            {
                string DeptHead = null;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["IsDeptHead"])))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }
                int CustBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                int Count = 0;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (satutoryinternal == "Statutory")
                    {
                        if (DeptHead == "1")
                        {
                            List<sp_ComplianceInstanceAssignmentViewCount_DeptHead_Result> TotalList = new List<sp_ComplianceInstanceAssignmentViewCount_DeptHead_Result>();
                            if (IsApprover == true)
                            {
                                TotalList = (from row in entities.sp_ComplianceInstanceAssignmentViewCount_DeptHead(AuthenticationHelper.UserID, DepartmentID, "APPR")
                                             select row).ToList();
                            }
                            else
                            {
                                TotalList = (from row in entities.sp_ComplianceInstanceAssignmentViewCount_DeptHead(AuthenticationHelper.UserID, DepartmentID, "DEPT")
                                             select row).ToList();
                            }

                            if (Branchlist.Count > 0)
                            {
                                TotalList = TotalList.Where(Entry => Branchlist.Contains(Entry.CustomerBranchID)).ToList();
                            }
                            else
                            {
                                if (CustBranchID != -1)
                                    TotalList = TotalList.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();
                            }                            
                            TotalList = TotalList.Where(Entry => Entry.DepartmentID == DepartmentID).ToList();

                            if (CountType == "U")
                                Count = TotalList.Select(Entry => Entry.UserID).Distinct().Count();

                            if (CountType == "C")
                                Count = TotalList.Select(Entry => Entry.ComplianceId).Distinct().Count();

                            if (CountType == "L")
                                Count = TotalList.Select(Entry => Entry.CustomerBranchID).Distinct().Count();
                        }
                       
                    }
                    else if (satutoryinternal == "Internal")
                    {
                        if (DeptHead == "1")
                        {
                            List<sp_InternalComplianceInstanceAssignmentViewCount_DeptHead_Result> TotalList = new List<sp_InternalComplianceInstanceAssignmentViewCount_DeptHead_Result>();
                            if (IsApprover == true)
                            {
                                TotalList = (from row in entities.sp_InternalComplianceInstanceAssignmentViewCount_DeptHead(AuthenticationHelper.UserID, DepartmentID, "APPR")
                                             select row).ToList();
                            }
                            else
                            {
                                TotalList = (from row in entities.sp_InternalComplianceInstanceAssignmentViewCount_DeptHead(AuthenticationHelper.UserID, DepartmentID, "DEPT")
                                             select row).ToList();
                            }

                            if (Branchlist.Count > 0)
                            {
                                TotalList = TotalList.Where(Entry => Branchlist.Contains(Entry.CustomerBranchID)).ToList();
                            }
                            else
                            {
                                if (CustBranchID != -1)
                                    TotalList = TotalList.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();
                            }                          
                            TotalList = TotalList.Where(Entry => Entry.DepartmentID == DepartmentID).ToList();

                            if (CountType == "U")
                                Count = TotalList.Select(Entry => Entry.UserID).Distinct().Count();

                            if (CountType == "C")
                                Count = TotalList.Select(Entry => Entry.InternalComplianceID).Distinct().Count();

                            if (CountType == "L")
                                Count = TotalList.Select(Entry => Entry.CustomerBranchID).Distinct().Count();
                        }                        
                    }

                    return Count;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdSummaryDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        protected void lnkCompliances_Click(object sender, EventArgs e)
        {
            try
            {
                string DeptHead = null;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["IsDeptHead"])))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }
                LinkButton btn = sender as LinkButton;
                GridViewRow row = btn.NamingContainer as GridViewRow;
                string pk = grdSummaryDetails.DataKeys[row.RowIndex].Values[0].ToString();
                if (DeptHead == "1")
                {
                    Response.Redirect("~/Management/ComplianceDetailsAPI.aspx?customerid=" + customerid + "&Internalsatutory=" + satutoryinternal + "&Category=" + pk + "&Branchid=" + tvFilterLocation.SelectedNode.Value + "&IsApprover=" + IsApprover + "&IsDeptHead=1", false);
                }
                else
                {
                    Response.Redirect("~/Management/ComplianceDetailsAPI.aspx?customerid=" + customerid + "&Internalsatutory=" + satutoryinternal + "&Category=" + pk + "&Branchid=" + tvFilterLocation.SelectedNode.Value + "&IsApprover=" + IsApprover + "", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkUsers_Click(object sender, EventArgs e)
        {
            try
            {
                string DeptHead = null;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["IsDeptHead"])))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }
                LinkButton btn = sender as LinkButton;
                GridViewRow row = btn.NamingContainer as GridViewRow;
                string pk = grdSummaryDetails.DataKeys[row.RowIndex].Values[0].ToString();
                if (DeptHead == "1")
                {
                    Response.Redirect("~/Management/ManagementusersAPI.aspx?customerid=" + customerid + "&Internalsatutory=" + satutoryinternal + "&Category=" + pk + "&Branchid=" + tvFilterLocation.SelectedNode.Value + "&IsApprover=" + IsApprover + "&IsDeptHead=1", false);
                }
                else
                {
                    Response.Redirect("~/Management/ManagementusersAPI.aspx?customerid=" + customerid + "&Internalsatutory=" + satutoryinternal + "&Category=" + pk + "&Branchid=" + tvFilterLocation.SelectedNode.Value + "&IsApprover=" + IsApprover + "", false);
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}