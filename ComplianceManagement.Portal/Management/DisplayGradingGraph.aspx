﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisplayGradingGraph.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.DisplayGradingGraph" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" src="../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/drilldown.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/exporting.js"></script>
    <script type="text/javascript" src="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <link href="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../newcss/spectrum.css" rel="stylesheet" />
    <script type="text/javascript" src="../newjs/spectrum.js"></script>

    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
 
</head>
<body>

    <form id="form1" runat="server" style="margin-left: -14px;">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div class="row Dashboard-white-widget">
            <div class="dashboard">
                <div class="col-lg-12 col-md-12 ">
                    <section class="panel">
                        
                        <div class="clearfix" style="clear: both; height: 10px;"></div>
                        <asp:HiddenField ID="highcolor" runat="server" Value="#FF7473" />
                        <asp:HiddenField ID="mediumcolor" runat="server" Value="#FFC952" />
                        <asp:HiddenField ID="lowcolor" runat="server" Value="#1FD9E1" />
                        <asp:HiddenField ID="criticalcolor" runat="server" Value="#f1c232" />
                       
                   
                        <div id="Graph" runat="server">
                            <div style="margin-bottom: 4px">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-2"></div>
                                        <div id="perRiskStackedColumnChartDiv" class="col-md-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <script>
            // replace/populate colors from user saved profile

            var perRiskStackedColumnChartColorScheme = {
                high: "<%=highcolor.Value %>",
                medium: "<%=mediumcolor.Value %>",
                low: "<%=lowcolor.Value %>",
                critical: "<%=criticalcolor.Value %>"

            };

            // function executes when page is ready
            // main documentReady function starts
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                var perRiskStackedColumnChart = Highcharts.chart('perRiskStackedColumnChartDiv', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Per Risk',
                        style: {
                            // "font-family": 'Helvetica',
                            display: 'none'
                        },
                    },
                    subtitle: {
                        //text: 'Click on graph to view documents',
                        text: '',
                        style: {
                            // "font-family": 'Helvetica',
                        }
                    },
                    xAxis: {
                        categories: [ 'Critical' ,'High', 'Medium', 'Low']
                    },
                    yAxis: {
                        title: {
                            text: 'Number of Compliances',
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: 'gray'
                            }
                        }
                    },
                    tooltip: {
                        hideDelay: 0,
                        backgroundColor: 'rgba(247,247,247,1)',
                        headerFormat: '<b>{point.x}</b><br/>',
                        pointFormat: '{series.name}: {point.y}'
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                color: 'white',
                                style: {

                                    textShadow: false,
                                }
                            },
                            cursor: 'pointer'
                        },
                    },
              <%=perGradingRiskChart%>
                });


                // change color in color picker according to chart selected
                $('input[name=radioButton]').change(function () {
                    // destroy all color pickers
                    $('#highColorPicker').simplecolorpicker('destroy');
                    $('#mediumColorPicker').simplecolorpicker('destroy');
                    $('#lowColorPicker').simplecolorpicker('destroy');
                    $('#criticalColorPicker').simplecolorpicker('destroy');


                    // setting the colors for risks according to chart selected
                    var chart = $('input[name=radioButton]:checked').val();
                    switch (chart) {
                        case "perFunction":
                            $('#highColorPicker').val(perFunctionChartColorScheme.high);
                            $('#mediumColorPicker').val(perFunctionChartColorScheme.medium);
                            $('#lowColorPicker').val(perFunctionChartColorScheme.low);
                            $('#criticalColorPicker').val(perFunctionChartColorScheme.critical);
                            break;
                        case "perRisk":
                            $('#highColorPicker').val(perRiskStackedColumnChartColorScheme.high);
                            $('#mediumColorPicker').val(perRiskStackedColumnChartColorScheme.medium);
                            $('#lowColorPicker').val(perRiskStackedColumnChartColorScheme.low);
                            $('#criticalColorPicker').val(perFunctionChartColorScheme.critical);
                            break;
                        case "perStatus":
                            $('#highColorPicker').val(perStatusChartColorScheme.high);
                            $('#mediumColorPicker').val(perStatusChartColorScheme.medium);
                            $('#lowColorPicker').val(perStatusChartColorScheme.low);
                            $('#criticalColorPicker').val(perStatusChartColorScheme.critical);
                            break;
                        default:
                            $('#highColorPicker').val('#7CB5EC');
                            $('#mediumColorPicker').val('#434348');
                            $('#lowColorPicker').val('#90ED7D');
                            $('#criticalColorPicker').val('#f1c232');
                    }

                    // initialise the color piskers again
                    $('#highColorPicker').simplecolorpicker({ picker: true });
                    $('#mediumColorPicker').simplecolorpicker({ picker: true });
                    $('#lowColorPicker').simplecolorpicker({ picker: true });
                    $('#criticalColorPicker').simplecolorpicker({ picker: true });
                });


                //apply color scheme to all charts [Apply to All] click event handler
                $('#applyToAllButton').click(function () {
                });

                $('#showModal').click(function () {
                    $('#modalDiv').modal();
                });

            });   //	main documentReady function END
        </script>

    </form>
</body>
</html>
