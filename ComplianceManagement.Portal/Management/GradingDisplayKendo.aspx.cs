﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Configuration;
using System.Web.Services;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class GradingDisplayKendo : System.Web.UI.Page
    {
        protected static int customerid;
        protected static string ComplianceTypeFlag;
        protected static string Type;
        protected static string sdate;
        protected static string edate;
        protected static int Customerbanchid;
        protected static string Customerbanchids;
        protected static bool IsApprover = false;
        protected List<Int32> roles;
        protected static bool DeptHead = false;
        protected static string Path;
        protected static string CId;
        protected static string UserId;
        protected static string Falg;
        protected static string Authorization;
        protected static string CustomerName;

        protected void Page_Load(object sender, EventArgs e)
        {
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CId = Convert.ToString(AuthenticationHelper.CustomerID);
            UserId = Convert.ToString(AuthenticationHelper.UserID);
            Customerbanchids = string.Empty;
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            CustomerName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(CId));
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            if (!IsPostBack)
            {
                try
                {
                    BindDetailView();
                    
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

            }

            if (AuthenticationHelper.Role == "EXCT")
            {
                Falg = "PRA";
            }
            else if (AuthenticationHelper.Role == "MGMT")
            {
                Falg = "MGMT";
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                Falg = "PRA";
            }
        }

      
        private void BindDetailView()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                    if (ComplianceTypeFlag == "Statutory")
                    {
                        ComplianceTypeFlag = "S";
                    }
                    else
                    {
                        ComplianceTypeFlag = "I";
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["type"]))
                {
                    Type = Request.QueryString["type"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["StartDate"]))
                {
                    sdate = Convert.ToString(Request.QueryString["StartDate"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["EndDate"]))
                {
                    edate = Convert.ToString(Request.QueryString["EndDate"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Customerbanchid"]))
                {
                    Customerbanchid = Convert.ToInt32(Request.QueryString["Customerbanchid"]);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var Customerbanchidss = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch(Convert.ToInt32(Customerbanchid), (int)customerid)
                                         select row).ToList();
                        for (int i = 0; i < Customerbanchidss.Count; i++) // Loop through List with for
                        {
                            Customerbanchids +=Customerbanchidss[i]+",";
                        }
                        Customerbanchids = Customerbanchids.TrimEnd(',');
                    }

                }
                if (Session["User_comp_Roles"] != null)
                {
                    roles = Session["User_comp_Roles"] as List<int>;
                }
                else
                {
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    Session["User_comp_Roles"] = roles;
                }
                if (Session["User_dept"] != null)
                {
                    DeptHead = (bool)Session["User_dept"];
                }
                else
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (!string.IsNullOrEmpty(Convert.ToString(LoggedUser.IsHead)))
                    {
                        DeptHead = (bool)LoggedUser.IsHead;
                    }
                }
                if (AuthenticationHelper.Role.Equals("MGMT"))
                {
                    IsApprover = false;
                }
                else if (DeptHead)
                {
                    IsApprover = false;
                }
                else
                {
                    if (roles.Contains(6))
                    {
                        IsApprover = true;
                    }
                    else
                    {
                        IsApprover = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}