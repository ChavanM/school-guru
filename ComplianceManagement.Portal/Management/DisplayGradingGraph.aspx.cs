﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class DisplayGradingGraph : System.Web.UI.Page
    {
        public static string perGradingRiskChart;
        protected void Page_Load(object sender, EventArgs e)
        {
            BindUserColors();

            if (!string.IsNullOrEmpty(Request.QueryString["perGradingRiskChart"]))
            {
                perGradingRiskChart = Request.QueryString["perGradingRiskChart"];
            }
        }
        private void BindUserColors()
        {
            try
            {
                var Cmd = Business.ComplianceManagement.Getcolor(AuthenticationHelper.UserID);

                if (Cmd != null)
                {
                    highcolor.Value = Cmd.High;
                    mediumcolor.Value = Cmd.Medium;
                    lowcolor.Value = Cmd.Low;
                    criticalcolor.Value = Cmd.Critical;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
    }
}