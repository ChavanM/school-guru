﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceDetailsAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.ComplianceDetailsAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
    <title></title>
    <style type="text/css">
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: inset 0 0 1px 1px #14699f;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px #d7dae0;
            box-shadow: inset 0 0 1px 1px white;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
            overflow: hidden;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px;
            color: inherit;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 4px 0;
            line-height: normal;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: -4px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-treeview .k-i-collapse, .k-treeview .k-i-expand, .k-treeview .k-i-minus, .k-treeview .k-i-plus {
            margin-left: -18px;
            cursor: pointer;
        }

        .k-button.k-button-icon .k-icon, .k-grid-filter .k-icon, .k-header .k-icon {
            text-indent: -99999px;
            overflow: hidden;
        }

        .k-header > .k-grid-filter, .k-header > .k-header-column-menu {
            float: right;
            margin: -.5em 0.4em -0.4em 0px;
            padding: .5em .2em .4em;
            position: relative;
            z-index: 1;
            color: inherit;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
        .k-filter-menu .k-button {
            width: 27%;
            background-color: #E9EAEA;
            color: black;
            border: 2px solid rgba(0, 0, 0, 0.5);
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">
        var perorrev = "";

        function BindGrid() {
            var grid = $("#grid").data("kendoGrid");
            if (grid != undefined || grid != null) {
                $("#grid").empty();
            }
            if ($("#dropdownlistUserRole").val() == "3") {

                perorrev = "Performer";

            }
            else if ($("#dropdownlistUserRole").val() == "4") {
                perorrev = "Reviewer";
            }
            else {
                perorrev = "Performer/Reviewer";
            }

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/DashboradComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&bid=<% =BID%>&catid=<% =catid%>&StatusFlag=<% =StatusFlagID%>&IsApprover=<% =isapprover%>&Isdept=<% =IsDeptHead%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                ComplianceID: { type: "string", },
                                InternalComplianceID: { type: "string", },
                                //  ScheduledOn: { type: "date" }
                            }
                        },
                        data: function (response) {
                            if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Statustory;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].DeptStatustory;
                            }
                            else if (<% =ComplianceTypeID%> == 2) {
                                return response[0].Internal;
                            }
                            else if (<% =ComplianceTypeID%> == 3) {
                                return response[0].DeptInternal;
                            }
                        },
                        total: function (response) {
                            if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Statustory.length;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].DeptStatustory.length;
                            }
                            else if (<% =ComplianceTypeID%> == 2) {
                                return response[0].Internal.length;
                            }
                            else if (<% =ComplianceTypeID%> == 3) {
                                return response[0].DeptInternal.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columnMenu: true,
                dataBinding: function () {

                },
                columns: [
                    {
                        field: "Branch", title: 'Location',
                        width: "25%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                      <% if (StatusFlagID == -1)
        {%> 
                    { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, search: true }, width: "10%" },
                   <%}%> 
                        <% if (StatusFlagID == 0)
        {%> 
                    { hidden: true, field: "InternalComplianceID", title: "Compliance ID", filterable: { multi: true, search: true }, width: "10%" },

                    { hidden: true, field: "InternalComplianceTypeName", title: "Internal Type", filterable: { multi: true, search: true }, width: "10%" },
                        <%}%>
                    <% if (StatusFlagID == -1)
        {%>   {
                        field: "Name", title: 'Act',
                        width: "24%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "Section", title: 'Section',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    <%}%>
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "34%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Frequency", title: 'Frequency',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "User", title: perorrev,
                        width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                ]
            });

            $("#grid").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
            window.parent.forchild($("body").height() + 100);
        }

        $(document).ready(function () {

            $("#txtSearchComplianceID").on('input', function (e) {
                FilterGrid();
            });

            $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Role",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    FilterGrid();
                },
                index: 0,
                dataSource: [
                    { text: "Role", value: "-1" },
                    { text: "Performer", value: "3" },
                    { text: "Reviewer", value: "4" }
                ]
            });

            <% if (StatusFlagID == -1){%>  

            $("#dropdownfunction").kendoDropDownTree({
                placeholder: "Select Category",
                filter: "contains",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",

                change: function () {
                    FilterGrid();
                    fCreateStoryBoardCompliances('dropdownfunction', 'filterCategory', 'function')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=MGMT"
                    },
                }
            });

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoardCompliances('dropdownACT', 'filterAct', 'act')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindComplianceWiseActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Flag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                },
                dataBound: function (e) {
                    e.sender.list.width("557");
                }
            });

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                // filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoardCompliances('dropdowntree', 'filtersstoryboard', 'loc');
                    //window.parent.forchild($("body").height()+50);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                          //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S"
                      },
                      schema: {
                          data: function (response) {
                              return response[0].locationList;
                          },
                          model: {
                              children: "Children"
                          }
                      }
                  }
              });

            <%}%>
            <%else
        {%>
            $("#dropdownfunction").kendoDropDownTree({
                placeholder: "Select Category",
                filter: "contains",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                change: function () {
                    FilterGrid();
                    fCreateStoryBoardCompliances('dropdownfunction', 'filterCategory', 'function');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindInternalFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                }
            });
            $("#dropdownType").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Type",
                change: function (e) {
                    FilterGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindInternalComplianceTypeDashboradComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&bid=<% =BID%>&catid=<% =catid%>&StatusFlag=<% =StatusFlagID%>&IsApprover=<% =isapprover%>&Isdept=<% =IsDeptHead%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });


            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                // filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoardCompliances('dropdowntree', 'filtersstoryboard', 'loc');
                    //window.parent.forchild($("body").height()+50);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=I',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
            <%}%>

            $("#dropdownFrequency").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Name",
                optionLabel: "Select Frequency",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoardCompliances('dropdownFrequency', 'filterCompType', 'frequency')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindFrequency',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindFrequency"
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("200");
                }
            });

          
            BindGrid();
        });

        function ClearAllFilterMain(e) {
            $("#txtSearchComplianceID").val('');
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistUserRole").data("kendoDropDownList").select(0);
            <% if (StatusFlagID == 0){%>  
            $("#dropdownType").data("kendoDropDownList").select(0);
            <%}%>
            <% if (StatusFlagID == -1){%>  
            if ($("#dropdownACT").val() != undefined || $("#dropdownACT").val() != null) {
                $("#dropdownACT").data("kendoDropDownList").select(0);
            }
            <%}%>
            $("#dropdownfunction").data("kendoDropDownTree").value([]);
            if ($("#dropdownFrequency").val() != undefined || $("#dropdownFrequency").val() != null) {
                $("#dropdownFrequency").data("kendoDropDownList").select(0);
            }
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function FilterGrid() {

            var IsDept = document.getElementById('IsDeptId').value;

            var IsSI = document.getElementById('IsSIId').value;

            var FunctiondetailsStat = [];

            var FunctiondetailsInt = [];

            var complianceID = "";
            var categoryID = "";
            <% if (StatusFlagID == -1)
        {%>
            complianceID = "ComplianceID";
            if ($("#dropdownfunction").data("kendoDropDownTree") != undefined) {
                FunctiondetailsStat = $("#dropdownfunction").data("kendoDropDownTree")._values;
            }
            <%}%>
            <% if (StatusFlagID == 0)
        {%>
            complianceID = "InternalComplianceID";
            if ($("#dropdownfunction").data("kendoDropDownTree") != undefined) {
                FunctiondetailsInt = $("#dropdownfunction").data("kendoDropDownTree")._values;
            }
            <%}%>

            var locationsdetails = [];
            if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
            }

            var finalSelectedfilter = { logic: "and", filters: [] };

            if (locationsdetails.length > 0
                || $("#txtSearchComplianceID").val() != ""
                || ($("#dropdownlistUserRole").val() != $("#dropdownlistUserRole").val() != "Role" && $("#dropdownlistUserRole").val() != null && $("#dropdownlistUserRole").val() != undefined)
                || ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != undefined)
                <% if (StatusFlagID == -1){%>
                || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
                || FunctiondetailsStat.length > 0
                <%}%>
                <% if (StatusFlagID == 0){%>
                || FunctiondetailsInt.length > 0
                || ($("#dropdownType").val() != "" && $("#dropdownType").val() != null && $("#dropdownType").val() != undefined)
                <%}%>
            ) {

                if (locationsdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locationsdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }
                if ($("#dropdownType").val() != "" && $("#dropdownType").val() != null && $("#dropdownType").val() != undefined) {
                    var TypeFilter = { logic: "or", filters: [] };
                    TypeFilter.filters.push({
                        field: "InternalComplianceTypeID", operator: "eq", value: $("#dropdownType").val()
                    });
                    finalSelectedfilter.filters.push(TypeFilter);
                }
                if ($("#dropdownlistUserRole").val() != undefined && $("#dropdownlistUserRole").val() != null && $("#dropdownlistUserRole").val() != "-1" && $("#dropdownlistUserRole").val() != "") {
                    var SeqFilter = { logic: "or", filters: [] };
                    SeqFilter.filters.push({
                        field: "RoleID", operator: "eq", value: $("#dropdownlistUserRole").val()
                    });
                    finalSelectedfilter.filters.push(SeqFilter);
                }
                if ($("#txtSearchComplianceID").val() != "") {
                    var RiskFilter = { logic: "or", filters: [] };
                    RiskFilter.filters.push({
                        field: complianceID, operator: "contains", value: $("#txtSearchComplianceID").val()
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }

                if (IsDept == 1) {
                    if (FunctiondetailsInt.length > 0) {
                        var FunctionFilter = { logic: "or", filters: [] };

                        $.each(Functiondetails, function (i, v) {
                            FunctionFilter.filters.push({
                                field: "DepartmentID", operator: "eq", value: parseInt(v)
                            });
                        });
                        finalSelectedfilter.filters.push(FunctionFilter);
                    }
                    if (FunctiondetailsStat.length > 0) {
                        var FunctionFilter = { logic: "or", filters: [] };

                        $.each(FunctiondetailsStat, function (i, v) {
                            FunctionFilter.filters.push({
                                field: "DepartmentID", operator: "eq", value: parseInt(v)
                            });
                        });
                        finalSelectedfilter.filters.push(FunctionFilter);
                    }
                }
                else {
                    if (IsSI != -1) {
                        if (FunctiondetailsInt.length > 0) {
                            var FunctionFilter = { logic: "or", filters: [] };

                            $.each(FunctiondetailsInt, function (i, v) {
                                FunctionFilter.filters.push({
                                    field: "InternalComplianceCategoryID", operator: "eq", value: parseInt(v)
                                });
                            });
                            finalSelectedfilter.filters.push(FunctionFilter);
                        }
                    }
                    else {
                        if (FunctiondetailsStat.length > 0) {
                            var FunctionFilter = { logic: "or", filters: [] };

                            $.each(FunctiondetailsStat, function (i, v) {
                                FunctionFilter.filters.push({
                                    field: "ComplianceCategoryId", operator: "eq", value: parseInt(v)
                                });
                            });
                            finalSelectedfilter.filters.push(FunctionFilter);
                        }
                    }

                }
                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != "-1" && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };

                    ActFilter.filters.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });

                    finalSelectedfilter.filters.push(ActFilter);
                }

                if ($("#dropdownFrequency").val() != undefined && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != "-1" && $("#dropdownFrequency").val() != "") {
                    var SeqFilter = { logic: "or", filters: [] };
                    SeqFilter.filters.push({
                        field: "Frequency", operator: "eq", value: $("#dropdownFrequency").val()
                    });
                    finalSelectedfilter.filters.push(SeqFilter);
                }


                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

        function exportReport(e) {

            var ReportName = "Report of Compliances";
            var customerName = document.getElementById('CustName').value;
            var todayDate = moment().format('DD-MMM-YYYY');

            var grid = $("#grid").getKendoGrid();
            var rows =
                [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: customerName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate }
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "Compliance ID", bold: true },
                            { value: "Location", bold: true },
                            <% if (StatusFlagID == 1){%>   
                            { value: "Act", bold: true },
                             <%}%> 
                            { value: "Category Name", bold: true },
                             <% if (StatusFlagID == 1){%>  
                            { value: "Section", bold: true },
                            <%}%> 
                            { value: "Short Description", bold: true },
                            { value: "Frequency", bold: true },
                            { value: "User", bold: true },
                        ]
                    }
                ];

            var trs = grid.dataSource;
            var filteredDataSource = new kendo.data.DataSource({
                data: trs.data(),
                filter: trs.filter()
            });

            filteredDataSource.read();
            var data = filteredDataSource.view();
            var complianceid = "";
            var CategoryName = "";

            for (var i = 0; i < data.length; i++) {
                var dataItem = data[i];
                var rowHeight = 20;
                if (dataItem.ShortDescription != null || dataItem.ShortDescription != "") {
                    rowHeight = dataItem.ShortDescription.length > 27 ? Math.ceil(dataItem.ShortDescription.length / 27) * 20 : 20;
                }
      

            <% if (StatusFlagID == -1){%>
                complianceid = dataItem.ComplianceID;
                CategoryName = dataItem.ComplianceCategoryName;
            <%}%> 
             <% if (StatusFlagID == 0){%>                      
                complianceid = dataItem.InternalComplianceID;
                CategoryName = dataItem.InternalComplianceCategoryName;
             <%}%> 
             <% if (IsDeptHead == 1){%>                      
                CategoryName = dataItem.DepartmentName;
             <%}%> 
                
                rows.push({
                    cells: [
                        { value: complianceid },
                        { value: dataItem.Branch },
                        <% if (StatusFlagID == 1){%>
                        { value: dataItem.Name },
                        <%}%> 
                        { value: CategoryName },
                        <% if (StatusFlagID == 1){%>
                        { value: dataItem.Section },
                         <%}%> 
                        { value: dataItem.ShortDescription },
                        { value: dataItem.Frequency },
                        { value: dataItem.User },
                    ],
                    height: rowHeight
                });

            }
            <% if (StatusFlagID == 1){%>
            for (var i = 4; i < rows.length; i++) {
                for (var j = 0; j < 8; j++) {
                    rows[i].cells[j].borderBottom = "#000000";
                    rows[i].cells[j].borderLeft = "#000000";
                    rows[i].cells[j].borderRight = "#000000";
                    rows[i].cells[j].borderTop = "#000000";
                    rows[i].cells[j].hAlign = "left";
                    rows[i].cells[j].vAlign = "top";
                    rows[i].cells[j].wrap = true;
                }
            }
            <%}%>  <% else{%>
            for (var i = 4; i < rows.length; i++) {
                for (var j = 0; j < 6; j++) {
                    rows[i].cells[j].borderBottom = "#000000";
                    rows[i].cells[j].borderLeft = "#000000";
                    rows[i].cells[j].borderRight = "#000000";
                    rows[i].cells[j].borderTop = "#000000";
                    rows[i].cells[j].hAlign = "left";
                    rows[i].cells[j].vAlign = "top";
                    rows[i].cells[j].wrap = true;
                }
            }
            <%}%> 
            excelExport(rows, ReportName);
            e.preventDefault();
            return false;
        }

        function excelExport(rows, ReportName) {

            var FileName = "Report of Statutory Compliances";
            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        columns: [
                             <% if (StatusFlagID == 0){%>
                            { autoWidth: true },
                            { width: 250 },
                            { width: 200 },
                            { width: 300 },
                            { width: 200 },
                            { width: 200 },
                             <%}%>  <% else{%>
                            { autoWidth: true },
                            { width: 250 },
                            { width: 300 },
                            { width: 200 },
                            { width: 200 },
                            { width: 300 },
                            { width: 200 },
                            { width: 200 },
                              <%}%> 
                        ],
                        title: FileName,
                        rows: rows
                    },
                ]
            });

            var nameOfPage = FileName;
            //var nameOfPage = "Test-1"; // insert here however you are getting name of screen
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
            return false;
        }

        function fcloseStory(obj) {
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoardCompliances('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoardCompliances('dropdownfunction', 'filterCategory', 'function');
        }

        function fCreateStoryBoardCompliances(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                //$('#ClearfilterMain').css('display', 'block');
            }

            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                //if (buttontest.length > 10) {
                //    buttontest = buttontest.substring(0, 10).concat("...");
                //}
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:1px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }
    </script>
</head>
<body style="overflow-x: hidden;">
    <form>
        <div>
            <h1 id="pagetype" style="height: 30px; background-color: #f8f8f8; margin-top: 0px; color: #666; margin-bottom: 12px; font-size: 19px; padding-left: 5px; padding-top: 5px; font-weight: bold;">Compliances</h1>
        </div>

        <div id="example">
            <div style="margin: 0.5% 0 0.5%;">
                <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 21%; margin-right: 0.5%;" />
                <input id="dropdownlistUserRole" data-placeholder="Role" style="width: 9%; margin-right: 0.5%;" />
                <% if (StatusFlagID == 0){%>
                <input id="dropdownType" style="width: 12%; margin-right: 0.5%;" />
                <%}%>
                <input id="dropdownfunction" style="width: 12%; margin-right: 0.5%;" />
                <% if (StatusFlagID == -1){%>
                <input id="dropdownACT" style="width: 18.4%; margin-right: 0.5%;" />
                <%}%>
                <input id="IsDeptId" type="hidden" value="<% =IsDeptHead%>" />
                <input id="IsSIId" type="hidden" value="<% =StatusFlagID%>" />
                <input id="dropdownFrequency" style="width: 12%; margin-right: 0.5%;" />
                <input id="txtSearchComplianceID" class="k-textbox" onkeydown="return (event.keyCode!=13);" placeholder="Compliance ID" style="width: 10%; margin-right: 0.5%;" />
                <button id="export" onclick="exportReport(event)" style="height: 30px;margin-right: 0.5%;" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                <button id="ClearfilterMain" style="height: 30px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
            </div>


            <div class="row" style="padding-bottom: 7px; font-size: 12px; display: none; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtertype">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filterrole">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filterstatus">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filterCategory">&nbsp;</div>
            <div id="grid"></div>

            <input id="CustName" type="hidden" value="<% =CustomerName%>" />
            <input id="UId" type="hidden" value="<% =UId%>" />
            <input id="Path" type="hidden" value="<% =Path%>" />
            <input id="CustomerId" type="hidden" value="<% =CustId%>" />


        </div>
        <script type="text/javascript">
            function fhead(Compliances) {
                $('#pagetype').html(val);
            }
        </script>
    </form>
</body>
</html>
