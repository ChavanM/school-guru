﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Net;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class SearchResult : System.Web.UI.Page
    {
        protected static String Type;
        protected static String SearchText;
        protected static int customerID;
        protected static string IsSatutoryInternal = "Statutory";
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                    {
                        Type = Convert.ToString(Request.QueryString["Type"]);
                        if (Type == "")
                        {
                            Type = "C";
                        }
                    }
                    else
                    {
                        Type = "C";
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["SearchText"]))
                    {
                        SearchText = Convert.ToString(Request.QueryString["SearchText"]);
                    }
                    BindDetailView();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
               
            }
        }

        private void BindDetailView()
        {
            try
            {
                if (Type == "U")
                {
                    liUser.Attributes.Add("class", "active");
                    liAct.Attributes.Add("class", "");
                    liInformative.Attributes.Add("class", "");
                    liComp.Attributes.Add("class", "");

                    var detailsview = Business.UserManagement.GetUserDetailsByText(SearchText, customerID);
                    Session["TotalRows"] = detailsview.Count;

                    GridUser.DataSource = detailsview;
                    GridUser.DataBind();

                    GridUser.Visible = true;
                    GridCompliance.Visible = false;
                    GridAct.Visible = false;
                    grdInformativeCompliance.Visible = false;
                }
                else if (Type == "C")
                {
                    liUser.Attributes.Add("class", "");
                    liAct.Attributes.Add("class", "");
                    liInformative.Attributes.Add("class", "");
                    liComp.Attributes.Add("class", "active");

                    var detailsview = Business.ComplianceManagement.GetComplianceDetailsByText(SearchText, customerID);
                    Session["TotalRows"] = detailsview.Count;

                    GridCompliance.DataSource = detailsview;
                    GridCompliance.DataBind();

                    GridUser.Visible = false;
                    GridCompliance.Visible = true;
                    GridAct.Visible = false;
                    grdInformativeCompliance.Visible = false;
                }
                else if (Type == "I")
                {
                    liUser.Attributes.Add("class", "");
                    liAct.Attributes.Add("class", "");
                    liInformative.Attributes.Add("class", "active");
                    liComp.Attributes.Add("class", "");

                    var detailsview = Business.ComplianceManagement.GetInformativeComplianceDetailsByText(SearchText);
                    Session["TotalRows"] = detailsview.Count;

                    grdInformativeCompliance.DataSource = detailsview;
                    grdInformativeCompliance.DataBind();

                    grdInformativeCompliance.Visible = true;
                    GridUser.Visible = false;
                    GridCompliance.Visible = false;
                    GridAct.Visible = false;
                }
                else if (Type == "AI")
                {
                    liUser.Attributes.Add("class", "");
                    liAct.Attributes.Add("class", "");
                    liInformative.Attributes.Add("class", "active");
                    liComp.Attributes.Add("class", "");

                    var detailsview = Business.ComplianceManagement.GetAllInformativeComplianceDetails();
                    Session["TotalRows"] = detailsview.Count;

                    grdInformativeCompliance.DataSource = detailsview;
                    grdInformativeCompliance.DataBind();

                    grdInformativeCompliance.Visible = true;
                    GridUser.Visible = false;
                    GridCompliance.Visible = false;
                    GridAct.Visible = false;
                }
                else if (Type == "A")
                {
                    liUser.Attributes.Add("class", "");
                    liAct.Attributes.Add("class", "active");
                    liComp.Attributes.Add("class", "");
                    liInformative.Attributes.Add("class", "");

                    var detailsview = Business.ActManagement.GetActDetailsByText(SearchText, customerID);
                    Session["TotalRows"] = detailsview.Count;

                    GridAct.DataSource = detailsview;
                    GridAct.DataBind();

                    GridUser.Visible = false;
                    GridCompliance.Visible = false;
                    GridAct.Visible = true;
                    grdInformativeCompliance.Visible = false;

                }

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
           
        }


        protected void lnkUser_click(object sender, EventArgs e)
        {
            try
            {
                Type = "U";
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }           
        }
        protected void lnkAct_click(object sender, EventArgs e)
        {
            try
            {
                Type = "A";
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
           
        }
        protected void lnkCompliance_click(object sender, EventArgs e)
        {
            try
            {
                Type = "C";
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }           
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                if (Type == "U")
                {
                    GridUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (Type == "A")
                {
                    GridAct.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridAct.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (Type == "C")
                {
                    GridCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }                

                //Reload the Grid
                BindDetailView();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected String GetRole(long UserID)
        {
            try
            {
                String Role = String.Empty;               

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var DistinctRoles = (from row in entities.ComplianceAssignments
                                         where row.UserID == UserID
                                         select row).GroupBy(Entry => Entry.RoleID)
                                         .Select(a => a.FirstOrDefault())
                                         .Select(a => a.RoleID).ToList();                  

                    DistinctRoles.ForEach(EachRole =>
                    {
                        var RoleName = (from row in entities.Roles
                                        where row.ID == EachRole
                                        select row.Name).FirstOrDefault();

                        if (RoleName != null)
                        {
                            if (RoleName == "Reviewer1" || RoleName == "Reviewer2")
                                RoleName = "Reviewer";

                            if (!String.IsNullOrEmpty(Role))
                                Role += ", " + RoleName;
                            else
                                Role += RoleName;
                        }
                    });
                }

                return Role;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BinaryWrite(file.FileData); // create the file
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int complianceID = Convert.ToInt32(commandArgs[0]);
                BindTransactionDetails(complianceID);

                upComplianceDetailsModal.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }

        protected void btnViewCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ActID = Convert.ToInt32(commandArgs[0]);

                Response.Redirect("~/Management/ComplianceDetails.aspx?customerid=" + customerID + "&ActID=" + ActID);
                      
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTransactionDetails(int ComplianceID)
        {
            try
            {
                lblPenalty.Text = string.Empty;
                lblRisk.Text = string.Empty;
                lbDownloadSample.Text = string.Empty;
                
                var complianceInfo = Business.ComplianceManagement.GetByID(ComplianceID);

                var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);

                if (complianceInfo != null)
                {
                    lblComplianceID.Text = Convert.ToString(complianceInfo.ID);
                    lnkSampleForm.Text = Convert.ToString(complianceInfo.SampleFormLink);
                    lblComplianceDiscription.Text = complianceInfo.ShortDescription;
                    lblDetailedDiscription.Text = complianceInfo.Description;
                    lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));

                    //lblRefrenceText.Text = complianceInfo.ReferenceMaterialText;
                    lblRefrenceText.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);

                    string Penalty = Business.ComplianceManagement.GetPanalty(complianceInfo);
                    lblPenalty.Text = Penalty;

                    string risk = Business.ComplianceManagement.GetRiskType(complianceInfo,-1);
                    lblRiskType.Text = Business.ComplianceManagement.GetRisk(complianceInfo,-1);
                    lblRisk.Text = risk;

                    var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);

                    if (ActInfo != null)
                    {
                        lblActName.Text = ActInfo.Name;
                    }

                    divRiskType.Attributes.Remove("style");

                    if (risk == "HIGH")
                    {                       
                        divRiskType.Attributes.Add("style", "background-color:red;");
                    }
                    else if (risk == "MEDIUM")
                    {
                        divRiskType.Attributes.Add("style", "background-color:yellow;");                        
                    }
                    else if (risk == "LOW")
                    {
                        divRiskType.Attributes.Add("style", "background-color:green;");                       
                    }

                    //if (risk == "HIGH")
                    //{
                    //    divRiskType.Attributes.Add("style",) = "background-color:red;";
                    //}
                    //else if (risk == "MEDIUM")
                    //{
                    //    divRiskType.Attributes["style"] = "background-color:yellow;";
                    //}
                    //else if (risk == "LOW")
                    //{
                    //    divRiskType.Attributes["style"] = "background-color:green;";
                    //}

                    lblRule.Text = complianceInfo.Sections;
                    lblFormNumber.Text = complianceInfo.RequiredForms;
                }                

                if (complianceInfo.UploadDocument == true && complianceForm != null)
                {
                    // lbDownloadSample.Text = "<u style=color:blue>Click here</u> to download sample form.";
                    lbDownloadSample.Text = "Download";
                    lbDownloadSample.CommandArgument = complianceForm.ComplianceID.ToString();
                    //lblNote.Visible = true;
                    lnkViewSampleForm1.Visible = true;
                }
                else
                {
                    //lblNote.Visible = false;
                    lnkViewSampleForm1.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }

        protected String GetComplianceLocations(long ComplianceID)
        {
            try
            {
                String Role = String.Empty;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var CompDetails = (from row in entities.Compliances
                                       join row1 in entities.ComplianceInstances
                                       on row.ID equals row1.ComplianceId
                                       join row2 in entities.CustomerBranches
                                       on row1.CustomerBranchID equals row2.ID
                                       where row2.CustomerID == customerID
                                       && row.ID == ComplianceID
                                       select row2.Name).Distinct().ToList();

                    CompDetails.ForEach(EachLoc =>
                    {
                        if (!String.IsNullOrEmpty(Role))
                            Role += ", " + EachLoc;
                        else
                            Role += EachLoc;
                    });
                }

                return Role;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected String GetCustomerBranch(int CustBranchID)
        {
            try
            {
                String Name = String.Empty;

                if (CustBranchID != 0)
                {
                    CustomerBranch BranchDetails = CustomerBranchManagement.GetByID(Convert.ToInt32(CustBranchID));

                    if (BranchDetails != null)
                    {
                        Name = BranchDetails.Name;
                    }
                }

                return Name;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (Type == "U")
                {
                    GridUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (Type == "A")
                {
                    GridAct.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridAct.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (Type == "C")
                {
                    GridCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (Type == "I" || Type == "AI")
                {
                    grdInformativeCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdInformativeCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (Type == "U")
                {
                    GridUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (Type == "A")
                {
                    GridAct.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridAct.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (Type == "C")
                {
                    GridCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lnkInformativeCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                Type = "I";
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}