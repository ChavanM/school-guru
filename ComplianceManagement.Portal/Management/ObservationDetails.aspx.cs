﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;
using Saplin.Controls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class ObservationDetails : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected int CustomerId = 0;        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //BindProcess("P");
                //BindVertical();
                //BindFinancialYear();
                //BindLegalEntityData();
                //BindData();
                //bindPageNumber();
                //ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
                //ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
                //ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
                //ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));                
                //ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
                //ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindProcess("P");
                BindFinancialYear();
                string financialYearCommaSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    financialYearCommaSeparatedList = Request.QueryString["FinYear"].ToString();
                }
                if (!string.IsNullOrEmpty(financialYearCommaSeparatedList))
                {
                    List<string> finYearsList = financialYearCommaSeparatedList.Split(',').ToList();
                    if (finYearsList.Count > 0)
                    {
                        for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                        {
                            foreach (string fy in finYearsList)
                            {
                                if (ddlFinancialYearMultiSelect.Items[i].Text == fy)
                                {
                                    ddlFinancialYearMultiSelect.Items[i].Selected = true;
                                }
                            }
                        }
                    }
                }
                BindLegalEntityData();
                string branchIdsCommanSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    branchIdsCommanSeparatedList = Request.QueryString["branchid"].ToString();
                }

                List<long> CHKBranchlist = new List<long>();
                if (!string.IsNullOrEmpty(branchIdsCommanSeparatedList) && branchIdsCommanSeparatedList != "-1")
                {
                    List<string> branchIdsList = branchIdsCommanSeparatedList.Split(',').ToList();
                    if (branchIdsList.Count > 0)
                    {
                        for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                        {
                            foreach (string bId in branchIdsList)
                            {
                                if (ddlLegalEntityMultiSelect.Items[i].Value == bId)
                                {
                                    ddlLegalEntityMultiSelect.Items[i].Selected = true;
                                    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                                }
                            }
                        }
                    }
                }

                List<long> SelectedSubEnityOneListId = new List<long>();
                if (CHKBranchlist.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity1MultiSelect, CHKBranchlist);
                    string subEntityOneIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["subEntityOneIdCommaSeparatedList"]))
                    {
                        subEntityOneIdCommaSeparatedList = Request.QueryString["subEntityOneIdCommaSeparatedList"].ToString();


                        if (!string.IsNullOrEmpty(subEntityOneIdCommaSeparatedList) && subEntityOneIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityOneIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity1MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity1MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityOneListId.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                List<long> SelectedSubEnityTwoListId = new List<long>();
                if (SelectedSubEnityOneListId.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity2MultiSelect, SelectedSubEnityOneListId);
                    string subEntityTwoIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["SubEntityTwoIdCommaSeparatedList"]))
                    {
                        subEntityTwoIdCommaSeparatedList = Request.QueryString["SubEntityTwoIdCommaSeparatedList"].ToString();
                        if (!string.IsNullOrEmpty(subEntityTwoIdCommaSeparatedList) && subEntityTwoIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityTwoIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity2MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity2MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityTwoListId.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                List<long> SelectedSubEnityThreeListId = new List<long>();
                if (SelectedSubEnityTwoListId.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity3MultiSelect, SelectedSubEnityTwoListId);

                    string subEntityThreeIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["SubEntityThreeIdCommaSeparatedList"]))
                    {
                        subEntityThreeIdCommaSeparatedList = Request.QueryString["SubEntityThreeIdCommaSeparatedList"].ToString();
                        if (!string.IsNullOrEmpty(subEntityThreeIdCommaSeparatedList) && subEntityThreeIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityThreeIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity3MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity3MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityThreeListId.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (SelectedSubEnityTwoListId.Count > 0)
                {
                    BindSubEntityData(ddlFilterLocationMultiSelect, SelectedSubEnityThreeListId);

                    string filterLocationIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["FilterLocationIdCommaSeparatedList"]))
                    {
                        filterLocationIdCommaSeparatedList = Request.QueryString["FilterLocationIdCommaSeparatedList"].ToString();
                        if (!string.IsNullOrEmpty(filterLocationIdCommaSeparatedList) && filterLocationIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = filterLocationIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlFilterLocationMultiSelect.Items[i].Value == bId)
                                        {
                                            ddlFilterLocationMultiSelect.Items[i].Selected = true;
                                            CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        Session["BranchListMG"] = CHKBranchlist;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        Session["BranchListDH"] = CHKBranchlist;
                    }
                }
                BindSchedulingType();
                for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
                {
                    ddlSchedulingTypeMultiSelect.Items[i].Selected = true;
                }
                BindVertical();
                BindPeriod();
                BindData();
                bindPageNumber();
            }
        }

        private void BindPeriod()
        {
            List<string> FlagList = new List<string>();
            List<int> CHKBranchlist = new List<int>();
            for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
            {
                if (ddlSchedulingTypeMultiSelect.Items[i].Selected)
                {
                    if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Annually")
                    {
                        FlagList.Add("A");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Half Yearly")
                    {
                        FlagList.Add("H");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Quarterly")
                    {
                        FlagList.Add("Q");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Monthly")
                    {
                        FlagList.Add("M");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Special Audit")
                    {
                        FlagList.Add("S");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Phase")
                    {
                        bool clearLegalBranchList = false;
                        bool clearSubEntity1List = false;
                        bool clearSubEntity2List = false;
                        bool clearSubEntity3List = false;
                        bool clearFilterLocation = false;

                        CHKBranchlist.Clear();
                        for (int j = 0; j < ddlLegalEntityMultiSelect.Items.Count; j++)
                        {
                            if (ddlLegalEntityMultiSelect.Items[j].Selected)
                            {
                                clearLegalBranchList = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[j].Value));
                            }
                        }

                        for (int k = 0; k < ddlSubEntity1MultiSelect.Items.Count; k++)
                        {
                            if (ddlSubEntity1MultiSelect.Items[k].Selected)
                            {
                                if (clearLegalBranchList && CHKBranchlist.Count > 0)
                                {
                                    //CHKBranchlist.Clear();
                                    clearLegalBranchList = false;
                                }
                                clearSubEntity1List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[k].Value));
                            }

                        }

                        for (int l = 0; l < ddlSubEntity2MultiSelect.Items.Count; l++)
                        {
                            if (ddlSubEntity2MultiSelect.Items[l].Selected)
                            {
                                if (clearSubEntity1List && CHKBranchlist.Count > 0)
                                {
                                    //CHKBranchlist.Clear();
                                    clearSubEntity1List = false;
                                }
                                clearSubEntity2List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[l].Value));
                            }
                        }

                        for (int m = 0; m < ddlSubEntity3MultiSelect.Items.Count; m++)
                        {
                            if (ddlSubEntity3MultiSelect.Items[m].Selected)
                            {
                                if (clearSubEntity2List && CHKBranchlist.Count > 0)
                                {
                                    //CHKBranchlist.Clear();
                                    clearSubEntity2List = false;
                                }
                                clearSubEntity3List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[m].Value));
                            }
                        }
                    }
                    List<int?> recordCount = new List<int?>();
                    recordCount = UserManagementRisk.GetPhaseCountForAuditHeadDashboard(CHKBranchlist);
                    if (recordCount.Count > 0)
                    {
                        FlagList.Add("P");
                    }
                    BindAuditSchedule(FlagList, recordCount);
                }
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;            
            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);            
            BindData();
        }
        private void BindProcess(string flag)
        {
            try
            {
                bool IsAuditManager = false;
                bool IsDepartmentHead = false;
                bool IsManagement = false;
                List<int> branchidList = new List<int>();
                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity1MultiSelect.Items[i].Selected)
                    {
                        if (clearLegalBranchList && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearLegalBranchList = false;
                        }
                        clearSubEntity1List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity2MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity1List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity3MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity2List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                {
                    if (ddlFilterLocationMultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity3List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity3List = false;
                        }
                        clearFilterLocation = true;
                        branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                    }
                }
                ddlProcessMultiSelect.Items.Clear();
                ddlProcessMultiSelect.DataTextField = "Name";
                ddlProcessMultiSelect.DataValueField = "Id";

                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        IsDepartmentHead = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                if (IsAuditManager)
                {
                    ddlProcessMultiSelect.DataSource = ProcessManagement.FillProcessDropdownAuditManager(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcessMultiSelect.DataBind();
                }
                else if (IsDepartmentHead)
                {
                    ddlProcessMultiSelect.DataSource = ProcessManagement.FillProcessDropdownDepartmentHead(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcessMultiSelect.DataBind();
                }
                else if (IsManagement)
                {
                    ddlProcessMultiSelect.DataSource = ProcessManagement.FillProcessDropdownManagement(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcessMultiSelect.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindVertical()
        {
            try
            {
                List<int> branchidList = new List<int>();
                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;

                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity1MultiSelect.Items[i].Selected)
                    {
                        if (clearLegalBranchList && branchidList.Count > 0)
                        {
                            // branchidList.Clear();
                            clearLegalBranchList = false;
                        }
                        clearSubEntity1List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity2MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity1List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity3MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity2List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                {
                    if (ddlFilterLocationMultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity3List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity3List = false;
                        }
                        clearFilterLocation = true;
                        branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                    }
                }
                ddlVerticalList.DataTextField = "VerticalName";
                ddlVerticalList.DataValueField = "VerticalsId";
                ddlVerticalList.Items.Clear();
                ddlVerticalList.DataSource = UserManagementRisk.FillVerticalListFromMultiselect(branchidList);
                ddlVerticalList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindFinancialYear()
        {
            ddlFinancialYearMultiSelect.Items.Clear();
            ddlFinancialYearMultiSelect.DataTextField = "Name";
            ddlFinancialYearMultiSelect.DataValueField = "ID";
            ddlFinancialYearMultiSelect.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYearMultiSelect.DataBind();
            //ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            ddlLegalEntityMultiSelect.DataTextField = "Name";
            ddlLegalEntityMultiSelect.DataValueField = "ID";
            ddlLegalEntityMultiSelect.Items.Clear();
            if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
            {
                if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                {
                    ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(CustomerId,UserID);
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                {
                    ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(CustomerId, UserID);
                }
            }
            
            ddlLegalEntityMultiSelect.DataBind();
            //ddlLegalEntity.Items.Insert(0, new ListItem("Select Legal Entity", "-1"));
        }
        public void BindSubEntityData(DropDownCheckBoxes DRP, List<long> ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            bool IsAuditManager = false;
            bool IsDepartmentHead = false;
            bool IsManagement = false;
            if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
            {
                if (Convert.ToString(Request.QueryString["UType"]) == "AM" || Convert.ToString(Request.QueryString["UType"]) == "AH")
                {
                    IsAuditManager = true;
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                {
                    IsDepartmentHead = true;
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    IsManagement = true;
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (IsAuditManager)
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityDataMultiple(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (IsDepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityDataMultiple(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (IsManagement)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityDataMultiple(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
        }
        public void BindSchedulingType()
        {
            List<int> branchidList = new List<int>();
            bool clearLegalBranchList = false;
            bool clearSubEntity1List = false;
            bool clearSubEntity2List = false;
            bool clearSubEntity3List = false;
            bool clearFilterLocation = false;

            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    clearLegalBranchList = true;
                    branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity1MultiSelect.Items[i].Selected)
                {
                    if (clearLegalBranchList && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearLegalBranchList = false;
                    }

                    clearSubEntity1List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity1List && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearSubEntity1List = false;
                    }
                    clearSubEntity2List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity3MultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity2List && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearSubEntity2List = false;
                    }
                    clearSubEntity3List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
            {
                if (ddlFilterLocationMultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity3List && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearSubEntity3List = false;
                    }
                    clearFilterLocation = true;
                    branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                }
            }
            ddlSchedulingTypeMultiSelect.DataTextField = "Name";
            ddlSchedulingTypeMultiSelect.DataValueField = "ID";
            ddlSchedulingTypeMultiSelect.DataSource = UserManagementRisk.FillSchedulingTypeMultiple(branchidList);
            ddlSchedulingTypeMultiSelect.DataBind();
        }
        
        public void BindAuditSchedule(List<string> flagList, List<int?> countList)
        {
            List<string> FlagList = new List<string>();
            try
            {
                foreach (var flag in flagList)
                {
                    if (flag == "A")
                    {
                        FlagList.Add("Annually");
                    }
                    else if (flag == "H")
                    {
                        FlagList.Add("Apr-Sep");
                        FlagList.Add("Oct-Mar");
                    }
                    else if (flag == "Q")
                    {
                        FlagList.Add("Apr-Jun");
                        FlagList.Add("Jul-Sep");
                        FlagList.Add("Oct-Dec");
                        FlagList.Add("Jan-Mar");
                    }
                    else if (flag == "M")
                    {
                        FlagList.Add("Apr");
                        FlagList.Add("May");
                        FlagList.Add("Jun");
                        FlagList.Add("Jul");
                        FlagList.Add("Aug");
                        FlagList.Add("Sep");
                        FlagList.Add("Oct");
                        FlagList.Add("Nov");
                        FlagList.Add("Dec");
                        FlagList.Add("Jan");
                        FlagList.Add("Feb");
                        FlagList.Add("Mar");
                    }
                    else if (flag == "S")
                    {
                        FlagList.Add("Special Audit");
                    }
                    else
                    {
                        int count = countList.Count;
                        if (count == 1)
                        {
                            FlagList.Add("Phase1");
                        }
                        else if (count == 2)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                        }
                        else if (count == 3)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                        }
                        else if (count == 4)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                            FlagList.Add("Phase4");
                        }
                        else
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                            FlagList.Add("Phase4");
                            FlagList.Add("Phase5");
                        }
                    }
                }
                if (FlagList.Count > 0)
                {
                    int setIndex = 0;
                    ddlPeriodMultiSelect.Items.Clear();
                    foreach (string item in FlagList)
                    {
                        ddlPeriodMultiSelect.Items.Insert(setIndex, item);
                        setIndex = setIndex + 1;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        private void BindData()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ApplyFilter = false;
                string ObjCatName = string.Empty;
                int UserID = -1;
                bool IsAuditManager = false;
                bool IsDepartmentHead = false;
                bool IsManagement = false;
                List<String> FinancialYearList = new List<string>();
                List<string> periodList = new List<string>();
                String Type = String.Empty;
                List<int> ObsRating = new List<int>();
                List<long> ProcessIDList = new List<long>();
                List<long?> SubProcessProcessIDList = new List<long?>();
                List<int> VerticalIDList = new List<int>();
                List<string> StatusListAudit = new List<string>();
                List<string> ProcessListComma = new List<string>();
                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    Type = Request.QueryString["Type"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    string FY = Convert.ToString(Request.QueryString["FinYear"]);
                    FinancialYearList = FY.Split(',').ToList();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ObjCatName"]))
                {
                    ObjCatName = Convert.ToString(Request.QueryString["ObjCatName"]);
                    if (!string.IsNullOrEmpty(ObjCatName))
                    {
                        if (ObjCatName == "null")
                        {
                            ObjCatName = null;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PName"]))
                {
                    string ProcessName = Convert.ToString(Request.QueryString["PName"]);
                    ProcessListComma = ProcessName.Split(',').ToList();
                    for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
                    {
                        if (ProcessListComma.Contains(ddlProcessMultiSelect.Items[i].Text))
                        {
                            ddlProcessMultiSelect.Items[i].Selected = true;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["statusListwithComma"]))
                {
                    string statusListwithComma = Convert.ToString(Request.QueryString["statusListwithComma"]);
                    StatusListAudit = statusListwithComma.Split(',').ToList();
                }
                for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
                {
                    if (ddlProcessMultiSelect.Items[i].Selected)
                    {
                        ProcessIDList.Add(Convert.ToInt32(ddlProcessMultiSelect.Items[i].Value));
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VerticalID"]))
                {
                    string VerticalID = Convert.ToString(Request.QueryString["VerticalID"]);
                    List<string> verticalList = new List<string>();
                    verticalList = VerticalID.Split(',').ToList();
                    for (int i = 0; i < ddlVerticalList.Items.Count; i++)
                    {
                        if (verticalList.Contains(ddlVerticalList.Items[i].Value))
                        {
                            ddlVerticalList.Items[i].Selected = true;
                            VerticalIDList.Add(Convert.ToInt32(ddlVerticalList.Items[i].Value));
                        }
                    }
                }
                BindSubProcess();
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        IsDepartmentHead = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                List<long> BranchList = new List<long>();
                if (Session["BranchList"] != null)
                {
                    BranchList = (List<long>)(Session["BranchList"]);
                }
                if (Type.Trim().Equals("Major"))
                    ObsRating.Add(1);
                else if (Type.Trim().Equals("Moderate"))
                {
                    ObsRating.Add(2);
                }
                else if (Type.Trim().Equals("Minor"))
                {
                    ObsRating.Add(3);
                }
                else
                {
                    ObsRating.Add(1);
                    ObsRating.Add(2);
                    ObsRating.Add(3);
                }
                if (Session["BranchList"] != null)
                {
                    BranchList = (List<long>)(Session["BranchList"]);
                }
                if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                {
                    IsDepartmentHead = true;
                    BranchList = (List<long>)(Session["BranchListDH"]);
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    IsManagement = true;
                    BranchList = (List<long>)(Session["BranchListMG"]);
                }
                var detailView = InternalControlManagementDashboardRisk.GetObservationDetailView(CustomerId, BranchList, FinancialYearList, periodList, ObsRating, ObjCatName, ProcessIDList, UserID, IsAuditManager, IsManagement, IsDepartmentHead, SubProcessProcessIDList, VerticalIDList);
                if (StatusListAudit.Count > 0)
                {
                    List<int> ClosestutusList = new List<int>();
                    ClosestutusList.Add(2); ClosestutusList.Add(3);
                    if (StatusListAudit.Count == 1)
                    {
                        List<int> OpenstutusList = new List<int>();
                        OpenstutusList.Add(4); OpenstutusList.Add(5);
                        if (StatusListAudit.Contains("0"))
                        {
                            detailView = detailView.Where(entry => entry.StatusTimeline < DateTime.Now && (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                        }
                        else if (StatusListAudit.Contains("1"))
                        {
                            detailView = detailView.Where(entry => entry.StatusTimeline >= DateTime.Now && (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                        }
                        else if (StatusListAudit.Contains("2"))
                        {
                            detailView = detailView.Where(entry => entry.AuditImplementStatus == 3 && OpenstutusList.Contains((int)entry.ImplementationStatus)).ToList();
                        }
                    }
                    else if (StatusListAudit.Count == 2)
                    {
                        List<int> OpenstutusList = new List<int>();
                        OpenstutusList.Add(4); OpenstutusList.Add(5);
                        if (StatusListAudit.Contains("0") && StatusListAudit.Contains("1"))
                        {
                            detailView = detailView.Where(entry => (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                        }
                        else if (StatusListAudit.Contains("1") && StatusListAudit.Contains("2"))
                        {
                            detailView = detailView.Where(entry => entry.StatusTimeline >= DateTime.Now).ToList();
                        }
                        else if (StatusListAudit.Contains("2") && StatusListAudit.Contains("0"))
                        {
                            detailView = detailView.Where(entry => entry.StatusTimeline < DateTime.Now).ToList();
                        }
                    }
                }
                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();
                Session["grdDetailData"] = detailView.ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindDataFilter()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                int CustBranchID = -1;
                int CatID = -1;
                List<long> ProcessIDList = new List<long>();
                List<long?> SubProcessIDList = new List<long?>();
                int UserID = -1;
                bool IsAuditManager = false;
                bool IsDepartmentHead = false;
                bool IsManagement = false;
                List<String> FinancialYearList = new List<string>();
                List<String> PeriodList = new List<String>();
                String Type = String.Empty;
                string ObjCatName = string.Empty;
                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;
                List<int> VerticalIDList = new List<int>();
                List<long> CHKBranchlist = new List<long>();
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity1MultiSelect.Items[i].Selected)
                    {
                        if (clearLegalBranchList && CHKBranchlist.Count > 0)
                        {
                            //CHKBranchlist.Clear();
                            clearLegalBranchList = false;
                        }
                        clearSubEntity1List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    }

                }

                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity2MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity1List && CHKBranchlist.Count > 0)
                        {
                            //CHKBranchlist.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity3MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity2List && CHKBranchlist.Count > 0)
                        {
                            //CHKBranchlist.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                {

                    if (ddlFilterLocationMultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity3List && CHKBranchlist.Count > 0)
                        {
                            //CHKBranchlist.Clear();
                            clearSubEntity3List = false;
                        }
                        clearFilterLocation = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                    }
                }
                for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                {
                    if (ddlFinancialYearMultiSelect.Items[i].Selected)
                    {
                        FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                    }
                }

                for (int i = 0; i < ddlPeriodMultiSelect.Items.Count; i++)
                {
                    if (ddlPeriodMultiSelect.Items[i].Selected)
                    {
                        PeriodList.Add(ddlPeriodMultiSelect.Items[i].Text);
                    }
                }

                for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
                {
                    if (ddlProcessMultiSelect.Items[i].Selected)
                    {
                        ProcessIDList.Add(Convert.ToInt32(ddlProcessMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubProcessMultiSelect.Items.Count; i++)
                {
                    if (ddlSubProcessMultiSelect.Items[i].Selected)
                    {
                        SubProcessIDList.Add(Convert.ToInt32(ddlSubProcessMultiSelect.Items[i].Value));
                    }
                }
                for (int i = 0; i < ddlVerticalList.Items.Count; i++)
                {
                    if (ddlVerticalList.Items[i].Selected)
                    {
                        VerticalIDList.Add(Convert.ToInt32(ddlVerticalList.Items[i].Value));
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    Type = Request.QueryString["Type"];
                }
                //if (!string.IsNullOrEmpty(Request.QueryString["ObjCatName"]))
                //{
                //    ObjCatName = Convert.ToString(Request.QueryString["ObjCatName"]);
                //    if (ObjCatName == "null")
                //    {
                //        ObjCatName = null;
                //    }
                //}
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        IsDepartmentHead = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                List<long> ListofBranch = new List<long>();
                if (CustBranchID == -1)
                {
                    if (Session["BranchList"] != null)
                    {
                        ListofBranch = (List<long>)(Session["BranchList"]);
                    }
                }
                else
                {
                    Branchlist.Clear();
                    GetAllHierarchy(CustomerId, CustBranchID);
                    if (Branchlist.Count > 0)
                    {
                        ListofBranch = Branchlist.Select(x => (long)x).ToList();
                    }
                }
                List<int> ObsRating = new List<int>();
                if (Type.Trim().Equals("Major"))
                    ObsRating.Add(1);
                else if (Type.Trim().Equals("Moderate"))
                    ObsRating.Add(2);
                else if (Type.Trim().Equals("Minor"))
                    ObsRating.Add(3);
                else
                {
                    ObsRating.Add(1);
                    ObsRating.Add(2);
                    ObsRating.Add(3);
                }
                var detailView = InternalControlManagementDashboardRisk.GetObservationDetailView(CustomerId, CHKBranchlist, FinancialYearList, PeriodList, ObsRating, ObjCatName, ProcessIDList, UserID, IsAuditManager, IsManagement, IsDepartmentHead, SubProcessIDList, VerticalIDList);
                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();
                Session["grdDetailData"] = detailView.ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindUsers()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlPersonresponsible.Items.Clear();
            ddlPersonresponsible.DataTextField = "Name";
            ddlPersonresponsible.DataValueField = "ID";
            ddlPersonresponsible.DataSource = RiskCategoryManagement.FillUsers(CustomerId);
            ddlPersonresponsible.DataBind();
            ddlPersonresponsible.Items.Insert(0, new System.Web.UI.WebControls.ListItem("< Select Person Responsible >", "-1"));
        }
        public void BindObservationCategory()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlObservationCategory.DataTextField = "Name";
            ddlObservationCategory.DataValueField = "ID";
            ddlObservationCategory.Items.Clear();
            ddlObservationCategory.DataSource = ObservationSubcategory.FillObservationCategory(CustomerId);
            ddlObservationCategory.DataBind();
            ddlObservationCategory.Items.Insert(0, new System.Web.UI.WebControls.ListItem("< Select Observation Category >", "-1"));
        }

        public void BindObservationSubCategory(int ObservationId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlObservationSubCategory.DataTextField = "Name";
            ddlObservationSubCategory.DataValueField = "ID";
            ddlObservationSubCategory.Items.Clear();
            ddlObservationSubCategory.DataSource = ObservationSubcategory.FillObservationSubCategory(ObservationId, CustomerId);
            ddlObservationSubCategory.DataBind();
            ddlObservationSubCategory.Items.Insert(0, new ListItem("Observation SubCategory", "-1"));
        }

        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ApplyFilter = true;
                BindDataFilter();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            //if (ddlLegalEntity.SelectedValue != "-1")
            //{
            //    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            //}
            //else
            //{
            //    if (ddlSubEntity1.Items.Count > 0)
            //    {
            //        ddlSubEntity1.Items.Clear();
            //        ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
            //    }
            //    if (ddlSubEntity2.Items.Count > 0)
            //    {
            //        ddlSubEntity2.Items.Clear();
            //        ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
            //    }
            //    if (ddlSubEntity3.Items.Count > 0)
            //    {
            //        ddlSubEntity3.Items.Clear();
            //        ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
            //    }
            //    if (ddlFilterLocation.Items.Count > 0)
            //    {
            //        ddlFilterLocation.Items.Clear();
            //        ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
            //    }
            //}
            //BindProcess("P");
            //BindVertical();
            //BindSchedulingType();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                //BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
               // BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                //BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> FlagList = new List<string>();
            List<int> CHKBranchlist = new List<int>();
            for (int i = 0; i < ddlSchedulingType.Items.Count; i++)
            {
                if (ddlSchedulingType.Items[i].Selected)
                {
                    if (ddlSchedulingType.Items[i].Text == "Annually")
                    {
                        FlagList.Add("A");
                    }
                    else if (ddlSchedulingType.Items[i].Text == "Half Yearly")
                    {
                        FlagList.Add("H");
                    }
                    else if (ddlSchedulingType.Items[i].Text == "Quarterly")
                    {
                        FlagList.Add("Q");
                    }
                    else if (ddlSchedulingType.Items[i].Text == "Monthly")
                    {
                        FlagList.Add("M");
                    }
                    else if (ddlSchedulingType.Items[i].Text == "Special Audit")
                    {
                        FlagList.Add("S");
                    }
                    else if (ddlSchedulingType.Items[i].Text == "Phase")
                    {
                        bool clearLegalBranchList = false;
                        bool clearSubEntity1List = false;
                        bool clearSubEntity2List = false;
                        bool clearSubEntity3List = false;
                        bool clearFilterLocation = false;

                        CHKBranchlist.Clear();
                        for (int j = 0; j < ddlLegalEntityMultiSelect.Items.Count; j++)
                        {
                            if (ddlLegalEntityMultiSelect.Items[j].Selected)
                            {
                                clearLegalBranchList = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[j].Value));
                            }
                        }

                        for (int k = 0; k < ddlSubEntity1MultiSelect.Items.Count; k++)
                        {
                            if (ddlSubEntity1MultiSelect.Items[k].Selected)
                            {
                                if (clearLegalBranchList && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearLegalBranchList = false;
                                }
                                clearSubEntity1List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[k].Value));
                            }

                        }

                        for (int l = 0; l < ddlSubEntity2MultiSelect.Items.Count; l++)
                        {
                            if (ddlSubEntity2MultiSelect.Items[l].Selected)
                            {
                                if (clearSubEntity1List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity1List = false;
                                }
                                clearSubEntity2List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[l].Value));
                            }
                        }

                        for (int m = 0; m < ddlSubEntity3MultiSelect.Items.Count; m++)
                        {
                            if (ddlSubEntity3MultiSelect.Items[m].Selected)
                            {
                                if (clearSubEntity2List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity2List = false;
                                }
                                clearSubEntity3List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[m].Value));
                            }
                        }
                    }
                    List<int?> recordCount = new List<int?>(); ;
                    recordCount = UserManagementRisk.GetPhaseCountForAuditHeadDashboard(CHKBranchlist);
                    if (recordCount.Count > 0)
                    {
                        FlagList.Add("P");
                    }
                    BindAuditSchedule(FlagList, recordCount);
                }
            }
        }
        public void BindDocument(int ScheduledOnID, int ATBDID,int AuditID)
        {
            try
            {
                List<GetInternalAuditDocumentsView> ComplianceDocument = new List<GetInternalAuditDocumentsView>();
                divDeleteDocument.Visible = true;
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetInternalAuditDocumentsView(ScheduledOnID, ATBDID, AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptComplianceDocumnets.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindTransactions(int ScheduledOnID, int RiskCreationId,int AuditID)
        {
            try
            {                
                grdTransactionHistory.DataSource = Business.DashboardManagementRisk.GetAllInternalAuditTransactions(ScheduledOnID, RiskCreationId, AuditID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRemarks(int ProcessId, int ScheduledOnID, int InternalAuditInstance, int ATBDID,int AuditID)
        {
            try
            {                
                GridRemarks.DataSource = Business.DashboardManagementRisk.GetInternalAllRemarks(ProcessId, ScheduledOnID, ATBDID, AuditID);
                GridRemarks.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool CanChangeStatus(string Flag)
        {
            try
            {
                bool result = false;

                if (Flag == "OS")
                {
                    result = true;
                }                
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        public static List<InternalReviewHistory> GetFileData1(int id, int InternalAuditInstance, int AuditScheduleOnId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.InternalReviewHistories
                                where row.ID == id && row.InternalAuditInstance == InternalAuditInstance
                                && row.AuditScheduleOnID == AuditScheduleOnId
                                select row).ToList();

                return fileData;
            }
        }
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }
        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                int atbdid = -1;
                int custbranchid = -1;
                string FinancialYear = string.Empty;
                BindUsers();
                BindObservationCategory();
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                if (!string.IsNullOrEmpty(commandArgs[0]))
                {
                    atbdid = Convert.ToInt32(commandArgs[0]);
                }
                if (!string.IsNullOrEmpty(commandArgs[1]))
                {
                    custbranchid = Convert.ToInt32(commandArgs[1]);
                }
                if (!string.IsNullOrEmpty(commandArgs[2]))
                {
                    FinancialYear = Convert.ToString(commandArgs[2]);
                }
                txtAuditObjective.Enabled = false;
                txtAuditSteps.Enabled = false;
                txtAnalysisToBePerformed.Enabled = false;
                txtWalkthrough.Enabled = false;
                txtActualWorkDone.Enabled = false;
                txtpopulation.Enabled = false;
                txtSample.Enabled = false;
                txtObservationTitle.Enabled = false;
                txtObservationNumber.Enabled = false;
                txtObservation.Enabled = false;
                txtRisk.Enabled = false;
                txtRootcost.Enabled = false;
                txtfinancialImpact.Enabled = false;
                txtRecommendation.Enabled = false;
                txtMgtResponse.Enabled = false;
                txtTimeLine.Enabled = false;
                ddlPersonresponsible.Enabled = false;
                ddlObservationCategory.Enabled = false;
                ddlobservationRating.Enabled = false;
                ddlObservationSubCategory.Enabled = false;
                tbxDate.Enabled = false;

                rptComplianceDocumnets.Enabled = false;
                grdTransactionHistory.Enabled = false;
                GridRemarks.Enabled = false;

                txtAuditObjective.Text = string.Empty;
                txtAuditSteps.Text = string.Empty;
                txtAnalysisToBePerformed.Text = string.Empty;
                txtWalkthrough.Text = string.Empty;
                txtActualWorkDone.Text = string.Empty;
                txtpopulation.Text = string.Empty;
                txtSample.Text = string.Empty;
                txtObservationTitle.Text = string.Empty;
                txtObservationNumber.Enabled = false;
                txtObservation.Text = string.Empty;
                txtRisk.Text = string.Empty;
                txtRootcost.Text = string.Empty;
                txtfinancialImpact.Text = string.Empty;
                txtRecommendation.Text = string.Empty;
                txtMgtResponse.Text = string.Empty;
                txtTimeLine.Text = string.Empty;
                ddlPersonresponsible.SelectedValue = "-1";
                ddlObservationCategory.SelectedValue = "-1";
                ddlobservationRating.SelectedValue = "-1";
                tbxDate.Text = string.Empty;

                rptComplianceDocumnets.DataSource = null;
                rptComplianceDocumnets.DataBind();

                grdTransactionHistory.DataSource = null;
                grdTransactionHistory.DataBind();

                GridRemarks.DataSource = null;
                GridRemarks.DataBind();

                var complianceInfo = RiskCategoryManagement.GetInternalControlAuditResultByInstanceID(atbdid, custbranchid, FinancialYear,-1);

                if (complianceInfo != null)
                {
                    if (complianceInfo.AuditObjective != null)
                    {
                        txtAuditObjective.Text = complianceInfo.AuditObjective.Trim();
                    }

                    if (complianceInfo.AuditSteps != null)
                    {
                        txtAuditSteps.Text = complianceInfo.AuditSteps.Trim();
                    }

                    if (complianceInfo.AnalysisToBePerofrmed != null)
                    {
                        txtAnalysisToBePerformed.Text = complianceInfo.AnalysisToBePerofrmed.Trim();
                    }

                    if (complianceInfo.ProcessWalkthrough != null)
                    {
                        txtWalkthrough.Text = complianceInfo.ProcessWalkthrough.Trim();
                    }

                    if (complianceInfo.ActivityToBeDone != null)
                    {
                        txtActualWorkDone.Text = complianceInfo.ActivityToBeDone.Trim();
                    }

                    if (complianceInfo.Population != null)
                    {
                        txtpopulation.Text = complianceInfo.Population.Trim();
                    }
                    if (complianceInfo.Sample != null)
                    {
                        txtSample.Text = complianceInfo.Sample.Trim();
                    }
                    if (complianceInfo.ObservationNumber != null)
                    {
                        txtObservationNumber.Text = complianceInfo.ObservationNumber.Trim();
                    }
                    if (complianceInfo.ObservationTitle != null)
                    {
                        txtObservationTitle.Text = complianceInfo.ObservationTitle.Trim();
                    }
                    if (complianceInfo.Observation != null)
                    {
                        txtObservation.Text = complianceInfo.Observation.Trim();
                    }

                    if (complianceInfo.Risk != null)
                    {
                        txtRisk.Text = complianceInfo.Risk.Trim();
                    }
                    if (complianceInfo.RootCost != null)
                    {
                        txtRootcost.Text = Convert.ToString(complianceInfo.RootCost);
                    }
                    if (complianceInfo.FinancialImpact != null)
                    {
                        txtfinancialImpact.Text = Convert.ToString(complianceInfo.FinancialImpact);
                    }
                    if (complianceInfo.Recomendation != null)
                    {
                        txtRecommendation.Text = complianceInfo.Recomendation.Trim();
                    }
                    if (complianceInfo.ManagementResponse != null)
                    {
                        txtMgtResponse.Text = complianceInfo.ManagementResponse.Trim();
                    }
                    if (complianceInfo.TimeLine != null)
                    {
                        txtTimeLine.Text = Convert.ToDateTime(complianceInfo.TimeLine).ToString("dd-MMM-yyyy");
                    }
                    if (complianceInfo.PersonResponsible != null)
                    {
                        if (complianceInfo.PersonResponsible != -1)
                        {
                            ddlPersonresponsible.SelectedValue = Convert.ToString(complianceInfo.PersonResponsible);
                        }
                    }
                    if (complianceInfo.ObservationRating != null)
                    {
                        if (complianceInfo.ObservationRating != -1)
                        {
                            ddlobservationRating.SelectedValue = Convert.ToString(complianceInfo.ObservationRating);
                        }
                    }
                    if (complianceInfo.ObservationCategory != null)
                    {
                        if (complianceInfo.ObservationCategory != -1)
                        {
                            ddlObservationCategory.SelectedValue = Convert.ToString(complianceInfo.ObservationCategory);

                            if (ddlObservationCategory.SelectedValue != "-1")
                                ddlObservationCategory_SelectedIndexChanged(sender, e);
                        }
                    }

                    if (complianceInfo.ObservationSubCategory != null)
                    {
                        if (complianceInfo.ObservationSubCategory != -1)
                        {
                            ddlObservationSubCategory.SelectedValue = Convert.ToString(complianceInfo.ObservationSubCategory);                            
                        }
                    }
                   
                    var getDated = RiskCategoryManagement.GetInternalAuditTransactionbyIDNew(Convert.ToInt64(complianceInfo.AuditScheduleOnID), Convert.ToInt32(complianceInfo.ATBDId), complianceInfo.FinancialYear, complianceInfo.ForPerid, 4,-1);

                    if (getDated != null)
                    {
                        tbxDate.Text = getDated.Dated != null ? getDated.Dated.Value.ToString("dd-MM-yyyy") : null;
                    }

                    BindDocument(Convert.ToInt32(complianceInfo.AuditScheduleOnID), Convert.ToInt32(complianceInfo.ATBDId),-1);
                    BindTransactions(Convert.ToInt32(complianceInfo.AuditScheduleOnID), Convert.ToInt32(complianceInfo.ATBDId),-1);
                    BindRemarks(Convert.ToInt32(complianceInfo.ProcessId), Convert.ToInt32(complianceInfo.AuditScheduleOnID), Convert.ToInt32(complianceInfo.InternalAuditInstance), Convert.ToInt32(complianceInfo.ATBDId),-1);
                    // tbxDate.Text = complianceInfo.Risk.Trim();

                    Tab1_Click(sender, e);

                    MainView.ActiveViewIndex = 0;

                    upATBDDetails.Update();

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDetailsDialog();", true);                   
                }
                else
                {
                    var RiskActivityToBeDoneMapping = RiskCategoryManagement.GetRiskActivityToBeDoneMappingByInstanceID(atbdid);

                    if (RiskActivityToBeDoneMapping != null)
                    {
                        if (RiskActivityToBeDoneMapping.ActivityTobeDone != null)
                        {
                            txtAuditSteps.Text = RiskActivityToBeDoneMapping.ActivityTobeDone.Trim();
                        }
                    }

                    Tab1_Click(sender, e);

                    MainView.ActiveViewIndex = 0;
                    upATBDDetails.Update();

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDetailsDialog();", true);                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlObservationCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
            {
                if (ddlObservationCategory.SelectedValue != "-1")
                {
                    BindObservationSubCategory(Convert.ToInt32(ddlObservationCategory.SelectedValue));
                }
            }
        }
        protected void DownLoadClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblID = (Label)gvr.FindControl("lblID");
                Label lblriskID = (Label)gvr.FindControl("lblriskID");
                Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<InternalReviewHistory> fileData = GetFileData1(Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text));
                    int i = 0;
                    string directoryName = "abc";
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }                            
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TestingDocument.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    String FileName = String.Empty;

                    FileName = "Observation Report";

                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                    DataTable ExcelData = null;

                    DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                    ExcelData = view.ToTable("Selected", false, "Branch", "FinancialYear", "ForMonth", "ProcessName", "ActivityDescription", "Observation",  "ManagementResponse", "Recomendation", "StatusTimeline", "ObservationCategoryName", "ObservatioRating");
                    ExcelData.Columns.Add("Rating");

                    var customer = UserManagementRisk.GetCustomerName(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID));

                    exWorkSheet.Cells["A1"].Value ="Report Generated On:";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");                    
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A2"].Value = customer;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                    foreach (DataRow item in ExcelData.Rows)
                    {
                        if (item["ObservatioRating"] != null)
                        {
                            if (item["ObservatioRating"].ToString() == "1")
                                item["Rating"] = "Major";
                            else if (item["ObservatioRating"].ToString() == "2")
                                item["Rating"] = "Moderate";
                            else if (item["ObservatioRating"].ToString() == "3")
                                item["Rating"] = "Minor";
                        }
                    }

                    ExcelData.Columns.Remove("ObservatioRating");
                    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                    //exWorkSheet.Cells["A3"].Value = lblDetailViewTitle.Text + " Report";
                    exWorkSheet.Cells["A3"].Value = "Observation Report";
                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A3"].AutoFitColumns(50);

                    exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A5"].Value = "Location";
                    exWorkSheet.Cells["A5"].AutoFitColumns(50);

                    exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B5"].Value = "Financial Year";
                    exWorkSheet.Cells["B5"].AutoFitColumns(15);

                    exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C5"].Value = "Period";
                    exWorkSheet.Cells["C5"].AutoFitColumns(15);

                    exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D5"].Value = "Process";
                    exWorkSheet.Cells["D5"].AutoFitColumns(25);

                    exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E5"].Value = "Activity Description";
                    exWorkSheet.Cells["E5"].AutoFitColumns(50);

                    exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F5"].Value = "Observation";
                    exWorkSheet.Cells["F5"].AutoFitColumns(50);

                    exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G5"].Value = "Management Response";
                    exWorkSheet.Cells["G5"].AutoFitColumns(50);

                    exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H5"].Value = "Recommandation";
                    exWorkSheet.Cells["H5"].AutoFitColumns(50);

                    exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I5"].Value = "Time Line";
                    exWorkSheet.Cells["I5"].AutoFitColumns(15);

                    exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J5"].Value = "Observation Category";
                    exWorkSheet.Cells["J5"].AutoFitColumns(25);

                    exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K5"].Value = "Observation Rating";
                    exWorkSheet.Cells["K5"].AutoFitColumns(20);

                    //Assign borders
                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 11])
                    {
                        col.Style.Numberformat.Format = "dd/MM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.WrapText = true;
                        //col.AutoFitColumns(20);
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        //col.Style.Border.Right.Styl
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnets");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbutton");
            if (lbtLinkDocbutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                if (ApplyFilter)
                {
                    BindDataFilter();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
                else
                {
                    BindData();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }        
        protected void Tab1_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "active");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 0;         
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "active");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 1;           
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "active");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 2;            
        }
        protected void Tab4_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "active");
            MainView.ActiveViewIndex = 3;            
        }

        protected void ddlLegalEntityMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> CHKBranchlist = new List<long>();

            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }

            if (CHKBranchlist.Count > 0)
            {
                BindSubEntityData(ddlSubEntity1MultiSelect, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity1MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity1MultiSelect.Items.Clear();
                }
                if (ddlSubEntity2MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity2MultiSelect.Items.Clear();
                }
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity1MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> parentIdsList = new List<long>();
            parentIdsList.Clear();
            for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity1MultiSelect.Items[i].Selected)
                {
                    parentIdsList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                }
            }
            if (parentIdsList.Count > 0)
            {
                BindSubEntityData(ddlSubEntity2MultiSelect, parentIdsList);
            }
            else
            {
                if (ddlSubEntity2MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity2MultiSelect.Items.Clear();
                }
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity2MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> parentIdsList = new List<long>();
            parentIdsList.Clear();
            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    parentIdsList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }
            if (parentIdsList.Count > 0)
            {
                BindSubEntityData(ddlSubEntity3MultiSelect, parentIdsList);
            }
            else
            {
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlFilterLocationMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity3MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> parentIdsList = new List<long>();
            parentIdsList.Clear();
            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    parentIdsList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }
            if (parentIdsList.Count > 0)
            {
                BindSubEntityData(ddlFilterLocationMultiSelect, parentIdsList);
            }
            else
            {
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            bindPageNumber();
        }

        protected void ddlProcessMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSubProcess();
        }

        private void BindSubProcess()
        {
            List<long?> ProcessIdsList = new List<long?>();
            for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
            {
                if (ddlProcessMultiSelect.Items[i].Selected == true)
                {
                    ProcessIdsList.Add(Convert.ToInt32(ddlProcessMultiSelect.Items[i].Value));
                }
            }
            if (ProcessIdsList.Count > 0)
            {
                ddlSubProcessMultiSelect.DataTextField = "Name";
                ddlSubProcessMultiSelect.DataValueField = "ID";
                ddlSubProcessMultiSelect.DataSource = ProcessManagement.FillSubProcessDropdownManagementForAuditHeadDashBoard(ProcessIdsList, Portal.Common.AuthenticationHelper.UserID);
                ddlSubProcessMultiSelect.DataBind();
            }
            else
            {
                ddlSubProcessMultiSelect.Items.Clear();
            }
        }

        protected void ddlSchedulingTypeMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> FlagList = new List<string>();
            List<int> CHKBranchlist = new List<int>();
            for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
            {
                if (ddlSchedulingTypeMultiSelect.Items[i].Selected)
                {
                    if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Annually")
                    {
                        FlagList.Add("A");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Half Yearly")
                    {
                        FlagList.Add("H");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Quarterly")
                    {
                        FlagList.Add("Q");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Monthly")
                    {
                        FlagList.Add("M");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Special Audit")
                    {
                        FlagList.Add("S");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Phase")
                    {
                        bool clearLegalBranchList = false;
                        bool clearSubEntity1List = false;
                        bool clearSubEntity2List = false;
                        bool clearSubEntity3List = false;
                        bool clearFilterLocation = false;

                        CHKBranchlist.Clear();
                        for (int j = 0; j < ddlLegalEntityMultiSelect.Items.Count; j++)
                        {
                            if (ddlLegalEntityMultiSelect.Items[j].Selected)
                            {
                                clearLegalBranchList = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[j].Value));
                            }
                        }

                        for (int k = 0; k < ddlSubEntity1MultiSelect.Items.Count; k++)
                        {
                            if (ddlSubEntity1MultiSelect.Items[k].Selected)
                            {
                                if (clearLegalBranchList && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearLegalBranchList = false;
                                }
                                clearSubEntity1List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[k].Value));
                            }

                        }

                        for (int l = 0; l < ddlSubEntity2MultiSelect.Items.Count; l++)
                        {
                            if (ddlSubEntity2MultiSelect.Items[l].Selected)
                            {
                                if (clearSubEntity1List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity1List = false;
                                }
                                clearSubEntity2List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[l].Value));
                            }
                        }

                        for (int m = 0; m < ddlSubEntity3MultiSelect.Items.Count; m++)
                        {
                            if (ddlSubEntity3MultiSelect.Items[m].Selected)
                            {
                                if (clearSubEntity2List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity2List = false;
                                }
                                clearSubEntity3List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[m].Value));
                            }
                        }
                    }
                    List<int?> recordCount = new List<int?>(); ;
                    recordCount = UserManagementRisk.GetPhaseCountForAuditHeadDashboard(CHKBranchlist);
                    if (recordCount.Count > 0)
                    {
                        FlagList.Add("P");
                    }
                    BindAuditSchedule(FlagList, recordCount);
                }
            }
        }
    }
}