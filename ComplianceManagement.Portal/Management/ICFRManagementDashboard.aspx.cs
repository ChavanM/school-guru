﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class ICFRManagementDashboard : System.Web.UI.Page
    {
        protected string perStatusPieChartAPRJUN;
        protected string perStatusPieChartJULSEP;
        protected string perStatusPieChartOCTDEC;
        protected string perStatusPieChartJANMAR;
        protected string QuarterlyFailedControlSeries;
        protected string ProcesswiseObservationSeries;
        public  List<long> Branchlist = new List<long>();
        protected static string ProcessWiseObservationStatusChart;
        protected static string QuarterlyFailedControlStatusChart;
        protected static string ProcessWiseFailedControlStatusChart;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                home.Value = "true";              
                BindProcess("P");                
                BindFinancialYear();
                BindLegalEntityData();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                BindGraphData();
            }
        }
        public  List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(long customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public  void LoadSubEntities(long customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {


            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                ddlSubProcess.Items.Clear();
                ddlSubProcess.DataTextField = "Name";
                ddlSubProcess.DataValueField = "Id";
                ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                ddlSubProcess.DataBind();
                ddlSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        private void BindProcess(string flag)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ddlProcess.Items.Clear();
                ddlProcess.DataTextField = "Name";
                ddlProcess.DataValueField = "Id";
                ddlProcess.DataSource = ProcessManagement.FillProcessDropdownManagement(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                ddlProcess.DataBind();
                ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }                
        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindGraphData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {          
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();                    
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();                    
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }            
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();                    
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }                        
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }                        
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {                        
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        
        

        public static List<Mst_ObservationCategory> GetObservationCategoryAll(string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditCategorys = (from MOC in entities.Mst_ObservationCategory
                                      where MOC.IsActive == false
                                      select MOC).ToList().Distinct();

                AuditCategorys = AuditCategorys.OrderBy(entry => entry.Name);

                return AuditCategorys.ToList();
            }
        }

        public void BindGraphData()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                int branchid = -1;
                int ProcessID = -1;
                int SubProcessID = -1;
                string FinYear = string.Empty;
                string Period = string.Empty;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }                
                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        ProcessID = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        SubProcessID = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }

                if (FinYear == "")
                {
                    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                }

                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(CustomerId, branchid);

                List<int?> ListofBranch = new List<int?>();

                Session["BranchList"] = null;

                if (Branchlist.Count > 0)
                {
                    ListofBranch = Branchlist.Select(x => (int?) x).ToList();
                    Session["BranchList"] = ListofBranch;
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;
                    var dataAuditSummary = (from c in entities.AuditInstanceTransactionViews
                                            join EAAMR in entities.EntitiesAssignmentManagementRisks
                                            on c.ProcessId equals EAAMR.ProcessId
                                            where c.CustomerBranchID == EAAMR.BranchID
                                            && c.CustomerID == CustomerId
                                            && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                            && EAAMR.ISACTIVE == true
                                            select c).ToList();

                    //var dataAuditSummary = (from c in entities.AuditInstanceTransactionViews
                    //                        where c.CustomerID == customerID
                    //                        select c).ToList();                    

                    GetQuarterlyTestingStatusDashboardCount(dataAuditSummary, CustomerId, Branchlist.ToList(), FinYear, ProcessID, SubProcessID);

                    GetQuarterlyFailedControlCount(dataAuditSummary, CustomerId, Branchlist.ToList(), FinYear, ProcessID, SubProcessID);

                    GetProcessWiseObservationDashboardCount(dataAuditSummary, CustomerId, Branchlist.ToList(), FinYear, Period, ProcessID);

                    GetProcessWiseFailedControlCount(dataAuditSummary, CustomerId, Branchlist.ToList(), FinYear, Period, ProcessID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void GetQuarterlyTestingStatusDashboardCount(List<AuditInstanceTransactionView> MasterRecords, long CustomerID, List<long> BranchList, String FinYear, int ProcessID, int SubProcessID)
        {
            try
            {
                int CustomerBranchID = -1;
                List<string> GraphName = new List<string>();
                if (ddlSchedulingType.SelectedValue != "-1")
                {
                    if (ddlSchedulingType.SelectedItem.Text == "Annually")
                    {
                        #region Annually - Test Results
                        List<string> quarter = new List<string>();
                        GraphName.Clear();
                        GraphName.Add("Annually");

                        quarter.Clear();
                        quarter.Add("Apr - Jun");
                        quarter.Add("Jul - Sep");
                        quarter.Add("Oct - Dec");
                        quarter.Add("Jan - Mar");

                        long totalPassCount = 0;
                        long PassKeyCount = 0;
                        long PassNonKeyCount = 0;

                        long totalFailcount = 0;
                        long FailKeyCount = 0;
                        long FailNonKeyCount = 0;

                        long totalNotTestedcount = 0;
                        long NotTestedKeyCount = 0;
                        long NotTestedNonKeyCount = 0;

                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            if (GraphName.Count > 0)
                            {
                                for (int i = 0; i < GraphName.Count; i++)
                                {
                                    string qname = "";
                                    qname = GraphName[i].ToString();

                                    var Records = (from row in MasterRecords
                                                   where row.RoleID == 3 && quarter.Contains(row.ForMonth)
                                                   select row).ToList();

                                    if (BranchList.Count > 0)
                                        Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                                    if (FinYear != "")
                                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                                    if (ProcessID != -1)
                                        Records = Records.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                                    if (SubProcessID != -1)
                                        Records = Records.Where(Entry => Entry.SubProcessId == SubProcessID).ToList();

                                    Records = Records.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                                   var NotTestedcount = Records.Where(a => a.TOD == null && a.TOE == null).ToList();
                                   // var NotTestedcount = Records.Where(a => ((a.TOD == 2 && a.TOE == -1 && a.AuditStatusID == 3) || (a.TOD == null && a.TOE == null))).ToList();
                                    totalNotTestedcount = NotTestedcount.Count;
                                    NotTestedKeyCount = NotTestedcount.Where(a => a.KeyId == 1).ToList().Count;
                                    NotTestedNonKeyCount = NotTestedcount.Where(a => a.KeyId == 2).ToList().Count;

                                    var PassCount = Records.Where(a => (a.TOD == 1 && a.TOE == 1) || (a.TOD == 1 && a.TOE == 3) || (a.TOD == 3 && a.TOE == 1) || (a.TOD == 3 && a.TOE == 3)).ToList();
                                    totalPassCount = PassCount.Count;
                                    PassKeyCount = PassCount.Where(a => a.KeyId == 1).ToList().Count;
                                    PassNonKeyCount = PassCount.Where(a => a.KeyId == 2).ToList().Count;

                                    var Failcount = Records.Where(a => a.AuditStatusID == 3 && ((a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2))).ToList();
                                    totalFailcount = Failcount.Count;
                                    FailKeyCount = Failcount.Where(a => a.KeyId == 1).ToList().Count;
                                    FailNonKeyCount = Failcount.Where(a => a.KeyId == 2).ToList().Count;


                                    if (GraphName[i].ToString() == "Annually")
                                    {
                                        perStatusPieChartDivAPRJUN.Attributes["class"] = "col-md-12";
                                        perStatusPieChartDivAPRJUN.Style["width"] = "100%";
                                        perStatusPieChartDivAPRJUN.Style["height"] = "300px";

                                        perStatusPieChartDivJULSEP.Attributes["class"] = "progressno";
                                        perStatusPieChartDivOCTDEC.Attributes["class"] = "progressno";
                                        perStatusPieChartDivJANMAR.Attributes["class"] = "progressno";

                                        perStatusPieChartAPRJUN = "chart: {type: 'pie',events:{drilldown: function(e) {this.setTitle({ text: e.point.name });this.subtitle.update({ text: 'Annually' });}, " +
                                        " drillup: function() {this.setTitle({ text: 'Per Status' });this.subtitle.update({ text: 'Annually' });}},}," +
                                        " title:{text: 'Per Status',style:{display:'none'},},subtitle:{text: 'Annually',style:{fontWeight: '300', " +
                                        " fontSize:'15px'}},xAxis:{type: 'category',},plotOptions:{series:{dataLabels:{enabled: true,format: '{y}',distance: 5,},showInLegend: true,},}, " +
                                        " legend:{itemDistance: 0,},tooltip:{hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',pointFormat:'{series.name}: <b>{point.y}</b>'},";

                                        perStatusPieChartAPRJUN += "series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>',},data: [{name: 'Pass',y: " + totalPassCount + ",color: '#FF7473',drilldown: 'pass',},{name: 'Fail',y: " + totalFailcount + ",color: '#FFC952',drilldown: 'fail',},{name: 'NotTested',y: " + totalNotTestedcount + ",color:'#1FD9E1',drilldown: 'nottested',}],}],";
                                        perStatusPieChartAPRJUN += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                                           " series: [" +

                                                            " {id: 'nottested',name: 'NotTested',cursor: 'pointer',data: [{name: 'Key',y: " + NotTestedKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Not Tested- Key                   
                                                            " fpopulateddataQuarter('Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','A','MG')" +
                                                            " },},},{name: 'Non Key',y: " + NotTestedNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                            // " // Not Tested - Non Key                               
                                                            " fpopulateddataQuarter('Non Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','A','MG')" +
                                                            " },},}],}," +
                                                            " {id: 'fail',name: 'Fail',cursor: 'pointer',data: [{name: 'Key',y: " + FailKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Fail- Key   
                                                            " fpopulateddataQuarter('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','A','MG')" +
                                                            " },},},{name: 'Non Key',y: " + FailNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                            // " // Fail - Non Key   
                                                            " fpopulateddataQuarter('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','A','MG')" +

                                                            " },},}],}," +
                                                            " {id: 'pass',name: 'Pass',cursor: 'pointer',data: [{name: 'Key',y: " + PassKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Pass- Key  
                                                            " fpopulateddataQuarter('Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','A','MG')" +
                                                            " },},},{name: 'Non Key',y: " + PassNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                            // " // Pass - Non Key   
                                                            " fpopulateddataQuarter('Non Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','A','MG')" +

                                                            " },},}],}],}";
                                    }
                                }
                            }//Query Count End                            
                        }//Using End
                        #endregion                     
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                    {
                        #region Half Yearly - Test Results
                        List<string> HalfYear1 = new List<string>();
                        List<string> HalfYear2 = new List<string>();
                        GraphName.Clear();
                        GraphName.Add("Apr - Sep");
                        GraphName.Add("Oct - Mar");

                        HalfYear1.Clear();
                        HalfYear1.Add("Apr - Jun");
                        HalfYear1.Add("Jul - Sep");

                        HalfYear2.Clear();
                        HalfYear2.Add("Oct - Dec");
                        HalfYear2.Add("Jan - Mar");

                        long totalPassCount = 0;
                        long PassKeyCount = 0;
                        long PassNonKeyCount = 0;

                        long totalFailcount = 0;
                        long FailKeyCount = 0;
                        long FailNonKeyCount = 0;

                        long totalNotTestedcount = 0;
                        long NotTestedKeyCount = 0;
                        long NotTestedNonKeyCount = 0;

                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            if (GraphName.Count > 0)
                            {
                                for (int i = 0; i < GraphName.Count; i++)
                                {
                                    string qname = "";
                                    qname = GraphName[i].ToString();

                                    var Records = (from row in MasterRecords
                                                   where row.RoleID == 3
                                                   select row).ToList();

                                    if (qname == "Apr - Sep")
                                    {
                                        Records = Records.Where(Entry => HalfYear1.Contains(Entry.ForMonth)).ToList();
                                    }
                                    if (qname == "Oct - Mar")
                                    {
                                        Records = Records.Where(Entry => HalfYear2.Contains(Entry.ForMonth)).ToList();
                                    }
                                    if (BranchList.Count > 0)
                                        Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                                    if (FinYear != "")
                                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                                    if (ProcessID != -1)
                                        Records = Records.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                                    if (SubProcessID != -1)
                                        Records = Records.Where(Entry => Entry.SubProcessId == SubProcessID).ToList();

                                    Records = Records.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                                    var NotTestedcount = Records.Where(a => a.TOD == null && a.TOE == null).ToList();
                                    //var NotTestedcount = Records.Where(a => ((a.TOD == 2 && a.TOE == -1 && a.AuditStatusID == 3) || (a.TOD == null && a.TOE == null))).ToList();
                                    totalNotTestedcount = NotTestedcount.Count;
                                    NotTestedKeyCount = NotTestedcount.Where(a => a.KeyId == 1).ToList().Count;
                                    NotTestedNonKeyCount = NotTestedcount.Where(a => a.KeyId == 2).ToList().Count;

                                    var PassCount = Records.Where(a => (a.TOD == 1 && a.TOE == 1) || (a.TOD == 1 && a.TOE == 3) || (a.TOD == 3 && a.TOE == 1) || (a.TOD == 3 && a.TOE == 3)).ToList();
                                    totalPassCount = PassCount.Count;
                                    PassKeyCount = PassCount.Where(a => a.KeyId == 1).ToList().Count;
                                    PassNonKeyCount = PassCount.Where(a => a.KeyId == 2).ToList().Count;

                                    var Failcount = Records.Where(a => a.AuditStatusID == 3 && ((a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2))).ToList();
                                    totalFailcount = Failcount.Count;
                                    FailKeyCount = Failcount.Where(a => a.KeyId == 1).ToList().Count;
                                    FailNonKeyCount = Failcount.Where(a => a.KeyId == 2).ToList().Count;


                                    if (GraphName[i].ToString() == "Apr - Sep")
                                    {
                                        //perStatusPieChartDivAPRJUN.Attributes["class"] = "progressHF1";
                                        perStatusPieChartDivAPRJUN.Attributes["class"] = "col-md-6";
                                        perStatusPieChartDivAPRJUN.Style["width"] = "50%";
                                        perStatusPieChartDivAPRJUN.Style["height"] = "300px";

                                        perStatusPieChartDivJULSEP.Attributes["class"] = "progressno";

                                        //perStatusPieChartDivOCTDEC.Attributes["class"] = "progressHF2";
                                        perStatusPieChartDivOCTDEC.Attributes["class"] = "col-md-6";
                                        perStatusPieChartDivOCTDEC.Style["width"] = "50%";
                                        perStatusPieChartDivOCTDEC.Style["height"] = "300px";

                                        perStatusPieChartDivJANMAR.Attributes["class"] = "progressno";

                                        perStatusPieChartAPRJUN = "chart: {type: 'pie',events:{drilldown: function(e) {this.setTitle({ text: e.point.name });this.subtitle.update({ text: 'Apr - Sep' });}, " +
                                       "drillup: function() {this.setTitle({ text: 'Per Status' });this.subtitle.update({ text: 'Apr - Sep' });}},}, " +
                                       "title:{text: 'Per Status',style:{display:'none'},},subtitle:{text: 'Apr - Sep',style:{fontWeight: '300',fontSize:'15px'}},xAxis:{type: 'category',}, " +
                                       "plotOptions:{series:{dataLabels:{enabled: true,format: '{y}',distance: 5,},showInLegend: true,},},legend:{itemDistance: 0,},tooltip:{hideDelay: 0, " +
                                       "backgroundColor: 'rgba(247,247,247,1)',pointFormat:'{series.name}: <b>{point.y}</b>'},";


                                        perStatusPieChartAPRJUN += "series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>',},data: [{name: 'Pass',y: " + totalPassCount + ",color: '#FF7473',drilldown: 'pass',},{name: 'Fail',y: " + totalFailcount + ",color: '#FFC952',drilldown: 'fail',},{name: 'NotTested',y: " + totalNotTestedcount + ",color:'#1FD9E1',drilldown: 'nottested',}],}],";
                                        perStatusPieChartAPRJUN += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                                           " series: [" +

                                                            " {id: 'nottested',name: 'NotTested',cursor: 'pointer',data: [{name: 'Key',y: " + NotTestedKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Not Tested- Key                   
                                                            " fpopulateddataQuarter('Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF1','MG')" +
                                                            " },},},{name: 'Non Key',y: " + NotTestedNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                            // " // Not Tested - Non Key                               
                                                            " fpopulateddataQuarter('Non Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF1','MG')" +
                                                            " },},}],}," +
                                                            " {id: 'fail',name: 'Fail',cursor: 'pointer',data: [{name: 'Key',y: " + FailKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Fail- Key   
                                                            " fpopulateddataQuarter('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF1','MG')" +
                                                            " },},},{name: 'Non Key',y: " + FailNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                            // " // Fail - Non Key   
                                                            " fpopulateddataQuarter('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF1','MG')" +

                                                            " },},}],}," +
                                                            " {id: 'pass',name: 'Pass',cursor: 'pointer',data: [{name: 'Key',y: " + PassKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Pass- Key  
                                                            " fpopulateddataQuarter('Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF1','MG')" +
                                                            " },},},{name: 'Non Key',y: " + PassNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                            // " // Pass - Non Key   
                                                            " fpopulateddataQuarter('Non Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF1','MG')" +

                                                            " },},}],}],}";
                                    }
                                    if (GraphName[i].ToString() == "Oct - Mar")
                                    {
                                        //perStatusPieChartDivAPRJUN.Attributes["class"] = "progressHF1";
                                        perStatusPieChartDivAPRJUN.Attributes["class"] = "col-md-6";
                                        perStatusPieChartDivAPRJUN.Style["width"] = "50%";
                                        perStatusPieChartDivAPRJUN.Style["height"] = "300px";

                                        perStatusPieChartDivJULSEP.Attributes["class"] = "progressno";

                                        //perStatusPieChartDivOCTDEC.Attributes["class"] = "progressHF2";
                                        perStatusPieChartDivOCTDEC.Attributes["class"] = "col-md-6";
                                        perStatusPieChartDivOCTDEC.Style["width"] = "50%";
                                        perStatusPieChartDivOCTDEC.Style["height"] = "300px";

                                        perStatusPieChartDivJANMAR.Attributes["class"] = "progressno";

                                        perStatusPieChartOCTDEC = "chart:{type: 'pie',events:{drilldown: function(e) {this.setTitle({ text: e.point.name });this.subtitle.update({ text: 'Oct - Mar' }); " +
                                        " },drillup: function() {this.setTitle({ text: 'Per Status' });this.subtitle.update({ text: 'Oct - Mar' });}},}, " +
                                        " title:{text: 'Per Status',style:{display:'none'},},subtitle:{text: 'Oct - Mar',style:{fontWeight: '300',fontSize:'15px'}}, " +
                                        " xAxis:{type: 'category',},plotOptions:{series:{dataLabels:{enabled: true,format: '{y}',distance: 5,},showInLegend: true, " +
                                        " },},legend:{itemDistance: 0,},tooltip:{hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',pointFormat:'{series.name}: <b>{point.y}</b>'},";


                                        perStatusPieChartOCTDEC += "series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>',},data: [{name: 'Pass',y: " + totalPassCount + ",color: '#FF7473',drilldown: 'pass',},{name: 'Fail',y: " + totalFailcount + ",color: '#FFC952',drilldown: 'fail',},{name: 'NotTested',y: " + totalNotTestedcount + ",color:'#1FD9E1',drilldown: 'nottested',}],}],";
                                        perStatusPieChartOCTDEC += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                                           " series: [" +

                                                            " {id: 'nottested',name: 'NotTested',cursor: 'pointer',data: [{name: 'Key',y: " + NotTestedKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                             // " // Not Tested- Key                   
                                                             " fpopulateddataQuarter('Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF2','MG')" +
                                                            " },},},{name: 'Non Key',y: " + NotTestedNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Not Tested - Non Key                               
                                                             " fpopulateddataQuarter('Non Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF2','MG')" +
                                                            " },},}],}," +
                                                            " {id: 'fail',name: 'Fail',cursor: 'pointer',data: [{name: 'Key',y: " + FailKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                             // " // Fail- Key   
                                                             " fpopulateddataQuarter('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF2','MG')" +
                                                            " },},},{name: 'Non Key',y: " + FailNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Fail - Non Key   
                                                             " fpopulateddataQuarter('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF2','MG')" +

                                                            " },},}],}," +
                                                            " {id: 'pass',name: 'Pass',cursor: 'pointer',data: [{name: 'Key',y: " + PassKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Pass- Key  
                                                            " fpopulateddataQuarter('Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF2','MG')" +
                                                            " },},},{name: 'Non Key',y: " + PassNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Pass - Non Key   
                                                             " fpopulateddataQuarter('Non Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','HF2','MG')" +

                                                            " },},}],}],}";
                                    }
                                }
                            }//Query Count End                            
                        }//Using End
                        #endregion
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                    {
                        #region Quarterly - Test Results
                        List<string> quarter = new List<string>();
                        GraphName.Clear();
                        GraphName.Add("Apr - Jun");
                        GraphName.Add("Jul - Sep");
                        GraphName.Add("Oct - Dec");
                        GraphName.Add("Jan - Mar");

                        quarter.Clear();
                        quarter.Add("Apr - Jun");
                        quarter.Add("Jul - Sep");
                        quarter.Add("Oct - Dec");
                        quarter.Add("Jan - Mar");

                        perStatusPieChartAPRJUN = string.Empty;
                        perStatusPieChartJULSEP = string.Empty;
                        perStatusPieChartOCTDEC = string.Empty;
                        perStatusPieChartJANMAR = string.Empty;
                        long totalPassCount = 0;
                        long PassKeyCount = 0;
                        long PassNonKeyCount = 0;

                        long totalFailcount = 0;
                        long FailKeyCount = 0;
                        long FailNonKeyCount = 0;

                        long totalNotTestedcount = 0;
                        long NotTestedKeyCount = 0;
                        long NotTestedNonKeyCount = 0;


                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            if (quarter.Count > 0)
                            {
                                for (int i = 0; i < quarter.Count; i++)
                                {
                                    string qname = "";
                                    qname = quarter[i].ToString();

                                    var Records = (from row in MasterRecords
                                                   where row.RoleID == 3
                                                   && row.ForMonth == qname
                                                   select row).ToList();

                                    if (BranchList.Count > 0)
                                        Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                                    if (FinYear != "")
                                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();


                                    if (ProcessID != -1)
                                        Records = Records.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                                    if (SubProcessID != -1)
                                        Records = Records.Where(Entry => Entry.SubProcessId == SubProcessID).ToList();



                                    Records = Records.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                                    var NotTestedcount= Records.Where(a => a.TOD == null && a.TOE == null).ToList();
                                   // var NotTestedcount = Records.Where(a => ((a.TOD == 2 && a.TOE == -1 && a.AuditStatusID == 3) || (a.TOD == null && a.TOE == null))).ToList();
                                    totalNotTestedcount = NotTestedcount.Count;
                                    NotTestedKeyCount = NotTestedcount.Where(a => a.KeyId == 1).ToList().Count;
                                    NotTestedNonKeyCount = NotTestedcount.Where(a => a.KeyId == 2).ToList().Count;

                                    var PassCount = Records.Where(a => (a.TOD == 1 && a.TOE == 1) || (a.TOD == 1 && a.TOE == 3) || (a.TOD == 3 && a.TOE == 1) || (a.TOD == 3 && a.TOE == 3)).ToList();
                                    totalPassCount = PassCount.Count;
                                    PassKeyCount = PassCount.Where(a => a.KeyId == 1).ToList().Count;
                                    PassNonKeyCount = PassCount.Where(a => a.KeyId == 2).ToList().Count;
                                    //var Failcount = Records.Where(a =>  (a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2)).ToList();
                                    var Failcount = Records.Where(a => a.AuditStatusID == 3 && ((a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2))).ToList();
                                    totalFailcount = Failcount.Count;
                                    FailKeyCount = Failcount.Where(a => a.KeyId == 1).ToList().Count;
                                    FailNonKeyCount = Failcount.Where(a => a.KeyId == 2).ToList().Count;
                                    if (quarter[i].ToString() == "Apr - Jun")
                                    {
                                        //perStatusPieChartDivAPRJUN.Attributes["class"] = "progressQ1";
                                        perStatusPieChartDivAPRJUN.Attributes["class"] = "col-md-5";
                                        perStatusPieChartDivAPRJUN.Style["width"] = "25%";
                                        perStatusPieChartDivAPRJUN.Style["height"] = "300px";

                                        perStatusPieChartAPRJUN = "chart: {type: 'pie',events:{drilldown: function(e) {this.setTitle({ text: e.point.name });this.subtitle.update({ text: 'Apr - Jun' });}, " +
                                       "drillup: function() {this.setTitle({ text: 'Per Status' });this.subtitle.update({ text: 'Apr - Jun' });}},}, " +
                                       "title:{text: 'Per Status',style:{display:'none'},},subtitle:{text: 'Apr - Jun',style:{fontWeight: '300',fontSize:'15px'}},xAxis:{type: 'category',}, " +
                                       "plotOptions:{series:{dataLabels:{enabled: true,format: '{y}',distance: 5,},showInLegend: true,},},legend:{itemDistance: 0,},tooltip:{hideDelay: 0, " +
                                       "backgroundColor: 'rgba(247,247,247,1)',pointFormat:'{series.name}: <b>{point.y}</b>'},";


                                        perStatusPieChartAPRJUN += "series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>',},data: [{name: 'Pass',y: " + totalPassCount + ",color: '#FF7473',drilldown: 'pass',},{name: 'Fail',y: " + totalFailcount + ",color: '#FFC952',drilldown: 'fail',},{name: 'NotTested',y: " + totalNotTestedcount + ",color:'#1FD9E1',drilldown: 'nottested',}],}],";
                                        perStatusPieChartAPRJUN += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                                           " series: [" +
                                                            " {id: 'nottested',name: 'NotTested',cursor: 'pointer',data: [{name: 'Key',y: " + NotTestedKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Not Tested- Key                   
                                                            " fpopulateddataQuarter('Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + NotTestedNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                            // " // Not Tested - Non Key                               
                                                            " fpopulateddataQuarter('Non Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},}],}," +
                                                            " {id: 'fail',name: 'Fail',cursor: 'pointer',data: [{name: 'Key',y: " + FailKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Fail- Key   
                                                            " fpopulateddataQuarter('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + FailNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                            // " // Fail - Non Key   
                                                            " fpopulateddataQuarter('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +

                                                            " },},}],}," +
                                                            " {id: 'pass',name: 'Pass',cursor: 'pointer',data: [{name: 'Key',y: " + PassKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Pass- Key  
                                                            " fpopulateddataQuarter('Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + PassNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                            // " // Pass - Non Key   
                                                            " fpopulateddataQuarter('Non Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +

                                                            " },},}],}],}";
                                    }
                                    if (quarter[i].ToString() == "Jul - Sep")
                                    {
                                        //perStatusPieChartDivJULSEP.Attributes["class"] = "progressQ2";
                                        perStatusPieChartDivJULSEP.Attributes["class"] = "col-md-5";
                                        perStatusPieChartDivJULSEP.Style["width"] = "25%";
                                        perStatusPieChartDivJULSEP.Style["height"] = "300px";

                                        perStatusPieChartJULSEP = "chart:{type: 'pie',events:{drilldown: function(e) {this.setTitle({ text: e.point.name });" +
                                        " this.subtitle.update({ text: 'Jul - Sep' });},drillup: function() {this.setTitle({ text: 'Per Status' });" +
                                        " this.subtitle.update({ text: 'Jul - Sep' });}},},title:{text: 'Per Status',style:{display:'none'},},subtitle:{" +
                                        " text: 'Jul - Sep',style:{fontWeight: '300',fontSize:'15px'}},xAxis:{type: 'category',},plotOptions:{series:{" +
                                        " dataLabels:{enabled: true,format: '{y}',distance: 5,},showInLegend: true,},},legend:{itemDistance: 0,}," +
                                        " tooltip:{hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',pointFormat:'{series.name}: <b>{point.y}</b>'},";

                                        perStatusPieChartJULSEP += "series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>',},data: [{name: 'Pass',y: " + totalPassCount + ",color: '#FF7473',drilldown: 'pass',},{name: 'Fail',y: " + totalFailcount + ",color: '#FFC952',drilldown: 'fail',},{name: 'NotTested',y: " + totalNotTestedcount + ",color:'#1FD9E1',drilldown: 'nottested',}],}],";
                                        perStatusPieChartJULSEP += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                                           " series: [" +

                                                            " {id: 'nottested',name: 'NotTested',cursor: 'pointer',data: [{name: 'Key',y: " + NotTestedKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                             // " // Not Tested- Key                   
                                                             " fpopulateddataQuarter('Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + NotTestedNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Not Tested - Non Key                               
                                                             " fpopulateddataQuarter('Non Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},}],}," +
                                                            " {id: 'fail',name: 'Fail',cursor: 'pointer',data: [{name: 'Key',y: " + FailKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                             // " // Fail- Key   
                                                             " fpopulateddataQuarter('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + FailNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Fail - Non Key   
                                                             " fpopulateddataQuarter('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +

                                                            " },},}],}," +
                                                            " {id: 'pass',name: 'Pass',cursor: 'pointer',data: [{name: 'Key',y: " + PassKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Pass- Key  
                                                            " fpopulateddataQuarter('Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + PassNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Pass - Non Key   
                                                             " fpopulateddataQuarter('Non Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +

                                                            " },},}],}],}";
                                    }
                                    if (quarter[i].ToString() == "Oct - Dec")
                                    {
                                        //perStatusPieChartDivOCTDEC.Attributes["class"] = "progressQ3";
                                        perStatusPieChartDivOCTDEC.Attributes["class"] = "col-md-5";
                                        perStatusPieChartDivOCTDEC.Style["width"] = "25%";
                                        perStatusPieChartDivOCTDEC.Style["height"] = "300px";

                                        perStatusPieChartOCTDEC = "chart:{ type: 'pie',events:{drilldown: function(e) {this.setTitle({ text: e.point.name });this.subtitle.update({ text: 'Oct - Dec' });" +
                                        " },drillup: function() {this.setTitle({ text: 'Per Status' });this.subtitle.update({ text: 'Oct - Dec' });}},}, " +
                                        " title:{text: 'Per Status',style:{display:'none'},},subtitle:{text: 'Oct - Dec',style:{fontWeight: '300',fontSize:'15px'}}," +
                                        " xAxis:{type: 'category',},plotOptions:{series:{dataLabels:{enabled: true,format: '{y}',distance: 5,},showInLegend: true," +
                                        " },},legend:{itemDistance: 0,},tooltip:{hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',pointFormat:'{series.name}: <b>{point.y}</b>'},";

                                        perStatusPieChartOCTDEC += "series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>',},data: [{name: 'Pass',y: " + totalPassCount + ",color: '#FF7473',drilldown: 'pass',},{name: 'Fail',y: " + totalFailcount + ",color: '#FFC952',drilldown: 'fail',},{name: 'NotTested',y: " + totalNotTestedcount + ",color:'#1FD9E1',drilldown: 'nottested',}],}],";
                                        perStatusPieChartOCTDEC += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                                           " series: [" +

                                                            " {id: 'nottested',name: 'NotTested',cursor: 'pointer',data: [{name: 'Key',y: " + NotTestedKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                             // " // Not Tested- Key                   
                                                             " fpopulateddataQuarter('Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + NotTestedNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Not Tested - Non Key                               
                                                             " fpopulateddataQuarter('Non Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},}],}," +
                                                            " {id: 'fail',name: 'Fail',cursor: 'pointer',data: [{name: 'Key',y: " + FailKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                             // " // Fail- Key   
                                                             " fpopulateddataQuarter('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + FailNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Fail - Non Key   
                                                             " fpopulateddataQuarter('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +

                                                            " },},}],}," +
                                                            " {id: 'pass',name: 'Pass',cursor: 'pointer',data: [{name: 'Key',y: " + PassKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Pass- Key  
                                                            " fpopulateddataQuarter('Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + PassNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Pass - Non Key   
                                                             " fpopulateddataQuarter('Non Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +

                                                            " },},}],}],}";
                                    }// END OCT - DEC
                                    if (quarter[i].ToString() == "Jan - Mar")
                                    {
                                        //perStatusPieChartDivJANMAR.Attributes["class"] = "progressQ4";
                                        perStatusPieChartDivJANMAR.Attributes["class"] = "col-md-5";
                                        perStatusPieChartDivJANMAR.Style["width"] = "25%";
                                        perStatusPieChartDivJANMAR.Style["height"] = "300px";

                                        perStatusPieChartJANMAR = "chart:{type: 'pie',events:{drilldown: function(e) {this.setTitle({ text: e.point.name });this.subtitle.update({ text: 'Jan - Mar' }); " +
                                        " },drillup: function() {this.setTitle({ text: 'Per Status' });this.subtitle.update({ text: 'Jan - Mar' });}},}, " +
                                        " title:{text: 'Per Status',style:{display:'none'},},subtitle:{text: 'Jan - Mar',style:{fontWeight: '300',fontSize:'15px'}}, " +
                                        " xAxis:{type: 'category',},plotOptions:{series:{dataLabels:{enabled: true,format: '{y}',distance: 5,},showInLegend: true, " +
                                        " },},legend:{itemDistance: 0,},tooltip:{hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',pointFormat:'{series.name}: <b>{point.y}</b>'},";

                                        perStatusPieChartJANMAR += "series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>',},data: [{name: 'Pass',y: " + totalPassCount + ",color: '#FF7473',drilldown: 'pass',},{name: 'Fail',y: " + totalFailcount + ",color: '#FFC952',drilldown: 'fail',},{name: 'NotTested',y: " + totalNotTestedcount + ",color:'#1FD9E1',drilldown: 'nottested',}],}],";
                                        perStatusPieChartJANMAR += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                                           " series: [" +

                                                            " {id: 'nottested',name: 'NotTested',cursor: 'pointer',data: [{name: 'Key',y: " + NotTestedKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                             // " // Not Tested- Key                   
                                                             " fpopulateddataQuarter('Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + NotTestedNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Not Tested - Non Key                               
                                                             " fpopulateddataQuarter('Non Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},}],}," +
                                                            " {id: 'fail',name: 'Fail',cursor: 'pointer',data: [{name: 'Key',y: " + FailKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                             // " // Fail- Key   
                                                             " fpopulateddataQuarter('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + FailNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Fail - Non Key   
                                                             " fpopulateddataQuarter('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +

                                                            " },},}],}," +
                                                            " {id: 'pass',name: 'Pass',cursor: 'pointer',data: [{name: 'Key',y: " + PassKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                            // " // Pass- Key  
                                                            " fpopulateddataQuarter('Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +
                                                            " },},},{name: 'Non Key',y: " + PassNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                             // " // Pass - Non Key   
                                                             " fpopulateddataQuarter('Non Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG')" +

                                                            " },},}],}],}";
                                    }// END JAN - MAR
                                }
                            }//Query Count End                            
                        }//Using End
                        #endregion
                    }
                    upDivFilters.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void GetQuarterlyFailedControlCount(List<AuditInstanceTransactionView> MasterRecords, long CustomerID, List<long> BranchList, String FinYear, int ProcessID, int SubProcessID)
        {
            try
            {
                int CustomerBranchID = -1;
                List<string> GraphName = new List<string>();
                if (ddlSchedulingType.SelectedValue != "-1")
                {
                    if (ddlSchedulingType.SelectedItem.Text == "Annually")
                    {
                        #region Annually - Falied Controls
                        List<string> quarter = new List<string>();
                        GraphName.Clear();
                        GraphName.Add("Annually");

                        quarter.Clear();
                        quarter.Add("Apr - Jun");
                        quarter.Add("Jul - Sep");
                        quarter.Add("Oct - Dec");
                        quarter.Add("Jan - Mar");

                        QuarterlyFailedControlSeries = String.Empty;
                        string ListofQuarters = String.Empty;
                        string KeyCountList = String.Empty;
                        string NonKeyCountList = String.Empty;
                        long totalFailcount = 0;
                        long FailKeyCount = 0;
                        long FailNonKeyCount = 0;

                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            if (GraphName.Count > 0)
                            {
                                QuarterlyFailedControlSeries = "series: [";

                                KeyCountList = "{ name: 'Key', color: '#FF7473', data: [";
                                NonKeyCountList = "{ name: 'Non-Key', color: '#1FD9E1', data: [";

                                for (int i = 0; i < GraphName.Count; i++)
                                {
                                    string qname = "";
                                    qname = GraphName[i].ToString();

                                    ListofQuarters += "'" + qname + "',";

                                    var Records = (from row in MasterRecords
                                                   where row.RoleID == 3 && quarter.Contains(row.ForMonth)
                                                   select row).ToList();

                                    if (BranchList.Count > 0)
                                        Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                                    if (FinYear != "")
                                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                                    if (ProcessID != -1)
                                        Records = Records.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                                    if (SubProcessID != -1)
                                        Records = Records.Where(Entry => Entry.SubProcessId == SubProcessID).ToList();

                                    Records = Records.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                                    var Failcount = Records.Where(a => a.AuditStatusID == 3 && ((a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2))).ToList();
                                    //var Failcount = Records.Where(a => (a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2)).ToList();
                                    totalFailcount = Failcount.Count;
                                    FailKeyCount = Failcount.Where(a => a.KeyId == 1).ToList().Count;
                                    FailNonKeyCount = Failcount.Where(a => a.KeyId == 2).ToList().Count;

                                    KeyCountList += "{ y: " + FailKeyCount + ", events: { click: function(e) { fpopulateddataQuarter('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','A','MG') } } },";
                                    NonKeyCountList += "{ y: " + FailNonKeyCount + ", events: { click: function(e) { fpopulateddataQuarter('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','A','MG') } } },";
                                }

                                KeyCountList = KeyCountList.Trim(',') + "]},";
                                NonKeyCountList = NonKeyCountList.Trim(',') + "]";

                                QuarterlyFailedControlSeries = QuarterlyFailedControlSeries + KeyCountList + NonKeyCountList + " }]";

                                ListofQuarters = ListofQuarters.Trim(',');

                                QuarterlyFailedControlSeries = QuarterlyFailedControlSeries.Trim(',');

                                QuarterlyFailedControlStatusChart = "xAxis: { categories: [" + ListofQuarters + "] }, yAxis: { min: 0,	title: { text: 'No(s) of Failed Controls(s)' }," +
                                "stackLabels: {	enabled: true, style: {	fontWeight: 'bold',	color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' }	} }," +
                                "legend: { enabled: true }," +
                                //"legend: { align: 'center',	x: 0, verticalAlign: 'bottom',	y: 40,	floating: true,	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',	borderColor: '#CCC', borderWidth: 1, shadow: false }," +
                                "tooltip: {	headerFormat: '<b>{point.x}</b><br/>', pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}' }," +
                                "plotOptions: {	column: { stacking: 'normal', dataLabels: {	enabled: true, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'	} }	}," +
                                 QuarterlyFailedControlSeries;
                            }
                        }
                        #endregion
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                    {
                        #region Half Yearly - Falied Controls
                        List<string> HalfYear1 = new List<string>();
                        List<string> HalfYear2 = new List<string>();
                        GraphName.Clear();
                        GraphName.Add("Apr - Sep");
                        GraphName.Add("Oct - Mar");

                        HalfYear1.Clear();
                        HalfYear1.Add("Apr - Jun");
                        HalfYear1.Add("Jul - Sep");

                        HalfYear2.Clear();
                        HalfYear2.Add("Oct - Dec");
                        HalfYear2.Add("Jan - Mar");

                        QuarterlyFailedControlSeries = String.Empty;
                        string ListofQuarters = String.Empty;
                        string KeyCountList = String.Empty;
                        string NonKeyCountList = String.Empty;
                        long totalFailcount = 0;
                        long FailKeyCount = 0;
                        long FailNonKeyCount = 0;

                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            if (GraphName.Count > 0)
                            {
                                QuarterlyFailedControlSeries = "series: [";

                                KeyCountList = "{ name: 'Key', color: '#FF7473', data: [";
                                NonKeyCountList = "{ name: 'Non-Key', color: '#1FD9E1', data: [";

                                for (int i = 0; i < GraphName.Count; i++)
                                {
                                    string qname = "";
                                    string ISAHYQ = "";
                                    qname = GraphName[i].ToString();

                                    ListofQuarters += "'" + qname + "',";
                                    var Records = (from row in MasterRecords
                                                   where row.RoleID == 3
                                                   select row).ToList();
                                    ISAHYQ = "";
                                    if (qname == "Apr - Sep")
                                    {
                                        ISAHYQ = "HF1";
                                        Records = Records.Where(Entry => HalfYear1.Contains(Entry.ForMonth)).ToList();
                                    }
                                    if (qname == "Oct - Mar")
                                    {
                                        ISAHYQ = "HF2";
                                        Records = Records.Where(Entry => HalfYear2.Contains(Entry.ForMonth)).ToList();
                                    }
                                    if (BranchList.Count > 0)
                                        Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                                    if (FinYear != "")
                                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                                    if (ProcessID != -1)
                                        Records = Records.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                                    if (SubProcessID != -1)
                                        Records = Records.Where(Entry => Entry.SubProcessId == SubProcessID).ToList();

                                    Records = Records.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                                    //var Failcount = Records.Where(a => (a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2)).ToList();
                                    var Failcount = Records.Where(a => a.AuditStatusID == 3 && ((a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2))).ToList();
                                    totalFailcount = Failcount.Count;
                                    FailKeyCount = Failcount.Where(a => a.KeyId == 1).ToList().Count;
                                    FailNonKeyCount = Failcount.Where(a => a.KeyId == 2).ToList().Count;

                                    KeyCountList += "{ y: " + FailKeyCount + ", events: { click: function(e) { fpopulateddataQuarter('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','" + ISAHYQ + "','MG') } } },";
                                    NonKeyCountList += "{ y: " + FailNonKeyCount + ", events: { click: function(e) { fpopulateddataQuarter('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','" + ISAHYQ + "','MG') } } },";

                                }

                                KeyCountList = KeyCountList.Trim(',') + "]},";
                                NonKeyCountList = NonKeyCountList.Trim(',') + "]";

                                QuarterlyFailedControlSeries = QuarterlyFailedControlSeries + KeyCountList + NonKeyCountList + " }]";

                                ListofQuarters = ListofQuarters.Trim(',');

                                QuarterlyFailedControlSeries = QuarterlyFailedControlSeries.Trim(',');

                                QuarterlyFailedControlStatusChart = "xAxis: { categories: [" + ListofQuarters + "] }, yAxis: { min: 0,	title: { text: 'No(s) of Failed Controls(s)' }," +
                                "stackLabels: {	enabled: true, style: {	fontWeight: 'bold',	color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' }	} }," +
                                "legend: { enabled: true }," +
                                //"legend: { align: 'center',	x: 0, verticalAlign: 'bottom',	y: 40,	floating: true,	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',	borderColor: '#CCC', borderWidth: 1, shadow: false }," +
                                "tooltip: {	headerFormat: '<b>{point.x}</b><br/>', pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}' }," +
                                "plotOptions: {	column: { stacking: 'normal', dataLabels: {	enabled: true, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'	} }	}," +
                                 QuarterlyFailedControlSeries;
                            }
                        }
                        #endregion
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                    {
                        #region Quarterly - Falied Controls
                        List<string> quarter = new List<string>();
                        quarter.Clear();
                        quarter.Add("Apr - Jun");
                        quarter.Add("Jul - Sep");
                        quarter.Add("Oct - Dec");
                        quarter.Add("Jan - Mar");

                        QuarterlyFailedControlSeries = String.Empty;
                        string ListofQuarters = String.Empty;
                        string KeyCountList = String.Empty;
                        string NonKeyCountList = String.Empty;
                        long totalFailcount = 0;
                        long FailKeyCount = 0;
                        long FailNonKeyCount = 0;

                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            if (quarter.Count > 0)
                            {
                                QuarterlyFailedControlSeries = "series: [";

                                KeyCountList = "{ name: 'Key', color: '#FF7473', data: [";
                                NonKeyCountList = "{ name: 'Non-Key', color: '#1FD9E1', data: [";

                                for (int i = 0; i < quarter.Count; i++)
                                {
                                    string qname = "";
                                    qname = quarter[i].ToString();

                                    ListofQuarters += "'" + qname + "',";
                                    var Records = (from row in MasterRecords
                                                   where row.RoleID == 3
                                                   && row.ForMonth == qname
                                                   select row).ToList();

                                    if (BranchList.Count > 0)
                                        Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                                    if (FinYear != "")
                                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                                    if (ProcessID != -1)
                                        Records = Records.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                                    if (SubProcessID != -1)
                                        Records = Records.Where(Entry => Entry.SubProcessId == SubProcessID).ToList();

                                    Records = Records.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                                    //var Failcount = Records.Where(a => (a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2)).ToList();
                                    var Failcount = Records.Where(a => a.AuditStatusID == 3 && ((a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2))).ToList();
                                    totalFailcount = Failcount.Count;
                                    FailKeyCount = Failcount.Where(a => a.KeyId == 1).ToList().Count;
                                    FailNonKeyCount = Failcount.Where(a => a.KeyId == 2).ToList().Count;

                                    KeyCountList += "{ y: " + FailKeyCount + ", events: { click: function(e) { fpopulateddataQuarter('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG') } } },";
                                    NonKeyCountList += "{ y: " + FailNonKeyCount + ", events: { click: function(e) { fpopulateddataQuarter('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + qname + "','Q','MG') } } },";

                                }

                                KeyCountList = KeyCountList.Trim(',') + "]},";
                                NonKeyCountList = NonKeyCountList.Trim(',') + "]";

                                QuarterlyFailedControlSeries = QuarterlyFailedControlSeries + KeyCountList + NonKeyCountList + " }]";

                                ListofQuarters = ListofQuarters.Trim(',');

                                QuarterlyFailedControlSeries = QuarterlyFailedControlSeries.Trim(',');

                                QuarterlyFailedControlStatusChart = "xAxis: { categories: [" + ListofQuarters + "] }, yAxis: { min: 0,	title: { text: 'No(s) of Failed Controls(s)' }," +
                                "stackLabels: {	enabled: true, style: {	fontWeight: 'bold',	color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' }	} }," +
                                "legend: { enabled: true }," +
                                //"legend: { align: 'center',	x: 0, verticalAlign: 'bottom',	y: 40,	floating: true,	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',	borderColor: '#CCC', borderWidth: 1, shadow: false }," +
                                "tooltip: {	headerFormat: '<b>{point.x}</b><br/>', pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}' }," +
                                "plotOptions: {	column: { stacking: 'normal', dataLabels: {	enabled: true, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'	} }	}," +
                                 QuarterlyFailedControlSeries;
                            }
                        }
                        #endregion
                    }
                    upDivFilters.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void GetProcessWiseObservationDashboardCount(List<AuditInstanceTransactionView> MasterRecords, long CustomerID, List<long> BranchList, String FinYear, String Period, int ProcessID)
        {
            try
            {
                ProcessWiseObservationStatusChart = String.Empty;
                string ListofProcesses = String.Empty;
                string HighCountList = String.Empty;
                string MediumCountList = String.Empty;
                string LowCountList = String.Empty;
                string newDivName = String.Empty;
                long UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<Mst_Process> ProcessList = new List<Mst_Process>();

                    if (BranchList.Count > 0)
                        //  ProcessList = ProcessManagement.GetProcess("P", CustomerID, BranchList);
                        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, BranchList, UserID);
                    else
                        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, UserID);

                    if (ProcessID != -1)
                        ProcessList = ProcessList.Where(Entry => Entry.Id == ProcessID).ToList();

                    if (ProcessList.Count > 0)
                    {

                        int CustomerBranchID = -1;
                        long totalcount = 0;
                        long totalPassCount = 0;
                        long PassKeyCount = 0;
                        long PassNonKeyCount = 0;

                        long totalFailcount = 0;
                        long FailKeyCount = 0;
                        long FailNonKeyCount = 0;

                        long totalNotTestedcount = 0;
                        long NotTestedKeyCount = 0;
                        long NotTestedNonKeyCount = 0;

                        int Count = 1;
                        ProcessList.ForEach(EachProcess =>
                        {
                            var Records = (from row in MasterRecords
                                           where row.ProcessId == EachProcess.Id && row.RoleID == 3
                                           select row).ToList();
                            
                            if (BranchList.Count > 0)
                                Records = Records.Where(Entry => BranchList.Contains((long) Entry.CustomerBranchID)).ToList();

                            if (FinYear != "")
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                            if (Period != "")
                                Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                            Records = Records.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                            var NotTestedcount = Records.Where(a => a.TOD == null && a.TOE == null).ToList();
                            //var NotTestedcount = Records.Where(a => ((a.TOD == 2 && a.TOE == -1 && a.AuditStatusID == 3) || (a.TOD == null && a.TOE == null))).ToList();
                            totalNotTestedcount = NotTestedcount.Count;
                            NotTestedKeyCount = NotTestedcount.Where(a => a.KeyId == 1).ToList().Count;
                            NotTestedNonKeyCount = NotTestedcount.Where(a => a.KeyId == 2).ToList().Count;

                            var PassCount = Records.Where(a => (a.TOD == 1 && a.TOE == 1) || (a.TOD == 1 && a.TOE == 3) || (a.TOD == 3 && a.TOE == 1) || (a.TOD == 3 && a.TOE == 3)).ToList();
                            totalPassCount = PassCount.Count;
                            PassKeyCount = PassCount.Where(a => a.KeyId == 1).ToList().Count;
                            PassNonKeyCount = PassCount.Where(a => a.KeyId == 2).ToList().Count;

                            //var Failcount = Records.Where(a => (a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2)).ToList();
                            var Failcount = Records.Where(a => a.AuditStatusID == 3 && ((a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2))).ToList();
                            totalFailcount = Failcount.Count;
                            FailKeyCount = Failcount.Where(a => a.KeyId == 1).ToList().Count;
                            FailNonKeyCount = Failcount.Where(a => a.KeyId == 2).ToList().Count;

                            totalcount = totalPassCount + totalFailcount + totalNotTestedcount;

                            if (totalcount != 0)
                            {
                                ListofProcesses += "'" + EachProcess.Name + "',";

                                System.Web.UI.HtmlControls.HtmlGenericControl newDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                                newDivName = "DivProcessObs" + Count;
                                newDiv.ID = newDivName;

                                String newDivClientID = newDiv.ClientID;

                                newDiv.Style["height"] = "250px";
                                newDiv.Style["width"] = "25%";
                                newDiv.Style["float"] = "left";

                                DivGraphProcessObsStatus.Controls.Add(newDiv);
                                ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie' }," +
                                "title: {text: '" + EachProcess.Name + "',style: { fontWeight: '300',fontSize: '12px'}}, legend: { itemDistance:0, }, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                                "plotOptions: {pie: {size: '100%', showInLegend: true, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: 5," +
                                "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} } } }," +
                                "series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>'},data: [{name: 'Pass',y: " + totalPassCount + ",color: '#FF7473',drilldown: 'pass',},{name: 'Fail',y: " + totalFailcount + ",color: '#FFC952',drilldown: 'fail',},{name: 'NotTested',y: " + totalNotTestedcount + ",color:'#1FD9E1',drilldown: 'nottested',}],}],";

                                ProcesswiseObservationSeries += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                                   " series: [" +
                                                    " {id: 'nottested',name: 'NotTested',cursor: 'pointer',data: [{name: 'Key',y: " + NotTestedKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                    // " // Not Tested- Key                   
                                                    " fpopulateddataprocess('Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "' ," + EachProcess.Id + ",'-1','MG')" +
                                                    " },},},{name: 'Non Key',y: " + NotTestedNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                     // " // Not Tested - Non Key                               
                                                     " fpopulateddataprocess('Non Key','Not Tested'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "' ," + EachProcess.Id + ",'-1','MG')" +
                                                    " },},}],}," +
                                                    " {id: 'fail',name: 'Fail',cursor: 'pointer',data: [{name: 'Key',y: " + FailKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                    // " // Fail- Key   
                                                    " fpopulateddataprocess('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "' ," + EachProcess.Id + ",'-1','MG')" +
                                                    " },},},{name: 'Non Key',y: " + FailNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                    // " // Fail - Non Key   
                                                    " fpopulateddataprocess('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "' ," + EachProcess.Id + ",'-1','MG')" +
                                                    " },},}],}," +
                                                    " {id: 'pass',name: 'Pass',cursor: 'pointer',data: [{name: 'Key',y: " + PassKeyCount + ",color: '#FF7473',events:{click: function(e) { " +
                                                    // " // Pass- Key  
                                                    " fpopulateddataprocess('Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "' ," + EachProcess.Id + ",'-1','MG')" +
                                                    " },},},{name: 'Non Key',y: " + PassNonKeyCount + ",color: '#FFC952',events:{click: function(e) { " +
                                                    // " // Pass - Non Key   
                                                    " fpopulateddataprocess('Non Key','Pass'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "' ," + EachProcess.Id + ",'-1','MG')" +
                                                    " },},}],}],}});";
                                ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;
                                Count++;
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void GetProcessWiseFailedControlCount(List<AuditInstanceTransactionView> MasterRecords, long CustomerID, List<long> BranchList, String FinYear, String Period, int ProcessID)
        {
            try
            {
                int CustomerBranchID = -1;

                ProcessWiseFailedControlStatusChart = String.Empty;

                String ProcessWiseFailedControlSeries = String.Empty;
                String ListofProcesses = String.Empty;

                String KeyCountList = String.Empty;
                String NonKeyCountList = String.Empty;

                List<long> FrequencyIds = new List<long>();

                long totalFailcount = 0;
                long FailKeyCount = 0;
                long FailNonKeyCount = 0;
                long UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    ProcessWiseFailedControlSeries = "series: [";

                    KeyCountList = "{ name: 'Key', color: '#FF7473', data: [";
                    NonKeyCountList = "{ name: 'Non-Key', color: '#1FD9E1', data: [";

                    List<Mst_Process> ProcessList = new List<Mst_Process>();

                    if (BranchList.Count > 0)
                        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, BranchList, UserID);                    
                    else
                        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, UserID);

                    if (ProcessID != -1)
                        ProcessList = ProcessList.Where(Entry => Entry.Id == ProcessID).ToList();

                    if (ProcessList.Count > 0)
                    {
                        ProcessList.ForEach(EachProcess =>
                        {
                            var Records = (from row in MasterRecords
                                           where row.ProcessId == EachProcess.Id
                                           && row.RoleID == 3
                                           select row).ToList();

                            if (BranchList.Count > 0)
                                Records = Records.Where(Entry => BranchList.Contains((long) Entry.CustomerBranchID)).ToList();

                            if (FinYear != "")
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                            if (Period != "")
                                Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                            Records = Records.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                            ListofProcesses += "'" + EachProcess.Name + "',";

                            //var Failcount = Records.Where(a => (a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2)).ToList();
                            var Failcount = Records.Where(a => a.AuditStatusID == 3 && ((a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2))).ToList();
                            totalFailcount = Failcount.Count;

                            if (totalFailcount > 0)
                            {
                                FailKeyCount = Failcount.Where(a => a.KeyId == 1).ToList().Count;
                                FailNonKeyCount = Failcount.Where(a => a.KeyId == 2).ToList().Count;

                                KeyCountList += "{ y: " + FailKeyCount + ", events: { click: function(e) { fpopulateddataprocess('Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "' ," + EachProcess.Id + ",'-1','MG') } } },";
                                NonKeyCountList += "{ y: " + FailNonKeyCount + ", events: { click: function(e) { fpopulateddataprocess('Non Key','Fail'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "' ," + EachProcess.Id + ",'-1','MG') } } },";
                            }

                        });

                        KeyCountList = KeyCountList.Trim(',') + "]},";
                        NonKeyCountList = NonKeyCountList.Trim(',') + "]";

                        ProcessWiseFailedControlSeries = ProcessWiseFailedControlSeries + KeyCountList + NonKeyCountList + " }]";

                        ListofProcesses = ListofProcesses.Trim(',');

                        ProcessWiseFailedControlSeries = ProcessWiseFailedControlSeries.Trim(',');

                        ProcessWiseFailedControlStatusChart = "xAxis: { categories: [" + ListofProcesses + "] }, yAxis: { min: 0,	title: { text: 'No(s) of Failed Controls(s)' }," +
                        "stackLabels: {	enabled: true, style: {	fontWeight: 'bold',	color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' }	} }," +
                        "legend: { enabled: true }," +
                        //"legend: { align: 'center',	x: 0, verticalAlign: 'bottom',	y: 40,	floating: true,	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',	borderColor: '#CCC', borderWidth: 1, shadow: false }," +
                        "tooltip: {	headerFormat: '<b>{point.x}</b><br/>', pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}' }," +
                        "plotOptions: {	column: { stacking: 'normal', dataLabels: {	enabled: true, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'	} }	}," +
                         ProcessWiseFailedControlSeries;
                    }

                    upDivFilters.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
       
        public DateTime? GetDate(string date)
        {
            if (date != null && date != "")
            {
                string date1 = "";

                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" +date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" +date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" +date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }

                return Convert.ToDateTime(date1);
            }
            else
            {
                return null;
            }
        }

        public DateTime GetLastDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, DateTime.DaysInMonth(dateTime.Year, dateTime.Month));
        }

        protected void AddSteps(string[] items, BulletedList ListName)
        {
            ListName.Items.Clear();
            ListName.Attributes["data-progtrckr-steps"] = items.Length.ToString();

            for (int i = 0; i < items.Length; i++)
            {
                ListName.Items.Add(new ListItem(items[i]));
            }
        }

        protected void SetProgress(int current, BulletedList ListName)
        {
            for (int i = 0; i <ListName.Items.Count; i++)
            {
                ListName.Items[i].Attributes["class"] =
                    (i < current) ? "progtrckr-done" :
                    (i == current) ? "progtrckr-current" : "progtrckr-todo";
            }
        }

       
    }
}