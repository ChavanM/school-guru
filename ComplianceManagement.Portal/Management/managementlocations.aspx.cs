﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{

    public partial class managementlocations : System.Web.UI.Page
    {
        public string compliancetype = "statutory";
        protected static bool IsApprover = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["ctype"]))
                    {
                        compliancetype = Request.QueryString["ctype"].ToString();
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                    {
                        IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                    }

                    GetAllDataPerformer();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }               
            }
        }
        public void GetAllDataPerformer()
        {
            try
            {
                string DeptHead = null;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["IsDeptHead"])))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }
                int CustomerId = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                string str = "";
                if (compliancetype.ToLower() == "internal")
                {
                    if (DeptHead == "1")
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            List<string> Internalquery = new List<string>();
                            if (IsApprover == true)
                            {
                                Internalquery = (from row in entities.InternalComplianceAssignedInstancesView_DeptHead
                                                 join Cbranch in entities.CustomerBranches
                                                  on row.CustomerBranchID equals Cbranch.ID
                                                 join city in entities.Cities
                                                 on Cbranch.CityID equals city.ID
                                                 join stat in entities.States
                                                 on city.StateId equals stat.ID
                                                 where Cbranch.CustomerID == CustomerId && row.UserID == AuthenticationHelper.UserID
                                                 && row.RoleID == 6
                                                 select city.Name + "," + stat.Name).Distinct().ToList();
                            }
                            else
                            {
                                Internalquery = (from row in entities.EntitiesAssignmentInternals
                                                 join Cbranch in entities.CustomerBranches
                                                  on row.BranchID equals Cbranch.ID
                                                 join city in entities.Cities
                                                 on Cbranch.CityID equals city.ID
                                                 join stat in entities.States
                                                 on city.StateId equals stat.ID
                                                 where Cbranch.CustomerID == CustomerId
                                                  && row.UserID == AuthenticationHelper.UserID
                                                 select city.Name + "," + stat.Name).Distinct().ToList();
                            }

                            foreach (var item in Internalquery)
                            {
                                GraphDisplay display = new GraphDisplay();

                                display.Name = "Location";
                                display.LocationName = item;
                                display.Percentage = 0;

                                str += " ['" + display.Name + "', '" + display.LocationName + "',0],";

                            }
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript", "<script language='javascript'> var locations = [" + str.TrimEnd(',') + "];</script>");
                        }
                    }
                    else
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            List<string> Internalquery = new List<string>();
                            if (IsApprover == true)
                            {
                                Internalquery = (from row in entities.InternalComplianceAssignedInstancesViews
                                                 join Cbranch in entities.CustomerBranches
                                                  on row.CustomerBranchID equals Cbranch.ID
                                                 join city in entities.Cities
                                                 on Cbranch.CityID equals city.ID
                                                 join stat in entities.States
                                                 on city.StateId equals stat.ID
                                                 where Cbranch.CustomerID == CustomerId && row.UserID == AuthenticationHelper.UserID
                                                 && row.RoleID == 6
                                                 select city.Name + "," + stat.Name).Distinct().ToList();
                            }
                            else
                            {
                                Internalquery = (from row in entities.EntitiesAssignmentInternals
                                                 join Cbranch in entities.CustomerBranches
                                                  on row.BranchID equals Cbranch.ID
                                                 join city in entities.Cities
                                                 on Cbranch.CityID equals city.ID
                                                 join stat in entities.States
                                                 on city.StateId equals stat.ID
                                                 where Cbranch.CustomerID == CustomerId
                                                  && row.UserID == AuthenticationHelper.UserID
                                                 select city.Name + "," + stat.Name).Distinct().ToList();
                            }

                            foreach (var item in Internalquery)
                            {
                                GraphDisplay display = new GraphDisplay();

                                display.Name = "Location";
                                display.LocationName = item;
                                display.Percentage = 0;

                                str += " ['" + display.Name + "', '" + display.LocationName + "',0],";

                            }
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript", "<script language='javascript'> var locations = [" + str.TrimEnd(',') + "];</script>");
                        }
                    }
                }
                else
                {
                    if (DeptHead == "1")
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            List<string> query = new List<string>();
                            if (IsApprover == true)
                            {
                                query = (from row in entities.ComplianceAssignedInstancesView_DeptHead
                                         join Cbranch in entities.CustomerBranches
                                          on row.CustomerBranchID equals Cbranch.ID
                                         join city in entities.Cities
                                         on Cbranch.CityID equals city.ID
                                         join stat in entities.States
                                         on city.StateId equals stat.ID
                                         where Cbranch.CustomerID == CustomerId && row.UserID == AuthenticationHelper.UserID
                                         && row.RoleID == 6
                                         select city.Name + "," + stat.Name).Distinct().ToList();

                            }
                            else
                            {
                                query = (from row in entities.EntitiesAssignments
                                         join Cbranch in entities.CustomerBranches
                                         on row.BranchID equals Cbranch.ID
                                         join city in entities.Cities
                                         on Cbranch.CityID equals city.ID
                                         join stat in entities.States
                                         on city.StateId equals stat.ID
                                         where Cbranch.CustomerID == CustomerId
                                         && row.UserID == AuthenticationHelper.UserID
                                         select city.Name + "," + stat.Name).Distinct().ToList();
                            }
                            foreach (var item in query)
                            {
                                GraphDisplay display = new GraphDisplay();

                                display.Name = "Location";
                                display.LocationName = item;
                                display.Percentage = 0;
                                str += " ['" + display.Name + "', '" + display.LocationName + "',0],";
                            }
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript", "<script language='javascript'> var locations = [" + str.TrimEnd(',') + "];</script>");
                        }
                    }
                    else
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            List<string> query = new List<string>();
                            if (IsApprover == true)
                            {
                                query = (from row in entities.ComplianceAssignedInstancesViews
                                         join Cbranch in entities.CustomerBranches
                                          on row.CustomerBranchID equals Cbranch.ID
                                         join city in entities.Cities
                                         on Cbranch.CityID equals city.ID
                                         join stat in entities.States
                                         on city.StateId equals stat.ID
                                         where Cbranch.CustomerID == CustomerId && row.UserID == AuthenticationHelper.UserID
                                         && row.RoleID == 6
                                         select city.Name + "," + stat.Name).Distinct().ToList();

                            }
                            else
                            {
                                query = (from row in entities.EntitiesAssignments
                                         join Cbranch in entities.CustomerBranches
                                         on row.BranchID equals Cbranch.ID
                                         join city in entities.Cities
                                         on Cbranch.CityID equals city.ID
                                         join stat in entities.States
                                         on city.StateId equals stat.ID
                                         where Cbranch.CustomerID == CustomerId
                                         && row.UserID == AuthenticationHelper.UserID
                                         select city.Name + "," + stat.Name).Distinct().ToList();
                            }
                            foreach (var item in query)
                            {
                                GraphDisplay display = new GraphDisplay();

                                display.Name = "Location";
                                display.LocationName = item;
                                display.Percentage = 0;
                                str += " ['" + display.Name + "', '" + display.LocationName + "',0],";
                            }
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript", "<script language='javascript'> var locations = [" + str.TrimEnd(',') + "];</script>");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<Sp_GetPerformerAndReviewer_Result> GetPerformerAndReviewerProcedure(int Customerid, int userid, int roleid)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceReminders = entities.Sp_GetPerformerAndReviewer(Customerid, userid, roleid).ToList();
                return complianceReminders;
            }
        }
    }
}