﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FunctionDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.FunctionDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .h1, h1 {
        font-size: 25px;
        margin-inline-end: -726px;
       }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        $(document).ready(function () {
            window.parent.forchild($("body").height() + 15);
        });
    </script>


</head>
<body>
    <form id="form1" runat="server">

      <div class="col-lg-5 col-md-5 colpadding0">
            <h1 id="pagetype" style="height: 30px;background-color: #f8f8f8;margin-top: 0px;color: #666;font-weight: bold;font-size: 19px;padding-left:5px;padding-top:5px;margin-bottom: 12px;">Categories</h1>
        </div> 

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="clearfix"></div>
       

            <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                <ContentTemplate>

                    <div style="float: left; margin-right: 5px; margin-top: -30px;">
                    <div style="float: left; margin-right: 5px;">
                        <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 300px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93; margin-top: 40px"
                            CssClass="txtbox" />
                        <div style="margin-left: 1px; position: absolute; z-index: 10; margin-top: -20px;" id="divFilterLocation">
                            <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="300px" NodeStyle-ForeColor="#8e8e93"
                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                    </div>

                    <div style="float: left; margin-right: 5px;">
                        <asp:DropDownList runat="server" ID="ddlCategory" class="form-control m-bot15 select_location" Style="width: 150px; margin-top: 40px">
                        </asp:DropDownList>
                    </div>

                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" runat="server" Text="Apply" OnClick="btnSearch_Click" Style="margin-top: 40px" />                    
                   <asp:Button ID="LinkButton1" CausesValidation="false" class="btn btn-search"  runat="server" Text="Clear" OnClick="lnkClearAdvanceSearch_Click" Style="margin-left: 10px;margin-top: 40px;"/> 
                        </div>
                    <div class="clearfix"></div>

                    <div id="divAdvSearch" class="col-md-12 AdvanceSearchScrum" runat="server" visible="false">
                        <p>
                            <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                        </p>
                        <p style="margin-left: 354px;margin-top: -49px;">
                            <%--<asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Search</asp:LinkButton>--%>
                           
                        </p>
                    </div>

                    <div style="margin-bottom: 10px; font-weight: bold; font-size: 12px;">
                        <asp:Label runat="server" ID="lblDetailViewTitle"></asp:Label>
                    </div>

                    <asp:GridView runat="server" ID="grdSummaryDetails" AutoGenerateColumns="false" GridLines="none" AllowSorting="true"
                        CellPadding="4" CssClass="table" AllowPaging="true" style='border: 2px solid #dddddd;'
                        PageSize="12" Width="100%" DataKeyNames="Id" OnRowDataBound="grdSummaryDetails_RowDataBound">                     
                        <Columns>
                            <asp:TemplateField HeaderText="Sr.No.">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="Name" HeaderText="Name" />

                            <asp:TemplateField HeaderText="Users" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkUsers" runat="server" OnClientClick="window.parent.fusersRahul()" Style="color: #666"
                                         Text='<%# GetCount((int)Eval("Id"),"U") %>' OnClick="lnkUsers_Click"></asp:LinkButton>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Compliances" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkCompliances" runat="server" OnClientClick="window.parent.fCompliancesRahul()"
                                        Style="color: #666" 
                                        Text='<%# GetCount((int)Eval("Id"),"C") %>' OnClick="lnkCompliances_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Locations" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkLocations" runat="server" Style="color: #666" 
                                        Text='<%# GetCount((int)Eval("Id"),"L") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerTemplate>
                            <table style="display: none">
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        
    </form>
</body>
</html>
