﻿<%@ Page Title="Search Result" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="SearchResult.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.SearchResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('Search Result');
            if (ReadQuerySt('SearchText') != '') {
                $('#txtSearch').val(ReadQuerySt('SearchText'));
            }
        });

        function openComplianceDetailsModal() {
            $('#ComplianceDetailsModal').modal('show');
            return true;
        }

        function fCompliances(obj) {

            $('#divreports').modal('show');
            $('#showdetails').attr('width', '1150px');
            $('#showdetails').attr('height', '580px');
            $('.modal-dialog').css('width', '1200px');
            $('#showdetails').attr('src', "../Management/ComplianceDetails.aspx?customerid=" + $(obj).attr('data-cid') + "&Internalsatutory=" + $(obj).attr('data-iss') + "&ActID=" + $(obj).attr('data-actid'));
        }

        function fUsers(obj) {

            $('#divreports').modal('show');
            $('#showdetails').attr('width', '1150px');
            $('#showdetails').attr('height', '580px');
            $('.modal-dialog').css('width', '1200px');
            $('#showdetails').attr('src', "../Management/ComplianceDetails.aspx?customerid=" + $(obj).attr('data-cid') + "&Internalsatutory=" + $(obj).attr('data-iss') + "&UserID=" + $(obj).attr('data-userid'));
        }

        function fCompliances2(customerid, IsSatutoryInternal, ActID) {

            $('#showdetails').attr('src', 'blank.html');
            $('#divreports').modal('show');
            $('#showdetails').attr('width', '1150px');
            $('#showdetails').attr('height', '580px');
            $('.modal-dialog').css('width', '1200px');
            $('#showdetails').attr('src', "../Management/ComplianceDetails.aspx?customerid=" + customerid + "&Internalsatutory=" + IsSatutoryInternal + "&ActID" + ActID);
        }
    </script>

    <style>
        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }

        .btnss {
            background-image: url(../Images/View-icon-new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional">
        <%--OnLoad="upComplianceDetails_Load"--%>
        <ContentTemplate>

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                               <header class="panel-heading tab-bg-primary ">
                                      <ul id="ulTab" class="nav nav-tabs">                                          
                                        <li class="" id="liUser" runat="server">
                                            <asp:LinkButton ID="lnkUser" runat="server" OnClick="lnkUser_click">User</asp:LinkButton>                                           
                                        </li>
                                           
                                        <li class=""  id="liAct" runat="server">
                                            <asp:LinkButton ID="lnkAct" runat="server" OnClick="lnkAct_click">Act</asp:LinkButton>                                        
                                        </li>

                                          <li class="" id="liComp" runat="server">
                                            <asp:LinkButton ID="lnkCompliance" runat="server" OnClick="lnkCompliance_click">Compliance</asp:LinkButton>                                           
                                        </li> 
                                          
                                            <li class="" id="liInformative" runat="server">
                                            <asp:LinkButton ID="lnkInformativeCompliance" runat="server" OnClick="lnkInformativeCompliance_Click">Informative Compliance</asp:LinkButton>                                           
                                        </li>                                           
                                    </ul>
                                </header>

                                <div class="clearfix"></div>

                             <div class="panel-body">

                <div class="col-md-12 AdvanceSearchScrum">
                <div class="col-md-2 colpadding0 entrycount">
                    <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">                        
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>

                   <%-- <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px; margin-left: 5px;">Entries</p>
                    </div>--%>
                     </div>   
                    <div runat="server" id="DivRecordsScrum" style="float: right; margin-top: 5px;">
                       <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>  
                    
                     <div class="col-md-6 colpadding0" style="width: 200px; margin-left: 20px;">
                          <asp:TextBox runat="server" placeholder="Type to Filter" class="form-control" Width="250px" ID="tbxFilter" MaxLength="50" AutoPostBack="true" Visible="false" /> <%--OnTextChanged="tbxFilter_TextChanged"--%>
                     </div> 
               </div>


                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="GridUser" AutoGenerateColumns="false" CssClass="table" GridLines="None" BorderWidth="0px"
                                    CellPadding="4" Width="100%" DataKeyNames="ID" AllowPaging="True" PageSize="10" AutoPostBack="true">
                                    <Columns>                                  

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>  
                                        
                                         <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("FirstName").ToString()+" "+Eval("LastName").ToString() %>' ToolTip='<%# Eval("FirstName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Role">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRole" runat="server" Text='<%# GetRole((long)Eval("ID")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Location">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("CustomerBranchID")!=null?GetCustomerBranch((int)Eval("CustomerBranchID")):"" %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Contact No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContactNumber" runat="server" Text='<%# Eval("ContactNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>  
                                        
                                         <asp:TemplateField HeaderText="View">     
                                            <ItemTemplate>                                                            
                                                <asp:ImageButton runat="server" ID="btnViewUserCompliance" ImageUrl="~/Images/View-icon-new.png" CssClass="btnss" OnClientClick="fUsers(this);"  data-userid='<%# Eval("ID") %>' data-cid="<%#customerID %>" data-iss="<%#IsSatutoryInternal%>" ToolTip="View User Details" />                                                          
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                         <RowStyle CssClass="clsROWgrid"   />
                                         <HeaderStyle CssClass="clsheadergrid"    />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Search Result Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="GridAct" AutoGenerateColumns="false" CssClass="table" GridLines="none" BorderWidth="0px"
                                    CellPadding="4" Width="100%" DataKeyNames="ActID" AllowPaging="True" PageSize="10" AutoPostBack="true">
                                    <Columns>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        
                                           <asp:TemplateField HeaderText="Act">
                                    <ItemTemplate>
                                        <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>  
                                        
                                         <asp:TemplateField HeaderText="Category">
                                    <ItemTemplate>
                                        <asp:Label ID="lblComplianceCategory" runat="server" Text='<%# Eval("ComplianceCategoryId").ToString()!=""?com.VirtuosoITech.ComplianceManagement.Business.ComplianceCategoryManagement.GetByID((int)Eval("ComplianceCategoryId")).Name:"" %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                        
                                         <asp:TemplateField HeaderText="View">     
                                            <ItemTemplate>                                                            
                                                <asp:ImageButton runat="server" ID="btnViewCompliance1" ImageUrl="~/Images/View-icon-new.png" CssClass="btnss" OnClientClick="fCompliances(this);"  data-actid='<%# Eval("ActID") %>' data-cid="<%#customerID %>" data-iss="<%#IsSatutoryInternal%>" ToolTip="View Compliances" />                                                          
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                         <RowStyle CssClass="clsROWgrid"   />
                                         <HeaderStyle CssClass="clsheadergrid"    />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                      <EmptyDataTemplate>
                                        No Search Result Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                                  <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="GridCompliance" AutoGenerateColumns="false" CssClass="table" GridLines="none" BorderWidth="0px"
                                    CellPadding="4" Width="100%" DataKeyNames="ComplianceID" AllowPaging="True" PageSize="10" AutoPostBack="true">
                                    <Columns>                                      

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        
                                         <asp:TemplateField HeaderText="Act">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAct" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                                     

                                        <asp:TemplateField HeaderText="Compliance">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCategory" runat="server" Text='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server" Text='<%# Eval("ComplianceTypeName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                        
                                       <asp:TemplateField HeaderText="Category">
                                    <ItemTemplate>
                                        <asp:Label ID="lblComplianceCategory" runat="server" Text='<%# Eval("ComplianceCategoryId").ToString()!=""?com.VirtuosoITech.ComplianceManagement.Business.ComplianceCategoryManagement.GetByID((int)Eval("ComplianceCategoryId")).Name:"" %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Assigned to Location(s)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLoc" runat="server" Text='<%# GetComplianceLocations((long)Eval("ComplianceID")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>   
                                        
                                        <asp:TemplateField HeaderText="View">     
                                            <ItemTemplate>                                                            
                                                <asp:Button runat="Server"  OnClientClick="return openComplianceDetailsModal()" ID="btnChangeStatus" OnClick="btnChangeStatus_Click" ImageUrl="~/Images/View-icon-new.png"   CssClass="btnss" 
                                                CommandName="CHANGE_STATUS"  CommandArgument='<%# Eval("ComplianceID") %>' ToolTip="View Compliance Details" />                                                          
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                         <RowStyle CssClass="clsROWgrid"   />
                                         <HeaderStyle CssClass="clsheadergrid"    />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                      <EmptyDataTemplate>
                                        No Search Result Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                                  <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdInformativeCompliance" AutoGenerateColumns="false" CssClass="table" GridLines="none" BorderWidth="0px"
                                    CellPadding="4" Width="100%" DataKeyNames="ComplianceID" AllowPaging="True" PageSize="10" AutoPostBack="true">
                                    <Columns>                                      

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        
                                         <asp:TemplateField HeaderText="Act">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAct" runat="server" Text='<%# Eval("ActName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                                     

                                        <asp:TemplateField HeaderText="Compliance">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCategory" runat="server" Text='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server" Text='<%# Eval("ComplianceTypeName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                           <asp:TemplateField HeaderText="Category">
                                    <ItemTemplate>
                                        <asp:Label ID="lblComplianceCategory" runat="server" Text='<%# Eval("ComplianceCategory") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="View">     
                                            <ItemTemplate>                                                            
                                                <asp:Button runat="Server"  OnClientClick="return openComplianceDetailsModal()" ID="btnChangeStatus" OnClick="btnChangeStatus_Click" ImageUrl="~/Images/View-icon-new.png"   CssClass="btnss" 
                                                CommandName="CHANGE_STATUS"  CommandArgument='<%# Eval("ComplianceID") %>' ToolTip="View Compliance Details" />                                                          
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                         <RowStyle CssClass="clsROWgrid"   />
                                         <HeaderStyle CssClass="clsheadergrid"    />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                      <EmptyDataTemplate>
                                        No Search Result Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                 <div class="col-md-12 colpadding0">
                    <div class="col-md-6 colpadding0" style="float: right;">
                        <div class="table-paging" style="margin-bottom: 20px;">
                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                            <div class="table-paging-text">
                                <p>
                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                    <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                </p>
                            </div>

                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
         

                                 </div>
                         </section>
                    </div>
                </div>
            </div>



            <div>
            </div>

        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>

    <div class="modal fade" id="ComplianceDetailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 34px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <asp:UpdatePanel ID="upComplianceDetailsModal" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div>
                                <div id="divRiskType" runat="server" class="circle"></div>
                                <asp:Label ID="lblRiskType" Style="width: 300px; margin-left: -17px; font-size: 13px; color: #333;"
                                    maximunsize="300px" autosize="true" runat="server" />
                            </div>

                            <div id="ActDetails" class="row Dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                                        <h2>Act Details</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize collapsed" onclick="btnminimize(this)" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-down"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseActDetails" class="collapse">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Act Name</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblActName" Style="width: 88%; font-size: 13px; color: #333;"
                                                                        autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Section /Rule</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblRule" Style="width: 88%; font-size: 13px; color: #333;"
                                                                        autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="ComplianceDetails" class="row Dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px;">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails">
                                                        <h2>Compliance Details</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails"><i class="fa fa-chevron-up"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseComplianceDetails" class="panel-collapse collapse in">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Compliance ID</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblComplianceID" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Short Description</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblComplianceDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Detailed Description</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblDetailedDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Penalty</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblPenalty" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Frequency</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblFrequency" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="OthersDetails" class="row Dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px;">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails">
                                                        <h2>Additional Details</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize collapsed" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails"><i class="fa fa-chevron-down"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseOthersDetails" class="collapse">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Risk Type</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Sample Form</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblFormNumber" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                    <asp:LinkButton ID="lbDownloadSample" Style="width: 300px; font-size: 13px; color: blue"
                                                                        runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                                                                    <%--   <asp:LinkButton ID="lnkViewSampleForm" CssClass="view-pdf" Text=" View" Style="width: 300px; font-size: 13px; color:blue"
                                                            runat="server" Font-Underline="false"  OnClick="lnkViewSampleForm_Click" />--%>
                                                                    <a href-data="~/Images/about-us.png" href="#" mime-type="image/png" class="view-pdf" id="lnkViewSampleForm1" runat="server">View</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Regulatory Website Link</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:LinkButton ID="lnkSampleForm" Text="" Style="width: 300px; font-size: 13px; color: blue"
                                                                        runat="server" Font-Underline="false" />
                                                                    <%--OnClick="lnkSampleForm_Click"--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Additional/Reference Text </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblRefrenceText" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divreports" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="showdetails" src="blank.html" width="1150px" height="100%" frameborder="0"></iframe>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
