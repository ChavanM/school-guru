﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class WidgetComplianceAPI : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int UId;
        protected static List<Int32> roles;
        protected static int RoleID;
        protected static int UserRoleID;
        protected static int StatusFlagID;
        protected static int RoleFlag;
        protected static string Flag;
        protected static bool DisableFalg;
        protected static string Path;
        protected static int isapprover;
        protected static string CustomerName;
        protected static string pointname;
        protected static string attribute;
        protected static string widgetid;
        protected static string RiskID;
        protected static string Authorization;
        protected static string FromDate;
        protected static string Enddate;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                if (!string.IsNullOrEmpty(Request.QueryString["pointname"]))
                {
                    pointname = Request.QueryString["pointname"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["attrubute"]))
                {
                    attribute = Request.QueryString["attrubute"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Widgetid"]))
                {
                    widgetid = Convert.ToString(Request.QueryString["Widgetid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["RISKID"]))
                {
                    RiskID = Convert.ToString(Request.QueryString["RISKID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
                {
                    FromDate = Convert.ToString(Request.QueryString["FromDate"]);
                }
                else
                {
                    FromDate = "01-01-1900";
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Enddate"]))
                {
                    Enddate = Convert.ToString(Request.QueryString["Enddate"]);
                }
                else
                {
                    Enddate = "01-01-1900";
                }
                if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                {
                    isapprover = 0;
                    Flag = "MGMT";
                }
                else
                {
                    var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();
                    if (GetApprover.Count > 0)
                    {
                        isapprover = 1;
                        Flag = "APPR";
                    }
                }
                if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "AUDT" || isapprover==1 || AuthenticationHelper.ComplianceProductType == 3)
                {
                    Path = ConfigurationManager.AppSettings["KendoPathApp"];
                    CId = Convert.ToString(AuthenticationHelper.CustomerID);
                    UserId = Convert.ToString(AuthenticationHelper.UserID);
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
                    RoleFlag = 0;
                    CustomerName = CustomerManagement.CustomerGetByIDName(CustId);
                }
            }
        }
    }
}