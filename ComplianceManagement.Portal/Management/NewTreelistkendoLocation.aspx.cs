﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class NewTreelistkendoLocation : System.Web.UI.Page
    {
        protected static int UId;
        protected static int CustId;
        protected static string Path;
        protected static string ComplianceTypeFlag;
        protected static bool IsApprover = false;
        protected int IsDeptHead;
        protected string Role;
        protected void Page_Load(object sender, EventArgs e)
        {
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            Path = ConfigurationManager.AppSettings["KendoPathApp"];

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                IsDeptHead = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["IsDeptHead"]))
                {
                    IsDeptHead = Convert.ToInt32(Request.QueryString["IsDeptHead"]);
                }

                if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                {
                    Role = "MGMT";
                }
                else if (IsDeptHead == 1)
                {
                    Role = "DEPT";
                }
                else
                {
                    var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();
                    if (GetApprover.Count > 0)
                    {
                        Role = "APPR";
                    }
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
            {
                ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                if (ComplianceTypeFlag == "0")
                {
                    ComplianceTypeFlag = "S";
                }
                else
                {
                    ComplianceTypeFlag = "I";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }

            }
        }
    }
}