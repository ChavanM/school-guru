﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using Saml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;


namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class AzureSAMLResponse : System.Web.UI.Page
    {
        private string rawSamlData;
        protected static Business.Data.User user;
        protected static string userEmail;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
            string IdentifierEntityID_idpid = string.Empty;
            string errMessage = string.Empty;

            try
            {
                if (!IsPostBack)
                {
                    userEmail = string.Empty;
                    foreach (string s in Request.Params.Keys)
                    {
                        if (s.ToString() == "SAMLResponse")
                        {
                            rawSamlData = Request.Params[s];
                            break;
                        }
                    }

                    byte[] samlData = Convert.FromBase64String(rawSamlData);

                    string samlAssertion = Encoding.UTF8.GetString(samlData);
                   // samlAssertion = "<samlp:Response ID ='_118278f6-654d-40c6-acf9-d59c9461962d' Version = '2.0' IssueInstant = '2021-07-06T08:56:09.117Z' Destination='https://login.avantis.co.in/azureSAMLResponse.aspx' xmlns:samlp='urn:oasis:names:tc:SAML:2.0:protocol'><Issuer xmlns='urn:oasis:names:tc:SAML:2.0:assertion'>https://sts.windows.net/02dbf22a-371e-4cac-bdbc-60197ffafbe8/</Issuer><samlp:Status><samlp:StatusCode Value='urn:oasis:names:tc:SAML:2.0:status:Success'/></samlp:Status><Assertion ID='_a9c14e9c-5f78-40fd-80da-764818bd7002' IssueInstant='2021-07-06T08:56:09.101Z' Version='2.0' xmlns='urn:oasis:names:tc:SAML:2.0:assertion'><Issuer>https://sts.windows.net/02dbf22a-371e-4cac-bdbc-60197ffafbe8/</Issuer><Signature xmlns='http://www.w3.org/2000/09/xmldsig#'><SignedInfo><CanonicalizationMethod Algorithm='http://www.w3.org/2001/10/xml-exc-c14n#'/><SignatureMethod Algorithm='http://www.w3.org/2001/04/xmldsig-more#rsa-sha256'/><Reference URI='#_a9c14e9c-5f78-40fd-80da-764818bd7002'><Transforms><Transform Algorithm='http://www.w3.org/2000/09/xmldsig#enveloped-signature'/><Transform Algorithm='http://www.w3.org/2001/10/xml-exc-c14n#'/></Transforms><DigestMethod Algorithm='http://www.w3.org/2001/04/xmlenc#sha256'/><DigestValue>PMZ0gmJYXN+ZlM2ExHeWa7IdeYLPDzzFWIlUX5fnoxo=</DigestValue></Reference></SignedInfo><SignatureValue>LRIBSZBi0Szj2fwyLrPGPl3Obn8y7whrCHoPznBE1gQ0XsXdPlrAxzccs4jUyRoHP4qG/a5QbcOoQPba+Q1+8InldCSTy96CRcOnmnLTRu9HzXHdO1OFeoXdfisda8lxBQCL3YI3/xCi3xmJBgNLz6ktH6OymkHaoAh0eY0/mpoTvOPFAPAeCkalDF8KXdj2xJeb4wrqTzBFIkYTsLETNXgHV0bAqKR+Afvrx15czG7QhKCKvMYSNbmIK9E7V4bbAVihnJ2EC+PitYzDZGetMzm6socs1wpxE8iYa055LxJfsnHUddZsMan70S+g9co2jChH0KNCZK/u69fM8wCWog==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIC8DCCAdigAwIBAgIQPwDdP/EigbVCpFG9zlTPwTANBgkqhkiG9w0BAQsFADA0MTIwMAYDVQQDEylNaWNyb3NvZnQgQXp1cmUgRmVkZXJhdGVkIFNTTyBDZXJ0aWZpY2F0ZTAeFw0yMTA1MzEwNzUzMzZaFw0yNDA1MzEwNzUzMzZaMDQxMjAwBgNVBAMTKU1pY3Jvc29mdCBBenVyZSBGZWRlcmF0ZWQgU1NPIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyY2Q6rqLIXoVvNH15tAGgs2Whp4cYRdc4d6bhizsYgiQlGvORSxi3GikQ4JtQvk65YF/qR1cu5TeHsQCWUWPFhwj/L02PAJPKm/tYMZJnc5PmJ205uNB1NGeAHd08UgfcicH1e/VzKNN3TOYDh0T+mC8blwoe8NawuAdCn/Xd9SKeCqVB/rTIUqVqKc6Rncnq09+UFSvth7qeMBECLAfnWYpjBNJwqPu3mxhYGlg2TJClflZKhMX5sC6U3WycYt3P4JI7/1wknZNKO7/daGpq8VqO7G2yxW8rPXK+5FnUQ4c4uy36PTjpovTZcmizknwYs/q2+zyPJNqUHrKC+pmsQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQBxx5Qo7Ew5dnBOWFWuQViWIRNgotCnm88d7q0DBO6JeVfkyqhMPJGB+bNGpcMOnEBFAi1Ufxg5rZ3IvAq+6qXAW7KSC388WSbfo6OE5Uax15wrj9WEvOekjdXAQgWSoJHryNWcWbLBOFDy+Xl/D+5Ks/oqKDF+G7HlqDdupsjEtxmgdzZzDXYPO/4ZnOQAkoiVxg4u2A6PagMrZ661bZa1Yg2f4KouPq7Ex5PBacI+vDLe5tT4ZnqmVnm7p2NVAduhWpcYLrw+7/aXpCeSzPsTMG28P4xrlg1vptxPGu3lQ3V0Xc4MUHEq4baRfpzVN+dXzMlKa3SicvCC7mxNO72/</X509Certificate></X509Data></KeyInfo></Signature><Subject><NameID Format='urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress'>Ninad.Chavan@edelweissfin.com</NameID><SubjectConfirmation Method='urn:oasis:names:tc:SAML:2.0:cm:bearer'><SubjectConfirmationData NotOnOrAfter='2021-07-06T09:56:08.789Z' Recipient='https://login.avantis.co.in/azureSAMLResponse.aspx'/></SubjectConfirmation></Subject><Conditions NotBefore='2021-07-06T08:51:08.789Z' NotOnOrAfter='2021-07-06T09:56:08.789Z'><AudienceRestriction><Audience>spn:8a882290-2efb-4d9f-b35e-c93c90e71ac4</Audience></AudienceRestriction></Conditions><AttributeStatement><Attribute Name='http://schemas.microsoft.com/identity/claims/tenantid'><AttributeValue>02dbf22a-371e-4cac-bdbc-60197ffafbe8</AttributeValue></Attribute><Attribute Name='http://schemas.microsoft.com/identity/claims/objectidentifier'><AttributeValue>57177b66-9015-4ae0-85b7-cb4b69c1c419</AttributeValue></Attribute><Attribute Name='http://schemas.microsoft.com/identity/claims/displayname'><AttributeValue>Ninad Chavan - GI</AttributeValue></Attribute><Attribute Name='http://schemas.microsoft.com/identity/claims/identityprovider'><AttributeValue>https://sts.windows.net/02dbf22a-371e-4cac-bdbc-60197ffafbe8/</AttributeValue></Attribute><Attribute Name='http://schemas.microsoft.com/claims/authnmethodsreferences'><AttributeValue>http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/password</AttributeValue><AttributeValue>http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/x509</AttributeValue><AttributeValue>http://schemas.microsoft.com/claims/multipleauthn</AttributeValue></Attribute><Attribute Name='http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname'><AttributeValue>Ninad</AttributeValue></Attribute><Attribute Name='http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname'><AttributeValue>Chavan</AttributeValue></Attribute><Attribute Name='http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'><AttributeValue>Ninad.Chavan@edelweissfin.com</AttributeValue></Attribute><Attribute Name='http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'><AttributeValue>Ninad.Chavan@edelweissfin.com</AttributeValue></Attribute></AttributeStatement><AuthnStatement AuthnInstant='2021-07-06T08:26:56.128Z' SessionIndex='_a9c14e9c-5f78-40fd-80da-764818bd7002'><AuthnContext><AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</AuthnContextClassRef></AuthnContext></AuthnStatement></Assertion></samlp:Response>";

                    //<samlp:Response ID = "_74e940ad-8384-42cf-b21d-6d2780ac3153" Version = "2.0" IssueInstant ="2021-07-06T09:20:17.688Z" Destination = "http://localhost:18156/azureSAMLResponse.aspx" xmlns: samlp = "urn:oasis:names:tc:SAML:2.0:protocol" >< Issuer xmlns = "urn:oasis:names:tc:SAML:2.0:assertion" > https://sts.windows.net/1dfd1568-66cc-4ea3-88a7-cb70671dd380/</Issuer><samlp:Status><samlp:StatusCode Value="urn:oasis:names:tc:SAML:2.0:status:Success"/></samlp:Status><Assertion ID="_8fb2b083-eb2a-4a41-aeef-1a68cc22bd01" IssueInstant="2021-07-06T09:20:17.688Z" Version="2.0" xmlns="urn:oasis:names:tc:SAML:2.0:assertion"><Issuer>https://sts.windows.net/1dfd1568-66cc-4ea3-88a7-cb70671dd380/</Issuer><Signature xmlns="http://www.w3.org/2000/09/xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/><Reference URI="#_8fb2b083-eb2a-4a41-aeef-1a68cc22bd01"><Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/><Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></Transforms><DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><DigestValue>Zt3MbWDRev4U01Kl4Vl67T3aZgAmVAg92BpuvqIcfPA=</DigestValue></Reference></SignedInfo><SignatureValue>d3AamqeFH5Nf7rdEg5YrXNS57dij7bcz6DhfKWWqQLj0H8zG52RvfeGJ8gIzN52WoUotaFeom2MSHp8hlBiDodKjT52r2GzNtPwYjDMm87X8lPhm/tfCoX+Eakyp2P09n2xfHF/mihI6PdW2zmvC4cxC42MS42NNCIIEcWktTSu7Sd1fujYJ4dFdsY0pwyM+FhrimFPdNVO3EVwGDrelw1Q0eAsKZILd1v5fsr1Z91Zw0unPj5ekGYDfUN368RLeBQ3g1CVTDKuLAKuldQe0VAzsikQKbV1I6omBHBiOlG+8uljdLiuxBWy/xbXs5Oo3qaR7aXVtdfRvWsaMiqWZRw==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIC8DCCAdigAwIBAgIQfVDQWqoICLZD24UkjBDAojANBgkqhkiG9w0BAQsFADA0MTIwMAYDVQQDEylNaWNyb3NvZnQgQXp1cmUgRmVkZXJhdGVkIFNTTyBDZXJ0aWZpY2F0ZTAeFw0yMTA0MjkxMjMzMTRaFw0yNDA0MjkxMjMyNTFaMDQxMjAwBgNVBAMTKU1pY3Jvc29mdCBBenVyZSBGZWRlcmF0ZWQgU1NPIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuYnB6q8Mqk+hYEYKocjxJZkjlcQ6zV4+OATk4Rcc7XmdkIFE1BoCPriv1r5Z6bG3HMFx2UrMUZhB6/2sWki7SmSO6TNR4kivIsK+L+gN09SXsD+wmKBfegB2uBRomEhSAOK86BH4Vz4QbP4UuRlC57ys/ZHYcu7RptYYlSfGgDpVstRF3TVu/JyjVrT1xYpbRDp5IEiI6Iq7rJ6d37neX01Zt3+BaEyCopcJa3kfSmCdjczznI/ENM1ze0k8GEVaNXk/ypFeDe7aofk/bnWOXffeShe7r2L2+ucGUBvmZkDz2P2dcIW3JOhK+9aItfL7mH+MqsUr4tPfUZBPUAi8aQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQB4xYgZxoho68LTE1qvJ1My418hI16tleIiV3j/1fJUB5g0navLlOowGqeZsM5Ll7XtQQZVgR8uqc2QuDbu9Y5NL98IJX0lklM4e9aERd5LLzBpmsQTQZoMP8gIpNIRty1socXmhIXhCFGpjUcVLXJjP6f2g0/3bzqJ858m9SHogdKqOuxjIxgG9HsCIvntQJFOssgWTiOp/zc4EgZx7RBE2i6XeOp4BQkPZ1fq0mP/2zFjRizAL6DrlYE8AHylH21+quAsmH85lyg6t+aLqERxjFCnItsC+/RykbAD3t1o9XoeJ+KG77tCUW2vxnfe4UOxzK1C4sGlF1W0jyLE7O7V</X509Certificate></X509Data></KeyInfo></Signature><Subject><NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress">rahul@avantis.info</NameID><SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer"><SubjectConfirmationData NotOnOrAfter="2021-07-06T10:20:17.563Z" Recipient="http://localhost:18156/azureSAMLResponse.aspx"/></SubjectConfirmation></Subject><Conditions NotBefore="2021-07-06T09:15:17.563Z" NotOnOrAfter="2021-07-06T10:20:17.563Z"><AudienceRestriction><Audience>Rahul-SAMLTEST</Audience></AudienceRestriction></Conditions><AttributeStatement><Attribute Name="http://schemas.microsoft.com/identity/claims/tenantid"><AttributeValue>1dfd1568-66cc-4ea3-88a7-cb70671dd380</AttributeValue></Attribute><Attribute Name="http://schemas.microsoft.com/identity/claims/objectidentifier"><AttributeValue>c6608806-b1be-4dfc-aac1-a7529e12d224</AttributeValue></Attribute><Attribute Name="http://schemas.microsoft.com/identity/claims/displayname"><AttributeValue>Rahul Mane</AttributeValue></Attribute><Attribute Name="http://schemas.microsoft.com/identity/claims/identityprovider"><AttributeValue>https://sts.windows.net/1dfd1568-66cc-4ea3-88a7-cb70671dd380/</AttributeValue></Attribute><Attribute Name="http://schemas.microsoft.com/claims/authnmethodsreferences"><AttributeValue>http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/password</AttributeValue></Attribute><Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"><AttributeValue>rahul@avantis.info</AttributeValue></Attribute><Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"><AttributeValue>rahul@avantis.info</AttributeValue></Attribute></AttributeStatement><AuthnStatement AuthnInstant="2021-07-06T06:33:41.319Z" SessionIndex="_8fb2b083-eb2a-4a41-aeef-1a68cc22bd01"><AuthnContext><AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</AuthnContextClassRef></AuthnContext></AuthnStatement></Assertion></samlp:Response>
                    if (!string.IsNullOrEmpty(samlAssertion))
                    {
                        LoggerMessage.InsertErrorMsg_DBLog(samlAssertion, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    XmlDocument doc = new XmlDocument();
                    XmlNamespaceManager xMan = new XmlNamespaceManager(doc.NameTable);
                    xMan.AddNamespace("saml2p", "urn:oasis:names:tc:SAML:2.0:protocol");
                    xMan.AddNamespace("saml2", "urn:oasis:names:tc:SAML:2.0:assertion");
                    xMan.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");

                    doc.LoadXml(Encoding.UTF8.GetString(samlData));
                    //doc.LoadXml(samlAssertion);
                    XmlNodeList nodeList = doc.GetElementsByTagName("Audience");

                    if (nodeList != null)
                    {
                        foreach (XmlNode curr in nodeList)
                        {
                            IdentifierEntityID_idpid = curr.InnerText;
                            break;
                        }
                    }

                    if (!string.IsNullOrEmpty(IdentifierEntityID_idpid))
                    {
                        //LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                        LogLibrary.WriteErrorLog("IdentifierEntityID :" + IdentifierEntityID_idpid);
                        SAMLConfiguration samlConfigRecord = new SAMLConfiguration();
                        //Check IdentifierEntityID Exists
                        if (IdentifierEntityID_idpid == "spn:8a882290-2efb-4d9f-b35e-c93c90e71ac4")
                        {
                            samlConfigRecord = SAMLManagement.CheckExistsIdentifierEntityID("SaaSAvacom-GI");
                        }
                        else
                        {
                            samlConfigRecord = SAMLManagement.CheckExistsIdentifierEntityID(IdentifierEntityID_idpid);
                        }

                        if (samlConfigRecord != null)
                        {
                            LogLibrary.WriteErrorLog("Start samlConfigRecord :" + samlConfigRecord.IdentifierEntityID_idpid);
                            string Certificate = Server.MapPath(samlConfigRecord.CertificatePath);

                            if (System.IO.File.Exists(Certificate))
                            {
                                LogLibrary.WriteErrorLog("Start Certificate :");
                                string samlCertificate = System.IO.File.ReadAllText(Certificate);

                                Saml.Response samlResponse = new Response(samlCertificate, Request.Form["SAMLResponse"]);

                                if (samlResponse.IsValid())
                                {
                                    string username, firstname, lastname;
                                    try
                                    {                                        
                                        username = samlResponse.GetNameID();
                                        userEmail = samlResponse.GetEmail();
                                        firstname = samlResponse.GetFirstName();
                                        lastname = samlResponse.GetLastName();

                                        LogLibrary.WriteErrorLog("username :" + username);
                                        LogLibrary.WriteErrorLog("userEmail :" + userEmail);
                                        LogLibrary.WriteErrorLog("firstname :" + firstname);
                                        LogLibrary.WriteErrorLog("lastname :" + lastname);
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }

                                    if (string.IsNullOrEmpty(userEmail))
                                    {
                                        XmlNode xNode = doc.SelectSingleNode("/saml2p:Response/saml2:Assertion/saml2:Subject/saml2:NameID", xMan);

                                        XmlNodeList AttributeNodeList = doc.GetElementsByTagName("Attribute");

                                        if (AttributeNodeList != null)
                                        {
                                            foreach (XmlNode curr in AttributeNodeList)
                                            {
                                                if (curr.OuterXml.Contains("emailaddress"))
                                                {
                                                    userEmail = curr.InnerText;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    LogLibrary.WriteErrorLog("emailaddress :" + userEmail);
                                    //Business.Data.User user = null;
                                    user = null;
                                    if (!string.IsNullOrEmpty(userEmail))
                                    {
                                        if (UserManagement.IsValidUser(userEmail, out user))
                                        {
                                            if (user != null)
                                            {
                                                SendMobileResponse("success", "");
                                            }
                                            else
                                            {
                                                errMessage = "No Registered User found with Email - " + userEmail;
                                                cvLogin.IsValid = false;
                                                cvLogin.ErrorMessage = errMessage;
                                                SendMobileResponse("error", errMessage);
                                            }
                                        }
                                        else
                                        {
                                            errMessage = "No Registered User found with Email - " + userEmail;
                                            cvLogin.IsValid = false;
                                            cvLogin.ErrorMessage = errMessage;

                                            SendMobileResponse("error", errMessage);
                                        }
                                    }
                                    else
                                    {
                                        errMessage = "Not able to get Email from Request, Please contact administrator for more details";
                                        cvLogin.IsValid = false;
                                        cvLogin.ErrorMessage = errMessage;
                                        SendMobileResponse("error", errMessage);
                                    }
                                }
                                else
                                {
                                    errMessage = "Not able to recongnize you are coming from valid source or certificate expired, Please contact administrator for more details";
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = errMessage;
                                    SendMobileResponse("error", errMessage);
                                }
                                LogLibrary.WriteErrorLog("End Certificate :");
                            }
                            else
                            {
                                errMessage = "Certificate not exists for Identifier(EntityID)- " + IdentifierEntityID_idpid + ", Please contact administrator for more details and check it is configured properly";
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = errMessage;
                                SendMobileResponse("error", errMessage);
                            }
                            LogLibrary.WriteErrorLog("End samlConfigRecord :" + samlConfigRecord.IdentifierEntityID_idpid);
                        }
                        else
                        {
                            errMessage = "Not able to Identity Registered Customer with Identifier(EntityID)- " + IdentifierEntityID_idpid + ", Please contact administrator for more details and check it is configured properly";
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = errMessage;
                            SendMobileResponse("error", errMessage);
                        }
                    }
                    else
                    {
                        errMessage = "Not able to get Identifier (Entity ID), Please contact administrator for more details";

                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = errMessage;
                        SendMobileResponse("error", errMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void SendMobileResponse(string successError, string errorMessage)
        {
            hdnStatus.Value = successError;
            hdnErrMessage.Value = errorMessage;
            divLoading.Visible = cvLogin.IsValid;
            int IsSuccess = Convert.ToInt32(cvLogin.IsValid);
            LoggerMessage.InsertErrorMsg_DBLog(hdnErrMessage.Value, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);   
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CheckUserAgent", "CheckUserAgent(" + IsSuccess + ");", true);          
        }

        protected void dtbutton_Click(object sender, EventArgs e)
        {
            ProceedLogin(user, string.Empty, userEmail);
        }
        private void ProcessAuthenticationInformation(Business.Data.User user,bool skipotp)
        {
            try
            {
                mst_User objmstuser = UserManagementRisk.GetByID(Convert.ToInt32(user.ID));
                string mstrole = RoleManagementRisk.GetByID(objmstuser.RoleID).Code;
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                }
                else
                {
                    Customer c = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                    var IsInternalComplianceApplicable = c.IComplianceApplicable;
                    if (IsInternalComplianceApplicable != -1)
                    {
                        checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    }
                    var IsTaskApplicable = c.TaskApplicable;
                    if (IsTaskApplicable != -1)
                    {
                        checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    }
                    var IsVerticlApplicable = c.VerticalApplicable;
                    if (IsVerticlApplicable != null)
                    {
                        if (IsVerticlApplicable != -1)
                        {
                            checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                        }
                    }
                    if (c.IsPayment != null)
                    {
                        IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                    }

                    var IsLabelApplicable = c.IsLabelApplicable;
                    if (IsLabelApplicable != -1)
                    {
                        checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    }

                    if (c.ComplianceProductType != null)
                    {
                        complianceProdType = Convert.ToInt32(c.ComplianceProductType);
                    }
                }

                Business.Data.User userToUpdate = new Business.Data.User();
                userToUpdate.ID = user.ID;
                userToUpdate.LastLoginTime = DateTime.Now;
                userToUpdate.ChangePasswordFlag = true;
                userToUpdate.ChangPasswordDate = DateTime.Now;
                userToUpdate.WrongAttempt = 0;

                Business.DataRisk.mst_User userToUpdate1 = new Business.DataRisk.mst_User();
                userToUpdate1.ID = user.ID;
                userToUpdate1.LastLoginTime = DateTime.Now;
                userToUpdate.ChangePasswordFlag = true;
                userToUpdate.ChangPasswordDate = DateTime.Now;
                userToUpdate.WrongAttempt = 0;

                UserManagement.Update(userToUpdate);
                UserManagementRisk.Update(userToUpdate1);

                if (role.Equals("SADMN"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Users/UserSummary.aspx", false);
                }
                else if (role.Equals("IMPT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
                else if (role.Equals("UPDT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/LegalUpdateAdmin.aspx", false);
                }
                else if (role.Equals("RPER"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/ResearchPerformerAdmin.aspx", false);
                }
                else if (role.Equals("RREV"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/ResearchReviewerAdmin.aspx", false);
                }
                else if (role.Equals("HVADM"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else
                {
                    ProductMappingStructure _obj = new ProductMappingStructure();
                    var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(user.CustomerID));
                    if (ProductMappingDetails.Count == 1)
                    {
                        if (ProductMappingDetails.Contains(1))
                        {
                            _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(2))
                        {
                            _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(3))
                        {
                            _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(4))
                        {
                            _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(5))
                        {
                            _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(6))
                        {
                            _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(7))
                        {
                            _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                    }
                    else if (ProductMappingDetails.Count > 1)
                    {
                        if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(2) && user.LitigationRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(5) && user.ContractRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(6) && user.LicenseRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(7) && user.VendorRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                            Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                        }
                    }
                    else
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                        Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                        Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                    }
                }
                //if (!skipotp)
                //{
                //    DateTime LastPasswordChangedDate = Convert.ToDateTime(UserManagement.GetByID(Convert.ToInt32(user.ID)).ChangPasswordDate);
                //    DateTime currentDate = DateTime.Now;
                //    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                //    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                //    if (user.CustomerID == 6)
                //    {
                //        noDays = 90;
                //    }
                //    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                //    if (dateDifference == noDays || dateDifference > noDays)
                //    {
                //        Session["ChangePassword"] = true;
                //        Response.Redirect("~/Account/ChangePassword.aspx", false);
                //    }
                //    else
                //    {
                //        Session["ChangePassword"] = false;
                //    }
                //}                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void ProceedLogin(User user, string ipaddress, string userEmail)
        {
            try
            {
                if (user != null)
                {
                    Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> blockip = null;
                    bool Success = true;
                    try
                    {
                        if (user.CustomerID != null)
                        {
                            blockip = UserManagement.GetBlockIpAddress((int)user.CustomerID, user.ID, ipaddress);
                            if (!blockip.Item1)
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                                return;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    if (user.IsActive)
                    {
                        Session["userID"] = user.ID;
                        Session["ContactNo"] = user.ContactNumber;
                        Session["Email"] = user.Email;
                        Session["CustomerID_new"] = user.CustomerID;
                        bool skipotp = false;
                        try
                        {
                            skipotp = UserManagement.GetMst_SkipOTPByCustID((int)user.CustomerID);
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }

                        if (!skipotp)
                        {
                            #region OTP
                            //Generate Random Number 6 Digit For OTP.
                            Random random = new Random();
                            int value = random.Next(1000000);
                            if (value == 0)
                            {
                                value = random.Next(1000000);
                            }
                            Session["ResendOTP"] = Convert.ToString(value);
                            long Contact;
                            VerifyOTP OTPData = new VerifyOTP()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                EmailId = userEmail,
                                OTP = Convert.ToInt32(value),
                                CreatedOn = DateTime.Now,
                                IsVerified = false,
                                IPAddress = ipaddress
                            };
                            bool OTPresult = false;
                            try
                            {
                                OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                if (OTPresult)
                                {
                                    OTPData.MobileNo = Contact;
                                }
                                else
                                {
                                    OTPData.MobileNo = 0;
                                }
                            }
                            catch (Exception ex)
                            {
                                OTPData.MobileNo = 0;
                                OTPresult = false;
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                            VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.                                                   
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            try
                            {
                                if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                {
                                    //Send Email on User Mail Id.
                                    if (user.CustomerID != 5)
                                    {
                                        //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                                        SendGridEmailManager.SendGridMail(SenderEmailAddress, "alert@avantis.co.in", new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Success = false;
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }


                            if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                            {
                                try
                                {
                                    if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                    {
                                        //Send SMS on User Mobile No.
                                        var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                        if (data != null)
                                        {
                                            SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                        }
                                        else
                                        {
                                            SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {
                                    Success = false;
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                            }

                            #endregion

                            MaintainLoginDetail objData = new MaintainLoginDetail()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                Email = userEmail,
                                CreatedOn = DateTime.Now,
                                IPAddress = ipaddress,
                                MACAddress = ipaddress,
                                LoginFrom = "WC",
                                //ProfileID=user.ID
                            };
                            UserManagement.Create(objData);

                            Business.Data.User userToUpdate = new Business.Data.User();
                            userToUpdate.ID = user.ID;
                            userToUpdate.LastLoginTime = DateTime.UtcNow;
                            userToUpdate.ChangePasswordFlag = true;
                            userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                            userToUpdate.WrongAttempt = 0;

                            Business.DataRisk.mst_User userToUpdate1 = new Business.DataRisk.mst_User();
                            userToUpdate1.ID = user.ID;
                            userToUpdate1.LastLoginTime = DateTime.UtcNow;
                            userToUpdate.ChangePasswordFlag = true;
                            userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                            userToUpdate.WrongAttempt = 0;

                            UserManagement.Update(userToUpdate);
                            UserManagementRisk.Update(userToUpdate1);

                            if (Success)
                            {
                                Session["RM"] = false;
                                Session["EA"] = Util.CalculateAESHash(userEmail.Trim());
                                Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                Response.Redirect("~/Users/OTPVerify.aspx", false);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForSuccess();", true);
                            }
                            else
                            {
                                if (UserManagement.HasUserSecurityQuestion(user.ID))
                                {
                                    Session["RM"] = false;
                                    Session["EA"] = Util.CalculateAESHash(userEmail.Trim());
                                    Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                    Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                }
                                else
                                {
                                    Session["QuestionBank"] = true;
                                    Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                }
                            }
                            
                        }
                        else
                        {

                            try
                            {
                                MaintainLoginDetail objData = new MaintainLoginDetail()
                                {
                                    UserId = Convert.ToInt32(user.ID),
                                    Email = userEmail,
                                    CreatedOn = DateTime.Now,
                                    IPAddress = ipaddress,
                                    MACAddress = ipaddress,
                                    LoginFrom = "WC",
                                    //ProfileID=user.ID
                                };
                                UserManagement.Create(objData);

                                Business.Data.User userToUpdate = new Business.Data.User();
                                userToUpdate.ID = user.ID;
                                userToUpdate.LastLoginTime = DateTime.UtcNow;
                                userToUpdate.ChangePasswordFlag = true;
                                userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                                userToUpdate.WrongAttempt = 0;

                                Business.DataRisk.mst_User userToUpdate1 = new Business.DataRisk.mst_User();
                                userToUpdate1.ID = user.ID;
                                userToUpdate1.LastLoginTime = DateTime.UtcNow;
                                userToUpdate.ChangePasswordFlag = true;
                                userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                                userToUpdate.WrongAttempt = 0;

                                UserManagement.Update(userToUpdate);
                                UserManagementRisk.Update(userToUpdate1);
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }


                            ProcessAuthenticationInformation(user, skipotp);
                        }
                    }
                    else
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Your Account is Disabled.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}