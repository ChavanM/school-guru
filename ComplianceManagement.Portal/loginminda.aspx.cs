﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using AjaxControlToolkit;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Data.Services.Client;
using System.Threading.Tasks;
using System.Security.Claims;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;


namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class loginminda : System.Web.UI.Page
    {
        public static string key = "avantis";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    try
                    {
                        Response.Redirect("https://myapps.microsoft.com/signin/samltest/7e7dac70-214e-4e34-8f18-2c165a2713d6?tenantId=1dfd1568-66cc-4ea3-88a7-cb70671dd380", false);

                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}