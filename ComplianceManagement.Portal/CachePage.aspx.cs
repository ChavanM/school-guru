﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class CachePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        //List<NameValueHierarchy> bracnhes;
                        //string key = "LocationList" + 2;
                        //if (CacheHelper.Exists(key))
                        //{
                        //    CacheHelper.Remove(key);
                        //}
                        
                        // bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(2);
                        // CacheHelper.Set<List<NameValueHierarchy>>(key, bracnhes);
                        

                        var userdata = GetAll();
                        var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];                        
                        string cashstatutoryFSMval = string.Empty;
                        List<sp_ComplianceInstanceAssignmentViewCount_Result> MasterManagementCompliancesSummaryQuery = new List<sp_ComplianceInstanceAssignmentViewCount_Result>();                        
                        foreach (var item in userdata)
                        {                          
                            if (objlocal == "Local")
                            {                                
                                cashstatutoryFSMval = "FSM_PSD" + AuthenticationHelper.UserID;
                            }
                            else
                            {                                
                                cashstatutoryFSMval = "FSMPSD" + AuthenticationHelper.UserID;
                            }                            
                            if (CacheHelper.Exists(cashstatutoryFSMval))
                            {
                                CacheHelper.Remove(cashstatutoryFSMval);
                            }
                            
                            MasterManagementCompliancesSummaryQuery = (from row in entities.sp_ComplianceInstanceAssignmentViewCount(AuthenticationHelper.UserID, -1, "MGMT")
                                                                       select row).ToList();
                            try
                            {
                                CacheHelper.Set<List<sp_ComplianceInstanceAssignmentViewCount_Result>>(cashstatutoryFSMval, MasterManagementCompliancesSummaryQuery);                                                                
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("SET Statutory Error exception :" + cashstatutoryFSMval);
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        public static List<SP_GetGradingReportUsers_Result> GetAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranches = (from row in entities.SP_GetGradingReportUsers("S", "NA")                                                                           
                                        select row).ToList();

                return customerBranches.ToList();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //List<NameValueHierarchy> bracnhes;
                    //string key = "LocationList" + 2;
                    //if (CacheHelper.Exists(key))
                    //{
                    //    CacheHelper.Remove(key);
                    //}

                    // bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(2);
                    // CacheHelper.Set<List<NameValueHierarchy>>(key, bracnhes);


                    var userdata = GetAll();
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string cashstatutoryFSMval = string.Empty;
                    List<sp_ComplianceInstanceAssignmentViewCount_Result> MasterManagementCompliancesSummaryQuery = new List<sp_ComplianceInstanceAssignmentViewCount_Result>();
                    foreach (var item in userdata)
                    {
                        if (objlocal == "Local")
                        {
                            cashstatutoryFSMval = "FSM_PSD" + item.UserID;
                        }
                        else
                        {
                            cashstatutoryFSMval = "FSMPSD" + item.UserID;
                        }
                        if (CacheHelper.Exists(cashstatutoryFSMval))
                        {
                            CacheHelper.Remove(cashstatutoryFSMval);
                        }

                        MasterManagementCompliancesSummaryQuery = (from row in entities.sp_ComplianceInstanceAssignmentViewCount((int)item.UserID, -1, "MGMT")
                                                                   select row).ToList();
                        try
                        {
                            CacheHelper.Set<List<sp_ComplianceInstanceAssignmentViewCount_Result>>(cashstatutoryFSMval, MasterManagementCompliancesSummaryQuery);
                        }
                        catch (Exception)
                        {
                            LogLibrary.WriteErrorLog("SET Statutory Error exception :" + cashstatutoryFSMval);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string email = string.Empty;
            if (!string.IsNullOrEmpty(txtEmail.Text))
            {
                email = txtEmail.Text.Trim();
            }

            long UserID = -1; ;
            if (!string.IsNullOrEmpty(txtUserID.Text))
            {
                UserID =Convert.ToInt32(txtUserID.Text.Trim());
            }

            if (UserID != -1)
            {
                #region UserID
                List<sp_ComplianceInstanceAssignmentViewCount_Result> MasterManagementCompliancesSummaryQuery = new List<sp_ComplianceInstanceAssignmentViewCount_Result>();
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashstatutoryFSMval = string.Empty;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                  
                    if (objlocal == "Local")
                    {
                        cashstatutoryFSMval = "FSM_PSD" + UserID;
                    }
                    else
                    {
                        cashstatutoryFSMval = "FSMPSD" + UserID;
                    }
                    if (CacheHelper.Exists(cashstatutoryFSMval))
                    {
                        CacheHelper.Remove(cashstatutoryFSMval);
                    }

                    MasterManagementCompliancesSummaryQuery = (from row in entities.sp_ComplianceInstanceAssignmentViewCount((int)UserID, -1, "MGMT")
                                                               select row).ToList();
                    try
                    {
                        CacheHelper.Set<List<sp_ComplianceInstanceAssignmentViewCount_Result>>(cashstatutoryFSMval, MasterManagementCompliancesSummaryQuery);
                    }
                    catch (Exception)
                    {
                        LogLibrary.WriteErrorLog("SET Statutory Error exception :" + cashstatutoryFSMval);
                    }
                }
                #endregion
            }
            else if (!string.IsNullOrEmpty(email))
            {
                #region Email
                List<sp_ComplianceInstanceAssignmentViewCount_Result> MasterManagementCompliancesSummaryQuery = new List<sp_ComplianceInstanceAssignmentViewCount_Result>();
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashstatutoryFSMval = string.Empty;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var users = (from row in entities.Users
                                 where row.Email.ToUpper().Trim() == email.ToUpper().Trim()
                                 select row).FirstOrDefault();



                    if (objlocal == "Local")
                    {
                        cashstatutoryFSMval = "FSM_PSD" + users.ID;
                    }
                    else
                    {
                        cashstatutoryFSMval = "FSMPSD" + users.ID;
                    }
                    if (CacheHelper.Exists(cashstatutoryFSMval))
                    {
                        CacheHelper.Remove(cashstatutoryFSMval);
                    }

                    MasterManagementCompliancesSummaryQuery = (from row in entities.sp_ComplianceInstanceAssignmentViewCount((int)users.ID, -1, "MGMT")
                                                               select row).ToList();
                    try
                    {
                        CacheHelper.Set<List<sp_ComplianceInstanceAssignmentViewCount_Result>>(cashstatutoryFSMval, MasterManagementCompliancesSummaryQuery);
                    }
                    catch (Exception)
                    {
                        LogLibrary.WriteErrorLog("SET Statutory Error exception :" + cashstatutoryFSMval);
                    }
                }
                #endregion
            }
        }
    }
}