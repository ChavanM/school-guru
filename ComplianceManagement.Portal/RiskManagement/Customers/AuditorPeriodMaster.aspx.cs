﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers
{
    public partial class AuditorPeriodMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindAuditorList();
                GetPageDisplaySummary();             
            }
        }                
        protected void upPromotor_Load(object sender, EventArgs e)
        {            
        }
        protected void grdAuditorPeriod_RowDataBound(object sender, GridViewRowEventArgs e)
        {           
        }
        private void BindAuditorList()
        {
            try
            {
                var AuditorMasterList = ProcessManagement.GetAllAuditorPeriodMaster();
                grdAuditorPeriod.DataSource = AuditorMasterList;
                Session["TotalRows"] = AuditorMasterList.Count;
                grdAuditorPeriod.DataBind();
                upPromotorList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                Mst_AuditPeriodMaster MstAuditPeriodMaster = new Mst_AuditPeriodMaster()
                {
                    Name = txtFName.Text,                                      
                    IsActive = false
                };
                if ((int)ViewState["Mode"] == 1)
                {
                    MstAuditPeriodMaster.ID = Convert.ToInt32(ViewState["AuditorID"]);
                }
                if ((int)ViewState["Mode"] == 0)
                {
                    if (ProcessManagement.AuditPeriodExists(MstAuditPeriodMaster.Name))
                    {
                        cvDuplicateEntry.ErrorMessage = "Audit Period Already Exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    ProcessManagement.CreateAuditorPeriodMaster(MstAuditPeriodMaster);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    if (ProcessManagement.AuditPeriodExists(MstAuditPeriodMaster.Name))
                    {
                        cvDuplicateEntry.ErrorMessage = "Audit Period Already Exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    ProcessManagement.UpdateAuditorPeriodMaster(MstAuditPeriodMaster);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAuditorPeriodDialog\").dialog('close')", true);
                BindAuditorList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {               
                ViewState["Mode"] = 0;
                txtFName.Text = string.Empty;                
                upPromotor.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorPeriodDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditorPeriod_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var AuditorMasterList = ProcessManagement.GetAllPeriodList();
                if (direction == SortDirection.Ascending)
                {
                    AuditorMasterList = AuditorMasterList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    AuditorMasterList = AuditorMasterList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdAuditorPeriod.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAuditorPeriod.Columns.IndexOf(field);
                    }
                }
                grdAuditorPeriod.DataSource = AuditorMasterList;
                grdAuditorPeriod.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditorPeriod_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int AuditorID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_PROMOTER"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["AuditorID"] = AuditorID;

                    Mst_AuditPeriodMaster RPD = ProcessManagement.AuditorPeriodMasterGetByID(AuditorID);                                 
                    txtFName.Text = RPD.Name;                   
                    upPromotor.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorPeriodDialog\").dialog('open')", true);

                }
                else if (e.CommandName.Equals("DELETE_PROMOTER"))
                {
                    ProcessManagement.DeleteAuditorPeriodMaster(AuditorID);
                    BindAuditorList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditorPeriod_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAuditorPeriod.PageIndex = e.NewPageIndex;
                BindAuditorList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void grdAuditorPeriod_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
        //        if (sortColumnIndex != -1)
        //        {
        //            AddSortImage(sortColumnIndex, e.Row);
        //        }
        //    }
        //}
        //protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        //{
        //    System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
        //    sortImage.ImageAlign = ImageAlign.AbsMiddle;

        //    if (direction == SortDirection.Ascending)
        //    {
        //        sortImage.ImageUrl = "../../Images/SortAsc.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../../Images/SortDesc.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }
        //    headerRow.Cells[columnIndex].Controls.Add(sortImage);
        //}
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdAuditorPeriod.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditorPeriod.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindAuditorList();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }


        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    grdAuditorPeriod.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditorPeriod.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindAuditorList();                
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    grdAuditorPeriod.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditorPeriod.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                }
                //Reload the Grid
                BindAuditorList();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

    }
}