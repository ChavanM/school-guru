﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AuditStatusUI : System.Web.UI.Page
    {
        int Statusid;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        public static List<long> Branchlist = new List<long>();
        public static List<sp_PerformerReviewView_Result> PerformerReviewViewList = new List<sp_PerformerReviewView_Result>();
        protected void Page_Load(object sender, EventArgs e)
        {
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "Open")
                    {
                        ViewState["Status"] = 1;
                    }

                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["Status"] = 3;
                    }
                }

                BindFinancialYear();
                if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    string FinancialYear = Convert.ToString(Request.QueryString["FY"]);
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                else
                {
                    string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    if (FinancialYear != null)
                    {
                        ddlFinancialYear.ClearSelection();
                        ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                    }
                }
                //string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                //if (FinancialYear != null)
                //{
                //    ddlFinancialYear.ClearSelection();
                //    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                //}
                BindLegalEntityData();
                BindVertical();
                PerformerReviewViewList = DashboardManagementRisk.sp_GetPerFormerReviewer();
                if (roles.Contains(3))
                {
                    PerformerFlag = true;
                    ShowPerformer(sender, e);
                }
                else if (roles.Contains(4))
                {
                    ReviewerFlag = true;
                    ShowReviewer(sender, e);
                }
                else if (roles.Contains(5))
                {
                    ReviewerFlag = true;
                    ShowReviewer(sender, e);
                }
                else
                {
                    PerformerFlag = true;
                }
                bindPageNumber();
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdRiskActivityMatrix.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                //ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, new ListItem("Annually", "1"));
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindCustomer()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlFilterLocation.DataBind();
            //ddlFilterLocation.Items.Insert(0, new ListItem(" Select Location ", "-1"));
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            //ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, customerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindSchedulingType(int BranchId)
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(BranchId);
            ddlSchedulingType.DataBind();
            //ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindSchedulingType(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }

            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindSchedulingType(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }

            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindSchedulingType(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindSchedulingType(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                    {
                        BindSchedulingType(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindVertical();
                        BindData("P");
                        bindPageNumber();
                        //GetPageDisplaySummary();
                    }
                }
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                    {
                        BindVertical();
                        BindData("N");
                        bindPageNumber();
                        //GetPageDisplaySummary();
                    }
                }
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");

            // GetPageDisplaySummary();
            bindPageNumber();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            //{
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                //BindAuditSchedule("P", 5);

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        int count = 0;
                        count = UserManagementRisk.GetPhaseCount(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindAuditSchedule("P", count);
                    }
                }
            }
            //}
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");
            bindPageNumber();
        }



        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindData("P");
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindData("N");
            }
        }
        public string ShowRating(string RiskRating)
        {
            string processnonprocess = "";
            if (RiskRating == "1")
            {
                processnonprocess = "High";
            }
            else if (RiskRating == "2")
            {
                processnonprocess = "Medium";
            }
            else if (RiskRating == "3")
            {
                processnonprocess = "Low";
            }
            return processnonprocess.Trim(',');
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
            BindData("P");
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
            BindData("P");
        }
        private void BindData(string Flag)
        {
            try
            {
                int roleid = -1;
                int branchid = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;
                int Verticalid = -1;
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ViewState["Status"].ToString()))
                {
                    Statusid = Convert.ToInt32(ViewState["Status"]);
                }

                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }

                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }              
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        Verticalid = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            Verticalid = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }
                if (FinYear == "")
                    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                if (PerformerFlag)
                    roleid = 3;

                else if (ReviewerFlag)
                    roleid = 4;
                roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                if (roles.Contains(5))
                {
                    roleid = 5;
                }
                if (PerformerFlag)
                {
                    roleid = 3;
                }
                Branchlist.Clear();
                GetAllHierarchy(customerID, branchid);
                Branchlist.ToList();

                if (Flag == "P")
                {
                    var AuditLists = DashboardManagementRisk.GetAudits(FinYear, Period, customerID, Branchlist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, Verticalid, roleid);
                    grdRiskActivityMatrix.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdRiskActivityMatrix.DataBind();
                    AuditLists.Clear();
                    AuditLists = null;
                }
                else
                {
                    var AuditLists = DashboardManagementRisk.GetAudits(FinYear, Period, customerID, Branchlist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, Verticalid, roleid);
                    grdRiskActivityMatrix.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdRiskActivityMatrix.DataBind();
                    AuditLists.Clear();
                    AuditLists = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        protected bool CheckEnable(String Role, String SDate, String EDate)
        {
            if (Role != "Performer")
                return false;
            else if ((SDate == "" || EDate == "") && Role == "Performer")
                return true;
            else if (SDate != "" && EDate != "")
                return false;
            else
                return true;
        }

        protected bool CheckActionButtonEnable(String Role, String SDate, String EDate)
        {
            if ((SDate == "" || EDate == "") && Role == "Performer")
                return false;
            else if (SDate != "" && EDate != "")
                return true;
            else
                return true;
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void upAuditSummaryDetails_Load(object sender, EventArgs e)
        {

        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {

        }
        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("SaveDate"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        long AuditID = Convert.ToInt32(commandArgs[6]);
                        GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        int RowIndex = gvr.RowIndex;
                        TextBox txtStartDate = (TextBox)grdRiskActivityMatrix.Rows[RowIndex].FindControl("txtStartDate");
                        TextBox txtEndDate = (TextBox)grdRiskActivityMatrix.Rows[RowIndex].FindControl("txtEndDate");

                        if (txtStartDate.Text != "" && txtEndDate.Text != "")
                        {
                            try
                            {
                                Convert.ToDateTime(GetDate(txtStartDate.Text));
                                Convert.ToDateTime(GetDate(txtEndDate.Text));
                            }
                            catch (Exception ex)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Provide Valid Audit Start Date and End Date.";
                                return;
                            }

                            if (Convert.ToDateTime(GetDate(txtStartDate.Text)) <= Convert.ToDateTime(GetDate(txtEndDate.Text)))
                            {

                                UserManagementRisk.UpdateInternalControlAuditAssignment(Convert.ToDateTime(GetDate(txtStartDate.Text)), Convert.ToDateTime(GetDate(txtEndDate.Text)), AuditID);

                                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                                    BindData("P");
                                else
                                    BindData("N");
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Audit Start Date should be less or equal to end date.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Provide Start Date and End Date";
                        }
                    }
                }
                else if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                        string ForMonth = Convert.ToString(commandArgs[1]);
                        string FinancialYear = Convert.ToString(commandArgs[2]);
                        
                        int RoleID = Convert.ToInt32(commandArgs[3]);
                        long AuditID = Convert.ToInt32(commandArgs[6]);
                        if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                        {
                            Response.Redirect("~/RiskManagement/InternalAuditTool/AuditStatusSummary.aspx?Role=" + RoleID + "&AuditID=" + AuditID + "&FY=" + FinancialYear + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString(), false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskManagement/InternalAuditTool/AuditStatusSummary.aspx?Role=" + RoleID + "&AuditID=" + AuditID + "&FY=" + FinancialYear + "&ReturnUrl1=", false);
                        }
                    }
                }
                else if (e.CommandName.Equals("ViewprerequsiteSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        int RoleID = Convert.ToInt32(commandArgs[0]);
                        int UserID = Convert.ToInt32(commandArgs[1]);
                        long AuditID = Convert.ToInt32(commandArgs[2]);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + RoleID + "," + UserID + "," + AuditID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData("P");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdRiskActivityMatrix_RowBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label AuditId = (Label)e.Row.FindControl("AuditId");
                Label lblProcess = (Label)e.Row.FindControl("lblProcess");
                Label lblPerformer = (Label)e.Row.FindControl("lblPerformer");
                long cbranchId = Convert.ToInt64(AuditId.Text);

                int roleid = 3;
                List<sp_PerformerReviewView_Result> Rowss = (from C in PerformerReviewViewList
                                                             where C.AuditID == cbranchId
                                                             select C).Distinct().ToList();

                var ProcessName = (from C in Rowss
                                   where C.Roleid == roleid
                                   select C.ProcessName).Distinct().ToList();

                string Process = "";
                foreach (var item in ProcessName)
                {
                    Process += item + ",";
                }
                lblProcess.Text = Process.TrimEnd(',');
                lblProcess.Attributes.Add("tooltip", lblProcess.Text);

            }

        }
    

    protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //Reload the Grid
                BindData("P");
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskActivityMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }


        public string GetProcessName(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Processlist = (from row in entities.ProcessCommaSaperates
                                   where row.AuditID == AuditID
                                   select row.ProcessName).FirstOrDefault();

                return Processlist;
            }
        }

        public string GetSubProcessName(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Processlist = (from row in entities.SubProcessCommaSeparates
                                   where row.AuditID == AuditID
                                   select row.SubProcessName).FirstOrDefault();

                return Processlist;
            }
        }
    }
}