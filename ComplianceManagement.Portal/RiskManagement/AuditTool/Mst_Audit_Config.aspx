﻿<%@ Page Title="Configuration List" Language="C#" MasterPageFile="~/AuditTool.Master"
    AutoEventWireup="true" Async="true" EnableEventValidation="false"
    CodeBehind="Mst_Audit_Config.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.Mst_Audit_Config" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-12 colpadding0">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
            ValidationGroup="ComplianceInstanceValidationGroup" />
        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
        <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
    </div>
    <asp:UpdatePanel ID="upReminderConfiguration" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <div style="margin-bottom: 7px">
                <div>
                    <fieldset style="border: 1px solid #999;">
                        <legend>Audit  Reminder</legend>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0" style="margin-right: 5px; width: 48%;">

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-5 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">Before In Days </p>
                                    </div>
                                    <div class="col-md-1 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">:</p>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-5 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">Reminder to auditee sent in every </p>
                                    </div>
                                    <div class="col-md-1 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">:</p>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">
                                            <asp:DropDownList ID="DropDownList1" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                                <asp:ListItem>22</asp:ListItem>
                                                <asp:ListItem>23</asp:ListItem>
                                                <asp:ListItem>24</asp:ListItem>
                                                <asp:ListItem>25</asp:ListItem>
                                                <asp:ListItem>26</asp:ListItem>
                                                <asp:ListItem>27</asp:ListItem>
                                                <asp:ListItem>28</asp:ListItem>
                                                <asp:ListItem>29</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                            </asp:DropDownList>
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0" style="text-align: center">
                                    <asp:Button ID="Button1" runat="server" Text="Save" CssClass="btn btn-primary" />
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0" style="text-align: center">
                                    <asp:CheckBoxList runat="server" ID="CheckBoxList1" RepeatDirection="Horizontal" RepeatLayout="Table"
                                        Enabled="false"
                                        CellPadding="5">
                                    </asp:CheckBoxList>
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="margin-right: 5px; width: 48%;">

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-6 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">Reminder to auditee sent after every </p>
                                    </div>
                                    <div class="col-md-1 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">:</p>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">
                                            <asp:DropDownList ID="DropDownList2" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                                <asp:ListItem>22</asp:ListItem>
                                                <asp:ListItem>23</asp:ListItem>
                                                <asp:ListItem>24</asp:ListItem>
                                                <asp:ListItem>25</asp:ListItem>
                                                <asp:ListItem>26</asp:ListItem>
                                                <asp:ListItem>27</asp:ListItem>
                                                <asp:ListItem>28</asp:ListItem>
                                                <asp:ListItem>29</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                            </asp:DropDownList>
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0" style="text-align: center">
                                    <asp:Button ID="Button2" runat="server" Text="Save" CssClass="btn btn-primary"  />
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0" style="text-align: center">
                                    <asp:CheckBoxList runat="server" ID="CheckBoxList2" RepeatDirection="Horizontal" RepeatLayout="Table"
                                        Enabled="false"
                                        CellPadding="5">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div>
                    <fieldset style="border: 1px solid #999;">
                        <legend>Pre-Requisite  Reminder</legend>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0" style="margin-right: 5px; width: 48%;">

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-5 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">Before In Days </p>
                                    </div>
                                    <div class="col-md-1 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">:</p>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">
                                            <asp:TextBox ID="txtBeforeInDays" runat="server"></asp:TextBox>
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-5 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">Reminder to auditee sent in every </p>
                                    </div>
                                    <div class="col-md-1 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">:</p>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">
                                            <asp:DropDownList ID="ddlpre" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                                <asp:ListItem>22</asp:ListItem>
                                                <asp:ListItem>23</asp:ListItem>
                                                <asp:ListItem>24</asp:ListItem>
                                                <asp:ListItem>25</asp:ListItem>
                                                <asp:ListItem>26</asp:ListItem>
                                                <asp:ListItem>27</asp:ListItem>
                                                <asp:ListItem>28</asp:ListItem>
                                                <asp:ListItem>29</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                            </asp:DropDownList>
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0" style="text-align: center">
                                    <asp:Button ID="btnpresave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnpresave_Click" />
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0" style="text-align: center">
                                    <asp:CheckBoxList runat="server" ID="cblMonthly" RepeatDirection="Horizontal" RepeatLayout="Table"
                                        Enabled="false"
                                        CellPadding="5">
                                    </asp:CheckBoxList>
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="margin-right: 5px; width: 48%;">

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-6 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">Reminder to auditee sent after every </p>
                                    </div>
                                    <div class="col-md-1 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">:</p>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">
                                            <asp:DropDownList ID="ddlescalate" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                                <asp:ListItem>22</asp:ListItem>
                                                <asp:ListItem>23</asp:ListItem>
                                                <asp:ListItem>24</asp:ListItem>
                                                <asp:ListItem>25</asp:ListItem>
                                                <asp:ListItem>26</asp:ListItem>
                                                <asp:ListItem>27</asp:ListItem>
                                                <asp:ListItem>28</asp:ListItem>
                                                <asp:ListItem>29</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                            </asp:DropDownList>
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0" style="text-align: center">
                                    <asp:Button ID="btnescatate" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnescatate_Click" />
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0" style="text-align: center">
                                    <asp:CheckBoxList runat="server" ID="cbescalate" RepeatDirection="Horizontal" RepeatLayout="Table"
                                        Enabled="false"
                                        CellPadding="5">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
