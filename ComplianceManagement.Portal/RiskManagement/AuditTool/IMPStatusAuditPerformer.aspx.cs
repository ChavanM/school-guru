﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class IMPStatusAuditPerformer : System.Web.UI.Page
    {
        protected string FinYear;
        protected string Period;
        protected int BranchId;
        protected int ResultID;
        protected int StatusId;
        protected int VerticalID;
        protected int ScheduledOnId;
        protected static string IsAfterPerformer;
        protected int AuditID;
        protected static int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindCustomer();
                BindFinancialYear();
                BindUsersNEWIMP();
                //BindUsersIMP();
                //BindObservationCategoryIMP();

                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Request.QueryString["FinYear"];
                    if (FinYear != "")
                    {
                        ViewState["FinYear"] = FinYear;
                        ddlFinancialYear.ClearSelection();
                        ddlFinancialYear.Items.FindByText(FinYear).Selected = true;
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchId = Convert.ToInt32(Request.QueryString["BID"]);
                    ViewState["BID"] = Request.QueryString["BID"];
                    if (BranchId != 0)
                    {
                        ddlFilterLocation.ClearSelection();
                        ddlFilterLocation.Items.FindByValue(BranchId.ToString()).Selected = true;
                        BindSchedulingType(BranchId);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    ViewState["AuditID"] = AuditID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                    ViewState["ForMonth"] = Request.QueryString["ForMonth"];
                    String SType = GetSchedulingType(BranchId, FinYear, Period, AuditID);
                    if (SType != "")
                    {
                        ddlSchedulingType.ClearSelection();

                        if (SType == "A")
                            ddlSchedulingType.Items.FindByText("Annually").Selected = true;
                        else if (SType == "H")
                            ddlSchedulingType.Items.FindByText("Half Yearly").Selected = true;
                        else if (SType == "Q")
                            ddlSchedulingType.Items.FindByText("Quarterly").Selected = true;
                        else if (SType == "M")
                            ddlSchedulingType.Items.FindByText("Monthly").Selected = true;
                        else if (SType == "P")
                            ddlSchedulingType.Items.FindByText("Phase").Selected = true;
                        else if (SType == "S")
                            ddlSchedulingType.Items.FindByText("Special Audit").Selected = true;

                        ddlSchedulingType_SelectedIndexChanged(null, null);

                        ddlPeriod.ClearSelection();
                        ddlPeriod.Items.FindByText(Period).Selected = true;
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                    ViewState["ResultID"] = Request.QueryString["ResultID"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    ViewState["VID"] = Request.QueryString["VID"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                    ViewState["scheduledonid"] = Request.QueryString["scheduledonid"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    StatusId = Convert.ToInt32(Request.QueryString["SID"]);
                    if (StatusId != 0)
                    {
                        ddlFilterStatus.ClearSelection();
                        ddlFilterStatus.Items.FindByValue(StatusId.ToString()).Selected = true;
                    }
                }
                tvImplementation_SelectedNodeChanged(null, null);
                btnFinalStatus_Click(sender, e);
            }
            DateTime date = DateTime.MinValue;
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
        }

        #region Implementation Status 

        private void BindStatusListIMP(int statusID, List<int> rolelist, string isAuditmanager)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                }
                long ProcessID = ProcessManagement.GetProcessIDByResultID(ResultID);
                rdbtnStatusNEW.Items.Clear();
                var RoleListOfUser = CustomerManagementRisk.GetListOfRoleInAuditIMp(AuditID, Portal.Common.AuthenticationHelper.UserID, ProcessID);
                var statusList = AuditStatusManagement.GetInternalStatusList();
                List<InternalAuditStatu> allowedStatusList = null;
                List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.ID).ToList();
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                var RoleListInAudit = CustomerManagementRisk.GetAuditListRoleIMP(AuditID, ProcessID);

                #region SingleUser
                if (statusID == 7)
                {
                    if (ddlFilterStatus.SelectedValue == "6")
                    {
                        if (!RoleListInAudit.Contains(4))
                        {
                            if (RoleListOfUser.Contains(3))
                            {
                                #region                        
                                foreach (InternalAuditStatu st in allowedStatusList)
                                {
                                    if ((st.ID == 2) || (st.ID == 4) || (st.ID == 5))
                                    {
                                    }
                                    else
                                    {
                                        rdbtnStatusNEW.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                if (RoleListOfUser.Count == 0)
                                {
                                    btnIMPSave.Enabled = false;
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            if (RoleListOfUser.Contains(3))
                            {
                                #region       
                                foreach (InternalAuditStatu st in allowedStatusList)
                                {
                                    if ((st.ID == 3) || (st.ID == 4) || (st.ID == 5))
                                    {
                                    }
                                    else
                                    {
                                        if (st.ID == 2)
                                        {
                                            // rdbtnStatusNEW.Items.Insert(0, new ListItem("Team Review", Convert.ToString(st.ID)));
                                            rdbtnStatusNEW.Items.Insert(0, new ListItem("Review", Convert.ToString(st.ID)));
                                        }
                                        else
                                        {
                                            rdbtnStatusNEW.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                        }
                                    }
                                }
                                if (RoleListOfUser.Count == 0)
                                {
                                    btnIMPSave.Enabled = false;
                                }
                                #endregion
                            }
                        }
                    }
                }//Personresponsible End
                #endregion

                if (rdbtnStatusNEW.Items.Count > 0)
                {
                    StatusTR.Visible = true;
                }
                else
                {
                    StatusTR.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static String GetSchedulingType(int CustBranchID, String FinYear, String Period, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Record = (from row in entities.InternalAuditSchedulings
                              where row.CustomerBranchId == CustBranchID
                              && row.FinancialYear == FinYear
                              && row.TermName == Period
                              && row.AuditID == AuditID
                              select row.ISAHQMP).FirstOrDefault();

                if (Record != "")
                    return Record;
                else
                    return "";
            }
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {

                        if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                        {
                            AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                            int count = 0;
                            count = UserManagementRisk.GetPhaseCountNew(Convert.ToInt32(ddlFilterLocation.SelectedValue), AuditID);
                            BindAuditSchedule("P", count);
                        }
                    }
                }
            }
        }
        //protected void ddlObservationCategory_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    //if (!string.IsNullOrEmpty(ddlIMPObservationCategory.SelectedValue))
        //    //{
        //    //    if (ddlIMPObservationCategory.SelectedValue != "-1")
        //    //    {
        //    //        BindObservationSubCategory(Convert.ToInt32(ddlIMPObservationCategory.SelectedValue));
        //    //    }
        //    //}
        //}
        //public void BindObservationSubCategory(int ObservationId)
        //{
        //    if (CustomerId == 0)
        //    {
        //        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
        //    }
        //    ddlIMPObservationSubCategory.DataTextField = "Name";
        //    ddlIMPObservationSubCategory.DataValueField = "ID";
        //    ddlIMPObservationSubCategory.Items.Clear();
        //    ddlIMPObservationSubCategory.DataSource = ObservationSubcategory.FillObservationSubCategory(ObservationId, CustomerId);
        //    ddlIMPObservationSubCategory.DataBind();
        //    ddlIMPObservationSubCategory.Items.Insert(0, new ListItem("Observation Sub-Category", "-1"));
        //}
        public void BindSchedulingType(int BranchId)
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(BranchId);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        public void BindCustomer()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(CustomerId);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }

                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        //public void BindUsersIMP()
        //{
        //    if (CustomerId == 0)
        //    {
        //        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
        //    }
        //    //ddlIMPPersonresponsible.Items.Clear();
        //    //ddlIMPPersonresponsible.DataTextField = "Name";
        //    //ddlIMPPersonresponsible.DataValueField = "ID";
        //    //ddlIMPPersonresponsible.DataSource = RiskCategoryManagement.FillUsers(CustomerId);
        //    //ddlIMPPersonresponsible.DataBind();
        //    //ddlIMPPersonresponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
        //}
        public void BindUsersNEWIMP()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlIMPNewPersonresponsible.Items.Clear();
            ddlIMPNewPersonresponsible.DataTextField = "Name";
            ddlIMPNewPersonresponsible.DataValueField = "ID";
            ddlIMPNewPersonresponsible.DataSource = RiskCategoryManagement.FillUsers(CustomerId);
            ddlIMPNewPersonresponsible.DataBind();
            ddlIMPNewPersonresponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
        }
        //public void BindObservationCategoryIMP()
        //{
        //    if (CustomerId == 0)
        //    {
        //        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
        //    }
        //    ddlIMPObservationCategory.DataTextField = "Name";
        //    ddlIMPObservationCategory.DataValueField = "ID";
        //    ddlIMPObservationCategory.Items.Clear();
        //    ddlIMPObservationCategory.DataSource = ObservationSubcategory.FillObservationCategory(CustomerId);
        //    ddlIMPObservationCategory.DataBind();
        //    ddlIMPObservationCategory.Items.Insert(0, new ListItem("Select Observation Category", "-1"));
        //}
        public static List<ImplementationReviewHistory> GetFileDataIMP(int id, int ImplementationInstance, int ImplementationScheduleOnID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.ImplementationReviewHistories
                                where row.ID == id && row.ImplementationInstance == ImplementationInstance
                                && row.ImplementationScheduleOnID == ImplementationScheduleOnID
                                && row.AuditID == AuditID
                                select row).ToList();

                return fileData;
            }
        }
        protected void DownLoadClickIMP(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblID = (Label)gvr.FindControl("lblID");
                Label lblriskID = (Label)gvr.FindControl("lblriskID");
                Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<ImplementationReviewHistory> fileData = GetFileDataIMP(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text), AuditID);
                    int i = 0;
                    string directoryName = "abc";
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TestingDocument.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void DeleteFileIMP(int fileId, long AuditID)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileImplementationFileData_Risk(fileId, AuditID);
                if (file != null)
                {
                    string path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    DashboardManagementRisk.DeleteFileIMP(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void DownloadFileIMP(int fileId, long AuditID)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileImplementationFileData_Risk(fileId, AuditID);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();

                    }
                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindDocumentIMP(int ScheduledOnID, int resultID, long AuditID)
        {
            try
            {
                List<GetImplementationAuditDocumentsView> ComplianceDocument = new List<GetImplementationAuditDocumentsView>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetImplementationAuditDocumentsView(ScheduledOnID, resultID, AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptIMPComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptIMPComplianceDocumnets.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool CreateAuditImplementationTransaction(AuditImpementationScheduleOn ImpementationScheduleOnDetails, AuditImplementationTransaction transaction, long customerID, long AuditID)
        {
            bool flag = false;
            if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
            {
                ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
            }
            else
            {
                ResultID = Convert.ToInt32(ViewState["ResultID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
            {
                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
            }
            else
            {
                VerticalID = Convert.ToInt32(ViewState["VID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (ImpementationScheduleOnDetails != null)
            {
                if (fuIMPWorkingUpload.HasFile)
                {
                    HttpFileCollection fileCollection1 = Request.Files;
                    if (fileCollection1.Count > 0)
                    {
                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                        List<ImplementationFileData_Risk> files = new List<ImplementationFileData_Risk>();
                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                        var InstanceData = RiskCategoryManagement.GetAuditImplementationInstanceData(ImpementationScheduleOnDetails.ForMonth, Convert.ToInt32(ImpementationScheduleOnDetails.ImplementationInstance), Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, AuditID);
                        string directoryPath = "";
                        if (!string.IsNullOrEmpty(InstanceData.ForPeriod) && InstanceData.CustomerBranchID != -1)
                        {
                            directoryPath = Server.MapPath("~/ImplementationWorkingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + ddlFinancialYear.SelectedItem.Text + "/" + InstanceData.ForPeriod.ToString() + "/" + ImpementationScheduleOnDetails.Id + "/1.0");
                        }
                        DocumentManagement.CreateDirectory(directoryPath);
                        for (int i = 0; i < fileCollection1.Count; i++)
                        {
                            HttpPostedFile uploadfile = fileCollection1[i];
                            string[] keys = fileCollection1.Keys[i].Split('$');
                            string fileName = "";
                            if (keys[keys.Count() - 1].Equals("fuIMPWorkingUpload"))
                            {
                                fileName = uploadfile.FileName;
                            }
                            Guid fileKey = Guid.NewGuid();
                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                            Stream fs = uploadfile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                            if (uploadfile.ContentLength > 0)
                            {
                                ImplementationFileData_Risk file = new ImplementationFileData_Risk()
                                {
                                    Name = fileName,
                                    FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                    FileKey = fileKey.ToString(),
                                    Version = "1.0",
                                    VersionDate = DateTime.UtcNow,
                                    ForPeriod = ImpementationScheduleOnDetails.ForMonth,
                                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                    FinancialYear = ImpementationScheduleOnDetails.FinancialYear,
                                    IsDeleted = false,
                                    ResultId = ResultID,
                                    VerticalID = VerticalID,
                                    AuditID = AuditID,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };
                                files.Add(file);
                            }
                        }//For Loop End  
                        if (Filelist != null && list != null)
                            flag = UserManagementRisk.CreateAuditImplementationTransaction(transaction, files, list, Filelist);
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                        }
                    }//FileCollection Count End                    
                }//HasUpload File End
            }//ImpementationScheduleOnDetails Is null End
            return flag;
        }
        public bool UpdateAuditImplementationTransactionWithFile(AuditImpementationScheduleOn ImpementationScheduleOnDetails, AuditImplementationTransaction IAT, long customerID, long AuditID)
        {
            try
            {
                bool flag = false;
                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["ResultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (ImpementationScheduleOnDetails != null)
                {
                    if (fuIMPWorkingUpload.HasFile)
                    {
                        HttpFileCollection fileCollection1 = Request.Files;
                        if (fileCollection1.Count > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<ImplementationFileData_Risk> files = new List<ImplementationFileData_Risk>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            var InstanceData = RiskCategoryManagement.GetAuditImplementationInstanceData(ImpementationScheduleOnDetails.ForMonth, Convert.ToInt32(ImpementationScheduleOnDetails.ImplementationInstance), Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, AuditID);
                            string directoryPath = "";
                            if (!string.IsNullOrEmpty(InstanceData.ForPeriod) && InstanceData.CustomerBranchID != -1)
                            {
                                directoryPath = Server.MapPath("~/ImplementationWorkingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + ddlFinancialYear.SelectedItem.Text + "/" + InstanceData.ForPeriod.ToString() + "/" + ImpementationScheduleOnDetails.Id + "/1.0");
                            }
                            if (!File.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }
                            for (int i = 0; i < fileCollection1.Count; i++)
                            {
                                HttpPostedFile uploadfile = fileCollection1[i];
                                string[] keys = fileCollection1.Keys[i].Split('$');
                                string fileName = "";
                                if (keys[keys.Count() - 1].Equals("fuIMPWorkingUpload"))
                                {
                                    fileName = uploadfile.FileName;
                                }
                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                if (uploadfile.ContentLength > 0)
                                {
                                    ImplementationFileData_Risk file = new ImplementationFileData_Risk()
                                    {
                                        Name = fileName,
                                        FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                        FileKey = fileKey.ToString(),
                                        Version = "1.0",
                                        VersionDate = DateTime.UtcNow,
                                        ForPeriod = ImpementationScheduleOnDetails.ForMonth,
                                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                        FinancialYear = ImpementationScheduleOnDetails.FinancialYear,
                                        IsDeleted = false,
                                        ResultId = ResultID,
                                        VerticalID = VerticalID,
                                        AuditID = AuditID,
                                        UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        UpdatedOn = DateTime.Now,
                                    };
                                    files.Add(file);
                                }
                            }
                            if (Filelist != null && list != null)
                                flag = UserManagementRisk.UpdateAuditImplementationTransaction(IAT, files, list, Filelist);
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                            }
                        }//FileCollection Count End                    
                    }//HasUpload File End
                }//ImpementationScheduleOnDetails Is null End
                return flag;
            }
            catch (Exception)
            {
                return false;
            }
        }
        protected bool CreateAuditImplementationTransactionWithoutFile(AuditImplementationTransaction transaction)
        {
            bool flag = false;
            flag = UserManagementRisk.CreateAuditImplementationTransaction(transaction);
            return flag;
        }

        private void BindTransactionsIMP(int ScheduledOnID, int ResultID, int AuditID)
        {
            try
            {
                grdTransactionIMPHistory.DataSource = Business.DashboardManagementRisk.GetAllImplementationAuditTransactionViews(ScheduledOnID, ResultID, AuditID);
                grdTransactionIMPHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindRemarksIMP(string ForPeriod, string FinancialYear, int ScheduledOnID, int ResultID, long AuditID)
        {
            try
            {
                GrdIMPRemark.DataSource = Business.DashboardManagementRisk.GetImplementationReviewAllRemarks(ForPeriod, FinancialYear, ScheduledOnID, ResultID, AuditID);
                GrdIMPRemark.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactionDetailsIMP(int ScheduledOnID, int ImplementationInstanceID, int resultID, string ForPeriod, int CustomerBranchId, string FinancialYear, int VerticalID, int ProcessID, long AuditID)
        {
            try
            {
                //Need to check ScheduledOnID is same or diferent **
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnID = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnID = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                txtIMPNewTimeLine.Text = string.Empty;
                txtIMPNewManagementResponse.Text = string.Empty;
                txtIMPFinalStatusRemark.Text = string.Empty;

                var MstRiskResult = RiskCategoryManagement.GetImplementationAuditResultIMPlementationID(resultID, VerticalID, Convert.ToInt32(ScheduledOnID), AuditID);
                if (MstRiskResult != null)
                {
                    txtIMPFinalStatusRemark.Text = MstRiskResult.FixRemark;
                    txtIMPNewManagementResponse.Text = MstRiskResult.ManagementResponse;
                    txtIMPNewTimeLine.Text = MstRiskResult.TimeLine != null ? MstRiskResult.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.PersonResponsible)))
                    {
                        ddlIMPNewPersonresponsible.SelectedValue = Convert.ToString(MstRiskResult.PersonResponsible);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ImplementationStatus)))
                    {
                        if (MstRiskResult.ImplementationStatus != 0)
                        {
                            ddlIMPFinalStatus.SelectedValue = Convert.ToString(MstRiskResult.ImplementationStatus);
                            if (ddlIMPFinalStatus.SelectedValue == "4" || ddlIMPFinalStatus.SelectedValue == "5")
                            {
                                ddlIMPFinalStatus_SelectedIndexChanged(null, null);
                            }
                        }
                    }
                    var TimeLineHistory = RiskCategoryManagement.GetIMPTimeLineHistory(ResultID, Convert.ToInt32(AuditID), Convert.ToInt32(MstRiskResult.PersonResponsible));
                    grdTimelineHistory.DataSource = TimeLineHistory;
                    grdTimelineHistory.DataBind();
                }

                var getDated = RiskCategoryManagement.GetInternalAuditTransactionbyIDIMP(ScheduledOnID, Convert.ToInt32(resultID), ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, 4, VerticalID, AuditID);
                if (getDated != null)
                {
                    tbxDateIMP.Text = getDated.Dated != null ? getDated.Dated.Value.ToString("dd-MM-yyyy") : null;
                }
                BindDocumentIMP(ScheduledOnID, Convert.ToInt32(resultID), AuditID);
                BindTransactionsIMP(ScheduledOnID, Convert.ToInt32(resultID), Convert.ToInt32(AuditID));
                bool checkPRFlag = false;
                checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(CustomerId, CustomerBranchId);
                var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                var litrole = CustomerManagementRisk.IMPGetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                if (checkPRFlag == true)
                {
                    BindStatusListIMP(7, litrole.ToList(), AuditHeadOrManager);
                }
                else
                {
                    BindStatusListIMP(4, litrole.ToList(), AuditHeadOrManager);
                }
                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    BindRemarksIMP(ForPeriod, ddlFinancialYear.SelectedItem.Text, ScheduledOnID, Convert.ToInt32(resultID), AuditID);
                }
                if (GrdIMPRemark.Rows.Count != 0)
                {
                    divIMPReviewHistory.Visible = true;
                }
                else
                {
                    divIMPReviewHistory.Visible = true;
                    txtIMPFinalStatusRemark.Enabled = true;
                }
                if (string.IsNullOrEmpty(tbxDateIMP.Text.Trim()))
                {
                    tbxDateIMP.Text = DateTime.Now.ToString("dd-MM-yyyy");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void EnableDisableControlsIMP(bool Flag)
        {
            //txtIMPProcessWalkthrough.Enabled = Flag;
            //txtIMPActualWorkDone.Enabled = Flag;
            //txtIMPPopulation.Enabled = Flag;
            //txtIMPSample.Enabled = Flag;

            txtIMPNewManagementResponse.Enabled = false;
            txtIMPFinalStatusRemark.Enabled = Flag;
            ddlIMPNewPersonresponsible.Enabled = Flag;
            //btnIMPUpload.Enabled = Flag;
            //btnIMPNext2.Enabled = Flag;

            //rptIMPComplianceDocumnets.Enabled = Flag;
            txtIMPFinalStatusRemark.Enabled = Flag;
            ddlIMPFinalStatus.Enabled = false;
            txtIMPNewTimeLine.Enabled = false;

            btnIMPNext3.Enabled = Flag;
            btnIMPSave.Enabled = Flag;
            txtIMPReviewRemark.Enabled = Flag;
        }

        protected void ddlIMPFinalStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlIMPFinalStatus.SelectedValue))
            {
                if (ddlIMPFinalStatus.SelectedValue != "-1")
                {
                    if (ddlIMPFinalStatus.SelectedValue == "5")
                    {
                        TimeLineTr.Visible = true;
                        personresponsile.Visible = false;
                        lblTimeline.InnerText = "Date of Implementation";
                    }
                    else if (ddlIMPFinalStatus.SelectedValue == "3")
                    {
                        TimeLineTr.Visible = true;
                        personresponsile.Visible = false;
                        lblTimeline.InnerText = "Revised Date of Implementation";
                    }
                    else if (ddlIMPFinalStatus.SelectedValue == "4")
                    {
                        TimeLineTr.Visible = false;
                    }
                    else
                    {
                        lblTimeline.InnerText = "TimeLine";
                        TimeLineTr.Visible = true;
                    }
                }
                else
                {
                    lblTimeline.InnerText = "TimeLine";
                }
            }
        }

        protected void btnObservationHistory_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "");
            liFinalStatus.Attributes.Add("class", "");
            liReviewHistory.Attributes.Add("class", "");

            ImplementationView.ActiveViewIndex = 0;
        }

        protected void btnActualTestingWorkDone_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "active");
            liFinalStatus.Attributes.Add("class", "");
            liReviewHistory.Attributes.Add("class", "");

            ImplementationView.ActiveViewIndex = 1;
        }

        protected void btnFinalStatus_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "active");
            liFinalStatus.Attributes.Add("class", "active");
            liReviewHistory.Attributes.Add("class", "");

            ImplementationView.ActiveViewIndex = 0;
        }
        protected void btnReviewHistory_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "");
            liFinalStatus.Attributes.Add("class", "");
            liReviewHistory.Attributes.Add("class", "active");

            ImplementationView.ActiveViewIndex = 1;
        }
        protected void tvImplementation_SelectedNodeChanged(object sender, EventArgs e)
        {
            string ActionCHK = null;
            btnFinalStatus_Click(sender, e);
            ImplementationView.ActiveViewIndex = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
            {
                ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
            }
            else
            {
                ResultID = Convert.ToInt32(ViewState["resultID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
            {
                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
            }
            else
            {
                VerticalID = Convert.ToInt32(ViewState["VID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
            {
                ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
            }
            else
            {
                ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ActionCHK"]))
            {
                ActionCHK = Request.QueryString["ActionCHK"].ToString();
            }
            else
            {
                ActionCHK = Convert.ToString(ViewState["ActionCHK"]);
            }

            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            if (ddlFinancialYear.SelectedValue != "-1")
                            {
                                if (ddlPeriod.SelectedValue != "0")
                                {
                                    var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, ScheduledOnId, AuditID);
                                    if (getauditimplementationscheduledetails != null)
                                    {
                                        BindTransactionDetailsIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), Convert.ToInt32(getauditimplementationscheduledetails.ImplementationInstance), ResultID, getauditimplementationscheduledetails.ForMonth, Convert.ToInt32(ddlFilterLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text, VerticalID, Convert.ToInt32(getauditimplementationscheduledetails.ProcessId), AuditID);
                                    }
                                    TDIMPTab.Visible = true;
                                    btnFinalStatus_Click(sender, e);
                                    var MstRiskResult = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(ResultID, VerticalID, AuditID);
                                    if (ddlFilterStatus.SelectedValue == "6")
                                    {
                                        if (MstRiskResult.AuditeeResponse == "AS" || MstRiskResult.AuditeeResponse == "AR" || MstRiskResult.AuditeeResponse == "R2")
                                        {
                                            EnableDisableControlsIMP(true);
                                        }
                                        else
                                        {
                                            EnableDisableControlsIMP(false);
                                        }
                                    }
                                    else
                                    {
                                        EnableDisableControlsIMP(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            long ProcessID = ProcessManagement.GetProcessIDByResultID(ResultID);
            var RoleListOfUser = CustomerManagementRisk.GetListOfRoleInAuditIMp(AuditID, Portal.Common.AuthenticationHelper.UserID, ProcessID);
            if (RoleListOfUser.Contains(4) || RoleListOfUser.Contains(5) || RoleListOfUser.Count == 0)
            {
                EnableDisableControlsIMP(false);
            }
        }
        protected void btnIMPUpload_Click(object sender, EventArgs e)
        {
            try
            {
                long roleid = 3;
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                var ImpementationScheduleOnDetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, ScheduledOnId, AuditID);
                if (ImpementationScheduleOnDetails != null)
                {
                    #region                   
                    ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                    {
                        AuditScheduleOnID = ImpementationScheduleOnDetails.Id,
                        FinancialYear = ImpementationScheduleOnDetails.FinancialYear,
                        ForPeriod = ImpementationScheduleOnDetails.ForMonth,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ResultID = ResultID,
                        RoleID = roleid,
                        ImplementationInstance = ImpementationScheduleOnDetails.ImplementationInstance,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(ImpementationScheduleOnDetails.ProcessId),
                        AuditID = AuditID,
                    };
                    AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                    {
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        ImplementationScheduleOnID = ImpementationScheduleOnDetails.Id,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        ImplementationInstance = ImpementationScheduleOnDetails.ImplementationInstance,
                        ForPeriod = ddlPeriod.SelectedItem.Text,
                        ResultID = ResultID,
                        RoleID = roleid,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(ImpementationScheduleOnDetails.ProcessId),
                        AuditID = AuditID,
                    };
                    //if (txtIMPProcessWalkthrough.Text.Trim() == "")
                    //    MstRiskResult.ProcessWalkthrough = "";
                    //else
                    //    MstRiskResult.ProcessWalkthrough = txtIMPProcessWalkthrough.Text.Trim();

                    //if (txtIMPActualWorkDone.Text.Trim() == "")
                    //    MstRiskResult.ActualWorkDone = "";
                    //else
                    //    MstRiskResult.ActualWorkDone = txtIMPActualWorkDone.Text.Trim();

                    //if (txtIMPPopulation.Text.Trim() == "")
                    //    MstRiskResult.Population = "";
                    //else
                    //    MstRiskResult.Population = txtIMPPopulation.Text.Trim();

                    //if (txtIMPSample.Text.Trim() == "")
                    //    MstRiskResult.Sample = "";
                    //else
                    //    MstRiskResult.Sample = txtIMPSample.Text.Trim();

                    if (txtIMPNewManagementResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtIMPNewManagementResponse.Text.Trim();

                    if (txtIMPFinalStatusRemark.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = txtIMPFinalStatusRemark.Text.Trim();

                    if (ddlIMPNewPersonresponsible.SelectedValue == "-1")
                    {
                        MstRiskResult.PersonResponsible = null;
                        transaction.PersonResponsible = null;
                    }
                    else
                    {
                        MstRiskResult.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                        transaction.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                    }
                    int checkStatusId = -1;
                    if (ddlFilterStatus.SelectedValue != "")
                    {
                        if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4)
                        {
                            checkStatusId = 4;
                        }
                        else
                        {
                            checkStatusId = 2;
                        }
                    }
                    if (checkStatusId != -1)
                    {
                        MstRiskResult.Status = checkStatusId;
                    }
                    if (ddlIMPFinalStatus.SelectedValue == "-1")
                    {
                        MstRiskResult.ImplementationStatus = null;
                    }
                    else
                    {
                        MstRiskResult.ImplementationStatus = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                    }
                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtIMPNewTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;


                    bool Success1 = false;
                    bool Success2 = false;

                    if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                    {
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                    }
                    #endregion

                    if (RiskCategoryManagement.AuditImplementationTransactionExists(transaction))
                    {
                        transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        transaction.UpdatedOn = DateTime.Now;
                        Success2 = UpdateAuditImplementationTransactionWithFile(ImpementationScheduleOnDetails, transaction, CustomerId, AuditID);
                    }
                    else
                    {
                        transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        transaction.CreatedOn = DateTime.Now;
                        // Success2 = CreateAuditImplementationTransaction(ImpementationScheduleOnDetails, transaction, CustomerId, AuditID);
                    }
                    if (Success1 && Success2)
                    {
                        BindDocumentIMP(Convert.ToInt32(ImpementationScheduleOnDetails.Id), VerticalID, AuditID);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }//  ImpementationScheduleOnDetails end
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnIMPNext2_Click(object sender, EventArgs e)
        {
            try
            {
                long roleid = 3;
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, ScheduledOnId, AuditID);
                if (getauditimplementationscheduledetails != null)
                {
                    ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                    {
                        AuditScheduleOnID = getauditimplementationscheduledetails.Id,
                        FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                        ForPeriod = getauditimplementationscheduledetails.ForMonth,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ResultID = ResultID,
                        RoleID = roleid,
                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                        AuditID = AuditID,
                    };
                    AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                    {
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                        ForPeriod = ddlPeriod.SelectedItem.Text,
                        ResultID = ResultID,
                        RoleID = roleid,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                        AuditID = AuditID,
                    };


                    //if (txtIMPProcessWalkthrough.Text.Trim() == "")
                    //    MstRiskResult.ProcessWalkthrough = "";
                    //else
                    //    MstRiskResult.ProcessWalkthrough = txtIMPProcessWalkthrough.Text.Trim();

                    //if (txtIMPActualWorkDone.Text.Trim() == "")
                    //    MstRiskResult.ActualWorkDone = "";
                    //else
                    //    MstRiskResult.ActualWorkDone = txtIMPActualWorkDone.Text.Trim();

                    //if (txtIMPPopulation.Text.Trim() == "")
                    //    MstRiskResult.Population = "";
                    //else
                    //    MstRiskResult.Population = txtIMPPopulation.Text.Trim();

                    //if (txtIMPSample.Text.Trim() == "")
                    //    MstRiskResult.Sample = "";
                    //else
                    //    MstRiskResult.Sample = txtIMPSample.Text.Trim();

                    if (txtIMPNewManagementResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtIMPNewManagementResponse.Text.Trim();

                    if (txtIMPFinalStatusRemark.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = txtIMPFinalStatusRemark.Text.Trim();

                    if (ddlIMPNewPersonresponsible.SelectedValue == "-1")
                    {
                        MstRiskResult.PersonResponsible = null;
                        transaction.PersonResponsible = null;
                    }
                    else
                    {
                        MstRiskResult.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                        transaction.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                    }
                    int checkStatusId = -1;
                    if (ddlFilterStatus.SelectedValue != "")
                    {
                        if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4)
                        {
                            checkStatusId = 4;
                        }
                        else
                        {
                            checkStatusId = 2;
                        }
                    }
                    if (checkStatusId != -1)
                    {
                        MstRiskResult.Status = checkStatusId;
                    }
                    if (ddlIMPFinalStatus.SelectedValue == "-1")
                    {
                        MstRiskResult.ImplementationStatus = null;
                    }
                    else
                    {
                        MstRiskResult.ImplementationStatus = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                    }
                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtIMPNewTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    bool Success1 = false;
                    bool Success2 = false;

                    if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                    {
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                    }

                    if (RiskCategoryManagement.AuditImplementationTransactionExists(transaction))
                    {
                        transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        transaction.UpdatedOn = DateTime.Now;
                        Success2 = RiskCategoryManagement.UpdateAuditImplementationTransaction(transaction);
                    }
                    else
                    {
                        transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        transaction.CreatedOn = DateTime.Now;
                        Success2 = CreateAuditImplementationTransactionWithoutFile(transaction);
                    }
                    if (Success1 && Success2)
                    {
                        btnFinalStatus_Click(sender, e);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnIMPNext3_Click(object sender, EventArgs e)
        {
            try
            {
                long roleid = 3;
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (ddlIMPFinalStatus.SelectedValue != "-1")
                {
                    var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, ScheduledOnId, AuditID);
                    if (getauditimplementationscheduledetails != null)
                    {
                        ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                        {
                            AuditScheduleOnID = getauditimplementationscheduledetails.Id,
                            FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                            ForPeriod = getauditimplementationscheduledetails.ForMonth,
                            CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                            IsDeleted = false,
                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            ResultID = ResultID,
                            RoleID = roleid,
                            ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                            VerticalID = VerticalID,
                            ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                            AuditID = AuditID,
                        };

                        AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                        {
                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                            ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                            FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                            CustomerBranchId = BranchId,
                            ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                            ForPeriod = getauditimplementationscheduledetails.ForMonth,
                            ResultID = ResultID,
                            RoleID = roleid,
                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            VerticalID = VerticalID,
                            ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                            AuditID = AuditID,
                        };

                        //if (txtIMPProcessWalkthrough.Text.Trim() == "")
                        //    MstRiskResult.ProcessWalkthrough = "";
                        //else
                        //    MstRiskResult.ProcessWalkthrough = txtIMPProcessWalkthrough.Text.Trim();

                        //if (txtIMPActualWorkDone.Text.Trim() == "")
                        //    MstRiskResult.ActualWorkDone = "";
                        //else
                        //    MstRiskResult.ActualWorkDone = txtIMPActualWorkDone.Text.Trim();

                        //if (txtIMPPopulation.Text.Trim() == "")
                        //    MstRiskResult.Population = "";
                        //else
                        //    MstRiskResult.Population = txtIMPPopulation.Text.Trim();

                        //if (txtIMPSample.Text.Trim() == "")
                        //    MstRiskResult.Sample = "";
                        //else
                        //    MstRiskResult.Sample = txtIMPSample.Text.Trim();

                        if (txtIMPNewManagementResponse.Text.Trim() == "")
                            MstRiskResult.ManagementResponse = "";
                        else
                            MstRiskResult.ManagementResponse = txtIMPNewManagementResponse.Text.Trim();

                        if (txtIMPFinalStatusRemark.Text.Trim() == "")
                        {
                            MstRiskResult.FixRemark = "";
                            transaction.AuditorRemark = "";
                        }
                        else
                        {
                            MstRiskResult.FixRemark = txtIMPFinalStatusRemark.Text.Trim();
                            transaction.AuditorRemark = txtIMPFinalStatusRemark.Text.Trim();
                        }

                        if (ddlIMPNewPersonresponsible.SelectedValue == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                            transaction.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                            transaction.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                        }
                        if (ddlIMPFinalStatus.SelectedValue == "-1")
                        {
                            MstRiskResult.ImplementationStatus = null;
                            transaction.ImplementationStatusId = null;
                        }
                        else
                        {
                            MstRiskResult.ImplementationStatus = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                            transaction.ImplementationStatusId = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                        }

                        DateTime dt = new DateTime();
                        if (!string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                        {
                            dt = DateTime.ParseExact(txtIMPNewTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.TimeLine = dt.Date;
                        }
                        else
                        {
                            MstRiskResult.TimeLine = null;
                        }
                        var ResponstStatus = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(ResultID, VerticalID, AuditID);
                        if (ResponstStatus != null)
                        {
                            transaction.AuditeeResponse = ResponstStatus.AuditeeResponse;
                        }
                        int checkStatusId = -1;
                        if (ddlFilterStatus.SelectedValue != "")
                        {
                            if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4)
                            {
                                checkStatusId = 4;
                            }
                            else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 5)
                            {
                                checkStatusId = 5;
                            }
                            else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6)
                            {
                                checkStatusId = 6;
                            }
                            else
                            {
                                checkStatusId = 2;
                            }
                        }
                        if (checkStatusId != -1)
                        {
                            MstRiskResult.Status = checkStatusId;
                            transaction.StatusId = checkStatusId;
                        }
                        bool Success1 = false;

                        BindRemarksIMP(Convert.ToString(getauditimplementationscheduledetails.ForMonth), ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, AuditID);

                        if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                        {
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                        }
                        else
                        {
                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                        }
                        transaction.StatusId = 6;
                        transaction.Remarks = "Implementation Step Under Auditee Review.";
                        transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        transaction.CreatedOn = DateTime.Now;
                        if (Success1)
                        {
                            CreateAuditImplementationTransaction(getauditimplementationscheduledetails, transaction, CustomerId, AuditID);
                            BindDocumentIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, Convert.ToInt32(AuditID));
                            BindTransactionsIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, Convert.ToInt32(AuditID));
                            btnReviewHistory_Click(sender, e);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Implementation Status";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnIMPSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool Flag = false;
                long roleid = 3;
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, ScheduledOnId, AuditID);
                if (getauditimplementationscheduledetails != null)
                {
                    if (ddlFilterStatus.SelectedValue != "" && ddlFilterStatus.SelectedValue != "-1")
                    {
                        if (ddlIMPFinalStatus.SelectedValue != "-1" && ddlIMPFinalStatus.SelectedValue != "")
                        {
                            if (rdbtnStatusNEW.SelectedValue != "-1" && rdbtnStatusNEW.SelectedValue != "")
                            {
                                string remark = string.Empty;
                                int checkStatusId = -1;
                                if (Convert.ToInt32(rdbtnStatusNEW.SelectedValue) == 6)
                                {
                                    checkStatusId = 6;
                                    remark = "Implementation Step Under Audittee Review";
                                }
                                else if (Convert.ToInt32(rdbtnStatusNEW.SelectedValue) == 3)
                                {
                                    checkStatusId = 3;
                                }
                                else
                                {
                                    checkStatusId = 2;
                                    remark = "Implementation Step Submitted.";
                                }
                                ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                                {
                                    AuditScheduleOnID = getauditimplementationscheduledetails.Id,
                                    FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                                    ForPeriod = getauditimplementationscheduledetails.ForMonth,
                                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                    IsDeleted = false,
                                    UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    ResultID = ResultID,
                                    RoleID = roleid,
                                    ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                    Status = checkStatusId,
                                    VerticalID = VerticalID,
                                    ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                    AuditID = AuditID,
                                };
                                AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                                {
                                    StatusId = checkStatusId,
                                    CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                    StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                                    ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                    FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                    ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                    ForPeriod = ddlPeriod.SelectedItem.Text,
                                    ResultID = ResultID,
                                    RoleID = roleid,
                                    UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    VerticalID = VerticalID,
                                    ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                    AuditID = AuditID,
                                };

                                var ResponstStatus = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(ResultID, VerticalID, AuditID);
                                if (checkStatusId == 6)
                                {
                                    transaction.AuditeeResponse = "PA";
                                }
                                else if (checkStatusId == 3)
                                {
                                    if (ddlIMPFinalStatus.SelectedValue == "3" || ddlIMPFinalStatus.SelectedValue == "2")
                                    {
                                        transaction.AuditeeResponse = null;
                                    }
                                    else
                                    {
                                        transaction.AuditeeResponse = "AC";
                                    }
                                }
                                else
                                {
                                    if (ResponstStatus != null)
                                    {
                                        if (ResponstStatus.AuditeeResponse == "R2")
                                        {
                                            transaction.AuditeeResponse = "2R";
                                        }
                                        else
                                        {
                                            transaction.AuditeeResponse = "PS";
                                        }
                                    }
                                    else
                                    {
                                        transaction.AuditeeResponse = "PS";
                                    }
                                }

                                //if (txtIMPProcessWalkthrough.Text.Trim() == "")
                                //    MstRiskResult.ProcessWalkthrough = "";
                                //else
                                //    MstRiskResult.ProcessWalkthrough = txtIMPProcessWalkthrough.Text.Trim();

                                //if (txtIMPActualWorkDone.Text.Trim() == "")
                                //    MstRiskResult.ActualWorkDone = "";
                                //else
                                //    MstRiskResult.ActualWorkDone = txtIMPActualWorkDone.Text.Trim();

                                //if (txtIMPPopulation.Text.Trim() == "")
                                //    MstRiskResult.Population = "";
                                //else
                                //    MstRiskResult.Population = txtIMPPopulation.Text.Trim();

                                //if (txtIMPSample.Text.Trim() == "")
                                //    MstRiskResult.Sample = "";
                                //else
                                //    MstRiskResult.Sample = txtIMPSample.Text.Trim();

                                if (txtIMPNewManagementResponse.Text.Trim() == "")
                                    MstRiskResult.ManagementResponse = "";
                                else
                                    MstRiskResult.ManagementResponse = txtIMPNewManagementResponse.Text.Trim();

                                if (txtIMPFinalStatusRemark.Text.Trim() == "")
                                {
                                    MstRiskResult.FixRemark = "";
                                    transaction.AuditorRemark = "";
                                }
                                else
                                {
                                    MstRiskResult.FixRemark = txtIMPFinalStatusRemark.Text.Trim();
                                    transaction.AuditorRemark = txtIMPFinalStatusRemark.Text.Trim();
                                }

                                if (ddlIMPNewPersonresponsible.SelectedValue == "-1")
                                {
                                    MstRiskResult.PersonResponsible = null;
                                    transaction.PersonResponsible = null;
                                }
                                else
                                {
                                    MstRiskResult.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                                    transaction.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                                }
                                if (ddlIMPFinalStatus.SelectedValue == "-1")
                                {
                                    MstRiskResult.ImplementationStatus = null;
                                    transaction.ImplementationStatusId = null;
                                }
                                else
                                {
                                    MstRiskResult.ImplementationStatus = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                                    transaction.ImplementationStatusId = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                                }
                                DateTime dt = new DateTime();
                                if (!string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                                {
                                    dt = DateTime.ParseExact(txtIMPNewTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    MstRiskResult.TimeLine = dt.Date;
                                }
                                else
                                    MstRiskResult.TimeLine = null;
                                transaction.Remarks = remark.Trim();
                                bool Success1 = false;
                                bool Success2 = false;

                                if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                                {
                                    MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    MstRiskResult.UpdatedOn = DateTime.Now;
                                    Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                                }
                                else
                                {
                                    MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    MstRiskResult.CreatedOn = DateTime.Now;
                                    Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                }
                                if (RiskCategoryManagement.AuditImplementationTransactionExists(transaction))
                                {
                                    transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    transaction.UpdatedOn = DateTime.Now;
                                    Success2 = RiskCategoryManagement.UpdateAuditImplementationTransactionStatus(transaction);
                                }
                                else
                                {
                                    transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    transaction.CreatedOn = DateTime.Now;
                                    Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                                }
                                string Testremark = "";
                                if (string.IsNullOrEmpty(txtIMPReviewRemark.Text))
                                {
                                    Testremark = "NA";
                                }
                                else
                                {
                                    Testremark = txtIMPReviewRemark.Text.Trim();
                                }
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                HttpFileCollection fileCollection1 = Request.Files;
                                if (fileCollection1.Count > 0)
                                {
                                    var InstanceData = RiskCategoryManagement.GetAuditImplementationInstanceData(getauditimplementationscheduledetails.ForMonth, Convert.ToInt32(getauditimplementationscheduledetails.ImplementationInstance), Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, AuditID);
                                    string directoryPath = "";
                                    if (!string.IsNullOrEmpty(InstanceData.ForPeriod) && InstanceData.CustomerBranchID != -1)
                                    {
                                        directoryPath = Server.MapPath("~/ImplementationAdditionalDocument/" + CustomerId + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + ddlFinancialYear.SelectedItem.Text + "/" + InstanceData.ForPeriod.ToString() + "/" + getauditimplementationscheduledetails.Id + "/1.0");
                                    }
                                    DocumentManagement.CreateDirectory(directoryPath);
                                    for (int i = 0; i < fileCollection1.Count; i++)
                                    {
                                        HttpPostedFile uploadfile1 = fileCollection1[i];
                                        string[] keys1 = fileCollection1.Keys[i].Split('$');
                                        String fileName1 = "";
                                        if (keys1[keys1.Count() - 1].Equals("ReviewFileUpload"))
                                        {
                                            fileName1 = uploadfile1.FileName;
                                        }
                                        Guid fileKey1 = Guid.NewGuid();
                                        string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadfile1.FileName));
                                        Stream fs = uploadfile1.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                        if (uploadfile1.ContentLength > 0)
                                        {
                                            ImplementationReviewHistory RH = new ImplementationReviewHistory()
                                            {
                                                ResultID = ResultID,
                                                ForMonth = Convert.ToString(getauditimplementationscheduledetails.ForMonth),
                                                ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                                CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                                Dated = DateTime.Now,
                                                Remarks = Testremark,
                                                ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                                FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                                                CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                                Name = fileName1,
                                                FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                                FileKey = fileKey1.ToString(),
                                                Version = "1.0",
                                                VersionDate = DateTime.UtcNow,
                                                FixRemark = txtIMPFinalStatusRemark.Text.Trim(),
                                                VerticalID = VerticalID,
                                                ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                                CreatedOn = DateTime.Now,
                                                AuditID = AuditID,
                                            };
                                            Flag = RiskCategoryManagement.CreateImplementationReviewHistoryRemark(RH);
                                            DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                            Label2.Text = "";
                                            Label1.Text = "";
                                        }
                                        else
                                        {
                                            ImplementationReviewHistory RH = new ImplementationReviewHistory()
                                            {
                                                ForMonth = Convert.ToString(getauditimplementationscheduledetails.ForMonth),
                                                ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                                CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                                Dated = DateTime.Now,
                                                Remarks = Testremark,
                                                ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                                FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                                                CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                                ResultID = ResultID,
                                                FixRemark = txtIMPFinalStatusRemark.Text.Trim(),
                                                VerticalID = VerticalID,
                                                ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                                CreatedOn = DateTime.Now,
                                                AuditID = AuditID,
                                            };
                                            Flag = RiskCategoryManagement.CreateImplementationReviewHistoryRemark(RH);
                                        }
                                    }
                                }
                                else
                                {
                                    ImplementationReviewHistory RH = new ImplementationReviewHistory()
                                    {
                                        ForMonth = Convert.ToString(getauditimplementationscheduledetails.ForMonth),
                                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                        Dated = DateTime.Now,
                                        Remarks = Testremark,
                                        ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                        FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                        ResultID = ResultID,
                                        FixRemark = txtIMPFinalStatusRemark.Text.Trim(),
                                        VerticalID = VerticalID,
                                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                        CreatedOn = DateTime.Now,
                                        AuditID = AuditID,
                                    };
                                    Flag = RiskCategoryManagement.CreateImplementationReviewHistoryRemark(RH);
                                }
                                BindRemarksIMP(Convert.ToString(getauditimplementationscheduledetails.ForMonth), getauditimplementationscheduledetails.FinancialYear, Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, AuditID);
                                if (Success1 == true && Success2 == true && Flag == true)
                                {
                                    if (ddlFilterStatus.SelectedValue == "1")
                                    {
                                        btnObservationHistory.Enabled = false;
                                        btnActualTestingWorkDone.Enabled = false;
                                        btnFinalStatus.Enabled = false;
                                        btnReviewHistory.Enabled = false;
                                    }
                                    else
                                    {
                                        btnObservationHistory.Enabled = true;
                                        btnActualTestingWorkDone.Enabled = true;
                                        btnFinalStatus.Enabled = true;
                                        btnReviewHistory.Enabled = true;
                                    }

                                    BindTransactionsIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, Convert.ToInt32(AuditID));

                                    txtIMPReviewRemark.Text = string.Empty;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Implementation Step Submitted Successfully";
                                    EnableDisableControlsIMP(false);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Select Implementation Status";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Status";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Status";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void rptIMPComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (e.CommandName.Equals("DownloadIMP"))
                {
                    DownloadFileIMP(Convert.ToInt32(e.CommandArgument), AuditID);
                }
                else if (e.CommandName.Equals("DeleteIMP"))
                {
                    DeleteFileIMP(Convert.ToInt32(e.CommandArgument), AuditID);
                    BindDocumentIMP(ScheduledOnId, ResultID, AuditID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptIMPComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnetsIMP");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbuttonIMP");
            if (lbtLinkDocbutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
            }
        }
        protected void grdTransactionIMPHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                grdTransactionIMPHistory.PageIndex = e.NewPageIndex;
                BindTransactionsIMP(ScheduledOnId, ResultID, Convert.ToInt32(AuditID));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void GrdIMPRemark_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["ResultID"]);
                }
                GrdIMPRemark.PageIndex = e.NewPageIndex;
                BindRemarksIMP(Period, ddlFinancialYear.SelectedItem.Text, ScheduledOnId, ResultID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploaded";
            }
            return processnonprocess;
        }

        protected void btnDownloadIMP_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                var file = Business.RiskCategoryManagement.GetFileImplementationFileData_Risk(Convert.ToInt32(hdlSelectedDocumentIDIMP.Value), AuditID);

                Response.Buffer = true;
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #endregion

        public string ShowSampleDocumentNameView(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "/View";
            }
            return processnonprocess;
        }
        protected void lblViewFile_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            Label lblID = (Label)gvr.FindControl("lblID");
            Label lblriskID = (Label)gvr.FindControl("lblriskID");
            Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
            Label lblATBDId = (Label)gvr.FindControl("lblATBDId");

            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            List<ImplementationReviewHistory> fileData = GetFileDataIMP(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text), AuditID);

            foreach (var file in fileData)
            {
                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                if (file.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles";
                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + FileData;

                    string extension = System.IO.Path.GetExtension(filePath);

                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                    }

                    string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    if (file.EnType == "M")
                    {
                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    else
                    {
                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    bw.Close();

                    string CompDocReviewPath = FileName;
                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestViewIMPPerformer('" + CompDocReviewPath + "');", true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                }
                break;
            }
        }

        protected void btnUploadAuditorfile_Click(object sender, EventArgs e)
        {

        }

        protected void grdTimelineHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
    }
}