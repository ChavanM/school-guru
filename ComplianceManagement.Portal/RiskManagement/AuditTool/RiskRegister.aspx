﻿<%@ Page Title="Risk Register" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="RiskRegister.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.RiskRegister" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .btn1 {
            display: inline-block; /* padding: 6px 12px; */
            margin-bottom: 0;
            cursor: pointer;
            white-space: nowrap;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -o-user-select: none;
            user-select: none;
            font-weight: 300;
            -webkit-transition: all .15s;
            -moz-transition: all .15s;
            transition: all .15s;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
        }
    </style>
    	
    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

      


        function initializeCombobox() {
            $("#<%= ddlFilterLocation.ClientID %>").combobox();
        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
        $(document).ready(function () {
            setactivemenu('Risk Register');
            fhead('Risk Register');
           
        });
        function openModal() {
            $('#ComplaincePerformer').modal('show');
            return true;
        }
        function CloseWin() {
            $('#ComplaincePerformer').modal('hide');
        };
    </script>
    <style type="text/css">
        .td1 {
            width: 10%;
        }

        .td2 {
            width: 23%;
        }

        .td3 {
            width: 10%;
        }

        .td4 {
            width: 23%;
        }

        .td5 {
            width: 10%;
        }

        .td6 {
            width: 23%;
        }
        
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style>
      

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
    <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                            <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ForeColor="Red" 
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                           </div>  
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <div class="col-md-2 colpadding0" >
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                                        <asp:ListItem Text="5" Selected="True"/>
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                        </asp:DropDownList>                                     
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px; color:#999;">     
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" Style="margin-left: 0px"
                                    RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged">
                                    <asp:ListItem Text="Process" Value="Process" Selected="True" />
                                    <asp:ListItem Text="Others" Value="Others" style="margin-left:20px"/>
                                    </asp:RadioButtonList>              
                                </div> 
                                <div style="float:right;">         
                                    <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-search" CausesValidation="false"  OnClick="lbtnExportExcel_Click" runat="server"/>
                                </div                              
                            </div>                                                       
                            <div class="clearfix"></div>                                          
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="80%" Height="32px"
                                    AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="80%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="80%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit 2">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="80%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit 3">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>                               
                            <div class="clearfix"></div>
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0" style="margin-top:5px;">                                 
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Location"
                                    OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" Width="80%" Height="32px" class="form-control m-bot15">
                                    </asp:DropDownListChosen>                          
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">                               
                                    <asp:DropDownListChosen ID="ddlFilterProcess" runat="server" AutoPostBack="true"  DataPlaceHolder="Process" class="form-control m-bot15"   Width="80%" Height="32px"
                                    OnSelectedIndexChanged="ddlFilterProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlFilterProcess" ID="rfvProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">                                  
                                    <asp:DropDownListChosen ID="ddlFilterSubProcess" runat="server" AutoPostBack="true" DataPlaceHolder="Sub Process" class="form-control m-bot15"  Width="80%" Height="32px"
                                    OnSelectedIndexChanged="ddlFilterSubProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlFilterSubProcess" ID="rfvSubProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />  
                                </div>                               
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">                                
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocationType" AutoPostBack="true" DataPlaceHolder="Location Type"  class="form-control m-bot15"  Width="80%" Height="32px"
                                    OnSelectedIndexChanged="ddlFilterLocationType_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                  
                                </div>                                                                                              
                            </div>                                                                                         
                            <div class="clearfix"></div>   
                            <div>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                            <div class="col-lg-12 col-md-12 " style="overflow:auto">
                            <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" AutoPostBack="true" AllowSorting="true"
                            PageSize="5" AllowPaging="true" CssClass="table" GridLines="none" Width="100%"
                            OnRowDataBound="grdRiskActivityMatrix_RowDataBound" 
                            DataKeyNames="Id" OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging" OnRowCommand="grdRiskActivityMatrix_RowCommand">
                            <Columns>                                                        
                            <asp:TemplateField HeaderText="Sr">
                            <ItemTemplate>
                            <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="Control&nbsp;#">
                            <ItemTemplate>
                            <asp:Label ID="lblControlNo" runat="server" Text='<%# Eval("ControlNo") %>'></asp:Label>
                            </ItemTemplate>                                                                              
                            </asp:TemplateField>
                            <asp:TemplateField  HeaderText="Process&nbsp;Name">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                            <asp:Label ID="ItemTemplateProcessName" runat="server" Text='<%# ShowProcessName((long)Eval("ProcessId")) %>'></asp:Label>
                            </div> 
                            </ItemTemplate>                                        
                            </asp:TemplateField>
                            <asp:TemplateField  HeaderText="SubProcess&nbsp;Name">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                            <asp:Label ID="ItemTemplateSubProcessName" runat="server" Text='<%# ShowSubProcessName((long)Eval("ProcessId"),(long)Eval("SubProcessId")) %>'></asp:Label>
                            </div> 
                            </ItemTemplate>                                       
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Risk Description">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                            <asp:Label ID="lblActivityDescription" runat="server" Text='<%# Eval("ActivityDescription") %>'  data-toggle="tooltip" data-placement="bottom"   ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                            </div> 
                            </ItemTemplate>                                       
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Control Objective" >
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                            <asp:Label ID="lblControlObjective" runat="server" Text='<%# Eval("ControlObjective") %>'  data-toggle="tooltip" data-placement="bottom"   ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                            </div>
                            </ItemTemplate>                                       
                            </asp:TemplateField>
                           
                            <asp:TemplateField HeaderText="LocationType">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                            <asp:Label ID="lblLocationType" runat="server" Text='<%# ShowLocationType((int)Eval("LocationType")) %>'></asp:Label>
                            </div>  
                            </ItemTemplate>                                       
                            </asp:TemplateField>
                                        
                            <asp:TemplateField>                                          
                            <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_RISKREGISTER" CommandArgument='<%# Eval("RiskCreationId") %>' OnClientClick="return openModal()"  CausesValidation="false"><img src="../../Images/edit_icon_new.png" alt="Display Rating" title="Display Rating"  /></asp:LinkButton>                                                                   
                            </ItemTemplate>                                       
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>                                                                                                                                             
                            </asp:GridView>     
                            </div>
                            <div class="col-md-5 colpadding0">
                            <div class="table-Selecteddownload">
                            <div class="table-Selecteddownload-text">
                            <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                            </div>                                   
                            </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right;">
                            <div class="table-paging" style="margin-bottom: 10px;">
                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                            <div class="table-paging-text">
                            <p>
                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                            </p>
                            </div>
                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                            </div>                                                                                                                                                                                                         
                            </ContentTemplate>
                            </asp:UpdatePanel>                                                                      
                            </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>





    <%--/////////////////////////////PopUp code Start//////////////////--%>

    <div class="modal fade" id="ComplaincePerformer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 34px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-body">

                            <table style="width: 100%">
                                <tr>
                                    <td colspan="4">
                                        <div style="margin-bottom: 7px; margin-left: 20px;">
                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary"
                                                ValidationGroup="CustomerValidationGroup1" ForeColor="Red" />
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                                ValidationGroup="CustomerValidationGroup1" Display="None" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Control</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxControl" Style="width: 390px;" Enabled="false" CssClass="form-control" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Control number can not be empty."
                                            ControlToValidate="tbxControl" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            ErrorMessage="Please enter a valid name." ControlToValidate="tbxControl" ValidationExpression="^[a-zA-Z_&]+[a-zA-Z0-9&_ .-]*$"></asp:RegularExpressionValidator>
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                            Process</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen ID="ddlProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="100%" Height="32px"
                                            OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlProcess" ID="RequiredFieldValidator9"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                            Sub Process</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="100%" Height="32px"
                                            OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlSubProcess" ID="RequiredFieldValidator10"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Location
                                        </label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlClientProcess" AutoPostBack="true" Width="100%" Height="32px" class="form-control m-bot15"
                                            OnSelectedIndexChanged="ddlClientProcess_SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Location." ControlToValidate="ddlClientProcess"
                                            ID="RequiredFieldValidator12"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                <tr>

                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Risk Description</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxRiskDesc" Style="width: 390px;" CssClass="form-control" MaxLength="500"
                                            TextMode="MultiLine" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Control Objective</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxControlObj" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>

                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Assertions</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownCheckBoxes ID="ddlAssertions" runat="server" Width="200px"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                            OnSelectedIndexChanged="checkBoxesAssertionsProcess_SelcetedIndexChanged">
                                            <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                            <Texts SelectBoxCaption="Select Assertions" />
                                        </asp:DropDownCheckBoxes>
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Risk Category</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownCheckBoxes ID="ddlRiskCategory" runat="server" Width="200px"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                            OnSelectedIndexChanged="checkBoxesRiskCategoryProcess_SelcetedIndexChanged">
                                            <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                            <Texts SelectBoxCaption="Select Risk Category" />
                                        </asp:DropDownCheckBoxes>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Industry</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownCheckBoxes ID="ddlIndultory" runat="server" Width="200px" AutoPostBack="true"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                            OnSelectedIndexChanged="checkBoxesProcess_SelcetedIndexChanged">
                                            <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                            <Texts SelectBoxCaption="Select Industry" />
                                        </asp:DropDownCheckBoxes>
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Location Type</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen ID="ddlLocationType" runat="server" class="form-control m-bot15" Width="100%" Height="32px"></asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Location Type." ControlToValidate="ddlLocationType"
                                            ID="RequiredFieldValidator11"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button Text="Save" runat="server" ID="btnSavePop" CssClass="btn-search" Style="border: 0px; width: 70px; height: 30px; border-radius: 3px; margin-right: 5px"
                                            ValidationGroup="CustomerValidationGroup1" OnClick="btnSavePop_Click" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn-search" Style="border: 0px; width: 70px; height: 30px; border-radius: 3px;" OnClientClick="$('#ComplaincePerformer').dialog('close');" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                            <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%--/////////////////////////////PopUp code End//////////////////--%>
    <%-- //////////////////GridView used for Export which is not display///////////////////////--%>
    <div style="display: none;">
        <asp:GridView ID="GridExportExcel" runat="server" AutoGenerateColumns="false" AllowPaging="false"
            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" ForeColor="Black">
            <Columns>
                <asp:BoundField DataField="ControlNo" HeaderText="Control No" ItemStyle-Width="150px" />
                <asp:BoundField DataField="RiskCreationId" HeaderText="Task Id" ItemStyle-Width="150px" />
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Process Name">
                    <ItemTemplate>
                        <asp:Label ID="ProcessName" runat="server" Text='<%# ShowProcessName((long)Eval("ProcessId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="SubProcess Name">
                    <ItemTemplate>
                        <asp:Label ID="SubProcessName" runat="server" Text='<%# ShowSubProcessName((long)Eval("ProcessId"),(long)Eval("SubProcessId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Risk Description">
                    <ItemTemplate>
                        <asp:Label ID="lblActivityDescription" runat="server" Text='<%# Eval("ActivityDescription") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Control Objective">
                    <ItemTemplate>
                        <asp:Label ID="lblControlObjective" runat="server" Text='<%# Eval("ControlObjective") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="RiskCategory">
                    <ItemTemplate>
                        <asp:Label ID="lblRiskCategory" runat="server" Text='<%# ShowRiskCategoryName((long)Eval("RiskCreationId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Location">
                    <ItemTemplate>
                        <asp:Label ID="lblClient" runat="server" Text='<%# ShowCustomerBranchName((long)Eval("RiskCreationId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Assertions">
                    <ItemTemplate>
                        <asp:Label ID="lblAssertions" runat="server" Text='<%# ShowAssertionsName((long)Eval("RiskCreationId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ControlDescription" HeaderText="Control Description" ItemStyle-Width="150px" />
                <asp:BoundField DataField="MControlDescription" HeaderText="MitigatingControl Description" ItemStyle-Width="150px" />
                <asp:BoundField DataField="PersonResponsible" HeaderText="Person Responsible" ItemStyle-Width="150px" />
                <asp:BoundField DataField="EffectiveDate" HeaderText="EffectiveDate" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Key" HeaderText="Key" ItemStyle-Width="150px" />
                <asp:BoundField DataField="PreventiveControl" HeaderText="Preventive Control" ItemStyle-Width="150px" />
                <asp:BoundField DataField="AutomatedControl" HeaderText="Automated Control" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Frequency" HeaderText="Frequency" ItemStyle-Width="150px" />
                <asp:BoundField DataField="GapDescription" HeaderText="Gap Description" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Recommendations" HeaderText="Recommendations" ItemStyle-Width="150px" />
                <asp:BoundField DataField="ActionRemediationplan" HeaderText="Action Remediation Plan" ItemStyle-Width="150px" />
                <asp:BoundField DataField="IPE" HeaderText="Information Produced by Entity" ItemStyle-Width="150px" />
                <asp:BoundField DataField="ERPsystem" HeaderText="ERP System" ItemStyle-Width="150px" />
                <asp:BoundField DataField="FRC" HeaderText="Fraud Risk Control" ItemStyle-Width="150px" />
                <asp:BoundField DataField="UniqueReferred" HeaderText="Unique Referred" ItemStyle-Width="150px" />
                <asp:BoundField DataField="TestStrategy" HeaderText="Test Strategy" ItemStyle-Width="150px" />
                <asp:BoundField DataField="DocumentsExamined" HeaderText="Documents Examined" ItemStyle-Width="150px" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
            <PagerSettings Position="Top" />
            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
            <AlternatingRowStyle BackColor="#E6EFF7" />
        </asp:GridView>
    </div>
    <%-- //////////////////END GridView used for Export which is not display///////////////////////--%>
</asp:Content>
