﻿<%@ Page Title="Risk Control Matrix" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="RiskControlMatrix.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.RiskControlMatrix" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeDatePicker(date) {
            var startDate = new Date();
            $('#<%= txtStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1,
            });

            if (date != null) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }

        function openModal() {
            $('#ComplaincePerformer').modal('show');
            return true;
        }

        function CloseWin() {
            $('#ComplaincePerformer').modal('hide');
        };

        $(document).ready(function () {
            //setactivemenu('Risk Control Matrix');
            fhead('Risk Control Matrix');
        });
    </script>
    <style type="text/css">
        .dd_chk_select {
            height: 81px !important;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
            height: 30px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">  
                            <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true" Display="None"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                             </div>

                             <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px; width:25%">
                                    <div class="col-md-3 colpadding0" >
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left" 
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                                    <asp:ListItem Text="5"/>
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" Selected="True"/>
                                    <asp:ListItem Text="100" />
                                    </asp:DropDownList>                                       
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px; color:#999;width:25%">  
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" Style="margin-left: 0px;Display:none"
                                    RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged">
                                    <asp:ListItem Text="Process" Value="Process" Selected="True" />
                                    <asp:ListItem Text="Others" Value="Others" style="margin-left:20px"/>
                                    </asp:RadioButtonList>    
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px; color:#999;width:25%">                                                                                                                    
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px; color:#999;width:25%">
                                    <div style="width:90%"> 
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>                                               
                                                    <asp:Button ID="lbtnExportExcel" Text="Export to Excel" 
                                                    class="btn btn-primary" CausesValidation="true" 
                                                    OnClick="lbtnExportExcel_Click" runat="server" />
                                                    <asp:LinkButton Text="Add New" style="float: right;" CssClass="btn btn-primary"
                                                    runat="server" OnClientClick="return openModal()" ID="btnAddPromotor" OnClick="btnAddPromotor_Click"  /> 
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lbtnExportExcel" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              
                             <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:25%">
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:25%">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15" Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:25%">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15"  Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit 2">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:25%">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit 3">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>  
                              <div class="clearfix"></div>
                            <div class="col-md-12 colpadding0">                                                                                                                                                                                                                                                                                  
                                <div class="col-md-3 colpadding0" style="margin-top:5px;width:25%">                                 
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Location"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3"    OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" Width="90%" Height="32px" class="form-control m-bot15">
                                    </asp:DropDownListChosen>                          
                                </div>                                 
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;width:25%">                               
                                    <asp:DropDownListChosen ID="ddlFilterProcess" runat="server" AutoPostBack="true"  DataPlaceHolder="Process"   
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"     OnSelectedIndexChanged="ddlFilterProcess_SelectedIndexChanged" Width="90%" Height="32px" class="form-control m-bot15">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlFilterProcess" ID="rfvProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;width:25%">                                  
                                    <asp:DropDownListChosen ID="ddlFilterSubProcess" runat="server" AutoPostBack="true" DataPlaceHolder="Sub Process" 
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  class="form-control m-bot15"  Width="90%" Height="32px" OnSelectedIndexChanged="ddlFilterSubProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlFilterSubProcess" ID="rfvSubProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />  
                                </div>   
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;width:25%">                                
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocationType" AutoPostBack="true" DataPlaceHolder="Location Type"  
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  class="form-control m-bot15"  Width="90%" Height="32px" OnSelectedIndexChanged="ddlFilterLocationType_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                  
                                </div>                                                                                                                                                                 
                           </div>
                            <div class="clearfix"></div>                            
                            <%--//////////////////////////////////////////Gridview/////////////////////////--%>
                             <div style="margin-bottom: 4px">  
                                <div class="col-lg-12 col-md-12">        
                                        <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" GridLines="none" 
                                            CssClass="table"  BorderWidth="0px" OnRowCommand="grdRiskActivityMatrix_RowCommand" ShowHeaderWhenEmpty="true"
                                        AllowPaging="True" PageSize="50" DataKeyNames="RiskCreationId" OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging">                        
                                        <Columns>                      
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Task Id" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltaskidItemTemplate" runat="server" Text='<%# Eval("RiskCreationId") %>'></asp:Label>                                                  
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                             <asp:TemplateField HeaderText="Branch">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                     <asp:Label ID="lblBranch" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                    </div>
                                                         </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Control#">
                                                <ItemTemplate>
                                                     <asp:Label ID="lblControlNo" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlNo") %>' ToolTip='<%# Eval("ControlNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                              <asp:TemplateField HeaderText="Process">
                                                <ItemTemplate>
                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                     <asp:Label ID="lblProcess" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                 </div>  
                                                          </ItemTemplate>
                                            </asp:TemplateField> 
                                             <asp:TemplateField HeaderText="Sub Process">
                                                <ItemTemplate>
                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                     <asp:Label ID="lblSubProcess" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("SubProcessName") %>' ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                  </div>  
                                                         </ItemTemplate>
                                            </asp:TemplateField>                                                   
                                            <asp:TemplateField HeaderText="Risk Description">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 180px;">
                                                    <asp:Label ID="lblRiskActivityDescriptionItemTemplate" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActivityDescription") %>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                                    </div>                                
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Control Objective">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 180px;">
                                                    <asp:Label ID="lblControlObjectiveItemTemplate" runat="server" data-toggle="tooltip" data-placement="left" Text='<%# Eval("ControlObjective") %>' ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                               
                                          
                                            <asp:TemplateField HeaderText="Control Description">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 180px;">
                                                    <asp:Label ID="txtExistingControlDescriptionItemTemplate" data-toggle="tooltip"  data-placement="left" runat="server" Text='<%# Eval("ControlDescription") %>' ToolTip='<%# Eval("ControlDescription") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>    
                                                                                                                                                                   
                                           <%-- <asp:TemplateField  HeaderText="Action" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_RCM" CommandArgument='<%# Eval("ID") + "," + Eval("RiskCreationId") %>' OnClientClick="return openModal()"  CausesValidation="false">
                                                        <img src="../../Images/edit_icon_new.png" alt="Display Rating" title="Display Rating"  /></asp:LinkButton>                                        
                                                </ItemTemplate>                                                                    
                                            </asp:TemplateField>--%>
                                        </Columns>

                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />                                         
                                        <PagerStyle HorizontalAlign="Right" />
                                        <PagerSettings Visible="false" /> 
                                        <PagerTemplate>
                                          
                                        </PagerTemplate>   
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>                                         
                                        </asp:GridView>
                                     <div style="float: right;">
                                          <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                          class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                         </asp:DropDownListChosen>  
                                     </div>
                                  </div>
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>                                   
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    
                                    <div class="table-paging-text" style="float: right;">
                                        <p>Page
                                            
                                        </p>
                                    </div>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                      </div>
                            </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lbtnExportExcel" />--%>
        </Triggers>
    </asp:UpdatePanel>

    <%--/////////////////////////////PopUp code Start//////////////////--%>

    <div class="modal fade" id="ComplaincePerformer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 34px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-body">

                            <table style="width: 100%">
                                <tr>
                                    <td colspan="4">
                                        <div style="margin-bottom: 7px; margin-left: 20px;">
                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary"
                                                ValidationGroup="CustomerValidationGroup1" ForeColor="Red" />
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                                ValidationGroup="CustomerValidationGroup1" Display="None" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Control#</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxControl" Style="width: 390px;" CssClass="form-control" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Control number can not be empty."
                                            ControlToValidate="tbxControl" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Risk Description</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxRiskDesc" Style="width: 390px;" CssClass="form-control" MaxLength="500"
                                            TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Control Objective</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxControlObj" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Control Description</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxControlDesc" Style="width: 390px;" CssClass="form-control" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Location
                                        </label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlClientProcess" AutoPostBack="true" Width="100%" Height="32px" class="form-control m-bot15"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlClientProcess_SelectedIndexChanged" />

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ErrorMessage="Please Select Location."
                                            ControlToValidate="ddlClientProcess" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Location." ControlToValidate="ddlClientProcess"
                                            ID="RequiredFieldValidator12"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Verticals
                                        </label>
                                    </td>
                                    <td style="width: 25%;">                                       
                                        <asp:DropDownListChosen runat="server" ID="ddlVerticalBranch" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                             AutoPostBack="true" Width="100%" Height="32px" class="form-control m-bot15" />

                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator20" ErrorMessage="Please Select Vertical."
                                            ControlToValidate="ddlVerticalBranch" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Vertical." ControlToValidate="ddlVerticalBranch"
                                            ID="RequiredFieldValidator9"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>

                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                            Process</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen ID="ddlProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="100%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ErrorMessage="Please Select Process."
                                            ControlToValidate="ddlProcess" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlProcess"
                                             ID="RequiredFdsfieldValidator9"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                            Sub Process</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="100%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Please Select Sub Process."
                                            ControlToValidate="ddlSubProcess" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlSubProcess" 
                                            ID="RequiredFieldValidator10"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                <tr>
                                     <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                            Activity</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen ID="ddlActivity" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="100%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlActivity_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Please Select Activity."
                                            ControlToValidate="ddlActivity" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Activity." ControlToValidate="ddlActivity" ID="RequiredFieldValidator15"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>

                                     <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Risk Category</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownCheckBoxes ID="ddlRiskCategoryCheck" runat="server" Width="200px"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                            OnSelectedIndexChanged="checkBoxesRiskCategoryProcess_SelcetedIndexChanged">
                                            <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                            <Texts SelectBoxCaption="Select Risk Category" />
                                        </asp:DropDownCheckBoxes>
                                    </td>                                  
                                </tr>
                                <tr>
                                      <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Person Responsible</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlPersonResponsible" Width="100%" 
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  Height="32px" class="form-control m-bot15" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" ErrorMessage="Please Select Person Responsible."
                                            ControlToValidate="ddlPersonResponsible" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Person Responsible." ControlToValidate="ddlPersonResponsible"
                                            ID="RequiredFieldValidator3"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>

                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Control Owner</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlControlOwner" Width="100%"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" Height="32px" class="form-control m-bot15" />

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" ErrorMessage="Please Select Control Owner."
                                            ControlToValidate="ddlControlOwner" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Control Owner." ControlToValidate="ddlControlOwner"
                                            ID="RequiredFieldValidator16"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Industry</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownCheckBoxes ID="ddlIndultory" runat="server" Width="200px" AutoPostBack="true"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                            OnSelectedIndexChanged="checkBoxesProcess_SelcetedIndexChanged">
                                            <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                            <Texts SelectBoxCaption="Select Industry" />
                                        </asp:DropDownCheckBoxes>
                                    </td>
                                     <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Assertions</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownCheckBoxes ID="ddlAssertions" runat="server" Width="200px"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                            OnSelectedIndexChanged="checkBoxesAssertionsProcess_SelcetedIndexChanged">
                                            <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                            <Texts SelectBoxCaption="Select Assertions" />
                                        </asp:DropDownCheckBoxes>
                                    </td>

                                </tr>
                               
                                 <tr>                                   
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            KC-1</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlKC1" Width="100%" Height="32px" 
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15" />
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator24" ErrorMessage="Please Select KC-1 Multiple risks addressed."
                                            ControlToValidate="ddlKC1" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select KC-1 Multiple risks addressed." ControlToValidate="ddlKC1"
                                            ID="RequiredFieldValidator18"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>

                                      <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            KC-2</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlKey" Width="100%" AllowSingleDeselect="false" DisableSearchThreshold="3" Height="32px" class="form-control m-bot15" />
                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator25" ErrorMessage="Please Select Key."
                                            ControlToValidate="ddlKey" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Key." ControlToValidate="ddlKey"
                                            ID="RequiredFieldValidator2"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Automated Control</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlAutomated" Width="100%"  AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                            Height="32px" class="form-control m-bot15" />
                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator26" ErrorMessage="Please Select Automated Control."
                                            ControlToValidate="ddlAutomated" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Automated Control." ControlToValidate="ddlAutomated"
                                            ID="RequiredFieldValidator8"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                   <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Preventive Control</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlPreventive" AllowSingleDeselect="false" DisableSearchThreshold="3" Width="100%" Height="32px" class="form-control m-bot15" />

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" ErrorMessage="Please Select Preventive Control."
                                            ControlToValidate="ddlPreventive" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Preventive Control." ControlToValidate="ddlPreventive"
                                            ID="RequiredFieldValidator4"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                <tr>
                                     <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Frequency</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlFrequency" AllowSingleDeselect="false" DisableSearchThreshold="3" Width="100%" Height="32px" class="form-control m-bot15" />

                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator28" ErrorMessage="Please Select Frequency."
                                            ControlToValidate="ddlFrequency" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Frequency." ControlToValidate="ddlFrequency"
                                            ID="RequiredFieldValidator5"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Location Type</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen ID="ddlLocationType" runat="server" AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15" Width="100%" Height="32px"></asp:DropDownListChosen>

                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator29" ErrorMessage="Please Select Location Type."
                                            ControlToValidate="ddlLocationType" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Location Type." ControlToValidate="ddlLocationType"
                                            ID="RequiredFieldValidator11"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                <tr>

                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Primary / Secondary</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlPrimarySecondary" AllowSingleDeselect="false" DisableSearchThreshold="3" Width="100%" Height="32px" class="form-control m-bot15" />

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" ErrorMessage="Please Select Primary / Secondary."
                                            ControlToValidate="ddlPrimarySecondary" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Primary / Secondary." ControlToValidate="ddlPrimarySecondary"
                                            ID="RequiredFieldValidator17"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Effective Date</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="txtStartDate" placeholder="DD-MM-YYYY" Style="width: 390px;" CssClass="form-control"
                                             MaxLength="200" />
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator33" ErrorMessage="Effective Date can not be empty."
                                            ControlToValidate="txtStartDate" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Gap Description</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxGapDesc" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Recommendations</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxRecom" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Action/ Remediation plan</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxActionPlan" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            IPE(Information Produced by Entity)</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxIPE" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            ERP System</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxERPSystem" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            FRC(Fraud Risk Control)</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxFRC" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Mitigating Control</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="txtMitigation" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Unique / Referred</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxUnique" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Control Rating</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlControlRating" AllowSingleDeselect="false" DisableSearchThreshold="3" Width="100%" Height="32px" class="form-control m-bot15" />

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" ErrorMessage="Please Select Control Rating."
                                            ControlToValidate="ddlControlRating" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />

                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Control Rating." ControlToValidate="ddlControlRating"
                                            ID="RequiredFieldValidator6"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Risk Rating</label>
                                    </td>
                                    <td style="width: 25%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlRiskRating" AllowSingleDeselect="false" DisableSearchThreshold="3" Width="100%" Height="32px" class="form-control m-bot15" />
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator31" ErrorMessage="Please Select Risk Rating."
                                            ControlToValidate="ddlRiskRating" runat="server" ValidationGroup="CustomerValidationGroup1"
                                            Display="None" />


                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Risk Rating." ControlToValidate="ddlRiskRating"
                                            ID="RequiredFieldValidator7"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button Text="Save" runat="server" ID="btnSavePop" CssClass="btn-search" Style="border: 0px; width: 70px; height: 30px; border-radius: 3px; margin-right: 5px"
                                            ValidationGroup="CustomerValidationGroup1" OnClick="btnSavePop_Click" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn-search" Style="border: 0px; width: 70px; height: 30px; border-radius: 3px;" OnClientClick="return CloseWin()" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                            <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--/////////////////////////////PopUp code End//////////////////--%>

    <%-- //////////////////GridView used for Export which is not display///////////////////////--%>
    <div style="display: none;">
        <asp:GridView ID="GridExportExcel" runat="server" AutoGenerateColumns="false" AllowPaging="false"
            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" ForeColor="Black">
            <Columns>
                <asp:BoundField DataField="ControlNo" HeaderText="Control No" ItemStyle-Width="150px" />
                <asp:BoundField DataField="RiskCreationId" HeaderText="Task Id" ItemStyle-Width="150px" />
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Process Name">
                    <ItemTemplate>
                        <asp:Label ID="ProcessName" runat="server" Text='<%# ShowProcessName((long)Eval("ProcessId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="SubProcess Name">
                    <ItemTemplate>
                        <asp:Label ID="SubProcessName" runat="server" Text='<%# ShowSubProcessName((long)Eval("ProcessId"),(long)Eval("SubProcessId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Risk Description">
                    <ItemTemplate>
                        <asp:Label ID="lblActivityDescription" runat="server" Text='<%# Eval("ActivityDescription") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Control Objective">
                    <ItemTemplate>
                        <asp:Label ID="lblControlObjective" runat="server" Text='<%# Eval("ControlObjective") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%-- <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="RiskCategory">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# ShowRiskCategoryName((long)Eval("RiskCreationId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>

                <asp:BoundField DataField="BranchName" HeaderText="Branch" ItemStyle-Width="150px" />
                <%-- <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Location">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# ShowCustomerBranchName((long)Eval("RiskCreationId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>

                <%-- <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Assertions">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# ShowAssertionsName((long)Eval("RiskCreationId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:BoundField DataField="ControlDescription" HeaderText="Control Description" ItemStyle-Width="150px" />
                <asp:BoundField DataField="MControlDescription" HeaderText="MitigatingControl Description" ItemStyle-Width="150px" />
                <asp:BoundField DataField="PersonResponsible" HeaderText="Person Responsible" ItemStyle-Width="150px" />
                <asp:BoundField DataField="EffectiveDate" HeaderText="EffectiveDate" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Key" HeaderText="Key" ItemStyle-Width="150px" />
                <asp:BoundField DataField="PrevationControl" HeaderText="Preventive Control" ItemStyle-Width="150px" />
                <asp:BoundField DataField="AutomatedControl" HeaderText="Automated Control" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Frequency" HeaderText="Frequency" ItemStyle-Width="150px" />
                <asp:BoundField DataField="GapDescription" HeaderText="Gap Description" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Recommendations" HeaderText="Recommendations" ItemStyle-Width="150px" />
                <asp:BoundField DataField="ActionRemediationplan" HeaderText="Action Remediation Plan" ItemStyle-Width="150px" />
                <asp:BoundField DataField="IPE" HeaderText="Information Produced by Entity" ItemStyle-Width="150px" />
                <asp:BoundField DataField="ERPsystem" HeaderText="ERP System" ItemStyle-Width="150px" />
                <asp:BoundField DataField="FRC" HeaderText="Fraud Risk Control" ItemStyle-Width="150px" />
                <asp:BoundField DataField="UniqueReferred" HeaderText="Unique Referred" ItemStyle-Width="150px" />
                <asp:BoundField DataField="TestStrategy" HeaderText="Test Strategy" ItemStyle-Width="150px" />
                <asp:BoundField DataField="DocumentsExamined" HeaderText="Documents Examined" ItemStyle-Width="150px" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
            <PagerSettings Position="Top" />
            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
            <AlternatingRowStyle BackColor="#E6EFF7" />
        </asp:GridView>
    </div>
    <div style="margin-bottom: 7px; float: right; margin-right: 467px; margin-top: 10px; display: none; clear: both">
        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" ValidationGroup="ComplianceInstanceValidationGroup" />
        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="btn btn-primary" />
    </div>

</asp:Content>
