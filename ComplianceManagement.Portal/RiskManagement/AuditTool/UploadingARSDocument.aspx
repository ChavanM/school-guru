﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadingARSDocument.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.UploadingARSDocument" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title></title>
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            width: 80% !important;
        }
    </style>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        .td1 {
            width: 25%;
        }

        .td2 {
            width: 25%;
        }

        .td3 {
            width: 25%;
        }

        .td4 {
            width: 25%;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script>
        //function closeModal() {
        //    window.location.reload();
        //}
        function CloseEvent() {
            //$('#divAuditFrequencyDialog').modal('hide');
            window.location.reload();
        }
        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        $('.btn-search').on('click', function () {

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
        <%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td colspan="4">
                            <div style="margin-bottom: 7px">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                    ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                    ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <asp:DropDownListChosen runat="server" ID="ddlLegalEntityPopPup" DataPlaceHolder="Unit"
                                class="form-control m-bot15" Width="80%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntityPopPup_SelectedIndexChanged">
                            </asp:DropDownListChosen></td>
                        <td class="td2">
                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity1PopPup" DataPlaceHolder="Sub Unit"
                                class="form-control m-bot15" Width="80%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1PopPup_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                        </td>
                        <td class="td3">
                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity2PopPup" DataPlaceHolder="Sub Unit"
                                 class="form-control m-bot15" Width="80%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2PopPup_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                        </td>
                        <td class="td4">
                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity3PopPup" DataPlaceHolder="Sub Unit"
                                class="form-control m-bot15" Width="80%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3PopPup_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <asp:DropDownListChosen runat="server" ID="ddlFilterLocationPopPup" DataPlaceHolder="Location" 
                              class="form-control m-bot15" Width="80%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocationPopPup_SelectedIndexChanged" />
                        </td>
                        <td class="td2">
                             <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                             <%{%>
                                <asp:DropDownCheckBoxes ID="ddlVertical" runat="server" Width="40%" Height="30px" AutoPostBack="true"
                                    AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True">
                                    <Style SelectBoxWidth="90%" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                    <Texts SelectBoxCaption="Select Vertical" />
                                </asp:DropDownCheckBoxes>
                             <%}%>   
                        </td>
                        <td class="td3">
                            <asp:DropDownListChosen runat="server" ID="ddlFilterFinancialPopPup" AutoPostBack="true"
                                DataPlaceHolder="Finanacial Year"  class="form-control m-bot15" Width="80%" Height="32px"
                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                OnSelectedIndexChanged="ddlFilterFinancialPopPup_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select Finanacial Year"
                                ControlToValidate="ddlFilterFinancialPopPup" runat="server" ValidationGroup="PromotorValidationGroup"
                                Display="None" />
                            <asp:CompareValidator ID="CompareValidator15" ErrorMessage="Please Select Finanacial Year"
                                ControlToValidate="ddlFilterFinancialPopPup" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                ValidationGroup="PromotorValidationGroup" Display="None" />
                        </td>
                        <td class="td4"></td>
                    </tr>
                    <tr style="margin-top:100px;">
                        <td class="td1">
                            <asp:FileUpload ID="FileUploadObservation" runat="server" />
                            <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="FileUploadObservation"
                                runat="server" ID="rfvFile" ValidationGroup="PromotorValidationGroup" Display="None" />
                        </td>
                        <td class="td2">
                            <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                ValidationGroup="PromotorValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="btnCancel" Visible="false" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseEvent()" />
                        </td>
                        <td class="td3"></td>
                        <td class="td4"></td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <asp:GridView runat="server" ID="grdVersionDisplayDownload" AutoGenerateColumns="false" PageSize="5" AllowPaging="true"
                                CssClass="table" GridLines="none" Width="100%" AllowSorting="true" ShowHeaderWhenEmpty="true">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("BranchName")%>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vertical">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("VerticalName")%>' ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Financial&nbsp;Year">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label ID="lblFinancialYear" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear")%>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FileName">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                <asp:Label ID="lblFileName" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("SampleDownloadFileName")%>' ToolTip='<%# Eval("SampleDownloadFileName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <PagerTemplate>
                                    <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>

                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
