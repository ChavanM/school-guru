﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class CheckListPreReqsiteDocuments : System.Web.UI.Page
    {       
        protected long ATBDID;       
        protected long ChecklistID;
        protected long AuditID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ATBDID = -1;
                ChecklistID = -1;
                AuditID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    ViewState["AuditID"] = AuditID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
                    ViewState["ATBDID"] = ATBDID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
                {
                    ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
                    ViewState["ChecklistID"] = ChecklistID;
                }
                if (ATBDID != -1 && ChecklistID != -1 && AuditID != -1)
                {                   
                    BindDocument(ChecklistID, ATBDID, AuditID);                    
                }
            }
        }
        protected string getstatus(long AuditID, long ATBDID, long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.RecentPTUs
                             where row.AuditID == AuditID && row.ATBDId == ATBDID
                             && row.checklistId == ChecklistID
                             select row.Status).FirstOrDefault();
                if (query != null)
                {
                    return Convert.ToString(query);
                }
                else
                {
                    return "";
                }
            }
        }
        public void rptComplianceDocumnets_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void rptComplianceDocumnets_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                rptComplianceDocumnets.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
                {
                    ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
                }
                else
                {
                    ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
                }
                
                BindDocument(ChecklistID, ATBDID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Page"))
                {                  
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
                    {
                        ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
                    }
                    else
                    {
                        ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
                    }                    
                    BindDocument(ChecklistID, ATBDID, AuditID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static PrerequisiteTran_Upload GetFileInternalFileData_Risk(int fileID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var file = (from row in entities.PrerequisiteTran_Upload
                            where row.Id == fileID
                            select row).FirstOrDefault();

                return file;
            }
        }
        public void DownloadFile(int fileId)
        {
            try
            {
                var file =GetFileInternalFileData_Risk(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.FileName);
                        Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        Response.Flush(); // send it to the client to download
                        Response.End();

                    }
                }
            }
            catch (Exception)
            {
                throw;

            }
        }     
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }
        public string ShowSampleDocumentNameView(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "/View";
            }

            return processnonprocess;
        }
        public static List<PrerequisiteTran_Upload> GetFileData(int id, int checklistID, int ATBDId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.PrerequisiteTran_Upload
                                where row.Id == id && row.AuditID == AuditID
                                && row.checklistId == checklistID && row.ATBDId == ATBDId                              
                                 && row.IsDeleted == false
                                select row).ToList();

                return fileData;
            }
        }
        protected void lblViewFile_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;          
            Label lblID = (Label)gvr.FindControl("lblID");
            Label lblATBDId = (Label)gvr.FindControl("lblATBDId");
            Label lblchecklistId = (Label)gvr.FindControl("lblchecklistId");            
            Label lblAuditId = (Label)gvr.FindControl("lblAuditId");

            List<PrerequisiteTran_Upload> fileData = GetFileData(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblchecklistId.Text), Convert.ToInt32(lblATBDId.Text), Convert.ToInt32(lblAuditId.Text));

            foreach (var file in fileData)
            {
                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                if (file.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles";
                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + FileData;

                    string extension = System.IO.Path.GetExtension(filePath);

                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                    }

                    string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    bw.Close();

                    string CompDocReviewPath = FileName;
                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestView('" + CompDocReviewPath + "');", true);
                }
                break;
            }
        }
        public void BindDocument(long checklistid, long ATBDID, long AuditID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<PreRequsiteBindDocument_Result> ComplianceDocument = new List<PreRequsiteBindDocument_Result>();

                    ComplianceDocument = (from row in entities.PreRequsiteBindDocument(AuditID, ATBDID, checklistid)
                                          where row.ATBDId == ATBDID && row.AuditID == AuditID                                                                                                                       
                                          select row).ToList();
                    
                    if (ComplianceDocument != null && ComplianceDocument.Count>0)
                    {
                        string status = getstatus(AuditID, ATBDID, ChecklistID);
                        if (status == "Approved")
                        {
                            btnApprove.Visible = false;
                            btnReject.Visible = false;
                        }
                        else if (status == "Rejected")
                        {
                            btnApprove.Visible = false;
                            btnReject.Visible = false;
                        }
                        else
                        {
                            btnApprove.Visible = true;
                            btnReject.Visible = true;
                        }                       
                        rptComplianceDocumnets.DataSource = ComplianceDocument.ToList();
                        rptComplianceDocumnets.DataBind();
                    }
                    else
                    {
                        btnApprove.Visible = false;
                        btnReject.Visible = false;
                        rptComplianceDocumnets.DataSource = null;
                        rptComplianceDocumnets.DataBind();
                    }                  
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnets");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbutton");
            if (lbtLinkDocbutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lbtLinkDocbutton);
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
                    {
                        ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
                    }
                    else
                    {
                        ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
                    }
                    ApproveUpdatePrerequisiteTran_Upload(AuditID, ChecklistID, ATBDID, 3);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Approved Sucessfully.";
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void ApproveUpdatePrerequisiteTran_Upload(long auid, long chkid, long atbdid,int status)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.PrerequisiteTran_Upload
                                where row.AuditID == auid
                                && row.checklistId == chkid && row.ATBDId == atbdid
                                && row.Status !=4
                                && row.IsDeleted == false
                                select row).ToList();

                if (fileData.Count > 0)
                {
                    fileData.ForEach(entry =>
                    {
                        entry.Status = status;                      
                    });

                    entities.SaveChanges();
                }
            }
        }

        public static void DeleteUpdatePrerequisiteTran_Upload(long auid, long chkid, long atbdid, int status,string remark)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> sid = new List<int>();
                sid.Add(1);
                sid.Add(4);
                var fileData = (from row in entities.PrerequisiteTran_Upload
                                where row.AuditID == auid
                                && row.checklistId == chkid && row.ATBDId == atbdid
                                && !sid.Contains(row.Status)
                                && row.IsDeleted == false
                                select row).ToList();

                if (fileData.Count > 0)
                {
                    fileData.ForEach(entry =>
                    {
                        entry.Status = status;
                        entry.Remark = remark;
                        var interfiledata = (from row in entities.InternalFileData_Risk
                                             where row.AuditID == auid
                                             && row.ATBDId == atbdid
                                             && row.IsDeleted == false
                                             && row.Name==entry.FileName
                                             select row).FirstOrDefault();
                        if (interfiledata !=null)
                        {
                            interfiledata.IsDeleted = true;
                            entities.SaveChanges();
                        }
                    });

                    entities.SaveChanges();
                }              
            }
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
                    {
                        ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
                    }
                    else
                    {
                        ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
                    }
                    if (string.IsNullOrEmpty(txtRemark.Text))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please enter remark.";
                        return;
                    }
                    if (!string.IsNullOrEmpty(txtRemark.Text))
                    {
                        DeleteUpdatePrerequisiteTran_Upload(AuditID, ChecklistID, ATBDID, 4, txtRemark.Text.Trim());
                        BindDocument(ChecklistID, ATBDID, AuditID);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Rejected Sucessfully.";
                        btnApprove.Visible = false;
                        btnReject.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}