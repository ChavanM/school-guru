﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class KickoffReport : System.Web.UI.Page
    {
        protected List<int> Branchlist = new List<int>();
        protected List<string> Quarterlist = new List<string>();
        protected static string AuditHeadOrManagerReport = null;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        protected int CustomerId = 0;
        protected bool DepartmentHead = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindFnancialYear();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFilterFinancial.ClearSelection();
                    ddlFilterFinancial.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }

        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            upComplianceTypeList.Update();
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling Type", "-1"));
        }
        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }

        protected string GetTestResult(string TOD, string TOE, string AuditStatusID)
        {
            try
            {
                string returnvalue = string.Empty;
                if ((TOD == "2" || TOD == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Design Failure (TOD)";
                }
                else if ((TOE == "2" || TOE == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Effectiveness Failure (TOE)";
                }
                else if ((TOD == null || TOE == "") || (TOE == null || TOE == ""))
                {
                    returnvalue = "Not Tested";
                }
                else if ((TOD == "2" || TOD == "-1") && AuditStatusID == "2")
                {
                    returnvalue = "Not Tested";
                }
                else
                {
                    returnvalue = "Pass";
                }
                return returnvalue;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetFrequencyName(long ID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var transactionsQuery = (from row in entities.mst_Frequency
                                             where row.Id == ID
                                             select row).FirstOrDefault();

                    return transactionsQuery.Name.Trim();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(userID));

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, userID);
            }
            //ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Legal Entity", "-1"));
        }

        public void BindFnancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem(" Select Financial Year ", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            else if (DepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
            upComplianceTypeList.Update();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
            upComplianceTypeList.Update();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
        }

        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSchedulingType(); BindVertical();
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    if (CustomerId == 0)
                    {
                        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    }
                    int UserId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                    // string chk = txtfromMonthDate.Text;
                    // int Year = Convert.ToInt32(chk.Substring(chk.Length - 4));
                    // string YourString = chk.Remove(chk.Length - 5);
                    // int monthnumber = DateTime.ParseExact(YourString, "MMMM", CultureInfo.InvariantCulture).Month;
                    // DateTime fromDatePeroid = new DateTime(Year, monthnumber, 1);

                    // string chk2 = txttoMonthDate.Text;
                    // int Yearto = Convert.ToInt32(chk2.Substring(chk2.Length - 4));
                    // string YourString2 = chk2.Remove(chk2.Length - 5);
                    // int monthnumber2 = DateTime.ParseExact(YourString2, "MMMM", CultureInfo.InvariantCulture).Month;
                    // int days = DateTime.DaysInMonth(Yearto, monthnumber2);
                    // DateTime toDatePeroid = new DateTime(Yearto, monthnumber2, days);
                    // if (fromDatePeroid <= toDatePeroid)

                    if (1 == 1)
                    {
                        int RoleID = -1;
                        ArrayList ItemList = new ArrayList();
                        DataTable Observationcategorytable = new DataTable();
                        Observationcategorytable.Columns.Add("ObservationCategory", typeof(string));
                        Observationcategorytable.Columns.Add("ObservationRating", typeof(int));
                        Observationcategorytable.Columns.Add("ProcessName", typeof(string));
                        if (PerformerFlag)
                            RoleID = 3;
                        if (ReviewerFlag)
                            RoleID = 4;
                        string ForPeriod = "";
                        string FnancialYear = "";
                        int CustomerBranchId = -1;
                        string CustomerBranchName = "";
                        int VerticalID = -1;
                        if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                        {
                            if (ddlLegalEntity.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                                CustomerBranchName = ddlLegalEntity.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                        {
                            if (ddlSubEntity1.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                                CustomerBranchName = ddlSubEntity1.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                        {
                            if (ddlSubEntity2.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                                CustomerBranchName = ddlSubEntity2.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                        {
                            if (ddlSubEntity3.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                                CustomerBranchName = ddlSubEntity3.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                        {
                            if (ddlSubEntity4.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                                CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                        {
                            if (ddlFilterFinancial.SelectedValue != "-1")
                            {
                                FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                            }
                        }
                        else
                        {
                            FnancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                        }
                        if (!String.IsNullOrEmpty(ddlVertical.SelectedValue))
                        {
                            if (ddlVertical.SelectedValue != "-1")
                            {
                                VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                            }
                        }

                        if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                        {
                            if (ddlPeriod.SelectedValue != "-1")
                            {
                                ForPeriod = ddlPeriod.SelectedItem.Text;
                            }
                        }
                        string IsAuditHeadFlag = string.Empty;
                        if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                        {
                            IsAuditHeadFlag = "AM";
                            if (RoleID == -1)
                            {
                                RoleID = 4;
                            }
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchy(CustomerId, CustomerBranchId);
                            var Branchlistloop = Branchlist.ToList();
                        }
                        else
                        {
                            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
                            {
                                IsAuditHeadFlag = "CMA";
                            }
                            else if (DepartmentHead)
                            {
                                IsAuditHeadFlag = "DH";
                            }
                            else if (CustomerManagementRisk.CheckIsManagement(UserId) == 8)
                            {
                                IsAuditHeadFlag = "MA";
                            }
                            else
                            {
                                IsAuditHeadFlag = "FE";
                            }
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchy(CustomerId, CustomerBranchId);
                            var Branchlistloop = Branchlist.ToList();
                        }

                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            List<SP_GetKickoffReportNew_Result> table = new List<SP_GetKickoffReportNew_Result>();

                            table = (from row in entities.SP_GetKickoffReportNew(UserId, IsAuditHeadFlag)
                                     select row).ToList();
                            if (Branchlist.Count > 0)
                            {
                                List<long?> BranchAssigned = Branchlist.Select(x => (long?)x).ToList();
                                table = table.Where(Entry => BranchAssigned.Contains(Entry.BranchId)).ToList();
                            }
                            if (FnancialYear != "")
                            {
                                table = table.Where(entry => entry.FinancialYear == FnancialYear).ToList();
                            }
                            if (ForPeriod != "")
                            {
                                table = table.Where(entry => entry.ForPeriod == ForPeriod).ToList();
                            }
                            if (table.Count > 0)
                            {

                                ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Deviation Scheduling vs. Audit Start Date");
                                DataTable ExcelData1 = null;
                                DataView view1 = new System.Data.DataView((table as List<SP_GetKickoffReportNew_Result>).ToDataTable());

                                //ExcelData1 = view1.ToTable(false, "ProcessName", "BranchName", "FinancialYear", "ForPeriod", "AuditPeriodFrom", "AuditPeriodTo", "AuditPlanStartDate", "AuditPlanEndDate", "AuditVistiStartDate", "AuditVisitEndDate", "ClosureDate", "BranchId", "VerticalID");

                              ExcelData1 = view1.ToTable(false, "ProcessName", "BranchName", "FinancialYear", "ForPeriod", "AuditPlanStartDate", "AuditPlanEndDate", "AuditVistiStartDate", "AuditVisitEndDate", "ClosureDate", "BranchId", "VerticalID", "Performer", "Reviewer1", "Reviewer2");

                                string MonthToDisplay = string.Empty;

                                foreach (DataRow item in ExcelData1.Rows)
                                {
                                    using (AuditControlEntities entities1 = new AuditControlEntities())
                                    {
                                        string ProcessNames = Convert.ToString(item["ProcessName"]);
                                        ProcessNames = ProcessNames.Trim(',');
                                        string[] processnamescomaseprated = ProcessNames.Split(',');
                                        item["ProcessName"] = ProcessNames;
                                    }
                                }

                                ExcelData1.Columns.Remove("BranchId");
                                ExcelData1.Columns.Remove("VerticalID");

                                exWorkSheet1.Cells["A1"].Value = "Scheduling vs. Audit Start Date";// "MSD Tracker";
                                exWorkSheet1.Cells["A1:J1"].Merge = true;

                                exWorkSheet1.Cells["A2"].Value = "Areas Covered";
                                exWorkSheet1.Cells["A2:A3"].Merge = true;
                                exWorkSheet1.Cells["A2"].AutoFitColumns(25);
                                exWorkSheet1.Cells["A2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["A2"].Style.WrapText = true;
                                exWorkSheet1.Cells["A2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                                exWorkSheet1.Cells["B2"].Value = "Location/s Covered";
                                exWorkSheet1.Cells["B2:B3"].Merge = true;
                                exWorkSheet1.Cells["B2"].AutoFitColumns(25);
                                exWorkSheet1.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["B2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["B2"].Style.WrapText = true;
                                exWorkSheet1.Cells["B2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                                exWorkSheet1.Cells["C2"].Value = "Financial Year";
                                exWorkSheet1.Cells["C2:C3"].Merge = true;
                                exWorkSheet1.Cells["C2"].AutoFitColumns(15);
                                exWorkSheet1.Cells["C2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["C2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["C2"].Style.WrapText = true;

                                exWorkSheet1.Cells["D2"].Value = "Period";
                                exWorkSheet1.Cells["D2:D3"].Merge = true;
                                exWorkSheet1.Cells["D2"].AutoFitColumns(15);
                                exWorkSheet1.Cells["D2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["D2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["D2"].Style.WrapText = true;

                                //exWorkSheet1.Cells["E2"].Value = "Audit Period";
                                //exWorkSheet1.Cells["E2:F2"].Merge = true;
                                //exWorkSheet1.Cells["E2"].AutoFitColumns(26);
                                //exWorkSheet1.Cells["E2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["E2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                //exWorkSheet1.Cells["E2"].Style.WrapText = true;

                                //exWorkSheet1.Cells["E3"].Value = "From";
                                //exWorkSheet1.Cells["E3"].AutoFitColumns(13);
                                //exWorkSheet1.Cells["E3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["E3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                //exWorkSheet1.Cells["E3"].Style.WrapText = true;

                                //exWorkSheet1.Cells["F3"].Value = "To";
                                //exWorkSheet1.Cells["F3"].AutoFitColumns(13);
                                //exWorkSheet1.Cells["F3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["F3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                //exWorkSheet1.Cells["F3"].Style.WrapText = true;

                                exWorkSheet1.Cells["E2"].Value = "Audit Planned";
                                exWorkSheet1.Cells["E2:F2"].Merge = true;
                                exWorkSheet1.Cells["E2"].AutoFitColumns(26);
                                exWorkSheet1.Cells["E2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["E2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["E2"].Style.WrapText = true;

                                exWorkSheet1.Cells["E3"].Value = "From";
                                exWorkSheet1.Cells["E3"].AutoFitColumns(13);
                                exWorkSheet1.Cells["E3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["E3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["E3"].Style.WrapText = true;

                                exWorkSheet1.Cells["F3"].Value = "To";
                                exWorkSheet1.Cells["F3"].AutoFitColumns(13);
                                exWorkSheet1.Cells["F3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["F3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["F3"].Style.WrapText = true;

                                exWorkSheet1.Cells["G2"].Value = "Audit Visit";
                                exWorkSheet1.Cells["G2:H2"].Merge = true;
                                exWorkSheet1.Cells["G2"].AutoFitColumns(26);
                                exWorkSheet1.Cells["G2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["G2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["G2"].Style.WrapText = true;

                                exWorkSheet1.Cells["G3"].Value = "From";
                                exWorkSheet1.Cells["G3"].AutoFitColumns(13);
                                exWorkSheet1.Cells["G3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["G3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["G3"].Style.WrapText = true;

                                exWorkSheet1.Cells["H3"].Value = "To";
                                exWorkSheet1.Cells["H3"].AutoFitColumns(13);
                                exWorkSheet1.Cells["H3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["H3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["H3"].Style.WrapText = true;


                                exWorkSheet1.Cells["I2"].Value = "Audit Close Date";
                                exWorkSheet1.Cells["I2:I3"].Merge = true;
                                exWorkSheet1.Cells["I2"].AutoFitColumns(26);
                                exWorkSheet1.Cells["I2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["I2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["I2"].Style.WrapText = true;

                                exWorkSheet1.Cells["J2"].Value = "Performer";
                                exWorkSheet1.Cells["J2:J3"].Merge = true;
                                exWorkSheet1.Cells["J2"].AutoFitColumns(15);
                                exWorkSheet1.Cells["J2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["J2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["J2"].Style.WrapText = true;
                                
                                exWorkSheet1.Cells["K2"].Value = "Reviewer1";
                                exWorkSheet1.Cells["K2:K3"].Merge = true;
                                exWorkSheet1.Cells["K2"].AutoFitColumns(15);
                                exWorkSheet1.Cells["K2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["K2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["K2"].Style.WrapText = true;

                                exWorkSheet1.Cells["L2"].Value = "Reviewer2";
                                exWorkSheet1.Cells["L2:L3"].Merge = true;
                                exWorkSheet1.Cells["L2"].AutoFitColumns(15);
                                exWorkSheet1.Cells["L2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["L2"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["L2"].Style.WrapText = true;

                                exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, false);

                                using (ExcelRange col = exWorkSheet1.Cells[1, 1, 3 + ExcelData1.Rows.Count, 12 + ItemList.Count])
                                {
                                    col.Style.WrapText = true;
                                    using (ExcelRange col1 = exWorkSheet1.Cells[1, 1, 3 + ExcelData1.Rows.Count, 12])
                                    {
                                        col1.Style.Numberformat.Format = "dd-MMM-yyyy";
                                    }
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                                    Response.ClearContent();
                                    Response.Buffer = true;
                                    Response.AddHeader("content-disposition", "attachment;filename=Deviation Scheduling vs. Audit Start Date.xlsx");
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.ms-excel";
                                    StringWriter sw = new StringWriter();
                                    Response.BinaryWrite(fileBytes);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Audit Available for selected period.";
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "From Period should be less than To Period.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        public List<NameValueHierarchy> GetAllHierarchy(long customerID, int customerbranchid)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public void LoadSubEntities(long customerid, NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                }
            }
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {

        }
    }
}