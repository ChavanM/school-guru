﻿<%@ Page Title="Control Testing" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="TestingUI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.TestingUI" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">              
        #aBackChk:hover {
            color: blue;
            text-decoration: underline;
        }      
    </style>
      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
   <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
  
     <script type="text/javascript">

         function CloseWin2() {
             $('#divDetailsDialog').modal('hide');
             window.location.reload();
         };
        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }
        function setDate() {
            $(".StartDate").datepicker();
        }

        function AddCustomerPopup() {
            $('#divDetailsDialog').modal('show');
            return true;
        }

        //This is used for Close Popup after save or update data.
        function CloseWin1() {
            $('#divDetailsDialog').modal('hide');
        };

        //This is used for Close Popup after button click.
        function caller1() {
            setInterval(CloseWin1, 3000);
        };
        
        function ShowDialog(ScheduledOnID, RiskCreationId, FinancialYear, CustomerBranchID,Period) {  
            $('#divDetailsDialog').modal('show');
                $('#showdetails').attr('width', '910px');
                $('#showdetails').attr('height', '440px');
                $('.modal-dialog').css('width', '1000px');
                $('#showdetails').attr('src', "../AuditTool/AuditStatusTransaction.aspx?ScheduledOnID=" + ScheduledOnID + "&RiskCreationId=" + RiskCreationId + "&FinancialYear="+FinancialYear+"&CustomerBranchID="+CustomerBranchID+"&Period="+Period);
        }

        function ShowReviewerDialog(ScheduledOnID, RiskCreationId, FinancialYear, CustomerBranchID, Period) {
            $('#divDetailsDialog').modal('show');
            $('#showdetails').attr('width', '910px');
            $('#showdetails').attr('height', '440px');
            $('.modal-dialog').css('width', '1000px');
            $('#showdetails').attr('src', "../Controls/ReviewerStatus.aspx?ScheduledOnID=" + ScheduledOnID + "&RiskCreationId=" + RiskCreationId + "&FinancialYear=" + FinancialYear + "&CustomerBranchID=" + CustomerBranchID + "&Period=" + Period);
        }

    </script>
    
    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Workspace');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                             <header class="panel-heading tab-bg-primary " >
                                      <ul id="rblRole1" class="nav nav-tabs" style="display:none;">
                                           <%if (roles.Contains(3))%>
                                           <%{%>
                                                <li class="active" id="liPerformer" runat="server">
                                                    <asp:LinkButton ID="lnkPerformer"  OnClick="ShowPerformer"  runat="server">Performer</asp:LinkButton>                                           
                                                </li>
                                           <%}%>
                                            <%if (roles.Contains(4))%>
                                           <%{%>
                                                <li class=""  id="liReviewer" runat="server">
                                                      <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                                                                         
                                                </li>
                                           <%}%>
                                    </ul>
                                </header>
                         
                            <div class="clearfix"></div>
                           
                             <div style="margin-bottom: 4px">
                                <asp:ValidationSummary runat="server" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                            </div>

                            <div class="clearfix"></div>

                                 <div style="float:left;width:100%">

                                   <div style="float:left;width:13%">
                                        <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;width:150px;">
                                            <div class="col-md-2 colpadding0" style="width:40px">
                                                <p style="color: #999; margin-top: 5px;">Show </p>
                                            </div>
                                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                <asp:ListItem Text="5" Selected="True" />
                                                <asp:ListItem Text="10" />
                                                <asp:ListItem Text="20" />
                                                <asp:ListItem Text="50" />
                                            </asp:DropDownList>
                                        </div>
                                     </div>
                                     
                                      <div style="float:left;width:87%">
                                          <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial" class="form-control m-bot15" Width="90%" Height="32px" Enabled="false"
                                                    AutoPostBack="true" DataPlaceHolder="Financial Year" OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                            </div>

                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                  
                                                <asp:DropDownListChosen runat="server" ID="ddlQuarter" AutoPostBack="true" CssClass="form-control m-bot15"  Width="90%" Height="32px"
                                                OnSelectedIndexChanged="ddlQuarter_SelectedIndexChanged" DataPlaceHolder="Period" Enabled="false">
                                                <asp:ListItem Value="-1">All</asp:ListItem>
                                                <asp:ListItem Value="1">Apr - Jun</asp:ListItem>
                                                <asp:ListItem Value="2">Jul - Sep</asp:ListItem>
                                                <asp:ListItem Value="3">Oct - Dec</asp:ListItem>
                                                <asp:ListItem Value="4">Jan - Mar</asp:ListItem>
                                                </asp:DropDownListChosen>
                                                <asp:CompareValidator ErrorMessage="Select Quarter" ControlToValidate="ddlQuarter"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                                Display="None" />
                                            </div>

                                          <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                                <asp:DropDownListChosen runat="server" ID="ddlStatus" AutoPostBack="true" CssClass="form-control m-bot15" Enabled="false"
                                                OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" Width="90%" Height="32px" DataPlaceHolder="Status">
                                                <asp:ListItem Value="-1">All</asp:ListItem>
                                                <asp:ListItem Value="1">Open</asp:ListItem>
                                                <asp:ListItem Value="2">Submitted</asp:ListItem>
                                                <asp:ListItem Value="4">Review Comment</asp:ListItem>
                                                <asp:ListItem Value="3">Closed</asp:ListItem>                                
                                                </asp:DropDownListChosen>
                                            </div>

                                           <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; text-align:right">
                                                <asp:LinkButton runat="server" ID="btnBack" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />
                                                </div>                                                                                     
                                         </div>                                     
                                       </div>
                            
                              <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0">
                            <div runat="server" id="LblPageDetails" style="color: #666"></div>
                        </div>
                        <div class="clearfix"></div>  
                                     
                        <div style="margin-bottom: 4px">
                            <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" PageSize="5" AllowPaging="true" AutoPostBack="true"
                                 CssClass="table" GridLines="None" Width="100%"  AllowSorting="false" OnRowDataBound="grdComplianceTransactions_RowDataBound" 
                                DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging" ShowHeaderWhenEmpty="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Task ID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("RiskCreationId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                     
                                      <asp:TemplateField HeaderText="Control&nbsp;No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblControlNo" runat="server" Text='<%# Eval("ControlNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Risk">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 225px;">
                                                <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Width="230px"
                                                     Text='<%# Eval("ActivityDescription") %>' 
                                                    ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                  

                                    <asp:TemplateField HeaderText="Control Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 180px;">
                                                <asp:Label ID="lblControlDescription" Width="180px" data-toggle="tooltip" data-placement="bottom" runat="server" Text='<%# Eval("ControlDescription")%>' ToolTip='<%# Eval("ControlDescription")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Test Strategy">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 180px;">
                                                <asp:Label ID="lblTestStrategy" data-toggle="tooltip" Width="180px" data-placement="bottom" runat="server" Text='<%# Eval("TestStrategy")%>' ToolTip='<%# Eval("TestStrategy")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Period" Visible="false">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth")%>' ToolTip='<%# Eval("ForMonth")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Frequency">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblFrequency" runat="server" Text='<%# ShowFrequencyName((long?)Eval("Frequency")) %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Reviewer">
                                        <ItemTemplate>
					                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">                                           
                                                <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# GetReviewer((int)Eval("AuditStatusID") ,(long?)Eval("ScheduledOnID"), (long)Eval("RiskCreationId")) %>'  tooltip='<%# GetReviewer((int)Eval("AuditStatusID") ,(long?)Eval("ScheduledOnID"), (long)Eval("RiskCreationId")) %>'></asp:Label>
                                            </div>
				                       </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnChangeStatus" runat="server" Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("AuditStatusID")) %>'
                                                CommandName="CHANGE_STATUS" OnClientClick="AddCustomerPopup()" OnClick="btnChangeStatus_Click" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("RiskCreationId") + "," + Eval("FinancialYear") +"," + Eval("CustomerBranchID") +"," + Eval("ForMonth") +"," + Eval("RoleID")%>'><img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                  <PagerSettings Visible="false" />   
                                <PagerTemplate>
                                   <%-- <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>--%>
                                </PagerTemplate>
                                <EmptyDataTemplate>No Records Found</EmptyDataTemplate>
                            </asp:GridView>
                              <div style="float: right;">
                              <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                  class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                              </asp:DropDownListChosen>  
                            </div>
                        </div>
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click" />--%>
                                   <div class="table-paging-text" style="float:right;">
                                        <p>Page
                                         <%--   <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click" />--%>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                            </section>
                        </div>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div class="modal fade" id="divDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 1200px; ">
            <div class="modal-content" style="width: 96.5%;background-color:#eee;">
                <div class="modal-header" style="background-color:#eee;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"  
                        style="margin-top: -6px;" onclick="javascript:window.location.reload()">&times;</button>
                </div>
                <div class="modal-body" style="background-color:#eee; border-radius:10px; padding: 0px 15px;">
                    <iframe id="showdetails" src="about:blank" width="1150px"  style="height:750px;"   frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>






<%--<vit:AuditStatusTransaction runat="server" ID="udcStatusTranscatopn" />--%>
</asp:Content>
