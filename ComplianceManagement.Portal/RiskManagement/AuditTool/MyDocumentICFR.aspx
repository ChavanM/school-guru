﻿<%@ Page Title="My Document" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="MyDocumentICFR.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.MyDocumentICFR" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 34px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clsheadergrid, .table tr th label {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        .clsheadergrid, .table tr th input {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftdocumentmenu');
            fhead('My Document');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <section class="panel"> 
                                <header class="panel-heading tab-bg-primary ">
                                    <ul id="rblRole1" class="nav nav-tabs">
                                    <%if (roles.Contains(3))%>
                                    <%{%>
                                        <li class="active" id="liPerformer" runat="server">
                                        <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                        </li>
                                    <%}%>
                                    <%if (roles.Contains(4))%>
                                    <%{%>
                                        <li class=""  id="liReviewer" runat="server">
                                        <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                        </li>
                                    <%}%>     
                                        <%if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")%>
                                           <%{%>
                                        <li class=""  id="liAuditHead" runat="server">
                                            <asp:LinkButton ID="lnkAuditHead" OnClick="ShowAuditHead" runat="server">Audit Head</asp:LinkButton>                                        
                                        </li>
                                          <%}%>                                
                                    </ul>
                                </header>
                                <div class="clearfix"></div>  
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" Selected="True" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit" 
                                        class="form-control m-bot15 select_location"  AllowSingleDeselect="false" DisableSearchThreshold="3" Width="90%" Height="32px" Style="float: left; width:90%;"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit" 
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px" Style="float: left; width:90%;"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit" 
                                        class="form-control m-bot15 select_location"  Width="90%" Height="32px" Style="float: left; width:90%;"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Unit" 
                                            class="form-control m-bot15 select_location" Width="90%" Height="32px" Style="float: left; width:90%;"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" 
                                        OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"  Width="90%" Height="32px" Style="float: left; width:90%;"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15 select_location" DataPlaceHolder="Sub Unit">
                                        </asp:DropDownListChosen>
                                    </div>                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true"
                                             OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged"
                                             class="form-control m-bot15 select_location"  Width="90%" Height="32px" Style="float: left; width:90%;"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Financial Year">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlPeriod" DataPlaceHolder="Period" AutoPostBack="true"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"  Width="90%" Height="32px" Style="float: left; width:90%;"
                                        class="form-control m-bot15 select_location">
                                        <asp:ListItem Value="-1">Period</asp:ListItem>
                                        <asp:ListItem Value="1">Apr - Jun</asp:ListItem>
                                        <asp:ListItem Value="2">Jul - Sep</asp:ListItem>
                                        <asp:ListItem Value="3">Oct - Dec</asp:ListItem>
                                        <asp:ListItem Value="4">Jan - Mar</asp:ListItem>                           
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">                                                                      
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen ID="ddlProcess" runat="server" AutoPostBack="true"
                                        class="form-control m-bot15 select_location"  Width="90%" Height="32px" Style="float: left; width:90%;"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged" DataPlaceHolder="Process">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true"
                                             class="form-control m-bot15 select_location"  Width="90%" Height="32px" Style="float: left; width:90%;"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged" DataPlaceHolder="Sub Process">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div>
                                    <asp:GridView runat="server" ID="grdMyDocumentICFR" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                                    CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="5" Width="100%" DataKeyNames="RiskCreationId"
                                    Font-Size="12px" ShowHeaderWhenEmpty="true">
                                    <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                 <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Control#">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblControlNo" runat="server" Text='<%# Eval("ControlNo") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ControlNo") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Risk Description">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label ID="lblPlannedFrom" runat="server" Text='<%# Eval("ActivityDescription") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Testing Document">
                                            <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="UpdateOriDoc" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:LinkButton ID="lbOriDocDownload" runat="server" CommandName="DownloadOriginalDocument"  CssClass="btn btn-primary" 
                                                    CommandArgument='<%# Eval("CustomerBranchID") + "," + Eval("ForMonth") + "," + Eval("FinancialYear") +"," + Eval("UserID")+"," + Eval("RiskCreationId")+","+Eval("RoleID") %>'
                                                    OnClick="lbOriDocDownload_Click" CausesValidation="false">Download</asp:LinkButton>
                                                    </div>
                                                </ContentTemplate>
                                                    <Triggers>
                                                    <asp:PostBackTrigger ControlID="lbOriDocDownload" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Review Document">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="UpdateReviewDoc" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                            <asp:LinkButton ID="lbReviewDocDownload" runat="server"  CssClass="btn btn-primary" 
                                                            CommandName="DownloadReviewDocument"
                                                            CommandArgument='<%# Eval("CustomerBranchID") + "," + Eval("ForMonth") + "," + Eval("FinancialYear") +"," + Eval("UserID")+"," + Eval("RiskCreationId")+","+Eval("ScheduledOnID") %>'                                                               
                                                            CausesValidation="false" OnClick="lbReviewDocDownload_Click">Download</asp:LinkButton>
                                                        </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lbReviewDocDownload" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerSettings Visible="false" />        
                                        <PagerTemplate>
                                     <%--   <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Records Found.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                          <div style="float: right;">
                                              <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                                  class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                              </asp:DropDownListChosen>  
                                        </div>
                                </div>
                                <div class="clearfix"></div>    
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                    <p>
                                    <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                    </p>
                                    </div>
                                    </div>
                                    </div>

                                    <div class="col-md-6 colpadding0" style="float: right;">
                                    <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click" />--%>
                                    <div class="table-paging-text" style="margin-top:-35px;margin-left:19px;">
                                    <p>Page
                                  <%--  <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                    <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                    </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click" />--%>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
