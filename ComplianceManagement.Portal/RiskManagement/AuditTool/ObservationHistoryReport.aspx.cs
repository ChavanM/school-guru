﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class ObservationHistoryReport : System.Web.UI.Page
    {

        protected List<int> Branchlist = new List<int>();
        protected List<string> Quarterlist = new List<string>();
        protected static string AuditHeadOrManagerReport=null;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                BindLegalEntityData();
                BindFnancialYear();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFilterFinancial.ClearSelection();
                    ddlFilterFinancial.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }

                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling Type", "-1"));
        }
        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected string GetTestResult(string TOD, string TOE, string AuditStatusID)
        {
            try
            {
                string returnvalue = string.Empty;
                if ((TOD == "2" || TOD == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Design Failure (TOD)";
                }
                else if ((TOE == "2" || TOE == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Effectiveness Failure (TOE)";
                }
                else if ((TOD == null || TOE == "") || (TOE == null || TOE == ""))
                {
                    returnvalue = "Not Tested";
                }
                else if ((TOD == "2" || TOD == "-1") && AuditStatusID == "2")
                {
                    returnvalue = "Not Tested";
                }
                else
                {
                    returnvalue = "Pass";
                }
                return returnvalue;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetFrequencyName(long ID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var transactionsQuery = (from row in entities.mst_Frequency
                                             where row.Id == ID
                                             select row).FirstOrDefault();

                    return transactionsQuery.Name.Trim();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindFnancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem(" Select Financial Year ", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
            upComplianceTypeList.Update();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
            upComplianceTypeList.Update();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
        }

        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSchedulingType(); BindVertical();
        }

        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            upComplianceTypeList.Update();
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        public static DataTable GetICFRAuditStatus(List<AuditInstanceTransactionView> MasterRecords, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();

                var transactionsQuery = (from row in MasterRecords
                                         select row).ToList();

                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("NoOfControlKey", typeof(long));
                table.Columns.Add("NoOfControlNonKey", typeof(long));

                table.Columns.Add("TestResultPassKey", typeof(long));
                table.Columns.Add("TestResultPassNonKey", typeof(long));

                table.Columns.Add("TestResultFailKey", typeof(long));
                table.Columns.Add("TestResultFailNonKey", typeof(long));
                table.Columns.Add("NotTestedcount", typeof(long));

                table.Columns.Add("Total", typeof(long));

                var ProcessList = ProcessManagement.GetAllNew(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                long totalcount = 0;

                long NoOfControlKeyNonKeycount = 0;
                long NoOfControlKey = 0;
                long NoOfControlNonKey = 0;

                long PassKeyCount = 0;
                long PassNonKeyCount = 0;

                long FailKeyCount = 0;
                long FailNonKeyCount = 0;
                long totalNotTestedcount = 0;
                foreach (Mst_Process cc in ProcessList)
                {
                    var NotTestedcount = transactionsQuery.Where(a => a.ProcessId == cc.Id && ((a.TOD == 2 && a.TOE == -1 && a.AuditStatusID == 3) || (a.TOD == null && a.TOE == null))).ToList();
                    totalNotTestedcount = NotTestedcount.Count;
                    NoOfControlKeyNonKeycount = transactionsQuery.Where(a => a.ProcessId == cc.Id).ToList().Count;
                    NoOfControlKey = transactionsQuery.Where(a => a.KeyId == 1 && a.ProcessId == cc.Id).ToList().Count;
                    NoOfControlNonKey = transactionsQuery.Where(a => a.KeyId == 2 && a.ProcessId == cc.Id).ToList().Count;

                    var PassCount = transactionsQuery.Where(a => a.ProcessId == cc.Id && (a.TOD == 1 && a.TOE == 1) || (a.TOD == 1 && a.TOE == 3) || (a.TOD == 3 && a.TOE == 1) || (a.TOD == 3 && a.TOE == 3)).ToList();
                    PassKeyCount = PassCount.Where(a => a.AuditStatusID == 3 && a.KeyId == 1 && a.ProcessId == cc.Id).ToList().Count;
                    PassNonKeyCount = PassCount.Where(a => a.AuditStatusID == 3 && a.KeyId == 2 && a.ProcessId == cc.Id).ToList().Count;

                    var Failcount = transactionsQuery.Where(a => a.AuditStatusID == 3 && a.ProcessId == cc.Id && (a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2)).ToList();
                    FailKeyCount = Failcount.Where(a => a.KeyId == 1 && a.ProcessId == cc.Id).ToList().Count;
                    FailNonKeyCount = Failcount.Where(a => a.KeyId == 2 && a.ProcessId == cc.Id).ToList().Count;

                    totalcount = NoOfControlKeyNonKeycount;

                    if (totalcount != 0)
                        table.Rows.Add(cc.Id, cc.Name, NoOfControlKey, NoOfControlNonKey, PassKeyCount,
                            PassNonKeyCount, FailKeyCount, FailNonKeyCount, totalNotTestedcount, totalcount);
                }
                return table;
            }
        }
        public List<NameValueHierarchy> GetAllHierarchy(long customerID, int customerbranchid)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public void LoadSubEntities(long customerid, NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                }
            }
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            #region Sheet 1   
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    string ForPeriod = "";
                    string FnancialYear = "";
                    int CustomerBranchId = -1;
                    string CustomerBranchName = "";
                    int VerticalID = -1;
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                            CustomerBranchName = ddlLegalEntity.SelectedItem.Text;
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                            CustomerBranchName = ddlSubEntity1.SelectedItem.Text;
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                            CustomerBranchName = ddlSubEntity2.SelectedItem.Text;
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                            CustomerBranchName = ddlSubEntity3.SelectedItem.Text;
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                            CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                    {
                        if (ddlFilterFinancial.SelectedValue != "-1")
                        {
                            FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                        }
                    }
                    else
                    {
                        FnancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    }                   
                    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                    {
                        int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                        if (vid != -1)
                        {
                            VerticalID = vid;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                        {
                            if (ddlVertical.SelectedValue != "-1")
                            {
                                VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                            CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                    {
                        if (ddlPeriod.SelectedValue != "-1")
                        {
                            ForPeriod = ddlPeriod.SelectedItem.Text;
                        }
                    }
                    if (AuditHeadOrManagerReport == null)
                    {
                        AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
                    }
                   
                   var AuditID = UserManagementRisk.GetAuditManagerAuditID(CustomerBranchId, Portal.Common.AuthenticationHelper.UserID);
                    var customer = UserManagementRisk.GetCustomer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                    List<ObservationHistoryReportView> table1 = new List<ObservationHistoryReportView>();
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        table1 = (from row in entities.ObservationHistoryReportViews
                                  select row).ToList();

                        if (AuditID.Count>0)
                        {
                            table1 = table1.Where(entry => AuditID.Contains((int)entry.AuditId)).ToList();
                        }
                        //table1 = table1.OrderBy(a => a.ProcessOrder).ToList();
                        DataTable table = new DataTable();
                        table.Columns.Add("Process", typeof(string));
                        table.Columns.Add("SubProcess", typeof(string));
                        table.Columns.Add("AuditStepDescription", typeof(string));
                        table.Columns.Add("Title", typeof(string));
                        table.Columns.Add("OriginalDocument", typeof(string));
                        table.Columns.Add("RevisedDocument", typeof(string));
                        table.Columns.Add("OriginalCratedBy", typeof(string));
                        table.Columns.Add("ModifiedBy", typeof(string));
                        table.Columns.Add("ModifiedDate", typeof(string));

                        string ProcessVal = string.Empty;
                        string SubProNameVal = string.Empty;
                        string AuditstepDescVal = string.Empty;
                        string ProcessWalkthroughVal = string.Empty;
                        string ActualWorkDoneVal = string.Empty;
                        string PopulationVal = string.Empty;
                        string SampleVal = string.Empty;
                        string RemarksVal = string.Empty;
                        string ObservationTitleVal = string.Empty;
                        string ObservationVal = string.Empty;
                        string ObservationReportVal = string.Empty;
                        string RiskVal = string.Empty;
                        string RootCauseVal = string.Empty;
                        string FinancialImpactVal = string.Empty;
                        string RecommendationVal = string.Empty;
                        string ManagementResponseVal = string.Empty;
                        string TimeLineVal = string.Empty;
                        string ScoreVal = string.Empty;
                        string PersonResponsibleVal = string.Empty;
                        string OwnerNameVal = string.Empty;
                        string ObservationRatingVal = string.Empty;
                        string ObservationCategoryVal = string.Empty;
                        string ObservationSubCategoryVal = string.Empty;


                        string CheckProcessWalkthrough = string.Empty;

                        #region data

                        foreach (var item in table1)
                        {
                            if (ProcessVal != item.Process)
                            {
                                ProcessVal = item.Process;
                                SubProNameVal = item.SubProcess;
                                AuditstepDescVal = item.AuditStepDescription;
                                string checkprocessnamevalue = item.Process;
                                string checkSubprocessnamevalue = item.SubProcess;
                                string checkAuditStepevalue = item.AuditStepDescription;
                                if (!item.ProcessWalkthroughOld.Equals(item.ProcessWalkthrough))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Process Walkthrough", item.ProcessWalkthroughOld, item.ProcessWalkthrough, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ActualWorkDoneOld.Equals(item.ActualWorkDone))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Actual Work Done", item.ActualWorkDoneOld, item.ActualWorkDone, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.PopulationOld.Equals(item.Population))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Population", item.PopulationOld, item.Population, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.SampleOld.Equals(item.Sample))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Sample", item.SampleOld, item.Sample, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RemarksOld.Equals(item.Remarks))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Remarks", item.RemarksOld, item.Remarks, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationTitleOld.Equals(item.ObservationTitle))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Title	", item.ObservationTitleOld, item.ObservationTitle, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationOld.Equals(item.Observation))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation", item.ObservationOld, item.Observation, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RiskOld.Equals(item.Risk))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Risk", item.RiskOld, item.Risk, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RootCauseOld.Equals(item.RootCause))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Root Cause", item.RootCauseOld, item.RootCause, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.FinancialImpactOld.Equals(item.FinancialImpact))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Financial Impact", item.FinancialImpactOld, item.FinancialImpact, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RecommendationOld.Equals(item.Recommendation))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Recommendation", item.RecommendationOld, item.Recommendation, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ManagementResponseOld.Equals(item.ManagementResponse))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Management Response", item.ManagementResponseOld, item.ManagementResponse, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.TimeLineOld.Equals(item.TimeLine))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Time Line", item.TimeLineOld, item.TimeLine, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ScoreOld.Equals(item.Score))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Score", item.ScoreOld, item.Score, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.PersonResponsibleOld.Equals(item.PersonResponsible))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Person Responsible", item.PersonResponsibleOld, item.PersonResponsible, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.OwnerOld.Equals(item.Owner))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Owner Name", item.OwnerOld, item.Owner, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationRatingOld.Equals(item.ObservationRating))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Rating", item.ObservationRatingOld, item.ObservationRating, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationCategoryOld.Equals(item.ObservationCategory))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Category", item.ObservationCategoryOld, item.ObservationCategory, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationSubCategoryOld.Equals(item.ObservationSubCategory))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation SubCategory", item.ObservationSubCategoryOld, item.ObservationSubCategory, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                            }
                            if (SubProNameVal != item.SubProcess)
                            {
                                ProcessVal = item.Process;
                                SubProNameVal = item.SubProcess;
                                AuditstepDescVal = item.AuditStepDescription;
                                string checkprocessnamevalue = item.Process;
                                string checkSubprocessnamevalue = item.SubProcess;
                                string checkAuditStepevalue = item.AuditStepDescription;
                                if (!item.ProcessWalkthroughOld.Equals(item.ProcessWalkthrough))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Process Walkthrough", item.ProcessWalkthroughOld, item.ProcessWalkthrough, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ActualWorkDoneOld.Equals(item.ActualWorkDone))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Actual Work Done", item.ActualWorkDoneOld, item.ActualWorkDone, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.PopulationOld.Equals(item.Population))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Population", item.PopulationOld, item.Population, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.SampleOld.Equals(item.Sample))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Sample", item.SampleOld, item.Sample, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RemarksOld.Equals(item.Remarks))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Remarks", item.RemarksOld, item.Remarks, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationTitleOld.Equals(item.ObservationTitle))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Title	", item.ObservationTitleOld, item.ObservationTitle, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationOld.Equals(item.Observation))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation", item.ObservationOld, item.Observation, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RiskOld.Equals(item.Risk))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Risk", item.RiskOld, item.Risk, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RootCauseOld.Equals(item.RootCause))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Root Cause", item.RootCauseOld, item.RootCause, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.FinancialImpactOld.Equals(item.FinancialImpact))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Financial Impact", item.FinancialImpactOld, item.FinancialImpact, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RecommendationOld.Equals(item.Recommendation))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Recommendation", item.RecommendationOld, item.Recommendation, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ManagementResponseOld.Equals(item.ManagementResponse))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Management Response", item.ManagementResponseOld, item.ManagementResponse, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.TimeLineOld.Equals(item.TimeLine))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Time Line", item.TimeLineOld, item.TimeLine, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ScoreOld.Equals(item.Score))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Score", item.ScoreOld, item.Score, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.PersonResponsibleOld.Equals(item.PersonResponsible))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Person Responsible", item.PersonResponsibleOld, item.PersonResponsible, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.OwnerOld.Equals(item.Owner))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Owner Name", item.OwnerOld, item.Owner, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationRatingOld.Equals(item.ObservationRating))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Rating", item.ObservationRatingOld, item.ObservationRating, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }

                                if (!item.ObservationCategoryOld.Equals(item.ObservationCategory))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Category", item.ObservationCategoryOld, item.ObservationCategory, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }

                                if (!item.ObservationSubCategoryOld.Equals(item.ObservationSubCategory))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation SubCategory", item.ObservationSubCategoryOld, item.ObservationSubCategory, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                            }
                            if (AuditstepDescVal != item.AuditStepDescription)
                            {
                                ProcessVal = item.Process;
                                SubProNameVal = item.SubProcess;
                                AuditstepDescVal = item.AuditStepDescription;
                                string checkprocessnamevalue = item.Process;
                                string checkSubprocessnamevalue = item.SubProcess;
                                string checkAuditStepevalue = item.AuditStepDescription;
                                if (!item.ProcessWalkthroughOld.Equals(item.ProcessWalkthrough))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Process Walkthrough", item.ProcessWalkthroughOld, item.ProcessWalkthrough, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ActualWorkDoneOld.Equals(item.ActualWorkDone))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Actual Work Done", item.ActualWorkDoneOld, item.ActualWorkDone, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.PopulationOld.Equals(item.Population))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Population", item.PopulationOld, item.Population, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.SampleOld.Equals(item.Sample))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Sample", item.SampleOld, item.Sample, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RemarksOld.Equals(item.Remarks))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Remarks", item.RemarksOld, item.Remarks, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationTitleOld.Equals(item.ObservationTitle))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Title	", item.ObservationTitleOld, item.ObservationTitle, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationOld.Equals(item.Observation))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation", item.ObservationOld, item.Observation, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RiskOld.Equals(item.Risk))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Risk", item.RiskOld, item.Risk, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RootCauseOld.Equals(item.RootCause))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Root Cause", item.RootCauseOld, item.RootCause, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.FinancialImpactOld.Equals(item.FinancialImpact))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Financial Impact", item.FinancialImpactOld, item.FinancialImpact, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RecommendationOld.Equals(item.Recommendation))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Recommendation", item.RecommendationOld, item.Recommendation, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ManagementResponseOld.Equals(item.ManagementResponse))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Management Response", item.ManagementResponseOld, item.ManagementResponse, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.TimeLineOld.Equals(item.TimeLine))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Time Line", item.TimeLineOld, item.TimeLine, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ScoreOld.Equals(item.Score))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Score", item.ScoreOld, item.Score, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.PersonResponsibleOld.Equals(item.PersonResponsible))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Person Responsible", item.PersonResponsibleOld, item.PersonResponsible, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.OwnerOld.Equals(item.Owner))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Owner Name", item.OwnerOld, item.Owner, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationRatingOld.Equals(item.ObservationRating))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Rating", item.ObservationRatingOld, item.ObservationRating, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationCategoryOld.Equals(item.ObservationCategory))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Category", item.ObservationCategoryOld, item.ObservationCategory, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationSubCategoryOld.Equals(item.ObservationSubCategory))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation SubCategory", item.ObservationSubCategoryOld, item.ObservationSubCategory, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                            }
                            else
                            {
                                ProcessVal = item.Process;
                                SubProNameVal = item.SubProcess;
                                AuditstepDescVal = item.AuditStepDescription;
                                string checkprocessnamevalue = item.Process;
                                string checkSubprocessnamevalue = item.SubProcess;
                                string checkAuditStepevalue = item.AuditStepDescription;
                                if (!item.ProcessWalkthroughOld.Equals(item.ProcessWalkthrough))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Process Walkthrough", item.ProcessWalkthroughOld, item.ProcessWalkthrough, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ActualWorkDoneOld.Equals(item.ActualWorkDone))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Actual Work Done", item.ActualWorkDoneOld, item.ActualWorkDone, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.PopulationOld.Equals(item.Population))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Population", item.PopulationOld, item.Population, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.SampleOld.Equals(item.Sample))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Sample", item.SampleOld, item.Sample, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RemarksOld.Equals(item.Remarks))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Remarks", item.RemarksOld, item.Remarks, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationTitleOld.Equals(item.ObservationTitle))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Title	", item.ObservationTitleOld, item.ObservationTitle, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationOld.Equals(item.Observation))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation", item.ObservationOld, item.Observation, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RiskOld.Equals(item.Risk))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Risk", item.RiskOld, item.Risk, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RootCauseOld.Equals(item.RootCause))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Root Cause", item.RootCauseOld, item.RootCause, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.FinancialImpactOld.Equals(item.FinancialImpact))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Financial Impact", item.FinancialImpactOld, item.FinancialImpact, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.RecommendationOld.Equals(item.Recommendation))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Recommendation", item.RecommendationOld, item.Recommendation, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ManagementResponseOld.Equals(item.ManagementResponse))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Management Response", item.ManagementResponseOld, item.ManagementResponse, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.TimeLineOld.Equals(item.TimeLine))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Time Line", item.TimeLineOld, item.TimeLine, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ScoreOld.Equals(item.Score))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Score", item.ScoreOld, item.Score, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.PersonResponsibleOld.Equals(item.PersonResponsible))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Person Responsible", item.PersonResponsibleOld, item.PersonResponsible, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.OwnerOld.Equals(item.Owner))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Owner Name", item.OwnerOld, item.Owner, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationRatingOld.Equals(item.ObservationRating))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Rating", item.ObservationRatingOld, item.ObservationRating, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationCategoryOld.Equals(item.ObservationCategory))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation Category", item.ObservationCategoryOld, item.ObservationCategory, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                                if (!item.ObservationSubCategoryOld.Equals(item.ObservationSubCategory))
                                {
                                    table.Rows.Add(checkprocessnamevalue, checkSubprocessnamevalue, checkAuditStepevalue, "Observation SubCategory", item.ObservationSubCategoryOld, item.ObservationSubCategory, item.OldUser, item.CreatedBy, item.CreatedOn);
                                }
                            }

                        }
                        #endregion



                        List<FetchDataDeviation> tabletest = new List<FetchDataDeviation>();

                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            FetchDataDeviation obj = new FetchDataDeviation();
                            obj.Process = table.Rows[i]["Process"].ToString();
                            obj.SubProcess = table.Rows[i]["SubProcess"].ToString();
                            obj.AuditStepDescription = table.Rows[i]["AuditStepDescription"].ToString();
                            obj.Title = table.Rows[i]["Title"].ToString();
                            obj.OriginalDocument = table.Rows[i]["OriginalDocument"].ToString();
                            obj.RevisedDocument = table.Rows[i]["RevisedDocument"].ToString();
                            obj.OriginalCratedBy = table.Rows[i]["OriginalCratedBy"].ToString();
                            obj.ModifiedBy = table.Rows[i]["ModifiedBy"].ToString();
                            obj.ModifiedDate = table.Rows[i]["ModifiedDate"].ToString();

                            tabletest.Add(obj);
                        }

                        string asdfsd = "asd";



                        tabletest = tabletest.OrderBy(x => x.Process).ThenBy(x => x.SubProcess).ThenBy(x => x.AuditStepDescription).ThenBy(x => x.Title).ToList();

                        List<FetchDataDeviation> tabletestfinal = new List<FetchDataDeviation>();

                        string prcname = string.Empty;
                        string subprcname = string.Empty;
                        string auditstepname = string.Empty;
                        string checktitle = string.Empty;
                        foreach (var item in tabletest)
                        {
                            if (!prcname.Equals(item.Process))
                            {
                                prcname = item.Process;
                                subprcname = item.SubProcess;
                                auditstepname = item.AuditStepDescription;
                                checktitle = item.Title;
                                tabletestfinal.Add(item);
                            }
                            else if (!subprcname.Equals(item.SubProcess))
                            {
                                prcname = item.Process;
                                subprcname = item.SubProcess;
                                auditstepname = item.AuditStepDescription;
                                checktitle = item.Title;
                                item.Process = "";
                                //item.SubProcess = "";
                                tabletestfinal.Add(item);
                            }
                            else if (!auditstepname.Equals(item.AuditStepDescription))
                            {
                                prcname = item.Process;
                                subprcname = item.SubProcess;
                                auditstepname = item.AuditStepDescription;
                                checktitle = item.Title;
                                item.Process = "";
                                item.SubProcess = "";
                                tabletestfinal.Add(item);
                            }
                            //else if (!checktitle.Equals(item.Title))
                            //{
                            //    prcname = item.Process;
                            //    subprcname = item.SubProcess;
                            //    auditstepname = item.AuditStepDescription;
                            //    checktitle = item.Title;
                            //    item.Process = "";
                            //    item.SubProcess = "";
                            //    item.AuditStepDescription = "";
                            //    tabletestfinal.Add(item);
                            //}
                            else
                            {
                                prcname = item.Process;
                                subprcname = item.SubProcess;
                                auditstepname = item.AuditStepDescription;
                                checktitle = item.Title;
                                item.Process = "";
                                item.SubProcess = "";
                                item.AuditStepDescription = "";
                                //item.Title = "";
                                tabletestfinal.Add(item);
                            }

                        }

                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Observation History Report");
                        DataTable ExcelData = null;
                        DataView view1 = new System.Data.DataView((tabletestfinal as List<FetchDataDeviation>).ToDataTable());

                        ExcelData = view1.ToTable("Selected", false, "Process", "SubProcess", "AuditStepDescription", "Title", "OriginalDocument", "RevisedDocument", "OriginalCratedBy", "ModifiedBy", "ModifiedDate");

                        exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].AutoFitColumns(50);
                        exWorkSheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B1"].AutoFitColumns(50);
                        exWorkSheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["A2"].Value = "Name of Group:";
                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A2"].AutoFitColumns(50);
                        exWorkSheet.Cells["A2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["B2"].Value = customer.Name;
                        exWorkSheet.Cells["B2"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B2"].AutoFitColumns(50);
                        exWorkSheet.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));


                        exWorkSheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["A3"].Value = "Location:";
                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A3"].AutoFitColumns(50);

                        exWorkSheet.Cells["B3"].Value = CustomerBranchName;
                        exWorkSheet.Cells["B3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B3"].AutoFitColumns(50);

                        exWorkSheet.Cells["A4"].Value = "Financial Year:";
                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A4"].AutoFitColumns(50);

                        exWorkSheet.Cells["B4"].Value = FnancialYear;
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B4"].AutoFitColumns(50);

                        exWorkSheet.Cells["A5"].Value = "Period:";
                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A5"].AutoFitColumns(50);

                        exWorkSheet.Cells["B5"].Value = ForPeriod;
                        exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B5"].AutoFitColumns(50);

                        exWorkSheet.Cells["A6"].Value = "Type Of Report:";
                        exWorkSheet.Cells["A6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A6"].AutoFitColumns(50);

                        exWorkSheet.Cells["B6"].Value = "Observation History Report";
                        exWorkSheet.Cells["B6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B6"].AutoFitColumns(50);

                        exWorkSheet.Cells["A9"].LoadFromDataTable(ExcelData, false);

                        exWorkSheet.Cells["A8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A8"].Value = "Process";
                        exWorkSheet.Cells["A8"].AutoFitColumns(25);

                        exWorkSheet.Cells["B8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B8"].Value = "SubProcess";
                        exWorkSheet.Cells["B8"].AutoFitColumns(25);

                        exWorkSheet.Cells["C8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C8"].Value = "Audit Step Description";
                        exWorkSheet.Cells["C8"].AutoFitColumns(25);

                        exWorkSheet.Cells["D8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D8"].Value = "Title";
                        exWorkSheet.Cells["D8"].AutoFitColumns(25);


                        exWorkSheet.Cells["E8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E8"].Value = "Original Documentation";
                        exWorkSheet.Cells["E8"].AutoFitColumns(25);

                        exWorkSheet.Cells["F8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F8"].Value = "Revised Documenttion";
                        exWorkSheet.Cells["F8"].AutoFitColumns(25);

                        exWorkSheet.Cells["G8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G8"].Value = "Original Created By";
                        exWorkSheet.Cells["G8"].AutoFitColumns(25);

                        exWorkSheet.Cells["H8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H8"].Value = "Modified By";
                        exWorkSheet.Cells["H8"].AutoFitColumns(25);

                        exWorkSheet.Cells["I8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I8"].Value = "Modified Date";
                        exWorkSheet.Cells["I8"].AutoFitColumns(25);

                        using (ExcelRange col = exWorkSheet.Cells[8, 1, 8 + ExcelData.Rows.Count, 9])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                            exWorkSheet.Cells["A8:I8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A8:I8"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        //Response.ClearContent();
                        //Response.Buffer = true;
                        //Response.AddHeader("content-disposition", "attachment;filename=DeviationReport_"+ CustomerBranchName + ".xlsx");
                        //Response.Charset = "";
                        //Response.ContentType = "application/vnd.ms-excel";
                        //StringWriter sw = new StringWriter();
                        //Response.BinaryWrite(fileBytes);
                        //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        
                        CustomerBranchName = CustomerBranchName.Replace(',', ' ');
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("content-disposition", "attachment;filename=DeviationReport_" + CustomerBranchName + ".xlsx");//locatioName                                                                             
                        Response.BinaryWrite(fileBytes);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                   

                    }
                }
                catch (Exception ex)
                {
                    com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            #endregion
        }
    }
}





