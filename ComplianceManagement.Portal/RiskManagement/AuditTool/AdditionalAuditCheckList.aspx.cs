﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AdditionalAuditCheckList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public static long GetReviewerID(string ForPeriod, string FinancialYear,int CustomerBranchid,int VerticalId,int ProcessID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var reviewerid = (from row in entities.InternalControlAuditAssignments
                                  join row1 in entities.InternalAuditScheduleOns
                                  on row.InternalAuditInstance equals row1.InternalAuditInstance 
                                  where row.ProcessId == row1.ProcessId  && row.CustomerBranchID == CustomerBranchid &&
                                  row.VerticalID== VerticalId &&
                                  row1.ForMonth == ForPeriod && row1.FinancialYear == FinancialYear
                                  && row.ProcessId== ProcessID
                                  &&  row.RoleID == 4
                                  select row.UserID).FirstOrDefault();

                return (long) reviewerid;
            }
        }
        public static long GetAuditManagerID(int Customerid,int CustomerBranchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var reviewerid = (from row in entities.EntitiesAssignmentAuditManagerRisks                                 
                                  where row.CustomerID == Customerid && row.BranchID == CustomerBranchid  
                                  && row.ISACTIVE==true                           
                                  select row.UserID).FirstOrDefault();

                return (long) reviewerid;
            }
        }
        //private void ProcessReminderReviewer(string ForPeriod, string FinancialYear, int CustomerBranchid, int VerticalId, int ProcessID)
        //{
        //    try
        //    {
        //        long userid = GetReviewerID(ForPeriod, FinancialYear, CustomerBranchid, VerticalId, ProcessID);
        //        var user = UserManagement.GetByID(Convert.ToInt32(userid));
        //        if (user != null)
        //        {
        //            int customerID = -1;                    
        //            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //            string ReplyEmailAddressName = CustomerManagementRisk.GetByID(Convert.ToInt32(customerID)).Name;

        //            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //            string message = Properties.Settings.Default.EmailTemplate_AuditManagerAuditReviewer
        //                 .Replace("@User", username)
        //                 .Replace("@PeriodName", ForPeriod)
        //                 .Replace("@FinancialYear", FinancialYear)
        //                 .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                 .Replace("@From", ReplyEmailAddressName);

        //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Audit Reminder on Final review", message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        //private void ProcessReminderAuditManager(int CustomerBranchid)
        //{
        //    try
        //    {
        //        int customerID = -1;
        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //        long userid = GetAuditManagerID(customerID, CustomerBranchid);
        //        var user = UserManagement.GetByID(Convert.ToInt32(userid));
        //        if (user != null)
        //        {
                   
        //            string ReplyEmailAddressName = CustomerManagementRisk.GetByID(Convert.ToInt32(customerID)).Name;

        //            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //            string message = Properties.Settings.Default.EmailTemplate_AuditManagerAuditReviewer
        //                 .Replace("@User", username)
        //                 //.Replace("@PeriodName", ForPeriod)
        //                 //.Replace("@FinancialYear", FinancialYear)
        //                 .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                 .Replace("@From", ReplyEmailAddressName);

        //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Audit Reminder on Final review", message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        protected void btnPopupSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(Request.QueryString["BID"]))
                    if (!String.IsNullOrEmpty(Request.QueryString["VID"]))
                        if (!String.IsNullOrEmpty(Request.QueryString["PID"]))
                            if (!String.IsNullOrEmpty(Request.QueryString["SPID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["AUID"]))
                                {
                                    bool sucess= false;
                                    int ProcessId = -1;
                                    int Subprocessid = -1;
                                    int BranchID = -1;
                                    string financialyear = String.Empty;
                                    string period = string.Empty;
                                    int Veritcalid = -1;
                                    long auditID = -1;
                                    long auditStepMasterID = 0;
                                    int riskcreationId = 0;
                                    int RiskActivityID = 0;
                                    long customerID = -1;
                                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                                    int activityid = 0;
                                    int ATBDId = 0;

                                    if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                                    {
                                        BranchID = Convert.ToInt32(Request.QueryString["BID"]);
                                    }
                                    if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                                    {
                                        Veritcalid = Convert.ToInt32(Request.QueryString["VID"]);
                                    }
                                    if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                                    {
                                        ProcessId = Convert.ToInt32(Request.QueryString["PID"]);
                                    }
                                    if (!string.IsNullOrEmpty(Request.QueryString["SPID"]))
                                    {
                                        Subprocessid = Convert.ToInt32(Request.QueryString["SPID"]);
                                    }
                                    if (!string.IsNullOrEmpty(Request.QueryString["AUID"]))
                                    {
                                        auditID = Convert.ToInt32(Request.QueryString["AUID"]);
                                    }

                                    if (BranchID != -1 && BranchID != 0)
                                    {
                                        if (Veritcalid != -1 && Veritcalid != 0)
                                        {
                                            if (ProcessId != -1 && ProcessId != 0)
                                            {
                                                if (Subprocessid != -1 && Subprocessid != 0)
                                                {
                                                    if (auditID != -1 && auditID != 0)
                                                    {
                                                        #region riskcreationID
                                                        if ((RiskCategoryManagement.riskCategorycreationExists("NA", BranchID, ProcessId, Subprocessid)))
                                                        {
                                                            riskcreationId = RiskCategoryManagement.GetRiskCategpryCreationID("NA", BranchID, ProcessId, Subprocessid);
                                                        }
                                                        else
                                                        {
                                                            RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation();
                                                            riskcategorycreation.ControlNo = "NA";
                                                            riskcategorycreation.ProcessId = ProcessId;
                                                            riskcategorycreation.SubProcessId = Subprocessid;
                                                            riskcategorycreation.ActivityDescription = "NA";
                                                            riskcategorycreation.ControlObjective = "NA";
                                                            riskcategorycreation.IsDeleted = false;
                                                            riskcategorycreation.LocationType = 1;
                                                            riskcategorycreation.IsInternalAudit = "N";
                                                            riskcategorycreation.CustomerId = Convert.ToInt32(customerID);
                                                            riskcategorycreation.CustomerBranchId = BranchID;

                                                            sucess= RiskCategoryManagement.CreateriskCategorycreation(riskcategorycreation);
                                                            if (sucess)
                                                            {
                                                                riskcreationId = Convert.ToInt32(riskcategorycreation.Id);
                                                            }
                                                        }
                                                        #endregion

                                                        if (riskcreationId != 0)
                                                        {
                                                            #region  RiskActivityID                                                            
                                                            if ((RiskCategoryManagement.riskactivitytransactionExists("NA", BranchID, Veritcalid, ProcessId, Subprocessid, riskcreationId)))
                                                            {
                                                                RiskActivityID = RiskCategoryManagement.GetRiskActivityID("NA", BranchID, Veritcalid, ProcessId, Subprocessid, riskcreationId);
                                                            }
                                                            else
                                                            {
                                                                #region  mst_Activity                                                
                                                                if (!(RiskCategoryManagement.ActivityExists(ProcessId, Subprocessid)))
                                                                {
                                                                    activityid = RiskCategoryManagement.GetActivityID(ProcessId, Subprocessid);
                                                                }
                                                                else
                                                                {
                                                                    mst_Activity subactivity = new mst_Activity();
                                                                    subactivity.Name = "NA";
                                                                    subactivity.ProcessId = ProcessId;
                                                                    subactivity.SubProcessId = Subprocessid;
                                                                    subactivity.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                    subactivity.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                    subactivity.IsDeleted = false;
                                                                    subactivity.CreatedOn = DateTime.Now;
                                                                    subactivity.UpdatedOn = DateTime.Now;

                                                                    sucess=RiskCategoryManagement.Createmst_Activity(subactivity);
                                                                    if (sucess)
                                                                    {
                                                                        activityid = Convert.ToInt32(subactivity.Id);
                                                                    }
                                                                }
                                                                #endregion

                                                                RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                                                                riskactivitytransaction.CustomerBranchId = BranchID;
                                                                riskactivitytransaction.VerticalsId = Veritcalid;
                                                                riskactivitytransaction.ProcessId = ProcessId;
                                                                riskactivitytransaction.SubProcessId = Subprocessid;
                                                                riskactivitytransaction.RiskCreationId = riskcreationId;
                                                                riskactivitytransaction.ControlDescription = "NA";
                                                                riskactivitytransaction.MControlDescription = "NA";
                                                                riskactivitytransaction.PersonResponsible = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                riskactivitytransaction.EffectiveDate = DateTime.Now;
                                                                riskactivitytransaction.Key_Value = 1;
                                                                riskactivitytransaction.PrevationControl = 1;
                                                                riskactivitytransaction.AutomatedControl = 4;
                                                                riskactivitytransaction.Frequency = 1;
                                                                riskactivitytransaction.IsDeleted = false;
                                                                riskactivitytransaction.GapDescription = "NA";
                                                                riskactivitytransaction.Recommendations = "NA";
                                                                riskactivitytransaction.ActionRemediationplan = "NA";
                                                                riskactivitytransaction.IPE = "NA";
                                                                riskactivitytransaction.ERPsystem = "NA";
                                                                riskactivitytransaction.FRC = "NA";
                                                                riskactivitytransaction.UniqueReferred = "NA";
                                                                riskactivitytransaction.TestStrategy = "NA";
                                                                riskactivitytransaction.RiskRating = 1;
                                                                riskactivitytransaction.ControlRating = 2;
                                                                riskactivitytransaction.ProcessScore = 0;
                                                                riskactivitytransaction.ActivityID = activityid;
                                                                riskactivitytransaction.KC2 = 1;
                                                                riskactivitytransaction.Primary_Secondary = 10;
                                                                riskactivitytransaction.ControlOwner = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                riskactivitytransaction.ISICFR_Close = true;

                                                                sucess= RiskCategoryManagement.CreateRiskActivityTransactionRahul(riskactivitytransaction);
                                                                if (sucess)
                                                                {
                                                                    RiskActivityID = riskactivitytransaction.Id;
                                                                }
                                                            }
                                                            #endregion

                                                            if (RiskActivityID != 0)
                                                            {
                                                                if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists(riskcreationId, RiskActivityID, BranchID, Veritcalid, ProcessId, Subprocessid, txtATBD.Text.Trim())))
                                                                {
                                                                    #region  auditStepMasterID
                                                                    if ((RiskCategoryManagement.AuditStepMasterExists(txtATBD.Text.Trim())))
                                                                    {
                                                                        auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(txtATBD.Text.Trim());
                                                                    }
                                                                    else
                                                                    {
                                                                        AuditStepMaster newauditstepmaster = new AuditStepMaster()
                                                                        {
                                                                            AuditStep = Regex.Replace(txtATBD.Text.Trim(), @"\t|\n|\r", "")
                                                                        };

                                                                        sucess = RiskCategoryManagement.CreateAuditStepMaster(newauditstepmaster);
                                                                        if (sucess)
                                                                        {
                                                                            auditStepMasterID = newauditstepmaster.ID;
                                                                        }
                                                                       
                                                                    }
                                                                    #endregion

                                                                    if (auditStepMasterID != 0)
                                                                    {
                                                                        #region ATBDId
                                                                        RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping();
                                                                        RATBDM.AuditStepMasterID = auditStepMasterID;
                                                                        RATBDM.RiskCategoryCreationId = riskcreationId;
                                                                        RATBDM.RiskActivityId = RiskActivityID;
                                                                        RATBDM.CustomerBranchID = BranchID;
                                                                        RATBDM.VerticalID = Veritcalid;
                                                                        RATBDM.ProcessId = ProcessId;
                                                                        RATBDM.SubProcessId = Subprocessid;
                                                                        RATBDM.AuditObjective = txtAuditObjective.Text.Trim();
                                                                        RATBDM.ActivityTobeDone = Regex.Replace(txtATBD.Text.Trim(), @"\t|\n|\r", "");                                                                        
                                                                        RATBDM.IsActive = true;

                                                                        sucess = RiskCategoryManagement.CreateRiskActivityToBeDoneMapping(RATBDM);
                                                                        if (sucess)
                                                                        {
                                                                            ATBDId = RATBDM.ID;
                                                                        }

                                                                        if (!string.IsNullOrEmpty(ddlRatingItemTemplate.SelectedValue))
                                                                        {
                                                                            if (ddlRatingItemTemplate.SelectedValue !="-1")
                                                                            {
                                                                                RATBDM.Rating = Convert.ToInt32(ddlRatingItemTemplate.SelectedValue);
                                                                            }
                                                                            else
                                                                            {
                                                                                RATBDM.Rating = null;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            RATBDM.Rating = null;
                                                                        }

                                                                        #endregion

                                                                        if (ATBDId != 0)
                                                                        {
                                                                            #region AuditStepMapping
                                                                            AuditStepMapping newASM = new AuditStepMapping()
                                                                            {
                                                                                AuditID = auditID,
                                                                                ATBDId = ATBDId,
                                                                                IsActive = true,
                                                                            };
                                                                            if (!RiskCategoryManagement.AuditStepMappingExists(newASM))
                                                                            {
                                                                                sucess = RiskCategoryManagement.CreateAuditStepMapping(newASM);

                                                                                #region AuditClosureDetails                                                                              
                                                                                sucess = RiskCategoryManagement.UpdateAuditClosureDetailsCount(auditID);
                                                                                #endregion
                                                                            }
                                                                            #endregion

                                                                            if (sucess)
                                                                            {
                                                                                //var details = UserManagementRisk.GetDataAuditClosureDetails(Convert.ToInt32(auditID));
                                                                                //if (details != null)
                                                                                //{
                                                                                //    ProcessReminderReviewer(details.ForMonth,details.FinancialYear,BranchID, Veritcalid, ProcessId);
                                                                                //    ProcessReminderAuditManager(BranchID);
                                                                                //}
                                                                                cvDuplicateEntry.ErrorMessage = "Audit Step Save Sucessfully.";
                                                                                cvDuplicateEntry.IsValid = false;
                                                                            }
                                                                        }//ATBDId != 0 end
                                                                        else
                                                                        {
                                                                            cvDuplicateEntry.ErrorMessage = "something went wrong(ATBDId) please try again later";
                                                                            cvDuplicateEntry.IsValid = false;
                                                                        }
                                                                    }//auditStepMasterID != 0 end  
                                                                    else
                                                                    {
                                                                        cvDuplicateEntry.ErrorMessage = "something went wrong(auditStepMasterID) please try again later";
                                                                        cvDuplicateEntry.IsValid = false;
                                                                    }
                                                                }//RiskactivityTobedonemapping exists end
                                                                else
                                                                {
                                                                    cvDuplicateEntry.ErrorMessage = "Audit Step already exists.";
                                                                    cvDuplicateEntry.IsValid = false;                                                                    
                                                                }
                                                            }//RiskActivityID != -1 end
                                                            else
                                                            {
                                                                cvDuplicateEntry.ErrorMessage = "something went wrong(RiskActivityID) please try again later";
                                                                cvDuplicateEntry.IsValid = false;
                                                            }
                                                        }//riskcreationId != -1 end
                                                        else
                                                        {                                                            
                                                            cvDuplicateEntry.ErrorMessage = "something went wrong(riskcreationId) please try again later";
                                                            cvDuplicateEntry.IsValid = false;
                                                        }
                                                    }//auditID != -1 end
                                                    else
                                                    {
                                                        cvDuplicateEntry.ErrorMessage = "something went wrong(auditID) please try again later";
                                                        cvDuplicateEntry.IsValid = false;
                                                    }
                                                }//Subprocessid != -1 end
                                                else
                                                {
                                                    cvDuplicateEntry.ErrorMessage = "something went wrong(SubProcessId) please try again later";
                                                    cvDuplicateEntry.IsValid = false;
                                                }
                                            }//ProcessId != -1 end
                                            else
                                            {
                                                cvDuplicateEntry.ErrorMessage = "something went wrong(ProcessId) please try again later";
                                                cvDuplicateEntry.IsValid = false;
                                            }
                                        }//Veritcalid != -1 end
                                        else
                                        {
                                            cvDuplicateEntry.ErrorMessage = "something went wrong(Veritcalid) please try again later";
                                            cvDuplicateEntry.IsValid = false;
                                        }
                                    }//BranchID != -1 end
                                    else
                                    {
                                        cvDuplicateEntry.ErrorMessage = "something went wrong(BranchID) please try again later";
                                        cvDuplicateEntry.IsValid = false;
                                    }
                                }//Query String Validations end
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}