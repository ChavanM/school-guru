﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="AuditStatusTransaction.aspx.cs"
     Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.AuditStatusTransaction" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href="../../assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="../../NewCSS/owl.carousel.css" type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href="../../NewCSS/fullcalendar.css" type="text/css" />
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="../../Newjs/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bs_leftnavi.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>


    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <%-- <script type="text/javascript" src="../../Newjs/jquery-combobox.js"></script> --%>
    <script type="text/javascript" src="../../Newjs/bs_leftnavi.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD50uFhROVysfZducuIgGl1ltBV3Hx-Ig8"></script>

    <title></title>  
    <script type="text/javascript">
        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }
    </script>
    <script type="text/javascript"> 
        
        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }

        function initializeConfirmDatePicker1(date) {
            var startDate1 = new Date();
            $('#<%= txtDateResp.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate1,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }

        function allowOnlyNumber(evt) {           
            var charCode = (evt.which) ? evt.which : event.keyCode            
            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;
            return true;
        };


          var validFilesTypes = ["exe", "bat", "dll"];
          function ValidateFile() {

              var label = document.getElementById("<%=Label1.ClientID%>");
            var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
            var FileuploadAdditionalFile = $("#<%=FileuploadAdditionalFile.ClientID%>").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }


            for (var i = 0; i < FileuploadAdditionalFile.length; i++) {
                var fileExtension = FileuploadAdditionalFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }

            if (!isValidFile) {
                label.style.color = "red";
                //label.innerHTML = "Invalid file uploded. .exe,.zip,.bat formats not supported.";
                label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
            }
            return isValidFile;
        }        
        function CloseWindow() {
            
            window.parent.CloseWin2();
        };
        

        function ConfirmtestViewICFRPER(ink) {

            $('#DocumentPopUpICFRPER').modal();
            $('#docViewerAllICFRPER').attr('src', "../../docviewer.aspx?docurl=" + ink);
        }

        $(document).ready(function () {
            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#DocumentPopUpICFRPER').modal('hide');
            });
        });

    </script>

    <style type="text/css">
        .tdAudit1 {
            width: 20%;
        }

        .tdAudit2 {
            width: 40%;
        }

        .tdAudit3 {
            width: 16%;
        }

        .tdAudit4 {
            width: 24%;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }
    </style>
</head>
<body>
    <form runat="server">
        <asp:ScriptManager ID="scriptManage" runat="server"></asp:ScriptManager>
        <div class="clearfix"></div>
        <div class="col-md-12 colpadding0">
                <div runat="server" id="LblPageDetails" style="color: #666"></div>
            </div>
            <div class="clearfix"></div>
        <div id="divComplianceDetailsDialog">
            <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true"
                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnAuditInstanceID" />
                            <asp:HiddenField runat="server" ID="hdnRiskCreatinID" />
                            <asp:HiddenField runat="server" ID="hdnAuditScheduledOnId" />
                        </div>
                        <div id="TOD" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTestofDesign">
                                                    <h2>Test of Design (TOD)</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)"  data-toggle="collapse" 
                                                        data-parent="#accordion" href="#collapseTestofDesign">
                                                        <i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseTestofDesign" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd;">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="vertical-align:top;" class="tdAudit1">
                                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Documents Examined</label>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtDocumentExamined" runat="server" CssClass="form-control" TextMode="MultiLine" Style="height: 80px; width: 100%"></asp:TextBox></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1" colspan="4" style="height: 5px;"></td>
                                                    </tr>
                                                    <tr>

                                                        <td style="vertical-align:top;" class="tdAudit1">
                                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Reason for Pass or Fail</label>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtReasonforPassorFail" runat="server" CssClass="form-control" TextMode="MultiLine" Style="height: 80px; width: 100%;"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1" colspan="4" style="height: 5px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align:top;" class="tdAudit1">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">*</label>
                                                            <label style="width: 140px; display: block; float: left; font-size: 13px; color: #333;">
                                                                TOD (Pass or Fail)</label>
                                                        </td>
                                                        <td class="tdAudit2">
                                                            <asp:DropDownList ID="ddlDesign" runat="server" AutoPostBack="true" CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlDesign_SelectedIndexChanged" Width="200px">
                                                                <asp:ListItem Value="-1">Select TOD</asp:ListItem>
                                                                <asp:ListItem Value="1">Pass</asp:ListItem>
                                                                <asp:ListItem Value="2">Fail</asp:ListItem>
                                                                <asp:ListItem Value="3">Pass With Mitigating Control</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RFVPerRes" ErrorMessage="Please select Test of Design (TOD)."
                                                                ControlToValidate="ddlDesign" InitialValue="-1"
                                                                runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />                                                           
                                                        </td>
                                                        <td class="tdAudit3"></td>
                                                        <td class="tdAudit4">
                                                            <asp:LinkButton ID="lbDownloadSample" Style="width: 290px; font-size: 12px; color: #333;"
                                                                runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />

                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div id="TOE" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTestofEffectiveness">
                                                    <h2>Test of Effectiveness (TOE) </h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" 
                                                        data-toggle="collapse" data-parent="#accordion" 
                                                        href="#collapseTestofEffectiveness"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseTestofEffectiveness" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                <table style="width: 100%; margin-top: 10px;">
                                                    <tr>
                                                        <td style="vertical-align:top;" class="tdAudit1">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">
                                                                &nbsp;</label>
                                                            <label style="width: 112px; display: block; float: left; font-size: 13px; color: #333;">
                                                                TOE (Pass or Fail)</label>
                                                        </td>
                                                        <td class="tdAudit2">
                                                            <asp:DropDownList ID="ddlOprationEffectiveness" runat="server" AutoPostBack="true" CssClass="form-control m-bot15" Width="200px" OnSelectedIndexChanged="ddlOprationEffectiveness_SelectedIndexChanged">
                                                                <asp:ListItem Value="-1">Select TOE</asp:ListItem>
                                                                <asp:ListItem Value="1">Pass</asp:ListItem>
                                                                <asp:ListItem Value="2">Fail</asp:ListItem>
                                                                <asp:ListItem Value="3">Pass With Extended Testing</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ErrorMessage="Please select Test of Effectiveness (TOE)." 
                                                                ControlToValidate="ddlOprationEffectiveness"
                                                                InitialValue!="-1"
                                                                runat="server" ID="RFVOE" ValidationGroup="ComplianceValidationGroup" 
                                                                Display="None" Enabled="false" />--%>

                                                         <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please select Test of Effectiveness (TOE)."
                                                                ControlToValidate="ddlOprationEffectiveness" InitialValue="-1"
                                                                runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" /> --%>                                                          
                                                        </td>
                                                        <td  class="tdAudit3">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">
                                                                &nbsp;</label>
                                                            <label style="width: 110px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Sample Tested</label>
                                                        </td>
                                                        <td class="tdAudit4">
                                                            <asp:TextBox ID="txtSampleTested" runat="server" CssClass="form-control" Style="width: 200px;" onkeypress="return allowOnlyNumber(event);" onpaste="return false;"></asp:TextBox>                                                            
                                                            <asp:CompareValidator ID="cvRootCost" runat="server" ControlToValidate="txtSampleTested"
                                                                 ErrorMessage="Only Numbers in Sample Tested"
                                                                ValidationGroup="ComplianceValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1" colspan="4" style="height: 5px;"></td>
                                                    </tr>
                                                    <tr>                                                        
                                                        <td  class="tdAudit1">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">
                                                                *</label>
                                                            <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Date</label>
                                                        </td>
                                                        <td class="tdAudit2">
                                                            <asp:TextBox runat="server" ID="tbxDate" CssClass="form-control" Style="width: 200px;" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Date." ControlToValidate="tbxDate"
                                                                runat="server" ID="RequiredFieldValidator1" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                        </td>
                                                        <td class="tdAudit3">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">
                                                                &nbsp;</label>
                                                            <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Deviation %</label></td>
                                                        <td class="tdAudit4">
                                                            <asp:TextBox ID="txtDeviation" runat="server" CssClass="form-control" Style="width: 200px;" 
                                                              onkeypress="return allowOnlyNumber(event);" onpaste="return false;"  ></asp:TextBox>
                                                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtDeviation"
                                                                 ErrorMessage="Only Numbers in  Deviation %"
                                                                ValidationGroup="ComplianceValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>                                                            
                                                        </td>                                                                                                               
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1" colspan="4" style="height: 5px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">
                                                                &nbsp;</label>
                                                            <label style="width: 112px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Remarks</label>
                                                        </td>
                                                       <td colspan="3">
                                                            <asp:TextBox runat="server" ID="tbxRemarks" CssClass="form-control" TextMode="MultiLine" Style="height: 80px; width: 100%;" />
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1" colspan="4" style="height: 5px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">
                                                                *</label>
                                                            <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Upload Testing Document(s)</label></td>
                                                        <td colspan="3">
                                                            <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="fuSampleFile"
                                                                runat="server" ID="rfvFile" ValidationGroup="ComplianceValidationGroup" Display="None" Enabled="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="divpersonresponsible" class="row Dashboard-white-widget" runat="server" visible="false">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsePersonresponsible">
                                                    <h2>Responsible Person</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize"  onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion"
                                                         href="#collapsePersonresponsible"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapsePersonresponsible" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td class="tdAudit1">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">*</label>
                                                            <label style="width: 140px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Person Responsible</label></td>
                                                        <td class="tdAudit2">
                                                            <asp:DropDownList ID="ddlPersonResponsible" CssClass="form-control m-bot15" runat="server" AutoPostBack="true"
                                                                Width="297px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="tdAudit3">
                                                            <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Date</label>
                                                        </td>
                                                        <td class="tdAudit4">
                                                            <asp:TextBox runat="server" ID="txtDateResp" CssClass="form-control" Style="width: 200px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1" style="vertical-align: top;">
                                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Management Response</label></td>
                                                        <td colspan="3">
                                                            <asp:TextBox runat="server" ID="txtMngntesponce" CssClass="form-control" TextMode="MultiLine" Style="height: 80px; width:100%;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div id="Review" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseReviewHistory">
                                                    <h2>Review History</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" 
                                                         data-toggle="collapse" data-parent="#accordion" href="#collapseReviewHistory"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseReviewHistory" style="margin-bottom: 7px; clear: both; margin-top: 10px;" >
                                            <asp:Panel ID="divReviewHistory" runat="server" Style="width: 100%">
                                                <div style="text-align: left;">
                                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">

                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                                        <asp:GridView runat="server" ID="GridRemarks" AutoGenerateColumns="false" AllowSorting="true"
                                                                            PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="riskID" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblriskID" runat="server" Text='<%# Eval("RiskCreationId") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Scheduleon" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblAuditScheduleOnId" runat="server" Text='<%# Eval("AuditScheduleOnID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Created By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Remarks">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Date">
                                                                                    <ItemTemplate>
                                                                                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="150px" HeaderText="Documents" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton ID="lblDownLoadfile" runat="server" Text='<%# ShowSampleDocumentName((string)Eval("Name")) %>' OnClick="DownLoadClick"></asp:LinkButton>
                                                                                                <asp:LinkButton ID="lblViewFile" runat="server" Text='<%# ShowSampleDocumentNameView((string)Eval("Name")) %>' OnClick="lblViewFile_Click"></asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                                                                <asp:PostBackTrigger ControlID="lblViewFile" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <PagerTemplate>
                                                                                <table style="display: none">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </PagerTemplate>
                                                                            <EmptyDataTemplate>
                                                                                No Records Found.
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdAudit1">
                                                                    <label style="width: 185px; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Review Remark</label>
                                                                </td>
                                                                <td class="tdAudit2">
                                                                    <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" TextMode="MultiLine" Style="height: 50px; width: 180px;"></asp:TextBox>
                                                                </td>
                                                                <td class="tdAudit3"></td>
                                                                <td class="tdAudit4">
                                                                    <asp:FileUpload ID="FileuploadAdditionalFile" runat="server" /></td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="margin-bottom: 7px; margin-left: 1%; margin-top: 10px;">
                            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" 
                                ValidationGroup="ComplianceValidationGroup"
                                CausesValidation="true" />
                            <%--<asp:Button Text="Close" runat="server" ID="btnCancel" OnClientClick="CloseWindow2();" 
                                CssClass="btn btn-primary" data-dismiss="modal" />--%>
                        </div>

                        <div id="LogDetails" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseLOG">
                                                    <h2>Log</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse"
                                                         data-parent="#accordion" href="#collapseLOG"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseLOG" style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                            <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"
                                                OnPageIndexChanging="grdTransactionHistory_PageIndexChanging" OnSorting="grdTransactionHistory_Sorting"
                                                DataKeyNames="AuditTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                                <Columns>
                                                    <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                    <asp:TemplateField HeaderText="Date">
                                                        <ItemTemplate>
                                                            <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Records Found.
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />                        
                    </div>
                </ContentTemplate>
                <Triggers>                    
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="lbDownloadSample" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
     <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

        <div class="modal fade" id="DocumentPopUpICFRPER" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <iframe src="about:blank" id="docViewerAllICFRPER" runat="server" width="100%" height="550px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
