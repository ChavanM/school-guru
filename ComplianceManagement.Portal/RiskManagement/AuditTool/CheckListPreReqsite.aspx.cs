﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class CheckListPreReqsite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                BindPerformerFilter(ddlPerformerFilter);
                BindReviewerFilter();
                BindProcessAudits();
                bindPageNumber();
                BindProcess(customerID);
                BindData();
            }
        }
        public static object FillProcessDropdownPerformerAndReviewer(int Customerid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.InternalControlAuditAssignments
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row.AuditID == AuditID
                         && row.UserID==AuthenticationHelper.UserID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillSubProcess(long Processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Subprocess
                             where row.IsDeleted == false && row.ProcessId == Processid
                             select row);
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        private void BindProcess(int CustomerID)
        {
            try
            {
                int auditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditID = Convert.ToInt32(Request.QueryString["AuditID"]);

                    var data = FillProcessDropdownPerformerAndReviewer(CustomerID, auditID);

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();

                    ddlSubProcess1.Items.Clear();
                    ddlProcess1.Items.Clear();

                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";

                    ddlProcess1.DataTextField = "Name";
                    ddlProcess1.DataValueField = "Id";

                    ddlProcess.DataSource = data;
                    ddlProcess.DataBind();

                    ddlProcess1.DataSource = data;
                    ddlProcess1.DataBind();

                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                    ddlProcess1.Items.Insert(0, new ListItem("Select Process", "-1"));

                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue));
                    }
                    if (!String.IsNullOrEmpty(ddlProcess1.SelectedValue))
                    {
                        BindSubProcess1(Convert.ToInt32(ddlProcess1.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess(long Processid)
        {
            try
            {
                int AuditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    var data = ProcessManagement.FillSubProcessUserWise(Processid, "P", AuditID, Portal.Common.AuthenticationHelper.UserID);
                    //var data = FillSubProcess(Processid);
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = data;
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess1(long Processid)
        {
            try
            {
                int AuditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    var data = ProcessManagement.FillSubProcessUserWise(Processid, "P", AuditID, Portal.Common.AuthenticationHelper.UserID);
                    ddlSubProcess1.Items.Clear();
                    ddlSubProcess1.DataTextField = "Name";
                    ddlSubProcess1.DataValueField = "Id";
                    ddlSubProcess1.DataSource = data;
                    ddlSubProcess1.DataBind();
                    ddlSubProcess1.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            else
            {
                ddlSubProcess.Items.Clear();
                ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
            }
            BindData();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Resetbuttonminisize();", true);
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Resetbuttonminisize();", true);
        }

        protected void ddlProcess1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlProcess1.SelectedValue))
            {
                if (ddlProcess1.SelectedValue != "-1")
                {
                    BindSubProcess1(Convert.ToInt32(ddlProcess1.SelectedValue));
                }
                else
                {
                    ddlSubProcess1.Items.Clear();
                    ddlSubProcess1.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            else
            {
                ddlSubProcess1.Items.Clear();
                ddlSubProcess1.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
            }
            BindProcessAudits();
        }
        protected void ddlSubProcess1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcessAudits();
        }
        private void BindData()
        {
            try
            {
                long processid = -1;
                long subprocessid = -1;
                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }
                int auditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    string FinYear = string.Empty;
                    var AuditLists = GetAudits(FinYear, auditID);
                    if (processid != -1)
                    {
                        AuditLists = AuditLists.Where(entry => entry.ProcessID == processid).ToList();
                    }
                    if (subprocessid != -1)
                    {
                        AuditLists = AuditLists.Where(entry => entry.SubprocessID == subprocessid).ToList();
                    }
                    grdRiskActivityMatrix.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdRiskActivityMatrix.DataBind();
                    AuditLists.Clear();
                    AuditLists = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<SP_PrequsiteReportData_Result> GetAudits(string FinYear, long auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<SP_PrequsiteReportData_Result> Masterrecord = new List<SP_PrequsiteReportData_Result>();

                entities.Database.CommandTimeout = 300;
                Masterrecord = (from row in entities.SP_PrequsiteReportData(auditID, Portal.Common.AuthenticationHelper.UserID)
                                select row).ToList();
                
                if (FinYear != "")
                    Masterrecord = Masterrecord.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                return Masterrecord;
            }
        }

        public void BindPerformerFilter(DropDownList ddl)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            ddl.Items.Clear();
            ddl.DataTextField = "Name";
            ddl.DataValueField = "ID";
            ddl.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("Auditee ", "-1"));

        }
        public void BindReviewerFilter()
        {

            int customerID = -1;
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            ddlReviewerFilter.Items.Clear();
            ddlReviewerFilter.DataTextField = "Name";
            ddlReviewerFilter.DataValueField = "ID";
            ddlReviewerFilter.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
            ddlReviewerFilter.DataBind();
            ddlReviewerFilter.Items.Insert(0, new ListItem("Reporting Manager", "-1"));
        }
        public void PerformerReviewerDropDownEnableDisable(DropDownList ddl, int auditID, int Checklistid, Label lbl, String flag, TextBox txt, List<Master_AuditPrerequisite> Records)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (Records.Count > 0)
                {
                    entities.Database.CommandTimeout = 300;
                    var docupload = (from row in entities.PrerequisiteTran_Upload
                                     where row.AuditID == auditID
                                     && row.checklistId == Checklistid
                                     && row.Status != 1
                                     select row).ToList();


                    List<long> Data = new List<long>();
                    if (flag == "A")
                    {
                        Data = (from row in Records
                                where row.ChecklistId == Checklistid
                                select (long)row.AuditeeId).Distinct().ToList();
                    }
                    else
                    {
                        Data = (from row in Records
                                where row.ChecklistId == Checklistid
                                select (long)row.BossId).Distinct().ToList();
                    }
                    if (Data.Count == 1)
                    {
                        if (Data.ElementAt(0) != 0)
                        {
                            ddl.Items.FindByValue(Data.ElementAt(0).ToString()).Selected = true;
                            if (docupload.Count > 0)
                            {
                                ddl.Enabled = false;
                                txt.Enabled = false;
                            }
                            else
                            {
                                ddl.Enabled = true;
                                txt.Enabled = true;
                            }

                            lbl.Visible = false;
                        }
                    }
                    else if (Data.Count > 1)
                    {
                        ddl.ClearSelection();
                        if (docupload.Count > 0)
                        {
                            ddl.Enabled = false;
                            ddl.Visible = false;

                            txt.Enabled = false;
                            txt.Visible = false;
                        }
                        else
                        {
                            ddl.Enabled = true;
                            ddl.Visible = true;

                            txt.Enabled = true;
                            txt.Visible = true;
                        }

                        if (lbl != null)
                        {
                            lbl.Text = string.Empty;
                            Data.ForEach(EachUserID =>
                            {
                                if (EachUserID != 0 && EachUserID != -1)
                                {

                                    var UserDetails = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(EachUserID));
                                    lbl.Text = lbl.Text + UserDetails.FirstName + " " + UserDetails.LastName + ", ";
                                }
                            });

                            lbl.Text = lbl.Text.Trim(',');
                            lbl.ToolTip = lbl.Text.Trim(',');
                            lbl.Visible = true;
                        }
                    }
                    else
                    {
                        txt.Enabled = true;
                        ddl.Enabled = true;
                        lbl.Visible = false;
                    }
                }
            }
        }
        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdProcessAudit_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                        Label lblAuditID = (e.Row.FindControl("lblAuditID") as Label);
                        Label lblChecklistid = (e.Row.FindControl("lblChecklistid") as Label);
                        Label lblPerformerName = (e.Row.FindControl("lblPerformerName") as Label);
                        Label lblReviewerName = (e.Row.FindControl("lblReviewerName") as Label);
                        DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                        DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                        TextBox txtStartDate = (e.Row.FindControl("txtStartDate") as TextBox);
                        if (lblStatus != null)
                        {
                            if (!string.IsNullOrEmpty(lblStatus.Text))
                            {
                                if (lblStatus.Text == "Submitted")
                                {
                                    lblStatus.Text = "";
                                    lblStatus.BackColor = Color.White;
                                }
                                else if (lblStatus.Text == "Open")
                                {
                                    lblStatus.Text = "";
                                    lblStatus.BackColor = Color.White;
                                }
                                else if (lblStatus.Text == "Re-Submitted")
                                {
                                    lblStatus.Text = "";
                                    lblStatus.BackColor = Color.Orange;
                                }
                                else if (lblStatus.Text == "Approved")
                                {
                                    lblStatus.Text = "";
                                    lblStatus.BackColor = Color.Green;
                                   
                                }
                                else if (lblStatus.Text == "Rejected")
                                {
                                    lblStatus.Text = "";
                                    lblStatus.BackColor = Color.Red;
                                    
                                }
                            }
                        }
                        List<Master_AuditPrerequisite> Records = new List<Master_AuditPrerequisite>();
                        if (HttpContext.Current.Cache["ProcessAuditPrerequisiteData"] == null)
                        {
                            int auditID = Convert.ToInt32(lblAuditID.Text);
                            Records = (from row in entities.Master_AuditPrerequisite.AsNoTracking()
                                       where row.AuditId == auditID
                                       select row).ToList();

                            HttpContext.Current.Cache.Insert("ProcessAuditPrerequisiteData", Records, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero); // add it to cache
                        }
                        else
                            Records = (List<Master_AuditPrerequisite>)HttpContext.Current.Cache["ProcessAuditPrerequisiteData"];


                        #region Audit
                        if (ddlPerformer != null)
                        {
                            ddlPerformer.Items.Clear();
                            ddlPerformer.DataTextField = "Name";
                            ddlPerformer.DataValueField = "ID";
                            ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlPerformer.DataBind();
                            ddlPerformer.Items.Insert(0, new ListItem("Auditee", "-1"));

                            if (lblAuditID != null)
                            {
                                PerformerReviewerDropDownEnableDisable(ddlPerformer, Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblChecklistid.Text), lblPerformerName, "A", txtStartDate, Records);
                            }
                        }

                        if (ddlReviewer != null)
                        {
                            ddlReviewer.Items.Clear();
                            ddlReviewer.DataTextField = "Name";
                            ddlReviewer.DataValueField = "ID";
                            ddlReviewer.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                            ddlReviewer.DataBind();
                            ddlReviewer.Items.Insert(0, new ListItem("Reporting Manager", "-1"));

                            if (lblAuditID != null)
                            {
                                PerformerReviewerDropDownEnableDisable(ddlReviewer, Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblChecklistid.Text), lblReviewerName, "B", txtStartDate, Records);
                            }
                        }

                        int checklistid = Convert.ToInt32(lblChecklistid.Text);
                        var timelinedetails = Records.Where(entry => entry.ChecklistId == checklistid).FirstOrDefault();
                        if (timelinedetails != null)
                        {
                            txtStartDate.Text = timelinedetails.Timeline != null ? Convert.ToDateTime(timelinedetails.Timeline).ToString("dd-MM-yyyy") : "";
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdProcessAudit.PageIndex = chkSelectedPage - 1;
            grdProcessAudit.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindProcessAudits();
        }
        protected void grdProcessAudit_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("SHOW_SCHEDULE"))
            {
                //CommandArgument = '<%# Eval("AuditID") +"," + Eval("ATBDID")+","+Eval("ChecklistID")  %>'
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (commandArgs.Length > 0)
                {
                    int AuditID = Convert.ToInt32(commandArgs[0]);
                    int ATBDID = Convert.ToInt32(commandArgs[1]);
                    long ChecklistID = Convert.ToInt32(commandArgs[2]);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDOCDialog(" + AuditID + "," + ATBDID + "," + ChecklistID + ");", true);
                }
            }
            else if (e.CommandName.Equals("Add_CheckList"))
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (commandArgs.Length > 0)
                {
                    //ViewState["AuditID"] = Convert.ToInt32(commandArgs[0]);
                    ViewState["ATBDID"] = Convert.ToInt32(commandArgs[1]);
                    tbxCheckListName.Text = string.Empty;
                    // ViewState["ChecklistID"] = Convert.ToInt32(commandArgs[2]);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowAddCheckList();", true);
                }
            }
        }
        public static void AddDetailsMasterAuditPrerequisites(Master_AuditPrerequisite MAP)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Master_AuditPrerequisite.Add(MAP);
                entities.SaveChanges();
            }

        }
        public static List<SP_PreRequsiteManualReminder_Result> GetPrerequsiteReminderEscalation(string flag, long processid, long subprocessid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var lstprerequsiteAssignedCurrentDate = (from row in entities.SP_PreRequsiteManualReminder(flag, processid, subprocessid, AuditID)
                                                         select row).ToList();

                return lstprerequsiteAssignedCurrentDate;
            }
        }
        protected void btnReminderEscalation_Click(object sender, EventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    //int chkValidationflag = 0;
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    foreach (GridViewRow rw in grdRiskActivityMatrix.Rows)
                    {
                        CheckBox chkBxrechild = (CheckBox)rw.FindControl("rechild");
                        CheckBox checkAll = (CheckBox)grdRiskActivityMatrix.HeaderRow.FindControl("checkAll");

                        CheckBox chkBxeschild = (CheckBox)rw.FindControl("eschild");
                        CheckBox escalationcheckAll = (CheckBox)grdRiskActivityMatrix.HeaderRow.FindControl("escalationcheckAll");

                        string lblAuditID = (rw.FindControl("lblAuditID") as Label).Text;
                        string lblProcessID = (rw.FindControl("lblProcessID") as Label).Text;
                        string lblsubprocessID = (rw.FindControl("lblsubprocessID") as Label).Text;
                        string lblPending = (rw.FindControl("lblPending") as Label).Text;

                        if (lblAuditID != null && lblProcessID != null && lblsubprocessID != null && lblPending != null)
                        {
                            string portalURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                portalURL = Urloutput.URL;
                            }
                            else
                            {
                                portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }

                            #region reminder
                            if (chkBxrechild != null && chkBxrechild.Checked)
                            {
                                if (lblPending != "0")
                                {
                                    var lstprerequsite = GetPrerequsiteReminderEscalation("REM", Convert.ToInt32(lblProcessID), Convert.ToInt32(lblsubprocessID), Convert.ToInt32(lblAuditID));
                                    if (lstprerequsite.Count > 0)
                                    {
                                        lstprerequsite.ForEach(entry =>
                                        {
                                            if (!string.IsNullOrEmpty(entry.Email))
                                            {
                                                var message = Settings.Default.EmailTemplate_PreRequsiteBefore2Days
                                                                  .Replace("@User", entry.Username)
                                                                  .Replace("@URL", Convert.ToString(portalURL))
                                                                  .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]));


                                                //var message = Settings.Default.EmailTemplate_PreRequsiteBefore2Days
                                                //                    .Replace("@User", entry.Username)
                                                //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                //                    .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]));

                                                string Subject = "Audit Pre-Requisite Reminder: Cheklist Timeline";
                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { entry.Email }).ToList(), null, null, Subject, message);
                                            }
                                        });
                                    }
                                }
                            }
                            #endregion

                            #region Escalation 
                            if (chkBxeschild != null && chkBxeschild.Checked)
                            {
                                if (lblPending != "0")
                                {
                                    var lstprerequsite = GetPrerequsiteReminderEscalation("ESC", Convert.ToInt32(lblProcessID), Convert.ToInt32(lblsubprocessID), Convert.ToInt32(lblAuditID));
                                    if (lstprerequsite.Count > 0)
                                    {
                                        lstprerequsite.ForEach(entry =>
                                        {
                                            if (!string.IsNullOrEmpty(entry.Email))
                                            {
                                                var message = Settings.Default.EmailTemplate_PreRequsiteEscalation
                                                                 .Replace("@User", entry.Username)
                                                                 .Replace("@URL", Convert.ToString(portalURL))
                                                                 .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]));


                                                //var message = Settings.Default.EmailTemplate_PreRequsiteEscalation
                                                //                   .Replace("@User", entry.Username)
                                                //                   .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                //                   .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]));

                                                string Subject = "Audit Pre-Requisite Reminder:Cheklist Escalation";
                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { entry.Email }).ToList(), null, null, Subject, message);
                                            }
                                        });
                                    }
                                }
                            }
                            #endregion
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool CreatePrerequisiteTran_UploadResult(PrerequisiteTran_Upload DraftClosureResult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.PrerequisiteTran_Upload.Add(DraftClosureResult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool Tran_PrerequisiteTran_UploadExists(PrerequisiteTran_Upload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.AuditID == ICR.AuditID
                             && row.ATBDId == ICR.ATBDId
                             && row.checklistId == ICR.checklistId
                             && row.FileName == ICR.FileName
                             && row.IsDeleted == false
                             && row.Status != 1
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool masterauditprerequisiteexists(Master_AuditPrerequisite objinstance)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (objinstance !=null)
                {
                    var query = (from row in entities.Master_AuditPrerequisite
                                 where row.AuditId==objinstance.AuditId
                                 && row.Atbdid == objinstance.Atbdid
                                 && row.ChecklistId == objinstance.ChecklistId                                                                
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }

            }
        }

        public static void Updatemasterauditprerequisite(Master_AuditPrerequisite objinstance)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                
                var RiskToUpdate = (from row in entities.Master_AuditPrerequisite
                                    where row.AuditId == objinstance.AuditId
                                         && row.Atbdid == objinstance.Atbdid
                                         && row.ChecklistId == objinstance.ChecklistId
                                    select row).FirstOrDefault();
                RiskToUpdate.IsActive = false;
                RiskToUpdate.AuditeeId = objinstance.AuditeeId;
                RiskToUpdate.BossId = objinstance.BossId;
                entities.SaveChanges();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool SaveFlag = false;
                #region 
                List<Master_AuditPrerequisite> tempauditpreList = new List<Master_AuditPrerequisite>();
                tempauditpreList.Clear();
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    foreach (GridViewRow g1 in grdProcessAudit.Rows)
                    {
                        #region
                        DropDownList ddlper = (g1.FindControl("ddlPerformer") as DropDownList);
                        DropDownList ddlrev = (g1.FindControl("ddlReviewer") as DropDownList);
                        List<int> Auditstepdetails = new List<int>();
                        if (ddlper != null && ddlper.Enabled && ddlrev != null && ddlrev.Enabled)
                        {

                            string lblAuditID = (g1.FindControl("lblAuditID") as Label).Text;
                            string lblatbdID = (g1.FindControl("lblatbdID") as Label).Text;
                            string lblChecklistid = (g1.FindControl("lblChecklistid") as Label).Text;
                            string lblPerformerUserID = (g1.FindControl("ddlPerformer") as DropDownList).SelectedItem.Value;
                            string lblReviewerUserID = (g1.FindControl("ddlReviewer") as DropDownList).SelectedItem.Value;
                            string txtStartDate = (g1.FindControl("txtStartDate") as TextBox).Text;
                            string lblChecklistDocument = (g1.FindControl("lblChecklistDocument") as Label).Text;

                            if (lblAuditID != "" && lblatbdID != "" && lblChecklistid != "")
                            {
                                if (lblPerformerUserID != "-1" && lblReviewerUserID !="-1")
                                {
                                    DateTime stdate = DateTime.ParseExact(txtStartDate, "dd-MM-yyyy", null);

                                    #region Audit save
                                    Master_AuditPrerequisite instance = new Master_AuditPrerequisite();
                                    instance.AuditId = Convert.ToInt32(lblAuditID);
                                    instance.Atbdid = Convert.ToInt32(lblatbdID);
                                    instance.ChecklistId = Convert.ToInt32(lblChecklistid);
                                    instance.AuditeeId = Convert.ToInt32(lblPerformerUserID);
                                    instance.BossId = Convert.ToInt32(lblReviewerUserID);
                                    instance.CreatedOn = DateTime.Now;
                                    instance.Createdby = AuthenticationHelper.UserID;
                                    instance.Timeline = stdate;
                                    if (!masterauditprerequisiteexists(instance))
                                    {
                                        AddDetailsMasterAuditPrerequisites(instance);

                                        PrerequisiteTran_Upload PTUinstance = new PrerequisiteTran_Upload()
                                        {
                                            CustomerId = AuthenticationHelper.CustomerID,
                                            ATBDId = Convert.ToInt32(lblatbdID),
                                            FileName = null,
                                            AuditID = Convert.ToInt32(lblAuditID),
                                            checklistId = Convert.ToInt32(lblChecklistid),
                                            checklistName = lblChecklistDocument,
                                            IsDeleted = false,
                                            FilePath = null,
                                            FileKey = null,
                                            Version = "0.0",
                                            VersionDate = DateTime.Now,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            Status = 1,
                                            StatusRemak = "New Audit Checklist Assigned.",
                                        };
                                        SaveFlag= CreatePrerequisiteTran_UploadResult(PTUinstance);
                                    }
                                    else
                                    {
                                        
                                        Updatemasterauditprerequisite(instance);
                                        SaveFlag = true;
                                    }
                                    #endregion
                                }
                            }
                        }//ddlper != null && ddlper.Enabled && ddlrev != null && ddlrev.Enabled
                        #endregion
                    } //foreach end                   
                }//using end
                BindProcessAudits();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdProcessAudit.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                if (SaveFlag)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Save Sucessfully.";
                }
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlPerformerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
              
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlPerformer = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlPerformer");
                    if (ddlPerformer.Enabled)
                    {
                        ddlPerformer.ClearSelection();
                        ddlPerformer.SelectedValue = ddlPerformerFilter.SelectedValue;
                        upPR.Update();
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "setTimeLine();", true);// added by Sagar More on 19-12-2019
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void txtStartDateKickoff_TextChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    TextBox txtStartDate = (TextBox)grdProcessAudit.Rows[i].FindControl("txtStartDate");
                    if (txtStartDate.Enabled)
                    {
                        txtStartDate.Text = "";
                        txtStartDate.Text = txtStartDateKickoff.Text;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlReviewerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlReviewer = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlReviewer");
                    if (ddlReviewer.Enabled)
                    {
                        ddlReviewer.ClearSelection();
                        ddlReviewer.SelectedValue = ddlReviewerFilter.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdProcessAudit.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindProcessAudits();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdProcessAudit.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected string getstatus(long AuditID, long ATBDID, long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.RecentPTUs
                             where row.AuditID == AuditID && row.ATBDId == ATBDID
                             && row.checklistId == ChecklistID
                             select row.Status).FirstOrDefault();
                if (query != null)
                {
                    return Convert.ToString(query);
                }
                else
                {
                    return "";
                }
            }
        }
        protected bool CheckActionButtonEnable(long AuditID, long ATBDID, long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.AuditID == AuditID && row.ATBDId == ATBDID
                             && row.checklistId == ChecklistID
                             && row.IsDeleted == false
                             && row.Status != 1
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public void BindProcessAudits()
        {
            try
            {
                string flag = string.Empty;
                string FinancialYear = string.Empty;
                int customerID = -1;
                long processid = -1;
                long subprocessid = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {

                        if (!String.IsNullOrEmpty(ddlProcess1.SelectedValue))
                        {
                            if (ddlProcess1.SelectedValue != "-1")
                            {
                                processid = Convert.ToInt32(ddlProcess1.SelectedValue);
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlSubProcess1.SelectedValue))
                        {
                            if (ddlSubProcess1.SelectedValue != "-1")
                            {
                                subprocessid = Convert.ToInt32(ddlSubProcess1.SelectedValue);
                            }
                        }

                        int AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                        lblaname.Text = (from row in entities.SP_GetAuditName(AuditID)
                                         select row).FirstOrDefault();
                        //aval

                        var query = (from row in entities.SP_prerequisiteData(AuditID, AuthenticationHelper.UserID)
                                     select row).ToList();

                        grdProcessAudit.DataSource = null;
                        grdProcessAudit.DataBind();

                        if (query != null)
                        {
                            if (processid != -1)
                            {
                                query = query.Where(entry => entry.ProcessId == processid).ToList();
                            }
                            if (subprocessid != -1)
                            {
                                query = query.Where(entry => entry.SubProcessId == subprocessid).ToList();
                            }

                            grdProcessAudit.DataSource = query;
                            Session["TotalRows"] = null;
                            Session["TotalRows"] = query.Count;
                            grdProcessAudit.DataBind();



                            if (query.Count > 0)
                                btnSave.Visible = true;
                            else
                                btnSave.Visible = false;
                        }
                    }

                    //if (HttpContext.Current.Cache.Get("ProcessAuditPrerequisiteData") != null)
                    //    HttpContext.Current.Cache.Remove("ProcessAuditPrerequisiteData");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void btnHiddenUpdatUser_Click(object sender, EventArgs e)
        {
            BindPerformerFilter(ddlPerformerFilter); BindReviewerFilter();
            BindProcessAudits();
        }

        protected void btnAddCheckList_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbxCheckListName.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ATBDID"])))
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        StepChecklistMapping objStepChecklistMapping = new StepChecklistMapping();
                        objStepChecklistMapping.ATBDID = Convert.ToInt64(ViewState["ATBDID"]);
                        objStepChecklistMapping.ChecklistDocument = tbxCheckListName.Text.Trim();
                        objStepChecklistMapping.IsActive = true;

                        var result = (from row in entities.StepChecklistMappings
                                      where row.ATBDID == objStepChecklistMapping.ATBDID &&
                                      row.ChecklistDocument == tbxCheckListName.Text.Trim()
                                      select row).FirstOrDefault();

                        if (result == null)
                        {
                            entities.StepChecklistMappings.Add(objStepChecklistMapping);
                            entities.SaveChanges();
                            cvChecklist.IsValid = false;
                            cvChecklist.ErrorMessage = "Document Name Added Successfully.";
                            tbxCheckListName.Text = string.Empty;
                            BindProcessAudits();
                        }
                        else
                        {
                            cvChecklist.IsValid = false;
                            cvChecklist.ErrorMessage = "Already same document name exist.";
                        }
                    }
                }
            }
            else
            {
                cvChecklist.IsValid = false;
                cvChecklist.ErrorMessage = "Document name should not be blank.";
            }
        }
    }
}