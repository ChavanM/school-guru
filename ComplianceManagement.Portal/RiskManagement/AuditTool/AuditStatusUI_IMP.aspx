﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditStatusUI_IMP.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.AuditStatusUI_IMP" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style>
        span#ContentPlaceHolder1_rdRiskActivityProcess > Label {
            margin-left: 5px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        input#ContentPlaceHolder1_rdRiskActivityProcess_1 {
            margin-left: 12px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            var filterbytype = ReadQuerySt('Status');
            if (filterbytype == '') {
                fhead('My Workspace');
            } else {
                //$('#pagetype').css("font-size", "20px")
                if (filterbytype == 'Open') {
                    filterbytype = 'Open Audits';
                } else if (filterbytype == 'Closed') {
                    filterbytype = 'Closed Audits';
                }
                fhead('My Workspace/ Implementation / ' + filterbytype);
                $('#pagetype').css('font-size', '21px');
            }
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.
            
            $(function () {
                $('input[id*=txtStartDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                });
            });

            $(function () {
                $('input[id*=txtEndDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                });
            });
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                              
                             <div class="clearfix"></div> 
                             <div class="panel-body">

                                 <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                           <%if (roles.Contains(3))%>
                                           <%{%>
                                        <li class="active" id="liPerformer" runat="server">
                                            <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                        </li>
                                           <%}%>
                                            <%if (roles.Contains(4))%>
                                           <%{%>
                                        <li class=""  id="liReviewer" runat="server">
                                            <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer" runat="server">Reviewer</asp:LinkButton>                                        
                                        </li>
                                          <%}%>
                                    </ul>
                                </header>   
                                 <div class="clearfix"></div> 
                                 <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="ComplianceInstanceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>

                                 <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5"/>
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" Selected="True" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                    </div>  
                                    <div class="col-md-3 colpadding0" style="color: #999; padding-top: 9px;width: 24%; display:none;" >
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" ForeColor="Black"
                                        RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged">
                                        <asp:ListItem Text="Process"  Value="Process" Selected="True" />
                                        <asp:ListItem Text="Non Process" Value="Non Process" />
                                    </asp:RadioButtonList>
                                </div>                         
                                 </div>   
                                                           
                                <div class="clearfix"></div>

                                 <div class="col-md-12 colpadding0">

                               <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit"  class="form-control m-bot15" Width="90%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"    AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit" class="form-control m-bot15" Width="90%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"   AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                   
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit" class="form-control m-bot15" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"     AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                      
                                </div> 
                                                                                                
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Sub Unit"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>                                     
                                     </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">                                       
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Sub Unit"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>
                                     <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                     <%{%>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                           <asp:DropDownListChosen runat="server" ID="ddlVertical" AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" DataPlaceHolder="Vertical" class="form-control m-bot15" Width="90%" Height="32px" OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged">
                                            </asp:DropDownListChosen>                                                        
                                        </div>   
                                    <%}%>           
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true" DataPlaceHolder="Financial Year" class="form-control m-bot15" Width="90%" Height="32px"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:CompareValidator ErrorMessage="Please Select Financial Year." ControlToValidate="ddlFinancialYear"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" Display="None" />                    
                                    </div> 
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true" DataPlaceHolder="Schedule"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>
                                    </div> 
                                 <div class="clearfix"></div> 
                                 <div class="col-md-12 colpadding0"> 
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true" DataPlaceHolder="Period"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>                    
                                 </div>
                                                                     
                                <div class="clearfix"></div>  
                                                                 
                                <div style="margin-bottom: 4px">       
                                    <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" 
                                    OnSorting="grdRiskActivityMatrix_Sorting" OnRowDataBound="grdRiskActivityMatrix_RowDataBound" 
                                    PageSize="20" AllowPaging="true" AutoPostBack="true" ShowHeaderWhenEmpty="true"
                                    CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                    OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging" ShowFooter="true" OnRowCommand="grdRiskActivityMatrix_RowCommand">
                                    <Columns>

                                       <%-- <asp:TemplateField HeaderText="Location" Visible="false">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                          <asp:TemplateField HeaderText="Process" Visible="false">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("CProcessName") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CProcessName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Financial Year"  Visible="false">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblFinYear" runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Period"  Visible="false">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                         
                                         <asp:TemplateField HeaderText="Performer"  Visible="false">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblPerformer" runat="server" Text='<%# Eval("UserName") %>'  data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                                         </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                                                 
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%" HeaderText="Action"  Visible="false">
                                            <ItemTemplate>                       
                                                <asp:LinkButton ID="lnkAuditDetails" runat="server" CommandName="ViewAuditStatusSummary" CommandArgument='<%# Eval("CustomerBranchID") + "," + Eval("ForMonth") + "," + Eval("FinancialYear") +"," + Eval("RoleID") +"," + Eval("UserID")+","+Eval("VerticalId")+","+Eval("AuditID")  %>'
                                                    CausesValidation="false"><img src="../../Images/View-icon-new.png" alt="View Audit Details" title="View Audit Details" /></asp:LinkButton> 
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Audit Name">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; width: 200px;">
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("AuditName") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AuditName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="Total Observation" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskTotal" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Total;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID") %>'
                                                    Text='<%# Eval("Total") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Open Observations" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskNotDone" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "NotDone;"+ Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID") %>'
                                                    Text='<%# Eval("NotDone") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Submitted" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskCompleted" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Submitted;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID") %>'
                                                    Text='<%# Eval("Submited") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                           
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Team Review" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskTeamReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "TeamReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID") %>'
                                                    Text='<%# Eval("TeamReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Auditee Review" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblAuditeeReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "AuditeeReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID") %>'
                                                    Text='<%# Eval("AuditeeReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Final Review" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblFinalReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "FinalReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID") %>'
                                                    Text='<%# Eval("FinalReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Closed" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskClosed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Closed;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID") %>'
                                                    Text='<%# Eval("Closed") %>' ></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />  
                                        <PagerSettings Visible="false" />                     
                                    <PagerTemplate>                                        
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate> 
                                </asp:GridView>
                                                 <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                                </div>

                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">                                    
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page
                                          
                                        </p>
                                    </div>                                    
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>           
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
