﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="ARSReports.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.ARSReports" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
        
    </style>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 34px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clsheadergrid, .table tr th label {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        .clsheadergrid, .table tr th input {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }
    
      
       .nav-tabs li a {
       color: #0090CB;
    margin-right: 2px;
    line-height: 1.428571429;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    background-color: lightgray;
   
}

         

            

     
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('Audit Status Report');
            fhead('Audit Status Report');
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ARSReportUPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-md-12 colpadding0">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                            <header class="panel-heading tab-bg-primary ">
                                       <ul id="rblRole1" class="nav nav-tabs" style="margin: -10px -23px -11px 0;">
                                           <%if (roles.Contains(3) || (roles.Contains(4)))%>
                                           <%{%>
                                                    <%if (roles.Contains(3))%>
                                                    <%{%>      
                                                        <li class="active" id="liPerformer" runat="server">
                                                            <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                                        </li>
                                                    <%}%>
                                                    <%if (roles.Contains(4))%>
                                                    <%{%>      
                                                        <li class=""  id="liReviewer" runat="server">
                                                            <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                                        </li> 
                                                    <%}%>
                                                    
                                               <% if (AuditHeadOrManagerReport == null)%>
                                               <%{%>
                                                    <li style="float: right;">   
                                                        <div>                                                        
                                                                <button class="form-control m-bot15" type="button" style="background-color:none;" data-toggle="dropdown">More Reports
                                                                <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                                <ul class="dropdown-menu">                                                                                                                      
                                                                     <li><a href="../AuditTool/MyReportAudit.aspx">Final Audit</a></li>
                                                                     <li><a href="../InternalAuditTool/AuditObservationDraft.aspx">Draft Observation</a></li>                                                                    
                                                                     <li><a href="../InternalAuditTool/FrmObservationReportWord.aspx">Observation-Word</a></li>
                                                                     <li><a href="../InternalAuditTool/OpenObervationReport.aspx">Open Observation-Excel</a></li>
                                                                     <li><a href="../InternalAuditTool/SchedulingReports.aspx">Audit Scheduling</a></li>
                                                                     <li><a href="../InternalAuditTool/PastAuditUpload.aspx">Past Audit Reports</a></li>                                                                                                  
                                                                   <%--<li><a href="../InternalAuditTool/FrmMasterAuditObservationReport.aspx">Audit Observation</a></li>                                                    
                                                                    <li><a href="../InternalAuditTool/FrmInternalAuditReportInWord.aspx">Internal Observation Word</a></li>                                                                                                      
                                                                    <li><a href="../InternalAuditTool/InternalAuditReportInDocument.aspx">Internal Observation Revised</a></li> --%>                                                   
                                                                </ul>                                                       
                                                        </div>
                                                    </li>  
                                                <%}%>
                                           <%}%>                                                               
                                    </ul>
                                </header>  
                            <div class="clearfix"></div>                  
                        <div style="margin-bottom: 4px"/> 
                         <div class="col-md-12 colpadding0">
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                            <div class="col-md-2 colpadding0" style="width:30%">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                <asp:ListItem Text="5" Selected="True" />
                                <asp:ListItem Text="10" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                                 </div>
                             <div style="text-align:right;" >
                                <asp:LinkButton Text="Export to Excel" CssClass="btn btn-primary" runat="server" ID="btnExportExcel" OnClick="btnExportExcel_Click" />                     
                            </div>  
                        </div>          
                            
                            <div class="clearfix"></div>                    
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">                                    
                                <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                              AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                    
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                              AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                    
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                   
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                               AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                      
                            </div>                                                                                                 
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                    
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                               AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                    
                            </div>
                            </div>

                            <div class="clearfix">

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" DataPlaceHolder="Sub Unit" AutoPostBack="true"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged" class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>
                                </div> 
                                 <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                               <%{%> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                    <asp:DropDownListChosen runat="server" ID="ddlVerticalID" DataPlaceHolder="Verticals" AutoPostBack="true"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15 select_location" Width="90%" Height="32px" OnSelectedIndexChanged="ddlVerticalID_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                                 <%}%>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" DataPlaceHolder="Financial Year">
                                        </asp:DropDownListChosen>                    
                                    </div> 
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" class="form-control m-bot15 select_location" 
                                            Width="90%" Height="32px" DataPlaceHolder="Scheduling">
                                        </asp:DropDownListChosen>
                                    </div>  
                            </div>
                                <div class="clearfix">
                                    <div class="clearfix">

                                    <div class="col-md-12 colpadding0">                                                                    
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                            <asp:DropDownListChosen runat="server" ID="ddlPeriod" DataPlaceHolder="Period" AutoPostBack="true"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"
                                            class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                            </asp:DropDownListChosen>
                                        </div> 
                                
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlProcess" DataPlaceHolder="Process" AutoPostBack="true"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged" class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>
                                    </div> 
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                    <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged"  DataPlaceHolder="Sub Process">
                                    </asp:DropDownListChosen>                                       
                                    </div>                           
                                </div>
                            <div class="clearfix">
                                <div class="clearfix">
                            <div class="col-md-12 colpadding0">
                            <%--<div class="col-md-8 colpadding0 entrycount" style="margin-top: 5px;width:100% !important">  
                                <ul id="rbl" class="nav nav-tabs"> 
                                    <li class="active" id="liNotDone" runat="server">
                                        <asp:LinkButton Text="Not Done" runat="server" ID="btnNotDone"  class="active" OnClick="btnNotDone_Click" onmoueclick="this.style.backcolor='#F7F9F9'"  style="color:black"  />  
                                    </li> 
                                    <li class="" id="liSubmitted" runat="server">
                                        <asp:LinkButton Text="Submitted" runat="server" ID="btnSubmitted" class=""  OnClick="btnSubmitted_Click"  onmoueclick="this.style.background='#F7F9F9'" style="color:black" BackColor="LightGray" />
                                    </li>
                                    <li class="" id="liTeam" runat="server">
                                        <asp:LinkButton Text="Team Review" runat="server" ID="btnTeamReview" class=""  OnClick="btnTeamReview_Click" onmoueclick="this.style.background='#F7F9F9'" style="color:black" BackColor="LightGray"/>  
                                    </li>
                                    <li class="" id="liAuditee" runat="server">
                                        <asp:LinkButton Text="Auditee Review" runat="server" ID="btnAuditee" class=""  OnClick="btnAuditee_Click" onmoueclick="this.style.background='#F7F9F9'" style="color:black" BackColor="LightGray"/> 
                                    </li>
                                    <li class="" id="liFinal" runat="server">
                                        <asp:LinkButton Text="Final Review" runat="server" ID="btnFinalReview" class=""  OnClick="btnFinalReview_Click" onmoueclick="this.style.background='#F7F9F9'"  style="color:black" BackColor="LightGray"/>
                                    </li>
                                    <li class="" id="liClosed" runat="server">
                                        <asp:LinkButton Text="Closed" runat="server" ID="btnClosed" class=""  OnClick="btnClosed_Click" onmoueclick="this.style.background='#F7F9F9'" style="color:black" BackColor="LightGray"/>
                                    </li>
                                </ul>
                            </div>--%>
                                <div class="col-md-8 colpadding0 entrycount" style="margin-top: 5px;width:100% !important">  
                                <ul id="rbl" class="nav nav-tabs"> 
                                    <li class="active" id="liNotDone" runat="server">
                                        <asp:LinkButton Text="Not Done"  runat="server" ID="btnNotDone"  class="active" OnClick="btnNotDone_Click" />  
                                    </li> 
                                    <li class="" id="liSubmitted" runat="server">
                                        <asp:LinkButton Text="Submitted" runat="server" ID="btnSubmitted" class=""  OnClick="btnSubmitted_Click" />
                                    </li>
                                    <li class="" id="liTeam" runat="server">
                                        <asp:LinkButton Text="Team Review" runat="server" ID="btnTeamReview" class=""  OnClick="btnTeamReview_Click" />  
                                    </li>
                                    <li class="" id="liAuditee" runat="server">
                                        <asp:LinkButton Text="Auditee Review" runat="server" ID="btnAuditee" class=""  OnClick="btnAuditee_Click"/> 
                                    </li>
                                    <li class="" id="liFinal" runat="server">
                                        <asp:LinkButton Text="Final Review" runat="server" ID="btnFinalReview" class=""  OnClick="btnFinalReview_Click" />
                                    </li>
                                    <li class="" id="liClosed" runat="server">
                                        <asp:LinkButton Text="Closed" runat="server" ID="btnClosed" class=""  OnClick="btnClosed_Click"/>
                                    </li>
                                </ul>
                            </div>
                        </div>   
                                      
                            <div class="clearfix" style="margin-bottom:40px"></div>
                                  
                            <div style="margin-bottom: 4px">          
                                <asp:GridView runat="server" ID="grdARSReport" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                 PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" >
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                         <ItemTemplate>
                                           <%#Container.DataItemIndex+1 %>
                                         </ItemTemplate>
                                         </asp:TemplateField>
                                        <asp:TemplateField  HeaderText="Control No">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 60px;"> 
                                                <asp:Label ID="lblControlNo" runat="server" Text='<%# Eval("ControlNo") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ControlNo") %>'></asp:Label>    
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField  HeaderText="Location">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 90px;"> 
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Branch") %>' ></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField  HeaderText="Vertical">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 70px;"> 
                                                <asp:Label ID="lblVertical" runat="server" Text='<%# Eval("VerticalName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("VerticalName") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField  HeaderText="Financial Year">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 70px;"> 
                                                <asp:Label ID="lblFyear" runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                         <asp:TemplateField  HeaderText="Period">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 70px;"> 
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ForMonth") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                         <asp:TemplateField  HeaderText="Process">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 100px;"> 
                                                <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("ProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField  HeaderText="SubProcess">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 100px;"> 
                                                <asp:Label ID="lblSubProcess" runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField  HeaderText=" Audit Step">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 100px;"> 
                                                <asp:Label ID="lblStep" runat="server" Text='<%# Eval("ActivityToBeDone") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ActivityToBeDone") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid"   />
                                    <HeaderStyle CssClass="clsheadergrid"  />  
                                    <HeaderStyle BackColor="#ECF0F1" />          
                                      <PagerSettings Visible="false" />        
                    <PagerTemplate>
                        <%--<table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>--%>
                      </PagerTemplate>
                                     <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                      </asp:GridView>
                       
                            </div>
                                       <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page
<%--                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
