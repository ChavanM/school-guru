﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class InternalAuditControlAuditManager_ObservationList : System.Web.UI.Page
    {
        protected string FinYear;
        protected string Period;
        protected int BranchID;
        protected int ATBDID;
        protected int StatusID;
        protected int ProcessID;
        protected int VerticalID;
        protected int AuditID;
        public static string DocumentPath = "";
        protected int SubProcessID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomer();
                BindObservationCategory();
                BindUsers();
                BindFinancialYear();
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Request.QueryString["FinYear"];
                    if (FinYear != "")
                    {
                        ddlFinancialYear.ClearSelection();
                        ddlFinancialYear.Items.FindByText(FinYear).Selected = true;
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchID = Convert.ToInt32(Request.QueryString["BID"]);
                    if (BranchID != 0)
                    {
                        ddlFilterLocation.ClearSelection();
                        ddlFilterLocation.Items.FindByValue(BranchID.ToString()).Selected = true;
                        BindSchedulingType(BranchID);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    ViewState["AuditID"] = AuditID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                    ViewState["Processed"] = ProcessID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    ViewState["VerticalID"] = VerticalID;
                }

                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                    ViewState["ForMonth"] = Period;
                    String SType = GetSchedulingType(BranchID, ProcessID, FinYear, Period, AuditID);

                    if (SType != "")
                    {
                        ddlSchedulingType.ClearSelection();

                        if (SType == "A")
                            ddlSchedulingType.Items.FindByText("Annually").Selected = true;
                        else if (SType == "H")
                            ddlSchedulingType.Items.FindByText("Half Yearly").Selected = true;
                        else if (SType == "Q")
                            ddlSchedulingType.Items.FindByText("Quarterly").Selected = true;
                        else if (SType == "M")
                            ddlSchedulingType.Items.FindByText("Monthly").Selected = true;
                        else if (SType == "P")
                            ddlSchedulingType.Items.FindByText("Phase").Selected = true;
                        else if (SType == "S")
                            ddlSchedulingType.Items.FindByText("Special Audit").Selected = true;
                        else if (SType == "S")
                            ddlSchedulingType.Items.FindByText("Special Audit").Selected = true;
                        ddlSchedulingType_SelectedIndexChanged(null, null);

                        ddlPeriod.ClearSelection();
                        ddlPeriod.Items.FindByText(Period).Selected = true;
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                    ViewState["ATBDID"] = VerticalID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    ViewState["VerticalID"] = VerticalID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    StatusID = Convert.ToInt32(Request.QueryString["SID"]);

                    if (StatusID != 0)
                    {
                        ddlFilterStatus.ClearSelection();
                        ddlFilterStatus.Items.FindByValue(StatusID.ToString()).Selected = true;
                    }
                }
                tvProcessRisk_SelectedNodeChanged1(null, null);
                Tab1_Click(sender, e);
                if (!string.IsNullOrEmpty(Request.QueryString["SPID"]))
                {
                    SubProcessID = Convert.ToInt32(Request.QueryString["SPID"]);
                }
                //Page Title
                int customerID = -1;
                string FY = string.Empty;
                string Period1 = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["FY"])))
                {
                    FY = "/ " + Convert.ToString(Request.QueryString["FY"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ForMonth"])))
                {
                    Period1 = "/ " + Convert.ToString(Request.QueryString["ForMonth"]);
                }
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string ControlNo = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                {
                    ControlNo = "/ " + Convert.ToString(Request.QueryString["CID"]);
                }
                string Pname = string.Empty;
                string SPname = string.Empty;
                string VName = string.Empty;
                Mst_Process objprocess = new Mst_Process();
                mst_Subprocess objsubProcess = new mst_Subprocess();
                objprocess = ProcessManagement.GetByID(ProcessID, customerID);
                objsubProcess = ProcessManagement.GetSubProcessByID(SubProcessID);
                if (objprocess != null)
                {
                    Pname = "/ " + objprocess.Name;
                }
                if (objsubProcess != null)
                {
                    SPname = "/ " + objprocess.Name;
                }

                var BrachName = CustomerBranchManagement.GetByID(BranchID).Name;
                var VerticalName = UserManagementRisk.GetVerticalName(customerID, Convert.ToInt32((Request.QueryString["VID"])));
                if (string.IsNullOrEmpty(VerticalName))
                {
                    VName = "/ " + VerticalName;
                }
                LblPageDetails.InnerText = BrachName + FY + Period1 + Pname + SPname + VName + ControlNo;

                //End              
            }
            DateTime date = DateTime.MinValue;
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "active");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 0;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "active");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 1;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "active");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 2;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void Tab4_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "active");
            MainView.ActiveViewIndex = 3;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void tvProcessRisk_SelectedNodeChanged1(object sender, EventArgs e)
        {
            MainView.ActiveViewIndex = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
            {
                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
            }
            else
            {
                VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
            {
                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
            }
            else
            {
                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
            }
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(ddlSchedulingType.SelectedValue))
                    {
                        if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                        {
                            if (ddlFilterLocation.SelectedValue != "-1")
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    if (ddlSchedulingType.SelectedValue != "-1")
                                    {
                                        if (ddlPeriod.SelectedValue != "0")
                                        {
                                            var getscheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 4, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                                            if (getscheduleondetails != null)
                                            {
                                                lnkAuditCoverage.Enabled = true;
                                                lnkActualTesting.Enabled = true;
                                                lnkObservation.Enabled = true;
                                                lnkReviewLog.Enabled = true;
                                                TDTab.Visible = true;
                                                if (Convert.ToInt32(ddlFilterStatus.SelectedValue) != 1)
                                                {
                                                    BindTransactionDetails(Convert.ToInt32(getscheduleondetails.ID), Convert.ToInt32(getscheduleondetails.ProcessId), ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), Convert.ToInt32(getscheduleondetails.InternalAuditInstance), VerticalID, AuditID, ATBDID);
                                                }
                                                Tab1_Click(sender, e);
                                                var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Convert.ToInt32(ddlFilterLocation.SelectedValue));
                                                bool checkPRFlag = false;
                                                var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID), Convert.ToInt32(ddlFilterLocation.SelectedValue));
                                                if (checkPRFlag == true)
                                                {
                                                    bool checkPRRiskActivationFlag = false;
                                                    checkPRRiskActivationFlag = CustomerManagementRisk.CheckPersonResponsibleFlowApplicable(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Convert.ToInt32(ddlFilterLocation.SelectedValue));

                                                    //IF Status IS Closed And Open
                                                    if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 3 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 1)
                                                    {
                                                        txtAuditStepScore.Enabled = false;
                                                        txtObservation.Enabled = false;
                                                        txtAuditObjective.Enabled = false;
                                                        txtAuditSteps.Enabled = false;
                                                        txtAnalysisToBePerformed.Enabled = false;
                                                        txtWalkthrough.Enabled = false;
                                                        txtActualWorkDone.Enabled = false;
                                                        txtpopulation.Enabled = false;
                                                        txtSample.Enabled = false;
                                                        txtObservationNumber.Enabled = false;
                                                        txtObservationTitle.Enabled = false;
                                                        txtRisk.Enabled = false;
                                                        txtRootcost.Enabled = false;
                                                        txtfinancialImpact.Enabled = false;
                                                        txtRecommendation.Enabled = false;
                                                        txtMgtResponse.Enabled = false;
                                                        ddlPersonresponsible.Enabled = false;
                                                        ddlOwnerName.Enabled = false;
                                                        ddlobservationRating.Enabled = true;
                                                        ddlObservationCategory.Enabled = true;
                                                        ddlObservationSubCategory.Enabled = true;
                                                        btnNext1.Enabled = false;
                                                        btnNext2.Enabled = true;
                                                        btnNext3.Enabled = true;
                                                        rptComplianceDocumnets.Enabled = false;
                                                        GridRemarks.Enabled = false;
                                                        txtRemark.Enabled = false;
                                                        FileuploadAdditionalFile.Enabled = false;
                                                        ddlReviewerRiskRatiing.Enabled = false;
                                                        txtTimeLine.Enabled = false;
                                                        btnSave.Enabled = true;
                                                        ObjReport1.Enabled = false;
                                                        FileObjUpload.Enabled = false;
                                                        btnObjfileupload.Enabled = false;
                                                        tbxUHComment.Enabled = false;
                                                        tbxPresidentComment.Enabled = false;
                                                        ddlPersonresponsibleUH.Enabled = false;
                                                        ddlPersonresponsiblePresident.Enabled = false;
                                                        tbxBriefObservation.Enabled = false;
                                                        tbxObjBackground.Enabled = false;
                                                        ddlDefeciencyType.Enabled = false;
                                                        txtMultilineVideolink.Enabled = false;// added by sagar more on 09-01-2020
                                                        // btnAddTable.Enabled = false;
                                                        txtAnnexueTitle.Enabled = false;// added by sagar more on 10-01-2020
                                                        if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                                                        {
                                                            if (Convert.ToString(Request.QueryString["Type"]) == "AuditClosed")
                                                            {
                                                                EnableDisableControls(true);
                                                            }
                                                        }
                                                    }
                                                    else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 2 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6)
                                                    {
                                                        if (litrole.Contains(4))
                                                        {
                                                            EnableDisableControls(true);
                                                        }
                                                        else if ((AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH") && checkPRRiskActivationFlag)
                                                        {
                                                            if (litrole.Contains(5))
                                                            {
                                                                EnableDisableControls(true);
                                                            }
                                                            else
                                                            {
                                                                EnableDisableControls(false);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            EnableDisableControls(false);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        EnableDisableControls(true);
                                                    }
                                                }
                                                else
                                                {
                                                    if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 3)
                                                    {
                                                        txtAuditStepScore.Enabled = false;
                                                        txtObservation.Enabled = false;
                                                        txtAuditObjective.Enabled = false;
                                                        txtAuditSteps.Enabled = false;
                                                        txtAnalysisToBePerformed.Enabled = false;
                                                        txtWalkthrough.Enabled = false;
                                                        txtActualWorkDone.Enabled = false;
                                                        txtpopulation.Enabled = false;
                                                        txtSample.Enabled = false;
                                                        txtObservationNumber.Enabled = false;
                                                        txtObservationTitle.Enabled = false;
                                                        txtRisk.Enabled = false;
                                                        txtRootcost.Enabled = false;
                                                        txtfinancialImpact.Enabled = false;
                                                        txtRecommendation.Enabled = false;
                                                        txtMgtResponse.Enabled = false;
                                                        ddlPersonresponsible.Enabled = false;
                                                        ddlOwnerName.Enabled = false;
                                                        ddlobservationRating.Enabled = true;
                                                        ddlObservationCategory.Enabled = true;
                                                        ddlObservationSubCategory.Enabled = true;
                                                        btnNext1.Enabled = false;
                                                        btnNext2.Enabled = false;
                                                        btnNext3.Enabled = false;
                                                        FileObjUpload.Enabled = false;
                                                        btnObjfileupload.Enabled = false;
                                                        //GrdobservationFile.Enabled = false;
                                                        rptComplianceDocumnets.Enabled = false;
                                                        GridRemarks.Enabled = false;
                                                        txtRemark.Enabled = false;
                                                        FileuploadAdditionalFile.Enabled = false;
                                                        ddlReviewerRiskRatiing.Enabled = false;
                                                        txtTimeLine.Enabled = false;
                                                        btnSave.Enabled = false;
                                                        ObjReport1.Enabled = false;
                                                        tbxUHComment.Enabled = false;
                                                        tbxPresidentComment.Enabled = false;
                                                        ddlPersonresponsibleUH.Enabled = false;
                                                        ddlPersonresponsiblePresident.Enabled = false;
                                                        tbxBriefObservation.Enabled = false;
                                                        tbxObjBackground.Enabled = false;
                                                        ddlDefeciencyType.Enabled = false;
                                                        // btnAddTable.Enabled = false;
                                                        txtMultilineVideolink.Enabled = false;// added by sagar more on 09-01-2020
                                                        txtAnnexueTitle.Enabled = false;// added by sagar more on 10-01-2020
                                                        if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                                                        {
                                                            if (Convert.ToString(Request.QueryString["Type"]) == "AuditClosed")
                                                            {
                                                                EnableDisableControls(true);
                                                            }
                                                        }
                                                    }
                                                    else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6
                                                    || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4
                                                    || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 1)
                                                    {
                                                        EnableDisableControls(false);
                                                    }
                                                    else
                                                    {
                                                        EnableDisableControls(true);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public void BindObservationSubCategory(int ObservationId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlObservationSubCategory.DataTextField = "Name";
            ddlObservationSubCategory.DataValueField = "ID";
            ddlObservationSubCategory.Items.Clear();
            ddlObservationSubCategory.DataSource = ObservationSubcategory.FillObservationSubCategory(ObservationId, customerID);
            ddlObservationSubCategory.DataBind();
            ddlObservationSubCategory.Items.Insert(0, new ListItem("Observation SubCategory", "-1"));
        }
        public static String GetSchedulingType(int CustBranchID, int processid, String FinYear, String Period, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Record = (from row in entities.InternalAuditSchedulings
                              where row.CustomerBranchId == CustBranchID
                              && row.Process == processid
                              && row.FinancialYear == FinYear
                              && row.TermName == Period
                              && row.AuditID == AuditID
                              select row.ISAHQMP).FirstOrDefault();

                if (Record != "")
                    return Record;
                else
                    return "";
            }
        }
        public static long GetReviewer(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.InternalControlAuditAssignments
                                  join row1 in entities.InternalAuditScheduleOns
                                  on row.ProcessId equals row1.ProcessId
                                  where row.InternalAuditInstance == row1.InternalAuditInstance && row.ProcessId == processid &&
                                  row1.ForMonth == ForPeriod && row1.FinancialYear == FinancialYear
                                  && row1.ID == ScheduledOnID && row.RoleID == 4
                                  && row.AuditID == AuditID
                                  select row.UserID).FirstOrDefault();

                return (long)statusList;
            }
        }
        public void BindSchedulingType(int BranchId)
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(BranchId);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("< Select Scheduling Type >", "-1"));
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            //ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("< Select Financial Year >", "-1"));
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void BindUsers()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            var AllUser = RiskCategoryManagement.FillUsers(customerID);

            ddlPersonresponsible.Items.Clear();
            ddlPersonresponsible.DataTextField = "Name";
            ddlPersonresponsible.DataValueField = "ID";
            ddlPersonresponsible.DataSource = AllUser;
            ddlPersonresponsible.DataBind();
            ddlPersonresponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));

            ddlPersonresponsibleUH.Items.Clear();
            ddlPersonresponsibleUH.DataTextField = "Name";
            ddlPersonresponsibleUH.DataValueField = "ID";
            ddlPersonresponsibleUH.DataSource = AllUser;
            ddlPersonresponsibleUH.DataBind();
            ddlPersonresponsibleUH.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));

            ddlPersonresponsiblePresident.Items.Clear();
            ddlPersonresponsiblePresident.DataTextField = "Name";
            ddlPersonresponsiblePresident.DataValueField = "ID";
            ddlPersonresponsiblePresident.DataSource = AllUser;
            ddlPersonresponsiblePresident.DataBind();
            ddlPersonresponsiblePresident.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));

            ddlOwnerName.Items.Clear();
            ddlOwnerName.DataTextField = "Name";
            ddlOwnerName.DataValueField = "ID";
            ddlOwnerName.DataSource = AllUser;
            ddlOwnerName.DataBind();
            ddlOwnerName.Items.Insert(0, new ListItem("Select Owner", "-1"));
        }
        public void BindCustomer()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem("< Select Location >", "-1"));
        }
        public void BindObservationCategory()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlObservationCategory.DataTextField = "Name";
            ddlObservationCategory.DataValueField = "ID";
            ddlObservationCategory.Items.Clear();
            ddlObservationCategory.DataSource = ObservationSubcategory.FillObservationCategory(customerID);
            ddlObservationCategory.DataBind();
            ddlObservationCategory.Items.Insert(0, new ListItem("< Select Observation Category >", "-1"));
        }
        protected void EnableDisableControls(bool Flag)
        {

            txtAuditStepScore.Enabled = Flag;
            txtObservation.Enabled = Flag;
            txtAuditObjective.Enabled = false;
            txtAuditSteps.Enabled = false;
            txtAnalysisToBePerformed.Enabled = false;
            txtWalkthrough.Enabled = Flag;
            txtActualWorkDone.Enabled = Flag;
            txtpopulation.Enabled = Flag;
            txtSample.Enabled = Flag;
            txtObservationNumber.Enabled = Flag;
            txtObservationTitle.Enabled = Flag;
            txtRisk.Enabled = Flag;
            txtRootcost.Enabled = Flag;
            txtfinancialImpact.Enabled = Flag;
            txtRecommendation.Enabled = Flag;
            txtMgtResponse.Enabled = Flag;
            btnNext1.Enabled = Flag;
            btnNext2.Enabled = Flag;
            btnNext3.Enabled = Flag;
            rptComplianceDocumnets.Enabled = Flag;
            GridRemarks.Enabled = Flag;
            txtRemark.Enabled = Flag;
            FileuploadAdditionalFile.Enabled = Flag;
            ddlReviewerRiskRatiing.Enabled = Flag;
            txtTimeLine.Enabled = Flag;
            btnSave.Enabled = Flag;
            ObjReport1.Enabled = Flag;
            tbxUHComment.Enabled = Flag;
            tbxPresidentComment.Enabled = Flag;
            ddlPersonresponsibleUH.Enabled = Flag;
            ddlPersonresponsiblePresident.Enabled = Flag;
            tbxBriefObservation.Enabled = Flag;
            tbxObjBackground.Enabled = Flag;
            ddlDefeciencyType.Enabled = Flag;
            txtMultilineVideolink.Enabled = Flag;
            txtAnnexueTitle.Enabled = Flag;
        }
        private void BindTransactionDetails(int ScheduledOnID, int processid, string FinancialYear, int CustomerBranchID, int InternalAuditInstance, int verticalid, int AuditID, int Atbtid)
        {
            try
            {
                txtObservation.Text = string.Empty;
                txtAuditObjective.Text = string.Empty;
                txtAuditSteps.Text = string.Empty;
                txtAnalysisToBePerformed.Text = string.Empty;
                txtWalkthrough.Text = string.Empty;
                txtActualWorkDone.Text = string.Empty;
                txtpopulation.Text = string.Empty;
                txtSample.Text = string.Empty;
                txtObservationNumber.Text = string.Empty;
                txtObservationTitle.Text = string.Empty;
                txtRisk.Text = string.Empty;
                txtRootcost.Text = string.Empty;
                txtfinancialImpact.Text = string.Empty;
                txtRecommendation.Text = string.Empty;
                txtMgtResponse.Text = string.Empty;
                ddlPersonresponsible.SelectedValue = "-1";
                ddlOwnerName.SelectedValue = "-1";
                ddlobservationRating.SelectedValue = "-1";
                ddlObservationCategory.SelectedValue = "-1";
                tbxRemarks.Text = string.Empty;
                txtAuditStepScore.Text = string.Empty;
                tbxBriefObservation.Text = string.Empty;
                tbxObjBackground.Text = string.Empty;
                ddlDefeciencyType.SelectedValue = "-1";

                tbxTable.Text = string.Empty;
                tbxUHComment.Text = string.Empty;
                tbxPresidentComment.Text = string.Empty;
                ddlPersonresponsibleUH.SelectedValue = "-1";
                ddlPersonresponsiblePresident.SelectedValue = "-1";
                txtMultilineVideolink.Text = string.Empty;// added by sagar more on 10-01-2020
                txtAnnexueTitle.Text = string.Empty;// added by sagar more on 10-01-2020

                var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(ScheduledOnID, Atbtid, Convert.ToInt32(ddlFilterStatus.SelectedValue), verticalid, AuditID);
                var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(ScheduledOnID), Atbtid);
                if (MstRiskResult != null)
                {
                    if (MstRiskResult.ISACPORMIS != null)
                    {
                        ObjReport1.SelectedValue = Convert.ToString(MstRiskResult.ISACPORMIS);
                    }
                    txtObservation.Text = MstRiskResult.Observation;
                    txtAuditObjective.Text = MstRiskResult.AuditObjective;
                    txtAuditSteps.Text = MstRiskResult.AuditSteps;
                    txtAnalysisToBePerformed.Text = MstRiskResult.AnalysisToBePerofrmed;
                    txtWalkthrough.Text = MstRiskResult.ProcessWalkthrough;
                    txtActualWorkDone.Text = MstRiskResult.ActivityToBeDone;
                    txtpopulation.Text = MstRiskResult.Population;
                    txtSample.Text = MstRiskResult.Sample;
                    txtObservationNumber.Text = MstRiskResult.ObservationNumber;
                    txtObservationTitle.Text = MstRiskResult.ObservationTitle;
                    txtAnnexueTitle.Text = MstRiskResult.AnnexueTitle; // added by sagar more on 09-01-2020
                    txtRisk.Text = MstRiskResult.Risk;
                    txtRootcost.Text = MstRiskResult.RootCost;
                    txtfinancialImpact.Text = Convert.ToString(MstRiskResult.FinancialImpact);
                    txtRecommendation.Text = MstRiskResult.Recomendation;
                    txtMgtResponse.Text = MstRiskResult.ManagementResponse;
                    txtAuditStepScore.Text = Convert.ToString(MstRiskResult.AuditScores);
                    tbxBriefObservation.Text = MstRiskResult.BriefObservation;
                    tbxObjBackground.Text = MstRiskResult.ObjBackground;
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.DefeciencyType)))
                    {
                        ddlDefeciencyType.SelectedValue = Convert.ToString(MstRiskResult.DefeciencyType);
                    }
                    txtTimeLine.Text = MstRiskResult.TimeLine != null ? MstRiskResult.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                    tbxRemarks.Text = MstRiskResult.FixRemark;
                    hidObservation.Value = MstRiskResult.Observation;
                    hidAuditObjective.Value = MstRiskResult.AuditObjective;
                    hidAuditSteps.Value = MstRiskResult.AuditSteps;
                    hidAnalysisToBePerformed.Value = MstRiskResult.AnalysisToBePerofrmed;
                    hidWalkthrough.Value = MstRiskResult.ProcessWalkthrough;
                    hidActualWorkDone.Value = MstRiskResult.ActivityToBeDone;
                    hidpopulation.Value = MstRiskResult.Population;
                    hidSample.Value = MstRiskResult.Sample;
                    hidObservationNumber.Value = MstRiskResult.ObservationNumber;
                    hidObservationTitle.Value = MstRiskResult.ObservationTitle;
                    hidRisk.Value = MstRiskResult.Risk;
                    hidRootcost.Value = MstRiskResult.RootCost;
                    hidfinancialImpact.Value = Convert.ToString(MstRiskResult.FinancialImpact);
                    hidRecommendation.Value = MstRiskResult.Recomendation;
                    hidMgtResponse.Value = MstRiskResult.ManagementResponse;
                    hidRemarks.Value = MstRiskResult.FixRemark;
                    hidAuditStepScore.Value = Convert.ToString(MstRiskResult.AuditScores);
                    hidISACPORMIS.Value = Convert.ToString(MstRiskResult.ISACPORMIS);
                    hidTimeLine.Value = MstRiskResult.TimeLine != null ? MstRiskResult.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                    //New Field
                    hidPersonResponsible.Value = Convert.ToString(MstRiskResult.PersonResponsible);
                    hidObservationRating.Value = Convert.ToString(MstRiskResult.ObservationRating);
                    hidObservationCategory.Value = Convert.ToString(MstRiskResult.ObservationCategory);
                    hidObservationSubCategory.Value = Convert.ToString(MstRiskResult.ObservationSubCategory);
                    hidOwner.Value = Convert.ToString(MstRiskResult.Owner);
                    hidUserID.Value = Convert.ToString(MstRiskResult.UserID);

                    hidUHPersonResponsible.Value = Convert.ToString(MstRiskResult.UHPersonResponsible);
                    hidPRESIDENTPersonResponsible.Value = Convert.ToString(MstRiskResult.PRESIDENTPersonResponsible);
                    hidUHComment.Value = Convert.ToString(MstRiskResult.UHComment);
                    hidPRESIDENTComment.Value = Convert.ToString(MstRiskResult.PRESIDENTComment);
                    hidBodyContent.Value = Convert.ToString(MstRiskResult.BodyContent);

                    tbxTable.Text = MstRiskResult.BodyContent;
                    tbxUHComment.Text = MstRiskResult.UHComment;
                    tbxPresidentComment.Text = MstRiskResult.PRESIDENTComment;
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.UHPersonResponsible)))
                    {
                        ddlPersonresponsibleUH.SelectedValue = Convert.ToString(MstRiskResult.UHPersonResponsible);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.PRESIDENTPersonResponsible)))
                    {
                        ddlPersonresponsiblePresident.SelectedValue = Convert.ToString(MstRiskResult.PRESIDENTPersonResponsible);
                    }

                    var getDated = RiskCategoryManagement.GetInternalAuditTransactionbyID(ScheduledOnID, Atbtid, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, 4, verticalid, AuditID);
                    if (getDated != null)
                    {
                        if (getDated.ReviewerRiskRating != null)
                        {
                            if (getDated.ReviewerRiskRating != -1)
                            {
                                ddlReviewerRiskRatiing.SelectedValue = Convert.ToString(getDated.ReviewerRiskRating);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.PersonResponsible)))
                    {
                        ddlPersonresponsible.SelectedValue = Convert.ToString(MstRiskResult.PersonResponsible);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.PersonResponsible)))
                    {
                        ddlOwnerName.SelectedValue = Convert.ToString(MstRiskResult.Owner);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationRating)))
                    {
                        ddlobservationRating.SelectedValue = Convert.ToString(MstRiskResult.ObservationRating);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationCategory)))
                    {
                        ddlObservationCategory.SelectedValue = Convert.ToString(MstRiskResult.ObservationCategory);
                        BindObservationSubCategory(Convert.ToInt32(MstRiskResult.ObservationCategory));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationSubCategory)))
                    {
                        ddlObservationSubCategory.SelectedValue = Convert.ToString(MstRiskResult.ObservationSubCategory);
                    }
                    BindObservationImages();
                }
                else if (MstRiskResult == null)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var checklistdetails = (from row in entities.RiskActivityToBeDoneMappings
                                                where row.ID == Atbtid
                                                select row).FirstOrDefault();
                        if (checklistdetails != null)
                        {
                            txtAuditSteps.Text = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.ActivityTobeDone)))
                            {
                                txtAuditSteps.Text = checklistdetails.ActivityTobeDone.Trim();
                            }
                            txtAuditObjective.Text = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.AuditObjective)))
                            {
                                txtAuditObjective.Text = checklistdetails.AuditObjective.Trim();
                            }

                            txtAnalysisToBePerformed.Text = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.Analyis_To_Be_Performed)))
                            {
                                txtAnalysisToBePerformed.Text = checklistdetails.Analyis_To_Be_Performed.Trim();
                            }
                        }
                    }
                }
                ViewState["ScheduledOnID"] = null;
                ViewState["ScheduledOnID"] = ScheduledOnID;
                lblNote.Visible = false;
                rptComplianceDocumnets.DataSource = null;
                rptComplianceDocumnets.DataBind();
                BindDocument(ScheduledOnID, Atbtid, AuditID);
                BindObservationDocument(ScheduledOnID, Atbtid, AuditID);
                BindDocumentAnnxeture(Convert.ToString(ddlPeriod.SelectedValue), Convert.ToString(ddlFinancialYear.SelectedItem.Text), Atbtid, processid, Convert.ToInt32(ddlFilterLocation.SelectedValue), verticalid, AuditID);
                bool checkPRFlag = false;
                checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID), Convert.ToInt32(ddlFilterLocation.SelectedValue));
                var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                if (checkPRFlag == true)
                {
                    BindStatusList(7, litrole.ToList(), AuditHeadOrManager);
                }
                else
                {
                    BindStatusList(5, litrole.ToList(), AuditHeadOrManager);
                }
                BindTransactions(ScheduledOnID, Atbtid, AuditID);
                BindRemarks(processid, ScheduledOnID, Atbtid, AuditID);

                if (GridRemarks.Rows.Count != 0)
                {
                    tbxRemarks.Enabled = false;
                }
                tbxDate.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";
                rdbtnStatus.Attributes.Add("disabled", "disabled");
                tbxRemarks.Attributes.Add("disabled", "disabled");
                tbxDate.Attributes.Add("disabled", "disabled");

                // code added by sagar more on 09-01-2020
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    string audioVideoLink = string.Empty;
                    List<ObservationAudioVideo> observationAudioVideoList = new List<ObservationAudioVideo>();
                    observationAudioVideoList = (from cs in entities.ObservationAudioVideos
                                                 where cs.ATBTID == ATBDID && cs.AuditId == AuditID
                                                 && cs.IsActive == true
                                                 select cs).ToList();
                    if (observationAudioVideoList.Count > 0)
                    {
                        foreach (ObservationAudioVideo item in observationAudioVideoList)
                        {
                            audioVideoLink = audioVideoLink + "," + item.AudioVideoLink;
                        }
                        audioVideoLink = audioVideoLink.TrimStart(',');
                    }
                    txtMultilineVideolink.Text = audioVideoLink;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRemarks(int ProcessId, int ScheduledOnID, int ATBDID, int AuditID)
        {
            try
            {
                GridRemarks.DataSource = Business.DashboardManagementRisk.GetInternalAllRemarks(ProcessId, ScheduledOnID, ATBDID, AuditID);
                GridRemarks.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindTransactions(int ScheduledOnID, int Atbtid, int AuditId)
        {
            try
            {
                grdTransactionHistory.DataSource = Business.DashboardManagementRisk.GetAllInternalAuditTransactions(ScheduledOnID, Atbtid, AuditId);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindStatusList(int statusID, List<int> rolelist, string isAuditmanager)
        {
            try
            {
                rdbtnStatus.Items.Clear();
                var statusList = AuditStatusManagement.GetInternalStatusList();
                List<InternalAuditStatu> allowedStatusList = null;
                List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.ID).ToList();

                if (statusID == 7)
                {
                    if (ddlFilterStatus.SelectedValue == "6")
                    {
                        if (isAuditmanager == "AM" || isAuditmanager == "AH")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (st.ID == 4 || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                        else
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (st.ID == 4 || st.ID == 6 || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }

                    }
                    else if (ddlFilterStatus.SelectedValue == "5")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((st.ID == 4) || (st.ID == 6) || (st.ID == 2))
                            {
                            }
                            else
                            {
                                rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                        }
                    }
                    else if (ddlFilterStatus.SelectedValue == "4")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 5))
                            {
                                if (!(st.ID == 2))
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                            {
                                rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                            else
                            {

                                if (!(st.ID == 5))
                                {
                                    if (!(st.ID == 2))
                                    {
                                        rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                        }
                    }
                }//Personresponsible End
                else
                {
                    foreach (InternalAuditStatu st in allowedStatusList)
                    {
                        if (!(st.ID == 6))
                        {
                            if (!(st.ID == 2))
                            {
                                rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                            }
                        }
                    }
                    lblStatus.Visible = allowedStatusList.Count > 0 ? true : false;
                }
                rdbtnStatus.Items.FindByText("Closed").Selected = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
        }
        protected void rptComplianceDocumnets_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                rptComplianceDocumnets.PageIndex = e.NewPageIndex;

                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }

                BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
                BindObservationDocument(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
                //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnetsAnnexure_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            rptComplianceDocumnetsAnnexure.PageIndex = e.NewPageIndex;

            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
            {
                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
            }
            else
            {
                VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
            {
                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
            }
            else
            {
                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
            {
                ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
            }
            else
            {
                ProcessID = Convert.ToInt32(ViewState["Processed"]);
            }
            BindDocumentAnnxeture(Convert.ToString(ddlPeriod.SelectedValue), Convert.ToString(ddlFinancialYear.SelectedItem.Text), ATBDID, ProcessID, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, AuditID);
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        #region Document Upload delete download view
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(Convert.ToInt32(hdlSelectedDocumentID.Value));
                Response.Buffer = true;
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();

                    }
                }


            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowSampleDocumentNameView(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "/View";
            }
            return processnonprocess;
        }
        protected void lblViewFile_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            Label lblID = (Label)gvr.FindControl("lblID");
            Label lblriskID = (Label)gvr.FindControl("lblriskID");
            Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
            Label lblATBDId = (Label)gvr.FindControl("lblATBDId");
            Label lblAuditId = (Label)gvr.FindControl("lblAuditId");

            List<InternalReviewHistory> fileData = GetFileData(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text), Convert.ToInt32(lblAuditId.Text));

            foreach (var file in fileData)
            {
                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                if (file.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles";
                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + FileData;

                    string extension = System.IO.Path.GetExtension(filePath);

                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                    }

                    string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    if (file.EnType == "M")
                    {
                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    else
                    {
                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    bw.Close();

                    string CompDocReviewPath = FileName;
                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestViewAM('" + CompDocReviewPath + "');", true);
                }
                break;
            }
        }
        public void DeleteFileAnnxeture(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Annxeture(fileId);
                if (file != null)
                {
                    string path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.FileName));
                    DashboardManagementRisk.DeleteFileAnnxeture(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindDocumentAnnxeture(string Period, string FinancialYear, int ATBDID, int ProcessId, int CustomerBranchId, int VerticalID, int AuditId)
        {
            try
            {
                List<Tran_AnnxetureUpload> ComplianceDocument = new List<Tran_AnnxetureUpload>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetTran_AnnxetureUpload(ATBDID, FinancialYear, Period, ProcessId, CustomerBranchId, VerticalID, AuditId).Where(entry => entry.Version.Equals("1.0")).ToList();
                if (ComplianceDocument != null)
                {
                    rptComplianceDocumnetsAnnexure.DataSource = ComplianceDocument.ToList();
                    rptComplianceDocumnetsAnnexure.DataBind();
                }
                else
                {
                    rptComplianceDocumnetsAnnexure.DataSource = null;
                    rptComplianceDocumnetsAnnexure.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void DownloadAnnxetureFile(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Annxeture(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.FileName);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void DeleteFile(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(fileId);
                if (file != null)
                {
                    string path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    DashboardManagementRisk.DeleteFile(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindDocument(int ScheduledOnID, int ATBDID, int AuditID)
        {
            try
            {
                List<GetInternalAuditDocumentsView> ComplianceDocument = new List<GetInternalAuditDocumentsView>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetInternalAuditDocumentsView(ScheduledOnID, ATBDID, AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                ComplianceDocument = ComplianceDocument.Where(e => e.TypeOfFile != "OB").ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.OrderBy(x => x.FileType).ToList();
                rptComplianceDocumnets.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindObservationDocument(int ScheduledOnID, int ATBDID, int AuditID)
        {
            try
            {
                ViewState["ScheduledOnID"] = ScheduledOnID;
                ViewState["ATBDID"] = ATBDID;
                ViewState["AuditID"] = AuditID;

                List<GetInternalAuditDocumentsView> ComplianceDocument = new List<GetInternalAuditDocumentsView>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetInternalAuditDocumentsView(ScheduledOnID, ATBDID, AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                ComplianceDocument = ComplianceDocument.Where(e => e.TypeOfFile == "OB").ToList();
                GrdobservationFile.DataSource = ComplianceDocument.OrderBy(x => x.FileType).ToList();
                GrdobservationFile.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<InternalReviewHistory> GetFileData(int id, int AuditScheduleOnId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.InternalReviewHistories
                                where row.ID == id
                                //&& row.InternalAuditInstance == InternalAuditInstance
                                && row.AuditID == AuditID
                                && row.AuditScheduleOnID == AuditScheduleOnId
                                select row).ToList();

                return fileData;
            }
        }
        protected void DownLoadClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblID = (Label)gvr.FindControl("lblID");
                Label lblriskID = (Label)gvr.FindControl("lblriskID");
                Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
                Label lblAuditId = (Label)gvr.FindControl("lblAuditId");
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<InternalReviewHistory> fileData = GetFileData(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text), Convert.ToInt32(lblAuditId.Text));
                    int i = 0;
                    string directoryName = "Review Comments";
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TestingDocument.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            // processnonprocess = ProcessManagement.GetClientnameName(branchid);
            return processnonprocess;
        }
        #endregion

        #region Grid Events
        public void rptComplianceDocumnets_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void rptComplianceDocumnetsAnnexure_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DownloadAnnuxture"))
                {
                    DownloadAnnxetureFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("DeleteAnnuxture"))
                {
                    DeleteFileAnnxeture(Convert.ToInt32(e.CommandArgument));
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                    {
                        VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    }
                    else
                    {
                        VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                    {
                        ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                    }
                    else
                    {
                        ProcessID = Convert.ToInt32(ViewState["Processed"]);
                    }
                    BindDocumentAnnxeture(Convert.ToString(ddlPeriod.SelectedValue), Convert.ToString(ddlFinancialYear.SelectedItem.Text), ATBDID, ProcessID, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, AuditID);
                }
                //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnetsAnnexure_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnetsAnnexure");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocdeletebutton = (LinkButton)e.Row.FindControl("lbtLinkDocbuttonAnnuxture");
            if (lbtLinkDocdeletebutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lbtLinkDocdeletebutton);
            }
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void rptComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                    }

                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
                    BindObservationDocument(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
                    upComplianceDetails1.Update();
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnets");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbutton");
            if (lbtLinkDocbutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lbtLinkDocbutton);
            }
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(fileID);

                    if (file.FilePath == null)
                    {
                        using (FileStream fs = File.OpenRead(Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name))))
                        {
                            int length = (int)fs.Length;
                            byte[] buffer;

                            using (BinaryReader br = new BinaryReader(fs))
                            {
                                buffer = br.ReadBytes(length);
                            }

                            Response.Buffer = true;
                            Response.Clear();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void GridRemarks_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                }
                else
                {
                    ProcessID = Convert.ToInt32(ViewState["Processed"]);
                }
                GridRemarks.PageIndex = e.NewPageIndex;
                BindRemarks(ProcessID, Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        #region Email
        //private void ProcessReminderOnAuditManagerAuditReviewer(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid,int AuditID)
        //{
        //    try
        //    {
        //        long userid = GetReviewer(ForPeriod, FinancialYear, ScheduledOnID, processid, AuditID);
        //        var user = UserManagement.GetByID(Convert.ToInt32(userid));
        //        if (user != null)
        //        {
        //            int customerID = -1;                   
        //            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //            string ReplyEmailAddressName = CustomerManagementRisk.GetByID(Convert.ToInt32(customerID)).Name;

        //            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //            string message = Properties.Settings.Default.EmailTemplate_AuditManagerAuditReviewer
        //                 .Replace("@User", username)
        //                 .Replace("@PeriodName", ForPeriod)
        //                 .Replace("@FinancialYear", FinancialYear)
        //                 .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                 .Replace("@From", ReplyEmailAddressName);

        //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Audit Reminder on Final review", message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        #endregion

        #region Button Click Events
        protected void btnNext1_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                long roleid = 4;
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                var getscheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getscheduleondetails != null)
                {
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getscheduleondetails.ID,
                        ProcessId = getscheduleondetails.ProcessId,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        ForPerid = ddlPeriod.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                        AnnexueTitle = txtAnnexueTitle.Text.Trim()// added by sagar more on 10-01-2020
                    };

                    //Code added by Sushant
                    if (tbxTable.Text.Trim() == "")
                        MstRiskResult.BodyContent = "";
                    else
                        MstRiskResult.BodyContent = tbxTable.Text.Trim();

                    if (tbxUHComment.Text.Trim() == "")
                        MstRiskResult.UHComment = "";
                    else
                        MstRiskResult.UHComment = tbxUHComment.Text.Trim();

                    if (tbxPresidentComment.Text.Trim() == "")
                        MstRiskResult.PRESIDENTComment = "";
                    else
                        MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();

                    if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                    {
                        if (ddlPersonresponsibleUH.SelectedValue == "-1")
                        {
                            MstRiskResult.UHPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                    {
                        if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                        }
                    }

                    //End

                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();

                    if (txtActualWorkDone.Text.Trim() == "")
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();

                    if (txtpopulation.Text.Trim() == "")
                        MstRiskResult.Population = "";
                    else
                        MstRiskResult.Population = txtpopulation.Text.Trim();

                    if (txtSample.Text.Trim() == "")
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = txtSample.Text.Trim();

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();

                    // added by sagar more on 09-01-2020
                    if (txtAnnexueTitle.Text.Trim() == "")
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        if (grdObservationImg.Rows.Count > 0 || !string.IsNullOrEmpty(tbxTable.Text.Trim()) || !string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                        }
                        else if (grdObservationImg.Rows.Count == 0 && string.IsNullOrEmpty(tbxTable.Text.Trim()) && string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            CompareValidator1.IsValid = false;
                            CompareValidator1.ErrorMessage = "Please Draw Table or Upload Images or Add Audio/Video link.";
                            return;
                        }
                    }

                    if (txtObservation.Text.Trim() == "")
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = txtObservation.Text.Trim();

                    if (txtRisk.Text.Trim() == "")
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = txtRisk.Text.Trim();

                    if (txtRootcost.Text.Trim() == "")
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();

                    if (txtAuditStepScore.Text.Trim() == "")
                        MstRiskResult.AuditScores = null;
                    else
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());

                    if (txtfinancialImpact.Text.Trim() == "")
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim(); // changed by sagar more on 24-01-2020

                    if (txtRecommendation.Text.Trim() == "")
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();

                    if (txtMgtResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();

                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                            MstRiskResult.PersonResponsible = null;
                        else
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                            MstRiskResult.Owner = null;
                        else
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {

                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }
                    var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(getscheduleondetails.ID), ATBDID);
                    if (RecentComplianceTransaction != null)
                    {
                        tbxDate.Text = RecentComplianceTransaction.Dated != null ? RecentComplianceTransaction.Dated.Value.ToString("dd-MM-yyyy") : " ";
                    }

                    bool Success = true;

                    MstRiskResult.AStatusId = 5;
                    MstRiskResult.AuditeeResponse = "HF";
                    MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    MstRiskResult.CreatedOn = DateTime.Now;
                    //Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);

                    if (Success)
                    {
                        //SaveFlag = true;
                        Tab2_Click(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnNext2_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                long roleid = 4;
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                var getscheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getscheduleondetails != null)
                {
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getscheduleondetails.ID,
                        ProcessId = getscheduleondetails.ProcessId,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        ForPerid = ddlPeriod.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                        AuditeeResponse = "HF",

                    };

                    // added by sagar more on 22-01-2020
                    if (txtpopulation.Text.Length > 200)
                    {
                        CustomValidator4.IsValid = false;
                        CustomValidator4.ErrorMessage = "Population should not greater than 200 characters.";
                        return;
                    }

                    //Code added by Sushant
                    if (tbxTable.Text.Trim() == "")
                    {
                        MstRiskResult.BodyContent = "";
                    }
                    else
                    {
                        MstRiskResult.BodyContent = tbxTable.Text.Trim();
                    }
                    if (tbxUHComment.Text.Trim() == "")
                    {
                        MstRiskResult.UHComment = "";
                    }
                    else
                    {
                        MstRiskResult.UHComment = tbxUHComment.Text.Trim();
                    }

                    if (tbxPresidentComment.Text.Trim() == "")
                    {
                        MstRiskResult.PRESIDENTComment = "";
                    }
                    else
                    {
                        MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                    {
                        if (ddlPersonresponsibleUH.SelectedValue == "-1")
                        {
                            MstRiskResult.UHPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                    {
                        if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                        }
                    }
                    //End

                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();

                    if (txtActualWorkDone.Text.Trim() == "")
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();

                    if (txtpopulation.Text.Trim() == "")
                        MstRiskResult.Population = "";
                    else
                        MstRiskResult.Population = txtpopulation.Text.Trim();

                    if (txtSample.Text.Trim() == "")
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = txtSample.Text.Trim();

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();

                    // added by sagar more on 10-01-2020
                    if (txtAnnexueTitle.Text.Trim() == "")
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        if (grdObservationImg.Rows.Count > 0 || !string.IsNullOrEmpty(tbxTable.Text.Trim()) || !string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                        }
                        else if (grdObservationImg.Rows.Count == 0 && string.IsNullOrEmpty(tbxTable.Text.Trim()) && string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            CompareValidator1.IsValid = false;
                            CompareValidator1.ErrorMessage = "Please Draw Table or Upload Images or Add Audio/Video link.";
                            return;
                        }
                    }

                    if (txtObservation.Text.Trim() == "")
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = txtObservation.Text.Trim();

                    if (txtRisk.Text.Trim() == "")
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = txtRisk.Text.Trim();

                    if (txtRootcost.Text.Trim() == "")
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();

                    if (txtAuditStepScore.Text.Trim() == "")
                        MstRiskResult.AuditScores = null;
                    else
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());

                    if (txtfinancialImpact.Text.Trim() == "")
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();// changed by sagar more on 24-01-2020

                    if (txtRecommendation.Text.Trim() == "")
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();

                    if (txtMgtResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();

                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                            //transaction.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            //transaction.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                        {
                            MstRiskResult.Owner = null;
                            //transaction.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            // transaction.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {
                            //transaction.ObservatioRating = null;
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            //transaction.ObservatioRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {
                            //transaction.ObservationCategory = null;
                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            //transaction.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }
                    var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(getscheduleondetails.ID), ATBDID);
                    if (RecentComplianceTransaction != null)
                    {
                        tbxDate.Text = RecentComplianceTransaction.Dated != null ? RecentComplianceTransaction.Dated.Value.ToString("dd-MM-yyyy") : " ";
                    }
                    bool Success1 = false;
                    //bool Success2 = false;

                    if (RiskCategoryManagement.InternalControlResultExists_ObservationList(MstRiskResult))
                    {
                        MstRiskResult.AStatusId = 3;
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateInternalControlResult_ObaservationList(MstRiskResult);
                    }
                    if (Success1)
                    {
                        //SaveFlag = true;
                        Tab3_Click(sender, e);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Oops....Error Occured During Saving, Please Try Again";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnNext3_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                long roleid = 4;
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                var getScheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getScheduleondetails != null)
                {
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getScheduleondetails.ID,
                        ProcessId = getScheduleondetails.ProcessId,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        ForPerid = ddlPeriod.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getScheduleondetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                        AuditeeResponse = "HF",
                        AnnexueTitle = txtAnnexueTitle.Text.Trim()// added by sagar more on 10-01-2020
                    };
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        ProcessId = getScheduleondetails.ProcessId,
                        SubProcessId = -1,
                        ForPeriod = ddlPeriod.SelectedItem.Text,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                    };

                    AuditClosure AuditClosureResult = new AuditClosure()
                    {
                        ProcessId = getScheduleondetails.ProcessId,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        ForPeriod = ddlPeriod.SelectedItem.Text,
                        CustomerbranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        ATBDId = ATBDID,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                        AnnexueTitle = txtAnnexueTitle.Text.Trim()// added by sagar more on 10-01-2020
                    };

                    ObservationHistory objHistory = new ObservationHistory()
                    {
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        RoleID = roleid,
                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        ATBTID = ATBDID,
                    };

                    // added by sagar more on 22-01-2020
                    if (txtpopulation.Text.Length > 200)
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Population should not greater than 200 characters.";
                        return;
                    }
                    DateTime dt1 = new DateTime();
                    objHistory.ObservationOld = hidObservation.Value;
                    objHistory.ProcessWalkthroughOld = hidObservation.Value;
                    objHistory.ActualWorkDoneOld = hidActualWorkDone.Value;
                    objHistory.PopulationOld = hidpopulation.Value;
                    objHistory.SampleOld = hidSample.Value;
                    objHistory.ObservationTitleOld = hidObservationTitle.Value;
                    objHistory.RiskOld = hidRisk.Value;
                    objHistory.RootCauseOld = hidRootcost.Value;
                    objHistory.FinancialImpactOld = hidfinancialImpact.Value;
                    objHistory.RecommendationOld = hidRecommendation.Value;
                    objHistory.ManagementResponseOld = hidMgtResponse.Value;
                    objHistory.RemarksOld = hidRemarks.Value;
                    objHistory.ScoreOld = hidAuditStepScore.Value;
                    //New Field

                    //Code added by Sushant

                    objHistory.OldBodyContent = hidBodyContent.Value;
                    objHistory.OldUHComment = hidUHComment.Value;
                    objHistory.OldPRESIDENTComment = hidPRESIDENTComment.Value;
                    if (!string.IsNullOrEmpty(hidUHPersonResponsible.Value))
                    {
                        objHistory.OldUHPersonResponsible = Convert.ToInt32(hidUHPersonResponsible.Value);
                    }
                    if (!string.IsNullOrEmpty(hidPRESIDENTPersonResponsible.Value))
                    {
                        objHistory.OldPRESIDENTPersonResponsible = Convert.ToInt32(hidPRESIDENTPersonResponsible.Value);
                    }

                    if (tbxTable.Text.Trim() == "")
                    {
                        MstRiskResult.BodyContent = "";
                        objHistory.BodyContent = "";
                        transaction.BodyContent = "";
                        AuditClosureResult.BodyContent = "";
                    }
                    else
                    {
                        MstRiskResult.BodyContent = tbxTable.Text.Trim();
                        objHistory.BodyContent = tbxTable.Text.Trim();
                        transaction.BodyContent = tbxTable.Text.Trim();
                        AuditClosureResult.BodyContent = tbxTable.Text.Trim();
                    }

                    if (tbxUHComment.Text.Trim() == "")
                    {
                        MstRiskResult.UHComment = "";
                        objHistory.UHComment = "";
                        transaction.UHComment = "";
                        AuditClosureResult.UHComment = "";
                    }
                    else
                    {
                        MstRiskResult.UHComment = tbxUHComment.Text.Trim();
                        objHistory.UHComment = tbxUHComment.Text.Trim();
                        transaction.UHComment = tbxUHComment.Text.Trim();
                        AuditClosureResult.UHComment = tbxUHComment.Text.Trim();
                    }

                    if (tbxPresidentComment.Text.Trim() == "")
                    {
                        MstRiskResult.PRESIDENTComment = "";
                        objHistory.PRESIDENTComment = "";
                        transaction.PRESIDENTComment = "";
                        AuditClosureResult.PRESIDENTComment = "";
                    }
                    else
                    {
                        MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                        objHistory.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                        transaction.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                        AuditClosureResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                    }

                    if (!string.IsNullOrEmpty(tbxBriefObservation.Text.Trim()))
                    {
                        MstRiskResult.BriefObservation = tbxBriefObservation.Text.Trim();
                        transaction.BriefObservation = tbxBriefObservation.Text.Trim();
                        AuditClosureResult.BriefObservation = tbxBriefObservation.Text.Trim();
                    }
                    else
                    {
                        MstRiskResult.BriefObservation = null;
                        transaction.BriefObservation = null;
                        AuditClosureResult.BriefObservation = null;
                    }
                    if (!string.IsNullOrEmpty(tbxObjBackground.Text.Trim()))
                    {
                        MstRiskResult.ObjBackground = tbxObjBackground.Text.Trim();
                        transaction.ObjBackground = tbxObjBackground.Text.Trim();
                        AuditClosureResult.ObjBackground = tbxObjBackground.Text.Trim();
                    }
                    else
                    {
                        MstRiskResult.ObjBackground = null;
                        transaction.ObjBackground = null;
                        AuditClosureResult.ObjBackground = null;
                    }

                    // added by sagar more on 10-01-2020
                    if (!string.IsNullOrEmpty(txtAnnexueTitle.Text.Trim()))
                    {
                        MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                        AuditClosureResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                    }
                    else
                    {
                        MstRiskResult.AnnexueTitle = null;
                        AuditClosureResult.AnnexueTitle = null;
                    }
                    if (ddlDefeciencyType.SelectedValue != "-1")
                    {
                        MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                        transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                        AuditClosureResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                    }
                    else
                    {
                        MstRiskResult.DefeciencyType = null;
                        transaction.DefeciencyType = null;
                        AuditClosureResult.DefeciencyType = null;
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                    {
                        if (ddlPersonresponsibleUH.SelectedValue == "-1")
                        {
                            MstRiskResult.UHPersonResponsible = null;
                            objHistory.UHPersonResponsible = null;
                            transaction.UHPersonResponsible = null;
                            AuditClosureResult.UHPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                            objHistory.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                            transaction.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                            AuditClosureResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                    {
                        if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = null;
                            objHistory.PRESIDENTPersonResponsible = null;
                            transaction.PRESIDENTPersonResponsible = null;
                            AuditClosureResult.PRESIDENTPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                            objHistory.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                            transaction.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                            AuditClosureResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                        }
                    }

                    //End
                    if (!string.IsNullOrEmpty(hidPersonResponsible.Value))
                    {
                        objHistory.PersonResponsibleOld = Convert.ToInt32(hidPersonResponsible.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationRating.Value))
                    {
                        objHistory.ObservationRatingOld = Convert.ToInt32(hidObservationRating.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationCategory.Value))
                    {
                        objHistory.ObservationCategoryOld = Convert.ToInt32(hidObservationCategory.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationSubCategory.Value))
                    {
                        objHistory.ObservationSubCategoryOld = Convert.ToInt32(hidObservationSubCategory.Value);
                    }
                    if (!string.IsNullOrEmpty(hidOwner.Value))
                    {
                        objHistory.OwnerOld = Convert.ToInt32(hidOwner.Value);
                    }
                    if (!string.IsNullOrEmpty(hidUserID.Value))
                    {
                        objHistory.UserOld = Convert.ToInt32(hidUserID.Value);
                    }
                    if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                    {
                        objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                    }
                    if (!string.IsNullOrEmpty(hidTimeLine.Value))
                    {
                        dt1 = DateTime.ParseExact(hidTimeLine.Value, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        objHistory.TimeLineOld = dt1.Date;
                    }

                    objHistory.Remarks = tbxRemarks.Text;
                    dt1 = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt1 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        objHistory.TimeLine = dt1.Date;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                    {
                        objHistory.AuditId = AuditID;
                    }

                    if (ObjReport1.SelectedItem != null)
                    {
                        MstRiskResult.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                        objHistory.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                        AuditClosureResult.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                    }

                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                    {
                        MstRiskResult.ProcessWalkthrough = "";
                        objHistory.ProcessWalkthrough = "";
                    }
                    else
                    {
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                        objHistory.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                    }

                    if (txtActualWorkDone.Text.Trim() == "")
                    {
                        MstRiskResult.ActivityToBeDone = "";
                        objHistory.ActualWorkDone = "";
                    }
                    else
                    {
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();
                        objHistory.ActualWorkDone = txtActualWorkDone.Text.Trim();
                    }

                    if (txtpopulation.Text.Trim() == "")
                    {
                        MstRiskResult.Population = "";
                        objHistory.Population = "";
                    }
                    else
                    {
                        MstRiskResult.Population = txtpopulation.Text.Trim();
                        objHistory.Population = txtpopulation.Text.Trim();
                    }
                    if (txtSample.Text.Trim() == "")
                    {
                        MstRiskResult.Sample = "";
                        objHistory.Sample = "";
                    }
                    else
                    {
                        MstRiskResult.Sample = txtSample.Text.Trim();
                        objHistory.Sample = txtSample.Text.Trim();
                    }

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();
                    if (txtObservationTitle.Text.Trim() == "")
                    {
                        MstRiskResult.ObservationTitle = "";
                        objHistory.ObservationTitle = "";
                        AuditClosureResult.ObservationTitle = "";
                    }
                    else
                    {
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();
                        objHistory.ObservationTitle = txtObservationTitle.Text;
                        AuditClosureResult.ObservationTitle = txtObservationTitle.Text;
                    }

                    // added by sagar more on 10-01-2020
                    if (txtAnnexueTitle.Text.Trim() == "")
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        if (grdObservationImg.Rows.Count > 0 || !string.IsNullOrEmpty(tbxTable.Text.Trim()) || !string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                        }
                        else if (grdObservationImg.Rows.Count == 0 && string.IsNullOrEmpty(tbxTable.Text.Trim()) && string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            CompareValidator1.IsValid = false;
                            CompareValidator1.ErrorMessage = "Please Draw Table or Upload Images or Add Audio/Video link.";
                            return;
                        }
                    }

                    if (txtObservation.Text.Trim() == "")
                    {
                        MstRiskResult.Observation = "";
                        objHistory.Observation = null;
                        AuditClosureResult.Observation = "";
                    }
                    else
                    {
                        MstRiskResult.Observation = txtObservation.Text.Trim();
                        objHistory.Observation = txtObservation.Text.Trim();
                        AuditClosureResult.Observation = txtObservation.Text.Trim();
                    }

                    if (txtRisk.Text.Trim() == "")
                    {
                        MstRiskResult.Risk = "";
                        objHistory.Risk = "";
                        AuditClosureResult.Risk = "";
                    }
                    else
                    {
                        MstRiskResult.Risk = txtRisk.Text.Trim();
                        objHistory.Risk = txtRisk.Text;
                        AuditClosureResult.Risk = txtRisk.Text;
                    }

                    if (txtRootcost.Text.Trim() == "")
                    {
                        MstRiskResult.RootCost = null;
                        objHistory.RootCause = null;
                        AuditClosureResult.RootCost = null;
                    }
                    else
                    {
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();
                        objHistory.RootCause = txtRootcost.Text.Trim();
                        AuditClosureResult.RootCost = txtRootcost.Text.Trim();
                    }

                    if (txtAuditStepScore.Text.Trim() == "")
                    {
                        MstRiskResult.AuditScores = null;
                        objHistory.Score = null;
                    }
                    else
                    {
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());
                        objHistory.Score = txtAuditStepScore.Text;
                    }

                    if (txtfinancialImpact.Text.Trim() == "")
                    {
                        MstRiskResult.FinancialImpact = null;
                        objHistory.FinancialImpact = null;
                        AuditClosureResult.FinancialImpact = null;
                    }
                    else
                    {
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();// changed by sagar more on 24-01-2020
                        objHistory.FinancialImpact = txtfinancialImpact.Text;
                        AuditClosureResult.FinancialImpact = txtfinancialImpact.Text.Trim();// changed by sagar more on 24-01-2020
                    }
                    if (txtRecommendation.Text.Trim() == "")
                    {
                        MstRiskResult.Recomendation = "";
                        objHistory.Recommendation = "";
                        AuditClosureResult.Recomendation = "";
                    }
                    else
                    {
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();
                        objHistory.Recommendation = txtRecommendation.Text.Trim();
                        AuditClosureResult.Recomendation = txtRecommendation.Text.Trim();
                    }

                    if (txtMgtResponse.Text.Trim() == "")
                    {
                        MstRiskResult.ManagementResponse = "";
                        objHistory.ManagementResponse = "";
                        AuditClosureResult.ManagementResponse = "";
                    }
                    else
                    {
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();
                        objHistory.ManagementResponse = txtMgtResponse.Text.Trim();
                        AuditClosureResult.ManagementResponse = txtMgtResponse.Text.Trim();
                    }
                    if (tbxRemarks.Text.Trim() == "")
                    {
                        MstRiskResult.FixRemark = "";
                    }
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                        AuditClosureResult.TimeLine = dt.Date;
                    }
                    else
                    {
                        MstRiskResult.TimeLine = null;
                        AuditClosureResult.TimeLine = null;
                    }
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                            objHistory.PersonResponsible = null;
                            AuditClosureResult.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            objHistory.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            AuditClosureResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                        {
                            MstRiskResult.Owner = null;
                            transaction.Owner = null;
                            objHistory.Owner = null;
                            AuditClosureResult.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            transaction.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            objHistory.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            AuditClosureResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationRating = null;
                            objHistory.ObservationRating = null;
                            AuditClosureResult.ObservationRating = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            objHistory.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            AuditClosureResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {
                            transaction.ObservationCategory = null;
                            MstRiskResult.ObservationCategory = null;
                            AuditClosureResult.ObservationCategory = null;
                            objHistory.ObservationCategory = null;
                            AuditClosureResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            transaction.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            AuditClosureResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            objHistory.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            AuditClosureResult.ObservationSubCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {
                            transaction.ObservationSubCategory = null;
                            MstRiskResult.ObservationSubCategory = null;
                            AuditClosureResult.ObservationSubCategory = null;
                            objHistory.ObservationSubCategory = null;
                            AuditClosureResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            transaction.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            AuditClosureResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            objHistory.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            AuditClosureResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }
                    var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(getScheduleondetails.ID), ATBDID);
                    if (RecentComplianceTransaction != null)
                    {
                        tbxDate.Text = RecentComplianceTransaction.Dated != null ? RecentComplianceTransaction.Dated.Value.ToString("dd-MM-yyyy") : " ";
                    }
                    bool Success1 = false;
                    bool Success2 = false;
                    bool Success3 = false;

                    if (RiskCategoryManagement.InternalControlResultExists_ObservationList(MstRiskResult))
                    {
                        MstRiskResult.AStatusId = 3;
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateInternalControlResult_ObaservationList(MstRiskResult);
                    }
                    if (RiskCategoryManagement.InternalAuditTxnExistsAuditManager_ObaservationList(transaction))
                    {
                        transaction.StatusId = 3;
                        transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        transaction.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.updateInternalAuditTxnObjCatergeriAM(transaction);
                    }
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {

                        var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                              where row.ProcessId == MstRiskResult.ProcessId
                                              && row.FinancialYear == MstRiskResult.FinancialYear
                                              && row.ForPerid == MstRiskResult.ForPerid
                                              && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                              && row.UserID == MstRiskResult.UserID
                                              && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                              && row.VerticalID == MstRiskResult.VerticalID
                                              && row.AuditID == MstRiskResult.AuditID
                                              select row.ID).OrderByDescending(x => x).FirstOrDefault();
                        if (RecordtoUpdate != null)
                        {
                            AuditClosureResult.ResultID = RecordtoUpdate;
                            if (RiskCategoryManagement.AuditClosureRecordExists(AuditClosureResult))
                            {
                                AuditClosureResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                AuditClosureResult.UpdatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.UpdateCategeriAuditClosure_ObservationList(AuditClosureResult);
                            }
                        }
                    }
                    if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                    {
                        RiskCategoryManagement.AddObservationHistory(objHistory);
                    }
                    if (Success1 == true)
                    {

                        Tab4_Click(sender, e);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                    if (Success1)
                    {
                        if (txtMultilineVideolink.Text.Trim() != "")
                        {
                            //List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(AuditID, ATBDID);
                            //if (observationAudioVideoList.Count > 0)
                            //{
                            //    string[] audioVideoLinks = txtMultilineVideolink.Text.Split(',');
                            //    var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                            //    if (filtered.Count > 0)
                            //    {
                            //        foreach (var item in filtered)
                            //        {
                            //            var comparisons = audioVideoLinks.Select(x => targetString.CompareTo(x));
                            //            RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, AuditID, ATBDID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                            //        }
                            //    }
                            //}

                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(AuditID, ATBDID);
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (var item in observationAudioVideoList)
                                {
                                    RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, AuditID, ATBDID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                }
                            }

                            ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                            {
                                AuditId = AuditID,
                                ATBTID = ATBDID,
                                IsActive = true,
                            };

                            if (txtMultilineVideolink.Text.Contains(',')) //checking entered single value or multiple values
                            {
                                string[] audioVideoLinks = txtMultilineVideolink.Text.Split(',');
                                int lenght = audioVideoLinks.Length;
                                string link = string.Empty;
                                for (int i = 0; i < audioVideoLinks.Length; i++)
                                {
                                    link = audioVideoLinks[i].ToString();
                                    objObservationAudioVideo.AudioVideoLink = link.Trim();
                                    if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                    {
                                        objObservationAudioVideo.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        objObservationAudioVideo.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                    }
                                    else
                                    {
                                        objObservationAudioVideo.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                        RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                    }
                                }
                            }
                            else
                            {
                                objObservationAudioVideo.AudioVideoLink = txtMultilineVideolink.Text.Trim();
                                if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                {
                                    objObservationAudioVideo.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    objObservationAudioVideo.CreatedOn = DateTime.Now;
                                    RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                }
                                else
                                {
                                    objObservationAudioVideo.AudioVideoLink = txtMultilineVideolink.Text.Trim();
                                    objObservationAudioVideo.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                    RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                }
                            }
                        }
                        else
                        {
                            // when user want delete all existing link then we will get null from txtMultilineVideolink
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(AuditID, ATBDID);
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                {
                                    RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, AuditID, ATBDID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                }

                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                #region
                if (!string.IsNullOrEmpty(txtObservation.Text.Trim()))
                {
                    if (string.IsNullOrEmpty(txtObservationTitle.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Observation Title.";
                        MainView.ActiveViewIndex = 2;
                        txtObservationTitle.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtRisk.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Risk.";
                        MainView.ActiveViewIndex = 2;
                        txtRisk.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtRootcost.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Root Cause.";
                        MainView.ActiveViewIndex = 2;
                        txtRootcost.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtfinancialImpact.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Financial Impact.";
                        MainView.ActiveViewIndex = 2;
                        txtfinancialImpact.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtRecommendation.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Recommendation.";
                        MainView.ActiveViewIndex = 2;
                        txtRecommendation.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtMgtResponse.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Management Response.";
                        MainView.ActiveViewIndex = 2;
                        txtMgtResponse.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter TimeLine.";
                        MainView.ActiveViewIndex = 2;
                        txtTimeLine.Focus();
                        return;
                    }
                    //else if (string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    else if (string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue) || ddlPersonresponsible.SelectedValue == "-1")
                    {
                        //if (ddlPersonresponsible.SelectedValue == "-1")
                        //{
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Select Person Responsible.";
                        MainView.ActiveViewIndex = 2;
                        return;
                        //}
                    }
                    //else if (string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    else if (string.IsNullOrEmpty(ddlOwnerName.SelectedValue) || ddlOwnerName.SelectedValue == "-1")
                    {
                        //if (ddlOwnerName.SelectedValue == "-1")
                        //{
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Select Owner.";
                        MainView.ActiveViewIndex = 2;
                        return;
                        //}
                    }
                    else if (string.IsNullOrEmpty(ddlobservationRating.SelectedValue) || ddlobservationRating.SelectedValue == "-1")
                    {

                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Select Observation Rating.";
                        MainView.ActiveViewIndex = 2;
                        return;
                    }
                    else if (string.IsNullOrEmpty(ddlObservationCategory.SelectedValue) || ddlObservationCategory.SelectedValue == "-1")
                    {

                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Select Observation Category.";
                        MainView.ActiveViewIndex = 2;
                        return;
                    }
                    //else if (string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue) || ddlObservationSubCategory.SelectedValue == "-1" || ddlObservationSubCategory.SelectedValue == "")
                    //{
                    //    CustomValidator2.IsValid = false;
                    //    CustomValidator2.ErrorMessage = "Please Select Observation Sub Category.";
                    //    MainView.ActiveViewIndex = 2;
                    //    return;
                    //}
                }
                #endregion

                // added by sagar more on 22-01-2020
                if (txtpopulation.Text.Length > 200)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Population should not greater than 200 characters.";
                    return;
                }

                long roleid = 4;

                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }

                bool Flag = false;
                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                // int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var getScheduledonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getScheduledonDetails != null)
                {
                    if (rdbtnStatus.SelectedValue != "")
                    {
                        #region 
                        int StatusID = 0;
                        if (lblStatus.Visible)
                            StatusID = Convert.ToInt32(rdbtnStatus.SelectedValue);
                        else
                            StatusID = Convert.ToInt32(ComplianceManagement.Business.CustomerManagementRisk.GetClosedTransaction(Convert.ToInt32(getScheduledonDetails.ID)).AuditStatusID);

                        InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                        {
                            AuditScheduleOnID = getScheduledonDetails.ID,
                            ProcessId = getScheduledonDetails.ProcessId,
                            FinancialYear = ddlFinancialYear.SelectedItem.Text,
                            ForPerid = ddlPeriod.SelectedItem.Text,
                            CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                            IsDeleted = false,
                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            ATBDId = ATBDID,
                            RoleID = roleid,
                            InternalAuditInstance = getScheduledonDetails.InternalAuditInstance,
                            VerticalID = VerticalID,
                            AuditID = AuditID,
                        };

                        InternalAuditTransaction transaction = new InternalAuditTransaction()
                        {
                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                            StatusChangedOn = GetDate(b.ToString("dd/MM/yyyy")),
                            AuditScheduleOnID = getScheduledonDetails.ID,
                            FinancialYear = ddlFinancialYear.SelectedItem.Text,
                            CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                            InternalAuditInstance = getScheduledonDetails.InternalAuditInstance,
                            ProcessId = getScheduledonDetails.ProcessId,
                            SubProcessId = -1,
                            ForPeriod = ddlPeriod.SelectedItem.Text,
                            ATBDId = ATBDID,
                            RoleID = roleid,
                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            VerticalID = VerticalID,
                            AuditID = AuditID,

                        };

                        AuditClosure AuditClosureResult = new AuditClosure()
                        {
                            ProcessId = getScheduledonDetails.ProcessId,
                            FinancialYear = ddlFinancialYear.SelectedItem.Text,
                            ForPeriod = ddlPeriod.SelectedItem.Text,
                            CustomerbranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                            ACStatus = 1,
                            AuditCommiteRemark = "",
                            ATBDId = ATBDID,
                            VerticalID = VerticalID,
                            AuditID = AuditID,
                            AuditCommiteFlag = 0,
                            AnnexueTitle = txtAnnexueTitle.Text.Trim()// added by sagar more on 10-01-2020
                        };
                        DateTime dt1 = new DateTime();
                        ObservationHistory objHistory = new ObservationHistory()
                        {
                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            RoleID = roleid,
                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            ATBTID = ATBDID
                        };
                        objHistory.ObservationOld = hidObservation.Value;
                        objHistory.ProcessWalkthroughOld = hidObservation.Value;
                        objHistory.ActualWorkDoneOld = hidActualWorkDone.Value;
                        objHistory.PopulationOld = hidpopulation.Value;
                        objHistory.SampleOld = hidSample.Value;
                        objHistory.ObservationTitleOld = hidObservationTitle.Value;
                        objHistory.RiskOld = hidRisk.Value;
                        objHistory.RootCauseOld = hidRootcost.Value;
                        objHistory.FinancialImpactOld = hidfinancialImpact.Value;
                        objHistory.RecommendationOld = hidRecommendation.Value;
                        objHistory.ManagementResponseOld = hidMgtResponse.Value;
                        objHistory.RemarksOld = hidRemarks.Value;
                        objHistory.ScoreOld = hidAuditStepScore.Value;
                        //New Field

                        //Code added by Sushant

                        objHistory.OldBodyContent = hidBodyContent.Value;
                        objHistory.OldUHComment = hidUHComment.Value;
                        objHistory.OldPRESIDENTComment = hidPRESIDENTComment.Value;
                        if (!string.IsNullOrEmpty(hidUHPersonResponsible.Value))
                        {
                            objHistory.OldUHPersonResponsible = Convert.ToInt32(hidUHPersonResponsible.Value);
                        }
                        if (!string.IsNullOrEmpty(hidPRESIDENTPersonResponsible.Value))
                        {
                            objHistory.OldPRESIDENTPersonResponsible = Convert.ToInt32(hidPRESIDENTPersonResponsible.Value);
                        }
                        if (!string.IsNullOrEmpty(tbxBriefObservation.Text.Trim()))
                        {
                            MstRiskResult.BriefObservation = tbxBriefObservation.Text.Trim();
                            transaction.BriefObservation = tbxBriefObservation.Text.Trim();
                            AuditClosureResult.BriefObservation = tbxBriefObservation.Text.Trim();
                        }
                        else
                        {
                            MstRiskResult.BriefObservation = null;
                            transaction.BriefObservation = null;
                            AuditClosureResult.BriefObservation = null;
                        }

                        // added by sagar more on 10-01-2020
                        if (!string.IsNullOrEmpty(txtAnnexueTitle.Text.Trim()))
                        {
                            MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                            AuditClosureResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                        }
                        else
                        {
                            MstRiskResult.AnnexueTitle = null;
                            AuditClosureResult.AnnexueTitle = null;
                        }

                        if (!string.IsNullOrEmpty(tbxObjBackground.Text.Trim()))
                        {
                            MstRiskResult.ObjBackground = tbxObjBackground.Text.Trim();
                            transaction.ObjBackground = tbxObjBackground.Text.Trim();
                            AuditClosureResult.ObjBackground = tbxObjBackground.Text.Trim();
                        }
                        else
                        {
                            MstRiskResult.ObjBackground = null;
                            transaction.ObjBackground = null;
                            AuditClosureResult.ObjBackground = null;
                        }
                        if (ddlDefeciencyType.SelectedValue != "-1")
                        {
                            MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                            transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                            AuditClosureResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                        }
                        else
                        {
                            MstRiskResult.DefeciencyType = null;
                            transaction.DefeciencyType = null;
                            AuditClosureResult.DefeciencyType = null;
                        }

                        if (tbxTable.Text.Trim() == "")
                        {
                            MstRiskResult.BodyContent = "";
                            objHistory.BodyContent = "";
                            transaction.BodyContent = "";
                            AuditClosureResult.BodyContent = "";
                        }
                        else
                        {
                            MstRiskResult.BodyContent = tbxTable.Text.Trim();
                            objHistory.BodyContent = tbxTable.Text.Trim();
                            transaction.BodyContent = tbxTable.Text.Trim();
                            AuditClosureResult.BodyContent = tbxTable.Text.Trim();
                        }


                        if (tbxUHComment.Text.Trim() == "")
                        {
                            MstRiskResult.UHComment = "";
                            objHistory.UHComment = "";
                            transaction.UHComment = "";
                            AuditClosureResult.UHComment = "";
                        }
                        else
                        {
                            MstRiskResult.UHComment = tbxUHComment.Text.Trim();
                            objHistory.UHComment = tbxUHComment.Text.Trim();
                            transaction.UHComment = tbxUHComment.Text.Trim();
                            AuditClosureResult.UHComment = tbxUHComment.Text.Trim();
                        }

                        if (tbxPresidentComment.Text.Trim() == "")
                        {
                            MstRiskResult.PRESIDENTComment = "";
                            objHistory.PRESIDENTComment = "";
                            transaction.PRESIDENTComment = "";
                            AuditClosureResult.PRESIDENTComment = "";
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                            objHistory.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                            transaction.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                            AuditClosureResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                        }

                        if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                        {
                            if (ddlPersonresponsibleUH.SelectedValue == "-1")
                            {
                                MstRiskResult.UHPersonResponsible = null;
                                objHistory.UHPersonResponsible = null;
                                transaction.UHPersonResponsible = null;
                                AuditClosureResult.UHPersonResponsible = null;
                            }
                            else
                            {
                                MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                                objHistory.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                                transaction.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                                AuditClosureResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                            }
                        }

                        if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                        {
                            if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                            {
                                MstRiskResult.PRESIDENTPersonResponsible = null;
                                objHistory.PRESIDENTPersonResponsible = null;
                                transaction.PRESIDENTPersonResponsible = null;
                                AuditClosureResult.PRESIDENTPersonResponsible = null;
                            }
                            else
                            {
                                MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                                objHistory.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                                transaction.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                                AuditClosureResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                            }
                        }

                        //End

                        if (!string.IsNullOrEmpty(hidPersonResponsible.Value))
                        {
                            objHistory.PersonResponsibleOld = Convert.ToInt32(hidPersonResponsible.Value);
                        }
                        if (!string.IsNullOrEmpty(hidObservationRating.Value))
                        {
                            objHistory.ObservationRatingOld = Convert.ToInt32(hidObservationRating.Value);
                        }
                        if (!string.IsNullOrEmpty(hidObservationCategory.Value))
                        {
                            objHistory.ObservationCategoryOld = Convert.ToInt32(hidObservationCategory.Value);
                        }
                        if (!string.IsNullOrEmpty(hidObservationSubCategory.Value))
                        {
                            objHistory.ObservationSubCategoryOld = Convert.ToInt32(hidObservationSubCategory.Value);
                        }
                        if (!string.IsNullOrEmpty(hidOwner.Value))
                        {
                            objHistory.OwnerOld = Convert.ToInt32(hidOwner.Value);
                        }
                        if (!string.IsNullOrEmpty(hidUserID.Value))
                        {
                            objHistory.UserOld = Convert.ToInt32(hidUserID.Value);
                        }
                        if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                        {
                            objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                        }
                        if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                        {
                            objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                        }
                        DateTime dt = new DateTime();
                        dt = new DateTime();
                        if (!string.IsNullOrEmpty(hidTimeLine.Value))
                        {
                            dt = DateTime.ParseExact(hidTimeLine.Value, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            objHistory.TimeLineOld = dt.Date;
                        }
                        objHistory.Remarks = tbxRemarks.Text;
                        dt1 = new DateTime();
                        if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                        {
                            dt1 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                            objHistory.TimeLine = dt1.Date;
                        }

                        //var AuditID = UserManagementRisk.GetAuditID(Convert.ToInt64(ViewState["Branch"]), Convert.ToInt64(ViewState["VerticalID"]), ViewState["FinYear"].ToString(), ViewState["ForMonth"].ToString());
                        if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                        {
                            objHistory.AuditId = AuditID;
                        }

                        if (ObjReport1.SelectedItem != null)
                        {
                            MstRiskResult.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                            objHistory.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                            AuditClosureResult.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                        }

                        string remark = string.Empty;
                        if (rdbtnStatus.SelectedItem.Text == "Closed")
                        {
                            transaction.StatusId = 3;
                            MstRiskResult.AStatusId = 3;
                            remark = "Audit Steps Closed.";
                            MstRiskResult.AuditeeResponse = "HC";
                        }
                        else if (rdbtnStatus.SelectedItem.Text == "Team Review")
                        {
                            transaction.StatusId = 4;
                            MstRiskResult.AStatusId = 4;
                            remark = "Audit Steps Under Team Review.";
                            MstRiskResult.AuditeeResponse = "HT";
                        }
                        else if (rdbtnStatus.SelectedItem.Text == "Final Review")
                        {
                            transaction.StatusId = 5;
                            MstRiskResult.AStatusId = 5;
                            remark = "Audit Steps Under Final Review.";
                            MstRiskResult.AuditeeResponse = "HF";
                        }
                        else if (rdbtnStatus.SelectedItem.Text == "Auditee Review")
                        {
                            transaction.StatusId = 6;
                            MstRiskResult.AStatusId = 6;
                            remark = "Audit Steps Under Auditee Review.";
                            MstRiskResult.AuditeeResponse = "RA";
                        }

                        if (txtAuditObjective.Text.Trim() == "")
                            MstRiskResult.AuditObjective = null;
                        else
                            MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                        if (txtAuditSteps.Text.Trim() == "")
                            MstRiskResult.AuditSteps = "";
                        else
                            MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                        if (txtAnalysisToBePerformed.Text.Trim() == "")
                            MstRiskResult.AnalysisToBePerofrmed = "";
                        else
                            MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                        if (txtWalkthrough.Text.Trim() == "")
                        {
                            MstRiskResult.ProcessWalkthrough = "";
                            objHistory.ProcessWalkthrough = "";
                        }
                        else
                        {
                            MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                            objHistory.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                        }

                        if (txtActualWorkDone.Text.Trim() == "")
                        {
                            MstRiskResult.ActivityToBeDone = "";
                            objHistory.ActualWorkDone = "";
                        }
                        else
                        {
                            MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();
                            objHistory.ActualWorkDone = txtActualWorkDone.Text.Trim();
                        }
                        if (txtpopulation.Text.Trim() == "")
                        {
                            MstRiskResult.Population = "";
                            objHistory.Population = "";
                        }
                        else
                        {
                            MstRiskResult.Population = txtpopulation.Text.Trim();
                            objHistory.Population = txtpopulation.Text.Trim();
                        }
                        if (txtSample.Text.Trim() == "")
                        {
                            MstRiskResult.Sample = "";
                            objHistory.Sample = "";
                        }
                        else
                        {
                            MstRiskResult.Sample = txtSample.Text.Trim();
                            objHistory.Sample = txtSample.Text.Trim();
                        }

                        if (txtObservationNumber.Text.Trim() == "")
                        {
                            MstRiskResult.ObservationNumber = "";
                            AuditClosureResult.ObservationNumber = "";
                        }
                        else
                        {
                            MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();
                            AuditClosureResult.ObservationNumber = txtObservationNumber.Text.Trim();
                        }

                        // added by sagar more on 10-01-2020
                        if (txtAnnexueTitle.Text.Trim() == "")
                        {
                            MstRiskResult.AnnexueTitle = "";
                            AuditClosureResult.AnnexueTitle = "";
                        }
                        else
                        {
                            if (grdObservationImg.Rows.Count > 0 || !string.IsNullOrEmpty(tbxTable.Text.Trim()) || !string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                            {
                                MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                                AuditClosureResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                            }
                            else if (grdObservationImg.Rows.Count == 0 && string.IsNullOrEmpty(tbxTable.Text.Trim()) && string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Draw Table or Upload Images or Add Audio/Video link.";
                                return;
                            }
                        }

                        if (txtObservationTitle.Text.Trim() == "")
                        {
                            MstRiskResult.ObservationTitle = "";
                            AuditClosureResult.ObservationTitle = "";
                            objHistory.ObservationTitle = "";
                        }
                        else
                        {
                            MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();
                            AuditClosureResult.ObservationTitle = txtObservationTitle.Text.Trim();
                            objHistory.ObservationTitle = txtObservationTitle.Text.Trim();
                        }


                        if (txtObservation.Text.Trim() == "")
                        {
                            MstRiskResult.Observation = "";
                            AuditClosureResult.Observation = "";
                            objHistory.Observation = "";
                        }
                        else
                        {
                            MstRiskResult.Observation = txtObservation.Text.Trim();
                            AuditClosureResult.Observation = txtObservation.Text.Trim();
                            objHistory.Observation = txtObservation.Text.Trim();
                        }

                        if (txtRisk.Text.Trim() == "")
                        {
                            MstRiskResult.Risk = "";
                            AuditClosureResult.Risk = "";
                            objHistory.Risk = "";
                        }
                        else
                        {
                            MstRiskResult.Risk = txtRisk.Text.Trim();
                            AuditClosureResult.Risk = txtRisk.Text.Trim();
                            objHistory.Risk = txtRisk.Text.Trim();
                        }

                        if (txtRootcost.Text.Trim() == "")
                        {
                            MstRiskResult.RootCost = null;
                            AuditClosureResult.RootCost = null;
                            objHistory.RootCause = null;
                        }
                        else
                        {
                            MstRiskResult.RootCost = txtRootcost.Text.Trim();
                            AuditClosureResult.RootCost = txtRootcost.Text.Trim();
                            objHistory.RootCause = txtRootcost.Text.Trim();
                        }

                        if (txtAuditStepScore.Text.Trim() == "")
                        {
                            MstRiskResult.AuditScores = null;
                            objHistory.Score = null;
                        }
                        else
                        {
                            MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());
                            objHistory.Score = txtAuditStepScore.Text;
                        }

                        if (txtfinancialImpact.Text.Trim() == "")
                        {
                            MstRiskResult.FinancialImpact = null;
                            AuditClosureResult.FinancialImpact = null;
                            objHistory.FinancialImpact = null;
                        }
                        else
                        {
                            MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();// changed by sagar more on 24-01-2020
                            AuditClosureResult.FinancialImpact = txtfinancialImpact.Text.Trim();// changed by sagar more on 24-01-2020
                            objHistory.FinancialImpact = txtfinancialImpact.Text;
                        }

                        if (txtRecommendation.Text.Trim() == "")
                        {
                            MstRiskResult.Recomendation = "";
                            AuditClosureResult.Recomendation = "";
                            objHistory.Recommendation = "";
                        }
                        else
                        {
                            MstRiskResult.Recomendation = txtRecommendation.Text.Trim();
                            AuditClosureResult.Recomendation = txtRecommendation.Text.Trim();
                            objHistory.Recommendation = txtRecommendation.Text.Trim();
                        }
                        if (txtMgtResponse.Text.Trim() == "")
                        {
                            MstRiskResult.ManagementResponse = "";
                            AuditClosureResult.ManagementResponse = "";
                            objHistory.ManagementResponse = "";
                        }
                        else
                        {
                            MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();
                            AuditClosureResult.ManagementResponse = txtMgtResponse.Text.Trim();
                            objHistory.ManagementResponse = txtMgtResponse.Text.Trim();
                        }

                        if (tbxRemarks.Text.Trim() == "")
                            MstRiskResult.FixRemark = "";
                        else
                            MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                        DateTime dt2 = new DateTime();
                        if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                        {
                            dt2 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.TimeLine = dt2.Date;
                            AuditClosureResult.TimeLine = dt2.Date;
                        }
                        else
                        {
                            MstRiskResult.TimeLine = null;
                            AuditClosureResult.TimeLine = null;
                        }
                        if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                        {
                            if (ddlPersonresponsible.SelectedValue == "-1")
                            {
                                MstRiskResult.PersonResponsible = null;
                                transaction.PersonResponsible = null;
                                AuditClosureResult.PersonResponsible = null;
                                objHistory.PersonResponsible = null;
                            }
                            else
                            {
                                MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                                transaction.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                                AuditClosureResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                                objHistory.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                        {
                            if (ddlOwnerName.SelectedValue == "-1")
                            {
                                MstRiskResult.Owner = null;
                                transaction.Owner = null;
                                AuditClosureResult.Owner = null;
                                objHistory.Owner = null;
                            }
                            else
                            {
                                MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                                transaction.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                                AuditClosureResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                                objHistory.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                        {
                            if (ddlobservationRating.SelectedValue == "-1")
                            {
                                transaction.ObservatioRating = null;
                                MstRiskResult.ObservationRating = null;
                                AuditClosureResult.ObservationRating = null;
                                objHistory.ObservationRating = null;
                            }
                            else
                            {
                                transaction.ObservatioRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                                MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                                AuditClosureResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                                objHistory.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                        {
                            if (ddlObservationCategory.SelectedValue == "-1")
                            {
                                transaction.ObservationCategory = null;
                                MstRiskResult.ObservationCategory = null;
                                AuditClosureResult.ObservationCategory = null;
                                objHistory.ObservationCategory = null;
                            }
                            else
                            {
                                transaction.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                                MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                                AuditClosureResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                                objHistory.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                        {
                            if (ddlObservationSubCategory.SelectedValue == "-1")
                            {
                                transaction.ObservationSubCategory = null;
                                MstRiskResult.ObservationSubCategory = null;
                                AuditClosureResult.ObservationSubCategory = null;
                                objHistory.ObservationSubCategory = null;
                            }
                            else
                            {
                                transaction.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                                MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                                AuditClosureResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                                objHistory.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            }
                        }
                        var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(getScheduledonDetails.ID), ATBDID);
                        if (RecentComplianceTransaction != null)
                        {
                            tbxDate.Text = RecentComplianceTransaction.Dated != null ? RecentComplianceTransaction.Dated.Value.ToString("dd-MM-yyyy") : " ";
                        }
                        transaction.Remarks = remark.Trim();
                        if (!string.IsNullOrEmpty(ddlReviewerRiskRatiing.SelectedValue))
                        {
                            if (ddlReviewerRiskRatiing.SelectedValue == "-1")
                                transaction.ReviewerRiskRating = null;
                            else
                                transaction.ReviewerRiskRating = Convert.ToInt32(ddlReviewerRiskRatiing.SelectedValue);
                        }
                        string Testremark = "";
                        if (string.IsNullOrEmpty(txtRemark.Text))
                        {
                            Testremark = "NA";
                        }
                        else
                        {
                            Testremark = txtRemark.Text.Trim();
                        }
                        if (rdbtnStatus.SelectedItem.Text == "Closed")
                        {
                            MstRiskResult.AuditeeResponse = "C";
                        }

                        bool Success1 = false;
                        bool Success2 = false;
                        bool Success3 = false;
                        bool Success4 = false;
                        MstRiskResult.AStatusId = 3;
                        if (RiskCategoryManagement.InternalControlResultExists_ObservationList(MstRiskResult))
                        {
                            MstRiskResult.AStatusId = 3;
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.UpdateInternalControlResult_ObaservationList(MstRiskResult);
                        }

                        if (RiskCategoryManagement.InternalAuditTxnExistsAuditManager_ObaservationList(transaction))
                        {
                            transaction.StatusId = 3;
                            transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.UpdatedOn = DateTime.Now;
                            Success2 = RiskCategoryManagement.UpdateInternalAuditTxnStatusReviewer_ObaservationList(transaction);
                        }

                        using (AuditControlEntities entities = new AuditControlEntities())
                        {

                            var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                  where row.ProcessId == MstRiskResult.ProcessId
                                                  && row.FinancialYear == MstRiskResult.FinancialYear
                                                  && row.ForPerid == MstRiskResult.ForPerid
                                                  && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                  && row.UserID == MstRiskResult.UserID
                                                  && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                  && row.VerticalID == MstRiskResult.VerticalID
                                                  && row.AuditID == MstRiskResult.AuditID
                                                  select row.ID).OrderByDescending(x => x).FirstOrDefault();
                            if (RecordtoUpdate != null)
                            {
                                AuditClosureResult.ResultID = RecordtoUpdate;
                                if (RiskCategoryManagement.AuditClosureRecordExists(AuditClosureResult))
                                {
                                    AuditClosureResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    AuditClosureResult.UpdatedOn = DateTime.Now;
                                    Success1 = RiskCategoryManagement.UpdateCategeriAuditClosure_ObservationList(AuditClosureResult);
                                }
                            }
                        }
                        #endregion

                        HttpFileCollection fileCollection1 = Request.Files;
                        // string UniqueFlag = DateTime.Now.ToString("yyyyMMddHHmmss");
                        if (fileCollection1.Count > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                            var InstanceData = RiskCategoryManagement.GetInternalAuditInstanceData(Convert.ToInt32(getScheduledonDetails.InternalAuditInstance));
                            string directoryPath1 = "";
                            directoryPath1 = Server.MapPath("~/InternalTestingAdditionalDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + getScheduledonDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + getScheduledonDetails.ID + "/1.0");
                            DocumentManagement.CreateDirectory(directoryPath1);
                            for (int i = 0; i < fileCollection1.Count; i++)
                            {
                                HttpPostedFile uploadfile1 = fileCollection1[i];
                                string[] keys1 = fileCollection1.Keys[i].Split('$');
                                String fileName1 = "";
                                if (keys1[keys1.Count() - 1].Equals("FileuploadAdditionalFile"))
                                {
                                    fileName1 = uploadfile1.FileName;

                                }
                                Guid fileKey1 = Guid.NewGuid();
                                string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadfile1.FileName));
                                Stream fs = uploadfile1.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                if (uploadfile1.ContentLength > 0)
                                {
                                    InternalReviewHistory RH = new InternalReviewHistory()
                                    {
                                        ATBDId = ATBDID,
                                        ProcessId = Convert.ToInt32(getScheduledonDetails.ProcessId),
                                        InternalAuditInstance = getScheduledonDetails.InternalAuditInstance,
                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                        Dated = DateTime.Now,
                                        Remarks = Testremark,
                                        AuditScheduleOnID = getScheduledonDetails.ID,
                                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                        Name = fileName1,
                                        FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                        FileKey = fileKey1.ToString(),
                                        Version = "1.0",
                                        VersionDate = DateTime.UtcNow,
                                        FixRemark = tbxRemarks.Text,
                                        VerticalID = VerticalID,
                                        AuditID = AuditID,
                                        // UniqueFlag=UniqueFlag,
                                    };
                                    Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                }
                                else
                                {
                                    InternalReviewHistory RH = new InternalReviewHistory()
                                    {
                                        ProcessId = Convert.ToInt32(getScheduledonDetails.ProcessId),
                                        InternalAuditInstance = getScheduledonDetails.InternalAuditInstance,
                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                        Dated = DateTime.Now,
                                        Remarks = Testremark,
                                        AuditScheduleOnID = getScheduledonDetails.ID,
                                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                        ATBDId = ATBDID,
                                        FixRemark = tbxRemarks.Text,
                                        VerticalID = VerticalID,
                                        AuditID = AuditID,
                                        // UniqueFlag = UniqueFlag,
                                    };
                                    Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                                }
                            }
                        }
                        else
                        {
                            InternalReviewHistory RH = new InternalReviewHistory()
                            {
                                ProcessId = Convert.ToInt32(getScheduledonDetails.ProcessId),
                                InternalAuditInstance = getScheduledonDetails.InternalAuditInstance,
                                CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                Dated = DateTime.Now,
                                Remarks = Testremark,
                                AuditScheduleOnID = getScheduledonDetails.ID,
                                FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                ATBDId = ATBDID,
                                FixRemark = tbxRemarks.Text,
                                VerticalID = VerticalID,
                                AuditID = AuditID,
                                //  UniqueFlag = UniqueFlag,
                            };
                            Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                        }
                        BindRemarks(Convert.ToInt32(getScheduledonDetails.ProcessId), Convert.ToInt32(getScheduledonDetails.ID), ATBDID, AuditID);
                        if (Success1 == true && Success2 == true && Flag == true)
                        {
                            if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                            {
                                RiskCategoryManagement.AddObservationHistory(objHistory);
                            }
                            BindTransactions(Convert.ToInt32(getScheduledonDetails.ID), ATBDID, AuditID);

                            lnkAuditCoverage.Enabled = false;
                            lnkActualTesting.Enabled = false;
                            lnkObservation.Enabled = false;
                            lnkReviewLog.Enabled = false;
                            txtRemark.Text = string.Empty;
                            if (rdbtnStatus.SelectedItem.Text == "Closed")
                            {
                                if (!string.IsNullOrEmpty(txtObservation.Text.Trim()))
                                {
                                    //cvDuplicateEntryForthView.IsValid = false;
                                    //cvDuplicateEntryForthView.ErrorMessage = "Audit Steps Closed Successfully";
                                    // added by Sagar More on 19-12-2019
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Submit Successfully.";
                                    divMessage.Attributes.CssStyle.Add("background-color", "#ffe0e6");
                                    divMessage.Attributes.CssStyle.Add("height", "50px");
                                }
                                else
                                {
                                    if (rdbtnStatus.SelectedItem.Text == "Closed")
                                    {
                                        //cvDuplicateEntry.IsValid = false;
                                        //cvDuplicateEntry.ErrorMessage = "Audit Steps Closed Successfully";
                                        // added by Sagar More on 19-12-2019
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Submit Successfully";
                                        divMessage.Attributes.CssStyle.Add("background-color", "#ffe0e6");
                                        divMessage.Attributes.CssStyle.Add("height", "50px");
                                    }
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntryForthView.IsValid = false;
                            cvDuplicateEntryForthView.ErrorMessage = "Server Error Occured. Please try again.";
                        }

                        // added by sagar more on 10-01-2020
                        if (Success1)
                        {
                            if (txtMultilineVideolink.Text.Trim() != "")
                            {
                                List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(AuditID, ATBDID);
                                if (observationAudioVideoList.Count > 0)
                                {
                                    string[] audioVideoLinks = txtMultilineVideolink.Text.Split(',');
                                    var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                    if (filtered.Count > 0)
                                    {
                                        foreach (var item in filtered)
                                        {
                                            RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, AuditID, ATBDID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                        }
                                    }
                                }

                                ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                                {
                                    AuditId = AuditID,
                                    ATBTID = ATBDID,
                                    IsActive = true,
                                };

                                if (txtMultilineVideolink.Text.Contains(',')) //checking entered single value or multiple values
                                {
                                    string[] audioVideoLinks = txtMultilineVideolink.Text.Split(',');
                                    int lenght = audioVideoLinks.Length;
                                    string link = string.Empty;
                                    for (int i = 0; i < audioVideoLinks.Length; i++)
                                    {
                                        link = audioVideoLinks[i].ToString();
                                        objObservationAudioVideo.AudioVideoLink = link.Trim();
                                        if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                        {
                                            objObservationAudioVideo.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            objObservationAudioVideo.CreatedOn = DateTime.Now;
                                            RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                        }
                                        else
                                        {
                                            objObservationAudioVideo.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                            RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                        }
                                    }
                                }
                                else
                                {
                                    objObservationAudioVideo.AudioVideoLink = txtMultilineVideolink.Text.Trim();
                                    if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                    {
                                        objObservationAudioVideo.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        objObservationAudioVideo.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                    }
                                    else
                                    {
                                        objObservationAudioVideo.AudioVideoLink = txtMultilineVideolink.Text.Trim();
                                        objObservationAudioVideo.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                        RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                    }
                                }
                            }
                            else
                            {
                                // when user want delete all existing link then we will get null from txtMultilineVideolink
                                List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(AuditID, ATBDID);
                                if (observationAudioVideoList.Count > 0)
                                {
                                    foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                    {
                                        RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, AuditID, ATBDID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                    }

                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                        //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                    }
                    else
                    {
                        cvDuplicateEntryForthView.IsValid = false;
                        cvDuplicateEntryForthView.ErrorMessage = "Please Select Status";
                    }
                }//getScheduledonDetails end
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        #region Dropdownchange
        protected void ddlObservationCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
            {
                if (ddlObservationCategory.SelectedValue != "-1")
                {
                    BindObservationSubCategory(Convert.ToInt32(ddlObservationCategory.SelectedValue));
                }
            }
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    BindSchedulingType(Convert.ToInt32(ddlFilterLocation.SelectedValue));

                    if (ddlSchedulingType.Items.Count == 2)
                    {
                        string fal = string.Empty;
                        if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                        {
                            AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                            fal = UserManagementRisk.GetFrequencyFlag(Convert.ToInt32(ddlFilterLocation.SelectedValue), AuditID);
                        }
                        if (fal != "")
                        {
                            if (fal == "A")
                            {
                                ddlSchedulingType.SelectedValue = "3";
                            }
                            else if (fal == "H")
                            {
                                ddlSchedulingType.SelectedValue = "2";
                            }
                            else if (fal == "Q")
                            {
                                ddlSchedulingType.SelectedValue = "4";
                            }
                            else if (fal == "M")
                            {
                                ddlSchedulingType.SelectedValue = "5";
                            }
                            else if (fal == "P")
                            {
                                ddlSchedulingType.SelectedValue = "1";
                            }
                            ddlSchedulingType_SelectedIndexChanged(null, null);
                        }
                    }
                    else
                    {
                        string fal = string.Empty;
                        if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                        {
                            AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                            fal = UserManagementRisk.GetFrequencyFlag(Convert.ToInt32(ddlFilterLocation.SelectedValue), AuditID);
                        }
                        if (fal != "")
                        {
                            if (fal == "A")
                            {
                                ddlSchedulingType.SelectedItem.Text = "Annually";
                            }
                            else if (fal == "H")
                            {
                                ddlSchedulingType.SelectedItem.Text = "Half Yearly";
                            }
                            else if (fal == "Q")
                            {
                                ddlSchedulingType.SelectedItem.Text = "Quarterly";
                            }
                            else if (fal == "M")
                            {
                                ddlSchedulingType.SelectedItem.Text = "Monthly";
                            }
                            else if (fal == "P")
                            {
                                ddlSchedulingType.SelectedItem.Text = "Phase";
                            }
                            ddlSchedulingType_SelectedIndexChanged(null, null);
                        }
                    }
                }
            }
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            //{
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                // BindAuditSchedule("P", 5);

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                        {
                            AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                            int count = 0;
                            count = UserManagementRisk.GetPhaseCountNew(Convert.ToInt32(ddlFilterLocation.SelectedValue), AuditID);
                            BindAuditSchedule("P", count);
                        }
                    }
                }
            }
            //}
        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        #endregion

        protected void btnAddTable_Click(object sender, EventArgs e)
        {
            ShowTable.Visible = true;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }


        public void BindObservationImages()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
            {
                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
            }
            else
            {
                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
            }
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ImageListdata = (from row in entities.ObservationImages
                                     where row.ATBTID == ATBDID &&
                                     row.AuditID == AuditID && row.IsActive == true
                                     select row).ToList();

                if (ImageListdata.Count > 0)
                {
                    grdObservationImg.DataSource = ImageListdata;
                    grdObservationImg.DataBind();
                    UpdatePanel3.Update();
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }
                else
                {
                    // added by sagar more on 10-01-2020
                    grdObservationImg.DataSource = null;
                    grdObservationImg.DataBind();
                    UpdatePanel3.Update();
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }
            }
        }
        protected void grdObservationImg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdObservationImg.PageIndex = e.NewPageIndex;
            BindObservationImages();
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }

        protected void grdObservationImg_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DeleteImage"))
                    {
                        int ID = Convert.ToInt32(e.CommandArgument);
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var Imagelist = (from row in entities.ObservationImages
                                             where row.ID == ID
                                             select row).FirstOrDefault();
                            if (Imagelist != null)
                            {
                                Imagelist.IsActive = false;
                                entities.SaveChanges();
                                BindObservationImages();
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewImage"))
                    {
                        ObservationImage AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFile(Convert.ToInt32(e.CommandArgument));
                        if (AllinOneDocumentList != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.ImagePath), AllinOneDocumentList.ImageName);
                            if (AllinOneDocumentList.ImagePath != null && File.Exists(filePath))
                            {
                                DocumentPath = filePath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            else
                            {
                                lblMessage.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                            }
                        }
                        //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                    }
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}