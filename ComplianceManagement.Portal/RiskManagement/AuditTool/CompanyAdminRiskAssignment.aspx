﻿<%@ Page Title="Company Admin Risk Assignment" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="CompanyAdminRiskAssignment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.CompanyAdminRiskAssignment" %>
<%--<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>--%>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            width:90% !important;
            
        }
        .dd_chk_drop
        {
            width:90% !important;
            margin-top:17px!important;
        }
    </style>
    <style type="text/css">        
        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <style type="text/css">
        .td1 {
            width: 10%;
        }

        .td2 {
            width: 20%;
        }

        .td3 {
            width: 10%;
        }

        .td4 {
            width: 20%;
        }        
        
    </style>
    <script type="text/javascript">
        function SelectheaderCheckboxes(headerchk) {
            var rolecolumn;
            var chkheaderid = headerchk.id.split("_");
            if (chkheaderid[2] == "chkAssignSelectAll") {
                rolecolumn = 5;
            }

            var gvcheck = document.getElementById("<%=grdComplianceRoleMatrix.ClientID %>");
            var i;

            if (headerchk.checked) {
                for (i = 1; i < gvcheck.rows.length; i++) {
                    if (gvcheck.rows[i].cells[1] != undefined) {
                        if (gvcheck.rows[i].cells[1].getElementsByTagName("INPUT")[0] != undefined) {
                            gvcheck.rows[i].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                        }
                    }
                }
            }
            else {
                for (i = 1; i < gvcheck.rows.length; i++) {
                    if (gvcheck.rows[i].cells[1] != undefined) {
                        if (gvcheck.rows[i].cells[1].getElementsByTagName("INPUT")[0] != undefined) {
                            gvcheck.rows[i].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                        }
                    }
                }
            }
        }

        function Selectchildcheckboxes(header) {
            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck = document.getElementById("<%=grdComplianceRoleMatrix.ClientID %>");
            var headerchk = document.getElementById(header);
            var chkheaderid = header.split("_");

            if (chkheaderid[2] == "chkAssignSelectAll") {
                rolecolumn = 5;
            }
            var rowcount = gvcheck.rows.length;

            for (i = 1; i < gvcheck.rows.length; i++) {
                var inputs = gvcheck.rows[i].getElementsByTagName('input');
                if (gvcheck.rows[i].cells[1] != undefined) {
                    if (inputs[0].checked) {
                        count++;
                    }
                }
            }

            if (count == gvcheck.rows.length - 1) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }

    </script>

    <script type="text/javascript">
        function SelectheaderCheckboxesPopup(headerchk) {
            var rolecolumn;
            var chkheaderid = headerchk.id.split("_");
            if (chkheaderid[2] == "chkAssignSelectAll") {
                rolecolumn = 5;
            }

            var gvcheck = document.getElementById("<%=grdReassign.ClientID %>");
            var i;

            if (headerchk.checked) {
                for (i = 1; i < gvcheck.rows.length; i++) {
                    if (gvcheck.rows[i].cells[1] != undefined) {
                        if (gvcheck.rows[i].cells[1].getElementsByTagName("INPUT")[0] != undefined) {
                            gvcheck.rows[i].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                        }
                    }
                }
            }
            else {
                for (i = 1; i < gvcheck.rows.length; i++) {
                    if (gvcheck.rows[i].cells[1] != undefined) {
                        if (gvcheck.rows[i].cells[1].getElementsByTagName("INPUT")[0] != undefined) {
                            gvcheck.rows[i].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                        }
                    }
                }
            }
        }

        function SelectchildcheckboxesPopup(header) {

            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck = document.getElementById("<%=grdReassign.ClientID %>");
            var headerchk = document.getElementById(header);

            var rowcount = gvcheck.rows.length;

            for (i = 1; i < gvcheck.rows.length; i++) {
                var inputs = gvcheck.rows[i].getElementsByTagName('input');
                if (gvcheck.rows[i].cells[1] != undefined) {
                    if (inputs[0].checked) {
                        count++;
                    }
                }
            }

            if (count == gvcheck.rows.length - 1) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }

    </script>

    <script type="text/javascript">
        

        function openModal() {
            $('#divReAssign').modal('show');
            return true;
        }

        function closeModal() {
            $('#divReAssign').modal('hide');
            window.location.reload();
        };

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            //setactivemenu('Control Testing Assignment');
            fhead('Control Testing Assignment');
        });


        function ConfirmEvent() {
            debugger;
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_Event_value";
            var oldUser = $("#<%=ddlUser.ClientID%> option:selected").text();
            var newUser = $("#<%=ddlREAssignUser.ClientID%> option:selected").text();
            var type = $("#<%=rbtEventReAssignment.ClientID%> input:checked").val();

            if (type == "ICFR") {
                if (newUser != "Select User") {
                    if (confirm("Are you sure you want re-assign selected Risk Control of " + oldUser + "?")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                }
            }

            document.forms[0].appendChild(confirm_value);
        }

        function CloseModelDelete() {
            $('#ShowDeleteModel').modal('hide');
            location.reload();
        }

        function OpenModelDeleteAssignment() {
            $('#ShowDeleteModel').modal('show');
            $('#ContentPlaceHolder1_IframeDeleteAssignment').attr('src', "../../RiskManagement/AuditTool/DeleteAssigment.aspx");
        }

    </script>

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upComplianceTypeList">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">   
                            <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"  ForeColor="Red"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                            </div> 

                            <div class="col-md-12 colpadding0">                                                           
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                   <%-- <div class="col-md-2 colpadding0">--%>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="float: left; width:75px; margin-bottom: 0px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5"  />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" Selected="True" />
                                        <asp:ListItem Text="100" />
                                        </asp:DropDownList>
                                    <%--</div>--%>
                                </div>  
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;"></div>
                                <div class="col-md-1 colpadding0" style="margin-top: 5px;"></div>
                                <div class="col-md-5 colpadding0" style="margin-top: 5px;">
                                    <div style="width:95%;text-align: right;">   
                                        <asp:Button ID="btnUploadFile1" runat="server" Text="Export To Excel" OnClick="btnUploadFile1_Click" Width="120px" CssClass="btn btn-primary" Visible="false" />                                            
                                        <asp:Button Text="Re-Assign" runat="server" ID="btnReAssign" CssClass="btn btn-advanceSearch" OnClick="btnReAssign_Click"/>  <%----%>
                                        <asp:Button ID="btnDeleteAssigned" runat="server" Text="Delete Assigment" Width="130px" CssClass="btn btn-primary" OnClientClick="OpenModelDeleteAssignment();"/>                                            
                                    </div>
                                </div>                                                                
                            </div>
                               
                            <div class="clearfix"></div>  
                                                                                                      
                            <div class="col-md-12 colpadding0">  
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15"  Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>
                                </div>                                                                                                                                                                           
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>
                                </div>                                
                            </div>

                            <div class="clearfix"></div>  
                                                        
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">  
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>                                                          
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" class="form-control m-bot15" DataPlaceHolder="Location" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"  >
                                    </asp:DropDownListChosen>                                          
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>               
                                    <asp:DropDownListChosen runat="server" ID="ddlAuditHead" class="form-control m-bot15" OnSelectedIndexChanged="ddlAuditHead_SelectedIndexChanged"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"  DataPlaceHolder="Select Audit Head" Width="90%" Height="32px" >
                                    </asp:DropDownListChosen>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"  
                                    ErrorMessage="Select Audit Head/Management" ForeColor="Red" InitialValue=""
                                    Font-Size="0.9em" ControlToValidate="ddlAuditHead" 
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>

                                    <asp:CompareValidator ErrorMessage="Select Audit Head/Management"  ControlToValidate="ddlAuditHead"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                                </div>

                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>                                                                                      
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterProcess" AutoPostBack="true"  CausesValidation="true" class="form-control m-bot15" DataPlaceHolder="Process" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterProcess_SelectedIndexChanged" >
                                    </asp:DropDownListChosen>                                          
                                </div>                                  
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>                                     
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterSubProcess" AutoPostBack="true" class="form-control m-bot15"  DataPlaceHolder="Sub Process" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterSubProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                          
                                </div>                                                                                        
                                                                                                
                            </div>  

                            <div class="clearfix"></div>  
                                                                                                                                                                                                                                                                                                                    
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">    
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>                                                                         
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial" AutoPostBack="true" class="form-control m-bot15" 
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Financial Year" Width="90%" Height="32px"
                                    OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  
                                    ErrorMessage="Select Financial Year" ForeColor="Red" InitialValue=""
                                    Font-Size="0.9em" ControlToValidate="ddlFilterFinancial" 
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>

                                    <asp:CompareValidator ErrorMessage="Select Financial Year" ControlToValidate="ddlFilterFinancial"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>               
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterPerformer" class="form-control m-bot15"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Performer" Width="90%" Height="32px" >
                                    </asp:DropDownListChosen>

                                    <asp:RequiredFieldValidator ID="cboNoOfSubjects_RequiredFieldValidator" runat="server"  
                                    ErrorMessage="Select Performer" ForeColor="Red" InitialValue=""
                                    Font-Size="0.9em" ControlToValidate="ddlFilterPerformer" 
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>

                                    <asp:CompareValidator ErrorMessage="Select Performer"  ControlToValidate="ddlFilterPerformer"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>                     
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterReviewer" 
                                        AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15" DataPlaceHolder="Reviewer" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"  
                                    ErrorMessage="Select Reviewer" ForeColor="Red" InitialValue=""
                                    Font-Size="0.9em" ControlToValidate="ddlFilterReviewer" 
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ErrorMessage="Select Reviewer" ControlToValidate="ddlFilterReviewer"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />                                       
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px; ">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>                                      
                                     <asp:DropDownList ID="ddlQuarter" runat="server" AutoPostBack="true" Width="90%" CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlQuarter_SelectedIndexChanged"> </asp:DropDownList>                                                                                               
                                </div>
                            </div>



                            <div class="col-md-12 colpadding0">
                            <div class="col-md-8 colpadding0 entrycount" style="margin-top: 5px;width:100% !important">  
                                <ul id="rbl" class="nav nav-tabs"> 
                                    <li class="active" id="liNotAssigned" runat="server">
                                        <asp:LinkButton Text="Not Assigned" runat="server" ID="btnNotAssigned" class="active" OnClick="btnNotAssigned_Click"/>  
                                    </li>
                                    <li class="" id="liAssigned" runat="server">
                                        <asp:LinkButton Text="Assigned" runat="server" ID="btnAssigned" class=""  OnClick="btnAssigned_Click"/>
                                    </li>                                    
                                </ul>
                            </div>
                            </div>

                            <div class="clearfix"></div> 
                             
                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                OnRowDataBound="grdComplianceRoleMatrix__RowDataBound" DataKeyNames="ID" 
                                PageSize="50" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" AllowSorting="true"                                
                                Width="100%"  OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField  ItemStyle-Width="2%" HeaderStyle-Width="2%">                                     
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAssignSelectAll" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkAssign" runat="server"  />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="RiskId"  Visible="false">
                                        <ItemTemplate>                                            
                                            <asp:Label ID="lblRiskCategoryCreationId" runat="server" Text='<%#Eval("RiskCategoryCreationId")%>'></asp:Label> 
                                            <asp:Label ID="lblProcessId" runat="server" Text='<%# Eval("ProcessId")%>'></asp:Label> 
                                            <asp:Label ID="lblSubProcessId" runat="server" Text='<%# Eval("SubProcessId")%>'></asp:Label>  
                                            <asp:Label ID="lblCustomerBranchId" runat="server" Text='<%# Eval("CustomerBranchId")%>'></asp:Label>                                                                                                                                   
                                            <asp:Label ID="lblAssigned" runat="server" Text='<%# Eval("Assigned")%>'></asp:Label>                                                                                                                                   
                                            Assigned
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Branch">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch")%>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Control#">
                                        <ItemTemplate> 
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">                                    
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlNo")%>' ToolTip='<%# Eval("ControlNo") %>'></asp:Label>  
                                                </div>                                          
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Risk Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 175px;">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" 
                                                    Text='<%# Eval("ActivityDescription")%>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Control Objective">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 175px;">
                                                <asp:Label ID="lblControlObjective" runat="server" data-toggle="tooltip" 
                                                    data-placement="left" Text='<%# Eval("ControlObjective")%>' 
                                                    ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Control Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 175px;">
                                                <asp:Label ID="lblControlDescription" runat="server" data-toggle="tooltip"
                                                     data-placement="left" Text='<%# Eval("ControlDescription")%>'
                                                     ToolTip='<%# Eval("ControlDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>   

                                      <asp:TemplateField HeaderText="Frequency">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblFrequency" runat="server" data-toggle="tooltip"
                                                     data-placement="left" Text='<%# Eval("FrequencyName")%>'
                                                     ToolTip='<%# Eval("FrequencyName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                 
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <PagerSettings Visible="false" />  
                                <PagerTemplate> </PagerTemplate>     
                                <EmptyDataTemplate>
                                 No Record Found
                                </EmptyDataTemplate>                           
                                </asp:GridView>                            
                            </div>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0" style="text-align:right;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" Width="90px"
                                    ValidationGroup="ComplianceInstanceValidationGroup" CausesValidation="true" />                               
                                </div>
                                <div class="col-md-6 colpadding0" style="float:right;">
                                    <div class="table-paging" style="margin-bottom: 10px;float:right;color: #999;">                                    
                                        <p>Page
                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                            class="form-control m-bot15" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                            </asp:DropDownListChosen>  
                                        </p>                                  
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                       </section>
                    </div>
                </div>
            </div>
            <div class="clearfix" style="height: 20px"></div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnUploadFile1" />
        </Triggers>
    </asp:UpdatePanel>


    <div style="display: none;">
        <asp:GridView runat="server" ID="grdComplianceRoleMatrixExport" AutoGenerateColumns="false"
            DataKeyNames="ID"
            PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" AllowSorting="true"
            Width="100%">
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="RiskId" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblRiskCategoryCreationId" runat="server" Text='<%#Eval("RiskCategoryCreationId")%>'></asp:Label>
                        <asp:Label ID="lblProcessId" runat="server" Text='<%# Eval("ProcessId")%>'></asp:Label>
                        <asp:Label ID="lblSubProcessId" runat="server" Text='<%# Eval("SubProcessId")%>'></asp:Label>
                        <asp:Label ID="lblCustomerBranchId" runat="server" Text='<%# Eval("CustomerBranchId")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Branch">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch")%>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Control#">
                    <ItemTemplate>
                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlNo")%>' ToolTip='<%# Eval("ControlNo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Risk Description">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActivityDescription")%>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Control Objective">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                            <asp:Label ID="lblControlObjective" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlObjective")%>' ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Control Description">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                            <asp:Label ID="lblControlDescription" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlDescription")%>' ToolTip='<%# Eval("ControlDescription") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="clsROWgrid" />
            <HeaderStyle CssClass="clsheadergrid" />
            <PagerTemplate>
                <table style="display: none">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </PagerTemplate>
        </asp:GridView>
    </div>

    <div id="divReAssign" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 1150px; height: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">×</button>
                </div>

                <div class="modal-body">
                    <asp:UpdatePanel ID="ReAssign" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-lg-12 col-md-12 ">
                                <section class="panel">  
                                    <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="VSReassignSummary" runat="server" Display="None" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="ReAssignValidationGroup" />
                                        <asp:CustomValidator ID="cvReAssignAudit" runat="server" EnableClientScript="False" 
                                            ValidationGroup="ReAssignValidationGroup" Display="None" />                          
                                    </div>                                                                       
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">                                   
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">
                                        <div class="col-md-2 colpadding0" style="width: 20%;">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSizeReAssign" class="form-control m-bot15" Style="width: 70px; float: left"
                                                OnSelectedIndexChanged="ddlPageSizeReAssign_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Text="5" Selected="True"/>
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                            </asp:DropDownList> 
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;"> 
                                            <asp:DropDownListChosen runat="server" ID="ddlUser" Width="90%" Height="32px" AutoPostBack="true" DataPlaceHolder="User"
                                             AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlUser_SelectedIndexChanged" CssClass="form-control m-bot15" />
                                            <asp:Label ID="lblReAssign" Visible="false" runat="server" style="color: #333;"/>
                                        </div>

                                         <div class="col-md-6 colpadding0">
                                              <asp:RadioButtonList ID="rbtEventReAssignment" runat="server" AutoPostBack="true" 
                                                 OnSelectedIndexChanged="rbtEventReAssignment_SelectedIndexChanged" RepeatDirection="Horizontal" RepeatLayout="Table">                                                
                                                 <asp:ListItem Text="Control Testing Assginment" Value="ICFR" Selected="True" style="margin: 5px; margin-left: 0.5em;"></asp:ListItem>                                                
                                                </asp:RadioButtonList>
                                             </div>

                                    </div>

                                    
                                         <div class="clearfix"></div>

                                         <div class="col-md-12 colpadding0">                                       

                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">  
                                            <asp:DropDownListChosen ID="ddlLegalEntityPopup" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" 
                                             AllowSingleDeselect="false" DisableSearchThreshold="3"   Height="32px" DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntityPopUp_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlLegalEntity" ID="RequiredFieldValidator4"
                                            runat="server" InitialValue="-1" ValidationGroup="ReAssignValidationGroup" Display="None" />                                    
                                        </div>

                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                            <asp:DropDownListChosen ID="ddlSubEntity1Popup" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                           AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1Popup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select ." ControlToValidate="ddlSubEntity1" ID="RequiredFieldValidator5"
                                                runat="server" InitialValue="-1" ValidationGroup="ReAssignValidationGroup" Display="None" />
                                        </div>

                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                            <asp:DropDownListChosen ID="ddlSubEntity2Popup" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                           AllowSingleDeselect="false" DisableSearchThreshold="3"   DataPlaceHolder="Sub Unit"   OnSelectedIndexChanged="ddlSubEntity2Popup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Unit." ControlToValidate="ddlSubEntity2" ID="RequiredFieldValidator6"
                                            runat="server" InitialValue="-1" ValidationGroup="ReAssignValidationGroup" Display="None" />  
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                             <asp:DropDownListChosen runat="server" ID="ddlSubEntity3Popup" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                           AllowSingleDeselect="false" DisableSearchThreshold="3"    DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3Popup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                         </div>
                            
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                            <asp:DropDownListChosen runat="server" ID="ddlLocationPopup" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"    OnSelectedIndexChanged="ddlLocationPopup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>                                  
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                            <asp:DropDownListChosen runat="server" ID="ddlFinYearPopup" class="form-control m-bot15" Width="90%" Height="32px"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"   DataPlaceHolder="Financial Year"  AutoPostBack="true" OnSelectedIndexChanged="ddlFinYearPopup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div>

                                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                        <asp:DropDownListChosen ID="ddlQuarterReassing" runat="server" AutoPostBack="true" DataPlaceHolder="Quarters"
                                            Width="90%" Height="32px" CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlQuarterReassing_SelectedIndexChanged"></asp:DropDownListChosen>
                                    </div>
                                  
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">
                                             <asp:DropDownListChosen runat="server" ID="ddlProcessPopUp" Width="90%" Height="32px" AutoPostBack="true" DataPlaceHolder="Process"
                                               AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlProcessPopUp_SelectedIndexChanged" CssClass="form-control m-bot15" />
                                        </div> 
                                             
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">
                                             <asp:DropDownListChosen runat="server" ID="ddlSubProcessPopUp" Width="90%" Height="32px" AutoPostBack="true" DataPlaceHolder="Sub Process"
                                              AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlSubProcessPopUp_SelectedIndexChanged" CssClass="form-control m-bot15" />
                                        </div>                                       

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;"> 
                                            <asp:DropDownListChosen runat="server" ID="ddlRole"  Width="90%" Height="32px" class="form-control m-bot15"
                                           AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" DataPlaceHolder="Select Role" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged">                                                 
                                            <asp:ListItem Text="Performer" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Reviewer" Value="4"></asp:ListItem>
                                            </asp:DropDownListChosen> 
                                        </div>                                       
                                             
                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                                            
                                            <asp:DropDownListChosen runat="server" ID="ddlREAssignUser" Width="90%" AutoPostBack="true"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  CssClass="form-control m-bot15" DataPlaceHolder="New User to Assign" />
                                            <asp:CompareValidator  ErrorMessage="Please Select New User to Assign." ControlToValidate="ddlREAssignUser"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ReAssignValidationGroup"
                                            Display="None" />
                                         </div>                             
                                   </div>
                            </div>


                            <div runat="server" id="div2" style="margin-bottom: 7px; width: 100%; height: 300px; overflow-y: auto;">
                                <asp:GridView runat="server" ID="grdReassign" AutoGenerateColumns="false" AllowPaging="true" PageSize="5" ShowHeaderWhenEmpty="true"
                                    GridLines="None" AllowSorting="true" Width="100%" CssClass="table" ShowFooter="false" OnRowDataBound="grdReassignNew__RowDataBound">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkReAssignSelectAll" runat="server" onclick="javascript:SelectheaderCheckboxesPopup(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkReAssign" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RiskId" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRiskCategoryCreationId" runat="server" Text='<%#Eval("RiskCreationId")%>'></asp:Label>
                                                <asp:Label ID="lblProcessId" runat="server" Text='<%# Eval("ProcessId")%>'></asp:Label>
                                                <asp:Label ID="lblSubProcessId" runat="server" Text='<%# Eval("SubProcessId")%>'></asp:Label>
                                                <asp:Label ID="lblCustomerBranchId" runat="server" Text='<%# Eval("CustomerBranchId")%>'></asp:Label>
                                                <asp:Label ID="lblFY" runat="server" Text='<%# Eval("FinancialYear")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Branch">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch")%>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Control#">
                                            <ItemTemplate>
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlNo")%>' ToolTip='<%# Eval("ControlNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Risk Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActivityDescription")%>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Control Objective">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblControlObjective" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlObjective")%>' ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Control Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblControlDescription" runat="server" data-toggle="tooltip" data-placement="left" Text='<%# Eval("ControlDescription")%>' ToolTip='<%# Eval("ControlDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>


                            <div class="clearfix"></div>

                            <div id="divSavePageSummary" runat="server" style="display: none;">
                                <div class="col-md-12 colpadding0" style="margin: 5px; margin-bottom: 40px;">
                                    <div class="col-md-6 colpadding0" style="margin: auto; text-align: right;">
                                        <asp:Button Text="Save" runat="server" ID="btnReassignEvent" OnClick="btnReassignAuditSave_Click"
                                            CssClass="btn btn-primary" ValidationGroup="ReAssignValidationSummary" OnClientClick="ConfirmEvent();" />
                                        <asp:Button Text="Close" runat="server" ID="btnReassignClose" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="closeModal()" />
                                    </div>

                                    <div class="col-md-6 colpadding0">
                                        <div class="table-paging" style="margin-bottom: 10px;">
                                            <asp:ImageButton ID="lBPreviousReAssign" CssClass="table-paging-left" runat="server"
                                                ImageUrl="~/img/paging-left-active.png" OnClick="lBPreviousReAssign_Click" />
                                            <div class="table-paging-text">
                                                <p>
                                                    <asp:Label ID="SelectedPageNoReAssign" runat="server" Text=""></asp:Label>/
                                                    <asp:Label ID="lTotalCountReAssign" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                            <asp:ImageButton ID="lBNextReAssign" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png"
                                                OnClick="lBNextReAssign_Click" />
                                            <asp:HiddenField ID="TotalRowsReAssign" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </section>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ShowDeleteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 85%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 260px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Delete Assignment</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeDeleteAssignment" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
