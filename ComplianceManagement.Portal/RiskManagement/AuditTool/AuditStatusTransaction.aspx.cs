﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using Ionic.Zip;
using System.IO;
using System.Reflection;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;


namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AuditStatusTransaction : System.Web.UI.Page
    {
        int RiskCreationId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["ScheduledOnID"]))
                    if (!String.IsNullOrEmpty(Request.QueryString["RiskCreationId"]))
                        if (!String.IsNullOrEmpty(Request.QueryString["FinancialYear"]))
                            if (!String.IsNullOrEmpty(Request.QueryString["CustomerBranchID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["Period"]))
                                {
                                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["ScheduledOnID"]);
                                    RiskCreationId = Convert.ToInt32(Request.QueryString["RiskCreationId"]);
                                    string FinancialYear = Request.QueryString["FinancialYear"];
                                    int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                                    string Period = Request.QueryString["Period"];
                                    BindTransactionDetails(ScheduledOnID, RiskCreationId, FinancialYear, CustomerBranchID, Period);
                                }
                BindResponsiblePersons(RiskCreationId);
                //Page Title
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int SubProcessID = 0;
                int ProcessID = 0;
                string ControlNo = string.Empty;
                int CB = 0;
                string Pname = string.Empty;
                string SPname = string.Empty;
                string VName = string.Empty;
                var BrachName = string.Empty;
                var Period1 = string.Empty;
                var FY = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["FinancialYear"])))
                {
                    FY = "/" + Convert.ToString(Request.QueryString["FinancialYear"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["Period"])))
                {
                    Period1 = "/" + Convert.ToString(Request.QueryString["Period"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["CustomerBranchID"]))
                {
                    CB = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                    BrachName = CustomerBranchManagement.GetByID(CB).Name;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SPID"]))
                {
                    SubProcessID = Convert.ToInt32(Request.QueryString["SPID"]);
                    SPname = "/ " + ProcessManagement.GetSubProcessByID(SubProcessID).Name;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                    Pname = "/ " + ProcessManagement.GetByID(ProcessID, customerID).Name;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                {
                    ControlNo = "/ " + Convert.ToString(Request.QueryString["CID"]);
                }

                LblPageDetails.InnerText = BrachName + FY + Period1 + Pname + SPname + ControlNo;

                //End
            }
        }


        private void BindResponsiblePersons(int RiskCreationId)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlPersonResponsible.DataTextField = "Name";
                ddlPersonResponsible.DataValueField = "ID";
                ddlPersonResponsible.DataSource = UserManagementRisk.GetAllNonAdminUser(customerID, RiskCreationId);
                //var users = UserManagementRisk.GetAllNonAdminUser(customerID, RiskCreationId);
                ddlPersonResponsible.DataBind();
                ddlPersonResponsible.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {

                bool result = false;

                if (userID == com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int RiskCreationId, string FinancialYear, int CustomerBranchID, string period)
        {
            try
            {
                txtDocumentExamined.Text = string.Empty;
                txtReasonforPassorFail.Text = string.Empty;
                lbDownloadSample.Text = string.Empty;
                rfvFile.Visible = true;
                ViewState["AuditScheduledOnID"] = ScheduledOnID;
                var complianceInfo = RiskCategoryManagement.GetAuditByInstanceID(ScheduledOnID, CustomerBranchID);
                var complianceForm = RiskCategoryManagement.GetAuditSampleFormByID(complianceInfo.RiskCreationId);
                var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByComplianceID(ScheduledOnID);
                txtDocumentExamined.Text = complianceInfo.DocumentsExamined;
                ViewState["FinancialYear"] = null;
                ViewState["FinancialYear"] = FinancialYear;
                ViewState["CustomerBranchID"] = null;
                ViewState["CustomerBranchID"] = CustomerBranchID;

                ViewState["Period"] = null;
                ViewState["Period"] = period;
                if (complianceForm != null)
                {
                    lbDownloadSample.Text = "Click <u>here</u> to download sample form.";
                    lbDownloadSample.CommandArgument = complianceForm.RiskCreationID.ToString();
                }
                btnSave.Attributes.Remove("disabled");
                tbxRemarks.Text = string.Empty;
                BindTransactions(ScheduledOnID);
                BindRemarks(RiskCreationId, FinancialYear, period);
                if (GridRemarks.Rows.Count != 0)
                {
                    divReviewHistory.Visible = true;
                    GridRemarks.Visible = true;
                    txtRemark.Visible = true;
                    FileuploadAdditionalFile.Visible = true;
                    tbxRemarks.Enabled = false;

                    if (RiskCreationId != -1 && ScheduledOnID != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var MstRiskResult = (from row in entities.ReviewHistories
                                                 where row.RiskCreationId == RiskCreationId && row.AuditScheduleOnID == ScheduledOnID
                                                 select row).FirstOrDefault();
                            if (MstRiskResult != null)
                            {
                                tbxRemarks.Text = MstRiskResult.Remarks;
                            }
                        }
                    }
                }
                else
                {
                    divReviewHistory.Visible = false;
                    tbxRemarks.Enabled = true;
                }
                hdnRiskCreatinID.Value = RiskCreationId.ToString();
                hdnAuditInstanceID.Value = RecentComplianceTransaction.AuditInstanceId.ToString();
                hdnAuditScheduledOnId.Value = ScheduledOnID.ToString();
                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            // processnonprocess = ProcessManagement.GetClientnameName(branchid);
            return processnonprocess;
        }
        public static List<ReviewHistory> GetFileData1(int id, int RiskCreationId, int AuditScheduleOnId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.ReviewHistories
                                where row.ID == id && row.RiskCreationId == RiskCreationId
                                && row.AuditScheduleOnID == AuditScheduleOnId
                                select row).ToList();

                return fileData;
            }
        }
        protected void DownLoadClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblID = (Label)gvr.FindControl("lblID");
                Label lblriskID = (Label)gvr.FindControl("lblriskID");
                Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");


                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<ReviewHistory> fileData = GetFileData1(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text));
                    int i = 0;
                    string directoryName = string.Empty;
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        directoryName = file.ForMonth;
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TestingDocument.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void ddlDesign_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDesign.SelectedItem.Text == "Fail")
            {
                //fuSampleFile.Enabled = false;
                //txtSampleTested.Enabled = false;
                //ddlOprationEffectiveness.Enabled = false;
                //txtDeviation.Enabled = false;
                //rfvFile.Enabled = false;

                //Change on 20 FEB 2018 On Client Requirement
                fuSampleFile.Enabled = true;
                txtSampleTested.Enabled = false;
                ddlOprationEffectiveness.Enabled = false;
                txtDeviation.Enabled = false;
                rfvFile.Enabled = true;

                //fuSampleFile.Enabled = false;
                //txtSampleTested.Enabled = false;
                //ddlOprationEffectiveness.Enabled = false;
                //txtDeviation.Enabled = false;
                //rfvFile.Enabled = false;

            }
            else
            {
                fuSampleFile.Enabled = true;
                txtSampleTested.Enabled = true;
                ddlOprationEffectiveness.Enabled = true;
                txtDeviation.Enabled = true;
                rfvFile.Enabled = true;
            }
            if (ddlDesign.SelectedItem.Text == "Fail" || ddlOprationEffectiveness.SelectedItem.Text == "Fail")
            {
                divpersonresponsible.Visible = true;
            }
            else
            {
                divpersonresponsible.Visible = false;
            }
        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {

                var file = Business.RiskCategoryManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));
                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        //Response.Buffer = true;
                        //Response.Clear();
                        //Response.ClearContent();
                        //Response.ClearHeaders();
                        //Response.ContentType = "application/octet-stream";
                        //Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);

                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush();
                        Response.End();
                    }
                }
                //Response.Buffer = true;
                //Response.Clear();
                //Response.ContentType = "application/octet-stream";
                //Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.FileData); // create the file
                //Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
            try
            {
                if (ddlDesign.SelectedItem.Text == "Pass" && ddlOprationEffectiveness.SelectedItem.Text == "Select TOE")
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select Test of Effectiveness (TOE).";
                    return;
                }
                if (ddlDesign.SelectedItem.Text == "Pass With Mitigating Control" && ddlOprationEffectiveness.SelectedItem.Text == "Select TOE")
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select Test of Effectiveness (TOE).";
                    return;
                }
                if (ddlDesign.SelectedItem.Text == "Fail" || ddlOprationEffectiveness.SelectedItem.Text == "Fail")
                {
                    if (!string.IsNullOrEmpty(ddlPersonResponsible.SelectedValue))
                    {
                        if (ddlPersonResponsible.SelectedValue == "-1")
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Person Responsible.";
                            return;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(tbxDate.Text))
                {
                    AuditTransaction transaction = new AuditTransaction()
                    {
                        AuditScheduleOnID = Convert.ToInt64(hdnAuditScheduledOnId.Value),
                        AuditInstanceId = Convert.ToInt64(hdnAuditInstanceID.Value),
                        RiskCreationID = Convert.ToInt64(hdnRiskCreatinID.Value),
                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        StatusId = 2,
                        StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks.Text,
                        DesignResult = Convert.ToInt32(ddlDesign.SelectedValue),
                        OprationResult = Convert.ToInt32(ddlOprationEffectiveness.SelectedValue),
                        CustomerBranchId = Convert.ToInt32(ViewState["CustomerBranchID"].ToString()),
                        FinancialYear = ViewState["FinancialYear"].ToString(),

                    };
                    Mst_RiskResult MstRiskResult = new Mst_RiskResult()
                    {
                        AuditScheduleOnID = Convert.ToInt64(hdnAuditScheduledOnId.Value),
                        RiskCreationId = Convert.ToInt64(hdnRiskCreatinID.Value),
                        AuditInstanceId = Convert.ToInt64(hdnAuditInstanceID.Value),
                        Deviation = txtDeviation.Text,
                        SampleTested = txtSampleTested.Text,
                        ResonForPassFail = txtReasonforPassorFail.Text,
                        TOD = Convert.ToInt32(ddlDesign.SelectedValue),
                        TOE = Convert.ToInt32(ddlOprationEffectiveness.SelectedValue),
                        CustomerBranchId = Convert.ToInt32(ViewState["CustomerBranchID"].ToString()),
                        FinancialYear = ViewState["FinancialYear"].ToString(),
                        IsDeleted = false,

                    };
                    RiskCategoryManagement.CreateRiskResult(MstRiskResult);
                    List<FileData_Risk> files = new List<FileData_Risk>();
                    List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                    HttpFileCollection fileCollection = Request.Files;
                    bool blankfileCount = true;
                    if (fileCollection.Count > 0)
                    {
                        int? customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        var InstanceData = RiskCategoryManagement.GetAuditInstanceData(Convert.ToInt32(hdnRiskCreatinID.Value), Convert.ToInt64(hdnAuditInstanceID.Value));
                        string directoryPath = "";
                        if (InstanceData.ProcessId != -1 && InstanceData.SubProcessId != -1 && InstanceData.CustomerBranchID != -1)
                        {
                            directoryPath = Server.MapPath("~/AuditTestingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.RiskCreationId + "/" + ViewState["FinancialYear"].ToString() + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.SubProcessId.ToString() + "/" + hdnAuditScheduledOnId.Value + "/1.0");
                        }
                        else if (InstanceData.ProcessId != -1 && InstanceData.CustomerBranchID != -1)
                        {
                            directoryPath = Server.MapPath("~/AuditTestingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.RiskCreationId + "/" + ViewState["FinancialYear"].ToString() + "/" + InstanceData.ProcessId.ToString() + "/" + hdnAuditScheduledOnId.Value + "/1.0");
                        }
                        DocumentManagement.CreateDirectory(directoryPath);
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = fileCollection[i];
                            string[] keys = fileCollection.Keys[i].Split('$');
                            String fileName = "";
                            if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                            {
                                fileName = "AuditTestingDocumentDoc_" + uploadfile.FileName;
                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                            }
                            Guid fileKey = Guid.NewGuid();
                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                            Stream fs = uploadfile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                            if (uploadfile.ContentLength > 0)
                            {
                                FileData_Risk file = new FileData_Risk()
                                {
                                    Name = fileName,
                                    FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                    FileKey = fileKey.ToString(),
                                    Version = "1.0",
                                    VersionDate = DateTime.UtcNow,
                                    RiskCreationId = Convert.ToInt64(hdnRiskCreatinID.Value),
                                    AuditInstanceId = Convert.ToInt64(hdnAuditInstanceID.Value),
                                    CustomerBranchId = Convert.ToInt32(ViewState["CustomerBranchID"].ToString()),
                                    FinancialYear = ViewState["FinancialYear"].ToString(),
                                    IsDeleted = false,
                                };
                                files.Add(file);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(uploadfile.FileName))
                                    blankfileCount = false;
                            }
                        }
                    }
                    bool flag = false;
                    if (blankfileCount)
                    {
                        flag = UserManagementRisk.CreateTransaction(transaction, files, list, Filelist);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                    }
                    //bool flag = true;
                    if (flag != true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                    if (OnSaved != null)
                    {
                        OnSaved(this, null);
                    }
                    if (GridRemarks.Rows.Count == 0)
                    {
                        ReviewHistory RH = new ReviewHistory()
                        {
                            RiskCreationId = Convert.ToInt64(hdnRiskCreatinID.Value),
                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                            Dated = DateTime.Now,
                            Remarks = tbxRemarks.Text,
                            AuditScheduleOnID = Convert.ToInt64(hdnAuditScheduledOnId.Value),
                            FinancialYear = ViewState["FinancialYear"].ToString(),
                            CustomerBranchId = Convert.ToInt32(ViewState["CustomerBranchID"].ToString()),
                            ForMonth = ViewState["Period"].ToString(),
                        };
                        bool Flag = RiskCategoryManagement.CreateReviewRemark(RH);
                        if (Flag == true)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Risk Control Submitted Successfully";
                        }
                    }
                    else
                    {
                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                        HttpFileCollection fileCollection1 = Request.Files;
                        if (fileCollection1.Count > 0)
                        {
                            int? customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                            var InstanceData = RiskCategoryManagement.GetAuditInstanceData(Convert.ToInt32(hdnRiskCreatinID.Value), Convert.ToInt64(hdnAuditInstanceID.Value));
                            string directoryPath1 = "";
                            if (InstanceData.ProcessId != -1 && InstanceData.SubProcessId != -1 && InstanceData.CustomerBranchID != -1)
                            {
                                directoryPath1 = Server.MapPath("~/AuditTestingAdditionalDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.RiskCreationId + "/" + ViewState["FinancialYear"].ToString() + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.SubProcessId.ToString() + "/" + hdnAuditScheduledOnId.Value + "/1.0");
                            }
                            else if (InstanceData.ProcessId != -1 && InstanceData.CustomerBranchID != -1)
                            {
                                directoryPath1 = Server.MapPath("~/AuditTestingAdditionalDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.RiskCreationId + "/" + ViewState["FinancialYear"].ToString() + "/" + InstanceData.ProcessId.ToString() + "/" + hdnAuditScheduledOnId.Value + "/1.0");
                            }
                            DocumentManagement.CreateDirectory(directoryPath1);
                            for (int i = 0; i < fileCollection1.Count; i++)
                            {
                                HttpPostedFile uploadfile1 = fileCollection1[i];
                                string[] keys1 = fileCollection1.Keys[i].Split('$');
                                String fileName1 = "";
                                if (keys1[keys1.Count() - 1].Equals("FileuploadAdditionalFile"))
                                {
                                    fileName1 = "AuditTestingAdditionalDocumentDoc_" + uploadfile1.FileName;
                                }
                                Guid fileKey1 = Guid.NewGuid();
                                string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadfile1.FileName));
                                Stream fs = uploadfile1.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                if (uploadfile1.ContentLength > 0)
                                {
                                    ReviewHistory RH = new ReviewHistory()
                                    {
                                        RiskCreationId = Convert.ToInt64(hdnRiskCreatinID.Value),
                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                        Dated = DateTime.Now,
                                        Remarks = txtRemark.Text,
                                        AuditScheduleOnID = Convert.ToInt64(hdnAuditScheduledOnId.Value),
                                        FinancialYear = ViewState["FinancialYear"].ToString(),
                                        CustomerBranchId = Convert.ToInt32(ViewState["CustomerBranchID"].ToString()),
                                        ForMonth = ViewState["Period"].ToString(),
                                        Name = fileName1,
                                        FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                        FileKey = fileKey1.ToString(),
                                        Version = "1.0",
                                        VersionDate = DateTime.UtcNow,
                                    };
                                    bool Flag = RiskCategoryManagement.CreateReviewRemark(RH);
                                    if (Flag == true)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Risk Control Submitted Successfully";
                                    }
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                }
                            }
                        }
                    }
                    BindRemarks(Convert.ToInt32(hdnRiskCreatinID.Value), ViewState["FinancialYear"].ToString(), ViewState["Period"].ToString());


                    RiskActivityTransaction objRiskDoc = new RiskActivityTransaction()
                    {
                        RiskCreationId = Convert.ToInt64(hdnRiskCreatinID.Value),
                        DocumentsExamined = txtDocumentExamined.Text.ToString()
                    };
                    UserManagementRisk.UpdateDocumentExamine(objRiskDoc);


                    if (ddlDesign.SelectedItem.Text == "Fail" || ddlOprationEffectiveness.SelectedItem.Text == "Fail")
                    {

                        ActionPlanAssignment transaction1 = new ActionPlanAssignment()
                        {
                            RiskCreationId = Convert.ToInt64(hdnRiskCreatinID.Value),
                            ActionPlan = txtMngntesponce.Text,
                            TOD = Convert.ToInt32(ddlDesign.SelectedValue),
                            TOE = Convert.ToInt32(ddlOprationEffectiveness.SelectedValue),
                            PersonResponsible = Convert.ToInt32(ddlPersonResponsible.SelectedValue),
                            CustomerBranchId = Convert.ToInt32(ViewState["CustomerBranchID"].ToString()),
                            FinancialYear = ViewState["FinancialYear"].ToString(),
                            AuditScheduleOnID = Convert.ToInt64(hdnAuditScheduledOnId.Value),
                        };
                        if (!string.IsNullOrEmpty(txtDateResp.Text))
                        {
                            transaction1.TimeLine = DateTime.ParseExact(txtDateResp.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        UserManagementRisk.CreateActionPlanAssignment(transaction1);
                    }
                }//date filter End
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["AuditScheduledOnID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                grdTransactionHistory.DataSource = Business.DashboardManagementRisk.GetAllTransactions(ScheduledOnID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindRemarks(int RiskCreationId, string financialyear, string period)
        {
            try
            {
                if (RiskCreationId != -1 && !string.IsNullOrEmpty(financialyear) && !string.IsNullOrEmpty(period))
                {
                    GridRemarks.DataSource = Business.DashboardManagementRisk.GetAllRemarks(RiskCreationId, financialyear, period);
                    GridRemarks.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //private void BindStatusList(int statusID)
        //{
        //    try
        //    {
        //        ddlStatus.DataSource = null;
        //        ddlStatus.DataBind();
        //        ddlStatus.ClearSelection();

        //        ddlStatus.DataTextField = "Name";
        //        ddlStatus.DataValueField = "ID";

        //        var statusList = AuditStatusManagement.GetStatusList();

        //        List<AuditStatu> allowedStatusList = null;

        //        List<AuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetStatusTransitionListByInitialId(statusID);
        //        List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

        //        allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

        //        ddlStatus.DataSource = allowedStatusList;
        //        ddlStatus.DataBind();

        //        ddlStatus.Items.Insert(0, new ListItem("Select", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void grdTransactionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceTransactionHistory = Business.DashboardManagementRisk.GetAllTransactions(Convert.ToInt32(ViewState["AuditScheduledOnID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory.DataSource = complianceTransactionHistory;
                grdTransactionHistory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.RiskCategoryManagement.GetFile(fileID);
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //Response.BinaryWrite(file.Data); // create the file
                    Response.Flush(); // send it to the client to download
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        public void OpenTransactionPage(int? ScheduledOnID, int RiskCreationId, string FinancialYear, int CustomerBranchID, string Period)
        {
            try
            {
                long a = Convert.ToInt32(ScheduledOnID);
                ddlDesign.SelectedValue = "-1";
                ddlOprationEffectiveness.SelectedValue = "-1";
                tbxRemarks.Text = tbxDate.Text = txtSampleTested.Text = txtDeviation.Text = txtReasonforPassorFail.Text = string.Empty;
                BindTransactionDetails(Convert.ToInt32(a), RiskCreationId, FinancialYear, CustomerBranchID, Period);
                fuSampleFile.Enabled = true;
                txtSampleTested.Enabled = true;
                ddlOprationEffectiveness.Enabled = true;
                txtDeviation.Enabled = true;
                rfvFile.Visible = true;
                tbxDate.Enabled = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer", "initializeConfirmDatePicker(null);", true);
                }
                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", string.Format("initializeConfirmDatePicker1(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", "initializeConfirmDatePicker1(null);", true);
                }

                //ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeComboboxUpcoming();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlOprationEffectiveness_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDesign.SelectedItem.Text == "Fail" || ddlOprationEffectiveness.SelectedItem.Text == "Fail")
            {
                divpersonresponsible.Visible = true;
            }
            else
            {
                divpersonresponsible.Visible = false;
            }
        }

        public string ShowSampleDocumentNameView(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "/View";
            }
            return processnonprocess;
        }
        protected void lblViewFile_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            Label lblID = (Label)gvr.FindControl("lblID");
            Label lblriskID = (Label)gvr.FindControl("lblriskID");
            Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
            Label lblATBDId = (Label)gvr.FindControl("lblATBDId");

            List<ReviewHistory> fileData = GetFileData1(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text));

            foreach (var file in fileData)
            {
                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                if (file.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles";
                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + File;

                    string extension = System.IO.Path.GetExtension(filePath);

                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                    }

                    string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    if (file.EnType == "M")
                    {
                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    else
                    {
                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    bw.Close();

                    string CompDocReviewPath = FileName;
                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestViewICFRPER('" + CompDocReviewPath + "');", true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                }
                break;
            }
        }
    }
}