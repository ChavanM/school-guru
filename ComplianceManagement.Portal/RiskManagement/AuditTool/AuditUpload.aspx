﻿<%@ Page Title="Audit Upload" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditUpload.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.AuditUpload" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style> 
    <script type="text/javascript">
        function Confirmtest(ink) {

            var testchk = $(ink).attr('attr-data');
            //alert(testchk);

            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (testchk == "Download") {
                if (confirm("Do you want to overwrite document, if you have already uploaded?")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                    return false;
                }
            }

            document.forms[0].appendChild(confirm_value);
        }

        function ConfirmtestView(ink) {

            $('#DocumentPopUp').modal();
            $('#ContentPlaceHolder1_docViewerAll').attr('src', "../../docviewer.aspx?docurl=" + ink);
        }

    </script>

    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            //setactivemenu('Sample Upload');
            fhead('Sample Upload');
            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#DocumentPopUp').modal('hide');
            });
        });

    </script>

    <style type="text/css">
        .td1 {
            width: 10%;
        }

        .td2 {
            width: 20%;
        }

        .td3 {
            width: 10%;
        }

        .td4 {
            width: 20%;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
           
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">   
                             <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ForeColor="Red" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label> 
                              </div>              

                             <div style="width:100%;float:left">
                                 <div style="width:13%;float:left">
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 100%;">
                                        <div class="col-md-1 colpadding0" style="width: 39%;">
                                            <p style="color: #999;">Show </p>
                                        </div>
                                        <div class="col-md-2 colpadding0">
                                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left; margin-bottom: 0px;" 
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                            <asp:ListItem Text="5" Selected="True" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                            </asp:DropDownList>
                                        </div>
                                   </div>
                                  </div>
                                 <div style="width:87%;float:left">
                                     <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label> 
                                            <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label> 
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="90%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label> 
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="90%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit 2">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label> 
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit 3">
                                            </asp:DropDownListChosen>
                                        </div>                                                                    
                                      </div>
                                     <div class="col-md-12 colpadding0">  
                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                             <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label> 
                                            <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true"  Width="90%" Height="32px" DataPlaceHolder="Location" class="form-control m-bot15"
                                           AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                          </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label> 
                                            <asp:DropDownListChosen runat="server" ID="ddlFilterProcess" AutoPostBack="true"  Width="90%" Height="32px"   DataPlaceHolder="Process" class="form-control m-bot15" 
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterProcess_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                          
                                          </div>                                          
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                             <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>  
                                            <asp:DropDownListChosen runat="server" ID="ddlFilterSubProcess" AutoPostBack="true"  Width="90%" Height="32px"  DataPlaceHolder="Sub Process"  class="form-control m-bot15" 
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlFilterSubProcess_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                          
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                             <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>    
                                            <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial" AutoPostBack="true"  Width="90%" Height="32px" DataPlaceHolder="Finanacial Year"   class="form-control m-bot15" 
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged">                                
                                            </asp:DropDownListChosen>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  
                                            ErrorMessage="Select Financial Year" ForeColor="Red" InitialValue=""
                                            Font-Size="0.9em" ControlToValidate="ddlFilterFinancial" 
                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>

                                            <asp:CompareValidator ErrorMessage="Select Financial Year" ControlToValidate="ddlFilterFinancial"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />                                                      
                                        </div> 
                                      </div>
                                 </div>  
                              </div> 
                            <div class="clearfix"></div>                                                     
                            <div class="clearfix"></div>                                   
                            <div style="margin-bottom: 4px">                   
                                <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" PageSize="5" AllowPaging="true" AutoPostBack="true"
                                    CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID" AllowSorting="true"
                                     OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField HeaderText="RiskCategoryCreationId" SortExpression="RiskCategoryCreationId" Visible="false">
                                            <ItemTemplate>
                                               
                                                <asp:Label ID="lblCustomerBranchId" runat="server" Text='<%# Eval("CustomerBranchId")%>'></asp:Label>    
                                                <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID")%>'></asp:Label>
                                                <asp:Label ID="lblSubProcessId" runat="server" Text='<%# Eval("SubProcessID")%>'></asp:Label>
                                                <asp:Label ID="lblRiskCategoryCreationId" runat="server" Text='<%# Eval("RiskCategoryCreationId")%>'></asp:Label>
                                                <asp:Label ID="lblFinancialYear" runat="server" Text='<%# Eval("FinancialYear")%>'></asp:Label>
                                      
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Control&nbsp;No">
                                            <ItemTemplate>                                     
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlNo")%>' ToolTip='<%# Eval("ControlNo") %>'></asp:Label>                                            
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Risk&nbsp;Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActivityDescription")%>'  ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Control&nbsp;Objective">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblControlObjective" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlObjective")%>' ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField  HeaderText="Documents" ItemStyle-Width="200px">
                                            <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="lblDownLoadfile" runat="server" Font-Underline="true" Text='<%# ShowSampleDocumentName((string)Eval("SampleDownloadFileName")) %>' OnClick="DownLoadClick"></asp:LinkButton>
                                                         <asp:LinkButton ID="lblViewfile" runat="server" Font-Underline="true"  attr-data='<%# Eval("SampleDownloadFileName") %>' Text='<%# ShowSampleConditionDocumentName((string)Eval("SampleDownloadFileName")) %>' OnClick="ViewDocument"></asp:LinkButton>      
                                                  <asp:Label ID="lblpathsample" runat="server" style="display:none" ></asp:Label>
                                                          </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="200px">
                                            <ItemTemplate>
                                                        <asp:FileUpload ID="FileUpload1" runat="server" />                                                   
                                            </ItemTemplate>
                                             </asp:TemplateField>
                                             <asp:TemplateField ItemStyle-Width="13%">
                                             <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="aaa" UpdateMode="Conditional">
                                                    <ContentTemplate>           <%--OnClientClick="Confirm()"   --%>                                      
                                                        <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-primary"  Text="Upload" attr-data='<%# ShowSampleDocumentName((string)Eval("SampleDownloadFileName")) %>' OnClick="btnUpload_Click" OnClientClick="Confirmtest(this)"  ValidationGroup="ComplianceInstanceValidationGroup" CausesValidation="true" />                                                    
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnUpload" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                      <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                          
                                        </PagerTemplate>
                                    <EmptyDataTemplate>
                                    No Records Found.
                                 </EmptyDataTemplate>                               
                                </asp:GridView>
                                     <div style="float: right;">                                                
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>                                   
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0" style="float:right;">
                                    <div class="table-paging" style="margin-bottom: 10px;">
                                       
                                        <div class="table-paging-text" style="float: right;">
                                            <p>Page
                                              
                                            </p>
                                        </div>
                                       
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                       
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ddlFilterFinancial" />
            <asp:PostBackTrigger ControlID="ddlFilterSubProcess" />
            <asp:PostBackTrigger ControlID="ddlFilterProcess" />
            <asp:PostBackTrigger ControlID="ddlFilterLocation" />
        </Triggers>
    </asp:UpdatePanel>
      <div class="modal fade" id="DocumentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y:hidden; top: 5%;" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="width: 100%;"">
                
                    <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" Height="550px"  ></iframe>
                   
                </div>
            </div>
        </div>
    </div>
</asp:Content>
