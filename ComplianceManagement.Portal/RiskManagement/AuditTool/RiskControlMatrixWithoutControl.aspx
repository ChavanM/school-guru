﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="RiskControlMatrixWithoutControl.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.RiskControlMatrixWithoutControl" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <script type="text/javascript">
 

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function initializeDatePicker(date) {
        var startDate = new Date();
        $('#<%= txtStartDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: startDate,
            numberOfMonths: 1,
        });

        if (date != null) {
            $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
        }
    }
        function openModal() {
            $('#ComplaincePerformer').modal('show');
            return true;
        }
        function CloseWin() {
            $('#ComplaincePerformer').modal('hide');
        };
        $(document).ready(function () {
            setactivemenu('Risk Control Matrix');
            fhead('Risk Control Matrix');
        });
    </script>  
    <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
        } 
    </style>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
    <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">  
                            <div class="col-md-12 colpadding0"> 
                                <div class="col-md-2 colpadding0 entrycount" style="margin-top:5px;">
                                <div class="col-md-3 colpadding0" >
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                   <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left" 
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                                    <asp:ListItem Text="5" Selected="True"/>
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>                                       
                             </div>                                                                                           
                                <div class="col-md-8 colpadding0 entrycount" style="margin-top: 5px; color:#999;">     
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" Style="margin-left: 0px"
                                        RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged">
                                        <asp:ListItem Text="Process" Value="Process" Selected="True" />
                                        <asp:ListItem Text="Others" Value="Others" style="margin-left:20px"/>
                                    </asp:RadioButtonList>              
                                </div> 
                                <div style="text-align:right">
                                    <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server"  Width="12%" OnClientClick="return openModal()" ID="btnAddPromotor" OnClick="btnAddPromotor_Click"  />  
                                       
                                </div>
                                <div style="float:right; margin-top:5px;">                                                               
                                 <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-primary" CausesValidation="false" OnClick="lbtnExportExcel_Click" runat="server"/>                                
                                </div>
                                <div class="clearfix"></div><div class="clearfix"></div>


                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                    Location</label>
                                    <asp:DropDownList runat="server" ID="ddlFilterLocation" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15" Style="width: 220px; float: left">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                     <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                        Process</label>
                                    <asp:DropDownList ID="ddlFilterProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Style="width: 220px; float: left"
                                        OnSelectedIndexChanged="ddlFilterProcess_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlFilterProcess" ID="rfvProcess"
                                        runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                              </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                    Sub Process</label>
                                    <asp:DropDownList ID="ddlFilterSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Style="width: 220px; float: left"
                                    OnSelectedIndexChanged="ddlFilterSubProcess_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlFilterSubProcess" ID="rfvSubProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />  
                                </div>
                                  
                               <%-- <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                    Risk Category</label>
                                    <asp:DropDownList runat="server" ID="ddlFilterRiskCategory" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlFilterRiskCategory_SelectedIndexChanged" class="form-control m-bot15" Style="width: 220px; float: left">
                                    </asp:DropDownList>
                                </div>    
                            
                                <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px;">
                                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                    Industry</label>
                                <asp:DropDownList runat="server" ID="ddlFilterIndustry" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlFilterIndustry_SelectedIndexChanged" class="form-control m-bot15" Style="width: 220px; float: left">
                                </asp:DropDownList>
                                </div>--%>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                 <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Location Type</label>
                                <asp:DropDownList runat="server" ID="ddlFilterLocationType" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlFilterLocationType_SelectedIndexChanged" class="form-control m-bot15" Style="width: 220px; float: left">
                                </asp:DropDownList>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                           </div>
                           <div class="clearfix"></div>
                                <div class="clearfix"></div>
                            <%--//////////////////////////////////////////Gridview/////////////////////////--%>

                             <div style="margin-bottom: 4px">  
                                <div class="col-lg-12 col-md-12">        
                                        <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" GridLines="none" CssClass="table"  BorderWidth="0px"
                                       OnRowCommand="grdRiskActivityMatrix_RowCommand"
                                        AllowPaging="True" PageSize="5"  DataKeyNames="Id" OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging">                        
                                        <Columns>                      
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Task Id" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltaskidItemTemplate" runat="server" Text='<%# Eval("RiskCreationId") %>'></asp:Label>
                                                    <asp:Label ID="lblRiskActivityIDItemTemplate" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                            <asp:TemplateField HeaderText="Control #">
                                                <ItemTemplate>
                                                     <asp:Label ID="lblControlNo" runat="server" Text='<%# Eval("ControlNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                                    
                                            <asp:TemplateField HeaderText="Risk Description">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblRiskActivityDescriptionItemTemplate" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActivityDescription") %>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                                    </div>                                
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Control Objective">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblControlObjectiveItemTemplate" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlObjective") %>' ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                               
                                            <%--<asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 135px;">
                                                    <asp:Label ID="lblClient" runat="server" Text='<%# ShowCustomerBranchName((long)Eval("RiskCreationId")) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                           <%-- <asp:TemplateField HeaderText="RiskCategory">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblRiskCategory" runat="server" Text='<%# ShowRiskCategoryName((long)Eval("RiskCreationId")) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                           <%-- <asp:TemplateField HeaderText="Assertions">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblAssertions" runat="server" Text='<%# ShowAssertionsName((long)Eval("RiskCreationId")) %>'></asp:Label>
                                                    </div>  
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Control Description">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="txtExistingControlDescriptionItemTemplate" data-toggle="tooltip" data-placement="bottom" runat="server" Text='<%# Eval("ControlDescription") %>' ToolTip='<%# Eval("ControlDescription") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <%-- <asp:TemplateField HeaderText="Mitigating Control">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="txtMControlDescriptionItemTemplate" runat="server"  Text='<%# Eval("MControlDescription") %>' />
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Person Responsible">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="ddlPersonResponsibleItemTemplate" runat="server" Text='<%#  ShowPersonResposibleName((long)Eval("PersonResponsible")) %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        <%--    <asp:TemplateField HeaderText="Effective Date">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="txtEffectiveDateItemTemplate" runat="server" Style="height: 16px;"  CssClass="textbox" Text='<%# Eval("EffectiveDate")==null?"":ShowDate((DateTime)Eval("EffectiveDate")) %>' />                                                           
                                                </div> 
                                            </ItemTemplate>
                                            </asp:TemplateField>--%>
                                           <%-- <asp:TemplateField HeaderText="Key">
                                                <ItemTemplate>                                               
                                                    <asp:Label ID="ddlKeyItemTemplate" runat="server"  Text='<%# ShowKeyName((long)Eval("Key")) %>'></asp:Label>                            
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <%--<asp:TemplateField HeaderText="Preventive Control">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="ddlPreventiveItemTemplate" runat="server"  Text='<%# ShowControlName((long)Eval("PrevationControl")) %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Automated Control">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="ddlAutomatedItemTemplate" runat="server"  Text='<%# ShowControlName((long)Eval("AutomatedControl")) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Frequency">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                    <asp:Label ID="ddlFrequencytemTemplate" runat="server"  Text='<%# ShowFrequencyName((long)Eval("Frequency")) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <%-- <asp:TemplateField HeaderText="Gap Description">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 130px;">
                                                    <asp:Label ID="txtGapDescriptionItemTemplate" runat="server"  Text='<%# Eval("GapDescription") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Recommendations">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="txtRecommendationsItemTemplate" runat="server"  Text='<%# Eval("Recommendations") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action/ Remediation plan">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="txtActionRemediationplanItemTemplate" runat="server"  Text='<%# Eval("ActionRemediationplan") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="IPE(Information Produced by Entity)">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                                    <asp:Label ID="txtIPEItemTemplate" runat="server"  Text='<%# Eval("IPE") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ERP System">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="txtERPsystemItemTemplate" runat="server" Text='<%# Eval("ERPSystem") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="FRC(Fraud Risk Control)">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 180px;">
                                                    <asp:Label ID="txtFRCItemTemplate" runat="server" Text='<%# Eval("FRC") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Unique / Referred">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px;">
                                                    <asp:Label ID="txtUniqueReferredItemTemplate" runat="server" Text='<%# Eval("UniqueReferred") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                             
                                            <asp:TemplateField HeaderText="Risk Rating">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="ddlRiskItemTemplate" runat="server" Text='<%# ShowRiskRatingName((int?)Eval("RiskRating")) %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Control Rating">
                                            <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                            <asp:Label ID="ddlControlItemTemplate" runat="server"  Text='<%# ShowControlRatingName((int?)Eval("ControlRating")) %>'></asp:Label>
                                            </div> 
                                            </ItemTemplate>
                                            </asp:TemplateField> --%>   
                                        
                                            <%-- <asp:TemplateField HeaderText="Action">     
                                                <ItemTemplate>                                                            
                                                    <asp:Button runat="server"  OnClientClick="return openModal()" ID="btnChangeStatus" OnClick="btnChangeStatus_Click"   CssClass="btnss"  Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID")) %>'
                                                    CommandName="CHANGE_STATUS"  CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("ComplianceInstanceID") %>' />                                                          
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                           
                                            <asp:TemplateField  HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_RCM" CommandArgument='<%# Eval("ID") + "," + Eval("RiskCreationId") %>' OnClientClick="return openModal()"  CausesValidation="false"><img src="../../Images/edit_icon_new.png" alt="Display Rating" title="Display Rating"  /></asp:LinkButton>                                        
                                                </ItemTemplate>                                                                    
                                            </asp:TemplateField>
                                        </Columns>

                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />                                         <PagerStyle HorizontalAlign="Right" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        </asp:GridView>
                                  </div>
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>                                   
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0" style="margin-left: 500px;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                      </div>
                            </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>

    <%--/////////////////////////////PopUp code Start//////////////////--%>

    <div class="modal fade" id="ComplaincePerformer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 34px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                 <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional">
                      <ContentTemplate>
                        <div class="modal-body" style="background-color: #f7f7f7;">   
                            
                            <table style="width:100%">
                                 <tr>
                                    <td colspan="4">
                                        <div style="margin-bottom: 7px; margin-left: 20px;">
                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary"
                                                ValidationGroup="CustomerValidationGroup1" ForeColor="Red" />
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                                ValidationGroup="CustomerValidationGroup1" Display="None" />                      
                                        </div>
                                    </td>                                   
                                </tr>
                                <tr>
                                    <td style="width:25%;"> 
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Control</label>
                                    </td>
                                    <td style="width:25%;">
                                        <asp:TextBox runat="server" ID="tbxControl" Style="width: 390px;" CssClass="form-control" MaxLength="50"  />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Control number can not be empty."
                                        ControlToValidate="tbxControl" runat="server" ValidationGroup="CustomerValidationGroup1"
                                        Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CustomerValidationGroup1"
                                        ErrorMessage="Please enter a valid name." ControlToValidate="tbxControl" ValidationExpression="^[a-zA-Z_&]+[a-zA-Z0-9&_ .-]*$"></asp:RegularExpressionValidator>
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Risk Description</label>
                                    </td>
                                    <td style="width:25%;">                                         
                                        <asp:TextBox runat="server" ID="tbxRiskDesc" Style="width: 390px;" CssClass="form-control" MaxLength="500"
                                        TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Control Objective</label>
                                    </td>
                                    <td style="width:25%;">
                                          <asp:TextBox runat="server" ID="tbxControlObj" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Control Description</label>
                                    </td>
                                    <td style="width:25%;">
                                        <asp:TextBox runat="server" ID="tbxControlDesc" Style="width: 390px;" CssClass="form-control"    TextMode="MultiLine" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Location
                                        </label>
                                    </td>
                                    <td style="width:25%;">                                                                         
                                    <asp:DropDownList runat="server" ID="ddlClientProcess" AutoPostBack="true" Style="width: 390px;" class="form-control m-bot15"
                                         OnSelectedIndexChanged="ddlClientProcess_SelectedIndexChanged" />
                                         <asp:RequiredFieldValidator ErrorMessage="Please Select Location." ControlToValidate="ddlClientProcess"
                                               ID="RequiredFieldValidator12"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Risk Category</label>                                                     
                                    </td>
                                    <td style="width:25%;">
                                        <asp:DropDownCheckBoxes ID="ddlRiskCategoryCheck" runat="server" Width="200px"
                                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                                OnSelectedIndexChanged="checkBoxesRiskCategoryProcess_SelcetedIndexChanged">
                                                                <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200"  DropDownBoxBoxHeight="250"></Style>
                                                                <Texts SelectBoxCaption="Select Risk Category" />
                                                            </asp:DropDownCheckBoxes>                                         
                                    </td>
                                </tr>
                                 <tr>
                                     <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                        Process</label>
                                    </td>
                                    <td style="width:25%;">
                                       <asp:DropDownList ID="ddlProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Style="width: 390px;" 
                                        OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlProcess" ID="RequiredFieldValidator9"
                                        runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                        Sub Process</label>
                                    </td>
                                    <td style="width:25%;">
                                        <asp:DropDownList ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Style="width: 390px;"
                                    OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlSubProcess" ID="RequiredFieldValidator10"
                                    runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />  
                                    </td>                                                                            
                                </tr>
                                <tr>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Assertions</label>                                                      
                                    </td>
                                    <td style="width:25%;">
                                        <asp:DropDownCheckBoxes ID="ddlAssertions" runat="server" Width="200px"
                                        AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                        OnSelectedIndexChanged="checkBoxesAssertionsProcess_SelcetedIndexChanged">
                                        <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200"  DropDownBoxBoxHeight="250"></Style>
                                        <Texts SelectBoxCaption="Select Assertions" />
                                        </asp:DropDownCheckBoxes>                                           
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Person Responsible</label>                               
                                    </td>
                                    <td style="width:25%;">
                                         <asp:DropDownList runat="server" ID="ddlPersonResponsible" Style="width: 390px;" class="form-control m-bot15" />
                                          <asp:RequiredFieldValidator ErrorMessage="Please Select Person Responsible." ControlToValidate="ddlPersonResponsible"
                                               ID="RequiredFieldValidator3"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Key</label>
                                    </td>
                                    <td style="width:25%;">
                                         <asp:DropDownList runat="server" ID="ddlKey" Style="width: 390px;" class="form-control m-bot15" />
                                          <asp:RequiredFieldValidator ErrorMessage="Please Select Key." ControlToValidate="ddlKey"
                                               ID="RequiredFieldValidator2"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />

                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Preventive Control</label>
                                    </td>
                                    <td style="width:25%;">
                                         <asp:DropDownList runat="server" ID="ddlPreventive" Style="width: 390px;" class="form-control m-bot15" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Preventive." ControlToValidate="ddlPreventive"
                                               ID="RequiredFieldValidator4"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                  <tr>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Automated Control</label>
                                    </td>
                                    <td style="width:25%;">
                                        <asp:DropDownList runat="server" ID="ddlAutomated" Style="width: 390px;" class="form-control m-bot15" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Automated." ControlToValidate="ddlAutomated"
                                               ID="RequiredFieldValidator8"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Frequency</label>
                                    </td>
                                    <td style="width:25%;">
                                         <asp:DropDownList runat="server" ID="ddlFrequency" Style="width: 390px;" class="form-control m-bot15" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Frequency." ControlToValidate="ddlFrequency"
                                               ID="RequiredFieldValidator5"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                <tr>
                                     <td style="width:25%;">
                                          <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Industry</label>
                                    </td>
                                    <td style="width:25%;">                                        
                                          <asp:DropDownCheckBoxes ID="ddlIndultory" runat="server" Width="200px" AutoPostBack="true"
                                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                                OnSelectedIndexChanged="checkBoxesProcess_SelcetedIndexChanged">
                                                               <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200"  DropDownBoxBoxHeight="250"></Style>
                                                                <Texts SelectBoxCaption="Select Industry" />
                                                            </asp:DropDownCheckBoxes>    
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                       Location Type</label>                            
                                    </td>
                                    <td style="width:25%;">                                    
                                        <asp:DropDownList ID="ddlLocationType" runat="server" class="form-control m-bot15"  Style="width: 390px;"></asp:DropDownList>                                          
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Location Type." ControlToValidate="ddlLocationType"
                                               ID="RequiredFieldValidator11"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>                                                                      
                                </tr>
                              
                                  <tr>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Gap Description</label>
                                    </td>
                                    <td style="width:25%;">
                                         <asp:TextBox runat="server" ID="tbxGapDesc" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Recommendations</label>
                                    </td>
                                    <td style="width:25%;">
                                         <asp:TextBox runat="server" ID="tbxRecom" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Action/ Remediation plan</label>
                                    </td>
                                    <td style="width:25%;">
                                          <asp:TextBox runat="server" ID="tbxActionPlan" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        IPE(Information Produced by Entity)</label>
                                    </td>
                                    <td style="width:25%;">
                                          <asp:TextBox runat="server" ID="tbxIPE" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        ERP System</label>
                                    </td>
                                    <td style="width:25%;">
                                        <asp:TextBox runat="server" ID="tbxERPSystem" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        FRC(Fraud Risk Control)</label>
                                    </td>
                                    <td style="width:25%;">
                                          <asp:TextBox runat="server" ID="tbxFRC" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Mitigating Control</label>                          
                                    </td>
                                    <td style="width:25%;">
                                        <asp:TextBox runat="server" ID="txtMitigation" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>
                                     <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Unique / Referred</label>
                                    </td>
                                    <td style="width:25%;">
                                        <asp:TextBox runat="server" ID="tbxUnique" Style="width: 390px;" CssClass="form-control" MaxLength="500" TextMode="MultiLine" />
                                    </td>                                    
                                </tr>
                                <tr>
                                     <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Control Rating</label>
                                    </td>
                                    <td style="width:25%;">
                                          <asp:DropDownList runat="server" ID="ddlControlRating" Style="width: 390px;" class="form-control m-bot15" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Control Rating." ControlToValidate="ddlControlRating"
                                               ID="RequiredFieldValidator6"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                    <td style="width:25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Risk Rating</label>
                                    </td>
                                    <td style="width:25%;">
                                         <asp:DropDownList runat="server" ID="ddlRiskRating" Style="width: 390px;" class="form-control m-bot15" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Risk Rating." ControlToValidate="ddlRiskRating"
                                               ID="RequiredFieldValidator7"
                                            runat="server" InitialValue="-1" ValidationGroup="CustomerValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                  <tr>
                                    <td style="width:25%;"> 
                                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Effective Date</label> 
                                    </td>
                                    <td style="width:25%;">  
                                        <asp:TextBox runat="server" ID="txtStartDate" placeholder="DD-MM-YYYY" Style="width: 390px;" CssClass="form-control" MaxLength="200" />                                                               
                                    </td>
                                    <td style="width:25%;"> </td>
                                    <td style="width:25%;"> </td>
                                </tr>
                       
                                <tr>
                                    <td align="center"  colspan="4">
                                        <asp:Button Text="Save" runat="server" ID="btnSavePop" CssClass="btn-search" Style="border: 0px; width: 70px; height: 30px; border-radius: 3px; margin-right: 5px"
                                        ValidationGroup="CustomerValidationGroup1" OnClick="btnSavePop_Click" />                                        
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn-search" Style="border: 0px; width: 70px; height: 30px; border-radius: 3px;" OnClientClick="$('#ComplaincePerformer').dialog('close');" />
                                    </td>                                   
                                </tr>
                                <tr>
                                    <td  colspan="4">     
                                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                        </div> 
                                    </td>                                   
                                </tr>
                            </table>                                                                                                                                             
                      </div>
                    </ContentTemplate>
                  </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--/////////////////////////////PopUp code End//////////////////--%>



    <%-- //////////////////GridView used for Export which is not display///////////////////////--%>
    <div style="display: none;">
        <asp:GridView ID="GridExportExcel" runat="server" AutoGenerateColumns="false" AllowPaging="false"
            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" ForeColor="Black">
            <Columns>
                <asp:BoundField DataField="ControlNo" HeaderText="Control No" ItemStyle-Width="150px" />
                <asp:BoundField DataField="RiskCreationId" HeaderText="Task Id" ItemStyle-Width="150px" />
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Process Name">
                    <ItemTemplate>
                        <asp:Label ID="ProcessName" runat="server" Text='<%# ShowProcessName((long)Eval("ProcessId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="SubProcess Name">
                    <ItemTemplate>
                        <asp:Label ID="SubProcessName" runat="server" Text='<%# ShowSubProcessName((long)Eval("ProcessId"),(long)Eval("SubProcessId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Risk Description">
                    <ItemTemplate>
                        <asp:Label ID="lblActivityDescription" runat="server" Text='<%# Eval("ActivityDescription") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Control Objective">
                    <ItemTemplate>
                        <asp:Label ID="lblControlObjective" runat="server" Text='<%# Eval("ControlObjective") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="RiskCategory">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# ShowRiskCategoryName((long)Eval("RiskCreationId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Location">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# ShowCustomerBranchName((long)Eval("RiskCreationId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Assertions">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# ShowAssertionsName((long)Eval("RiskCreationId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ControlDescription" HeaderText="Control Description" ItemStyle-Width="150px" />
                <asp:BoundField DataField="MControlDescription" HeaderText="MitigatingControl Description" ItemStyle-Width="150px" />
                <asp:BoundField DataField="PersonResponsible" HeaderText="Person Responsible" ItemStyle-Width="150px" />
                <asp:BoundField DataField="EffectiveDate" HeaderText="EffectiveDate" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Key" HeaderText="Key" ItemStyle-Width="150px" />
                <asp:BoundField DataField="PrevationControl" HeaderText="Preventive Control" ItemStyle-Width="150px" />
                <asp:BoundField DataField="AutomatedControl" HeaderText="Automated Control" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Frequency" HeaderText="Frequency" ItemStyle-Width="150px" />
                <asp:BoundField DataField="GapDescription" HeaderText="Gap Description" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Recommendations" HeaderText="Recommendations" ItemStyle-Width="150px" />
                <asp:BoundField DataField="ActionRemediationplan" HeaderText="Action Remediation Plan" ItemStyle-Width="150px" />
                <asp:BoundField DataField="IPE" HeaderText="Information Produced by Entity" ItemStyle-Width="150px" />
                <asp:BoundField DataField="ERPsystem" HeaderText="ERP System" ItemStyle-Width="150px" />
                <asp:BoundField DataField="FRC" HeaderText="Fraud Risk Control" ItemStyle-Width="150px" />
                <asp:BoundField DataField="UniqueReferred" HeaderText="Unique Referred" ItemStyle-Width="150px" />
                <asp:BoundField DataField="TestStrategy" HeaderText="Test Strategy" ItemStyle-Width="150px" />
                <asp:BoundField DataField="DocumentsExamined" HeaderText="Documents Examined" ItemStyle-Width="150px" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
            <PagerSettings Position="Top" />
            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
            <AlternatingRowStyle BackColor="#E6EFF7" />
        </asp:GridView>
    </div>
    <div style="margin-bottom: 7px; float: right; margin-right: 467px; margin-top: 10px; display: none; clear: both">
        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" ValidationGroup="ComplianceInstanceValidationGroup" />
        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="btn btn-primary" OnClientClick="$('#divAssignComplianceDialog').dialog('close');" />
    </div>

</asp:Content>
