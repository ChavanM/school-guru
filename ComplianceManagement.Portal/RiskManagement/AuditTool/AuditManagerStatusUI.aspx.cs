﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AuditManagerStatusUI : System.Web.UI.Page
    {
        int Statusid;
        protected List<Int32> roles;
        public static List<long> Branchlist = new List<long>();
        protected int CustomerId = 0;
        public static List<sp_PerformerReviewView_Result> PerformerReviewViewList = new List<sp_PerformerReviewView_Result>();
        protected bool DepartmentHead = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindFinancialYear();
                BindLegalEntityData();
                BindVertical();

                if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    string FinancialYear = Convert.ToString(Request.QueryString["FY"]);
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                else
                {
                    string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    if (FinancialYear != null)
                    {
                        ddlFinancialYear.ClearSelection();
                        ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                    }
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "Open")
                    {
                        ViewState["Status"] = 1;
                    }
                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["Status"] = 3;
                    }
                }
                else
                {
                    ViewState["Status"] = 1;
                }
                PerformerReviewViewList = DashboardManagementRisk.sp_GetPerFormerReviewer();
                if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Request.QueryString["Type"] == "Process")
                    {
                        ShowProcessGrid(sender, e);
                    }
                    else if (Request.QueryString["Type"] == "Implementation")
                    {
                        ShowImplementationGrid(sender, e);
                    }
                    else
                    {
                        ShowProcessGrid(sender, e);
                    }
                }
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
            {
                if (ViewState["Type"].ToString() == "Process")
                {
                    grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdProcessAudits.PageIndex = chkSelectedPage - 1;
                }
                else if (ViewState["Type"].ToString() == "Implementation")
                {
                    grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdImplementationAudits.PageIndex = chkSelectedPage - 1;
                }
            }
            BindData();
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
        }

        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, UserID);
            }
            //ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            //ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                //ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindData()
        {
            try
            {
                int branchid = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;
                string Flag = String.Empty;
                int VerticalID = -1;
                string departmentheadFlag = string.Empty;
                if (DepartmentHead)
                {
                    departmentheadFlag = "DH";
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ViewState["Status"].ToString()))
                {
                    Statusid = Convert.ToInt32(ViewState["Status"]);
                }
                if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
                {
                    Flag = Convert.ToString(ViewState["Type"]);
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }
                if (FinYear == "")
                    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                Branchlist.Clear();
                GetAllHierarchy(CustomerId, branchid);
                Branchlist.ToList();

                if (Flag == "Process")
                {
                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    grdImplementationAudits.Visible = false;
                    grdProcessAudits.Visible = true;
                    var AuditLists = DashboardManagementRisk.GetAuditManagerAudits(FinYear, Period, CustomerId, Branchlist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalID, 4, departmentheadFlag);

                    grdProcessAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdProcessAudits.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }
                else if (Flag == "Implementation")
                {
                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    grdImplementationAudits.Visible = true;
                    grdProcessAudits.Visible = false;

                    var AuditLists = DashboardManagementRisk.GetAuditManagerAuditsIMP(FinYear, Period, CustomerId, branchid, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalID);
                    AuditLists = (from row in AuditLists
                                  select row).GroupBy(entity => entity.AuditID).Select(entity => entity.FirstOrDefault()).ToList();
                    grdImplementationAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdImplementationAudits.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }

                bindPageNumber();
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            BindSchedulingType();
            BindData();
            BindVertical();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                {
                    BindSchedulingType();
                    BindVertical();
                    BindData();
                }
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        int count = 0;
                        count = UserManagementRisk.GetPhaseCount(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindAuditSchedule("P", count);
                    }
                }
            }
            bindPageNumber();
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
                {
                    if (ViewState["Type"].ToString() == "Process")
                    {
                        grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    }
                    else if (ViewState["Type"].ToString() == "Implementation")
                    {
                        grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    }
                }

                BindData();
                if (ViewState["Type"].ToString() == "Process")
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdProcessAudits.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
                else if (ViewState["Type"].ToString() == "Implementation")
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdImplementationAudits.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void grdProcessAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    String Args = e.CommandArgument.ToString();

                    string[] arg = Args.ToString().Split(',');

                    string URLStr = string.Empty;

                    URLStr = "~/RiskManagement/InternalAuditTool/AuditManagerMainUI.aspx?Status=" + arg[0] + "&CustBranchID=" + arg[1] + "&peroid=" + arg[2] + "&FY=" + arg[3] + "&VID=" + arg[4] + "&AuditID=" + arg[5] + "&ID=0" + "&SID=0" + "&PID=0" + "&SPID=0" + "&AH=True";

                    if (Args != "")
                        Response.Redirect(URLStr, false);

                }
                else if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                    string ForMonth = Convert.ToString(commandArgs[1]);
                    string FinancialYear = Convert.ToString(commandArgs[2]);
                    int Verticalid = Convert.ToInt32(commandArgs[3]);
                    var AuditID = Convert.ToInt32(commandArgs[4]);


                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerStatusSummary.aspx?AuditID=" + AuditID + "&FY=" + FinancialYear + "&Type=Process" + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString(), false);
                    }
                    else
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerStatusSummary.aspx?AuditID=" + AuditID + "&FY=" + FinancialYear + "&Type=Process" + "&ReturnUrl1=", false);
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdImplementationAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                    string ForMonth = Convert.ToString(commandArgs[1]);
                    string FinancialYear = Convert.ToString(commandArgs[2]);
                    int Verticalid = Convert.ToInt32(commandArgs[3]);
                    long AuditID = Convert.ToInt64(commandArgs[4]);

                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString() + "&AuditID=" + AuditID + "&FY=" + FinancialYear, false);
                    }
                    else
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=" + "&AuditID=" + AuditID + "&FY=" + FinancialYear, false);
                    }
                }
                else if (e.CommandName.Equals("DRILLDOWNIMP"))
                {
                    string[] arg = e.CommandArgument.ToString().Split(new char[] { ',' });
                    Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerMainUI_IMP.aspx?Status=" + "FinalReview" + "&ID=" + -1 + "&SID=" + -1 + "&CustBranchID=" + arg[0] + "&FinYear=" + arg[2] + "&Period=" + arg[1] + "&VID=" + arg[3] + "&AuditID=" + arg[4] + "&DrilDown=Yes");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ShowImplementationGrid(object sender, EventArgs e)
        {
            liImplementation.Attributes.Add("class", "active");
            liProcess.Attributes.Add("class", "");
            ViewState["Type"] = "Implementation";

            BindData();
            int chknumber = Convert.ToInt32(grdProcessAudits.PageIndex);

            if (chknumber > 0)
            {
                chknumber = chknumber + 1;
                DropDownListPageNo.SelectedValue = (chknumber).ToString();
            }
        }

        protected void ShowProcessGrid(object sender, EventArgs e)
        {
            liProcess.Attributes.Add("class", "active");
            liImplementation.Attributes.Add("class", "");
            ViewState["Type"] = "Process";

            BindData();

            int chknumber = Convert.ToInt32(grdProcessAudits.PageIndex);

            if (chknumber > 0)
            {
                chknumber = chknumber + 1;
                DropDownListPageNo.SelectedValue = (chknumber).ToString();
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void grdProcessAudits_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkAuditDetails = (LinkButton)e.Row.FindControl("lnkAuditDetails");
                LinkButton lblFinalReview = (LinkButton)e.Row.FindControl("lblFinalReview");
                string Args = lnkAuditDetails.CommandArgument;
                int custbranchid = -1;
                int verticalid = -1;
                long AuditID = -1;

                if (!string.IsNullOrEmpty(Args))
                {
                    string[] arg = Args.ToString().Split(',');
                    custbranchid = Convert.ToInt32(arg[0]);
                    verticalid = Convert.ToInt32(arg[3]);
                    AuditID = Convert.ToInt32(arg[4]);

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(custbranchid, verticalid, AuditID)
                                                         select row).ToList();

                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.RoleID == 4 && row.AuditID == AuditID
                                                   select row).ToList();
                        //select row).ToList().GroupBy(entity => entity.ATBDID).ToList();

                        //var AuditCount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();
                        var AuditCount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();
                        if (AuditCount > 0)
                        {
                            lblFinalReview.Text = Convert.ToString(AuditCount);
                        }
                        else
                        {
                            lblFinalReview.Text = string.Empty;
                        }
                    }
                }
                //Label AuditId = (Label)e.Row.FindControl("AuditId");
                //Label lblProcess = (Label)e.Row.FindControl("lblProcess");
                //Label lblPerformer = (Label)e.Row.FindControl("lblPerformer");
                //long cbranchId = Convert.ToInt64(AuditId.Text);

                //int roleid = 3;
                //List<sp_PerformerReviewView_Result> Rowss = (from C in PerformerReviewViewList
                //                                             where C.AuditID == cbranchId
                //                                             select C).Distinct().ToList();

                //var ProcessName = (from C in Rowss
                //                   where C.Roleid == roleid
                //                   select C.ProcessName).Distinct().ToList();

                //string Process = "";
                //foreach (var item in ProcessName)
                //{
                //    Process += item + ",";
                //}
                //lblProcess.Text = Process.TrimEnd(',');
                //lblProcess.Attributes.Add("tooltip", lblProcess.Text);
            }
        }

        protected void grdImplementationAudits_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkAuditDetails = (LinkButton)e.Row.FindControl("lnkAuditDetails");
                LinkButton lblFinalReviewIMP = (LinkButton)e.Row.FindControl("lblFinalReviewIMP");
                string Args = lnkAuditDetails.CommandArgument;
                int UserID = Portal.Common.AuthenticationHelper.UserID;
                int custbranchid = -1;
                int verticalid = -1;
                long AuditID = -1;

                if (!string.IsNullOrEmpty(Args))
                {
                    string[] arg = Args.ToString().Split(',');
                    custbranchid = Convert.ToInt32(arg[0]);
                    verticalid = Convert.ToInt32(arg[3]);
                    AuditID = Convert.ToInt32(arg[4]);

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var AuditSummaryMasterRecords = (from row in entities.SP_ImplementationAuditSummary(CustomerId)
                                                         where row.AuditID == AuditID
                                                         //&& row.UserID== UserID
                                                         && row.RoleID == 4
                                                         select row).ToList();

                        var AuditCount = AuditSummaryMasterRecords.Where(entity => entity.AuditStatusID == 5).Count();
                        if (AuditCount > 0)
                        {
                            lblFinalReviewIMP.Text = Convert.ToString(AuditCount);
                        }
                        else
                        {
                            lblFinalReviewIMP.Text = string.Empty;
                        }
                    }
                }
            }
        }

        public string GetProcessName(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Processlist = (from row in entities.ProcessCommaSaperates
                                   where row.AuditID == AuditID
                                   select row.ProcessName).FirstOrDefault();

                return Processlist;
            }
        }

        public string GetSubProcessName(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Processlist = (from row in entities.SubProcessCommaSeparates
                                   where row.AuditID == AuditID
                                   select row.SubProcessName).FirstOrDefault();

                return Processlist;
            }
        }
    }
}