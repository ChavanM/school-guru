﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckListPreReqsiteDocuments.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.CheckListPreReqsiteDocuments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/owl.carousel.css" type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/fullcalendar.css" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/jquery-ui-1.10.7.min.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>


    <%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .table-paging-text p {
            color: #555;
            text-align: center;
            font-size: 14px;
            padding-top: 7px;
        }
    </style>

    <script type="text/javascript">
        function ConfirmtestView(ink) {
            $('#DocumentPopUp').modal();
            $('#docViewerAll').attr('src', "../../docviewer.aspx?docurl=" + ink);
        }
        $(document).ready(function () {
            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#DocumentPopUp').modal('hide');
            });
        });
        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        $(document).ready(function () {
            $(function () {
                $('input[id*=txtStartDateKickoff]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });

                $('input[id*=txtStartDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                });
            });
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $(function () {
                    $(function () {
                        $('input[id*=txtStartDateKickoff]').datepicker(
                            {
                                dateFormat: 'dd-mm-yy'
                            });
                        $('input[id*=txtStartDate]').datepicker({
                            dateFormat: 'dd-mm-yy',
                        });
                    });
                });
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upPR">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <asp:UpdatePanel ID="upPR" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12 colpadding0">
                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" class="alert alert-block alert-danger fade in" />
                        <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                    </div>
                    <div id="ActDetails" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <asp:UpdatePanel ID="UpWorkingDocument" runat="server">
                                        <ContentTemplate>

                                            <asp:GridView runat="server" ID="rptComplianceDocumnets"
                                                AutoGenerateColumns="false"
                                                AllowSorting="true" PageSize="10"
                                                AllowPaging="true" GridLines="none" CssClass="table"
                                                Font-Size="12px" Width="100%"
                                                OnRowCommand="rptComplianceDocumnets_RowCommand"
                                                OnRowDeleting="rptComplianceDocumnets_RowDeleting"
                                                OnRowDataBound="rptComplianceDocumnets_RowDataBound"
                                                OnPageIndexChanging="rptComplianceDocumnets_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblATBDId" runat="server" Text='<%# Eval("ATBDId") %>'></asp:Label>
                                                            <asp:Label ID="lblAuditId" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                            <asp:Label ID="lblchecklistId" runat="server" Text='<%# Eval("checklistId") %>'></asp:Label>
                                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Working Document" ItemStyle-Width="70%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="btnComplianceDocumnets1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="CreatedDate" ItemStyle-Width="10%">
                                                        <ItemTemplate>                                                           
                                                            <asp:Label ID="lblcdate" runat="server" Text='<%# Eval("CreatedDate")!=null? Convert.ToDateTime(Eval("CreatedDate")).ToString("dd-MMM-yyyy"):"" %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                     <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%">
                                                        <ItemTemplate>
                                                      
                                                              <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Download/View" ItemStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("Id")%>' CommandName="Download"
                                                                        ID="btnComplianceDocumnets" runat="server" Text='<%# ShowSampleDocumentName((string)Eval("FileName")) %>'>
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton ID="lblViewFile" runat="server" Text='<%# ShowSampleDocumentNameView((string)Eval("FileName")) %>' OnClick="lblViewFile_Click"></asp:LinkButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnComplianceDocumnets" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel runat="server" ID="aa1na" UpdateMode="Always">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                        AutoPostBack="true" CommandName="Delete"
                                                                        OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                        ID="lbtLinkDocbutton" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' 
                                                                                                    alt="Delete" title="Delete" width="15px" height="15px" />
                                                                    </asp:LinkButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="lbtLinkDocbutton" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <EmptyDataTemplate>
                                                    No Records Found.
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div class="col-md-12 colpadding0">                                       
                                        <div class="col-md-1 colpadding0">
                                           
                                             <p style="color: #666; margin-top: 5px; margin-right: 5px;">Remark </p>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" TextMode="MultiLine" Style="height: 100px; width: 945px;"></asp:TextBox>

                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix" style="height:40px;"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-5 colpadding0"></div>
                                        <div class="col-md-1 colpadding0">
                                            <asp:Button Text="Approve" runat="server" ID="btnApprove" Visible="false"
                                                CssClass="btn btn-primary" OnClick="btnApprove_Click" />
                                        </div>
                                        <div class="col-md-1 colpadding0">
                                            <asp:Button Text="Reject" runat="server" ID="btnReject" Visible="false"
                                                CssClass="btn btn-primary" OnClick="btnReject_Click" />
                                        </div>
                                        <div class="col-md-5 colpadding0"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="modal fade" id="DocumentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
