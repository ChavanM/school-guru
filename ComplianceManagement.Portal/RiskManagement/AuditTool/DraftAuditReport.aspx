﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="DraftAuditReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.DraftAuditReport" %>


<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important; 
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        span#ContentPlaceHolder1_rdRiskActivityProcess > Label {
            margin-left: 5px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        input#ContentPlaceHolder1_rdRiskActivityProcess_1 {
            margin-left: 12px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            var filterbytype = ReadQuerySt('Status');
            if (filterbytype == '') {
                fhead('Draft Audit Report');
            } else {
                //$('#pagetype').css("font-size", "20px")
                if (filterbytype == 'Open') {
                    filterbytype = 'Open Audits';
                } else if (filterbytype == 'Closed') {
                    filterbytype = 'Closed Audits';
                }
                fhead('My Workspace/ ' + filterbytype);
            }
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.

            $(function () {
                $('input[id*=txtStartDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                });
            });

            $(function () {
                $('input[id*=txtEndDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                });
            });
        }

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upAuditStatusUI" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                              
                                <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">                                           
                                        <li class="active" id="liProcess" runat="server">
                                            <asp:LinkButton ID="lnkProcess" OnClick="ShowProcessGrid" Visible="false" CausesValidation="false" runat="server">Process</asp:LinkButton>                                           
                                        </li>
                                          
                                        <li class=""  id="liImplementation" runat="server">
                                            <asp:LinkButton ID="lnkImplementation" OnClick="ShowImplementationGrid" Visible="false" CausesValidation="false" runat="server">Implementation</asp:LinkButton>                                        
                                        </li>                                        
                                    </ul>
                                </header>
                              
                                 <div class="clearfix"></div> 

                            <div class="panel-body">

                                 <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="ComplianceInstanceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>

                                 <div class="col-md-12 colpadding0">
                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">
                                        <div class="col-md-2 colpadding0" style="width: 20%;">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" Selected="True" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                    </div>  
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit"  Width="90%"  class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"   AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                </div>
                                     
                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                                    
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit"  Width="90%" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div> 
                                     
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                                   
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit"  Width="90%" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                      
                                </div>
                                     
                                       <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Unit" Width="90%" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"     AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>                       
                                 </div>  

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">                                       
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Location"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"  Width="90%" class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownListChosen>
                                    </div> 
                                               
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                     
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" DataPlaceHolder="Financial Year" AutoPostBack="true"  Width="90%" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:CompareValidator ErrorMessage="Please Select Financial Year." ControlToValidate="ddlFinancialYear"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" Display="None" />                    
                                    </div>    
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true"  Width="90%" DataPlaceHolder="Scheduling Type"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownListChosen>
                                    </div>
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true"  Width="90%" DataPlaceHolder="Period"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownListChosen>
                                    </div> 
                                       <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                      <%{%>
                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">
                                            <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true" DataPlaceHolder="Vertical" Width="90%"
                                           AllowSingleDeselect="false" DisableSearchThreshold="3"       OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;" >
                                            </asp:DropDownListChosen>  
                                        </div>
                                       <%}%>              
                                </div>  
                                                                 
                                <div class="clearfix"></div>
                                
                                </div>  

                             <div class="clearfix"></div>                                                                 
                                <div style="margin-bottom: 4px">                                       
                                      <asp:GridView runat="server" ID="grdProcessAudits" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                   PageSize="5" AllowPaging="true" AutoPostBack="true"  CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                     ShowFooter="true" OnRowCommand="grdProcessAudits_RowCommand">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                         <ItemTemplate>
                                             <%#Container.DataItemIndex+1 %>
                                         </ItemTemplate>
                                         </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                               <div class="text_NlinesusingCSS" style="width: 80px;">
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Process">
                                            <ItemTemplate>
                                                  <div class="text_NlinesusingCSS" style="width: 100px;">
                                                <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("CProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("CProcessName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                     
                                        <asp:TemplateField HeaderText="Financial Year">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 70px;">
                                                <asp:Label ID="lblFinYear" runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Period">
                                            <ItemTemplate>
                                              <div class="text_NlinesusingCSS" style="width: 70px;">
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                          <asp:TemplateField HeaderText="Performer" Visible="false">
                                            <ItemTemplate>
                                               <asp:Label ID="lblPerformer" runat="server" Text='<%# Eval("UserName") %>'  data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="Audit Start Date">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtStartDate" style="width: 110px; text-align: center;" runat="server" Enabled="false" 
                                                    Text='<%# Eval("ExpectedStartDate")!=null? Convert.ToDateTime(Eval("ExpectedStartDate")).ToString("dd-MMM-yyyy"):"" %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>  

                                        <asp:TemplateField HeaderText="Audit End Date">
                                            <ItemTemplate>
                                               <asp:TextBox ID="txtEndDate" runat="server" style="width: 110px; text-align: center;" Enabled="false"
                                                   Text='<%# Eval("ExpectedStartDate")!=null?Convert.ToDateTime(Eval("ExpectedEndDate")).ToString("dd-MMM-yyyy"):"" %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                             

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%" HeaderText="Action">
                                            <ItemTemplate>   
                                                    <asp:UpdatePanel ID="upAu" runat="server">
                                                    <ContentTemplate>                            
                                                    <asp:LinkButton ID="lnkAuditDetails" CssClass="btn btn-primary" runat="server" CommandName="ViewAuditStatusSummary" CommandArgument='<%# Eval("CustomerBranchID") + "," + Eval("ForMonth") + "," + Eval("FinancialYear") +","+Eval("VerticalId")+","+Eval("AuditID") %>'
                                                    CausesValidation="false">Generate Report</asp:LinkButton> <%--CommandArgument='<%# Eval("InternalAuditInstanceID") %>' --%>
                                                    </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lnkAuditDetails" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />   
                                            <PagerSettings Visible="false" />                   
                                    <PagerTemplate>                                      
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate> 
                                </asp:GridView>
                  
                                </div>

                            <div style="margin-bottom:4px;">
                                 <asp:GridView runat="server" ID="grdImplementationAudits" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                   PageSize="5" AllowPaging="true" AutoPostBack="true"  CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                     ShowFooter="true" OnRowCommand="grdImplementationAudits_RowCommand">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                         <ItemTemplate>
                                             <%#Container.DataItemIndex+1 %>
                                         </ItemTemplate>
                                         </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                      </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Process">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("CProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("CProcessName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                     
                                        <asp:TemplateField HeaderText="Financial Year">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblFinYear" runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                      </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Period">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                      </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                       
                                          <asp:TemplateField HeaderText="Performer">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblPerformer" runat="server" Text='<%# Eval("UserName") %>'  data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                                         </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%" HeaderText="Action">
                                            <ItemTemplate>                                               
                                                <asp:LinkButton ID="lnkAuditDetails" runat="server" CommandName="ViewAuditStatusSummary" CommandArgument='<%# Eval("CustomerBranchID") + "," + Eval("ForMonth") + "," + Eval("FinancialYear")+","+Eval("VerticalId")+","+Eval("AuditID")  %>'
                                                    CausesValidation="false"><img src="../../Images/View-icon-new.png" alt="View Audit Details" title="View Audit Details" /></asp:LinkButton> <%--CommandArgument='<%# Eval("InternalAuditInstanceID") %>' --%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />                    
                                    <PagerTemplate>                                       
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate> 
                                </asp:GridView>            
                </div>
                    </div>
                    <div style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                            class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                        </asp:DropDownListChosen>
                    </div>
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-5 colpadding0">
                            <div class="table-Selecteddownload">
                                <div class="table-Selecteddownload-text">
                                    <p>
                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 colpadding0" style="float: right;">
                            <div class="table-paging" style="margin-bottom: 10px;">                                
                                <div class="table-paging-text" style="float: right;">
                                    <p>
                                        Page                                         
                                    </p>
                                </div>                                
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>
                    </div>
                    </section>
                </div>
            </div>
            </div>       
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
