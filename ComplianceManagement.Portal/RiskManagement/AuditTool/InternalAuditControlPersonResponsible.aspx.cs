﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using DropDownListChosen;
namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class InternalAuditControlPersonResponsible : System.Web.UI.Page
    {

        protected string FinYear;
        protected string Period;
        protected int BranchID;
        protected int ATBDID;
        protected int StatusID;
        protected int ProcessID;
        protected int VerticalID;
        protected int AuditID;
        protected int AuditStatusID;
        public static string DocumentPath = "";
        protected int SubProcessID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TDTab.Visible = true;
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Request.QueryString["FinYear"];
                    ViewState["FinYear"] = FinYear;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchID = Convert.ToInt32(Request.QueryString["BID"]);
                    ViewState["BranchID"] = BranchID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    ViewState["AuditID"] = AuditID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                    ViewState["ATBDID"] = VerticalID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                    ViewState["ForMonth"] = Period;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    ViewState["VerticalID"] = VerticalID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditStatusID"]))
                {
                    AuditStatusID = Convert.ToInt32(Request.QueryString["AuditStatusID"]);
                }
                OpenTransactionPage(ATBDID, BranchID, FinYear, Period, VerticalID, AuditStatusID, AuditID);
                //Page Title
                string FY = string.Empty;
                string Period1 = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["FinYear"])))
                {
                    FY = "/ " + Convert.ToString(Request.QueryString["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ForMonth"])))
                {
                    Period1 = "/ " + Convert.ToString(Request.QueryString["ForMonth"]);
                }
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string ControlNo = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                {
                    ControlNo = "/ " + Convert.ToString(Request.QueryString["CID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SPID"]))
                {
                    SubProcessID = Convert.ToInt32(Request.QueryString["SPID"]);
                }
                string Pname = string.Empty;
                string SPname = string.Empty;
                string VName = string.Empty;
                Mst_Process objprocess = new Mst_Process();
                mst_Subprocess objsubProcess = new mst_Subprocess();
                objprocess = ProcessManagement.GetByID(ProcessID, customerID);
                objsubProcess = ProcessManagement.GetSubProcessByID(SubProcessID);
                if (objprocess != null)
                {
                    Pname = objprocess.Name;
                }
                if (objsubProcess != null)
                {
                    SPname = objprocess.Name;
                }

                var BrachName = CustomerBranchManagement.GetByID(BranchID).Name;
                var VerticalName = UserManagementRisk.GetVerticalName(customerID, Convert.ToInt32((Request.QueryString["VID"])));
                if (string.IsNullOrEmpty(VerticalName))
                {
                    VName = "/ " + VerticalName;
                }
                LblPageDetails.InnerText = BrachName + FY + Period1 + Pname + SPname + VName + ControlNo;

                //End
                liAuditCoverage.Attributes.Add("class", "");
                liActualTesting.Attributes.Add("class", "");
                liObservation.Attributes.Add("class", "active");
                liReviewLog.Attributes.Add("class", "");
                MainView.ActiveViewIndex = 2;
            }
            DateTime date = DateTime.MinValue;
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker112", string.Format("initializeConfirmDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "active");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 0;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "active");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 1;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "active");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 2;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void Tab4_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "active");
            MainView.ActiveViewIndex = 3;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
        }
        public void BindObservationSubCategory(int ObservationId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            ddlObservationSubCategory.DataTextField = "Name";
            ddlObservationSubCategory.DataValueField = "ID";
            ddlObservationSubCategory.Items.Clear();
            ddlObservationSubCategory.DataSource = ObservationSubcategory.FillObservationSubCategory(ObservationId, customerID);
            ddlObservationSubCategory.DataBind();
            ddlObservationSubCategory.Items.Insert(0, new ListItem("Select Observation SubCategory", "-1"));
        }
        public void BindUsers(int customerID)
        {
            var AllUser = RiskCategoryManagement.FillUsers(customerID);
            ddlPersonresponsible.Items.Clear();
            ddlPersonresponsible.DataTextField = "Name";
            ddlPersonresponsible.DataValueField = "ID";
            ddlPersonresponsible.DataSource = AllUser;
            ddlPersonresponsible.DataBind();
            ddlPersonresponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));

            ddlPersonresponsibleUH.Items.Clear();
            ddlPersonresponsibleUH.DataTextField = "Name";
            ddlPersonresponsibleUH.DataValueField = "ID";
            ddlPersonresponsibleUH.DataSource = AllUser;
            ddlPersonresponsibleUH.DataBind();
            ddlPersonresponsibleUH.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));

            ddlPersonresponsiblePresident.Items.Clear();
            ddlPersonresponsiblePresident.DataTextField = "Name";
            ddlPersonresponsiblePresident.DataValueField = "ID";
            ddlPersonresponsiblePresident.DataSource = AllUser;
            ddlPersonresponsiblePresident.DataBind();
            ddlPersonresponsiblePresident.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));

            ddlOwnerName.Items.Clear();
            ddlOwnerName.DataTextField = "Name";
            ddlOwnerName.DataValueField = "ID";
            ddlOwnerName.DataSource = AllUser;
            ddlOwnerName.DataBind();
            ddlOwnerName.Items.Insert(0, new ListItem("Select Owner", "-1"));
        }
        public void BindObservationCategory(int customerID)
        {
            ddlObservationCategory.DataTextField = "Name";
            ddlObservationCategory.DataValueField = "ID";
            ddlObservationCategory.Items.Clear();
            ddlObservationCategory.DataSource = ObservationSubcategory.FillObservationCategory(customerID);
            ddlObservationCategory.DataBind();
            ddlObservationCategory.Items.Insert(0, new ListItem("Select Observation Category", "-1"));
        }
        private void BindTransactions(int ScheduledOnID, int Atbtid, int AuditID)
        {
            try
            {
                grdTransactionHistory.DataSource = Business.DashboardManagementRisk.GetAllInternalAuditTransactions(ScheduledOnID, Atbtid, AuditID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRemarks(int ProcessId, int ScheduledOnID, int AibtID, int AuditID)
        {
            try
            {
                GridRemarks.DataSource = Business.DashboardManagementRisk.GetInternalAllRemarks(ProcessId, ScheduledOnID, AibtID, AuditID);
                GridRemarks.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static long GetReviewer(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid, int AuditId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.InternalControlAuditAssignments
                                  join row1 in entities.InternalAuditScheduleOns
                                  on row.ProcessId equals row1.ProcessId
                                  where row.InternalAuditInstance == row1.InternalAuditInstance && row.ProcessId == processid &&
                                  row1.ForMonth == ForPeriod && row1.FinancialYear == FinancialYear
                                  && row1.ID == ScheduledOnID && row.RoleID == 4
                                  && row.AuditID == AuditId
                                  select row.UserID).FirstOrDefault();

                return (long)statusList;
            }
        }
        public void OpenTransactionPage(int ATBDID, int custbranchid, string FinancialYear, string Forperiod, int VerticalID, int AuditStatusID, int AuditID)
        {
            try
            {

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                ViewState["ATBDID"] = null;
                ViewState["ATBDID"] = ATBDID;
                ViewState["VerticalID"] = null;
                ViewState["VerticalID"] = VerticalID;
                ViewState["custbranchid"] = null;
                ViewState["custbranchid"] = custbranchid;
                ViewState["FinancialYear"] = null;
                ViewState["FinancialYear"] = FinancialYear;
                ViewState["Forperiod"] = null;
                ViewState["Forperiod"] = Forperiod;
                BindObservationCategory(customerID);
                BindUsers(customerID);
                tbxDate.Text = string.Empty;
                txtAuditObjective.Enabled = false;
                txtAuditSteps.Enabled = false;
                txtAnalysisToBePerformed.Enabled = false;
                txtWalkthrough.Enabled = false;
                txtActualWorkDone.Enabled = false;
                txtpopulation.Enabled = false;
                txtSample.Enabled = false;
                txtObservationTitle.Enabled = false;
                txtObservationNumber.Enabled = false;
                txtObservation.Enabled = false;
                ObjReport1.Enabled = false;
                txtRisk.Enabled = false;
                txtRootcost.Enabled = false;
                txtAuditStepScore.Enabled = false;
                txtfinancialImpact.Enabled = false;
                txtRecommendation.Enabled = false;
                ddlPersonresponsible.Enabled = false;
                ddlOwnerName.Enabled = false;
                ddlObservationCategory.Enabled = false;
                ddlObservationSubCategory.Enabled = false;
                ddlobservationRating.Enabled = false;
                tbxDate.Enabled = false;
                rptComplianceDocumnets.Enabled = true;
                grdTransactionHistory.Enabled = true;
                GridRemarks.Enabled = true;
                ddlReviewerRiskRatiing.Enabled = false;

                txtAuditObjective.Text = string.Empty;
                txtAuditSteps.Text = string.Empty;
                txtAnalysisToBePerformed.Text = string.Empty;
                txtWalkthrough.Text = string.Empty;
                txtActualWorkDone.Text = string.Empty;
                txtpopulation.Text = string.Empty;
                txtSample.Text = string.Empty;
                txtObservationTitle.Text = string.Empty;
                txtObservationNumber.Enabled = false;
                txtObservation.Text = string.Empty;
                txtRisk.Text = string.Empty;
                txtRootcost.Text = string.Empty;
                txtfinancialImpact.Text = string.Empty;
                txtRecommendation.Text = string.Empty;
                txtMgtResponse.Text = string.Empty;
                txtTimeLine.Text = string.Empty;
                FileObjUpload.Enabled = false;
                btnObjfileupload.Enabled = false;
                //GrdobservationFile.Enabled = false;
                tbxBriefObservation.Text = string.Empty;
                tbxObjBackground.Text = string.Empty;
                ddlDefeciencyType.SelectedValue = "-1";
                txtAuditStepScore.Text = string.Empty;
                ddlPersonresponsible.SelectedValue = "-1";
                ddlOwnerName.SelectedValue = "-1";
                ddlObservationCategory.SelectedValue = "-1";
                ddlobservationRating.SelectedValue = "-1";
                tbxDate.Text = string.Empty;
                rptComplianceDocumnets.DataSource = null;
                rptComplianceDocumnets.DataBind();
                grdTransactionHistory.DataSource = null;
                grdTransactionHistory.DataBind();
                GridRemarks.DataSource = null;
                GridRemarks.DataBind();
                ShowTable.Attributes.Add("class", "disable");
                
                tbxUHComment.Enabled = false;
                tbxPresidentComment.Enabled = false;
                ddlPersonresponsibleUH.Enabled = false;
                ddlPersonresponsiblePresident.Enabled = false;
                tbxTable.Text = string.Empty;
                tbxUHComment.Text = string.Empty;
                tbxPresidentComment.Text = string.Empty;
                ddlPersonresponsibleUH.SelectedValue = "-1";
                ddlPersonresponsiblePresident.SelectedValue = "-1";
                tbxBriefObservation.Enabled = false;
                tbxObjBackground.Enabled = false;
                ddlDefeciencyType.Enabled = false;
                txtMultilineVideolink.Enabled = false;// added by sagar more on 08-01-2020
                txtMultilineVideolink.Text = string.Empty;// added by sagar more on 08-01-2020
                txtAnnexueTitle.Text = string.Empty; // added by sagar more on 09-01-2020
                txtAnnexueTitle.Enabled = false; // added by sagar more on 09-01-2020

                InternalControlAuditResult complianceInfo = new InternalControlAuditResult();
                int ResultID = 0;
                string Tag = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["tag"]))
                {
                    Tag = Convert.ToString(Request.QueryString["tag"]);
                }
                if (Tag == "Implement" && ResultID != 0)
                {
                    complianceInfo = RiskCategoryManagement.GetInternalControlAuditResultbyID(ResultID);
                }
                else
                {
                    complianceInfo = RiskCategoryManagement.GetInternalControlAuditResultByInstanceID(ATBDID, custbranchid, FinancialYear, AuditStatusID, VerticalID, AuditID);
                }


                if (complianceInfo != null)
                {
                    hidObservation.Value = complianceInfo.Observation;
                    hidAuditObjective.Value = complianceInfo.AuditObjective;
                    hidAuditSteps.Value = complianceInfo.AuditSteps;
                    hidAnalysisToBePerformed.Value = complianceInfo.AnalysisToBePerofrmed;
                    hidWalkthrough.Value = complianceInfo.ProcessWalkthrough;
                    hidActualWorkDone.Value = complianceInfo.ActivityToBeDone;
                    hidpopulation.Value = complianceInfo.Population;
                    hidSample.Value = complianceInfo.Sample;
                    hidObservationNumber.Value = complianceInfo.ObservationNumber;
                    hidObservationTitle.Value = complianceInfo.ObservationTitle;
                    hidRisk.Value = complianceInfo.Risk;
                    hidRootcost.Value = complianceInfo.RootCost;
                    hidfinancialImpact.Value = Convert.ToString(complianceInfo.FinancialImpact);
                    hidRecommendation.Value = complianceInfo.Recomendation;
                    hidMgtResponse.Value = complianceInfo.ManagementResponse;
                    hidRemarks.Value = complianceInfo.FixRemark;
                    hidAuditStepScore.Value = Convert.ToString(complianceInfo.AuditScores);
                    hidISACPORMIS.Value = Convert.ToString(complianceInfo.ISACPORMIS);
                    hidTimeLine.Value = complianceInfo.TimeLine != null ? complianceInfo.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                    //New Field
                    hidPersonResponsible.Value = Convert.ToString(complianceInfo.PersonResponsible);
                    hidObservationRating.Value = Convert.ToString(complianceInfo.ObservationRating);
                    hidObservationCategory.Value = Convert.ToString(complianceInfo.ObservationCategory);
                    hidObservationSubCategory.Value = Convert.ToString(complianceInfo.ObservationSubCategory);
                    hidOwner.Value = Convert.ToString(complianceInfo.Owner);
                    hidUserID.Value = Convert.ToString(complianceInfo.UserID);

                    hidUHPersonResponsible.Value = Convert.ToString(complianceInfo.UHPersonResponsible);
                    hidPRESIDENTPersonResponsible.Value = Convert.ToString(complianceInfo.PRESIDENTPersonResponsible);
                    hidUHComment.Value = Convert.ToString(complianceInfo.UHComment);
                    hidPRESIDENTComment.Value = Convert.ToString(complianceInfo.PRESIDENTComment);
                    hidBodyContent.Value = Convert.ToString(complianceInfo.BodyContent);

                    tbxTable.Text = complianceInfo.BodyContent;
                    tbxUHComment.Text = complianceInfo.UHComment;
                    tbxPresidentComment.Text = complianceInfo.PRESIDENTComment;
                    if (!string.IsNullOrEmpty(Convert.ToString(complianceInfo.UHPersonResponsible)))
                    {
                        ddlPersonresponsibleUH.SelectedValue = Convert.ToString(complianceInfo.UHPersonResponsible);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(complianceInfo.PRESIDENTPersonResponsible)))
                    {
                        ddlPersonresponsiblePresident.SelectedValue = Convert.ToString(complianceInfo.PRESIDENTPersonResponsible);
                    }

                    if (complianceInfo.AuditObjective != null)
                    {
                        txtAuditObjective.Text = complianceInfo.AuditObjective.Trim();
                    }
                    if (complianceInfo.AuditSteps != null)
                    {
                        txtAuditSteps.Text = complianceInfo.AuditSteps.Trim();
                    }
                    if (complianceInfo.AnalysisToBePerofrmed != null)
                    {
                        txtAnalysisToBePerformed.Text = complianceInfo.AnalysisToBePerofrmed.Trim();
                    }
                    if (complianceInfo.ProcessWalkthrough != null)
                    {
                        txtWalkthrough.Text = complianceInfo.ProcessWalkthrough.Trim();
                    }
                    if (complianceInfo.ActivityToBeDone != null)
                    {
                        txtActualWorkDone.Text = complianceInfo.ActivityToBeDone.Trim();
                    }
                    if (complianceInfo.Population != null)
                    {
                        txtpopulation.Text = complianceInfo.Population.Trim();
                    }
                    if (complianceInfo.Sample != null)
                    {
                        txtSample.Text = complianceInfo.Sample.Trim();
                    }
                    if (complianceInfo.ObservationNumber != null)
                    {
                        txtObservationNumber.Text = complianceInfo.ObservationNumber.Trim();
                    }
                    if (complianceInfo.ObservationTitle != null)
                    {
                        txtObservationTitle.Text = complianceInfo.ObservationTitle.Trim();
                    }

                    // added by sagar more on 09-01-2020
                    if (complianceInfo.AnnexueTitle != null)
                    {
                        txtAnnexueTitle.Text = complianceInfo.AnnexueTitle.Trim();
                    }
                    if (complianceInfo.Observation != null)
                    {
                        txtObservation.Text = complianceInfo.Observation.Trim();
                    }
                    if (complianceInfo.ISACPORMIS != null)
                    {
                        ObjReport1.SelectedValue = Convert.ToString(complianceInfo.ISACPORMIS);
                    }
                    if (complianceInfo.Risk != null)
                    {
                        txtRisk.Text = complianceInfo.Risk.Trim();
                    }
                    if (complianceInfo.RootCost != null)
                    {
                        txtRootcost.Text = Convert.ToString(complianceInfo.RootCost);
                    }
                    if (complianceInfo.FinancialImpact != null)
                    {
                        txtfinancialImpact.Text = Convert.ToString(complianceInfo.FinancialImpact);
                    }
                    if (complianceInfo.Recomendation != null)
                    {
                        txtRecommendation.Text = complianceInfo.Recomendation.Trim();
                    }
                    if (complianceInfo.ManagementResponse != null)
                    {
                        txtMgtResponse.Text = complianceInfo.ManagementResponse.Trim();
                    }
                    if (complianceInfo.FixRemark != null)
                    {
                        tbxRemarks.Text = complianceInfo.FixRemark;
                    }
                    if (complianceInfo.AuditScores != null)
                    {
                        txtAuditStepScore.Text = Convert.ToString(complianceInfo.AuditScores);
                    }
                    if (complianceInfo.TimeLine != null)
                    {
                        txtTimeLine.Text = Convert.ToDateTime(complianceInfo.TimeLine).ToString("dd-MM-yyyy");
                    }
                    if (complianceInfo.PersonResponsible != null)
                    {
                        if (complianceInfo.PersonResponsible != -1)
                        {
                            ddlPersonresponsible.SelectedValue = Convert.ToString(complianceInfo.PersonResponsible);
                        }
                    }
                    tbxBriefObservation.Text = complianceInfo.BriefObservation;
                    tbxObjBackground.Text = complianceInfo.ObjBackground;
                    if (!string.IsNullOrEmpty(Convert.ToString(complianceInfo.DefeciencyType)))
                    {
                        ddlDefeciencyType.SelectedValue = Convert.ToString(complianceInfo.DefeciencyType);
                    }
                    if (complianceInfo.Owner != null)
                    {
                        if (complianceInfo.Owner != -1)
                        {
                            ddlOwnerName.SelectedValue = Convert.ToString(complianceInfo.Owner);
                        }
                    }
                    if (complianceInfo.ObservationRating != null)
                    {
                        if (complianceInfo.ObservationRating != -1)
                        {
                            ddlobservationRating.SelectedValue = Convert.ToString(complianceInfo.ObservationRating);
                        }
                    }
                    if (complianceInfo.ObservationCategory != null)
                    {
                        if (complianceInfo.ObservationCategory != -1)
                        {
                            ddlObservationCategory.SelectedValue = Convert.ToString(complianceInfo.ObservationCategory);
                            BindObservationSubCategory(Convert.ToInt32(complianceInfo.ObservationCategory));
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(complianceInfo.ObservationSubCategory)))
                    {
                        ddlObservationSubCategory.SelectedValue = Convert.ToString(complianceInfo.ObservationSubCategory);
                    }
                    var getDated = RiskCategoryManagement.GetInternalAuditTransactionbyID(Convert.ToInt64(complianceInfo.AuditScheduleOnID), Convert.ToInt32(complianceInfo.ATBDId), complianceInfo.FinancialYear, complianceInfo.ForPerid, 4, VerticalID, AuditID);
                    if (getDated != null)
                    {
                        tbxDate.Text = getDated.Dated != null ? getDated.Dated.Value.ToString("dd-MM-yyyy") : null;

                        if (getDated.ReviewerRiskRating != null)
                        {
                            if (getDated.ReviewerRiskRating != -1)
                            {
                                ddlReviewerRiskRatiing.SelectedValue = Convert.ToString(getDated.ReviewerRiskRating);
                            }
                        }
                    }
                    if (AuditStatusID == 3 || AuditStatusID == 5)
                    {
                        btnSave.Enabled = false;
                    }
                    if (complianceInfo.AuditeeResponse == "AS")
                    {
                        btnSave.Enabled = false;
                    }
                    BindObservationImages();
                    BindDocument(Convert.ToInt32(complianceInfo.AuditScheduleOnID), Convert.ToInt32(complianceInfo.ATBDId), AuditID);
                    BindObservationDocument(Convert.ToInt32(complianceInfo.AuditScheduleOnID), Convert.ToInt32(complianceInfo.ATBDId), AuditID);
                    BindDocumentAnnxeture(Convert.ToString(Forperiod), Convert.ToString(FinancialYear), Convert.ToInt32(complianceInfo.ATBDId), Convert.ToInt32(complianceInfo.ProcessId), Convert.ToInt32(custbranchid), VerticalID, AuditID);
                    BindTransactions(Convert.ToInt32(complianceInfo.AuditScheduleOnID), Convert.ToInt32(complianceInfo.ATBDId), AuditID);
                    BindRemarks(Convert.ToInt32(complianceInfo.ProcessId), Convert.ToInt32(complianceInfo.AuditScheduleOnID), Convert.ToInt32(complianceInfo.ATBDId), AuditID);
                    ViewState["ScheduledOnID"] = null;
                    ViewState["ScheduledOnID"] = Convert.ToInt32(complianceInfo.AuditScheduleOnID);
                    ViewState["Processed"] = null;
                    ViewState["Processed"] = Convert.ToInt32(complianceInfo.ProcessId);

                    if (GridRemarks.Rows.Count != 0)
                    {
                        tbxRemarks.Enabled = false;
                    }
                    lnkAuditCoverage.Enabled = true;
                    lnkActualTesting.Enabled = true;
                    lnkObservation.Enabled = true;
                    lnkReviewLog.Enabled = true;
                    Tab3_Click(null, null);
                    MainView.ActiveViewIndex = 2;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divComplianceDetailsDialog\").dialog('open');", true);
                }
                else
                {
                    var RiskActivityToBeDoneMapping = RiskCategoryManagement.GetRiskActivityToBeDoneMappingByInstanceID(ATBDID);
                    if (RiskActivityToBeDoneMapping != null)
                    {
                        if (RiskActivityToBeDoneMapping.ActivityTobeDone != null)
                        {
                            txtAuditSteps.Text = RiskActivityToBeDoneMapping.ActivityTobeDone.Trim();
                        }
                    }
                    lnkAuditCoverage.Enabled = true;
                    lnkActualTesting.Enabled = true;
                    lnkObservation.Enabled = true;
                    lnkReviewLog.Enabled = true;
                    Tab3_Click(null, null);
                    MainView.ActiveViewIndex = 2;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divComplianceDetailsDialog\").dialog('open');", true);
                }

                // code added by sagar more on 08-01-2020
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    string audioVideoLink = string.Empty;
                    List<ObservationAudioVideo> observationAudioVideoList = new List<ObservationAudioVideo>();
                    observationAudioVideoList = (from cs in entities.ObservationAudioVideos
                                                 where cs.ATBTID == ATBDID && cs.AuditId == AuditID
                                                 && cs.IsActive == true
                                                 select cs).ToList();
                    if (observationAudioVideoList.Count > 0)
                    {
                        foreach (ObservationAudioVideo item in observationAudioVideoList)
                        {
                            audioVideoLink = audioVideoLink + "," + item.AudioVideoLink;
                        }
                        audioVideoLink = audioVideoLink.TrimStart(',');
                    }
                    txtMultilineVideolink.Text = audioVideoLink;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        #region Document Upload delete download view      
        public void BindDocumentAnnxeture(string Period, string FinancialYear, int ATBDID, int ProcessId, int CustomerBranchId, int VerticalID, int AuditID)
        {
            try
            {

                List<Tran_AnnxetureUpload> ComplianceDocument = new List<Tran_AnnxetureUpload>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetTran_AnnxetureUpload(ATBDID, FinancialYear, Period, ProcessId, CustomerBranchId, VerticalID, AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                if (ComplianceDocument != null)
                {
                    rptComplianceDocumnetsAnnexure.DataSource = ComplianceDocument.ToList();
                    rptComplianceDocumnetsAnnexure.DataBind();
                }
                else
                {
                    rptComplianceDocumnetsAnnexure.DataSource = null;
                    rptComplianceDocumnetsAnnexure.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindDocument(int ScheduledOnID, int ATBDID, int AuditID)
        {
            try
            {
                List<GetInternalAuditDocumentsView> ComplianceDocument = new List<GetInternalAuditDocumentsView>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetInternalAuditDocumentsView(ScheduledOnID, ATBDID, AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                ComplianceDocument = ComplianceDocument.Where(e => e.TypeOfFile != "OB").ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.OrderBy(x => x.FileType).ToList();
                rptComplianceDocumnets.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindObservationDocument(int ScheduledOnID, int ATBDID, int AuditID)
        {
            try
            {
                ViewState["ScheduledOnID"] = ScheduledOnID;
                ViewState["ATBDID"] = ATBDID;
                ViewState["AuditID"] = AuditID;

                List<GetInternalAuditDocumentsView> ComplianceDocument = new List<GetInternalAuditDocumentsView>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetInternalAuditDocumentsView(ScheduledOnID, ATBDID, AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                ComplianceDocument = ComplianceDocument.Where(e => e.TypeOfFile == "OB").ToList();
                GrdobservationFile.DataSource = ComplianceDocument.OrderBy(x => x.FileType).ToList();
                GrdobservationFile.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<InternalReviewHistory> GetFileData(int id, int AuditID, int AuditScheduleOnId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.InternalReviewHistories
                                where row.ID == id
                                //&& row.InternalAuditInstance == InternalAuditInstance
                                && row.AuditID == AuditID
                                && row.AuditScheduleOnID == AuditScheduleOnId
                                select row).ToList();

                return fileData;
            }
        }
        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();

                    }
                }
            }
            catch (Exception)
            {
                throw;

            }
        }
        public void DownloadAnnxetureFile(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Annxeture(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.FileName);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void DownLoadClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblID = (Label)gvr.FindControl("lblID");
                Label lblriskID = (Label)gvr.FindControl("lblriskID");
                Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
                Label lblAuditId = (Label)gvr.FindControl("lblAuditId");

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<InternalReviewHistory> fileData = GetFileData(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblAuditId.Text), Convert.ToInt32(lblAuditScheduleOnId.Text));
                    int i = 0;
                    string directoryName = "Review Comments";
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TestingDocument.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }
        #endregion

        #region Grid Events

        protected void rptComplianceDocumnetsObj_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                    }
                    // DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
                    BindObservationDocument(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetInternalAuditDocumentsView> ComplianceFileData = new List<GetInternalAuditDocumentsView>();
                List<GetInternalAuditDocumentsView> ComplianceDocument = new List<GetInternalAuditDocumentsView>();
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }

                ComplianceDocument = DashboardManagementRisk.GetFileDataGetInternalAuditDocumentsView(Convert.ToInt32(commandArgs[0]), ATBDID, AuditID).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("version"))
                {
                    if (e.CommandName.Equals("version"))
                    {

                    }
                }
                else if (e.CommandName.Equals("Download"))
                {
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        var ComplianceData = DashboardManagementRisk.GetForMonthInternalAuditInstanceTransaction(Convert.ToInt32(commandArgs[0]), ATBDID, AuditID);
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);
                        if (ComplianceFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string[] filename = file.FileName.Split('.');
                                    string str = filename[0] + i + "." + filename[1];
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] data = zipMs.ToArray();

                        Response.Buffer = true;

                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=TestingDocuments.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();
                    }
                }
                //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnets");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbutton");
            if (lbtLinkDocbutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
            }
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(fileID);

                    if (file.FilePath == null)
                    {
                        using (FileStream fs = File.OpenRead(Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name))))
                        {
                            int length = (int)fs.Length;
                            byte[] buffer;

                            using (BinaryReader br = new BinaryReader(fs))
                            {
                                buffer = br.ReadBytes(length);
                            }

                            Response.Buffer = true;
                            Response.Clear();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void GridRemarks_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                GridRemarks.PageIndex = e.NewPageIndex;
                BindRemarks(Convert.ToInt32(ViewState["Processed"]), Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnetsAnnexure_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnetsAnnexure");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void rptComplianceDocumnetsAnnexure_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DownloadAnnuxture"))
                {
                    DownloadAnnxetureFile(Convert.ToInt32(e.CommandArgument));
                }
                //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        #region  Button Click Events
        protected void btnNext1_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                long roleid = 4;
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchID = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchID = Convert.ToInt32(ViewState["BranchID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Convert.ToString(Request.QueryString["ForMonth"]);
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(Request.QueryString["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                var getscheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, FinYear, Period, BranchID, ATBDID, VerticalID, AuditID);
                if (getscheduleondetails != null)
                {
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getscheduleondetails.ID,
                        ProcessId = getscheduleondetails.ProcessId,
                        FinancialYear = FinYear,
                        ForPerid = Period,
                        CustomerBranchId = BranchID,
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                        AuditeeResponse = "AS"
                    };

                    if (tbxTable.Text.Trim() == "")
                        MstRiskResult.BodyContent = "";
                    else
                        MstRiskResult.BodyContent = tbxTable.Text.Trim();

                    if (tbxUHComment.Text.Trim() == "")
                        MstRiskResult.UHComment = "";
                    else
                        MstRiskResult.UHComment = tbxUHComment.Text.Trim();

                    if (tbxPresidentComment.Text.Trim() == "")
                        MstRiskResult.PRESIDENTComment = "";
                    else
                        MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();

                    if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                    {
                        if (ddlPersonresponsibleUH.SelectedValue == "-1")
                        {
                            MstRiskResult.UHPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                    {
                        if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                        }
                    }

                    //End

                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();

                    if (txtActualWorkDone.Text.Trim() == "")
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();

                    if (txtpopulation.Text.Trim() == "")
                        MstRiskResult.Population = "";
                    else
                        MstRiskResult.Population = txtpopulation.Text.Trim();

                    if (txtSample.Text.Trim() == "")
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = txtSample.Text.Trim();

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();

                    // added by sagar more on 09-01-2020
                    if (txtAnnexueTitle.Text.Trim() == "")
                        MstRiskResult.AnnexueTitle = "";
                    else
                        MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();

                    if (txtObservation.Text.Trim() == "")
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = txtObservation.Text.Trim();

                    if (txtRisk.Text.Trim() == "")
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = txtRisk.Text.Trim();

                    if (txtRootcost.Text.Trim() == "")
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();

                    if (txtAuditStepScore.Text.Trim() == "")
                        MstRiskResult.AuditScores = null;
                    else
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());


                    if (txtfinancialImpact.Text.Trim() == "")
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();// changed by sagar more on 24-01-2020

                    if (txtRecommendation.Text.Trim() == "")
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();

                    if (txtMgtResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();

                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                            MstRiskResult.PersonResponsible = null;
                        else
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);

                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                            MstRiskResult.Owner = null;
                        else
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);

                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {

                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {

                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {

                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {

                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {

                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }
                    bool Success = false;
                    var AuditeeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    MstRiskResult.AuditeeResponse = AuditeeResponse;
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                    }
                    if (Success)
                    {
                        //SaveFlag = true;
                        Tab2_Click(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnNext2_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                long roleid = 4;
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchID = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchID = Convert.ToInt32(ViewState["BranchID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Convert.ToString(Request.QueryString["ForMonth"]);
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(Request.QueryString["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                var getscheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, FinYear, Period, BranchID, ATBDID, VerticalID, AuditID);
                if (getscheduleondetails != null)
                {
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getscheduleondetails.ID,
                        ProcessId = getscheduleondetails.ProcessId,
                        FinancialYear = FinYear,
                        ForPerid = Period,
                        CustomerBranchId = BranchID,
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                        AuditeeResponse = "AS"
                    };

                    if (tbxTable.Text.Trim() == "")
                        MstRiskResult.BodyContent = "";
                    else
                        MstRiskResult.BodyContent = tbxTable.Text.Trim();

                    if (tbxUHComment.Text.Trim() == "")
                    {
                        MstRiskResult.UHComment = "";
                    }
                    else
                    {
                        MstRiskResult.UHComment = tbxUHComment.Text.Trim();
                    }

                    if (tbxPresidentComment.Text.Trim() == "")
                    {
                        MstRiskResult.PRESIDENTComment = "";
                    }
                    else
                    {
                        MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                    {
                        if (ddlPersonresponsibleUH.SelectedValue == "-1")
                        {
                            MstRiskResult.UHPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                    {
                        if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                        }
                    }


                    //End

                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();

                    if (txtActualWorkDone.Text.Trim() == "")
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();

                    if (txtpopulation.Text.Trim() == "")
                        MstRiskResult.Population = "";
                    else
                        MstRiskResult.Population = txtpopulation.Text.Trim();

                    if (txtSample.Text.Trim() == "")
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = txtSample.Text.Trim();

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();

                    // added by sagar more on 09-01-2020
                    if (txtAnnexueTitle.Text.Trim() == "")
                        MstRiskResult.AnnexueTitle = "";
                    else
                        MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();

                    if (txtObservation.Text.Trim() == "")
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = txtObservation.Text.Trim();

                    if (txtRisk.Text.Trim() == "")
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = txtRisk.Text.Trim();

                    if (txtRootcost.Text.Trim() == "")
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();

                    if (txtAuditStepScore.Text.Trim() == "")
                        MstRiskResult.AuditScores = null;
                    else
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());


                    if (txtfinancialImpact.Text.Trim() == "")
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();// changed by sagar more on 24-01-2020

                    if (txtRecommendation.Text.Trim() == "")
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();

                    if (txtMgtResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();


                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                            // transaction.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            //transaction.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                        {
                            MstRiskResult.Owner = null;
                            // transaction.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            //transaction.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {
                            // transaction.ObservatioRating = null;
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            //transaction.ObservatioRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {
                            // transaction.ObservationCategory = null;
                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            // transaction.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {

                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }
                    bool Success1 = false;
                    var AuditeeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    MstRiskResult.AuditeeResponse = AuditeeResponse;
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                    }
                    if (Success1)
                    {

                        Tab3_Click(sender, e);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Oops....Error Occured During Saving, Please Try Again";
                    }

                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnNext3_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                long roleid = 4;
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchID = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchID = Convert.ToInt32(ViewState["BranchID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Convert.ToString(Request.QueryString["ForMonth"]);
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(Request.QueryString["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                var getscheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, FinYear, Period, BranchID, ATBDID, VerticalID, AuditID);
                if (getscheduleondetails != null)
                {
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getscheduleondetails.ID,
                        ProcessId = getscheduleondetails.ProcessId,
                        FinancialYear = FinYear,
                        ForPerid = Period,
                        CustomerBranchId = BranchID,
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                        AuditeeResponse = "AS",
                        AnnexueTitle = txtAnnexueTitle.Text.Trim()// added by sagar more 
                    };
                    DateTime dt1 = new DateTime();
                    ObservationHistory objHistory = new ObservationHistory()
                    {
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        RoleID = roleid,
                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        ATBTID = ATBDID,
                    };
                    objHistory.ObservationOld = hidObservation.Value;
                    objHistory.ProcessWalkthroughOld = hidObservation.Value;
                    objHistory.ActualWorkDoneOld = hidActualWorkDone.Value;
                    objHistory.PopulationOld = hidpopulation.Value;
                    objHistory.SampleOld = hidSample.Value;
                    objHistory.ObservationTitleOld = hidObservationTitle.Value;
                    objHistory.RiskOld = hidRisk.Value;
                    objHistory.RootCauseOld = hidRootcost.Value;
                    objHistory.FinancialImpactOld = hidfinancialImpact.Value;
                    objHistory.RecommendationOld = hidRecommendation.Value;
                    objHistory.ManagementResponseOld = hidMgtResponse.Value;
                    objHistory.RemarksOld = hidRemarks.Value;
                    objHistory.ScoreOld = hidAuditStepScore.Value;
                    //New Field

                    //Code added by Sushant
                    if (!string.IsNullOrEmpty(tbxBriefObservation.Text.Trim()))
                    {
                        MstRiskResult.BriefObservation = tbxBriefObservation.Text.Trim();
                        // transaction.BriefObservation = tbxBriefObservation.Text.Trim();
                    }
                    else
                    {
                        MstRiskResult.BriefObservation = null;
                        // transaction.BriefObservation = null;
                    }
                    if (!string.IsNullOrEmpty(tbxObjBackground.Text.Trim()))
                    {
                        MstRiskResult.ObjBackground = tbxObjBackground.Text.Trim();
                    }
                    else
                    {
                        MstRiskResult.ObjBackground = null;
                    }
                    if (ddlDefeciencyType.SelectedValue != "-1")
                    {
                        MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                        // transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                    }
                    else
                    {
                        MstRiskResult.DefeciencyType = null;
                        // transaction.DefeciencyType = null;
                    }
                    objHistory.OldBodyContent = hidBodyContent.Value;
                    objHistory.OldUHComment = hidUHComment.Value;
                    objHistory.OldPRESIDENTComment = hidPRESIDENTComment.Value;
                    if (!string.IsNullOrEmpty(hidUHPersonResponsible.Value))
                    {
                        objHistory.OldUHPersonResponsible = Convert.ToInt32(hidUHPersonResponsible.Value);
                    }
                    if (!string.IsNullOrEmpty(hidPRESIDENTPersonResponsible.Value))
                    {
                        objHistory.OldPRESIDENTPersonResponsible = Convert.ToInt32(hidPRESIDENTPersonResponsible.Value);
                    }


                    if (tbxTable.Text.Trim() == "")
                    {
                        MstRiskResult.BodyContent = "";
                        objHistory.BodyContent = "";
                    }
                    else
                    {
                        MstRiskResult.BodyContent = tbxTable.Text.Trim();
                        objHistory.BodyContent = tbxTable.Text.Trim();
                    }

                    if (tbxUHComment.Text.Trim() == "")
                    {
                        MstRiskResult.UHComment = "";
                        objHistory.UHComment = "";
                    }
                    else
                    {
                        MstRiskResult.UHComment = tbxUHComment.Text.Trim();
                        objHistory.UHComment = tbxUHComment.Text.Trim();
                    }

                    if (tbxPresidentComment.Text.Trim() == "")
                    {
                        MstRiskResult.PRESIDENTComment = "";
                        objHistory.PRESIDENTComment = "";
                    }
                    else
                    {
                        MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                        objHistory.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                    {
                        if (ddlPersonresponsibleUH.SelectedValue == "-1")
                        {
                            MstRiskResult.UHPersonResponsible = null;
                            objHistory.UHPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                            objHistory.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                    {
                        if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = null;
                            objHistory.PRESIDENTPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                            objHistory.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                        }
                    }
                    //End

                    if (!string.IsNullOrEmpty(hidPersonResponsible.Value))
                    {
                        objHistory.PersonResponsibleOld = Convert.ToInt32(hidPersonResponsible.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationRating.Value))
                    {
                        objHistory.ObservationRatingOld = Convert.ToInt32(hidObservationRating.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationCategory.Value))
                    {
                        objHistory.ObservationCategoryOld = Convert.ToInt32(hidObservationCategory.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationSubCategory.Value))
                    {
                        objHistory.ObservationSubCategoryOld = Convert.ToInt32(hidObservationSubCategory.Value);
                    }
                    if (!string.IsNullOrEmpty(hidOwner.Value))
                    {
                        objHistory.OwnerOld = Convert.ToInt32(hidOwner.Value);
                    }
                    if (!string.IsNullOrEmpty(hidUserID.Value))
                    {
                        objHistory.UserOld = Convert.ToInt32(hidUserID.Value);
                    }
                    if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                    {
                        objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                    }

                    if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                    {
                        objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                    }
                    DateTime dt = new DateTime();
                    dt = new DateTime();
                    if (!string.IsNullOrEmpty(hidTimeLine.Value))
                    {
                        dt = DateTime.ParseExact(hidTimeLine.Value, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        objHistory.TimeLineOld = dt.Date;
                    }
                    objHistory.Remarks = tbxRemarks.Text;
                    dt1 = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt1 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        objHistory.TimeLine = dt1.Date;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                    {
                        objHistory.AuditId = AuditID;
                    }

                    if (ObjReport1.SelectedItem != null)
                    {
                        MstRiskResult.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                        objHistory.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);

                    }
                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                    {
                        MstRiskResult.ProcessWalkthrough = "";
                        objHistory.ProcessWalkthrough = "";
                    }
                    else
                    {
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                        objHistory.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                    }

                    if (txtActualWorkDone.Text.Trim() == "")
                    {
                        MstRiskResult.ActivityToBeDone = "";
                        objHistory.ActualWorkDone = "";
                    }
                    else
                    {
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();
                        objHistory.ActualWorkDone = txtActualWorkDone.Text.Trim();
                    }

                    if (txtpopulation.Text.Trim() == "")
                    {
                        MstRiskResult.Population = "";
                        objHistory.Population = "";
                    }
                    else
                    {
                        MstRiskResult.Population = txtpopulation.Text.Trim();
                        objHistory.Population = txtpopulation.Text.Trim();
                    }
                    if (txtSample.Text.Trim() == "")
                    {
                        MstRiskResult.Sample = "";
                        objHistory.Sample = "";
                    }
                    else
                    {
                        MstRiskResult.Sample = txtSample.Text.Trim();
                        objHistory.Sample = txtSample.Text.Trim();
                    }

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                    {
                        MstRiskResult.ObservationTitle = "";
                        objHistory.ObservationTitle = "";
                    }
                    else
                    {
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();
                        objHistory.ObservationTitle = txtObservationTitle.Text;
                    }

                    //added by sagar more on 09-01-2020
                    if (txtAnnexueTitle.Text.Trim() == "")
                        MstRiskResult.AnnexueTitle = "";
                    else
                        MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();

                    if (txtObservation.Text.Trim() == "")
                    {
                        MstRiskResult.Observation = "";
                        objHistory.Observation = null;
                    }
                    else
                    {
                        MstRiskResult.Observation = txtObservation.Text.Trim();
                        objHistory.Observation = txtObservation.Text.Trim();
                    }

                    if (txtRisk.Text.Trim() == "")
                    {
                        MstRiskResult.Risk = "";
                        objHistory.Risk = "";
                    }
                    else
                    {
                        MstRiskResult.Risk = txtRisk.Text.Trim();
                        objHistory.Risk = txtRisk.Text;
                    }

                    if (txtRootcost.Text.Trim() == "")
                    {
                        MstRiskResult.RootCost = null;
                        objHistory.RootCause = null;
                    }
                    else
                    {
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();
                        objHistory.RootCause = txtRootcost.Text.Trim();
                    }

                    if (txtAuditStepScore.Text.Trim() == "")
                    {
                        MstRiskResult.AuditScores = null;
                        objHistory.Score = null;
                    }
                    else
                    {
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());
                        objHistory.Score = txtAuditStepScore.Text;
                    }

                    if (txtfinancialImpact.Text.Trim() == "")
                    {
                        MstRiskResult.FinancialImpact = null;
                        objHistory.FinancialImpact = null;
                    }
                    else
                    {
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();// changed by sagar more on 24-01-2020
                        objHistory.FinancialImpact = txtfinancialImpact.Text;
                    }
                    if (txtRecommendation.Text.Trim() == "")
                    {
                        MstRiskResult.Recomendation = "";
                        objHistory.Recommendation = "";
                    }
                    else
                    {
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();
                        objHistory.Recommendation = txtRecommendation.Text.Trim();
                    }

                    if (txtMgtResponse.Text.Trim() == "")
                    {
                        MstRiskResult.ManagementResponse = "";
                        objHistory.ManagementResponse = "";
                    }
                    else
                    {
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();
                        objHistory.ManagementResponse = txtMgtResponse.Text.Trim();
                    }

                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                    DateTime dt2 = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt2 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt2.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                            objHistory.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            objHistory.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                        {
                            MstRiskResult.Owner = null;
                            objHistory.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            objHistory.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationRating = null;
                            objHistory.ObservationRating = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            objHistory.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationCategory = null;
                            objHistory.ObservationCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            objHistory.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {
                            objHistory.ObservationSubCategory = null;
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            objHistory.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }
                    MstRiskResult.AStatusId = 6;
                    bool Success1 = false;
                    var AuditeeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    MstRiskResult.AuditeeResponse = AuditeeResponse;
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        if (RiskCategoryManagement.InternalControlResultExistsCheckLastestNullWithStatus(MstRiskResult))
                        {
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                        }
                        else
                        {
                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                        }
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                    }
                    if (Success1)
                    {
                        if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                        {
                            RiskCategoryManagement.AddObservationHistory(objHistory);
                        }
                        Tab4_Click(sender, e); UpdatePanel2.Update();
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }                   
                }
                //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(txtObservation.Text.Trim()))
                {
                    if (string.IsNullOrEmpty(txtMgtResponse.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Management Response.";
                        MainView.ActiveViewIndex = 2;
                        txtMgtResponse.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter TimeLine.";
                        MainView.ActiveViewIndex = 2;
                        txtTimeLine.Focus();
                        return;
                    }
                }

                bool Flag = false;
                long roleid = 4;
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchID = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchID = Convert.ToInt32(ViewState["BranchID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Convert.ToString(Request.QueryString["ForMonth"]);
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(Request.QueryString["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var getscheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, FinYear, Period, BranchID, ATBDID, VerticalID, AuditID);
                if (getscheduleondetails != null)
                {

                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getscheduleondetails.ID,
                        ProcessId = getscheduleondetails.ProcessId,
                        FinancialYear = FinYear,
                        ForPerid = Period,
                        CustomerBranchId = BranchID,
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                        AuditeeResponse = "AS"
                    };
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        StatusChangedOn = GetDate(b.ToString("dd/MM/yyyy")),
                        AuditScheduleOnID = getscheduleondetails.ID,
                        FinancialYear = FinYear,
                        CustomerBranchId = BranchID,
                        InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                        ProcessId = getscheduleondetails.ProcessId,
                        ForPeriod = Period,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        VerticalID = VerticalID,
                        AuditID = AuditID
                    };
                    DateTime dt1 = new DateTime();
                    ObservationHistory objHistory = new ObservationHistory()
                    {
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        RoleID = roleid,
                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        ATBTID = ATBDID
                    };
                    objHistory.ObservationOld = hidObservation.Value;
                    objHistory.ProcessWalkthroughOld = hidObservation.Value;
                    objHistory.ActualWorkDoneOld = hidActualWorkDone.Value;
                    objHistory.PopulationOld = hidpopulation.Value;
                    objHistory.SampleOld = hidSample.Value;
                    objHistory.ObservationTitleOld = hidObservationTitle.Value;
                    objHistory.RiskOld = hidRisk.Value;
                    objHistory.RootCauseOld = hidRootcost.Value;
                    objHistory.FinancialImpactOld = hidfinancialImpact.Value;
                    objHistory.RecommendationOld = hidRecommendation.Value;
                    objHistory.ManagementResponseOld = hidMgtResponse.Value;
                    objHistory.RemarksOld = hidRemarks.Value;
                    objHistory.ScoreOld = hidAuditStepScore.Value;
                    //New Field

                    //Code added by Sushant
                    if (!string.IsNullOrEmpty(tbxBriefObservation.Text.Trim()))
                    {
                        MstRiskResult.BriefObservation = tbxBriefObservation.Text.Trim();
                        transaction.BriefObservation = tbxBriefObservation.Text.Trim();
                    }
                    else
                    {
                        MstRiskResult.BriefObservation = null;
                        transaction.BriefObservation = null;
                    }
                    if (!string.IsNullOrEmpty(tbxObjBackground.Text.Trim()))
                    {
                        MstRiskResult.ObjBackground = tbxObjBackground.Text.Trim();
                        transaction.ObjBackground = tbxObjBackground.Text.Trim();
                    }
                    else
                    {
                        MstRiskResult.ObjBackground = null;
                        transaction.ObjBackground = null;
                    }
                    if (ddlDefeciencyType.SelectedValue != "-1")
                    {
                        MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                        transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                    }
                    else
                    {
                        MstRiskResult.DefeciencyType = null;
                        transaction.DefeciencyType = null;
                    }
                    objHistory.OldBodyContent = hidBodyContent.Value;
                    objHistory.OldUHComment = hidUHComment.Value;
                    objHistory.OldPRESIDENTComment = hidPRESIDENTComment.Value;
                    if (!string.IsNullOrEmpty(hidUHPersonResponsible.Value))
                    {
                        objHistory.OldUHPersonResponsible = Convert.ToInt32(hidUHPersonResponsible.Value);
                    }
                    if (!string.IsNullOrEmpty(hidPRESIDENTPersonResponsible.Value))
                    {
                        objHistory.OldPRESIDENTPersonResponsible = Convert.ToInt32(hidPRESIDENTPersonResponsible.Value);
                    }
                    
                    if (tbxTable.Text.Trim() == "")
                    {
                        MstRiskResult.BodyContent = "";
                        objHistory.BodyContent = "";
                        transaction.BodyContent = "";
                    }
                    else
                    {
                        MstRiskResult.BodyContent = tbxTable.Text.Trim();
                        objHistory.BodyContent = tbxTable.Text.Trim();
                        transaction.BodyContent = tbxTable.Text.Trim();
                    }

                    if (tbxUHComment.Text.Trim() == "")
                    {
                        MstRiskResult.UHComment = "";
                        objHistory.UHComment = "";
                        transaction.UHComment = "";
                    }
                    else
                    {
                        MstRiskResult.UHComment = tbxUHComment.Text.Trim();
                        objHistory.UHComment = tbxUHComment.Text.Trim();
                        transaction.UHComment = tbxUHComment.Text.Trim();
                    }

                    if (tbxPresidentComment.Text.Trim() == "")
                    {
                        MstRiskResult.PRESIDENTComment = "";
                        objHistory.PRESIDENTComment = "";
                        transaction.PRESIDENTComment = "";
                    }
                    else
                    {
                        MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                        objHistory.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                        transaction.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                    {
                        if (ddlPersonresponsibleUH.SelectedValue == "-1")
                        {
                            MstRiskResult.UHPersonResponsible = null;
                            objHistory.UHPersonResponsible = null;
                            transaction.UHPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                            objHistory.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                            transaction.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                    {
                        if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = null;
                            objHistory.PRESIDENTPersonResponsible = null;
                            transaction.PRESIDENTPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                            objHistory.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                            transaction.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                        }
                    }

                    //End

                    if (!string.IsNullOrEmpty(hidPersonResponsible.Value))
                    {
                        objHistory.PersonResponsibleOld = Convert.ToInt32(hidPersonResponsible.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationRating.Value))
                    {
                        objHistory.ObservationRatingOld = Convert.ToInt32(hidObservationRating.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationCategory.Value))
                    {
                        objHistory.ObservationCategoryOld = Convert.ToInt32(hidObservationCategory.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationSubCategory.Value))
                    {
                        objHistory.ObservationSubCategoryOld = Convert.ToInt32(hidObservationSubCategory.Value);
                    }
                    if (!string.IsNullOrEmpty(hidOwner.Value))
                    {
                        objHistory.OwnerOld = Convert.ToInt32(hidOwner.Value);
                    }
                    if (!string.IsNullOrEmpty(hidUserID.Value))
                    {
                        objHistory.UserOld = Convert.ToInt32(hidUserID.Value);
                    }
                    if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                    {
                        objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                    }

                    if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                    {
                        objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                    }
                    DateTime dt = new DateTime();
                    dt = new DateTime();
                    if (!string.IsNullOrEmpty(hidTimeLine.Value))
                    {
                        dt = DateTime.ParseExact(hidTimeLine.Value, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        objHistory.TimeLineOld = dt.Date;
                    }
                    objHistory.Remarks = tbxRemarks.Text;
                    dt1 = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt1 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        objHistory.TimeLine = dt1.Date;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                    {
                        objHistory.AuditId = AuditID;
                    }
                    if (ObjReport1.SelectedItem != null)
                    {
                        MstRiskResult.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                        objHistory.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);

                    }
                    string remark = string.Empty;

                    transaction.StatusId = 6;
                    MstRiskResult.AStatusId = 6;
                    remark = "Audit Steps Under Auditee Review.";


                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                    {
                        MstRiskResult.ProcessWalkthrough = "";
                        objHistory.ProcessWalkthrough = "";
                    }
                    else
                    {
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                        objHistory.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                    }

                    if (txtActualWorkDone.Text.Trim() == "")
                    {
                        MstRiskResult.ActivityToBeDone = "";
                        objHistory.ActualWorkDone = "";
                    }
                    else
                    {
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();
                        objHistory.ActualWorkDone = txtActualWorkDone.Text.Trim();
                    }

                    if (txtpopulation.Text.Trim() == "")
                    {
                        MstRiskResult.Population = "";
                        objHistory.Population = "";
                    }
                    else
                    {
                        MstRiskResult.Population = txtpopulation.Text.Trim();
                        objHistory.Population = txtpopulation.Text.Trim();
                    }
                    if (txtSample.Text.Trim() == "")
                    {
                        MstRiskResult.Sample = "";
                        objHistory.Sample = "";
                    }
                    else
                    {
                        MstRiskResult.Sample = txtSample.Text.Trim();
                        objHistory.Sample = txtSample.Text.Trim();
                    }

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                    {
                        MstRiskResult.ObservationTitle = "";
                        objHistory.ObservationTitle = "";
                    }
                    else
                    {
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();
                        objHistory.ObservationTitle = txtObservationTitle.Text;
                    }

                    // added by sagar more on 09-01-2020
                    if (txtAnnexueTitle.Text.Trim() == "")
                        MstRiskResult.AnnexueTitle = "";
                    else
                        MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();

                    if (txtObservation.Text.Trim() == "")
                    {
                        MstRiskResult.Observation = "";
                        objHistory.Observation = null;
                    }
                    else
                    {
                        MstRiskResult.Observation = txtObservation.Text.Trim();
                        objHistory.Observation = txtObservation.Text.Trim();
                    }

                    if (txtRisk.Text.Trim() == "")
                    {
                        MstRiskResult.Risk = "";
                        objHistory.Risk = "";
                    }
                    else
                    {
                        MstRiskResult.Risk = txtRisk.Text.Trim();
                        objHistory.Risk = txtRisk.Text;
                    }

                    if (txtRootcost.Text.Trim() == "")
                    {
                        MstRiskResult.RootCost = null;
                        objHistory.RootCause = null;
                    }
                    else
                    {
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();
                        objHistory.RootCause = txtRootcost.Text.Trim();
                    }

                    if (txtAuditStepScore.Text.Trim() == "")
                    {
                        MstRiskResult.AuditScores = null;
                        objHistory.Score = null;
                    }
                    else
                    {
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());
                        objHistory.Score = txtAuditStepScore.Text;
                    }

                    if (txtfinancialImpact.Text.Trim() == "")
                    {
                        MstRiskResult.FinancialImpact = null;
                        objHistory.FinancialImpact = null;
                    }
                    else
                    {
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();// changed by sagar more on 24-01-2020
                        objHistory.FinancialImpact = txtfinancialImpact.Text;
                    }
                    if (txtRecommendation.Text.Trim() == "")
                    {
                        MstRiskResult.Recomendation = "";
                        objHistory.Recommendation = "";
                    }
                    else
                    {
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();
                        objHistory.Recommendation = txtRecommendation.Text.Trim();
                    }

                    if (txtMgtResponse.Text.Trim() == "")
                    {
                        MstRiskResult.ManagementResponse = "";
                        objHistory.ManagementResponse = "";
                    }
                    else
                    {
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();
                        objHistory.ManagementResponse = txtMgtResponse.Text.Trim();
                    }
                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                    DateTime dt3 = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt3 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt3.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                            transaction.PersonResponsible = null;
                            objHistory.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            transaction.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            objHistory.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                        {
                            MstRiskResult.Owner = null;
                            transaction.Owner = null;
                            objHistory.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            transaction.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            objHistory.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {
                            transaction.ObservatioRating = null;
                            MstRiskResult.ObservationRating = null;
                            objHistory.ObservationRating = null;
                        }
                        else
                        {
                            transaction.ObservatioRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            objHistory.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {
                            transaction.ObservationCategory = null;
                            MstRiskResult.ObservationCategory = null;
                            objHistory.ObservationCategory = null;
                        }
                        else
                        {
                            transaction.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            objHistory.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {
                            transaction.ObservationSubCategory = null;
                            MstRiskResult.ObservationSubCategory = null;
                            objHistory.ObservationSubCategory = null;
                        }
                        else
                        {
                            transaction.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            objHistory.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }

                    transaction.Remarks = remark.Trim();
                    if (!string.IsNullOrEmpty(ddlReviewerRiskRatiing.SelectedValue))
                    {
                        if (ddlReviewerRiskRatiing.SelectedValue == "-1")
                            transaction.ReviewerRiskRating = null;
                        else
                            transaction.ReviewerRiskRating = Convert.ToInt32(ddlReviewerRiskRatiing.SelectedValue);
                    }

                    string Testremark = "";
                    if (string.IsNullOrEmpty(txtRemark.Text))
                    {
                        Testremark = "NA";
                    }
                    else
                    {
                        Testremark = txtRemark.Text.Trim();
                    }
                    bool Success1 = false;
                    bool Success2 = false;
                    if (RiskCategoryManagement.InternalControlResultExistsCheckLastestNullWithStatus(MstRiskResult))
                    {
                        if (MstRiskResult.AStatusId == 6)
                        {
                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                        }
                        else
                        {
                            if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                            {
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                            }
                        }
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                    }


                    if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                    {
                        transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        transaction.CreatedOn = DateTime.Now;
                        Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                    }
                    else
                    {
                        transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        transaction.CreatedOn = DateTime.Now;
                        Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                    }
                    if (RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                    {
                        RiskCategoryManagement.AddObservationHistory(objHistory);
                    }


                    HttpFileCollection fileCollection2 = this.Request.Files;
                    // string UniqueFlag = DateTime.Now.ToString("yyyyMMddHHmmss");
                    if (fileCollection2.Count > 0)
                    {
                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                        //int? customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        var InstanceData = RiskCategoryManagement.GetInternalAuditInstanceData(Convert.ToInt32(getscheduleondetails.InternalAuditInstance));
                        string directoryPath1 = "";

                        directoryPath1 = Server.MapPath("~/InternalTestingAdditionalDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + getscheduleondetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + getscheduleondetails.ID + "/1.0");
                        DocumentManagement.CreateDirectory(directoryPath1);
                        for (int i = 0; i < fileCollection2.Count; i++)
                        {
                            HttpPostedFile uploadfile1 = fileCollection2[i];
                            string[] keys1 = fileCollection2.Keys[i].Split('$');
                            String fileName1 = "";
                            if (keys1[keys1.Count() - 1].Equals("FileuploadAdditionalFile"))
                            {
                                fileName1 = uploadfile1.FileName;
                            }
                            Guid fileKey1 = Guid.NewGuid();
                            string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadfile1.FileName));
                            Stream fs = uploadfile1.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                            if (uploadfile1.ContentLength > 0)
                            {
                                InternalReviewHistory RH = new InternalReviewHistory()
                                {
                                    ATBDId = ATBDID,
                                    ProcessId = Convert.ToInt32(getscheduleondetails.ProcessId),
                                    InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                    Dated = DateTime.Now,
                                    Remarks = Testremark,
                                    AuditScheduleOnID = getscheduleondetails.ID,
                                    FinancialYear = getscheduleondetails.FinancialYear,
                                    CustomerBranchId = BranchID,
                                    Name = fileName1,
                                    FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                    FileKey = fileKey1.ToString(),
                                    Version = "1.0",
                                    VersionDate = DateTime.UtcNow,
                                    FixRemark = tbxRemarks.Text.Trim(),
                                    VerticalID = VerticalID,
                                    AuditID = AuditID,
                                    //  UniqueFlag= UniqueFlag
                                };
                                Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                                DocumentManagement.Audit_SaveDocFiles(Filelist1);
                            }
                            else
                            {
                                InternalReviewHistory RH = new InternalReviewHistory()
                                {
                                    ProcessId = Convert.ToInt32(getscheduleondetails.ProcessId),
                                    InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                    Dated = DateTime.Now,
                                    Remarks = Testremark,
                                    AuditScheduleOnID = getscheduleondetails.ID,
                                    FinancialYear = getscheduleondetails.FinancialYear,
                                    CustomerBranchId = BranchID,
                                    ATBDId = ATBDID,
                                    FixRemark = tbxRemarks.Text.Trim(),
                                    VerticalID = VerticalID,
                                    AuditID = AuditID,
                                    //  UniqueFlag = UniqueFlag,
                                };
                                Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                            }
                        }
                    }
                    else
                    {
                        InternalReviewHistory RH = new InternalReviewHistory()
                        {
                            ProcessId = Convert.ToInt32(getscheduleondetails.ProcessId),
                            InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                            Dated = DateTime.Now,
                            Remarks = Testremark,
                            AuditScheduleOnID = getscheduleondetails.ID,
                            FinancialYear = getscheduleondetails.FinancialYear,
                            CustomerBranchId = BranchID,
                            ATBDId = ATBDID,
                            FixRemark = tbxRemarks.Text.Trim(),
                            VerticalID = VerticalID,
                            AuditID = AuditID,
                            // UniqueFlag= UniqueFlag
                        };
                        Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                    }
                    BindRemarks(Convert.ToInt32(getscheduleondetails.ProcessId), Convert.ToInt32(getscheduleondetails.ID), ATBDID, AuditID);
                    if (Success1 == true && Success2 == true && Flag == true)
                    {
                        //ProcessReminderOnAuditeeReviewReviewer(getscheduleondetails.ForMonth, getscheduleondetails.FinancialYear, Convert.ToInt32(getscheduleondetails.ID), Convert.ToInt32(getscheduleondetails.ProcessId), AuditID);
                        BindTransactions(Convert.ToInt32(getscheduleondetails.ID), ATBDID, AuditID);

                        lnkAuditCoverage.Enabled = false;
                        lnkActualTesting.Enabled = false;
                        lnkObservation.Enabled = false;
                        lnkReviewLog.Enabled = false;

                        txtRemark.Text = string.Empty;
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Audit Steps Submitted Successfully";
                        btnSave.Enabled = false;
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }

                }
                //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        #region Email
        //private void ProcessReminderOnAuditeeReviewReviewer(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid,int AuditID)
        //{
        //    try
        //    {
        //        long userid = GetReviewer(ForPeriod, FinancialYear, ScheduledOnID, processid, AuditID);
        //        var user = UserManagement.GetByID(Convert.ToInt32(userid));
        //        if (user != null)
        //        {
        //            int customerID = -1;
        //            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //            string ReplyEmailAddressName = CustomerManagementRisk.GetByID(customerID).Name;

        //            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //            string message = Properties.Settings.Default.EmailTemplate_AuditeeReviewAuditReviewer
        //                 .Replace("@User", username)
        //                 .Replace("@PeriodName", ForPeriod)
        //                 .Replace("@FinancialYear", FinancialYear)
        //                 .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                 .Replace("@From", ReplyEmailAddressName);

        //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Audit Reminder on Auditee Review", message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        #endregion

        #region Dropdownchange
        protected void ddlObservationCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
            {
                if (ddlObservationCategory.SelectedValue != "-1")
                {
                    BindObservationSubCategory(Convert.ToInt32(ddlObservationCategory.SelectedValue));
                }
            }
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        #endregion

        protected void btnAddTable_Click(object sender, EventArgs e)
        {
            ShowTable.Visible = true;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }


        public void BindObservationImages()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
            {
                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
            }
            else
            {
                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
            }
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ImageListdata = (from row in entities.ObservationImages
                                     where row.ATBTID == ATBDID &&
                                     row.AuditID == AuditID && row.IsActive == true
                                     select row).ToList();

                if (ImageListdata.Count > 0)
                {
                    grdObservationImg.DataSource = ImageListdata;
                    grdObservationImg.DataBind();
                    UpdatePanel1.Update();
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }
                else
                {
                    // added by sagar more on 23-01-2020
                    grdObservationImg.DataSource = null;
                    grdObservationImg.DataBind();
                    UpdatePanel1.Update();
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }
            }
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void grdObservationImg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdObservationImg.PageIndex = e.NewPageIndex;
            BindObservationImages();
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }

        protected void grdObservationImg_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DeleteImage"))
                    {
                        int ID = Convert.ToInt32(e.CommandArgument);
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var Imagelist = (from row in entities.ObservationImages
                                             where row.ID == ID
                                             select row).FirstOrDefault();
                            if (Imagelist != null)
                            {
                                Imagelist.IsActive = false;
                                entities.SaveChanges();
                                BindObservationImages();
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewImage"))
                    {
                        ObservationImage AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFile(Convert.ToInt32(e.CommandArgument));
                        if (AllinOneDocumentList != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.ImagePath), AllinOneDocumentList.ImageName);
                            if (AllinOneDocumentList.ImagePath != null && File.Exists(filePath))
                            {
                                DocumentPath = filePath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            else
                            {
                                lblMessage.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                            }
                        }
                        //ObservationImage AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFile(Convert.ToInt32(e.CommandArgument));
                        //if (AllinOneDocumentList != null)
                        //{
                        //    string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.ImagePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.ImageName));
                        //    if (AllinOneDocumentList.ImagePath != null && File.Exists(filePath))
                        //    {
                        //        string Folder = "~/TempFiles";
                        //        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                        //        string DateFolder = Folder + "/" + File;

                        //        string extension = System.IO.Path.GetExtension(filePath);

                        //        Directory.CreateDirectory(Server.MapPath(DateFolder));

                        //        if (!Directory.Exists(DateFolder))
                        //        {
                        //            Directory.CreateDirectory(Server.MapPath(DateFolder));
                        //        }

                        //        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                        //        int customerID = -1;
                        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        //        string User = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                        //        string FileName = DateFolder + "/" + User + "" + extension;

                        //        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        //        BinaryWriter bw = new BinaryWriter(fs);
                        //        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                        //        bw.Close();
                        //        DocumentPath = FileName;
                        //        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                        //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + DocumentPath + "');", true);
                        //        lblMessage.Text = "";
                        //    }
                        //    else
                        //    {
                        //        lblMessage.Text = "There is no file to preview";
                        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                        //    }
                        //}
                    }
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}