﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteAssigment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.DeleteAssigment" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Delete Control Assignment</title>

  
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />        
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>    
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 30px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
        .dd_chk_drop{
            top:30px !important;
        }
    </style>
     <script type="text/javascript">
         function CloseModel() {
             window.parent.CloseModelDelete();
         }
    </script>

    
</head>
<body style=" overflow-y:hidden;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upComplianceTypeList">
            <ProgressTemplate>
                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                        AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="row Dashboard-white-widget" style="height: 490px; overflow-y: scroll;">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12 ">
                            <section class="panel">
                                <div class="col-md-12 colpadding0">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ForeColor="Red"
                                        ValidationGroup="ComplianceInstanceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true"
                                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                </div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="float: left; width: 75px; margin-bottom: 0px;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" Selected="True" />
                                            <asp:ListItem Text="100" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;"></div>
                                    <div class="col-md-2 colpadding0" style="margin-top: 5px;"></div>
                                    <div class="col-md-4 colpadding0" style="margin-top: 5px;">
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" class="form-control m-bot15" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background: none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ErrorMessage="Select Location" ForeColor="Red" InitialValue=""
                                            Font-Size="0.9em" ControlToValidate="ddlLegalEntity"
                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>

                                        <asp:CompareValidator ErrorMessage="Select Location" ControlToValidate="ddlLegalEntity"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" class="form-control m-bot15" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" class="form-control m-bot15" DataPlaceHolder="Location" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterProcess" AutoPostBack="true" CausesValidation="true" class="form-control m-bot15" DataPlaceHolder="Process" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterProcess_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterSubProcess" AutoPostBack="true" class="form-control m-bot15" DataPlaceHolder="Sub Process" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterSubProcess_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>

                                        <asp:DropDownCheckBoxes ID="ddlControllist" runat="server" AutoPostBack="true" Visible="true"
                                            CssClass="form-control" OnSelectedIndexChanged="ddlControllist_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                            Style="padding: 0px; margin: 0px; width: 90%; height: 30px !important;">
                                            <Style SelectBoxWidth="230" SelectBoxCssClass="form-control"
                                                 DropDownBoxBoxWidth="230" DropDownBoxBoxHeight="130" />
                                            <Texts SelectBoxCaption="Control" />
                                        </asp:DropDownCheckBoxes>

                                    </div>

                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">

                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial" AutoPostBack="true" class="form-control m-bot15"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" DataPlaceHolder="Financial Year" Width="90%" Height="32px"
                                            OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged">
                                        </asp:DropDownListChosen>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ErrorMessage="Select Financial Year" ForeColor="Red" InitialValue=""
                                            Font-Size="0.9em" ControlToValidate="ddlFilterFinancial"
                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>

                                        <asp:CompareValidator ErrorMessage="Select Financial Year" ControlToValidate="ddlFilterFinancial"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <asp:DropDownList ID="ddlQuarter" runat="server" AutoPostBack="true" Width="90%" CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlQuarter_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div style="margin-bottom: 4px">
                                    <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                        DataKeyNames="RiskCreationID" PageSize="50" AllowPaging="true" AutoPostBack="true" CssClass="table" 
                                        GridLines="None" AllowSorting="true" Width="100%">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                            <asp:TemplateField HeaderText="RiskId" Visible="false">
                                                <ItemTemplate>
                                                    
                                                    <asp:Label ID="lblRiskCategoryCreationId" runat="server" Text='<%#Eval("RiskCreationID")%>'></asp:Label>
                                                    <asp:Label ID="lblAuditInstanceId" runat="server" Text='<%#Eval("AuditInstanceId")%>'></asp:Label>
                                                    <asp:Label ID="lblProcessId" runat="server" Text='<%# Eval("ProcessId")%>'></asp:Label>
                                                    <asp:Label ID="lblSubProcessId" runat="server" Text='<%# Eval("SubProcessId")%>'></asp:Label>
                                                    <asp:Label ID="lblCustomerBranchId" runat="server" Text='<%# Eval("CustomerBranchId")%>'></asp:Label>
                                                    Assigned
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Branch">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                        <asp:Label runat="server" ID="lblBranch" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Control#">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                        <asp:Label runat="server" ID="lblControlNO" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlNo")%>' ToolTip='<%# Eval("ControlNo") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Risk Description">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 175px;">
                                                        <asp:Label runat="server" ID="lblActivityDescription" data-toggle="tooltip" data-placement="bottom"
                                                            Text='<%# Eval("ActivityDescription")%>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Control Objective">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 175px;">
                                                        <asp:Label ID="lblControlObjective" runat="server" data-toggle="tooltip"
                                                            data-placement="left" Text='<%# Eval("ControlObjective")%>'
                                                            ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerSettings Visible="false" />
                                        <PagerTemplate></PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-6 colpadding0" style="text-align: right;">
                                        <asp:Button Text="Delete" runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Width="90px"
                                            ValidationGroup="ComplianceInstanceValidationGroup" CausesValidation="true" />
                                        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="btn btn-primary" Width="90px"
                                           OnClientClick="CloseModel();" />
                                    </div>
                                    <div class="col-md-6 colpadding0" style="float: right;">
                                        <div class="table-paging" style="margin-bottom: 10px; float: right; color: #999;">
                                            <p>
                                                Page
                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" AllowSingleDeselect="false"
                                                class="form-control m-bot15" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            </p>
                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
