﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Configuration;
using System.Threading;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AuditUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region Compliance User
                User user = new User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    RoleID = -1,
                    IsExternal = false,
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                    IsActive = true,
                    IsHead = false,
                    EnType = "A"
                };
                #endregion
                #region Audit User
                mst_User mstUser = new mst_User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    RoleID = 7,
                    IsExternal = false,
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                    IsActive = true,
                    IsHead = false
                };

                #endregion

                bool emailExists;
                UserManagement.Exists(user, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                UserManagementRisk.Exists(mstUser, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                bool result = false;
                int resultValue = 0;
                user.CreatedBy = AuthenticationHelper.UserID;
                user.CreatedByText = AuthenticationHelper.User;
                string passwordText = Util.CreateRandomPassword(10);
                user.Password = Util.CalculateMD5Hash(passwordText);
                user.CreatedOn = DateTime.Now;

                mstUser.CreatedOn = DateTime.Now;
                mstUser.CreatedBy = AuthenticationHelper.UserID;
                mstUser.CreatedByText = AuthenticationHelper.User;
                mstUser.Password = Util.CalculateMD5Hash(passwordText);
                resultValue = UserManagement.CreateNewAuditUser(user);
                if (resultValue > 0)
                {
                    result = UserManagementRisk.CreateNewAuditUser(mstUser);
                    if (result == false)
                    {
                        UserManagement.deleteUser(resultValue);
                    }
                }
                if (result)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User Created Successfully.";
                }
                else
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}