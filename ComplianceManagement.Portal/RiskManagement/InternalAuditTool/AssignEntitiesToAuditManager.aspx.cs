﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Saplin.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AssignEntitiesToAuditManager : System.Web.UI.Page
    {
        public static List<int> Processlist = new List<int>();
        public static List<int> MgmtProcesslist = new List<int>();
        public static List<int> Departmentlist = new List<int>();
        public static List<long> Branchlist = new List<long>();
        protected static int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindEntityAssignment();
                BindUsers(ddlFilterUsers, "All");
                bindPageNumber();
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAssignEntities.PageIndex = chkSelectedPage - 1;
            grdAssignEntities.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindEntityAssignment();
        }

        #region Comman Function
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }

            var legalentitydata = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = legalentitydata;
            ddlLegalEntity.DataBind();
            //ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));

            ddlLegalEntityAuditHeadPopPup.DataTextField = "Name";
            ddlLegalEntityAuditHeadPopPup.DataValueField = "ID";
            ddlLegalEntityAuditHeadPopPup.Items.Clear();
            ddlLegalEntityAuditHeadPopPup.DataSource = legalentitydata;
            ddlLegalEntityAuditHeadPopPup.DataBind();
            // ddlLegalEntityAuditHeadPopPup.Items.Insert(0, new ListItem("Unit", "-1"));

            ddlLegalEntityManagementPopPup.DataTextField = "Name";
            ddlLegalEntityManagementPopPup.DataValueField = "ID";
            ddlLegalEntityManagementPopPup.Items.Clear();
            ddlLegalEntityManagementPopPup.DataSource = legalentitydata;
            ddlLegalEntityManagementPopPup.DataBind();
            //ddlLegalEntityManagementPopPup.Items.Insert(0, new ListItem("Unit", "-1"));

            ddlLegalEntityDepartmentHeadPopPup.DataTextField = "Name";
            ddlLegalEntityDepartmentHeadPopPup.DataValueField = "ID";
            ddlLegalEntityDepartmentHeadPopPup.Items.Clear();
            ddlLegalEntityDepartmentHeadPopPup.DataSource = legalentitydata;
            ddlLegalEntityDepartmentHeadPopPup.DataBind();
            // ddlLegalEntityDepartmentHeadPopPup.Items.Insert(0, new ListItem("Unit", "-1"));

            //****************Edit****************//

            ddlLegalEntityAuditHeadPopPupEdit.DataTextField = "Name";
            ddlLegalEntityAuditHeadPopPupEdit.DataValueField = "ID";
            ddlLegalEntityAuditHeadPopPupEdit.Items.Clear();
            ddlLegalEntityAuditHeadPopPupEdit.DataSource = legalentitydata;
            ddlLegalEntityAuditHeadPopPupEdit.DataBind();
            //ddlLegalEntityAuditHeadPopPupEdit.Items.Insert(0, new ListItem("Unit", "-1"));

            ddlLegalEntityManagementPopPupEdit.DataTextField = "Name";
            ddlLegalEntityManagementPopPupEdit.DataValueField = "ID";
            ddlLegalEntityManagementPopPupEdit.Items.Clear();
            ddlLegalEntityManagementPopPupEdit.DataSource = legalentitydata;
            ddlLegalEntityManagementPopPupEdit.DataBind();
            // ddlLegalEntityManagementPopPupEdit.Items.Insert(0, new ListItem("Unit", "-1"));

            ddlLegalEntityDepartmentHeadPopPupEdit.DataTextField = "Name";
            ddlLegalEntityDepartmentHeadPopPupEdit.DataValueField = "ID";
            ddlLegalEntityDepartmentHeadPopPupEdit.Items.Clear();
            ddlLegalEntityDepartmentHeadPopPupEdit.DataSource = legalentitydata;
            ddlLegalEntityDepartmentHeadPopPupEdit.DataBind();
            // ddlLegalEntityDepartmentHeadPopPupEdit.Items.Insert(0, new ListItem("Unit", "-1"));

        }

        public void BindSubEntityData(DropDownCheckBoxes DRP, List<long> ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityDataList(ParentId, CustomerId);
            DRP.DataBind();
            // DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }
        private void BindUsers(DropDownList ddlUserList, string flag)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                if (flag.ToUpper().Trim() == "DH")
                {
                    var users = UserManagement.GetAllDepartmentHeadUser(CustomerId);
                    ddlUserList.DataSource = users;
                    ddlUserList.DataBind();
                }
                else if (flag.ToUpper().Trim() == "AM")
                {
                    var users = UserManagement.GetAllUserAM(CustomerId);
                    ddlUserList.DataSource = users;
                    ddlUserList.DataBind();
                }
                else if (flag.ToUpper().Trim() == "MGMT")
                {
                    var users = UserManagement.GetAllUserMGMT(CustomerId);
                    ddlUserList.DataSource = users;
                    ddlUserList.DataBind();
                }
                else
                {
                    var users = UserManagement.GetAllUserAMorMGMTorDH(CustomerId);
                    ddlUserList.DataSource = users;
                    ddlUserList.DataBind();
                }

                if (ddlUserList.Items.Count > 0)
                {
                    ddlUserList.Items.Insert(0, new ListItem("Select User", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                AuditHeadDuplicateEntry.IsValid = false;
                AuditHeadDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void SplitString(string StrToSplit, Saplin.Controls.DropDownCheckBoxes DrpToFill)
        {
            string[] commandArgs = StrToSplit.Split(new char[] { ',' });

            string a = string.Empty;

            if (commandArgs.Length > 0)
            {
                for (int i = 0; i < commandArgs.Length; i++)
                {
                    a = commandArgs[i];
                    if (!string.IsNullOrEmpty(a))
                    {
                        if (DrpToFill.Items.FindByValue(a.Trim()) != null)
                        {
                            DrpToFill.Items.FindByValue(a.Trim()).Selected = true;
                        }
                    }
                }
            }
        }
        #endregion

        #region Main Screen
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityDataMainWindow(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }
            BindEntityAssignment();
            bindPageNumber();
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {

                BindSubEntityDataMainWindow(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));

            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
            BindEntityAssignment();
            bindPageNumber();
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {

                BindSubEntityDataMainWindow(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));

            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
            BindEntityAssignment();
            bindPageNumber();
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityDataMainWindow(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
            BindEntityAssignment();
            bindPageNumber();
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEntityAssignment();
            bindPageNumber();
        }
        private void BindEntityAssignment()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                int UserID = -1;
                string UserRole = string.Empty;
                if (!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue))
                {
                    if (ddlFilterUsers.SelectedValue != "-1")
                    {
                        UserID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                    }
                }
                int CustomerBranchId = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                
                var AssignEntities = AssignEntityAuditManager.SelectAllEntities(CustomerBranchId, UserID, CustomerId, UserRole);
                if (AssignEntities.Count > 0)
                {
                    AssignEntities = AssignEntities.OrderBy(entry => entry.userID).ToList();
                }
                grdAssignEntities.DataSource = AssignEntities;
                Session["TotalRows"] = AssignEntities.Count;
                grdAssignEntities.DataBind();

                upComplianceTypeList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                AuditHeadDuplicateEntry.IsValid = false;
                AuditHeadDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEntityAssignment();
            bindPageNumber();
        }
        protected void btnAddComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                lblErrorMassageManagement.Text = "";
                btnTabAuditHead_Click(sender, e);
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                AuditHeadDuplicateEntry.IsValid = false;
                AuditHeadDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddComplianceTypeEdit_Click(object sender, EventArgs e)
        {
            try
            {
                lblErrorMassageManagementEdit.Text = "";
                btnTabAuditHeadEdit_Click(sender, e);
                upComplianceEdit.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                AuditHeadDuplicateEntryEdit.IsValid = false;
                AuditHeadDuplicateEntryEdit.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignEntities_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAssignEntities.PageIndex = e.NewPageIndex;
                BindEntityAssignment();
                bindPageNumber();
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                AuditHeadDuplicateEntry.IsValid = false;
                AuditHeadDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        protected void grdAssignEntities_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DELETE_Entities"))
                {
                    int branchid = -1;
                    int DepartmentId = -1;
                    int ProcessId = -1;
                    string ISDepartmentOrAuditHead = string.Empty;
                    int Userid = -1;
                    String Args = e.CommandArgument.ToString();
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    branchid = Convert.ToInt32(commandArgs[0]);
                    DepartmentId = Convert.ToInt32(commandArgs[1]);
                    ProcessId = Convert.ToInt32(commandArgs[2]);
                    ISDepartmentOrAuditHead = commandArgs[3];
                    Userid = Convert.ToInt32(commandArgs[4]);
                    //Audit Head

                    if (CustomerId == 0)
                    {
                        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    }
                    if (ISDepartmentOrAuditHead == "Audit Head")
                    {
                        if (!AssignEntityAuditManager.checkInternalAuditSchedulingExist(branchid, ProcessId))
                        {
                            AssignEntityAuditManager.AuditManagerDelete(ProcessId, branchid, Userid, CustomerId);
                        }
                        else
                        {
                            CvEntry.IsValid = false;
                            CvEntry.ErrorMessage = "This Process already Schedule. So, Unit Assignment can not be Edit/Delete...!";
                        }
                    }
                    else if (ISDepartmentOrAuditHead == "Department Head")
                    {
                        AssignEntityAuditManager.DepartmentHeadDelete(DepartmentId, branchid, Userid, CustomerId);
                    }
                    else if (ISDepartmentOrAuditHead == "Management")
                    {
                        AssignEntityAuditManager.ManagementDelete(ProcessId, branchid, Userid, CustomerId);
                    }
                    BindEntityAssignment();
                    bindPageNumber();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvEntry.IsValid = false;
                CvEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAssignEntities.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindEntityAssignment();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAssignEntities.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                AuditHeadDuplicateEntry.IsValid = false;
                AuditHeadDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public void BindSubEntityDataMainWindow(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, CustomerId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }
        #endregion

        #region Add Tab
        protected void btnTabAuditHead_Click(object sender, EventArgs e)
        {
            liAuditHead.Attributes.Add("class", "active");
            liManagement.Attributes.Add("class", "");
            liDepartmentHead.Attributes.Add("class", "");

            divAuditHead.Visible = true;
            divAuditHead.Attributes.Remove("class");
            divAuditHead.Attributes.Add("class", "tab-pane active");

            divManagement.Visible = false;
            divManagement.Attributes.Remove("class");
            divManagement.Attributes.Add("class", "tab-pane");

            divDepartmentHead.Visible = false;
            divDepartmentHead.Attributes.Remove("class");
            divDepartmentHead.Attributes.Add("class", "tab-pane");

            if (divAuditHead.Visible)
            {
                BindUsers(ddlAuditHeadUsers, "AM");
            }
        }
        protected void btnTabManagement_Click(object sender, EventArgs e)
        {
            liAuditHead.Attributes.Add("class", "");
            liManagement.Attributes.Add("class", "active");
            liDepartmentHead.Attributes.Add("class", "");

            divAuditHead.Visible = false;
            divAuditHead.Attributes.Remove("class");
            divAuditHead.Attributes.Add("class", "tab-pane");

            divManagement.Visible = true;
            divManagement.Attributes.Remove("class");
            divManagement.Attributes.Add("class", "tab-pane active");

            divDepartmentHead.Visible = false;
            divDepartmentHead.Attributes.Remove("class");
            divDepartmentHead.Attributes.Add("class", "tab-pane");
            if (divManagement.Visible)
            {
                BindUsers(ddlManagementUsers, "MGMT");
            }
        }
        protected void btnTabDepartmentHead_Click(object sender, EventArgs e)
        {
            liAuditHead.Attributes.Add("class", "");
            liManagement.Attributes.Add("class", "");
            liDepartmentHead.Attributes.Add("class", "active");

            divAuditHead.Visible = false;
            divAuditHead.Attributes.Remove("class");
            divAuditHead.Attributes.Add("class", "tab-pane");

            divManagement.Visible = false;
            divManagement.Attributes.Remove("class");
            divManagement.Attributes.Add("class", "tab-pane");

            divDepartmentHead.Visible = true;
            divDepartmentHead.Attributes.Remove("class");
            divDepartmentHead.Attributes.Add("class", "tab-pane active");

            if (divDepartmentHead.Visible)
            {
                BindUsers(ddlDepartmentHeadUsers, "DH");
            }
        }
        #endregion

        #region Audit Head 
        protected void ddlLegalEntityAuditHeadPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> AHBranchList = new List<long>();
            AHBranchList.Clear();
            for (int i = 0; i < ddlLegalEntityAuditHeadPopPup.Items.Count; i++)
            {
                if (ddlLegalEntityAuditHeadPopPup.Items[i].Selected)
                {
                    AHBranchList.Add(Convert.ToInt32(ddlLegalEntityAuditHeadPopPup.Items[i].Value));
                }
            }

            if (AHBranchList.Count > 0)
            {
                BindSubEntityData(ddlSubEntity1AuditHeadPopPup, AHBranchList);
                BindProcesBranch();
                ddlProcessAuditHead.ClearSelection();
            }
            else
            {
                if (ddlSubEntity1AuditHeadPopPup.Items.Count > 0)
                    ddlSubEntity1AuditHeadPopPup.Items.Clear();

                if (ddlSubEntity2AuditHeadPopPup.Items.Count > 0)
                    ddlSubEntity2AuditHeadPopPup.Items.Clear();

                if (ddlSubEntity3AuditHeadPopPup.Items.Count > 0)
                    ddlSubEntity3AuditHeadPopPup.Items.Clear();

                if (ddlFilterLocationAuditHeadPopPup.Items.Count > 0)
                    ddlFilterLocationAuditHeadPopPup.Items.Clear();
            }
        }
        protected void ddlSubEntity1AuditHeadPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1AuditHeadPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity1AuditHeadPopPup.Items.Count; i++)
                {
                    if (ddlSubEntity1AuditHeadPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity1AuditHeadPopPup.Items[i].Value));
                    }
                }

                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity2AuditHeadPopPup, AHBranchList);
                    BindProcesBranch();
                    ddlProcessAuditHead.ClearSelection();
                }
                else
                {
                    if (ddlSubEntity2AuditHeadPopPup.Items.Count > 0)
                        ddlSubEntity2AuditHeadPopPup.ClearSelection();

                    if (ddlSubEntity3AuditHeadPopPup.Items.Count > 0)
                        ddlSubEntity3AuditHeadPopPup.ClearSelection();

                    if (ddlFilterLocationAuditHeadPopPup.Items.Count > 0)
                        ddlFilterLocationAuditHeadPopPup.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity2AuditHeadPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2AuditHeadPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity2AuditHeadPopPup.Items.Count; i++)
                {
                    if (ddlSubEntity2AuditHeadPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity2AuditHeadPopPup.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity3AuditHeadPopPup, AHBranchList);
                    BindProcesBranch();
                    ddlProcessAuditHead.ClearSelection();
                    //SplitString(GetBranchProcessList(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, Convert.ToInt32(ddlSubEntity2AuditHeadPopPup.SelectedValue)), ddlProcessAuditHead);
                }
                else
                {
                    if (ddlSubEntity3AuditHeadPopPup.Items.Count > 0)
                        ddlSubEntity3AuditHeadPopPup.ClearSelection();

                    if (ddlFilterLocationAuditHeadPopPup.Items.Count > 0)
                        ddlFilterLocationAuditHeadPopPup.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity3AuditHeadPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3AuditHeadPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity3AuditHeadPopPup.Items.Count; i++)
                {
                    if (ddlSubEntity3AuditHeadPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity3AuditHeadPopPup.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlFilterLocationAuditHeadPopPup, AHBranchList);
                    BindProcesBranch();
                    ddlProcessAuditHead.ClearSelection();
                }
                else
                {
                    if (ddlFilterLocationAuditHeadPopPup.Items.Count > 0)
                        ddlFilterLocationAuditHeadPopPup.ClearSelection();
                }
            }
        }
        protected void ddlFilterLocationAuditHeadPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcesBranch();
            ddlProcessAuditHead.ClearSelection();
        }
        protected void btnAuditHeadSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                if (ddlLegalEntityAuditHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlLegalEntityAuditHeadPopPup.Items.Count; i++)
                    {
                        if (ddlLegalEntityAuditHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlLegalEntityAuditHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity1AuditHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity1AuditHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity1AuditHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity1AuditHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity2AuditHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity2AuditHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity2AuditHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity2AuditHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity3AuditHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity3AuditHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity3AuditHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity3AuditHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlFilterLocationAuditHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlFilterLocationAuditHeadPopPup.Items.Count; i++)
                    {
                        if (ddlFilterLocationAuditHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlFilterLocationAuditHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    Branchlist.Clear();
                    //var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                    Branchlist = AHBranchList.Distinct().ToList();
                    var Branchlistloop = Branchlist.Distinct().ToList();

                    bool sucess = false;

                    int userID = -1;
                    if (!string.IsNullOrEmpty(ddlAuditHeadUsers.SelectedValue))
                    {
                        if (ddlAuditHeadUsers.SelectedValue != "-1")
                        {
                            userID = Convert.ToInt32(ddlAuditHeadUsers.SelectedValue);
                            Processlist.Clear();
                            for (int i = 0; i < ddlProcessAuditHead.Items.Count; i++)
                            {
                                if (ddlProcessAuditHead.Items[i].Selected)
                                {
                                    Processlist.Add(Convert.ToInt32(ddlProcessAuditHead.Items[i].Value));
                                }
                            }
                            List<EntitiesAssignmentAuditManagerRisk> EntitiesAssignmentAuditManagerRisklist = new List<EntitiesAssignmentAuditManagerRisk>();
                            if (Processlist.Count > 0)
                            {
                                foreach (var aItem in Processlist)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        for (int i = 0; i < Branchlist.Count; i++)
                                        {
                                            if (AssignEntityAuditManager.EntitiesAssignmentAuditManagerRiskExist(CustomerId, Branchlist[i], userID, aItem))
                                            {
                                                AssignEntityAuditManager.EditEntitiesAssignmentAuditManagerRiskExist(CustomerId, Branchlist[i], userID, aItem, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                            }
                                            else
                                            {
                                                //if (AssignEntityAuditManager.EntitiesAssignmentAuditManagerLocationProcessAssigned(Convert.ToInt32(CustomerId), Branchlist[i], aItem))
                                                //{
                                                EntitiesAssignmentAuditManagerRisk objEntitiesAssignmentrisk = new EntitiesAssignmentAuditManagerRisk();
                                                objEntitiesAssignmentrisk.UserID = userID;
                                                objEntitiesAssignmentrisk.CustomerID = CustomerId;
                                                objEntitiesAssignmentrisk.BranchID = Branchlist[i];
                                                objEntitiesAssignmentrisk.CreatedOn = DateTime.Today.Date;
                                                objEntitiesAssignmentrisk.ISACTIVE = true;
                                                objEntitiesAssignmentrisk.ProcessId = aItem;
                                                objEntitiesAssignmentrisk.CretedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                EntitiesAssignmentAuditManagerRisklist.Add(objEntitiesAssignmentrisk);
                                                //}
                                            }
                                        }
                                    }
                                }
                                sucess = AssignEntityAuditManager.CreateEntitiesAssignmentAuditManagerRisklist(EntitiesAssignmentAuditManagerRisklist);
                                Processlist.Clear();
                                if (sucess)
                                {
                                    AuditHeadDuplicateEntry.IsValid = false;
                                    AuditHeadDuplicateEntry.ErrorMessage = "Save successfully.";
                                }
                                else
                                {
                                    AuditHeadDuplicateEntry.IsValid = false;
                                    AuditHeadDuplicateEntry.ErrorMessage = "Error Please try again.";
                                }
                            }
                            else
                            {
                                AuditHeadDuplicateEntry.IsValid = false;
                                AuditHeadDuplicateEntry.ErrorMessage = "Please select Process.";
                            }
                        }
                    }
                    BindEntityAssignment();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdAssignEntities.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }//CustomerBranchId != -1
                else
                {
                    AuditHeadDuplicateEntry.IsValid = false;
                    AuditHeadDuplicateEntry.ErrorMessage = "Please select Unit,Subunit.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                AuditHeadDuplicateEntry.IsValid = false;
                AuditHeadDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindProcesBranch()
        {
            if (ddlProcessAuditHead.SelectedValue != null)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                if (ddlLegalEntityAuditHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlLegalEntityAuditHeadPopPup.Items.Count; i++)
                    {
                        if (ddlLegalEntityAuditHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlLegalEntityAuditHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity1AuditHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity1AuditHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity1AuditHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity1AuditHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity2AuditHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity2AuditHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity2AuditHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity2AuditHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity3AuditHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity3AuditHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity3AuditHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity3AuditHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlFilterLocationAuditHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlFilterLocationAuditHeadPopPup.Items.Count; i++)
                    {
                        if (ddlFilterLocationAuditHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlFilterLocationAuditHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                Branchlist.Clear();
                var bracnhes = GetAllHierarchyBranchList(Convert.ToInt32(CustomerId), AHBranchList);
                var Branchlistloop = Branchlist.ToList();
                ddlProcessAuditHead.Items.Clear();
                ddlProcessAuditHead.DataTextField = "Name";
                ddlProcessAuditHead.DataValueField = "ID";
                ddlProcessAuditHead.DataSource = ProcessManagement.FillProcessFromRATBDM(Convert.ToInt32(CustomerId), Branchlist);//, Branchlist
                ddlProcessAuditHead.DataBind();
            }
        }
        #endregion

        #region Management
        protected void ddlLegalEntityManagementPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntityManagementPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlLegalEntityManagementPopPup.Items.Count; i++)
                {
                    if (ddlLegalEntityManagementPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlLegalEntityManagementPopPup.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity1ManagementPopPup, AHBranchList);
                    BindManagementProcesBranch();
                    ddlProcessManagement.ClearSelection();
                }
                else
                {
                    if (ddlSubEntity1ManagementPopPup.Items.Count > 0)
                        ddlSubEntity1ManagementPopPup.Items.Clear();

                    if (ddlSubEntity2ManagementPopPup.Items.Count > 0)
                        ddlSubEntity2ManagementPopPup.Items.Clear();

                    if (ddlSubEntity3ManagementPopPup.Items.Count > 0)
                        ddlSubEntity3ManagementPopPup.Items.Clear();

                    if (ddlFilterLocationManagementPopPup.Items.Count > 0)
                        ddlFilterLocationManagementPopPup.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity1ManagementPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1ManagementPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity1ManagementPopPup.Items.Count; i++)
                {
                    if (ddlSubEntity1ManagementPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity1ManagementPopPup.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity2ManagementPopPup, AHBranchList);
                    BindManagementProcesBranch();
                    ddlProcessManagement.ClearSelection();
                }
                else
                {
                    if (ddlSubEntity2ManagementPopPup.Items.Count > 0)
                        ddlSubEntity2ManagementPopPup.ClearSelection();

                    if (ddlSubEntity3ManagementPopPup.Items.Count > 0)
                        ddlSubEntity3ManagementPopPup.ClearSelection();

                    if (ddlFilterLocationManagementPopPup.Items.Count > 0)
                        ddlFilterLocationManagementPopPup.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity2ManagementPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2ManagementPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity2ManagementPopPup.Items.Count; i++)
                {
                    if (ddlSubEntity2ManagementPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity2ManagementPopPup.Items[i].Value));
                    }
                }
                if (ddlSubEntity2ManagementPopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3ManagementPopPup, AHBranchList);
                    BindManagementProcesBranch();
                    ddlProcessManagement.ClearSelection();
                }
                else
                {
                    if (ddlSubEntity3ManagementPopPup.Items.Count > 0)
                        ddlSubEntity3ManagementPopPup.ClearSelection();

                    if (ddlFilterLocationManagementPopPup.Items.Count > 0)
                        ddlFilterLocationManagementPopPup.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity3ManagementPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3ManagementPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity3ManagementPopPup.Items.Count; i++)
                {
                    if (ddlSubEntity3ManagementPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity3ManagementPopPup.Items[i].Value));
                    }
                }
                if (ddlSubEntity3ManagementPopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocationManagementPopPup, AHBranchList);
                    BindManagementProcesBranch();
                    ddlProcessManagement.ClearSelection();
                }
                else
                {
                    if (ddlFilterLocationManagementPopPup.Items.Count > 0)
                        ddlFilterLocationManagementPopPup.ClearSelection();
                }
            }
        }
        protected void ddlFilterLocationManagementPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindManagementProcesBranch();
            ddlProcessManagement.ClearSelection();
        }
        protected void btnManagementSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (ddlLegalEntityManagementPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlLegalEntityManagementPopPup.Items.Count; i++)
                    {
                        if (ddlLegalEntityManagementPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlLegalEntityManagementPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity1ManagementPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity1ManagementPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity1ManagementPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity1ManagementPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity2ManagementPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity2ManagementPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity2ManagementPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity2ManagementPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity3ManagementPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity3ManagementPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity3ManagementPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity3ManagementPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlFilterLocationManagementPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlFilterLocationManagementPopPup.Items.Count; i++)
                    {
                        if (ddlFilterLocationManagementPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlFilterLocationManagementPopPup.Items[i].Value));
                        }
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    Branchlist.Clear();
                    //var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                    Branchlist = AHBranchList.Distinct().ToList();
                    bool sucess = false;

                    int userID = -1;
                    if (!string.IsNullOrEmpty(ddlManagementUsers.SelectedValue))
                    {
                        if (ddlManagementUsers.SelectedValue != "-1")
                        {
                            userID = Convert.ToInt32(ddlManagementUsers.SelectedValue);
                            MgmtProcesslist.Clear();
                            for (int i = 0; i < ddlProcessManagement.Items.Count; i++)
                            {
                                if (ddlProcessManagement.Items[i].Selected)
                                {
                                    MgmtProcesslist.Add(Convert.ToInt32(ddlProcessManagement.Items[i].Value));
                                }
                            }
                            List<EntitiesAssignmentManagementRisk> EntitiesAssignmentManagementRisklist = new List<EntitiesAssignmentManagementRisk>();
                            if (MgmtProcesslist.Count > 0)
                            {
                                foreach (var aItem in MgmtProcesslist)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        for (int i = 0; i < Branchlist.Count; i++)
                                        {
                                            if (AssignEntityAuditManager.EntitiesAssignmentManagementRiskExist(CustomerId, Branchlist[i], userID, aItem))
                                            {
                                                AssignEntityAuditManager.EditEntitiesAssignmentManagementRiskExist(CustomerId, Branchlist[i], userID, aItem, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                            }
                                            else
                                            {
                                                //if (AssignEntityAuditManager.EntitiesAssignmentAuditManagerLocationProcessAssigned(Convert.ToInt32(CustomerId), Branchlist[i], aItem))
                                                //{
                                                EntitiesAssignmentManagementRisk objEntitiesAssignmentrisk = new EntitiesAssignmentManagementRisk();
                                                objEntitiesAssignmentrisk.UserID = userID;
                                                objEntitiesAssignmentrisk.CustomerID = CustomerId;
                                                objEntitiesAssignmentrisk.BranchID = Branchlist[i];
                                                objEntitiesAssignmentrisk.CreatedOn = DateTime.Today.Date;
                                                objEntitiesAssignmentrisk.ISACTIVE = true;
                                                objEntitiesAssignmentrisk.ProcessId = aItem;
                                                objEntitiesAssignmentrisk.CretedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                EntitiesAssignmentManagementRisklist.Add(objEntitiesAssignmentrisk);
                                                // }
                                            }
                                        }
                                    }
                                }
                                sucess = AssignEntityAuditManager.CreateEntitiesAssignmentManagementRisklist(EntitiesAssignmentManagementRisklist);
                                if (sucess)
                                {
                                    ManagementDuplicateEntry.IsValid = false;
                                    ManagementDuplicateEntry.ErrorMessage = "Save successfully.";
                                }
                                else
                                {
                                    ManagementDuplicateEntry.IsValid = false;
                                    ManagementDuplicateEntry.ErrorMessage = "Error Please try again.";
                                }
                            }
                            else
                            {
                                ManagementDuplicateEntry.IsValid = false;
                                ManagementDuplicateEntry.ErrorMessage = "Please select Process.";
                            }
                        }
                    }
                    BindEntityAssignment();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdAssignEntities.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }//CustomerBranchId != -1
                else
                {
                    ManagementDuplicateEntry.IsValid = false;
                    ManagementDuplicateEntry.ErrorMessage = "Please select Unit & Sub Unit.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                ManagementDuplicateEntry.IsValid = false;
                ManagementDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindManagementProcesBranch()
        {
            if (ddlProcessManagement.SelectedValue != null)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                if (ddlLegalEntityManagementPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlLegalEntityManagementPopPup.Items.Count; i++)
                    {
                        if (ddlLegalEntityManagementPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlLegalEntityManagementPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity1ManagementPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity1ManagementPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity1ManagementPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity1ManagementPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity2ManagementPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity2ManagementPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity2ManagementPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity2ManagementPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity3ManagementPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity3ManagementPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity3ManagementPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity3ManagementPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlFilterLocationManagementPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlFilterLocationManagementPopPup.Items.Count; i++)
                    {
                        if (ddlFilterLocationManagementPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlFilterLocationManagementPopPup.Items[i].Value));
                        }
                    }
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                Branchlist.Clear();
                var bracnhes = GetAllHierarchyBranchList(Convert.ToInt32(CustomerId), AHBranchList);
                var Branchlistloop = Branchlist.ToList();
                ddlProcessManagement.Items.Clear();
                ddlProcessManagement.DataTextField = "Name";
                ddlProcessManagement.DataValueField = "ID";
                ddlProcessManagement.DataSource = ProcessManagement.FillProcessFromRATBDM(Convert.ToInt32(CustomerId), Branchlist);
                //ddlProcessManagement.DataSource = ProcessManagement.FillProcessFromManagementRATBDM(Convert.ToInt32(CustomerId), Branchlist, Portal.Common.AuthenticationHelper.UserID);
                ddlProcessManagement.DataBind();
            }
        }

        #endregion

        #region DepartmentHead
        protected void ddlLegalEntityDepartmentHeadPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntityDepartmentHeadPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlLegalEntityDepartmentHeadPopPup.Items.Count; i++)
                {
                    if (ddlLegalEntityDepartmentHeadPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlLegalEntityDepartmentHeadPopPup.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity1DepartmentHeadPopPup, AHBranchList);
                    BindDepartment();
                    ddlDepartment.ClearSelection();
                }
                else
                {
                    if (ddlSubEntity1DepartmentHeadPopPup.Items.Count > 0)
                        ddlSubEntity1DepartmentHeadPopPup.Items.Clear();

                    if (ddlSubEntity2DepartmentHeadPopPup.Items.Count > 0)
                        ddlSubEntity2DepartmentHeadPopPup.Items.Clear();

                    if (ddlSubEntity3DepartmentHeadPopPup.Items.Count > 0)
                        ddlSubEntity3DepartmentHeadPopPup.Items.Clear();

                    if (ddlFilterLocationDepartmentHeadPopPup.Items.Count > 0)
                        ddlFilterLocationDepartmentHeadPopPup.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity1DepartmentHeadPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1DepartmentHeadPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity1DepartmentHeadPopPup.Items.Count; i++)
                {
                    if (ddlSubEntity1DepartmentHeadPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity1DepartmentHeadPopPup.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity2DepartmentHeadPopPup, AHBranchList);
                    BindDepartment();
                    ddlDepartment.ClearSelection();
                }
                else
                {
                    if (ddlSubEntity2DepartmentHeadPopPup.Items.Count > 0)
                        ddlSubEntity2DepartmentHeadPopPup.ClearSelection();

                    if (ddlSubEntity3DepartmentHeadPopPup.Items.Count > 0)
                        ddlSubEntity3DepartmentHeadPopPup.ClearSelection();

                    if (ddlFilterLocationDepartmentHeadPopPup.Items.Count > 0)
                        ddlFilterLocationDepartmentHeadPopPup.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity2DepartmentHeadPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2DepartmentHeadPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity2DepartmentHeadPopPup.Items.Count; i++)
                {
                    if (ddlSubEntity2DepartmentHeadPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity2DepartmentHeadPopPup.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity3DepartmentHeadPopPup, AHBranchList);
                    BindDepartment();
                    ddlDepartment.ClearSelection();
                }
                else
                {
                    if (ddlSubEntity3DepartmentHeadPopPup.Items.Count > 0)
                        ddlSubEntity3DepartmentHeadPopPup.ClearSelection();

                    if (ddlFilterLocationDepartmentHeadPopPup.Items.Count > 0)
                        ddlFilterLocationDepartmentHeadPopPup.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity3DepartmentHeadPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3DepartmentHeadPopPup.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity3DepartmentHeadPopPup.Items.Count; i++)
                {
                    if (ddlSubEntity3DepartmentHeadPopPup.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity3DepartmentHeadPopPup.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlFilterLocationDepartmentHeadPopPup, AHBranchList);
                    BindDepartment();
                    ddlDepartment.ClearSelection();
                }
                else
                {
                    if (ddlFilterLocationDepartmentHeadPopPup.Items.Count > 0)
                        ddlFilterLocationDepartmentHeadPopPup.ClearSelection();
                }
            }
        }
        protected void ddlFilterLocationDepartmentHeadPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDepartment();
            ddlDepartment.ClearSelection();
        }
        protected void btnDepartmentHeadSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (ddlLegalEntityDepartmentHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlLegalEntityDepartmentHeadPopPup.Items.Count; i++)
                    {
                        if (ddlLegalEntityDepartmentHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlLegalEntityDepartmentHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity1DepartmentHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity1DepartmentHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity1DepartmentHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity1DepartmentHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity2DepartmentHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity2DepartmentHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity2DepartmentHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity2DepartmentHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity3DepartmentHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity3DepartmentHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity3DepartmentHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity3DepartmentHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlFilterLocationDepartmentHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlFilterLocationDepartmentHeadPopPup.Items.Count; i++)
                    {
                        if (ddlFilterLocationDepartmentHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlFilterLocationDepartmentHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (AHBranchList.Count != -1)
                {
                    Branchlist.Clear();
                    //var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                    Branchlist = AHBranchList.Distinct().ToList(); 
                    bool sucess = false;
                    int userID = -1;
                    if (!string.IsNullOrEmpty(ddlDepartmentHeadUsers.SelectedValue))
                    {
                        if (ddlDepartmentHeadUsers.SelectedValue != "-1")
                        {
                            userID = Convert.ToInt32(ddlDepartmentHeadUsers.SelectedValue);
                            Departmentlist.Clear();
                            for (int i = 0; i < ddlDepartment.Items.Count; i++)
                            {
                                if (ddlDepartment.Items[i].Selected)
                                {
                                    Departmentlist.Add(Convert.ToInt32(ddlDepartment.Items[i].Value));
                                }
                            }
                            List<EntitiesAssignmentDepartmentHead> EntitiesAssignmentDepartmentHeadlist = new List<EntitiesAssignmentDepartmentHead>();
                            if (Departmentlist.Count > 0)
                            {
                                foreach (var aItem in Departmentlist)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        for (int i = 0; i < Branchlist.Count; i++)
                                        {
                                            if (AssignEntityAuditManager.EntitiesAssignmentDepartmentHeadExist(CustomerId, Branchlist[i], userID, Convert.ToInt32(aItem)))
                                            {
                                                AssignEntityAuditManager.EditEntitiesAssignmentDepartmentHeadExist(CustomerId, Branchlist[i], userID, Convert.ToInt32(aItem), com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                            }
                                            else
                                            {
                                                if (CompDeptManagement.EntitiesAssignmentDepartMentHeadLocationDepartmentAssigned(CustomerId, Branchlist[i], Convert.ToInt32(aItem)))
                                                {
                                                    EntitiesAssignmentDepartmentHead objEntitiesAssignmentDepartmentHead = new EntitiesAssignmentDepartmentHead();
                                                    objEntitiesAssignmentDepartmentHead.UserID = userID;
                                                    objEntitiesAssignmentDepartmentHead.CustomerID = CustomerId;
                                                    objEntitiesAssignmentDepartmentHead.BranchID = Branchlist[i];
                                                    objEntitiesAssignmentDepartmentHead.CreatedOn = DateTime.Today.Date;
                                                    objEntitiesAssignmentDepartmentHead.ISACTIVE = true;
                                                    objEntitiesAssignmentDepartmentHead.DepartmentID = Convert.ToInt32(aItem);
                                                    objEntitiesAssignmentDepartmentHead.CretedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    EntitiesAssignmentDepartmentHeadlist.Add(objEntitiesAssignmentDepartmentHead);
                                                }
                                            }
                                        }
                                    }
                                }
                                sucess = AssignEntityAuditManager.CreateEntitiesAssignmentDepartmentHeadlist(EntitiesAssignmentDepartmentHeadlist);
                                if (sucess)
                                {
                                    DepartmentHeadDuplicateEntry.IsValid = false;
                                    DepartmentHeadDuplicateEntry.ErrorMessage = "Save successfully.";
                                }
                                else
                                {
                                    DepartmentHeadDuplicateEntry.IsValid = false;
                                    DepartmentHeadDuplicateEntry.ErrorMessage = "Error Please try again.";
                                }
                            }
                            else
                            {
                                DepartmentHeadDuplicateEntry.IsValid = false;
                                DepartmentHeadDuplicateEntry.ErrorMessage = "Please select Department.";
                            }
                        }
                    }

                    BindEntityAssignment();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdAssignEntities.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }//CustomerBranchId != -1
                else
                {
                    DepartmentHeadDuplicateEntry.IsValid = false;
                    DepartmentHeadDuplicateEntry.ErrorMessage = "Please select Unit & SubUnit.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                DepartmentHeadDuplicateEntry.IsValid = false;
                DepartmentHeadDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindDepartment()
        {
            if (ddlDepartment.SelectedValue != null)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (ddlLegalEntityDepartmentHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlLegalEntityDepartmentHeadPopPup.Items.Count; i++)
                    {
                        if (ddlLegalEntityDepartmentHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlLegalEntityDepartmentHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity1DepartmentHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity1DepartmentHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity1DepartmentHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity1DepartmentHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity2DepartmentHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity2DepartmentHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity2DepartmentHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity2DepartmentHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity3DepartmentHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity3DepartmentHeadPopPup.Items.Count; i++)
                    {
                        if (ddlSubEntity3DepartmentHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity3DepartmentHeadPopPup.Items[i].Value));
                        }
                    }
                }
                if (ddlFilterLocationDepartmentHeadPopPup.Items.Count > 0)
                {
                    for (int i = 0; i < ddlFilterLocationDepartmentHeadPopPup.Items.Count; i++)
                    {
                        if (ddlFilterLocationDepartmentHeadPopPup.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlFilterLocationDepartmentHeadPopPup.Items[i].Value));
                        }
                    }
                }
                Branchlist.Clear();
                var bracnhes = GetAllHierarchyBranchList(Convert.ToInt32(CustomerId), AHBranchList);
                var Branchlistloop = Branchlist.ToList();
                ddlDepartment.Items.Clear();
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";
                ddlDepartment.DataSource = CompDeptManagement.FillDepartmentFromRATBDM(Convert.ToInt32(CustomerId), Branchlist);
                ddlDepartment.DataBind();
            }
        }
        #endregion

        #region Branch Hierarchy
        public static List<NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        public static List<NameValueHierarchy> GetAllHierarchyBranchList(int customerID, List<long> customerbranchid)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && customerbranchid.Contains(row.ID)
                             select row);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).Distinct().ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
                Branchlist = Branchlist.Distinct().ToList();
            }
            return hierarchy;
        }
        #endregion

        #region Edit Code

        #region Edit Tab
        protected void btnTabAuditHeadEdit_Click(object sender, EventArgs e)
        {
            liAuditHeadEdit.Attributes.Add("class", "active");
            liManagementEdit.Attributes.Add("class", "");
            liDepartmentHeadEdit.Attributes.Add("class", "");

            divAuditHeadEdit.Visible = true;
            divAuditHeadEdit.Attributes.Remove("class");
            divAuditHeadEdit.Attributes.Add("class", "tab-pane active");

            divManagementEdit.Visible = false;
            divManagementEdit.Attributes.Remove("class");
            divManagementEdit.Attributes.Add("class", "tab-pane");

            divDepartmentHeadEdit.Visible = false;
            divDepartmentHeadEdit.Attributes.Remove("class");
            divDepartmentHeadEdit.Attributes.Add("class", "tab-pane");

            if (divAuditHeadEdit.Visible)
            {
                BindUsers(ddlAuditHeadUsersEdit, "AM");
            }
        }
        protected void btnTabManagementEdit_Click(object sender, EventArgs e)
        {
            liAuditHeadEdit.Attributes.Add("class", "");
            liManagementEdit.Attributes.Add("class", "active");
            liDepartmentHeadEdit.Attributes.Add("class", "");

            divAuditHeadEdit.Visible = false;
            divAuditHeadEdit.Attributes.Remove("class");
            divAuditHeadEdit.Attributes.Add("class", "tab-pane");

            divManagementEdit.Visible = true;
            divManagementEdit.Attributes.Remove("class");
            divManagementEdit.Attributes.Add("class", "tab-pane active");

            divDepartmentHeadEdit.Visible = false;
            divDepartmentHeadEdit.Attributes.Remove("class");
            divDepartmentHeadEdit.Attributes.Add("class", "tab-pane");
            if (divManagementEdit.Visible)
            {
                BindUsers(ddlManagementUsersEdit, "MGMT");
            }
        }
        protected void btnTabDepartmentHeadEdit_Click(object sender, EventArgs e)
        {
            liAuditHeadEdit.Attributes.Add("class", "");
            liManagementEdit.Attributes.Add("class", "");
            liDepartmentHeadEdit.Attributes.Add("class", "active");

            divAuditHeadEdit.Visible = false;
            divAuditHeadEdit.Attributes.Remove("class");
            divAuditHeadEdit.Attributes.Add("class", "tab-pane");

            divManagementEdit.Visible = false;
            divManagementEdit.Attributes.Remove("class");
            divManagementEdit.Attributes.Add("class", "tab-pane");

            divDepartmentHeadEdit.Visible = true;
            divDepartmentHeadEdit.Attributes.Remove("class");
            divDepartmentHeadEdit.Attributes.Add("class", "tab-pane active");

            if (divDepartmentHeadEdit.Visible)
            {
                BindUsers(ddlDepartmentHeadUsersEdit, "DH");
            }
        }

        #endregion

        #region Audit Head Edit
        protected void ddlLegalEntityAuditHeadPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntityAuditHeadPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlLegalEntityAuditHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlLegalEntityAuditHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlLegalEntityAuditHeadPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity1AuditHeadPopPupEdit, AHBranchList);

                    if (!string.IsNullOrEmpty(ddlAuditHeadUsersEdit.SelectedValue))
                    {
                        if (ddlAuditHeadUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindProcesBranchEdit(Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue));
                            ddlProcessAuditHeadEdit.ClearSelection();
                            SplitString(GetBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue)), ddlProcessAuditHeadEdit);
                        }
                        else
                        {
                            ddlProcessAuditHeadEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlProcessAuditHeadEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlSubEntity1AuditHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity1AuditHeadPopPupEdit.Items.Clear();

                    if (ddlSubEntity2AuditHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity2AuditHeadPopPupEdit.Items.Clear();

                    if (ddlSubEntity3AuditHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity3AuditHeadPopPupEdit.Items.Clear();

                    if (ddlFilterLocationAuditHeadPopPup.Items.Count > 0)
                        ddlFilterLocationAuditHeadPopPup.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity1AuditHeadPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1AuditHeadPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity1AuditHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity1AuditHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity1AuditHeadPopPupEdit.Items[i].Value));
                    }
                }

                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity2AuditHeadPopPupEdit, AHBranchList);

                    if (!string.IsNullOrEmpty(ddlAuditHeadUsersEdit.SelectedValue))
                    {
                        if (ddlAuditHeadUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindProcesBranchEdit(Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue));
                            ddlProcessAuditHeadEdit.ClearSelection();
                            SplitString(GetBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue)), ddlProcessAuditHeadEdit);
                        }
                        else
                        {
                            ddlProcessAuditHeadEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlProcessAuditHeadEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlSubEntity2AuditHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity2AuditHeadPopPupEdit.ClearSelection();

                    if (ddlSubEntity3AuditHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity3AuditHeadPopPupEdit.ClearSelection();

                    if (ddlFilterLocationAuditHeadPopPupEdit.Items.Count > 0)
                        ddlFilterLocationAuditHeadPopPupEdit.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity2AuditHeadPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2AuditHeadPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity2AuditHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity2AuditHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity2AuditHeadPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity3AuditHeadPopPupEdit, AHBranchList);
                    if (!string.IsNullOrEmpty(ddlAuditHeadUsersEdit.SelectedValue))
                    {
                        if (ddlAuditHeadUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindProcesBranchEdit(Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue));
                            ddlProcessAuditHeadEdit.ClearSelection();
                            SplitString(GetBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue)), ddlProcessAuditHeadEdit);
                        }
                        else
                        {
                            ddlProcessAuditHeadEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlProcessAuditHeadEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlSubEntity3AuditHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity3AuditHeadPopPupEdit.ClearSelection();

                    if (ddlFilterLocationAuditHeadPopPupEdit.Items.Count > 0)
                        ddlFilterLocationAuditHeadPopPupEdit.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity3AuditHeadPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3AuditHeadPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity3AuditHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity3AuditHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity3AuditHeadPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlFilterLocationAuditHeadPopPupEdit, AHBranchList);
                    if (!string.IsNullOrEmpty(ddlAuditHeadUsersEdit.SelectedValue))
                    {
                        if (ddlAuditHeadUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindProcesBranchEdit(Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue));
                            ddlProcessAuditHeadEdit.ClearSelection();
                            SplitString(GetBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue)), ddlProcessAuditHeadEdit);
                        }
                        else
                        {
                            ddlProcessAuditHeadEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlProcessAuditHeadEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlFilterLocationAuditHeadPopPupEdit.Items.Count > 0)
                        ddlFilterLocationAuditHeadPopPupEdit.ClearSelection();
                }
            }
        }
        protected void ddlFilterLocationAuditHeadPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> AHBranchList = new List<long>();
            AHBranchList.Clear();
            if (ddlFilterLocationAuditHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlFilterLocationAuditHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlFilterLocationAuditHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlFilterLocationAuditHeadPopPupEdit.Items[i].Value));
                    }
                }
            }

            if (!string.IsNullOrEmpty(ddlAuditHeadUsersEdit.SelectedValue))
            {
                if (ddlAuditHeadUsersEdit.SelectedValue != "-1")
                {
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                    var Branchlistloop = Branchlist.ToList();
                    BindProcesBranchEdit(Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue));
                    ddlProcessAuditHeadEdit.ClearSelection();
                    SplitString(GetBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue)), ddlProcessAuditHeadEdit);
                }
                else
                {
                    ddlProcessAuditHeadEdit.Items.Clear();
                }
            }
            else
            {
                ddlProcessAuditHeadEdit.Items.Clear();
            }
        }
        protected void ddlAuditHeadUsersEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> AHBranchList = new List<long>();
            AHBranchList.Clear();
            if (ddlLegalEntityAuditHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlLegalEntityAuditHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlLegalEntityAuditHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlLegalEntityAuditHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (ddlSubEntity1AuditHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlSubEntity1AuditHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity1AuditHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity1AuditHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (ddlSubEntity2AuditHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlSubEntity2AuditHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity2AuditHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity2AuditHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (ddlSubEntity3AuditHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlSubEntity3AuditHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity3AuditHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity3AuditHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (ddlFilterLocationAuditHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlFilterLocationAuditHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlFilterLocationAuditHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlFilterLocationAuditHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (AHBranchList.Count > 0)
            {
                if (!string.IsNullOrEmpty(ddlAuditHeadUsersEdit.SelectedValue))
                {
                    if (ddlAuditHeadUsersEdit.SelectedValue != "-1")
                    {
                        if (CustomerId == 0)
                        {
                            CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        }
                        Branchlist.Clear();
                        var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                        var Branchlistloop = Branchlist.ToList();
                        BindProcesBranchEdit(Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue));
                        ddlProcessAuditHeadEdit.ClearSelection();
                        SplitString(GetBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue)), ddlProcessAuditHeadEdit);
                    }
                }
            }
        }
        public string GetBranchProcessList(long CustID, List<long> CustomerBranchList, long UserId)
        {
            List<EntitiesAssignmentAuditManagerRisk> EntitiesAssignmentAuditManagerList = new List<EntitiesAssignmentAuditManagerRisk>();
            string BranchProcessList = "";
            EntitiesAssignmentAuditManagerList = AssignEntityAuditManager.GetAllEntitiesAssignmentAuditManagerList(CustID, CustomerBranchList, UserId).ToList();
            EntitiesAssignmentAuditManagerList = EntitiesAssignmentAuditManagerList.OrderBy(entry => entry.ProcessId).ToList();
            if (EntitiesAssignmentAuditManagerList.Count > 0)
            {
                foreach (var row in EntitiesAssignmentAuditManagerList)
                {
                    BranchProcessList += row.ProcessId + " ,";
                }
            }
            return BranchProcessList.Trim(',');
        }
        protected void btnAuditHeadSaveEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                if (ddlLegalEntityAuditHeadPopPupEdit.Items.Count > 0)
                {
                    for (int i = 0; i < ddlLegalEntityAuditHeadPopPupEdit.Items.Count; i++)
                    {
                        if (ddlLegalEntityAuditHeadPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlLegalEntityAuditHeadPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity1AuditHeadPopPupEdit.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity1AuditHeadPopPupEdit.Items.Count; i++)
                    {
                        if (ddlSubEntity1AuditHeadPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity1AuditHeadPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity2AuditHeadPopPupEdit.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity2AuditHeadPopPupEdit.Items.Count; i++)
                    {
                        if (ddlSubEntity2AuditHeadPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity2AuditHeadPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity3AuditHeadPopPupEdit.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity3AuditHeadPopPupEdit.Items.Count; i++)
                    {
                        if (ddlSubEntity3AuditHeadPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity3AuditHeadPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (ddlFilterLocationAuditHeadPopPupEdit.Items.Count > 0)
                {
                    for (int i = 0; i < ddlFilterLocationAuditHeadPopPupEdit.Items.Count; i++)
                    {
                        if (ddlFilterLocationAuditHeadPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlFilterLocationAuditHeadPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                    var Branchlistloop = Branchlist.ToList();
                    bool sucess = false;

                    int userID = -1;
                    if (!string.IsNullOrEmpty(ddlAuditHeadUsersEdit.SelectedValue))
                    {
                        if (ddlAuditHeadUsersEdit.SelectedValue != "-1")
                        {
                            userID = Convert.ToInt32(ddlAuditHeadUsersEdit.SelectedValue);
                            Processlist.Clear();
                            for (int i = 0; i < ddlProcessAuditHeadEdit.Items.Count; i++)
                            {
                                if (ddlProcessAuditHeadEdit.Items[i].Selected)
                                {
                                    Processlist.Add(Convert.ToInt32(ddlProcessAuditHeadEdit.Items[i].Value));
                                }
                            }
                            sucess = AssignEntityAuditManager.UpdateEntitiesAssignmentAuditManager(CustomerId, Branchlist, Convert.ToInt32(userID));
                            List<EntitiesAssignmentAuditManagerRisk> EntitiesAssignmentAuditManagerRisklist = new List<EntitiesAssignmentAuditManagerRisk>();
                            if (Processlist.Count > 0)
                            {
                                foreach (var aItem in Processlist)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        for (int i = 0; i < Branchlist.Count; i++)
                                        {
                                            if (AssignEntityAuditManager.EntitiesAssignmentAuditManagerRiskExist(CustomerId, Branchlist[i], userID, aItem))
                                            {
                                                AssignEntityAuditManager.EditEntitiesAssignmentAuditManagerRiskExist(CustomerId, Branchlist[i], userID, aItem, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                            }
                                            else
                                            {
                                                EntitiesAssignmentAuditManagerRisk objEntitiesAssignmentrisk = new EntitiesAssignmentAuditManagerRisk();
                                                objEntitiesAssignmentrisk.UserID = userID;
                                                objEntitiesAssignmentrisk.CustomerID = CustomerId;
                                                objEntitiesAssignmentrisk.BranchID = Branchlist[i];
                                                objEntitiesAssignmentrisk.CreatedOn = DateTime.Today.Date;
                                                objEntitiesAssignmentrisk.ISACTIVE = true;
                                                objEntitiesAssignmentrisk.ProcessId = aItem;
                                                objEntitiesAssignmentrisk.CretedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                EntitiesAssignmentAuditManagerRisklist.Add(objEntitiesAssignmentrisk);
                                            }
                                        }
                                    }
                                }
                                sucess = AssignEntityAuditManager.CreateEntitiesAssignmentAuditManagerRisklist(EntitiesAssignmentAuditManagerRisklist);
                                Processlist.Clear();
                            }
                            else
                            {
                                DepartmentHeadDuplicateEntryEdit.IsValid = false;
                                DepartmentHeadDuplicateEntryEdit.ErrorMessage = "Please select Process.";
                            }
                            if (sucess)
                            {
                                AuditHeadDuplicateEntryEdit.IsValid = false;
                                AuditHeadDuplicateEntryEdit.ErrorMessage = "Save successfully.";
                            }
                            else
                            {
                                AuditHeadDuplicateEntryEdit.IsValid = false;
                                AuditHeadDuplicateEntryEdit.ErrorMessage = "Error Please try again.";
                            }
                        }
                    }
                    BindEntityAssignment();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdAssignEntities.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }//CustomerBranchId != -1
                else
                {
                    AuditHeadDuplicateEntryEdit.IsValid = false;
                    AuditHeadDuplicateEntryEdit.ErrorMessage = "Please select Unit & SubUnit.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                AuditHeadDuplicateEntryEdit.IsValid = false;
                AuditHeadDuplicateEntryEdit.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindProcesBranchEdit(List<long> CustomerBranchList, int UserID)
        {
            if (ddlProcessAuditHeadEdit.SelectedValue != null)
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ddlProcessAuditHeadEdit.Items.Clear();
                ddlProcessAuditHeadEdit.DataTextField = "Name";
                ddlProcessAuditHeadEdit.DataValueField = "ID";
                ddlProcessAuditHeadEdit.DataSource = ProcessManagement.FillProcessFromRATBDM(Convert.ToInt32(CustomerId), CustomerBranchList);
                //ddlProcessAuditHeadEdit.DataSource = ProcessManagement.FillProcessFromRATBDMEdit(Convert.ToInt32(CustomerId), CustomerBranchList);//, CustomerBranchList, UserID
                ddlProcessAuditHeadEdit.DataBind();
            }
        }
        #endregion

        #region Management Edit
        protected void ddlLegalEntityManagementPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntityManagementPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlLegalEntityManagementPopPupEdit.Items.Count; i++)
                {
                    if (ddlLegalEntityManagementPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlLegalEntityManagementPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity1ManagementPopPupEdit, AHBranchList);
                    if (!string.IsNullOrEmpty(ddlManagementUsersEdit.SelectedValue))
                    {
                        if (ddlManagementUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindManagementProcesBranchEdit(Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue));
                            ddlProcessManagementEdit.ClearSelection();
                            SplitString(GetManagementBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue)), ddlProcessManagementEdit);
                        }
                        else
                        {
                            ddlProcessManagementEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlProcessManagementEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlSubEntity1ManagementPopPupEdit.Items.Count > 0)
                        ddlSubEntity1ManagementPopPupEdit.Items.Clear();

                    if (ddlSubEntity2ManagementPopPupEdit.Items.Count > 0)
                        ddlSubEntity2ManagementPopPupEdit.Items.Clear();

                    if (ddlSubEntity3ManagementPopPupEdit.Items.Count > 0)
                        ddlSubEntity3ManagementPopPupEdit.Items.Clear();

                    if (ddlFilterLocationManagementPopPupEdit.Items.Count > 0)
                        ddlFilterLocationManagementPopPupEdit.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity1ManagementPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1ManagementPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity1ManagementPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity1ManagementPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity1ManagementPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity2ManagementPopPupEdit, AHBranchList);
                    if (!string.IsNullOrEmpty(ddlManagementUsersEdit.SelectedValue))
                    {
                        if (ddlManagementUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindManagementProcesBranchEdit(Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue));
                            ddlProcessManagementEdit.ClearSelection();
                            SplitString(GetManagementBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue)), ddlProcessManagementEdit);
                        }
                        else
                        {
                            ddlProcessManagementEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlProcessManagementEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlSubEntity2ManagementPopPupEdit.Items.Count > 0)
                        ddlSubEntity2ManagementPopPupEdit.ClearSelection();

                    if (ddlSubEntity3ManagementPopPupEdit.Items.Count > 0)
                        ddlSubEntity3ManagementPopPupEdit.ClearSelection();

                    if (ddlFilterLocationManagementPopPupEdit.Items.Count > 0)
                        ddlFilterLocationManagementPopPupEdit.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity2ManagementPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2ManagementPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity2ManagementPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity2ManagementPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity2ManagementPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity3ManagementPopPupEdit, AHBranchList);
                    if (!string.IsNullOrEmpty(ddlManagementUsersEdit.SelectedValue))
                    {
                        if (ddlManagementUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindManagementProcesBranchEdit(Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue));
                            ddlProcessManagementEdit.ClearSelection();
                            SplitString(GetManagementBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue)), ddlProcessManagementEdit);
                        }
                        else
                        {
                            ddlProcessManagementEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlProcessManagementEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlSubEntity3ManagementPopPupEdit.Items.Count > 0)
                        ddlSubEntity3ManagementPopPupEdit.ClearSelection();

                    if (ddlFilterLocationManagementPopPupEdit.Items.Count > 0)
                        ddlFilterLocationManagementPopPupEdit.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity3ManagementPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3ManagementPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity3ManagementPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity3ManagementPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity3ManagementPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlFilterLocationManagementPopPupEdit, AHBranchList);
                    if (!string.IsNullOrEmpty(ddlManagementUsersEdit.SelectedValue))
                    {
                        if (ddlManagementUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindManagementProcesBranchEdit(Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue));
                            ddlProcessManagementEdit.ClearSelection();
                            SplitString(GetManagementBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue)), ddlProcessManagementEdit);
                        }
                        else
                        {
                            ddlProcessManagementEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlProcessManagementEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlFilterLocationManagementPopPupEdit.Items.Count > 0)
                        ddlFilterLocationManagementPopPupEdit.ClearSelection();
                }
            }
        }
        protected void ddlFilterLocationManagementPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocationManagementPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlFilterLocationManagementPopPupEdit.Items.Count; i++)
                {
                    if (ddlFilterLocationManagementPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlFilterLocationManagementPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    if (!string.IsNullOrEmpty(ddlManagementUsersEdit.SelectedValue))
                    {
                        if (ddlManagementUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindManagementProcesBranchEdit(Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue));
                            ddlProcessManagementEdit.ClearSelection();
                            SplitString(GetManagementBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue)), ddlProcessManagementEdit);
                        }
                        else
                        {
                            ddlProcessManagementEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlProcessManagementEdit.Items.Clear();
                    }
                }
            }
        }
        protected void ddlManagementUsersEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> AHBranchList = new List<long>();
            AHBranchList.Clear();
            if (ddlLegalEntityManagementPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlLegalEntityManagementPopPupEdit.Items.Count; i++)
                {
                    if (ddlLegalEntityManagementPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlLegalEntityManagementPopPupEdit.Items[i].Value));
                    }
                }
            }

            //int CustomerBranchId = -1;
            //if (!string.IsNullOrEmpty(ddlLegalEntityManagementPopPupEdit.SelectedValue))
            //{
            //    if (ddlLegalEntityManagementPopPupEdit.SelectedValue != "-1")
            //    {
            //        CustomerBranchId = Convert.ToInt32(ddlLegalEntityManagementPopPupEdit.SelectedValue);
            //    }
            //}
            //if (!string.IsNullOrEmpty(ddlSubEntity1ManagementPopPupEdit.SelectedValue))
            //{
            //    if (ddlSubEntity1ManagementPopPupEdit.SelectedValue != "-1")
            //    {
            //        CustomerBranchId = Convert.ToInt32(ddlSubEntity1ManagementPopPupEdit.SelectedValue);
            //    }
            //}
            if (ddlSubEntity1ManagementPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlSubEntity1ManagementPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity1ManagementPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity1ManagementPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (ddlSubEntity2ManagementPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlSubEntity2ManagementPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity2ManagementPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity2ManagementPopPupEdit.Items[i].Value));
                    }
                }
            }
            //if (!string.IsNullOrEmpty(ddlSubEntity2ManagementPopPupEdit.SelectedValue))
            //{
            //    if (ddlSubEntity2ManagementPopPupEdit.SelectedValue != "-1")
            //    {
            //        CustomerBranchId = Convert.ToInt32(ddlSubEntity2ManagementPopPupEdit.SelectedValue);
            //    }
            //}
            if (ddlSubEntity3ManagementPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlSubEntity3ManagementPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity3ManagementPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity3ManagementPopPupEdit.Items[i].Value));
                    }
                }
            }
            //if (!string.IsNullOrEmpty(ddlSubEntity3ManagementPopPupEdit.SelectedValue))
            //{
            //    if (ddlSubEntity3ManagementPopPupEdit.SelectedValue != "-1")
            //    {
            //        CustomerBranchId = Convert.ToInt32(ddlSubEntity3ManagementPopPupEdit.SelectedValue);
            //    }
            //}
            //if (!string.IsNullOrEmpty(ddlFilterLocationManagementPopPupEdit.SelectedValue))
            //{
            //    if (ddlFilterLocationManagementPopPupEdit.SelectedValue != "-1")
            //    {
            //        CustomerBranchId = Convert.ToInt32(ddlFilterLocationManagementPopPupEdit.SelectedValue);
            //    }
            //}
            if (ddlFilterLocationManagementPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlFilterLocationManagementPopPupEdit.Items.Count; i++)
                {
                    if (ddlFilterLocationManagementPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlFilterLocationManagementPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (AHBranchList.Count > 0)
            {
                if (!string.IsNullOrEmpty(ddlManagementUsersEdit.SelectedValue))
                {
                    if (ddlManagementUsersEdit.SelectedValue != "-1")
                    {
                        if (CustomerId == 0)
                        {
                            CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        }
                        Branchlist.Clear();
                        var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                        var Branchlistloop = Branchlist.ToList();
                        BindManagementProcesBranchEdit(Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue));
                        ddlProcessManagementEdit.ClearSelection();
                        SplitString(GetManagementBranchProcessList(CustomerId, Branchlist, Convert.ToInt32(ddlManagementUsersEdit.SelectedValue)), ddlProcessManagementEdit);
                    }
                }
            }
        }
        private void BindManagementProcesBranchEdit(List<long> CustomerBranchList, int UserID)
        {
            if (ddlProcessManagementEdit.SelectedValue != null)
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ddlProcessManagementEdit.Items.Clear();
                ddlProcessManagementEdit.DataTextField = "Name";
                ddlProcessManagementEdit.DataValueField = "ID";
                //ddlProcessManagementEdit.DataSource = ProcessManagement.FillProcessFromManagementRATBDMEdit(Convert.ToInt32(CustomerId), CustomerBranchList, UserID);
                ddlProcessManagementEdit.DataSource = ProcessManagement.FillProcessFromRATBDM(Convert.ToInt32(CustomerId), CustomerBranchList);
                ddlProcessManagementEdit.DataBind();
            }
        }
        public string GetManagementBranchProcessList(long CustID, List<long> CustomerBranchList, long UserId)
        {
            List<EntitiesAssignmentManagementRisk> EntitiesAssignmentManagementList = new List<EntitiesAssignmentManagementRisk>();
            string BranchProcessList = "";
            EntitiesAssignmentManagementList = AssignEntityAuditManager.GetAllEntitiesAssignmentManagementList(CustID, CustomerBranchList, UserId).ToList();
            EntitiesAssignmentManagementList = EntitiesAssignmentManagementList.OrderBy(entry => entry.ProcessId).ToList();
            if (EntitiesAssignmentManagementList.Count > 0)
            {
                foreach (var row in EntitiesAssignmentManagementList)
                {
                    BranchProcessList += row.ProcessId + " ,";
                }
            }
            return BranchProcessList.Trim(',');
        }
        protected void btnManagementSaveEdit_Click(object sender, EventArgs e)
        {
            try
            {
                List<long> AHBranchList = new List<long>();
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                //int CustomerBranchId = -1;
                //if (!string.IsNullOrEmpty(ddlLegalEntityManagementPopPupEdit.SelectedValue))
                //{
                //    if (ddlLegalEntityManagementPopPupEdit.SelectedValue != "-1")
                //    {
                //        CustomerBranchId = Convert.ToInt32(ddlLegalEntityManagementPopPupEdit.SelectedValue);
                //    }
                //}
                if (ddlLegalEntityManagementPopPupEdit.Items.Count > 0)
                {
                    AHBranchList.Clear();
                    for (int i = 0; i < ddlLegalEntityManagementPopPupEdit.Items.Count; i++)
                    {
                        if (ddlLegalEntityManagementPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlLegalEntityManagementPopPupEdit.Items[i].Value));
                        }
                    }
                }
                //if (!string.IsNullOrEmpty(ddlSubEntity1ManagementPopPupEdit.SelectedValue))
                //{
                //    if (ddlSubEntity1ManagementPopPupEdit.SelectedValue != "-1")
                //    {
                //        CustomerBranchId = Convert.ToInt32(ddlSubEntity1ManagementPopPupEdit.SelectedValue);
                //    }
                //}
                if (ddlSubEntity1ManagementPopPupEdit.Items.Count > 0)
                {
                    AHBranchList.Clear();
                    for (int i = 0; i < ddlSubEntity1ManagementPopPupEdit.Items.Count; i++)
                    {
                        if (ddlSubEntity1ManagementPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity1ManagementPopPupEdit.Items[i].Value));
                        }
                    }
                }
                //if (!string.IsNullOrEmpty(ddlSubEntity2ManagementPopPupEdit.SelectedValue))
                //{
                //    if (ddlSubEntity2ManagementPopPupEdit.SelectedValue != "-1")
                //    {
                //        CustomerBranchId = Convert.ToInt32(ddlSubEntity2ManagementPopPupEdit.SelectedValue);
                //    }
                //}
                if (ddlSubEntity2ManagementPopPupEdit.Items.Count > 0)
                {
                    AHBranchList.Clear();
                    for (int i = 0; i < ddlSubEntity2ManagementPopPupEdit.Items.Count; i++)
                    {
                        if (ddlSubEntity2ManagementPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity2ManagementPopPupEdit.Items[i].Value));
                        }
                    }
                }
                //if (!string.IsNullOrEmpty(ddlSubEntity3ManagementPopPupEdit.SelectedValue))
                //{
                //    if (ddlSubEntity3ManagementPopPupEdit.SelectedValue != "-1")
                //    {
                //        CustomerBranchId = Convert.ToInt32(ddlSubEntity3ManagementPopPupEdit.SelectedValue);
                //    }
                //}
                if (ddlSubEntity3ManagementPopPupEdit.Items.Count > 0)
                {
                    AHBranchList.Clear();
                    for (int i = 0; i < ddlSubEntity3ManagementPopPupEdit.Items.Count; i++)
                    {
                        if (ddlSubEntity3ManagementPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity3ManagementPopPupEdit.Items[i].Value));
                        }
                    }
                }
                //if (!string.IsNullOrEmpty(ddlFilterLocationManagementPopPupEdit.SelectedValue))
                //{
                //    if (ddlFilterLocationManagementPopPupEdit.SelectedValue != "-1")
                //    {
                //        CustomerBranchId = Convert.ToInt32(ddlFilterLocationManagementPopPupEdit.SelectedValue);
                //    }
                //}
                if (ddlFilterLocationManagementPopPupEdit.Items.Count > 0)
                {
                    AHBranchList.Clear();
                    for (int i = 0; i < ddlFilterLocationManagementPopPupEdit.Items.Count; i++)
                    {
                        if (ddlFilterLocationManagementPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlFilterLocationManagementPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                    var Branchlistloop = Branchlist.ToList();
                    bool sucess = false;

                    int userID = -1;
                    if (!string.IsNullOrEmpty(ddlManagementUsersEdit.SelectedValue))
                    {
                        if (ddlManagementUsersEdit.SelectedValue != "-1")
                        {
                            userID = Convert.ToInt32(ddlManagementUsersEdit.SelectedValue);
                            MgmtProcesslist.Clear();
                            for (int i = 0; i < ddlProcessManagementEdit.Items.Count; i++)
                            {
                                if (ddlProcessManagementEdit.Items[i].Selected)
                                {
                                    MgmtProcesslist.Add(Convert.ToInt32(ddlProcessManagementEdit.Items[i].Value));
                                }
                            }
                            sucess = AssignEntityAuditManager.UpdateEntitiesAssignmentManagementRisk(CustomerId, Branchlist, Convert.ToInt32(userID));
                            List<EntitiesAssignmentManagementRisk> EntitiesAssignmentManagementRisklist = new List<EntitiesAssignmentManagementRisk>();
                            if (MgmtProcesslist.Count > 0)
                            {
                                foreach (var aItem in MgmtProcesslist)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        for (int i = 0; i < Branchlist.Count; i++)
                                        {
                                            if (AssignEntityAuditManager.EntitiesAssignmentManagementRiskExist(CustomerId, Branchlist[i], userID, aItem))
                                            {
                                                AssignEntityAuditManager.EditEntitiesAssignmentManagementRiskExist(CustomerId, Branchlist[i], userID, aItem, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                            }
                                            else
                                            {
                                                EntitiesAssignmentManagementRisk objEntitiesAssignmentrisk = new EntitiesAssignmentManagementRisk();
                                                objEntitiesAssignmentrisk.UserID = userID;
                                                objEntitiesAssignmentrisk.CustomerID = CustomerId;
                                                objEntitiesAssignmentrisk.BranchID = Branchlist[i];
                                                objEntitiesAssignmentrisk.CreatedOn = DateTime.Today.Date;
                                                objEntitiesAssignmentrisk.ISACTIVE = true;
                                                objEntitiesAssignmentrisk.ProcessId = aItem;
                                                objEntitiesAssignmentrisk.CretedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                EntitiesAssignmentManagementRisklist.Add(objEntitiesAssignmentrisk);
                                            }
                                        }
                                    }
                                }
                                sucess = AssignEntityAuditManager.CreateEntitiesAssignmentManagementRisklist(EntitiesAssignmentManagementRisklist);
                            }
                            else
                            {
                                ManagementDuplicateEntry.IsValid = false;
                                ManagementDuplicateEntry.ErrorMessage = "Please select Process.";
                            }
                            if (sucess)
                            {
                                ManagementDuplicateEntryEdit.IsValid = false;
                                ManagementDuplicateEntryEdit.ErrorMessage = "Save successfully.";
                            }
                            else
                            {
                                ManagementDuplicateEntryEdit.IsValid = false;
                                ManagementDuplicateEntryEdit.ErrorMessage = "Error Please try again.";
                            }
                        }
                    }
                    BindEntityAssignment();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdAssignEntities.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }//CustomerBranchId != -1
                else
                {
                    ManagementDuplicateEntryEdit.IsValid = false;
                    ManagementDuplicateEntryEdit.ErrorMessage = "Please select Unit & SubUnit.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                ManagementDuplicateEntryEdit.IsValid = false;
                ManagementDuplicateEntryEdit.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #endregion

        #region Department Head Edit
        protected void ddlLegalEntityDepartmentHeadPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntityDepartmentHeadPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlLegalEntityDepartmentHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlLegalEntityDepartmentHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlLegalEntityDepartmentHeadPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity1DepartmentHeadPopPupEdit, AHBranchList);
                    if (!string.IsNullOrEmpty(ddlDepartmentHeadUsersEdit.SelectedValue))
                    {
                        if (ddlDepartmentHeadUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindDepartmentEdit(Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue));
                            ddlDepartmentEdit.ClearSelection();
                            SplitString(GetBranchDepartmentList(CustomerId, Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue)), ddlDepartmentEdit);
                        }
                        else
                        {
                            ddlDepartmentEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlDepartmentEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlSubEntity1DepartmentHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity1DepartmentHeadPopPupEdit.Items.Clear();

                    if (ddlSubEntity2DepartmentHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity2DepartmentHeadPopPupEdit.Items.Clear();

                    if (ddlSubEntity3DepartmentHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity3DepartmentHeadPopPupEdit.Items.Clear();

                    if (ddlFilterLocationDepartmentHeadPopPupEdit.Items.Count > 0)
                        ddlFilterLocationDepartmentHeadPopPupEdit.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity1DepartmentHeadPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1DepartmentHeadPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity1DepartmentHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity1DepartmentHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity1DepartmentHeadPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity2DepartmentHeadPopPupEdit, AHBranchList);
                    if (!string.IsNullOrEmpty(ddlDepartmentHeadUsersEdit.SelectedValue))
                    {
                        if (ddlDepartmentHeadUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindDepartmentEdit(Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue));
                            ddlDepartmentEdit.ClearSelection();
                            SplitString(GetBranchDepartmentList(CustomerId, Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue)), ddlDepartmentEdit);
                        }
                        else
                        {
                            ddlDepartmentEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlDepartmentEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlSubEntity2DepartmentHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity2DepartmentHeadPopPupEdit.ClearSelection();

                    if (ddlSubEntity3DepartmentHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity3DepartmentHeadPopPupEdit.ClearSelection();

                    if (ddlFilterLocationDepartmentHeadPopPupEdit.Items.Count > 0)
                        ddlFilterLocationDepartmentHeadPopPupEdit.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity2DepartmentHeadPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2DepartmentHeadPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity2DepartmentHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity2DepartmentHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity2DepartmentHeadPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity3DepartmentHeadPopPupEdit, AHBranchList);
                    if (!string.IsNullOrEmpty(ddlDepartmentHeadUsersEdit.SelectedValue))
                    {
                        if (ddlDepartmentHeadUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindDepartmentEdit(Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue));
                            ddlDepartmentEdit.ClearSelection();
                            SplitString(GetBranchDepartmentList(CustomerId, Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue)), ddlDepartmentEdit);
                        }
                        else
                        {
                            ddlDepartmentEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlDepartmentEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlSubEntity3DepartmentHeadPopPupEdit.Items.Count > 0)
                        ddlSubEntity3DepartmentHeadPopPupEdit.ClearSelection();

                    if (ddlFilterLocationDepartmentHeadPopPupEdit.Items.Count > 0)
                        ddlFilterLocationDepartmentHeadPopPupEdit.ClearSelection();
                }
            }
        }
        protected void ddlSubEntity3DepartmentHeadPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3DepartmentHeadPopPupEdit.Items.Count > 0)
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                for (int i = 0; i < ddlSubEntity3DepartmentHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity3DepartmentHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity3DepartmentHeadPopPupEdit.Items[i].Value));
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    BindSubEntityData(ddlFilterLocationDepartmentHeadPopPupEdit, AHBranchList);
                    if (!string.IsNullOrEmpty(ddlDepartmentHeadUsersEdit.SelectedValue))
                    {
                        if (ddlDepartmentHeadUsersEdit.SelectedValue != "-1")
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                            var Branchlistloop = Branchlist.ToList();
                            BindDepartmentEdit(Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue));
                            ddlDepartmentEdit.ClearSelection();
                            SplitString(GetBranchDepartmentList(CustomerId, Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue)), ddlDepartmentEdit);
                        }
                        else
                        {
                            ddlDepartmentEdit.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlDepartmentEdit.Items.Clear();
                    }
                }
                else
                {
                    if (ddlFilterLocationDepartmentHeadPopPupEdit.Items.Count > 0)
                        ddlFilterLocationDepartmentHeadPopPupEdit.ClearSelection();
                }
            }
        }
        protected void ddlFilterLocationDepartmentHeadPopPupEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> AHBranchList = new List<long>();
            if (ddlFilterLocationDepartmentHeadPopPupEdit.Items.Count > 0)
            {
                AHBranchList.Clear();
                for (int i = 0; i < ddlFilterLocationDepartmentHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlFilterLocationDepartmentHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlFilterLocationDepartmentHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (!string.IsNullOrEmpty(ddlDepartmentHeadUsersEdit.SelectedValue))
            {
                if (ddlDepartmentHeadUsersEdit.SelectedValue != "-1")
                {
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                    var Branchlistloop = Branchlist.ToList();
                    BindDepartmentEdit(Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue));
                    ddlDepartmentEdit.ClearSelection();
                    SplitString(GetBranchDepartmentList(CustomerId, Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue)), ddlDepartmentEdit);
                }
                else
                {
                    ddlDepartmentEdit.Items.Clear();
                }
            }
            else
            {
                ddlDepartmentEdit.Items.Clear();
            }
        }
        protected void ddlDepartmentHeadUsersEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> AHBranchList = new List<long>();
            AHBranchList.Clear();
            if (ddlLegalEntityDepartmentHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlLegalEntityDepartmentHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlLegalEntityDepartmentHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlLegalEntityDepartmentHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (ddlSubEntity1DepartmentHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlSubEntity1DepartmentHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity1DepartmentHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity1DepartmentHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (ddlSubEntity2DepartmentHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlSubEntity2DepartmentHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity2DepartmentHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity2DepartmentHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (ddlSubEntity3DepartmentHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlSubEntity3DepartmentHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlSubEntity3DepartmentHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlSubEntity3DepartmentHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (ddlFilterLocationDepartmentHeadPopPupEdit.Items.Count > 0)
            {
                for (int i = 0; i < ddlFilterLocationDepartmentHeadPopPupEdit.Items.Count; i++)
                {
                    if (ddlFilterLocationDepartmentHeadPopPupEdit.Items[i].Selected)
                    {
                        AHBranchList.Add(Convert.ToInt32(ddlFilterLocationDepartmentHeadPopPupEdit.Items[i].Value));
                    }
                }
            }
            if (AHBranchList.Count > 0)
            {
                if (!string.IsNullOrEmpty(ddlDepartmentHeadUsersEdit.SelectedValue))
                {
                    if (ddlDepartmentHeadUsersEdit.SelectedValue != "-1")
                    {
                        if (CustomerId == 0)
                        {
                            CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        }
                        Branchlist.Clear();
                        var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                        var Branchlistloop = Branchlist.ToList();
                        BindDepartmentEdit(Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue));
                        ddlDepartmentEdit.ClearSelection();
                        SplitString(GetBranchDepartmentList(CustomerId, Branchlist, Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue)), ddlDepartmentEdit);
                    }
                }
            }
        }
        public string GetBranchDepartmentList(long CustID, List<long> CustomerBranchList, long UserId)
        {
            List<EntitiesAssignmentDepartmentHead> EntitiesAssignmentDepartmentHeadList = new List<EntitiesAssignmentDepartmentHead>();
            string DepartmentHeadList = "";
            EntitiesAssignmentDepartmentHeadList = AssignEntityAuditManager.GetAllEntitiesAssignmentDepartmentHeadList(CustID, CustomerBranchList, UserId).ToList();
            EntitiesAssignmentDepartmentHeadList = EntitiesAssignmentDepartmentHeadList.OrderBy(entry => entry.DepartmentID).ToList();
            if (EntitiesAssignmentDepartmentHeadList.Count > 0)
            {
                foreach (var row in EntitiesAssignmentDepartmentHeadList)
                {
                    DepartmentHeadList += row.DepartmentID + " ,";
                }
            }
            return DepartmentHeadList.Trim(',');
        }
        private void BindDepartmentEdit(List<long> CustomerBranchList, int UserID)
        {
            if (ddlDepartmentEdit.SelectedValue != null)
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                ddlDepartmentEdit.Items.Clear();
                ddlDepartmentEdit.DataTextField = "Name";
                ddlDepartmentEdit.DataValueField = "ID";
                ddlDepartmentEdit.DataSource = CompDeptManagement.FillDepartmentFromRATBDMEdit(Convert.ToInt32(CustomerId), CustomerBranchList, UserID);
                ddlDepartmentEdit.DataBind();
            }
        }
        protected void btnDepartmentHeadSaveEdit_Click(object sender, EventArgs e)
        {
            try
            {
                List<long> AHBranchList = new List<long>();
                AHBranchList.Clear();
                if (ddlLegalEntityDepartmentHeadPopPupEdit.Items.Count > 0)
                {
                    for (int i = 0; i < ddlLegalEntityDepartmentHeadPopPupEdit.Items.Count; i++)
                    {
                        if (ddlLegalEntityDepartmentHeadPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlLegalEntityDepartmentHeadPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity1DepartmentHeadPopPupEdit.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity1DepartmentHeadPopPupEdit.Items.Count; i++)
                    {
                        if (ddlSubEntity1DepartmentHeadPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity1DepartmentHeadPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity2DepartmentHeadPopPupEdit.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity2DepartmentHeadPopPupEdit.Items.Count; i++)
                    {
                        if (ddlSubEntity2DepartmentHeadPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity2DepartmentHeadPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (ddlSubEntity3DepartmentHeadPopPupEdit.Items.Count > 0)
                {
                    for (int i = 0; i < ddlSubEntity3DepartmentHeadPopPupEdit.Items.Count; i++)
                    {
                        if (ddlSubEntity3DepartmentHeadPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlSubEntity3DepartmentHeadPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (ddlFilterLocationDepartmentHeadPopPupEdit.Items.Count > 0)
                {
                    for (int i = 0; i < ddlFilterLocationDepartmentHeadPopPupEdit.Items.Count; i++)
                    {
                        if (ddlFilterLocationDepartmentHeadPopPupEdit.Items[i].Selected)
                        {
                            AHBranchList.Add(Convert.ToInt32(ddlFilterLocationDepartmentHeadPopPupEdit.Items[i].Value));
                        }
                    }
                }
                if (AHBranchList.Count > 0)
                {
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchyBranchList(CustomerId, AHBranchList);
                    var Branchlistloop = Branchlist.ToList();
                    bool sucess = false;
                    int userID = -1;
                    if (!string.IsNullOrEmpty(ddlDepartmentHeadUsersEdit.SelectedValue))
                    {
                        if (ddlDepartmentHeadUsersEdit.SelectedValue != "-1")
                        {
                            userID = Convert.ToInt32(ddlDepartmentHeadUsersEdit.SelectedValue);
                            Departmentlist.Clear();
                            for (int i = 0; i < ddlDepartmentEdit.Items.Count; i++)
                            {
                                if (ddlDepartmentEdit.Items[i].Selected)
                                {
                                    Departmentlist.Add(Convert.ToInt32(ddlDepartmentEdit.Items[i].Value));
                                }
                            }
                            sucess = AssignEntityAuditManager.UpdateEntitiesAssignmentDepartmentHead(CustomerId, Branchlist, Convert.ToInt32(userID));
                            List<EntitiesAssignmentDepartmentHead> EntitiesAssignmentDepartmentHeadlist = new List<EntitiesAssignmentDepartmentHead>();
                            if (Departmentlist.Count > 0)
                            {
                                foreach (var aItem in Departmentlist)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        for (int i = 0; i < Branchlist.Count; i++)
                                        {
                                            if (AssignEntityAuditManager.EntitiesAssignmentDepartmentHeadExist(CustomerId, Branchlist[i], userID, Convert.ToInt32(aItem)))
                                            {
                                                AssignEntityAuditManager.EditEntitiesAssignmentDepartmentHeadExist(CustomerId, Branchlist[i], userID, Convert.ToInt32(aItem), com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                            }
                                            else
                                            {
                                                EntitiesAssignmentDepartmentHead objEntitiesAssignmentDepartmentHead = new EntitiesAssignmentDepartmentHead();
                                                objEntitiesAssignmentDepartmentHead.UserID = userID;
                                                objEntitiesAssignmentDepartmentHead.CustomerID = CustomerId;
                                                objEntitiesAssignmentDepartmentHead.BranchID = Branchlist[i];
                                                objEntitiesAssignmentDepartmentHead.CreatedOn = DateTime.Today.Date;
                                                objEntitiesAssignmentDepartmentHead.ISACTIVE = true;
                                                objEntitiesAssignmentDepartmentHead.DepartmentID = Convert.ToInt32(aItem);
                                                objEntitiesAssignmentDepartmentHead.CretedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                EntitiesAssignmentDepartmentHeadlist.Add(objEntitiesAssignmentDepartmentHead);
                                            }
                                        }
                                    }
                                }
                                sucess = AssignEntityAuditManager.CreateEntitiesAssignmentDepartmentHeadlist(EntitiesAssignmentDepartmentHeadlist);
                            }
                            else
                            {
                                DepartmentHeadDuplicateEntryEdit.IsValid = false;
                                DepartmentHeadDuplicateEntryEdit.ErrorMessage = "Please select Department.";
                            }
                            if (sucess)
                            {
                                DepartmentHeadDuplicateEntryEdit.IsValid = false;
                                DepartmentHeadDuplicateEntryEdit.ErrorMessage = "Save successfully.";
                            }
                            else
                            {
                                DepartmentHeadDuplicateEntryEdit.IsValid = false;
                                DepartmentHeadDuplicateEntryEdit.ErrorMessage = "Error Please try again.";
                            }
                        }
                    }
                    BindEntityAssignment();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdAssignEntities.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }//CustomerBranchId != -1
                else
                {
                    DepartmentHeadDuplicateEntryEdit.IsValid = false;
                    DepartmentHeadDuplicateEntryEdit.ErrorMessage = "Please select Unit & SubUnit.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                DepartmentHeadDuplicateEntry.IsValid = false;
                DepartmentHeadDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #endregion

        #endregion


        #region Reassign   


        private void BindProcesBranchReassign(List<long> CustomerBranchList, int UserID)
        {
            if (ddlProcess.SelectedValue != null)
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ddlProcess.Items.Clear();
                ddlProcess.DataTextField = "Name";
                ddlProcess.DataValueField = "ID";
                //ddlProcess.DataSource = ProcessManagement.FillProcessFromRATBDMEdit(Convert.ToInt32(CustomerId), CustomerBranchList, UserID);
                ddlProcess.DataSource = ProcessManagement.FillProcessFromRATBDMEdit(Convert.ToInt32(CustomerId), CustomerBranchList);
                ddlProcess.DataBind();
            }
        }
        protected void btnReAssign_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                lblReAssign.Text = string.Empty;
                ViewState["ReAssignID"] = Portal.Common.AuthenticationHelper.UserID;
                User user = UserManagement.GetByID(Portal.Common.AuthenticationHelper.UserID);
                lblReAssign.Text = user.FirstName + " " + user.LastName;
                BindUserList(CustomerId, ddlUser);
                BindUserList(CustomerId, ddlREAssignUser);
                BindLegalEntityDataPopUp();
                ReAssign.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenModel", "javascript:openModal()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public bool SaveReAssignProcessAuditDetails()
        {
            try
            {
                bool success = false;
                // Tuple < long OldUserID, long NewUserid, long Branchid, long ProcessID>        
                List<Tuple<long, long, long, long>> list = new List<Tuple<long, long, long, long>>();
                for (int i = 0; i < grdReassign.Rows.Count; i++)
                {
                    DropDownList ddlGridReAssignUser = (DropDownList)grdReassign.Rows[i].FindControl("ddlGridReAssignUser");
                    if (ddlGridReAssignUser != null && ddlGridReAssignUser.SelectedValue != "-1" && ddlGridReAssignUser.SelectedValue != "")
                    {
                        string lblBranchID = (grdReassign.Rows[i].FindControl("lblBranchID") as Label).Text;
                        string lblprocessId = (grdReassign.Rows[i].FindControl("lblprocessId") as Label).Text;
                        if (lblBranchID != "" && lblprocessId != "")
                        {

                            list.Add(new Tuple<long, long, long, long>(Convert.ToInt64(ddlUser.SelectedValue), Convert.ToInt64(ddlGridReAssignUser.SelectedValue), Convert.ToInt64(lblBranchID), Convert.ToInt64(lblprocessId)));
                            //success = UserManagementRisk.ReAssignedProcessOwner(Convert.ToInt32(ddlUser.SelectedValue),
                            //    Convert.ToInt32(ddlGridReAssignUser.SelectedValue), Convert.ToInt32(ddlRole.SelectedValue), Convert.ToInt64(AuditID));
                        }
                    }
                }
                if (list.Count > 0)
                {
                    success = UserManagementRisk.ReAssignedProcessOwner(list);
                }
                return success;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        protected void btnReassignAuditSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                string[] confirmValue = Request.Form["confirm_Event_value"].Split(',');

                if (confirmValue[confirmValue.Length - 1] == "Yes")
                {

                    int branchid = -1;
                    if (CustomerId == 0)
                    {
                        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    }
                    if (!String.IsNullOrEmpty(ddlLegalEntityPopup.SelectedValue))
                    {
                        if (ddlLegalEntityPopup.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntityPopup.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity1Popup.SelectedValue))
                    {
                        if (ddlSubEntity1Popup.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1Popup.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity2Popup.SelectedValue))
                    {
                        if (ddlSubEntity2Popup.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2Popup.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity3Popup.SelectedValue))
                    {
                        if (ddlSubEntity3Popup.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3Popup.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlLocationPopup.SelectedValue))
                    {
                        if (ddlLocationPopup.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLocationPopup.SelectedValue);
                        }
                    }
                    //Branchlist.Clear();
                    //GetAllHierarchy(CustomerId, branchid);
                    if (ddlUser.SelectedValue != "-1" && ddlUser.SelectedValue != "")
                    {
                        saveSuccess = SaveReAssignProcessAuditDetails();
                    }
                    else
                    {
                        cvReAssignAudit.IsValid = false;
                        cvReAssignAudit.ErrorMessage = "Please Select User.";
                    }
                    BindReAssingGrid();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divReAssign\").dialog('close')", true);
                    ViewState["ReAssignedEventID"] = null;
                    if (saveSuccess)
                    {
                        cvReAssignAudit.IsValid = false;
                        cvReAssignAudit.ErrorMessage = "Process Re-Assign Details Save Successfully.";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divReAssign\").dialog('close')", true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvReAssignAudit.IsValid = false;
                cvReAssignAudit.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void lBPreviousReAssign_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNoReAssign.Text) > 1)
                {
                    SelectedPageNoReAssign.Text = (Convert.ToInt32(SelectedPageNoReAssign.Text) - 1).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                BindReAssingGrid();
            }
            catch (Exception ex)
            {
            }
        }
        protected void lBNextReAssign_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNoReAssign.Text);
                if (currentPageNo < GetTotalPagesCountReAssign())
                {
                    SelectedPageNoReAssign.Text = (currentPageNo + 1).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                BindReAssingGrid();
            }
            catch (Exception ex)
            {

            }
        }
        public void BindReAssingGrid()
        {
            try
            {
                int UserID = -1;
                if (ViewState["ReAssignID"] != null)
                {
                    UserID = Convert.ToInt32(ViewState["ReAssignID"]);
                }
                Session["TotalRowsReAssging"] = null;
                var AuditList = GetProcessAuditData();
                grdReassign.DataSource = AuditList;
                Session["TotalRowsReAssging"] = AuditList.Count;
                grdReassign.DataBind();
                if (AuditList.Count > 0)
                    divSavePageSummary.Attributes.Add("style", "display:block;");
                else
                    divSavePageSummary.Attributes.Add("style", "display:none;");
                GetPageDisplaySummaryReAssign();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvReAssignAudit.IsValid = false;
                cvReAssignAudit.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private List<Sp_ReassignProcessOwner_Result> GetProcessAuditData()
        {
            int processID = -1;
            int branchid = -1;
            int UserID = -1;
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            if (!String.IsNullOrEmpty(ddlLegalEntityPopup.SelectedValue))
            {
                if (ddlLegalEntityPopup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntityPopup.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity1Popup.SelectedValue))
            {
                if (ddlSubEntity1Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1Popup.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity2Popup.SelectedValue))
            {
                if (ddlSubEntity2Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2Popup.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity3Popup.SelectedValue))
            {
                if (ddlSubEntity3Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3Popup.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlLocationPopup.SelectedValue))
            {
                if (ddlLocationPopup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLocationPopup.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlUser.SelectedValue))
            {
                if (ddlUser.SelectedValue != "-1")
                {
                    UserID = Convert.ToInt32(ddlUser.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    processID = Convert.ToInt32(ddlProcess.SelectedValue);
                }
            }

            Branchlist.Clear();
            GetAllHierarchy(CustomerId, branchid);
            List<Sp_ReassignProcessOwner_Result> AuditLists = new List<Sp_ReassignProcessOwner_Result>();
            AuditLists = UserManagementRisk.GetAllReassignDataProcessOwner(UserID, Branchlist);

            if (processID != -1)
            {
                AuditLists = AuditLists.Where(entry => entry.ProcessId == processID).ToList();
            }
            return AuditLists;
        }
        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ViewState["ReAssignID"] = null;
            if (ddlUser.SelectedValue != "" || ddlUser.SelectedValue != "-1")
            {
                if (ddlUser.SelectedValue != "-1" || ddlUser.SelectedValue != null || ddlUser.SelectedValue != "")
                    ddlREAssignUser.Items.Remove(ddlREAssignUser.Items.FindByValue(ddlUser.SelectedValue));
                ViewState["ReAssignID"] = Convert.ToInt32(ddlUser.SelectedValue);
            }
            BindReAssingGrid();
        }
        public void BindLegalEntityDataPopUp()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlLegalEntityPopup.DataTextField = "Name";
            ddlLegalEntityPopup.DataValueField = "ID";
            ddlLegalEntityPopup.Items.Clear();
            ddlLegalEntityPopup.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntityPopup.DataBind();
            //ddlLegalEntityPopup.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityDataPopUp(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            DRP.DataBind();
            // DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }
        private void BindUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";
                ddllist.DataSource = UserManagement.GetAllUserAM(customerID);
                ddllist.DataBind();
                ddllist.Items.Insert(0, new ListItem("Select Process Owner", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvReAssignAudit.IsValid = false;
                cvReAssignAudit.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdReassign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlGridReAssignUser = (e.Row.FindControl("ddlGridReAssignUser") as DropDownList);

                    if (ddlGridReAssignUser != null)
                    {
                        BindUserList(CustomerId, ddlGridReAssignUser);

                        if (ddlUser.SelectedValue != "-1" || ddlUser.SelectedValue != null || ddlUser.SelectedValue != "")
                            ddlREAssignUser.Items.Remove(ddlREAssignUser.Items.FindByValue(ddlUser.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvReAssignAudit.IsValid = false;
                cvReAssignAudit.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlREAssignUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdReassign.Rows.Count; i++)
                {
                    DropDownList ddlGridReAssignUser = (DropDownList)grdReassign.Rows[i].FindControl("ddlGridReAssignUser");

                    if (ddlGridReAssignUser.Enabled)
                    {
                        ddlGridReAssignUser.ClearSelection();
                        ddlGridReAssignUser.SelectedValue = ddlREAssignUser.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvReAssignAudit.IsValid = false;
                cvReAssignAudit.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlPageSizeReAssign_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNoReAssign.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNoReAssign.Text);
                if (currentPageNo <= GetTotalPagesCountReAssign())
                {
                    SelectedPageNoReAssign.Text = (currentPageNo).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                BindReAssingGrid();
            }
            catch (Exception ex)
            {
            }
        }
        private int GetTotalPagesCountReAssign()
        {
            try
            {
                TotalRowsReAssign.Value = Session["TotalRowsReAssging"].ToString();
                int totalPages = Convert.ToInt32(TotalRowsReAssign.Value) / Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRowsReAssign.Value) % Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void GetPageDisplaySummaryReAssign()
        {
            try
            {
                lTotalCountReAssign.Text = GetTotalPagesCountReAssign().ToString();

                if (lTotalCountReAssign.Text != "0")
                {
                    if (SelectedPageNoReAssign.Text == "")
                        SelectedPageNoReAssign.Text = "1";

                    if (SelectedPageNoReAssign.Text == "0")
                        SelectedPageNoReAssign.Text = "1";
                }
                else if (lTotalCountReAssign.Text == "0")
                {
                    SelectedPageNoReAssign.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void ddlLegalEntityPopUp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntityPopup.SelectedValue))
            {
                if (ddlLegalEntityPopup.SelectedValue != "-1")
                {
                    BindSubEntityDataPopUp(ddlSubEntity1Popup, Convert.ToInt32(ddlLegalEntityPopup.SelectedValue));
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(ddlLegalEntityPopup.SelectedValue));
                    var Branchlistloop = Branchlist.ToList();
                    BindProcesBranchReassign(Branchlist, Convert.ToInt32(ddlUser.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1Popup.Items.Count > 0)
                        ddlSubEntity1Popup.Items.Clear();

                    if (ddlSubEntity2Popup.Items.Count > 0)
                        ddlSubEntity2Popup.Items.Clear();

                    if (ddlSubEntity3Popup.Items.Count > 0)
                        ddlSubEntity3Popup.Items.Clear();

                    if (ddlLocationPopup.Items.Count > 0)
                        ddlLocationPopup.Items.Clear();
                }
                ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity1Popup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1Popup.SelectedValue))
            {
                if (ddlSubEntity1Popup.SelectedValue != "-1")
                {
                    BindSubEntityDataPopUp(ddlSubEntity2Popup, Convert.ToInt32(ddlSubEntity1Popup.SelectedValue));
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(ddlSubEntity1Popup.SelectedValue));
                    var Branchlistloop = Branchlist.ToList();
                    BindProcesBranchReassign(Branchlist, Convert.ToInt32(ddlUser.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2Popup.Items.Count > 0)
                        ddlSubEntity2Popup.Items.Clear();

                    if (ddlSubEntity3Popup.Items.Count > 0)
                        ddlSubEntity3Popup.Items.Clear();

                    if (ddlLocationPopup.Items.Count > 0)
                        ddlLocationPopup.Items.Clear();
                }
                ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity2Popup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2Popup.SelectedValue))
            {
                if (ddlSubEntity2Popup.SelectedValue != "-1")
                {
                    BindSubEntityDataPopUp(ddlSubEntity3Popup, Convert.ToInt32(ddlSubEntity2Popup.SelectedValue));
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(ddlSubEntity2Popup.SelectedValue));
                    var Branchlistloop = Branchlist.ToList();
                    BindProcesBranchReassign(Branchlist, Convert.ToInt32(ddlUser.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3Popup.Items.Count > 0)
                        ddlSubEntity3Popup.Items.Clear();

                    if (ddlLocationPopup.Items.Count > 0)
                        ddlLocationPopup.Items.Clear();
                }
                ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity3Popup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3Popup.SelectedValue))
            {
                if (ddlSubEntity3Popup.SelectedValue != "-1")
                {
                    BindSubEntityDataPopUp(ddlLocationPopup, Convert.ToInt32(ddlSubEntity3Popup.SelectedValue));
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(ddlSubEntity3Popup.SelectedValue));
                    var Branchlistloop = Branchlist.ToList();
                    BindProcesBranchReassign(Branchlist, Convert.ToInt32(ddlUser.SelectedValue));
                }
                else
                {
                    if (ddlLocationPopup.Items.Count > 0)
                        ddlLocationPopup.Items.Clear();
                }
                ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlLocationPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            Branchlist.Clear();
            var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(ddlLocationPopup.SelectedValue));
            var Branchlistloop = Branchlist.ToList();
            BindProcesBranchReassign(Branchlist, Convert.ToInt32(ddlUser.SelectedValue));
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindReAssingGrid();
        }

        #endregion

        protected void upCompliance_Load(object sender, EventArgs e)
        {

        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {

        }
    }
}