﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class FrmReportMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomer(); 
            }
        }
        public void BindCustomer()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLocation.DataTextField = "Name";
            ddlLocation.DataValueField = "ID";
            ddlLocation.Items.Clear();
            ddlLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<Mst_ReportMaster> reportmasterlist = new List<Mst_ReportMaster>();
            string strText = "";
            string strValue = "";
            //GET THE SELECTED VALUE FROM CHECKBOX LIST
            for (int i = 0; i < ChkServicestopitch.Items.Count; i++)
            {
                if (ChkServicestopitch.Items[i].Selected)
                {
                    Mst_ReportMaster reportmaster = new Mst_ReportMaster()
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue),
                        DisplayText = ChkServicestopitch.Items[i].Text,
                        DisplayValue = Convert.ToInt32(ChkServicestopitch.Items[i].Value),
                        IsActive = 1,
                    };
                    reportmasterlist.Add(reportmaster);
                }
            }
        }
    }
}