﻿<%@ Page Title="Audit Assignment" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditAssignment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditAssignment" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 0px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 34px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clsheadergrid, .table tr th label {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        .clsheadergrid, .table tr th input {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        
        .dd_chk_select {
            height: 30px !important;  
            text-align: center;          
            border-radius: 5px;
        }

         div.dd_chk_select div#caption
        {
            margin-top: 5px;
        }
    </style>

    <script type="text/javascript">
        var interval_A = null;
        function fopenpopup() {
            $('#AuditAssignment').modal('show');
            clearInterval(interval_A);
            return true;
        }

        function CloseWin() {
            $('#AuditAssignment').modal('hide');
        };

        //This is used for Close Popup after button click.
        function caller() {
            interval_A = setInterval(CloseWin, 3000);
        };

        function CloseWin1() {
            $('#DivApplyTo').modal('hide');           
        };

        function CloseEvent() {
            $('#AuditAssignment').modal('hide');
            window.location.reload();
        };

        //This is used for Close Popup after button click.
        function caller1() {
            interval_A = setInterval(CloseWin1, 3000);
        };
    </script>

    <script type="text/javascript">

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function OpenApplytoPopUp() {
            $('#DivApplyTo').modal('show');
            clearInterval(interval_A);
        }

        function ApplyToconfirm() {
            
            var confirmed = confirm('Are you sure you want to Apply this item?', 'Are you sure you want to Apply this item?');

            if (confirmed) {
                $('#btnapply').show();
            }
            else {
                caller();
            }
        }

    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Audit Assignment');
        });
        function fintercheck(e) {

            if ($(e).is(':checked')) {
                var extcheckbox = $(e).attr('id').replace('ïnterInnercheck', 'externalInnercheck');
                var dropcheckbox1 = $(e).attr('id').replace('ïnterInnercheck', 'ddlAuditordr');
                document.getElementById(extcheckbox).checked = false;
                $('#' + dropcheckbox1).attr('disabled', 'disabled').trigger('chosen:updated');

            }
        }
        function fextcheck(e) {
            if ($(e).is(':checked')) {
                var innnercheckbox = $(e).attr('id').replace('externalInnercheck', 'ïnterInnercheck');
                var dropcheckbox = $(e).attr('id').replace('externalInnercheck', 'ddlAuditordr');
                document.getElementById(innnercheckbox).checked = false;
                $('#' + dropcheckbox).removeAttr('disabled').trigger('chosen:updated');

            }
        }
        function fintercheckUp(e) {
            if ($(e).is(':checked')) {
                document.getElementById('ContentPlaceHolder1_grdAuditor_Extercheck').checked = false;

                for (var i = 0; i < $('.intdummyclass').find('input').length; i++) {
                    var hsd = $('.intdummyclass').find('input')[i];
                    document.getElementById($(hsd).attr('id')).checked = true;
                    $('#' + $(hsd).attr('id').replace('ïnterInnercheck', 'ddlAuditordr')).attr('disabled', 'disabled').trigger('chosen:updated');
                }

                for (var i = 0; i < $('.extdummyclass').find('input').length; i++) {
                    var hsd1 = $('.extdummyclass').find('input')[i];
                    document.getElementById($(hsd1).attr('id')).checked = false;
                }

            } else {

                for (var i = 0; i < $('.intdummyclass').find('input').length; i++) {
                    var hsd1 = $('.intdummyclass').find('input')[i];
                    document.getElementById($(hsd1).attr('id')).checked = false;
                }
            }

        }
        function fextcheckUp(e) {
            if ($(e).is(':checked')) {
                document.getElementById('ContentPlaceHolder1_grdAuditor_ïntercheck').checked = false;
                for (var i = 0; i < $('.intdummyclass').find('input').length; i++) {
                    var hsd = $('.intdummyclass').find('input')[i];
                    document.getElementById($(hsd).attr('id')).checked = false;
                }

                for (var i = 0; i < $('.extdummyclass').find('input').length; i++) {
                    var hsd1 = $('.extdummyclass').find('input')[i];
                    document.getElementById($(hsd1).attr('id')).checked = true;

                    $('#' + $(hsd1).attr('id').replace('externalInnercheck', 'ddlAuditordr')).removeAttr('disabled').trigger('chosen:updated');
                }


                $('.auddr').removeAttr('disabled').trigger('chosen:updated');
            } else {
                for (var i = 0; i < $('.extdummyclass').find('input').length; i++) {
                    var hsd1 = $('.extdummyclass').find('input')[i];
                    document.getElementById($(hsd1).attr('id')).checked = false;
                }
            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                         <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                            <div class="col-md-2 colpadding0" style="width:30%">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                <asp:ListItem Text="5" Selected="True" />
                                <asp:ListItem Text="10" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                        </div>                                  
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">                                    
                                <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit"
                                      class="form-control m-bot15 select_location" Width="90%" Height="32px" Style="float: left; width:90%;"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                    
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit"
                                     class="form-control m-bot15 select_location" Width="90%" Height="32px" 
                                    AllowSingleDeselect="false" DisableSearchThreshold="3"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                    
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                   
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit"
                                     class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                      
                            </div>                                                                                                 
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                    
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Unit" 
                                    class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                    
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" DataPlaceHolder="Sub Unit" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"
                                     class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                </asp:DropDownListChosen>
                            </div>
                              <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                              <%{%>  
                               <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                <asp:DropDownListChosen runat="server" ID="ddlVerticalID" DataPlaceHolder="Verticals" AutoPostBack="true"
                                 class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"
                                     OnSelectedIndexChanged="ddlVerticalID_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                               </div>
                             <%}%>
                        </div>
                                                                                                                       
                            <div style="text-align:right;" >
                                <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnAddAuditAssignment" OnClick="btnAddAuditAssignment_Click" />                     
                            </div>                
                            <div class="clearfix" style="height:50px"></div>
                            <div class="clearfix"></div>
                             <div style="margin-bottom: 4px">
                                 &nbsp;                 
                                <asp:GridView runat="server" ID="grdAuditor" AutoGenerateColumns="false" AllowSorting="true" OnRowDataBound="grdAuditor_RowDataBound"
                                    OnSorting="grdAuditor_Sorting" DataKeyNames="Id" OnRowCommand="grdAuditor_RowCommand"
                                    PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" 
                                    OnPageIndexChanging="grdAuditor_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                         <ItemTemplate>
                                                          <%#Container.DataItemIndex+1 %>
                                         </ItemTemplate>
                                         </asp:TemplateField>
                                        <asp:TemplateField  HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClient" runat="server" Text='<%# ShowCustomerBranchName((long)Eval("CustomerBranchid")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField  HeaderText="Verticals">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVerticalName" runat="server" Text='<%# Eval("VerticalName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField  HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAssigned" runat="server" Text='<%# ShowAssignedTo((string)Eval("AssignedTo")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField  HeaderText="Auditor">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAuditor" runat="server" Text='<%# ShowExternalAuditor((long)Eval("ExternalAuditorId")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                                                 
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="5%" HeaderText="Action" Visible="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEditAssignment" runat="server" CommandName="EDIT_Assignment" OnClientClick="fopenpopup()"
                                                    CommandArgument='<%# Eval("CustomerBranchid") +","+ Eval("VerticalID") %>' Visible='<%# EditAssignmentVisibleorNot(Convert.ToInt32((Eval("CustomerBranchid"))),Convert.ToInt32((Eval("VerticalID")))) %>'>
                                                    <img src="../../Images/edit_icon_new.png" alt="Edit Audit Assignment" title="Edit Audit Assignment Details" /></asp:LinkButton>                               
                                            </ItemTemplate>                            
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid"   />
                                    <HeaderStyle CssClass="clsheadergrid"    />
                                    <HeaderStyle BackColor="#ECF0F1" />     
                                      <PagerSettings Visible="false" />       
                                    <PagerTemplate>
                                      <%--  <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                                      </PagerTemplate> 
                                      </asp:GridView>
                                 <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float: right;">
                                        <p>Page
                                     <%--       <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="modal fade" id="AuditAssignment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width:1100px;">
            <div class="modal-content" style="height:280px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divAuditorDialog">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional" OnLoad="upPromotor_Load">
                            <ContentTemplate>
                                <div class="col-md-12 colpadding0">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>
                              <div class="col-md-12 colpadding0"> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">  
                                      <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>                                   
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntityPopPup" DataPlaceHolder="Unit" 
                                         class="form-control m-bot15 select_location" Width="90%" Height="32px" Style="float: left; width:90%;"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntityPopPup_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select Unit"
                                    ControlToValidate="ddlLegalEntityPopPup" runat="server" ValidationGroup="PromotorValidationGroup"
                                    Display="None"/>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">  
                                      <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>                                   
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1PopPup" DataPlaceHolder="Sub Unit" 
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1PopPup_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                    
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                   
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2PopPup" DataPlaceHolder="Sub Unit"
                                         class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2PopPup_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                      
                                </div>                                                                                                 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                    
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3PopPup" DataPlaceHolder="Sub Unit" 
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3PopPup_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                    
                                </div>
                             </div>
                             <div class="col-md-12 colpadding0">                             
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">    
                                       <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>                  
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocationPopPup" DataPlaceHolder="Sub Unit" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlFilterLocationPopPup_SelectedIndexChanged"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>
                                 </div> 
                                   <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                              <%{%>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">  
                                     <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>                   
                          
                                         <asp:DropDownCheckBoxes ID="ddlVerticalIDPopPup" runat="server" AutoPostBack="true" Visible="true" CssClass="form-control m-bot15"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                              Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="235" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="130" />
                                            <Texts SelectBoxCaption="Select Vertical" />
                                        </asp:DropDownCheckBoxes>


                                 </div>
                                  <%}%>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlAssignedTo" class="form-control m-bot15" Width="90%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlAssignedTo_SelectedIndexChanged">
                                    <asp:ListItem Text="Internal">Internal</asp:ListItem>
                                    <asp:ListItem Text="External">External</asp:ListItem>
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select Assigned To."
                                    ControlToValidate="ddlAssignedTo" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                    Display="None"/>
                                </div> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <div runat="server" id="DivIsExternal" visible="false">                                          
                                    <asp:DropDownListChosen runat="server" ID="ddlAuditor" class="form-control m-bot15" 
                                        Style="float: left" Width="90%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlAuditor_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    </div>
                                </div>
                             </div>
                            <div style="margin-bottom: 7px">                                                                             
                                <div class="clearfix"></div>
                                <div style="margin-bottom: 7px; margin-left: 410px; margin-top: 10px">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                        ValidationGroup="PromotorValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" OnClientClick="CloseEvent()" CssClass="btn btn-primary" data-dismiss="modal" />
                                     <asp:Button Text="Apply To" runat="server" ID="btnapply" Visible="false" CssClass="btn btn-primary" OnClientClick="OpenApplytoPopUp();" OnClick="btnapply_Click"/>
                                </div>
                                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>
                                <div class="clearfix" style="height: 50px">
                                </div>
                            </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="DivApplyTo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 500px; height: 250px; margin-top: 70px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateApplyToPopUp">
                        <ContentTemplate>
                            <div>
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="ComplianceInstanceValidationGroup" /> 
                                <asp:CustomValidator ID="cvDuplicateEntryApplyPop" runat="server" EnableClientScript="true"
                                  ValidationGroup="ComplianceInstanceValidationGroup" Display="None"  />
                                <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
                            </div>

                            <div style="margin-bottom: 7px; margin-left: 90px;">
                             
                                <asp:DropDownCheckBoxes ID="ddlBranchList" runat="server" AutoPostBack="true" Visible="true"
                                    AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True">
                                    <Style SelectBoxWidth="300" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="250" />
                                    <Texts SelectBoxCaption="Select Unit" />
                                </asp:DropDownCheckBoxes>
                                <div style="margin-bottom: 7px; margin-left: 75px; margin-top: 10px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:Button Text="Save" runat="server" ID="btnSaveApplyto" CssClass="btn btn-primary" OnClick="btnSaveApplyto_Click" />
                                    <asp:Button Text="Close" runat="server" ID="Button4" CssClass="btn btn-primary" data-dismiss="modal" />
                                </div>
                            </div>
                        </ContentTemplate>                        
                    </asp:UpdatePanel>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
